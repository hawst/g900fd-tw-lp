.class Lcom/google/android/videos/tagging/CardsViewAnimationHelper;
.super Ljava/lang/Object;
.source "CardsViewAnimationHelper.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;
.implements Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/CardsViewAnimationHelper$Listener;
    }
.end annotation


# static fields
.field private static final ACCELERATE_DECELERATE_INTERPOLATOR:Landroid/view/animation/AccelerateDecelerateInterpolator;

.field private static final DECELERATE_INTERPOLATOR:Landroid/view/animation/DecelerateInterpolator;


# instance fields
.field private final animationThreshold:F

.field private final cardsView:Lcom/google/android/videos/tagging/CardsView;

.field private collapseEventCause:I

.field private currentAnimator:Landroid/animation/ObjectAnimator;

.field private detectCollapseGesture:Z

.field private detectExpandGesture:Z

.field private expandEventCause:I

.field private fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

.field private final gestureDetector:Lcom/google/android/videos/ui/GestureDetectorPlus;

.field private grabbedFromExpanded:Z

.field private handledInThisDispatch:Z

.field private final knowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

.field private final listener:Lcom/google/android/videos/tagging/CardsViewAnimationHelper$Listener;

.field private final maxAnimationDurationMillis:I

.field private maxTranslationY:F

.field private final minFlingVelocity:I

.field private onInterceptTouchEventResult:Z

.field private onTouchEventResult:Z

.field private pendingState:I

.field private shouldDelegateToFallbackListener:I

.field private state:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->ACCELERATE_DECELERATE_INTERPOLATOR:Landroid/view/animation/AccelerateDecelerateInterpolator;

    .line 52
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->DECELERATE_INTERPOLATOR:Landroid/view/animation/DecelerateInterpolator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;Lcom/google/android/videos/tagging/CardsView;Lcom/google/android/videos/tagging/CardsViewAnimationHelper$Listener;)V
    .locals 7
    .param p1, "knowledgeOverlay"    # Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;
    .param p2, "cardsView"    # Lcom/google/android/videos/tagging/CardsView;
    .param p3, "listener"    # Lcom/google/android/videos/tagging/CardsViewAnimationHelper$Listener;

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput v4, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->state:I

    .line 85
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    iput-object v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->knowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    .line 87
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/tagging/CardsView;

    iput-object v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    .line 88
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/tagging/CardsViewAnimationHelper$Listener;

    iput-object v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->listener:Lcom/google/android/videos/tagging/CardsViewAnimationHelper$Listener;

    .line 90
    invoke-virtual {p1}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 91
    .local v0, "context":Landroid/content/Context;
    new-instance v3, Lcom/google/android/videos/ui/GestureDetectorPlus;

    invoke-direct {v3, v0, p0}, Lcom/google/android/videos/ui/GestureDetectorPlus;-><init>(Landroid/content/Context;Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;)V

    iput-object v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->gestureDetector:Lcom/google/android/videos/ui/GestureDetectorPlus;

    .line 92
    iget-object v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->gestureDetector:Lcom/google/android/videos/ui/GestureDetectorPlus;

    invoke-virtual {v3, v6}, Lcom/google/android/videos/ui/GestureDetectorPlus;->setIsLongpressEnabled(Z)V

    .line 93
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 94
    .local v1, "resources":Landroid/content/res/Resources;
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    new-array v4, v4, [I

    const v5, 0x1010112

    aput v5, v4, v6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 96
    .local v2, "typedArray":Landroid/content/res/TypedArray;
    const/16 v3, 0x12c

    invoke-virtual {v2, v6, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->maxAnimationDurationMillis:I

    .line 97
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 98
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->minFlingVelocity:I

    .line 100
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e019f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    iput v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->animationThreshold:F

    .line 102
    return-void
.end method

.method private collapse(ILandroid/animation/TimeInterpolator;I)V
    .locals 7
    .param p1, "duration"    # I
    .param p2, "interpolator"    # Landroid/animation/TimeInterpolator;
    .param p3, "eventCause"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 149
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v0, v6, v6}, Lcom/google/android/videos/tagging/CardsView;->smoothScrollTo(II)V

    .line 153
    iput v6, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->expandEventCause:I

    .line 154
    iput p3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->collapseEventCause:I

    .line 156
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->knowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->hasContent()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/CardsView;->isAnyCardShown()Z

    move-result v0

    if-nez v0, :cond_2

    .line 157
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->maxTranslationY:F

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/CardsView;->setTranslationY(F)V

    .line 158
    invoke-direct {p0, v4}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->setState(I)V

    .line 162
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v0, v6}, Lcom/google/android/videos/tagging/CardsView;->setChangeAppearingAnimatorEnabled(Z)V

    .line 163
    return-void

    .line 160
    :cond_2
    const/4 v1, 0x5

    iget v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->maxTranslationY:F

    move-object v0, p0

    move v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->startAnimation(IIFILandroid/animation/TimeInterpolator;)V

    goto :goto_0
.end method

.method private collapseOrExpandToNearestState()V
    .locals 5

    .prologue
    const/4 v1, 0x3

    const/4 v0, 0x0

    .line 436
    iget-object v2, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v2}, Lcom/google/android/videos/tagging/CardsView;->getTranslationY()F

    move-result v2

    iget v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->maxTranslationY:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_1

    .line 437
    iget-boolean v2, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->grabbedFromExpanded:Z

    if-eqz v2, :cond_0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->expand(I)V

    .line 443
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 437
    goto :goto_0

    .line 440
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->grabbedFromExpanded:Z

    if-eqz v2, :cond_2

    :goto_2
    invoke-virtual {p0, v1}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->collapse(I)V

    goto :goto_1

    :cond_2
    move v1, v0

    goto :goto_2
.end method

.method private expand(ILandroid/animation/TimeInterpolator;I)V
    .locals 6
    .param p1, "duration"    # I
    .param p2, "interpolator"    # Landroid/animation/TimeInterpolator;
    .param p3, "eventCause"    # I

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 138
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->collapseEventCause:I

    .line 139
    iput p3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->expandEventCause:I

    .line 140
    const/4 v1, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x4

    move-object v0, p0

    move v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->startAnimation(IIFILandroid/animation/TimeInterpolator;)V

    .line 141
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/CardsView;->setChangeAppearingAnimatorEnabled(Z)V

    .line 142
    return-void
.end method

.method private getFlingAnimationDuration(FF)I
    .locals 4
    .param p1, "distance"    # F
    .param p2, "velocity"    # F

    .prologue
    .line 413
    const/high16 v1, 0x40400000    # 3.0f

    div-float v2, p1, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    mul-float v0, v1, v2

    .line 414
    .local v0, "duration":F
    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->maxAnimationDurationMillis:I

    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float/2addr v2, v0

    const/high16 v3, 0x3f000000    # 0.5f

    add-float/2addr v2, v3

    float-to-int v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    return v1
.end method

.method private handleFling(F)Z
    .locals 6
    .param p1, "velocityY"    # F

    .prologue
    const/4 v5, 0x3

    const/4 v2, 0x1

    const/4 v4, 0x2

    const/4 v1, 0x0

    .line 364
    iget-boolean v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    if-eqz v3, :cond_2

    .line 365
    iput-boolean v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    .line 366
    iget v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->minFlingVelocity:I

    int-to-float v3, v3

    cmpg-float v3, p1, v3

    if-gez v3, :cond_1

    .line 403
    :cond_0
    :goto_0
    return v1

    .line 370
    :cond_1
    invoke-direct {p0, v4}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->setState(I)V

    .line 371
    iput-boolean v2, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->grabbedFromExpanded:Z

    .line 374
    :cond_2
    iget-boolean v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->detectExpandGesture:Z

    if-eqz v3, :cond_3

    .line 375
    iput-boolean v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->detectExpandGesture:Z

    .line 376
    iget v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->minFlingVelocity:I

    neg-int v3, v3

    int-to-float v3, v3

    cmpl-float v3, p1, v3

    if-gtz v3, :cond_0

    .line 380
    invoke-direct {p0, v4}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->setState(I)V

    .line 381
    iput-boolean v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->grabbedFromExpanded:Z

    .line 384
    :cond_3
    iget v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->state:I

    if-ne v3, v4, :cond_0

    .line 389
    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->minFlingVelocity:I

    int-to-float v1, v1

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_4

    .line 391
    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->knowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v3}, Lcom/google/android/videos/tagging/CardsView;->getTranslationY()F

    move-result v3

    sub-float v0, v1, v3

    .line 392
    .local v0, "distance":F
    invoke-direct {p0, v0, p1}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->getFlingAnimationDuration(FF)I

    move-result v1

    sget-object v3, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->DECELERATE_INTERPOLATOR:Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p0, v1, v3, v5}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->collapse(ILandroid/animation/TimeInterpolator;I)V

    .end local v0    # "distance":F
    :goto_1
    move v1, v2

    .line 403
    goto :goto_0

    .line 394
    :cond_4
    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->minFlingVelocity:I

    neg-int v1, v1

    int-to-float v1, v1

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_5

    .line 396
    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/CardsView;->getTranslationY()F

    move-result v0

    .line 397
    .restart local v0    # "distance":F
    invoke-direct {p0, v0, p1}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->getFlingAnimationDuration(FF)I

    move-result v1

    sget-object v3, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->DECELERATE_INTERPOLATOR:Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p0, v1, v3, v5}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->expand(ILandroid/animation/TimeInterpolator;I)V

    goto :goto_1

    .line 401
    .end local v0    # "distance":F
    :cond_5
    invoke-direct {p0}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->collapseOrExpandToNearestState()V

    goto :goto_1
.end method

.method private handleScroll(FF)Z
    .locals 7
    .param p1, "distanceX"    # F
    .param p2, "distanceY"    # F

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 297
    iget-boolean v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    if-eqz v3, :cond_2

    .line 298
    iput-boolean v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    .line 299
    cmpl-float v3, p2, v5

    if-gez v3, :cond_0

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1

    .line 330
    :cond_0
    :goto_0
    return v1

    .line 303
    :cond_1
    invoke-direct {p0, v6}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->setState(I)V

    .line 304
    iput-boolean v2, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->grabbedFromExpanded:Z

    .line 307
    :cond_2
    iget-boolean v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->detectExpandGesture:Z

    if-eqz v3, :cond_3

    .line 308
    iput-boolean v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->detectExpandGesture:Z

    .line 309
    cmpg-float v3, p2, v5

    if-lez v3, :cond_0

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_0

    .line 313
    invoke-direct {p0, v6}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->setState(I)V

    .line 314
    iput-boolean v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->grabbedFromExpanded:Z

    .line 317
    :cond_3
    iget v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->state:I

    if-ne v3, v6, :cond_0

    .line 322
    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/CardsView;->getTranslationY()F

    move-result v1

    sub-float v0, v1, p2

    .line 323
    .local v0, "newTranslationY":F
    cmpg-float v1, v0, v5

    if-gez v1, :cond_5

    .line 324
    const/4 v0, 0x0

    .line 328
    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/tagging/CardsView;->setTranslationY(F)V

    .line 329
    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->reportCollapseProgress(F)V

    move v1, v2

    .line 330
    goto :goto_0

    .line 325
    :cond_5
    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->maxTranslationY:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_4

    .line 326
    iget v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->maxTranslationY:F

    goto :goto_1
.end method

.method private inCardsViewColumn(F)Z
    .locals 1
    .param p1, "x"    # F

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/CardsView;->getLeft()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/CardsView;->getRight()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private reportCollapseProgress(F)V
    .locals 2
    .param p1, "translationY"    # F

    .prologue
    .line 334
    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->maxTranslationY:F

    div-float v0, p1, v1

    .line 335
    .local v0, "progress":F
    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->listener:Lcom/google/android/videos/tagging/CardsViewAnimationHelper$Listener;

    invoke-interface {v1, v0}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper$Listener;->onCardListCollapseProgress(F)V

    .line 336
    return-void
.end method

.method private setState(I)V
    .locals 3
    .param p1, "newState"    # I

    .prologue
    const/4 v2, 0x0

    .line 183
    iget v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->state:I

    if-ne v0, p1, :cond_0

    .line 197
    :goto_0
    return-void

    .line 186
    :cond_0
    iput p1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->state:I

    .line 187
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 189
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->listener:Lcom/google/android/videos/tagging/CardsViewAnimationHelper$Listener;

    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->collapseEventCause:I

    invoke-interface {v0, v1}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper$Listener;->onCardListCollapsed(I)V

    .line 190
    iput v2, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->collapseEventCause:I

    goto :goto_0

    .line 193
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->listener:Lcom/google/android/videos/tagging/CardsViewAnimationHelper$Listener;

    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->expandEventCause:I

    invoke-interface {v0, v1}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper$Listener;->onCardListExpanded(I)V

    .line 194
    iput v2, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->expandEventCause:I

    goto :goto_0

    .line 187
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private startAnimation(IIFILandroid/animation/TimeInterpolator;)V
    .locals 4
    .param p1, "startState"    # I
    .param p2, "duration"    # I
    .param p3, "targetTranslationY"    # F
    .param p4, "endState"    # I
    .param p5, "interpolator"    # Landroid/animation/TimeInterpolator;

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/CardsView;->getTranslationY()F

    move-result v0

    sub-float/2addr v0, p3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->animationThreshold:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    const-string v1, "translationY"

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput p3, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    .line 169
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 170
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 171
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 172
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 173
    iput p4, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->pendingState:I

    .line 174
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->setState(I)V

    .line 175
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 180
    :goto_0
    return-void

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v0, p3}, Lcom/google/android/videos/tagging/CardsView;->setTranslationY(F)V

    .line 178
    invoke-direct {p0, p4}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->setState(I)V

    goto :goto_0
.end method


# virtual methods
.method public collapse(I)V
    .locals 2
    .param p1, "eventCause"    # I

    .prologue
    .line 145
    iget v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->maxAnimationDurationMillis:I

    sget-object v1, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->ACCELERATE_DECELERATE_INTERPOLATOR:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->collapse(ILandroid/animation/TimeInterpolator;I)V

    .line 146
    return-void
.end method

.method public expand(I)V
    .locals 2
    .param p1, "eventCause"    # I

    .prologue
    .line 131
    iget v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->maxAnimationDurationMillis:I

    sget-object v1, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->ACCELERATE_DECELERATE_INTERPOLATOR:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->expand(ILandroid/animation/TimeInterpolator;I)V

    .line 132
    return-void
.end method

.method public isExpanded()Z
    .locals 2

    .prologue
    .line 127
    iget v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->state:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 527
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-ne v0, p1, :cond_0

    .line 528
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    .line 530
    :cond_0
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-ne v0, p1, :cond_0

    .line 520
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    .line 521
    iget v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->pendingState:I

    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->setState(I)V

    .line 523
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 535
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 515
    return-void
.end method

.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-ne v0, p1, :cond_0

    .line 540
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->reportCollapseProgress(F)V

    .line 542
    :cond_0
    return-void
.end method

.method public onCancel(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    .line 447
    iput-boolean v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    .line 448
    iput-boolean v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->detectExpandGesture:Z

    .line 449
    const/4 v0, 0x0

    .line 450
    .local v0, "handled":Z
    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->state:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 452
    iget-boolean v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->grabbedFromExpanded:Z

    if-eqz v1, :cond_2

    .line 453
    invoke-virtual {p0, v3}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->expand(I)V

    .line 457
    :goto_0
    const/4 v0, 0x1

    .line 461
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    if-eqz v1, :cond_1

    .line 462
    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    invoke-interface {v1, p1}, Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;->onCancel(Landroid/view/MotionEvent;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 464
    :cond_1
    return v0

    .line 455
    :cond_2
    invoke-virtual {p0, v3}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->collapse(I)V

    goto :goto_0
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    .line 480
    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    invoke-interface {v1, p1}, Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 481
    iput v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    .line 484
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    .line 489
    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    invoke-interface {v1, p1}, Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 490
    iput v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    .line 493
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 224
    iput-boolean v4, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->onTouchEventResult:Z

    .line 226
    iput-boolean v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->onInterceptTouchEventResult:Z

    .line 227
    iput-boolean v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    .line 228
    iput-boolean v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->detectExpandGesture:Z

    .line 229
    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    iput v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    .line 232
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 233
    .local v0, "x":F
    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->inCardsViewColumn(F)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 234
    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->state:I

    const/4 v5, 0x4

    if-ne v1, v5, :cond_3

    .line 236
    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/CardsView;->getScrollY()I

    move-result v1

    if-nez v1, :cond_2

    move v1, v4

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    .line 238
    iput v2, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    .line 252
    :cond_0
    :goto_2
    return v3

    .end local v0    # "x":F
    :cond_1
    move v1, v3

    .line 229
    goto :goto_0

    .restart local v0    # "x":F
    :cond_2
    move v1, v3

    .line 236
    goto :goto_1

    .line 244
    :cond_3
    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->state:I

    if-ne v1, v4, :cond_5

    :goto_3
    iput-boolean v4, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->detectExpandGesture:Z

    .line 248
    :cond_4
    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    if-eq v1, v2, :cond_0

    .line 249
    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    invoke-interface {v1, p1}, Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;->onDown(Landroid/view/MotionEvent;)Z

    goto :goto_2

    :cond_5
    move v4, v3

    .line 244
    goto :goto_3
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 340
    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    if-ne v1, v3, :cond_0

    .line 341
    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    .line 342
    .local v0, "handled":Z
    iput-boolean v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->onInterceptTouchEventResult:Z

    .line 343
    iput-boolean v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->onTouchEventResult:Z

    .line 358
    :goto_0
    return v2

    .line 347
    .end local v0    # "handled":Z
    :cond_0
    invoke-direct {p0, p4}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->handleFling(F)Z

    move-result v0

    .line 348
    .restart local v0    # "handled":Z
    if-eqz v0, :cond_2

    .line 349
    iput v4, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    .line 356
    :cond_1
    :goto_1
    iput-boolean v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->onInterceptTouchEventResult:Z

    .line 357
    iput-boolean v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->onTouchEventResult:Z

    goto :goto_0

    .line 350
    :cond_2
    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    if-eq v1, v4, :cond_1

    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 352
    iput v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    .line 353
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->gestureDetector:Lcom/google/android/videos/ui/GestureDetectorPlus;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/GestureDetectorPlus;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 205
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->handledInThisDispatch:Z

    .line 206
    iget-boolean v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->onInterceptTouchEventResult:Z

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 498
    iget v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 499
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    invoke-interface {v0, p1}, Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    .line 501
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 275
    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    if-ne v1, v3, :cond_0

    .line 276
    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    .line 277
    .local v0, "handled":Z
    iput-boolean v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->onInterceptTouchEventResult:Z

    .line 278
    iput-boolean v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->onTouchEventResult:Z

    .line 293
    :goto_0
    return v2

    .line 282
    .end local v0    # "handled":Z
    :cond_0
    invoke-direct {p0, p3, p4}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->handleScroll(FF)Z

    move-result v0

    .line 283
    .restart local v0    # "handled":Z
    if-eqz v0, :cond_2

    .line 284
    iput v4, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    .line 291
    :cond_1
    :goto_1
    iput-boolean v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->onInterceptTouchEventResult:Z

    .line 292
    iput-boolean v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->onTouchEventResult:Z

    goto :goto_0

    .line 285
    :cond_2
    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    if-eq v1, v4, :cond_1

    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 287
    iput v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    .line 288
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 505
    iget v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 506
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    invoke-interface {v0, p1}, Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;->onShowPress(Landroid/view/MotionEvent;)V

    .line 508
    :cond_0
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 475
    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 258
    iput-boolean v2, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->onInterceptTouchEventResult:Z

    .line 259
    iput-boolean v2, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->onTouchEventResult:Z

    .line 261
    iget v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 262
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    invoke-interface {v0, p1}, Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    .line 270
    :cond_0
    :goto_0
    return v2

    .line 266
    :cond_1
    iget v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->shouldDelegateToFallbackListener:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 267
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    invoke-interface {v0, p1}, Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    goto :goto_0
.end method

.method public onStartDispatchTouchEvent()V
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->handledInThisDispatch:Z

    .line 201
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 210
    iget-boolean v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->handledInThisDispatch:Z

    if-nez v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->gestureDetector:Lcom/google/android/videos/ui/GestureDetectorPlus;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/GestureDetectorPlus;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 212
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->handledInThisDispatch:Z

    .line 214
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->onTouchEventResult:Z

    return v0
.end method

.method public onUp(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 419
    iput-boolean v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->detectCollapseGesture:Z

    .line 420
    iput-boolean v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->detectExpandGesture:Z

    .line 421
    const/4 v0, 0x0

    .line 422
    .local v0, "handled":Z
    iget v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->state:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 424
    invoke-direct {p0}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->collapseOrExpandToNearestState()V

    .line 425
    const/4 v0, 0x1

    .line 429
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    if-eqz v1, :cond_1

    .line 430
    iget-object v1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    invoke-interface {v1, p1}, Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;->onUp(Landroid/view/MotionEvent;)Z

    .line 432
    :cond_1
    return v0
.end method

.method public setFallbackGestureListener(Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->fallbackGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    .line 106
    return-void
.end method

.method public updateMaxTranslationY()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 109
    iget-object v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->knowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    invoke-virtual {v3}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->getHeight()I

    move-result v1

    .line 110
    .local v1, "height":I
    iget-object v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v3}, Lcom/google/android/videos/tagging/CardsView;->getTopInset()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v4}, Lcom/google/android/videos/tagging/CardsView;->getPaddingBottom()I

    move-result v4

    add-int v2, v3, v4

    .line 111
    .local v2, "inset":I
    iget-object v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v3}, Lcom/google/android/videos/tagging/CardsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e019e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 113
    .local v0, "collapsedHeight":I
    sub-int v3, v1, v2

    sub-int/2addr v3, v0

    invoke-static {v8, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    int-to-float v3, v3

    iput v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->maxTranslationY:F

    .line 114
    iget v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->state:I

    sparse-switch v3, :sswitch_data_0

    .line 124
    :goto_0
    return-void

    .line 116
    :sswitch_0
    iget-object v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    iget v4, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->maxTranslationY:F

    invoke-virtual {v3, v4}, Lcom/google/android/videos/tagging/CardsView;->setTranslationY(F)V

    goto :goto_0

    .line 120
    :sswitch_1
    iget-object v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->getDuration()J

    move-result-wide v4

    iget-object v3, p0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->getCurrentPlayTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    long-to-int v3, v4

    sget-object v4, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->DECELERATE_INTERPOLATOR:Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {p0, v3, v4, v8}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->collapse(ILandroid/animation/TimeInterpolator;I)V

    goto :goto_0

    .line 114
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
    .end sparse-switch
.end method
