.class public Lcom/google/android/videos/welcome/PromoDismisser;
.super Ljava/lang/Object;
.source "PromoDismisser.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/welcome/PromoDismisser$Listener;
    }
.end annotation


# instance fields
.field private listener:Lcom/google/android/videos/welcome/PromoDismisser$Listener;

.field private final preferences:Landroid/content/SharedPreferences;

.field private final promoId:Ljava/lang/String;

.field private final verticalId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/SharedPreferences;)V
    .locals 1
    .param p1, "verticalId"    # Ljava/lang/String;
    .param p2, "promoId"    # Ljava/lang/String;
    .param p3, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/videos/welcome/PromoDismisser;->verticalId:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/google/android/videos/welcome/PromoDismisser;->promoId:Ljava/lang/String;

    .line 26
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/videos/welcome/PromoDismisser;->preferences:Landroid/content/SharedPreferences;

    .line 27
    return-void
.end method


# virtual methods
.method public clearRegisteredListener()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/welcome/PromoDismisser;->setRegisteredListener(Lcom/google/android/videos/welcome/PromoDismisser$Listener;)V

    .line 40
    return-void
.end method

.method public dismissPromo()V
    .locals 5

    .prologue
    .line 43
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 44
    .local v0, "now":J
    iget-object v2, p0, Lcom/google/android/videos/welcome/PromoDismisser;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/videos/welcome/PromoDismisser;->promoId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_promo_dismissed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/videos/welcome/PromoDismisser;->verticalId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_last_promo_dismissed_timestamp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 48
    return-void
.end method

.method public isPromoDismissed()Z
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/videos/welcome/PromoDismisser;->preferences:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/videos/welcome/PromoDismisser;->promoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_promo_dismissed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/videos/welcome/PromoDismisser;->listener:Lcom/google/android/videos/welcome/PromoDismisser$Listener;

    if-nez v0, :cond_1

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    if-eqz p2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/videos/welcome/PromoDismisser;->promoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_promo_dismissed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/videos/welcome/PromoDismisser;->verticalId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_last_promo_dismissed_timestamp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/welcome/PromoDismisser;->listener:Lcom/google/android/videos/welcome/PromoDismisser$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/welcome/PromoDismisser$Listener;->onPromoStateChanged()V

    goto :goto_0
.end method

.method public setRegisteredListener(Lcom/google/android/videos/welcome/PromoDismisser$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/videos/welcome/PromoDismisser$Listener;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/videos/welcome/PromoDismisser;->listener:Lcom/google/android/videos/welcome/PromoDismisser$Listener;

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 31
    iget-object v0, p0, Lcom/google/android/videos/welcome/PromoDismisser;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 35
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/google/android/videos/welcome/PromoDismisser;->listener:Lcom/google/android/videos/welcome/PromoDismisser$Listener;

    .line 36
    return-void

    .line 32
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/welcome/PromoDismisser;->listener:Lcom/google/android/videos/welcome/PromoDismisser$Listener;

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 33
    iget-object v0, p0, Lcom/google/android/videos/welcome/PromoDismisser;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    goto :goto_0
.end method

.method public wasPromoRecentlyDismissedInVertical()Z
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 55
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/videos/welcome/PromoDismisser;->preferences:Landroid/content/SharedPreferences;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/google/android/videos/welcome/PromoDismisser;->verticalId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_last_promo_dismissed_timestamp"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 57
    .local v0, "elapsedTimeSinceLastDismissed":J
    cmp-long v2, v0, v8

    if-ltz v2, :cond_0

    const-wide/32 v2, 0x2932e00

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
