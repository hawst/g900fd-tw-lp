.class final Lcom/google/android/videos/ui/CursorHelper$ResultCallback;
.super Ljava/lang/Object;
.source "CursorHelper.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/CursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ResultCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<TS;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/CursorHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/ui/CursorHelper;)V
    .locals 0

    .prologue
    .line 194
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper$ResultCallback;, "Lcom/google/android/videos/ui/CursorHelper<TS;>.ResultCallback;"
    iput-object p1, p0, Lcom/google/android/videos/ui/CursorHelper$ResultCallback;->this$0:Lcom/google/android/videos/ui/CursorHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/ui/CursorHelper;Lcom/google/android/videos/ui/CursorHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/ui/CursorHelper;
    .param p2, "x1"    # Lcom/google/android/videos/ui/CursorHelper$1;

    .prologue
    .line 194
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper$ResultCallback;, "Lcom/google/android/videos/ui/CursorHelper<TS;>.ResultCallback;"
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/CursorHelper$ResultCallback;-><init>(Lcom/google/android/videos/ui/CursorHelper;)V

    return-void
.end method

.method private processResponse(Ljava/lang/Object;Landroid/database/Cursor;)V
    .locals 2
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 207
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper$ResultCallback;, "Lcom/google/android/videos/ui/CursorHelper<TS;>.ResultCallback;"
    .local p1, "request":Ljava/lang/Object;, "TS;"
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ResultCallback;->this$0:Lcom/google/android/videos/ui/CursorHelper;

    # getter for: Lcom/google/android/videos/ui/CursorHelper;->currentRequest:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/videos/ui/CursorHelper;->access$100(Lcom/google/android/videos/ui/CursorHelper;)Ljava/lang/Object;

    move-result-object v0

    if-eq p1, v0, :cond_1

    .line 208
    if-eqz p2, :cond_0

    .line 209
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ResultCallback;->this$0:Lcom/google/android/videos/ui/CursorHelper;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/videos/ui/CursorHelper;->currentRequest:Ljava/lang/Object;
    invoke-static {v0, v1}, Lcom/google/android/videos/ui/CursorHelper;->access$102(Lcom/google/android/videos/ui/CursorHelper;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ResultCallback;->this$0:Lcom/google/android/videos/ui/CursorHelper;

    # invokes: Lcom/google/android/videos/ui/CursorHelper;->changeCursor(Landroid/database/Cursor;)V
    invoke-static {v0, p2}, Lcom/google/android/videos/ui/CursorHelper;->access$200(Lcom/google/android/videos/ui/CursorHelper;Landroid/database/Cursor;)V

    .line 215
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ResultCallback;->this$0:Lcom/google/android/videos/ui/CursorHelper;

    # getter for: Lcom/google/android/videos/ui/CursorHelper;->pendingCursorUpdate:Z
    invoke-static {v0}, Lcom/google/android/videos/ui/CursorHelper;->access$300(Lcom/google/android/videos/ui/CursorHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ResultCallback;->this$0:Lcom/google/android/videos/ui/CursorHelper;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/videos/ui/CursorHelper;->pendingCursorUpdate:Z
    invoke-static {v0, v1}, Lcom/google/android/videos/ui/CursorHelper;->access$302(Lcom/google/android/videos/ui/CursorHelper;Z)Z

    .line 217
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ResultCallback;->this$0:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper;->scheduleUpdate()V

    goto :goto_0
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 203
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper$ResultCallback;, "Lcom/google/android/videos/ui/CursorHelper<TS;>.ResultCallback;"
    .local p1, "request":Ljava/lang/Object;, "TS;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/ui/CursorHelper$ResultCallback;->processResponse(Ljava/lang/Object;Landroid/database/Cursor;)V

    .line 204
    return-void
.end method

.method public onResponse(Ljava/lang/Object;Landroid/database/Cursor;)V
    .locals 0
    .param p2, "response"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 198
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper$ResultCallback;, "Lcom/google/android/videos/ui/CursorHelper<TS;>.ResultCallback;"
    .local p1, "request":Ljava/lang/Object;, "TS;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/ui/CursorHelper$ResultCallback;->processResponse(Ljava/lang/Object;Landroid/database/Cursor;)V

    .line 199
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 194
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper$ResultCallback;, "Lcom/google/android/videos/ui/CursorHelper<TS;>.ResultCallback;"
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/CursorHelper$ResultCallback;->onResponse(Ljava/lang/Object;Landroid/database/Cursor;)V

    return-void
.end method
