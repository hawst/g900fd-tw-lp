.class public Lcom/google/android/videos/utils/MediaNotMountedException;
.super Ljava/io/IOException;
.source "MediaNotMountedException.java"


# instance fields
.field public final errorCode:I

.field public final errorStringResourceId:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "errorCode"    # I
    .param p2, "errorStringResourceId"    # I

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/io/IOException;-><init>()V

    .line 18
    iput p1, p0, Lcom/google/android/videos/utils/MediaNotMountedException;->errorCode:I

    .line 19
    iput p2, p0, Lcom/google/android/videos/utils/MediaNotMountedException;->errorStringResourceId:I

    .line 20
    return-void
.end method
