.class Lcom/google/android/videos/activity/DmaReconsentFragment$1;
.super Ljava/lang/Object;
.source "DmaReconsentFragment.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/activity/DmaReconsentFragment;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/LinkedAccountRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/DmaReconsentFragment;

.field final synthetic val$activity:Landroid/support/v4/app/FragmentActivity;

.field final synthetic val$unlinkAccounts:Z


# direct methods
.method constructor <init>(Lcom/google/android/videos/activity/DmaReconsentFragment;Landroid/support/v4/app/FragmentActivity;Z)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/google/android/videos/activity/DmaReconsentFragment$1;->this$0:Lcom/google/android/videos/activity/DmaReconsentFragment;

    iput-object p2, p0, Lcom/google/android/videos/activity/DmaReconsentFragment$1;->val$activity:Landroid/support/v4/app/FragmentActivity;

    iput-boolean p3, p0, Lcom/google/android/videos/activity/DmaReconsentFragment$1;->val$unlinkAccounts:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/api/LinkedAccountRequest;Ljava/lang/Exception;)V
    .locals 6
    .param p1, "request"    # Lcom/google/android/videos/api/LinkedAccountRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 177
    iget-object v1, p0, Lcom/google/android/videos/activity/DmaReconsentFragment$1;->val$activity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v1}, Lcom/google/android/videos/activity/DmaReconsentFragment$InProgressDialogFragment;->dismiss(Landroid/support/v4/app/FragmentActivity;)V

    .line 178
    iget-object v1, p0, Lcom/google/android/videos/activity/DmaReconsentFragment$1;->this$0:Lcom/google/android/videos/activity/DmaReconsentFragment;

    const v2, 0x7f0b0234

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/videos/activity/DmaReconsentFragment$1;->this$0:Lcom/google/android/videos/activity/DmaReconsentFragment;

    # getter for: Lcom/google/android/videos/activity/DmaReconsentFragment;->videosGlobals:Lcom/google/android/videos/VideosGlobals;
    invoke-static {v5}, Lcom/google/android/videos/activity/DmaReconsentFragment;->access$200(Lcom/google/android/videos/activity/DmaReconsentFragment;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/videos/VideosGlobals;->getErrorHelper()Lcom/google/android/videos/utils/ErrorHelper;

    move-result-object v5

    invoke-virtual {v5, p2}, Lcom/google/android/videos/utils/ErrorHelper;->humanize(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/activity/DmaReconsentFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 180
    .local v0, "message":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/videos/activity/DmaReconsentFragment$1;->this$0:Lcom/google/android/videos/activity/DmaReconsentFragment;

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/google/android/videos/activity/DmaReconsentFragment$1;->val$activity:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0b010c

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v2

    # setter for: Lcom/google/android/videos/activity/DmaReconsentFragment;->dialog:Landroid/app/AlertDialog;
    invoke-static {v1, v2}, Lcom/google/android/videos/activity/DmaReconsentFragment;->access$302(Lcom/google/android/videos/activity/DmaReconsentFragment;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 184
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 162
    check-cast p1, Lcom/google/android/videos/api/LinkedAccountRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/activity/DmaReconsentFragment$1;->onError(Lcom/google/android/videos/api/LinkedAccountRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/LinkedAccountRequest;Ljava/lang/Void;)V
    .locals 7
    .param p1, "request"    # Lcom/google/android/videos/api/LinkedAccountRequest;
    .param p2, "response"    # Ljava/lang/Void;

    .prologue
    const/4 v6, 0x1

    .line 166
    iget-object v0, p0, Lcom/google/android/videos/activity/DmaReconsentFragment$1;->val$activity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0}, Lcom/google/android/videos/activity/DmaReconsentFragment$InProgressDialogFragment;->dismiss(Landroid/support/v4/app/FragmentActivity;)V

    .line 167
    iget-object v0, p0, Lcom/google/android/videos/activity/DmaReconsentFragment$1;->this$0:Lcom/google/android/videos/activity/DmaReconsentFragment;

    # getter for: Lcom/google/android/videos/activity/DmaReconsentFragment;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;
    invoke-static {v0}, Lcom/google/android/videos/activity/DmaReconsentFragment;->access$100(Lcom/google/android/videos/activity/DmaReconsentFragment;)Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/activity/DmaReconsentFragment$1;->this$0:Lcom/google/android/videos/activity/DmaReconsentFragment;

    # getter for: Lcom/google/android/videos/activity/DmaReconsentFragment;->account:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/activity/DmaReconsentFragment;->access$000(Lcom/google/android/videos/activity/DmaReconsentFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/google/android/videos/async/IgnoreCallback;->get()Lcom/google/android/videos/async/NewCallback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/ConfigurationStore;->syncUserConfiguration(Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V

    .line 168
    iget-boolean v0, p0, Lcom/google/android/videos/activity/DmaReconsentFragment$1;->val$unlinkAccounts:Z

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/google/android/videos/activity/DmaReconsentFragment$1;->val$activity:Landroid/support/v4/app/FragmentActivity;

    iget-object v1, p0, Lcom/google/android/videos/activity/DmaReconsentFragment$1;->val$activity:Landroid/support/v4/app/FragmentActivity;

    const v2, 0x7f0b0233

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/videos/activity/DmaReconsentFragment$1;->this$0:Lcom/google/android/videos/activity/DmaReconsentFragment;

    # getter for: Lcom/google/android/videos/activity/DmaReconsentFragment;->account:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/videos/activity/DmaReconsentFragment;->access$000(Lcom/google/android/videos/activity/DmaReconsentFragment;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lcom/google/android/videos/utils/Util;->showToast(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    .line 173
    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 162
    check-cast p1, Lcom/google/android/videos/api/LinkedAccountRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Void;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/activity/DmaReconsentFragment$1;->onResponse(Lcom/google/android/videos/api/LinkedAccountRequest;Ljava/lang/Void;)V

    return-void
.end method
