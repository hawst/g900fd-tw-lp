.class Lcom/google/android/videos/pinning/DashLicenseTask$Refresh;
.super Lcom/google/android/videos/pinning/DashLicenseTask;
.source "DashLicenseTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pinning/DashLicenseTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Refresh"
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/logging/EventLogger;)V
    .locals 10
    .param p1, "videosApp"    # Lcom/google/android/videos/VideosGlobals;
    .param p2, "key"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p3, "wakeLock"    # Landroid/os/PowerManager$WakeLock;
    .param p4, "wifiLock"    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p5, "listener"    # Lcom/google/android/videos/pinning/Task$Listener;
    .param p6, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;

    .prologue
    .line 139
    const/4 v2, 0x2

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/pinning/DashLicenseTask;-><init>(Lcom/google/android/videos/VideosGlobals;ILcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/logging/EventLogger;ZLcom/google/android/videos/pinning/DashLicenseTask$1;)V

    .line 140
    return-void
.end method

.method private logError(Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 174
    iget-object v1, p0, Lcom/google/android/videos/pinning/DashLicenseTask$Refresh;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v0, p0, Lcom/google/android/videos/pinning/DashLicenseTask$Refresh;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/videos/pinning/DashLicenseTask$Refresh;->getThrowableToLog(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/videos/logging/EventLogger;->onLicenseRefreshError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 175
    return-void
.end method

.method private unpinAndClearLicense()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 167
    invoke-static {}, Lcom/google/android/videos/pinning/PinningDbHelper;->getClearedLicenseContentValues()Landroid/content/ContentValues;

    move-result-object v0

    .line 168
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "pinned"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 169
    const-string v1, "pinning_notification_active"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 170
    iget-object v2, p0, Lcom/google/android/videos/pinning/DashLicenseTask$Refresh;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/pinning/DashLicenseTask$Refresh;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/videos/pinning/DownloadKey;

    invoke-static {v2, v1, v0}, Lcom/google/android/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    .line 171
    return-void
.end method


# virtual methods
.method protected onCompleted()V
    .locals 2

    .prologue
    .line 181
    iget-object v1, p0, Lcom/google/android/videos/pinning/DashLicenseTask$Refresh;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v0, p0, Lcom/google/android/videos/pinning/DashLicenseTask$Refresh;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/EventLogger;->onLicenseRefreshCompleted(Ljava/lang/String;)V

    .line 182
    return-void
.end method

.method protected onError(Ljava/lang/Throwable;ZZ)V
    .locals 0
    .param p1, "cause"    # Ljava/lang/Throwable;
    .param p2, "maxRetries"    # Z
    .param p3, "fatal"    # Z

    .prologue
    .line 186
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/DashLicenseTask$Refresh;->logError(Ljava/lang/Throwable;)V

    .line 187
    return-void
.end method

.method protected onRequestCompleted(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;[B)V
    .locals 4
    .param p1, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    .param p2, "response"    # [B

    .prologue
    .line 144
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 145
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "license_cenc_key_set_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 146
    const-string v1, "license_last_synced_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 147
    const-string v1, "license_force_sync"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 148
    const-string v1, "license_activation"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 149
    iget-object v2, p0, Lcom/google/android/videos/pinning/DashLicenseTask$Refresh;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/pinning/DashLicenseTask$Refresh;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/videos/pinning/DownloadKey;

    invoke-static {v2, v1, v0}, Lcom/google/android/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    .line 150
    return-void
.end method

.method protected onRequestError(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    .param p2, "cause"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;
        }
    .end annotation

    .prologue
    .line 156
    new-instance v0, Lcom/google/android/videos/pinning/Task$TaskException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to refresh "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/pinning/DashLicenseTask$Refresh;->key:Lcom/google/android/videos/pinning/Task$Key;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method protected onRequestImpossible(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 162
    invoke-direct {p0}, Lcom/google/android/videos/pinning/DashLicenseTask$Refresh;->unpinAndClearLicense()V

    .line 163
    invoke-direct {p0, p2}, Lcom/google/android/videos/pinning/DashLicenseTask$Refresh;->logError(Ljava/lang/Throwable;)V

    .line 164
    return-void
.end method
