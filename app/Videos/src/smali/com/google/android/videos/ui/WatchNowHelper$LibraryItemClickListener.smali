.class final Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;
.super Ljava/lang/Object;
.source "WatchNowHelper.java"

# interfaces
.implements Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/WatchNowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LibraryItemClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/WatchNowHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/ui/WatchNowHelper;)V
    .locals 0

    .prologue
    .line 474
    iput-object p1, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/ui/WatchNowHelper;Lcom/google/android/videos/ui/WatchNowHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/ui/WatchNowHelper;
    .param p2, "x1"    # Lcom/google/android/videos/ui/WatchNowHelper$1;

    .prologue
    .line 474
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;-><init>(Lcom/google/android/videos/ui/WatchNowHelper;)V

    return-void
.end method

.method private handleItemClick(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V
    .locals 10
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/VideosDataSource;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "videoId"    # Ljava/lang/String;
    .param p4, "account"    # Ljava/lang/String;
    .param p5, "screenshot"    # Landroid/view/View;

    .prologue
    .line 516
    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getSeasonId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 517
    .local v2, "seasonId":Ljava/lang/String;
    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 518
    .local v1, "showId":Ljava/lang/String;
    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->isActive(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 519
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v0

    const/16 v3, 0xf

    invoke-static {v0, p3, p4, v3}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewMovieDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V

    .line 533
    :goto_0
    return-void

    .line 522
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v0

    const/4 v5, 0x4

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewEpisodeDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 525
    :cond_1
    instance-of v0, p1, Lcom/google/android/videos/adapter/MoviesDataSource;

    if-eqz v0, :cond_2

    move-object v9, p1

    .line 526
    check-cast v9, Lcom/google/android/videos/adapter/MoviesDataSource;

    .line 527
    .local v9, "moviesDataSource":Lcom/google/android/videos/adapter/MoviesDataSource;
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->config:Lcom/google/android/videos/Config;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1400(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-virtual {v9, p2, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->getVideoMetadata(Landroid/database/Cursor;Lcom/google/android/videos/Config;)Lcom/google/android/videos/store/VideoMetadata;

    move-result-object v0

    invoke-virtual {v9, p2}, Lcom/google/android/videos/adapter/MoviesDataSource;->getVideoDownloadStatus(Landroid/database/Cursor;)Lcom/google/android/videos/store/VideoDownloadStatus;

    move-result-object v3

    invoke-direct {p0, p4, v0, v3, p5}, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->startMovieDetails(Ljava/lang/String;Lcom/google/android/videos/store/VideoMetadata;Lcom/google/android/videos/store/VideoDownloadStatus;Landroid/view/View;)V

    goto :goto_0

    .line 530
    .end local v9    # "moviesDataSource":Lcom/google/android/videos/adapter/MoviesDataSource;
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v3}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v3

    const/4 v8, 0x0

    move-object v4, p4

    move-object v5, p3

    move-object v6, v2

    move-object v7, v1

    invoke-static/range {v3 .. v8}, Lcom/google/android/videos/activity/WatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/videos/activity/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private handlePinClick(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/VideosDataSource;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "videoId"    # Ljava/lang/String;
    .param p4, "account"    # Ljava/lang/String;

    .prologue
    .line 501
    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v8

    .line 502
    .local v8, "pinningStatus":Ljava/lang/Integer;
    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->isPinned(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 503
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->pinHelper:Lcom/google/android/videos/ui/PinHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1200(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/ui/PinHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v1}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v1

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getShowTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v7

    move-object v2, p4

    move-object v3, p3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/videos/ui/PinHelper;->onUnpinClicked(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;)V

    .line 512
    :goto_0
    return-void

    .line 506
    :cond_0
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 507
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->showErrorDialog(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;)V

    goto :goto_0

    .line 509
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->pinHelper:Lcom/google/android/videos/ui/PinHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1200(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/ui/PinHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v1}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p4, p3}, Lcom/google/android/videos/ui/PinHelper;->pinVideo(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1300(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/videos/logging/EventLogger;->onPinClick(Z)V

    goto :goto_0
.end method

.method private handleTextboxClick(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V
    .locals 15
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/VideosDataSource;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "videoId"    # Ljava/lang/String;
    .param p4, "account"    # Ljava/lang/String;
    .param p5, "screenshot"    # Landroid/view/View;

    .prologue
    .line 538
    invoke-interface/range {p1 .. p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    .line 539
    .local v5, "showId":Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 540
    iget-object v3, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v3}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v3

    invoke-interface/range {p1 .. p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getSeasonId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v4, p4

    move-object/from16 v7, p3

    invoke-static/range {v3 .. v9}, Lcom/google/android/videos/activity/ShowActivity;->createEpisodeIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/content/Intent;

    move-result-object v11

    .line 542
    .local v11, "intent":Landroid/content/Intent;
    sget v3, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_0

    .line 543
    new-instance v13, Lcom/google/android/videos/activity/ShowActivity$SharedElements;

    invoke-direct {v13}, Lcom/google/android/videos/activity/ShowActivity$SharedElements;-><init>()V

    .line 544
    .local v13, "shared":Lcom/google/android/videos/activity/ShowActivity$SharedElements;
    move-object/from16 v0, p5

    iput-object v0, v13, Lcom/google/android/videos/activity/ShowActivity$SharedElements;->poster:Landroid/view/View;

    .line 545
    iget-object v3, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v3}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v3

    invoke-static {v3, v11, v13}, Lcom/google/android/videos/activity/ShowActivity;->createAnimationBundleV21(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/videos/activity/ShowActivity$SharedElements;)Landroid/os/Bundle;

    move-result-object v10

    .line 546
    .local v10, "bundle":Landroid/os/Bundle;
    iget-object v3, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v3}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v3

    invoke-static {v3, v11, v10}, Landroid/support/v4/app/ActivityCompat;->startActivity(Landroid/app/Activity;Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 563
    .end local v10    # "bundle":Landroid/os/Bundle;
    .end local v11    # "intent":Landroid/content/Intent;
    .end local v13    # "shared":Lcom/google/android/videos/activity/ShowActivity$SharedElements;
    :goto_0
    return-void

    .line 548
    .restart local v11    # "intent":Landroid/content/Intent;
    :cond_0
    iget-object v3, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v3}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v3

    invoke-virtual {v3, v11}, Lcom/google/android/videos/activity/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 551
    .end local v11    # "intent":Landroid/content/Intent;
    :cond_1
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/google/android/videos/adapter/MoviesDataSource;

    if-eqz v3, :cond_2

    move-object/from16 v12, p1

    .line 552
    check-cast v12, Lcom/google/android/videos/adapter/MoviesDataSource;

    .line 553
    .local v12, "moviesDataSource":Lcom/google/android/videos/adapter/MoviesDataSource;
    iget-object v3, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->config:Lcom/google/android/videos/Config;
    invoke-static {v3}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1400(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/Config;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v12, v0, v3}, Lcom/google/android/videos/adapter/MoviesDataSource;->getVideoMetadata(Landroid/database/Cursor;Lcom/google/android/videos/Config;)Lcom/google/android/videos/store/VideoMetadata;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->getVideoDownloadStatus(Landroid/database/Cursor;)Lcom/google/android/videos/store/VideoDownloadStatus;

    move-result-object v4

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-direct {p0, v0, v3, v4, v1}, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->startMovieDetails(Ljava/lang/String;Lcom/google/android/videos/store/VideoMetadata;Lcom/google/android/videos/store/VideoDownloadStatus;Landroid/view/View;)V

    goto :goto_0

    .line 555
    .end local v12    # "moviesDataSource":Lcom/google/android/videos/adapter/MoviesDataSource;
    :cond_2
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/google/android/videos/adapter/WatchNowDataSource;

    if-eqz v3, :cond_3

    move-object/from16 v14, p1

    .line 556
    check-cast v14, Lcom/google/android/videos/adapter/WatchNowDataSource;

    .line 557
    .local v14, "watchNowDataSource":Lcom/google/android/videos/adapter/WatchNowDataSource;
    iget-object v3, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->config:Lcom/google/android/videos/Config;
    invoke-static {v3}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1400(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/Config;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v14, v0, v3}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getVideoMetadata(Landroid/database/Cursor;Lcom/google/android/videos/Config;)Lcom/google/android/videos/store/VideoMetadata;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getVideoDownloadStatus(Landroid/database/Cursor;)Lcom/google/android/videos/store/VideoDownloadStatus;

    move-result-object v4

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    invoke-direct {p0, v0, v3, v4, v1}, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->startMovieDetails(Ljava/lang/String;Lcom/google/android/videos/store/VideoMetadata;Lcom/google/android/videos/store/VideoDownloadStatus;Landroid/view/View;)V

    goto :goto_0

    .line 560
    .end local v14    # "watchNowDataSource":Lcom/google/android/videos/adapter/WatchNowDataSource;
    :cond_3
    move-object/from16 v0, p4

    move-object/from16 v1, p3

    move-object/from16 v2, p5

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->startMovieDetails(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    goto :goto_0
.end method

.method private showErrorDialog(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;)V
    .locals 7
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/VideosDataSource;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 590
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$900(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/ui/SyncHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v1

    .line 591
    .local v1, "account":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 600
    :goto_0
    return-void

    .line 594
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v0

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getDownloadSize(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getPinningDrmErrorCode(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->isRental(Landroid/database/Cursor;)Z

    move-result v6

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/ui/PinHelper;->showErrorDialog(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Integer;Z)V

    goto :goto_0
.end method

.method private startMovieDetails(Landroid/content/Intent;Landroid/view/View;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "screenshot"    # Landroid/view/View;

    .prologue
    .line 578
    sget v2, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_0

    .line 579
    new-instance v1, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;

    invoke-direct {v1}, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;-><init>()V

    .line 580
    .local v1, "shared":Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v2}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/activity/HomeActivity;->getCurrentPlayHeaderListLayout()Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;->playHeaderListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 581
    iput-object p2, v1, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;->screenshot:Landroid/view/View;

    .line 582
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v2}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v2

    invoke-static {v2, p1, v1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->createAnimationBundleV21(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;)Landroid/os/Bundle;

    move-result-object v0

    .line 583
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v2}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v2

    invoke-static {v2, p1, v0}, Landroid/support/v4/app/ActivityCompat;->startActivity(Landroid/app/Activity;Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 587
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "shared":Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;
    :goto_0
    return-void

    .line 585
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v2}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/videos/activity/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private startMovieDetails(Ljava/lang/String;Lcom/google/android/videos/store/VideoMetadata;Lcom/google/android/videos/store/VideoDownloadStatus;Landroid/view/View;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "metadata"    # Lcom/google/android/videos/store/VideoMetadata;
    .param p3, "downloadStatus"    # Lcom/google/android/videos/store/VideoDownloadStatus;
    .param p4, "screenshot"    # Landroid/view/View;

    .prologue
    .line 572
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v1}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v1

    invoke-static {v1, p1, p2, p3}, Lcom/google/android/videos/activity/MovieDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/videos/store/VideoMetadata;Lcom/google/android/videos/store/VideoDownloadStatus;)Landroid/content/Intent;

    move-result-object v0

    .line 574
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0, v0, p4}, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->startMovieDetails(Landroid/content/Intent;Landroid/view/View;)V

    .line 575
    return-void
.end method

.method private startMovieDetails(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "screenshot"    # Landroid/view/View;

    .prologue
    .line 566
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;
    invoke-static {v1}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1100(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/activity/HomeActivity;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/google/android/videos/activity/MovieDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 567
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0, v0, p3}, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->startMovieDetails(Landroid/content/Intent;Landroid/view/View;)V

    .line 568
    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/google/android/videos/adapter/DataSource;ILandroid/view/View;)V
    .locals 6
    .param p2, "position"    # I
    .param p3, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/adapter/DataSource",
            "<*>;I",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 478
    .local p1, "dataSource":Lcom/google/android/videos/adapter/DataSource;, "Lcom/google/android/videos/adapter/DataSource<*>;"
    move-object v1, p1

    check-cast v1, Lcom/google/android/videos/adapter/VideosDataSource;

    .line 479
    .local v1, "videosDataSource":Lcom/google/android/videos/adapter/VideosDataSource;
    invoke-interface {v1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    .line 480
    .local v2, "cursor":Landroid/database/Cursor;
    invoke-interface {v1, v2}, Lcom/google/android/videos/adapter/VideosDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 481
    .local v3, "videoId":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$900(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/ui/SyncHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v4

    .line 483
    .local v4, "account":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 492
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->this$0:Lcom/google/android/videos/ui/WatchNowHelper;

    # getter for: Lcom/google/android/videos/ui/WatchNowHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;
    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowHelper;->access$1000(Lcom/google/android/videos/ui/WatchNowHelper;)Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-result-object v0

    invoke-static {p3}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->findUiElementNode(Landroid/view/View;)Lcom/google/android/videos/logging/UiElementNode;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendClickEvent(Lcom/google/android/videos/logging/UiElementNode;)V

    .line 493
    invoke-static {p3}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->findThumbnailView(Landroid/view/View;)Landroid/view/View;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->handleItemClick(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    .line 497
    :goto_0
    return-void

    .line 485
    :sswitch_0
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->handlePinClick(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 488
    :sswitch_1
    invoke-static {p3}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->findThumbnailView(Landroid/view/View;)Landroid/view/View;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/ui/WatchNowHelper$LibraryItemClickListener;->handleTextboxClick(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    goto :goto_0

    .line 483
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f00d1 -> :sswitch_0
        0x7f0f00dc -> :sswitch_1
    .end sparse-switch
.end method
