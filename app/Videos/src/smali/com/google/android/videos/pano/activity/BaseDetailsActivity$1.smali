.class Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;
.super Lcom/google/android/videos/ui/TransitionUtil$TransitionListenerAdapter;
.source "BaseDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->setupNormalTransition()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    invoke-direct {p0}, Lcom/google/android/videos/ui/TransitionUtil$TransitionListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransitionEnd(Landroid/transition/Transition;)V
    .locals 4
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    const/4 v3, 0x0

    .line 192
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->finishAfterTransition:Z
    invoke-static {v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$100(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    :goto_0
    return-void

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iget-object v0, v0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->fullAdapterContents:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 196
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->objectAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    invoke-static {v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$200(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->clear()V

    .line 197
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->objectAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    invoke-static {v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$200(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iget-object v2, v2, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->fullAdapterContents:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->addAll(ILjava/util/Collection;)V

    .line 201
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iput-object v3, v0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->fullAdapterContents:Ljava/util/ArrayList;

    .line 202
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iput-object v3, v0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->rowAdapterContents:Ljava/util/ArrayList;

    goto :goto_0

    .line 198
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iget-object v0, v0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->rowAdapterContents:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 199
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->objectAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    invoke-static {v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$200(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->objectAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    invoke-static {v1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$200(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->size()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iget-object v2, v2, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->rowAdapterContents:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->addAll(ILjava/util/Collection;)V

    goto :goto_1
.end method

.method public onTransitionStart(Landroid/transition/Transition;)V
    .locals 2
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    const/4 v1, 0x0

    .line 186
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iput-boolean v1, v0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->pendingEnterTransition:Z

    .line 187
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # setter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->finishAfterTransition:Z
    invoke-static {v0, v1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$102(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;Z)Z

    .line 188
    return-void
.end method
