.class public Lcom/google/android/videos/api/AssetsRequest;
.super Lcom/google/android/videos/async/Request;
.source "AssetsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/api/AssetsRequest$Builder;
    }
.end annotation


# instance fields
.field public final flags:I

.field public final ids:Ljava/lang/String;

.field public final locale:Ljava/util/Locale;

.field public final userCountry:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;IZ)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "ids"    # Ljava/lang/String;
    .param p3, "userCountry"    # Ljava/lang/String;
    .param p4, "locale"    # Ljava/util/Locale;
    .param p5, "flags"    # I
    .param p6, "requireAuthentication"    # Z

    .prologue
    .line 35
    invoke-direct {p0, p1, p6}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;Z)V

    .line 36
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/AssetsRequest;->ids:Ljava/lang/String;

    .line 37
    iput-object p3, p0, Lcom/google/android/videos/api/AssetsRequest;->userCountry:Ljava/lang/String;

    .line 38
    iput-object p4, p0, Lcom/google/android/videos/api/AssetsRequest;->locale:Ljava/util/Locale;

    .line 39
    iput p5, p0, Lcom/google/android/videos/api/AssetsRequest;->flags:I

    .line 40
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 44
    if-ne p0, p1, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v1

    .line 47
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 48
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 50
    check-cast v0, Lcom/google/android/videos/api/AssetsRequest;

    .line 51
    .local v0, "other":Lcom/google/android/videos/api/AssetsRequest;
    iget-object v3, p0, Lcom/google/android/videos/api/AssetsRequest;->account:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/AssetsRequest;->account:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/AssetsRequest;->ids:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/AssetsRequest;->ids:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/AssetsRequest;->userCountry:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/AssetsRequest;->userCountry:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/AssetsRequest;->locale:Ljava/util/Locale;

    iget-object v4, v0, Lcom/google/android/videos/api/AssetsRequest;->locale:Ljava/util/Locale;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget v3, p0, Lcom/google/android/videos/api/AssetsRequest;->flags:I

    iget v4, v0, Lcom/google/android/videos/api/AssetsRequest;->flags:I

    if-eq v3, v4, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 60
    iget-object v2, p0, Lcom/google/android/videos/api/AssetsRequest;->account:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/videos/api/AssetsRequest;->account:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 61
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/AssetsRequest;->ids:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/videos/api/AssetsRequest;->ids:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 62
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/AssetsRequest;->userCountry:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/videos/api/AssetsRequest;->userCountry:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 63
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/api/AssetsRequest;->locale:Ljava/util/Locale;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/api/AssetsRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 64
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/api/AssetsRequest;->flags:I

    add-int v0, v1, v2

    .line 65
    return v0

    .end local v0    # "result":I
    :cond_1
    move v0, v1

    .line 60
    goto :goto_0

    .restart local v0    # "result":I
    :cond_2
    move v2, v1

    .line 61
    goto :goto_1

    :cond_3
    move v2, v1

    .line 62
    goto :goto_2
.end method
