.class public Lcom/google/android/videos/player/exo/VideosHttpDataSource;
.super Lcom/google/android/exoplayer/upstream/HttpDataSource;
.source "VideosHttpDataSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/exo/VideosHttpDataSource$LoadTimeoutExceededException;,
        Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;
    }
.end annotation


# instance fields
.field private final alternateRedirectEnabled:Z

.field private cachedDirectUri:Landroid/net/Uri;

.field private cachedDirectUriTimestamp:J

.field private currentDataSpec:Lcom/google/android/exoplayer/upstream/DataSpec;

.field private lastSpecUri:Landroid/net/Uri;

.field private final loadTimeoutMillis:I

.field private openTimestamp:J

.field private requestAlternateRedirect:Z

.field private final videosHttpDataSourceHandler:Landroid/os/Handler;

.field private final videosHttpDataSourceListener:Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/exoplayer/util/Predicate;Lcom/google/android/exoplayer/upstream/TransferListener;Landroid/os/Handler;Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;IZ)V
    .locals 0
    .param p1, "userAgent"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/google/android/exoplayer/upstream/TransferListener;
    .param p4, "videosHttpDataSourceHandler"    # Landroid/os/Handler;
    .param p5, "videosHttpDataSourceListener"    # Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;
    .param p6, "loadTimeoutMillis"    # I
    .param p7, "alternateRedirectEnabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/exoplayer/util/Predicate",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/exoplayer/upstream/TransferListener;",
            "Landroid/os/Handler;",
            "Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p2, "contentTypePredicate":Lcom/google/android/exoplayer/util/Predicate;, "Lcom/google/android/exoplayer/util/Predicate<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/exoplayer/upstream/HttpDataSource;-><init>(Ljava/lang/String;Lcom/google/android/exoplayer/util/Predicate;Lcom/google/android/exoplayer/upstream/TransferListener;)V

    .line 97
    iput-object p4, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->videosHttpDataSourceHandler:Landroid/os/Handler;

    .line 98
    iput-object p5, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->videosHttpDataSourceListener:Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;

    .line 99
    iput p6, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->loadTimeoutMillis:I

    .line 100
    iput-boolean p7, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->alternateRedirectEnabled:Z

    .line 101
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/player/exo/VideosHttpDataSource;)Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/exo/VideosHttpDataSource;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->videosHttpDataSourceListener:Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;

    return-object v0
.end method

.method private clearRedirect()V
    .locals 2

    .prologue
    .line 172
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->cachedDirectUri:Landroid/net/Uri;

    .line 173
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->cachedDirectUriTimestamp:J

    .line 174
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->requestAlternateRedirect:Z

    .line 175
    return-void
.end method

.method private notifySocketOpened(J)V
    .locals 3
    .param p1, "elapsedTimeMillis"    # J

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->videosHttpDataSourceHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->videosHttpDataSourceListener:Lcom/google/android/videos/player/exo/VideosHttpDataSource$VideosHttpDataSourceListener;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->videosHttpDataSourceHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/videos/player/exo/VideosHttpDataSource$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/videos/player/exo/VideosHttpDataSource$1;-><init>(Lcom/google/android/videos/player/exo/VideosHttpDataSource;J)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 192
    :cond_0
    return-void
.end method

.method private onError()V
    .locals 2

    .prologue
    .line 178
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->cachedDirectUri:Landroid/net/Uri;

    .line 179
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->cachedDirectUriTimestamp:J

    .line 180
    iget-boolean v0, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->alternateRedirectEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->requestAlternateRedirect:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->requestAlternateRedirect:Z

    .line 181
    return-void

    .line 180
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;
        }
    .end annotation

    .prologue
    .line 160
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->getConnection()Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 161
    .local v0, "connection":Ljava/net/HttpURLConnection;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->bytesRemaining()J

    move-result-wide v2

    const-wide/16 v4, 0x800

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 162
    invoke-virtual {p0}, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->getConnection()Ljava/net/HttpURLConnection;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/utils/Util;->terminateInputStream(Ljava/net/HttpURLConnection;)V

    .line 164
    :cond_0
    invoke-super {p0}, Lcom/google/android/exoplayer/upstream/HttpDataSource;->close()V
    :try_end_0
    .catch Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    return-void

    .line 165
    .end local v0    # "connection":Ljava/net/HttpURLConnection;
    :catch_0
    move-exception v1

    .line 166
    .local v1, "e":Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->onError()V

    .line 167
    throw v1
.end method

.method public open(Lcom/google/android/exoplayer/upstream/DataSpec;)J
    .locals 12
    .param p1, "dataSpec"    # Lcom/google/android/exoplayer/upstream/DataSpec;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;
        }
    .end annotation

    .prologue
    .line 105
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->openTimestamp:J

    .line 107
    iget-object v2, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->cachedDirectUri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->cachedDirectUriTimestamp:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x927c0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 109
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->clearRedirect()V

    .line 111
    :cond_0
    iget-object v2, p1, Lcom/google/android/exoplayer/upstream/DataSpec;->uri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->lastSpecUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 113
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->clearRedirect()V

    .line 115
    :cond_1
    iget-object v2, p1, Lcom/google/android/exoplayer/upstream/DataSpec;->uri:Landroid/net/Uri;

    iput-object v2, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->lastSpecUri:Landroid/net/Uri;

    .line 116
    iget-object v2, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->cachedDirectUri:Landroid/net/Uri;

    if-eqz v2, :cond_4

    .line 117
    new-instance v0, Lcom/google/android/exoplayer/upstream/DataSpec;

    iget-object v1, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->cachedDirectUri:Landroid/net/Uri;

    iget-wide v2, p1, Lcom/google/android/exoplayer/upstream/DataSpec;->absoluteStreamPosition:J

    iget-wide v4, p1, Lcom/google/android/exoplayer/upstream/DataSpec;->length:J

    iget-object v6, p1, Lcom/google/android/exoplayer/upstream/DataSpec;->key:Ljava/lang/String;

    iget-wide v7, p1, Lcom/google/android/exoplayer/upstream/DataSpec;->position:J

    invoke-direct/range {v0 .. v8}, Lcom/google/android/exoplayer/upstream/DataSpec;-><init>(Landroid/net/Uri;JJLjava/lang/String;J)V

    .end local p1    # "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    .local v0, "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    move-object p1, v0

    .line 127
    .end local v0    # "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    .restart local p1    # "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    :cond_2
    :goto_0
    :try_start_0
    iput-object p1, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->currentDataSpec:Lcom/google/android/exoplayer/upstream/DataSpec;

    .line 128
    iget-object v2, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->currentDataSpec:Lcom/google/android/exoplayer/upstream/DataSpec;

    invoke-super {p0, v2}, Lcom/google/android/exoplayer/upstream/HttpDataSource;->open(Lcom/google/android/exoplayer/upstream/DataSpec;)J

    move-result-wide v10

    .line 129
    .local v10, "resolvedLength":J
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->openTimestamp:J

    sub-long/2addr v2, v4

    invoke-direct {p0, v2, v3}, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->notifySocketOpened(J)V

    .line 130
    iget-object v2, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->cachedDirectUri:Landroid/net/Uri;

    if-nez v2, :cond_3

    .line 131
    invoke-virtual {p0}, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->getConnection()Ljava/net/HttpURLConnection;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->cachedDirectUri:Landroid/net/Uri;

    .line 132
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->cachedDirectUriTimestamp:J
    :try_end_0
    .catch Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    :cond_3
    return-wide v10

    .line 119
    .end local v10    # "resolvedLength":J
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->requestAlternateRedirect:Z

    if-eqz v2, :cond_2

    .line 120
    iget-object v2, p1, Lcom/google/android/exoplayer/upstream/DataSpec;->uri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "cmo"

    const-string v4, "pf=1"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 122
    .local v1, "fallbackUri":Landroid/net/Uri;
    new-instance v0, Lcom/google/android/exoplayer/upstream/DataSpec;

    iget-wide v2, p1, Lcom/google/android/exoplayer/upstream/DataSpec;->absoluteStreamPosition:J

    iget-wide v4, p1, Lcom/google/android/exoplayer/upstream/DataSpec;->length:J

    iget-object v6, p1, Lcom/google/android/exoplayer/upstream/DataSpec;->key:Ljava/lang/String;

    iget-wide v7, p1, Lcom/google/android/exoplayer/upstream/DataSpec;->position:J

    invoke-direct/range {v0 .. v8}, Lcom/google/android/exoplayer/upstream/DataSpec;-><init>(Landroid/net/Uri;JJLjava/lang/String;J)V

    .end local p1    # "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    .restart local v0    # "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    move-object p1, v0

    .end local v0    # "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    .restart local p1    # "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    goto :goto_0

    .line 135
    .end local v1    # "fallbackUri":Landroid/net/Uri;
    :catch_0
    move-exception v9

    .line 136
    .local v9, "e":Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->onError()V

    .line 137
    throw v9
.end method

.method public read([BII)I
    .locals 8
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "readLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;
        }
    .end annotation

    .prologue
    .line 144
    :try_start_0
    iget v1, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->loadTimeoutMillis:I

    if-lez v1, :cond_0

    .line 145
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->openTimestamp:J

    sub-long v2, v4, v6

    .line 146
    .local v2, "elapsedTimeMs":J
    iget v1, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->loadTimeoutMillis:I

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 147
    new-instance v1, Lcom/google/android/videos/player/exo/VideosHttpDataSource$LoadTimeoutExceededException;

    invoke-virtual {p0}, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->bytesRead()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->currentDataSpec:Lcom/google/android/exoplayer/upstream/DataSpec;

    invoke-direct/range {v1 .. v6}, Lcom/google/android/videos/player/exo/VideosHttpDataSource$LoadTimeoutExceededException;-><init>(JJLcom/google/android/exoplayer/upstream/DataSpec;)V

    throw v1
    :try_end_0
    .catch Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    .end local v2    # "elapsedTimeMs":J
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/VideosHttpDataSource;->onError()V

    .line 153
    throw v0

    .line 150
    .end local v0    # "e":Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;
    :cond_0
    :try_start_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/exoplayer/upstream/HttpDataSource;->read([BII)I
    :try_end_1
    .catch Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v1

    return v1
.end method
