.class public Lcom/google/android/videos/pano/model/Item;
.super Ljava/lang/Object;
.source "Item.java"


# instance fields
.field private hashCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 69
    if-ne p1, p0, :cond_1

    .line 99
    :cond_0
    :goto_0
    return v1

    .line 72
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/pano/model/Item;

    if-nez v3, :cond_2

    move v1, v2

    .line 73
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 75
    check-cast v0, Lcom/google/android/videos/pano/model/Item;

    .line 77
    .local v0, "item":Lcom/google/android/videos/pano/model/Item;
    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getImageResourceId()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/videos/pano/model/Item;->getImageResourceId()I

    move-result v4

    if-ne v3, v4, :cond_3

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getImageHeight()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/videos/pano/model/Item;->getImageHeight()I

    move-result v4

    if-ne v3, v4, :cond_3

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getImageWidth()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/videos/pano/model/Item;->getImageWidth()I

    move-result v4

    if-ne v3, v4, :cond_3

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getBadgeResourceId()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/videos/pano/model/Item;->getBadgeResourceId()I

    move-result v4

    if-ne v3, v4, :cond_3

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getProgress()F

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/videos/pano/model/Item;->getProgress()F

    move-result v4

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getDurationInSeconds()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/videos/pano/model/Item;->getDurationInSeconds()I

    move-result v4

    if-eq v3, v4, :cond_4

    :cond_3
    move v1, v2

    .line 83
    goto :goto_0

    .line 86
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/videos/pano/model/Item;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getSubtitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/videos/pano/model/Item;->getSubtitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getImageUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/videos/pano/model/Item;->getImageUri()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getBackgroundUri()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getBackgroundUri()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    :cond_5
    move v1, v2

    .line 90
    goto/16 :goto_0

    .line 93
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getCheapestOffer()Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/videos/pano/model/Item;->getCheapestOffer()Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 94
    goto/16 :goto_0
.end method

.method public getBackgroundUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBadgeResourceId()I
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return v0
.end method

.method public getCheapestOffer()Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDurationInSeconds()I
    .locals 1

    .prologue
    .line 61
    const/4 v0, -0x1

    return v0
.end method

.method public getImageHeight()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public getImageResourceId()I
    .locals 1

    .prologue
    .line 28
    const v0, 0x7f02017e

    return v0
.end method

.method public getImageUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return-object v0
.end method

.method public getImageWidth()I
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method public getProgress()F
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public getStarRating()F
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public getSubtitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 104
    iget v0, p0, Lcom/google/android/videos/pano/model/Item;->hashCode:I

    .line 105
    .local v0, "result":I
    if-nez v0, :cond_0

    .line 106
    const/16 v0, 0x11

    .line 107
    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getImageResourceId()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 108
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getImageHeight()I

    move-result v2

    add-int v0, v1, v2

    .line 109
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getImageWidth()I

    move-result v2

    add-int v0, v1, v2

    .line 110
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getBadgeResourceId()I

    move-result v2

    add-int v0, v1, v2

    .line 111
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getProgress()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int v0, v1, v2

    .line 112
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getDurationInSeconds()I

    move-result v2

    add-int v0, v1, v2

    .line 113
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/utils/Util;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 114
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getSubtitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/utils/Util;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 115
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getImageUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/utils/Util;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 116
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getBackgroundUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/utils/Util;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 117
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/videos/pano/model/Item;->getCheapestOffer()Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/utils/Util;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 118
    iput v0, p0, Lcom/google/android/videos/pano/model/Item;->hashCode:I

    .line 120
    :cond_0
    return v0
.end method

.method public onClick(Landroid/app/Activity;Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 65
    return-void
.end method
