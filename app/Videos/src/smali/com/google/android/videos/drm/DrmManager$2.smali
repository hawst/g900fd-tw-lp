.class Lcom/google/android/videos/drm/DrmManager$2;
.super Ljava/lang/Object;
.source "DrmManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/drm/DrmManager;->notifyPlaybackStopped(Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$StopReason;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/drm/DrmManager;

.field final synthetic val$assetPathKey:Ljava/lang/String;

.field final synthetic val$reason:Lcom/google/android/videos/drm/DrmManager$StopReason;


# direct methods
.method constructor <init>(Lcom/google/android/videos/drm/DrmManager;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$StopReason;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/google/android/videos/drm/DrmManager$2;->this$0:Lcom/google/android/videos/drm/DrmManager;

    iput-object p2, p0, Lcom/google/android/videos/drm/DrmManager$2;->val$assetPathKey:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/videos/drm/DrmManager$2;->val$reason:Lcom/google/android/videos/drm/DrmManager$StopReason;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmManager$2;->this$0:Lcom/google/android/videos/drm/DrmManager;

    # getter for: Lcom/google/android/videos/drm/DrmManager;->listener:Lcom/google/android/videos/drm/DrmManager$Listener;
    invoke-static {v0}, Lcom/google/android/videos/drm/DrmManager;->access$000(Lcom/google/android/videos/drm/DrmManager;)Lcom/google/android/videos/drm/DrmManager$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmManager$2;->this$0:Lcom/google/android/videos/drm/DrmManager;

    # getter for: Lcom/google/android/videos/drm/DrmManager;->listener:Lcom/google/android/videos/drm/DrmManager$Listener;
    invoke-static {v0}, Lcom/google/android/videos/drm/DrmManager;->access$000(Lcom/google/android/videos/drm/DrmManager;)Lcom/google/android/videos/drm/DrmManager$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/drm/DrmManager$2;->val$assetPathKey:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/drm/DrmManager$2;->val$reason:Lcom/google/android/videos/drm/DrmManager$StopReason;

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/drm/DrmManager$Listener;->onPlaybackStopped(Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$StopReason;)V

    .line 210
    :cond_0
    return-void
.end method
