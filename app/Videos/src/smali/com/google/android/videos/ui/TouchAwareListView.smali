.class public Lcom/google/android/videos/ui/TouchAwareListView;
.super Landroid/widget/ListView;
.source "TouchAwareListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/TouchAwareListView$OnTouchStateChangedListener;
    }
.end annotation


# instance fields
.field private final commitRunnable:Ljava/lang/Runnable;

.field private isBeingTouched:Z

.field private listener:Lcom/google/android/videos/ui/TouchAwareListView$OnTouchStateChangedListener;

.field private pendingIsBeingTouched:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/ui/TouchAwareListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/ui/TouchAwareListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    invoke-direct {p0}, Lcom/google/android/videos/ui/TouchAwareListView;->createCommitRunnable()Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/TouchAwareListView;->commitRunnable:Ljava/lang/Runnable;

    .line 48
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/TouchAwareListView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/ui/TouchAwareListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/TouchAwareListView;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/videos/ui/TouchAwareListView;->commitIsBeingTouched()V

    return-void
.end method

.method private commitIsBeingTouched()V
    .locals 2

    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/google/android/videos/ui/TouchAwareListView;->isBeingTouched:Z

    iget-boolean v1, p0, Lcom/google/android/videos/ui/TouchAwareListView;->pendingIsBeingTouched:Z

    if-eq v0, v1, :cond_0

    .line 205
    iget-boolean v0, p0, Lcom/google/android/videos/ui/TouchAwareListView;->pendingIsBeingTouched:Z

    iput-boolean v0, p0, Lcom/google/android/videos/ui/TouchAwareListView;->isBeingTouched:Z

    .line 206
    iget-object v0, p0, Lcom/google/android/videos/ui/TouchAwareListView;->listener:Lcom/google/android/videos/ui/TouchAwareListView$OnTouchStateChangedListener;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/google/android/videos/ui/TouchAwareListView;->listener:Lcom/google/android/videos/ui/TouchAwareListView$OnTouchStateChangedListener;

    iget-boolean v1, p0, Lcom/google/android/videos/ui/TouchAwareListView;->isBeingTouched:Z

    invoke-interface {v0, v1}, Lcom/google/android/videos/ui/TouchAwareListView$OnTouchStateChangedListener;->onTouchStateChanged(Z)V

    .line 210
    :cond_0
    return-void
.end method

.method private createCommitRunnable()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 195
    new-instance v0, Lcom/google/android/videos/ui/TouchAwareListView$1;

    invoke-direct {v0, p0}, Lcom/google/android/videos/ui/TouchAwareListView$1;-><init>(Lcom/google/android/videos/ui/TouchAwareListView;)V

    return-object v0
.end method

.method private getTopFromThis(Landroid/view/View;)I
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 213
    if-ne p0, p1, :cond_0

    .line 214
    const/4 v1, 0x0

    .line 218
    :goto_0
    return v1

    .line 216
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 217
    .local v0, "parentView":Landroid/view/ViewParent;
    instance-of v1, v0, Landroid/view/View;

    invoke-static {v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 218
    check-cast v0, Landroid/view/View;

    .end local v0    # "parentView":Landroid/view/ViewParent;
    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/TouchAwareListView;->getTopFromThis(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0
.end method

.method private isClipped(Landroid/view/View;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 189
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 190
    .local v0, "visibleRect":Landroid/graphics/Rect;
    invoke-virtual {p1, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 191
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    if-ge v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private keyCodeToFocusDirection(I)I
    .locals 1
    .param p1, "keyCode"    # I

    .prologue
    .line 222
    packed-switch p1, :pswitch_data_0

    .line 232
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 224
    :pswitch_0
    const/16 v0, 0x42

    goto :goto_0

    .line 226
    :pswitch_1
    const/16 v0, 0x11

    goto :goto_0

    .line 228
    :pswitch_2
    const/16 v0, 0x82

    goto :goto_0

    .line 230
    :pswitch_3
    const/16 v0, 0x21

    goto :goto_0

    .line 222
    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 64
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 65
    .local v0, "action":I
    if-nez v0, :cond_0

    .line 66
    iput-boolean v2, p0, Lcom/google/android/videos/ui/TouchAwareListView;->pendingIsBeingTouched:Z

    .line 68
    invoke-direct {p0}, Lcom/google/android/videos/ui/TouchAwareListView;->commitIsBeingTouched()V

    .line 70
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 71
    if-eq v0, v2, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 72
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/videos/ui/TouchAwareListView;->pendingIsBeingTouched:Z

    .line 74
    iget-object v1, p0, Lcom/google/android/videos/ui/TouchAwareListView;->commitRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/google/android/videos/ui/TouchAwareListView;->post(Ljava/lang/Runnable;)Z

    .line 77
    :cond_2
    return v2
.end method

.method public isBeingTouched()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/videos/ui/TouchAwareListView;->isBeingTouched:Z

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 11
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v9, 0x82

    const/4 v7, 0x1

    const/16 v10, 0x21

    .line 87
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/TouchAwareListView;->keyCodeToFocusDirection(I)I

    move-result v1

    .line 88
    .local v1, "focusDirection":I
    invoke-virtual {p0}, Lcom/google/android/videos/ui/TouchAwareListView;->findFocus()Landroid/view/View;

    move-result-object v2

    .line 89
    .local v2, "focusedView":Landroid/view/View;
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-eq v2, p0, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->isShown()Z

    move-result v8

    if-nez v8, :cond_2

    .line 91
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    .line 156
    :cond_1
    :goto_0
    return v7

    .line 94
    :cond_2
    invoke-virtual {v2, v1}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    move-result-object v4

    .line 95
    .local v4, "nextView":Landroid/view/View;
    if-eqz v4, :cond_3

    .line 96
    invoke-virtual {p0, v4}, Lcom/google/android/videos/ui/TouchAwareListView;->getPositionForView(Landroid/view/View;)I

    move-result v3

    .line 97
    .local v3, "nextPosition":I
    if-ltz v3, :cond_3

    .line 108
    invoke-virtual {p0, v3}, Lcom/google/android/videos/ui/TouchAwareListView;->smoothScrollToPosition(I)V

    .line 109
    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 114
    .end local v3    # "nextPosition":I
    :cond_3
    const/16 v8, 0x42

    if-eq v1, v8, :cond_4

    const/16 v8, 0x11

    if-ne v1, v8, :cond_5

    .line 117
    :cond_4
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    goto :goto_0

    .line 119
    :cond_5
    invoke-virtual {p0, v2}, Lcom/google/android/videos/ui/TouchAwareListView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 120
    .local v0, "currentPosition":I
    if-nez v0, :cond_6

    if-eq v1, v10, :cond_7

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/videos/ui/TouchAwareListView;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ne v0, v8, :cond_8

    if-ne v1, v9, :cond_8

    .line 122
    :cond_7
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    goto :goto_0

    .line 125
    :cond_8
    if-ne v1, v10, :cond_9

    invoke-virtual {p0}, Lcom/google/android/videos/ui/TouchAwareListView;->getFirstVisiblePosition()I

    move-result v8

    if-eqz v8, :cond_a

    :cond_9
    if-ne v1, v9, :cond_c

    invoke-virtual {p0}, Lcom/google/android/videos/ui/TouchAwareListView;->getLastVisiblePosition()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/videos/ui/TouchAwareListView;->getCount()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ne v8, v9, :cond_c

    .line 131
    :cond_a
    if-ne v1, v10, :cond_b

    .line 132
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/google/android/videos/ui/TouchAwareListView;->setSelection(I)V

    .line 136
    :goto_1
    if-eqz v4, :cond_1

    .line 137
    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    .line 134
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/videos/ui/TouchAwareListView;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p0, v8}, Lcom/google/android/videos/ui/TouchAwareListView;->setSelection(I)V

    goto :goto_1

    .line 144
    :cond_c
    const v8, 0x3e19999a    # 0.15f

    invoke-virtual {p0}, Lcom/google/android/videos/ui/TouchAwareListView;->getHeight()I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v8, v9

    float-to-int v5, v8

    .line 145
    .local v5, "top":I
    invoke-direct {p0, v2}, Lcom/google/android/videos/ui/TouchAwareListView;->getTopFromThis(Landroid/view/View;)I

    move-result v6

    .line 146
    .local v6, "topOfFocusedView":I
    if-le v6, v5, :cond_d

    .line 147
    move v5, v6

    .line 150
    :cond_d
    if-ne v1, v10, :cond_e

    .line 151
    add-int/lit8 v8, v0, -0x1

    invoke-virtual {p0, v8, v5}, Lcom/google/android/videos/ui/TouchAwareListView;->setSelectionFromTop(II)V

    goto :goto_0

    .line 153
    :cond_e
    add-int/lit8 v8, v0, 0x1

    invoke-virtual {p0, v8, v5}, Lcom/google/android/videos/ui/TouchAwareListView;->setSelectionFromTop(II)V

    goto/16 :goto_0
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 7
    .param p1, "direction"    # I
    .param p2, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/android/videos/ui/TouchAwareListView;->getChildCount()I

    move-result v1

    .line 168
    .local v1, "count":I
    and-int/lit8 v6, p1, 0x2

    if-eqz v6, :cond_0

    .line 169
    const/4 v5, 0x0

    .line 170
    .local v5, "index":I
    const/4 v4, 0x1

    .line 171
    .local v4, "increment":I
    move v2, v1

    .line 177
    .local v2, "end":I
    :goto_0
    move v3, v5

    .local v3, "i":I
    :goto_1
    if-eq v3, v2, :cond_2

    .line 178
    invoke-virtual {p0, v3}, Lcom/google/android/videos/ui/TouchAwareListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 179
    .local v0, "child":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/videos/ui/TouchAwareListView;->getVisibility()I

    move-result v6

    if-nez v6, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/TouchAwareListView;->isClipped(Landroid/view/View;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 180
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 181
    const/4 v6, 0x1

    .line 185
    .end local v0    # "child":Landroid/view/View;
    :goto_2
    return v6

    .line 173
    .end local v2    # "end":I
    .end local v3    # "i":I
    .end local v4    # "increment":I
    .end local v5    # "index":I
    :cond_0
    add-int/lit8 v5, v1, -0x1

    .line 174
    .restart local v5    # "index":I
    const/4 v4, -0x1

    .line 175
    .restart local v4    # "increment":I
    const/4 v2, -0x1

    .restart local v2    # "end":I
    goto :goto_0

    .line 177
    .restart local v0    # "child":Landroid/view/View;
    .restart local v3    # "i":I
    :cond_1
    add-int/2addr v3, v4

    goto :goto_1

    .line 185
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z

    move-result v6

    goto :goto_2
.end method

.method public setOnTouchStateChangedListener(Lcom/google/android/videos/ui/TouchAwareListView$OnTouchStateChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/ui/TouchAwareListView$OnTouchStateChangedListener;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/videos/ui/TouchAwareListView;->listener:Lcom/google/android/videos/ui/TouchAwareListView$OnTouchStateChangedListener;

    .line 53
    return-void
.end method
