.class public Lcom/google/android/videos/ui/VideosDrawerHelper;
.super Ljava/lang/Object;
.source "VideosDrawerHelper.java"

# interfaces
.implements Landroid/accounts/OnAccountsUpdateListener;
.implements Landroid/support/v4/widget/DrawerLayout$DrawerListener;
.implements Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;
.implements Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;


# instance fields
.field private account:Ljava/lang/String;

.field private final accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

.field private activeVerticalKey:I

.field private final activity:Lcom/google/android/videos/activity/DrawerActivity;

.field private final alwaysOpen:Z

.field private final downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

.field private drawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private final handler:Landroid/os/Handler;

.field private libraryContentVertical:I

.field private final pageView:Landroid/view/ViewGroup;

.field private final playCommonNetworkStackWrapper:Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;

.field private final playDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

.field private final preferences:Landroid/content/SharedPreferences;

.field private final primaryActionItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;",
            ">;"
        }
    .end annotation
.end field

.field private final secondaryActionItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;",
            ">;"
        }
    .end annotation
.end field

.field private final shopDrawerItem:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

.field private final verticalItems:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;",
            ">;"
        }
    .end annotation
.end field

.field private final verticalItemsActive:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;",
            ">;"
        }
    .end annotation
.end field

.field private verticalKeys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/play/drawer/PlayDrawerLayout;Lcom/google/android/videos/activity/DrawerActivity;Landroid/content/SharedPreferences;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/logging/EventLogger;Z)V
    .locals 11
    .param p1, "playDrawerLayout"    # Lcom/google/android/play/drawer/PlayDrawerLayout;
    .param p2, "activity"    # Lcom/google/android/videos/activity/DrawerActivity;
    .param p3, "preferences"    # Landroid/content/SharedPreferences;
    .param p4, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p5, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p6, "alwaysOpen"    # Z

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/drawer/PlayDrawerLayout;

    iput-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->playDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    .line 81
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/activity/DrawerActivity;

    iput-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->activity:Lcom/google/android/videos/activity/DrawerActivity;

    .line 82
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->preferences:Landroid/content/SharedPreferences;

    .line 83
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iput-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 84
    invoke-static/range {p5 .. p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/logging/EventLogger;

    iput-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 85
    move/from16 v0, p6

    iput-boolean v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->alwaysOpen:Z

    .line 86
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->handler:Landroid/os/Handler;

    .line 87
    new-instance v1, Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-direct {v1, p3}, Lcom/google/android/videos/utils/DownloadedOnlyManager;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .line 88
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v1, p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->registerObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V

    .line 90
    const v1, 0x7f0f011e

    invoke-virtual {p2, v1}, Lcom/google/android/videos/activity/DrawerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->pageView:Landroid/view/ViewGroup;

    .line 92
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->libraryContentVertical:I

    .line 94
    new-instance v1, Landroid/util/SparseArray;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->verticalItems:Landroid/util/SparseArray;

    .line 95
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->verticalItems:Landroid/util/SparseArray;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/ui/VideosDrawerHelper;->createVerticalItems(Landroid/util/SparseArray;Z)V

    .line 96
    new-instance v1, Landroid/util/SparseArray;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->verticalItemsActive:Landroid/util/SparseArray;

    .line 97
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->verticalItemsActive:Landroid/util/SparseArray;

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/ui/VideosDrawerHelper;->createVerticalItems(Landroid/util/SparseArray;Z)V

    .line 99
    invoke-static {p2}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v9

    .line 101
    .local v9, "globals":Lcom/google/android/videos/VideosGlobals;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->primaryActionItems:Ljava/util/List;

    .line 102
    invoke-virtual {v9}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/videos/Config;->suggestionsEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    const v2, 0x7f0b00d1

    invoke-virtual {p2, v2}, Lcom/google/android/videos/activity/DrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f020090

    const v4, 0x7f020091

    const v5, 0x7f0a0085

    const/4 v6, 0x0

    const/4 v7, 0x1

    new-instance v8, Lcom/google/android/videos/ui/VideosDrawerHelper$1;

    invoke-direct {v8, p0}, Lcom/google/android/videos/ui/VideosDrawerHelper$1;-><init>(Lcom/google/android/videos/ui/VideosDrawerHelper;)V

    invoke-direct/range {v1 .. v8}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;-><init>(Ljava/lang/String;IIIZZLjava/lang/Runnable;)V

    :goto_0
    iput-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->shopDrawerItem:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    .line 114
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->secondaryActionItems:Ljava/util/List;

    .line 115
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->secondaryActionItems:Ljava/util/List;

    new-instance v2, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    const v3, 0x7f0b00d4

    invoke-virtual {p2, v3}, Lcom/google/android/videos/activity/DrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/videos/ui/VideosDrawerHelper$2;

    invoke-direct {v4, p0, p2}, Lcom/google/android/videos/ui/VideosDrawerHelper$2;-><init>(Lcom/google/android/videos/ui/VideosDrawerHelper;Lcom/google/android/videos/activity/DrawerActivity;)V

    invoke-direct {v2, v3, v4}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;-><init>(Ljava/lang/String;Ljava/lang/Runnable;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->secondaryActionItems:Ljava/util/List;

    new-instance v2, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    const v3, 0x7f0b00d2

    invoke-virtual {p2, v3}, Lcom/google/android/videos/activity/DrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/videos/ui/VideosDrawerHelper$3;

    invoke-direct {v4, p0, p2}, Lcom/google/android/videos/ui/VideosDrawerHelper$3;-><init>(Lcom/google/android/videos/ui/VideosDrawerHelper;Lcom/google/android/videos/activity/DrawerActivity;)V

    invoke-direct {v2, v3, v4}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;-><init>(Ljava/lang/String;Ljava/lang/Runnable;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    invoke-virtual {v9}, Lcom/google/android/videos/VideosGlobals;->getPlayCommonNetworkStackWrapper()Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->playCommonNetworkStackWrapper:Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;

    .line 140
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->playCommonNetworkStackWrapper:Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;->getPlayCommonNetworkStack()Lcom/google/android/play/utils/PlayCommonNetworkStack;

    move-result-object v10

    .line 142
    .local v10, "playCommonNetworkStack":Lcom/google/android/play/utils/PlayCommonNetworkStack;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v10}, Lcom/google/android/play/utils/PlayCommonNetworkStack;->getPlayDfeApiProvider()Lcom/google/android/play/dfe/api/PlayDfeApiProvider;

    move-result-object v6

    invoke-virtual {v10}, Lcom/google/android/play/utils/PlayCommonNetworkStack;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v7

    move-object v1, p1

    move-object v2, p2

    move-object v8, p0

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/play/drawer/PlayDrawerLayout;->configure(Landroid/app/Activity;ZIZLcom/google/android/play/dfe/api/PlayDfeApiProvider;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerContentClickListener;)V

    .line 145
    if-eqz p6, :cond_1

    .line 146
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->setDrawerLockMode(I)V

    .line 147
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/videos/ui/VideosDrawerHelper;->setDrawerIndicatorEnabled(Z)V

    .line 151
    :goto_1
    return-void

    .line 102
    .end local v10    # "playCommonNetworkStack":Lcom/google/android/play/utils/PlayCommonNetworkStack;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 149
    .restart local v10    # "playCommonNetworkStack":Lcom/google/android/play/utils/PlayCommonNetworkStack;
    :cond_1
    invoke-virtual {p1, p0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/google/android/videos/ui/VideosDrawerHelper;)Lcom/google/android/videos/activity/DrawerActivity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/VideosDrawerHelper;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->activity:Lcom/google/android/videos/activity/DrawerActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/ui/VideosDrawerHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/VideosDrawerHelper;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->account:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/ui/VideosDrawerHelper;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/VideosDrawerHelper;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/ui/VideosDrawerHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/VideosDrawerHelper;

    .prologue
    .line 42
    iget v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->libraryContentVertical:I

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/videos/ui/VideosDrawerHelper;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/VideosDrawerHelper;
    .param p1, "x1"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->libraryContentVertical:I

    return p1
.end method

.method private createVerticalItem(Landroid/app/Activity;IIIZ)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    .locals 8
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "vertical"    # I
    .param p3, "iconId"    # I
    .param p4, "activeIconId"    # I
    .param p5, "isActive"    # Z

    .prologue
    .line 198
    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    invoke-static {p2}, Lcom/google/android/videos/ui/VerticalsHelper;->getVerticalTitleResourceId(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v4, 0x7f0a0085

    const/4 v6, 0x1

    new-instance v7, Lcom/google/android/videos/ui/VideosDrawerHelper$5;

    invoke-direct {v7, p0, p1, p2}, Lcom/google/android/videos/ui/VideosDrawerHelper$5;-><init>(Lcom/google/android/videos/ui/VideosDrawerHelper;Landroid/app/Activity;I)V

    move v2, p3

    move v3, p4

    move v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;-><init>(Ljava/lang/String;IIIZZLjava/lang/Runnable;)V

    return-object v0
.end method

.method private createVerticalItems(Landroid/util/SparseArray;Z)V
    .locals 10
    .param p2, "isActive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p1, "items":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;>;"
    const/16 v9, 0x10

    const/16 v8, 0x8

    const/4 v2, 0x4

    .line 162
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->activity:Lcom/google/android/videos/activity/DrawerActivity;

    const v3, 0x7f020092

    const v4, 0x7f020093

    move-object v0, p0

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/ui/VideosDrawerHelper;->createVerticalItem(Landroid/app/Activity;IIIZ)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 166
    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->activity:Lcom/google/android/videos/activity/DrawerActivity;

    invoke-static {v9}, Lcom/google/android/videos/ui/VerticalsHelper;->getVerticalTitleResourceId(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/videos/activity/DrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f02008e

    const v3, 0x7f02008f

    const v4, 0x7f0a0085

    const/4 v6, 0x1

    new-instance v7, Lcom/google/android/videos/ui/VideosDrawerHelper$4;

    invoke-direct {v7, p0}, Lcom/google/android/videos/ui/VideosDrawerHelper$4;-><init>(Lcom/google/android/videos/ui/VideosDrawerHelper;)V

    move v5, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;-><init>(Ljava/lang/String;IIIZZLjava/lang/Runnable;)V

    .line 189
    .local v0, "myLibraryAction":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    invoke-virtual {p1, v9, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 191
    iget-object v2, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->activity:Lcom/google/android/videos/activity/DrawerActivity;

    const v4, 0x7f020095

    const v5, 0x7f020096

    move-object v1, p0

    move v3, v8

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/videos/ui/VideosDrawerHelper;->createVerticalItem(Landroid/app/Activity;IIIZ)Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    move-result-object v1

    invoke-virtual {p1, v8, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 194
    return-void
.end method

.method private setDrawerClosedFlag()V
    .locals 3

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->preferences:Landroid/content/SharedPreferences;

    const-string v1, "side_drawer_closed_once"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "side_drawer_closed_once"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 361
    :cond_0
    return-void
.end method

.method private updateContent()V
    .locals 10

    .prologue
    .line 331
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->primaryActionItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 332
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->verticalKeys:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v7, v1, :cond_1

    .line 333
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->verticalKeys:Ljava/util/List;

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 334
    .local v9, "verticalKey":I
    iget v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->activeVerticalKey:I

    if-ne v9, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->verticalItemsActive:Landroid/util/SparseArray;

    invoke-virtual {v1, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    move-object v8, v1

    .line 336
    .local v8, "item":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    :goto_1
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->primaryActionItems:Ljava/util/List;

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 332
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 334
    .end local v8    # "item":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->verticalItems:Landroid/util/SparseArray;

    invoke-virtual {v1, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    move-object v8, v1

    goto :goto_1

    .line 338
    .end local v9    # "verticalKey":I
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->verticalKeys:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->shopDrawerItem:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    if-eqz v1, :cond_2

    .line 339
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->primaryActionItems:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->shopDrawerItem:Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 345
    :cond_2
    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->activity:Lcom/google/android/videos/activity/DrawerActivity;

    invoke-virtual {v1}, Lcom/google/android/videos/activity/DrawerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b009d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->activity:Lcom/google/android/videos/activity/DrawerActivity;

    invoke-virtual {v2}, Lcom/google/android/videos/activity/DrawerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0079

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const v3, 0x7f0201df

    const v4, 0x7f0201da

    iget-object v5, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v5}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isDownloadedOnly()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;-><init>(Ljava/lang/String;IIIZ)V

    .line 352
    .local v0, "downloadSwitch":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->playDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    iget-object v2, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->account:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    new-array v3, v3, [Landroid/accounts/Account;

    :goto_2
    iget-object v4, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->primaryActionItems:Ljava/util/List;

    iget-object v6, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->secondaryActionItems:Ljava/util/List;

    move-object v5, v0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/play/drawer/PlayDrawerLayout;->updateContent(Ljava/lang/String;[Landroid/accounts/Account;Ljava/util/List;Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;Ljava/util/List;)V

    .line 355
    return-void

    .line 352
    :cond_3
    iget-object v3, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v3}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v3

    goto :goto_2
.end method

.method public static verticalsMaskToList(I)Ljava/util/List;
    .locals 2
    .param p0, "verticals"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 315
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 316
    .local v0, "verticalsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    and-int/lit8 v1, p0, 0x4

    if-eqz v1, :cond_0

    .line 317
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 319
    :cond_0
    and-int/lit8 v1, p0, 0x10

    if-eqz v1, :cond_1

    .line 320
    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 322
    :cond_1
    and-int/lit8 v1, p0, 0x8

    if-eqz v1, :cond_2

    .line 323
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 325
    :cond_2
    return-object v0
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->unregisterObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V

    .line 159
    return-void
.end method

.method public getPageView()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->pageView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public init(Ljava/lang/String;I)Z
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "verticals"    # I

    .prologue
    .line 233
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/videos/ui/VideosDrawerHelper;->init(Ljava/lang/String;IIZ)Z

    move-result v0

    return v0
.end method

.method public init(Ljava/lang/String;IIZ)Z
    .locals 6
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "verticals"    # I
    .param p3, "activeVertical"    # I
    .param p4, "forceCloseDrawer"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 238
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez p2, :cond_4

    :cond_0
    move v2, v4

    :goto_0
    const-string v5, "account must be specified if initializing with any vertical items"

    invoke-static {v2, v5}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 242
    if-eq p3, v4, :cond_1

    const/4 v2, 0x2

    if-ne p3, v2, :cond_2

    .line 244
    :cond_1
    iput p3, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->libraryContentVertical:I

    .line 245
    const/16 p3, 0x10

    .line 248
    :cond_2
    iput-object p1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->account:Ljava/lang/String;

    .line 249
    invoke-static {p2}, Lcom/google/android/videos/ui/VideosDrawerHelper;->verticalsMaskToList(I)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->verticalKeys:Ljava/util/List;

    .line 250
    iput p3, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->activeVerticalKey:I

    .line 251
    iget-object v2, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v2, p0}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V

    .line 252
    iget-object v2, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v2, p0, v3}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Z)V

    .line 253
    iget-object v2, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->playCommonNetworkStackWrapper:Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;->register()V

    .line 255
    invoke-virtual {p0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->isDrawerOpen()Z

    move-result v1

    .line 256
    .local v1, "isDrawerOpen":Z
    iget-object v2, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->verticalItemsActive:Landroid/util/SparseArray;

    invoke-virtual {v2, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    .line 257
    .local v0, "activeItem":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    if-eqz v0, :cond_5

    .line 258
    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->onPrimaryActionClicked(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;)Z

    .line 259
    const/4 v1, 0x0

    .line 267
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->activity:Lcom/google/android/videos/activity/DrawerActivity;

    invoke-virtual {v2}, Lcom/google/android/videos/activity/DrawerActivity;->supportInvalidateOptionsMenu()V

    .line 268
    return v1

    .end local v0    # "activeItem":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    .end local v1    # "isDrawerOpen":Z
    :cond_4
    move v2, v3

    .line 238
    goto :goto_0

    .line 261
    .restart local v0    # "activeItem":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    .restart local v1    # "isDrawerOpen":Z
    :cond_5
    invoke-direct {p0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->updateContent()V

    .line 262
    if-eqz p4, :cond_3

    invoke-virtual {p0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->isDrawerOpen()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 263
    invoke-virtual {p0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->toggleDrawer()V

    .line 264
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public initEmpty()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 229
    const-string v0, ""

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v2, v1, v2}, Lcom/google/android/videos/ui/VideosDrawerHelper;->init(Ljava/lang/String;IIZ)Z

    move-result v0

    return v0
.end method

.method public isDrawerOpen()Z
    .locals 1

    .prologue
    .line 301
    iget-boolean v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->alwaysOpen:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->playDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->isDrawerOpen()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAccountListToggleButtonClicked(Z)V
    .locals 0
    .param p1, "isListExpanded"    # Z

    .prologue
    .line 443
    return-void
.end method

.method public onAccountsUpdated([Landroid/accounts/Account;)V
    .locals 0
    .param p1, "accounts"    # [Landroid/accounts/Account;

    .prologue
    .line 398
    invoke-direct {p0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->updateContent()V

    .line 399
    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 285
    iget-boolean v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->alwaysOpen:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->isDrawerOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    invoke-virtual {p0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->toggleDrawer()V

    .line 287
    const/4 v0, 0x1

    .line 289
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCurrentAccountClicked(ZLcom/google/android/finsky/protos/DocumentV2$DocV2;)Z
    .locals 1
    .param p1, "isLoaded"    # Z
    .param p2, "profileDoc"    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .prologue
    .line 403
    const/4 v0, 0x0

    return v0
.end method

.method public onDownloadToggleClicked(Z)V
    .locals 1
    .param p1, "isDownloadOnly"    # Z

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->setDownloadedOnly(Z)V

    .line 448
    return-void
.end method

.method public onDownloadedOnlyChanged(Z)V
    .locals 0
    .param p1, "downloadedOnly"    # Z

    .prologue
    .line 452
    invoke-direct {p0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->updateContent()V

    .line 453
    return-void
.end method

.method public onDrawerClosed(Landroid/view/View;)V
    .locals 1
    .param p1, "drawer"    # Landroid/view/View;

    .prologue
    .line 367
    invoke-direct {p0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->setDrawerClosedFlag()V

    .line 368
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->drawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->drawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    invoke-interface {v0, p1}, Landroid/support/v4/widget/DrawerLayout$DrawerListener;->onDrawerClosed(Landroid/view/View;)V

    .line 371
    :cond_0
    return-void
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 1
    .param p1, "drawer"    # Landroid/view/View;

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->drawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->drawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    invoke-interface {v0, p1}, Landroid/support/v4/widget/DrawerLayout$DrawerListener;->onDrawerOpened(Landroid/view/View;)V

    .line 378
    :cond_0
    return-void
.end method

.method public onDrawerSlide(Landroid/view/View;F)V
    .locals 1
    .param p1, "drawer"    # Landroid/view/View;
    .param p2, "slideOffset"    # F

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->drawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->drawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    invoke-interface {v0, p1, p2}, Landroid/support/v4/widget/DrawerLayout$DrawerListener;->onDrawerSlide(Landroid/view/View;F)V

    .line 385
    :cond_0
    return-void
.end method

.method public onDrawerStateChanged(I)V
    .locals 1
    .param p1, "newState"    # I

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->drawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->drawerListener:Landroid/support/v4/widget/DrawerLayout$DrawerListener;

    invoke-interface {v0, p1}, Landroid/support/v4/widget/DrawerLayout$DrawerListener;->onDrawerStateChanged(I)V

    .line 392
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->playDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPrimaryActionClicked(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;)Z
    .locals 2
    .param p1, "primaryAction"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    .prologue
    .line 424
    iget-object v1, p1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;->actionSelectedRunnable:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 425
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->verticalItems:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 426
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->verticalItems:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_1

    .line 427
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->verticalItems:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->activeVerticalKey:I

    .line 431
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->updateContent()V

    .line 432
    iget-boolean v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->alwaysOpen:Z

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    return v1

    .line 425
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 432
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public onSecondaryAccountClicked(Ljava/lang/String;)Z
    .locals 4
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 410
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->account:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 419
    :cond_0
    :goto_0
    return v0

    .line 415
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->account:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 416
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    const-string v2, "user_account"

    invoke-interface {v1, v2, v3}, Lcom/google/android/videos/logging/EventLogger;->onPreferenceChange(Ljava/lang/String;Ljava/lang/Object;)V

    .line 417
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->activity:Lcom/google/android/videos/activity/DrawerActivity;

    iget-object v2, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->activity:Lcom/google/android/videos/activity/DrawerActivity;

    invoke-static {v2, p1, v3}, Lcom/google/android/videos/activity/HomeActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/videos/activity/DrawerActivity;->startActivity(Landroid/content/Intent;)V

    .line 419
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->alwaysOpen:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onSecondaryActionClicked(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;)Z
    .locals 1
    .param p1, "secondaryAction"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;

    .prologue
    .line 437
    iget-object v0, p1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerSecondaryAction;->actionSelectedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 438
    iget-boolean v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->alwaysOpen:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 224
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V

    .line 225
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->playCommonNetworkStackWrapper:Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;->unregister()V

    .line 226
    return-void
.end method

.method public selectVertical(I)Z
    .locals 2
    .param p1, "verticalToSelect"    # I

    .prologue
    .line 272
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->verticalItems:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;

    .line 273
    .local v0, "verticalItem":Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;
    if-eqz v0, :cond_0

    .line 274
    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->onPrimaryActionClicked(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerPrimaryAction;)Z

    .line 275
    const/4 v1, 0x1

    .line 277
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDrawerIndicatorEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->playDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->setDrawerIndicatorEnabled(Z)V

    .line 282
    return-void
.end method

.method public syncState()V
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->playDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->syncDrawerIndicator()V

    .line 298
    return-void
.end method

.method public toggleDrawer()V
    .locals 1

    .prologue
    .line 309
    iget-boolean v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->alwaysOpen:Z

    if-nez v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/google/android/videos/ui/VideosDrawerHelper;->playDrawerLayout:Lcom/google/android/play/drawer/PlayDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/play/drawer/PlayDrawerLayout;->toggleDrawer()V

    .line 312
    :cond_0
    return-void
.end method
