.class public abstract Lcom/google/android/videos/activity/DrawerActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "DrawerActivity.java"


# instance fields
.field protected videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/videos/activity/DrawerActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/activity/DrawerActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->onBackPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    :goto_0
    return-void

    .line 37
    :cond_0
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onHelpSelected()V
    .locals 1

    .prologue
    .line 55
    const-string v0, "mobile_movies_default"

    invoke-static {p0, v0}, Lcom/google/android/videos/ui/HelpHelper;->startContextualHelp(Landroid/app/Activity;Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/Util;->handlesMenuKeyEvent(ILandroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/support/v7/app/ActionBarActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 71
    invoke-static {p1, p0}, Lcom/google/android/videos/utils/Util;->handlesMenuKeyEvent(ILandroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/support/v7/app/ActionBarActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 43
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0f0221

    if-ne v1, v2, :cond_1

    .line 44
    invoke-virtual {p0}, Lcom/google/android/videos/activity/DrawerActivity;->onHelpSelected()V

    .line 50
    :cond_0
    :goto_0
    return v0

    .line 47
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/activity/DrawerActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/videos/activity/DrawerActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/ui/VideosDrawerHelper;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 50
    :cond_2
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 29
    invoke-virtual {p0}, Lcom/google/android/videos/activity/DrawerActivity;->syncDrawerState()V

    .line 30
    return-void
.end method

.method public resetDrawer()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/videos/activity/DrawerActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/google/android/videos/activity/DrawerActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->reset()V

    .line 62
    :cond_0
    return-void
.end method

.method public syncDrawerState()V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/videos/activity/DrawerActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    if-eqz v0, :cond_0

    .line 22
    iget-object v0, p0, Lcom/google/android/videos/activity/DrawerActivity;->videosDrawerHelper:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/VideosDrawerHelper;->syncState()V

    .line 24
    :cond_0
    return-void
.end method
