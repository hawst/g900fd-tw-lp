.class Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;
.super Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;
.source "AudioCcDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/overlay/AudioCcDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SubtitleListHelper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper",
        "<",
        "Lcom/google/android/videos/subtitles/SubtitleTrack;",
        ">;"
    }
.end annotation


# instance fields
.field private selectedPosition:I

.field final synthetic this$0:Lcom/google/android/videos/player/overlay/AudioCcDialog;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/player/overlay/AudioCcDialog;Landroid/content/Context;Ljava/util/ArrayList;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p4, "selectedPosition"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 252
    .local p3, "subtitles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    iput-object p1, p0, Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;->this$0:Lcom/google/android/videos/player/overlay/AudioCcDialog;

    .line 253
    invoke-direct {p0, p2, p3}, Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 254
    iput p4, p0, Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;->selectedPosition:I

    .line 255
    return-void
.end method

.method private getCaptionSettingsIntentV19(Landroid/app/Activity;)Landroid/content/Intent;
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 306
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.CAPTIONING_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 307
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p1, v0}, Lcom/google/android/videos/utils/Util;->canResolveIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-object v0

    .restart local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getFooter(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 259
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x109000f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 261
    .local v0, "footer":Landroid/widget/CheckedTextView;
    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;->adjustItemPadding(Landroid/view/View;)V

    .line 262
    const v1, 0x7f02010e

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setCheckMarkDrawable(I)V

    .line 263
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 264
    const v1, 0x7f0b00d4

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setText(I)V

    .line 265
    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 270
    move-object v0, p2

    check-cast v0, Landroid/widget/TextView;

    .line 271
    .local v0, "view":Landroid/widget/TextView;
    if-nez v0, :cond_0

    .line 272
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x109000f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .end local v0    # "view":Landroid/widget/TextView;
    check-cast v0, Landroid/widget/TextView;

    .line 274
    .restart local v0    # "view":Landroid/widget/TextView;
    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;->adjustItemPadding(Landroid/view/View;)V

    .line 276
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/subtitles/SubtitleTrack;

    invoke-virtual {v1}, Lcom/google/android/videos/subtitles/SubtitleTrack;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 282
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;->this$0:Lcom/google/android/videos/player/overlay/AudioCcDialog;

    invoke-virtual {v4}, Lcom/google/android/videos/player/overlay/AudioCcDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 283
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_0

    .line 302
    .end local v0    # "activity":Landroid/app/Activity;
    :goto_0
    return-void

    .line 286
    .restart local v0    # "activity":Landroid/app/Activity;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;->getCount()I

    move-result v4

    if-ne p3, v4, :cond_2

    move-object v2, p1

    .line 288
    check-cast v2, Landroid/widget/ListView;

    .line 289
    .local v2, "listView":Landroid/widget/ListView;
    iget v4, p0, Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;->selectedPosition:I

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 291
    sget v4, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;->getCaptionSettingsIntentV19(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v1

    .line 293
    .local v1, "captionSettingsIntent":Landroid/content/Intent;
    :goto_1
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;->this$0:Lcom/google/android/videos/player/overlay/AudioCcDialog;

    invoke-virtual {v4, v1}, Lcom/google/android/videos/player/overlay/AudioCcDialog;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 291
    .end local v1    # "captionSettingsIntent":Landroid/content/Intent;
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/google/android/videos/activity/CaptionSettingsActivity;

    invoke-direct {v1, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    .line 296
    .end local v2    # "listView":Landroid/widget/ListView;
    :cond_2
    iput p3, p0, Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;->selectedPosition:I

    .line 297
    invoke-virtual {p0, p3}, Lcom/google/android/videos/player/overlay/AudioCcDialog$SubtitleListHelper;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/subtitles/SubtitleTrack;

    .line 298
    .local v3, "selected":Lcom/google/android/videos/subtitles/SubtitleTrack;
    invoke-virtual {v3}, Lcom/google/android/videos/subtitles/SubtitleTrack;->isDisableTrack()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 299
    const/4 v3, 0x0

    .line 301
    :cond_3
    check-cast v0, Lcom/google/android/videos/player/overlay/TrackChangeListener;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-interface {v0, v3}, Lcom/google/android/videos/player/overlay/TrackChangeListener;->onSelectSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;)V

    goto :goto_0
.end method
