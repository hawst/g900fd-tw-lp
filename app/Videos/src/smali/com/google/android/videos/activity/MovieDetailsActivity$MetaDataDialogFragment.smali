.class public Lcom/google/android/videos/activity/MovieDetailsActivity$MetaDataDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "MovieDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/MovieDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MetaDataDialogFragment"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1111
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method private showCredits(Ljava/util/List;ILandroid/widget/TextView;)V
    .locals 8
    .param p2, "labelStringId"    # I
    .param p3, "creditsView"    # Landroid/widget/TextView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Landroid/widget/TextView;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "credits":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 1177
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity$MetaDataDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1178
    .local v2, "resources":Landroid/content/res/Resources;
    invoke-static {v2, v7, p1}, Lcom/google/android/videos/utils/Util;->buildListString(Landroid/content/res/Resources;ZLjava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 1179
    .local v0, "creditsText":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1180
    const/16 v4, 0x8

    invoke-virtual {p3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1189
    :goto_0
    return-void

    .line 1182
    :cond_0
    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1183
    .local v1, "label":Ljava/lang/String;
    new-instance v3, Landroid/text/SpannableString;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x20

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1184
    .local v3, "text":Landroid/text/SpannableString;
    new-instance v4, Landroid/text/style/StyleSpan;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x21

    invoke-virtual {v3, v4, v7, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1186
    invoke-virtual {p3, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1187
    invoke-virtual {p3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static showInstance(Landroid/support/v4/app/FragmentActivity;Lcom/google/android/videos/store/VideoMetadata;)V
    .locals 4
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p1, "video"    # Lcom/google/android/videos/store/VideoMetadata;

    .prologue
    .line 1115
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1116
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "video"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1118
    new-instance v1, Lcom/google/android/videos/activity/MovieDetailsActivity$MetaDataDialogFragment;

    invoke-direct {v1}, Lcom/google/android/videos/activity/MovieDetailsActivity$MetaDataDialogFragment;-><init>()V

    .line 1119
    .local v1, "instance":Lcom/google/android/videos/activity/MovieDetailsActivity$MetaDataDialogFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/videos/activity/MovieDetailsActivity$MetaDataDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1120
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-class v3, Lcom/google/android/videos/activity/MovieDetailsActivity$MetaDataDialogFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/activity/MovieDetailsActivity$MetaDataDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1121
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1125
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1127
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/activity/MovieDetailsActivity$MetaDataDialogFragment;->setStyle(II)V

    .line 1128
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 13
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1133
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity$MetaDataDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1135
    .local v2, "resources":Landroid/content/res/Resources;
    const v8, 0x7f040074

    const/4 v9, 0x0

    invoke-virtual {p1, v8, p2, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 1137
    .local v7, "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MovieDetailsActivity$MetaDataDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 1138
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v8, "video"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Lcom/google/android/videos/store/VideoMetadata;

    .line 1140
    .local v6, "video":Lcom/google/android/videos/store/VideoMetadata;
    const v8, 0x7f0f00a8

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1141
    .local v5, "titleView":Landroid/widget/TextView;
    iget-object v8, v6, Lcom/google/android/videos/store/VideoMetadata;->title:Ljava/lang/String;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1143
    const v8, 0x7f0f017d

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1144
    .local v4, "subheadingView":Landroid/widget/TextView;
    const/4 v8, 0x3

    new-array v3, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    iget-object v9, v6, Lcom/google/android/videos/store/VideoMetadata;->classification:Ljava/lang/String;

    aput-object v9, v3, v8

    const/4 v9, 0x1

    iget v8, v6, Lcom/google/android/videos/store/VideoMetadata;->releaseYear:I

    if-lez v8, :cond_0

    iget v8, v6, Lcom/google/android/videos/store/VideoMetadata;->releaseYear:I

    invoke-static {v8}, Lcom/google/android/videos/utils/TimeUtil;->getStandaloneYearString(I)Ljava/lang/String;

    move-result-object v8

    :goto_0
    aput-object v8, v3, v9

    const/4 v9, 0x2

    iget v8, v6, Lcom/google/android/videos/store/VideoMetadata;->durationSecs:I

    if-lez v8, :cond_1

    const v8, 0x7f0b0138

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget v12, v6, Lcom/google/android/videos/store/VideoMetadata;->durationSecs:I

    div-int/lit8 v12, v12, 0x3c

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v2, v8, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    :goto_1
    aput-object v8, v3, v9

    .line 1150
    .local v3, "subHeadingStrings":[Ljava/lang/String;
    const/4 v8, 0x1

    invoke-static {v2, v8, v3}, Lcom/google/android/videos/utils/Util;->buildListString(Landroid/content/res/Resources;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1152
    const v8, 0x7f0f0102

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1153
    .local v1, "descriptionView":Landroid/widget/TextView;
    iget-object v8, v6, Lcom/google/android/videos/store/VideoMetadata;->description:Ljava/lang/String;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1154
    new-instance v8, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v8}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1156
    const v8, 0x7f0f0181

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iget-boolean v8, v6, Lcom/google/android/videos/store/VideoMetadata;->purchasedHd:Z

    if-eqz v8, :cond_2

    const/4 v8, 0x0

    :goto_2
    invoke-virtual {v9, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1157
    const v8, 0x7f0f0182

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iget-boolean v8, v6, Lcom/google/android/videos/store/VideoMetadata;->surroundSoundPurchasedAndAvailable:Z

    if-eqz v8, :cond_3

    const/4 v8, 0x0

    :goto_3
    invoke-virtual {v9, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1159
    const v8, 0x7f0f0183

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iget-boolean v8, v6, Lcom/google/android/videos/store/VideoMetadata;->knowledgeEnabled:Z

    if-eqz v8, :cond_4

    const/4 v8, 0x0

    :goto_4
    invoke-virtual {v9, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1161
    const v8, 0x7f0f0184

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iget-boolean v8, v6, Lcom/google/android/videos/store/VideoMetadata;->captionAvailable:Z

    if-eqz v8, :cond_5

    const/4 v8, 0x0

    :goto_5
    invoke-virtual {v9, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1164
    iget-object v9, v6, Lcom/google/android/videos/store/VideoMetadata;->directors:Ljava/util/List;

    const v10, 0x7f0b0135

    const v8, 0x7f0f0186

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-direct {p0, v9, v10, v8}, Lcom/google/android/videos/activity/MovieDetailsActivity$MetaDataDialogFragment;->showCredits(Ljava/util/List;ILandroid/widget/TextView;)V

    .line 1166
    iget-object v9, v6, Lcom/google/android/videos/store/VideoMetadata;->writers:Ljava/util/List;

    const v10, 0x7f0b0136

    const v8, 0x7f0f0187

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-direct {p0, v9, v10, v8}, Lcom/google/android/videos/activity/MovieDetailsActivity$MetaDataDialogFragment;->showCredits(Ljava/util/List;ILandroid/widget/TextView;)V

    .line 1168
    iget-object v9, v6, Lcom/google/android/videos/store/VideoMetadata;->actors:Ljava/util/List;

    const v10, 0x7f0b0134

    const v8, 0x7f0f0185

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-direct {p0, v9, v10, v8}, Lcom/google/android/videos/activity/MovieDetailsActivity$MetaDataDialogFragment;->showCredits(Ljava/util/List;ILandroid/widget/TextView;)V

    .line 1170
    iget-object v9, v6, Lcom/google/android/videos/store/VideoMetadata;->producers:Ljava/util/List;

    const v10, 0x7f0b0137

    const v8, 0x7f0f0188

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-direct {p0, v9, v10, v8}, Lcom/google/android/videos/activity/MovieDetailsActivity$MetaDataDialogFragment;->showCredits(Ljava/util/List;ILandroid/widget/TextView;)V

    .line 1173
    return-object v7

    .line 1144
    .end local v1    # "descriptionView":Landroid/widget/TextView;
    .end local v3    # "subHeadingStrings":[Ljava/lang/String;
    :cond_0
    const/4 v8, 0x0

    goto/16 :goto_0

    :cond_1
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 1156
    .restart local v1    # "descriptionView":Landroid/widget/TextView;
    .restart local v3    # "subHeadingStrings":[Ljava/lang/String;
    :cond_2
    const/16 v8, 0x8

    goto :goto_2

    .line 1157
    :cond_3
    const/16 v8, 0x8

    goto :goto_3

    .line 1159
    :cond_4
    const/16 v8, 0x8

    goto :goto_4

    .line 1161
    :cond_5
    const/16 v8, 0x8

    goto :goto_5
.end method
