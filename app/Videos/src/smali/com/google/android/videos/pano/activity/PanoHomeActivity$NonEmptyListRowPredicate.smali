.class Lcom/google/android/videos/pano/activity/PanoHomeActivity$NonEmptyListRowPredicate;
.super Ljava/lang/Object;
.source "PanoHomeActivity.java"

# interfaces
.implements Lcom/google/android/repolib/common/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/activity/PanoHomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NonEmptyListRowPredicate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Predicate",
        "<",
        "Landroid/support/v17/leanback/widget/ListRow;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pano/activity/PanoHomeActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pano/activity/PanoHomeActivity$1;

    .prologue
    .line 364
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/PanoHomeActivity$NonEmptyListRowPredicate;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Landroid/support/v17/leanback/widget/ListRow;)Z
    .locals 1
    .param p1, "input"    # Landroid/support/v17/leanback/widget/ListRow;

    .prologue
    .line 367
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ListRow;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 364
    check-cast p1, Landroid/support/v17/leanback/widget/ListRow;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/pano/activity/PanoHomeActivity$NonEmptyListRowPredicate;->apply(Landroid/support/v17/leanback/widget/ListRow;)Z

    move-result v0

    return v0
.end method
