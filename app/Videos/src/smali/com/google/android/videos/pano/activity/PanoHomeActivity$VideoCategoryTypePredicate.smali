.class Lcom/google/android/videos/pano/activity/PanoHomeActivity$VideoCategoryTypePredicate;
.super Ljava/lang/Object;
.source "PanoHomeActivity.java"

# interfaces
.implements Lcom/google/android/repolib/common/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/activity/PanoHomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VideoCategoryTypePredicate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Predicate",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/VideoCategory;",
        ">;"
    }
.end annotation


# instance fields
.field private final type:I


# direct methods
.method private constructor <init>(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 355
    iput p1, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity$VideoCategoryTypePredicate;->type:I

    .line 356
    return-void
.end method

.method synthetic constructor <init>(ILcom/google/android/videos/pano/activity/PanoHomeActivity$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # Lcom/google/android/videos/pano/activity/PanoHomeActivity$1;

    .prologue
    .line 351
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/activity/PanoHomeActivity$VideoCategoryTypePredicate;-><init>(I)V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/wireless/android/video/magma/proto/VideoCategory;)Z
    .locals 2
    .param p1, "input"    # Lcom/google/wireless/android/video/magma/proto/VideoCategory;

    .prologue
    .line 360
    iget v0, p1, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->type:I

    iget v1, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity$VideoCategoryTypePredicate;->type:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 351
    check-cast p1, Lcom/google/wireless/android/video/magma/proto/VideoCategory;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/pano/activity/PanoHomeActivity$VideoCategoryTypePredicate;->apply(Lcom/google/wireless/android/video/magma/proto/VideoCategory;)Z

    move-result v0

    return v0
.end method
