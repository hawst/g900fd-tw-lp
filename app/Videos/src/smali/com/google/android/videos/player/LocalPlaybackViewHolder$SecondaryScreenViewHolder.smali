.class public Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;
.super Landroid/app/Presentation;
.source "LocalPlaybackViewHolder.java"

# interfaces
.implements Lcom/google/android/videos/player/LocalPlaybackViewHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/LocalPlaybackViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SecondaryScreenViewHolder"
.end annotation


# instance fields
.field private final localKnowledgeOverlay:Lcom/google/android/videos/tagging/KnowledgeView;

.field private playerView:Lcom/google/android/videos/player/PlayerView;

.field private remoteScreenInfoOverlay:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

.field private subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

.field private taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/Display;Lcom/google/android/videos/tagging/KnowledgeView;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "display"    # Landroid/view/Display;
    .param p3, "localKnowledgeOverlay"    # Lcom/google/android/videos/tagging/KnowledgeView;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException;
        }
    .end annotation

    .prologue
    .line 113
    const v1, 0x7f100167

    invoke-direct {p0, p1, p2, v1}, Landroid/app/Presentation;-><init>(Landroid/content/Context;Landroid/view/Display;I)V

    .line 114
    iput-object p3, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->localKnowledgeOverlay:Lcom/google/android/videos/tagging/KnowledgeView;

    .line 116
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$InvalidDisplayException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    return-void

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Landroid/view/WindowManager$InvalidDisplayException;
    new-instance v1, Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException;

    invoke-virtual {v0}, Landroid/view/WindowManager$InvalidDisplayException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public getPlayerView()Lcom/google/android/videos/player/PlayerView;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->playerView:Lcom/google/android/videos/player/PlayerView;

    return-object v0
.end method

.method public getRemoteScreenInfoOverlay()Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->remoteScreenInfoOverlay:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    return-object v0
.end method

.method public getSubtitlesOverlay()Lcom/google/android/videos/player/overlay/SubtitlesOverlay;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    return-object v0
.end method

.method public getTaggingKnowledgeView()Lcom/google/android/videos/tagging/KnowledgeView;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    return-object v0
.end method

.method public getTaggingKnowledgeViewResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public getTaglessKnowledgeView()Lcom/google/android/videos/tagging/KnowledgeView;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->localKnowledgeOverlay:Lcom/google/android/videos/tagging/KnowledgeView;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 124
    invoke-super {p0, p1}, Landroid/app/Presentation;->onCreate(Landroid/os/Bundle;)V

    .line 125
    invoke-virtual {p0}, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 126
    .local v0, "context":Landroid/content/Context;
    new-instance v2, Lcom/google/android/videos/player/PlayerView;

    invoke-direct {v2, v0}, Lcom/google/android/videos/player/PlayerView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->playerView:Lcom/google/android/videos/player/PlayerView;

    .line 127
    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->playerView:Lcom/google/android/videos/player/PlayerView;

    invoke-virtual {v2, v5}, Lcom/google/android/videos/player/PlayerView;->setMakeSafeForOverscan(Z)V

    .line 128
    new-instance v2, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;

    invoke-direct {v2, v0}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    .line 129
    new-instance v2, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    invoke-direct {v2, v0}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->remoteScreenInfoOverlay:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    .line 130
    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->localKnowledgeOverlay:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v2, :cond_0

    .line 131
    new-instance v1, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;

    invoke-direct {v1, v0}, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;-><init>(Landroid/content/Context;)V

    .line 132
    .local v1, "knowledgeOverlay":Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;
    iput-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    .line 133
    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->playerView:Lcom/google/android/videos/player/PlayerView;

    const/4 v3, 0x3

    new-array v3, v3, [Lcom/google/android/videos/player/PlayerView$PlayerOverlay;

    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    aput-object v4, v3, v6

    aput-object v1, v3, v5

    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->remoteScreenInfoOverlay:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    aput-object v4, v3, v7

    invoke-virtual {v2, v3}, Lcom/google/android/videos/player/PlayerView;->addOverlays([Lcom/google/android/videos/player/PlayerView$PlayerOverlay;)V

    .line 137
    .end local v1    # "knowledgeOverlay":Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->playerView:Lcom/google/android/videos/player/PlayerView;

    invoke-virtual {p0, v2}, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->setContentView(Landroid/view/View;)V

    .line 138
    return-void

    .line 135
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->playerView:Lcom/google/android/videos/player/PlayerView;

    new-array v3, v7, [Lcom/google/android/videos/player/PlayerView$PlayerOverlay;

    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->remoteScreenInfoOverlay:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Lcom/google/android/videos/player/PlayerView;->addOverlays([Lcom/google/android/videos/player/PlayerView$PlayerOverlay;)V

    goto :goto_0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/google/android/videos/player/LocalPlaybackViewHolder$SecondaryScreenViewHolder;->cancel()V

    .line 173
    return-void
.end method
