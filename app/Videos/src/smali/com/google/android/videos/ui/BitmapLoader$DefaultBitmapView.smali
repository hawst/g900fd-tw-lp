.class Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;
.super Ljava/lang/Object;
.source "BitmapLoader.java"

# interfaces
.implements Lcom/google/android/videos/ui/BitmapLoader$BitmapView;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/BitmapLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DefaultBitmapView"
.end annotation


# instance fields
.field private animator:Landroid/animation/Animator;

.field private final imageView:Landroid/widget/ImageView;

.field private missingThumbnail:Z

.field private final noThumbnailBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private final resources:Landroid/content/res/Resources;

.field private final selector:Landroid/graphics/drawable/Drawable;

.field private final shouldSetScaleType:Z

.field private thumbnail:Landroid/graphics/Bitmap;

.field private thumbnailTag:Ljava/lang/Object;


# direct methods
.method private constructor <init>(Landroid/widget/ImageView;ZLandroid/graphics/drawable/Drawable;Z)V
    .locals 6
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "shouldSetScaleType"    # Z
    .param p3, "noThumbnailBackgroundDrawable"    # Landroid/graphics/drawable/Drawable;
    .param p4, "selectable"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 238
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->imageView:Landroid/widget/ImageView;

    .line 239
    iput-boolean p2, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->shouldSetScaleType:Z

    .line 240
    iput-object p3, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->noThumbnailBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 241
    if-eqz p4, :cond_0

    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    :goto_0
    iput-object v2, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->resources:Landroid/content/res/Resources;

    .line 242
    if-eqz p4, :cond_1

    .line 243
    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [I

    const v4, 0x7f010051

    aput v4, v3, v5

    invoke-virtual {v2, v3}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 245
    .local v0, "attrs":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 246
    .local v1, "selectableBackground":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 247
    iput-object v1, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->selector:Landroid/graphics/drawable/Drawable;

    .line 251
    .end local v0    # "attrs":Landroid/content/res/TypedArray;
    .end local v1    # "selectableBackground":Landroid/graphics/drawable/Drawable;
    :goto_1
    return-void

    :cond_0
    move-object v2, v3

    .line 241
    goto :goto_0

    .line 249
    :cond_1
    iput-object v3, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->selector:Landroid/graphics/drawable/Drawable;

    goto :goto_1
.end method

.method synthetic constructor <init>(Landroid/widget/ImageView;ZLandroid/graphics/drawable/Drawable;ZLcom/google/android/videos/ui/BitmapLoader$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/ImageView;
    .param p2, "x1"    # Z
    .param p3, "x2"    # Landroid/graphics/drawable/Drawable;
    .param p4, "x3"    # Z
    .param p5, "x4"    # Lcom/google/android/videos/ui/BitmapLoader$1;

    .prologue
    .line 222
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;-><init>(Landroid/widget/ImageView;ZLandroid/graphics/drawable/Drawable;Z)V

    return-void
.end method

.method private cancelAnimationsV16()V
    .locals 2

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->animator:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->animator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 312
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->imageView:Landroid/widget/ImageView;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 314
    :cond_0
    return-void
.end method

.method private fadeInImageViewV16()V
    .locals 4

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->animator:Landroid/animation/Animator;

    if-nez v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->imageView:Landroid/widget/ImageView;

    const-string v1, "imageAlpha"

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x10e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->animator:Landroid/animation/Animator;

    .line 321
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->animator:Landroid/animation/Animator;

    new-instance v1, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView$1;

    invoke-direct {v1, p0}, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView$1;-><init>(Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 330
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->animator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 331
    return-void

    .line 328
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->animator:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    goto :goto_0

    .line 319
    nop

    :array_0
    .array-data 4
        0x0
        0xff
    .end array-data
.end method


# virtual methods
.method public getThumbnailTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->thumbnailTag:Ljava/lang/Object;

    return-object v0
.end method

.method onAnimationEnd()V
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->noThumbnailBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->imageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/ViewUtil;->setViewBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 306
    :cond_0
    return-void
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;Z)V
    .locals 4
    .param p1, "thumbnail"    # Landroid/graphics/Bitmap;
    .param p2, "missingThumbnail"    # Z

    .prologue
    const/16 v3, 0x10

    .line 265
    iget-object v1, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->thumbnail:Landroid/graphics/Bitmap;

    if-ne v1, p1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->missingThumbnail:Z

    if-ne v1, p2, :cond_0

    .line 300
    :goto_0
    return-void

    .line 269
    :cond_0
    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    if-lt v1, v3, :cond_1

    .line 270
    invoke-direct {p0}, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->cancelAnimationsV16()V

    .line 273
    :cond_1
    iput-object p1, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->thumbnail:Landroid/graphics/Bitmap;

    .line 274
    iput-boolean p2, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->missingThumbnail:Z

    .line 276
    if-eqz p2, :cond_4

    .line 277
    iget-boolean v1, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->shouldSetScaleType:Z

    if-eqz v1, :cond_2

    .line 278
    iget-object v1, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->imageView:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 280
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->noThumbnailBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    .line 281
    iget-object v1, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->imageView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->noThumbnailBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/ViewUtil;->setViewBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 294
    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->selector:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_7

    .line 295
    const/4 v1, 0x2

    new-array v0, v1, [Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->resources:Landroid/content/res/Resources;

    invoke-direct {v2, v3, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->selector:Landroid/graphics/drawable/Drawable;

    aput-object v2, v0, v1

    .line 296
    .local v0, "layers":[Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->imageView:Landroid/widget/ImageView;

    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v2, v0}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 284
    .end local v0    # "layers":[Landroid/graphics/drawable/Drawable;
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->shouldSetScaleType:Z

    if-eqz v1, :cond_5

    .line 285
    iget-object v1, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->imageView:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 287
    :cond_5
    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    if-lt v1, v3, :cond_6

    .line 288
    invoke-direct {p0}, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->fadeInImageViewV16()V

    goto :goto_1

    .line 290
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->onAnimationEnd()V

    goto :goto_1

    .line 298
    :cond_7
    iget-object v1, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public bridge synthetic setThumbnail(Ljava/lang/Object;Z)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Z

    .prologue
    .line 222
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->setThumbnail(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method public setThumbnailTag(Ljava/lang/Object;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/Object;

    .prologue
    .line 260
    iput-object p1, p0, Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;->thumbnailTag:Ljava/lang/Object;

    .line 261
    return-void
.end method
