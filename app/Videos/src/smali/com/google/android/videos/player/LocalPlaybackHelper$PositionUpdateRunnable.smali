.class Lcom/google/android/videos/player/LocalPlaybackHelper$PositionUpdateRunnable;
.super Ljava/lang/Object;
.source "LocalPlaybackHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/LocalPlaybackHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PositionUpdateRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/player/LocalPlaybackHelper;)V
    .locals 0

    .prologue
    .line 1301
    iput-object p1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$PositionUpdateRunnable;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/player/LocalPlaybackHelper;Lcom/google/android/videos/player/LocalPlaybackHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/player/LocalPlaybackHelper;
    .param p2, "x1"    # Lcom/google/android/videos/player/LocalPlaybackHelper$1;

    .prologue
    .line 1301
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/LocalPlaybackHelper$PositionUpdateRunnable;-><init>(Lcom/google/android/videos/player/LocalPlaybackHelper;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1305
    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$PositionUpdateRunnable;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;
    invoke-static {v2}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$1000(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/player/VideosPlayer;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1312
    :goto_0
    return-void

    .line 1308
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$PositionUpdateRunnable;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;
    invoke-static {v2}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$1000(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/player/VideosPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/player/VideosPlayer;->getCurrentPosition()I

    move-result v1

    .line 1309
    .local v1, "currentTimeMillis":I
    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$PositionUpdateRunnable;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;
    invoke-static {v2}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$1000(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/player/VideosPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/player/VideosPlayer;->getBufferedPercentage()I

    move-result v0

    .line 1310
    .local v0, "bufferedPercent":I
    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$PositionUpdateRunnable;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;
    invoke-static {v2}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$900(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/player/overlay/ControllerOverlay;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$PositionUpdateRunnable;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;
    invoke-static {v3}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$700(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    move-result-object v3

    iget v3, v3, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->durationMillis:I

    invoke-interface {v2, v1, v3, v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setTimes(III)V

    .line 1311
    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$PositionUpdateRunnable;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    const/16 v3, 0x3e8

    invoke-virtual {v2, v3}, Lcom/google/android/videos/player/LocalPlaybackHelper;->maybeSchedulePositionUpdate(I)V

    goto :goto_0
.end method
