.class public final Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;
.super Ljava/lang/Object;
.source "PurchaseRequestRepository.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/repolib/observers/UpdatablesChanged;
.implements Lcom/google/android/repolib/repositories/Repository;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/observers/Updatable;",
        "Lcom/google/android/repolib/observers/UpdatablesChanged;",
        "Lcom/google/android/repolib/repositories/Repository",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final database:Lcom/google/android/videos/store/Database;

.field private final lowPassUpdateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

.field private final repository:Lcom/google/android/repolib/repositories/Repository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final signInManager:Lcom/google/android/videos/accounts/SignInManager;

.field private final updateDatabaseDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

.field private final updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/repolib/common/Factory;Lcom/google/android/repolib/common/Creator;Lcom/google/android/videos/store/Database;Ljava/util/concurrent/Executor;Lcom/google/android/repolib/common/Action;)V
    .locals 2
    .param p1, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .param p5, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/accounts/SignInManager;",
            "Lcom/google/android/repolib/common/Factory",
            "<+TT;",
            "Landroid/database/Cursor;",
            ">;",
            "Lcom/google/android/repolib/common/Creator",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Lcom/google/android/videos/store/Database;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/repolib/common/Action",
            "<",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p0, "this":Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;, "Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository<TT;>;"
    .local p2, "itemFactory":Lcom/google/android/repolib/common/Factory;, "Lcom/google/android/repolib/common/Factory<+TT;Landroid/database/Cursor;>;"
    .local p3, "cursorCreator":Lcom/google/android/repolib/common/Creator;, "Lcom/google/android/repolib/common/Creator<Landroid/database/Cursor;>;"
    .local p6, "errorHandler":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<Ljava/lang/Throwable;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->database:Lcom/google/android/videos/store/Database;

    .line 49
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/accounts/SignInManager;

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    .line 50
    const/16 v0, 0x1388

    invoke-static {v0}, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;->lowPassFilterUpdateDispatcher(I)Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->lowPassUpdateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 51
    invoke-static {}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher()Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->updateDatabaseDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 52
    invoke-static {p3, p2}, Lcom/google/android/repolib/database/DefaultCursorDataSource;->defaultCursorDataSource(Lcom/google/android/repolib/common/Creator;Lcom/google/android/repolib/common/Factory;)Lcom/google/android/repolib/database/CursorDataSource;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->updateDatabaseDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-static {p5, v0, v1, p6}, Lcom/google/android/repolib/database/DatabaseRepository;->databaseRepository(Ljava/util/concurrent/Executor;Lcom/google/android/repolib/database/CursorDataSource;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/repolib/common/Action;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->repository:Lcom/google/android/repolib/repositories/Repository;

    .line 55
    invoke-static {p0}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher(Lcom/google/android/repolib/observers/UpdatablesChanged;)Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 56
    return-void
.end method

.method public static purchaseRequestRepository(Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/repolib/common/Factory;Lcom/google/android/repolib/common/Creator;Lcom/google/android/videos/store/Database;Lcom/google/android/repolib/common/Action;Ljava/util/concurrent/Executor;)Lcom/google/android/repolib/repositories/Repository;
    .locals 7
    .param p0, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p3, "database"    # Lcom/google/android/videos/store/Database;
    .param p5, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/accounts/SignInManager;",
            "Lcom/google/android/repolib/common/Factory",
            "<+TT;",
            "Landroid/database/Cursor;",
            ">;",
            "Lcom/google/android/repolib/common/Creator",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/repolib/common/Action",
            "<",
            "Ljava/lang/Throwable;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            ")",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 62
    .local p1, "itemFactory":Lcom/google/android/repolib/common/Factory;, "Lcom/google/android/repolib/common/Factory<+TT;Landroid/database/Cursor;>;"
    .local p2, "requestCreator":Lcom/google/android/repolib/common/Creator;, "Lcom/google/android/repolib/common/Creator<Landroid/database/Cursor;>;"
    .local p4, "errorHandler":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<Ljava/lang/Throwable;>;"
    new-instance v0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;-><init>(Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/repolib/common/Factory;Lcom/google/android/repolib/common/Creator;Lcom/google/android/videos/store/Database;Ljava/util/concurrent/Executor;Lcom/google/android/repolib/common/Action;)V

    return-object v0
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 85
    .local p0, "this":Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;, "Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 86
    return-void
.end method

.method public firstUpdatableAdded()V
    .locals 2

    .prologue
    .line 68
    .local p0, "this":Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;, "Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->repository:Lcom/google/android/repolib/repositories/Repository;

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, v1}, Lcom/google/android/repolib/repositories/Repository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->lowPassUpdateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p0}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/accounts/SignInManager;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 71
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->lowPassUpdateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/Database;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 72
    invoke-virtual {p0}, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->update()V

    .line 73
    return-void
.end method

.method public indexer()Lcom/google/android/repolib/common/Indexer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/repolib/common/Indexer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 109
    .local p0, "this":Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;, "Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->repository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0}, Lcom/google/android/repolib/repositories/Repository;->indexer()Lcom/google/android/repolib/common/Indexer;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 103
    .local p0, "this":Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;, "Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->repository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0}, Lcom/google/android/repolib/repositories/Repository;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public lastUpdatableRemoved()V
    .locals 2

    .prologue
    .line 77
    .local p0, "this":Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;, "Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->lowPassUpdateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/Database;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/accounts/SignInManager;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->lowPassUpdateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p0}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->repository:Lcom/google/android/repolib/repositories/Repository;

    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, v1}, Lcom/google/android/repolib/repositories/Repository;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 81
    return-void
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 90
    .local p0, "this":Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;, "Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 91
    return-void
.end method

.method public update()V
    .locals 1

    .prologue
    .line 95
    .local p0, "this":Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;, "Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v0}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/PurchaseRequestRepository;->updateDatabaseDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0}, Lcom/google/android/repolib/observers/UpdateDispatcher;->update()V

    .line 99
    :cond_0
    return-void
.end method
