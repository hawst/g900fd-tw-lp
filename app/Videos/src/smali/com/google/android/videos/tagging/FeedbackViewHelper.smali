.class Lcom/google/android/videos/tagging/FeedbackViewHelper;
.super Ljava/lang/Object;
.source "FeedbackViewHelper.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/FeedbackViewHelper$Listener;
    }
.end annotation


# instance fields
.field private final cardsView:Lcom/google/android/videos/tagging/CardsView;

.field private currentIndex:I

.field private final feedbackBox:Landroid/view/View;

.field private final feedbackView:Landroid/view/View;

.field private final hideTagsWhenObscured:Z

.field private final incorrectlyTaggedKnowledgeEntities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field private final listener:Lcom/google/android/videos/tagging/FeedbackViewHelper$Listener;

.field private final noButton:Landroid/view/View;

.field private final questionView:Landroid/widget/TextView;

.field private taggedKnowledgeEntities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field private final tagsView:Lcom/google/android/videos/tagging/TagsView;

.field private final yesButton:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/google/android/videos/tagging/TagsView;Lcom/google/android/videos/tagging/CardsView;ZLcom/google/android/videos/tagging/FeedbackViewHelper$Listener;)V
    .locals 1
    .param p1, "feedbackView"    # Landroid/view/View;
    .param p2, "tagsView"    # Lcom/google/android/videos/tagging/TagsView;
    .param p3, "cardsView"    # Lcom/google/android/videos/tagging/CardsView;
    .param p4, "hideTagsWhenObscured"    # Z
    .param p5, "listener"    # Lcom/google/android/videos/tagging/FeedbackViewHelper$Listener;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    .line 48
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 49
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/TagsView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    .line 51
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/CardsView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    .line 52
    iput-boolean p4, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->hideTagsWhenObscured:Z

    .line 53
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/FeedbackViewHelper$Listener;

    iput-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->listener:Lcom/google/android/videos/tagging/FeedbackViewHelper$Listener;

    .line 55
    const v0, 0x7f0f0118

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackBox:Landroid/view/View;

    .line 56
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackBox:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackBox:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackBox:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    :cond_0
    const v0, 0x7f0f0115

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->questionView:Landroid/widget/TextView;

    .line 61
    const v0, 0x7f0f0116

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->yesButton:Landroid/view/View;

    .line 62
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->yesButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 63
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->yesButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    const v0, 0x7f0f0117

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->noButton:Landroid/view/View;

    .line 65
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->noButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->noButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    .line 68
    return-void
.end method

.method private askAboutCurrentTaggedKnowledgeEntity()V
    .locals 7

    .prologue
    .line 102
    iget-object v1, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    iget v2, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->currentIndex:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;

    .line 103
    .local v0, "taggedKnowledgeEntity":Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;
    iget-object v1, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->questionView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->questionView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01e6

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v0, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    iget-object v6, v6, Lcom/google/android/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v1, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/tagging/TagsView;->setSpotlight(Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->reset()V

    .line 109
    :cond_0
    return-void
.end method

.method private varargs startAlphaAnimation(Landroid/view/View;[F)Landroid/animation/ObjectAnimator;
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "alphaValues"    # [F

    .prologue
    .line 75
    const-string v1, "alpha"

    invoke-static {p1, v1, p2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v2, 0x96

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 77
    .local v0, "animator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 78
    return-object v0
.end method


# virtual methods
.method public collectFeedback(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "taggedKnowledgeEntities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;>;"
    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 86
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->reset()V

    .line 87
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    iput-object p1, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    .line 91
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 92
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackBox:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackBox:Landroid/view/View;

    :goto_1
    new-array v1, v2, [F

    fill-array-data v1, :array_0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->startAlphaAnimation(Landroid/view/View;[F)Landroid/animation/ObjectAnimator;

    .line 93
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    new-array v1, v2, [F

    fill-array-data v1, :array_1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->startAlphaAnimation(Landroid/view/View;[F)Landroid/animation/ObjectAnimator;

    .line 94
    iget-boolean v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->hideTagsWhenObscured:Z

    if-eqz v0, :cond_2

    .line 95
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    new-array v1, v2, [F

    fill-array-data v1, :array_2

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->startAlphaAnimation(Landroid/view/View;[F)Landroid/animation/ObjectAnimator;

    .line 97
    :cond_2
    iput v3, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->currentIndex:I

    .line 98
    invoke-direct {p0}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->askAboutCurrentTaggedKnowledgeEntity()V

    goto :goto_0

    .line 92
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    goto :goto_1

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 93
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 95
    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public isCollectingFeedback()Z
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 159
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 164
    iget-boolean v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->hideTagsWhenObscured:Z

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/TagsView;->clearSpotlight()V

    .line 167
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 172
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 177
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->isCollectingFeedback()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackBox:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    if-ne p1, v0, :cond_2

    .line 117
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->reset()V

    goto :goto_0

    .line 120
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->noButton:Landroid/view/View;

    if-ne p1, v0, :cond_5

    .line 121
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    iget v2, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->currentIndex:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    :cond_3
    iget v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->currentIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->currentIndex:I

    iget-object v1, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_6

    .line 126
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 127
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->listener:Lcom/google/android/videos/tagging/FeedbackViewHelper$Listener;

    iget-object v1, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    invoke-static {v1}, Lcom/google/android/videos/utils/CollectionUtil;->immutableCopyOf(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/videos/tagging/FeedbackViewHelper$Listener;->onFeedbackCollected(Ljava/util/List;)V

    .line 130
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->reset()V

    goto :goto_0

    .line 122
    :cond_5
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->yesButton:Landroid/view/View;

    if-eq p1, v0, :cond_3

    goto :goto_0

    .line 132
    :cond_6
    invoke-direct {p0}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->askAboutCurrentTaggedKnowledgeEntity()V

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 182
    const/4 v0, 0x0

    return v0
.end method

.method public reset()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 141
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->isCollectingFeedback()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->taggedKnowledgeEntities:Ljava/util/List;

    .line 143
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->currentIndex:I

    .line 144
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->incorrectlyTaggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 145
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    new-array v1, v2, [F

    fill-array-data v1, :array_0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->startAlphaAnimation(Landroid/view/View;[F)Landroid/animation/ObjectAnimator;

    .line 146
    iget-boolean v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->hideTagsWhenObscured:Z

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    new-array v1, v2, [F

    fill-array-data v1, :array_1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->startAlphaAnimation(Landroid/view/View;[F)Landroid/animation/ObjectAnimator;

    .line 152
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackBox:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackBox:Landroid/view/View;

    :goto_1
    new-array v1, v2, [F

    fill-array-data v1, :array_2

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->startAlphaAnimation(Landroid/view/View;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 154
    :cond_0
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/TagsView;->clearSpotlight()V

    goto :goto_0

    .line 152
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    goto :goto_1

    .line 145
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 147
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 152
    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public setPadding(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/videos/tagging/FeedbackViewHelper;->feedbackView:Landroid/view/View;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/view/View;->setPadding(IIII)V

    .line 72
    return-void
.end method
