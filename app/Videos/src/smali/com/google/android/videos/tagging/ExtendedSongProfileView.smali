.class public Lcom/google/android/videos/tagging/ExtendedSongProfileView;
.super Landroid/widget/FrameLayout;
.source "ExtendedSongProfileView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/videos/store/StoreStatusMonitor$Listener;


# instance fields
.field private account:Ljava/lang/String;

.field private activity:Landroid/app/Activity;

.field private albumId:Ljava/lang/String;

.field private bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private cardFrame:Landroid/view/View;

.field private closeButton:Landroid/widget/ImageButton;

.field private entitiesViewGroup:Landroid/view/ViewGroup;

.field private isHiding:Z

.field private listener:Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;

.field private performerView:Landroid/widget/TextView;

.field private requester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private song:Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

.field private storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

.field private final thumbnailImageDimension:I

.field private thumbnailView:Landroid/widget/ImageView;

.field private titleView:Landroid/widget/TextView;

.field private transitionProfileImageFrame:Landroid/view/ViewGroup;

.field private transitionProfileImageView:Landroid/view/View;

.field private viewRoot:Landroid/view/ViewGroup;

.field private wishlistView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 88
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 89
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->setClickable(Z)V

    .line 90
    invoke-direct {p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->inflate()V

    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e01fa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->thumbnailImageDimension:I

    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/tagging/ExtendedSongProfileView;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageFrame:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/videos/tagging/ExtendedSongProfileView;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedSongProfileView;
    .param p1, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageFrame:Landroid/view/ViewGroup;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/videos/tagging/ExtendedSongProfileView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedSongProfileView;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->startHideTransitionStepTwo(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/videos/tagging/ExtendedSongProfileView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/videos/tagging/ExtendedSongProfileView;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedSongProfileView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/videos/tagging/ExtendedSongProfileView;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->viewRoot:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/videos/tagging/ExtendedSongProfileView;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedSongProfileView;
    .param p1, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->viewRoot:Landroid/view/ViewGroup;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/videos/tagging/ExtendedSongProfileView;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/videos/tagging/ExtendedSongProfileView;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedSongProfileView;
    .param p1, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/videos/tagging/ExtendedSongProfileView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->hideInternal()V

    return-void
.end method

.method private createFadeCardElementsTransition(I)Landroid/transition/Transition;
    .locals 2
    .param p1, "fadingMode"    # I

    .prologue
    .line 354
    new-instance v0, Landroid/transition/Fade;

    invoke-direct {v0, p1}, Landroid/transition/Fade;-><init>(I)V

    .line 355
    .local v0, "transition":Landroid/transition/Transition;
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->wishlistView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 356
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 357
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->performerView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 358
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->closeButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 359
    return-object v0
.end method

.method private createProfileImageTransition()Landroid/transition/Transition;
    .locals 6

    .prologue
    .line 364
    new-instance v0, Landroid/view/animation/PathInterpolator;

    const v2, 0x3ecccccd    # 0.4f

    const/4 v3, 0x0

    const v4, 0x3e4ccccd    # 0.2f

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/view/animation/PathInterpolator;-><init>(FFFF)V

    .line 365
    .local v0, "interpolator":Landroid/view/animation/Interpolator;
    new-instance v1, Landroid/transition/TransitionSet;

    invoke-direct {v1}, Landroid/transition/TransitionSet;-><init>()V

    .line 366
    .local v1, "transition":Landroid/transition/TransitionSet;
    new-instance v2, Landroid/transition/ChangeBounds;

    invoke-direct {v2}, Landroid/transition/ChangeBounds;-><init>()V

    invoke-virtual {v2, v0}, Landroid/transition/ChangeBounds;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 367
    new-instance v2, Landroid/transition/ChangeTransform;

    invoke-direct {v2}, Landroid/transition/ChangeTransform;-><init>()V

    invoke-virtual {v2, v0}, Landroid/transition/ChangeTransform;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 368
    new-instance v2, Landroid/transition/ChangeImageTransform;

    invoke-direct {v2}, Landroid/transition/ChangeImageTransform;-><init>()V

    invoke-virtual {v2, v0}, Landroid/transition/ChangeImageTransform;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 369
    const-string v2, "song_image_transition"

    invoke-virtual {v1, v2}, Landroid/transition/TransitionSet;->addTarget(Ljava/lang/String;)Landroid/transition/TransitionSet;

    .line 370
    const-string v2, "song_card_transition"

    invoke-virtual {v1, v2}, Landroid/transition/TransitionSet;->addTarget(Ljava/lang/String;)Landroid/transition/TransitionSet;

    .line 371
    return-object v1
.end method

.method private getAlbumId(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "storeUrl"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 258
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v0, v4

    .line 269
    :cond_0
    :goto_0
    return-object v0

    .line 261
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 262
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 263
    .local v1, "path":Ljava/lang/String;
    const-string v5, "id"

    invoke-virtual {v3, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, "id":Ljava/lang/String;
    const-string v5, "tid"

    invoke-virtual {v3, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 265
    .local v2, "tid":Ljava/lang/String;
    const-string v5, "/store/music/album"

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "song-"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    :cond_2
    move-object v0, v4

    .line 269
    goto :goto_0
.end method

.method private hideInternal()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 375
    iput-boolean v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->isHiding:Z

    .line 376
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    invoke-static {v0}, Lcom/google/android/videos/ui/BitmapLoader;->cancel(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;)V

    .line 379
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    if-eqz v0, :cond_1

    .line 380
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->removeListener(Lcom/google/android/videos/store/StoreStatusMonitor$Listener;)V

    .line 382
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->listener:Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;

    if-eqz v0, :cond_2

    .line 383
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->listener:Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;

    invoke-interface {v0, p0, v1}, Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;->onExtendedProfileVisibilityChanged(Landroid/view/View;Z)V

    .line 385
    :cond_2
    return-void
.end method

.method private inflate()V
    .locals 3

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->removeAllViews()V

    .line 104
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04003b

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 105
    const v0, 0x7f0a00e8

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->setBackgroundResource(I)V

    .line 106
    const v0, 0x7f0f0113

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->cardFrame:Landroid/view/View;

    .line 107
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->cardFrame:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    const v0, 0x7f0f0108

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->closeButton:Landroid/widget/ImageButton;

    .line 109
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->closeButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/videos/tagging/ExtendedSongProfileView$1;

    invoke-direct {v1, p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView$1;-><init>(Lcom/google/android/videos/tagging/ExtendedSongProfileView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    const v0, 0x7f0f00c9

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->thumbnailView:Landroid/widget/ImageView;

    .line 116
    const v0, 0x7f0f00ca

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->wishlistView:Landroid/view/View;

    .line 117
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->wishlistView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    const v0, 0x7f0f00a8

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->titleView:Landroid/widget/TextView;

    .line 119
    const v0, 0x7f0f0114

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->performerView:Landroid/widget/TextView;

    .line 120
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->cardFrame:Landroid/view/View;

    const-string v1, "song_card_transition"

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setTransitionName(Landroid/view/View;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->thumbnailView:Landroid/widget/ImageView;

    const-string v1, "song_image_transition"

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setTransitionName(Landroid/view/View;Ljava/lang/String;)V

    .line 122
    return-void
.end method

.method private showInternal(Lcom/google/android/videos/tagging/KnowledgeEntity$Song;Lcom/google/android/videos/async/Requester;)V
    .locals 3
    .param p1, "song"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Song;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Song;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "requester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    const/4 v1, 0x0

    .line 190
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    invoke-static {v0}, Lcom/google/android/videos/ui/BitmapLoader;->cancel(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;)V

    .line 193
    :cond_0
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->song:Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    .line 194
    iput-object p2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->requester:Lcom/google/android/videos/async/Requester;

    .line 195
    invoke-virtual {p0, v1}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->setVisibility(I)V

    .line 196
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->wishlistView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 197
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->performerView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 199
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->closeButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 200
    iget-object v0, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    if-nez v0, :cond_2

    .line 201
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->thumbnailView:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/google/android/videos/tagging/Cards;->setDefaultSongImage(Landroid/widget/ImageView;)V

    .line 207
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->titleView:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->performerView:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->performer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v0, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->googlePlayUrl:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->getAlbumId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->albumId:Ljava/lang/String;

    .line 210
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->addListener(Lcom/google/android/videos/store/StoreStatusMonitor$Listener;)V

    .line 211
    invoke-direct {p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->updateWishlistView()V

    .line 212
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->listener:Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;

    if-eqz v0, :cond_1

    .line 213
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->listener:Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;

    const/4 v1, 0x1

    invoke-interface {v0, p0, v1}, Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;->onExtendedProfileVisibilityChanged(Landroid/view/View;Z)V

    .line 215
    :cond_1
    return-void

    .line 203
    :cond_2
    new-instance v0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$SongBitmapView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->thumbnailView:Landroid/widget/ImageView;

    iget v2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->thumbnailImageDimension:I

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$SongBitmapView;-><init>(Lcom/google/android/videos/tagging/KnowledgeEntity$Song;Landroid/widget/ImageView;I)V

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    .line 205
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    iget-object v1, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    invoke-static {v0, p2, v1}, Lcom/google/android/videos/ui/BitmapLoader;->setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private startHideTransitionStepOne(Z)V
    .locals 5
    .param p1, "zeroDurationTransition"    # Z

    .prologue
    const/4 v4, 0x4

    .line 296
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageFrame:Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setHasTransientState(Z)V

    .line 297
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->createFadeCardElementsTransition(I)Landroid/transition/Transition;

    move-result-object v0

    .line 298
    .local v0, "fade":Landroid/transition/Transition;
    new-instance v1, Lcom/google/android/videos/tagging/ExtendedSongProfileView$4;

    invoke-direct {v1, p0, p1}, Lcom/google/android/videos/tagging/ExtendedSongProfileView$4;-><init>(Lcom/google/android/videos/tagging/ExtendedSongProfileView;Z)V

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 304
    if-eqz p1, :cond_0

    .line 305
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/transition/Transition;->setDuration(J)Landroid/transition/Transition;

    .line 307
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->viewRoot:Landroid/view/ViewGroup;

    invoke-static {v1, v0}, Landroid/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;Landroid/transition/Transition;)V

    .line 308
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->wishlistView:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 309
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 310
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->performerView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 311
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->closeButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 312
    return-void
.end method

.method private startHideTransitionStepTwo(Z)V
    .locals 4
    .param p1, "zeroDurationTransition"    # Z

    .prologue
    .line 316
    new-instance v0, Landroid/transition/TransitionSet;

    invoke-direct {v0}, Landroid/transition/TransitionSet;-><init>()V

    .line 317
    .local v0, "hideTransitionStepTwo":Landroid/transition/TransitionSet;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->setOrdering(I)Landroid/transition/TransitionSet;

    .line 318
    const-string v1, "song_image_transition"

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 319
    invoke-direct {p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->createProfileImageTransition()Landroid/transition/Transition;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 325
    :goto_0
    new-instance v1, Landroid/transition/Fade;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/transition/Fade;-><init>(I)V

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 326
    new-instance v1, Lcom/google/android/videos/tagging/ExtendedSongProfileView$5;

    invoke-direct {v1, p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView$5;-><init>(Lcom/google/android/videos/tagging/ExtendedSongProfileView;)V

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/TransitionSet;

    .line 340
    if-eqz p1, :cond_0

    .line 341
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    .line 343
    :cond_0
    new-instance v1, Lcom/google/android/videos/tagging/ExtendedSongProfileView$6;

    invoke-direct {v1, p0, v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView$6;-><init>(Lcom/google/android/videos/tagging/ExtendedSongProfileView;Landroid/transition/TransitionSet;)V

    invoke-virtual {p0, v1}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->post(Ljava/lang/Runnable;)Z

    .line 350
    return-void

    .line 322
    :cond_1
    new-instance v1, Landroid/transition/Fade;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/transition/Fade;-><init>(I)V

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->cardFrame:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    goto :goto_0
.end method

.method private startShowTransition(Landroid/widget/FrameLayout;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 6
    .param p1, "transitionRoot"    # Landroid/widget/FrameLayout;
    .param p2, "transitionViewGroup"    # Landroid/view/ViewGroup;
    .param p3, "frame"    # Landroid/view/ViewGroup;
    .param p4, "image"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    .line 220
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->viewRoot:Landroid/view/ViewGroup;

    .line 221
    iput-object p2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;

    .line 222
    iput-object p4, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageView:Landroid/view/View;

    .line 223
    iget-object v3, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageView:Landroid/view/View;

    const-string v4, "song_image_transition"

    invoke-virtual {v3, v4}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 224
    iput-object p3, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageFrame:Landroid/view/ViewGroup;

    .line 225
    iget-object v3, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageFrame:Landroid/view/ViewGroup;

    const v4, 0x7f0a0039

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 226
    iget-object v3, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageFrame:Landroid/view/ViewGroup;

    const-string v4, "song_card_transition"

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setTransitionName(Ljava/lang/String;)V

    .line 227
    iget-object v3, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageFrame:Landroid/view/ViewGroup;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setHasTransientState(Z)V

    .line 228
    new-instance v2, Landroid/transition/TransitionSet;

    invoke-direct {v2}, Landroid/transition/TransitionSet;-><init>()V

    .line 229
    .local v2, "showTransition":Landroid/transition/TransitionSet;
    invoke-virtual {v2, v5}, Landroid/transition/TransitionSet;->setOrdering(I)Landroid/transition/TransitionSet;

    .line 231
    new-instance v1, Landroid/transition/TransitionSet;

    invoke-direct {v1}, Landroid/transition/TransitionSet;-><init>()V

    .line 232
    .local v1, "fadeMoveTransition":Landroid/transition/TransitionSet;
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/transition/TransitionSet;->setOrdering(I)Landroid/transition/TransitionSet;

    .line 234
    new-instance v0, Landroid/transition/Fade;

    const/4 v3, 0x2

    invoke-direct {v0, v3}, Landroid/transition/Fade;-><init>(I)V

    .line 235
    .local v0, "fadeEntities":Landroid/transition/Fade;
    invoke-virtual {v0, p2}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 236
    invoke-virtual {v1, v0}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 238
    invoke-direct {p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->createProfileImageTransition()Landroid/transition/Transition;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 239
    invoke-virtual {v2, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 241
    invoke-direct {p0, v5}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->createFadeCardElementsTransition(I)Landroid/transition/Transition;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 242
    new-instance v3, Lcom/google/android/videos/tagging/ExtendedSongProfileView$2;

    invoke-direct {v3, p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView$2;-><init>(Lcom/google/android/videos/tagging/ExtendedSongProfileView;)V

    invoke-virtual {v2, v3}, Landroid/transition/TransitionSet;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/TransitionSet;

    .line 248
    new-instance v3, Lcom/google/android/videos/tagging/ExtendedSongProfileView$3;

    invoke-direct {v3, p0, p1, v2}, Lcom/google/android/videos/tagging/ExtendedSongProfileView$3;-><init>(Lcom/google/android/videos/tagging/ExtendedSongProfileView;Landroid/widget/FrameLayout;Landroid/transition/TransitionSet;)V

    invoke-virtual {p0, v3}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->post(Ljava/lang/Runnable;)Z

    .line 255
    return-void
.end method

.method private updateWishlistView()V
    .locals 7

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 131
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->albumId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->wishlistView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 153
    :goto_0
    return-void

    .line 135
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->wishlistView:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 136
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->albumId:Ljava/lang/String;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/store/StoreStatusMonitor;->getStatus(Ljava/lang/String;I)I

    move-result v0

    .line 137
    .local v0, "status":I
    invoke-static {v0}, Lcom/google/android/videos/store/StoreStatusMonitor;->isPurchased(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 139
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->wishlistView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 142
    :cond_1
    invoke-static {v0}, Lcom/google/android/videos/store/StoreStatusMonitor;->isWishlisted(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 143
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->wishlistView:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setSelected(Z)V

    .line 144
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->wishlistView:Landroid/view/View;

    const v2, 0x7f0b011b

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->updateWishlistViewText(Landroid/view/View;I)V

    .line 145
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->wishlistView:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->wishlistView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01b3

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->song:Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    iget-object v5, v5, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->name:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 148
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->wishlistView:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setSelected(Z)V

    .line 149
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->wishlistView:Landroid/view/View;

    const v2, 0x7f0b011a

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->updateWishlistViewText(Landroid/view/View;I)V

    .line 150
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->wishlistView:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->wishlistView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b01b2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->song:Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    iget-object v5, v5, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->name:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateWishlistViewText(Landroid/view/View;I)V
    .locals 2
    .param p1, "wishlistView"    # Landroid/view/View;
    .param p2, "stringResource"    # I

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 157
    check-cast p1, Landroid/widget/Button;

    .end local p1    # "wishlistView":Landroid/view/View;
    invoke-virtual {p1, p2}, Landroid/widget/Button;->setText(I)V

    .line 159
    :cond_0
    return-void
.end method


# virtual methods
.method public hide(Z)V
    .locals 4
    .param p1, "withTransition"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 273
    iget-boolean v2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->isHiding:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->isVisible()Z

    move-result v2

    if-nez v2, :cond_1

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 276
    :cond_1
    iput-boolean v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->isHiding:Z

    .line 277
    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;

    if-eqz v2, :cond_2

    .line 278
    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 280
    :cond_2
    sget v2, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->transitionProfileImageView:Landroid/view/View;

    if-nez v2, :cond_4

    .line 281
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->viewRoot:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 282
    invoke-direct {p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->hideInternal()V

    goto :goto_0

    .line 287
    :cond_4
    if-nez p1, :cond_5

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->startHideTransitionStepOne(Z)V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x3

    .line 389
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0f00ca

    if-ne v0, v1, :cond_2

    .line 390
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->albumId:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/videos/store/StoreStatusMonitor;->getStatus(Ljava/lang/String;I)I

    move-result v7

    .line 391
    .local v7, "status":I
    invoke-static {v7}, Lcom/google/android/videos/store/StoreStatusMonitor;->isPurchased(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406
    .end local v7    # "status":I
    :goto_0
    return-void

    .line 395
    .restart local v7    # "status":I
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->albumId:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/videos/store/StoreStatusMonitor;->isWishlisted(I)Z

    move-result v4

    if-nez v4, :cond_1

    const/4 v4, 0x1

    :goto_1
    move-object v6, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/store/WishlistService;->requestSetWishlisted(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZILandroid/view/View;)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    .line 397
    .end local v7    # "status":I
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->song:Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    iget-object v0, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->googlePlayUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 399
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->song:Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    iget-object v1, v1, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/videos/tagging/Cards;->toQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "music"

    iget-object v3, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->account:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v5}, Lcom/google/android/videos/utils/PlayStoreUtil;->startSearch(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 403
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->song:Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    iget-object v1, v1, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;->googlePlayUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->account:Ljava/lang/String;

    invoke-static {v0, v1, v2, v5}, Lcom/google/android/videos/utils/PlayStoreUtil;->startForUri(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public onInit(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/videos/store/StoreStatusMonitor;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "storeStatusMonitor"    # Lcom/google/android/videos/store/StoreStatusMonitor;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->activity:Landroid/app/Activity;

    .line 163
    iput-object p2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->account:Ljava/lang/String;

    .line 164
    iput-object p3, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    .line 165
    return-void
.end method

.method public onOrientationChange()V
    .locals 2

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->inflate()V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->song:Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->requester:Lcom/google/android/videos/async/Requester;

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->showInternal(Lcom/google/android/videos/tagging/KnowledgeEntity$Song;Lcom/google/android/videos/async/Requester;)V

    .line 100
    :cond_0
    return-void
.end method

.method public onStoreStatusChanged(Lcom/google/android/videos/store/StoreStatusMonitor;)V
    .locals 0
    .param p1, "sender"    # Lcom/google/android/videos/store/StoreStatusMonitor;

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->updateWishlistView()V

    .line 127
    return-void
.end method

.method public setListener(Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->listener:Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;

    .line 169
    return-void
.end method

.method public show(Landroid/widget/FrameLayout;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/View;Lcom/google/android/videos/tagging/KnowledgeEntity$Song;Lcom/google/android/videos/async/Requester;)V
    .locals 2
    .param p1, "viewRoot"    # Landroid/widget/FrameLayout;
    .param p2, "entitiesViewGroup"    # Landroid/view/ViewGroup;
    .param p3, "frame"    # Landroid/view/ViewGroup;
    .param p4, "image"    # Landroid/view/View;
    .param p5, "song"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Song;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/FrameLayout;",
            "Landroid/view/ViewGroup;",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Song;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 177
    .local p6, "requester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    invoke-direct {p0, p5, p6}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->showInternal(Lcom/google/android/videos/tagging/KnowledgeEntity$Song;Lcom/google/android/videos/async/Requester;)V

    .line 178
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 179
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->viewRoot:Landroid/view/ViewGroup;

    .line 180
    iput-object p2, p0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;

    .line 181
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 182
    invoke-virtual {p1, p0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 186
    :goto_0
    return-void

    .line 185
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->startShowTransition(Landroid/widget/FrameLayout;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/View;)V

    goto :goto_0
.end method
