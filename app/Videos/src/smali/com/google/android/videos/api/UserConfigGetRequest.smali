.class public Lcom/google/android/videos/api/UserConfigGetRequest;
.super Lcom/google/android/videos/async/Request;
.source "UserConfigGetRequest.java"


# instance fields
.field public final countryOverride:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "countryOverride"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;)V

    .line 18
    iput-object p2, p0, Lcom/google/android/videos/api/UserConfigGetRequest;->countryOverride:Ljava/lang/String;

    .line 19
    return-void
.end method
