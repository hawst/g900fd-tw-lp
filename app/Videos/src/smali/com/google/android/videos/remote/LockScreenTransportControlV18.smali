.class Lcom/google/android/videos/remote/LockScreenTransportControlV18;
.super Lcom/google/android/videos/remote/LockScreenTransportControlV16;
.source "LockScreenTransportControlV18.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/remote/LockScreenTransportControlV18$1;,
        Lcom/google/android/videos/remote/LockScreenTransportControlV18$PlaybackPositionUpdateListener;,
        Lcom/google/android/videos/remote/LockScreenTransportControlV18$GetPlaybackPositionUpdateListener;
    }
.end annotation


# instance fields
.field private final getPlaybackPositionUpdateListener:Lcom/google/android/videos/remote/LockScreenTransportControlV18$GetPlaybackPositionUpdateListener;

.field private final playbackPositionUpdateListener:Lcom/google/android/videos/remote/LockScreenTransportControlV18$PlaybackPositionUpdateListener;


# direct methods
.method constructor <init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)V
    .locals 2
    .param p1, "remoteTracker"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/remote/LockScreenTransportControlV16;-><init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)V

    .line 23
    new-instance v0, Lcom/google/android/videos/remote/LockScreenTransportControlV18$PlaybackPositionUpdateListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/remote/LockScreenTransportControlV18$PlaybackPositionUpdateListener;-><init>(Lcom/google/android/videos/remote/LockScreenTransportControlV18;Lcom/google/android/videos/remote/LockScreenTransportControlV18$1;)V

    iput-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV18;->playbackPositionUpdateListener:Lcom/google/android/videos/remote/LockScreenTransportControlV18$PlaybackPositionUpdateListener;

    .line 24
    new-instance v0, Lcom/google/android/videos/remote/LockScreenTransportControlV18$GetPlaybackPositionUpdateListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/remote/LockScreenTransportControlV18$GetPlaybackPositionUpdateListener;-><init>(Lcom/google/android/videos/remote/LockScreenTransportControlV18;Lcom/google/android/videos/remote/LockScreenTransportControlV18$1;)V

    iput-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV18;->getPlaybackPositionUpdateListener:Lcom/google/android/videos/remote/LockScreenTransportControlV18$GetPlaybackPositionUpdateListener;

    .line 25
    return-void
.end method


# virtual methods
.method protected getTransportControlFlags()I
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV16;->getTransportControlFlags()I

    move-result v0

    or-int/lit16 v0, v0, 0x100

    return v0
.end method

.method protected register()V
    .locals 2

    .prologue
    .line 29
    invoke-super {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV16;->register()V

    .line 30
    invoke-virtual {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV18;->getRemoteControlClient()Landroid/media/RemoteControlClient;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV18;->playbackPositionUpdateListener:Lcom/google/android/videos/remote/LockScreenTransportControlV18$PlaybackPositionUpdateListener;

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setPlaybackPositionUpdateListener(Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;)V

    .line 31
    invoke-virtual {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV18;->getRemoteControlClient()Landroid/media/RemoteControlClient;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV18;->getPlaybackPositionUpdateListener:Lcom/google/android/videos/remote/LockScreenTransportControlV18$GetPlaybackPositionUpdateListener;

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setOnGetPlaybackPositionListener(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V

    .line 32
    return-void
.end method

.method protected unregister()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-virtual {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV18;->getRemoteControlClient()Landroid/media/RemoteControlClient;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setOnGetPlaybackPositionListener(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV18;->getRemoteControlClient()Landroid/media/RemoteControlClient;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setPlaybackPositionUpdateListener(Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;)V

    .line 38
    invoke-super {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV16;->unregister()V

    .line 39
    return-void
.end method

.method protected updatePlaybackState(I)V
    .locals 5
    .param p1, "state"    # I

    .prologue
    .line 43
    iget-object v1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV18;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v1}, Lcom/google/android/videos/remote/RemoteTracker;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v0

    .line 44
    .local v0, "playerState":Lcom/google/android/videos/remote/PlayerState;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV18;->getRemoteControlClient()Landroid/media/RemoteControlClient;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 45
    invoke-virtual {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV18;->getRemoteControlClient()Landroid/media/RemoteControlClient;

    move-result-object v1

    iget v2, v0, Lcom/google/android/videos/remote/PlayerState;->time:I

    int-to-long v2, v2

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v1, p1, v2, v3, v4}, Landroid/media/RemoteControlClient;->setPlaybackState(IJF)V

    .line 49
    :goto_0
    return-void

    .line 47
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/videos/remote/LockScreenTransportControlV16;->updatePlaybackState(I)V

    goto :goto_0
.end method
