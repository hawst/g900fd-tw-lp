.class Lcom/google/android/videos/cast/v2/CastV2RemoteControl$8;
.super Ljava/lang/Object;
.source "CastV2RemoteControl.java"

# interfaces
.implements Lcom/google/android/gms/common/api/ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->checkCommandStatus(Ljava/lang/String;Lcom/google/android/gms/common/api/PendingResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/ResultCallback",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

.field final synthetic val$command:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 840
    iput-object p1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$8;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    iput-object p2, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$8;->val$command:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Lcom/google/android/gms/common/api/Result;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 843
    .local p1, "result":Lcom/google/android/gms/common/api/Result;, "TT;"
    invoke-interface {p1}, Lcom/google/android/gms/common/api/Result;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v0

    if-nez v0, :cond_0

    .line 844
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$8;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$8;->val$command:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/common/api/Result;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->onSendCommandFailure(Ljava/lang/String;Lcom/google/android/gms/common/api/Status;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$1400(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/lang/String;Lcom/google/android/gms/common/api/Status;)V

    .line 846
    :cond_0
    return-void
.end method
