.class public Lcom/google/android/videos/subtitles/CaptionStyleUtil;
.super Ljava/lang/Object;
.source "CaptionStyleUtil.java"


# static fields
.field private static final PRESETS:[Lcom/google/android/exoplayer/text/CaptionStyleCompat;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .prologue
    const/4 v1, -0x1

    const/16 v15, -0x100

    const/4 v6, 0x0

    const/high16 v2, -0x1000000

    const/4 v3, 0x0

    .line 19
    const/4 v0, 0x4

    new-array v14, v0, [Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    new-instance v0, Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    move v4, v3

    move v5, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/text/CaptionStyleCompat;-><init>(IIIIILandroid/graphics/Typeface;)V

    aput-object v0, v14, v3

    const/4 v0, 0x1

    new-instance v7, Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    move v8, v2

    move v9, v1

    move v10, v3

    move v11, v3

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/google/android/exoplayer/text/CaptionStyleCompat;-><init>(IIIIILandroid/graphics/Typeface;)V

    aput-object v7, v14, v0

    const/4 v7, 0x2

    new-instance v0, Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    move v1, v15

    move v4, v3

    move v5, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/text/CaptionStyleCompat;-><init>(IIIIILandroid/graphics/Typeface;)V

    aput-object v0, v14, v7

    const/4 v0, 0x3

    new-instance v7, Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    const v9, -0xffff01

    move v8, v15

    move v10, v3

    move v11, v3

    move v12, v2

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/google/android/exoplayer/text/CaptionStyleCompat;-><init>(IIIIILandroid/graphics/Typeface;)V

    aput-object v7, v14, v0

    sput-object v14, Lcom/google/android/videos/subtitles/CaptionStyleUtil;->PRESETS:[Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    return-void
.end method

.method public static createCaptionStyleFromPreferences(ILandroid/content/SharedPreferences;Landroid/content/res/AssetManager;)Lcom/google/android/exoplayer/text/CaptionStyleCompat;
    .locals 11
    .param p0, "preset"    # I
    .param p1, "preferences"    # Landroid/content/SharedPreferences;
    .param p2, "assetManager"    # Landroid/content/res/AssetManager;

    .prologue
    .line 42
    const/4 v0, -0x1

    if-eq p0, v0, :cond_0

    .line 43
    sget-object v0, Lcom/google/android/videos/subtitles/CaptionStyleUtil;->PRESETS:[Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    aget-object v0, v0, p0

    .line 60
    :goto_0
    return-object v0

    .line 46
    :cond_0
    sget-object v7, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->DEFAULT:Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    .line 47
    .local v7, "defStyle":Lcom/google/android/exoplayer/text/CaptionStyleCompat;
    const-string v0, "captioning_edge_type"

    iget v9, v7, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->edgeType:I

    invoke-interface {p1, v0, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 48
    .local v4, "edgeType":I
    const-string v0, "captioning_edge_color"

    iget v9, v7, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->edgeColor:I

    invoke-interface {p1, v0, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 50
    .local v5, "edgeColor":I
    const-string v0, "captioning_background_color"

    const-string v9, "captioning_background_opacity"

    iget v10, v7, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->backgroundColor:I

    invoke-static {p1, v0, v9, v10}, Lcom/google/android/videos/subtitles/CaptionStyleUtil;->getColor(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    .line 52
    .local v2, "backgroundColor":I
    const-string v0, "captioning_window_color"

    const-string v9, "captioning_window_opacity"

    iget v10, v7, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->windowColor:I

    invoke-static {p1, v0, v9, v10}, Lcom/google/android/videos/subtitles/CaptionStyleUtil;->getColor(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    .line 54
    .local v3, "windowColor":I
    const-string v0, "captioning_foreground_color"

    const-string v9, "captioning_foreground_opacity"

    iget v10, v7, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->foregroundColor:I

    invoke-static {p1, v0, v9, v10}, Lcom/google/android/videos/subtitles/CaptionStyleUtil;->getColor(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    .line 57
    .local v1, "foregroundColor":I
    const-string v0, "captioning_typeface"

    const/4 v9, 0x0

    invoke-interface {p1, v0, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 58
    .local v8, "rawTypeface":Ljava/lang/String;
    invoke-static {p2, v8}, Lcom/google/android/videos/subtitles/CaptionStyleUtil;->getTypeface(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v6

    .line 60
    .local v6, "typeface":Landroid/graphics/Typeface;
    new-instance v0, Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/text/CaptionStyleCompat;-><init>(IIIIILandroid/graphics/Typeface;)V

    goto :goto_0
.end method

.method public static createCaptionStyleFromPreferences(Landroid/content/SharedPreferences;Landroid/content/res/AssetManager;)Lcom/google/android/exoplayer/text/CaptionStyleCompat;
    .locals 3
    .param p0, "preferences"    # Landroid/content/SharedPreferences;
    .param p1, "assetManager"    # Landroid/content/res/AssetManager;

    .prologue
    .line 36
    const-string v1, "captioning_preset"

    const/4 v2, -0x1

    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 37
    .local v0, "preset":I
    invoke-static {v0, p0, p1}, Lcom/google/android/videos/subtitles/CaptionStyleUtil;->createCaptionStyleFromPreferences(ILandroid/content/SharedPreferences;Landroid/content/res/AssetManager;)Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    move-result-object v1

    return-object v1
.end method

.method private static getColor(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 3
    .param p0, "preferences"    # Landroid/content/SharedPreferences;
    .param p1, "prefColor"    # Ljava/lang/String;
    .param p2, "prefOpacity"    # Ljava/lang/String;
    .param p3, "defaultColor"    # I

    .prologue
    .line 80
    invoke-interface {p0, p1, p3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 81
    .local v1, "solidColor":I
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 82
    move v1, p3

    .line 84
    :cond_0
    invoke-interface {p0, p2, p3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 85
    .local v0, "opacity":I
    invoke-static {v1, v0}, Lcom/google/android/videos/subtitles/CaptionStyleUtil;->getColorWithOpacity(II)I

    move-result v2

    return v2
.end method

.method private static getColorWithOpacity(II)I
    .locals 2
    .param p0, "solidColor"    # I
    .param p1, "opacity"    # I

    .prologue
    .line 89
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const v0, 0xffffff

    and-int/2addr v0, p0

    const/high16 v1, -0x1000000

    and-int/2addr v1, p1

    or-int/2addr v0, v1

    goto :goto_0
.end method

.method private static getTypeface(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 1
    .param p0, "assetManager"    # Landroid/content/res/AssetManager;
    .param p1, "rawTypeface"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const/4 v0, 0x0

    .line 74
    :goto_0
    return-object v0

    .line 67
    :cond_0
    const-string v0, "casual"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    const-string v0, "fonts/ComingSoon.ttf"

    invoke-static {p0, v0}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0

    .line 69
    :cond_1
    const-string v0, "cursive"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 70
    const-string v0, "fonts/DancingScript-Regular.ttf"

    invoke-static {p0, v0}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0

    .line 71
    :cond_2
    const-string v0, "sans-serif-smallcaps"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 72
    const-string v0, "fonts/CarroisGothicSC-Regular.ttf"

    invoke-static {p0, v0}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0

    .line 74
    :cond_3
    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0
.end method
