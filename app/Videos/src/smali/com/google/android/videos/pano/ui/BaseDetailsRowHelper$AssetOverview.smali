.class public Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;
.super Ljava/lang/Object;
.source "BaseDetailsRowHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AssetOverview"
.end annotation


# instance fields
.field public asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

.field public expirationTimestamp:J

.field public preorder:Z

.field public purchased:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 484
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;->expirationTimestamp:J

    return-void
.end method
