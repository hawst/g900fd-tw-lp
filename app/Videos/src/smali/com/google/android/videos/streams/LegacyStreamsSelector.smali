.class public Lcom/google/android/videos/streams/LegacyStreamsSelector;
.super Ljava/lang/Object;
.source "LegacyStreamsSelector.java"


# instance fields
.field private final config:Lcom/google/android/videos/Config;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/Config;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/utils/NetworkStatus;)V
    .locals 1
    .param p1, "config"    # Lcom/google/android/videos/Config;
    .param p2, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p3, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/Config;

    iput-object v0, p0, Lcom/google/android/videos/streams/LegacyStreamsSelector;->config:Lcom/google/android/videos/Config;

    .line 34
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/NetworkStatus;

    iput-object v0, p0, Lcom/google/android/videos/streams/LegacyStreamsSelector;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 35
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/logging/EventLogger;

    iput-object v0, p0, Lcom/google/android/videos/streams/LegacyStreamsSelector;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 36
    return-void
.end method

.method private buildStreamSelection(Ljava/util/List;Ljava/util/List;)Lcom/google/android/videos/streams/LegacyStreamSelection;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;)",
            "Lcom/google/android/videos/streams/LegacyStreamSelection;"
        }
    .end annotation

    .prologue
    .local p1, "hiStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    .local p2, "loStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 179
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 181
    .local v1, "audioInfos":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/AudioInfo;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v5, v7, :cond_0

    .line 182
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/videos/streams/MediaStream;

    iget-object v7, v7, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v7, v7, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 184
    :cond_0
    const/4 v5, 0x0

    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v7

    if-ge v5, v7, :cond_2

    .line 185
    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/videos/streams/MediaStream;

    iget-object v7, v7, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v0, v7, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 186
    .local v0, "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    invoke-static {v1, v0, v8}, Lcom/google/android/videos/utils/AudioInfoUtil;->containsInfo(Ljava/util/List;Lcom/google/wireless/android/video/magma/proto/AudioInfo;Z)Z

    move-result v7

    if-nez v7, :cond_1

    .line 187
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 192
    .end local v0    # "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    invoke-direct {v2, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 193
    .local v2, "finalHiStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    invoke-direct {v3, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 194
    .local v3, "finalLoStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v5, 0x0

    :goto_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v5, v7, :cond_7

    .line 195
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 196
    .restart local v0    # "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/streams/LegacyStreamsSelector;->findStream(Ljava/util/List;Lcom/google/wireless/android/video/magma/proto/AudioInfo;)Lcom/google/android/videos/streams/MediaStream;

    move-result-object v4

    .line 197
    .local v4, "hiStream":Lcom/google/android/videos/streams/MediaStream;
    invoke-direct {p0, p2, v0}, Lcom/google/android/videos/streams/LegacyStreamsSelector;->findStream(Ljava/util/List;Lcom/google/wireless/android/video/magma/proto/AudioInfo;)Lcom/google/android/videos/streams/MediaStream;

    move-result-object v6

    .line 198
    .local v6, "loStream":Lcom/google/android/videos/streams/MediaStream;
    if-nez v4, :cond_3

    .line 200
    if-eqz v6, :cond_5

    move v7, v8

    :goto_3
    invoke-static {v7}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 201
    move-object v4, v6

    .line 203
    :cond_3
    if-nez v6, :cond_4

    .line 205
    if-eqz v4, :cond_6

    move v7, v8

    :goto_4
    invoke-static {v7}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 206
    move-object v6, v4

    .line 208
    :cond_4
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_5
    move v7, v9

    .line 200
    goto :goto_3

    :cond_6
    move v7, v9

    .line 205
    goto :goto_4

    .line 211
    .end local v0    # "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .end local v4    # "hiStream":Lcom/google/android/videos/streams/MediaStream;
    .end local v6    # "loStream":Lcom/google/android/videos/streams/MediaStream;
    :cond_7
    new-instance v7, Lcom/google/android/videos/streams/LegacyStreamSelection;

    invoke-direct {v7, v2, v3}, Lcom/google/android/videos/streams/LegacyStreamSelection;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v7
.end method

.method private filterStreams(Ljava/util/List;IZZ)Ljava/util/List;
    .locals 5
    .param p2, "drmLevel"    # I
    .param p3, "surroundSound"    # Z
    .param p4, "drmStreams"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;IZZ)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "streams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 99
    .local v0, "filteredStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 100
    .local v2, "streamIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/videos/streams/MediaStream;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 101
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/streams/MediaStream;

    .line 102
    .local v1, "stream":Lcom/google/android/videos/streams/MediaStream;
    invoke-direct {p0, v1, p2, p3, p4}, Lcom/google/android/videos/streams/LegacyStreamsSelector;->shouldRetainStream(Lcom/google/android/videos/streams/MediaStream;IZZ)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/videos/streams/LegacyStreamsSelector;->config:Lcom/google/android/videos/Config;

    iget-object v4, p0, Lcom/google/android/videos/streams/LegacyStreamsSelector;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-static {v3, v4, v1}, Lcom/google/android/videos/utils/Util;->deviceSupportsStreamResolution(Lcom/google/android/videos/Config;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/streams/MediaStream;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 105
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 108
    .end local v1    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :cond_2
    return-object v0
.end method

.method private findStream(Ljava/util/List;Lcom/google/wireless/android/video/magma/proto/AudioInfo;)Lcom/google/android/videos/streams/MediaStream;
    .locals 3
    .param p2, "info"    # Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ")",
            "Lcom/google/android/videos/streams/MediaStream;"
        }
    .end annotation

    .prologue
    .line 215
    .local p1, "streams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 216
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/streams/MediaStream;

    iget-object v1, v1, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v1, v1, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    const/4 v2, 0x1

    invoke-static {v1, p2, v2}, Lcom/google/android/videos/utils/AudioInfoUtil;->areEqual(Lcom/google/wireless/android/video/magma/proto/AudioInfo;Lcom/google/wireless/android/video/magma/proto/AudioInfo;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 217
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/streams/MediaStream;

    .line 220
    :goto_1
    return-object v1

    .line 215
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 220
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private getOnlineStreams(Landroid/view/Display;Ljava/util/List;)Lcom/google/android/videos/streams/LegacyStreamSelection;
    .locals 5
    .param p1, "display"    # Landroid/view/Display;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Display;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;)",
            "Lcom/google/android/videos/streams/LegacyStreamSelection;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/streams/MissingStreamException;
        }
    .end annotation

    .prologue
    .line 84
    .local p2, "streams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    iget-object v4, p0, Lcom/google/android/videos/streams/LegacyStreamsSelector;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4, p1}, Lcom/google/android/videos/Config;->orderedHqStreamingFormats(Landroid/view/Display;)Ljava/util/List;

    move-result-object v1

    .line 85
    .local v1, "hqStreamFormats":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v4, p0, Lcom/google/android/videos/streams/LegacyStreamsSelector;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v4}, Lcom/google/android/videos/utils/NetworkStatus;->isFastNetwork()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/videos/streams/LegacyStreamsSelector;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4, p1}, Lcom/google/android/videos/Config;->orderedMqStreamingFormats(Landroid/view/Display;)Ljava/util/List;

    move-result-object v3

    .line 88
    .local v3, "lqStreamsFormats":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :goto_0
    invoke-direct {p0, v1, p2}, Lcom/google/android/videos/streams/LegacyStreamsSelector;->getPreferredStreams(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 89
    .local v0, "hiPreferredStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    invoke-direct {p0, v3, p2}, Lcom/google/android/videos/streams/LegacyStreamsSelector;->getPreferredStreams(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 90
    .local v2, "loPreferredStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 91
    new-instance v4, Lcom/google/android/videos/streams/MissingStreamException;

    invoke-direct {v4}, Lcom/google/android/videos/streams/MissingStreamException;-><init>()V

    throw v4

    .line 85
    .end local v0    # "hiPreferredStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    .end local v2    # "loPreferredStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    .end local v3    # "lqStreamsFormats":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/streams/LegacyStreamsSelector;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4, p1}, Lcom/google/android/videos/Config;->orderedLqStreamingFormats(Landroid/view/Display;)Ljava/util/List;

    move-result-object v3

    goto :goto_0

    .line 93
    .restart local v0    # "hiPreferredStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    .restart local v2    # "loPreferredStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    .restart local v3    # "lqStreamsFormats":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_1
    invoke-direct {p0, v0, v2}, Lcom/google/android/videos/streams/LegacyStreamsSelector;->buildStreamSelection(Ljava/util/List;Ljava/util/List;)Lcom/google/android/videos/streams/LegacyStreamSelection;

    move-result-object v4

    return-object v4
.end method

.method private getPreferredStreams(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation

    .prologue
    .line 140
    .local p1, "preferredStreamFormats":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local p2, "streams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 141
    .local v5, "orderedStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v1, 0x0

    .line 142
    .local v1, "haveTracksWithAudioInfo":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v2, v7, :cond_4

    .line 143
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 144
    .local v3, "itag":I
    const/4 v4, 0x0

    .local v4, "k":I
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v7

    if-ge v4, v7, :cond_3

    .line 145
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/videos/streams/MediaStream;

    .line 146
    .local v6, "stream":Lcom/google/android/videos/streams/MediaStream;
    iget-object v7, v6, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v7, v7, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    if-ne v7, v3, :cond_1

    .line 147
    iget-object v7, v6, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-object v0, v7, Lcom/google/android/videos/proto/StreamInfo;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 148
    .local v0, "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    if-eqz v0, :cond_0

    if-nez v1, :cond_0

    .line 151
    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 152
    const/4 v1, 0x1

    .line 154
    :cond_0
    if-nez v1, :cond_2

    .line 156
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 157
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    .end local v0    # "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 159
    .restart local v0    # "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    :cond_2
    if-eqz v0, :cond_1

    .line 161
    const/4 v7, 0x1

    invoke-static {v5, v0, v7}, Lcom/google/android/videos/utils/AudioInfoUtil;->containsStreamWithInfo(Ljava/util/List;Lcom/google/wireless/android/video/magma/proto/AudioInfo;Z)Z

    move-result v7

    if-nez v7, :cond_1

    .line 164
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 142
    .end local v0    # "audioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .end local v6    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 169
    .end local v3    # "itag":I
    .end local v4    # "k":I
    :cond_4
    return-object v5
.end method

.method private shouldRetainStream(Lcom/google/android/videos/streams/MediaStream;IZZ)Z
    .locals 6
    .param p1, "stream"    # Lcom/google/android/videos/streams/MediaStream;
    .param p2, "drmLevel"    # I
    .param p3, "surroundSound"    # Z
    .param p4, "drmStreams"    # Z

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 113
    iget-object v0, p1, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    .line 114
    .local v0, "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    iget v3, v0, Lcom/google/android/videos/streams/ItagInfo;->drmType:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1

    move v1, v2

    .line 128
    :cond_0
    :goto_0
    return v1

    .line 118
    :cond_1
    if-eqz p3, :cond_2

    iget-object v3, p0, Lcom/google/android/videos/streams/LegacyStreamsSelector;->config:Lcom/google/android/videos/Config;

    invoke-interface {v3}, Lcom/google/android/videos/Config;->allowSurroundSoundFormats()Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    iget-object v3, p1, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    invoke-virtual {v3}, Lcom/google/android/videos/streams/ItagInfo;->isSurroundSound()Z

    move-result v3

    if-eqz v3, :cond_3

    move v1, v2

    .line 120
    goto :goto_0

    .line 122
    :cond_3
    if-nez p4, :cond_4

    .line 123
    iget v3, v0, Lcom/google/android/videos/streams/ItagInfo;->drmType:I

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    .line 125
    :cond_4
    if-ne p2, v1, :cond_5

    .line 126
    iget v3, v0, Lcom/google/android/videos/streams/ItagInfo;->drmType:I

    if-eq v3, v1, :cond_0

    move v1, v2

    goto :goto_0

    .line 128
    :cond_5
    iget v3, v0, Lcom/google/android/videos/streams/ItagInfo;->drmType:I

    if-ne v3, v5, :cond_6

    if-ne p2, v5, :cond_0

    iget-object v3, p0, Lcom/google/android/videos/streams/LegacyStreamsSelector;->config:Lcom/google/android/videos/Config;

    invoke-interface {v3}, Lcom/google/android/videos/Config;->fieldProvisionedFormats()Ljava/util/List;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v4, v4, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_6
    move v1, v2

    goto :goto_0
.end method


# virtual methods
.method public getDownloadDrmStreams(Ljava/util/List;ZII)Ljava/util/List;
    .locals 6
    .param p2, "surroundSound"    # Z
    .param p3, "drmLevel"    # I
    .param p4, "preferredQuality"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;ZII)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/streams/MissingStreamException;
        }
    .end annotation

    .prologue
    .line 52
    .local p1, "videoStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v5, 0x1

    invoke-direct {p0, p1, p3, p2, v5}, Lcom/google/android/videos/streams/LegacyStreamsSelector;->filterStreams(Ljava/util/List;IZZ)Ljava/util/List;

    move-result-object v0

    .line 53
    .local v0, "filteredStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 54
    .local v4, "streamsMatchingQuality":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 55
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/streams/MediaStream;

    .line 56
    .local v3, "stream":Lcom/google/android/videos/streams/MediaStream;
    iget-object v5, v3, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    invoke-virtual {v5}, Lcom/google/android/videos/streams/ItagInfo;->getQuality()I

    move-result v5

    if-ne v5, p4, :cond_0

    .line 57
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 60
    .end local v3    # "stream":Lcom/google/android/videos/streams/MediaStream;
    :cond_1
    iget-object v5, p0, Lcom/google/android/videos/streams/LegacyStreamsSelector;->config:Lcom/google/android/videos/Config;

    invoke-interface {v5}, Lcom/google/android/videos/Config;->orderedDownloadFormats()Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, v5, v4}, Lcom/google/android/videos/streams/LegacyStreamsSelector;->getPreferredStreams(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 62
    .local v2, "preferredStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 64
    iget-object v5, p0, Lcom/google/android/videos/streams/LegacyStreamsSelector;->config:Lcom/google/android/videos/Config;

    invoke-interface {v5}, Lcom/google/android/videos/Config;->orderedDownloadFormats()Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, v5, v0}, Lcom/google/android/videos/streams/LegacyStreamsSelector;->getPreferredStreams(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 66
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 67
    new-instance v5, Lcom/google/android/videos/streams/MissingStreamException;

    invoke-direct {v5}, Lcom/google/android/videos/streams/MissingStreamException;-><init>()V

    throw v5

    .line 69
    :cond_3
    return-object v2
.end method

.method public getOnlineClearStreams(Landroid/view/Display;Ljava/util/List;Z)Lcom/google/android/videos/streams/LegacyStreamSelection;
    .locals 1
    .param p1, "display"    # Landroid/view/Display;
    .param p3, "surroundSound"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Display;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;Z)",
            "Lcom/google/android/videos/streams/LegacyStreamSelection;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/streams/MissingStreamException;
        }
    .end annotation

    .prologue
    .local p2, "mediaStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v0, 0x0

    .line 79
    invoke-direct {p0, p2, v0, p3, v0}, Lcom/google/android/videos/streams/LegacyStreamsSelector;->filterStreams(Ljava/util/List;IZZ)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/streams/LegacyStreamsSelector;->getOnlineStreams(Landroid/view/Display;Ljava/util/List;)Lcom/google/android/videos/streams/LegacyStreamSelection;

    move-result-object v0

    return-object v0
.end method

.method public getOnlineDrmStreams(Landroid/view/Display;Ljava/util/List;ZI)Lcom/google/android/videos/streams/LegacyStreamSelection;
    .locals 1
    .param p1, "display"    # Landroid/view/Display;
    .param p3, "surroundSound"    # Z
    .param p4, "drmLevel"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Display;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;ZI)",
            "Lcom/google/android/videos/streams/LegacyStreamSelection;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/streams/MissingStreamException;
        }
    .end annotation

    .prologue
    .line 74
    .local p2, "mediaStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p2, p4, p3, v0}, Lcom/google/android/videos/streams/LegacyStreamsSelector;->filterStreams(Ljava/util/List;IZZ)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/streams/LegacyStreamsSelector;->getOnlineStreams(Landroid/view/Display;Ljava/util/List;)Lcom/google/android/videos/streams/LegacyStreamSelection;

    move-result-object v0

    return-object v0
.end method
