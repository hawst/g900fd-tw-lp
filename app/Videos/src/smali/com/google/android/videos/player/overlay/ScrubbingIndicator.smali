.class public Lcom/google/android/videos/player/overlay/ScrubbingIndicator;
.super Landroid/view/View;
.source "ScrubbingIndicator.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# static fields
.field private static final COS_30:F


# instance fields
.field private animation:Landroid/animation/ValueAnimator;

.field private final animationInterpolator:Landroid/animation/TimeInterpolator;

.field private lastTriangle:I

.field private rewinding:Z

.field private scrubberOffset:F

.field private strokeWidth:F

.field private triangleFillPaint:Landroid/graphics/Paint;

.field private final triangleOutlinePaint:Landroid/graphics/Paint;

.field private triangleOutlinePath:Landroid/graphics/Path;

.field private trianglePath:Landroid/graphics/Path;

.field private trianglePoints:[F

.field private triangleScale:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    const-wide v0, 0x3fe0c152382d7365L    # 0.5235987755982988

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->COS_30:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeSet"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, -0x1

    .line 51
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v1, 0x6

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePoints:[F

    .line 53
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleOutlinePaint:Landroid/graphics/Paint;

    .line 54
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 57
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0e01dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->strokeWidth:F

    .line 58
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleOutlinePaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->strokeWidth:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 59
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleOutlinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 60
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleOutlinePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 61
    const v1, 0x7f0c0035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->lastTriangle:I

    .line 64
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleFillPaint:Landroid/graphics/Paint;

    .line 65
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleFillPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 66
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleFillPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 68
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleOutlinePath:Landroid/graphics/Path;

    .line 69
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePath:Landroid/graphics/Path;

    .line 71
    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->animationInterpolator:Landroid/animation/TimeInterpolator;

    .line 72
    return-void
.end method

.method private drawTriangle(Landroid/graphics/Canvas;I)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "index"    # I

    .prologue
    const/high16 v6, 0x40400000    # 3.0f

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v4, 0x3f800000    # 1.0f

    .line 143
    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->scrubberOffset:F

    add-float/2addr v2, v5

    int-to-float v3, p2

    sub-float v1, v2, v3

    .line 144
    .local v1, "phase":F
    const/4 v0, 0x0

    .line 145
    .local v0, "looping":Z
    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->lastTriangle:I

    if-ne p2, v2, :cond_1

    cmpl-float v2, v1, v4

    if-lez v2, :cond_1

    .line 146
    const/high16 v1, 0x3f800000    # 1.0f

    .line 157
    :cond_0
    :goto_0
    invoke-direct {p0, p1, v1}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->drawTriangleFill(Landroid/graphics/Canvas;F)V

    .line 158
    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->drawTriangleOutline(Landroid/graphics/Canvas;FZ)V

    .line 159
    return-void

    .line 147
    :cond_1
    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->lastTriangle:I

    add-int/lit8 v2, v2, -0x1

    if-ne p2, v2, :cond_0

    cmpl-float v2, v1, v6

    if-lez v2, :cond_0

    .line 149
    sub-float v2, v1, v6

    rem-float v1, v2, v5

    .line 151
    cmpl-float v2, v1, v4

    if-lez v2, :cond_2

    .line 152
    add-float/2addr v1, v4

    .line 154
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private drawTriangleArc(Landroid/graphics/Canvas;FF)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "from"    # F
    .param p3, "length"    # F

    .prologue
    .line 186
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleOutlinePath:Landroid/graphics/Path;

    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    .line 187
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    .line 188
    .local v2, "partialPoint":Landroid/graphics/PointF;
    invoke-direct {p0, p2, v2}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->interpolatePoint(FLandroid/graphics/PointF;)V

    .line 189
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleOutlinePath:Landroid/graphics/Path;

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v5, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 190
    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v3, p2

    float-to-int v1, v3

    .local v1, "i":I
    :goto_0
    int-to-float v3, v1

    add-float v4, p2, p3

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    .line 191
    rem-int/lit8 v3, v1, 0x3

    mul-int/lit8 v0, v3, 0x2

    .line 192
    .local v0, "corner":I
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleOutlinePath:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePoints:[F

    aget v4, v4, v0

    iget-object v5, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePoints:[F

    add-int/lit8 v6, v0, 0x1

    aget v5, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 190
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 194
    .end local v0    # "corner":I
    :cond_0
    add-float v3, p2, p3

    invoke-direct {p0, v3, v2}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->interpolatePoint(FLandroid/graphics/PointF;)V

    .line 195
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleOutlinePath:Landroid/graphics/Path;

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v5, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 197
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleOutlinePath:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 198
    return-void
.end method

.method private drawTriangleFill(Landroid/graphics/Canvas;F)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "phase"    # F

    .prologue
    const v4, 0x4019999a    # 2.4f

    const v3, 0x3f19999a    # 0.6f

    const v2, 0x3ecccccd    # 0.4f

    .line 206
    cmpl-float v1, p2, v3

    if-ltz v1, :cond_0

    cmpg-float v1, p2, v4

    if-gtz v1, :cond_0

    .line 208
    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v1, p2, v1

    if-gtz v1, :cond_1

    .line 209
    sub-float v1, p2, v3

    div-float v0, v1, v2

    .line 215
    .local v0, "alpha":F
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleFillPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v2, v0

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 216
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleFillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 218
    .end local v0    # "alpha":F
    :cond_0
    return-void

    .line 210
    :cond_1
    const/high16 v1, 0x40000000    # 2.0f

    cmpg-float v1, p2, v1

    if-gtz v1, :cond_2

    .line 211
    const/high16 v0, 0x3f800000    # 1.0f

    .restart local v0    # "alpha":F
    goto :goto_0

    .line 213
    .end local v0    # "alpha":F
    :cond_2
    sub-float v1, v4, p2

    div-float v0, v1, v2

    .restart local v0    # "alpha":F
    goto :goto_0
.end method

.method private drawTriangleOutline(Landroid/graphics/Canvas;FZ)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "phase"    # F
    .param p3, "looping"    # Z

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v4, 0x40400000    # 3.0f

    .line 168
    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->strokeWidth:F

    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleScale:F

    div-float v1, v2, v3

    .line 169
    .local v1, "miterOffset":F
    const/4 v2, 0x0

    cmpg-float v2, p2, v2

    if-gez v2, :cond_1

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 171
    :cond_1
    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v2, p2, v2

    if-gez v2, :cond_3

    .line 172
    if-eqz p3, :cond_2

    .line 173
    neg-float v2, v1

    mul-float v3, p2, v4

    invoke-direct {p0, p1, v2, v3}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->drawTriangleArc(Landroid/graphics/Canvas;FF)V

    goto :goto_0

    .line 175
    :cond_2
    const/high16 v2, 0x3fc00000    # 1.5f

    mul-float v3, p2, v4

    invoke-direct {p0, p1, v2, v3}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->drawTriangleArc(Landroid/graphics/Canvas;FF)V

    goto :goto_0

    .line 177
    :cond_3
    cmpg-float v2, p2, v5

    if-gez v2, :cond_4

    .line 178
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePath:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 179
    :cond_4
    cmpg-float v2, p2, v4

    if-gez v2, :cond_0

    .line 180
    sub-float v2, p2, v5

    mul-float v0, v2, v4

    .line 181
    .local v0, "fadedArc":F
    add-float v2, v1, v0

    sub-float v3, v4, v0

    invoke-direct {p0, p1, v2, v3}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->drawTriangleArc(Landroid/graphics/Canvas;FF)V

    goto :goto_0
.end method

.method private interpolatePoint(FLandroid/graphics/PointF;)V
    .locals 10
    .param p1, "arc"    # F
    .param p2, "result"    # Landroid/graphics/PointF;

    .prologue
    const/high16 v9, 0x40400000    # 3.0f

    .line 221
    const/4 v7, 0x0

    cmpg-float v7, p1, v7

    if-gez v7, :cond_0

    .line 222
    const/high16 v7, -0x3fc00000    # -3.0f

    div-float v7, p1, v7

    const/high16 v8, 0x3f800000    # 1.0f

    add-float/2addr v7, v8

    float-to-int v7, v7

    int-to-float v7, v7

    mul-float/2addr v7, v9

    add-float/2addr p1, v7

    .line 226
    :goto_0
    float-to-int v0, p1

    .line 227
    .local v0, "from":I
    add-int/lit8 v7, v0, 0x1

    rem-int/lit8 v4, v7, 0x3

    .line 228
    .local v4, "to":I
    int-to-float v7, v0

    sub-float v3, p1, v7

    .line 229
    .local v3, "point":F
    iget-object v7, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePoints:[F

    mul-int/lit8 v8, v0, 0x2

    aget v1, v7, v8

    .line 230
    .local v1, "fromX":F
    iget-object v7, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePoints:[F

    mul-int/lit8 v8, v0, 0x2

    add-int/lit8 v8, v8, 0x1

    aget v2, v7, v8

    .line 231
    .local v2, "fromY":F
    iget-object v7, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePoints:[F

    mul-int/lit8 v8, v4, 0x2

    aget v5, v7, v8

    .line 232
    .local v5, "toX":F
    iget-object v7, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePoints:[F

    mul-int/lit8 v8, v4, 0x2

    add-int/lit8 v8, v8, 0x1

    aget v6, v7, v8

    .line 233
    .local v6, "toY":F
    sub-float v7, v5, v1

    mul-float/2addr v7, v3

    add-float/2addr v7, v1

    iput v7, p2, Landroid/graphics/PointF;->x:F

    .line 234
    sub-float v7, v6, v2

    mul-float/2addr v7, v3

    add-float/2addr v7, v2

    iput v7, p2, Landroid/graphics/PointF;->y:F

    .line 235
    return-void

    .line 224
    .end local v0    # "from":I
    .end local v1    # "fromX":F
    .end local v2    # "fromY":F
    .end local v3    # "point":F
    .end local v4    # "to":I
    .end local v5    # "toX":F
    .end local v6    # "toY":F
    :cond_0
    rem-float/2addr p1, v9

    goto :goto_0
.end method


# virtual methods
.method public endAnimation()V
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->animation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->animation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->animation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 265
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->animation:Landroid/animation/ValueAnimator;

    .line 267
    :cond_0
    return-void
.end method

.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 239
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->scrubberOffset:F

    .line 240
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->invalidate()V

    .line 241
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v10, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v7, 0x3f800000    # 1.0f

    const v6, 0x3dcccccd    # 0.1f

    const/4 v8, 0x0

    .line 107
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 109
    iget-boolean v5, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->rewinding:Z

    if-eqz v5, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->getWidth()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p1, v5, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 112
    const/high16 v5, -0x40800000    # -1.0f

    invoke-virtual {p1, v5, v7}, Landroid/graphics/Canvas;->scale(FF)V

    .line 115
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->getHeight()I

    move-result v5

    int-to-float v1, v5

    .line 117
    .local v1, "height":F
    mul-float v5, v6, v1

    mul-float/2addr v6, v1

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 122
    iget v5, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->scrubberOffset:F

    add-float/2addr v5, v9

    iget v6, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->lastTriangle:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    cmpl-float v5, v5, v7

    if-lez v5, :cond_1

    .line 123
    iget v5, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->lastTriangle:I

    add-int/lit8 v3, v5, -0x1

    .line 124
    .local v3, "start":I
    iget v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->lastTriangle:I

    .line 134
    .local v0, "end":I
    :goto_0
    sget v5, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->COS_30:F

    iget v6, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleScale:F

    mul-float/2addr v5, v6

    const/high16 v6, 0x3f000000    # 0.5f

    sget v7, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->COS_30:F

    add-float/2addr v6, v7

    iget v7, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->strokeWidth:F

    mul-float/2addr v6, v7

    add-float v4, v5, v6

    .line 135
    .local v4, "triangleWidth":F
    int-to-float v5, v3

    mul-float/2addr v5, v4

    invoke-virtual {p1, v5, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 136
    move v2, v3

    .local v2, "i":I
    :goto_1
    if-gt v2, v0, :cond_2

    .line 137
    invoke-direct {p0, p1, v2}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->drawTriangle(Landroid/graphics/Canvas;I)V

    .line 138
    invoke-virtual {p1, v4, v8}, Landroid/graphics/Canvas;->translate(FF)V

    .line 136
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 127
    .end local v0    # "end":I
    .end local v2    # "i":I
    .end local v3    # "start":I
    .end local v4    # "triangleWidth":F
    :cond_1
    iget v5, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->scrubberOffset:F

    add-float/2addr v5, v9

    const/high16 v6, 0x40400000    # 3.0f

    sub-float/2addr v5, v6

    float-to-int v3, v5

    .line 128
    .restart local v3    # "start":I
    add-int/lit8 v0, v3, 0x3

    .line 129
    .restart local v0    # "end":I
    invoke-static {v3, v10}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 130
    invoke-static {v0, v10}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    .line 140
    .restart local v2    # "i":I
    .restart local v4    # "triangleWidth":F
    :cond_2
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 6
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 92
    int-to-float v0, p2

    const v1, 0x3f4ccccd    # 0.8f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleScale:F

    .line 94
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePoints:[F

    sget v1, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->COS_30:F

    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleScale:F

    mul-float/2addr v1, v2

    aput v1, v0, v3

    .line 95
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePoints:[F

    const/high16 v1, 0x3f000000    # 0.5f

    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleScale:F

    mul-float/2addr v1, v2

    aput v1, v0, v4

    .line 96
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePoints:[F

    iget v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->triangleScale:F

    aput v1, v0, v5

    .line 98
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 99
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePoints:[F

    aget v1, v1, v3

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePoints:[F

    aget v2, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 100
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePoints:[F

    const/4 v2, 0x2

    aget v1, v1, v2

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePoints:[F

    aget v2, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 101
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePoints:[F

    const/4 v2, 0x4

    aget v1, v1, v2

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePoints:[F

    const/4 v3, 0x5

    aget v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 102
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->trianglePath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 103
    return-void
.end method

.method public setRewinding(Z)V
    .locals 0
    .param p1, "rewinding"    # Z

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->rewinding:Z

    .line 79
    return-void
.end method

.method public setScrubTime(I)V
    .locals 2
    .param p1, "timeMillis"    # I

    .prologue
    .line 86
    int-to-float v0, p1

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    const v1, 0x3e23d70a    # 0.16f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->scrubberOffset:F

    .line 87
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->invalidate()V

    .line 88
    return-void
.end method

.method public startAutoScrubAnimation()V
    .locals 4

    .prologue
    .line 254
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->scrubberOffset:F

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->scrubberOffset:F

    const/high16 v3, 0x447a0000    # 1000.0f

    add-float/2addr v2, v3

    aput v2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->animation:Landroid/animation/ValueAnimator;

    .line 256
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->animation:Landroid/animation/ValueAnimator;

    const-wide/32 v2, 0x16e360

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 257
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->animation:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->animationInterpolator:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 258
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->animation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 259
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->animation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 260
    return-void
.end method

.method public startFlingAnimation()V
    .locals 4

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->endAnimation()V

    .line 245
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->animation:Landroid/animation/ValueAnimator;

    .line 246
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->animation:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 247
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->animation:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->animationInterpolator:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 248
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->animation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->animation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 250
    return-void

    .line 245
    :array_0
    .array-data 4
        -0x40000000    # -2.0f
        0x0
    .end array-data
.end method
