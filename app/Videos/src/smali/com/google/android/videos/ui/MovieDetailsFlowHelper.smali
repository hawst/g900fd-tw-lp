.class public Lcom/google/android/videos/ui/MovieDetailsFlowHelper;
.super Lcom/google/android/videos/remote/TransportControl;
.source "MovieDetailsFlowHelper.java"

# interfaces
.implements Lcom/google/android/videos/flow/ViewHolderCreator;
.implements Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/MovieDetailsFlowHelper$StorylineViewHolder;,
        Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;,
        Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder;,
        Lcom/google/android/videos/ui/MovieDetailsFlowHelper$PosterViewHolder;,
        Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelViewHolder;,
        Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/remote/TransportControl;",
        "Lcom/google/android/videos/flow/ViewHolderCreator",
        "<",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;",
        "Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;"
    }
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final activity:Lcom/google/android/videos/activity/MovieDetailsActivity;

.field private final allowDownloads:Z

.field private downloadStatus:Lcom/google/android/videos/store/VideoDownloadStatus;

.field private final flow:Lcom/google/android/videos/flow/Flow;

.field private final infoPanelItemClickListener:Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;

.field private isPlayingRemotely:Z

.field private movieTitle:Ljava/lang/String;

.field private networkConnected:Z

.field private final posterFlow:Lcom/google/android/videos/flow/SingleViewFlow;

.field private final posterStorePosterRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final seeMoreOnClickListener:Landroid/view/View$OnClickListener;

.field private final shareHelper:Lcom/google/android/videos/ui/ShareHelper;

.field private storyline:Ljava/lang/String;

.field private final storylineFlow:Lcom/google/android/videos/flow/SingleViewFlow;

.field private subheading:Ljava/lang/String;

.field private final subheadingDownloadFlow:Lcom/google/android/videos/flow/SingleViewFlow;

.field private suggestionLimitApplied:Z

.field private final suggestionsFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/ui/ItemsWithHeadingFlow",
            "<**>;"
        }
    .end annotation
.end field

.field private final titlePlayButtonFlow:Lcom/google/android/videos/flow/SingleViewFlow;

.field private final videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/activity/MovieDetailsActivity;Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/adapter/ArrayDataSource;Landroid/view/View$OnClickListener;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/logging/UiElementNode;)V
    .locals 18
    .param p1, "activity"    # Lcom/google/android/videos/activity/MovieDetailsActivity;
    .param p2, "infoPanelItemClickListener"    # Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;
    .param p5, "seeMoreOnClickListener"    # Landroid/view/View$OnClickListener;
    .param p7, "suggestionClickListener"    # Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;
    .param p8, "allowDownloads"    # Z
    .param p9, "account"    # Ljava/lang/String;
    .param p10, "videoId"    # Ljava/lang/String;
    .param p11, "remoteTracker"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p12, "parentNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/activity/MovieDetailsActivity;",
            "Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/adapter/ArrayDataSource",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/remote/RemoteTracker;",
            "Lcom/google/android/videos/logging/UiElementNode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 108
    .local p3, "posterStorePosterRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    .local p4, "suggestionsDataSource":Lcom/google/android/videos/adapter/ArrayDataSource;, "Lcom/google/android/videos/adapter/ArrayDataSource<Lcom/google/wireless/android/video/magma/proto/AssetResource;>;"
    .local p6, "posterArtRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, p11

    invoke-direct {v0, v1}, Lcom/google/android/videos/remote/TransportControl;-><init>(Lcom/google/android/videos/remote/RemoteTracker;)V

    .line 110
    invoke-static/range {p1 .. p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/activity/MovieDetailsActivity;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->activity:Lcom/google/android/videos/activity/MovieDetailsActivity;

    .line 111
    invoke-static/range {p2 .. p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->infoPanelItemClickListener:Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;

    .line 112
    invoke-static/range {p3 .. p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/async/Requester;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->posterStorePosterRequester:Lcom/google/android/videos/async/Requester;

    .line 113
    move/from16 v0, p8

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->allowDownloads:Z

    .line 114
    invoke-static/range {p9 .. p9}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->account:Ljava/lang/String;

    .line 115
    invoke-static/range {p10 .. p10}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->videoId:Ljava/lang/String;

    .line 116
    invoke-static/range {p5 .. p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View$OnClickListener;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->seeMoreOnClickListener:Landroid/view/View$OnClickListener;

    .line 117
    invoke-static/range {p4 .. p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    invoke-static/range {p6 .. p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    new-instance v16, Lcom/google/android/videos/flow/SingleViewFlow;

    const v2, 0x7f040018

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-direct {v0, v2, v1}, Lcom/google/android/videos/flow/SingleViewFlow;-><init>(ILcom/google/android/videos/flow/ViewHolderCreator;)V

    .line 121
    .local v16, "spacerFlow":Lcom/google/android/videos/flow/SingleViewFlow;
    new-instance v11, Lcom/google/android/videos/flow/SingleViewFlow;

    const v2, 0x7f04006d

    move-object/from16 v0, p0

    invoke-direct {v11, v2, v0}, Lcom/google/android/videos/flow/SingleViewFlow;-><init>(ILcom/google/android/videos/flow/ViewHolderCreator;)V

    .line 122
    .local v11, "infoPanelFlow":Lcom/google/android/videos/flow/SingleViewFlow;
    new-instance v2, Lcom/google/android/videos/flow/SingleViewFlow;

    const v3, 0x7f04006e

    move-object/from16 v0, p0

    invoke-direct {v2, v3, v0}, Lcom/google/android/videos/flow/SingleViewFlow;-><init>(ILcom/google/android/videos/flow/ViewHolderCreator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->posterFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    .line 123
    new-instance v2, Lcom/google/android/videos/flow/SingleViewFlow;

    const v3, 0x7f040073

    move-object/from16 v0, p0

    invoke-direct {v2, v3, v0}, Lcom/google/android/videos/flow/SingleViewFlow;-><init>(ILcom/google/android/videos/flow/ViewHolderCreator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->titlePlayButtonFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    .line 124
    new-instance v2, Lcom/google/android/videos/flow/SingleViewFlow;

    const v3, 0x7f0400ce

    move-object/from16 v0, p0

    invoke-direct {v2, v3, v0}, Lcom/google/android/videos/flow/SingleViewFlow;-><init>(ILcom/google/android/videos/flow/ViewHolderCreator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->subheadingDownloadFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    .line 126
    new-instance v2, Lcom/google/android/videos/flow/SingleViewFlow;

    const v3, 0x7f040070

    move-object/from16 v0, p0

    invoke-direct {v2, v3, v0}, Lcom/google/android/videos/flow/SingleViewFlow;-><init>(ILcom/google/android/videos/flow/ViewHolderCreator;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->storylineFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    .line 131
    new-instance v9, Lcom/google/android/videos/flow/SingleViewFlow;

    const v2, 0x7f04006c

    invoke-direct {v9, v2}, Lcom/google/android/videos/flow/SingleViewFlow;-><init>(I)V

    .line 134
    .local v9, "dividerFlow":Lcom/google/android/videos/flow/SingleViewFlow;
    const v2, 0x7f040070

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->parseFromLayoutResource(Landroid/content/Context;I)Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    move-result-object v17

    .line 137
    .local v17, "storylineLayoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    move-object/from16 v0, v17

    iget v2, v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridColumnCount:F

    invoke-static {v2}, Lcom/google/android/play/utils/Compound;->floatLengthToCompound(F)I

    move-result v2

    move-object/from16 v0, v17

    iget v3, v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->widthCompound:I

    if-ne v2, v3, :cond_0

    .line 140
    move-object v10, v9

    .line 141
    .local v10, "firstStorylineOrDividerFlow":Lcom/google/android/videos/flow/Flow;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->storylineFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    .line 148
    .local v13, "secondStorylineOrDividerFlow":Lcom/google/android/videos/flow/Flow;
    :goto_0
    new-instance v6, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView$Binder;

    move-object/from16 v0, p6

    move-object/from16 v1, p12

    invoke-direct {v6, v0, v1}, Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView$Binder;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/logging/UiElementNode;)V

    .line 150
    .local v6, "suggestionBinder":Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView$Binder;
    new-instance v2, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    const v5, 0x7f040027

    const/4 v8, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p4

    move-object/from16 v7, p7

    invoke-direct/range {v2 .. v8}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;-><init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;Lcom/google/android/videos/adapter/DataSource;ILcom/google/android/videos/ui/playnext/ClusterItemView$Binder;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->suggestionsFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    .line 159
    invoke-static/range {p10 .. p10}, Lcom/google/android/videos/utils/PlayStoreUriCreator;->createMovieDetailsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    .line 160
    .local v15, "shareUrl":Landroid/net/Uri;
    invoke-static/range {p10 .. p10}, Lcom/google/android/videos/utils/PlayStoreUriCreator;->createMoviePlusOneUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    .line 161
    .local v12, "plusOneUrl":Landroid/net/Uri;
    move-object/from16 v0, p1

    move-object/from16 v1, p9

    invoke-static {v0, v15, v12, v1}, Lcom/google/android/videos/ui/ShareHelper;->createShareHelper(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/videos/ui/ShareHelper;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->shareHelper:Lcom/google/android/videos/ui/ShareHelper;

    .line 162
    new-instance v14, Lcom/google/android/videos/flow/SingleViewFlow;

    invoke-static {}, Lcom/google/android/videos/ui/ShareHelper;->getMainLayout()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->shareHelper:Lcom/google/android/videos/ui/ShareHelper;

    invoke-direct {v14, v2, v3}, Lcom/google/android/videos/flow/SingleViewFlow;-><init>(ILcom/google/android/videos/flow/ViewHolderCreator;)V

    .line 164
    .local v14, "shareFlow":Lcom/google/android/videos/flow/SingleViewFlow;
    new-instance v2, Lcom/google/android/videos/flow/SequentialFlow;

    const/16 v3, 0x9

    new-array v3, v3, [Lcom/google/android/videos/flow/Flow;

    const/4 v4, 0x0

    aput-object v16, v3, v4

    const/4 v4, 0x1

    aput-object v11, v3, v4

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->posterFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->titlePlayButtonFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->subheadingDownloadFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    aput-object v5, v3, v4

    const/4 v4, 0x5

    aput-object v10, v3, v4

    const/4 v4, 0x6

    aput-object v13, v3, v4

    const/4 v4, 0x7

    aput-object v14, v3, v4

    const/16 v4, 0x8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->suggestionsFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    aput-object v5, v3, v4

    invoke-direct {v2, v3}, Lcom/google/android/videos/flow/SequentialFlow;-><init>([Lcom/google/android/videos/flow/Flow;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->flow:Lcom/google/android/videos/flow/Flow;

    .line 174
    return-void

    .line 144
    .end local v6    # "suggestionBinder":Lcom/google/android/videos/ui/playnext/MovieSuggestionClusterItemView$Binder;
    .end local v10    # "firstStorylineOrDividerFlow":Lcom/google/android/videos/flow/Flow;
    .end local v12    # "plusOneUrl":Landroid/net/Uri;
    .end local v13    # "secondStorylineOrDividerFlow":Lcom/google/android/videos/flow/Flow;
    .end local v14    # "shareFlow":Lcom/google/android/videos/flow/SingleViewFlow;
    .end local v15    # "shareUrl":Landroid/net/Uri;
    :cond_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->storylineFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    .line 145
    .restart local v10    # "firstStorylineOrDividerFlow":Lcom/google/android/videos/flow/Flow;
    move-object v13, v9

    .restart local v13    # "secondStorylineOrDividerFlow":Lcom/google/android/videos/flow/Flow;
    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/ui/ItemsWithHeadingFlow;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->suggestionsFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/activity/MovieDetailsActivity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->activity:Lcom/google/android/videos/activity/MovieDetailsActivity;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->storyline:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->videoId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/async/Requester;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->posterStorePosterRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->infoPanelItemClickListener:Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->movieTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->allowDownloads:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->subheading:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/store/VideoDownloadStatus;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->downloadStatus:Lcom/google/android/videos/store/VideoDownloadStatus;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->account:Ljava/lang/String;

    return-object v0
.end method

.method private maybeApplySuggestionLimit(Landroid/view/ViewGroup;)V
    .locals 6
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 188
    iget-boolean v4, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->suggestionLimitApplied:Z

    if-eqz v4, :cond_0

    .line 209
    :goto_0
    return-void

    .line 191
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->activity:Lcom/google/android/videos/activity/MovieDetailsActivity;

    const v5, 0x7f040027

    invoke-static {v4, v5}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->parseFromLayoutResource(Landroid/content/Context;I)Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    move-result-object v1

    .line 194
    .local v1, "suggestionItemLayoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v5

    sub-int v0, v4, v5

    .line 195
    .local v0, "fullContextWidth":I
    invoke-virtual {v1, v0}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getEffectiveColumnCount(I)F

    move-result v4

    float-to-int v2, v4

    .line 198
    .local v2, "suggestionLineItemCount":I
    iget-object v4, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->activity:Lcom/google/android/videos/activity/MovieDetailsActivity;

    invoke-virtual {v4}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0031

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    add-int/2addr v4, v2

    add-int/lit8 v4, v4, -0x1

    div-int/2addr v4, v2

    mul-int v3, v2, v4

    .line 202
    .local v3, "suggestionsItemCountLimit":I
    new-instance v4, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$1;

    invoke-direct {v4, p0, v3}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$1;-><init>(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;I)V

    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    .line 208
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->suggestionLimitApplied:Z

    goto :goto_0
.end method

.method private setIsPlayingRemotely(Z)V
    .locals 1
    .param p1, "isPlayingRemotely"    # Z

    .prologue
    .line 300
    iget-boolean v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->isPlayingRemotely:Z

    if-eq v0, p1, :cond_0

    .line 301
    iput-boolean p1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->isPlayingRemotely:Z

    .line 302
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->titlePlayButtonFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    invoke-virtual {v0}, Lcom/google/android/videos/flow/SingleViewFlow;->notifyItemChanged()V

    .line 304
    :cond_0
    return-void
.end method


# virtual methods
.method public bindSectionHeading(Lcom/google/android/videos/ui/SectionHeadingHelper;I)V
    .locals 7
    .param p1, "viewHolder"    # Lcom/google/android/videos/ui/SectionHeadingHelper;
    .param p2, "remaining"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 310
    iget-object v3, p1, Lcom/google/android/videos/ui/SectionHeadingHelper;->itemView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 311
    .local v0, "resources":Landroid/content/res/Resources;
    const v3, 0x7f0b0133

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->movieTitle:Ljava/lang/String;

    aput-object v5, v4, v2

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0b00d1

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v6, v4}, Lcom/google/android/videos/ui/SectionHeadingHelper;->setTexts(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 315
    iget-object v3, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->seeMoreOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v3, v2, v6}, Lcom/google/android/videos/ui/SectionHeadingHelper;->setOnClickListener(Landroid/view/View$OnClickListener;ILjava/lang/Object;)V

    .line 316
    iget-boolean v3, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->networkConnected:Z

    if-nez v3, :cond_0

    :goto_0
    invoke-virtual {p1, v1}, Lcom/google/android/videos/ui/SectionHeadingHelper;->setDimmed(Z)V

    .line 317
    return-void

    :cond_0
    move v1, v2

    .line 316
    goto :goto_0
.end method

.method public createViewHolder(ILandroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 4
    .param p1, "viewType"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 322
    invoke-direct {p0, p2}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->maybeApplySuggestionLimit(Landroid/view/ViewGroup;)V

    .line 324
    iget-object v1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->activity:Lcom/google/android/videos/activity/MovieDetailsActivity;

    invoke-virtual {v1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 325
    .local v0, "itemView":Landroid/view/View;
    const v1, 0x7f04006d

    if-eq p1, v1, :cond_0

    .line 326
    invoke-static {v0}, Lcom/google/android/videos/utils/ViewUtil;->removeOutlineProvider(Landroid/view/View;)V

    .line 328
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 341
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown viewType 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 330
    :sswitch_0
    new-instance v1, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelViewHolder;-><init>(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;Landroid/view/View;)V

    .line 338
    :goto_0
    return-object v1

    .line 332
    :sswitch_1
    new-instance v1, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$PosterViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$PosterViewHolder;-><init>(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;Landroid/view/View;)V

    goto :goto_0

    .line 334
    :sswitch_2
    new-instance v1, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder;-><init>(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;Landroid/view/View;)V

    goto :goto_0

    .line 336
    :sswitch_3
    new-instance v1, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$SubheadingDownloadViewHolder;-><init>(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;Landroid/view/View;)V

    goto :goto_0

    .line 338
    :sswitch_4
    new-instance v1, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$StorylineViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$StorylineViewHolder;-><init>(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;Landroid/view/View;)V

    goto :goto_0

    .line 328
    :sswitch_data_0
    .sparse-switch
        0x7f04006d -> :sswitch_0
        0x7f04006e -> :sswitch_1
        0x7f040070 -> :sswitch_4
        0x7f040073 -> :sswitch_2
        0x7f0400ce -> :sswitch_3
    .end sparse-switch
.end method

.method public getFlow()Lcom/google/android/videos/flow/Flow;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->flow:Lcom/google/android/videos/flow/Flow;

    return-object v0
.end method

.method public loadPoster()V
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->posterFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    invoke-virtual {v0}, Lcom/google/android/videos/flow/SingleViewFlow;->notifyItemChanged()V

    .line 276
    return-void
.end method

.method public onPlayerStateChanged()V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    iget-object v1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/remote/RemoteTracker;->isPlaying(Ljava/lang/String;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->setIsPlayingRemotely(Z)V

    .line 297
    return-void
.end method

.method public onVideo(Lcom/google/android/videos/store/VideoMetadata;Lcom/google/android/videos/store/VideoDownloadStatus;)V
    .locals 12
    .param p1, "video"    # Lcom/google/android/videos/store/VideoMetadata;
    .param p2, "downloadStatus"    # Lcom/google/android/videos/store/VideoDownloadStatus;

    .prologue
    const/4 v6, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 212
    const/4 v3, 0x0

    .line 213
    .local v3, "subheadingDownloadFlowChanged":Z
    const/4 v1, 0x0

    .line 214
    .local v1, "storylineFlowChanged":Z
    const/4 v4, 0x0

    .line 217
    .local v4, "suggestionsFlowChanged":Z
    iget-object v5, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->movieTitle:Ljava/lang/String;

    iget-object v7, p1, Lcom/google/android/videos/store/VideoMetadata;->title:Ljava/lang/String;

    invoke-static {v5, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 218
    iget-object v5, p1, Lcom/google/android/videos/store/VideoMetadata;->title:Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->movieTitle:Ljava/lang/String;

    .line 219
    iget-object v7, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->shareHelper:Lcom/google/android/videos/ui/ShareHelper;

    iget-object v5, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->movieTitle:Ljava/lang/String;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->movieTitle:Ljava/lang/String;

    :goto_0
    invoke-virtual {v7, v5}, Lcom/google/android/videos/ui/ShareHelper;->setTitle(Ljava/lang/String;)V

    .line 220
    iget-object v5, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->titlePlayButtonFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    invoke-virtual {v5}, Lcom/google/android/videos/flow/SingleViewFlow;->notifyItemChanged()V

    .line 221
    iget-boolean v5, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->allowDownloads:Z

    if-eqz v5, :cond_0

    .line 222
    const/4 v3, 0x1

    .line 224
    :cond_0
    const/4 v4, 0x1

    .line 228
    :cond_1
    const/4 v5, 0x3

    new-array v2, v5, [Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/videos/store/VideoMetadata;->classification:Ljava/lang/String;

    aput-object v5, v2, v11

    iget v5, p1, Lcom/google/android/videos/store/VideoMetadata;->releaseYear:I

    if-lez v5, :cond_a

    iget v5, p1, Lcom/google/android/videos/store/VideoMetadata;->releaseYear:I

    invoke-static {v5}, Lcom/google/android/videos/utils/TimeUtil;->getStandaloneYearString(I)Ljava/lang/String;

    move-result-object v5

    :goto_1
    aput-object v5, v2, v10

    const/4 v5, 0x2

    iget v7, p1, Lcom/google/android/videos/store/VideoMetadata;->durationSecs:I

    if-lez v7, :cond_2

    iget-object v6, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->activity:Lcom/google/android/videos/activity/MovieDetailsActivity;

    const v7, 0x7f0b0138

    new-array v8, v10, [Ljava/lang/Object;

    iget v9, p1, Lcom/google/android/videos/store/VideoMetadata;->durationSecs:I

    div-int/lit8 v9, v9, 0x3c

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-virtual {v6, v7, v8}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    :cond_2
    aput-object v6, v2, v5

    .line 237
    .local v2, "subHeadingStrings":[Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->activity:Lcom/google/android/videos/activity/MovieDetailsActivity;

    invoke-virtual {v5}, Lcom/google/android/videos/activity/MovieDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5, v10, v2}, Lcom/google/android/videos/utils/Util;->buildListString(Landroid/content/res/Resources;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 238
    .local v0, "newSubheading":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->subheading:Ljava/lang/String;

    invoke-static {v5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 239
    iput-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->subheading:Ljava/lang/String;

    .line 240
    const/4 v3, 0x1

    .line 244
    :cond_3
    iget-boolean v5, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->allowDownloads:Z

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->downloadStatus:Lcom/google/android/videos/store/VideoDownloadStatus;

    invoke-static {p2, v5}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 245
    iput-object p2, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->downloadStatus:Lcom/google/android/videos/store/VideoDownloadStatus;

    .line 246
    const/4 v3, 0x1

    .line 250
    :cond_4
    iget-object v5, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->storyline:Ljava/lang/String;

    iget-object v6, p1, Lcom/google/android/videos/store/VideoMetadata;->description:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 251
    iget-object v5, p1, Lcom/google/android/videos/store/VideoMetadata;->description:Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->storyline:Ljava/lang/String;

    .line 252
    const/4 v1, 0x1

    .line 256
    :cond_5
    if-eqz v3, :cond_6

    .line 257
    iget-object v5, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->subheadingDownloadFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    invoke-virtual {v5}, Lcom/google/android/videos/flow/SingleViewFlow;->notifyItemChanged()V

    .line 259
    :cond_6
    if-eqz v1, :cond_7

    .line 260
    iget-object v5, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->storylineFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    invoke-virtual {v5}, Lcom/google/android/videos/flow/SingleViewFlow;->notifyItemChanged()V

    .line 262
    :cond_7
    if-eqz v4, :cond_8

    .line 263
    iget-object v5, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->suggestionsFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    invoke-virtual {v5}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->notifyItemStatesChanged()V

    .line 265
    :cond_8
    return-void

    .line 219
    .end local v0    # "newSubheading":Ljava/lang/String;
    .end local v2    # "subHeadingStrings":[Ljava/lang/String;
    :cond_9
    const-string v5, ""

    goto/16 :goto_0

    :cond_a
    move-object v5, v6

    .line 228
    goto :goto_1
.end method

.method public onVideoDownloadStatus(Lcom/google/android/videos/store/VideoDownloadStatus;)V
    .locals 1
    .param p1, "downloadStatus"    # Lcom/google/android/videos/store/VideoDownloadStatus;

    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->allowDownloads:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->downloadStatus:Lcom/google/android/videos/store/VideoDownloadStatus;

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    iput-object p1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->downloadStatus:Lcom/google/android/videos/store/VideoDownloadStatus;

    .line 270
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->subheadingDownloadFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    invoke-virtual {v0}, Lcom/google/android/videos/flow/SingleViewFlow;->notifyItemChanged()V

    .line 272
    :cond_0
    return-void
.end method

.method public onVideoInfoChanged()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    iget-object v1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/remote/RemoteTracker;->isPlaying(Ljava/lang/String;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->setIsPlayingRemotely(Z)V

    .line 292
    return-void
.end method

.method public registerWithRemoteTracker()V
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/remote/RemoteTracker;->registerCustomTransportControl(Lcom/google/android/videos/remote/TransportControl;)V

    .line 282
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    iget-object v1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/remote/RemoteTracker;->isPlaying(Ljava/lang/String;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->setIsPlayingRemotely(Z)V

    .line 283
    return-void
.end method

.method public setNetworkConnected(Z)V
    .locals 1
    .param p1, "isConnected"    # Z

    .prologue
    .line 181
    iget-boolean v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->networkConnected:Z

    if-eq v0, p1, :cond_0

    .line 182
    iput-boolean p1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->networkConnected:Z

    .line 183
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->suggestionsFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->notifyItemStatesChanged()V

    .line 185
    :cond_0
    return-void
.end method

.method public unregisterWithRemoteTracker()V
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/remote/RemoteTracker;->unregisterCustomTransportControl(Lcom/google/android/videos/remote/TransportControl;)V

    .line 287
    return-void
.end method
