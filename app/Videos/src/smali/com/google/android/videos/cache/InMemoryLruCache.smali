.class public Lcom/google/android/videos/cache/InMemoryLruCache;
.super Ljava/lang/Object;
.source "InMemoryLruCache.java"

# interfaces
.implements Lcom/google/android/videos/cache/Cache;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/cache/Cache",
        "<TK;TE;>;"
    }
.end annotation


# instance fields
.field private final map:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<TK;TE;>;"
        }
    .end annotation
.end field

.field private final maxCacheSize:I


# direct methods
.method public constructor <init>(I)V
    .locals 5
    .param p1, "size"    # I

    .prologue
    .local p0, "this":Lcom/google/android/videos/cache/InMemoryLruCache;, "Lcom/google/android/videos/cache/InMemoryLruCache<TK;TE;>;"
    const/high16 v4, 0x3f400000    # 0.75f

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput p1, p0, Lcom/google/android/videos/cache/InMemoryLruCache;->maxCacheSize:I

    .line 41
    int-to-float v1, p1

    div-float/2addr v1, v4

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    add-int/lit8 v0, v1, 0x1

    .line 42
    .local v0, "mapSize":I
    new-instance v1, Lcom/google/android/videos/cache/InMemoryLruCache$1;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v0, v4, v2}, Lcom/google/android/videos/cache/InMemoryLruCache$1;-><init>(Lcom/google/android/videos/cache/InMemoryLruCache;IFZ)V

    iput-object v1, p0, Lcom/google/android/videos/cache/InMemoryLruCache;->map:Ljava/util/LinkedHashMap;

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/cache/InMemoryLruCache;)Ljava/util/LinkedHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cache/InMemoryLruCache;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/videos/cache/InMemoryLruCache;->map:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/cache/InMemoryLruCache;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/cache/InMemoryLruCache;

    .prologue
    .line 25
    iget v0, p0, Lcom/google/android/videos/cache/InMemoryLruCache;->maxCacheSize:I

    return v0
.end method

.method public static create(I)Lcom/google/android/videos/cache/InMemoryLruCache;
    .locals 1
    .param p0, "size"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "B:",
            "Ljava/lang/Object;",
            ">(I)",
            "Lcom/google/android/videos/cache/InMemoryLruCache",
            "<TA;TB;>;"
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lcom/google/android/videos/cache/InMemoryLruCache;

    invoke-direct {v0, p0}, Lcom/google/android/videos/cache/InMemoryLruCache;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public declared-synchronized get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TE;"
        }
    .end annotation

    .prologue
    .line 64
    .local p0, "this":Lcom/google/android/videos/cache/InMemoryLruCache;, "Lcom/google/android/videos/cache/InMemoryLruCache<TK;TE;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    iget-object v0, p0, Lcom/google/android/videos/cache/InMemoryLruCache;->map:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getValues()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 109
    .local p0, "this":Lcom/google/android/videos/cache/InMemoryLruCache;, "Lcom/google/android/videos/cache/InMemoryLruCache<TK;TE;>;"
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/videos/cache/InMemoryLruCache;->map:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized put(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TE;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p0, "this":Lcom/google/android/videos/cache/InMemoryLruCache;, "Lcom/google/android/videos/cache/InMemoryLruCache<TK;TE;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    .local p2, "element":Ljava/lang/Object;, "TE;"
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    iget-object v0, p0, Lcom/google/android/videos/cache/InMemoryLruCache;->map:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    monitor-exit p0

    return-void

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TE;"
        }
    .end annotation

    .prologue
    .line 80
    .local p0, "this":Lcom/google/android/videos/cache/InMemoryLruCache;, "Lcom/google/android/videos/cache/InMemoryLruCache<TK;TE;>;"
    .local p1, "key":Ljava/lang/Object;, "TK;"
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-object v0, p0, Lcom/google/android/videos/cache/InMemoryLruCache;->map:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized size()I
    .locals 1

    .prologue
    .line 96
    .local p0, "this":Lcom/google/android/videos/cache/InMemoryLruCache;, "Lcom/google/android/videos/cache/InMemoryLruCache<TK;TE;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/cache/InMemoryLruCache;->map:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    .local p0, "this":Lcom/google/android/videos/cache/InMemoryLruCache;, "Lcom/google/android/videos/cache/InMemoryLruCache<TK;TE;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/cache/InMemoryLruCache;->map:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
