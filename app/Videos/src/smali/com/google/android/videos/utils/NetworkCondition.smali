.class public Lcom/google/android/videos/utils/NetworkCondition;
.super Ljava/lang/Object;
.source "NetworkCondition.java"

# interfaces
.implements Lcom/google/android/repolib/common/Condition;


# instance fields
.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/utils/NetworkStatus;)V
    .locals 0
    .param p1, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/google/android/videos/utils/NetworkCondition;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 10
    return-void
.end method


# virtual methods
.method public applies()Z
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/videos/utils/NetworkCondition;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    return v0
.end method
