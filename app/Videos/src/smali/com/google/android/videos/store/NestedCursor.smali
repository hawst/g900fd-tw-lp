.class Lcom/google/android/videos/store/NestedCursor;
.super Landroid/database/AbstractCursor;
.source "NestedCursor.java"

# interfaces
.implements Lcom/google/android/videos/store/MasterSubCursor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/NestedCursor$SubCursorView;
    }
.end annotation


# instance fields
.field private currentIndex:I

.field private currentSubCursor:Landroid/database/Cursor;

.field private final masterCursor:Landroid/database/Cursor;

.field private final subCursorViews:[Lcom/google/android/videos/store/NestedCursor$SubCursorView;

.field private final subCursors:[Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;[Landroid/database/Cursor;)V
    .locals 5
    .param p1, "masterCursor"    # Landroid/database/Cursor;
    .param p2, "subCursors"    # [Landroid/database/Cursor;

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 28
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    iput-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->masterCursor:Landroid/database/Cursor;

    .line 29
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/database/Cursor;

    iput-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    .line 31
    array-length v1, p2

    .line 32
    .local v1, "length":I
    new-array v2, v1, [Lcom/google/android/videos/store/NestedCursor$SubCursorView;

    iput-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursorViews:[Lcom/google/android/videos/store/NestedCursor$SubCursorView;

    .line 33
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 34
    aget-object v2, p2, v0

    if-eqz v2, :cond_0

    .line 35
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursorViews:[Lcom/google/android/videos/store/NestedCursor$SubCursorView;

    new-instance v3, Lcom/google/android/videos/store/NestedCursor$SubCursorView;

    aget-object v4, p2, v0

    invoke-direct {v3, p0, v4}, Lcom/google/android/videos/store/NestedCursor$SubCursorView;-><init>(Lcom/google/android/videos/store/NestedCursor;Landroid/database/Cursor;)V

    aput-object v3, v2, v0

    .line 33
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 39
    :cond_1
    new-instance v2, Lcom/google/android/videos/store/NestedCursor$1;

    invoke-direct {v2, p0}, Lcom/google/android/videos/store/NestedCursor$1;-><init>(Lcom/google/android/videos/store/NestedCursor;)V

    invoke-virtual {p0, v2}, Lcom/google/android/videos/store/NestedCursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 50
    invoke-direct {p0}, Lcom/google/android/videos/store/NestedCursor;->resetPosition()V

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/store/NestedCursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/store/NestedCursor;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/videos/store/NestedCursor;->resetPosition()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/store/NestedCursor;)[Landroid/database/Cursor;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/NestedCursor;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/store/NestedCursor;)[Lcom/google/android/videos/store/NestedCursor$SubCursorView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/NestedCursor;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->subCursorViews:[Lcom/google/android/videos/store/NestedCursor$SubCursorView;

    return-object v0
.end method

.method private resetPosition()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 91
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->masterCursor:Landroid/database/Cursor;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->currentSubCursor:Landroid/database/Cursor;

    .line 93
    iput v1, p0, Lcom/google/android/videos/store/NestedCursor;->currentIndex:I

    .line 94
    return-void
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    .line 323
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->masterCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 324
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 325
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 326
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 327
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 325
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 330
    :cond_1
    invoke-super {p0}, Landroid/database/AbstractCursor;->close()V

    .line 331
    return-void
.end method

.method public deactivate()V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 311
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->masterCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->deactivate()V

    .line 312
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 313
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 314
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 315
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2}, Landroid/database/Cursor;->deactivate()V

    .line 313
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 318
    :cond_1
    invoke-super {p0}, Landroid/database/AbstractCursor;->deactivate()V

    .line 319
    return-void
.end method

.method public getBlob(I)[B
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->currentSubCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->currentSubCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->currentSubCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    .line 304
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getCount()I
    .locals 4

    .prologue
    .line 220
    const/4 v0, 0x0

    .line 221
    .local v0, "count":I
    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    array-length v2, v3

    .line 222
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 223
    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v3, v3, v1

    if-eqz v3, :cond_0

    .line 224
    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v3, v3, v1

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    add-int/2addr v0, v3

    .line 222
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 227
    :cond_1
    return v0
.end method

.method public getCurrentSubRange()Lcom/google/android/videos/store/MasterSubCursor;
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->currentSubCursor:Landroid/database/Cursor;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->subCursorViews:[Lcom/google/android/videos/store/NestedCursor$SubCursorView;

    iget v1, p0, Lcom/google/android/videos/store/NestedCursor;->currentIndex:I

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public getDouble(I)D
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->currentSubCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getFloat(I)F
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->currentSubCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public getInt(I)I
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->currentSubCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getLong(I)J
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->currentSubCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getMasterInt(I)I
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->masterCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getMasterLong(I)J
    .locals 2
    .param p1, "columnIndex"    # I

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->masterCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getMasterString(I)Ljava/lang/String;
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->masterCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNonEmptySubRangeCount()I
    .locals 4

    .prologue
    .line 55
    const/4 v0, 0x0

    .line 56
    .local v0, "count":I
    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    array-length v2, v3

    .line 57
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 58
    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v3, v3, v1

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v3, v3, v1

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-eqz v3, :cond_0

    .line 59
    add-int/lit8 v0, v0, 0x1

    .line 57
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 62
    :cond_1
    return v0
.end method

.method public getShort(I)S
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->currentSubCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    return v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->currentSubCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType(I)I
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->currentSubCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    return v0
.end method

.method public isNull(I)Z
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor;->currentSubCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public onMove(II)Z
    .locals 5
    .param p1, "oldPosition"    # I
    .param p2, "newPosition"    # I

    .prologue
    .line 232
    invoke-direct {p0}, Lcom/google/android/videos/store/NestedCursor;->resetPosition()V

    .line 233
    const/4 v0, 0x0

    .line 234
    .local v0, "cursorStartPos":I
    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    array-length v2, v3

    .line 235
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 236
    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v3, v3, v1

    if-nez v3, :cond_0

    .line 235
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 239
    :cond_0
    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v3, v3, v1

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    add-int/2addr v3, v0

    if-ge p2, v3, :cond_2

    .line 240
    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->masterCursor:Landroid/database/Cursor;

    invoke-interface {v3, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 241
    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v3, v3, v1

    iput-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->currentSubCursor:Landroid/database/Cursor;

    .line 242
    iput v1, p0, Lcom/google/android/videos/store/NestedCursor;->currentIndex:I

    .line 248
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->currentSubCursor:Landroid/database/Cursor;

    if-eqz v3, :cond_3

    .line 249
    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->currentSubCursor:Landroid/database/Cursor;

    sub-int v4, p2, v0

    invoke-interface {v3, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v3

    .line 251
    :goto_2
    return v3

    .line 245
    :cond_2
    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v3, v3, v1

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_1

    .line 251
    :cond_3
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 3
    .param p1, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 335
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->masterCursor:Landroid/database/Cursor;

    invoke-interface {v2, p1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 336
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 337
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 338
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 339
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 337
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 342
    :cond_1
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 3
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 357
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->masterCursor:Landroid/database/Cursor;

    invoke-interface {v2, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 358
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 359
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 360
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 361
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 359
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 364
    :cond_1
    return-void
.end method

.method public requery()Z
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 380
    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->masterCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->requery()Z

    move-result v3

    if-nez v3, :cond_1

    .line 389
    :cond_0
    :goto_0
    return v2

    .line 383
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    array-length v1, v3

    .line 384
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_3

    .line 385
    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v3, v3, v0

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v3, v3, v0

    invoke-interface {v3}, Landroid/database/Cursor;->requery()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 384
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 389
    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 3
    .param p1, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 346
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->masterCursor:Landroid/database/Cursor;

    invoke-interface {v2, p1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 347
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 348
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 349
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 350
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 348
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 353
    :cond_1
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 3
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 368
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->masterCursor:Landroid/database/Cursor;

    invoke-interface {v2, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 369
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 370
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 371
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 372
    iget-object v2, p0, Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 370
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 375
    :cond_1
    return-void
.end method
