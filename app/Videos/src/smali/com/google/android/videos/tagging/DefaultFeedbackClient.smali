.class public Lcom/google/android/videos/tagging/DefaultFeedbackClient;
.super Ljava/lang/Object;
.source "DefaultFeedbackClient.java"

# interfaces
.implements Lcom/google/android/videos/tagging/FeedbackClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitConverter;,
        Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;,
        Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;,
        Lcom/google/android/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;,
        Lcom/google/android/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;
    }
.end annotation


# instance fields
.field private final accountManager:Landroid/accounts/AccountManager;

.field private final sendFeedbackRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/Config;Ljava/lang/String;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "config"    # Lcom/google/android/videos/Config;
    .param p3, "applicationVersion"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient;->accountManager:Landroid/accounts/AccountManager;

    .line 56
    new-instance v0, Lorg/apache/http/impl/client/BasicCookieStore;

    invoke-direct {v0}, Lorg/apache/http/impl/client/BasicCookieStore;-><init>()V

    .line 60
    .local v0, "cookieStore":Lorg/apache/http/client/CookieStore;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Mozilla/5.0 (Linux; Android 4.1.1; Nexus 7 Build/JRO03D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Safari/535.19 com.google.android.videos/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 63
    .local v7, "userAgent":Ljava/lang/String;
    invoke-static {v7, v0}, Lcom/google/android/videos/utils/HttpClientFactory;->createCookieStoreHttpClient(Ljava/lang/String;Lorg/apache/http/client/CookieStore;)Lorg/apache/http/client/HttpClient;

    move-result-object v3

    .line 65
    .local v3, "httpClient":Lorg/apache/http/client/HttpClient;
    new-instance v5, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;

    invoke-interface {p2}, Lcom/google/android/videos/Config;->feedbackTokenUrl()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p2}, Lcom/google/android/videos/Config;->feedbackTokenExtractorRegex()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v8, v9}, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .local v5, "tokenConverter":Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackTokenConverter;
    invoke-static {v3, v5, v5}, Lcom/google/android/videos/async/HttpRequester;->create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;

    move-result-object v6

    .line 70
    .local v6, "tokenRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Lcom/google/android/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;>;"
    new-instance v4, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitConverter;

    invoke-interface {p2}, Lcom/google/android/videos/Config;->feedbackSubmitUrlTemplate()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitConverter;-><init>(Ljava/lang/String;)V

    .line 72
    .local v4, "submitConverter":Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitConverter;
    invoke-static {v3, v4, v4}, Lcom/google/android/videos/async/HttpRequester;->create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;

    move-result-object v2

    .line 76
    .local v2, "feedbackSubmitter":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;Ljava/lang/Void;>;"
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    .line 77
    .local v1, "executor":Ljava/util/concurrent/Executor;
    new-instance v8, Lcom/google/android/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;

    iget-object v9, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient;->accountManager:Landroid/accounts/AccountManager;

    invoke-direct {v8, v9, v0, v6, v2}, Lcom/google/android/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;-><init>(Landroid/accounts/AccountManager;Lorg/apache/http/client/CookieStore;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;)V

    invoke-static {v1, v8}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient;->sendFeedbackRequester:Lcom/google/android/videos/async/Requester;

    .line 79
    return-void
.end method


# virtual methods
.method public sendFeedback(Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;Lcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p1, "report"    # Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 83
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient;->sendFeedbackRequester:Lcom/google/android/videos/async/Requester;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 84
    return-void
.end method
