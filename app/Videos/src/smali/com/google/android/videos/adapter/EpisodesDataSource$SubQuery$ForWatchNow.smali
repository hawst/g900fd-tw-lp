.class public abstract Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery$ForWatchNow;
.super Ljava/lang/Object;
.source "EpisodesDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ForWatchNow"
.end annotation


# direct methods
.method public static createRequestForLastWatchedAndNextEpisode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "lastWatchedEpisodeId"    # Ljava/lang/String;
    .param p2, "nextEpisodeId"    # Ljava/lang/String;
    .param p3, "limit"    # I
    .param p4, "allowUnpurchased"    # Z
    .param p5, "downloadedOnly"    # Z

    .prologue
    const/4 v1, 0x0

    .line 886
    const/4 v0, 0x4

    new-array v6, v0, [Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v1

    const/4 v0, 0x1

    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v0

    const/4 v0, 0x2

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v0

    const/4 v0, 0x3

    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v0

    .line 892
    .local v6, "whereArgs":[Ljava/lang/String;
    if-eqz p4, :cond_1

    const-string v2, "assets, videos ON assets_type = 20 AND assets_id = video_id, seasons ON episode_season_id = season_id LEFT JOIN (purchased_assets, user_assets ON account = ? AND user_assets_account = account AND asset_type = 20 AND user_assets_type = 20 AND asset_id = user_assets_id AND purchase_status = 2 AND NOT (hidden IN (1, 3))) ON asset_id = assets_id"

    .line 893
    .local v2, "table":Ljava/lang/String;
    :goto_0
    if-eqz p4, :cond_2

    const-string v5, "(account IS NOT NULL AND video_id = ?) OR video_id = ?"

    .line 895
    .local v5, "where":Ljava/lang/String;
    :goto_1
    if-eqz p5, :cond_0

    .line 896
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " AND (pinned IS NOT NULL AND pinned > 0)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 898
    :cond_0
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    sget-object v3, Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "rating_id"

    const/4 v7, 0x0

    const-string v8, "season_seqno, episode_seqno"

    move v9, p3

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0

    .line 892
    .end local v2    # "table":Ljava/lang/String;
    .end local v5    # "where":Ljava/lang/String;
    :cond_1
    const-string v2, "assets, videos ON assets_type = 20 AND assets_id = video_id, seasons ON episode_season_id = season_id, purchased_assets ON asset_type = 20 AND asset_id = assets_id AND purchase_status = 2 AND NOT (hidden IN (1, 3)), user_assets ON user_assets_account = account AND user_assets_type = 20 AND user_assets_id = assets_id"

    goto :goto_0

    .line 893
    .restart local v2    # "table":Ljava/lang/String;
    :cond_2
    const-string v5, "account = ? AND video_id IN(?, ?)"

    goto :goto_1
.end method

.method public static createRequestForNextEpisodeOnly(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 12
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "nextEpisodeId"    # Ljava/lang/String;
    .param p2, "allowUnpurchased"    # Z
    .param p3, "downloadedOnly"    # Z

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 908
    const/4 v0, 0x3

    new-array v6, v0, [Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v9

    const/4 v0, 0x2

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v0

    .line 913
    .local v6, "whereArgs":[Ljava/lang/String;
    if-eqz p2, :cond_1

    const-string v2, "assets, videos ON assets_type = 20 AND assets_id = video_id, seasons ON episode_season_id = season_id LEFT JOIN (purchased_assets, user_assets ON account = ? AND user_assets_account = account AND asset_type = 20 AND user_assets_type = 20 AND asset_id = user_assets_id AND purchase_status = 2 AND NOT (hidden IN (1, 3))) ON asset_id = assets_id"

    .line 914
    .local v2, "table":Ljava/lang/String;
    :goto_0
    if-eqz p2, :cond_2

    const-string v5, "video_id = ?"

    .line 915
    .local v5, "where":Ljava/lang/String;
    :goto_1
    if-eqz p3, :cond_0

    .line 916
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " AND (pinned IS NOT NULL AND pinned > 0)"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 918
    :cond_0
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    sget-object v3, Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "rating_id"

    move-object v8, v7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0

    .line 913
    .end local v2    # "table":Ljava/lang/String;
    .end local v5    # "where":Ljava/lang/String;
    :cond_1
    const-string v2, "assets, videos ON assets_type = 20 AND assets_id = video_id, seasons ON episode_season_id = season_id, purchased_assets ON asset_type = 20 AND asset_id = assets_id AND purchase_status = 2 AND NOT (hidden IN (1, 3)), user_assets ON user_assets_account = account AND user_assets_type = 20 AND user_assets_id = assets_id"

    goto :goto_0

    .line 914
    .restart local v2    # "table":Ljava/lang/String;
    :cond_2
    const-string v5, "account = ? AND video_id = ?"

    goto :goto_1
.end method

.method public static createRequestStartingFromEpisodeSeqno(Ljava/lang/String;Ljava/lang/String;IIIZ)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "showId"    # Ljava/lang/String;
    .param p2, "seasonSeqno"    # I
    .param p3, "startEpisodeSeqno"    # I
    .param p4, "limit"    # I
    .param p5, "downloadedOnly"    # Z

    .prologue
    const/4 v1, 0x0

    .line 954
    const/4 v0, 0x5

    new-array v6, v0, [Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v1

    const/4 v0, 0x1

    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v0

    const/4 v0, 0x2

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v0

    const/4 v0, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v0

    const/4 v0, 0x4

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v0

    .line 961
    .local v6, "whereArgs":[Ljava/lang/String;
    const-string v5, "account = ? AND root_id = ? AND season_seqno = ? AND episode_seqno >= ?"

    .line 962
    .local v5, "where":Ljava/lang/String;
    if-eqz p5, :cond_0

    .line 963
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " AND (pinned IS NOT NULL AND pinned > 0)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 965
    :cond_0
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "assets, videos ON assets_type = 20 AND assets_id = video_id, seasons ON episode_season_id = season_id, purchased_assets ON asset_type = 20 AND asset_id = assets_id AND purchase_status = 2 AND NOT (hidden IN (1, 3)), user_assets ON user_assets_account = account AND user_assets_type = 20 AND user_assets_id = assets_id"

    sget-object v3, Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "rating_id"

    const/4 v7, 0x0

    const-string v8, "season_seqno, episode_seqno"

    move v9, p4

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createRequestStartingFromEpisodeSeqnoIncludingNextEpisode(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;IZ)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "showId"    # Ljava/lang/String;
    .param p2, "seasonSeqno"    # I
    .param p3, "startEpisodeSeqno"    # I
    .param p4, "nextEpisodeId"    # Ljava/lang/String;
    .param p5, "limit"    # I
    .param p6, "downloadedOnly"    # Z

    .prologue
    .line 930
    const/4 v0, 0x6

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x1

    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x2

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x4

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x5

    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v6, v0

    .line 938
    .local v6, "whereArgs":[Ljava/lang/String;
    const-string v5, "root_id = ? AND season_seqno = ? AND episode_seqno >= ? AND (account IS NOT NULL OR video_id = ?)"

    .line 939
    .local v5, "where":Ljava/lang/String;
    if-eqz p6, :cond_0

    .line 940
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND (pinned IS NOT NULL AND pinned > 0)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 942
    :cond_0
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v1, 0x0

    const-string v2, "assets, videos ON assets_type = 20 AND assets_id = video_id, seasons ON episode_season_id = season_id LEFT JOIN (purchased_assets, user_assets ON account = ? AND user_assets_account = account AND asset_type = 20 AND user_assets_type = 20 AND asset_id = user_assets_id AND purchase_status = 2 AND NOT (hidden IN (1, 3))) ON asset_id = assets_id"

    sget-object v3, Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "rating_id"

    const/4 v7, 0x0

    const-string v8, "season_seqno, episode_seqno"

    move v9, p5

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method
