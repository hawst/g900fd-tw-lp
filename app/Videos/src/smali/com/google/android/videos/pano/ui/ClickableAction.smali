.class public Lcom/google/android/videos/pano/ui/ClickableAction;
.super Landroid/support/v17/leanback/widget/Action;
.source "ClickableAction.java"


# instance fields
.field private final intent:Landroid/content/Intent;

.field private final requestCode:I


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "id"    # J
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "subtitle"    # Ljava/lang/String;

    .prologue
    .line 13
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/pano/ui/ClickableAction;-><init>(JLjava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    .line 14
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "subtitle"    # Ljava/lang/String;
    .param p5, "intent"    # Landroid/content/Intent;
    .param p6, "requestCode"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/support/v17/leanback/widget/Action;-><init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 22
    iput-object p5, p0, Lcom/google/android/videos/pano/ui/ClickableAction;->intent:Landroid/content/Intent;

    .line 23
    iput p6, p0, Lcom/google/android/videos/pano/ui/ClickableAction;->requestCode:I

    .line 24
    return-void
.end method


# virtual methods
.method public onClick(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ClickableAction;->intent:Landroid/content/Intent;

    if-nez v0, :cond_0

    .line 35
    :goto_0
    return-void

    .line 30
    :cond_0
    iget v0, p0, Lcom/google/android/videos/pano/ui/ClickableAction;->requestCode:I

    if-nez v0, :cond_1

    .line 31
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ClickableAction;->intent:Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 33
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ClickableAction;->intent:Landroid/content/Intent;

    iget v1, p0, Lcom/google/android/videos/pano/ui/ClickableAction;->requestCode:I

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
