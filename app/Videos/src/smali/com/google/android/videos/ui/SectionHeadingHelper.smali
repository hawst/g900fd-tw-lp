.class public Lcom/google/android/videos/ui/SectionHeadingHelper;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "SectionHeadingHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final actionView:Landroid/widget/TextView;

.field private final container:Landroid/widget/LinearLayout;

.field private final overlay:Landroid/view/View;

.field private final subheadingView:Landroid/widget/TextView;

.field private final titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 26
    const v0, 0x7f0f01f7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->container:Landroid/widget/LinearLayout;

    .line 27
    const v0, 0x7f0f00a8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->titleView:Landroid/widget/TextView;

    .line 28
    const v0, 0x7f0f017d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->subheadingView:Landroid/widget/TextView;

    .line 29
    const v0, 0x7f0f01f8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->actionView:Landroid/widget/TextView;

    .line 30
    const v0, 0x7f0f01f9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->overlay:Landroid/view/View;

    .line 31
    iget-object v0, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->actionView:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 32
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->actionView:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->performClick()Z

    .line 73
    :cond_0
    return-void
.end method

.method public setDimmed(Z)V
    .locals 2
    .param p1, "dimmed"    # Z

    .prologue
    .line 65
    iget-object v1, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->overlay:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 66
    return-void

    .line 65
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;ILjava/lang/Object;)V
    .locals 3
    .param p1, "onClickListener"    # Landroid/view/View$OnClickListener;
    .param p2, "tagKey"    # I
    .param p3, "tag"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    .line 51
    iget-object v2, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v2, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    if-eqz p1, :cond_1

    move v0, v1

    .line 53
    .local v0, "clickableFocusable":Z
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 54
    iget-object v2, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 55
    iget-object v1, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->actionView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setClickable(Z)V

    .line 56
    iget-object v1, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->actionView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 57
    if-eqz p3, :cond_2

    .line 58
    iget-object v1, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p2, p3}, Landroid/widget/LinearLayout;->setTag(ILjava/lang/Object;)V

    .line 62
    :cond_0
    :goto_1
    return-void

    .line 52
    .end local v0    # "clickableFocusable":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 59
    .restart local v0    # "clickableFocusable":Z
    :cond_2
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p2}, Landroid/widget/LinearLayout;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 60
    iget-object v1, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->container:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, Landroid/widget/LinearLayout;->setTag(ILjava/lang/Object;)V

    goto :goto_1
.end method

.method public setTexts(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "subheading"    # Ljava/lang/CharSequence;
    .param p3, "action"    # Ljava/lang/CharSequence;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 36
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->subheadingView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 42
    :goto_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43
    iget-object v0, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->actionView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 48
    :goto_1
    return-void

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->subheadingView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 40
    iget-object v0, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->subheadingView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 45
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->actionView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 46
    iget-object v0, p0, Lcom/google/android/videos/ui/SectionHeadingHelper;->actionView:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
