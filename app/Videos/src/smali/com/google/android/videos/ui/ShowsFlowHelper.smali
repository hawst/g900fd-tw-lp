.class public Lcom/google/android/videos/ui/ShowsFlowHelper;
.super Landroid/database/DataSetObserver;
.source "ShowsFlowHelper.java"

# interfaces
.implements Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;


# instance fields
.field private final downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

.field private final flow:Lcom/google/android/videos/flow/Flow;

.field private final noDownloadedContentFlow:Lcom/google/android/videos/flow/Flow;

.field private final showsDataSource:Lcom/google/android/videos/adapter/ShowsDataSource;

.field private final welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/utils/DownloadedOnlyManager;Lcom/google/android/videos/welcome/WelcomeFlow;Lcom/google/android/videos/adapter/ShowsDataSource;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;Lcom/google/android/videos/ui/playnext/ShowClusterItemView$Binder;)V
    .locals 9
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "downloadedOnlyManager"    # Lcom/google/android/videos/utils/DownloadedOnlyManager;
    .param p3, "welcomeFlow"    # Lcom/google/android/videos/welcome/WelcomeFlow;
    .param p4, "showsDataSource"    # Lcom/google/android/videos/adapter/ShowsDataSource;
    .param p5, "showClickListener"    # Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;
    .param p6, "showBinder"    # Lcom/google/android/videos/ui/playnext/ShowClusterItemView$Binder;

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x0

    .line 38
    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    .line 39
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/welcome/WelcomeFlow;

    iput-object v1, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    .line 41
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/adapter/ShowsDataSource;

    iput-object v1, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->showsDataSource:Lcom/google/android/videos/adapter/ShowsDataSource;

    .line 42
    iput-object p2, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .line 44
    new-instance v0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    const/4 v1, 0x0

    const v3, 0x7f040029

    const/4 v6, -0x1

    move-object v2, p4

    move-object v4, p6

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;-><init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;Lcom/google/android/videos/adapter/DataSource;ILcom/google/android/videos/ui/playnext/ClusterItemView$Binder;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;I)V

    .line 52
    .local v0, "showsFlow":Lcom/google/android/videos/flow/Flow;
    new-instance v1, Lcom/google/android/videos/ui/NoDownloadedContentFlow;

    invoke-static {p1}, Lcom/google/android/videos/activity/MyLibraryFragment;->getExtendedActionBarHeight(Landroid/content/Context;)I

    move-result v2

    invoke-direct {v1, v2}, Lcom/google/android/videos/ui/NoDownloadedContentFlow;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->noDownloadedContentFlow:Lcom/google/android/videos/flow/Flow;

    .line 55
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ShowsFlowHelper;->updateVisibilities()V

    .line 57
    new-instance v1, Lcom/google/android/videos/flow/SequentialFlow;

    const/4 v2, 0x4

    new-array v2, v2, [Lcom/google/android/videos/flow/Flow;

    iget-object v3, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->noDownloadedContentFlow:Lcom/google/android/videos/flow/Flow;

    aput-object v3, v2, v7

    const/4 v3, 0x1

    new-instance v4, Lcom/google/android/videos/ui/PlayListSpacerFlow;

    invoke-direct {v4, v7, v8}, Lcom/google/android/videos/ui/PlayListSpacerFlow;-><init>(II)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p3, v2, v3

    aput-object v0, v2, v8

    invoke-direct {v1, v2}, Lcom/google/android/videos/flow/SequentialFlow;-><init>([Lcom/google/android/videos/flow/Flow;)V

    iput-object v1, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->flow:Lcom/google/android/videos/flow/Flow;

    .line 62
    return-void
.end method


# virtual methods
.method public getFlow()Lcom/google/android/videos/flow/Flow;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->flow:Lcom/google/android/videos/flow/Flow;

    return-object v0
.end method

.method public onChanged()V
    .locals 0

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ShowsFlowHelper;->updateVisibilities()V

    .line 84
    return-void
.end method

.method public onDownloadedOnlyChanged(Z)V
    .locals 0
    .param p1, "downloadedOnly"    # Z

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ShowsFlowHelper;->updateVisibilities()V

    .line 99
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->showsDataSource:Lcom/google/android/videos/adapter/ShowsDataSource;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/adapter/ShowsDataSource;->registerObserver(Ljava/lang/Object;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    invoke-virtual {v0}, Lcom/google/android/videos/welcome/WelcomeFlow;->onStart()V

    .line 71
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->registerObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isDownloadedOnly()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/ShowsFlowHelper;->onDownloadedOnlyChanged(Z)V

    .line 73
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->showsDataSource:Lcom/google/android/videos/adapter/ShowsDataSource;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/adapter/ShowsDataSource;->unregisterObserver(Ljava/lang/Object;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    invoke-virtual {v0}, Lcom/google/android/videos/welcome/WelcomeFlow;->onStop()V

    .line 78
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->unregisterObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V

    .line 79
    return-void
.end method

.method public updateVisibilities()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 87
    iget-object v4, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->showsDataSource:Lcom/google/android/videos/adapter/ShowsDataSource;

    invoke-virtual {v4}, Lcom/google/android/videos/adapter/ShowsDataSource;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 88
    .local v1, "hasShows":Z
    :goto_0
    iget-object v4, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v4}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isDownloadedOnly()Z

    move-result v0

    .line 89
    .local v0, "downloadedOnly":Z
    iget-object v4, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    invoke-virtual {v4, v1}, Lcom/google/android/videos/welcome/WelcomeFlow;->setContentInVertical(Z)V

    .line 90
    iget-object v4, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    invoke-virtual {v4, v0}, Lcom/google/android/videos/welcome/WelcomeFlow;->setDownloadedOnly(Z)V

    .line 92
    iget-object v4, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->noDownloadedContentFlow:Lcom/google/android/videos/flow/Flow;

    if-eqz v0, :cond_1

    if-nez v1, :cond_1

    iget-object v5, p0, Lcom/google/android/videos/ui/ShowsFlowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    invoke-virtual {v5}, Lcom/google/android/videos/welcome/WelcomeFlow;->getCount()I

    move-result v5

    if-nez v5, :cond_1

    :goto_1
    invoke-virtual {v4, v2}, Lcom/google/android/videos/flow/Flow;->setVisible(Z)V

    .line 94
    return-void

    .end local v0    # "downloadedOnly":Z
    .end local v1    # "hasShows":Z
    :cond_0
    move v1, v3

    .line 87
    goto :goto_0

    .restart local v0    # "downloadedOnly":Z
    .restart local v1    # "hasShows":Z
    :cond_1
    move v2, v3

    .line 92
    goto :goto_1
.end method
