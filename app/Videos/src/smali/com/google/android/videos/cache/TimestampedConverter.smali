.class public Lcom/google/android/videos/cache/TimestampedConverter;
.super Ljava/lang/Object;
.source "TimestampedConverter.java"

# interfaces
.implements Lcom/google/android/videos/cache/Converter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/cache/Converter",
        "<",
        "Lcom/google/android/videos/async/Timestamped",
        "<TE;>;>;"
    }
.end annotation


# instance fields
.field private final wrappedElementConverter:Lcom/google/android/videos/cache/Converter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/cache/Converter",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/videos/cache/Converter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/cache/Converter",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "this":Lcom/google/android/videos/cache/TimestampedConverter;, "Lcom/google/android/videos/cache/TimestampedConverter<TE;>;"
    .local p1, "wrappedElementConverter":Lcom/google/android/videos/cache/Converter;, "Lcom/google/android/videos/cache/Converter<TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/cache/Converter;

    iput-object v0, p0, Lcom/google/android/videos/cache/TimestampedConverter;->wrappedElementConverter:Lcom/google/android/videos/cache/Converter;

    .line 26
    return-void
.end method

.method public static create(Lcom/google/android/videos/cache/Converter;)Lcom/google/android/videos/cache/TimestampedConverter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/cache/Converter",
            "<TE;>;)",
            "Lcom/google/android/videos/cache/TimestampedConverter",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 21
    .local p0, "wrappedElementConverter":Lcom/google/android/videos/cache/Converter;, "Lcom/google/android/videos/cache/Converter<TE;>;"
    new-instance v0, Lcom/google/android/videos/cache/TimestampedConverter;

    invoke-direct {v0, p0}, Lcom/google/android/videos/cache/TimestampedConverter;-><init>(Lcom/google/android/videos/cache/Converter;)V

    return-object v0
.end method


# virtual methods
.method public readElement(Ljava/io/InputStream;J)Lcom/google/android/videos/async/Timestamped;
    .locals 6
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "length"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "J)",
            "Lcom/google/android/videos/async/Timestamped",
            "<TE;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    .local p0, "this":Lcom/google/android/videos/cache/TimestampedConverter;, "Lcom/google/android/videos/cache/TimestampedConverter<TE;>;"
    invoke-static {p1}, Lcom/google/android/videos/cache/ConverterUtils;->readLong(Ljava/io/InputStream;)J

    move-result-wide v0

    .line 37
    .local v0, "timestamp":J
    iget-object v3, p0, Lcom/google/android/videos/cache/TimestampedConverter;->wrappedElementConverter:Lcom/google/android/videos/cache/Converter;

    const-wide/16 v4, 0x8

    sub-long v4, p2, v4

    invoke-interface {v3, p1, v4, v5}, Lcom/google/android/videos/cache/Converter;->readElement(Ljava/io/InputStream;J)Ljava/lang/Object;

    move-result-object v2

    .line 38
    .local v2, "wrappedElement":Ljava/lang/Object;, "TE;"
    new-instance v3, Lcom/google/android/videos/async/Timestamped;

    invoke-direct {v3, v2, v0, v1}, Lcom/google/android/videos/async/Timestamped;-><init>(Ljava/lang/Object;J)V

    return-object v3
.end method

.method public bridge synthetic readElement(Ljava/io/InputStream;J)Ljava/lang/Object;
    .locals 2
    .param p1, "x0"    # Ljava/io/InputStream;
    .param p2, "x1"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15
    .local p0, "this":Lcom/google/android/videos/cache/TimestampedConverter;, "Lcom/google/android/videos/cache/TimestampedConverter<TE;>;"
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/cache/TimestampedConverter;->readElement(Ljava/io/InputStream;J)Lcom/google/android/videos/async/Timestamped;

    move-result-object v0

    return-object v0
.end method

.method public writeElement(Lcom/google/android/videos/async/Timestamped;Ljava/io/OutputStream;)V
    .locals 2
    .param p2, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Timestamped",
            "<TE;>;",
            "Ljava/io/OutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "this":Lcom/google/android/videos/cache/TimestampedConverter;, "Lcom/google/android/videos/cache/TimestampedConverter<TE;>;"
    .local p1, "element":Lcom/google/android/videos/async/Timestamped;, "Lcom/google/android/videos/async/Timestamped<TE;>;"
    iget-wide v0, p1, Lcom/google/android/videos/async/Timestamped;->timestamp:J

    invoke-static {p2, v0, v1}, Lcom/google/android/videos/cache/ConverterUtils;->writeLong(Ljava/io/OutputStream;J)V

    .line 31
    iget-object v0, p0, Lcom/google/android/videos/cache/TimestampedConverter;->wrappedElementConverter:Lcom/google/android/videos/cache/Converter;

    iget-object v1, p1, Lcom/google/android/videos/async/Timestamped;->element:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/videos/cache/Converter;->writeElement(Ljava/lang/Object;Ljava/io/OutputStream;)V

    .line 32
    return-void
.end method

.method public bridge synthetic writeElement(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15
    .local p0, "this":Lcom/google/android/videos/cache/TimestampedConverter;, "Lcom/google/android/videos/cache/TimestampedConverter<TE;>;"
    check-cast p1, Lcom/google/android/videos/async/Timestamped;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/cache/TimestampedConverter;->writeElement(Lcom/google/android/videos/async/Timestamped;Ljava/io/OutputStream;)V

    return-void
.end method
