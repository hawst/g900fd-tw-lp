.class final Lcom/google/android/videos/bitmap/DefaultBitmapRequesters$5;
.super Lcom/google/android/videos/bitmap/BitmapCachingRequester;
.source "DefaultBitmapRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->createSyncBitmapRequester(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)Lcom/google/android/videos/async/Requester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/bitmap/BitmapCachingRequester",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)V
    .locals 0
    .param p2, "x1"    # Lcom/google/android/videos/bitmap/BitmapLruCache;

    .prologue
    .line 199
    .local p1, "x0":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/bitmap/BitmapCachingRequester;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)V

    return-void
.end method


# virtual methods
.method public toCacheKey(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".gb"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toCacheKey(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 199
    check-cast p1, Landroid/net/Uri;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters$5;->toCacheKey(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
