.class public Lcom/google/android/videos/store/PurchaseStore;
.super Ljava/lang/Object;
.source "PurchaseStore.java"

# interfaces
.implements Lcom/google/android/repolib/common/Factory;
.implements Lcom/google/android/videos/async/Requester;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnableV16;,
        Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;,
        Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;,
        Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Factory",
        "<",
        "Landroid/database/Cursor;",
        "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
        ">;",
        "Lcom/google/android/videos/async/Requester",
        "<",
        "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private allowUnratedContent:Z

.field private allowedContentRatingIds:[Ljava/lang/String;

.field private contentRestrictionsEnabled:Z

.field private final database:Lcom/google/android/videos/store/Database;

.field private final executor:Ljava/util/concurrent/Executor;

.field private inAllowedRatingIdsSql:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/videos/store/Database;)V
    .locals 1
    .param p1, "executor"    # Ljava/util/concurrent/Executor;
    .param p2, "database"    # Lcom/google/android/videos/store/Database;

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStore;->database:Lcom/google/android/videos/store/Database;

    .line 149
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStore;->executor:Ljava/util/concurrent/Executor;

    .line 150
    return-void
.end method

.method private getGetPurchasesRunnable(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;
    .locals 7
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;)",
            "Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;"
        }
    .end annotation

    .prologue
    .line 215
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    monitor-enter p0

    .line 221
    :try_start_0
    iget-boolean v4, p0, Lcom/google/android/videos/store/PurchaseStore;->contentRestrictionsEnabled:Z

    .line 222
    .local v4, "localContentRestrictionsEnabled":Z
    iget-boolean v5, p0, Lcom/google/android/videos/store/PurchaseStore;->allowUnratedContent:Z

    .line 223
    .local v5, "localAllowUnratedContent":Z
    iget-object v6, p0, Lcom/google/android/videos/store/PurchaseStore;->inAllowedRatingIdsSql:Ljava/lang/String;

    .line 224
    .local v6, "localInAllowedRatingIdsSql":Ljava/lang/String;
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 225
    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    .line 226
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;

    iget-object v1, p0, Lcom/google/android/videos/store/PurchaseStore;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;-><init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;ZZLjava/lang/String;)V

    .line 232
    .local v0, "task":Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;
    :goto_0
    return-object v0

    .line 224
    .end local v0    # "task":Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;
    .end local v4    # "localContentRestrictionsEnabled":Z
    .end local v5    # "localAllowUnratedContent":Z
    .end local v6    # "localInAllowedRatingIdsSql":Ljava/lang/String;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 229
    .restart local v4    # "localContentRestrictionsEnabled":Z
    .restart local v5    # "localAllowUnratedContent":Z
    .restart local v6    # "localInAllowedRatingIdsSql":Ljava/lang/String;
    :cond_0
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnableV16;

    iget-object v1, p0, Lcom/google/android/videos/store/PurchaseStore;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v1}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnableV16;-><init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;ZZLjava/lang/String;)V

    .restart local v0    # "task":Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;
    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized clearContentRestrictions()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 180
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/videos/store/PurchaseStore;->contentRestrictionsEnabled:Z

    if-eqz v1, :cond_0

    .line 181
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/store/PurchaseStore;->contentRestrictionsEnabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    const/4 v0, 0x1

    .line 184
    :cond_0
    monitor-exit p0

    return v0

    .line 180
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public createFrom(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Landroid/database/Cursor;
    .locals 1
    .param p1, "purchaseRequest"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .prologue
    .line 209
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/store/PurchaseStore;->getGetPurchasesRunnable(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createFrom(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 29
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/store/PurchaseStore;->createFrom(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getDatabase()Lcom/google/android/videos/store/Database;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStore;->database:Lcom/google/android/videos/store/Database;

    return-object v0
.end method

.method public getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 202
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/store/PurchaseStore;->getGetPurchasesRunnable(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;

    move-result-object v0

    .line 203
    .local v0, "task":Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;
    iget-object v1, p0, Lcom/google/android/videos/store/PurchaseStore;->executor:Ljava/util/concurrent/Executor;

    invoke-interface {v1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 204
    return-void
.end method

.method public request(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 193
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    .line 194
    return-void
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 29
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/PurchaseStore;->request(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method

.method public declared-synchronized setContentRestrictions([Ljava/lang/String;Z)Z
    .locals 5
    .param p1, "allowedContentRatingIds"    # [Ljava/lang/String;
    .param p2, "allowUnratedContent"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 154
    monitor-enter p0

    :try_start_0
    iget-boolean v4, p0, Lcom/google/android/videos/store/PurchaseStore;->contentRestrictionsEnabled:Z

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/videos/store/PurchaseStore;->allowUnratedContent:Z

    if-ne p2, v4, :cond_0

    iget-object v4, p0, Lcom/google/android/videos/store/PurchaseStore;->allowedContentRatingIds:[Ljava/lang/String;

    invoke-static {p1, v4}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_0

    .line 176
    :goto_0
    monitor-exit p0

    return v2

    .line 158
    :cond_0
    const/4 v2, 0x1

    :try_start_1
    iput-boolean v2, p0, Lcom/google/android/videos/store/PurchaseStore;->contentRestrictionsEnabled:Z

    .line 159
    iput-object p1, p0, Lcom/google/android/videos/store/PurchaseStore;->allowedContentRatingIds:[Ljava/lang/String;

    .line 160
    iput-boolean p2, p0, Lcom/google/android/videos/store/PurchaseStore;->allowUnratedContent:Z

    .line 162
    if-nez p1, :cond_1

    .line 163
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/videos/store/PurchaseStore;->inAllowedRatingIdsSql:Ljava/lang/String;

    :goto_1
    move v2, v3

    .line 176
    goto :goto_0

    .line 164
    :cond_1
    array-length v2, p1

    if-nez v2, :cond_2

    .line 165
    const-string v2, "IN ()"

    iput-object v2, p0, Lcom/google/android/videos/store/PurchaseStore;->inAllowedRatingIdsSql:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 154
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 167
    :cond_2
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "IN ("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 168
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-static {v0, v2}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 169
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_2
    array-length v2, p1

    if-ge v1, v2, :cond_3

    .line 170
    const/16 v2, 0x2c

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 171
    aget-object v2, p1, v1

    invoke-static {v0, v2}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 169
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 173
    :cond_3
    const/16 v2, 0x29

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 174
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/store/PurchaseStore;->inAllowedRatingIdsSql:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
