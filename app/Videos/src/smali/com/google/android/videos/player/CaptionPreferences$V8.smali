.class Lcom/google/android/videos/player/CaptionPreferences$V8;
.super Lcom/google/android/videos/player/CaptionPreferences;
.source "CaptionPreferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/CaptionPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V8"
.end annotation


# instance fields
.field private final assetManager:Landroid/content/res/AssetManager;

.field private final preferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/videos/player/CaptionPreferences;-><init>()V

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/CaptionPreferences$V8;->assetManager:Landroid/content/res/AssetManager;

    .line 53
    iput-object p2, p0, Lcom/google/android/videos/player/CaptionPreferences$V8;->preferences:Landroid/content/SharedPreferences;

    .line 54
    return-void
.end method


# virtual methods
.method public getCaptionStyle()Lcom/google/android/exoplayer/text/CaptionStyleCompat;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/videos/player/CaptionPreferences$V8;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/google/android/videos/player/CaptionPreferences$V8;->assetManager:Landroid/content/res/AssetManager;

    invoke-static {v0, v1}, Lcom/google/android/videos/subtitles/CaptionStyleUtil;->createCaptionStyleFromPreferences(Landroid/content/SharedPreferences;Landroid/content/res/AssetManager;)Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    move-result-object v0

    return-object v0
.end method

.method public getFontScale()F
    .locals 4

    .prologue
    .line 78
    iget-object v1, p0, Lcom/google/android/videos/player/CaptionPreferences$V8;->preferences:Landroid/content/SharedPreferences;

    const-string v2, "captioning_font_scale"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "rawScale":Ljava/lang/String;
    if-nez v0, :cond_0

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_0
    return v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    goto :goto_0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 63
    iget-object v3, p0, Lcom/google/android/videos/player/CaptionPreferences$V8;->preferences:Landroid/content/SharedPreferences;

    const-string v4, "captioning_locale"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "rawLocale":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 65
    const-string v3, "_"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 66
    .local v1, "splitLocale":[Ljava/lang/String;
    array-length v3, v1

    packed-switch v3, :pswitch_data_0

    .line 73
    .end local v1    # "splitLocale":[Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .line 67
    .restart local v1    # "splitLocale":[Ljava/lang/String;
    :pswitch_0
    new-instance v2, Ljava/util/Locale;

    aget-object v3, v1, v5

    invoke-direct {v2, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :pswitch_1
    new-instance v2, Ljava/util/Locale;

    aget-object v3, v1, v5

    aget-object v4, v1, v6

    invoke-direct {v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 69
    :pswitch_2
    new-instance v2, Ljava/util/Locale;

    aget-object v3, v1, v5

    aget-object v4, v1, v6

    const/4 v5, 0x2

    aget-object v5, v1, v5

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 66
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public isEnabled()Z
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/videos/player/CaptionPreferences$V8;->preferences:Landroid/content/SharedPreferences;

    const-string v1, "captioning_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
