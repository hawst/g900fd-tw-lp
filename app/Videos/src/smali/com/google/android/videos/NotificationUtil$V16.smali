.class Lcom/google/android/videos/NotificationUtil$V16;
.super Lcom/google/android/videos/NotificationUtil$V14;
.source "NotificationUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/NotificationUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V16"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/google/android/videos/NotificationUtil$V14;-><init>(Landroid/content/Context;)V

    .line 129
    return-void
.end method


# virtual methods
.method protected build(Landroid/app/Notification$Builder;)Landroid/app/Notification;
    .locals 1
    .param p1, "builder"    # Landroid/app/Notification$Builder;

    .prologue
    .line 156
    invoke-virtual {p1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method protected createBuilder(I)Landroid/app/Notification$Builder;
    .locals 2
    .param p1, "defaults"    # I

    .prologue
    .line 135
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x11

    if-ne v0, v1, :cond_0

    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    const-string v1, "/JOP40C/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    and-int/lit8 p1, p1, -0x2

    .line 138
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/videos/NotificationUtil$V14;->createBuilder(I)Landroid/app/Notification$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected setNotificationBigStyle(Landroid/app/Notification$Builder;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Ljava/lang/String;)V
    .locals 5
    .param p1, "builder"    # Landroid/app/Notification$Builder;
    .param p2, "playIntent"    # Landroid/app/PendingIntent;
    .param p3, "pinIntent"    # Landroid/app/PendingIntent;
    .param p4, "episodeNumber"    # Ljava/lang/String;

    .prologue
    .line 144
    new-instance v0, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v0}, Landroid/app/Notification$BigTextStyle;-><init>()V

    .line 145
    .local v0, "bigStyle":Landroid/app/Notification$BigTextStyle;
    iget-object v1, p0, Lcom/google/android/videos/NotificationUtil$V16;->context:Landroid/content/Context;

    const v2, 0x7f0b01ab

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p4, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    .line 148
    invoke-virtual {p1, v0}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    move-result-object v1

    const v2, 0x7f0200dd

    iget-object v3, p0, Lcom/google/android/videos/NotificationUtil$V16;->context:Landroid/content/Context;

    const v4, 0x7f0b0116

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, p2}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v1

    const v2, 0x7f0200ab

    iget-object v3, p0, Lcom/google/android/videos/NotificationUtil$V16;->context:Landroid/content/Context;

    const v4, 0x7f0b011e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, p3}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 152
    return-void
.end method
