.class public Lcom/google/android/videos/activity/WatchActivity;
.super Landroid/support/v7/app/ActionBarActivity;
.source "WatchActivity.java"

# interfaces
.implements Lcom/google/android/videos/player/overlay/TrackChangeListener;
.implements Lcom/google/android/videos/remote/MediaRouteManager$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/activity/WatchActivity$1;,
        Lcom/google/android/videos/activity/WatchActivity$TouchAwareControllerOverlay;
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

.field private config:Lcom/google/android/videos/Config;

.field private controllerOverlay:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

.field private currentRemote:Lcom/google/android/videos/remote/RemoteControl;

.field private director:Lcom/google/android/videos/player/Director;

.field private directorInitialized:Z

.field private dragPromoOverlay:Lcom/google/android/videos/player/overlay/DragPromoOverlay;

.field private gotDirectorActivityResult:Z

.field private hasBeenStopped:Z

.field private interactiveKnowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

.field private isTrailer:Z

.field private keyguardManager:Landroid/app/KeyguardManager;

.field private mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;

.field private mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

.field private orientation:I

.field private playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

.field private playerView:Lcom/google/android/videos/player/PlayerView;

.field private remoteHelper:Lcom/google/android/videos/remote/RemoteHelper;

.field private remoteScreenPanel:Landroid/view/View;

.field private seasonId:Ljava/lang/String;

.field private showId:Ljava/lang/String;

.field private subtitlesOverlay:Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;

.field private videoId:Ljava/lang/String;

.field private videosGlobals:Lcom/google/android/videos/VideosGlobals;

.field private watchActivityCompat:Lcom/google/android/videos/activity/WatchActivityCompat;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/support/v7/app/ActionBarActivity;-><init>()V

    .line 434
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/activity/WatchActivity;)Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/WatchActivity;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->interactiveKnowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    return-object v0
.end method

.method private createDirector()V
    .locals 12

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 181
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/videos/activity/WatchActivityCompat;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/WatchActivityCompat;->getPresentationDisplayRoute()Landroid/media/MediaRouter$RouteInfo;

    move-result-object v11

    .line 182
    .local v11, "routeInfo":Landroid/media/MediaRouter$RouteInfo;
    new-instance v0, Lcom/google/android/videos/player/Director;

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    iget-object v3, p0, Lcom/google/android/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/videos/activity/WatchActivityCompat;

    iget-object v4, p0, Lcom/google/android/videos/activity/WatchActivity;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    iget-object v5, p0, Lcom/google/android/videos/activity/WatchActivity;->videoId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/activity/WatchActivity;->seasonId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/videos/activity/WatchActivity;->showId:Ljava/lang/String;

    iget-boolean v8, p0, Lcom/google/android/videos/activity/WatchActivity;->isTrailer:Z

    iget-object v9, p0, Lcom/google/android/videos/activity/WatchActivity;->account:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivity;->isConnectedToRemote()Z

    move-result v10

    move-object v2, p0

    invoke-direct/range {v0 .. v11}, Lcom/google/android/videos/player/Director;-><init>(Lcom/google/android/videos/VideosGlobals;Landroid/support/v4/app/FragmentActivity;Lcom/google/android/videos/player/Director$Listener;Lcom/google/android/videos/player/PlaybackResumeState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLandroid/media/MediaRouter$RouteInfo;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    .line 185
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->playerView:Lcom/google/android/videos/player/PlayerView;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->playerView:Lcom/google/android/videos/player/PlayerView;

    iget-object v2, p0, Lcom/google/android/videos/activity/WatchActivity;->controllerOverlay:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    iget-object v3, p0, Lcom/google/android/videos/activity/WatchActivity;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;

    iget-object v4, p0, Lcom/google/android/videos/activity/WatchActivity;->remoteScreenPanel:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/videos/activity/WatchActivity;->interactiveKnowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    iget-object v6, p0, Lcom/google/android/videos/activity/WatchActivity;->interactiveKnowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/videos/activity/WatchActivity;->dragPromoOverlay:Lcom/google/android/videos/player/overlay/DragPromoOverlay;

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/videos/player/Director;->setViewsAndOverlays(Lcom/google/android/videos/player/PlayerView;Lcom/google/android/videos/player/overlay/ControllerOverlay;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;Landroid/view/View;Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;Lcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/player/overlay/DragPromoOverlay;)V

    .line 190
    :cond_0
    return-void

    .line 180
    .end local v11    # "routeInfo":Landroid/media/MediaRouter$RouteInfo;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;
    .param p5, "isTrailer"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 62
    if-nez p5, :cond_0

    if-eqz p1, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 63
    if-nez p4, :cond_1

    if-nez p3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    const-string v0, "ShowId cannot be null when seasonId is not null"

    invoke-static {v1, v0}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 65
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/google/android/videos/activity/WatchActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "video_id"

    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "season_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_trailer"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v1

    .line 62
    goto :goto_0
.end method

.method private initDirector(ZZ)V
    .locals 2
    .param p1, "firstResume"    # Z
    .param p2, "toggled"    # Z

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivity;->isConnectedToRemote()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 231
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/videos/player/Director;->initPlayback(Z)V

    .line 235
    :goto_1
    return-void

    .line 231
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 233
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/videos/activity/WatchActivityCompat;

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/videos/activity/WatchActivityCompat;->initPlayback(Lcom/google/android/videos/player/Director;ZZ)V

    goto :goto_1
.end method

.method private isRemotePlaybackFinished()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 397
    iget-object v2, p0, Lcom/google/android/videos/activity/WatchActivity;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    invoke-virtual {v2}, Lcom/google/android/videos/remote/RemoteControl;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v0

    .line 398
    .local v0, "playerState":Lcom/google/android/videos/remote/PlayerState;
    if-nez v0, :cond_1

    .line 401
    :cond_0
    :goto_0
    return v1

    :cond_1
    iget v2, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    if-eqz v2, :cond_2

    iget v2, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private maybeResetDirector()V
    .locals 1

    .prologue
    .line 260
    iget-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivity;->directorInitialized:Z

    if-eqz v0, :cond_0

    .line 261
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivity;->directorInitialized:Z

    .line 262
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivity;->hasBeenStopped:Z

    .line 263
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0}, Lcom/google/android/videos/player/Director;->reset()V

    .line 265
    :cond_0
    return-void
.end method


# virtual methods
.method isConnectedToRemote()Z
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    :goto_0
    return-void

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/videos/player/Director;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 293
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivity;->gotDirectorActivityResult:Z

    goto :goto_0

    .line 296
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/ActionBarActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0}, Lcom/google/android/videos/player/Director;->onBackPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 358
    invoke-static {p0}, Lcom/google/android/videos/utils/LockTaskModeCompat;->isInLockTaskMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 359
    invoke-static {p0}, Lcom/google/android/videos/utils/LockTaskModeCompat;->showLockTaskModeToast(Landroid/content/Context;)V

    .line 364
    :cond_0
    :goto_0
    return-void

    .line 361
    :cond_1
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 368
    iget v0, p0, Lcom/google/android/videos/activity/WatchActivity;->orientation:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_0

    .line 369
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/google/android/videos/activity/WatchActivity;->orientation:I

    .line 370
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0}, Lcom/google/android/videos/player/Director;->onOrientationChanged()V

    .line 372
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 373
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 105
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 108
    invoke-static {p0}, Lcom/google/android/videos/activity/WatchActivityCompat;->create(Lcom/google/android/videos/activity/WatchActivity;)Lcom/google/android/videos/activity/WatchActivityCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/videos/activity/WatchActivityCompat;

    .line 109
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 111
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    .line 112
    .local v13, "intent":Landroid/content/Intent;
    const-string v0, "authAccount"

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->account:Ljava/lang/String;

    .line 113
    const-string v0, "video_id"

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->videoId:Ljava/lang/String;

    .line 114
    const-string v0, "show_id"

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->showId:Ljava/lang/String;

    .line 115
    const-string v0, "season_id"

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->seasonId:Ljava/lang/String;

    .line 116
    const-string v0, "is_trailer"

    const/4 v1, 0x0

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivity;->isTrailer:Z

    .line 118
    if-eqz p1, :cond_2

    .line 119
    new-instance v0, Lcom/google/android/videos/player/PlaybackResumeState;

    const-string v1, "playback_resume_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/player/PlaybackResumeState;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    .line 125
    :goto_0
    if-eqz p1, :cond_3

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivity;->hasBeenStopped:Z

    .line 127
    iget-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivity;->isTrailer:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->account:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->videoId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->showId:Ljava/lang/String;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->seasonId:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 129
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "invalid arguments format: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->account:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->seasonId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->showId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/videos/activity/WatchActivity;->isTrailer:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 131
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivity;->finish()V

    .line 177
    :goto_2
    return-void

    .line 122
    :cond_2
    new-instance v0, Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-direct {v0}, Lcom/google/android/videos/player/PlaybackResumeState;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    goto :goto_0

    .line 125
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 135
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->account:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getMediaRouteManager()Lcom/google/android/videos/remote/MediaRouteManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;

    .line 138
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/remote/MediaRouteManager;->register(Lcom/google/android/videos/remote/MediaRouteManager$Listener;)V

    .line 139
    invoke-direct {p0}, Lcom/google/android/videos/activity/WatchActivity;->createDirector()V

    .line 141
    const v0, 0x7f0400c7

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/WatchActivity;->setContentView(I)V

    .line 142
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/videos/activity/WatchActivityCompat;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/WatchActivityCompat;->onCreate()V

    .line 143
    const v0, 0x7f0f020f

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/PlayerView;

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->playerView:Lcom/google/android/videos/player/PlayerView;

    .line 145
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400b6

    iget-object v2, p0, Lcom/google/android/videos/activity/WatchActivity;->playerView:Lcom/google/android/videos/player/PlayerView;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->remoteScreenPanel:Landroid/view/View;

    .line 147
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->playerView:Lcom/google/android/videos/player/PlayerView;

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->remoteScreenPanel:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/PlayerView;->addView(Landroid/view/View;)V

    .line 149
    new-instance v0, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;

    invoke-direct {v0, p0}, Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;

    .line 150
    new-instance v0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->dragPromoOverlay:Lcom/google/android/videos/player/overlay/DragPromoOverlay;

    .line 151
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->playerView:Lcom/google/android/videos/player/PlayerView;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/videos/player/PlayerView$PlayerOverlay;

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/videos/activity/WatchActivity;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;

    aput-object v4, v1, v2

    const/4 v2, 0x1

    iget-object v4, p0, Lcom/google/android/videos/activity/WatchActivity;->dragPromoOverlay:Lcom/google/android/videos/player/overlay/DragPromoOverlay;

    aput-object v4, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/PlayerView;->addOverlays([Lcom/google/android/videos/player/PlayerView$PlayerOverlay;)V

    .line 152
    new-instance v3, Lcom/google/android/videos/player/overlay/StoryboardHelper;

    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getStoryboardClient()Lcom/google/android/videos/store/StoryboardClient;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->videoId:Ljava/lang/String;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/videos/player/overlay/StoryboardHelper;-><init>(Landroid/content/Context;Lcom/google/android/videos/store/StoryboardClient;Ljava/lang/String;)V

    .line 155
    .local v3, "storyboardHelper":Lcom/google/android/videos/player/overlay/StoryboardHelper;
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0}, Lcom/google/android/videos/player/Director;->isKnowledgeEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 156
    new-instance v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    invoke-direct {v0, p0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->interactiveKnowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    .line 157
    new-instance v0, Lcom/google/android/videos/activity/WatchActivity$TouchAwareControllerOverlay;

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v4

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/activity/WatchActivity$TouchAwareControllerOverlay;-><init>(Lcom/google/android/videos/activity/WatchActivity;Landroid/support/v4/app/FragmentActivity;Lcom/google/android/videos/player/overlay/StoryboardHelper;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/activity/WatchActivity$1;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->controllerOverlay:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    .line 159
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->interactiveKnowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->controllerOverlay:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->setFallbackGestureListener(Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->playerView:Lcom/google/android/videos/player/PlayerView;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/videos/player/PlayerView$PlayerOverlay;

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/videos/activity/WatchActivity;->interactiveKnowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    aput-object v4, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/PlayerView;->addOverlays([Lcom/google/android/videos/player/PlayerView$PlayerOverlay;)V

    .line 165
    :goto_3
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->playerView:Lcom/google/android/videos/player/PlayerView;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/videos/player/PlayerView$PlayerOverlay;

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/videos/activity/WatchActivity;->controllerOverlay:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    aput-object v4, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/PlayerView;->addOverlays([Lcom/google/android/videos/player/PlayerView$PlayerOverlay;)V

    .line 167
    iget-object v4, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    iget-object v5, p0, Lcom/google/android/videos/activity/WatchActivity;->playerView:Lcom/google/android/videos/player/PlayerView;

    iget-object v6, p0, Lcom/google/android/videos/activity/WatchActivity;->controllerOverlay:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    iget-object v7, p0, Lcom/google/android/videos/activity/WatchActivity;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/DefaultSubtitlesOverlay;

    iget-object v8, p0, Lcom/google/android/videos/activity/WatchActivity;->remoteScreenPanel:Landroid/view/View;

    iget-object v9, p0, Lcom/google/android/videos/activity/WatchActivity;->interactiveKnowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    iget-object v10, p0, Lcom/google/android/videos/activity/WatchActivity;->interactiveKnowledgeOverlay:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/videos/activity/WatchActivity;->dragPromoOverlay:Lcom/google/android/videos/player/overlay/DragPromoOverlay;

    invoke-virtual/range {v4 .. v12}, Lcom/google/android/videos/player/Director;->setViewsAndOverlays(Lcom/google/android/videos/player/PlayerView;Lcom/google/android/videos/player/overlay/ControllerOverlay;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;Landroid/view/View;Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;Lcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/player/overlay/DragPromoOverlay;)V

    .line 170
    const-string v0, "keyguard"

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/WatchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->keyguardManager:Landroid/app/KeyguardManager;

    .line 171
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->config:Lcom/google/android/videos/Config;

    .line 172
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/MediaRouteManager;->getRouteSelector()Landroid/support/v7/media/MediaRouteSelector;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/videos/remote/MediaRouteProviderCompat;->create(Landroid/support/v7/app/ActionBarActivity;Landroid/support/v7/media/MediaRouteSelector;)Lcom/google/android/videos/remote/MediaRouteProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    .line 174
    new-instance v0, Lcom/google/android/videos/remote/RemoteHelper;

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;

    invoke-direct {v0, v1}, Lcom/google/android/videos/remote/RemoteHelper;-><init>(Lcom/google/android/videos/remote/MediaRouteManager;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->remoteHelper:Lcom/google/android/videos/remote/RemoteHelper;

    .line 175
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 176
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/google/android/videos/activity/WatchActivity;->orientation:I

    goto/16 :goto_2

    .line 162
    :cond_5
    new-instance v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v1

    invoke-direct {v0, p0, v3, v1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/google/android/videos/player/overlay/StoryboardHelper;Lcom/google/android/videos/utils/NetworkStatus;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->controllerOverlay:Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    goto :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 301
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 302
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 303
    .local v0, "inflater":Landroid/view/MenuInflater;
    iget-boolean v1, p0, Lcom/google/android/videos/activity/WatchActivity;->isTrailer:Z

    if-nez v1, :cond_0

    .line 304
    invoke-static {p1, v0}, Lcom/google/android/videos/VideosApplication;->createCommonMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 305
    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivity;->isConnectedToRemote()Z

    move-result v1

    if-nez v1, :cond_0

    .line 306
    const v1, 0x7f140002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 309
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    if-eqz v1, :cond_1

    .line 310
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    invoke-interface {v1, p1, v0}, Lcom/google/android/videos/remote/MediaRouteProvider;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 312
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/videos/activity/WatchActivityCompat;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/videos/activity/WatchActivityCompat;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 313
    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 269
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;

    invoke-virtual {v1, p0}, Lcom/google/android/videos/remote/MediaRouteManager;->unregister(Lcom/google/android/videos/remote/MediaRouteManager$Listener;)V

    .line 270
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    if-eqz v1, :cond_0

    .line 271
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v1}, Lcom/google/android/videos/player/Director;->reset()V

    .line 273
    :cond_0
    const v1, 0x7f0f020f

    invoke-virtual {p0, v1}, Lcom/google/android/videos/activity/WatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/PlayerView;

    .line 274
    .local v0, "playerView":Lcom/google/android/videos/player/PlayerView;
    if-eqz v0, :cond_1

    .line 275
    invoke-virtual {v0}, Lcom/google/android/videos/player/PlayerView;->getPlayerSurface()Lcom/google/android/videos/player/PlayerSurface;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/videos/player/PlayerSurface;->release()V

    .line 277
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/videos/activity/WatchActivityCompat;

    if-eqz v1, :cond_2

    .line 278
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/videos/activity/WatchActivityCompat;

    invoke-virtual {v1}, Lcom/google/android/videos/activity/WatchActivityCompat;->onDestroy()V

    .line 280
    :cond_2
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onDestroy()V

    .line 281
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/player/Director;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->remoteHelper:Lcom/google/android/videos/remote/RemoteHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/remote/RemoteHelper;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/Util;->handlesMenuKeyEvent(ILandroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/support/v7/app/ActionBarActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/player/Director;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->remoteHelper:Lcom/google/android/videos/remote/RemoteHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/remote/RemoteHelper;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1, p0}, Lcom/google/android/videos/utils/Util;->handlesMenuKeyEvent(ILandroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/support/v7/app/ActionBarActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 327
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0f0221

    if-ne v1, v2, :cond_1

    .line 328
    const-string v1, "mobile_movie_player"

    invoke-static {p0, v1}, Lcom/google/android/videos/ui/HelpHelper;->startContextualHelp(Landroid/app/Activity;Ljava/lang/String;)V

    .line 334
    :cond_0
    :goto_0
    return v0

    .line 330
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0f0223

    if-ne v1, v2, :cond_2

    .line 331
    invoke-static {p0}, Lcom/google/android/videos/utils/LockTaskModeCompat;->startLockTaskV21(Landroid/app/Activity;)V

    goto :goto_0

    .line 334
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/videos/activity/WatchActivityCompat;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/activity/WatchActivityCompat;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1, p0}, Lcom/google/android/videos/VideosApplication;->onCommonOptionsItemSelected(Landroid/view/MenuItem;Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0}, Lcom/google/android/videos/player/Director;->showingChildActivity()Z

    move-result v0

    if-nez v0, :cond_0

    .line 240
    invoke-direct {p0}, Lcom/google/android/videos/activity/WatchActivity;->maybeResetDirector()V

    .line 242
    :cond_0
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onPause()V

    .line 243
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 318
    invoke-static {p0}, Lcom/google/android/videos/utils/LockTaskModeCompat;->isInLockTaskMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    const v0, 0x7f0f0223

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 320
    const/4 v0, 0x0

    .line 322
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method onPresentationDisplayRouteChanged()V
    .locals 2

    .prologue
    .line 380
    iget-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivity;->directorInitialized:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivity;->isConnectedToRemote()Z

    move-result v0

    if-nez v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0}, Lcom/google/android/videos/player/Director;->reset()V

    .line 382
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    .line 383
    invoke-direct {p0}, Lcom/google/android/videos/activity/WatchActivity;->createDirector()V

    .line 384
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/activity/WatchActivity;->initDirector(ZZ)V

    .line 386
    :cond_0
    return-void
.end method

.method public onRemoteControlSelected(Lcom/google/android/videos/remote/RemoteControl;)V
    .locals 2
    .param p1, "remoteControl"    # Lcom/google/android/videos/remote/RemoteControl;

    .prologue
    .line 406
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    if-eq v0, p1, :cond_0

    .line 407
    iput-object p1, p0, Lcom/google/android/videos/activity/WatchActivity;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    .line 408
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/videos/activity/WatchActivityCompat;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/WatchActivityCompat;->onRemoteControlChanged()V

    .line 409
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->currentRemote:Lcom/google/android/videos/remote/RemoteControl;

    if-nez v0, :cond_1

    .line 410
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivity;->finish()V

    .line 422
    :cond_0
    :goto_0
    return-void

    .line 411
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivity;->directorInitialized:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->keyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0}, Lcom/google/android/videos/player/Director;->reset()V

    .line 413
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    .line 416
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v0}, Lcom/google/android/videos/player/PlaybackResumeState;->clearPlaybackError()V

    .line 418
    invoke-direct {p0}, Lcom/google/android/videos/activity/WatchActivity;->createDirector()V

    .line 419
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/activity/WatchActivity;->initDirector(ZZ)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 207
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onResume()V

    .line 208
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->refreshContentRestrictions()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivity;->finish()V

    .line 227
    :goto_0
    return-void

    .line 212
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivity;->hasBeenStopped:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivity;->isConnectedToRemote()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/videos/activity/WatchActivity;->isRemotePlaybackFinished()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivity;->finish()V

    goto :goto_0

    .line 216
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivity;->isTrailer:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/videos/activity/WatchActivity;->account:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_2

    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "account does not exist: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->account:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 218
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivity;->finish()V

    goto :goto_0

    .line 221
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivity;->gotDirectorActivityResult:Z

    if-nez v0, :cond_3

    .line 222
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/videos/activity/WatchActivity;->videoId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/activity/WatchActivity;->seasonId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/videos/activity/WatchActivity;->showId:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/videos/activity/WatchActivity;->isTrailer:Z

    invoke-interface {v0, v3, v4, v5, v6}, Lcom/google/android/videos/logging/EventLogger;->onPremiumWatchPageOpened(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 223
    iget-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivity;->hasBeenStopped:Z

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    invoke-direct {p0, v0, v2}, Lcom/google/android/videos/activity/WatchActivity;->initDirector(ZZ)V

    .line 224
    iput-boolean v1, p0, Lcom/google/android/videos/activity/WatchActivity;->directorInitialized:Z

    .line 226
    :cond_3
    iput-boolean v2, p0, Lcom/google/android/videos/activity/WatchActivity;->gotDirectorActivityResult:Z

    goto :goto_0

    :cond_4
    move v0, v2

    .line 223
    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 247
    invoke-super {p0, p1}, Landroid/support/v7/app/ActionBarActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 248
    const-string v0, "playback_resume_state"

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v1}, Lcom/google/android/videos/player/PlaybackResumeState;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 249
    return-void
.end method

.method public onSelectAudioTrackIndex(I)V
    .locals 1
    .param p1, "trackIndex"    # I

    .prologue
    .line 431
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/Director;->onSelectAudioTrackIndex(I)V

    .line 432
    return-void
.end method

.method public onSelectSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 1
    .param p1, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/Director;->onSelectSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;)V

    .line 427
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 194
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onStart()V

    .line 195
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    invoke-interface {v0}, Lcom/google/android/videos/remote/MediaRouteProvider;->onStart()V

    .line 196
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;

    invoke-virtual {v0}, Lcom/google/android/videos/remote/MediaRouteManager;->getCurrentlySelectedRemote()Lcom/google/android/videos/remote/RemoteControl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/WatchActivity;->onRemoteControlSelected(Lcom/google/android/videos/remote/RemoteControl;)V

    .line 197
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->config:Lcom/google/android/videos/Config;

    invoke-interface {v0}, Lcom/google/android/videos/Config;->knowledgeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/activity/WatchActivity;->isTrailer:Z

    if-nez v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getWishlistStoreSync()Lcom/google/android/videos/store/WishlistStoreSync;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivity;->account:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v1

    invoke-static {}, Lcom/google/android/videos/async/IgnoreCallback;->get()Lcom/google/android/videos/async/NewCallback;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/WishlistStoreSync;->syncWishlist(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/videos/activity/WatchActivityCompat;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/WatchActivityCompat;->onStart()V

    .line 203
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 253
    invoke-direct {p0}, Lcom/google/android/videos/activity/WatchActivity;->maybeResetDirector()V

    .line 254
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->mediaRouteProvider:Lcom/google/android/videos/remote/MediaRouteProvider;

    invoke-interface {v0}, Lcom/google/android/videos/remote/MediaRouteProvider;->onStop()V

    .line 255
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->watchActivityCompat:Lcom/google/android/videos/activity/WatchActivityCompat;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/WatchActivityCompat;->onStop()V

    .line 256
    invoke-super {p0}, Landroid/support/v7/app/ActionBarActivity;->onStop()V

    .line 257
    return-void
.end method

.method onUpPressed()V
    .locals 1

    .prologue
    .line 389
    invoke-static {p0}, Lcom/google/android/videos/utils/LockTaskModeCompat;->isInLockTaskMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 390
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivity;->finish()V

    .line 394
    :goto_0
    return-void

    .line 392
    :cond_0
    invoke-static {p0}, Lcom/google/android/videos/utils/LockTaskModeCompat;->showLockTaskModeToast(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public showControls()V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivity;->director:Lcom/google/android/videos/player/Director;

    invoke-virtual {v0}, Lcom/google/android/videos/player/Director;->showControls()V

    .line 285
    return-void
.end method
