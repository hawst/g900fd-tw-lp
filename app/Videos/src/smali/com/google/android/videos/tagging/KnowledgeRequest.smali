.class public final Lcom/google/android/videos/tagging/KnowledgeRequest;
.super Ljava/lang/Object;
.source "KnowledgeRequest.java"


# instance fields
.field public final account:Ljava/lang/String;

.field public final locale:Ljava/util/Locale;

.field public final userCountry:Ljava/lang/String;

.field public final videoId:Ljava/lang/String;

.field public final videoItag:I

.field public final videoTimestamp:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;ILjava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "userCountry"    # Ljava/lang/String;
    .param p4, "locale"    # Ljava/util/Locale;
    .param p5, "videoItag"    # I
    .param p6, "videoTimestamp"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->account:Ljava/lang/String;

    .line 67
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoId:Ljava/lang/String;

    .line 68
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->userCountry:Ljava/lang/String;

    .line 69
    iput-object p4, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->locale:Ljava/util/Locale;

    .line 70
    iput p5, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoItag:I

    .line 71
    iput-object p6, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoTimestamp:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public static createForDashDownloadWithCurrentLocale(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/videos/tagging/KnowledgeRequest;
    .locals 7
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "userCountry"    # Ljava/lang/String;
    .param p3, "timestamp"    # J

    .prologue
    .line 53
    new-instance v0, Lcom/google/android/videos/tagging/KnowledgeRequest;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const/16 v5, 0x91

    const-wide/16 v2, 0x0

    cmp-long v1, p3, v2

    if-nez v1, :cond_0

    const/4 v6, 0x0

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/tagging/KnowledgeRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;ILjava/lang/String;)V

    return-object v0

    :cond_0
    const-wide/16 v2, 0x3e8

    div-long v2, p3, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method public static createForDashWithCurrentLocale(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/tagging/KnowledgeRequest;
    .locals 7
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "userCountry"    # Ljava/lang/String;

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/videos/tagging/KnowledgeRequest;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const/16 v5, 0x91

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/tagging/KnowledgeRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;ILjava/lang/String;)V

    return-object v0
.end method

.method public static createWithCurrentLocale(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;)Lcom/google/android/videos/tagging/KnowledgeRequest;
    .locals 8
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "userCountry"    # Ljava/lang/String;
    .param p3, "stream"    # Lcom/google/android/videos/streams/MediaStream;

    .prologue
    .line 26
    iget-object v0, p3, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget-boolean v0, v0, Lcom/google/android/videos/streams/ItagInfo;->isDash:Z

    if-eqz v0, :cond_0

    .line 27
    invoke-static {p0, p1, p2}, Lcom/google/android/videos/tagging/KnowledgeRequest;->createForDashWithCurrentLocale(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/tagging/KnowledgeRequest;

    move-result-object v0

    .line 29
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/videos/tagging/KnowledgeRequest;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    iget-object v1, p3, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v5, v1, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    iget-object v1, p3, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget-wide v2, v1, Lcom/google/android/videos/proto/StreamInfo;->lastModifiedTimestamp:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/tagging/KnowledgeRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;ILjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 76
    if-ne p0, p1, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v1

    .line 78
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/tagging/KnowledgeRequest;

    if-nez v3, :cond_2

    move v1, v2

    .line 79
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 81
    check-cast v0, Lcom/google/android/videos/tagging/KnowledgeRequest;

    .line 82
    .local v0, "other":Lcom/google/android/videos/tagging/KnowledgeRequest;
    iget-object v3, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->account:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/tagging/KnowledgeRequest;->account:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoId:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->userCountry:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/tagging/KnowledgeRequest;->userCountry:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->locale:Ljava/util/Locale;

    iget-object v4, v0, Lcom/google/android/videos/tagging/KnowledgeRequest;->locale:Ljava/util/Locale;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoItag:I

    iget v4, v0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoItag:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoTimestamp:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoTimestamp:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 92
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->account:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    xor-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->userCountry:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    xor-int/2addr v2, v0

    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->locale:Ljava/util/Locale;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    xor-int/2addr v0, v2

    iget v2, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoItag:I

    xor-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoTimestamp:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    xor-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoTimestamp:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 99
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "KnowledgeRequest[videoId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",country="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->userCountry:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",locale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",itag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoItag:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",ts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoTimestamp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
