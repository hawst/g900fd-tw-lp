.class public Lcom/google/android/videos/ui/ShuffleAddItemAnimator;
.super Lcom/google/android/videos/ui/DefaultItemAnimator;
.source "ShuffleAddItemAnimator.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/ui/DefaultItemAnimator",
        "<",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator;-><init>()V

    return-void
.end method

.method private isCard(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z
    .locals 1
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 73
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    instance-of v0, v0, Lcom/google/android/videos/ui/playnext/ClusterItemView;

    return v0
.end method

.method private shouldShuffleAdd(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z
    .locals 2
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 69
    sget v0, Lcom/google/android/exoplayer/util/Util;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;->isCard(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected prepareAddAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;->shouldShuffleAdd(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22
    invoke-super {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->prepareAddAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 26
    :goto_0
    return-void

    .line 25
    :cond_0
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method protected runAddAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 4
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;->shouldShuffleAdd(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_1

    .line 35
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->runAddAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 54
    :goto_0
    return-void

    .line 38
    :cond_1
    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 39
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/google/android/play/animation/ShuffleAnimation;->shuffleAnimator(Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 41
    .local v0, "animation":Landroid/view/ViewPropertyAnimator;
    new-instance v2, Lcom/google/android/videos/ui/ShuffleAddItemAnimator$1;

    invoke-direct {v2, p0, v1, p1, v0}, Lcom/google/android/videos/ui/ShuffleAddItemAnimator$1;-><init>(Lcom/google/android/videos/ui/ShuffleAddItemAnimator;Landroid/view/View;Landroid/support/v7/widget/RecyclerView$ViewHolder;Landroid/view/ViewPropertyAnimator;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_0
.end method

.method protected setViewStateAfterEndAdditionAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    const/4 v2, 0x0

    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;->shouldShuffleAdd(Landroid/support/v7/widget/RecyclerView$ViewHolder;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 59
    invoke-super {p0, p1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->setViewStateAfterEndAdditionAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 66
    :goto_0
    return-void

    .line 62
    :cond_0
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 63
    .local v0, "view":Landroid/view/View;
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 64
    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 65
    invoke-virtual {v0, v2}, Landroid/view/View;->setRotation(F)V

    goto :goto_0
.end method
