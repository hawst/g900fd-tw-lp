.class public Lcom/google/android/videos/activity/MoviesFragment;
.super Lcom/google/android/videos/activity/HomeFragment;
.source "MoviesFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/videos/activity/HomeFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getTitleResourceId()I
    .locals 1

    .prologue
    .line 81
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected getUiElementType()I
    .locals 1

    .prologue
    .line 70
    const/16 v0, 0x1c

    return v0
.end method

.method protected onCreateHelper(Lcom/google/android/videos/activity/HomeActivity;Lcom/google/android/videos/VideosGlobals;)Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;
    .locals 30
    .param p1, "homeActivity"    # Lcom/google/android/videos/activity/HomeActivity;
    .param p2, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;

    .prologue
    .line 34
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v3

    .line 35
    .local v3, "config":Lcom/google/android/videos/Config;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v6

    .line 36
    .local v6, "purchaseStore":Lcom/google/android/videos/store/PurchaseStore;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v5

    .line 37
    .local v5, "database":Lcom/google/android/videos/store/Database;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/activity/HomeActivity;->getSyncHelper()Lcom/google/android/videos/ui/SyncHelper;

    move-result-object v7

    .line 39
    .local v7, "syncHelper":Lcom/google/android/videos/ui/SyncHelper;
    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 40
    .local v4, "mainHandler":Landroid/os/Handler;
    new-instance v1, Lcom/google/android/videos/ui/CursorHelper$MoviesPreorderCursorHelper;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/ui/CursorHelper$MoviesPreorderCursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/ui/SyncHelper;)V

    .line 42
    .local v1, "moviesPreorderCursorHelper":Lcom/google/android/videos/ui/CursorHelper$MoviesPreorderCursorHelper;
    new-instance v8, Lcom/google/android/videos/ui/CursorHelper$MoviesLibraryCursorHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/MoviesFragment;->getDownloadedOnlyManager()Lcom/google/android/videos/utils/DownloadedOnlyManager;

    move-result-object v15

    move-object/from16 v9, p1

    move-object v10, v3

    move-object v11, v4

    move-object v12, v5

    move-object v13, v6

    move-object v14, v7

    invoke-direct/range {v8 .. v15}, Lcom/google/android/videos/ui/CursorHelper$MoviesLibraryCursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/utils/DownloadedOnlyManager;)V

    .line 46
    .local v8, "moviesLibraryCursorHelper":Lcom/google/android/videos/ui/CursorHelper$MoviesLibraryCursorHelper;
    new-instance v9, Lcom/google/android/videos/ui/MoviesHelper;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/MoviesFragment;->getDownloadedOnlyManager()Lcom/google/android/videos/utils/DownloadedOnlyManager;

    move-result-object v12

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/MoviesFragment;->getView()Landroid/view/View;

    move-result-object v15

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v18

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v19

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getPosterStore()Lcom/google/android/videos/store/PosterStore;

    move-result-object v20

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v21

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getErrorHelper()Lcom/google/android/videos/utils/ErrorHelper;

    move-result-object v22

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getPinHelper()Lcom/google/android/videos/ui/PinHelper;

    move-result-object v24

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v25

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v26

    new-instance v27, Lcom/google/android/videos/logging/GenericUiElementNode;

    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/activity/MoviesFragment;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/activity/MoviesFragment;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-object/from16 v0, v27

    invoke-direct {v0, v2, v10, v13}, Lcom/google/android/videos/logging/GenericUiElementNode;-><init>(ILcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/MoviesFragment;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-object/from16 v29, v0

    move-object v10, v3

    move-object/from16 v13, p1

    move-object/from16 v16, v8

    move-object/from16 v17, v1

    move-object/from16 v23, v7

    invoke-direct/range {v9 .. v29}, Lcom/google/android/videos/ui/MoviesHelper;-><init>(Lcom/google/android/videos/Config;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/utils/DownloadedOnlyManager;Lcom/google/android/videos/activity/HomeActivity;Landroid/content/SharedPreferences;Landroid/view/View;Lcom/google/android/videos/ui/CursorHelper;Lcom/google/android/videos/ui/CursorHelper;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/ui/PinHelper;Lcom/google/android/videos/bitmap/BitmapRequesters;Lcom/google/android/videos/api/ApiRequesters;Lcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V

    return-object v9
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 75
    const v0, 0x7f0400b8

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
