.class public Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;
.super Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
.source "KnowledgeControlsAdapter.java"

# interfaces
.implements Lcom/google/android/videos/tagging/KnowledgeView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$1;,
        Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$DescriptionPresenter;,
        Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$CardPresenter;,
        Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$Listener;
    }
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;

.field private knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

.field private final listener:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$Listener;

.field private recentActorsStartIndex:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$Listener;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "listener"    # Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$Listener;

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->activity:Landroid/app/Activity;

    .line 47
    iput-object p2, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->listener:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$Listener;

    .line 48
    new-instance v0, Landroid/support/v17/leanback/widget/SinglePresenterSelector;

    new-instance v1, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$CardPresenter;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$CardPresenter;-><init>(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$1;)V

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/SinglePresenterSelector;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->setPresenterSelector(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 49
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;Lcom/google/android/videos/tagging/KnowledgeEntity;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;
    .param p1, "x1"    # Lcom/google/android/videos/tagging/KnowledgeEntity;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->getIndexOf(Lcom/google/android/videos/tagging/KnowledgeEntity;)I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->recentActorsStartIndex:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;)Lcom/google/android/videos/tagging/KnowledgeBundle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;)Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->listener:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$Listener;

    return-object v0
.end method

.method private clearKnowledge()V
    .locals 2

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->clear()V

    .line 66
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->listener:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$Listener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$Listener;->onFocusKnowledgeEntity(Lcom/google/android/videos/tagging/KnowledgeEntity;)V

    .line 67
    return-void
.end method

.method private getIndexOf(Lcom/google/android/videos/tagging/KnowledgeEntity;)I
    .locals 2
    .param p1, "entity"    # Lcom/google/android/videos/tagging/KnowledgeEntity;

    .prologue
    .line 137
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 138
    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 142
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 137
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 142
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method public createPresenter()Landroid/support/v17/leanback/widget/RowPresenter;
    .locals 4

    .prologue
    .line 58
    new-instance v0, Landroid/support/v17/leanback/widget/ListRowPresenter;

    invoke-direct {v0}, Landroid/support/v17/leanback/widget/ListRowPresenter;-><init>()V

    .line 59
    .local v0, "presenter":Landroid/support/v17/leanback/widget/ListRowPresenter;
    new-instance v1, Landroid/support/v17/leanback/widget/SinglePresenterSelector;

    new-instance v2, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$DescriptionPresenter;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$DescriptionPresenter;-><init>(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$1;)V

    invoke-direct {v1, v2}, Landroid/support/v17/leanback/widget/SinglePresenterSelector;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ListRowPresenter;->setHoverCardPresenterSelector(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 61
    return-object v0
.end method

.method public createRow()Landroid/support/v17/leanback/widget/ListRow;
    .locals 4

    .prologue
    .line 52
    iget-object v2, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->activity:Landroid/app/Activity;

    const v3, 0x7f0b01d2

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 53
    .local v1, "knowledgeTitle":Ljava/lang/String;
    new-instance v0, Landroid/support/v17/leanback/widget/HeaderItem;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/support/v17/leanback/widget/HeaderItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .local v0, "knowledgeHeader":Landroid/support/v17/leanback/widget/HeaderItem;
    new-instance v2, Landroid/support/v17/leanback/widget/ListRow;

    invoke-direct {v2, v0, p0}, Landroid/support/v17/leanback/widget/ListRow;-><init>(Landroid/support/v17/leanback/widget/HeaderItem;Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    return-object v2
.end method

.method public hasContent()Z
    .locals 1

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->size()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hideKnowledge()V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->clearKnowledge()V

    .line 106
    return-void
.end method

.method public initKnowledge(Lcom/google/android/videos/tagging/KnowledgeBundle;Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;I)V
    .locals 0
    .param p1, "knowledgeBundle"    # Lcom/google/android/videos/tagging/KnowledgeBundle;
    .param p2, "timeSupplier"    # Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;
    .param p3, "mode"    # I

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    .line 73
    return-void
.end method

.method public isInteracting()Z
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x1

    return v0
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->clearKnowledge()V

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    .line 112
    return-void
.end method

.method public setContentVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 134
    return-void
.end method

.method public showPausedKnowledge(IIILjava/util/List;I)V
    .locals 7
    .param p1, "timeMillis"    # I
    .param p2, "videoDisplayWidth"    # I
    .param p3, "videoDisplayHeight"    # I
    .param p5, "showRecentActorsWithinMillis"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .local p4, "taggedKnowledgeEntities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;>;"
    const/4 v6, 0x0

    .line 79
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 81
    .local v1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity;>;"
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 82
    .local v2, "localIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 83
    invoke-interface {p4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;

    .line 84
    .local v3, "taggedKnowledgeEntity":Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;
    iget-object v4, v3, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    iget v4, v4, Lcom/google/android/videos/tagging/KnowledgeEntity;->localId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 85
    iget-object v4, v3, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 88
    .end local v3    # "taggedKnowledgeEntity":Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    iput v4, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->recentActorsStartIndex:I

    .line 89
    if-lez p5, :cond_2

    .line 90
    iget-object v4, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    sub-int v5, p1, p5

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-virtual {v4, v5, p1, v2, v1}, Lcom/google/android/videos/tagging/KnowledgeBundle;->getRecentActors(IILjava/util/Set;Ljava/util/List;)V

    .line 94
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->clear()V

    .line 95
    invoke-virtual {p0, v6, v1}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->addAll(ILjava/util/Collection;)V

    .line 96
    return-void
.end method

.method public showPlayingKnowledge()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->clearKnowledge()V

    .line 101
    return-void
.end method
