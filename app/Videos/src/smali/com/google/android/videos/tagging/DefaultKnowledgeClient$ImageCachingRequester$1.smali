.class Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;
.super Ljava/lang/Object;
.source "DefaultKnowledgeClient.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->request(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Lcom/google/android/videos/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

.field final synthetic val$cacheKey:Ljava/lang/String;

.field final synthetic val$callback:Lcom/google/android/videos/async/Callback;

.field final synthetic val$request:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;


# direct methods
.method constructor <init>(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;Ljava/lang/String;Lcom/google/android/videos/async/Callback;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;)V
    .locals 0

    .prologue
    .line 373
    iput-object p1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->this$0:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    iput-object p2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->val$cacheKey:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iput-object p4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->val$request:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "unused"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->val$request:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    invoke-interface {v0, v1, p2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 382
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 373
    check-cast p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->onError(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "unused"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;
    .param p2, "response"    # Landroid/graphics/Bitmap;

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->this$0:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    # getter for: Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->globalBitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;
    invoke-static {v0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->access$800(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;)Lcom/google/android/videos/bitmap/BitmapLruCache;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->val$cacheKey:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/videos/bitmap/BitmapLruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->val$request:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    invoke-interface {v0, v1, p2}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 378
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 373
    check-cast p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;->onResponse(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;Landroid/graphics/Bitmap;)V

    return-void
.end method
