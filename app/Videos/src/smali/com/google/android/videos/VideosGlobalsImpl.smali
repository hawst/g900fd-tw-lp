.class Lcom/google/android/videos/VideosGlobalsImpl;
.super Lcom/google/android/videos/VideosGlobals;
.source "VideosGlobalsImpl.java"

# interfaces
.implements Lcom/google/android/videos/drm/DrmManager$Provider;


# static fields
.field private static final CPU_THREAD_POOL_SIZE:I


# instance fields
.field private accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

.field private apiRequesters:Lcom/google/android/videos/api/DefaultApiRequesters;

.field private final applicationContext:Landroid/content/Context;

.field private final applicationVersion:Ljava/lang/String;

.field private final assetImageUriCreator:Lcom/google/android/videos/ui/AssetImageUriCreator;

.field private assetStoreSync:Lcom/google/android/videos/store/AssetStoreSync;

.field private bitmapRequesters:Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;

.field private byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

.field private captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

.field private final config:Lcom/google/android/videos/Config;

.field private configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private contentNotificationManager:Lcom/google/android/videos/ContentNotificationManager;

.field private cpuExecutor:Ljava/util/concurrent/ExecutorService;

.field private dashStreamsSelector:Lcom/google/android/videos/streams/DashStreamsSelector;

.field private dataSources:Lcom/google/android/videos/pano/datasource/DataSources;

.field private database:Lcom/google/android/videos/store/Database;

.field private downloadNotificationManager:Lcom/google/android/videos/pinning/DownloadNotificationManager;

.field private drmManager:Lcom/google/android/videos/drm/DrmManager;

.field private errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private exoCacheProvider:Lcom/google/android/videos/pinning/ExoCacheProvider;

.field private freeMoviePromoWelcome:Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;

.field private gcmRegistrationManager:Lcom/google/android/videos/gcm/GcmRegistrationManager;

.field private gcmSender:Lcom/google/android/videos/gcm/GcmSender;

.field private globalBitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;

.field private httpClient:Lorg/apache/http/client/HttpClient;

.field private itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

.field private knowledgeClient:Lcom/google/android/videos/tagging/KnowledgeClient;

.field private legacyDownloadsHaveAppLevelDrm:Ljava/lang/Boolean;

.field private legacyStreamsSelector:Lcom/google/android/videos/streams/LegacyStreamsSelector;

.field private localExecutor:Ljava/util/concurrent/ExecutorService;

.field private localeObservable:Lcom/google/android/repolib/observers/Observable;

.field private final mainHandler:Landroid/os/Handler;

.field private mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;

.field private networkExecutor:Ljava/util/concurrent/ExecutorService;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private nowtificationHandler:Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;

.field private final panoEnabled:Z

.field private pinHelper:Lcom/google/android/videos/ui/PinHelper;

.field private playCommonNetworkStackWrapper:Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;

.field private playbackStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

.field private posterStore:Lcom/google/android/videos/store/PosterStore;

.field private preferences:Landroid/content/SharedPreferences;

.field private premiumStatus:I

.field private purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

.field private purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

.field private remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

.field private final resources:Landroid/content/res/Resources;

.field private screenshotFileStore:Lcom/google/android/videos/store/FileStore;

.field private showBannerFileStore:Lcom/google/android/videos/store/FileStore;

.field private showPosterFileStore:Lcom/google/android/videos/store/FileStore;

.field private signInManager:Lcom/google/android/videos/accounts/SignInManager;

.field private storyboardClient:Lcom/google/android/videos/store/StoryboardClient;

.field private subtitlesClient:Lcom/google/android/videos/store/SubtitlesClient;

.field private syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;

.field private final uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

.field private final userAgent:Ljava/lang/String;

.field private final versionCode:I

.field private verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

.field private videoPosterFileStore:Lcom/google/android/videos/store/FileStore;

.field private widgetNotifier:Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;

.field private wishlistStore:Lcom/google/android/videos/store/WishlistStore;

.field private wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;

.field private youtubeStatsPingSender:Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 131
    const/4 v0, 0x1

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    sput v0, Lcom/google/android/videos/VideosGlobalsImpl;->CPU_THREAD_POOL_SIZE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 22
    .param p1, "applicationContext"    # Landroid/content/Context;

    .prologue
    .line 239
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobals;-><init>()V

    .line 235
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->premiumStatus:I

    .line 240
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    .line 241
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->resources:Landroid/content/res/Resources;

    .line 242
    new-instance v2, Landroid/os/Handler;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->mainHandler:Landroid/os/Handler;

    .line 244
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v21

    .line 245
    .local v21, "resolver":Landroid/content/ContentResolver;
    invoke-static/range {v21 .. v21}, Lcom/google/android/videos/GservicesConfig;->bulkCache(Landroid/content/ContentResolver;)V

    .line 247
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    .line 248
    .local v13, "packageManager":Landroid/content/pm/PackageManager;
    const/16 v20, 0x0

    .line 250
    .local v20, "info":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v13, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v20

    .line 254
    :goto_0
    if-eqz v20, :cond_2

    .line 255
    move-object/from16 v0, v20

    iget-object v2, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationVersion:Ljava/lang/String;

    .line 256
    move-object/from16 v0, v20

    iget v2, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->versionCode:I

    .line 262
    :goto_1
    new-instance v2, Lorg/apache/http/conn/ssl/BrowserCompatHostnameVerifier;

    invoke-direct {v2}, Lorg/apache/http/conn/ssl/BrowserCompatHostnameVerifier;-><init>()V

    invoke-static {v2}, Ljavax/net/ssl/HttpsURLConnection;->setDefaultHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    .line 264
    invoke-static/range {v21 .. v21}, Lcom/google/android/videos/GservicesConfig;->getExperimentId(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v8

    .line 265
    .local v8, "experimentId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationVersion:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v8}, Lcom/google/android/videos/utils/Util;->buildUserAgent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->userAgent:Ljava/lang/String;

    .line 267
    invoke-static/range {p1 .. p1}, Lcom/google/android/videos/utils/NetworkObservable;->networkObservable(Landroid/content/Context;)Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 269
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getApplicationVersion()Ljava/lang/String;

    move-result-object v6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getUserAgent()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " gzip"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v21 .. v21}, Lcom/google/android/videos/GservicesConfig;->playLogServerUrl(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v21 .. v21}, Lcom/google/android/videos/GservicesConfig;->gservicesId(Landroid/content/ContentResolver;)J

    move-result-wide v10

    invoke-static/range {v21 .. v21}, Lcom/google/android/videos/GservicesConfig;->deviceCountry(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v12}, Lcom/google/android/videos/logging/AnalyticsClientFactory;->newInstance(Landroid/content/Context;Landroid/telephony/TelephonyManager;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/accounts/SignInManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Lcom/google/android/videos/logging/AnalyticsClient;

    move-result-object v17

    .line 280
    .local v17, "analyticsClient":Lcom/google/android/videos/logging/AnalyticsClient;
    new-instance v18, Lcom/google/android/videos/logging/DefaultEventLogger;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/logging/DefaultEventLogger;-><init>(Lcom/google/android/videos/logging/AnalyticsClient;Lcom/google/android/videos/utils/NetworkStatus;)V

    .line 281
    .local v18, "defaultEventLogger":Lcom/google/android/videos/logging/DefaultEventLogger;
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/VideosGlobalsImpl;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 283
    new-instance v12, Lcom/google/android/videos/utils/UriRewriter;

    move-object/from16 v0, v21

    invoke-direct {v12, v0}, Lcom/google/android/videos/utils/UriRewriter;-><init>(Landroid/content/ContentResolver;)V

    .line 284
    .local v12, "uriRewriter":Lcom/google/android/videos/utils/UriRewriter;
    new-instance v9, Lcom/google/android/videos/GservicesConfig;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/VideosGlobalsImpl;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v15

    move-object/from16 v10, p1

    move-object/from16 v11, v21

    move-object/from16 v16, p0

    invoke-direct/range {v9 .. v16}, Lcom/google/android/videos/GservicesConfig;-><init>(Landroid/content/Context;Landroid/content/ContentResolver;Lcom/google/android/videos/utils/UriRewriter;Landroid/content/pm/PackageManager;Lcom/google/android/videos/logging/EventLogger;Landroid/content/SharedPreferences;Lcom/google/android/videos/drm/DrmManager$Provider;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/videos/VideosGlobalsImpl;->config:Lcom/google/android/videos/Config;

    .line 286
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->config:Lcom/google/android/videos/Config;

    invoke-interface {v2}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->panoEnabled:Z

    .line 288
    new-instance v2, Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/VideosGlobalsImpl;->config:Lcom/google/android/videos/Config;

    move-object/from16 v0, v18

    invoke-direct {v2, v0, v3}, Lcom/google/android/videos/logging/UiEventLoggingHelper;-><init>(Lcom/google/android/videos/logging/UiEventLogger;Lcom/google/android/videos/Config;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    .line 290
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->config:Lcom/google/android/videos/Config;

    invoke-interface {v2}, Lcom/google/android/videos/Config;->dogfoodEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 291
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->setStrictModeVmPolicy()V

    .line 294
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->panoEnabled:Z

    if-eqz v2, :cond_3

    .line 295
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->resources:Landroid/content/res/Resources;

    invoke-static {v2}, Lcom/google/android/videos/ui/AssetImageUriCreator;->createForPano(Landroid/content/res/Resources;)Lcom/google/android/videos/ui/AssetImageUriCreator;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->assetImageUriCreator:Lcom/google/android/videos/ui/AssetImageUriCreator;

    .line 300
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->mainHandler:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/videos/VideosGlobalsImpl$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/videos/VideosGlobalsImpl$1;-><init>(Lcom/google/android/videos/VideosGlobalsImpl;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 308
    return-void

    .line 251
    .end local v8    # "experimentId":Ljava/lang/String;
    .end local v12    # "uriRewriter":Lcom/google/android/videos/utils/UriRewriter;
    .end local v17    # "analyticsClient":Lcom/google/android/videos/logging/AnalyticsClient;
    .end local v18    # "defaultEventLogger":Lcom/google/android/videos/logging/DefaultEventLogger;
    :catch_0
    move-exception v19

    .line 252
    .local v19, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "could not retrieve application version name"

    move-object/from16 v0, v19

    invoke-static {v2, v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 258
    .end local v19    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    const-string v2, "Unknown"

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationVersion:Ljava/lang/String;

    .line 259
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->versionCode:I

    goto/16 :goto_1

    .line 297
    .restart local v8    # "experimentId":Ljava/lang/String;
    .restart local v12    # "uriRewriter":Lcom/google/android/videos/utils/UriRewriter;
    .restart local v17    # "analyticsClient":Lcom/google/android/videos/logging/AnalyticsClient;
    .restart local v18    # "defaultEventLogger":Lcom/google/android/videos/logging/DefaultEventLogger;
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->resources:Landroid/content/res/Resources;

    invoke-static {v2}, Lcom/google/android/videos/ui/AssetImageUriCreator;->createForMobile(Landroid/content/res/Resources;)Lcom/google/android/videos/ui/AssetImageUriCreator;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/VideosGlobalsImpl;->assetImageUriCreator:Lcom/google/android/videos/ui/AssetImageUriCreator;

    goto :goto_2
.end method

.method static synthetic access$000(Lcom/google/android/videos/VideosGlobalsImpl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/VideosGlobalsImpl;

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->postStartup()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/VideosGlobalsImpl;)Lorg/apache/http/client/HttpClient;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/VideosGlobalsImpl;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->httpClient:Lorg/apache/http/client/HttpClient;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/VideosGlobalsImpl;)Lcom/google/android/videos/pinning/DownloadNotificationManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/VideosGlobalsImpl;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->downloadNotificationManager:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/VideosGlobalsImpl;)Lcom/google/android/videos/ui/VerticalsHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/VideosGlobalsImpl;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/VideosGlobalsImpl;)Lcom/google/android/videos/ContentNotificationManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/VideosGlobalsImpl;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->contentNotificationManager:Lcom/google/android/videos/ContentNotificationManager;

    return-object v0
.end method

.method private configureMobileNetworkUsageActivity()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 433
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/videos/utils/NetworkStatus;->isMobileNetworkCapable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 434
    new-instance v0, Landroid/content/ComponentName;

    iget-object v3, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    const-string v4, "com.google.android.videos.ManageNetworkUsageActivity"

    invoke-direct {v0, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 436
    .local v0, "manageActivity":Landroid/content/ComponentName;
    iget-object v3, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 437
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v2

    .line 438
    .local v2, "state":I
    if-eq v2, v5, :cond_0

    .line 439
    const-string v3, "Enabling network usage management"

    invoke-static {v3}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 440
    invoke-virtual {v1, v0, v5, v5}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 444
    .end local v0    # "manageActivity":Landroid/content/ComponentName;
    .end local v1    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v2    # "state":I
    :cond_0
    return-void
.end method

.method private ensureNotificationRelatedObjectsCreated()V
    .locals 5

    .prologue
    .line 1171
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->contentNotificationManager:Lcom/google/android/videos/ContentNotificationManager;

    if-eqz v0, :cond_0

    .line 1199
    :goto_0
    return-void

    .line 1174
    :cond_0
    new-instance v0, Lcom/google/android/videos/ContentNotificationManager;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getGcmSender()Lcom/google/android/videos/gcm/GcmSender;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/videos/ContentNotificationManager;-><init>(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/Config;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/gcm/GcmSender;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->contentNotificationManager:Lcom/google/android/videos/ContentNotificationManager;

    .line 1177
    iget-boolean v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->panoEnabled:Z

    if-nez v0, :cond_1

    .line 1178
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->contentNotificationManager:Lcom/google/android/videos/ContentNotificationManager;

    new-instance v1, Lcom/google/android/videos/DefaultContentNotificationProcessor;

    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPosterStore()Lcom/google/android/videos/store/PosterStore;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/videos/DefaultContentNotificationProcessor;-><init>(Landroid/content/Context;Lcom/google/android/videos/store/PosterStore;)V

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ContentNotificationManager;->setNotificationProcessor(Lcom/google/android/videos/ContentNotificationManager$Processor;)V

    .line 1180
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v0

    new-instance v1, Lcom/google/android/videos/VideosGlobalsImpl$6;

    invoke-direct {v1, p0}, Lcom/google/android/videos/VideosGlobalsImpl$6;-><init>(Lcom/google/android/videos/VideosGlobalsImpl;)V

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    goto :goto_0

    .line 1196
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDataSources()Lcom/google/android/videos/pano/datasource/DataSources;

    .line 1197
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNowtificationHandler()Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;

    goto :goto_0
.end method

.method private declared-synchronized getAssetStoreSync()Lcom/google/android/videos/store/AssetStoreSync;
    .locals 17

    .prologue
    .line 1110
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/videos/VideosGlobalsImpl;->assetStoreSync:Lcom/google/android/videos/store/AssetStoreSync;

    if-nez v1, :cond_0

    .line 1111
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v16

    .line 1112
    .local v16, "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    new-instance v1, Lcom/google/android/videos/store/AssetStoreSync;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getByteArrayPool()Lcom/google/android/videos/utils/ByteArrayPool;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v6

    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getVideoPosterFileStore()Lcom/google/android/videos/store/FileStore;

    move-result-object v7

    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getScreenshotFileStore()Lcom/google/android/videos/store/FileStore;

    move-result-object v8

    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getShowPosterFileStore()Lcom/google/android/videos/store/FileStore;

    move-result-object v9

    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getShowBannerFileStore()Lcom/google/android/videos/store/FileStore;

    move-result-object v10

    invoke-interface/range {v16 .. v16}, Lcom/google/android/videos/api/ApiRequesters;->getAssetsSyncRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v11

    invoke-interface/range {v16 .. v16}, Lcom/google/android/videos/api/ApiRequesters;->getConditionalEntitySyncRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v12

    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getSyncTaskManager()Lcom/google/android/videos/store/SyncTaskManager;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/VideosGlobalsImpl;->assetImageUriCreator:Lcom/google/android/videos/ui/AssetImageUriCreator;

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/videos/VideosGlobalsImpl;->panoEnabled:Z

    if-nez v15, :cond_1

    const/4 v15, 0x1

    :goto_0
    invoke-direct/range {v1 .. v15}, Lcom/google/android/videos/store/AssetStoreSync;-><init>(Lcom/google/android/videos/Config;Lcom/google/android/videos/utils/ByteArrayPool;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/SyncTaskManager;Lcom/google/android/videos/ui/AssetImageUriCreator;Z)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/videos/VideosGlobalsImpl;->assetStoreSync:Lcom/google/android/videos/store/AssetStoreSync;

    .line 1127
    .end local v16    # "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/videos/VideosGlobalsImpl;->assetStoreSync:Lcom/google/android/videos/store/AssetStoreSync;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 1112
    .restart local v16    # "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    :cond_1
    const/4 v15, 0x0

    goto :goto_0

    .line 1110
    .end local v16    # "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private getCachePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1099
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized getScreenshotFileStore()Lcom/google/android/videos/store/FileStore;
    .locals 4

    .prologue
    .line 1139
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->screenshotFileStore:Lcom/google/android/videos/store/FileStore;

    if-nez v0, :cond_0

    .line 1140
    new-instance v0, Lcom/google/android/videos/store/FileStore;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "screenshots"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/google/android/videos/store/FileStore;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->screenshotFileStore:Lcom/google/android/videos/store/FileStore;

    .line 1143
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->screenshotFileStore:Lcom/google/android/videos/store/FileStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1139
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getShowBannerFileStore()Lcom/google/android/videos/store/FileStore;
    .locals 4

    .prologue
    .line 1155
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->showBannerFileStore:Lcom/google/android/videos/store/FileStore;

    if-nez v0, :cond_0

    .line 1156
    new-instance v0, Lcom/google/android/videos/store/FileStore;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "show_banners"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/google/android/videos/store/FileStore;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->showBannerFileStore:Lcom/google/android/videos/store/FileStore;

    .line 1159
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->showBannerFileStore:Lcom/google/android/videos/store/FileStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1155
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getShowPosterFileStore()Lcom/google/android/videos/store/FileStore;
    .locals 4

    .prologue
    .line 1147
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->showPosterFileStore:Lcom/google/android/videos/store/FileStore;

    if-nez v0, :cond_0

    .line 1148
    new-instance v0, Lcom/google/android/videos/store/FileStore;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "show_posters"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/google/android/videos/store/FileStore;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->showPosterFileStore:Lcom/google/android/videos/store/FileStore;

    .line 1151
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->showPosterFileStore:Lcom/google/android/videos/store/FileStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getSyncTaskManager()Lcom/google/android/videos/store/SyncTaskManager;
    .locals 1

    .prologue
    .line 1103
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;

    if-nez v0, :cond_0

    .line 1104
    new-instance v0, Lcom/google/android/videos/store/SyncTaskManager;

    invoke-direct {v0}, Lcom/google/android/videos/store/SyncTaskManager;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;

    .line 1106
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1103
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private getTelephonyManager()Landroid/telephony/TelephonyManager;
    .locals 2

    .prologue
    .line 1095
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method private declared-synchronized getVideoPosterFileStore()Lcom/google/android/videos/store/FileStore;
    .locals 4

    .prologue
    .line 1131
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->videoPosterFileStore:Lcom/google/android/videos/store/FileStore;

    if-nez v0, :cond_0

    .line 1132
    new-instance v0, Lcom/google/android/videos/store/FileStore;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "posters"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/google/android/videos/store/FileStore;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->videoPosterFileStore:Lcom/google/android/videos/store/FileStore;

    .line 1135
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->videoPosterFileStore:Lcom/google/android/videos/store/FileStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1131
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getWidgetNotifier()Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;
    .locals 4

    .prologue
    .line 1163
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->widgetNotifier:Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;

    if-nez v0, :cond_0

    .line 1164
    new-instance v0, Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;

    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;-><init>(Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/Database;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->widgetNotifier:Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;

    .line 1167
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->widgetNotifier:Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1163
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private postStartup()V
    .locals 4

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getContentNotificationManager()Lcom/google/android/videos/ContentNotificationManager;

    .line 375
    iget-boolean v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->panoEnabled:Z

    if-nez v0, :cond_0

    .line 377
    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getWidgetNotifier()Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;

    .line 379
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/videos/pinning/TransferService;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 382
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDownloadNotificationManager()Lcom/google/android/videos/pinning/DownloadNotificationManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->onDownloadsStateChanged()V

    .line 384
    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->configureMobileNetworkUsageActivity()V

    .line 387
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "check_accounts_sync_status"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 388
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/videos/store/SyncService;->enableSyncForUninitializedAccounts(Lcom/google/android/videos/accounts/AccountManagerWrapper;Landroid/content/SharedPreferences;)V

    .line 389
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "check_accounts_sync_status"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 392
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getLocalExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getCachePath()Ljava/lang/String;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->panoEnabled:Z

    if-eqz v0, :cond_2

    const-wide/32 v0, 0x2000000

    :goto_0
    invoke-static {v2, v3, v0, v1}, Lcom/google/android/videos/cache/PersistentCache;->triggerCleanup(Ljava/util/concurrent/Executor;Ljava/lang/String;J)V

    .line 395
    new-instance v0, Lcom/google/android/videos/VideosGlobalsImpl$3;

    invoke-direct {v0, p0}, Lcom/google/android/videos/VideosGlobalsImpl$3;-><init>(Lcom/google/android/videos/VideosGlobalsImpl;)V

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobalsImpl$3;->start()V

    .line 430
    return-void

    .line 392
    :cond_2
    const-wide/32 v0, 0x500000

    goto :goto_0
.end method

.method private refreshContentRestrictionsV18()Z
    .locals 6

    .prologue
    .line 453
    iget-object v4, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    const-string v5, "user"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/UserManager;

    .line 455
    .local v3, "userManager":Landroid/os/UserManager;
    iget-object v4, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/UserManager;->getApplicationRestrictions(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 457
    .local v2, "restrictions":Landroid/os/Bundle;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 458
    const-string v4, "allow_unrated"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 459
    .local v0, "allowUnrated":Z
    const-string v4, "allowed_ids"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 460
    .local v1, "allowedRatingIds":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v4

    invoke-virtual {v4, v1, v0}, Lcom/google/android/videos/store/PurchaseStore;->setContentRestrictions([Ljava/lang/String;Z)Z

    move-result v4

    .line 463
    .end local v0    # "allowUnrated":Z
    .end local v1    # "allowedRatingIds":[Ljava/lang/String;
    :goto_0
    return v4

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/videos/store/PurchaseStore;->clearContentRestrictions()Z

    move-result v4

    goto :goto_0
.end method

.method private setStrictModeVmPolicy()V
    .locals 1

    .prologue
    .line 364
    new-instance v0, Landroid/os/StrictMode$VmPolicy$Builder;

    invoke-direct {v0}, Landroid/os/StrictMode$VmPolicy$Builder;-><init>()V

    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->detectAll()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->penaltyLog()Landroid/os/StrictMode$VmPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/StrictMode$VmPolicy$Builder;->build()Landroid/os/StrictMode$VmPolicy;

    move-result-object v0

    invoke-static {v0}, Landroid/os/StrictMode;->setVmPolicy(Landroid/os/StrictMode$VmPolicy;)V

    .line 369
    return-void
.end method


# virtual methods
.method public declared-synchronized getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .locals 6

    .prologue
    .line 794
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    if-nez v1, :cond_0

    .line 795
    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 796
    .local v0, "resolver":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/videos/GservicesConfig;->gmsCoreAvailable(Landroid/content/ContentResolver;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;

    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    const-string v3, "oauth2:https://www.googleapis.com/auth/android_video https://www.googleapis.com/auth/youtube https://www.googleapis.com/auth/pos"

    invoke-static {v0}, Lcom/google/android/videos/GservicesConfig;->tokenCacheDurationMs(Landroid/content/ContentResolver;)I

    move-result v4

    int-to-long v4, v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/videos/accounts/GmsAccountManagerWrapper;-><init>(Landroid/content/Context;Ljava/lang/String;J)V

    :goto_0
    iput-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 802
    .end local v0    # "resolver":Landroid/content/ContentResolver;
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 796
    .restart local v0    # "resolver":Landroid/content/ContentResolver;
    :cond_1
    :try_start_1
    new-instance v1, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper;

    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "oauth2:https://www.googleapis.com/auth/android_video https://www.googleapis.com/auth/youtube https://www.googleapis.com/auth/pos"

    const-string v4, "com.google"

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper;-><init>(Landroid/accounts/AccountManager;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 794
    .end local v0    # "resolver":Landroid/content/ContentResolver;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;
    .locals 8

    .prologue
    .line 586
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->apiRequesters:Lcom/google/android/videos/api/DefaultApiRequesters;

    if-nez v0, :cond_0

    .line 587
    new-instance v0, Lcom/google/android/videos/api/DefaultApiRequesters;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNetworkExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getByteArrayPool()Lcom/google/android/videos/utils/ByteArrayPool;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getCachePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getItagInfoStore()Lcom/google/android/videos/store/ItagInfoStore;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/api/DefaultApiRequesters;-><init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;Ljava/util/concurrent/Executor;Lcom/google/android/videos/utils/ByteArrayPool;Ljava/lang/String;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/Config;Lcom/google/android/videos/store/ItagInfoStore;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->apiRequesters:Lcom/google/android/videos/api/DefaultApiRequesters;

    .line 595
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->apiRequesters:Lcom/google/android/videos/api/DefaultApiRequesters;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 586
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    return-object v0
.end method

.method public getApplicationVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 884
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getApplicationVersionCode()I
    .locals 1

    .prologue
    .line 889
    iget v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->versionCode:I

    return v0
.end method

.method public getAssetImageUriCreator()Lcom/google/android/videos/ui/AssetImageUriCreator;
    .locals 1

    .prologue
    .line 1091
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->assetImageUriCreator:Lcom/google/android/videos/ui/AssetImageUriCreator;

    return-object v0
.end method

.method public declared-synchronized getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;
    .locals 9

    .prologue
    .line 571
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->bitmapRequesters:Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;

    if-nez v0, :cond_0

    .line 572
    new-instance v0, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;

    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getLocalExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNetworkExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getCpuExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getCachePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getGlobalBitmapCache()Lcom/google/android/videos/bitmap/BitmapLruCache;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/lang/String;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/Config;Lcom/google/android/videos/bitmap/BitmapLruCache;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->bitmapRequesters:Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;

    .line 581
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->bitmapRequesters:Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 571
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getByteArrayPool()Lcom/google/android/videos/utils/ByteArrayPool;
    .locals 2

    .prologue
    .line 1041
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    if-nez v0, :cond_0

    .line 1042
    new-instance v0, Lcom/google/android/videos/utils/ByteArrayPool;

    const/high16 v1, 0x80000

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/ByteArrayPool;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    .line 1044
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1041
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCaptionPreferences()Lcom/google/android/videos/player/CaptionPreferences;
    .locals 2

    .prologue
    .line 517
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

    if-nez v0, :cond_0

    .line 518
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/videos/player/CaptionPreferences;->create(Landroid/content/Context;Landroid/content/SharedPreferences;)Lcom/google/android/videos/player/CaptionPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

    .line 520
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 517
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getConfig()Lcom/google/android/videos/Config;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->config:Lcom/google/android/videos/Config;

    return-object v0
.end method

.method public declared-synchronized getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;
    .locals 10

    .prologue
    .line 690
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    if-nez v0, :cond_0

    .line 691
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v7

    .line 692
    .local v7, "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    new-instance v0, Lcom/google/android/videos/store/ConfigurationStore;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getLocalExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v4

    invoke-interface {v7}, Lcom/google/android/videos/api/ApiRequesters;->getUserConfigGetSyncRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v5

    invoke-interface {v7}, Lcom/google/android/videos/api/ApiRequesters;->getUserConfigGetRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/store/ConfigurationStore;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/videos/Config;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 700
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v8

    .line 701
    .local v8, "configurationCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Integer;Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v8}, Lcom/google/android/videos/store/ConfigurationStore;->loadConfiguration(ILcom/google/android/videos/async/Callback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 703
    :try_start_1
    invoke-virtual {v8}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 708
    .end local v7    # "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    .end local v8    # "configurationCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Integer;Ljava/lang/Void;>;"
    :cond_0
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 704
    .restart local v7    # "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    .restart local v8    # "configurationCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Integer;Ljava/lang/Void;>;"
    :catch_0
    move-exception v9

    .line 705
    .local v9, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v0, "Failed to load configuration"

    invoke-static {v0, v9}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 690
    .end local v7    # "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    .end local v8    # "configurationCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Integer;Ljava/lang/Void;>;"
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getContentNotificationManager()Lcom/google/android/videos/ContentNotificationManager;
    .locals 1

    .prologue
    .line 873
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->ensureNotificationRelatedObjectsCreated()V

    .line 874
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->contentNotificationManager:Lcom/google/android/videos/ContentNotificationManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 873
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getCpuExecutor()Ljava/util/concurrent/ExecutorService;
    .locals 10

    .prologue
    .line 833
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->cpuExecutor:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    .line 834
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    sget v2, Lcom/google/android/videos/VideosGlobalsImpl;->CPU_THREAD_POOL_SIZE:I

    sget v3, Lcom/google/android/videos/VideosGlobalsImpl;->CPU_THREAD_POOL_SIZE:I

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v8, Lcom/google/android/videos/utils/PriorityThreadFactory;

    const-string v0, "cpu"

    const/4 v9, 0x2

    invoke-direct {v8, v0, v9}, Lcom/google/android/videos/utils/PriorityThreadFactory;-><init>(Ljava/lang/String;I)V

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->cpuExecutor:Ljava/util/concurrent/ExecutorService;

    .line 841
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->cpuExecutor:Ljava/util/concurrent/ExecutorService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 833
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDashStreamsSelector()Lcom/google/android/videos/streams/DashStreamsSelector;
    .locals 4

    .prologue
    .line 615
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->dashStreamsSelector:Lcom/google/android/videos/streams/DashStreamsSelector;

    if-nez v0, :cond_0

    .line 616
    new-instance v0, Lcom/google/android/videos/streams/DashStreamsSelector;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/streams/DashStreamsSelector;-><init>(Lcom/google/android/videos/Config;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/utils/NetworkStatus;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->dashStreamsSelector:Lcom/google/android/videos/streams/DashStreamsSelector;

    .line 619
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->dashStreamsSelector:Lcom/google/android/videos/streams/DashStreamsSelector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 615
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDataSources()Lcom/google/android/videos/pano/datasource/DataSources;
    .locals 12

    .prologue
    .line 1022
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->dataSources:Lcom/google/android/videos/pano/datasource/DataSources;

    if-nez v0, :cond_0

    .line 1023
    iget-boolean v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->panoEnabled:Z

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 1024
    new-instance v0, Lcom/google/android/videos/pano/datasource/DataSources;

    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/videos/api/ApiRequesters;->getPromotionsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getContentNotificationManager()Lcom/google/android/videos/ContentNotificationManager;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v10

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getLocaleObservable()Lcom/google/android/repolib/observers/Observable;

    move-result-object v11

    invoke-direct/range {v0 .. v11}, Lcom/google/android/videos/pano/datasource/DataSources;-><init>(Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/Config;Lcom/google/android/videos/async/Requester;Landroid/content/SharedPreferences;Lcom/google/android/videos/ContentNotificationManager;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/repolib/observers/Observable;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->dataSources:Lcom/google/android/videos/pano/datasource/DataSources;

    .line 1036
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->dataSources:Lcom/google/android/videos/pano/datasource/DataSources;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1022
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDatabase()Lcom/google/android/videos/store/Database;
    .locals 5

    .prologue
    .line 681
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->database:Lcom/google/android/videos/store/Database;

    if-nez v0, :cond_0

    .line 682
    new-instance v0, Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    const-string v2, "purchase_store.db"

    iget-object v3, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/videos/store/Database;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Looper;Lcom/google/android/videos/logging/EventLogger;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->database:Lcom/google/android/videos/store/Database;

    .line 685
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->database:Lcom/google/android/videos/store/Database;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 681
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDownloadNotificationManager()Lcom/google/android/videos/pinning/DownloadNotificationManager;
    .locals 7

    .prologue
    .line 854
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->downloadNotificationManager:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    if-nez v0, :cond_0

    .line 855
    new-instance v0, Lcom/google/android/videos/pinning/DownloadNotificationManager;

    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->mainHandler:Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v3

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/videos/NotificationUtil;->create(Landroid/content/Context;)Lcom/google/android/videos/NotificationUtil;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPosterStore()Lcom/google/android/videos/store/PosterStore;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/pinning/DownloadNotificationManager;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Ljava/util/concurrent/Executor;Lcom/google/android/videos/NotificationUtil;Lcom/google/android/videos/store/PosterStore;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->downloadNotificationManager:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    .line 861
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v0

    new-instance v1, Lcom/google/android/videos/VideosGlobalsImpl$4;

    invoke-direct {v1, p0}, Lcom/google/android/videos/VideosGlobalsImpl$4;-><init>(Lcom/google/android/videos/VideosGlobalsImpl;)V

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 868
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->downloadNotificationManager:Lcom/google/android/videos/pinning/DownloadNotificationManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 854
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDrmManager()Lcom/google/android/videos/drm/DrmManager;
    .locals 8

    .prologue
    .line 664
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    if-nez v0, :cond_0

    .line 667
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->networkExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/videos/Config;->forceAppLevelDrm()Z

    move-result v7

    invoke-static/range {v0 .. v7}, Lcom/google/android/videos/drm/DrmManager;->newDrmManager(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/Config;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/logging/EventLogger;Z)Lcom/google/android/videos/drm/DrmManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    .line 676
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->drmManager:Lcom/google/android/videos/drm/DrmManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 664
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getErrorHelper()Lcom/google/android/videos/utils/ErrorHelper;
    .locals 3

    .prologue
    .line 846
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    if-nez v0, :cond_0

    .line 847
    new-instance v0, Lcom/google/android/videos/utils/ErrorHelper;

    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/utils/ErrorHelper;-><init>(Landroid/content/Context;Lcom/google/android/videos/utils/NetworkStatus;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    .line 849
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 846
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getEventLogger()Lcom/google/android/videos/logging/EventLogger;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    return-object v0
.end method

.method public declared-synchronized getExoCacheProvider()Lcom/google/android/videos/pinning/ExoCacheProvider;
    .locals 1

    .prologue
    .line 991
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->exoCacheProvider:Lcom/google/android/videos/pinning/ExoCacheProvider;

    if-nez v0, :cond_0

    .line 992
    new-instance v0, Lcom/google/android/videos/pinning/ExoCacheProvider;

    invoke-direct {v0}, Lcom/google/android/videos/pinning/ExoCacheProvider;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->exoCacheProvider:Lcom/google/android/videos/pinning/ExoCacheProvider;

    .line 994
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->exoCacheProvider:Lcom/google/android/videos/pinning/ExoCacheProvider;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 991
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getFreeMoviePromoWelcome()Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;
    .locals 14

    .prologue
    .line 999
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->freeMoviePromoWelcome:Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;

    if-nez v0, :cond_0

    .line 1000
    new-instance v0, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;

    const-string v1, "watchnow"

    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getErrorHelper()Lcom/google/android/videos/utils/ErrorHelper;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v10

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v11

    invoke-interface {v11}, Lcom/google/android/videos/api/ApiRequesters;->getPromotionsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v11

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v12

    invoke-interface {v12}, Lcom/google/android/videos/api/ApiRequesters;->getRedeemPromotionRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v12

    const/4 v13, 0x1

    invoke-direct/range {v0 .. v13}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/videos/Config;Landroid/content/SharedPreferences;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Z)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->freeMoviePromoWelcome:Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;

    .line 1014
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->freeMoviePromoWelcome:Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;

    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getWidgetNotifier()Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->setEligibilityChangedListener(Lcom/google/android/videos/welcome/Welcome$OnEligibilityChangedListener;)V

    .line 1015
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->freeMoviePromoWelcome:Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;

    invoke-virtual {v0}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->onStart()V

    .line 1017
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->freeMoviePromoWelcome:Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 999
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getGcmRegistrationManager()Lcom/google/android/videos/gcm/GcmRegistrationManager;
    .locals 6

    .prologue
    .line 525
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->gcmRegistrationManager:Lcom/google/android/videos/gcm/GcmRegistrationManager;

    if-nez v1, :cond_0

    .line 526
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/videos/Config;->gcmRegistrationEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 527
    new-instance v1, Lcom/google/android/videos/gcm/DummyGcmRegistrationManager;

    invoke-direct {v1}, Lcom/google/android/videos/gcm/DummyGcmRegistrationManager;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->gcmRegistrationManager:Lcom/google/android/videos/gcm/GcmRegistrationManager;

    .line 536
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->gcmRegistrationManager:Lcom/google/android/videos/gcm/GcmRegistrationManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 529
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v0

    .line 530
    .local v0, "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    new-instance v1, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;

    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v3

    invoke-interface {v0}, Lcom/google/android/videos/api/ApiRequesters;->getGcmRegisterSyncRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/android/videos/api/ApiRequesters;->getGcmCreateNotificationKeySyncRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/videos/gcm/DefaultGcmRegistrationManager;-><init>(Landroid/content/Context;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;)V

    iput-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->gcmRegistrationManager:Lcom/google/android/videos/gcm/GcmRegistrationManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 525
    .end local v0    # "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getGcmSender()Lcom/google/android/videos/gcm/GcmSender;
    .locals 3

    .prologue
    .line 541
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->gcmSender:Lcom/google/android/videos/gcm/GcmSender;

    if-nez v0, :cond_0

    .line 542
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->gcmMessagingEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 543
    new-instance v0, Lcom/google/android/videos/gcm/DummyGcmSender;

    invoke-direct {v0}, Lcom/google/android/videos/gcm/DummyGcmSender;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->gcmSender:Lcom/google/android/videos/gcm/GcmSender;

    .line 548
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->gcmSender:Lcom/google/android/videos/gcm/GcmSender;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 545
    :cond_1
    :try_start_1
    new-instance v0, Lcom/google/android/videos/gcm/DefaultGcmSender;

    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getGcmRegistrationManager()Lcom/google/android/videos/gcm/GcmRegistrationManager;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/gcm/DefaultGcmSender;-><init>(Landroid/content/Context;Lcom/google/android/videos/gcm/GcmRegistrationManager;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->gcmSender:Lcom/google/android/videos/gcm/GcmSender;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 541
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getGlobalBitmapCache()Lcom/google/android/videos/bitmap/BitmapLruCache;
    .locals 2

    .prologue
    .line 1049
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->globalBitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;

    if-nez v0, :cond_0

    .line 1050
    new-instance v0, Lcom/google/android/videos/bitmap/BitmapLruCache;

    const/high16 v1, 0x2000000

    invoke-direct {v0, v1}, Lcom/google/android/videos/bitmap/BitmapLruCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->globalBitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;

    .line 1052
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->globalBitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1049
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getHttpClient()Lorg/apache/http/client/HttpClient;
    .locals 2

    .prologue
    .line 786
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->httpClient:Lorg/apache/http/client/HttpClient;

    if-nez v0, :cond_0

    .line 787
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getUserAgent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " gzip"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/HttpClientFactory;->createDefaultHttpClient(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->httpClient:Lorg/apache/http/client/HttpClient;

    .line 789
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->httpClient:Lorg/apache/http/client/HttpClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 786
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getItagInfoStore()Lcom/google/android/videos/store/ItagInfoStore;
    .locals 6

    .prologue
    .line 942
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    if-nez v2, :cond_0

    .line 943
    new-instance v2, Lcom/google/android/videos/store/ItagInfoStore;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getLocalExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/videos/store/ItagInfoStore;-><init>(Lcom/google/android/videos/Config;Ljava/util/concurrent/Executor;Lcom/google/android/videos/store/Database;)V

    iput-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    .line 944
    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/videos/api/ApiRequesters;->getVideoFormatsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/videos/store/ItagInfoStore;->init(Lcom/google/android/videos/async/Requester;)V

    .line 946
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v1

    .line 947
    .local v1, "itagCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Integer;Ljava/lang/Void;>;"
    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Lcom/google/android/videos/store/ItagInfoStore;->load(ILcom/google/android/videos/async/Callback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 949
    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    .line 950
    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    invoke-virtual {v2}, Lcom/google/android/videos/store/ItagInfoStore;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 953
    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/android/videos/async/IgnoreCallback;->get()Lcom/google/android/videos/async/NewCallback;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/videos/store/ItagInfoStore;->refreshItagData(ILcom/google/android/videos/async/Callback;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 959
    .end local v1    # "itagCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Integer;Ljava/lang/Void;>;"
    :cond_0
    :goto_0
    :try_start_2
    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v2

    .line 955
    .restart local v1    # "itagCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Integer;Ljava/lang/Void;>;"
    :catch_0
    move-exception v0

    .line 956
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v2, "Failed to load itag data"

    invoke-static {v2, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 942
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "itagCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/Integer;Ljava/lang/Void;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized getKnowledgeClient()Lcom/google/android/videos/tagging/KnowledgeClient;
    .locals 13

    .prologue
    .line 643
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->knowledgeClient:Lcom/google/android/videos/tagging/KnowledgeClient;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->knowledgeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 644
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0e01ae

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 646
    .local v10, "desiredPosterWidth":I
    new-instance v0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;

    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getLocalExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNetworkExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v7

    invoke-interface {v7}, Lcom/google/android/videos/api/ApiRequesters;->getAssetsCachingRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getBitmapBytesRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getGlobalBitmapCache()Lcom/google/android/videos/bitmap/BitmapLruCache;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getApplicationVersion()Ljava/lang/String;

    move-result-object v12

    move v11, v10

    invoke-direct/range {v0 .. v12}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;-><init>(Landroid/content/Context;Lcom/google/android/videos/Config;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;IILjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->knowledgeClient:Lcom/google/android/videos/tagging/KnowledgeClient;

    .line 659
    .end local v10    # "desiredPosterWidth":I
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->knowledgeClient:Lcom/google/android/videos/tagging/KnowledgeClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 643
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLegacyStreamsSelector()Lcom/google/android/videos/streams/LegacyStreamsSelector;
    .locals 4

    .prologue
    .line 606
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->legacyStreamsSelector:Lcom/google/android/videos/streams/LegacyStreamsSelector;

    if-nez v0, :cond_0

    .line 607
    new-instance v0, Lcom/google/android/videos/streams/LegacyStreamsSelector;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/streams/LegacyStreamsSelector;-><init>(Lcom/google/android/videos/Config;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/utils/NetworkStatus;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->legacyStreamsSelector:Lcom/google/android/videos/streams/LegacyStreamsSelector;

    .line 610
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->legacyStreamsSelector:Lcom/google/android/videos/streams/LegacyStreamsSelector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 606
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLocalExecutor()Ljava/util/concurrent/ExecutorService;
    .locals 10

    .prologue
    .line 820
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->localExecutor:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    .line 821
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x2

    const/4 v3, 0x2

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v8, Lcom/google/android/videos/utils/PriorityThreadFactory;

    const-string v0, "local"

    const/4 v9, 0x1

    invoke-direct {v8, v0, v9}, Lcom/google/android/videos/utils/PriorityThreadFactory;-><init>(Ljava/lang/String;I)V

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->localExecutor:Ljava/util/concurrent/ExecutorService;

    .line 828
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->localExecutor:Ljava/util/concurrent/ExecutorService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 820
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getLocaleObservable()Lcom/google/android/repolib/observers/Observable;
    .locals 1

    .prologue
    .line 1075
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->localeObservable:Lcom/google/android/repolib/observers/Observable;

    if-nez v0, :cond_0

    .line 1076
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/videos/utils/LocaleObservable;->localeObservable(Landroid/content/Context;)Lcom/google/android/repolib/observers/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->localeObservable:Lcom/google/android/repolib/observers/Observable;

    .line 1078
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->localeObservable:Lcom/google/android/repolib/observers/Observable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1075
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMediaRouteManager()Lcom/google/android/videos/remote/MediaRouteManager;
    .locals 11

    .prologue
    .line 894
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;

    if-nez v1, :cond_1

    .line 895
    const/4 v0, 0x0

    .line 896
    .local v0, "castMediaRouter":Lcom/google/android/videos/cast/CastMediaRouter;
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v9

    .line 897
    .local v9, "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v10

    .line 898
    .local v10, "config":Lcom/google/android/videos/Config;
    invoke-interface {v10}, Lcom/google/android/videos/Config;->castV2Enabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 899
    new-instance v0, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;

    .end local v0    # "castMediaRouter":Lcom/google/android/videos/cast/CastMediaRouter;
    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-interface {v10}, Lcom/google/android/videos/Config;->castV2ReceiverAppId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v9}, Lcom/google/android/videos/api/ApiRequesters;->getMpdUrlGetRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v3

    invoke-interface {v9}, Lcom/google/android/videos/api/ApiRequesters;->getRecommendationsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getRecommendationsRequestFactory()Lcom/google/android/videos/api/RecommendationsRequest$Factory;

    move-result-object v5

    invoke-interface {v9}, Lcom/google/android/videos/api/ApiRequesters;->getRobotTokenRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v10}, Lcom/google/android/videos/Config;->castDebuggingEnabled()Z

    move-result v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/videos/cast/v2/CastV2MediaRouter;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/api/RecommendationsRequest$Factory;Lcom/google/android/videos/async/Requester;Landroid/content/SharedPreferences;Z)V

    .line 908
    .restart local v0    # "castMediaRouter":Lcom/google/android/videos/cast/CastMediaRouter;
    :cond_0
    new-instance v1, Lcom/google/android/videos/remote/MediaRouteManager;

    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v6

    move-object v5, v0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/videos/remote/MediaRouteManager;-><init>(Landroid/content/Context;Lcom/google/android/videos/logging/EventLogger;Landroid/content/SharedPreferences;Lcom/google/android/videos/cast/CastMediaRouter;Lcom/google/android/videos/utils/NetworkStatus;)V

    iput-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;

    .line 911
    .end local v0    # "castMediaRouter":Lcom/google/android/videos/cast/CastMediaRouter;
    .end local v9    # "apiRequesters":Lcom/google/android/videos/api/ApiRequesters;
    .end local v10    # "config":Lcom/google/android/videos/Config;
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->mediaRouteManager:Lcom/google/android/videos/remote/MediaRouteManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 894
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized getNetworkExecutor()Ljava/util/concurrent/ExecutorService;
    .locals 10

    .prologue
    .line 807
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->networkExecutor:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    .line 808
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x4

    const/4 v3, 0x4

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v8, Lcom/google/android/videos/utils/PriorityThreadFactory;

    const-string v0, "network"

    const/16 v9, 0xa

    invoke-direct {v8, v0, v9}, Lcom/google/android/videos/utils/PriorityThreadFactory;-><init>(Ljava/lang/String;I)V

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->networkExecutor:Ljava/util/concurrent/ExecutorService;

    .line 815
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->networkExecutor:Ljava/util/concurrent/ExecutorService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 807
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;
    .locals 1

    .prologue
    .line 879
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    return-object v0
.end method

.method public declared-synchronized getNowtificationHandler()Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;
    .locals 2

    .prologue
    .line 1083
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->nowtificationHandler:Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;

    if-nez v0, :cond_0

    .line 1084
    new-instance v0, Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;

    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->nowtificationHandler:Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;

    .line 1086
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->nowtificationHandler:Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1083
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPinHelper()Lcom/google/android/videos/ui/PinHelper;
    .locals 6

    .prologue
    .line 1066
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    if-nez v0, :cond_0

    .line 1067
    new-instance v0, Lcom/google/android/videos/ui/PinHelper;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getItagInfoStore()Lcom/google/android/videos/store/ItagInfoStore;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/ui/PinHelper;-><init>(Landroid/content/SharedPreferences;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/store/ItagInfoStore;Lcom/google/android/videos/store/PurchaseStore;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    .line 1070
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->pinHelper:Lcom/google/android/videos/ui/PinHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1066
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPlayCommonNetworkStackWrapper()Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;
    .locals 3

    .prologue
    .line 1057
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->playCommonNetworkStackWrapper:Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;

    if-nez v0, :cond_0

    .line 1058
    new-instance v0, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;

    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;-><init>(Landroid/content/Context;Lcom/google/android/videos/utils/NetworkStatus;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->playCommonNetworkStackWrapper:Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;

    .line 1061
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->playCommonNetworkStackWrapper:Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1057
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPlaybackStatusNotifier()Lcom/google/android/videos/player/PlaybackStatusNotifier;
    .locals 1

    .prologue
    .line 778
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->playbackStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    if-nez v0, :cond_0

    .line 779
    new-instance v0, Lcom/google/android/videos/player/PlaybackStatusNotifier;

    invoke-direct {v0}, Lcom/google/android/videos/player/PlaybackStatusNotifier;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->playbackStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    .line 781
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->playbackStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 778
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPosterStore()Lcom/google/android/videos/store/PosterStore;
    .locals 11

    .prologue
    .line 757
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->posterStore:Lcom/google/android/videos/store/PosterStore;

    if-nez v0, :cond_0

    .line 758
    new-instance v5, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;

    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->resources:Landroid/content/res/Resources;

    const v1, 0x7f0e017b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {v5, v0, v1}, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;-><init>(IZ)V

    .line 760
    .local v5, "posterConverter":Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;
    new-instance v8, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;

    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->assetImageUriCreator:Lcom/google/android/videos/ui/AssetImageUriCreator;

    iget v0, v0, Lcom/google/android/videos/ui/AssetImageUriCreator;->maxMovieScreenshotWidth:I

    const/4 v1, 0x1

    invoke-direct {v8, v0, v1}, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;-><init>(IZ)V

    .line 762
    .local v8, "widescreenImageConverter":Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;
    new-instance v0, Lcom/google/android/videos/store/PosterStore;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getLocalExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getCpuExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getVideoPosterFileStore()Lcom/google/android/videos/store/FileStore;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getShowPosterFileStore()Lcom/google/android/videos/store/FileStore;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getScreenshotFileStore()Lcom/google/android/videos/store/FileStore;

    move-result-object v6

    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getShowBannerFileStore()Lcom/google/android/videos/store/FileStore;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getGlobalBitmapCache()Lcom/google/android/videos/bitmap/BitmapLruCache;

    move-result-object v9

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getByteArrayPool()Lcom/google/android/videos/utils/ByteArrayPool;

    move-result-object v10

    invoke-direct/range {v0 .. v10}, Lcom/google/android/videos/store/PosterStore;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/store/FileStore;Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;Lcom/google/android/videos/bitmap/BitmapLruCache;Lcom/google/android/videos/utils/ByteArrayPool;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->posterStore:Lcom/google/android/videos/store/PosterStore;

    .line 773
    .end local v5    # "posterConverter":Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;
    .end local v8    # "widescreenImageConverter":Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->posterStore:Lcom/google/android/videos/store/PosterStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 757
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 508
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->preferences:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    const-string v1, "youtube"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->preferences:Landroid/content/SharedPreferences;

    .line 512
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->preferences:Landroid/content/SharedPreferences;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 508
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPremiumErrorMessage()Ljava/lang/CharSequence;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 489
    iget v3, p0, Lcom/google/android/videos/VideosGlobalsImpl;->premiumStatus:I

    packed-switch v3, :pswitch_data_0

    .line 503
    :goto_0
    :pswitch_0
    return-object v2

    .line 493
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    const v3, 0x7f0b00fc

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 495
    :pswitch_2
    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    const v3, 0x7f0b00fa

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 497
    .local v1, "deviceUpdateNeededText":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    goto :goto_0

    .line 499
    .end local v1    # "deviceUpdateNeededText":Ljava/lang/String;
    :pswitch_3
    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    const v3, 0x7f0b00fb

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {}, Lcom/google/android/videos/utils/PlayStoreUtil;->getPlayStoreVideosAppUri()Landroid/net/Uri;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 501
    .local v0, "applicationUpdateNeededText":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    goto :goto_0

    .line 489
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public declared-synchronized getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;
    .locals 3

    .prologue
    .line 748
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    if-nez v0, :cond_0

    .line 749
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getLocalExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/store/PurchaseStore;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/videos/store/Database;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    .line 750
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->refreshContentRestrictions()Z

    .line 752
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 748
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;
    .locals 9

    .prologue
    .line 732
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    if-nez v0, :cond_0

    .line 733
    new-instance v0, Lcom/google/android/videos/store/PurchaseStoreSync;

    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/videos/api/ApiRequesters;->getUserLibrarySyncRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v5

    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getSyncTaskManager()Lcom/google/android/videos/store/SyncTaskManager;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v7

    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getAssetStoreSync()Lcom/google/android/videos/store/AssetStoreSync;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/videos/store/PurchaseStoreSync;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/SyncTaskManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/AssetStoreSync;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    .line 741
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    invoke-virtual {v0}, Lcom/google/android/videos/store/PurchaseStoreSync;->cleanup()V

    .line 743
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 732
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getRecommendationsRequestFactory()Lcom/google/android/videos/api/RecommendationsRequest$Factory;
    .locals 3

    .prologue
    .line 600
    new-instance v0, Lcom/google/android/videos/api/RecommendationsRequest$DefaultFactory;

    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/api/RecommendationsRequest$DefaultFactory;-><init>(Landroid/telephony/TelephonyManager;Lcom/google/android/videos/store/ConfigurationStore;)V

    return-object v0
.end method

.method public declared-synchronized getRemoteTracker()Lcom/google/android/videos/remote/RemoteTracker;
    .locals 7

    .prologue
    .line 916
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    if-nez v0, :cond_0

    .line 917
    new-instance v0, Lcom/google/android/videos/remote/RemoteTracker;

    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getLocalExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPosterStore()Lcom/google/android/videos/store/PosterStore;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getMediaRouteManager()Lcom/google/android/videos/remote/MediaRouteManager;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/remote/RemoteTracker;-><init>(Landroid/content/Context;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/store/Database;Ljava/util/concurrent/Executor;Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/remote/MediaRouteManager;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    .line 920
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 916
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSignInManager()Lcom/google/android/videos/accounts/SignInManager;
    .locals 3

    .prologue
    .line 925
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    if-nez v0, :cond_0

    .line 926
    new-instance v0, Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/accounts/SignInManager;-><init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    .line 928
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->signInManager:Lcom/google/android/videos/accounts/SignInManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 925
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getStoryboardClient()Lcom/google/android/videos/store/StoryboardClient;
    .locals 6

    .prologue
    .line 633
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->storyboardClient:Lcom/google/android/videos/store/StoryboardClient;

    if-nez v0, :cond_0

    .line 634
    new-instance v0, Lcom/google/android/videos/store/StoryboardClient;

    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNetworkExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getLocalExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getSyncBitmapBytesRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getGlobalBitmapCache()Lcom/google/android/videos/bitmap/BitmapLruCache;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/store/StoryboardClient;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->storyboardClient:Lcom/google/android/videos/store/StoryboardClient;

    .line 638
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->storyboardClient:Lcom/google/android/videos/store/StoryboardClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 633
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getSubtitlesClient()Lcom/google/android/videos/store/SubtitlesClient;
    .locals 6

    .prologue
    .line 624
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->subtitlesClient:Lcom/google/android/videos/store/SubtitlesClient;

    if-nez v0, :cond_0

    .line 625
    new-instance v0, Lcom/google/android/videos/store/SubtitlesClient;

    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNetworkExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getLocalExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v4

    invoke-static {}, Lcom/google/android/videos/utils/XmlParser;->createPrefixesOnlyParser()Lcom/google/android/videos/utils/XmlParser;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/store/SubtitlesClient;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/utils/XmlParser;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->subtitlesClient:Lcom/google/android/videos/store/SubtitlesClient;

    .line 628
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->subtitlesClient:Lcom/google/android/videos/store/SubtitlesClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 624
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getUiEventLoggingHelper()Lcom/google/android/videos/logging/UiEventLoggingHelper;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    return-object v0
.end method

.method public getUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->userAgent:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getVerticalsHelper()Lcom/google/android/videos/ui/VerticalsHelper;
    .locals 5

    .prologue
    .line 964
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    if-nez v0, :cond_0

    .line 965
    new-instance v0, Lcom/google/android/videos/ui/VerticalsHelper;

    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->mainHandler:Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/videos/ui/VerticalsHelper;-><init>(Landroid/os/Handler;Lcom/google/android/videos/Config;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/PurchaseStore;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    .line 968
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v0

    new-instance v1, Lcom/google/android/videos/VideosGlobalsImpl$5;

    invoke-direct {v1, p0}, Lcom/google/android/videos/VideosGlobalsImpl$5;-><init>(Lcom/google/android/videos/VideosGlobalsImpl;)V

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 984
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/VerticalsHelper;->updateUsersContent()V

    .line 986
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 964
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getWishlistStore()Lcom/google/android/videos/store/WishlistStore;
    .locals 3

    .prologue
    .line 724
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->wishlistStore:Lcom/google/android/videos/store/WishlistStore;

    if-nez v0, :cond_0

    .line 725
    new-instance v0, Lcom/google/android/videos/store/WishlistStore;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getLocalExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/store/WishlistStore;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/videos/store/Database;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->wishlistStore:Lcom/google/android/videos/store/WishlistStore;

    .line 727
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->wishlistStore:Lcom/google/android/videos/store/WishlistStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 724
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getWishlistStoreSync()Lcom/google/android/videos/store/WishlistStoreSync;
    .locals 6

    .prologue
    .line 713
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;

    if-nez v0, :cond_0

    .line 714
    new-instance v0, Lcom/google/android/videos/store/WishlistStoreSync;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/videos/api/ApiRequesters;->getWishlistSyncRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getSyncTaskManager()Lcom/google/android/videos/store/SyncTaskManager;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getAssetStoreSync()Lcom/google/android/videos/store/AssetStoreSync;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/store/WishlistStoreSync;-><init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/SyncTaskManager;Lcom/google/android/videos/store/AssetStoreSync;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;

    .line 717
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;

    invoke-virtual {v0}, Lcom/google/android/videos/store/WishlistStoreSync;->cleanup()V

    .line 719
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 713
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getYouTubeStatsPingSender()Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;
    .locals 4

    .prologue
    .line 933
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->youtubeStatsPingSender:Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;

    if-nez v0, :cond_0

    .line 934
    new-instance v0, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNetworkExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;-><init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->youtubeStatsPingSender:Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;

    .line 937
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->youtubeStatsPingSender:Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 933
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public hasPremiumError()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 484
    iget v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->premiumStatus:I

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized legacyDownloadsHaveAppLevelDrm()Z
    .locals 5

    .prologue
    .line 553
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->legacyDownloadsHaveAppLevelDrm:Ljava/lang/Boolean;

    if-nez v2, :cond_0

    .line 554
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 555
    .local v1, "preferences":Landroid/content/SharedPreferences;
    const-string v2, "legacy_downloads_have_app_level_drm"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 556
    const-string v2, "legacy_downloads_have_app_level_drm"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->legacyDownloadsHaveAppLevelDrm:Ljava/lang/Boolean;

    .line 566
    .end local v1    # "preferences":Landroid/content/SharedPreferences;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->legacyDownloadsHaveAppLevelDrm:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    monitor-exit p0

    return v2

    .line 559
    .restart local v1    # "preferences":Landroid/content/SharedPreferences;
    :cond_1
    :try_start_1
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->applicationContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    const-string v3, "IDM"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    .local v0, "appDrmFolder":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl;->legacyDownloadsHaveAppLevelDrm:Ljava/lang/Boolean;

    .line 561
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "legacy_downloads_have_app_level_drm"

    iget-object v4, p0, Lcom/google/android/videos/VideosGlobalsImpl;->legacyDownloadsHaveAppLevelDrm:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 553
    .end local v0    # "appDrmFolder":Ljava/io/File;
    .end local v1    # "preferences":Landroid/content/SharedPreferences;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized onTrimMemory(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 312
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    invoke-virtual {v0}, Lcom/google/android/videos/utils/ByteArrayPool;->clear()V

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->globalBitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;

    if-eqz v0, :cond_1

    .line 316
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->globalBitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;

    invoke-virtual {v0}, Lcom/google/android/videos/bitmap/BitmapLruCache;->evictAll()V

    .line 318
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->getNetworkExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/google/android/videos/VideosGlobalsImpl$2;

    invoke-direct {v1, p0}, Lcom/google/android/videos/VideosGlobalsImpl$2;-><init>(Lcom/google/android/videos/VideosGlobalsImpl;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330
    monitor-exit p0

    return-void

    .line 312
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public refreshContentRestrictions()Z
    .locals 2

    .prologue
    .line 448
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/videos/VideosGlobalsImpl;->refreshContentRestrictionsV18()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updatePremiumStatus()I
    .locals 4

    .prologue
    .line 469
    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->config:Lcom/google/android/videos/Config;

    invoke-interface {v0}, Lcom/google/android/videos/Config;->needsSystemUpdate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 470
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->premiumStatus:I

    .line 479
    :goto_0
    iget v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->premiumStatus:I

    return v0

    .line 471
    :cond_0
    iget v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->versionCode:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->config:Lcom/google/android/videos/Config;

    invoke-interface {v0}, Lcom/google/android/videos/Config;->minimumVersion()I

    move-result v0

    iget v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->versionCode:I

    if-gt v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->config:Lcom/google/android/videos/Config;

    invoke-interface {v0}, Lcom/google/android/videos/Config;->blacklistedVersionsRegex()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/VideosGlobalsImpl;->versionCode:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 473
    :cond_1
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->premiumStatus:I

    goto :goto_0

    .line 474
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide v2, 0x148c901a000L

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    .line 475
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->premiumStatus:I

    goto :goto_0

    .line 477
    :cond_3
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/videos/VideosGlobalsImpl;->premiumStatus:I

    goto :goto_0
.end method
