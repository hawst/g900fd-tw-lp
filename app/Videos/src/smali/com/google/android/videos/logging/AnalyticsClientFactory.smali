.class public Lcom/google/android/videos/logging/AnalyticsClientFactory;
.super Ljava/lang/Object;
.source "AnalyticsClientFactory.java"


# direct methods
.method public static newInstance(Landroid/content/Context;Landroid/telephony/TelephonyManager;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/accounts/SignInManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)Lcom/google/android/videos/logging/AnalyticsClient;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "telephonyManager"    # Landroid/telephony/TelephonyManager;
    .param p2, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p3, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p4, "applicationVersion"    # Ljava/lang/String;
    .param p5, "userAgent"    # Ljava/lang/String;
    .param p6, "experimentId"    # Ljava/lang/String;
    .param p7, "playLogServerUrl"    # Ljava/lang/String;
    .param p8, "gServicesId"    # J
    .param p10, "deviceCountry"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-static/range {p7 .. p7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    new-instance v0, Lcom/google/android/videos/logging/DummyAnalyticsClient;

    invoke-direct {v0}, Lcom/google/android/videos/logging/DummyAnalyticsClient;-><init>()V

    .line 27
    :goto_0
    return-object v0

    .line 26
    :cond_0
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v7, 0x0

    .line 27
    .local v7, "experiments":[Ljava/lang/String;
    :goto_1
    new-instance v0, Lcom/google/android/videos/logging/PlayAnalyticsClient;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p1

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-wide/from16 v8, p8

    move-object/from16 v10, p10

    move-object/from16 v11, p7

    invoke-direct/range {v0 .. v11}, Lcom/google/android/videos/logging/PlayAnalyticsClient;-><init>(Landroid/content/Context;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/accounts/SignInManager;Landroid/telephony/TelephonyManager;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 26
    .end local v7    # "experiments":[Ljava/lang/String;
    :cond_1
    const/4 v0, 0x1

    new-array v7, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p6, v7, v0

    goto :goto_1
.end method
