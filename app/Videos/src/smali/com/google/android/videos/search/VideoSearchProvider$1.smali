.class Lcom/google/android/videos/search/VideoSearchProvider$1;
.super Ljava/lang/Object;
.source "VideoSearchProvider.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/search/VideoSearchProvider;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Ljava/lang/String;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/search/VideoSearchProvider;


# direct methods
.method constructor <init>(Lcom/google/android/videos/search/VideoSearchProvider;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/google/android/videos/search/VideoSearchProvider$1;->this$0:Lcom/google/android/videos/search/VideoSearchProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public convertRequest(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p1, "request"    # Ljava/lang/String;

    .prologue
    .line 126
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 123
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/search/VideoSearchProvider$1;->convertRequest(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
