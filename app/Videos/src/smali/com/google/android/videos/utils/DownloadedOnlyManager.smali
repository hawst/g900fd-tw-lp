.class public Lcom/google/android/videos/utils/DownloadedOnlyManager;
.super Ljava/lang/Object;
.source "DownloadedOnlyManager.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;
    }
.end annotation


# instance fields
.field private isRegistered:Z

.field private final listeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final preferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 1
    .param p1, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/videos/utils/DownloadedOnlyManager;->preferences:Landroid/content/SharedPreferences;

    .line 28
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/utils/DownloadedOnlyManager;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 29
    return-void
.end method

.method private updateRegistration()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    iget-object v3, p0, Lcom/google/android/videos/utils/DownloadedOnlyManager;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    .line 50
    .local v0, "shouldRegister":Z
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isRegistered:Z

    if-nez v3, :cond_2

    if-eqz v0, :cond_2

    .line 51
    iget-object v2, p0, Lcom/google/android/videos/utils/DownloadedOnlyManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v2, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 52
    iput-boolean v1, p0, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isRegistered:Z

    .line 57
    :cond_0
    :goto_1
    return-void

    .end local v0    # "shouldRegister":Z
    :cond_1
    move v0, v2

    .line 49
    goto :goto_0

    .line 53
    .restart local v0    # "shouldRegister":Z
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isRegistered:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 54
    iget-object v1, p0, Lcom/google/android/videos/utils/DownloadedOnlyManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 55
    iput-boolean v2, p0, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isRegistered:Z

    goto :goto_1
.end method


# virtual methods
.method public isDownloadedOnly()Z
    .locals 3

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/utils/DownloadedOnlyManager;->preferences:Landroid/content/SharedPreferences;

    const-string v1, "downloaded_only"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 69
    const-string v3, "downloaded_only"

    invoke-static {p2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isDownloadedOnly()Z

    move-result v1

    .line 71
    .local v1, "isDownloadedOnly":Z
    iget-object v3, p0, Lcom/google/android/videos/utils/DownloadedOnlyManager;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;

    .line 72
    .local v2, "listener":Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;
    invoke-interface {v2, v1}, Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;->onDownloadedOnlyChanged(Z)V

    goto :goto_0

    .line 75
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "isDownloadedOnly":Z
    .end local v2    # "listener":Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;
    :cond_0
    return-void
.end method

.method public registerObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->registerObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;Z)V

    .line 33
    return-void
.end method

.method public registerObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;Z)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;
    .param p2, "sendInitialState"    # Z

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/utils/DownloadedOnlyManager;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    invoke-direct {p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->updateRegistration()V

    .line 38
    if-eqz p2, :cond_0

    .line 39
    invoke-virtual {p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isDownloadedOnly()Z

    move-result v0

    invoke-interface {p1, v0}, Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;->onDownloadedOnlyChanged(Z)V

    .line 41
    :cond_0
    return-void
.end method

.method public setDownloadedOnly(Z)V
    .locals 2
    .param p1, "downloadedOnly"    # Z

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/videos/utils/DownloadedOnlyManager;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "downloaded_only"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 65
    return-void
.end method

.method public unregisterObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/videos/utils/DownloadedOnlyManager;->listeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 45
    invoke-direct {p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->updateRegistration()V

    .line 46
    return-void
.end method
