.class public interface abstract Lcom/google/android/videos/store/Database$Listener;
.super Ljava/lang/Object;
.source "Database.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/Database;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onMovieMetadataUpdated(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onMovieUserAssetsUpdated(Ljava/lang/String;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onPosterUpdated(Ljava/lang/String;)V
.end method

.method public abstract onPurchasesUpdated(Ljava/lang/String;)V
.end method

.method public abstract onScreenshotUpdated(Ljava/lang/String;)V
.end method

.method public abstract onShowBannerUpdated(Ljava/lang/String;)V
.end method

.method public abstract onShowMetadataUpdated(Ljava/lang/String;)V
.end method

.method public abstract onShowPosterUpdated(Ljava/lang/String;)V
.end method

.method public abstract onShowUserAssetsUpdated(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onWatchTimestampsUpdated(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onWishlistUpdated(Ljava/lang/String;)V
.end method
