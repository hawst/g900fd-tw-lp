.class final Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$PurchaseStoreSyncCallback;
.super Ljava/lang/Object;
.source "FreeMoviePromoRedeemer.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PurchaseStoreSyncCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$PurchaseStoreSyncCallback;->this$0:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;
    .param p2, "x1"    # Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$1;

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$PurchaseStoreSyncCallback;-><init>(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;)V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$PurchaseStoreSyncCallback;->this$0:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    # getter for: Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->listener:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;
    invoke-static {v0}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->access$500(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;)Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;->onPromoRedeemError(Ljava/lang/Exception;)V

    .line 82
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 71
    check-cast p1, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$PurchaseStoreSyncCallback;->onError(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    .param p2, "response"    # Ljava/lang/Void;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$PurchaseStoreSyncCallback;->this$0:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    # getter for: Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->listener:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;
    invoke-static {v0}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->access$500(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;)Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;->onPromoRedeemed()V

    .line 78
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 71
    check-cast p1, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Void;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$PurchaseStoreSyncCallback;->onResponse(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;)V

    return-void
.end method
