.class Lcom/google/android/videos/store/PurchaseStoreSync$1;
.super Ljava/lang/Object;
.source "PurchaseStoreSync.java"

# interfaces
.implements Lcom/google/android/videos/async/NewCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/store/PurchaseStoreSync;->syncPurchases(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/NewCallback",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

.field final synthetic val$callback:Lcom/google/android/videos/async/NewCallback;


# direct methods
.method constructor <init>(Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/async/NewCallback;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lcom/google/android/videos/store/PurchaseStoreSync$1;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    iput-object p2, p0, Lcom/google/android/videos/store/PurchaseStoreSync$1;->val$callback:Lcom/google/android/videos/async/NewCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private syncCompleted()V
    .locals 8

    .prologue
    .line 282
    iget-object v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$1;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->preferences:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$000(Lcom/google/android/videos/store/PurchaseStoreSync;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "initial_sync_completed"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_0

    .line 283
    iget-object v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$1;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->preferences:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$000(Lcom/google/android/videos/store/PurchaseStoreSync;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "initial_sync_completed"

    const/4 v6, 0x1

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 285
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 286
    .local v2, "now":J
    iget-object v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$1;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->preferences:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$000(Lcom/google/android/videos/store/PurchaseStoreSync;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "last_analyze_timestamp"

    const-wide/16 v6, 0x0

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 287
    .local v0, "lastAnalyzeTimestamp":J
    cmp-long v4, v0, v2

    if-gez v4, :cond_1

    const-wide/32 v4, 0x240c8400

    add-long/2addr v4, v0

    cmp-long v4, v4, v2

    if-gez v4, :cond_2

    .line 288
    :cond_1
    iget-object v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$1;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v4}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/videos/store/Database;->analyzeDatabase()V

    .line 289
    iget-object v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$1;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->preferences:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$000(Lcom/google/android/videos/store/PurchaseStoreSync;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "last_analyze_timestamp"

    invoke-interface {v4, v5, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 291
    :cond_2
    return-void
.end method


# virtual methods
.method public bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 263
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/store/PurchaseStoreSync$1;->onCancelled(Ljava/lang/String;)V

    return-void
.end method

.method public onCancelled(Ljava/lang/String;)V
    .locals 1
    .param p1, "request"    # Ljava/lang/String;

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$1;->val$callback:Lcom/google/android/videos/async/NewCallback;

    invoke-interface {v0, p1}, Lcom/google/android/videos/async/NewCallback;->onCancelled(Ljava/lang/Object;)V

    .line 279
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 263
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/PurchaseStoreSync$1;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "error"    # Ljava/lang/Exception;

    .prologue
    .line 272
    invoke-direct {p0}, Lcom/google/android/videos/store/PurchaseStoreSync$1;->syncCompleted()V

    .line 273
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$1;->val$callback:Lcom/google/android/videos/async/NewCallback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/async/NewCallback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 274
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 263
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Void;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/PurchaseStoreSync$1;->onResponse(Ljava/lang/String;Ljava/lang/Void;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Ljava/lang/Void;)V
    .locals 1
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "response"    # Ljava/lang/Void;

    .prologue
    .line 266
    invoke-direct {p0}, Lcom/google/android/videos/store/PurchaseStoreSync$1;->syncCompleted()V

    .line 267
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$1;->val$callback:Lcom/google/android/videos/async/NewCallback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/async/NewCallback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 268
    return-void
.end method
