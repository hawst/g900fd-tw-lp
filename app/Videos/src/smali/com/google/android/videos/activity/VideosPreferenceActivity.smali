.class public abstract Lcom/google/android/videos/activity/VideosPreferenceActivity;
.super Landroid/preference/PreferenceActivity;
.source "VideosPreferenceActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract configureSettings(Lcom/google/android/videos/activity/VideosPreferenceFragment;)V
.end method

.method public isValidFragment(Ljava/lang/String;)Z
    .locals 1
    .param p1, "fragmentName"    # Ljava/lang/String;

    .prologue
    .line 43
    const/4 v0, 0x1

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/google/android/videos/activity/VideosPreferenceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, ":android:no_headers"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    const-string v3, ":android:show_fragment"

    const-class v4, Lcom/google/android/videos/activity/VideosPreferenceFragment;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 22
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 24
    invoke-virtual {p0}, Lcom/google/android/videos/activity/VideosPreferenceActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 25
    .local v0, "actionBar":Landroid/app/ActionBar;
    const/16 v1, 0xc

    .line 26
    .local v1, "displayOptions":I
    invoke-virtual {v0, v1, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 27
    const v2, 0x7f0a0079

    invoke-static {p0, v0, v2}, Lcom/google/android/videos/ui/DogfoodHelper;->addPawsIfNeeded(Landroid/content/Context;Landroid/app/ActionBar;I)V

    .line 28
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 32
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 37
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 34
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/videos/activity/VideosPreferenceActivity;->finish()V

    .line 35
    const/4 v0, 0x1

    goto :goto_0

    .line 32
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
