.class public Lcom/google/android/videos/adapter/WishlistDataSource;
.super Lcom/google/android/videos/adapter/CursorDataSource;
.source "WishlistDataSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/adapter/WishlistDataSource$Query;
    }
.end annotation


# instance fields
.field private final removingMovies:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final removingShows:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final wishlistedMovies:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final wishlistedShows:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/videos/adapter/CursorDataSource;-><init>()V

    .line 28
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->wishlistedMovies:Ljava/util/Set;

    .line 29
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->wishlistedShows:Ljava/util/Set;

    .line 30
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->removingMovies:Ljava/util/Set;

    .line 31
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->removingShows:Ljava/util/Set;

    .line 32
    return-void
.end method


# virtual methods
.method public changeCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->wishlistedMovies:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 115
    iget-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->wishlistedShows:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 116
    if-eqz p1, :cond_0

    .line 117
    const/4 v0, -0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 118
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/WishlistDataSource;->getItemType(Landroid/database/Cursor;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 121
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->wishlistedMovies:Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/WishlistDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 124
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->wishlistedShows:Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/WishlistDataSource;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->removingMovies:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->wishlistedMovies:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 130
    iget-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->removingShows:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->wishlistedShows:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 131
    invoke-super {p0, p1}, Lcom/google/android/videos/adapter/CursorDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 132
    return-void

    .line 119
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method public getAllWishlistedMovies(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 145
    .local p1, "movieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->wishlistedMovies:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 146
    return-void
.end method

.method public getAllWishlistedShows(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 149
    .local p1, "showIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->wishlistedShows:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 150
    return-void
.end method

.method public getItemIdentifier(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 179
    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/WishlistDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    .line 180
    .local v0, "item":Landroid/database/Cursor;
    invoke-virtual {p0, v0}, Lcom/google/android/videos/adapter/WishlistDataSource;->getItemType(Landroid/database/Cursor;)I

    move-result v1

    .line 181
    .local v1, "itemType":I
    const/4 v2, 0x6

    if-ne v1, v2, :cond_0

    .line 182
    invoke-virtual {p0, v0}, Lcom/google/android/videos/adapter/WishlistDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromMovieId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 184
    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/videos/adapter/WishlistDataSource;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromShowId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getItemType(Landroid/database/Cursor;)I
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getShowId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 105
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getShowTitle(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 109
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 101
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVideoId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 97
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isRemoving(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 153
    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/WishlistDataSource;->getItemType(Landroid/database/Cursor;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 159
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 155
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->removingMovies:Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/WishlistDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 157
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->removingShows:Ljava/util/Set;

    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/WishlistDataSource;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 153
    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method public setRemoving(ILjava/lang/String;)V
    .locals 1
    .param p1, "itemType"    # I
    .param p2, "itemId"    # Ljava/lang/String;

    .prologue
    .line 163
    sparse-switch p1, :sswitch_data_0

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 165
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->wishlistedMovies:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->removingMovies:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    invoke-virtual {p0}, Lcom/google/android/videos/adapter/WishlistDataSource;->notifyChanged()V

    goto :goto_0

    .line 170
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->wishlistedShows:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/adapter/WishlistDataSource;->removingShows:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {p0}, Lcom/google/android/videos/adapter/WishlistDataSource;->notifyChanged()V

    goto :goto_0

    .line 163
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method
