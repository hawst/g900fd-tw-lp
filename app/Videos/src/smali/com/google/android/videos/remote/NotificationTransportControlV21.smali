.class public Lcom/google/android/videos/remote/NotificationTransportControlV21;
.super Lcom/google/android/videos/remote/NotificationTransportControl;
.source "NotificationTransportControlV21.java"


# instance fields
.field private mediaSession:Landroid/media/session/MediaSession;


# direct methods
.method protected constructor <init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)V
    .locals 0
    .param p1, "remoteTracker"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/remote/NotificationTransportControl;-><init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method private updateMediaSession()V
    .locals 4

    .prologue
    .line 98
    new-instance v1, Landroid/media/MediaMetadata$Builder;

    invoke-direct {v1}, Landroid/media/MediaMetadata$Builder;-><init>()V

    const-string v2, "android.media.metadata.ALBUM_ART"

    iget-object v3, p0, Lcom/google/android/videos/remote/NotificationTransportControlV21;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v3}, Lcom/google/android/videos/remote/RemoteTracker;->getPosterBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaMetadata$Builder;->putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/media/MediaMetadata$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaMetadata$Builder;->build()Landroid/media/MediaMetadata;

    move-result-object v0

    .line 101
    .local v0, "metadata":Landroid/media/MediaMetadata;
    iget-object v1, p0, Lcom/google/android/videos/remote/NotificationTransportControlV21;->mediaSession:Landroid/media/session/MediaSession;

    invoke-virtual {v1, v0}, Landroid/media/session/MediaSession;->setMetadata(Landroid/media/MediaMetadata;)V

    .line 102
    return-void
.end method


# virtual methods
.method protected createNotification(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V
    .locals 3
    .param p1, "service"    # Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    .prologue
    .line 34
    new-instance v0, Landroid/media/session/MediaSession;

    invoke-virtual {p0}, Lcom/google/android/videos/remote/NotificationTransportControlV21;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/media/session/MediaSession;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControlV21;->mediaSession:Landroid/media/session/MediaSession;

    .line 35
    iget-object v0, p0, Lcom/google/android/videos/remote/NotificationTransportControlV21;->mediaSession:Landroid/media/session/MediaSession;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setActive(Z)V

    .line 37
    const-string v0, "Creating notification"

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 38
    invoke-virtual {p0, p1}, Lcom/google/android/videos/remote/NotificationTransportControlV21;->updateNotification(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V

    .line 39
    return-void
.end method

.method protected destroyNotification()V
    .locals 3

    .prologue
    .line 106
    const-string v1, "Destroying notification"

    invoke-static {v1}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 107
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 108
    invoke-virtual {p0}, Lcom/google/android/videos/remote/NotificationTransportControlV21;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 110
    .local v0, "manager":Landroid/app/NotificationManager;
    const v1, 0x7f0f003b

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 112
    iget-object v1, p0, Lcom/google/android/videos/remote/NotificationTransportControlV21;->mediaSession:Landroid/media/session/MediaSession;

    invoke-virtual {v1}, Landroid/media/session/MediaSession;->release()V

    .line 113
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/videos/remote/NotificationTransportControlV21;->mediaSession:Landroid/media/session/MediaSession;

    .line 114
    return-void
.end method

.method public updateNotification(Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;)V
    .locals 9
    .param p1, "service"    # Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 43
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 45
    invoke-virtual {p0}, Lcom/google/android/videos/remote/NotificationTransportControlV21;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 46
    .local v1, "context":Landroid/content/Context;
    new-instance v4, Landroid/app/Notification$Builder;

    invoke-direct {v4, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v8}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v4

    const v5, 0x7f0200db

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/remote/NotificationTransportControlV21;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v5}, Lcom/google/android/videos/remote/RemoteTracker;->getPosterBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/videos/remote/NotificationTransportControlV21;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/videos/remote/NotificationTransportControlV21;->getStatus()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/Notification$Builder;->setShowWhen(Z)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;

    move-result-object v4

    const/4 v5, 0x7

    invoke-virtual {p1, v5}, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->getPendingIntent(I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v4

    const/4 v5, 0x6

    invoke-virtual {p1, v5}, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->getPendingIntent(I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00e3

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v4

    new-instance v5, Landroid/app/Notification$MediaStyle;

    invoke-direct {v5}, Landroid/app/Notification$MediaStyle;-><init>()V

    iget-object v6, p0, Lcom/google/android/videos/remote/NotificationTransportControlV21;->mediaSession:Landroid/media/session/MediaSession;

    invoke-virtual {v6}, Landroid/media/session/MediaSession;->getSessionToken()Landroid/media/session/MediaSession$Token;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/Notification$MediaStyle;->setMediaSession(Landroid/media/session/MediaSession$Token;)Landroid/app/Notification$MediaStyle;

    move-result-object v5

    new-array v6, v8, [I

    aput v7, v6, v7

    invoke-virtual {v5, v6}, Landroid/app/Notification$MediaStyle;->setShowActionsInCompactView([I)Landroid/app/Notification$MediaStyle;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 62
    .local v0, "builder":Landroid/app/Notification$Builder;
    iget-object v4, p0, Lcom/google/android/videos/remote/NotificationTransportControlV21;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v4}, Lcom/google/android/videos/remote/RemoteTracker;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v2

    .line 63
    .local v2, "playerState":Lcom/google/android/videos/remote/PlayerState;
    iget-object v4, p0, Lcom/google/android/videos/remote/NotificationTransportControlV21;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v4}, Lcom/google/android/videos/remote/RemoteTracker;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v3

    .line 65
    .local v3, "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 66
    iget v4, v3, Lcom/google/android/videos/remote/RemoteVideoInfo;->durationMillis:I

    iget v5, v2, Lcom/google/android/videos/remote/PlayerState;->time:I

    invoke-virtual {v0, v4, v5, v7}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 69
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/remote/NotificationTransportControlV21;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v4}, Lcom/google/android/videos/remote/RemoteTracker;->getError()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    if-eqz v2, :cond_1

    if-eqz v3, :cond_1

    .line 70
    iget v4, v2, Lcom/google/android/videos/remote/PlayerState;->state:I

    packed-switch v4, :pswitch_data_0

    .line 89
    :cond_1
    :goto_0
    const v4, 0x7f0200de

    const v5, 0x7f0b01d0

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x5

    invoke-virtual {p1, v6}, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->getPendingIntent(I)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v0, v4, v5, v6}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 93
    const v4, 0x7f0f003b

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->startForeground(ILandroid/app/Notification;)V

    .line 94
    invoke-direct {p0}, Lcom/google/android/videos/remote/NotificationTransportControlV21;->updateMediaSession()V

    .line 95
    return-void

    .line 72
    :pswitch_0
    const v4, 0x7f0200df

    const v5, 0x7f0b01c1

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v8}, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->getPendingIntent(I)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v0, v4, v5, v6}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    goto :goto_0

    .line 77
    :pswitch_1
    const v4, 0x7f0200e0

    const v5, 0x7f0b01c0

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v7}, Lcom/google/android/videos/remote/NotificationTransportControl$ListenerService;->getPendingIntent(I)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual {v0, v4, v5, v6}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    goto :goto_0

    .line 70
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
