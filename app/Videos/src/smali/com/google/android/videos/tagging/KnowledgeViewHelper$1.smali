.class Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;
.super Landroid/os/AsyncTask;
.source "KnowledgeViewHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/tagging/KnowledgeViewHelper;->loadChunkData(I)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/android/videos/tagging/ChunkData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

.field final synthetic val$chunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

.field final synthetic val$currentKnowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;


# direct methods
.method constructor <init>(Lcom/google/android/videos/tagging/KnowledgeViewHelper;Lcom/google/android/videos/tagging/KnowledgeBundle;Lcom/google/android/videos/tagging/TagStream$ChunkInfo;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->this$0:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    iput-object p2, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->val$currentKnowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    iput-object p3, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->val$chunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/videos/tagging/ChunkData;
    .locals 3
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 178
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->val$currentKnowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    iget-object v1, v1, Lcom/google/android/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    iget-object v2, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->val$chunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/tagging/TagStream;->loadChunkData(Lcom/google/android/videos/tagging/TagStream$ChunkInfo;)Lcom/google/android/videos/tagging/ChunkData;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 181
    :goto_0
    return-object v1

    .line 179
    :catch_0
    move-exception v0

    .line 180
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to load chunk for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->val$chunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    iget v2, v2, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;->startMillis:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->val$chunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    iget v2, v2, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;->endMillis:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 181
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 174
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->doInBackground([Ljava/lang/Void;)Lcom/google/android/videos/tagging/ChunkData;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/videos/tagging/ChunkData;)V
    .locals 6
    .param p1, "result"    # Lcom/google/android/videos/tagging/ChunkData;

    .prologue
    const/4 v2, 0x0

    .line 186
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->this$0:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    # getter for: Lcom/google/android/videos/tagging/KnowledgeViewHelper;->loadingChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;
    invoke-static {v0}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->access$000(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->val$chunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    if-eq v0, v1, :cond_1

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->this$0:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->val$chunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    # setter for: Lcom/google/android/videos/tagging/KnowledgeViewHelper;->currentChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;
    invoke-static {v0, v1}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->access$102(Lcom/google/android/videos/tagging/KnowledgeViewHelper;Lcom/google/android/videos/tagging/TagStream$ChunkInfo;)Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    .line 190
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->this$0:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    # setter for: Lcom/google/android/videos/tagging/KnowledgeViewHelper;->currentChunkData:Lcom/google/android/videos/tagging/ChunkData;
    invoke-static {v0, p1}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->access$202(Lcom/google/android/videos/tagging/KnowledgeViewHelper;Lcom/google/android/videos/tagging/ChunkData;)Lcom/google/android/videos/tagging/ChunkData;

    .line 191
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->this$0:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/videos/tagging/KnowledgeViewHelper;->loadingChunkInfo:Lcom/google/android/videos/tagging/TagStream$ChunkInfo;
    invoke-static {v0, v1}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->access$002(Lcom/google/android/videos/tagging/KnowledgeViewHelper;Lcom/google/android/videos/tagging/TagStream$ChunkInfo;)Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    .line 192
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->this$0:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    # getter for: Lcom/google/android/videos/tagging/KnowledgeViewHelper;->showKnowledgeAtMillis:I
    invoke-static {v0}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->access$300(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->this$0:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    # invokes: Lcom/google/android/videos/tagging/KnowledgeViewHelper;->showTaggedKnowledge()V
    invoke-static {v0}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->access$400(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)V

    .line 194
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->this$0:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    # getter for: Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;
    invoke-static {v0}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->access$500(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)Lcom/google/android/videos/tagging/KnowledgeView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->this$0:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    # getter for: Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;
    invoke-static {v0}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->access$500(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)Lcom/google/android/videos/tagging/KnowledgeView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->this$0:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    # getter for: Lcom/google/android/videos/tagging/KnowledgeViewHelper;->showKnowledgeAtMillis:I
    invoke-static {v1}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->access$300(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)I

    move-result v1

    iget-object v3, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->this$0:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    # getter for: Lcom/google/android/videos/tagging/KnowledgeViewHelper;->taggedKnowledgeEntities:Ljava/util/List;
    invoke-static {v3}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->access$600(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)Ljava/util/List;

    move-result-object v4

    iget-object v3, p0, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->this$0:Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    # getter for: Lcom/google/android/videos/tagging/KnowledgeViewHelper;->showRecentActorsWithinMillis:I
    invoke-static {v3}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->access$700(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)I

    move-result v5

    move v3, v2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/videos/tagging/KnowledgeView;->showPausedKnowledge(IIILjava/util/List;I)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 174
    check-cast p1, Lcom/google/android/videos/tagging/ChunkData;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/KnowledgeViewHelper$1;->onPostExecute(Lcom/google/android/videos/tagging/ChunkData;)V

    return-void
.end method
