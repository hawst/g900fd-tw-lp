.class Lcom/google/android/videos/activity/WatchActivityCompat$V17;
.super Lcom/google/android/videos/activity/WatchActivityCompat$V11;
.source "WatchActivityCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/WatchActivityCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V17"
.end annotation


# instance fields
.field private final mediaRouteCallback:Landroid/media/MediaRouter$SimpleCallback;

.field private final mediaRouter:Landroid/media/MediaRouter;

.field private presentationDisplayRoute:Landroid/media/MediaRouter$RouteInfo;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/activity/WatchActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/google/android/videos/activity/WatchActivity;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/WatchActivityCompat$V11;-><init>(Lcom/google/android/videos/activity/WatchActivity;)V

    .line 169
    new-instance v0, Lcom/google/android/videos/activity/WatchActivityCompat$V17$1;

    invoke-direct {v0, p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V17$1;-><init>(Lcom/google/android/videos/activity/WatchActivityCompat$V17;)V

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->mediaRouteCallback:Landroid/media/MediaRouter$SimpleCallback;

    .line 83
    const-string v0, "media_router"

    invoke-virtual {p1, v0}, Lcom/google/android/videos/activity/WatchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaRouter;

    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->mediaRouter:Landroid/media/MediaRouter;

    .line 84
    invoke-direct {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->updateDisplays()V

    .line 85
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/activity/WatchActivityCompat$V17;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/WatchActivityCompat$V17;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->updateDisplays()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/activity/WatchActivityCompat$V17;)Landroid/media/MediaRouter$RouteInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/WatchActivityCompat$V17;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->presentationDisplayRoute:Landroid/media/MediaRouter$RouteInfo;

    return-object v0
.end method

.method private supportsLiveVideo(Landroid/media/MediaRouter$RouteInfo;)Z
    .locals 2
    .param p1, "routeInfo"    # Landroid/media/MediaRouter$RouteInfo;

    .prologue
    .line 140
    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getSupportedTypes()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private supportsPresentationDisplay(Landroid/media/MediaRouter$RouteInfo;)Z
    .locals 5
    .param p1, "routeInfo"    # Landroid/media/MediaRouter$RouteInfo;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 150
    :try_start_0
    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v0

    .line 151
    .local v0, "display":Landroid/view/Display;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Display;->isValid()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getPlaybackType()I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-ne v4, v2, :cond_2

    .line 155
    .end local v0    # "display":Landroid/view/Display;
    :cond_1
    :goto_0
    return v2

    .restart local v0    # "display":Landroid/view/Display;
    :cond_2
    move v2, v3

    .line 151
    goto :goto_0

    .line 153
    .end local v0    # "display":Landroid/view/Display;
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/lang/NoSuchMethodError;
    move v2, v3

    .line 155
    goto :goto_0
.end method

.method private updateDisplays()V
    .locals 3

    .prologue
    .line 120
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->mediaRouter:Landroid/media/MediaRouter;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/media/MediaRouter;->getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v0

    .line 122
    .local v0, "newRoute":Landroid/media/MediaRouter$RouteInfo;
    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->supportsLiveVideo(Landroid/media/MediaRouter$RouteInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->supportsPresentationDisplay(Landroid/media/MediaRouter$RouteInfo;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 124
    :cond_0
    const/4 v0, 0x0

    .line 127
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->presentationDisplayRoute:Landroid/media/MediaRouter$RouteInfo;

    if-eq v1, v0, :cond_2

    .line 128
    iput-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->presentationDisplayRoute:Landroid/media/MediaRouter$RouteInfo;

    .line 129
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->updateOrientation()V

    .line 130
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->updateScreenBrightness()V

    .line 131
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {v1}, Lcom/google/android/videos/activity/WatchActivity;->onPresentationDisplayRouteChanged()V

    .line 132
    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->activity:Lcom/google/android/videos/activity/WatchActivity;

    invoke-virtual {v1}, Lcom/google/android/videos/activity/WatchActivity;->supportInvalidateOptionsMenu()V

    .line 134
    :cond_2
    return-void
.end method


# virtual methods
.method public getPresentationDisplayRoute()Landroid/media/MediaRouter$RouteInfo;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->presentationDisplayRoute:Landroid/media/MediaRouter$RouteInfo;

    return-object v0
.end method

.method public initPlayback(Lcom/google/android/videos/player/Director;ZZ)V
    .locals 1
    .param p1, "director"    # Lcom/google/android/videos/player/Director;
    .param p2, "firstResume"    # Z
    .param p3, "toggled"    # Z

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->presentationDisplayRoute:Landroid/media/MediaRouter$RouteInfo;

    if-eqz v0, :cond_2

    .line 95
    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/videos/player/Director;->initPlayback(Z)V

    .line 99
    :goto_1
    return-void

    .line 95
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 97
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->initPlayback(Lcom/google/android/videos/player/Director;ZZ)V

    goto :goto_1
.end method

.method protected isConnectedToExternalDisplay()Z
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->presentationDisplayRoute:Landroid/media/MediaRouter$RouteInfo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected lockOrientation()Z
    .locals 1

    .prologue
    .line 161
    invoke-super {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->lockOrientation()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->presentationDisplayRoute:Landroid/media/MediaRouter$RouteInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onHdmiPluggedState(Z)V
    .locals 0
    .param p1, "plugged"    # Z

    .prologue
    .line 117
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 103
    invoke-super {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->onStart()V

    .line 104
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->mediaRouter:Landroid/media/MediaRouter;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->mediaRouteCallback:Landroid/media/MediaRouter$SimpleCallback;

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaRouter;->addCallback(ILandroid/media/MediaRouter$Callback;)V

    .line 105
    invoke-direct {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->updateDisplays()V

    .line 106
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->mediaRouter:Landroid/media/MediaRouter;

    iget-object v1, p0, Lcom/google/android/videos/activity/WatchActivityCompat$V17;->mediaRouteCallback:Landroid/media/MediaRouter$SimpleCallback;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter;->removeCallback(Landroid/media/MediaRouter$Callback;)V

    .line 111
    invoke-super {p0}, Lcom/google/android/videos/activity/WatchActivityCompat$V11;->onStop()V

    .line 112
    return-void
.end method
