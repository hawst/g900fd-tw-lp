.class Lcom/google/android/videos/activity/MyLibraryFragment$1;
.super Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;
.source "MyLibraryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/activity/MyLibraryFragment;->createHeaderListLayoutConfigurator()Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/MyLibraryFragment;


# direct methods
.method constructor <init>(Lcom/google/android/videos/activity/MyLibraryFragment;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/videos/activity/MyLibraryFragment$1;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    invoke-direct {p0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 124
    new-instance v1, Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 125
    .local v1, "view":Landroid/view/View;
    iget-object v2, p0, Lcom/google/android/videos/activity/MyLibraryFragment$1;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    invoke-virtual {v2}, Lcom/google/android/videos/activity/MyLibraryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0079

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 126
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/google/android/videos/activity/MyLibraryFragment$1;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/videos/activity/MyLibraryFragment;->getExtendedActionBarHeight(Landroid/content/Context;)I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 128
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p2, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 129
    return-void
.end method

.method protected addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 115
    const v1, 0x7f040078

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 116
    .local v0, "contentView":Landroid/view/View;
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 117
    iget-object v2, p0, Lcom/google/android/videos/activity/MyLibraryFragment$1;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    const v1, 0x7f0f00c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    # setter for: Lcom/google/android/videos/activity/MyLibraryFragment;->viewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v2, v1}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$002(Lcom/google/android/videos/activity/MyLibraryFragment;Landroid/support/v4/view/ViewPager;)Landroid/support/v4/view/ViewPager;

    .line 118
    iget-object v1, p0, Lcom/google/android/videos/activity/MyLibraryFragment$1;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    new-instance v2, Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;

    iget-object v3, p0, Lcom/google/android/videos/activity/MyLibraryFragment$1;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    iget-object v4, p0, Lcom/google/android/videos/activity/MyLibraryFragment$1;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    invoke-virtual {v4}, Lcom/google/android/videos/activity/MyLibraryFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;-><init>(Lcom/google/android/videos/activity/MyLibraryFragment;Landroid/support/v4/app/FragmentManager;)V

    # setter for: Lcom/google/android/videos/activity/MyLibraryFragment;->pagerAdapter:Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;
    invoke-static {v1, v2}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$102(Lcom/google/android/videos/activity/MyLibraryFragment;Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;)Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;

    .line 119
    iget-object v1, p0, Lcom/google/android/videos/activity/MyLibraryFragment$1;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    # getter for: Lcom/google/android/videos/activity/MyLibraryFragment;->viewPager:Landroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$000(Lcom/google/android/videos/activity/MyLibraryFragment;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/activity/MyLibraryFragment$1;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    # getter for: Lcom/google/android/videos/activity/MyLibraryFragment;->pagerAdapter:Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;
    invoke-static {v2}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$100(Lcom/google/android/videos/activity/MyLibraryFragment;)Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 120
    return-void
.end method

.method protected getHeaderBottomMargin()I
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment$1;->mContext:Landroid/content/Context;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/videos/ui/PlayListSpacerFlow;->getHeaderContentGap(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method protected getHeaderHeight()I
    .locals 3

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment$1;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/ui/PlayListSpacerFlow;->getSpacerHeight(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method

.method protected getHeaderMode()I
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x0

    return v0
.end method

.method protected getListViewId()I
    .locals 1

    .prologue
    .line 138
    const v0, 0x7f0f0029

    return v0
.end method

.method protected getTabMode()I
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x0

    return v0
.end method

.method protected getTabPaddingMode()I
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x1

    return v0
.end method

.method protected getToolbarTitleMode()I
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x1

    return v0
.end method

.method protected getViewPagerId()I
    .locals 1

    .prologue
    .line 133
    const v0, 0x7f0f00c4

    return v0
.end method

.method protected hasViewPager()Z
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x1

    return v0
.end method

.method protected useBuiltInActionBar()Z
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x1

    return v0
.end method
