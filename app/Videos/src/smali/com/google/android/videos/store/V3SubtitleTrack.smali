.class public final Lcom/google/android/videos/store/V3SubtitleTrack;
.super Ljava/lang/Object;
.source "V3SubtitleTrack.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x10f9242fca68f931L


# instance fields
.field public final format:I

.field public final languageCode:Ljava/lang/String;

.field public final languageName:Ljava/lang/String;

.field public final sourceLanguageCode:Ljava/lang/String;

.field public final trackName:Ljava/lang/String;

.field public final videoId:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/google/android/videos/store/V3SubtitleTrack;->languageCode:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/google/android/videos/store/V3SubtitleTrack;->sourceLanguageCode:Ljava/lang/String;

    .line 25
    iput-object v0, p0, Lcom/google/android/videos/store/V3SubtitleTrack;->languageName:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/google/android/videos/store/V3SubtitleTrack;->trackName:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/google/android/videos/store/V3SubtitleTrack;->videoId:Ljava/lang/String;

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/store/V3SubtitleTrack;->format:I

    .line 29
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/videos/store/V3SubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/videos/store/V3SubtitleTrack;->trackName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/store/V3SubtitleTrack;->videoId:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/videos/store/V3SubtitleTrack;->format:I

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/videos/subtitles/SubtitleTrack;->createLegacy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v0

    return-object v0
.end method
