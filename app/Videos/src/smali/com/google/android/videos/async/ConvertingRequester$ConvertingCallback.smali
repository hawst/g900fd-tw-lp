.class final Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;
.super Ljava/lang/Object;
.source "ConvertingRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/async/ConvertingRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ConvertingCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<TS;TF;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private final convertedRequest:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TS;"
        }
    .end annotation
.end field

.field private final originalCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;"
        }
    .end annotation
.end field

.field private final originalRequest:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field private response:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TF;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/videos/async/ConvertingRequester;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/ConvertingRequester;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TS;",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 121
    .local p0, "this":Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;, "Lcom/google/android/videos/async/ConvertingRequester<TR;TE;TS;TF;>.ConvertingCallback;"
    .local p2, "originalRequest":Ljava/lang/Object;, "TR;"
    .local p3, "convertedRequest":Ljava/lang/Object;, "TS;"
    .local p4, "originalCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    iput-object p1, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->this$0:Lcom/google/android/videos/async/ConvertingRequester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    iput-object p2, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->originalRequest:Ljava/lang/Object;

    .line 123
    iput-object p3, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->convertedRequest:Ljava/lang/Object;

    .line 124
    iput-object p4, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    .line 125
    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 146
    .local p0, "this":Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;, "Lcom/google/android/videos/async/ConvertingRequester<TR;TE;TS;TF;>.ConvertingCallback;"
    .local p1, "request":Ljava/lang/Object;, "TS;"
    iget-object v0, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->originalRequest:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 147
    return-void
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;TF;)V"
        }
    .end annotation

    .prologue
    .line 129
    .local p0, "this":Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;, "Lcom/google/android/videos/async/ConvertingRequester<TR;TE;TS;TF;>.ConvertingCallback;"
    .local p1, "request":Ljava/lang/Object;, "TS;"
    .local p2, "response":Ljava/lang/Object;, "TF;"
    iput-object p2, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->response:Ljava/lang/Object;

    .line 130
    iget-object v1, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->this$0:Lcom/google/android/videos/async/ConvertingRequester;

    # getter for: Lcom/google/android/videos/async/ConvertingRequester;->responseConverter:Lcom/google/android/videos/converter/ResponseConverter;
    invoke-static {v1}, Lcom/google/android/videos/async/ConvertingRequester;->access$000(Lcom/google/android/videos/async/ConvertingRequester;)Lcom/google/android/videos/converter/ResponseConverter;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 131
    iget-object v1, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->this$0:Lcom/google/android/videos/async/ConvertingRequester;

    # getter for: Lcom/google/android/videos/async/ConvertingRequester;->executor:Ljava/util/concurrent/Executor;
    invoke-static {v1}, Lcom/google/android/videos/async/ConvertingRequester;->access$100(Lcom/google/android/videos/async/ConvertingRequester;)Ljava/util/concurrent/Executor;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 132
    iget-object v1, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->this$0:Lcom/google/android/videos/async/ConvertingRequester;

    # getter for: Lcom/google/android/videos/async/ConvertingRequester;->executor:Ljava/util/concurrent/Executor;
    invoke-static {v1}, Lcom/google/android/videos/async/ConvertingRequester;->access$100(Lcom/google/android/videos/async/ConvertingRequester;)Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-interface {v1, p0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 142
    :goto_0
    return-void

    .line 134
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->run()V

    goto :goto_0

    .line 139
    :cond_1
    move-object v0, p2

    .line 140
    .local v0, "convertedResponse":Ljava/lang/Object;, "TE;"
    iget-object v1, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    iget-object v2, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->originalRequest:Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public run()V
    .locals 6

    .prologue
    .line 152
    .local p0, "this":Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;, "Lcom/google/android/videos/async/ConvertingRequester<TR;TE;TS;TF;>.ConvertingCallback;"
    :try_start_0
    iget-object v2, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->this$0:Lcom/google/android/videos/async/ConvertingRequester;

    # getter for: Lcom/google/android/videos/async/ConvertingRequester;->responseConverter:Lcom/google/android/videos/converter/ResponseConverter;
    invoke-static {v2}, Lcom/google/android/videos/async/ConvertingRequester;->access$000(Lcom/google/android/videos/async/ConvertingRequester;)Lcom/google/android/videos/converter/ResponseConverter;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->response:Ljava/lang/Object;

    invoke-interface {v2, v3}, Lcom/google/android/videos/converter/ResponseConverter;->convertResponse(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 153
    .local v0, "convertedResponse":Ljava/lang/Object;, "TE;"
    iget-object v2, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    iget-object v3, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->originalRequest:Ljava/lang/Object;

    invoke-interface {v2, v3, v0}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/videos/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_1

    .line 159
    .end local v0    # "convertedResponse":Ljava/lang/Object;, "TE;"
    :goto_0
    return-void

    .line 154
    :catch_0
    move-exception v1

    .line 155
    .local v1, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->this$0:Lcom/google/android/videos/async/ConvertingRequester;

    iget-object v3, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->originalRequest:Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->convertedRequest:Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/google/android/videos/async/ConvertingRequester;->onConvertError(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;Ljava/lang/Exception;)V

    goto :goto_0

    .line 156
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 157
    .local v1, "e":Lcom/google/android/videos/converter/ConverterException;
    iget-object v2, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->this$0:Lcom/google/android/videos/async/ConvertingRequester;

    iget-object v3, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->originalRequest:Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->convertedRequest:Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/videos/async/ConvertingRequester$ConvertingCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/google/android/videos/async/ConvertingRequester;->onConvertError(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/async/Callback;Ljava/lang/Exception;)V

    goto :goto_0
.end method
