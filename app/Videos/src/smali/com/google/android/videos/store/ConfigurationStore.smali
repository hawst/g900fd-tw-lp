.class public Lcom/google/android/videos/store/ConfigurationStore;
.super Ljava/lang/Object;
.source "ConfigurationStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;,
        Lcom/google/android/videos/store/ConfigurationStore$GetConfigProto;,
        Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;,
        Lcom/google/android/videos/store/ConfigurationStore$LoadConfiguration;
    }
.end annotation


# static fields
.field private static final ACCOUNT_COLUMN:[Ljava/lang/String;

.field private static final GET_PROJECTION:[Ljava/lang/String;

.field private static final LOAD_PROJECTION:[Ljava/lang/String;


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

.field private final config:Lcom/google/android/videos/Config;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final localExecutor:Ljava/util/concurrent/Executor;

.field private final userAccountLinks:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final userConfigGetRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UserConfigGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final userConfigGetSyncRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UserConfigGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final userCountryCodes:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 40
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "config_account"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/videos/store/ConfigurationStore;->ACCOUNT_COLUMN:[Ljava/lang/String;

    .line 47
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "config_account"

    aput-object v1, v0, v3

    const-string v1, "config_play_country"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "account_links"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/store/ConfigurationStore;->LOAD_PROJECTION:[Ljava/lang/String;

    .line 54
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "config_proto"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/videos/store/ConfigurationStore;->GET_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/videos/Config;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;)V
    .locals 1
    .param p1, "localExecutor"    # Ljava/util/concurrent/Executor;
    .param p2, "config"    # Lcom/google/android/videos/Config;
    .param p3, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/videos/Config;",
            "Lcom/google/android/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UserConfigGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UserConfigGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p5, "userConfigGetSyncRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/UserConfigGetRequest;Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;>;"
    .local p6, "userConfigGetRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/UserConfigGetRequest;Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->localExecutor:Ljava/util/concurrent/Executor;

    .line 75
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/Config;

    iput-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->config:Lcom/google/android/videos/Config;

    .line 76
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iput-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 77
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->database:Lcom/google/android/videos/store/Database;

    .line 78
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->userConfigGetSyncRequester:Lcom/google/android/videos/async/Requester;

    .line 79
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->userConfigGetRequester:Lcom/google/android/videos/async/Requester;

    .line 80
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->userCountryCodes:Ljava/util/concurrent/ConcurrentHashMap;

    .line 81
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->userAccountLinks:Ljava/util/concurrent/ConcurrentHashMap;

    .line 82
    return-void
.end method

.method static synthetic access$000()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/videos/store/ConfigurationStore;->LOAD_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/store/ConfigurationStore;)Lcom/google/android/videos/store/Database;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/ConfigurationStore;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->database:Lcom/google/android/videos/store/Database;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/store/ConfigurationStore;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/ConfigurationStore;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->userCountryCodes:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/store/ConfigurationStore;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/ConfigurationStore;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->userAccountLinks:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic access$400(I)Z
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 36
    invoke-static {p0}, Lcom/google/android/videos/store/ConfigurationStore;->isValidPartnerId(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(I)J
    .locals 2
    .param p0, "x0"    # I

    .prologue
    .line 36
    invoke-static {p0}, Lcom/google/android/videos/store/ConfigurationStore;->getPartnerFlag(I)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$600()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/videos/store/ConfigurationStore;->GET_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/videos/store/ConfigurationStore;->ACCOUNT_COLUMN:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/videos/store/ConfigurationStore;)Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/ConfigurationStore;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    return-object v0
.end method

.method private static getPartnerFlag(I)J
    .locals 4
    .param p0, "partnerId"    # I

    .prologue
    .line 206
    add-int/lit8 v0, p0, -0x1

    .line 207
    .local v0, "shift":I
    const-wide/16 v2, 0x1

    shl-long/2addr v2, v0

    return-wide v2
.end method

.method private static isValidPartnerId(I)Z
    .locals 2
    .param p0, "partnerId"    # I

    .prologue
    .line 196
    add-int/lit8 v0, p0, -0x1

    .line 197
    .local v0, "shift":I
    if-ltz v0, :cond_0

    const/16 v1, 0x40

    if-ge v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public anyVerticalEnabled(Ljava/lang/String;)Z
    .locals 2
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->config:Lcom/google/android/videos/Config;

    invoke-virtual {p0, p1}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/videos/Config;->anyVerticalEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public blockingSyncUserConfiguration(Ljava/lang/String;)V
    .locals 5
    .param p1, "account"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 175
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v1

    .line 176
    .local v1, "syncCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/String;Ljava/lang/Void;>;"
    iget-object v2, p0, Lcom/google/android/videos/store/ConfigurationStore;->userConfigGetSyncRequester:Lcom/google/android/videos/async/Requester;

    new-instance v3, Lcom/google/android/videos/api/UserConfigGetRequest;

    const/4 v4, 0x0

    invoke-direct {v3, p1, v4}, Lcom/google/android/videos/api/UserConfigGetRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;

    invoke-direct {v4, p0, v1}, Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;-><init>(Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/async/Callback;)V

    invoke-interface {v2, v3, v4}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 179
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    return-void

    .line 180
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    throw v2
.end method

.method public cleanup(ILcom/google/android/videos/async/Callback;)V
    .locals 2
    .param p1, "request"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/async/Callback",
            "<-",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 192
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<-Ljava/lang/Integer;Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->localExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/videos/store/ConfigurationStore$CleanupTask;-><init>(Lcom/google/android/videos/store/ConfigurationStore;ILcom/google/android/videos/async/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 193
    return-void
.end method

.method public getPlayCountry(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 123
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124
    iget-object v1, p0, Lcom/google/android/videos/store/ConfigurationStore;->config:Lcom/google/android/videos/Config;

    invoke-interface {v1}, Lcom/google/android/videos/Config;->deviceCountry()Ljava/lang/String;

    move-result-object v0

    .line 127
    :cond_0
    :goto_0
    return-object v0

    .line 126
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/store/ConfigurationStore;->userCountryCodes:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 127
    .local v0, "countryCode":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/store/ConfigurationStore;->config:Lcom/google/android/videos/Config;

    invoke-interface {v1}, Lcom/google/android/videos/Config;->deviceCountry()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getPlayCountry(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "defaultCountry"    # Ljava/lang/String;

    .prologue
    .line 97
    iget-object v1, p0, Lcom/google/android/videos/store/ConfigurationStore;->userCountryCodes:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 98
    .local v0, "countryCode":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .end local p2    # "defaultCountry":Ljava/lang/String;
    :goto_0
    return-object p2

    .restart local p2    # "defaultCountry":Ljava/lang/String;
    :cond_0
    move-object p2, v0

    goto :goto_0
.end method

.method public isLinkedToPartner(Ljava/lang/String;I)Z
    .locals 6
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "partnerId"    # I

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-static {p2}, Lcom/google/android/videos/store/ConfigurationStore;->isValidPartnerId(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 112
    :cond_0
    :goto_0
    return v1

    .line 108
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 111
    iget-object v2, p0, Lcom/google/android/videos/store/ConfigurationStore;->userAccountLinks:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 112
    .local v0, "partnerIds":Ljava/lang/Long;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {p2}, Lcom/google/android/videos/store/ConfigurationStore;->getPartnerFlag(I)J

    move-result-wide v4

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isPlayCountryKnown(Ljava/lang/String;)Z
    .locals 1
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 85
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->userCountryCodes:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadConfiguration(ILcom/google/android/videos/async/Callback;)V
    .locals 2
    .param p1, "request"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/async/Callback",
            "<-",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 160
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<-Ljava/lang/Integer;Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->localExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/videos/store/ConfigurationStore$LoadConfiguration;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/videos/store/ConfigurationStore$LoadConfiguration;-><init>(Lcom/google/android/videos/store/ConfigurationStore;ILcom/google/android/videos/async/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 161
    return-void
.end method

.method public moviesVerticalEnabled(Ljava/lang/String;)Z
    .locals 2
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->config:Lcom/google/android/videos/Config;

    invoke-virtual {p0, p1}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/videos/Config;->moviesVerticalEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public requestUserConfiguration(Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/UserConfiguration;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 150
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/UserConfiguration;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->localExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/videos/store/ConfigurationStore$GetConfigProto;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/videos/store/ConfigurationStore$GetConfigProto;-><init>(Lcom/google/android/videos/store/ConfigurationStore;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 151
    return-void
.end method

.method public showsVerticalEnabled(Ljava/lang/String;)Z
    .locals 2
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->config:Lcom/google/android/videos/Config;

    invoke-virtual {p0, p1}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/videos/Config;->showsVerticalEnabled(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public syncUserConfiguration(Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/async/Callback",
            "<-",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 170
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<-Ljava/lang/String;Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/ConfigurationStore;->userConfigGetRequester:Lcom/google/android/videos/async/Requester;

    new-instance v1, Lcom/google/android/videos/api/UserConfigGetRequest;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/videos/api/UserConfigGetRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;

    invoke-direct {v2, p0, p2}, Lcom/google/android/videos/store/ConfigurationStore$UserConfigurationCallback;-><init>(Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/async/Callback;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 172
    return-void
.end method
