.class public final Lcom/google/android/videos/proto/TagProtos$Frame;
.super Lcom/google/protobuf/nano/MessageNano;
.source "TagProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/proto/TagProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Frame"
.end annotation


# instance fields
.field public offset:I

.field public tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

.field public tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 441
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 442
    invoke-virtual {p0}, Lcom/google/android/videos/proto/TagProtos$Frame;->clear()Lcom/google/android/videos/proto/TagProtos$Frame;

    .line 443
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/videos/proto/TagProtos$Frame;
    .locals 1

    .prologue
    .line 446
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->offset:I

    .line 447
    invoke-static {}, Lcom/google/android/videos/proto/TagProtos$Tag;->emptyArray()[Lcom/google/android/videos/proto/TagProtos$Tag;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    .line 448
    invoke-static {}, Lcom/google/android/videos/proto/TagProtos$Tag;->emptyArray()[Lcom/google/android/videos/proto/TagProtos$Tag;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    .line 449
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->cachedSize:I

    .line 450
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 514
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 515
    .local v2, "size":I
    iget v3, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->offset:I

    if-eqz v3, :cond_0

    .line 516
    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->offset:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 519
    :cond_0
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 520
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 521
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    aget-object v0, v3, v1

    .line 522
    .local v0, "element":Lcom/google/android/videos/proto/TagProtos$Tag;
    if-eqz v0, :cond_1

    .line 523
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 520
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 528
    .end local v0    # "element":Lcom/google/android/videos/proto/TagProtos$Tag;
    .end local v1    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    array-length v3, v3

    if-lez v3, :cond_4

    .line 529
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    array-length v3, v3

    if-ge v1, v3, :cond_4

    .line 530
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    aget-object v0, v3, v1

    .line 531
    .restart local v0    # "element":Lcom/google/android/videos/proto/TagProtos$Tag;
    if-eqz v0, :cond_3

    .line 532
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 529
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 537
    .end local v0    # "element":Lcom/google/android/videos/proto/TagProtos$Tag;
    .end local v1    # "i":I
    :cond_4
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 455
    if-ne p1, p0, :cond_1

    .line 473
    :cond_0
    :goto_0
    return v1

    .line 458
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/proto/TagProtos$Frame;

    if-nez v3, :cond_2

    move v1, v2

    .line 459
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 461
    check-cast v0, Lcom/google/android/videos/proto/TagProtos$Frame;

    .line 462
    .local v0, "other":Lcom/google/android/videos/proto/TagProtos$Frame;
    iget v3, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->offset:I

    iget v4, v0, Lcom/google/android/videos/proto/TagProtos$Frame;->offset:I

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 463
    goto :goto_0

    .line 465
    :cond_3
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    iget-object v4, v0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 467
    goto :goto_0

    .line 469
    :cond_4
    iget-object v3, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    iget-object v4, v0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 471
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 478
    const/16 v0, 0x11

    .line 479
    .local v0, "result":I
    iget v1, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->offset:I

    add-int/lit16 v0, v1, 0x20f

    .line 480
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 482
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 484
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/TagProtos$Frame;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 545
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 546
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 550
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 551
    :sswitch_0
    return-object p0

    .line 556
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->offset:I

    goto :goto_0

    .line 560
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 562
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    if-nez v5, :cond_2

    move v1, v4

    .line 563
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/videos/proto/TagProtos$Tag;

    .line 565
    .local v2, "newArray":[Lcom/google/android/videos/proto/TagProtos$Tag;
    if-eqz v1, :cond_1

    .line 566
    iget-object v5, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 568
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 569
    new-instance v5, Lcom/google/android/videos/proto/TagProtos$Tag;

    invoke-direct {v5}, Lcom/google/android/videos/proto/TagProtos$Tag;-><init>()V

    aput-object v5, v2, v1

    .line 570
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 571
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 568
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 562
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/TagProtos$Tag;
    :cond_2
    iget-object v5, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    array-length v1, v5

    goto :goto_1

    .line 574
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/videos/proto/TagProtos$Tag;
    :cond_3
    new-instance v5, Lcom/google/android/videos/proto/TagProtos$Tag;

    invoke-direct {v5}, Lcom/google/android/videos/proto/TagProtos$Tag;-><init>()V

    aput-object v5, v2, v1

    .line 575
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 576
    iput-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    goto :goto_0

    .line 580
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/TagProtos$Tag;
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 582
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    if-nez v5, :cond_5

    move v1, v4

    .line 583
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/videos/proto/TagProtos$Tag;

    .line 585
    .restart local v2    # "newArray":[Lcom/google/android/videos/proto/TagProtos$Tag;
    if-eqz v1, :cond_4

    .line 586
    iget-object v5, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 588
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 589
    new-instance v5, Lcom/google/android/videos/proto/TagProtos$Tag;

    invoke-direct {v5}, Lcom/google/android/videos/proto/TagProtos$Tag;-><init>()V

    aput-object v5, v2, v1

    .line 590
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 591
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 588
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 582
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/TagProtos$Tag;
    :cond_5
    iget-object v5, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    array-length v1, v5

    goto :goto_3

    .line 594
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/videos/proto/TagProtos$Tag;
    :cond_6
    new-instance v5, Lcom/google/android/videos/proto/TagProtos$Tag;

    invoke-direct {v5}, Lcom/google/android/videos/proto/TagProtos$Tag;-><init>()V

    aput-object v5, v2, v1

    .line 595
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 596
    iput-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    goto/16 :goto_0

    .line 546
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 415
    invoke-virtual {p0, p1}, Lcom/google/android/videos/proto/TagProtos$Frame;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/TagProtos$Frame;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 490
    iget v2, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->offset:I

    if-eqz v2, :cond_0

    .line 491
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->offset:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 493
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 494
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 495
    iget-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagIn:[Lcom/google/android/videos/proto/TagProtos$Tag;

    aget-object v0, v2, v1

    .line 496
    .local v0, "element":Lcom/google/android/videos/proto/TagProtos$Tag;
    if-eqz v0, :cond_1

    .line 497
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 494
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 501
    .end local v0    # "element":Lcom/google/android/videos/proto/TagProtos$Tag;
    .end local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 502
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 503
    iget-object v2, p0, Lcom/google/android/videos/proto/TagProtos$Frame;->tagOut:[Lcom/google/android/videos/proto/TagProtos$Tag;

    aget-object v0, v2, v1

    .line 504
    .restart local v0    # "element":Lcom/google/android/videos/proto/TagProtos$Tag;
    if-eqz v0, :cond_3

    .line 505
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 502
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 509
    .end local v0    # "element":Lcom/google/android/videos/proto/TagProtos$Tag;
    .end local v1    # "i":I
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 510
    return-void
.end method
