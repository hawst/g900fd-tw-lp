.class public Lcom/google/android/videos/store/SyncService;
.super Landroid/app/Service;
.source "SyncService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/SyncService$DummyContentProvider;,
        Lcom/google/android/videos/store/SyncService$SyncAdapter;
    }
.end annotation


# static fields
.field private static final syncServiceLock:Ljava/lang/Object;


# instance fields
.field private preferences:Landroid/content/SharedPreferences;

.field private purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

.field private syncAdapter:Lcom/google/android/videos/store/SyncService$SyncAdapter;

.field private volatile syncTaskControl:Lcom/google/android/videos/async/TaskControl;

.field private wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/videos/store/SyncService;->syncServiceLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 307
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/store/SyncService;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/SyncService;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/videos/store/SyncService;->preferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$100(Landroid/accounts/Account;Landroid/content/SharedPreferences;)Z
    .locals 1
    .param p0, "x0"    # Landroid/accounts/Account;
    .param p1, "x1"    # Landroid/content/SharedPreferences;

    .prologue
    .line 41
    invoke-static {p0, p1}, Lcom/google/android/videos/store/SyncService;->enableSyncIfAccountUninitialized(Landroid/accounts/Account;Landroid/content/SharedPreferences;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/videos/store/SyncService;->syncServiceLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/store/SyncService;)Lcom/google/android/videos/async/TaskControl;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/SyncService;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/videos/store/SyncService;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/videos/store/SyncService;Lcom/google/android/videos/async/TaskControl;)Lcom/google/android/videos/async/TaskControl;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/store/SyncService;
    .param p1, "x1"    # Lcom/google/android/videos/async/TaskControl;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/videos/store/SyncService;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    return-object p1
.end method

.method static synthetic access$400(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Landroid/accounts/Account;

    .prologue
    .line 41
    invoke-static {p0}, Lcom/google/android/videos/store/SyncService;->getLastSyncWasSuccessKey(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/store/SyncService;Ljava/lang/String;Ljava/lang/String;Landroid/content/SyncResult;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/SyncService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Landroid/content/SyncResult;
    .param p4, "x4"    # Z

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/store/SyncService;->syncVideoPurchase(Ljava/lang/String;Ljava/lang/String;Landroid/content/SyncResult;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/store/SyncService;Ljava/lang/String;Ljava/lang/String;Landroid/content/SyncResult;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/SyncService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Landroid/content/SyncResult;
    .param p4, "x4"    # Z

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/store/SyncService;->syncSeasonPurchase(Ljava/lang/String;Ljava/lang/String;Landroid/content/SyncResult;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/store/SyncService;Ljava/lang/String;Landroid/content/SyncResult;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/SyncService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/content/SyncResult;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/store/SyncService;->syncAllPurchasesAndWishlist(Ljava/lang/String;Landroid/content/SyncResult;)Z

    move-result v0

    return v0
.end method

.method public static enableSyncForUninitializedAccounts(Lcom/google/android/videos/accounts/AccountManagerWrapper;Landroid/content/SharedPreferences;)V
    .locals 3
    .param p0, "wrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p1, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    .line 80
    .local v0, "accounts":[Landroid/accounts/Account;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 81
    aget-object v2, v0, v1

    invoke-static {v2, p1}, Lcom/google/android/videos/store/SyncService;->enableSyncIfAccountUninitialized(Landroid/accounts/Account;Landroid/content/SharedPreferences;)Z

    .line 80
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 83
    :cond_0
    return-void
.end method

.method private static enableSyncIfAccountUninitialized(Landroid/accounts/Account;Landroid/content/SharedPreferences;)Z
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 87
    const-string v2, "com.google.android.videos.sync"

    invoke-static {p0, v2}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_0

    .line 89
    const-string v2, "com.google.android.videos.sync"

    invoke-static {p0, v2, v0}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 91
    const-string v2, "com.google.android.videos.sync"

    invoke-static {p0, v2, v1}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 93
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-static {p0}, Lcom/google/android/videos/store/SyncService;->getLastSyncWasSuccessKey(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 96
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static getLastSyncWasSuccessKey(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 2
    .param p0, "account"    # Landroid/accounts/Account;

    .prologue
    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "last_sync_was_success_prefix"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static startSeasonSync(Landroid/accounts/Account;Ljava/lang/String;ZZ)V
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "seasonId"    # Ljava/lang/String;
    .param p2, "forced"    # Z
    .param p3, "syncWishlist"    # Z

    .prologue
    .line 72
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 74
    const/4 v0, 0x0

    invoke-static {p0, v0, p1, p2, p3}, Lcom/google/android/videos/store/SyncService;->startSyncInternal(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 75
    return-void
.end method

.method public static startSync(Landroid/accounts/Account;Z)V
    .locals 2
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "forced"    # Z

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    const/4 v0, 0x1

    invoke-static {p0, v1, v1, p1, v0}, Lcom/google/android/videos/store/SyncService;->startSyncInternal(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 61
    return-void
.end method

.method private static startSyncInternal(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "seasonId"    # Ljava/lang/String;
    .param p3, "forced"    # Z
    .param p4, "syncWishlist"    # Z

    .prologue
    const/4 v2, 0x1

    .line 105
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 106
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz p1, :cond_2

    .line 107
    const-string v1, "video"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :cond_0
    :goto_0
    if-eqz p3, :cond_3

    .line 112
    const-string v1, "expedited"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 113
    const-string v1, "force"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 117
    :goto_1
    if-eqz p4, :cond_1

    .line 118
    const-string v1, "wishlist"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 120
    :cond_1
    const-string v1, "com.google.android.videos.sync"

    invoke-static {p0, v1, v0}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 121
    return-void

    .line 108
    :cond_2
    if-eqz p2, :cond_0

    .line 109
    const-string v1, "season"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 115
    :cond_3
    const-string v1, "ignore_backoff"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method public static startVideoSync(Landroid/accounts/Account;Ljava/lang/String;ZZ)V
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "forced"    # Z
    .param p3, "syncWishlist"    # Z

    .prologue
    .line 65
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 67
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, p2, p3}, Lcom/google/android/videos/store/SyncService;->startSyncInternal(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 68
    return-void
.end method

.method private syncAllPurchasesAndWishlist(Ljava/lang/String;Landroid/content/SyncResult;)Z
    .locals 5
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 176
    invoke-static {p1}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 177
    .local v0, "hashedAccount":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Starting sync for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 179
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v2

    .line 180
    .local v2, "purchasesSyncCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/String;Ljava/lang/Void;>;"
    iget-object v3, p0, Lcom/google/android/videos/store/SyncService;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    iget-object v4, p0, Lcom/google/android/videos/store/SyncService;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {p1, v4}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/google/android/videos/store/PurchaseStoreSync;->syncPurchases(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    .line 182
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " purchases"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3, p2}, Lcom/google/android/videos/store/SyncService;->waitForCompletion(Lcom/google/android/videos/async/SyncCallback;Ljava/lang/String;Landroid/content/SyncResult;)Z

    move-result v1

    .line 185
    .local v1, "purchaseSuccess":Z
    if-eqz v1, :cond_0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/videos/store/SyncService;->syncWishlist(Ljava/lang/String;Ljava/lang/String;Landroid/content/SyncResult;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private syncSeasonPurchase(Ljava/lang/String;Ljava/lang/String;Landroid/content/SyncResult;Z)Z
    .locals 7
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "seasonId"    # Ljava/lang/String;
    .param p3, "syncResult"    # Landroid/content/SyncResult;
    .param p4, "syncWishlist"    # Z

    .prologue
    .line 160
    invoke-static {p1}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 161
    .local v0, "hashedAccount":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " season "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 162
    .local v4, "syncInfo":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Starting sync for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 164
    invoke-static {p1, p2}, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->createForId(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    move-result-object v2

    .line 166
    .local v2, "request":Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v3

    .line 167
    .local v3, "syncCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;>;"
    iget-object v5, p0, Lcom/google/android/videos/store/SyncService;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    iget-object v6, p0, Lcom/google/android/videos/store/SyncService;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v2, v6}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Lcom/google/android/videos/store/PurchaseStoreSync;->syncPurchasesForSeason(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    .line 169
    invoke-direct {p0, v3, v4, p3}, Lcom/google/android/videos/store/SyncService;->waitForCompletion(Lcom/google/android/videos/async/SyncCallback;Ljava/lang/String;Landroid/content/SyncResult;)Z

    move-result v1

    .line 171
    .local v1, "purchaseSuccess":Z
    if-eqz p4, :cond_0

    if-eqz v1, :cond_1

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/videos/store/SyncService;->syncWishlist(Ljava/lang/String;Ljava/lang/String;Landroid/content/SyncResult;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v1, 0x1

    .end local v1    # "purchaseSuccess":Z
    :cond_0
    :goto_0
    return v1

    .restart local v1    # "purchaseSuccess":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private syncVideoPurchase(Ljava/lang/String;Ljava/lang/String;Landroid/content/SyncResult;Z)Z
    .locals 7
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "syncResult"    # Landroid/content/SyncResult;
    .param p4, "syncWishlist"    # Z

    .prologue
    .line 143
    invoke-static {p1}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 144
    .local v0, "hashedAccount":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " video "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 145
    .local v4, "syncInfo":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Starting sync for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 147
    invoke-static {p1, p2}, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->createForId(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    move-result-object v2

    .line 149
    .local v2, "request":Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v3

    .line 150
    .local v3, "syncCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;>;"
    iget-object v5, p0, Lcom/google/android/videos/store/SyncService;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    iget-object v6, p0, Lcom/google/android/videos/store/SyncService;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v2, v6}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Lcom/google/android/videos/store/PurchaseStoreSync;->syncPurchasesForVideo(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    .line 152
    invoke-direct {p0, v3, v4, p3}, Lcom/google/android/videos/store/SyncService;->waitForCompletion(Lcom/google/android/videos/async/SyncCallback;Ljava/lang/String;Landroid/content/SyncResult;)Z

    move-result v1

    .line 154
    .local v1, "purchaseSuccess":Z
    if-eqz p4, :cond_0

    if-eqz v1, :cond_1

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/videos/store/SyncService;->syncWishlist(Ljava/lang/String;Ljava/lang/String;Landroid/content/SyncResult;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v1, 0x1

    .end local v1    # "purchaseSuccess":Z
    :cond_0
    :goto_0
    return v1

    .restart local v1    # "purchaseSuccess":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private syncWishlist(Ljava/lang/String;Ljava/lang/String;Landroid/content/SyncResult;)Z
    .locals 3
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "hashedAccount"    # Ljava/lang/String;
    .param p3, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 189
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v0

    .line 190
    .local v0, "wishlistSyncCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/String;Ljava/lang/Void;>;"
    iget-object v1, p0, Lcom/google/android/videos/store/SyncService;->wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;

    iget-object v2, p0, Lcom/google/android/videos/store/SyncService;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {p1, v2}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/videos/store/WishlistStoreSync;->syncWishlist(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    .line 192
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " wishlist"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p3}, Lcom/google/android/videos/store/SyncService;->waitForCompletion(Lcom/google/android/videos/async/SyncCallback;Ljava/lang/String;Landroid/content/SyncResult;)Z

    move-result v1

    return v1
.end method

.method private waitForCompletion(Lcom/google/android/videos/async/SyncCallback;Ljava/lang/String;Landroid/content/SyncResult;)Z
    .locals 8
    .param p2, "syncInfo"    # Ljava/lang/String;
    .param p3, "syncResult"    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/SyncCallback",
            "<**>;",
            "Ljava/lang/String;",
            "Landroid/content/SyncResult;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 201
    .local p1, "syncCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<**>;"
    const-wide/32 v2, 0x1d4c0

    :try_start_0
    invoke-virtual {p1, v2, v3}, Lcom/google/android/videos/async/SyncCallback;->getResponse(J)Ljava/lang/Object;

    .line 202
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sync completed for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2

    .line 203
    const/4 v2, 0x1

    .line 221
    :goto_0
    return v2

    .line 204
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Ljava/util/concurrent/CancellationException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sync canceled for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 221
    .end local v0    # "e":Ljava/util/concurrent/CancellationException;
    :goto_1
    const/4 v2, 0x0

    goto :goto_0

    .line 207
    :catch_1
    move-exception v0

    .line 208
    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sync timeout for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    goto :goto_1

    .line 209
    .end local v0    # "e":Ljava/util/concurrent/TimeoutException;
    :catch_2
    move-exception v0

    .line 212
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    .line 213
    .local v1, "exception":Ljava/lang/Throwable;
    instance-of v2, v1, Lcom/google/android/videos/store/SyncTaskManager$SyncException;

    if-eqz v2, :cond_0

    move-object v2, v1

    check-cast v2, Lcom/google/android/videos/store/SyncTaskManager$SyncException;

    iget v2, v2, Lcom/google/android/videos/store/SyncTaskManager$SyncException;->errorLevel:I

    const/16 v3, 0x14

    if-ge v2, v3, :cond_0

    .line 215
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sync completed for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with failure to sync optional data: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/videos/L;->i(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 217
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sync failed for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/videos/L;->i(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 218
    iget-object v2, p3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 135
    const-string v0, "android.content.SyncAdapter"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/videos/store/SyncService;->syncAdapter:Lcom/google/android/videos/store/SyncService$SyncAdapter;

    invoke-virtual {v0}, Lcom/google/android/videos/store/SyncService$SyncAdapter;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 138
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 125
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 126
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    .line 127
    .local v0, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/SyncService;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    .line 128
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getWishlistStoreSync()Lcom/google/android/videos/store/WishlistStoreSync;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/SyncService;->wishlistStoreSync:Lcom/google/android/videos/store/WishlistStoreSync;

    .line 129
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/SyncService;->preferences:Landroid/content/SharedPreferences;

    .line 130
    new-instance v1, Lcom/google/android/videos/store/SyncService$SyncAdapter;

    invoke-direct {v1, p0}, Lcom/google/android/videos/store/SyncService$SyncAdapter;-><init>(Lcom/google/android/videos/store/SyncService;)V

    iput-object v1, p0, Lcom/google/android/videos/store/SyncService;->syncAdapter:Lcom/google/android/videos/store/SyncService$SyncAdapter;

    .line 131
    return-void
.end method
