.class public Lcom/google/android/videos/tagging/TagStream;
.super Ljava/lang/Object;
.source "TagStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/TagStream$ChunkInfo;
    }
.end annotation


# instance fields
.field private final allKnowledgeEntities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field private final chunkStarts:[I

.field private final chunks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TagStream$ChunkInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final contentLanguage:Ljava/lang/String;

.field private final framesPerSecond:F

.field private final knowledgeEntities:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field private final knowledgeEntitiesBySplitId:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field public final lastModified:Ljava/lang/String;

.field private final prefetchImages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            ">;"
        }
    .end annotation
.end field

.field private final source:[B

.field private valid:Z

.field public final videoId:Ljava/lang/String;

.field public final videoItag:I

.field public final videoTimestamp:Ljava/lang/String;

.field public final videoTitle:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[BFLjava/util/Collection;Ljava/util/Collection;Ljava/util/Collection;)V
    .locals 11
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "videoTitle"    # Ljava/lang/String;
    .param p3, "contentLanguage"    # Ljava/lang/String;
    .param p4, "lastModified"    # Ljava/lang/String;
    .param p5, "videoItag"    # I
    .param p6, "videoTimestamp"    # Ljava/lang/String;
    .param p7, "source"    # [B
    .param p8, "framesPerSecond"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "[BF",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/videos/tagging/TagStream$ChunkInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p9, "knowledgeEntities":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/videos/tagging/KnowledgeEntity;>;"
    .local p10, "prefetchImages":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;>;"
    .local p11, "chunks":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/videos/tagging/TagStream$ChunkInfo;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/videos/tagging/TagStream;->videoId:Ljava/lang/String;

    .line 46
    iput-object p2, p0, Lcom/google/android/videos/tagging/TagStream;->videoTitle:Ljava/lang/String;

    .line 47
    iput-object p3, p0, Lcom/google/android/videos/tagging/TagStream;->contentLanguage:Ljava/lang/String;

    .line 48
    iput-object p4, p0, Lcom/google/android/videos/tagging/TagStream;->lastModified:Ljava/lang/String;

    .line 49
    move/from16 v0, p5

    iput v0, p0, Lcom/google/android/videos/tagging/TagStream;->videoItag:I

    .line 50
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/videos/tagging/TagStream;->videoTimestamp:Ljava/lang/String;

    .line 52
    invoke-static/range {p7 .. p7}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [B

    iput-object v9, p0, Lcom/google/android/videos/tagging/TagStream;->source:[B

    .line 53
    move/from16 v0, p8

    iput v0, p0, Lcom/google/android/videos/tagging/TagStream;->framesPerSecond:F

    .line 54
    invoke-static/range {p9 .. p9}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    invoke-static/range {p9 .. p9}, Lcom/google/android/videos/utils/CollectionUtil;->immutableCopyOf(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/videos/tagging/TagStream;->allKnowledgeEntities:Ljava/util/List;

    .line 56
    invoke-interface/range {p9 .. p9}, Ljava/util/Collection;->size()I

    move-result v2

    .line 57
    .local v2, "count":I
    new-instance v9, Landroid/util/SparseArray;

    invoke-direct {v9, v2}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v9, p0, Lcom/google/android/videos/tagging/TagStream;->knowledgeEntities:Landroid/util/SparseArray;

    .line 58
    new-instance v9, Landroid/util/SparseArray;

    invoke-direct {v9, v2}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v9, p0, Lcom/google/android/videos/tagging/TagStream;->knowledgeEntitiesBySplitId:Landroid/util/SparseArray;

    .line 59
    invoke-interface/range {p9 .. p9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/videos/tagging/KnowledgeEntity;

    .line 60
    .local v6, "knowledgeEntity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    iget-object v9, p0, Lcom/google/android/videos/tagging/TagStream;->knowledgeEntities:Landroid/util/SparseArray;

    iget v10, v6, Lcom/google/android/videos/tagging/KnowledgeEntity;->localId:I

    invoke-virtual {v9, v10, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 61
    iget-object v1, v6, Lcom/google/android/videos/tagging/KnowledgeEntity;->splitIds:[I

    .local v1, "arr$":[I
    array-length v7, v1

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v7, :cond_0

    aget v8, v1, v5

    .line 62
    .local v8, "splitId":I
    iget-object v9, p0, Lcom/google/android/videos/tagging/TagStream;->knowledgeEntitiesBySplitId:Landroid/util/SparseArray;

    invoke-virtual {v9, v8, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 61
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 65
    .end local v1    # "arr$":[I
    .end local v5    # "i$":I
    .end local v6    # "knowledgeEntity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    .end local v7    # "len$":I
    .end local v8    # "splitId":I
    :cond_1
    invoke-static/range {p10 .. p10}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-static/range {p10 .. p10}, Lcom/google/android/videos/utils/CollectionUtil;->immutableCopyOf(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/videos/tagging/TagStream;->prefetchImages:Ljava/util/List;

    .line 67
    invoke-static/range {p11 .. p11}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    invoke-static/range {p11 .. p11}, Lcom/google/android/videos/utils/CollectionUtil;->immutableCopyOf(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/videos/tagging/TagStream;->chunks:Ljava/util/List;

    .line 69
    invoke-interface/range {p11 .. p11}, Ljava/util/Collection;->size()I

    move-result v9

    new-array v9, v9, [I

    iput-object v9, p0, Lcom/google/android/videos/tagging/TagStream;->chunkStarts:[I

    .line 70
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v9, p0, Lcom/google/android/videos/tagging/TagStream;->chunkStarts:[I

    array-length v9, v9

    if-ge v3, v9, :cond_2

    .line 71
    iget-object v10, p0, Lcom/google/android/videos/tagging/TagStream;->chunkStarts:[I

    iget-object v9, p0, Lcom/google/android/videos/tagging/TagStream;->chunks:Ljava/util/List;

    invoke-interface {v9, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    iget v9, v9, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;->startMillis:I

    aput v9, v10, v3

    .line 70
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 73
    :cond_2
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/google/android/videos/tagging/TagStream;->valid:Z

    .line 74
    return-void
.end method


# virtual methods
.method public getAllKnowledgeEntities()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagStream;->allKnowledgeEntities:Ljava/util/List;

    return-object v0
.end method

.method public getChunkAt(I)Lcom/google/android/videos/tagging/TagStream$ChunkInfo;
    .locals 3
    .param p1, "timeMillis"    # I

    .prologue
    const/4 v1, 0x0

    .line 117
    iget-boolean v2, p0, Lcom/google/android/videos/tagging/TagStream;->valid:Z

    if-nez v2, :cond_1

    .line 124
    :cond_0
    :goto_0
    return-object v1

    .line 120
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/tagging/TagStream;->chunkStarts:[I

    invoke-static {v2, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    .line 121
    .local v0, "index":I
    if-gez v0, :cond_2

    .line 122
    xor-int/lit8 v2, v0, -0x1

    add-int/lit8 v0, v2, -0x1

    .line 124
    :cond_2
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/tagging/TagStream;->chunks:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;

    goto :goto_0
.end method

.method public getKnowledgeEntityBySplitId(I)Lcom/google/android/videos/tagging/KnowledgeEntity;
    .locals 1
    .param p1, "splitId"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagStream;->knowledgeEntitiesBySplitId:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/KnowledgeEntity;

    return-object v0
.end method

.method public getUniquePrefetchImages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagStream;->prefetchImages:Ljava/util/List;

    return-object v0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/google/android/videos/tagging/TagStream;->valid:Z

    return v0
.end method

.method public loadChunkData(Lcom/google/android/videos/tagging/TagStream$ChunkInfo;)Lcom/google/android/videos/tagging/ChunkData;
    .locals 5
    .param p1, "chunkInfo"    # Lcom/google/android/videos/tagging/TagStream$ChunkInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    iget-object v1, p0, Lcom/google/android/videos/tagging/TagStream;->chunks:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "chunkInfo must be from this tag stream"

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 138
    :try_start_0
    new-instance v1, Lcom/google/android/videos/tagging/ChunkData$Parser;

    iget v2, p1, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;->startMillis:I

    invoke-direct {v1, p0, v2}, Lcom/google/android/videos/tagging/ChunkData$Parser;-><init>(Lcom/google/android/videos/tagging/TagStream;I)V

    iget-object v2, p0, Lcom/google/android/videos/tagging/TagStream;->source:[B

    iget v3, p1, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;->byteOffset:I

    iget v4, p1, Lcom/google/android/videos/tagging/TagStream$ChunkInfo;->byteCount:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/videos/tagging/ChunkData$Parser;->parseFrom([BII)Lcom/google/android/videos/tagging/ChunkData;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 140
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/videos/tagging/TagStream;->valid:Z

    .line 142
    throw v0
.end method

.method toMillis(I)I
    .locals 2
    .param p1, "frame"    # I

    .prologue
    .line 100
    int-to-float v0, p1

    iget v1, p0, Lcom/google/android/videos/tagging/TagStream;->framesPerSecond:F

    div-float/2addr v0, v1

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method
