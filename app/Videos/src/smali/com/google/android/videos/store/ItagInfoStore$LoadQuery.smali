.class interface abstract Lcom/google/android/videos/store/ItagInfoStore$LoadQuery;
.super Ljava/lang/Object;
.source "ItagInfoStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/ItagInfoStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "LoadQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 197
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "itag"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "width"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "height"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "drm_type"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "audio_channels"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "is_dash"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "is_multi"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/store/ItagInfoStore$LoadQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
