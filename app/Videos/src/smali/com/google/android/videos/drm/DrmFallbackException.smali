.class public Lcom/google/android/videos/drm/DrmFallbackException;
.super Lcom/google/android/videos/drm/DrmException;
.source "DrmFallbackException.java"


# instance fields
.field public final fallbackDrmLevel:I


# direct methods
.method public constructor <init>(Lcom/google/android/videos/drm/DrmException$DrmError;II)V
    .locals 2
    .param p1, "drmError"    # Lcom/google/android/videos/drm/DrmException$DrmError;
    .param p2, "errorCode"    # I
    .param p3, "fallbackLevel"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/drm/DrmException;-><init>(Lcom/google/android/videos/drm/DrmException$DrmError;I)V

    .line 22
    if-lez p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "fallbackLevel must be positive"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 23
    iput p3, p0, Lcom/google/android/videos/drm/DrmFallbackException;->fallbackDrmLevel:I

    .line 24
    return-void

    .line 22
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
