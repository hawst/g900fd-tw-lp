.class public interface abstract Lcom/google/android/videos/remote/RemotePlaybackHelper$Listener;
.super Ljava/lang/Object;
.source "RemotePlaybackHelper.java"

# interfaces
.implements Lcom/google/android/videos/player/PlaybackHelper$PlayerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/remote/RemotePlaybackHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onExitRemotePlayback()V
.end method

.method public abstract onRemoteVideoTitleChanged(Ljava/lang/String;)V
.end method
