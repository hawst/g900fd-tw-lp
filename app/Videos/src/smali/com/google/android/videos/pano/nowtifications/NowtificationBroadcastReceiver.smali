.class public Lcom/google/android/videos/pano/nowtifications/NowtificationBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "NowtificationBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 20
    invoke-static {p1}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v1

    .line 21
    .local v1, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 22
    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v0

    .line 23
    .local v0, "signInManager":Lcom/google/android/videos/accounts/SignInManager;
    invoke-virtual {v0}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 24
    invoke-virtual {v0}, Lcom/google/android/videos/accounts/SignInManager;->chooseFirstAccount()Ljava/lang/String;

    .line 26
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getNowtificationHandler()Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;

    .line 28
    .end local v0    # "signInManager":Lcom/google/android/videos/accounts/SignInManager;
    :cond_1
    return-void
.end method
