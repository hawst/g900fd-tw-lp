.class Lcom/google/android/videos/player/exo/CachedRegionTracker;
.super Ljava/lang/Object;
.source "CachedRegionTracker.java"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/cache/Cache$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;
    }
.end annotation


# instance fields
.field private final cache:Lcom/google/android/exoplayer/upstream/cache/Cache;

.field private final cacheKey:Ljava/lang/String;

.field private final indexEnd:J

.field private final lookupRegion:Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;

.field private final regions:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;",
            ">;"
        }
    .end annotation
.end field

.field private final sidx:Lcom/google/android/exoplayer/parser/SegmentIndex;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/cache/Cache;Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;Lcom/google/android/exoplayer/parser/SegmentIndex;)V
    .locals 8
    .param p1, "cache"    # Lcom/google/android/exoplayer/upstream/cache/Cache;
    .param p2, "representation"    # Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .param p3, "sidx"    # Lcom/google/android/exoplayer/parser/SegmentIndex;

    .prologue
    const-wide/16 v6, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->cache:Lcom/google/android/exoplayer/upstream/cache/Cache;

    .line 36
    invoke-virtual {p2}, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->getCacheKey()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->cacheKey:Ljava/lang/String;

    .line 37
    invoke-direct {p0, p2}, Lcom/google/android/videos/player/exo/CachedRegionTracker;->getIndexEnd(Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->indexEnd:J

    .line 38
    iput-object p3, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->sidx:Lcom/google/android/exoplayer/parser/SegmentIndex;

    .line 39
    new-instance v3, Ljava/util/TreeSet;

    invoke-direct {v3}, Ljava/util/TreeSet;-><init>()V

    iput-object v3, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->regions:Ljava/util/TreeSet;

    .line 40
    new-instance v3, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;

    invoke-direct {v3, v6, v7, v6, v7}, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;-><init>(JJ)V

    iput-object v3, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->lookupRegion:Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;

    .line 42
    monitor-enter p0

    .line 43
    :try_start_0
    iget-object v3, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->cacheKey:Ljava/lang/String;

    invoke-interface {p1, v3, p0}, Lcom/google/android/exoplayer/upstream/cache/Cache;->addListener(Ljava/lang/String;Lcom/google/android/exoplayer/upstream/cache/Cache$Listener;)Ljava/util/NavigableSet;

    move-result-object v0

    .line 46
    .local v0, "cacheSpans":Ljava/util/NavigableSet;, "Ljava/util/NavigableSet<Lcom/google/android/exoplayer/upstream/cache/CacheSpan;>;"
    invoke-interface {v0}, Ljava/util/NavigableSet;->descendingIterator()Ljava/util/Iterator;

    move-result-object v2

    .line 47
    .local v2, "spanIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/exoplayer/upstream/cache/CacheSpan;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 48
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer/upstream/cache/CacheSpan;

    .line 49
    .local v1, "span":Lcom/google/android/exoplayer/upstream/cache/CacheSpan;
    invoke-direct {p0, v1}, Lcom/google/android/videos/player/exo/CachedRegionTracker;->mergeSpan(Lcom/google/android/exoplayer/upstream/cache/CacheSpan;)V

    goto :goto_0

    .line 51
    .end local v0    # "cacheSpans":Ljava/util/NavigableSet;, "Ljava/util/NavigableSet<Lcom/google/android/exoplayer/upstream/cache/CacheSpan;>;"
    .end local v1    # "span":Lcom/google/android/exoplayer/upstream/cache/CacheSpan;
    .end local v2    # "spanIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/exoplayer/upstream/cache/CacheSpan;>;"
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "cacheSpans":Ljava/util/NavigableSet;, "Ljava/util/NavigableSet<Lcom/google/android/exoplayer/upstream/cache/CacheSpan;>;"
    .restart local v2    # "spanIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/exoplayer/upstream/cache/CacheSpan;>;"
    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52
    return-void
.end method

.method private getIndexEnd(Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)J
    .locals 6
    .param p1, "representation"    # Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    .prologue
    .line 144
    invoke-virtual {p1}, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->getIndexUri()Lcom/google/android/exoplayer/dash/mpd/RangedUri;

    move-result-object v0

    .line 145
    .local v0, "indexUri":Lcom/google/android/exoplayer/dash/mpd/RangedUri;
    iget-wide v2, v0, Lcom/google/android/exoplayer/dash/mpd/RangedUri;->start:J

    iget-wide v4, v0, Lcom/google/android/exoplayer/dash/mpd/RangedUri;->length:J

    add-long/2addr v2, v4

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    return-wide v2
.end method

.method private mergeSpan(Lcom/google/android/exoplayer/upstream/cache/CacheSpan;)V
    .locals 12
    .param p1, "span"    # Lcom/google/android/exoplayer/upstream/cache/CacheSpan;

    .prologue
    .line 101
    new-instance v5, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;

    iget-wide v6, p1, Lcom/google/android/exoplayer/upstream/cache/CacheSpan;->position:J

    iget-wide v8, p1, Lcom/google/android/exoplayer/upstream/cache/CacheSpan;->position:J

    iget-wide v10, p1, Lcom/google/android/exoplayer/upstream/cache/CacheSpan;->length:J

    add-long/2addr v8, v10

    const-wide/16 v10, 0x1

    sub-long/2addr v8, v10

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;-><init>(JJ)V

    .line 102
    .local v5, "newRegion":Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;
    iget-object v6, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->regions:Ljava/util/TreeSet;

    invoke-virtual {v6, v5}, Ljava/util/TreeSet;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;

    .line 103
    .local v3, "floorRegion":Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;
    iget-object v6, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->regions:Ljava/util/TreeSet;

    invoke-virtual {v6, v5}, Ljava/util/TreeSet;->ceiling(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;

    .line 104
    .local v1, "ceilingRegion":Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;
    invoke-direct {p0, v3, v5}, Lcom/google/android/videos/player/exo/CachedRegionTracker;->regionsConnect(Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;)Z

    move-result v2

    .line 105
    .local v2, "floorConnects":Z
    invoke-direct {p0, v5, v1}, Lcom/google/android/videos/player/exo/CachedRegionTracker;->regionsConnect(Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;)Z

    move-result v0

    .line 107
    .local v0, "ceilingConnects":Z
    if-eqz v0, :cond_1

    .line 108
    if-eqz v2, :cond_0

    .line 110
    iget-wide v6, v1, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endOffset:J

    iput-wide v6, v3, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endOffset:J

    .line 111
    iget v6, v1, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endIndex:I

    iput v6, v3, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endIndex:I

    .line 118
    :goto_0
    iget-object v6, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->regions:Ljava/util/TreeSet;

    invoke-virtual {v6, v1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 134
    :goto_1
    return-void

    .line 114
    :cond_0
    iget-wide v6, v1, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endOffset:J

    iput-wide v6, v5, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endOffset:J

    .line 115
    iget v6, v1, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endIndex:I

    iput v6, v5, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endIndex:I

    .line 116
    iget-object v6, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->regions:Ljava/util/TreeSet;

    invoke-virtual {v6, v5}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 119
    :cond_1
    if-eqz v2, :cond_3

    .line 121
    iget-wide v6, v5, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endOffset:J

    iput-wide v6, v3, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endOffset:J

    .line 122
    iget v4, v3, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endIndex:I

    .line 124
    .local v4, "index":I
    :goto_2
    iget-object v6, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->sidx:Lcom/google/android/exoplayer/parser/SegmentIndex;

    iget v6, v6, Lcom/google/android/exoplayer/parser/SegmentIndex;->length:I

    add-int/lit8 v6, v6, -0x1

    if-ge v4, v6, :cond_2

    iget-object v6, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->sidx:Lcom/google/android/exoplayer/parser/SegmentIndex;

    iget-object v6, v6, Lcom/google/android/exoplayer/parser/SegmentIndex;->offsets:[J

    add-int/lit8 v7, v4, 0x1

    aget-wide v6, v6, v7

    iget-wide v8, v3, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endOffset:J

    iget-wide v10, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->indexEnd:J

    sub-long/2addr v8, v10

    cmp-long v6, v6, v8

    if-gtz v6, :cond_2

    .line 125
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 127
    :cond_2
    iput v4, v3, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endIndex:I

    goto :goto_1

    .line 130
    .end local v4    # "index":I
    :cond_3
    iget-object v6, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->sidx:Lcom/google/android/exoplayer/parser/SegmentIndex;

    iget-object v6, v6, Lcom/google/android/exoplayer/parser/SegmentIndex;->offsets:[J

    iget-wide v8, v5, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endOffset:J

    iget-wide v10, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->indexEnd:J

    sub-long/2addr v8, v10

    invoke-static {v6, v8, v9}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v4

    .line 131
    .restart local v4    # "index":I
    if-gez v4, :cond_4

    neg-int v6, v4

    add-int/lit8 v4, v6, -0x2

    .end local v4    # "index":I
    :cond_4
    iput v4, v5, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endIndex:I

    .line 132
    iget-object v6, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->regions:Ljava/util/TreeSet;

    invoke-virtual {v6, v5}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private regionsConnect(Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;)Z
    .locals 6
    .param p1, "lower"    # Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;
    .param p2, "upper"    # Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;

    .prologue
    const/4 v0, 0x0

    .line 137
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 140
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v2, p1, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endOffset:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iget-wide v4, p2, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->startOffset:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized getRegionEndTimeMs(J)I
    .locals 11
    .param p1, "byteOffset"    # J

    .prologue
    const/4 v4, -0x1

    .line 69
    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->lookupRegion:Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;

    iput-wide p1, v5, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->startOffset:J

    .line 70
    iget-object v5, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->regions:Ljava/util/TreeSet;

    iget-object v6, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->lookupRegion:Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;

    invoke-virtual {v5, v6}, Ljava/util/TreeSet;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;

    .line 71
    .local v0, "floorRegion":Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;
    if-eqz v0, :cond_0

    iget-wide v6, v0, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endOffset:J

    cmp-long v5, p1, v6

    if-gtz v5, :cond_0

    iget v5, v0, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endIndex:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v5, v4, :cond_1

    .line 81
    :cond_0
    :goto_0
    monitor-exit p0

    return v4

    .line 74
    :cond_1
    :try_start_1
    iget v1, v0, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endIndex:I

    .line 75
    .local v1, "index":I
    iget-object v4, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->sidx:Lcom/google/android/exoplayer/parser/SegmentIndex;

    iget v4, v4, Lcom/google/android/exoplayer/parser/SegmentIndex;->length:I

    add-int/lit8 v4, v4, -0x1

    if-ne v1, v4, :cond_2

    iget-wide v4, v0, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endOffset:J

    iget-wide v6, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->indexEnd:J

    iget-object v8, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->sidx:Lcom/google/android/exoplayer/parser/SegmentIndex;

    iget-object v8, v8, Lcom/google/android/exoplayer/parser/SegmentIndex;->offsets:[J

    aget-wide v8, v8, v1

    add-long/2addr v6, v8

    iget-object v8, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->sidx:Lcom/google/android/exoplayer/parser/SegmentIndex;

    iget-object v8, v8, Lcom/google/android/exoplayer/parser/SegmentIndex;->sizes:[I

    aget v8, v8, v1

    int-to-long v8, v8

    add-long/2addr v6, v8

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    .line 77
    const/4 v4, -0x2

    goto :goto_0

    .line 79
    :cond_2
    iget-object v4, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->sidx:Lcom/google/android/exoplayer/parser/SegmentIndex;

    iget-object v4, v4, Lcom/google/android/exoplayer/parser/SegmentIndex;->durationsUs:[J

    aget-wide v4, v4, v1

    iget-wide v6, v0, Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;->endOffset:J

    iget-object v8, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->sidx:Lcom/google/android/exoplayer/parser/SegmentIndex;

    iget-object v8, v8, Lcom/google/android/exoplayer/parser/SegmentIndex;->offsets:[J

    aget-wide v8, v8, v1

    sub-long/2addr v6, v8

    iget-wide v8, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->indexEnd:J

    sub-long/2addr v6, v8

    mul-long/2addr v4, v6

    iget-object v6, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->sidx:Lcom/google/android/exoplayer/parser/SegmentIndex;

    iget-object v6, v6, Lcom/google/android/exoplayer/parser/SegmentIndex;->sizes:[I

    aget v6, v6, v1

    int-to-long v6, v6

    div-long v2, v4, v6

    .line 81
    .local v2, "segmentFractionUs":J
    iget-object v4, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->sidx:Lcom/google/android/exoplayer/parser/SegmentIndex;

    iget-object v4, v4, Lcom/google/android/exoplayer/parser/SegmentIndex;->timesUs:[J

    aget-wide v4, v4, v1

    add-long/2addr v4, v2

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    long-to-int v4, v4

    goto :goto_0

    .line 69
    .end local v0    # "floorRegion":Lcom/google/android/videos/player/exo/CachedRegionTracker$Region;
    .end local v1    # "index":I
    .end local v2    # "segmentFractionUs":J
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized onSpanAdded(Lcom/google/android/exoplayer/upstream/cache/Cache;Lcom/google/android/exoplayer/upstream/cache/CacheSpan;)V
    .locals 1
    .param p1, "cache"    # Lcom/google/android/exoplayer/upstream/cache/Cache;
    .param p2, "span"    # Lcom/google/android/exoplayer/upstream/cache/CacheSpan;

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p2}, Lcom/google/android/videos/player/exo/CachedRegionTracker;->mergeSpan(Lcom/google/android/exoplayer/upstream/cache/CacheSpan;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    monitor-exit p0

    return-void

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onSpanRemoved(Lcom/google/android/exoplayer/upstream/cache/Cache;Lcom/google/android/exoplayer/upstream/cache/CacheSpan;)V
    .locals 1
    .param p1, "cache"    # Lcom/google/android/exoplayer/upstream/cache/Cache;
    .param p2, "span"    # Lcom/google/android/exoplayer/upstream/cache/CacheSpan;

    .prologue
    .line 92
    const-string v0, "Unexpected call to onSpanRemoved"

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method public onSpanTouched(Lcom/google/android/exoplayer/upstream/cache/Cache;Lcom/google/android/exoplayer/upstream/cache/CacheSpan;Lcom/google/android/exoplayer/upstream/cache/CacheSpan;)V
    .locals 0
    .param p1, "cache"    # Lcom/google/android/exoplayer/upstream/cache/Cache;
    .param p2, "oldSpan"    # Lcom/google/android/exoplayer/upstream/cache/CacheSpan;
    .param p3, "newSpan"    # Lcom/google/android/exoplayer/upstream/cache/CacheSpan;

    .prologue
    .line 98
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->cache:Lcom/google/android/exoplayer/upstream/cache/Cache;

    iget-object v1, p0, Lcom/google/android/videos/player/exo/CachedRegionTracker;->cacheKey:Ljava/lang/String;

    invoke-interface {v0, v1, p0}, Lcom/google/android/exoplayer/upstream/cache/Cache;->removeListener(Ljava/lang/String;Lcom/google/android/exoplayer/upstream/cache/Cache$Listener;)V

    .line 56
    return-void
.end method
