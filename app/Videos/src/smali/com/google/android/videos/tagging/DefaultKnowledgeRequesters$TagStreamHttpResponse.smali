.class public final Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;
.super Ljava/lang/Object;
.source "DefaultKnowledgeRequesters.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TagStreamHttpResponse"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public final content:[B

.field public final contentLanguage:Ljava/lang/String;

.field public final isNotFound:Z

.field public final lastModified:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;[B)V
    .locals 0
    .param p1, "isNotFound"    # Z
    .param p2, "lastModified"    # Ljava/lang/String;
    .param p3, "contentLanguage"    # Ljava/lang/String;
    .param p4, "content"    # [B

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput-boolean p1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->isNotFound:Z

    .line 119
    iput-object p2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->lastModified:Ljava/lang/String;

    .line 120
    iput-object p3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->contentLanguage:Ljava/lang/String;

    .line 121
    iput-object p4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpResponse;->content:[B

    .line 122
    return-void
.end method
