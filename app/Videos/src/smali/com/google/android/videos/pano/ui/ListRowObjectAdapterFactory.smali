.class public final Lcom/google/android/videos/pano/ui/ListRowObjectAdapterFactory;
.super Ljava/lang/Object;
.source "ListRowObjectAdapterFactory.java"

# interfaces
.implements Lcom/google/android/repolib/common/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Factory",
        "<",
        "Landroid/support/v17/leanback/widget/ObjectAdapter;",
        "Lcom/google/android/repolib/repositories/Repository",
        "<",
        "Landroid/support/v17/leanback/widget/ListRow;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final presenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Landroid/support/v17/leanback/widget/SinglePresenterSelector;

    new-instance v1, Landroid/support/v17/leanback/widget/ListRowPresenter;

    invoke-direct {v1}, Landroid/support/v17/leanback/widget/ListRowPresenter;-><init>()V

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/SinglePresenterSelector;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/ListRowObjectAdapterFactory;->presenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    return-void
.end method


# virtual methods
.method public createFrom(Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/repositories/Repository",
            "<",
            "Landroid/support/v17/leanback/widget/ListRow;",
            ">;)",
            "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter",
            "<",
            "Landroid/support/v17/leanback/widget/ListRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    .local p1, "repository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<Landroid/support/v17/leanback/widget/ListRow;>;"
    new-instance v0, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/ListRowObjectAdapterFactory;->presenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    invoke-direct {v0, v1, p1}, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;Lcom/google/android/repolib/repositories/Repository;)V

    return-object v0
.end method

.method public bridge synthetic createFrom(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 14
    check-cast p1, Lcom/google/android/repolib/repositories/Repository;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/pano/ui/ListRowObjectAdapterFactory;->createFrom(Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;

    move-result-object v0

    return-object v0
.end method
