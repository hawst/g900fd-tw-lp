.class public final Lcom/google/android/videos/flow/FlowAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "FlowAdapter.java"

# interfaces
.implements Lcom/google/android/play/layout/FlowLayoutManager$AutoRegisteredOnViewRenderedListener;
.implements Lcom/google/android/videos/flow/Flow$FlowChangesListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;",
        "Lcom/google/android/play/layout/FlowLayoutManager$AutoRegisteredOnViewRenderedListener;",
        "Lcom/google/android/videos/flow/Flow$FlowChangesListener;"
    }
.end annotation


# instance fields
.field private final assignedItemIdentifiers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private debugInfo:Ljava/lang/String;

.field private events:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lcom/google/android/videos/flow/Flow;

.field private final viewTypes:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/flow/ViewHolderCreator",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/flow/Flow;)V
    .locals 1
    .param p1, "flow"    # Lcom/google/android/videos/flow/Flow;

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 37
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/flow/Flow;

    iput-object v0, p0, Lcom/google/android/videos/flow/FlowAdapter;->flow:Lcom/google/android/videos/flow/Flow;

    .line 38
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/flow/FlowAdapter;->viewTypes:Landroid/util/SparseArray;

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/flow/FlowAdapter;->assignedItemIdentifiers:Ljava/util/Map;

    .line 41
    iget-object v0, p0, Lcom/google/android/videos/flow/FlowAdapter;->viewTypes:Landroid/util/SparseArray;

    invoke-virtual {p1, v0}, Lcom/google/android/videos/flow/Flow;->onInitViewTypes(Landroid/util/SparseArray;)V

    .line 42
    invoke-virtual {p1, p0}, Lcom/google/android/videos/flow/Flow;->setFlowChangesListener(Lcom/google/android/videos/flow/Flow$FlowChangesListener;)V

    .line 43
    return-void
.end method

.method private pushEvent(Ljava/lang/String;II)V
    .locals 18
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "positionStart"    # I
    .param p3, "positionCount"    # I

    .prologue
    .line 146
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/flow/FlowAdapter;->debugInfo:Ljava/lang/String;

    if-nez v12, :cond_0

    .line 163
    :goto_0
    return-void

    .line 149
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/flow/FlowAdapter;->events:Ljava/util/ArrayDeque;

    invoke-virtual {v12}, Ljava/util/ArrayDeque;->size()I

    move-result v12

    const/16 v13, 0xa

    if-lt v12, v13, :cond_1

    .line 150
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/flow/FlowAdapter;->events:Ljava/util/ArrayDeque;

    invoke-virtual {v12}, Ljava/util/ArrayDeque;->poll()Ljava/lang/Object;

    .line 153
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 154
    .local v10, "time":J
    const-wide/16 v12, 0x3e8

    rem-long v6, v10, v12

    .line 155
    .local v6, "ms":J
    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    .line 156
    const-wide/16 v12, 0x3c

    rem-long v8, v10, v12

    .line 157
    .local v8, "s":J
    const-wide/16 v12, 0x3c

    div-long/2addr v10, v12

    .line 158
    const-wide/16 v12, 0x3c

    rem-long v4, v10, v12

    .line 159
    .local v4, "m":J
    const-wide/16 v12, 0x3c

    div-long/2addr v10, v12

    .line 160
    const-wide/16 v12, 0x18

    rem-long v2, v10, v12

    .line 161
    .local v2, "h":J
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/flow/FlowAdapter;->events:Ljava/util/ArrayDeque;

    sget-object v13, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "t=%02d:%02d:%02d.%03d %s(@%d, %d), #=%d"

    const/16 v15, 0x8

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x2

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x3

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x4

    aput-object p1, v15, v16

    const/16 v16, 0x5

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x6

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/flow/FlowAdapter;->getItemCount()I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v13, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method


# virtual methods
.method public dumpDebugInfo()V
    .locals 4

    .prologue
    .line 166
    iget-object v2, p0, Lcom/google/android/videos/flow/FlowAdapter;->debugInfo:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 173
    :cond_0
    return-void

    .line 169
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adapter("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/flow/FlowAdapter;->debugInfo:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") last "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/flow/FlowAdapter;->events:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " events:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 170
    iget-object v2, p0, Lcom/google/android/videos/flow/FlowAdapter;->events:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 171
    .local v0, "event":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/videos/flow/FlowAdapter;->flow:Lcom/google/android/videos/flow/Flow;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/flow/FlowAdapter;->flow:Lcom/google/android/videos/flow/Flow;

    invoke-virtual {v0}, Lcom/google/android/videos/flow/Flow;->getVisibleCount()I

    move-result v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/google/android/videos/flow/FlowAdapter;->hasStableIds()Z

    move-result v3

    if-nez v3, :cond_0

    .line 71
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemId(I)J

    move-result-wide v4

    .line 84
    :goto_0
    return-wide v4

    .line 73
    :cond_0
    iget-object v3, p0, Lcom/google/android/videos/flow/FlowAdapter;->flow:Lcom/google/android/videos/flow/Flow;

    invoke-virtual {v3, p1}, Lcom/google/android/videos/flow/Flow;->getItemIdentifier(I)Ljava/lang/Object;

    move-result-object v2

    .line 75
    .local v2, "itemIdentifier":Ljava/lang/Object;
    iget-object v4, p0, Lcom/google/android/videos/flow/FlowAdapter;->assignedItemIdentifiers:Ljava/util/Map;

    monitor-enter v4

    .line 76
    :try_start_0
    iget-object v3, p0, Lcom/google/android/videos/flow/FlowAdapter;->assignedItemIdentifiers:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 77
    .local v0, "assignedNumber":Ljava/lang/Integer;
    if-eqz v0, :cond_1

    .line 78
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 83
    .local v1, "idNumber":I
    :goto_1
    monitor-exit v4

    .line 84
    int-to-long v4, v1

    goto :goto_0

    .line 80
    .end local v1    # "idNumber":I
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/flow/FlowAdapter;->assignedItemIdentifiers:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    add-int/lit8 v1, v3, 0x1

    .line 81
    .restart local v1    # "idNumber":I
    iget-object v3, p0, Lcom/google/android/videos/flow/FlowAdapter;->assignedItemIdentifiers:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 83
    .end local v0    # "assignedNumber":Ljava/lang/Integer;
    .end local v1    # "idNumber":I
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/videos/flow/FlowAdapter;->flow:Lcom/google/android/videos/flow/Flow;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/flow/Flow;->getViewType(I)I

    move-result v0

    return v0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 1
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/flow/FlowAdapter;->flow:Lcom/google/android/videos/flow/Flow;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/flow/Flow;->bindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V

    .line 61
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 52
    iget-object v1, p0, Lcom/google/android/videos/flow/FlowAdapter;->viewTypes:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/flow/ViewHolderCreator;

    .line 53
    .local v0, "viewHolderCreator":Lcom/google/android/videos/flow/ViewHolderCreator;, "Lcom/google/android/videos/flow/ViewHolderCreator<*>;"
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "View type 0x%x not registered during onInitViewTypes()"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;Ljava/lang/Object;)V

    .line 55
    invoke-interface {v0, p2, p1}, Lcom/google/android/videos/flow/ViewHolderCreator;->createViewHolder(ILandroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v1

    return-object v1

    .line 53
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onDataSetChanged(Lcom/google/android/videos/flow/Flow;)V
    .locals 3
    .param p1, "flow"    # Lcom/google/android/videos/flow/Flow;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/videos/flow/FlowAdapter;->flow:Lcom/google/android/videos/flow/Flow;

    if-ne p1, v0, :cond_0

    .line 90
    const-string v0, "onDataSetChanged"

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/videos/flow/FlowAdapter;->pushEvent(Ljava/lang/String;II)V

    .line 91
    invoke-virtual {p0}, Lcom/google/android/videos/flow/FlowAdapter;->notifyDataSetChanged()V

    .line 93
    :cond_0
    return-void
.end method

.method public onItemsChanged(Lcom/google/android/videos/flow/Flow;II)V
    .locals 1
    .param p1, "flow"    # Lcom/google/android/videos/flow/Flow;
    .param p2, "positionStart"    # I
    .param p3, "positionCount"    # I

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/videos/flow/FlowAdapter;->flow:Lcom/google/android/videos/flow/Flow;

    if-ne p1, v0, :cond_0

    .line 114
    const-string v0, "onItemsChanged"

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/videos/flow/FlowAdapter;->pushEvent(Ljava/lang/String;II)V

    .line 115
    invoke-virtual {p0, p2, p3}, Lcom/google/android/videos/flow/FlowAdapter;->notifyItemRangeChanged(II)V

    .line 117
    :cond_0
    return-void
.end method

.method public onItemsInserted(Lcom/google/android/videos/flow/Flow;II)V
    .locals 1
    .param p1, "flow"    # Lcom/google/android/videos/flow/Flow;
    .param p2, "positionStart"    # I
    .param p3, "positionCount"    # I

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/videos/flow/FlowAdapter;->flow:Lcom/google/android/videos/flow/Flow;

    if-ne p1, v0, :cond_0

    .line 98
    const-string v0, "onItemsInserted"

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/videos/flow/FlowAdapter;->pushEvent(Ljava/lang/String;II)V

    .line 99
    invoke-virtual {p0, p2, p3}, Lcom/google/android/videos/flow/FlowAdapter;->notifyItemRangeInserted(II)V

    .line 101
    :cond_0
    return-void
.end method

.method public onItemsRemoved(Lcom/google/android/videos/flow/Flow;II)V
    .locals 1
    .param p1, "flow"    # Lcom/google/android/videos/flow/Flow;
    .param p2, "positionStart"    # I
    .param p3, "positionCount"    # I

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/videos/flow/FlowAdapter;->flow:Lcom/google/android/videos/flow/Flow;

    if-ne p1, v0, :cond_0

    .line 106
    const-string v0, "onItemsRemoved"

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/videos/flow/FlowAdapter;->pushEvent(Ljava/lang/String;II)V

    .line 107
    invoke-virtual {p0, p2, p3}, Lcom/google/android/videos/flow/FlowAdapter;->notifyItemRangeRemoved(II)V

    .line 109
    :cond_0
    return-void
.end method

.method public onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 1
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 131
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 132
    instance-of v0, p1, Lcom/google/android/videos/flow/SelfRecycledListener;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 133
    check-cast v0, Lcom/google/android/videos/flow/SelfRecycledListener;

    invoke-interface {v0}, Lcom/google/android/videos/flow/SelfRecycledListener;->onViewRecycled()V

    .line 135
    :cond_0
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    instance-of v0, v0, Lcom/google/android/videos/flow/SelfRecycledListener;

    if-eqz v0, :cond_1

    .line 136
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    check-cast v0, Lcom/google/android/videos/flow/SelfRecycledListener;

    invoke-interface {v0}, Lcom/google/android/videos/flow/SelfRecycledListener;->onViewRecycled()V

    .line 138
    :cond_1
    return-void
.end method

.method public onViewRendered(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 1
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 121
    instance-of v0, p1, Lcom/google/android/videos/flow/SelfRenderedListener;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 122
    check-cast v0, Lcom/google/android/videos/flow/SelfRenderedListener;

    invoke-interface {v0}, Lcom/google/android/videos/flow/SelfRenderedListener;->onViewRendered()V

    .line 124
    :cond_0
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    instance-of v0, v0, Lcom/google/android/videos/flow/SelfRenderedListener;

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    check-cast v0, Lcom/google/android/videos/flow/SelfRenderedListener;

    invoke-interface {v0}, Lcom/google/android/videos/flow/SelfRenderedListener;->onViewRendered()V

    .line 127
    :cond_1
    return-void
.end method

.method public setUpDebugInfo(Ljava/lang/String;)V
    .locals 2
    .param p1, "debugInfo"    # Ljava/lang/String;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/google/android/videos/flow/FlowAdapter;->debugInfo:Ljava/lang/String;

    .line 142
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/videos/flow/FlowAdapter;->events:Ljava/util/ArrayDeque;

    .line 143
    return-void

    .line 142
    :cond_0
    new-instance v0, Ljava/util/ArrayDeque;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/ArrayDeque;-><init>(I)V

    goto :goto_0
.end method
