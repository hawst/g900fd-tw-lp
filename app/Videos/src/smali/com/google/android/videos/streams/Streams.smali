.class public Lcom/google/android/videos/streams/Streams;
.super Ljava/lang/Object;
.source "Streams.java"


# instance fields
.field public final captions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;"
        }
    .end annotation
.end field

.field public final mediaStreams:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation
.end field

.field public final storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;[Lcom/google/wireless/android/video/magma/proto/Storyboard;)V
    .locals 0
    .param p3, "storyboards"    # [Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;[",
            "Lcom/google/wireless/android/video/magma/proto/Storyboard;",
            ")V"
        }
    .end annotation

    .prologue
    .line 20
    .local p1, "mediaStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    .local p2, "captions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/videos/streams/Streams;->mediaStreams:Ljava/util/List;

    .line 22
    iput-object p2, p0, Lcom/google/android/videos/streams/Streams;->captions:Ljava/util/List;

    .line 23
    iput-object p3, p0, Lcom/google/android/videos/streams/Streams;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .line 24
    return-void
.end method
