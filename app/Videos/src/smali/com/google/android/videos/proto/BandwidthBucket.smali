.class public final Lcom/google/android/videos/proto/BandwidthBucket;
.super Lcom/google/protobuf/nano/MessageNano;
.source "BandwidthBucket.java"


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/videos/proto/BandwidthBucket;


# instance fields
.field public count:I

.field public lowBound:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 30
    invoke-virtual {p0}, Lcom/google/android/videos/proto/BandwidthBucket;->clear()Lcom/google/android/videos/proto/BandwidthBucket;

    .line 31
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/videos/proto/BandwidthBucket;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/videos/proto/BandwidthBucket;->_emptyArray:[Lcom/google/android/videos/proto/BandwidthBucket;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/android/videos/proto/BandwidthBucket;->_emptyArray:[Lcom/google/android/videos/proto/BandwidthBucket;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/videos/proto/BandwidthBucket;

    sput-object v0, Lcom/google/android/videos/proto/BandwidthBucket;->_emptyArray:[Lcom/google/android/videos/proto/BandwidthBucket;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/android/videos/proto/BandwidthBucket;->_emptyArray:[Lcom/google/android/videos/proto/BandwidthBucket;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/videos/proto/BandwidthBucket;
    .locals 2

    .prologue
    .line 34
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/videos/proto/BandwidthBucket;->lowBound:J

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/proto/BandwidthBucket;->count:I

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/proto/BandwidthBucket;->cachedSize:I

    .line 37
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 81
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 82
    .local v0, "size":I
    iget-wide v2, p0, Lcom/google/android/videos/proto/BandwidthBucket;->lowBound:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 83
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/videos/proto/BandwidthBucket;->lowBound:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    :cond_0
    iget v1, p0, Lcom/google/android/videos/proto/BandwidthBucket;->count:I

    if-eqz v1, :cond_1

    .line 87
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/videos/proto/BandwidthBucket;->count:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    if-ne p1, p0, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v1

    .line 45
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/proto/BandwidthBucket;

    if-nez v3, :cond_2

    move v1, v2

    .line 46
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 48
    check-cast v0, Lcom/google/android/videos/proto/BandwidthBucket;

    .line 49
    .local v0, "other":Lcom/google/android/videos/proto/BandwidthBucket;
    iget-wide v4, p0, Lcom/google/android/videos/proto/BandwidthBucket;->lowBound:J

    iget-wide v6, v0, Lcom/google/android/videos/proto/BandwidthBucket;->lowBound:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    move v1, v2

    .line 50
    goto :goto_0

    .line 52
    :cond_3
    iget v3, p0, Lcom/google/android/videos/proto/BandwidthBucket;->count:I

    iget v4, v0, Lcom/google/android/videos/proto/BandwidthBucket;->count:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 53
    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 60
    const/16 v0, 0x11

    .line 61
    .local v0, "result":I
    iget-wide v2, p0, Lcom/google/android/videos/proto/BandwidthBucket;->lowBound:J

    iget-wide v4, p0, Lcom/google/android/videos/proto/BandwidthBucket;->lowBound:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/lit16 v0, v1, 0x20f

    .line 63
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/proto/BandwidthBucket;->count:I

    add-int v0, v1, v2

    .line 64
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/BandwidthBucket;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 99
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 103
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 104
    :sswitch_0
    return-object p0

    .line 109
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/videos/proto/BandwidthBucket;->lowBound:J

    goto :goto_0

    .line 113
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/proto/BandwidthBucket;->count:I

    goto :goto_0

    .line 99
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/android/videos/proto/BandwidthBucket;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/BandwidthBucket;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/google/android/videos/proto/BandwidthBucket;->lowBound:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 71
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/videos/proto/BandwidthBucket;->lowBound:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 73
    :cond_0
    iget v0, p0, Lcom/google/android/videos/proto/BandwidthBucket;->count:I

    if-eqz v0, :cond_1

    .line 74
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/videos/proto/BandwidthBucket;->count:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 76
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 77
    return-void
.end method
