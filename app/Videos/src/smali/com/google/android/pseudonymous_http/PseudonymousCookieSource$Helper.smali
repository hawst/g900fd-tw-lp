.class public final Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;
.super Ljava/lang/Object;
.source "PseudonymousCookieSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Helper"
.end annotation


# static fields
.field private static final COOKIE_PAIR:Ljava/util/regex/Pattern;

.field private static final DOT:I

.field private static final PSEUDONYMOUS_ID_DOMAINS:[Ljava/lang/String;

.field private static final TOKEN:Ljava/util/regex/Pattern;

.field private static final VALUE:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 57
    const-string v0, "."

    invoke-virtual {v0, v2}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    sput v0, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->DOT:I

    .line 59
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "google.com"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "googleapis.com"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->PSEUDONYMOUS_ID_DOMAINS:[Ljava/lang/String;

    .line 64
    const-string v0, "[^()<>@,;:\\\"/\\[\\]\\?={}\\s]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->TOKEN:Ljava/util/regex/Pattern;

    .line 67
    const-string v0, "[^,;\\s\"]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->VALUE:Ljava/util/regex/Pattern;

    .line 68
    const-string v0, "(^|[\\s;,]+)([^()<>@,;:\\\"/\\[\\]\\?={}\\s]+)\\s*=\\s*(\"[^\"]*\"|[^,;\\s\"]+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->COOKIE_PAIR:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static cookieSourceApplies(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)Z
    .locals 6
    .param p0, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p1, "cookieSource"    # Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    .prologue
    const/4 v4, 0x0

    .line 131
    if-eqz p1, :cond_0

    .line 132
    sget-object v0, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->PSEUDONYMOUS_ID_DOMAINS:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 133
    .local v1, "domain":Ljava/lang/String;
    invoke-interface {p0}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->domainMatches(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 134
    const/4 v4, 0x1

    .line 139
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "domain":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_0
    return v4

    .line 132
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "domain":Ljava/lang/String;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static domainMatches(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p0, "domain"    # Ljava/lang/String;
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 170
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-le v4, v5, :cond_1

    .line 187
    :cond_0
    :goto_0
    return v3

    .line 174
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v3, v2

    .line 175
    goto :goto_0

    .line 179
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .line 180
    .local v0, "cOffset":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    .local v1, "sOffset":I
    :goto_1
    const/4 v4, -0x1

    if-le v1, v4, :cond_3

    .line 181
    invoke-virtual {p1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v4

    invoke-virtual {p0, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->toLowerCase(I)I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 185
    add-int/lit8 v0, v0, -0x1

    .line 180
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 187
    :cond_3
    invoke-virtual {p1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v4

    sget v5, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->DOT:I

    if-ne v4, v5, :cond_4

    :goto_2
    move v3, v2

    goto :goto_0

    :cond_4
    move v2, v3

    goto :goto_2
.end method

.method public static getCookieValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "headerValue"    # Ljava/lang/String;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 229
    sget-object v4, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->COOKIE_PAIR:Ljava/util/regex/Pattern;

    invoke-virtual {v4, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 230
    .local v1, "matcher":Ljava/util/regex/Matcher;
    :cond_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 231
    const/4 v4, 0x2

    invoke-virtual {v1, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 232
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v0

    .line 233
    .local v0, "cookiePairString":Ljava/lang/String;
    const-string v4, "="

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 234
    .local v2, "pos":I
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 235
    .local v3, "value":Ljava/lang/String;
    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 236
    const/4 v4, 0x1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 241
    .end local v0    # "cookiePairString":Ljava/lang/String;
    .end local v2    # "pos":I
    .end local v3    # "value":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static isCookiePresent(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "headerValue"    # Ljava/lang/String;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 196
    sget-object v1, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->COOKIE_PAIR:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 197
    .local v0, "matcher":Ljava/util/regex/Matcher;
    :cond_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 198
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 199
    const/4 v1, 0x1

    .line 202
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static replaceCookie(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "headerValue"    # Ljava/lang/String;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 210
    sget-object v2, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->COOKIE_PAIR:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 211
    .local v0, "matcher":Ljava/util/regex/Matcher;
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 212
    .local v1, "sb":Ljava/lang/StringBuffer;
    :goto_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 213
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 214
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->wrapInQuotesIfNeeded(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_0

    .line 217
    :cond_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_0

    .line 220
    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 221
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static setRequestCookie(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 11
    .param p0, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p1, "cookieSource"    # Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    invoke-static {p0, p1}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->cookieSourceApplies(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 80
    invoke-interface {p1}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;->getCookieName()Ljava/lang/String;

    move-result-object v1

    .line 81
    .local v1, "cookieName":Ljava/lang/String;
    invoke-interface {p1}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;->getCookieValue()Ljava/lang/String;

    move-result-object v6

    .line 83
    .local v6, "value":Ljava/lang/String;
    const/4 v2, 0x0

    .line 84
    .local v2, "found":Z
    const-string v7, "Cookie"

    invoke-interface {p0, v7}, Lorg/apache/http/client/methods/HttpUriRequest;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v0

    .local v0, "arr$":[Lorg/apache/http/Header;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v3, v0, v4

    .line 85
    .local v3, "header":Lorg/apache/http/Header;
    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v1}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->isCookiePresent(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 86
    if-nez v2, :cond_0

    instance-of v7, p0, Lorg/apache/http/impl/client/RequestWrapper;

    if-nez v7, :cond_0

    .line 87
    invoke-static {p0}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->wrapRequest(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/impl/client/RequestWrapper;

    move-result-object p0

    .line 89
    :cond_0
    const/4 v2, 0x1

    .line 90
    invoke-interface {p0, v3}, Lorg/apache/http/client/methods/HttpUriRequest;->removeHeader(Lorg/apache/http/Header;)V

    .line 91
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v1, v6}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->replaceCookie(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v7}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Lorg/apache/http/Header;)V

    .line 84
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 95
    .end local v3    # "header":Lorg/apache/http/Header;
    :cond_2
    if-nez v2, :cond_3

    .line 96
    new-instance v7, Lorg/apache/http/message/BasicHeader;

    const-string v8, "Cookie"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v6}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->wrapInQuotesIfNeeded(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v7}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Lorg/apache/http/Header;)V

    .line 100
    .end local v0    # "arr$":[Lorg/apache/http/Header;
    .end local v1    # "cookieName":Ljava/lang/String;
    .end local v2    # "found":Z
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "value":Ljava/lang/String;
    :cond_3
    return-object p0
.end method

.method public static updateFromResponseCookie(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)Lorg/apache/http/HttpResponse;
    .locals 7
    .param p0, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p1, "response"    # Lorg/apache/http/HttpResponse;
    .param p2, "cookieSource"    # Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;

    .prologue
    .line 105
    invoke-static {p0, p2}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->cookieSourceApplies(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 106
    invoke-interface {p2}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;->getCookieName()Ljava/lang/String;

    move-result-object v1

    .line 107
    .local v1, "cookieName":Ljava/lang/String;
    const-string v6, "Set-Cookie"

    invoke-interface {p1, v6}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v0

    .local v0, "arr$":[Lorg/apache/http/Header;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v2, v0, v3

    .line 108
    .local v2, "header":Lorg/apache/http/Header;
    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v1}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->getCookieValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 109
    .local v5, "value":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 110
    invoke-interface {p2, v5}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;->setCookieValue(Ljava/lang/String;)V

    .line 107
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 114
    .end local v2    # "header":Lorg/apache/http/Header;
    .end local v5    # "value":Ljava/lang/String;
    :cond_1
    const-string v6, "Set-Cookie2"

    invoke-interface {p1, v6}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v0

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v2, v0, v3

    .line 115
    .restart local v2    # "header":Lorg/apache/http/Header;
    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v1}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->getCookieValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 116
    .restart local v5    # "value":Ljava/lang/String;
    if-eqz v5, :cond_2

    .line 117
    invoke-interface {p2, v5}, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource;->setCookieValue(Ljava/lang/String;)V

    .line 114
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 125
    .end local v0    # "arr$":[Lorg/apache/http/Header;
    .end local v1    # "cookieName":Ljava/lang/String;
    .end local v2    # "header":Lorg/apache/http/Header;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "value":Ljava/lang/String;
    :cond_3
    return-object p1
.end method

.method public static wrapInQuotesIfNeeded(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 247
    if-nez p0, :cond_1

    .line 248
    const-string p0, "\"\""

    .line 256
    .end local p0    # "value":Ljava/lang/String;
    .local v0, "matcher":Ljava/util/regex/Matcher;
    :cond_0
    :goto_0
    return-object p0

    .line 250
    .end local v0    # "matcher":Ljava/util/regex/Matcher;
    .restart local p0    # "value":Ljava/lang/String;
    :cond_1
    sget-object v1, Lcom/google/android/pseudonymous_http/PseudonymousCookieSource$Helper;->VALUE:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 251
    .restart local v0    # "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_0

    .line 256
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private static wrapRequest(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/impl/client/RequestWrapper;
    .locals 3
    .param p0, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 151
    :try_start_0
    instance-of v2, p0, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-eqz v2, :cond_0

    .line 152
    new-instance v1, Lorg/apache/http/impl/client/EntityEnclosingRequestWrapper;

    check-cast p0, Lorg/apache/http/HttpEntityEnclosingRequest;

    .end local p0    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    invoke-direct {v1, p0}, Lorg/apache/http/impl/client/EntityEnclosingRequestWrapper;-><init>(Lorg/apache/http/HttpEntityEnclosingRequest;)V

    .line 159
    .local v1, "wrapped":Lorg/apache/http/impl/client/RequestWrapper;
    :goto_0
    invoke-virtual {v1}, Lorg/apache/http/impl/client/RequestWrapper;->resetHeaders()V

    .line 161
    return-object v1

    .line 155
    .end local v1    # "wrapped":Lorg/apache/http/impl/client/RequestWrapper;
    .restart local p0    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    :cond_0
    new-instance v1, Lorg/apache/http/impl/client/RequestWrapper;

    invoke-direct {v1, p0}, Lorg/apache/http/impl/client/RequestWrapper;-><init>(Lorg/apache/http/HttpRequest;)V
    :try_end_0
    .catch Lorg/apache/http/ProtocolException; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v1    # "wrapped":Lorg/apache/http/impl/client/RequestWrapper;
    goto :goto_0

    .line 162
    .end local v1    # "wrapped":Lorg/apache/http/impl/client/RequestWrapper;
    .end local p0    # "request":Lorg/apache/http/client/methods/HttpUriRequest;
    :catch_0
    move-exception v0

    .line 163
    .local v0, "e":Lorg/apache/http/ProtocolException;
    new-instance v2, Lorg/apache/http/client/ClientProtocolException;

    invoke-direct {v2, v0}, Lorg/apache/http/client/ClientProtocolException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method
