.class Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;
.super Landroid/support/v7/widget/RecyclerView$OnScrollListener;
.source "DialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/app/DialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SelectorAnimator"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/recline/app/DialogFragment$SelectorAnimator$Listener;
    }
.end annotation


# instance fields
.field private final mAnimationDuration:I

.field private volatile mFadedOut:Z

.field private final mParentView:Landroid/view/ViewGroup;

.field private final mSelectorView:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 2
    .param p1, "selectorView"    # Landroid/view/View;
    .param p2, "parentView"    # Landroid/view/ViewGroup;

    .prologue
    .line 729
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;-><init>()V

    .line 727
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mFadedOut:Z

    .line 730
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mSelectorView:Landroid/view/View;

    .line 731
    iput-object p2, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mParentView:Landroid/view/ViewGroup;

    .line 732
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/recline/R$integer;->lb_dialog_animation_duration:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mAnimationDuration:I

    .line 734
    return-void
.end method

.method static synthetic access$202(Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;
    .param p1, "x1"    # Z

    .prologue
    .line 722
    iput-boolean p1, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mFadedOut:Z

    return p1
.end method


# virtual methods
.method public onScrollStateChanged(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 9
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "newState"    # I

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 742
    if-nez p2, :cond_3

    .line 746
    iget-object v5, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mSelectorView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 747
    .local v4, "selectorHeight":I
    if-nez v4, :cond_0

    .line 748
    iget-object v5, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mSelectorView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 749
    .local v2, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget-object v5, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mSelectorView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/recline/R$dimen;->lb_action_fragment_selector_min_height:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 751
    iget-object v5, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mSelectorView:Landroid/view/View;

    invoke-virtual {v5, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 753
    .end local v2    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    iget-object v5, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getFocusedChild()Landroid/view/View;

    move-result-object v1

    .line 754
    .local v1, "focusedChild":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 755
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v5, v5

    int-to-float v6, v4

    div-float v3, v5, v6

    .line 756
    .local v3, "scaleY":F
    iget-object v5, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mSelectorView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    new-instance v6, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator$Listener;

    const/4 v7, 0x0

    invoke-direct {v6, p0, v7}, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator$Listener;-><init>(Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;Z)V

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    iget v6, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mAnimationDuration:I

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v6, v8}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 761
    .local v0, "animation":Landroid/view/ViewPropertyAnimator;
    iget-boolean v5, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mFadedOut:Z

    if-eqz v5, :cond_2

    .line 765
    iget-object v5, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mSelectorView:Landroid/view/View;

    invoke-virtual {v5, v3}, Landroid/view/View;->setScaleY(F)V

    .line 772
    :goto_0
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 782
    .end local v0    # "animation":Landroid/view/ViewPropertyAnimator;
    .end local v1    # "focusedChild":Landroid/view/View;
    .end local v3    # "scaleY":F
    .end local v4    # "selectorHeight":I
    :cond_1
    :goto_1
    return-void

    .line 770
    .restart local v0    # "animation":Landroid/view/ViewPropertyAnimator;
    .restart local v1    # "focusedChild":Landroid/view/View;
    .restart local v3    # "scaleY":F
    .restart local v4    # "selectorHeight":I
    :cond_2
    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 775
    .end local v0    # "animation":Landroid/view/ViewPropertyAnimator;
    .end local v1    # "focusedChild":Landroid/view/View;
    .end local v3    # "scaleY":F
    .end local v4    # "selectorHeight":I
    :cond_3
    iget-object v5, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mSelectorView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    iget v6, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mAnimationDuration:I

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v6, v8}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    new-instance v6, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator$Listener;

    const/4 v7, 0x1

    invoke-direct {v6, p0, v7}, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator$Listener;-><init>(Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;Z)V

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_1
.end method
