.class Lcom/google/android/recline/app/DialogFragment$SelectorAnimator$Listener;
.super Ljava/lang/Object;
.source "DialogFragment.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Listener"
.end annotation


# instance fields
.field private mCanceled:Z

.field private mFadingOut:Z

.field final synthetic this$0:Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;


# direct methods
.method public constructor <init>(Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;Z)V
    .locals 0
    .param p2, "fadingOut"    # Z

    .prologue
    .line 795
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator$Listener;->this$0:Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 796
    iput-boolean p2, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator$Listener;->mFadingOut:Z

    .line 797
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 815
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator$Listener;->mCanceled:Z

    .line 816
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 808
    iget-boolean v0, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator$Listener;->mCanceled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator$Listener;->mFadingOut:Z

    if-eqz v0, :cond_0

    .line 809
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator$Listener;->this$0:Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mFadedOut:Z
    invoke-static {v0, v1}, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->access$202(Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;Z)Z

    .line 811
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 820
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 801
    iget-boolean v0, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator$Listener;->mFadingOut:Z

    if-nez v0, :cond_0

    .line 802
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator$Listener;->this$0:Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->mFadedOut:Z
    invoke-static {v0, v1}, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;->access$202(Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;Z)Z

    .line 804
    :cond_0
    return-void
.end method
