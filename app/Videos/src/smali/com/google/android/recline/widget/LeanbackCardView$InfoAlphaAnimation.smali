.class Lcom/google/android/recline/widget/LeanbackCardView$InfoAlphaAnimation;
.super Landroid/view/animation/Animation;
.source "LeanbackCardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/widget/LeanbackCardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InfoAlphaAnimation"
.end annotation


# instance fields
.field private mDelta:F

.field private mStartValue:F

.field final synthetic this$0:Lcom/google/android/recline/widget/LeanbackCardView;


# direct methods
.method public constructor <init>(Lcom/google/android/recline/widget/LeanbackCardView;FF)V
    .locals 1
    .param p2, "start"    # F
    .param p3, "end"    # F

    .prologue
    .line 832
    iput-object p1, p0, Lcom/google/android/recline/widget/LeanbackCardView$InfoAlphaAnimation;->this$0:Lcom/google/android/recline/widget/LeanbackCardView;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 833
    iput p2, p0, Lcom/google/android/recline/widget/LeanbackCardView$InfoAlphaAnimation;->mStartValue:F

    .line 834
    sub-float v0, p3, p2

    iput v0, p0, Lcom/google/android/recline/widget/LeanbackCardView$InfoAlphaAnimation;->mDelta:F

    .line 835
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 4
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    .line 839
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView$InfoAlphaAnimation;->this$0:Lcom/google/android/recline/widget/LeanbackCardView;

    iget v2, p0, Lcom/google/android/recline/widget/LeanbackCardView$InfoAlphaAnimation;->mStartValue:F

    iget v3, p0, Lcom/google/android/recline/widget/LeanbackCardView$InfoAlphaAnimation;->mDelta:F

    mul-float/2addr v3, p1

    add-float/2addr v2, v3

    # setter for: Lcom/google/android/recline/widget/LeanbackCardView;->mInfoAlpha:F
    invoke-static {v1, v2}, Lcom/google/android/recline/widget/LeanbackCardView;->access$302(Lcom/google/android/recline/widget/LeanbackCardView;F)F

    .line 840
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView$InfoAlphaAnimation;->this$0:Lcom/google/android/recline/widget/LeanbackCardView;

    # getter for: Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/recline/widget/LeanbackCardView;->access$400(Lcom/google/android/recline/widget/LeanbackCardView;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 841
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView$InfoAlphaAnimation;->this$0:Lcom/google/android/recline/widget/LeanbackCardView;

    # getter for: Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/recline/widget/LeanbackCardView;->access$400(Lcom/google/android/recline/widget/LeanbackCardView;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/recline/widget/LeanbackCardView$InfoAlphaAnimation;->this$0:Lcom/google/android/recline/widget/LeanbackCardView;

    # getter for: Lcom/google/android/recline/widget/LeanbackCardView;->mInfoAlpha:F
    invoke-static {v2}, Lcom/google/android/recline/widget/LeanbackCardView;->access$300(Lcom/google/android/recline/widget/LeanbackCardView;)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 840
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 843
    :cond_0
    return-void
.end method
