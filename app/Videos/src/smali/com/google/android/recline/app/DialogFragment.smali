.class public Lcom/google/android/recline/app/DialogFragment;
.super Landroid/app/Fragment;
.source "DialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/recline/app/DialogFragment$Action;,
        Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;,
        Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;,
        Lcom/google/android/recline/app/DialogFragment$Builder;
    }
.end annotation


# instance fields
.field private mActions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/recline/app/DialogFragment$Action;",
            ">;"
        }
    .end annotation
.end field

.field private mAdapter:Lcom/google/android/recline/app/DialogActionAdapter;

.field private mBreadcrumb:Ljava/lang/String;

.field private mDescription:Ljava/lang/String;

.field private mEntryTransitionPerformed:Z

.field private mIconBackgroundColor:I

.field private mIconBitmap:Landroid/graphics/Bitmap;

.field private mIconPadding:I

.field private mIconResourceId:I

.field private mIconUri:Landroid/net/Uri;

.field private mIntroAnimationInProgress:Z

.field private mListView:Landroid/support/v17/leanback/widget/VerticalGridView;

.field private mListener:Lcom/google/android/recline/app/DialogFragment$Action$Listener;

.field private mName:Ljava/lang/String;

.field private mSelectedIndex:I

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 199
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/recline/app/DialogFragment;->mIconBackgroundColor:I

    .line 203
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/recline/app/DialogFragment;->mSelectedIndex:I

    .line 929
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/recline/app/DialogFragment;)Lcom/google/android/recline/app/DialogFragment$Action$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogFragment;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment;->mListener:Lcom/google/android/recline/app/DialogFragment$Action$Listener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/recline/app/DialogFragment;Landroid/view/View;FZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogFragment;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # F
    .param p3, "x3"    # Z

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/recline/app/DialogFragment;->prepareAndAnimateView(Landroid/view/View;FZ)V

    return-void
.end method

.method public static add(Landroid/app/FragmentManager;Lcom/google/android/recline/app/DialogFragment;)V
    .locals 6
    .param p0, "fm"    # Landroid/app/FragmentManager;
    .param p1, "f"    # Lcom/google/android/recline/app/DialogFragment;

    .prologue
    const/4 v2, 0x1

    .line 178
    const-string v3, "leanBackDialogFragment"

    invoke-virtual {p0, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    if-eqz v3, :cond_1

    move v1, v2

    .line 179
    .local v1, "hasDialog":Z
    :goto_0
    invoke-virtual {p0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 181
    .local v0, "ft":Landroid/app/FragmentTransaction;
    if-eqz v1, :cond_0

    .line 182
    const/4 v3, 0x2

    const/4 v4, 0x3

    const/4 v5, 0x4

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/app/FragmentTransaction;->setCustomAnimations(IIII)Landroid/app/FragmentTransaction;

    .line 185
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 187
    :cond_0
    const v2, 0x1020002

    const-string v3, "leanBackDialogFragment"

    invoke-virtual {v0, v2, p1, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 188
    return-void

    .line 178
    .end local v0    # "ft":Landroid/app/FragmentTransaction;
    .end local v1    # "hasDialog":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private createAlphaAnimator(Landroid/view/View;FF)Landroid/animation/Animator;
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "fromAlpha"    # F
    .param p3, "toAlpha"    # F

    .prologue
    .line 717
    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput p2, v2, v3

    const/4 v3, 0x1

    aput p3, v2, v3

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 718
    .local v0, "alphaAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p0}, Lcom/google/android/recline/app/DialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x10e0002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 719
    return-object v0
.end method

.method private createDummyAnimator(Landroid/view/View;Ljava/util/ArrayList;)Landroid/animation/Animator;
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator;",
            ">;)",
            "Landroid/animation/Animator;"
        }
    .end annotation

    .prologue
    .line 673
    .local p2, "animators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 674
    .local v0, "animatorSet":Landroid/animation/AnimatorSet;
    invoke-virtual {v0, p2}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 675
    new-instance v1, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;

    invoke-direct {v1, v0}, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;-><init>(Landroid/animation/AnimatorSet;)V

    return-object v1
.end method

.method private createFadeOutAnimator(Landroid/view/View;)Landroid/animation/Animator;
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 701
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/recline/app/DialogFragment;->createAlphaAnimator(Landroid/view/View;FF)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method private createSlideLeftInAnimator(Landroid/view/View;)Landroid/animation/Animator;
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 689
    const/high16 v2, 0x43480000    # 200.0f

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/recline/app/DialogFragment;->createTranslateAlphaAnimator(Landroid/view/View;FFFF)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method private createSlideLeftOutAnimator(Landroid/view/View;)Landroid/animation/Animator;
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 685
    const/high16 v3, -0x3cb80000    # -200.0f

    const/high16 v4, 0x3f800000    # 1.0f

    move-object v0, p0

    move-object v1, p1

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/recline/app/DialogFragment;->createTranslateAlphaAnimator(Landroid/view/View;FFFF)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method private createSlideRightInAnimator(Landroid/view/View;)Landroid/animation/Animator;
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 693
    const/high16 v2, -0x3cb80000    # -200.0f

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/recline/app/DialogFragment;->createTranslateAlphaAnimator(Landroid/view/View;FFFF)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method private createSlideRightOutAnimator(Landroid/view/View;)Landroid/animation/Animator;
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 697
    const/high16 v3, 0x43480000    # 200.0f

    const/high16 v4, 0x3f800000    # 1.0f

    move-object v0, p0

    move-object v1, p1

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/recline/app/DialogFragment;->createTranslateAlphaAnimator(Landroid/view/View;FFFF)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method private createTranslateAlphaAnimator(Landroid/view/View;FFFF)Landroid/animation/Animator;
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "fromTranslateX"    # F
    .param p3, "toTranslateX"    # F
    .param p4, "fromAlpha"    # F
    .param p5, "toAlpha"    # F

    .prologue
    .line 706
    const-string v3, "translationX"

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput p2, v4, v5

    const/4 v5, 0x1

    aput p3, v4, v5

    invoke-static {p1, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 708
    .local v2, "translateAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p0}, Lcom/google/android/recline/app/DialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x10e0002

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 710
    invoke-direct {p0, p1, p4, p5}, Lcom/google/android/recline/app/DialogFragment;->createAlphaAnimator(Landroid/view/View;FF)Landroid/animation/Animator;

    move-result-object v0

    .line 711
    .local v0, "alphaAnimator":Landroid/animation/Animator;
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 712
    .local v1, "animatorSet":Landroid/animation/AnimatorSet;
    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 713
    return-object v1
.end method

.method private getFirstCheckedAction()I
    .locals 3

    .prologue
    .line 550
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v2, p0, Lcom/google/android/recline/app/DialogFragment;->mActions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "size":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 551
    iget-object v2, p0, Lcom/google/android/recline/app/DialogFragment;->mActions:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/recline/app/DialogFragment$Action;

    invoke-virtual {v2}, Lcom/google/android/recline/app/DialogFragment$Action;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 555
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 550
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 555
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private performEntryTransition()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 579
    invoke-virtual {p0}, Lcom/google/android/recline/app/DialogFragment;->getView()Landroid/view/View;

    move-result-object v3

    .line 580
    .local v3, "dialogView":Landroid/view/View;
    sget v0, Lcom/google/android/recline/R$id;->content_fragment:I

    invoke-virtual {v3, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 581
    .local v2, "contentView":Landroid/view/View;
    sget v0, Lcom/google/android/recline/R$id;->action_fragment:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 583
    .local v5, "actionContainerView":Landroid/view/View;
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/recline/app/DialogFragment;->mIntroAnimationInProgress:Z

    .line 586
    invoke-virtual {p0}, Lcom/google/android/recline/app/DialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/recline/R$anim;->lb_dialog_fade_out:I

    invoke-virtual {v0, v7, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 588
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/recline/R$color;->lb_dialog_activity_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 590
    .local v6, "bgColor":I
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    .line 591
    .local v4, "bgDrawable":Landroid/graphics/drawable/ColorDrawable;
    invoke-virtual {v4, v6}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 592
    invoke-virtual {v4, v7}, Landroid/graphics/drawable/ColorDrawable;->setAlpha(I)V

    .line 593
    invoke-virtual {v3, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 594
    const/4 v0, 0x4

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 600
    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v7

    new-instance v0, Lcom/google/android/recline/app/DialogFragment$3;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/recline/app/DialogFragment$3;-><init>(Lcom/google/android/recline/app/DialogFragment;Landroid/view/View;Landroid/view/View;Landroid/graphics/drawable/ColorDrawable;Landroid/view/View;)V

    invoke-virtual {v7, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 649
    return-void
.end method

.method private prepareAndAnimateView(Landroid/view/View;FZ)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "initTransX"    # F
    .param p3, "notifyAnimationFinished"    # Z

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 653
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 654
    invoke-virtual {p1}, Landroid/view/View;->buildLayer()V

    .line 655
    invoke-virtual {p1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 656
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    .line 657
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x78

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 659
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 660
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/recline/app/DialogFragment$4;

    invoke-direct {v1, p0, p1, p3}, Lcom/google/android/recline/app/DialogFragment$4;-><init>(Lcom/google/android/recline/app/DialogFragment;Landroid/view/View;Z)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 669
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 670
    return-void
.end method

.method private setActionView(Landroid/view/View;)V
    .locals 6
    .param p1, "action"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 490
    new-instance v1, Lcom/google/android/recline/app/DialogActionAdapter;

    new-instance v2, Lcom/google/android/recline/app/DialogFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/recline/app/DialogFragment$1;-><init>(Lcom/google/android/recline/app/DialogFragment;)V

    new-instance v3, Lcom/google/android/recline/app/DialogFragment$2;

    invoke-direct {v3, p0}, Lcom/google/android/recline/app/DialogFragment$2;-><init>(Lcom/google/android/recline/app/DialogFragment;)V

    iget-object v4, p0, Lcom/google/android/recline/app/DialogFragment;->mActions:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/recline/app/DialogActionAdapter;-><init>(Lcom/google/android/recline/app/DialogFragment$Action$Listener;Lcom/google/android/recline/app/DialogFragment$Action$OnFocusListener;Ljava/util/List;)V

    iput-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mAdapter:Lcom/google/android/recline/app/DialogActionAdapter;

    .line 520
    instance-of v1, p1, Landroid/support/v17/leanback/widget/VerticalGridView;

    if-eqz v1, :cond_1

    move-object v1, p1

    .line 521
    check-cast v1, Landroid/support/v17/leanback/widget/VerticalGridView;

    iput-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mListView:Landroid/support/v17/leanback/widget/VerticalGridView;

    .line 540
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mListView:Landroid/support/v17/leanback/widget/VerticalGridView;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->requestFocusFromTouch()Z

    .line 541
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mListView:Landroid/support/v17/leanback/widget/VerticalGridView;

    iget-object v2, p0, Lcom/google/android/recline/app/DialogFragment;->mAdapter:Lcom/google/android/recline/app/DialogActionAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 542
    iget-object v2, p0, Lcom/google/android/recline/app/DialogFragment;->mListView:Landroid/support/v17/leanback/widget/VerticalGridView;

    iget v1, p0, Lcom/google/android/recline/app/DialogFragment;->mSelectedIndex:I

    if-ltz v1, :cond_3

    iget v1, p0, Lcom/google/android/recline/app/DialogFragment;->mSelectedIndex:I

    iget-object v3, p0, Lcom/google/android/recline/app/DialogFragment;->mActions:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    iget v1, p0, Lcom/google/android/recline/app/DialogFragment;->mSelectedIndex:I

    :goto_1
    invoke-virtual {v2, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setSelectedPosition(I)V

    .line 545
    sget v1, Lcom/google/android/recline/R$id;->list:I

    iget-object v2, p0, Lcom/google/android/recline/app/DialogFragment;->mListView:Landroid/support/v17/leanback/widget/VerticalGridView;

    invoke-virtual {p1, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 546
    sget v1, Lcom/google/android/recline/R$id;->selector:I

    sget v2, Lcom/google/android/recline/R$id;->selector:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 547
    return-void

    .line 523
    :cond_1
    sget v1, Lcom/google/android/recline/R$id;->list:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/VerticalGridView;

    iput-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mListView:Landroid/support/v17/leanback/widget/VerticalGridView;

    .line 524
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mListView:Landroid/support/v17/leanback/widget/VerticalGridView;

    if-nez v1, :cond_2

    .line 525
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "No ListView exists."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 531
    :cond_2
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mListView:Landroid/support/v17/leanback/widget/VerticalGridView;

    invoke-virtual {v1, v5}, Landroid/support/v17/leanback/widget/VerticalGridView;->setWindowAlignmentOffset(I)V

    .line 532
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mListView:Landroid/support/v17/leanback/widget/VerticalGridView;

    const/high16 v2, 0x42480000    # 50.0f

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->setWindowAlignmentOffsetPercent(F)V

    .line 533
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mListView:Landroid/support/v17/leanback/widget/VerticalGridView;

    invoke-virtual {v1, v5}, Landroid/support/v17/leanback/widget/VerticalGridView;->setWindowAlignment(I)V

    .line 534
    sget v1, Lcom/google/android/recline/R$id;->selector:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 535
    .local v0, "selectorView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 536
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mListView:Landroid/support/v17/leanback/widget/VerticalGridView;

    new-instance v2, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;

    iget-object v3, p0, Lcom/google/android/recline/app/DialogFragment;->mListView:Landroid/support/v17/leanback/widget/VerticalGridView;

    invoke-direct {v2, v0, v3}, Lcom/google/android/recline/app/DialogFragment$SelectorAnimator;-><init>(Landroid/view/View;Landroid/view/ViewGroup;)V

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->setOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    goto :goto_0

    .line 542
    .end local v0    # "selectorView":Landroid/view/View;
    :cond_3
    invoke-direct {p0}, Lcom/google/android/recline/app/DialogFragment;->getFirstCheckedAction()I

    move-result v1

    goto :goto_1
.end method

.method private setContentView(Landroid/view/View;)V
    .locals 8
    .param p1, "content"    # Landroid/view/View;

    .prologue
    .line 434
    sget v4, Lcom/google/android/recline/R$id;->title:I

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 435
    .local v3, "titleView":Landroid/widget/TextView;
    sget v4, Lcom/google/android/recline/R$id;->breadcrumb:I

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 436
    .local v0, "breadcrumbView":Landroid/widget/TextView;
    sget v4, Lcom/google/android/recline/R$id;->description:I

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 437
    .local v1, "descriptionView":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/google/android/recline/app/DialogFragment;->mTitle:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 438
    iget-object v4, p0, Lcom/google/android/recline/app/DialogFragment;->mBreadcrumb:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 439
    iget-object v4, p0, Lcom/google/android/recline/app/DialogFragment;->mDescription:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 440
    sget v4, Lcom/google/android/recline/R$id;->icon:I

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 441
    .local v2, "iconImageView":Landroid/widget/ImageView;
    iget v4, p0, Lcom/google/android/recline/app/DialogFragment;->mIconBackgroundColor:I

    if-eqz v4, :cond_0

    .line 442
    iget v4, p0, Lcom/google/android/recline/app/DialogFragment;->mIconBackgroundColor:I

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 444
    :cond_0
    iget v4, p0, Lcom/google/android/recline/app/DialogFragment;->mIconPadding:I

    iget v5, p0, Lcom/google/android/recline/app/DialogFragment;->mIconPadding:I

    iget v6, p0, Lcom/google/android/recline/app/DialogFragment;->mIconPadding:I

    iget v7, p0, Lcom/google/android/recline/app/DialogFragment;->mIconPadding:I

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 446
    iget v4, p0, Lcom/google/android/recline/app/DialogFragment;->mIconResourceId:I

    if-eqz v4, :cond_1

    .line 447
    iget v4, p0, Lcom/google/android/recline/app/DialogFragment;->mIconResourceId:I

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 448
    invoke-direct {p0, v2}, Lcom/google/android/recline/app/DialogFragment;->updateViewSize(Landroid/widget/ImageView;)V

    .line 483
    :goto_0
    sget v4, Lcom/google/android/recline/R$id;->title:I

    invoke-virtual {p1, v4, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 484
    sget v4, Lcom/google/android/recline/R$id;->breadcrumb:I

    invoke-virtual {p1, v4, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 485
    sget v4, Lcom/google/android/recline/R$id;->description:I

    invoke-virtual {p1, v4, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 486
    sget v4, Lcom/google/android/recline/R$id;->icon:I

    invoke-virtual {p1, v4, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 487
    return-void

    .line 450
    :cond_1
    iget-object v4, p0, Lcom/google/android/recline/app/DialogFragment;->mIconBitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_2

    .line 451
    iget-object v4, p0, Lcom/google/android/recline/app/DialogFragment;->mIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 452
    invoke-direct {p0, v2}, Lcom/google/android/recline/app/DialogFragment;->updateViewSize(Landroid/widget/ImageView;)V

    goto :goto_0

    .line 454
    :cond_2
    iget-object v4, p0, Lcom/google/android/recline/app/DialogFragment;->mIconUri:Landroid/net/Uri;

    if-eqz v4, :cond_3

    .line 455
    const/4 v4, 0x4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 478
    :cond_3
    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateViewSize(Landroid/widget/ImageView;)V
    .locals 4
    .param p1, "iconView"    # Landroid/widget/ImageView;

    .prologue
    .line 559
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 560
    .local v0, "intrinsicWidth":I
    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 561
    .local v1, "lp":Landroid/view/ViewGroup$LayoutParams;
    if-lez v0, :cond_0

    .line 562
    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    mul-int/2addr v2, v3

    div-int/2addr v2, v0

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 568
    :goto_0
    return-void

    .line 566
    :cond_0
    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method


# virtual methods
.method public getSelectedItemPosition()I
    .locals 2

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment;->mListView:Landroid/support/v17/leanback/widget/VerticalGridView;

    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mListView:Landroid/support/v17/leanback/widget/VerticalGridView;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->getFocusedChild()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 209
    const-string v1, "DialogFragment"

    const-string v2, "onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 211
    if-eqz p1, :cond_b

    move-object v0, p1

    .line 212
    .local v0, "state":Landroid/os/Bundle;
    :goto_0
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mTitle:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 213
    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mTitle:Ljava/lang/String;

    .line 215
    :cond_0
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mBreadcrumb:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 216
    const-string v1, "breadcrumb"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mBreadcrumb:Ljava/lang/String;

    .line 218
    :cond_1
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mDescription:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 219
    const-string v1, "description"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mDescription:Ljava/lang/String;

    .line 221
    :cond_2
    iget v1, p0, Lcom/google/android/recline/app/DialogFragment;->mIconResourceId:I

    if-nez v1, :cond_3

    .line 222
    const-string v1, "iconResourceId"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/recline/app/DialogFragment;->mIconResourceId:I

    .line 224
    :cond_3
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mIconUri:Landroid/net/Uri;

    if-nez v1, :cond_4

    .line 225
    const-string v1, "iconUri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    iput-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mIconUri:Landroid/net/Uri;

    .line 227
    :cond_4
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mIconBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_5

    .line 228
    const-string v1, "iconBitmap"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mIconBitmap:Landroid/graphics/Bitmap;

    .line 230
    :cond_5
    iget v1, p0, Lcom/google/android/recline/app/DialogFragment;->mIconBackgroundColor:I

    if-nez v1, :cond_6

    .line 231
    const-string v1, "iconBackground"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/recline/app/DialogFragment;->mIconBackgroundColor:I

    .line 233
    :cond_6
    iget v1, p0, Lcom/google/android/recline/app/DialogFragment;->mIconPadding:I

    if-nez v1, :cond_7

    .line 234
    const-string v1, "iconPadding"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/recline/app/DialogFragment;->mIconPadding:I

    .line 236
    :cond_7
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mActions:Ljava/util/ArrayList;

    if-nez v1, :cond_8

    .line 237
    const-string v1, "actions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mActions:Ljava/util/ArrayList;

    .line 239
    :cond_8
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mName:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 240
    const-string v1, "name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mName:Ljava/lang/String;

    .line 242
    :cond_9
    iget v1, p0, Lcom/google/android/recline/app/DialogFragment;->mSelectedIndex:I

    if-ne v1, v4, :cond_a

    .line 243
    const-string v1, "selectedIndex"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/recline/app/DialogFragment;->mSelectedIndex:I

    .line 245
    :cond_a
    const-string v1, "entryTransitionPerformed"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/recline/app/DialogFragment;->mEntryTransitionPerformed:Z

    .line 246
    return-void

    .line 211
    .end local v0    # "state":Landroid/os/Bundle;
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/recline/app/DialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public onCreateAnimator(IZI)Landroid/animation/Animator;
    .locals 12
    .param p1, "transit"    # I
    .param p2, "enter"    # Z
    .param p3, "nextAnim"    # I

    .prologue
    .line 298
    invoke-virtual {p0}, Lcom/google/android/recline/app/DialogFragment;->getView()Landroid/view/View;

    move-result-object v6

    .line 299
    .local v6, "dialogView":Landroid/view/View;
    sget v11, Lcom/google/android/recline/R$id;->content_fragment:I

    invoke-virtual {v6, v11}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .line 300
    .local v4, "contentView":Landroid/view/View;
    sget v11, Lcom/google/android/recline/R$id;->action_fragment:I

    invoke-virtual {v6, v11}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 301
    .local v1, "actionView":Landroid/view/View;
    sget v11, Lcom/google/android/recline/R$id;->action_fragment:I

    invoke-virtual {v6, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 302
    .local v0, "actionContainerView":Landroid/view/View;
    sget v11, Lcom/google/android/recline/R$id;->title:I

    invoke-virtual {v4, v11}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    .line 303
    .local v10, "titleView":Landroid/view/View;
    sget v11, Lcom/google/android/recline/R$id;->breadcrumb:I

    invoke-virtual {v4, v11}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 304
    .local v3, "breadcrumbView":Landroid/view/View;
    sget v11, Lcom/google/android/recline/R$id;->description:I

    invoke-virtual {v4, v11}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    .line 305
    .local v5, "descriptionView":Landroid/view/View;
    sget v11, Lcom/google/android/recline/R$id;->icon:I

    invoke-virtual {v4, v11}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    .line 306
    .local v7, "iconView":Landroid/view/View;
    sget v11, Lcom/google/android/recline/R$id;->list:I

    invoke-virtual {v1, v11}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    .line 307
    .local v8, "listView":Landroid/view/View;
    sget v11, Lcom/google/android/recline/R$id;->selector:I

    invoke-virtual {v1, v11}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/View;

    .line 309
    .local v9, "selectorView":Landroid/view/View;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 311
    .local v2, "animators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    packed-switch p3, :pswitch_data_0

    .line 347
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onCreateAnimator(IZI)Landroid/animation/Animator;

    move-result-object v11

    .line 351
    :goto_0
    return-object v11

    .line 313
    :pswitch_0
    invoke-direct {p0, v10}, Lcom/google/android/recline/app/DialogFragment;->createSlideLeftInAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 314
    invoke-direct {p0, v3}, Lcom/google/android/recline/app/DialogFragment;->createSlideLeftInAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 315
    invoke-direct {p0, v5}, Lcom/google/android/recline/app/DialogFragment;->createSlideLeftInAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 316
    invoke-direct {p0, v7}, Lcom/google/android/recline/app/DialogFragment;->createSlideLeftInAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 317
    invoke-direct {p0, v8}, Lcom/google/android/recline/app/DialogFragment;->createSlideLeftInAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 318
    invoke-direct {p0, v9}, Lcom/google/android/recline/app/DialogFragment;->createSlideLeftInAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 350
    :goto_1
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/google/android/recline/app/DialogFragment;->mEntryTransitionPerformed:Z

    .line 351
    invoke-direct {p0, v6, v2}, Lcom/google/android/recline/app/DialogFragment;->createDummyAnimator(Landroid/view/View;Ljava/util/ArrayList;)Landroid/animation/Animator;

    move-result-object v11

    goto :goto_0

    .line 321
    :pswitch_1
    invoke-direct {p0, v10}, Lcom/google/android/recline/app/DialogFragment;->createSlideLeftOutAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 322
    invoke-direct {p0, v3}, Lcom/google/android/recline/app/DialogFragment;->createSlideLeftOutAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    invoke-direct {p0, v5}, Lcom/google/android/recline/app/DialogFragment;->createSlideLeftOutAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 324
    invoke-direct {p0, v7}, Lcom/google/android/recline/app/DialogFragment;->createSlideLeftOutAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 325
    invoke-direct {p0, v8}, Lcom/google/android/recline/app/DialogFragment;->createSlideLeftOutAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 326
    invoke-direct {p0, v9}, Lcom/google/android/recline/app/DialogFragment;->createSlideLeftOutAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 327
    invoke-direct {p0, v0}, Lcom/google/android/recline/app/DialogFragment;->createFadeOutAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 330
    :pswitch_2
    invoke-direct {p0, v10}, Lcom/google/android/recline/app/DialogFragment;->createSlideRightInAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 331
    invoke-direct {p0, v3}, Lcom/google/android/recline/app/DialogFragment;->createSlideRightInAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 332
    invoke-direct {p0, v5}, Lcom/google/android/recline/app/DialogFragment;->createSlideRightInAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 333
    invoke-direct {p0, v7}, Lcom/google/android/recline/app/DialogFragment;->createSlideRightInAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334
    invoke-direct {p0, v8}, Lcom/google/android/recline/app/DialogFragment;->createSlideRightInAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    invoke-direct {p0, v9}, Lcom/google/android/recline/app/DialogFragment;->createSlideRightInAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 338
    :pswitch_3
    invoke-direct {p0, v10}, Lcom/google/android/recline/app/DialogFragment;->createSlideRightOutAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 339
    invoke-direct {p0, v3}, Lcom/google/android/recline/app/DialogFragment;->createSlideRightOutAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 340
    invoke-direct {p0, v5}, Lcom/google/android/recline/app/DialogFragment;->createSlideRightOutAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 341
    invoke-direct {p0, v7}, Lcom/google/android/recline/app/DialogFragment;->createSlideRightOutAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 342
    invoke-direct {p0, v8}, Lcom/google/android/recline/app/DialogFragment;->createSlideRightOutAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 343
    invoke-direct {p0, v9}, Lcom/google/android/recline/app/DialogFragment;->createSlideRightOutAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    invoke-direct {p0, v0}, Lcom/google/android/recline/app/DialogFragment;->createFadeOutAnimator(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 311
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 252
    sget v5, Lcom/google/android/recline/R$layout;->lb_dialog_fragment:I

    invoke-virtual {p1, v5, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 254
    .local v4, "v":Landroid/view/View;
    sget v5, Lcom/google/android/recline/R$id;->content_fragment:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 255
    .local v3, "contentContainer":Landroid/view/View;
    sget v5, Lcom/google/android/recline/R$layout;->lb_dialog_content:I

    invoke-virtual {p1, v5, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 256
    .local v2, "content":Landroid/view/View;
    check-cast v3, Landroid/view/ViewGroup;

    .end local v3    # "contentContainer":Landroid/view/View;
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 257
    invoke-direct {p0, v2}, Lcom/google/android/recline/app/DialogFragment;->setContentView(Landroid/view/View;)V

    .line 258
    sget v5, Lcom/google/android/recline/R$id;->content_fragment:I

    invoke-virtual {v4, v5, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 260
    sget v5, Lcom/google/android/recline/R$id;->action_fragment:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 261
    .local v1, "actionContainer":Landroid/view/View;
    sget v5, Lcom/google/android/recline/R$layout;->lb_dialog_action_list:I

    invoke-virtual {p1, v5, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 262
    .local v0, "action":Landroid/view/View;
    check-cast v1, Landroid/view/ViewGroup;

    .end local v1    # "actionContainer":Landroid/view/View;
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 263
    invoke-direct {p0, v0}, Lcom/google/android/recline/app/DialogFragment;->setActionView(Landroid/view/View;)V

    .line 264
    sget v5, Lcom/google/android/recline/R$id;->action_fragment:I

    invoke-virtual {v4, v5, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 266
    return-object v4
.end method

.method public onIntroAnimationFinished()V
    .locals 7

    .prologue
    .line 414
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/recline/app/DialogFragment;->mIntroAnimationInProgress:Z

    .line 417
    iget-object v5, p0, Lcom/google/android/recline/app/DialogFragment;->mListView:Landroid/support/v17/leanback/widget/VerticalGridView;

    invoke-virtual {v5}, Landroid/support/v17/leanback/widget/VerticalGridView;->getFocusedChild()Landroid/view/View;

    move-result-object v1

    .line 418
    .local v1, "focusedChild":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 419
    invoke-virtual {p0}, Lcom/google/android/recline/app/DialogFragment;->getView()Landroid/view/View;

    move-result-object v5

    sget v6, Lcom/google/android/recline/R$id;->action_fragment:I

    invoke-virtual {v5, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 420
    .local v0, "actionView":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 421
    .local v2, "height":I
    sget v5, Lcom/google/android/recline/R$id;->selector:I

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 422
    .local v4, "selectorView":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 423
    .local v3, "lp":Landroid/view/ViewGroup$LayoutParams;
    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 424
    invoke-virtual {v4, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 425
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Landroid/view/View;->setAlpha(F)V

    .line 427
    .end local v0    # "actionView":Landroid/view/View;
    .end local v2    # "height":I
    .end local v3    # "lp":Landroid/view/ViewGroup$LayoutParams;
    .end local v4    # "selectorView":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 271
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 272
    const-string v0, "title"

    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    const-string v0, "breadcrumb"

    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mBreadcrumb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    const-string v0, "description"

    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    const-string v0, "iconResourceId"

    iget v1, p0, Lcom/google/android/recline/app/DialogFragment;->mIconResourceId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 276
    const-string v0, "iconUri"

    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mIconUri:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 277
    const-string v0, "iconBitmap"

    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 278
    const-string v0, "iconBackground"

    iget v1, p0, Lcom/google/android/recline/app/DialogFragment;->mIconBackgroundColor:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 279
    const-string v0, "iconPadding"

    iget v1, p0, Lcom/google/android/recline/app/DialogFragment;->mIconPadding:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 280
    const-string v0, "actions"

    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mActions:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 281
    const-string v1, "selectedIndex"

    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment;->mListView:Landroid/support/v17/leanback/widget/VerticalGridView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/recline/app/DialogFragment;->getSelectedItemPosition()I

    move-result v0

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 283
    const-string v0, "name"

    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const-string v0, "entryTransitionPerformed"

    iget-boolean v1, p0, Lcom/google/android/recline/app/DialogFragment;->mEntryTransitionPerformed:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 285
    return-void

    .line 281
    :cond_0
    iget v0, p0, Lcom/google/android/recline/app/DialogFragment;->mSelectedIndex:I

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 289
    invoke-super {p0}, Landroid/app/Fragment;->onStart()V

    .line 290
    iget-boolean v0, p0, Lcom/google/android/recline/app/DialogFragment;->mEntryTransitionPerformed:Z

    if-nez v0, :cond_0

    .line 291
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/recline/app/DialogFragment;->mEntryTransitionPerformed:Z

    .line 292
    invoke-direct {p0}, Lcom/google/android/recline/app/DialogFragment;->performEntryTransition()V

    .line 294
    :cond_0
    return-void
.end method

.method public setActions(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/recline/app/DialogFragment$Action;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 389
    .local p1, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/recline/app/DialogFragment$Action;>;"
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment;->mActions:Ljava/util/ArrayList;

    .line 390
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment;->mAdapter:Lcom/google/android/recline/app/DialogActionAdapter;

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment;->mAdapter:Lcom/google/android/recline/app/DialogActionAdapter;

    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment;->mActions:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/google/android/recline/app/DialogActionAdapter;->setActions(Ljava/util/ArrayList;)V

    .line 393
    :cond_0
    return-void
.end method

.method public setListener(Lcom/google/android/recline/app/DialogFragment$Action$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/recline/app/DialogFragment$Action$Listener;

    .prologue
    .line 377
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment;->mListener:Lcom/google/android/recline/app/DialogFragment$Action$Listener;

    .line 378
    return-void
.end method
