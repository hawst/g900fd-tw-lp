.class Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "DialogActionAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/app/DialogActionAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ActionViewHolder"
.end annotation


# instance fields
.field private mAction:Lcom/google/android/recline/app/DialogFragment$Action;

.field private final mActionOnFocusAnimator:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;

.field private final mActionOnKeyPressAnimator:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;

.field private final mViewOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "actionOnKeyPressAnimator"    # Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;
    .param p3, "actionOnFocusAnimator"    # Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;
    .param p4, "viewOnClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 155
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 156
    iput-object p2, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->mActionOnKeyPressAnimator:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;

    .line 157
    iput-object p3, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->mActionOnFocusAnimator:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;

    .line 158
    iput-object p4, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->mViewOnClickListener:Landroid/view/View$OnClickListener;

    .line 159
    return-void
.end method

.method private getDescriptionMaxHeight(Landroid/content/Context;Landroid/widget/TextView;)I
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "title"    # Landroid/widget/TextView;

    .prologue
    .line 269
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 270
    .local v1, "res":Landroid/content/res/Resources;
    sget v4, Lcom/google/android/recline/R$dimen;->lb_dialog_list_item_vertical_padding:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 271
    .local v3, "verticalPadding":F
    sget v4, Lcom/google/android/recline/R$integer;->lb_dialog_action_title_max_lines:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 272
    .local v2, "titleMaxLines":I
    const-string v4, "window"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getHeight()I

    move-result v0

    .line 278
    .local v0, "displayHeight":I
    int-to-float v4, v0

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v5, v3

    sub-float/2addr v4, v5

    mul-int/lit8 v5, v2, 0x2

    invoke-virtual {p2}, Landroid/widget/TextView;->getLineHeight()I

    move-result v6

    mul-int/2addr v5, v6

    int-to-float v5, v5

    sub-float/2addr v4, v5

    float-to-int v4, v4

    return v4
.end method

.method private setIndicator(Landroid/widget/ImageView;Lcom/google/android/recline/app/DialogFragment$Action;)Z
    .locals 5
    .param p1, "indicatorView"    # Landroid/widget/ImageView;
    .param p2, "action"    # Lcom/google/android/recline/app/DialogFragment$Action;

    .prologue
    const/4 v3, 0x0

    .line 218
    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 219
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {p2, v0}, Lcom/google/android/recline/app/DialogFragment$Action;->getIndicator(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 220
    .local v2, "indicator":Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_0

    .line 221
    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 222
    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 251
    :goto_0
    const/4 v3, 0x1

    :goto_1
    return v3

    .line 224
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/recline/app/DialogFragment$Action;->getIconUri()Landroid/net/Uri;

    move-result-object v1

    .line 225
    .local v1, "iconUri":Landroid/net/Uri;
    if-eqz v1, :cond_1

    .line 226
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 247
    :cond_1
    const/16 v4, 0x8

    invoke-virtual {p1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public getAction()Lcom/google/android/recline/app/DialogFragment$Action;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->mAction:Lcom/google/android/recline/app/DialogFragment$Action;

    return-object v0
.end method

.method public init(Lcom/google/android/recline/app/DialogFragment$Action;)V
    .locals 12
    .param p1, "action"    # Lcom/google/android/recline/app/DialogFragment$Action;

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x0

    .line 166
    iput-object p1, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->mAction:Lcom/google/android/recline/app/DialogFragment$Action;

    .line 174
    iget-object v8, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->itemView:Landroid/view/View;

    sget v11, Lcom/google/android/recline/R$id;->action_title:I

    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 175
    .local v7, "title":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->itemView:Landroid/view/View;

    sget v11, Lcom/google/android/recline/R$id;->action_description:I

    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 176
    .local v4, "description":Landroid/widget/TextView;
    invoke-virtual {p1}, Lcom/google/android/recline/app/DialogFragment$Action;->getDescription()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    invoke-virtual {p1}, Lcom/google/android/recline/app/DialogFragment$Action;->getDescription()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    const/16 v8, 0x8

    :goto_0
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 179
    invoke-virtual {p1}, Lcom/google/android/recline/app/DialogFragment$Action;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v8, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->itemView:Landroid/view/View;

    sget v11, Lcom/google/android/recline/R$id;->action_checkmark:I

    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 181
    .local v0, "checkmarkView":Landroid/widget/ImageView;
    invoke-virtual {p1}, Lcom/google/android/recline/app/DialogFragment$Action;->isChecked()Z

    move-result v8

    if-eqz v8, :cond_1

    move v8, v9

    :goto_1
    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 183
    iget-object v8, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->itemView:Landroid/view/View;

    sget v11, Lcom/google/android/recline/R$id;->action_icon:I

    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 184
    .local v5, "indicatorView":Landroid/widget/ImageView;
    iget-object v8, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->itemView:Landroid/view/View;

    sget v11, Lcom/google/android/recline/R$id;->action_content:I

    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 185
    .local v2, "content":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 186
    .local v3, "contentLp":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {p0, v5, p1}, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->setIndicator(Landroid/widget/ImageView;Lcom/google/android/recline/app/DialogFragment$Action;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 187
    iget-object v8, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v11, Lcom/google/android/recline/R$dimen;->lb_action_text_width:I

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 193
    :goto_2
    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 195
    iget-object v8, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->itemView:Landroid/view/View;

    sget v11, Lcom/google/android/recline/R$id;->action_next_chevron:I

    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 196
    .local v1, "chevronView":Landroid/widget/ImageView;
    invoke-virtual {p1}, Lcom/google/android/recline/app/DialogFragment$Action;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    :goto_3
    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 198
    iget-object v8, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 199
    .local v6, "res":Landroid/content/res/Resources;
    invoke-virtual {p1}, Lcom/google/android/recline/app/DialogFragment$Action;->hasMultilineDescription()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 200
    sget v8, Lcom/google/android/recline/R$integer;->lb_dialog_action_title_max_lines:I

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 201
    iget-object v8, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {p0, v8, v7}, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->getDescriptionMaxHeight(Landroid/content/Context;Landroid/widget/TextView;)I

    move-result v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setMaxHeight(I)V

    .line 209
    :goto_4
    iget-object v8, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->itemView:Landroid/view/View;

    sget v9, Lcom/google/android/recline/R$id;->action_title:I

    invoke-virtual {v8, v9, p0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 210
    iget-object v8, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->itemView:Landroid/view/View;

    iget-object v9, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->mActionOnKeyPressAnimator:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;

    invoke-virtual {v8, v9}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 211
    iget-object v8, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->itemView:Landroid/view/View;

    iget-object v9, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->mViewOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v8, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    iget-object v8, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->itemView:Landroid/view/View;

    iget-object v9, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->mActionOnFocusAnimator:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;

    invoke-virtual {v8, v9}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 213
    iget-object v8, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->mActionOnFocusAnimator:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;

    iget-object v9, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v8, v9}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->unFocus(Landroid/view/View;)V

    .line 214
    return-void

    .end local v0    # "checkmarkView":Landroid/widget/ImageView;
    .end local v1    # "chevronView":Landroid/widget/ImageView;
    .end local v2    # "content":Landroid/view/View;
    .end local v3    # "contentLp":Landroid/view/ViewGroup$LayoutParams;
    .end local v5    # "indicatorView":Landroid/widget/ImageView;
    .end local v6    # "res":Landroid/content/res/Resources;
    :cond_0
    move v8, v9

    .line 177
    goto/16 :goto_0

    .restart local v0    # "checkmarkView":Landroid/widget/ImageView;
    :cond_1
    move v8, v10

    .line 181
    goto/16 :goto_1

    .line 190
    .restart local v2    # "content":Landroid/view/View;
    .restart local v3    # "contentLp":Landroid/view/ViewGroup$LayoutParams;
    .restart local v5    # "indicatorView":Landroid/widget/ImageView;
    :cond_2
    iget-object v8, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v11, Lcom/google/android/recline/R$dimen;->lb_action_text_width_no_icon:I

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_2

    .restart local v1    # "chevronView":Landroid/widget/ImageView;
    :cond_3
    move v9, v10

    .line 196
    goto :goto_3

    .line 204
    .restart local v6    # "res":Landroid/content/res/Resources;
    :cond_4
    sget v8, Lcom/google/android/recline/R$integer;->lb_dialog_action_title_min_lines:I

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 205
    sget v8, Lcom/google/android/recline/R$integer;->lb_dialog_action_description_min_lines:I

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setMaxLines(I)V

    goto :goto_4
.end method
