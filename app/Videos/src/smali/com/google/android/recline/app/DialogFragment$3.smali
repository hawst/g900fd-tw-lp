.class Lcom/google/android/recline/app/DialogFragment$3;
.super Ljava/lang/Object;
.source "DialogFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/recline/app/DialogFragment;->performEntryTransition()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mEntryAnimationRunnable:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/google/android/recline/app/DialogFragment;

.field final synthetic val$actionContainerView:Landroid/view/View;

.field final synthetic val$bgDrawable:Landroid/graphics/drawable/ColorDrawable;

.field final synthetic val$contentView:Landroid/view/View;

.field final synthetic val$dialogView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/recline/app/DialogFragment;Landroid/view/View;Landroid/view/View;Landroid/graphics/drawable/ColorDrawable;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 601
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$3;->this$0:Lcom/google/android/recline/app/DialogFragment;

    iput-object p2, p0, Lcom/google/android/recline/app/DialogFragment$3;->val$contentView:Landroid/view/View;

    iput-object p3, p0, Lcom/google/android/recline/app/DialogFragment$3;->val$dialogView:Landroid/view/View;

    iput-object p4, p0, Lcom/google/android/recline/app/DialogFragment$3;->val$bgDrawable:Landroid/graphics/drawable/ColorDrawable;

    iput-object p5, p0, Lcom/google/android/recline/app/DialogFragment$3;->val$actionContainerView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 613
    new-instance v0, Lcom/google/android/recline/app/DialogFragment$3$1;

    invoke-direct {v0, p0}, Lcom/google/android/recline/app/DialogFragment$3$1;-><init>(Lcom/google/android/recline/app/DialogFragment$3;)V

    iput-object v0, p0, Lcom/google/android/recline/app/DialogFragment$3;->mEntryAnimationRunnable:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 4

    .prologue
    .line 604
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$3;->val$contentView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 610
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$3;->val$contentView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$3;->mEntryAnimationRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x226

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postOnAnimationDelayed(Ljava/lang/Runnable;J)V

    .line 611
    return-void
.end method
