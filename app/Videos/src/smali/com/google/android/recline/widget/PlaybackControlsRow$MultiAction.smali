.class public abstract Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;
.super Landroid/support/v17/leanback/widget/Action;
.source "PlaybackControlsRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/widget/PlaybackControlsRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MultiAction"
.end annotation


# instance fields
.field private mDrawables:[Landroid/graphics/drawable/Drawable;

.field private mIndex:I

.field private mLabels:[Ljava/lang/String;

.field private mLabels2:[Ljava/lang/String;


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 64
    int-to-long v0, p1

    invoke-direct {p0, v0, v1}, Landroid/support/v17/leanback/widget/Action;-><init>(J)V

    .line 65
    return-void
.end method


# virtual methods
.method public getActionCount()I
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mDrawables:[Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mDrawables:[Landroid/graphics/drawable/Drawable;

    array-length v0, v0

    .line 105
    :goto_0
    return v0

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mLabels:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 103
    iget-object v0, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mLabels:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_0

    .line 105
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mIndex:I

    return v0
.end method

.method public setDrawables([Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawables"    # [Landroid/graphics/drawable/Drawable;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mDrawables:[Landroid/graphics/drawable/Drawable;

    .line 73
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->setIndex(I)V

    .line 74
    return-void
.end method

.method public setIndex(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 140
    iput p1, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mIndex:I

    .line 141
    iget-object v0, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mDrawables:[Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mDrawables:[Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mIndex:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mLabels:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mLabels:[Ljava/lang/String;

    iget v1, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mIndex:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->setLabel1(Ljava/lang/CharSequence;)V

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mLabels2:[Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 148
    iget-object v0, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mLabels2:[Ljava/lang/String;

    iget v1, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mIndex:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->setLabel2(Ljava/lang/CharSequence;)V

    .line 150
    :cond_2
    return-void
.end method

.method public setLabels([Ljava/lang/String;)V
    .locals 1
    .param p1, "labels"    # [Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mLabels:[Ljava/lang/String;

    .line 83
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->setIndex(I)V

    .line 84
    return-void
.end method

.method public setSecondaryLabels([Ljava/lang/String;)V
    .locals 1
    .param p1, "labels"    # [Ljava/lang/String;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->mLabels2:[Ljava/lang/String;

    .line 92
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;->setIndex(I)V

    .line 93
    return-void
.end method
