.class public Lcom/google/android/recline/widget/CardView;
.super Lcom/google/android/recline/widget/BaseCardView;
.source "CardView.java"


# instance fields
.field protected mContentView:Landroid/widget/TextView;

.field protected mImageView:Landroid/widget/ImageView;

.field protected mInfoArea:Landroid/view/View;

.field protected mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/recline/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/recline/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 50
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/recline/widget/BaseCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 53
    sget v0, Lcom/google/android/recline/R$id;->art_work:I

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/recline/widget/CardView;->mImageView:Landroid/widget/ImageView;

    .line 54
    sget v0, Lcom/google/android/recline/R$id;->info_field:I

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/recline/widget/CardView;->mInfoArea:Landroid/view/View;

    .line 55
    sget v0, Lcom/google/android/recline/R$id;->title_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/recline/widget/CardView;->mTitleView:Landroid/widget/TextView;

    .line 56
    sget v0, Lcom/google/android/recline/R$id;->content_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/recline/widget/CardView;->mContentView:Landroid/widget/TextView;

    .line 58
    sget v0, Lcom/google/android/recline/R$color;->lb_card_info_background_color:I

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/CardView;->setBackgroundResource(I)V

    .line 59
    return-void
.end method


# virtual methods
.method public getContentText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/recline/widget/CardView;->mContentView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 98
    const/4 v0, 0x0

    .line 100
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/recline/widget/CardView;->mContentView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method protected getExtraView()Landroid/view/View;
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getInfoView()Landroid/view/View;
    .locals 4

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 115
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sget v2, Lcom/google/android/recline/R$layout;->lb_card_view_info:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 116
    .local v1, "v":Landroid/view/View;
    return-object v1
.end method

.method public getMainImage()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/recline/widget/CardView;->mImageView:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 70
    const/4 v0, 0x0

    .line 72
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/recline/widget/CardView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method protected getMainView()Landroid/view/View;
    .locals 4

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 107
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sget v2, Lcom/google/android/recline/R$layout;->lb_card_view_main:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 108
    .local v1, "v":Landroid/view/View;
    return-object v1
.end method

.method public getTitleText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/recline/widget/CardView;->mTitleView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x0

    .line 86
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/recline/widget/CardView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 0
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 126
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/recline/widget/BaseCardView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 128
    invoke-virtual {p0, p1}, Lcom/google/android/recline/widget/CardView;->setCardFocusedState(Z)V

    .line 129
    return-void
.end method

.method public setContentText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/recline/widget/CardView;->mContentView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 94
    :goto_0
    return-void

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/google/android/recline/widget/CardView;->mContentView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setMainImage(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "image"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/recline/widget/CardView;->mImageView:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 66
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/google/android/recline/widget/CardView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/recline/widget/CardView;->mTitleView:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 80
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/google/android/recline/widget/CardView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
