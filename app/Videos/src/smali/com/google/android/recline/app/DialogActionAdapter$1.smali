.class Lcom/google/android/recline/app/DialogActionAdapter$1;
.super Ljava/lang/Object;
.source "DialogActionAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/app/DialogActionAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/recline/app/DialogActionAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/recline/app/DialogActionAdapter;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/recline/app/DialogActionAdapter$1;->this$0:Lcom/google/android/recline/app/DialogActionAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 64
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter$1;->this$0:Lcom/google/android/recline/app/DialogActionAdapter;

    # getter for: Lcom/google/android/recline/app/DialogActionAdapter;->mListener:Lcom/google/android/recline/app/DialogFragment$Action$Listener;
    invoke-static {v0}, Lcom/google/android/recline/app/DialogActionAdapter;->access$000(Lcom/google/android/recline/app/DialogActionAdapter;)Lcom/google/android/recline/app/DialogFragment$Action$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter$1;->this$0:Lcom/google/android/recline/app/DialogActionAdapter;

    # getter for: Lcom/google/android/recline/app/DialogActionAdapter;->mListener:Lcom/google/android/recline/app/DialogFragment$Action$Listener;
    invoke-static {v0}, Lcom/google/android/recline/app/DialogActionAdapter;->access$000(Lcom/google/android/recline/app/DialogActionAdapter;)Lcom/google/android/recline/app/DialogFragment$Action$Listener;

    move-result-object v1

    sget v0, Lcom/google/android/recline/R$id;->action_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;

    invoke-virtual {v0}, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->getAction()Lcom/google/android/recline/app/DialogFragment$Action;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/recline/app/DialogFragment$Action$Listener;->onActionClicked(Lcom/google/android/recline/app/DialogFragment$Action;)V

    .line 67
    :cond_0
    return-void
.end method
