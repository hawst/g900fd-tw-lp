.class Lcom/google/android/recline/app/DialogFragment$3$1;
.super Ljava/lang/Object;
.source "DialogFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/app/DialogFragment$3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/recline/app/DialogFragment$3;


# direct methods
.method constructor <init>(Lcom/google/android/recline/app/DialogFragment$3;)V
    .locals 0

    .prologue
    .line 613
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$3$1;->this$1:Lcom/google/android/recline/app/DialogFragment$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/high16 v6, -0x3d100000    # -120.0f

    const/4 v5, 0x0

    .line 616
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$3$1;->this$1:Lcom/google/android/recline/app/DialogFragment$3;

    iget-object v1, v1, Lcom/google/android/recline/app/DialogFragment$3;->this$0:Lcom/google/android/recline/app/DialogFragment;

    invoke-virtual {v1}, Lcom/google/android/recline/app/DialogFragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 646
    :goto_0
    return-void

    .line 622
    :cond_0
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$3$1;->this$1:Lcom/google/android/recline/app/DialogFragment$3;

    iget-object v1, v1, Lcom/google/android/recline/app/DialogFragment$3;->val$dialogView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 625
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$3$1;->this$1:Lcom/google/android/recline/app/DialogFragment$3;

    iget-object v1, v1, Lcom/google/android/recline/app/DialogFragment$3;->val$bgDrawable:Landroid/graphics/drawable/ColorDrawable;

    const-string v2, "alpha"

    new-array v3, v7, [I

    const/16 v4, 0xff

    aput v4, v3, v5

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 626
    .local v0, "oa":Landroid/animation/ObjectAnimator;
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 627
    const-wide/16 v2, 0x78

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 628
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 629
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 633
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$3$1;->this$1:Lcom/google/android/recline/app/DialogFragment$3;

    iget-object v2, v1, Lcom/google/android/recline/app/DialogFragment$3;->this$0:Lcom/google/android/recline/app/DialogFragment;

    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$3$1;->this$1:Lcom/google/android/recline/app/DialogFragment$3;

    iget-object v1, v1, Lcom/google/android/recline/app/DialogFragment$3;->val$contentView:Landroid/view/View;

    sget v3, Lcom/google/android/recline/R$id;->title:I

    invoke-virtual {v1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    # invokes: Lcom/google/android/recline/app/DialogFragment;->prepareAndAnimateView(Landroid/view/View;FZ)V
    invoke-static {v2, v1, v6, v5}, Lcom/google/android/recline/app/DialogFragment;->access$100(Lcom/google/android/recline/app/DialogFragment;Landroid/view/View;FZ)V

    .line 635
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$3$1;->this$1:Lcom/google/android/recline/app/DialogFragment$3;

    iget-object v2, v1, Lcom/google/android/recline/app/DialogFragment$3;->this$0:Lcom/google/android/recline/app/DialogFragment;

    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$3$1;->this$1:Lcom/google/android/recline/app/DialogFragment$3;

    iget-object v1, v1, Lcom/google/android/recline/app/DialogFragment$3;->val$contentView:Landroid/view/View;

    sget v3, Lcom/google/android/recline/R$id;->breadcrumb:I

    invoke-virtual {v1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    # invokes: Lcom/google/android/recline/app/DialogFragment;->prepareAndAnimateView(Landroid/view/View;FZ)V
    invoke-static {v2, v1, v6, v5}, Lcom/google/android/recline/app/DialogFragment;->access$100(Lcom/google/android/recline/app/DialogFragment;Landroid/view/View;FZ)V

    .line 637
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$3$1;->this$1:Lcom/google/android/recline/app/DialogFragment$3;

    iget-object v2, v1, Lcom/google/android/recline/app/DialogFragment$3;->this$0:Lcom/google/android/recline/app/DialogFragment;

    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$3$1;->this$1:Lcom/google/android/recline/app/DialogFragment$3;

    iget-object v1, v1, Lcom/google/android/recline/app/DialogFragment$3;->val$contentView:Landroid/view/View;

    sget v3, Lcom/google/android/recline/R$id;->description:I

    invoke-virtual {v1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    # invokes: Lcom/google/android/recline/app/DialogFragment;->prepareAndAnimateView(Landroid/view/View;FZ)V
    invoke-static {v2, v1, v6, v5}, Lcom/google/android/recline/app/DialogFragment;->access$100(Lcom/google/android/recline/app/DialogFragment;Landroid/view/View;FZ)V

    .line 642
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$3$1;->this$1:Lcom/google/android/recline/app/DialogFragment$3;

    iget-object v1, v1, Lcom/google/android/recline/app/DialogFragment$3;->this$0:Lcom/google/android/recline/app/DialogFragment;

    iget-object v2, p0, Lcom/google/android/recline/app/DialogFragment$3$1;->this$1:Lcom/google/android/recline/app/DialogFragment$3;

    iget-object v2, v2, Lcom/google/android/recline/app/DialogFragment$3;->val$actionContainerView:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/recline/app/DialogFragment$3$1;->this$1:Lcom/google/android/recline/app/DialogFragment$3;

    iget-object v3, v3, Lcom/google/android/recline/app/DialogFragment$3;->val$actionContainerView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    int-to-float v3, v3

    # invokes: Lcom/google/android/recline/app/DialogFragment;->prepareAndAnimateView(Landroid/view/View;FZ)V
    invoke-static {v1, v2, v3, v5}, Lcom/google/android/recline/app/DialogFragment;->access$100(Lcom/google/android/recline/app/DialogFragment;Landroid/view/View;FZ)V

    .line 644
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$3$1;->this$1:Lcom/google/android/recline/app/DialogFragment$3;

    iget-object v2, v1, Lcom/google/android/recline/app/DialogFragment$3;->this$0:Lcom/google/android/recline/app/DialogFragment;

    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$3$1;->this$1:Lcom/google/android/recline/app/DialogFragment$3;

    iget-object v1, v1, Lcom/google/android/recline/app/DialogFragment$3;->val$contentView:Landroid/view/View;

    sget v3, Lcom/google/android/recline/R$id;->icon:I

    invoke-virtual {v1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    # invokes: Lcom/google/android/recline/app/DialogFragment;->prepareAndAnimateView(Landroid/view/View;FZ)V
    invoke-static {v2, v1, v6, v7}, Lcom/google/android/recline/app/DialogFragment;->access$100(Lcom/google/android/recline/app/DialogFragment;Landroid/view/View;FZ)V

    goto/16 :goto_0
.end method
