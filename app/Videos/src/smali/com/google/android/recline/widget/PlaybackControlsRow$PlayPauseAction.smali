.class public Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;
.super Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;
.source "PlaybackControlsRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/widget/PlaybackControlsRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PlayPauseAction"
.end annotation


# static fields
.field public static PAUSE:I

.field public static PLAY:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    sput v0, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->PLAY:I

    .line 172
    const/4 v0, 0x1

    sput v0, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->PAUSE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 179
    sget v2, Lcom/google/android/recline/R$id;->lb_control_play_pause:I

    invoke-direct {p0, v2}, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;-><init>(I)V

    .line 180
    const/4 v2, 0x2

    new-array v0, v2, [Landroid/graphics/drawable/Drawable;

    .line 181
    .local v0, "drawables":[Landroid/graphics/drawable/Drawable;
    sget v2, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->PLAY:I

    sget v3, Lcom/google/android/recline/R$styleable;->lbPlaybackControlsActionIcons_play:I

    # invokes: Lcom/google/android/recline/widget/PlaybackControlsRow;->getStyledDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    invoke-static {p1, v3}, Lcom/google/android/recline/widget/PlaybackControlsRow;->access$000(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v0, v2

    .line 183
    sget v2, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->PAUSE:I

    sget v3, Lcom/google/android/recline/R$styleable;->lbPlaybackControlsActionIcons_pause:I

    # invokes: Lcom/google/android/recline/widget/PlaybackControlsRow;->getStyledDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    invoke-static {p1, v3}, Lcom/google/android/recline/widget/PlaybackControlsRow;->access$000(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    aput-object v3, v0, v2

    .line 185
    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->setDrawables([Landroid/graphics/drawable/Drawable;)V

    .line 187
    array-length v2, v0

    new-array v1, v2, [Ljava/lang/String;

    .line 188
    .local v1, "labels":[Ljava/lang/String;
    sget v2, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->PLAY:I

    sget v3, Lcom/google/android/recline/R$string;->lb_playback_controls_play:I

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 189
    sget v2, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->PAUSE:I

    sget v3, Lcom/google/android/recline/R$string;->lb_playback_controls_pause:I

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 190
    invoke-virtual {p0, v1}, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->setLabels([Ljava/lang/String;)V

    .line 191
    return-void
.end method
