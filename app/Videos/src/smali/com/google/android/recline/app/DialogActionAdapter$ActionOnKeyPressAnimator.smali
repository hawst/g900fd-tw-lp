.class Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;
.super Ljava/lang/Object;
.source "DialogActionAdapter.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/app/DialogActionAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ActionOnKeyPressAnimator"
.end annotation


# instance fields
.field private final mActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/recline/app/DialogFragment$Action;",
            ">;"
        }
    .end annotation
.end field

.field private mKeyPressed:Z

.field private mListener:Lcom/google/android/recline/app/DialogFragment$Action$Listener;


# direct methods
.method public constructor <init>(Lcom/google/android/recline/app/DialogFragment$Action$Listener;Ljava/util/List;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/recline/app/DialogFragment$Action$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/recline/app/DialogFragment$Action$Listener;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/recline/app/DialogFragment$Action;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 417
    .local p2, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/recline/app/DialogFragment$Action;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->mKeyPressed:Z

    .line 418
    iput-object p1, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->mListener:Lcom/google/android/recline/app/DialogFragment$Action$Listener;

    .line 419
    iput-object p2, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->mActions:Ljava/util/List;

    .line 420
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;)Lcom/google/android/recline/app/DialogFragment$Action$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;

    .prologue
    .line 403
    iget-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->mListener:Lcom/google/android/recline/app/DialogFragment$Action$Listener;

    return-object v0
.end method

.method private fadeCheckmarks(Landroid/view/View;Lcom/google/android/recline/app/DialogFragment$Action;IILandroid/view/animation/Interpolator;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "action"    # Lcom/google/android/recline/app/DialogFragment$Action;
    .param p3, "duration"    # I
    .param p4, "delay"    # I
    .param p5, "interpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    .line 535
    invoke-virtual {p2}, Lcom/google/android/recline/app/DialogFragment$Action;->getCheckSetId()I

    move-result v1

    .line 536
    .local v1, "actionCheckSetId":I
    if-eqz v1, :cond_4

    .line 537
    sget v7, Lcom/google/android/recline/R$layout;->lb_dialog_action_list_item:I

    invoke-virtual {p1, v7}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 540
    .local v4, "parent":Landroid/view/ViewGroup;
    const/4 v3, 0x0

    .local v3, "i":I
    iget-object v7, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->mActions:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    .local v5, "size":I
    :goto_0
    if-ge v3, v5, :cond_2

    .line 541
    iget-object v7, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->mActions:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/recline/app/DialogFragment$Action;

    .line 542
    .local v0, "a":Lcom/google/android/recline/app/DialogFragment$Action;
    if-eq v0, p2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/recline/app/DialogFragment$Action;->getCheckSetId()I

    move-result v7

    if-ne v7, v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/recline/app/DialogFragment$Action;->isChecked()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 543
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Lcom/google/android/recline/app/DialogFragment$Action;->setChecked(Z)V

    .line 544
    invoke-virtual {v4, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 545
    .local v6, "viewToAnimateOut":Landroid/view/View;
    if-eqz v6, :cond_1

    .line 546
    sget v7, Lcom/google/android/recline/R$id;->action_checkmark:I

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 548
    .local v2, "checkView":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    int-to-long v8, p3

    invoke-virtual {v7, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    int-to-long v8, p4

    invoke-virtual {v7, v8, v9}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 550
    if-eqz p5, :cond_0

    .line 551
    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    invoke-virtual {v7, p5}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 553
    :cond_0
    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    new-instance v8, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator$2;

    invoke-direct {v8, p0, v2}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator$2;-><init>(Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;Landroid/view/View;)V

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 540
    .end local v2    # "checkView":Landroid/view/View;
    .end local v6    # "viewToAnimateOut":Landroid/view/View;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 564
    .end local v0    # "a":Lcom/google/android/recline/app/DialogFragment$Action;
    :cond_2
    invoke-virtual {p2}, Lcom/google/android/recline/app/DialogFragment$Action;->isChecked()Z

    move-result v7

    if-nez v7, :cond_4

    .line 565
    const/4 v7, 0x1

    invoke-virtual {p2, v7}, Lcom/google/android/recline/app/DialogFragment$Action;->setChecked(Z)V

    .line 566
    sget v7, Lcom/google/android/recline/R$id;->action_checkmark:I

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 567
    .restart local v2    # "checkView":Landroid/view/View;
    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 568
    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Landroid/view/View;->setAlpha(F)V

    .line 569
    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    int-to-long v8, p3

    invoke-virtual {v7, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    int-to-long v8, p4

    invoke-virtual {v7, v8, v9}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 571
    if-eqz p5, :cond_3

    .line 572
    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    invoke-virtual {v7, p5}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 574
    :cond_3
    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 577
    .end local v2    # "checkView":Landroid/view/View;
    .end local v3    # "i":I
    .end local v4    # "parent":Landroid/view/ViewGroup;
    .end local v5    # "size":I
    :cond_4
    return-void
.end method

.method private playSound(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "soundEffect"    # I

    .prologue
    .line 427
    const-string v1, "audio"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 428
    .local v0, "manager":Landroid/media/AudioManager;
    invoke-virtual {v0, p2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 429
    return-void
.end method

.method private prepareAndAnimateView(Landroid/view/View;FFIILandroid/view/animation/Interpolator;Z)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "initAlpha"    # F
    .param p3, "destAlpha"    # F
    .param p4, "duration"    # I
    .param p5, "delay"    # I
    .param p6, "interpolator"    # Landroid/view/animation/Interpolator;
    .param p7, "pressed"    # Z

    .prologue
    .line 503
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 504
    sget v0, Lcom/google/android/recline/R$id;->action_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;

    invoke-virtual {v0}, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->getAction()Lcom/google/android/recline/app/DialogFragment$Action;

    move-result-object v2

    .line 506
    .local v2, "action":Lcom/google/android/recline/app/DialogFragment$Action;
    if-nez p7, :cond_0

    move-object v0, p0

    move-object v1, p1

    move v3, p4

    move v4, p5

    move-object v5, p6

    .line 507
    invoke-direct/range {v0 .. v5}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->fadeCheckmarks(Landroid/view/View;Lcom/google/android/recline/app/DialogFragment$Action;IILandroid/view/animation/Interpolator;)V

    .line 510
    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    .line 511
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 512
    invoke-virtual {p1}, Landroid/view/View;->buildLayer()V

    .line 513
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v4, p4

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v4, p5

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 514
    if-eqz p6, :cond_1

    .line 515
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p6}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 517
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator$1;

    invoke-direct {v1, p0, p1, p7, v2}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator$1;-><init>(Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;Landroid/view/View;ZLcom/google/android/recline/app/DialogFragment$Action;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 529
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 531
    .end local v2    # "action":Lcom/google/android/recline/app/DialogFragment$Action;
    :cond_2
    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 436
    if-nez p1, :cond_1

    .line 437
    const/4 v9, 0x0

    .line 497
    :cond_0
    :goto_0
    return v9

    .line 439
    :cond_1
    const/4 v9, 0x0

    .line 440
    .local v9, "handled":Z
    sget v0, Lcom/google/android/recline/R$id;->action_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;

    invoke-virtual {v0}, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->getAction()Lcom/google/android/recline/app/DialogFragment$Action;

    move-result-object v8

    .line 441
    .local v8, "action":Lcom/google/android/recline/app/DialogFragment$Action;
    sparse-switch p2, :sswitch_data_0

    goto :goto_0

    .line 448
    :sswitch_0
    invoke-virtual {v8}, Lcom/google/android/recline/app/DialogFragment$Action;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v8}, Lcom/google/android/recline/app/DialogFragment$Action;->infoOnly()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 449
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->isSoundEffectsEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    .line 454
    :cond_3
    const/4 v9, 0x1

    goto :goto_0

    .line 457
    :cond_4
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 459
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->mKeyPressed:Z

    if-nez v0, :cond_0

    .line 460
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->mKeyPressed:Z

    .line 462
    invoke-virtual {p1}, Landroid/view/View;->isSoundEffectsEnabled()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 463
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->playSound(Landroid/content/Context;I)V

    .line 470
    :cond_5
    const/high16 v2, 0x3f800000    # 1.0f

    const v3, 0x3e4ccccd    # 0.2f

    const/16 v4, 0x64

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-boolean v7, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->mKeyPressed:Z

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->prepareAndAnimateView(Landroid/view/View;FFIILandroid/view/animation/Interpolator;Z)V

    .line 473
    const/4 v9, 0x1

    goto :goto_0

    .line 477
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->mKeyPressed:Z

    if-eqz v0, :cond_0

    .line 478
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->mKeyPressed:Z

    .line 484
    const v2, 0x3e4ccccd    # 0.2f

    const/high16 v3, 0x3f800000    # 1.0f

    const/16 v4, 0x64

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-boolean v7, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->mKeyPressed:Z

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->prepareAndAnimateView(Landroid/view/View;FFIILandroid/view/animation/Interpolator;Z)V

    .line 487
    const/4 v9, 0x1

    goto :goto_0

    .line 441
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
        0x63 -> :sswitch_0
        0x64 -> :sswitch_0
        0xa0 -> :sswitch_0
    .end sparse-switch

    .line 457
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
