.class Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;
.super Lcom/google/android/recline/widget/CardLayoutInfoUnder;
.source "CardLayoutInfoUnderWithExtra.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field private mAnim:Landroid/animation/ObjectAnimator;

.field private final mFocusAnimDuration:I

.field private mHandler:Landroid/os/Handler;

.field protected mInfoOffset:I

.field private mMeasuredHeight:I

.field private mMeasuredWidth:I


# direct methods
.method constructor <init>(Lcom/google/android/recline/widget/BaseCardView;)V
    .locals 4
    .param p1, "card"    # Lcom/google/android/recline/widget/BaseCardView;

    .prologue
    const/4 v2, 0x0

    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;-><init>(Lcom/google/android/recline/widget/BaseCardView;)V

    .line 35
    iput v2, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mInfoOffset:I

    .line 39
    new-instance v0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra$1;

    invoke-direct {v0, p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra$1;-><init>(Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;)V

    iput-object v0, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mHandler:Landroid/os/Handler;

    .line 54
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->cardView()Lcom/google/android/recline/widget/BaseCardView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/recline/widget/BaseCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/recline/R$integer;->lb_card_focus_animation_duration:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mFocusAnimDuration:I

    .line 57
    invoke-virtual {p0, v2}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->setInfoOffset(I)V

    .line 58
    const-string v0, "infoOffset"

    const/4 v1, 0x1

    new-array v1, v1, [I

    aput v2, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mAnim:Landroid/animation/ObjectAnimator;

    .line 59
    iget-object v0, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mAnim:Landroid/animation/ObjectAnimator;

    iget v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mFocusAnimDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 60
    iget-object v0, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mAnim:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 62
    return-void
.end method


# virtual methods
.method protected animateInfoField(Z)V
    .locals 6
    .param p1, "shown"    # Z

    .prologue
    const/4 v1, 0x0

    .line 185
    iget-object v2, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->isStarted()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 186
    iget-object v2, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 189
    :cond_0
    if-eqz p1, :cond_1

    .line 190
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getExtraView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 193
    :cond_1
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {p0, v2, v3}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->measureCardChildren(II)V

    .line 195
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getExtraView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 197
    .local v0, "extraHeight":I
    iget-object v2, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mAnim:Landroid/animation/ObjectAnimator;

    const/4 v3, 0x3

    new-array v3, v3, [I

    iget v4, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mInfoOffset:I

    aput v4, v3, v1

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getInfoOffset()I

    move-result v5

    aput v5, v3, v4

    const/4 v4, 0x2

    if-eqz p1, :cond_2

    .end local v0    # "extraHeight":I
    :goto_0
    aput v0, v3, v4

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    .line 198
    iget-object v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 199
    return-void

    .restart local v0    # "extraHeight":I
    :cond_2
    move v0, v1

    .line 197
    goto :goto_0
.end method

.method protected getInfoOffset()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mInfoOffset:I

    return v0
.end method

.method public hasExtraView()Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    return v0
.end method

.method protected initCardViews()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 71
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getInfoView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getInfoView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getExtraView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 76
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getExtraView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 78
    :cond_1
    return-void
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 214
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 207
    iget v0, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mInfoOffset:I

    if-nez v0, :cond_0

    .line 208
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getExtraView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 210
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 218
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 203
    return-void
.end method

.method protected onCardActiveStateChanged(Z)V
    .locals 3
    .param p1, "active"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 136
    if-eqz p1, :cond_1

    .line 137
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getInfoView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getInfoView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getInfoView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 142
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getInfoView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 144
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getExtraView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 145
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getExtraView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 147
    :cond_3
    iput v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mInfoOffset:I

    goto :goto_0
.end method

.method protected onCardFocusedStateChanged(Z)V
    .locals 4
    .param p1, "focused"    # Z

    .prologue
    const/4 v1, 0x1

    .line 153
    iget-object v0, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 155
    if-eqz p1, :cond_2

    .line 156
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getExtraView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getDelayFocusAnimation()Z

    move-result v0

    if-nez v0, :cond_1

    .line 158
    iget-object v0, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 159
    invoke-virtual {p0, v1}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->setDelayFocusAnimation(Z)V

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    iget-object v0, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getFocusAnimationDelayAmount()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 166
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getExtraView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 167
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->animateInfoField(Z)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 112
    const/4 v0, 0x0

    .line 114
    .local v0, "currTop":I
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getMainView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getMainView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v5, :cond_0

    .line 115
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getMainView()Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mMeasuredWidth:I

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getMainView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 117
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getMainView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 120
    :cond_0
    iget v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mInfoOffset:I

    sub-int/2addr v0, v1

    .line 122
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getInfoView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getInfoView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v5, :cond_1

    .line 123
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getInfoView()Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mMeasuredWidth:I

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getInfoView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 125
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getInfoView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 128
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getExtraView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getExtraView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v5, :cond_2

    .line 129
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getExtraView()Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mMeasuredWidth:I

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getExtraView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 132
    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v1, 0x0

    const/16 v4, 0x8

    .line 82
    iput v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mMeasuredWidth:I

    .line 83
    iput v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mMeasuredHeight:I

    .line 84
    const/4 v0, 0x0

    .line 86
    .local v0, "state":I
    invoke-virtual {p0, p1, p2}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->measureCardChildren(II)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getMainView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getMainView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v4, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getMainView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iput v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mMeasuredWidth:I

    .line 90
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getMainView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iput v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mMeasuredHeight:I

    .line 91
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getMainView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredState()I

    move-result v1

    invoke-static {v0, v1}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v0

    .line 94
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getInfoView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getInfoView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v4, :cond_1

    .line 95
    iget v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mMeasuredHeight:I

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getInfoView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget v3, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mInfoOffset:I

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mMeasuredHeight:I

    .line 96
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getInfoView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredState()I

    move-result v1

    invoke-static {v0, v1}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v0

    .line 99
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getExtraView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getExtraView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v4, :cond_2

    .line 100
    iget v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mMeasuredHeight:I

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getExtraView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mMeasuredHeight:I

    .line 101
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->getExtraView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredState()I

    move-result v1

    invoke-static {v0, v1}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v0

    .line 105
    :cond_2
    iget v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mMeasuredWidth:I

    invoke-static {v1, p1, v0}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v1

    iget v2, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mMeasuredHeight:I

    shl-int/lit8 v3, v0, 0x10

    invoke-static {v2, p2, v3}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->setCardMeasuredDimension(II)V

    .line 108
    return-void
.end method

.method protected setInfoOffset(I)V
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 173
    iput p1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->mInfoOffset:I

    .line 174
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;->cardView()Lcom/google/android/recline/widget/BaseCardView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/recline/widget/BaseCardView;->requestLayout()V

    .line 175
    return-void
.end method
