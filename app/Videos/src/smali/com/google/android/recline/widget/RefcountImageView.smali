.class public Lcom/google/android/recline/widget/RefcountImageView;
.super Landroid/widget/ImageView;
.source "RefcountImageView.java"


# instance fields
.field private mAutoUnrefOnDetach:Z

.field private mClipRect:Landroid/graphics/RectF;

.field private mHasClipRect:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/recline/widget/RefcountImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/recline/widget/RefcountImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/recline/widget/RefcountImageView;->mClipRect:Landroid/graphics/RectF;

    .line 47
    return-void
.end method

.method private static releaseRef(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p0, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 73
    instance-of v3, p0, Lcom/google/android/recline/util/RefcountBitmapDrawable;

    if-eqz v3, :cond_1

    .line 74
    check-cast p0, Lcom/google/android/recline/util/RefcountBitmapDrawable;

    .end local p0    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Lcom/google/android/recline/util/RefcountBitmapDrawable;->getRefcountObject()Lcom/google/android/recline/util/RefcountObject;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/recline/util/RefcountObject;->releaseRef()I

    .line 81
    .restart local p0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    return-void

    .line 75
    :cond_1
    instance-of v3, p0, Landroid/graphics/drawable/LayerDrawable;

    if-eqz v3, :cond_0

    move-object v1, p0

    .line 76
    check-cast v1, Landroid/graphics/drawable/LayerDrawable;

    .line 77
    .local v1, "layerDrawable":Landroid/graphics/drawable/LayerDrawable;
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {v1}, Landroid/graphics/drawable/LayerDrawable;->getNumberOfLayers()I

    move-result v2

    .local v2, "z":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 78
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/recline/widget/RefcountImageView;->releaseRef(Landroid/graphics/drawable/Drawable;)V

    .line 77
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public clearClipRect()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/recline/widget/RefcountImageView;->mHasClipRect:Z

    .line 90
    return-void
.end method

.method public getAutoUnrefOnDetach()Z
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/android/recline/widget/RefcountImageView;->mAutoUnrefOnDetach:Z

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/recline/widget/RefcountImageView;->mAutoUnrefOnDetach:Z

    if-eqz v0, :cond_0

    .line 60
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/RefcountImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 62
    :cond_0
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 63
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 94
    iget-boolean v1, p0, Lcom/google/android/recline/widget/RefcountImageView;->mHasClipRect:Z

    if-eqz v1, :cond_0

    .line 95
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 96
    .local v0, "saveCount":I
    iget-object v1, p0, Lcom/google/android/recline/widget/RefcountImageView;->mClipRect:Landroid/graphics/RectF;

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 97
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 98
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 102
    .end local v0    # "saveCount":I
    :goto_0
    return-void

    .line 100
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public setAutoUnrefOnDetach(Z)V
    .locals 0
    .param p1, "autoUnref"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/google/android/recline/widget/RefcountImageView;->mAutoUnrefOnDetach:Z

    .line 51
    return-void
.end method

.method public setClipRect(FFFF)V
    .locals 1
    .param p1, "l"    # F
    .param p2, "t"    # F
    .param p3, "r"    # F
    .param p4, "b"    # F

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/recline/widget/RefcountImageView;->mClipRect:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/recline/widget/RefcountImageView;->mHasClipRect:Z

    .line 86
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/google/android/recline/widget/RefcountImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 68
    .local v0, "previousDrawable":Landroid/graphics/drawable/Drawable;
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 69
    invoke-static {v0}, Lcom/google/android/recline/widget/RefcountImageView;->releaseRef(Landroid/graphics/drawable/Drawable;)V

    .line 70
    return-void
.end method
