.class Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;
.super Landroid/animation/Animator;
.source "DialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/app/DialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UntargetableAnimatorSet"
.end annotation


# instance fields
.field private final mAnimatorSet:Landroid/animation/AnimatorSet;


# direct methods
.method constructor <init>(Landroid/animation/AnimatorSet;)V
    .locals 0
    .param p1, "animatorSet"    # Landroid/animation/AnimatorSet;

    .prologue
    .line 828
    invoke-direct {p0}, Landroid/animation/Animator;-><init>()V

    .line 829
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    .line 830
    return-void
.end method


# virtual methods
.method public addListener(Landroid/animation/Animator$AnimatorListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    .line 834
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, p1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 835
    return-void
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 839
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 840
    return-void
.end method

.method public clone()Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 844
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->clone()Landroid/animation/AnimatorSet;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 824
    invoke-virtual {p0}, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->clone()Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method public end()V
    .locals 1

    .prologue
    .line 849
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 850
    return-void
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 854
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->getDuration()J

    move-result-wide v0

    return-wide v0
.end method

.method public getListeners()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator$AnimatorListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 859
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->getListeners()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getStartDelay()J
    .locals 2

    .prologue
    .line 864
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->getStartDelay()J

    move-result-wide v0

    return-wide v0
.end method

.method public isRunning()Z
    .locals 1

    .prologue
    .line 869
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    return v0
.end method

.method public isStarted()Z
    .locals 1

    .prologue
    .line 874
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v0

    return v0
.end method

.method public removeAllListeners()V
    .locals 1

    .prologue
    .line 879
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->removeAllListeners()V

    .line 880
    return-void
.end method

.method public removeListener(Landroid/animation/Animator$AnimatorListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    .line 884
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, p1}, Landroid/animation/AnimatorSet;->removeListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 885
    return-void
.end method

.method public setDuration(J)Landroid/animation/Animator;
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 889
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, p1, p2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v0

    return-object v0
.end method

.method public setInterpolator(Landroid/animation/TimeInterpolator;)V
    .locals 1
    .param p1, "value"    # Landroid/animation/TimeInterpolator;

    .prologue
    .line 894
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, p1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 895
    return-void
.end method

.method public setStartDelay(J)V
    .locals 1
    .param p1, "startDelay"    # J

    .prologue
    .line 899
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, p1, p2}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 900
    return-void
.end method

.method public setTarget(Ljava/lang/Object;)V
    .locals 0
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    .line 905
    return-void
.end method

.method public setupEndValues()V
    .locals 1

    .prologue
    .line 909
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->setupEndValues()V

    .line 910
    return-void
.end method

.method public setupStartValues()V
    .locals 1

    .prologue
    .line 914
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->setupStartValues()V

    .line 915
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 919
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$UntargetableAnimatorSet;->mAnimatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 920
    return-void
.end method
