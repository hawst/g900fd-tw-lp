.class public Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;
.super Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;
.source "PlaybackControlsRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/widget/PlaybackControlsRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FastForwardAction"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 203
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;-><init>(Landroid/content/Context;I)V

    .line 204
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "numSpeeds"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 212
    sget v5, Lcom/google/android/recline/R$id;->lb_control_fast_forward:I

    invoke-direct {p0, v5}, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;-><init>(I)V

    .line 214
    if-ge p2, v10, :cond_0

    .line 215
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "numSpeeds must be > 0"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 217
    :cond_0
    new-array v0, p2, [Landroid/graphics/drawable/Drawable;

    .line 218
    .local v0, "drawables":[Landroid/graphics/drawable/Drawable;
    sget v5, Lcom/google/android/recline/R$styleable;->lbPlaybackControlsActionIcons_fast_forward:I

    # invokes: Lcom/google/android/recline/widget/PlaybackControlsRow;->getStyledDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    invoke-static {p1, v5}, Lcom/google/android/recline/widget/PlaybackControlsRow;->access$000(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aput-object v5, v0, v9

    .line 220
    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;->setDrawables([Landroid/graphics/drawable/Drawable;)V

    .line 222
    invoke-virtual {p0}, Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;->getActionCount()I

    move-result v5

    new-array v2, v5, [Ljava/lang/String;

    .line 223
    .local v2, "labels":[Ljava/lang/String;
    sget v5, Lcom/google/android/recline/R$string;->lb_playback_controls_fast_forward:I

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v9

    .line 225
    invoke-virtual {p0}, Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;->getActionCount()I

    move-result v5

    new-array v3, v5, [Ljava/lang/String;

    .line 226
    .local v3, "labels2":[Ljava/lang/String;
    aget-object v5, v2, v9

    aput-object v5, v3, v9

    .line 228
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_1

    .line 229
    add-int/lit8 v4, v1, 0x1

    .line 230
    .local v4, "multiplier":I
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/recline/R$string;->recline_control_display_fast_forward_multiplier:I

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 232
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/recline/R$string;->recline_playback_controls_fast_forward_multiplier:I

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    .line 228
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 235
    .end local v4    # "multiplier":I
    :cond_1
    invoke-virtual {p0, v2}, Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;->setLabels([Ljava/lang/String;)V

    .line 236
    invoke-virtual {p0, v3}, Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;->setSecondaryLabels([Ljava/lang/String;)V

    .line 237
    return-void
.end method
