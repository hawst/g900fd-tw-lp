.class Lcom/google/android/recline/app/DialogActionAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "DialogActionAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;,
        Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;,
        Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;
    }
.end annotation


# instance fields
.field private final mActionOnFocusAnimator:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;

.field private final mActionOnKeyPressAnimator:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;

.field private final mActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/recline/app/DialogFragment$Action;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field private mListener:Lcom/google/android/recline/app/DialogFragment$Action$Listener;

.field private final mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/google/android/recline/app/DialogFragment$Action$Listener;Lcom/google/android/recline/app/DialogFragment$Action$OnFocusListener;Ljava/util/List;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/recline/app/DialogFragment$Action$Listener;
    .param p2, "onFocusListener"    # Lcom/google/android/recline/app/DialogFragment$Action$OnFocusListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/recline/app/DialogFragment$Action$Listener;",
            "Lcom/google/android/recline/app/DialogFragment$Action$OnFocusListener;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/recline/app/DialogFragment$Action;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p3, "actions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/recline/app/DialogFragment$Action;>;"
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 61
    new-instance v0, Lcom/google/android/recline/app/DialogActionAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/recline/app/DialogActionAdapter$1;-><init>(Lcom/google/android/recline/app/DialogActionAdapter;)V

    iput-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 73
    iput-object p1, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mListener:Lcom/google/android/recline/app/DialogFragment$Action$Listener;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mActions:Ljava/util/List;

    .line 75
    new-instance v0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;

    iget-object v1, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mActions:Ljava/util/List;

    invoke-direct {v0, p1, v1}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;-><init>(Lcom/google/android/recline/app/DialogFragment$Action$Listener;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mActionOnKeyPressAnimator:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;

    .line 76
    new-instance v0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;

    invoke-direct {v0, p2}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;-><init>(Lcom/google/android/recline/app/DialogFragment$Action$OnFocusListener;)V

    iput-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mActionOnFocusAnimator:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/recline/app/DialogActionAdapter;)Lcom/google/android/recline/app/DialogFragment$Action$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogActionAdapter;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mListener:Lcom/google/android/recline/app/DialogFragment$Action$Listener;

    return-object v0
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mActions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 2
    .param p1, "baseHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 92
    move-object v0, p1

    check-cast v0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;

    .line 94
    .local v0, "holder":Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;
    iget-object v1, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt p2, v1, :cond_0

    .line 99
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-object v1, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mActions:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/recline/app/DialogFragment$Action;

    invoke-virtual {v0, v1}, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->init(Lcom/google/android/recline/app/DialogFragment$Action;)V

    goto :goto_0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 81
    iget-object v1, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mInflater:Landroid/view/LayoutInflater;

    if-nez v1, :cond_0

    .line 82
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 85
    :cond_0
    iget-object v1, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/recline/R$layout;->lb_dialog_action_list_item:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 86
    .local v0, "v":Landroid/view/View;
    sget v1, Lcom/google/android/recline/R$layout;->lb_dialog_action_list_item:I

    invoke-virtual {v0, v1, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 87
    new-instance v1, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;

    iget-object v2, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mActionOnKeyPressAnimator:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;

    iget-object v3, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mActionOnFocusAnimator:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;

    iget-object v4, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;-><init>(Landroid/view/View;Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public setActions(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/recline/app/DialogFragment$Action;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 131
    .local p1, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/recline/app/DialogFragment$Action;>;"
    iget-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mActionOnFocusAnimator:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->unFocus(Landroid/view/View;)V

    .line 132
    iget-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mActions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 133
    iget-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter;->mActions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 134
    invoke-virtual {p0}, Lcom/google/android/recline/app/DialogActionAdapter;->notifyDataSetChanged()V

    .line 135
    return-void
.end method
