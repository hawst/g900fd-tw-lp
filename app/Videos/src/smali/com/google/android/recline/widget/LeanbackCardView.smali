.class public Lcom/google/android/recline/widget/LeanbackCardView;
.super Landroid/view/ViewGroup;
.source "LeanbackCardView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/recline/widget/LeanbackCardView$InfoAlphaAnimation;,
        Lcom/google/android/recline/widget/LeanbackCardView$InfoHeightAnimation;,
        Lcom/google/android/recline/widget/LeanbackCardView$InfoOffsetAnimation;,
        Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;
    }
.end annotation


# static fields
.field public static final CARD_TYPE_INFO_OVER:I = 0x1

.field public static final CARD_TYPE_INFO_UNDER:I = 0x2

.field public static final CARD_TYPE_INFO_UNDER_WITH_EXTRA:I = 0x3

.field public static final CARD_TYPE_MAIN_ONLY:I


# instance fields
.field private final mActivatedAnimDuration:I

.field private mAnim:Landroid/view/animation/Animation;

.field private final mAnimationTrigger:Ljava/lang/Runnable;

.field private mCardSelected:Z

.field private mCardType:I

.field private mDelaySelectedAnim:Z

.field private mExtraViewList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mInfoAlpha:F

.field private mInfoOffset:F

.field private mInfoOnSelected:Z

.field private mInfoViewList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mInfoVisFraction:F

.field private mMainViewList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mMeasuredHeight:I

.field private mMeasuredWidth:I

.field private final mSelectedAnimDuration:I

.field private mSelectedAnimationDelay:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 139
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/recline/widget/LeanbackCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 140
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 143
    sget v0, Lcom/google/android/recline/R$attr;->leanbackCardViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/recline/widget/LeanbackCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 144
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 147
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 128
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoAlpha:F

    .line 131
    new-instance v1, Lcom/google/android/recline/widget/LeanbackCardView$1;

    invoke-direct {v1, p0}, Lcom/google/android/recline/widget/LeanbackCardView$1;-><init>(Lcom/google/android/recline/widget/LeanbackCardView;)V

    iput-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnimationTrigger:Ljava/lang/Runnable;

    .line 149
    sget-object v1, Lcom/google/android/recline/R$styleable;->LeanbackCardView:[I

    invoke-virtual {p1, p2, v1, v4, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 152
    .local v0, "a":Landroid/content/res/TypedArray;
    :try_start_0
    sget v1, Lcom/google/android/recline/R$styleable;->LeanbackCardView_lbCardType:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    .line 153
    sget v1, Lcom/google/android/recline/R$styleable;->LeanbackCardView_infoOnSelectedOnly:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOnSelected:Z

    .line 154
    sget v1, Lcom/google/android/recline/R$styleable;->LeanbackCardView_lbSelectedAnimationDelay:I

    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/recline/R$integer;->lb_card_selected_animation_delay:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mSelectedAnimationDelay:I

    .line 158
    sget v1, Lcom/google/android/recline/R$styleable;->LeanbackCardView_lbSelectedAnimationDuration:I

    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/recline/R$integer;->lb_card_selected_animation_duration:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mSelectedAnimDuration:I

    .line 162
    sget v1, Lcom/google/android/recline/R$styleable;->LeanbackCardView_lbActivatedAnimationDuration:I

    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/recline/R$integer;->lb_card_activated_animation_duration:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mActivatedAnimDuration:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 169
    iput-boolean v4, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardSelected:Z

    .line 170
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mDelaySelectedAnim:Z

    .line 172
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMainViewList:Ljava/util/ArrayList;

    .line 173
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    .line 174
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mExtraViewList:Ljava/util/ArrayList;

    .line 176
    iput v5, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOffset:F

    .line 177
    iput v5, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoVisFraction:F

    .line 178
    return-void

    .line 166
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1
.end method

.method static synthetic access$000(Lcom/google/android/recline/widget/LeanbackCardView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/widget/LeanbackCardView;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/recline/widget/LeanbackCardView;->animateInfoOffset(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/recline/widget/LeanbackCardView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/recline/widget/LeanbackCardView;

    .prologue
    .line 60
    iget v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOffset:F

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/recline/widget/LeanbackCardView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/widget/LeanbackCardView;
    .param p1, "x1"    # F

    .prologue
    .line 60
    iput p1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOffset:F

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/recline/widget/LeanbackCardView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/recline/widget/LeanbackCardView;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mExtraViewList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/recline/widget/LeanbackCardView;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/recline/widget/LeanbackCardView;

    .prologue
    .line 60
    iget v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoAlpha:F

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/recline/widget/LeanbackCardView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/widget/LeanbackCardView;
    .param p1, "x1"    # F

    .prologue
    .line 60
    iput p1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoAlpha:F

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/recline/widget/LeanbackCardView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/recline/widget/LeanbackCardView;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/recline/widget/LeanbackCardView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/widget/LeanbackCardView;
    .param p1, "x1"    # F

    .prologue
    .line 60
    iput p1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoVisFraction:F

    return p1
.end method

.method private animateInfoAlpha(Z)V
    .locals 4
    .param p1, "shown"    # Z

    .prologue
    .line 674
    invoke-direct {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->cancelAnimations()V

    .line 676
    if-eqz p1, :cond_0

    .line 677
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 678
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 677
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 682
    .end local v0    # "i":I
    :cond_0
    new-instance v2, Lcom/google/android/recline/widget/LeanbackCardView$InfoAlphaAnimation;

    iget v3, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoAlpha:F

    if-eqz p1, :cond_1

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_1
    invoke-direct {v2, p0, v3, v1}, Lcom/google/android/recline/widget/LeanbackCardView$InfoAlphaAnimation;-><init>(Lcom/google/android/recline/widget/LeanbackCardView;FF)V

    iput-object v2, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    .line 683
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    iget v2, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mActivatedAnimDuration:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 684
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 685
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    new-instance v2, Lcom/google/android/recline/widget/LeanbackCardView$4;

    invoke-direct {v2, p0}, Lcom/google/android/recline/widget/LeanbackCardView$4;-><init>(Lcom/google/android/recline/widget/LeanbackCardView;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 704
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    invoke-virtual {p0, v1}, Lcom/google/android/recline/widget/LeanbackCardView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 705
    return-void

    .line 682
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private animateInfoHeight(Z)V
    .locals 8
    .param p1, "shown"    # Z

    .prologue
    const/4 v7, 0x0

    .line 629
    invoke-direct {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->cancelAnimations()V

    .line 631
    const/4 v0, 0x0

    .line 632
    .local v0, "extraHeight":I
    if-eqz p1, :cond_0

    .line 633
    iget v5, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMeasuredWidth:I

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 634
    .local v4, "widthSpec":I
    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 636
    .local v2, "heightSpec":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_0

    .line 637
    iget-object v5, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 638
    .local v1, "extraView":Landroid/view/View;
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 639
    invoke-virtual {v1, v4, v2}, Landroid/view/View;->measure(II)V

    .line 640
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 636
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 644
    .end local v1    # "extraView":Landroid/view/View;
    .end local v2    # "heightSpec":I
    .end local v3    # "i":I
    .end local v4    # "widthSpec":I
    :cond_0
    new-instance v6, Lcom/google/android/recline/widget/LeanbackCardView$InfoHeightAnimation;

    iget v7, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoVisFraction:F

    if-eqz p1, :cond_1

    const/high16 v5, 0x3f800000    # 1.0f

    :goto_1
    invoke-direct {v6, p0, v7, v5}, Lcom/google/android/recline/widget/LeanbackCardView$InfoHeightAnimation;-><init>(Lcom/google/android/recline/widget/LeanbackCardView;FF)V

    iput-object v6, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    .line 645
    iget-object v5, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    iget v6, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mSelectedAnimDuration:I

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 646
    iget-object v5, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    new-instance v6, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v5, v6}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 647
    iget-object v5, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    new-instance v6, Lcom/google/android/recline/widget/LeanbackCardView$3;

    invoke-direct {v6, p0}, Lcom/google/android/recline/widget/LeanbackCardView$3;-><init>(Lcom/google/android/recline/widget/LeanbackCardView;)V

    invoke-virtual {v5, v6}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 666
    iget-object v5, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    invoke-virtual {p0, v5}, Lcom/google/android/recline/widget/LeanbackCardView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 667
    return-void

    .line 644
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private animateInfoOffset(Z)V
    .locals 8
    .param p1, "shown"    # Z

    .prologue
    const/4 v7, 0x0

    .line 586
    invoke-direct {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->cancelAnimations()V

    .line 588
    const/4 v0, 0x0

    .line 589
    .local v0, "extraHeight":I
    if-eqz p1, :cond_0

    .line 590
    iget v5, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMeasuredWidth:I

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 591
    .local v4, "widthSpec":I
    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 593
    .local v2, "heightSpec":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_0

    .line 594
    iget-object v5, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 595
    .local v1, "extraView":Landroid/view/View;
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 596
    invoke-virtual {v1, v4, v2}, Landroid/view/View;->measure(II)V

    .line 597
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 593
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 601
    .end local v1    # "extraView":Landroid/view/View;
    .end local v2    # "heightSpec":I
    .end local v3    # "i":I
    .end local v4    # "widthSpec":I
    :cond_0
    new-instance v6, Lcom/google/android/recline/widget/LeanbackCardView$InfoOffsetAnimation;

    iget v7, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOffset:F

    if-eqz p1, :cond_1

    int-to-float v5, v0

    :goto_1
    invoke-direct {v6, p0, v7, v5}, Lcom/google/android/recline/widget/LeanbackCardView$InfoOffsetAnimation;-><init>(Lcom/google/android/recline/widget/LeanbackCardView;FF)V

    iput-object v6, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    .line 602
    iget-object v5, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    iget v6, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mSelectedAnimDuration:I

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 603
    iget-object v5, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    new-instance v6, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v5, v6}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 604
    iget-object v5, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    new-instance v6, Lcom/google/android/recline/widget/LeanbackCardView$2;

    invoke-direct {v6, p0}, Lcom/google/android/recline/widget/LeanbackCardView$2;-><init>(Lcom/google/android/recline/widget/LeanbackCardView;)V

    invoke-virtual {v5, v6}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 623
    iget-object v5, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    invoke-virtual {p0, v5}, Lcom/google/android/recline/widget/LeanbackCardView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 624
    return-void

    .line 601
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private applyActiveState(Z)V
    .locals 2
    .param p1, "active"    # Z

    .prologue
    .line 516
    iget v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOnSelected:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 518
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/recline/widget/LeanbackCardView;->setInfoVisibility(Z)V

    .line 520
    :cond_1
    return-void
.end method

.method private applySelectedState(Z)V
    .locals 4
    .param p1, "focused"    # Z

    .prologue
    const/4 v2, 0x1

    .line 555
    iget-object v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnimationTrigger:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/LeanbackCardView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 557
    iget v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 559
    if-eqz p1, :cond_2

    .line 560
    iget-boolean v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mDelaySelectedAnim:Z

    if-nez v0, :cond_1

    .line 561
    iget-object v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnimationTrigger:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/LeanbackCardView;->post(Ljava/lang/Runnable;)Z

    .line 562
    iput-boolean v2, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mDelaySelectedAnim:Z

    .line 573
    :cond_0
    :goto_0
    return-void

    .line 564
    :cond_1
    iget-object v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnimationTrigger:Ljava/lang/Runnable;

    iget v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mSelectedAnimationDelay:I

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/recline/widget/LeanbackCardView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 567
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/recline/widget/LeanbackCardView;->animateInfoOffset(Z)V

    goto :goto_0

    .line 569
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOnSelected:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    iget v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    if-ne v0, v2, :cond_0

    .line 571
    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/recline/widget/LeanbackCardView;->setInfoVisibility(Z)V

    goto :goto_0
.end method

.method private cancelAnimations()V
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 578
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnim:Landroid/view/animation/Animation;

    .line 580
    :cond_0
    return-void
.end method

.method private findChildrenViews()V
    .locals 13

    .prologue
    const/16 v9, 0x8

    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 468
    iget-object v8, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMainViewList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 469
    iget-object v8, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 470
    iget-object v8, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 472
    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->getChildCount()I

    move-result v1

    .line 474
    .local v1, "count":I
    const/4 v4, 0x0

    .line 475
    .local v4, "infoVisible":Z
    const/4 v2, 0x0

    .line 477
    .local v2, "extraVisible":Z
    iget v8, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    if-ne v8, v6, :cond_2

    .line 478
    iget-boolean v8, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOnSelected:Z

    if-eqz v8, :cond_1

    iget-boolean v4, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardSelected:Z

    .line 491
    :cond_0
    :goto_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v1, :cond_c

    .line 492
    invoke-virtual {p0, v3}, Lcom/google/android/recline/widget/LeanbackCardView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 494
    .local v0, "child":Landroid/view/View;
    if-nez v0, :cond_7

    .line 491
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 478
    .end local v0    # "child":Landroid/view/View;
    .end local v3    # "i":I
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->isActivated()Z

    move-result v4

    goto :goto_0

    .line 480
    :cond_2
    iget v8, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    if-ne v8, v12, :cond_5

    .line 481
    iget-boolean v8, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOnSelected:Z

    if-eqz v8, :cond_4

    iget v8, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoVisFraction:F

    cmpl-float v8, v8, v11

    if-lez v8, :cond_3

    move v4, v6

    :goto_3
    goto :goto_0

    :cond_3
    move v4, v7

    goto :goto_3

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->isActivated()Z

    move-result v4

    goto :goto_3

    .line 483
    :cond_5
    iget v8, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v10, 0x3

    if-ne v8, v10, :cond_0

    .line 484
    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->isActivated()Z

    move-result v4

    .line 485
    iget v8, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOffset:F

    cmpl-float v8, v8, v11

    if-lez v8, :cond_6

    move v2, v6

    :goto_4
    goto :goto_0

    :cond_6
    move v2, v7

    goto :goto_4

    .line 498
    .restart local v0    # "child":Landroid/view/View;
    .restart local v3    # "i":I
    :cond_7
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;

    .line 500
    .local v5, "lp":Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;
    iget v8, v5, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;->viewType:I

    if-ne v8, v6, :cond_9

    .line 501
    iget-object v8, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 502
    if-eqz v4, :cond_8

    move v8, v7

    :goto_5
    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_8
    move v8, v9

    goto :goto_5

    .line 503
    :cond_9
    iget v8, v5, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;->viewType:I

    if-ne v8, v12, :cond_b

    .line 504
    iget-object v8, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 505
    if-eqz v2, :cond_a

    move v8, v7

    :goto_6
    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_a
    move v8, v9

    goto :goto_6

    .line 508
    :cond_b
    iget-object v8, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMainViewList:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 509
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 513
    .end local v0    # "child":Landroid/view/View;
    .end local v5    # "lp":Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;
    :cond_c
    return-void
.end method

.method private setInfoVisibility(Z)V
    .locals 5
    .param p1, "visible"    # Z

    .prologue
    const/4 v3, 0x0

    const/16 v4, 0x8

    .line 523
    iget v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_4

    .line 526
    if-eqz p1, :cond_0

    .line 527
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 528
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 527
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 531
    .end local v0    # "i":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 532
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 531
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 534
    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 535
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 534
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 537
    :cond_2
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOffset:F

    .line 552
    .end local v0    # "i":I
    :cond_3
    :goto_3
    return-void

    .line 539
    :cond_4
    iget v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_7

    .line 541
    iget-boolean v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOnSelected:Z

    if-eqz v1, :cond_5

    .line 542
    invoke-direct {p0, p1}, Lcom/google/android/recline/widget/LeanbackCardView;->animateInfoHeight(Z)V

    goto :goto_3

    .line 544
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_4
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 545
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz p1, :cond_6

    move v2, v3

    :goto_5
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 544
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    move v2, v4

    .line 545
    goto :goto_5

    .line 548
    .end local v0    # "i":I
    :cond_7
    iget v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 550
    invoke-direct {p0, p1}, Lcom/google/android/recline/widget/LeanbackCardView;->animateInfoAlpha(Z)V

    goto :goto_3
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 729
    instance-of v0, p1, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;

    return v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->generateDefaultLayoutParams()Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 714
    new-instance v0, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "x0"    # Landroid/util/AttributeSet;

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/google/android/recline/widget/LeanbackCardView;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "x0"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/google/android/recline/widget/LeanbackCardView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 709
    new-instance v0, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;
    .locals 1
    .param p1, "lp"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 720
    instance-of v0, p1, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;

    if-eqz v0, :cond_0

    .line 721
    new-instance v0, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;

    check-cast p1, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;

    .end local p1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v0, p1}, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;-><init>(Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;)V

    .line 723
    :goto_0
    return-object v0

    .restart local p1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    new-instance v0, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public getCardType()I
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    return v0
.end method

.method public isInfoVisibleOnSelectedOnly()Z
    .locals 1

    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOnSelected:Z

    return v0
.end method

.method public isSelected()Z
    .locals 1

    .prologue
    .line 313
    iget-boolean v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardSelected:Z

    return v0
.end method

.method public isSelectedAnimationDelayed()Z
    .locals 1

    .prologue
    .line 210
    iget-boolean v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mDelaySelectedAnim:Z

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 460
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 461
    iget-object v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mAnimationTrigger:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/LeanbackCardView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 462
    invoke-direct {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->cancelAnimations()V

    .line 463
    iput v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOffset:F

    .line 464
    iput v1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoVisFraction:F

    .line 465
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 11
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 390
    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->getPaddingTop()I

    move-result v7

    int-to-float v0, v7

    .line 393
    .local v0, "currBottom":F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v7, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMainViewList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 394
    iget-object v7, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMainViewList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    .line 395
    .local v5, "mainView":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_0

    .line 396
    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->getPaddingLeft()I

    move-result v7

    float-to-int v8, v0

    iget v9, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMeasuredWidth:I

    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->getPaddingLeft()I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    int-to-float v10, v10

    add-float/2addr v10, v0

    float-to-int v10, v10

    invoke-virtual {v5, v7, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    .line 400
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v0, v7

    .line 393
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 404
    .end local v5    # "mainView":Landroid/view/View;
    :cond_1
    iget v7, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    if-eqz v7, :cond_a

    .line 405
    const/4 v3, 0x0

    .line 406
    .local v3, "infoHeight":F
    const/4 v2, 0x0

    :goto_1
    iget-object v7, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_2

    .line 407
    iget-object v7, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v3, v7

    .line 406
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 410
    :cond_2
    iget v7, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_7

    .line 412
    sub-float/2addr v0, v3

    .line 413
    const/4 v7, 0x0

    cmpg-float v7, v0, v7

    if-gez v7, :cond_3

    .line 414
    const/4 v0, 0x0

    .line 424
    :cond_3
    :goto_2
    const/4 v2, 0x0

    :goto_3
    iget-object v7, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_5

    .line 425
    iget-object v7, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .line 426
    .local v4, "infoView":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_9

    .line 427
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 428
    .local v6, "viewHeight":I
    int-to-float v7, v6

    cmpl-float v7, v7, v3

    if-lez v7, :cond_4

    .line 429
    float-to-int v6, v3

    .line 431
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->getPaddingLeft()I

    move-result v7

    float-to-int v8, v0

    iget v9, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMeasuredWidth:I

    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->getPaddingLeft()I

    move-result v10

    add-int/2addr v9, v10

    int-to-float v10, v6

    add-float/2addr v10, v0

    float-to-int v10, v10

    invoke-virtual {v4, v7, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    .line 435
    int-to-float v7, v6

    add-float/2addr v0, v7

    .line 436
    int-to-float v7, v6

    sub-float/2addr v3, v7

    .line 437
    const/4 v7, 0x0

    cmpg-float v7, v3, v7

    if-gtz v7, :cond_9

    .line 443
    .end local v4    # "infoView":Landroid/view/View;
    .end local v6    # "viewHeight":I
    :cond_5
    iget v7, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v8, 0x3

    if-ne v7, v8, :cond_a

    .line 444
    const/4 v2, 0x0

    :goto_4
    iget-object v7, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_a

    .line 445
    iget-object v7, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 446
    .local v1, "extraView":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_6

    .line 447
    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->getPaddingLeft()I

    move-result v7

    float-to-int v8, v0

    iget v9, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMeasuredWidth:I

    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->getPaddingLeft()I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    int-to-float v10, v10

    add-float/2addr v10, v0

    float-to-int v10, v10

    invoke-virtual {v1, v7, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    .line 451
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v0, v7

    .line 444
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 416
    .end local v1    # "extraView":Landroid/view/View;
    :cond_7
    iget v7, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_8

    .line 417
    iget-boolean v7, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOnSelected:Z

    if-eqz v7, :cond_3

    .line 418
    iget v7, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoVisFraction:F

    mul-float/2addr v3, v7

    goto/16 :goto_2

    .line 421
    :cond_8
    iget v7, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOffset:F

    sub-float/2addr v0, v7

    goto/16 :goto_2

    .line 424
    .restart local v4    # "infoView":Landroid/view/View;
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    .line 456
    .end local v3    # "infoHeight":F
    .end local v4    # "infoView":Landroid/view/View;
    :cond_a
    return-void
.end method

.method protected onMeasure(II)V
    .locals 15
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 323
    const/4 v12, 0x0

    iput v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMeasuredWidth:I

    .line 324
    const/4 v12, 0x0

    iput v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMeasuredHeight:I

    .line 325
    const/4 v10, 0x0

    .line 326
    .local v10, "state":I
    const/4 v8, 0x0

    .line 327
    .local v8, "mainHeight":I
    const/4 v5, 0x0

    .line 328
    .local v5, "infoHeight":I
    const/4 v2, 0x0

    .line 330
    .local v2, "extraHeight":I
    iget-boolean v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOnSelected:Z

    if-eqz v12, :cond_2

    iget v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v13, 0x2

    if-eq v12, v13, :cond_0

    iget v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_2

    :cond_0
    const/4 v6, 0x1

    .line 333
    .local v6, "infoOnSelected":Z
    :goto_0
    invoke-direct {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->findChildrenViews()V

    .line 335
    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v12, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    .line 337
    .local v11, "unspecifiedSpec":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget-object v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMainViewList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v4, v12, :cond_3

    .line 338
    iget-object v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMainViewList:Ljava/util/ArrayList;

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/View;

    .line 339
    .local v9, "mainView":Landroid/view/View;
    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-eq v12, v13, :cond_1

    .line 340
    invoke-virtual {p0, v9, v11, v11}, Lcom/google/android/recline/widget/LeanbackCardView;->measureChild(Landroid/view/View;II)V

    .line 341
    iget v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMeasuredWidth:I

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    invoke-static {v12, v13}, Ljava/lang/Math;->max(II)I

    move-result v12

    iput v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMeasuredWidth:I

    .line 342
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v8, v12

    .line 343
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredState()I

    move-result v12

    invoke-static {v10, v12}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v10

    .line 337
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 330
    .end local v4    # "i":I
    .end local v6    # "infoOnSelected":Z
    .end local v9    # "mainView":Landroid/view/View;
    .end local v11    # "unspecifiedSpec":I
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 349
    .restart local v4    # "i":I
    .restart local v6    # "infoOnSelected":Z
    .restart local v11    # "unspecifiedSpec":I
    :cond_3
    iget v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMeasuredWidth:I

    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v12, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 351
    .local v1, "cardWidthMeasureSpec":I
    iget v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v13, 0x1

    if-eq v12, v13, :cond_4

    iget v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v13, 0x2

    if-eq v12, v13, :cond_4

    iget v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v13, 0x3

    if-ne v12, v13, :cond_9

    .line 354
    :cond_4
    const/4 v4, 0x0

    :goto_2
    iget-object v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v4, v12, :cond_7

    .line 355
    iget-object v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    .line 356
    .local v7, "infoView":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-eq v12, v13, :cond_6

    .line 357
    invoke-virtual {p0, v7, v1, v11}, Lcom/google/android/recline/widget/LeanbackCardView;->measureChild(Landroid/view/View;II)V

    .line 358
    iget v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v13, 0x1

    if-eq v12, v13, :cond_5

    .line 359
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v5, v12

    .line 361
    :cond_5
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredState()I

    move-result v12

    invoke-static {v10, v12}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v10

    .line 354
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 365
    .end local v7    # "infoView":Landroid/view/View;
    :cond_7
    iget v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    const/4 v13, 0x3

    if-ne v12, v13, :cond_9

    .line 366
    const/4 v4, 0x0

    :goto_3
    iget-object v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v4, v12, :cond_9

    .line 367
    iget-object v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 368
    .local v3, "extraView":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-eq v12, v13, :cond_8

    .line 369
    invoke-virtual {p0, v3, v1, v11}, Lcom/google/android/recline/widget/LeanbackCardView;->measureChild(Landroid/view/View;II)V

    .line 370
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v2, v12

    .line 371
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredState()I

    move-result v12

    invoke-static {v10, v12}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v10

    .line 366
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 377
    .end local v3    # "extraView":Landroid/view/View;
    :cond_9
    int-to-float v13, v8

    if-eqz v6, :cond_a

    int-to-float v12, v5

    iget v14, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoVisFraction:F

    mul-float/2addr v12, v14

    :goto_4
    add-float/2addr v12, v13

    int-to-float v13, v2

    add-float/2addr v13, v12

    if-eqz v6, :cond_b

    const/4 v12, 0x0

    :goto_5
    sub-float v12, v13, v12

    float-to-int v12, v12

    iput v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMeasuredHeight:I

    .line 382
    iget v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMeasuredWidth:I

    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->getPaddingLeft()I

    move-result v13

    add-int/2addr v12, v13

    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->getPaddingRight()I

    move-result v13

    add-int/2addr v12, v13

    move/from16 v0, p1

    invoke-static {v12, v0, v10}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v12

    iget v13, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mMeasuredHeight:I

    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->getPaddingTop()I

    move-result v14

    add-int/2addr v13, v14

    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->getPaddingBottom()I

    move-result v14

    add-int/2addr v13, v14

    shl-int/lit8 v14, v10, 0x10

    move/from16 v0, p2

    invoke-static {v13, v0, v14}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v13

    invoke-virtual {p0, v12, v13}, Lcom/google/android/recline/widget/LeanbackCardView;->setMeasuredDimension(II)V

    .line 386
    return-void

    .line 377
    :cond_a
    int-to-float v12, v5

    goto :goto_4

    :cond_b
    iget v12, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOffset:F

    goto :goto_5
.end method

.method public setActivated(Z)V
    .locals 1
    .param p1, "activated"    # Z

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->isActivated()Z

    move-result v0

    if-eq p1, v0, :cond_0

    .line 284
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setActivated(Z)V

    .line 285
    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->isActivated()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/recline/widget/LeanbackCardView;->applyActiveState(Z)V

    .line 287
    :cond_0
    return-void
.end method

.method public setCardType(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 219
    iget v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    if-eq v0, p1, :cond_0

    .line 220
    if-ltz p1, :cond_1

    const/4 v0, 0x4

    if-ge p1, v0, :cond_1

    .line 222
    iput p1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    .line 228
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->requestLayout()V

    .line 230
    :cond_0
    return-void

    .line 224
    :cond_1
    const-string v0, "LeanbackCardView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid card type specified: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Defaulting to type CARD_TYPE_MAIN_ONLY."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardType:I

    goto :goto_0
.end method

.method public setInfoVisibleOnSelectedOnly(Z)V
    .locals 1
    .param p1, "selectedOnly"    # Z

    .prologue
    .line 253
    iget-boolean v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOnSelected:Z

    if-eq v0, p1, :cond_0

    .line 254
    iput-boolean p1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOnSelected:Z

    .line 255
    iget-boolean v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOnSelected:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardSelected:Z

    if-eqz v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    iput v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mInfoVisFraction:F

    .line 256
    invoke-virtual {p0}, Lcom/google/android/recline/widget/LeanbackCardView;->requestLayout()V

    .line 258
    :cond_0
    return-void

    .line 255
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSelected(Z)V
    .locals 1
    .param p1, "selected"    # Z

    .prologue
    .line 299
    iget-boolean v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardSelected:Z

    if-eq v0, p1, :cond_0

    .line 300
    iput-boolean p1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardSelected:Z

    .line 301
    iget-boolean v0, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mCardSelected:Z

    invoke-direct {p0, v0}, Lcom/google/android/recline/widget/LeanbackCardView;->applySelectedState(Z)V

    .line 303
    :cond_0
    return-void
.end method

.method public setSelectedAnimationDelayed(Z)V
    .locals 0
    .param p1, "delay"    # Z

    .prologue
    .line 198
    iput-boolean p1, p0, Lcom/google/android/recline/widget/LeanbackCardView;->mDelaySelectedAnim:Z

    .line 199
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .locals 1

    .prologue
    .line 318
    const/4 v0, 0x0

    return v0
.end method
