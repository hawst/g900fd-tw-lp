.class public Lcom/google/android/recline/app/DialogFragment$Builder;
.super Ljava/lang/Object;
.source "DialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/app/DialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mActions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/recline/app/DialogFragment$Action;",
            ">;"
        }
    .end annotation
.end field

.field private mContentBreadcrumb:Ljava/lang/String;

.field private mContentDescription:Ljava/lang/String;

.field private mContentTitle:Ljava/lang/String;

.field private mIconBackgroundColor:I

.field private mIconBitmap:Landroid/graphics/Bitmap;

.field private mIconPadding:I

.field private mIconResourceId:I

.field private mIconUri:Landroid/net/Uri;

.field private mName:Ljava/lang/String;

.field private mSelectedIndex:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mIconBackgroundColor:I

    return-void
.end method


# virtual methods
.method public actions(Ljava/util/ArrayList;)Lcom/google/android/recline/app/DialogFragment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/recline/app/DialogFragment$Action;",
            ">;)",
            "Lcom/google/android/recline/app/DialogFragment$Builder;"
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/recline/app/DialogFragment$Action;>;"
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mActions:Ljava/util/ArrayList;

    .line 163
    return-object p0
.end method

.method public breadcrumb(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Builder;
    .locals 0
    .param p1, "breadcrumb"    # Ljava/lang/String;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mContentBreadcrumb:Ljava/lang/String;

    .line 128
    return-object p0
.end method

.method public build()Lcom/google/android/recline/app/DialogFragment;
    .locals 4

    .prologue
    .line 104
    new-instance v1, Lcom/google/android/recline/app/DialogFragment;

    invoke-direct {v1}, Lcom/google/android/recline/app/DialogFragment;-><init>()V

    .line 105
    .local v1, "fragment":Lcom/google/android/recline/app/DialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 106
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "title"

    iget-object v3, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mContentTitle:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v2, "breadcrumb"

    iget-object v3, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mContentBreadcrumb:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v2, "description"

    iget-object v3, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mContentDescription:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v2, "iconResourceId"

    iget v3, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mIconResourceId:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 110
    const-string v2, "iconUri"

    iget-object v3, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mIconUri:Landroid/net/Uri;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 111
    const-string v2, "iconBitmap"

    iget-object v3, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mIconBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 112
    const-string v2, "iconBackground"

    iget v3, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mIconBackgroundColor:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 113
    const-string v2, "iconPadding"

    iget v3, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mIconPadding:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 114
    const-string v2, "actions"

    iget-object v3, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mActions:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 115
    const-string v2, "name"

    iget-object v3, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mName:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v2, "selectedIndex"

    iget v3, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mSelectedIndex:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117
    invoke-virtual {v1, v0}, Lcom/google/android/recline/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 118
    return-object v1
.end method

.method public description(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Builder;
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mContentDescription:Ljava/lang/String;

    .line 133
    return-object p0
.end method

.method public iconBitmap(Landroid/graphics/Bitmap;)Lcom/google/android/recline/app/DialogFragment$Builder;
    .locals 0
    .param p1, "iconBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mIconBitmap:Landroid/graphics/Bitmap;

    .line 148
    return-object p0
.end method

.method public selectedIndex(I)Lcom/google/android/recline/app/DialogFragment$Builder;
    .locals 0
    .param p1, "selectedIndex"    # I

    .prologue
    .line 172
    iput p1, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mSelectedIndex:I

    .line 173
    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Builder;
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$Builder;->mContentTitle:Ljava/lang/String;

    .line 123
    return-object p0
.end method
