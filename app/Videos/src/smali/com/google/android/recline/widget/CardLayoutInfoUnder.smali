.class Lcom/google/android/recline/widget/CardLayoutInfoUnder;
.super Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;
.source "CardLayoutInfoUnder.java"


# instance fields
.field private mMeasuredHeight:I

.field private mMeasuredWidth:I


# direct methods
.method constructor <init>(Lcom/google/android/recline/widget/BaseCardView;)V
    .locals 0
    .param p1, "card"    # Lcom/google/android/recline/widget/BaseCardView;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;-><init>(Lcom/google/android/recline/widget/BaseCardView;)V

    .line 32
    return-void
.end method


# virtual methods
.method public hasExtraView()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public hasInfoView()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x1

    return v0
.end method

.method public hasMainView()Z
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x1

    return v0
.end method

.method protected initCardViews()V
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getInfoView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getInfoView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 54
    :cond_0
    return-void
.end method

.method protected onCardActiveStateChanged(Z)V
    .locals 2
    .param p1, "active"    # Z

    .prologue
    .line 100
    if-eqz p1, :cond_1

    .line 101
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getInfoView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getInfoView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getInfoView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getInfoView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 83
    const/4 v0, 0x0

    .line 85
    .local v0, "currTop":I
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getMainView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getMainView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v5, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getMainView()Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->mMeasuredWidth:I

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getMainView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getMainView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getInfoView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getInfoView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v5, :cond_1

    .line 92
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getInfoView()Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->mMeasuredWidth:I

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getInfoView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {v1, v4, v0, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getInfoView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 58
    iput v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->mMeasuredWidth:I

    .line 59
    iput v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->mMeasuredHeight:I

    .line 60
    const/4 v0, 0x0

    .line 62
    .local v0, "state":I
    invoke-virtual {p0, p1, p2}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->measureCardChildren(II)V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getMainView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getMainView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getMainView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iput v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->mMeasuredWidth:I

    .line 66
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getMainView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iput v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->mMeasuredHeight:I

    .line 67
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getMainView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredState()I

    move-result v1

    invoke-static {v0, v1}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v0

    .line 70
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getInfoView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getInfoView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v2, :cond_1

    .line 71
    iget v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->mMeasuredHeight:I

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getInfoView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->mMeasuredHeight:I

    .line 72
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->getInfoView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredState()I

    move-result v1

    invoke-static {v0, v1}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v0

    .line 76
    :cond_1
    iget v1, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->mMeasuredWidth:I

    invoke-static {v1, p1, v0}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v1

    iget v2, p0, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->mMeasuredHeight:I

    shl-int/lit8 v3, v0, 0x10

    invoke-static {v2, p2, v3}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;->setCardMeasuredDimension(II)V

    .line 79
    return-void
.end method
