.class public Lcom/google/android/recline/widget/ControlButtonPresenterSelector;
.super Landroid/support/v17/leanback/widget/PresenterSelector;
.source "ControlButtonPresenterSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/recline/widget/ControlButtonPresenterSelector$ControlButtonPresenter;,
        Lcom/google/android/recline/widget/ControlButtonPresenterSelector$ActionViewHolder;
    }
.end annotation


# instance fields
.field private final mPrimaryPresenter:Landroid/support/v17/leanback/widget/Presenter;

.field private final mSecondaryPresenter:Landroid/support/v17/leanback/widget/Presenter;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/PresenterSelector;-><init>()V

    .line 36
    new-instance v0, Lcom/google/android/recline/widget/ControlButtonPresenterSelector$ControlButtonPresenter;

    sget v1, Lcom/google/android/recline/R$layout;->recline_control_button_primary:I

    invoke-direct {v0, v1}, Lcom/google/android/recline/widget/ControlButtonPresenterSelector$ControlButtonPresenter;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/recline/widget/ControlButtonPresenterSelector;->mPrimaryPresenter:Landroid/support/v17/leanback/widget/Presenter;

    .line 38
    new-instance v0, Lcom/google/android/recline/widget/ControlButtonPresenterSelector$ControlButtonPresenter;

    sget v1, Lcom/google/android/recline/R$layout;->lb_control_button_secondary:I

    invoke-direct {v0, v1}, Lcom/google/android/recline/widget/ControlButtonPresenterSelector$ControlButtonPresenter;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/recline/widget/ControlButtonPresenterSelector;->mSecondaryPresenter:Landroid/support/v17/leanback/widget/Presenter;

    .line 75
    return-void
.end method


# virtual methods
.method public getPresenter(Ljava/lang/Object;)Landroid/support/v17/leanback/widget/Presenter;
    .locals 1
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/recline/widget/ControlButtonPresenterSelector;->mPrimaryPresenter:Landroid/support/v17/leanback/widget/Presenter;

    return-object v0
.end method
