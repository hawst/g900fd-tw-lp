.class public abstract Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;
.super Ljava/lang/Object;
.source "BaseCardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/widget/BaseCardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "CardLayoutManager"
.end annotation


# instance fields
.field private mCard:Lcom/google/android/recline/widget/BaseCardView;

.field private mDelayFocusAnim:Z

.field protected mExtra:Landroid/view/View;

.field private mFocusAnimationDelay:J

.field private mInfo:Landroid/view/View;

.field private mMain:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/recline/widget/BaseCardView;)V
    .locals 2
    .param p1, "card"    # Lcom/google/android/recline/widget/BaseCardView;

    .prologue
    .line 318
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 319
    iput-object p1, p0, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->mCard:Lcom/google/android/recline/widget/BaseCardView;

    .line 321
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->mDelayFocusAnim:Z

    .line 322
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->mCard:Lcom/google/android/recline/widget/BaseCardView;

    invoke-virtual {v0}, Lcom/google/android/recline/widget/BaseCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/recline/R$integer;->lb_card_focus_animation_delay:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->mFocusAnimationDelay:J

    .line 324
    return-void
.end method


# virtual methods
.method protected cardView()Lcom/google/android/recline/widget/BaseCardView;
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->mCard:Lcom/google/android/recline/widget/BaseCardView;

    return-object v0
.end method

.method public getDelayFocusAnimation()Z
    .locals 1

    .prologue
    .line 331
    iget-boolean v0, p0, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->mDelayFocusAnim:Z

    return v0
.end method

.method public getExtraView()Landroid/view/View;
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->mExtra:Landroid/view/View;

    return-object v0
.end method

.method public getFocusAnimationDelayAmount()J
    .locals 2

    .prologue
    .line 339
    iget-wide v0, p0, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->mFocusAnimationDelay:J

    return-wide v0
.end method

.method public getInfoView()Landroid/view/View;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->mInfo:Landroid/view/View;

    return-object v0
.end method

.method public getMainView()Landroid/view/View;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->mMain:Landroid/view/View;

    return-object v0
.end method

.method public abstract hasExtraView()Z
.end method

.method public abstract hasInfoView()Z
.end method

.method public abstract hasMainView()Z
.end method

.method protected initCardViews()V
    .locals 0

    .prologue
    .line 380
    return-void
.end method

.method protected measureCardChildren(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->mCard:Lcom/google/android/recline/widget/BaseCardView;

    # invokes: Lcom/google/android/recline/widget/BaseCardView;->measureChildren(II)V
    invoke-static {v0, p1, p2}, Lcom/google/android/recline/widget/BaseCardView;->access$000(Lcom/google/android/recline/widget/BaseCardView;II)V

    .line 390
    return-void
.end method

.method protected onCardActiveStateChanged(Z)V
    .locals 0
    .param p1, "active"    # Z

    .prologue
    .line 383
    return-void
.end method

.method protected onCardFocusedStateChanged(Z)V
    .locals 0
    .param p1, "focused"    # Z

    .prologue
    .line 386
    return-void
.end method

.method protected abstract onLayout(ZIIII)V
.end method

.method protected abstract onMeasure(II)V
.end method

.method protected setCardMeasuredDimension(II)V
    .locals 1
    .param p1, "measuredWidth"    # I
    .param p2, "measuredHeight"    # I

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->mCard:Lcom/google/android/recline/widget/BaseCardView;

    # invokes: Lcom/google/android/recline/widget/BaseCardView;->setMeasuredDimension(II)V
    invoke-static {v0, p1, p2}, Lcom/google/android/recline/widget/BaseCardView;->access$100(Lcom/google/android/recline/widget/BaseCardView;II)V

    .line 394
    return-void
.end method

.method public setDelayFocusAnimation(Z)V
    .locals 0
    .param p1, "delay"    # Z

    .prologue
    .line 327
    iput-boolean p1, p0, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->mDelayFocusAnim:Z

    .line 328
    return-void
.end method

.method public setExtraView(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 359
    iput-object p1, p0, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->mExtra:Landroid/view/View;

    .line 360
    return-void
.end method

.method public setInfoView(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 351
    iput-object p1, p0, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->mInfo:Landroid/view/View;

    .line 352
    return-void
.end method

.method public setMainView(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 343
    iput-object p1, p0, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->mMain:Landroid/view/View;

    .line 344
    return-void
.end method
