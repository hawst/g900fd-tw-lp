.class Lcom/google/android/recline/app/DialogFragment$1;
.super Ljava/lang/Object;
.source "DialogFragment.java"

# interfaces
.implements Lcom/google/android/recline/app/DialogFragment$Action$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/recline/app/DialogFragment;->setActionView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/recline/app/DialogFragment;


# direct methods
.method constructor <init>(Lcom/google/android/recline/app/DialogFragment;)V
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$1;->this$0:Lcom/google/android/recline/app/DialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionClicked(Lcom/google/android/recline/app/DialogFragment$Action;)V
    .locals 2
    .param p1, "action"    # Lcom/google/android/recline/app/DialogFragment$Action;

    .prologue
    .line 494
    invoke-virtual {p1}, Lcom/google/android/recline/app/DialogFragment$Action;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/recline/app/DialogFragment$Action;->infoOnly()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 509
    :cond_0
    :goto_0
    return-void

    .line 503
    :cond_1
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$1;->this$0:Lcom/google/android/recline/app/DialogFragment;

    # getter for: Lcom/google/android/recline/app/DialogFragment;->mListener:Lcom/google/android/recline/app/DialogFragment$Action$Listener;
    invoke-static {v1}, Lcom/google/android/recline/app/DialogFragment;->access$000(Lcom/google/android/recline/app/DialogFragment;)Lcom/google/android/recline/app/DialogFragment$Action$Listener;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 504
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$1;->this$0:Lcom/google/android/recline/app/DialogFragment;

    # getter for: Lcom/google/android/recline/app/DialogFragment;->mListener:Lcom/google/android/recline/app/DialogFragment$Action$Listener;
    invoke-static {v1}, Lcom/google/android/recline/app/DialogFragment;->access$000(Lcom/google/android/recline/app/DialogFragment;)Lcom/google/android/recline/app/DialogFragment$Action$Listener;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/recline/app/DialogFragment$Action$Listener;->onActionClicked(Lcom/google/android/recline/app/DialogFragment$Action;)V

    goto :goto_0

    .line 505
    :cond_2
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$1;->this$0:Lcom/google/android/recline/app/DialogFragment;

    invoke-virtual {v1}, Lcom/google/android/recline/app/DialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/recline/app/DialogFragment$Action$Listener;

    if-eqz v1, :cond_0

    .line 506
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$1;->this$0:Lcom/google/android/recline/app/DialogFragment;

    invoke-virtual {v1}, Lcom/google/android/recline/app/DialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/recline/app/DialogFragment$Action$Listener;

    .line 507
    .local v0, "listener":Lcom/google/android/recline/app/DialogFragment$Action$Listener;
    invoke-interface {v0, p1}, Lcom/google/android/recline/app/DialogFragment$Action$Listener;->onActionClicked(Lcom/google/android/recline/app/DialogFragment$Action;)V

    goto :goto_0
.end method
