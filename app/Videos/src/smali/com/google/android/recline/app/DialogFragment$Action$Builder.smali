.class public Lcom/google/android/recline/app/DialogFragment$Action$Builder;
.super Ljava/lang/Object;
.source "DialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/app/DialogFragment$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mCheckSetId:I

.field private mChecked:Z

.field private mDescription:Ljava/lang/String;

.field private mDrawableResource:I

.field private mEnabled:Z

.field private mHasNext:Z

.field private mIconUri:Landroid/net/Uri;

.field private mInfoOnly:Z

.field private mIntent:Landroid/content/Intent;

.field private mKey:Ljava/lang/String;

.field private mMultilineDescription:Z

.field private mResourcePackageName:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 959
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 965
    iput v0, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mDrawableResource:I

    .line 971
    iput v0, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mCheckSetId:I

    .line 972
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mEnabled:Z

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/recline/app/DialogFragment$Action;
    .locals 2

    .prologue
    .line 975
    new-instance v0, Lcom/google/android/recline/app/DialogFragment$Action;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/recline/app/DialogFragment$Action;-><init>(Lcom/google/android/recline/app/DialogFragment$1;)V

    .line 976
    .local v0, "action":Lcom/google/android/recline/app/DialogFragment$Action;
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mKey:Ljava/lang/String;

    # setter for: Lcom/google/android/recline/app/DialogFragment$Action;->mKey:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/recline/app/DialogFragment$Action;->access$402(Lcom/google/android/recline/app/DialogFragment$Action;Ljava/lang/String;)Ljava/lang/String;

    .line 977
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mTitle:Ljava/lang/String;

    # setter for: Lcom/google/android/recline/app/DialogFragment$Action;->mTitle:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/recline/app/DialogFragment$Action;->access$502(Lcom/google/android/recline/app/DialogFragment$Action;Ljava/lang/String;)Ljava/lang/String;

    .line 978
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mDescription:Ljava/lang/String;

    # setter for: Lcom/google/android/recline/app/DialogFragment$Action;->mDescription:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/recline/app/DialogFragment$Action;->access$602(Lcom/google/android/recline/app/DialogFragment$Action;Ljava/lang/String;)Ljava/lang/String;

    .line 979
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mIntent:Landroid/content/Intent;

    # setter for: Lcom/google/android/recline/app/DialogFragment$Action;->mIntent:Landroid/content/Intent;
    invoke-static {v0, v1}, Lcom/google/android/recline/app/DialogFragment$Action;->access$702(Lcom/google/android/recline/app/DialogFragment$Action;Landroid/content/Intent;)Landroid/content/Intent;

    .line 980
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mResourcePackageName:Ljava/lang/String;

    # setter for: Lcom/google/android/recline/app/DialogFragment$Action;->mResourcePackageName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/recline/app/DialogFragment$Action;->access$802(Lcom/google/android/recline/app/DialogFragment$Action;Ljava/lang/String;)Ljava/lang/String;

    .line 981
    iget v1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mDrawableResource:I

    # setter for: Lcom/google/android/recline/app/DialogFragment$Action;->mDrawableResource:I
    invoke-static {v0, v1}, Lcom/google/android/recline/app/DialogFragment$Action;->access$902(Lcom/google/android/recline/app/DialogFragment$Action;I)I

    .line 982
    iget-object v1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mIconUri:Landroid/net/Uri;

    # setter for: Lcom/google/android/recline/app/DialogFragment$Action;->mIconUri:Landroid/net/Uri;
    invoke-static {v0, v1}, Lcom/google/android/recline/app/DialogFragment$Action;->access$1002(Lcom/google/android/recline/app/DialogFragment$Action;Landroid/net/Uri;)Landroid/net/Uri;

    .line 983
    iget-boolean v1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mChecked:Z

    # setter for: Lcom/google/android/recline/app/DialogFragment$Action;->mChecked:Z
    invoke-static {v0, v1}, Lcom/google/android/recline/app/DialogFragment$Action;->access$1102(Lcom/google/android/recline/app/DialogFragment$Action;Z)Z

    .line 984
    iget-boolean v1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mMultilineDescription:Z

    # setter for: Lcom/google/android/recline/app/DialogFragment$Action;->mMultilineDescription:Z
    invoke-static {v0, v1}, Lcom/google/android/recline/app/DialogFragment$Action;->access$1202(Lcom/google/android/recline/app/DialogFragment$Action;Z)Z

    .line 985
    iget-boolean v1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mHasNext:Z

    # setter for: Lcom/google/android/recline/app/DialogFragment$Action;->mHasNext:Z
    invoke-static {v0, v1}, Lcom/google/android/recline/app/DialogFragment$Action;->access$1302(Lcom/google/android/recline/app/DialogFragment$Action;Z)Z

    .line 986
    iget-boolean v1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mInfoOnly:Z

    # setter for: Lcom/google/android/recline/app/DialogFragment$Action;->mInfoOnly:Z
    invoke-static {v0, v1}, Lcom/google/android/recline/app/DialogFragment$Action;->access$1402(Lcom/google/android/recline/app/DialogFragment$Action;Z)Z

    .line 987
    iget v1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mCheckSetId:I

    # setter for: Lcom/google/android/recline/app/DialogFragment$Action;->mCheckSetId:I
    invoke-static {v0, v1}, Lcom/google/android/recline/app/DialogFragment$Action;->access$1502(Lcom/google/android/recline/app/DialogFragment$Action;I)I

    .line 988
    iget-boolean v1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mEnabled:Z

    # setter for: Lcom/google/android/recline/app/DialogFragment$Action;->mEnabled:Z
    invoke-static {v0, v1}, Lcom/google/android/recline/app/DialogFragment$Action;->access$1602(Lcom/google/android/recline/app/DialogFragment$Action;Z)Z

    .line 989
    return-object v0
.end method

.method public checkSetId(I)Lcom/google/android/recline/app/DialogFragment$Action$Builder;
    .locals 0
    .param p1, "checkSetId"    # I

    .prologue
    .line 1048
    iput p1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mCheckSetId:I

    .line 1049
    return-object p0
.end method

.method public checked(Z)Lcom/google/android/recline/app/DialogFragment$Action$Builder;
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 1028
    iput-boolean p1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mChecked:Z

    .line 1029
    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 1003
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mDescription:Ljava/lang/String;

    .line 1004
    return-object p0
.end method

.method public drawableResource(I)Lcom/google/android/recline/app/DialogFragment$Action$Builder;
    .locals 0
    .param p1, "drawableResource"    # I

    .prologue
    .line 1018
    iput p1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mDrawableResource:I

    .line 1019
    return-object p0
.end method

.method public iconUri(Landroid/net/Uri;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;
    .locals 0
    .param p1, "iconUri"    # Landroid/net/Uri;

    .prologue
    .line 1023
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mIconUri:Landroid/net/Uri;

    .line 1024
    return-object p0
.end method

.method public intent(Landroid/content/Intent;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1008
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mIntent:Landroid/content/Intent;

    .line 1009
    return-object p0
.end method

.method public key(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 993
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mKey:Ljava/lang/String;

    .line 994
    return-object p0
.end method

.method public multilineDescription(Z)Lcom/google/android/recline/app/DialogFragment$Action$Builder;
    .locals 0
    .param p1, "multilineDescription"    # Z

    .prologue
    .line 1033
    iput-boolean p1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mMultilineDescription:Z

    .line 1034
    return-object p0
.end method

.method public resourcePackageName(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;
    .locals 0
    .param p1, "resourcePackageName"    # Ljava/lang/String;

    .prologue
    .line 1013
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mResourcePackageName:Ljava/lang/String;

    .line 1014
    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 998
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->mTitle:Ljava/lang/String;

    .line 999
    return-object p0
.end method
