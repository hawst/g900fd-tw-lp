.class public Lcom/google/android/recline/widget/SparseArrayObjectAdapter;
.super Landroid/support/v17/leanback/widget/ObjectAdapter;
.source "SparseArrayObjectAdapter.java"


# instance fields
.field private mItems:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ObjectAdapter;-><init>()V

    .line 14
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V
    .locals 1
    .param p1, "presenterSelector"    # Landroid/support/v17/leanback/widget/PresenterSelector;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/ObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 14
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    .line 21
    return-void
.end method


# virtual methods
.method public clear(I)V
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 104
    iget-object v1, p0, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    .line 105
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 106
    iget-object v1, p0, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->delete(I)V

    .line 107
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->notifyItemRangeRemoved(II)V

    .line 109
    :cond_0
    return-void
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfValue(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public notifyArrayItemRangeChanged(II)V
    .locals 0
    .param p1, "positionStart"    # I
    .param p2, "itemCount"    # I

    .prologue
    .line 75
    invoke-virtual {p0, p1, p2}, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->notifyItemRangeChanged(II)V

    .line 76
    return-void
.end method

.method public set(ILjava/lang/Object;)V
    .locals 3
    .param p1, "key"    # I
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    .line 85
    iget-object v1, p0, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    .line 86
    .local v0, "index":I
    if-ltz v0, :cond_1

    .line 87
    iget-object v1, p0, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    if-eq v1, p2, :cond_0

    .line 88
    iget-object v1, p0, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v1, v0, p2}, Landroid/util/SparseArray;->setValueAt(ILjava/lang/Object;)V

    .line 89
    invoke-virtual {p0, v0, v2}, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->notifyItemRangeChanged(II)V

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    iget-object v1, p0, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, p2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 93
    iget-object v1, p0, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    .line 94
    invoke-virtual {p0, v0, v2}, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->notifyItemRangeInserted(II)V

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->mItems:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    return v0
.end method
