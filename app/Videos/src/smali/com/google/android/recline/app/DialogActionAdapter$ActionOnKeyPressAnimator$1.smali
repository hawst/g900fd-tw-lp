.class Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "DialogActionAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->prepareAndAnimateView(Landroid/view/View;FFIILandroid/view/animation/Interpolator;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;

.field final synthetic val$action:Lcom/google/android/recline/app/DialogFragment$Action;

.field final synthetic val$pressed:Z

.field final synthetic val$v:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;Landroid/view/View;ZLcom/google/android/recline/app/DialogFragment$Action;)V
    .locals 0

    .prologue
    .line 517
    iput-object p1, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator$1;->this$0:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;

    iput-object p2, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator$1;->val$v:Landroid/view/View;

    iput-boolean p3, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator$1;->val$pressed:Z

    iput-object p4, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator$1;->val$action:Lcom/google/android/recline/app/DialogFragment$Action;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator$1;->val$v:Landroid/view/View;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 522
    iget-boolean v0, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator$1;->val$pressed:Z

    if-nez v0, :cond_0

    .line 523
    iget-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator$1;->this$0:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;

    # getter for: Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->mListener:Lcom/google/android/recline/app/DialogFragment$Action$Listener;
    invoke-static {v0}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->access$100(Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;)Lcom/google/android/recline/app/DialogFragment$Action$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 524
    iget-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator$1;->this$0:Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;

    # getter for: Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->mListener:Lcom/google/android/recline/app/DialogFragment$Action$Listener;
    invoke-static {v0}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;->access$100(Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator;)Lcom/google/android/recline/app/DialogFragment$Action$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnKeyPressAnimator$1;->val$action:Lcom/google/android/recline/app/DialogFragment$Action;

    invoke-interface {v0, v1}, Lcom/google/android/recline/app/DialogFragment$Action$Listener;->onActionClicked(Lcom/google/android/recline/app/DialogFragment$Action;)V

    .line 527
    :cond_0
    return-void
.end method
