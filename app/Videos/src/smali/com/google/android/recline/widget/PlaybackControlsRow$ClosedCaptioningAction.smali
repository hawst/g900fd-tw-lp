.class public Lcom/google/android/recline/widget/PlaybackControlsRow$ClosedCaptioningAction;
.super Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;
.source "PlaybackControlsRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/widget/PlaybackControlsRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ClosedCaptioningAction"
.end annotation


# static fields
.field public static OFF:I

.field public static ON:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 536
    const/4 v0, 0x0

    sput v0, Lcom/google/android/recline/widget/PlaybackControlsRow$ClosedCaptioningAction;->OFF:I

    .line 537
    const/4 v0, 0x1

    sput v0, Lcom/google/android/recline/widget/PlaybackControlsRow$ClosedCaptioningAction;->ON:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 544
    sget v0, Lcom/google/android/recline/R$attr;->playbackControlsIconHighlightColor:I

    # invokes: Lcom/google/android/recline/widget/PlaybackControlsRow;->getColorFromTheme(Landroid/content/Context;I)I
    invoke-static {p1, v0}, Lcom/google/android/recline/widget/PlaybackControlsRow;->access$100(Landroid/content/Context;I)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/recline/widget/PlaybackControlsRow$ClosedCaptioningAction;-><init>(Landroid/content/Context;I)V

    .line 546
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "highlightColor"    # I

    .prologue
    .line 554
    sget v3, Lcom/google/android/recline/R$id;->lb_control_high_quality:I

    invoke-direct {p0, v3}, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;-><init>(I)V

    .line 555
    sget v3, Lcom/google/android/recline/R$styleable;->lbPlaybackControlsActionIcons_closed_captioning:I

    # invokes: Lcom/google/android/recline/widget/PlaybackControlsRow;->getStyledDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    invoke-static {p1, v3}, Lcom/google/android/recline/widget/PlaybackControlsRow;->access$000(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .line 557
    .local v2, "uncoloredDrawable":Landroid/graphics/drawable/BitmapDrawable;
    const/4 v3, 0x2

    new-array v0, v3, [Landroid/graphics/drawable/Drawable;

    .line 558
    .local v0, "drawables":[Landroid/graphics/drawable/Drawable;
    sget v3, Lcom/google/android/recline/widget/PlaybackControlsRow$ClosedCaptioningAction;->OFF:I

    aput-object v2, v0, v3

    .line 559
    sget v3, Lcom/google/android/recline/widget/PlaybackControlsRow$ClosedCaptioningAction;->ON:I

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    # invokes: Lcom/google/android/recline/widget/PlaybackControlsRow;->createBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    invoke-static {v6, p2}, Lcom/google/android/recline/widget/PlaybackControlsRow;->access$200(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v4, v0, v3

    .line 561
    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/PlaybackControlsRow$ClosedCaptioningAction;->setDrawables([Landroid/graphics/drawable/Drawable;)V

    .line 563
    array-length v3, v0

    new-array v1, v3, [Ljava/lang/String;

    .line 564
    .local v1, "labels":[Ljava/lang/String;
    sget v3, Lcom/google/android/recline/widget/PlaybackControlsRow$ClosedCaptioningAction;->OFF:I

    sget v4, Lcom/google/android/recline/R$string;->lb_playback_controls_closed_captioning_enable:I

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 565
    sget v3, Lcom/google/android/recline/widget/PlaybackControlsRow$ClosedCaptioningAction;->ON:I

    sget v4, Lcom/google/android/recline/R$string;->lb_playback_controls_closed_captioning_disable:I

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 566
    invoke-virtual {p0, v1}, Lcom/google/android/recline/widget/PlaybackControlsRow$ClosedCaptioningAction;->setLabels([Ljava/lang/String;)V

    .line 567
    return-void
.end method
