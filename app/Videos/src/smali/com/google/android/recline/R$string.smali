.class public final Lcom/google/android/recline/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final lb_dialog_list_item_disabled_chevron_background_alpha:I = 0x7f0b0068

.field public static final lb_dialog_list_item_disabled_description_text_alpha:I = 0x7f0b0067

.field public static final lb_dialog_list_item_disabled_title_text_alpha:I = 0x7f0b0066

.field public static final lb_dialog_list_item_selected_chevron_background_alpha:I = 0x7f0b0063

.field public static final lb_dialog_list_item_selected_description_text_alpha:I = 0x7f0b0062

.field public static final lb_dialog_list_item_selected_title_text_alpha:I = 0x7f0b0061

.field public static final lb_dialog_list_item_unselected_description_text_alpha:I = 0x7f0b0065

.field public static final lb_dialog_list_item_unselected_text_alpha:I = 0x7f0b0064

.field public static final lb_playback_controls_closed_captioning_disable:I = 0x7f0b004a

.field public static final lb_playback_controls_closed_captioning_enable:I = 0x7f0b0049

.field public static final lb_playback_controls_fast_forward:I = 0x7f0b0039

.field public static final lb_playback_controls_pause:I = 0x7f0b0038

.field public static final lb_playback_controls_play:I = 0x7f0b0037

.field public static final lb_playback_controls_rewind:I = 0x7f0b003a

.field public static final lb_playback_controls_skip_next:I = 0x7f0b003b

.field public static final lb_playback_controls_skip_previous:I = 0x7f0b003c

.field public static final recline_control_display_fast_forward_multiplier:I = 0x7f0b0069

.field public static final recline_control_display_rewind_multiplier:I = 0x7f0b006a

.field public static final recline_playback_controls_fast_forward_multiplier:I = 0x7f0b006b

.field public static final recline_playback_controls_rewind_multiplier:I = 0x7f0b006c
