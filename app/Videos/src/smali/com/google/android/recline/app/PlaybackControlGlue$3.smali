.class Lcom/google/android/recline/app/PlaybackControlGlue$3;
.super Ljava/lang/Object;
.source "PlaybackControlGlue.java"

# interfaces
.implements Lcom/google/android/recline/app/PlaybackOverlayFragment$InputEventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/app/PlaybackControlGlue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/recline/app/PlaybackControlGlue;


# direct methods
.method constructor <init>(Lcom/google/android/recline/app/PlaybackControlGlue;)V
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleInputEvent(Landroid/view/InputEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/InputEvent;

    .prologue
    const/4 v3, 0x1

    .line 198
    const/4 v1, 0x0

    .line 199
    .local v1, "result":Z
    instance-of v2, p1, Landroid/view/KeyEvent;

    if-eqz v2, :cond_0

    move-object v2, p1

    check-cast v2, Landroid/view/KeyEvent;

    invoke-virtual {v2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    .line 201
    check-cast p1, Landroid/view/KeyEvent;

    .end local p1    # "event":Landroid/view/InputEvent;
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 202
    .local v0, "keyCode":I
    sparse-switch v0, :sswitch_data_0

    .line 248
    .end local v0    # "keyCode":I
    :cond_0
    :goto_0
    return v1

    .line 208
    .restart local v0    # "keyCode":I
    :sswitch_0
    iget-object v2, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    # getter for: Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I
    invoke-static {v2}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$300(Lcom/google/android/recline/app/PlaybackControlGlue;)I

    move-result v2

    const/16 v4, 0xa

    if-ge v2, v4, :cond_1

    iget-object v2, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    # getter for: Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I
    invoke-static {v2}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$300(Lcom/google/android/recline/app/PlaybackControlGlue;)I

    move-result v2

    const/16 v4, -0xa

    if-gt v2, v4, :cond_0

    .line 210
    :cond_1
    iget-object v2, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    # setter for: Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I
    invoke-static {v2, v3}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$302(Lcom/google/android/recline/app/PlaybackControlGlue;I)I

    .line 211
    iget-object v2, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    iget-object v4, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    # getter for: Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I
    invoke-static {v4}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$300(Lcom/google/android/recline/app/PlaybackControlGlue;)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/android/recline/app/PlaybackControlGlue;->startPlayback(I)V

    .line 212
    iget-object v2, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    # invokes: Lcom/google/android/recline/app/PlaybackControlGlue;->updatePlaybackStatusAfterUserAction()V
    invoke-static {v2}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$400(Lcom/google/android/recline/app/PlaybackControlGlue;)V

    .line 213
    const/4 v2, 0x4

    if-ne v0, v2, :cond_2

    move v1, v3

    :goto_1
    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 217
    :sswitch_1
    iget-object v2, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    # getter for: Lcom/google/android/recline/app/PlaybackControlGlue;->mPlayPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;
    invoke-static {v2}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$500(Lcom/google/android/recline/app/PlaybackControlGlue;)Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 218
    iget-object v2, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    iget-object v3, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    # getter for: Lcom/google/android/recline/app/PlaybackControlGlue;->mPlayPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;
    invoke-static {v3}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$500(Lcom/google/android/recline/app/PlaybackControlGlue;)Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    move-result-object v3

    # invokes: Lcom/google/android/recline/app/PlaybackControlGlue;->handleActionClicked(Landroid/support/v17/leanback/widget/Action;)Z
    invoke-static {v2, v3}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$100(Lcom/google/android/recline/app/PlaybackControlGlue;Landroid/support/v17/leanback/widget/Action;)Z

    .line 219
    const/4 v1, 0x1

    goto :goto_0

    .line 223
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    # getter for: Lcom/google/android/recline/app/PlaybackControlGlue;->mFastForwardAction:Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;
    invoke-static {v2}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$600(Lcom/google/android/recline/app/PlaybackControlGlue;)Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 224
    iget-object v2, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    iget-object v3, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    # getter for: Lcom/google/android/recline/app/PlaybackControlGlue;->mFastForwardAction:Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;
    invoke-static {v3}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$600(Lcom/google/android/recline/app/PlaybackControlGlue;)Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;

    move-result-object v3

    # invokes: Lcom/google/android/recline/app/PlaybackControlGlue;->handleActionClicked(Landroid/support/v17/leanback/widget/Action;)Z
    invoke-static {v2, v3}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$100(Lcom/google/android/recline/app/PlaybackControlGlue;Landroid/support/v17/leanback/widget/Action;)Z

    .line 225
    const/4 v1, 0x1

    goto :goto_0

    .line 229
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    # getter for: Lcom/google/android/recline/app/PlaybackControlGlue;->mRewindAction:Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;
    invoke-static {v2}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$700(Lcom/google/android/recline/app/PlaybackControlGlue;)Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 230
    iget-object v2, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    iget-object v3, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    # getter for: Lcom/google/android/recline/app/PlaybackControlGlue;->mRewindAction:Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;
    invoke-static {v3}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$700(Lcom/google/android/recline/app/PlaybackControlGlue;)Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;

    move-result-object v3

    # invokes: Lcom/google/android/recline/app/PlaybackControlGlue;->handleActionClicked(Landroid/support/v17/leanback/widget/Action;)Z
    invoke-static {v2, v3}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$100(Lcom/google/android/recline/app/PlaybackControlGlue;Landroid/support/v17/leanback/widget/Action;)Z

    .line 231
    const/4 v1, 0x1

    goto :goto_0

    .line 235
    :sswitch_4
    iget-object v2, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    # getter for: Lcom/google/android/recline/app/PlaybackControlGlue;->mSkipPreviousAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipPreviousAction;
    invoke-static {v2}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$800(Lcom/google/android/recline/app/PlaybackControlGlue;)Lcom/google/android/recline/widget/PlaybackControlsRow$SkipPreviousAction;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 236
    iget-object v2, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    iget-object v3, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    # getter for: Lcom/google/android/recline/app/PlaybackControlGlue;->mSkipPreviousAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipPreviousAction;
    invoke-static {v3}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$800(Lcom/google/android/recline/app/PlaybackControlGlue;)Lcom/google/android/recline/widget/PlaybackControlsRow$SkipPreviousAction;

    move-result-object v3

    # invokes: Lcom/google/android/recline/app/PlaybackControlGlue;->handleActionClicked(Landroid/support/v17/leanback/widget/Action;)Z
    invoke-static {v2, v3}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$100(Lcom/google/android/recline/app/PlaybackControlGlue;Landroid/support/v17/leanback/widget/Action;)Z

    .line 237
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 241
    :sswitch_5
    iget-object v2, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    # getter for: Lcom/google/android/recline/app/PlaybackControlGlue;->mSkipNextAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipNextAction;
    invoke-static {v2}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$900(Lcom/google/android/recline/app/PlaybackControlGlue;)Lcom/google/android/recline/widget/PlaybackControlsRow$SkipNextAction;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 242
    iget-object v2, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    iget-object v3, p0, Lcom/google/android/recline/app/PlaybackControlGlue$3;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    # getter for: Lcom/google/android/recline/app/PlaybackControlGlue;->mSkipNextAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipNextAction;
    invoke-static {v3}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$900(Lcom/google/android/recline/app/PlaybackControlGlue;)Lcom/google/android/recline/widget/PlaybackControlsRow$SkipNextAction;

    move-result-object v3

    # invokes: Lcom/google/android/recline/app/PlaybackControlGlue;->handleActionClicked(Landroid/support/v17/leanback/widget/Action;)Z
    invoke-static {v2, v3}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$100(Lcom/google/android/recline/app/PlaybackControlGlue;Landroid/support/v17/leanback/widget/Action;)Z

    .line 243
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 202
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x55 -> :sswitch_1
        0x57 -> :sswitch_5
        0x58 -> :sswitch_4
        0x59 -> :sswitch_3
        0x5a -> :sswitch_2
    .end sparse-switch
.end method
