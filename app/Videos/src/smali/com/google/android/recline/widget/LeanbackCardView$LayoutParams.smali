.class public Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;
.super Landroid/view/ViewGroup$LayoutParams;
.source "LeanbackCardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/widget/LeanbackCardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field public viewType:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "layout"
        mapping = {
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x0
                to = "MAIN"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x1
                to = "INFO"
            .end subannotation,
            .subannotation Landroid/view/ViewDebug$IntToString;
                from = 0x2
                to = "EXTRA"
            .end subannotation
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 767
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 743
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;->viewType:I

    .line 768
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 754
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 743
    iput v2, p0, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;->viewType:I

    .line 755
    sget-object v1, Lcom/google/android/recline/R$styleable;->LeanbackCardView_Layout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 757
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v1, Lcom/google/android/recline/R$styleable;->LeanbackCardView_Layout_lbLayout_viewType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;->viewType:I

    .line 760
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 761
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 774
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 743
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;->viewType:I

    .line 775
    return-void
.end method

.method public constructor <init>(Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;)V
    .locals 1
    .param p1, "source"    # Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;

    .prologue
    .line 784
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 743
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;->viewType:I

    .line 786
    iget v0, p1, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;->viewType:I

    iput v0, p0, Lcom/google/android/recline/widget/LeanbackCardView$LayoutParams;->viewType:I

    .line 787
    return-void
.end method
