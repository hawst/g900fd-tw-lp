.class public Lcom/google/android/recline/util/RefcountObject;
.super Ljava/lang/Object;
.source "RefcountObject.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/recline/util/RefcountObject$RefcountListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mRefcount:I

.field private mRefcountListener:Lcom/google/android/recline/util/RefcountObject$RefcountListener;


# virtual methods
.method public declared-synchronized releaseRef()I
    .locals 1

    .prologue
    .line 40
    .local p0, "this":Lcom/google/android/recline/util/RefcountObject;, "Lcom/google/android/recline/util/RefcountObject<TT;>;"
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/recline/util/RefcountObject;->mRefcount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/recline/util/RefcountObject;->mRefcount:I

    .line 41
    iget v0, p0, Lcom/google/android/recline/util/RefcountObject;->mRefcount:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/recline/util/RefcountObject;->mRefcountListener:Lcom/google/android/recline/util/RefcountObject$RefcountListener;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/google/android/recline/util/RefcountObject;->mRefcountListener:Lcom/google/android/recline/util/RefcountObject$RefcountListener;

    invoke-interface {v0, p0}, Lcom/google/android/recline/util/RefcountObject$RefcountListener;->onRefcountZero(Lcom/google/android/recline/util/RefcountObject;)V

    .line 44
    :cond_0
    iget v0, p0, Lcom/google/android/recline/util/RefcountObject;->mRefcount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
