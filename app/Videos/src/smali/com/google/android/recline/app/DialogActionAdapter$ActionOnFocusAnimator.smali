.class Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;
.super Ljava/lang/Object;
.source "DialogActionAdapter.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/app/DialogActionAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ActionOnFocusAnimator"
.end annotation


# instance fields
.field private mAnimationDuration:I

.field private mDisabledChevronAlpha:F

.field private mDisabledDescriptionAlpha:F

.field private mDisabledTitleAlpha:F

.field private mOnFocusListener:Lcom/google/android/recline/app/DialogFragment$Action$OnFocusListener;

.field private mResourcesSet:Z

.field private mSelectedChevronAlpha:F

.field private mSelectedDescriptionAlpha:F

.field private mSelectedTitleAlpha:F

.field private mSelectedView:Landroid/view/View;

.field private mUnselectedAlpha:F

.field private mUnselectedDescriptionAlpha:F


# direct methods
.method constructor <init>(Lcom/google/android/recline/app/DialogFragment$Action$OnFocusListener;)V
    .locals 0
    .param p1, "onFocusListener"    # Lcom/google/android/recline/app/DialogFragment$Action$OnFocusListener;

    .prologue
    .line 299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 300
    iput-object p1, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mOnFocusListener:Lcom/google/android/recline/app/DialogFragment$Action$OnFocusListener;

    .line 301
    return-void
.end method

.method private changeFocus(Landroid/view/View;ZZ)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z
    .param p3, "shouldAnimate"    # Z

    .prologue
    .line 331
    if-nez p1, :cond_0

    .line 389
    :goto_0
    return-void

    .line 335
    :cond_0
    iget-boolean v10, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mResourcesSet:Z

    if-nez v10, :cond_1

    .line 336
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mResourcesSet:Z

    .line 337
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 339
    .local v7, "res":Landroid/content/res/Resources;
    sget v10, Lcom/google/android/recline/R$integer;->lb_dialog_animation_duration:I

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v10

    iput v10, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mAnimationDuration:I

    .line 340
    sget v10, Lcom/google/android/recline/R$string;->lb_dialog_list_item_unselected_text_alpha:I

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    iput v10, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mUnselectedAlpha:F

    .line 343
    sget v10, Lcom/google/android/recline/R$string;->lb_dialog_list_item_selected_title_text_alpha:I

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    iput v10, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mSelectedTitleAlpha:F

    .line 345
    sget v10, Lcom/google/android/recline/R$string;->lb_dialog_list_item_disabled_title_text_alpha:I

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    iput v10, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mDisabledTitleAlpha:F

    .line 348
    sget v10, Lcom/google/android/recline/R$string;->lb_dialog_list_item_selected_description_text_alpha:I

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    iput v10, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mSelectedDescriptionAlpha:F

    .line 351
    sget v10, Lcom/google/android/recline/R$string;->lb_dialog_list_item_unselected_description_text_alpha:I

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    iput v10, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mUnselectedDescriptionAlpha:F

    .line 354
    sget v10, Lcom/google/android/recline/R$string;->lb_dialog_list_item_disabled_description_text_alpha:I

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    iput v10, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mDisabledDescriptionAlpha:F

    .line 358
    sget v10, Lcom/google/android/recline/R$string;->lb_dialog_list_item_selected_chevron_background_alpha:I

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    iput v10, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mSelectedChevronAlpha:F

    .line 361
    sget v10, Lcom/google/android/recline/R$string;->lb_dialog_list_item_disabled_chevron_background_alpha:I

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    iput v10, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mDisabledChevronAlpha:F

    .line 366
    .end local v7    # "res":Landroid/content/res/Resources;
    :cond_1
    sget v10, Lcom/google/android/recline/R$id;->action_title:I

    invoke-virtual {p1, v10}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;

    invoke-virtual {v10}, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->getAction()Lcom/google/android/recline/app/DialogFragment$Action;

    move-result-object v0

    .line 368
    .local v0, "action":Lcom/google/android/recline/app/DialogFragment$Action;
    invoke-virtual {v0}, Lcom/google/android/recline/app/DialogFragment$Action;->isEnabled()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-virtual {v0}, Lcom/google/android/recline/app/DialogFragment$Action;->infoOnly()Z

    move-result v10

    if-nez v10, :cond_4

    if-eqz p2, :cond_3

    iget v9, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mSelectedTitleAlpha:F

    .line 370
    .local v9, "titleAlpha":F
    :goto_1
    if-eqz p2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/recline/app/DialogFragment$Action;->infoOnly()Z

    move-result v10

    if-eqz v10, :cond_5

    :cond_2
    iget v5, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mUnselectedDescriptionAlpha:F

    .line 372
    .local v5, "descriptionAlpha":F
    :goto_2
    invoke-virtual {v0}, Lcom/google/android/recline/app/DialogFragment$Action;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-virtual {v0}, Lcom/google/android/recline/app/DialogFragment$Action;->infoOnly()Z

    move-result v10

    if-nez v10, :cond_8

    invoke-virtual {v0}, Lcom/google/android/recline/app/DialogFragment$Action;->isEnabled()Z

    move-result v10

    if-eqz v10, :cond_7

    iget v3, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mSelectedChevronAlpha:F

    .line 375
    .local v3, "chevronAlpha":F
    :goto_3
    sget v10, Lcom/google/android/recline/R$id;->action_title:I

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 376
    .local v8, "title":Landroid/widget/TextView;
    invoke-direct {p0, v8, p3, v9}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->setAlpha(Landroid/view/View;ZF)V

    .line 378
    sget v10, Lcom/google/android/recline/R$id;->action_description:I

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 379
    .local v4, "description":Landroid/widget/TextView;
    invoke-direct {p0, v4, p3, v5}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->setAlpha(Landroid/view/View;ZF)V

    .line 381
    sget v10, Lcom/google/android/recline/R$id;->action_checkmark:I

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 382
    .local v1, "checkmark":Landroid/widget/ImageView;
    invoke-direct {p0, v1, p3, v9}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->setAlpha(Landroid/view/View;ZF)V

    .line 384
    sget v10, Lcom/google/android/recline/R$id;->action_icon:I

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 385
    .local v6, "icon":Landroid/widget/ImageView;
    invoke-direct {p0, v6, p3, v9}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->setAlpha(Landroid/view/View;ZF)V

    .line 387
    sget v10, Lcom/google/android/recline/R$id;->action_next_chevron:I

    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 388
    .local v2, "chevron":Landroid/widget/ImageView;
    invoke-direct {p0, v2, p3, v3}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->setAlpha(Landroid/view/View;ZF)V

    goto/16 :goto_0

    .line 368
    .end local v1    # "checkmark":Landroid/widget/ImageView;
    .end local v2    # "chevron":Landroid/widget/ImageView;
    .end local v3    # "chevronAlpha":F
    .end local v4    # "description":Landroid/widget/TextView;
    .end local v5    # "descriptionAlpha":F
    .end local v6    # "icon":Landroid/widget/ImageView;
    .end local v8    # "title":Landroid/widget/TextView;
    .end local v9    # "titleAlpha":F
    :cond_3
    iget v9, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mUnselectedAlpha:F

    goto :goto_1

    :cond_4
    iget v9, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mDisabledTitleAlpha:F

    goto :goto_1

    .line 370
    .restart local v9    # "titleAlpha":F
    :cond_5
    invoke-virtual {v0}, Lcom/google/android/recline/app/DialogFragment$Action;->isEnabled()Z

    move-result v10

    if-eqz v10, :cond_6

    iget v5, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mSelectedDescriptionAlpha:F

    goto :goto_2

    :cond_6
    iget v5, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mDisabledDescriptionAlpha:F

    goto :goto_2

    .line 372
    .restart local v5    # "descriptionAlpha":F
    :cond_7
    iget v3, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mDisabledChevronAlpha:F

    goto :goto_3

    :cond_8
    const/4 v3, 0x0

    goto :goto_3
.end method

.method private setAlpha(Landroid/view/View;ZF)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "shouldAnimate"    # Z
    .param p3, "alpha"    # F

    .prologue
    .line 392
    if-eqz p2, :cond_0

    .line 393
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mAnimationDuration:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 400
    :goto_0
    return-void

    .line 398
    :cond_0
    invoke-virtual {p1, p3}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    const/4 v1, 0x1

    .line 313
    if-eqz p2, :cond_1

    .line 314
    iput-object p1, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mSelectedView:Landroid/view/View;

    .line 315
    invoke-direct {p0, p1, v1, v1}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->changeFocus(Landroid/view/View;ZZ)V

    .line 316
    iget-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mOnFocusListener:Lcom/google/android/recline/app/DialogFragment$Action$OnFocusListener;

    if-eqz v0, :cond_0

    .line 319
    iget-object v1, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mOnFocusListener:Lcom/google/android/recline/app/DialogFragment$Action$OnFocusListener;

    sget v0, Lcom/google/android/recline/R$id;->action_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;

    invoke-virtual {v0}, Lcom/google/android/recline/app/DialogActionAdapter$ActionViewHolder;->getAction()Lcom/google/android/recline/app/DialogFragment$Action;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/recline/app/DialogFragment$Action$OnFocusListener;->onActionFocused(Lcom/google/android/recline/app/DialogFragment$Action;)V

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 323
    :cond_1
    iget-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mSelectedView:Landroid/view/View;

    if-ne v0, p1, :cond_2

    .line 324
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mSelectedView:Landroid/view/View;

    .line 326
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->changeFocus(Landroid/view/View;ZZ)V

    goto :goto_0
.end method

.method public unFocus(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x0

    .line 308
    if-eqz p1, :cond_0

    .end local p1    # "v":Landroid/view/View;
    :goto_0
    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->changeFocus(Landroid/view/View;ZZ)V

    .line 309
    return-void

    .line 308
    .restart local p1    # "v":Landroid/view/View;
    :cond_0
    iget-object p1, p0, Lcom/google/android/recline/app/DialogActionAdapter$ActionOnFocusAnimator;->mSelectedView:Landroid/view/View;

    goto :goto_0
.end method
