.class public abstract Lcom/google/android/recline/app/PlaybackControlGlue;
.super Ljava/lang/Object;
.source "PlaybackControlGlue.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

.field private mExternalOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

.field private mFadeWhenPlaying:Z

.field private mFastForwardAction:Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;

.field private final mFastForwardSpeeds:[I

.field private final mFragment:Lcom/google/android/recline/app/PlaybackOverlayFragment;

.field private final mHandler:Landroid/os/Handler;

.field private final mInputEventHandler:Lcom/google/android/recline/app/PlaybackOverlayFragment$InputEventHandler;

.field private final mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

.field private mPlayPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

.field private mPlaybackSpeed:I

.field private mPrimaryActionsAdapter:Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

.field private mRewindAction:Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;

.field private final mRewindSpeeds:[I

.field private mSkipNextAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipNextAction;

.field private mSkipPreviousAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipPreviousAction;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/recline/app/PlaybackOverlayFragment;[I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fragment"    # Lcom/google/android/recline/app/PlaybackOverlayFragment;
    .param p3, "seekSpeeds"    # [I

    .prologue
    .line 267
    invoke-direct {p0, p1, p2, p3, p3}, Lcom/google/android/recline/app/PlaybackControlGlue;-><init>(Landroid/content/Context;Lcom/google/android/recline/app/PlaybackOverlayFragment;[I[I)V

    .line 268
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/recline/app/PlaybackOverlayFragment;[I[I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fragment"    # Lcom/google/android/recline/app/PlaybackOverlayFragment;
    .param p3, "fastForwardSpeeds"    # [I
    .param p4, "rewindSpeeds"    # [I

    .prologue
    const/4 v2, 0x5

    .line 286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    .line 166
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFadeWhenPlaying:Z

    .line 168
    new-instance v0, Lcom/google/android/recline/app/PlaybackControlGlue$1;

    invoke-direct {v0, p0}, Lcom/google/android/recline/app/PlaybackControlGlue$1;-><init>(Lcom/google/android/recline/app/PlaybackControlGlue;)V

    iput-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mHandler:Landroid/os/Handler;

    .line 177
    new-instance v0, Lcom/google/android/recline/app/PlaybackControlGlue$2;

    invoke-direct {v0, p0}, Lcom/google/android/recline/app/PlaybackControlGlue$2;-><init>(Lcom/google/android/recline/app/PlaybackControlGlue;)V

    iput-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    .line 194
    new-instance v0, Lcom/google/android/recline/app/PlaybackControlGlue$3;

    invoke-direct {v0, p0}, Lcom/google/android/recline/app/PlaybackControlGlue$3;-><init>(Lcom/google/android/recline/app/PlaybackControlGlue;)V

    iput-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mInputEventHandler:Lcom/google/android/recline/app/PlaybackOverlayFragment$InputEventHandler;

    .line 287
    iput-object p1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mContext:Landroid/content/Context;

    .line 288
    iput-object p2, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFragment:Lcom/google/android/recline/app/PlaybackOverlayFragment;

    .line 289
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFragment:Lcom/google/android/recline/app/PlaybackOverlayFragment;

    invoke-virtual {v0}, Lcom/google/android/recline/app/PlaybackOverlayFragment;->getOnItemViewClickedListener()Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 290
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fragment OnItemViewClickedListener already present"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFragment:Lcom/google/android/recline/app/PlaybackOverlayFragment;

    iget-object v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    invoke-virtual {v0, v1}, Lcom/google/android/recline/app/PlaybackOverlayFragment;->setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V

    .line 293
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFragment:Lcom/google/android/recline/app/PlaybackOverlayFragment;

    invoke-virtual {v0}, Lcom/google/android/recline/app/PlaybackOverlayFragment;->getInputEventHandler_recline()Lcom/google/android/recline/app/PlaybackOverlayFragment$InputEventHandler;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 294
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fragment InputEventListener already present"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 296
    :cond_1
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFragment:Lcom/google/android/recline/app/PlaybackOverlayFragment;

    iget-object v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mInputEventHandler:Lcom/google/android/recline/app/PlaybackOverlayFragment$InputEventHandler;

    invoke-virtual {v0, v1}, Lcom/google/android/recline/app/PlaybackOverlayFragment;->setInputEventHandler(Lcom/google/android/recline/app/PlaybackOverlayFragment$InputEventHandler;)V

    .line 297
    array-length v0, p3

    if-eqz v0, :cond_2

    array-length v0, p3

    if-le v0, v2, :cond_3

    .line 298
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "invalid fastForwardSpeeds array size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :cond_3
    iput-object p3, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFastForwardSpeeds:[I

    .line 301
    array-length v0, p4

    if-eqz v0, :cond_4

    array-length v0, p4

    if-le v0, v2, :cond_5

    .line 302
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "invalid rewindSpeeds array size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 304
    :cond_5
    iput-object p4, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mRewindSpeeds:[I

    .line 305
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/recline/app/PlaybackControlGlue;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/PlaybackControlGlue;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->updatePlaybackState()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/recline/app/PlaybackControlGlue;Landroid/support/v17/leanback/widget/Action;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/recline/app/PlaybackControlGlue;
    .param p1, "x1"    # Landroid/support/v17/leanback/widget/Action;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/recline/app/PlaybackControlGlue;->handleActionClicked(Landroid/support/v17/leanback/widget/Action;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/recline/app/PlaybackControlGlue;)Landroid/support/v17/leanback/widget/OnItemViewClickedListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/recline/app/PlaybackControlGlue;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mExternalOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/recline/app/PlaybackControlGlue;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/recline/app/PlaybackControlGlue;

    .prologue
    .line 62
    iget v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/recline/app/PlaybackControlGlue;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/PlaybackControlGlue;
    .param p1, "x1"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/recline/app/PlaybackControlGlue;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/PlaybackControlGlue;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->updatePlaybackStatusAfterUserAction()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/recline/app/PlaybackControlGlue;)Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/recline/app/PlaybackControlGlue;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlayPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/recline/app/PlaybackControlGlue;)Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/recline/app/PlaybackControlGlue;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFastForwardAction:Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/recline/app/PlaybackControlGlue;)Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/recline/app/PlaybackControlGlue;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mRewindAction:Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/recline/app/PlaybackControlGlue;)Lcom/google/android/recline/widget/PlaybackControlsRow$SkipPreviousAction;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/recline/app/PlaybackControlGlue;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mSkipPreviousAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipPreviousAction;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/recline/app/PlaybackControlGlue;)Lcom/google/android/recline/widget/PlaybackControlsRow$SkipNextAction;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/recline/app/PlaybackControlGlue;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mSkipNextAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipNextAction;

    return-object v0
.end method

.method private getMaxForwardSpeedId()I
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFastForwardSpeeds:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v0, v0, 0xa

    return v0
.end method

.method private getMaxRewindSpeedId()I
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mRewindSpeeds:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v0, v0, 0xa

    return v0
.end method

.method private handleActionClicked(Landroid/support/v17/leanback/widget/Action;)Z
    .locals 3
    .param p1, "action"    # Landroid/support/v17/leanback/widget/Action;

    .prologue
    const/4 v2, 0x1

    .line 422
    const/4 v0, 0x0

    .line 423
    .local v0, "handled":Z
    iget-object v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlayPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    if-ne p1, v1, :cond_2

    .line 424
    iget v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    if-eq v1, v2, :cond_1

    .line 425
    iput v2, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    .line 426
    iget v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    invoke-virtual {p0, v1}, Lcom/google/android/recline/app/PlaybackControlGlue;->startPlayback(I)V

    .line 431
    :goto_0
    invoke-direct {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->updatePlaybackStatusAfterUserAction()V

    .line 432
    const/4 v0, 0x1

    .line 476
    :cond_0
    :goto_1
    return v0

    .line 428
    :cond_1
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    .line 429
    invoke-virtual {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->pausePlayback()V

    goto :goto_0

    .line 433
    :cond_2
    iget-object v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mSkipNextAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipNextAction;

    if-ne p1, v1, :cond_3

    .line 434
    invoke-virtual {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->skipToNext()V

    .line 435
    const/4 v0, 0x1

    goto :goto_1

    .line 436
    :cond_3
    iget-object v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mSkipPreviousAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipPreviousAction;

    if-ne p1, v1, :cond_4

    .line 437
    invoke-virtual {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->skipToPrevious()V

    .line 438
    const/4 v0, 0x1

    goto :goto_1

    .line 439
    :cond_4
    iget-object v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFastForwardAction:Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;

    if-ne p1, v1, :cond_6

    .line 440
    iget v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    invoke-direct {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->getMaxForwardSpeedId()I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 441
    iget v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    packed-switch v1, :pswitch_data_0

    .line 453
    :goto_2
    :pswitch_0
    iget v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    invoke-virtual {p0, v1}, Lcom/google/android/recline/app/PlaybackControlGlue;->startPlayback(I)V

    .line 454
    invoke-direct {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->updatePlaybackStatusAfterUserAction()V

    .line 456
    :cond_5
    const/4 v0, 0x1

    goto :goto_1

    .line 444
    :pswitch_1
    const/16 v1, 0xa

    iput v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    goto :goto_2

    .line 450
    :pswitch_2
    iget v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    goto :goto_2

    .line 457
    :cond_6
    iget-object v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mRewindAction:Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;

    if-ne p1, v1, :cond_0

    .line 458
    iget v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    invoke-direct {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->getMaxRewindSpeedId()I

    move-result v2

    neg-int v2, v2

    if-le v1, v2, :cond_7

    .line 459
    iget v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    sparse-switch v1, :sswitch_data_0

    .line 471
    :goto_3
    iget v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    invoke-virtual {p0, v1}, Lcom/google/android/recline/app/PlaybackControlGlue;->startPlayback(I)V

    .line 472
    invoke-direct {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->updatePlaybackStatusAfterUserAction()V

    .line 474
    :cond_7
    const/4 v0, 0x1

    goto :goto_1

    .line 462
    :sswitch_0
    const/16 v1, -0xa

    iput v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    goto :goto_3

    .line 468
    :sswitch_1
    iget v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    goto :goto_3

    .line 441
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 459
    :sswitch_data_0
    .sparse-switch
        -0xd -> :sswitch_1
        -0xc -> :sswitch_1
        -0xb -> :sswitch_1
        -0xa -> :sswitch_1
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
    .end sparse-switch
.end method

.method private static notifyItemChanged(Lcom/google/android/recline/widget/SparseArrayObjectAdapter;Ljava/lang/Object;)V
    .locals 2
    .param p0, "adapter"    # Lcom/google/android/recline/widget/SparseArrayObjectAdapter;
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 632
    invoke-virtual {p0, p1}, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 633
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 634
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->notifyArrayItemRangeChanged(II)V

    .line 636
    :cond_0
    return-void
.end method

.method private updateControlsRow()V
    .locals 2

    .prologue
    .line 488
    invoke-direct {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->updateRowMetadata()V

    .line 489
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 490
    invoke-direct {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->updatePlaybackState()V

    .line 491
    return-void
.end method

.method private updatePlaybackState()V
    .locals 1

    .prologue
    .line 522
    invoke-virtual {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->hasValidMedia()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 523
    invoke-virtual {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->getCurrentSpeedId()I

    move-result v0

    iput v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    .line 524
    iget v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    invoke-direct {p0, v0}, Lcom/google/android/recline/app/PlaybackControlGlue;->updatePlaybackState(I)V

    .line 526
    :cond_0
    return-void
.end method

.method private updatePlaybackState(I)V
    .locals 12
    .param p1, "playbackSpeed"    # I

    .prologue
    const/16 v9, 0x10

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-wide/16 v10, 0x0

    const/4 v8, 0x0

    .line 529
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    if-nez v5, :cond_1

    .line 629
    :cond_0
    :goto_0
    return-void

    .line 533
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->getSupportedActions()J

    move-result-wide v0

    .line 534
    .local v0, "actions":J
    const-wide/16 v6, 0x10

    and-long/2addr v6, v0

    cmp-long v5, v6, v10

    if-eqz v5, :cond_c

    .line 535
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mSkipPreviousAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipPreviousAction;

    if-nez v5, :cond_2

    .line 536
    new-instance v5, Lcom/google/android/recline/widget/PlaybackControlsRow$SkipPreviousAction;

    iget-object v6, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/google/android/recline/widget/PlaybackControlsRow$SkipPreviousAction;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mSkipPreviousAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipPreviousAction;

    .line 538
    :cond_2
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPrimaryActionsAdapter:Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

    iget-object v6, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mSkipPreviousAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipPreviousAction;

    invoke-virtual {v5, v9, v6}, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->set(ILjava/lang/Object;)V

    .line 543
    :goto_1
    const-wide/16 v6, 0x20

    and-long/2addr v6, v0

    cmp-long v5, v6, v10

    if-eqz v5, :cond_d

    .line 544
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mRewindAction:Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;

    if-nez v5, :cond_3

    .line 545
    new-instance v5, Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;

    iget-object v6, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mRewindSpeeds:[I

    array-length v7, v7

    invoke-direct {v5, v6, v7}, Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mRewindAction:Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;

    .line 548
    :cond_3
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPrimaryActionsAdapter:Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

    const/16 v6, 0x20

    iget-object v7, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mRewindAction:Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;

    invoke-virtual {v5, v6, v7}, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->set(ILjava/lang/Object;)V

    .line 553
    :goto_2
    const-wide/16 v6, 0x40

    and-long/2addr v6, v0

    cmp-long v5, v6, v10

    if-eqz v5, :cond_e

    .line 554
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlayPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    if-nez v5, :cond_4

    .line 555
    new-instance v5, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    iget-object v6, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlayPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    .line 557
    :cond_4
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPrimaryActionsAdapter:Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

    const/16 v6, 0x40

    iget-object v7, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlayPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    invoke-virtual {v5, v6, v7}, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->set(ILjava/lang/Object;)V

    .line 562
    :goto_3
    const-wide/16 v6, 0x80

    and-long/2addr v6, v0

    cmp-long v5, v6, v10

    if-eqz v5, :cond_f

    .line 563
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFastForwardAction:Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;

    if-nez v5, :cond_5

    .line 564
    new-instance v5, Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;

    iget-object v6, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFastForwardSpeeds:[I

    array-length v7, v7

    invoke-direct {v5, v6, v7}, Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;-><init>(Landroid/content/Context;I)V

    iput-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFastForwardAction:Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;

    .line 567
    :cond_5
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPrimaryActionsAdapter:Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

    const/16 v6, 0x80

    iget-object v7, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFastForwardAction:Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;

    invoke-virtual {v5, v6, v7}, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->set(ILjava/lang/Object;)V

    .line 572
    :goto_4
    const-wide/16 v6, 0x100

    and-long/2addr v6, v0

    cmp-long v5, v6, v10

    if-eqz v5, :cond_10

    .line 573
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mSkipNextAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipNextAction;

    if-nez v5, :cond_6

    .line 574
    new-instance v5, Lcom/google/android/recline/widget/PlaybackControlsRow$SkipNextAction;

    iget-object v6, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/google/android/recline/widget/PlaybackControlsRow$SkipNextAction;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mSkipNextAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipNextAction;

    .line 576
    :cond_6
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPrimaryActionsAdapter:Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

    const/16 v6, 0x100

    iget-object v7, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mSkipNextAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipNextAction;

    invoke-virtual {v5, v6, v7}, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->set(ILjava/lang/Object;)V

    .line 582
    :goto_5
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFastForwardAction:Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;

    if-eqz v5, :cond_8

    .line 583
    const/4 v2, 0x0

    .line 584
    .local v2, "index":I
    const/16 v5, 0xa

    if-lt p1, v5, :cond_7

    .line 585
    add-int/lit8 v2, p1, -0xa

    .line 586
    invoke-direct {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->getMaxForwardSpeedId()I

    move-result v5

    if-ge p1, v5, :cond_7

    .line 587
    add-int/lit8 v2, v2, 0x1

    .line 590
    :cond_7
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFastForwardAction:Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;

    invoke-virtual {v5}, Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;->getIndex()I

    move-result v5

    if-eq v5, v2, :cond_8

    .line 591
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFastForwardAction:Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;

    invoke-virtual {v5, v2}, Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;->setIndex(I)V

    .line 592
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPrimaryActionsAdapter:Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

    iget-object v6, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFastForwardAction:Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;

    invoke-static {v5, v6}, Lcom/google/android/recline/app/PlaybackControlGlue;->notifyItemChanged(Lcom/google/android/recline/widget/SparseArrayObjectAdapter;Ljava/lang/Object;)V

    .line 595
    .end local v2    # "index":I
    :cond_8
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mRewindAction:Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;

    if-eqz v5, :cond_a

    .line 596
    const/4 v2, 0x0

    .line 597
    .restart local v2    # "index":I
    const/16 v5, -0xa

    if-gt p1, v5, :cond_9

    .line 598
    neg-int v5, p1

    add-int/lit8 v2, v5, -0xa

    .line 599
    neg-int v5, p1

    invoke-direct {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->getMaxRewindSpeedId()I

    move-result v6

    if-ge v5, v6, :cond_9

    .line 600
    add-int/lit8 v2, v2, 0x1

    .line 603
    :cond_9
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mRewindAction:Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;

    invoke-virtual {v5}, Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;->getIndex()I

    move-result v5

    if-eq v5, v2, :cond_a

    .line 604
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mRewindAction:Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;

    invoke-virtual {v5, v2}, Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;->setIndex(I)V

    .line 605
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPrimaryActionsAdapter:Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

    iget-object v6, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mRewindAction:Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;

    invoke-static {v5, v6}, Lcom/google/android/recline/app/PlaybackControlGlue;->notifyItemChanged(Lcom/google/android/recline/widget/SparseArrayObjectAdapter;Ljava/lang/Object;)V

    .line 609
    .end local v2    # "index":I
    :cond_a
    if-nez p1, :cond_11

    .line 610
    invoke-virtual {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->updateProgress()V

    .line 611
    invoke-virtual {p0, v4}, Lcom/google/android/recline/app/PlaybackControlGlue;->enableProgressUpdating(Z)V

    .line 616
    :goto_6
    iget-boolean v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFadeWhenPlaying:Z

    if-eqz v5, :cond_b

    .line 617
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFragment:Lcom/google/android/recline/app/PlaybackOverlayFragment;

    if-ne p1, v3, :cond_12

    :goto_7
    invoke-virtual {v5, v3}, Lcom/google/android/recline/app/PlaybackOverlayFragment;->setFadingEnabled(Z)V

    .line 620
    :cond_b
    iget-object v3, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlayPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    if-eqz v3, :cond_0

    .line 621
    if-nez p1, :cond_13

    sget v2, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->PLAY:I

    .line 624
    .restart local v2    # "index":I
    :goto_8
    iget-object v3, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlayPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    invoke-virtual {v3}, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->getIndex()I

    move-result v3

    if-eq v3, v2, :cond_0

    .line 625
    iget-object v3, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlayPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    invoke-virtual {v3, v2}, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->setIndex(I)V

    .line 626
    iget-object v3, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPrimaryActionsAdapter:Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

    iget-object v4, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlayPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    invoke-static {v3, v4}, Lcom/google/android/recline/app/PlaybackControlGlue;->notifyItemChanged(Lcom/google/android/recline/widget/SparseArrayObjectAdapter;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 540
    .end local v2    # "index":I
    :cond_c
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPrimaryActionsAdapter:Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

    invoke-virtual {v5, v9}, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->clear(I)V

    .line 541
    iput-object v8, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mSkipPreviousAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipPreviousAction;

    goto/16 :goto_1

    .line 550
    :cond_d
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPrimaryActionsAdapter:Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

    const/16 v6, 0x20

    invoke-virtual {v5, v6}, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->clear(I)V

    .line 551
    iput-object v8, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mRewindAction:Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;

    goto/16 :goto_2

    .line 559
    :cond_e
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPrimaryActionsAdapter:Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

    const/16 v6, 0x40

    invoke-virtual {v5, v6}, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->clear(I)V

    .line 560
    iput-object v8, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlayPauseAction:Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;

    goto/16 :goto_3

    .line 569
    :cond_f
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPrimaryActionsAdapter:Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

    const/16 v6, 0x80

    invoke-virtual {v5, v6}, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->clear(I)V

    .line 570
    iput-object v8, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mFastForwardAction:Lcom/google/android/recline/widget/PlaybackControlsRow$FastForwardAction;

    goto/16 :goto_4

    .line 578
    :cond_10
    iget-object v5, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPrimaryActionsAdapter:Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

    const/16 v6, 0x100

    invoke-virtual {v5, v6}, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;->clear(I)V

    .line 579
    iput-object v8, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mSkipNextAction:Lcom/google/android/recline/widget/PlaybackControlsRow$SkipNextAction;

    goto/16 :goto_5

    .line 613
    :cond_11
    invoke-virtual {p0, v3}, Lcom/google/android/recline/app/PlaybackControlGlue;->enableProgressUpdating(Z)V

    goto :goto_6

    :cond_12
    move v3, v4

    .line 617
    goto :goto_7

    .line 621
    :cond_13
    sget v2, Lcom/google/android/recline/widget/PlaybackControlsRow$PlayPauseAction;->PAUSE:I

    goto :goto_8
.end method

.method private updatePlaybackStatusAfterUserAction()V
    .locals 4

    .prologue
    const/16 v1, 0x64

    .line 494
    iget v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    invoke-direct {p0, v0}, Lcom/google/android/recline/app/PlaybackControlGlue;->updatePlaybackState(I)V

    .line 496
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 497
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 499
    return-void
.end method

.method private updateRowMetadata()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 502
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    if-nez v0, :cond_0

    .line 519
    :goto_0
    return-void

    .line 508
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->hasValidMedia()Z

    move-result v0

    if-nez v0, :cond_1

    .line 509
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/recline/widget/PlaybackControlsRow;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 510
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    invoke-virtual {v0, v2}, Lcom/google/android/recline/widget/PlaybackControlsRow;->setTotalTime(I)V

    .line 511
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    invoke-virtual {v0, v2}, Lcom/google/android/recline/widget/PlaybackControlsRow;->setCurrentTime(I)V

    .line 518
    :goto_1
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    invoke-virtual {p0, v0}, Lcom/google/android/recline/app/PlaybackControlGlue;->onRowChanged(Lcom/google/android/recline/widget/PlaybackControlsRow;)V

    goto :goto_0

    .line 513
    :cond_1
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    invoke-virtual {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->getMediaArt()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/recline/widget/PlaybackControlsRow;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 514
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    invoke-virtual {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->getMediaDuration()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/recline/widget/PlaybackControlsRow;->setTotalTime(I)V

    .line 515
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    invoke-virtual {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/recline/widget/PlaybackControlsRow;->setCurrentTime(I)V

    goto :goto_1
.end method


# virtual methods
.method protected createPrimaryActionsAdapter(Landroid/support/v17/leanback/widget/PresenterSelector;)Lcom/google/android/recline/widget/SparseArrayObjectAdapter;
    .locals 1
    .param p1, "presenterSelector"    # Landroid/support/v17/leanback/widget/PresenterSelector;

    .prologue
    .line 751
    new-instance v0, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

    invoke-direct {v0, p1}, Lcom/google/android/recline/widget/SparseArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    return-object v0
.end method

.method public enableProgressUpdating(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 401
    return-void
.end method

.method public abstract getCurrentPosition()I
.end method

.method public abstract getCurrentSpeedId()I
.end method

.method public abstract getMediaArt()Landroid/graphics/drawable/Drawable;
.end method

.method public abstract getMediaDuration()I
.end method

.method public abstract getSupportedActions()J
.end method

.method public getUpdatePeriod()I
    .locals 1

    .prologue
    .line 409
    const/16 v0, 0x1f4

    return v0
.end method

.method public abstract hasValidMedia()Z
.end method

.method protected abstract onRowChanged(Lcom/google/android/recline/widget/PlaybackControlsRow;)V
.end method

.method protected onStateChanged()V
    .locals 5

    .prologue
    const/16 v4, 0x64

    .line 761
    invoke-virtual {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->hasValidMedia()Z

    move-result v0

    if-nez v0, :cond_0

    .line 777
    :goto_0
    return-void

    .line 764
    :cond_0
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 765
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 766
    invoke-virtual {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->getCurrentSpeedId()I

    move-result v0

    iget v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPlaybackSpeed:I

    if-eq v0, v1, :cond_1

    .line 768
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 772
    :cond_1
    invoke-direct {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->updatePlaybackState()V

    goto :goto_0

    .line 775
    :cond_2
    invoke-direct {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->updatePlaybackState()V

    goto :goto_0
.end method

.method protected abstract pausePlayback()V
.end method

.method public setControlsRow(Lcom/google/android/recline/widget/PlaybackControlsRow;)V
    .locals 2
    .param p1, "controlsRow"    # Lcom/google/android/recline/widget/PlaybackControlsRow;

    .prologue
    .line 382
    iput-object p1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    .line 383
    new-instance v0, Lcom/google/android/recline/widget/ControlButtonPresenterSelector;

    invoke-direct {v0}, Lcom/google/android/recline/widget/ControlButtonPresenterSelector;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/recline/app/PlaybackControlGlue;->createPrimaryActionsAdapter(Landroid/support/v17/leanback/widget/PresenterSelector;)Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPrimaryActionsAdapter:Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

    .line 385
    iget-object v0, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    iget-object v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mPrimaryActionsAdapter:Lcom/google/android/recline/widget/SparseArrayObjectAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/recline/widget/PlaybackControlsRow;->setPrimaryActionsAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 386
    invoke-direct {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->updateControlsRow()V

    .line 387
    return-void
.end method

.method public setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    .prologue
    .line 366
    iput-object p1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mExternalOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    .line 367
    return-void
.end method

.method protected abstract skipToNext()V
.end method

.method protected abstract skipToPrevious()V
.end method

.method protected abstract startPlayback(I)V
.end method

.method public updateProgress()V
    .locals 2

    .prologue
    .line 416
    invoke-virtual {p0}, Lcom/google/android/recline/app/PlaybackControlGlue;->getCurrentPosition()I

    move-result v0

    .line 418
    .local v0, "position":I
    iget-object v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue;->mControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;

    invoke-virtual {v1, v0}, Lcom/google/android/recline/widget/PlaybackControlsRow;->setCurrentTime(I)V

    .line 419
    return-void
.end method
