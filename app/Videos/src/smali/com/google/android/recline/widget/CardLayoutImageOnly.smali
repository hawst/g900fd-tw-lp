.class Lcom/google/android/recline/widget/CardLayoutImageOnly;
.super Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;
.source "CardLayoutImageOnly.java"


# instance fields
.field private mMeasuredHeight:I

.field private mMeasuredWidth:I


# direct methods
.method constructor <init>(Lcom/google/android/recline/widget/BaseCardView;)V
    .locals 0
    .param p1, "card"    # Lcom/google/android/recline/widget/BaseCardView;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;-><init>(Lcom/google/android/recline/widget/BaseCardView;)V

    .line 32
    return-void
.end method


# virtual methods
.method public hasExtraView()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public hasInfoView()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public hasMainView()Z
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x1

    return v0
.end method

.method protected initCardViews()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/4 v3, 0x0

    .line 75
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutImageOnly;->getMainView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutImageOnly;->getMainView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutImageOnly;->getMainView()Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/google/android/recline/widget/CardLayoutImageOnly;->mMeasuredWidth:I

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutImageOnly;->getMainView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 78
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v1, 0x0

    .line 55
    iput v1, p0, Lcom/google/android/recline/widget/CardLayoutImageOnly;->mMeasuredWidth:I

    .line 56
    iput v1, p0, Lcom/google/android/recline/widget/CardLayoutImageOnly;->mMeasuredHeight:I

    .line 57
    const/4 v0, 0x0

    .line 59
    .local v0, "state":I
    invoke-virtual {p0, p1, p2}, Lcom/google/android/recline/widget/CardLayoutImageOnly;->measureCardChildren(II)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutImageOnly;->getMainView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutImageOnly;->getMainView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutImageOnly;->getMainView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iput v1, p0, Lcom/google/android/recline/widget/CardLayoutImageOnly;->mMeasuredWidth:I

    .line 63
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutImageOnly;->getMainView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iput v1, p0, Lcom/google/android/recline/widget/CardLayoutImageOnly;->mMeasuredHeight:I

    .line 64
    invoke-virtual {p0}, Lcom/google/android/recline/widget/CardLayoutImageOnly;->getMainView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredState()I

    move-result v1

    invoke-static {v0, v1}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v0

    .line 68
    :cond_0
    iget v1, p0, Lcom/google/android/recline/widget/CardLayoutImageOnly;->mMeasuredWidth:I

    invoke-static {v1, p1, v0}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v1

    iget v2, p0, Lcom/google/android/recline/widget/CardLayoutImageOnly;->mMeasuredHeight:I

    shl-int/lit8 v3, v0, 0x10

    invoke-static {v2, p2, v3}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/recline/widget/CardLayoutImageOnly;->setCardMeasuredDimension(II)V

    .line 71
    return-void
.end method
