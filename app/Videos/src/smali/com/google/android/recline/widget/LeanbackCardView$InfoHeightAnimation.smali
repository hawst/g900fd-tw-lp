.class Lcom/google/android/recline/widget/LeanbackCardView$InfoHeightAnimation;
.super Landroid/view/animation/Animation;
.source "LeanbackCardView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/widget/LeanbackCardView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InfoHeightAnimation"
.end annotation


# instance fields
.field private mDelta:F

.field private mStartValue:F

.field final synthetic this$0:Lcom/google/android/recline/widget/LeanbackCardView;


# direct methods
.method public constructor <init>(Lcom/google/android/recline/widget/LeanbackCardView;FF)V
    .locals 1
    .param p2, "start"    # F
    .param p3, "end"    # F

    .prologue
    .line 814
    iput-object p1, p0, Lcom/google/android/recline/widget/LeanbackCardView$InfoHeightAnimation;->this$0:Lcom/google/android/recline/widget/LeanbackCardView;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 815
    iput p2, p0, Lcom/google/android/recline/widget/LeanbackCardView$InfoHeightAnimation;->mStartValue:F

    .line 816
    sub-float v0, p3, p2

    iput v0, p0, Lcom/google/android/recline/widget/LeanbackCardView$InfoHeightAnimation;->mDelta:F

    .line 817
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    .line 821
    iget-object v0, p0, Lcom/google/android/recline/widget/LeanbackCardView$InfoHeightAnimation;->this$0:Lcom/google/android/recline/widget/LeanbackCardView;

    iget v1, p0, Lcom/google/android/recline/widget/LeanbackCardView$InfoHeightAnimation;->mStartValue:F

    iget v2, p0, Lcom/google/android/recline/widget/LeanbackCardView$InfoHeightAnimation;->mDelta:F

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    # setter for: Lcom/google/android/recline/widget/LeanbackCardView;->mInfoVisFraction:F
    invoke-static {v0, v1}, Lcom/google/android/recline/widget/LeanbackCardView;->access$502(Lcom/google/android/recline/widget/LeanbackCardView;F)F

    .line 822
    iget-object v0, p0, Lcom/google/android/recline/widget/LeanbackCardView$InfoHeightAnimation;->this$0:Lcom/google/android/recline/widget/LeanbackCardView;

    invoke-virtual {v0}, Lcom/google/android/recline/widget/LeanbackCardView;->requestLayout()V

    .line 823
    return-void
.end method
