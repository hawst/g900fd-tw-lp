.class public abstract Lcom/google/android/recline/widget/BaseCardView;
.super Landroid/view/ViewGroup;
.source "BaseCardView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;
    }
.end annotation


# static fields
.field public static final CARD_TYPE_IMAGE_ONLY:I = 0x4

.field public static final CARD_TYPE_INFO_OVER:I = 0x3

.field public static final CARD_TYPE_INFO_UNDER:I = 0x1

.field public static final CARD_TYPE_INFO_UNDER_WITH_EXTRA:I = 0x2


# instance fields
.field private mCardActive:Z

.field private mCardFocused:Z

.field private mCardViewId:I

.field private mExtraView:Landroid/view/View;

.field private mExtraViewId:I

.field private mInfoView:Landroid/view/View;

.field private mInfoViewId:I

.field private mLayoutMgr:Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;

.field private mMainView:Landroid/view/View;

.field private mMainViewId:I

.field private final mType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "cardType"    # I

    .prologue
    const/4 v2, 0x0

    .line 129
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 131
    iput p4, p0, Lcom/google/android/recline/widget/BaseCardView;->mType:I

    .line 133
    invoke-virtual {p0}, Lcom/google/android/recline/widget/BaseCardView;->getId()I

    move-result v0

    iput v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mCardViewId:I

    .line 134
    iget v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mCardViewId:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 135
    invoke-static {}, Lcom/google/android/recline/widget/BaseCardView;->generateViewId()I

    move-result v0

    iput v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mCardViewId:I

    .line 136
    iget v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mCardViewId:I

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/BaseCardView;->setId(I)V

    .line 139
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/recline/widget/BaseCardView;->mCardFocused:Z

    .line 140
    iput-boolean v2, p0, Lcom/google/android/recline/widget/BaseCardView;->mCardActive:Z

    .line 142
    iget v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mType:I

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/BaseCardView;->getLayoutManager(I)Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mLayoutMgr:Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;

    .line 144
    invoke-virtual {p0}, Lcom/google/android/recline/widget/BaseCardView;->addViewsToLayout()V

    .line 145
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/recline/widget/BaseCardView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/widget/BaseCardView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 62
    invoke-virtual {p0, p1, p2}, Lcom/google/android/recline/widget/BaseCardView;->measureChildren(II)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/recline/widget/BaseCardView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/widget/BaseCardView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 62
    invoke-virtual {p0, p1, p2}, Lcom/google/android/recline/widget/BaseCardView;->setMeasuredDimension(II)V

    return-void
.end method


# virtual methods
.method protected addViewsToLayout()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 197
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mLayoutMgr:Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;

    if-nez v0, :cond_0

    .line 243
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mLayoutMgr:Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;

    invoke-virtual {v0}, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->hasMainView()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 201
    invoke-virtual {p0}, Lcom/google/android/recline/widget/BaseCardView;->getMainView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mMainView:Landroid/view/View;

    .line 202
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mMainView:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 203
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mMainView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    iput v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mMainViewId:I

    .line 204
    iget v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mMainViewId:I

    if-ne v0, v2, :cond_1

    .line 205
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v0

    iput v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mMainViewId:I

    .line 206
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mMainView:Landroid/view/View;

    iget v1, p0, Lcom/google/android/recline/widget/BaseCardView;->mMainViewId:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 209
    :cond_1
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mMainView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/BaseCardView;->addView(Landroid/view/View;)V

    .line 210
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mLayoutMgr:Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;

    iget-object v1, p0, Lcom/google/android/recline/widget/BaseCardView;->mMainView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->setMainView(Landroid/view/View;)V

    .line 214
    :cond_2
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mLayoutMgr:Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;

    invoke-virtual {v0}, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->hasInfoView()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 215
    invoke-virtual {p0}, Lcom/google/android/recline/widget/BaseCardView;->getInfoView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mInfoView:Landroid/view/View;

    .line 216
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mInfoView:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 217
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mInfoView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    iput v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mInfoViewId:I

    .line 218
    iget v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mInfoViewId:I

    if-ne v0, v2, :cond_3

    .line 219
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v0

    iput v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mInfoViewId:I

    .line 220
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mInfoView:Landroid/view/View;

    iget v1, p0, Lcom/google/android/recline/widget/BaseCardView;->mInfoViewId:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 223
    :cond_3
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mInfoView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/BaseCardView;->addView(Landroid/view/View;)V

    .line 224
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mLayoutMgr:Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;

    iget-object v1, p0, Lcom/google/android/recline/widget/BaseCardView;->mInfoView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->setInfoView(Landroid/view/View;)V

    .line 228
    :cond_4
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mLayoutMgr:Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;

    invoke-virtual {v0}, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->hasExtraView()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 229
    invoke-virtual {p0}, Lcom/google/android/recline/widget/BaseCardView;->getExtraView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mExtraView:Landroid/view/View;

    .line 230
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mExtraView:Landroid/view/View;

    if-eqz v0, :cond_6

    .line 231
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mExtraView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    iput v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mExtraViewId:I

    .line 232
    iget v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mExtraViewId:I

    if-ne v0, v2, :cond_5

    .line 233
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v0

    iput v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mExtraViewId:I

    .line 234
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mExtraView:Landroid/view/View;

    iget v1, p0, Lcom/google/android/recline/widget/BaseCardView;->mExtraViewId:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 237
    :cond_5
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mExtraView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/BaseCardView;->addView(Landroid/view/View;)V

    .line 238
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mLayoutMgr:Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;

    iget-object v1, p0, Lcom/google/android/recline/widget/BaseCardView;->mExtraView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->setExtraView(Landroid/view/View;)V

    .line 242
    :cond_6
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mLayoutMgr:Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;

    invoke-virtual {v0}, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->initCardViews()V

    goto/16 :goto_0
.end method

.method public cardType()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mType:I

    return v0
.end method

.method protected extraView()Landroid/view/View;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mExtraView:Landroid/view/View;

    return-object v0
.end method

.method protected abstract getExtraView()Landroid/view/View;
.end method

.method protected abstract getInfoView()Landroid/view/View;
.end method

.method protected getLayoutManager(I)Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;
    .locals 2
    .param p1, "cardType"    # I

    .prologue
    .line 246
    packed-switch p1, :pswitch_data_0

    .line 261
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "BaseCardView: Invalid Card Type specified."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :pswitch_0
    new-instance v0, Lcom/google/android/recline/widget/CardLayoutInfoUnder;

    invoke-direct {v0, p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnder;-><init>(Lcom/google/android/recline/widget/BaseCardView;)V

    .line 258
    :goto_0
    return-object v0

    .line 251
    :pswitch_1
    new-instance v0, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;

    invoke-direct {v0, p0}, Lcom/google/android/recline/widget/CardLayoutInfoUnderWithExtra;-><init>(Lcom/google/android/recline/widget/BaseCardView;)V

    goto :goto_0

    .line 255
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    .line 258
    :pswitch_3
    new-instance v0, Lcom/google/android/recline/widget/CardLayoutImageOnly;

    invoke-direct {v0, p0}, Lcom/google/android/recline/widget/CardLayoutImageOnly;-><init>(Lcom/google/android/recline/widget/BaseCardView;)V

    goto :goto_0

    .line 246
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected abstract getMainView()Landroid/view/View;
.end method

.method protected infoView()Landroid/view/View;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mInfoView:Landroid/view/View;

    return-object v0
.end method

.method public isCardActive()Z
    .locals 1

    .prologue
    .line 289
    iget-boolean v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mCardActive:Z

    return v0
.end method

.method public isCardFocused()Z
    .locals 1

    .prologue
    .line 300
    iget-boolean v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mCardFocused:Z

    return v0
.end method

.method protected mainView()Landroid/view/View;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mMainView:Landroid/view/View;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mLayoutMgr:Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->onLayout(ZIIII)V

    .line 278
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mLayoutMgr:Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->onMeasure(II)V

    .line 273
    return-void
.end method

.method public setCardActiveState(Z)V
    .locals 1
    .param p1, "active"    # Z

    .prologue
    .line 282
    iget-boolean v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mCardActive:Z

    if-eq v0, p1, :cond_0

    .line 283
    iput-boolean p1, p0, Lcom/google/android/recline/widget/BaseCardView;->mCardActive:Z

    .line 284
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mLayoutMgr:Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;

    invoke-virtual {v0, p1}, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->onCardActiveStateChanged(Z)V

    .line 286
    :cond_0
    return-void
.end method

.method public setCardFocusedState(Z)V
    .locals 1
    .param p1, "focused"    # Z

    .prologue
    .line 293
    iget-boolean v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mCardFocused:Z

    if-eq v0, p1, :cond_0

    .line 294
    iput-boolean p1, p0, Lcom/google/android/recline/widget/BaseCardView;->mCardFocused:Z

    .line 295
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mLayoutMgr:Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;

    invoke-virtual {v0, p1}, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->onCardFocusedStateChanged(Z)V

    .line 297
    :cond_0
    return-void
.end method

.method public setDelayFocusAnimation(Z)V
    .locals 1
    .param p1, "delay"    # Z

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/recline/widget/BaseCardView;->mLayoutMgr:Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;

    invoke-virtual {v0, p1}, Lcom/google/android/recline/widget/BaseCardView$CardLayoutManager;->setDelayFocusAnimation(Z)V

    .line 149
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .locals 1

    .prologue
    .line 267
    const/4 v0, 0x0

    return v0
.end method
