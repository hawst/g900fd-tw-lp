.class Lcom/google/android/recline/app/PlaybackControlGlue$2;
.super Ljava/lang/Object;
.source "PlaybackControlGlue.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/OnItemViewClickedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/app/PlaybackControlGlue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/recline/app/PlaybackControlGlue;


# direct methods
.method constructor <init>(Lcom/google/android/recline/app/PlaybackControlGlue;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/google/android/recline/app/PlaybackControlGlue$2;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClicked(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V
    .locals 3
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "object"    # Ljava/lang/Object;
    .param p3, "viewHolder2"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p4, "row"    # Landroid/support/v17/leanback/widget/Row;

    .prologue
    .line 183
    const/4 v0, 0x0

    .line 184
    .local v0, "handled":Z
    instance-of v1, p2, Landroid/support/v17/leanback/widget/Action;

    if-eqz v1, :cond_0

    .line 185
    iget-object v2, p0, Lcom/google/android/recline/app/PlaybackControlGlue$2;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    move-object v1, p2

    check-cast v1, Landroid/support/v17/leanback/widget/Action;

    # invokes: Lcom/google/android/recline/app/PlaybackControlGlue;->handleActionClicked(Landroid/support/v17/leanback/widget/Action;)Z
    invoke-static {v2, v1}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$100(Lcom/google/android/recline/app/PlaybackControlGlue;Landroid/support/v17/leanback/widget/Action;)Z

    move-result v0

    .line 187
    :cond_0
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue$2;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    # getter for: Lcom/google/android/recline/app/PlaybackControlGlue;->mExternalOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;
    invoke-static {v1}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$200(Lcom/google/android/recline/app/PlaybackControlGlue;)Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 188
    iget-object v1, p0, Lcom/google/android/recline/app/PlaybackControlGlue$2;->this$0:Lcom/google/android/recline/app/PlaybackControlGlue;

    # getter for: Lcom/google/android/recline/app/PlaybackControlGlue;->mExternalOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;
    invoke-static {v1}, Lcom/google/android/recline/app/PlaybackControlGlue;->access$200(Lcom/google/android/recline/app/PlaybackControlGlue;)Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    move-result-object v1

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/support/v17/leanback/widget/OnItemViewClickedListener;->onItemClicked(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V

    .line 191
    :cond_1
    return-void
.end method
