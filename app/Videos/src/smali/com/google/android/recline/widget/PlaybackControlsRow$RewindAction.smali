.class public Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;
.super Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;
.source "PlaybackControlsRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/widget/PlaybackControlsRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RewindAction"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 249
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;-><init>(Landroid/content/Context;I)V

    .line 250
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "numSpeeds"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 258
    sget v5, Lcom/google/android/recline/R$id;->lb_control_fast_rewind:I

    invoke-direct {p0, v5}, Lcom/google/android/recline/widget/PlaybackControlsRow$MultiAction;-><init>(I)V

    .line 260
    if-ge p2, v10, :cond_0

    .line 261
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "numSpeeds must be > 0"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 263
    :cond_0
    new-array v0, p2, [Landroid/graphics/drawable/Drawable;

    .line 264
    .local v0, "drawables":[Landroid/graphics/drawable/Drawable;
    sget v5, Lcom/google/android/recline/R$styleable;->lbPlaybackControlsActionIcons_rewind:I

    # invokes: Lcom/google/android/recline/widget/PlaybackControlsRow;->getStyledDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    invoke-static {p1, v5}, Lcom/google/android/recline/widget/PlaybackControlsRow;->access$000(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aput-object v5, v0, v9

    .line 266
    invoke-virtual {p0, v0}, Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;->setDrawables([Landroid/graphics/drawable/Drawable;)V

    .line 268
    invoke-virtual {p0}, Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;->getActionCount()I

    move-result v5

    new-array v2, v5, [Ljava/lang/String;

    .line 269
    .local v2, "labels":[Ljava/lang/String;
    sget v5, Lcom/google/android/recline/R$string;->lb_playback_controls_rewind:I

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v9

    .line 271
    invoke-virtual {p0}, Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;->getActionCount()I

    move-result v5

    new-array v3, v5, [Ljava/lang/String;

    .line 272
    .local v3, "labels2":[Ljava/lang/String;
    aget-object v5, v2, v9

    aput-object v5, v3, v9

    .line 274
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    if-ge v1, p2, :cond_1

    .line 275
    add-int/lit8 v4, v1, 0x1

    .line 276
    .local v4, "multiplier":I
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/recline/R$string;->recline_control_display_rewind_multiplier:I

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    aput-object v5, v2, v1

    .line 278
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/recline/R$string;->recline_playback_controls_rewind_multiplier:I

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    .line 274
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 281
    .end local v4    # "multiplier":I
    :cond_1
    invoke-virtual {p0, v2}, Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;->setLabels([Ljava/lang/String;)V

    .line 282
    invoke-virtual {p0, v3}, Lcom/google/android/recline/widget/PlaybackControlsRow$RewindAction;->setSecondaryLabels([Ljava/lang/String;)V

    .line 283
    return-void
.end method
