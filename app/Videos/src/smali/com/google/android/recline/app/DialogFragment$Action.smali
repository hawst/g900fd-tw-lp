.class public Lcom/google/android/recline/app/DialogFragment$Action;
.super Ljava/lang/Object;
.source "DialogFragment.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/recline/app/DialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/recline/app/DialogFragment$Action$Builder;,
        Lcom/google/android/recline/app/DialogFragment$Action$OnFocusListener;,
        Lcom/google/android/recline/app/DialogFragment$Action$Listener;
    }
.end annotation


# static fields
.field public static CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/recline/app/DialogFragment$Action;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCheckSetId:I

.field private mChecked:Z

.field private mDescription:Ljava/lang/String;

.field private mDrawableResource:I

.field private mEnabled:Z

.field private mHasNext:Z

.field private mIconUri:Landroid/net/Uri;

.field private mInfoOnly:Z

.field private mIntent:Landroid/content/Intent;

.field private mKey:Ljava/lang/String;

.field private mMultilineDescription:Z

.field private mResourcePackageName:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1194
    new-instance v0, Lcom/google/android/recline/app/DialogFragment$Action$1;

    invoke-direct {v0}, Lcom/google/android/recline/app/DialogFragment$Action$1;-><init>()V

    sput-object v0, Lcom/google/android/recline/app/DialogFragment$Action;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1078
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1079
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/recline/app/DialogFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/recline/app/DialogFragment$1;

    .prologue
    .line 929
    invoke-direct {p0}, Lcom/google/android/recline/app/DialogFragment$Action;-><init>()V

    return-void
.end method

.method static synthetic access$1002(Lcom/google/android/recline/app/DialogFragment$Action;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogFragment$Action;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 929
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mIconUri:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/google/android/recline/app/DialogFragment$Action;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogFragment$Action;
    .param p1, "x1"    # Z

    .prologue
    .line 929
    iput-boolean p1, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mChecked:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/android/recline/app/DialogFragment$Action;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogFragment$Action;
    .param p1, "x1"    # Z

    .prologue
    .line 929
    iput-boolean p1, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mMultilineDescription:Z

    return p1
.end method

.method static synthetic access$1302(Lcom/google/android/recline/app/DialogFragment$Action;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogFragment$Action;
    .param p1, "x1"    # Z

    .prologue
    .line 929
    iput-boolean p1, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mHasNext:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/google/android/recline/app/DialogFragment$Action;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogFragment$Action;
    .param p1, "x1"    # Z

    .prologue
    .line 929
    iput-boolean p1, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mInfoOnly:Z

    return p1
.end method

.method static synthetic access$1502(Lcom/google/android/recline/app/DialogFragment$Action;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogFragment$Action;
    .param p1, "x1"    # I

    .prologue
    .line 929
    iput p1, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mCheckSetId:I

    return p1
.end method

.method static synthetic access$1602(Lcom/google/android/recline/app/DialogFragment$Action;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogFragment$Action;
    .param p1, "x1"    # Z

    .prologue
    .line 929
    iput-boolean p1, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mEnabled:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/android/recline/app/DialogFragment$Action;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogFragment$Action;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 929
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mKey:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/android/recline/app/DialogFragment$Action;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogFragment$Action;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 929
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mTitle:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/android/recline/app/DialogFragment$Action;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogFragment$Action;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 929
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mDescription:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/android/recline/app/DialogFragment$Action;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogFragment$Action;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 929
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mIntent:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/android/recline/app/DialogFragment$Action;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogFragment$Action;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 929
    iput-object p1, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mResourcePackageName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/android/recline/app/DialogFragment$Action;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/recline/app/DialogFragment$Action;
    .param p1, "x1"    # I

    .prologue
    .line 929
    iput p1, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mDrawableResource:I

    return p1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 1221
    const/4 v0, 0x0

    return v0
.end method

.method public getCheckSetId()I
    .locals 1

    .prologue
    .line 1124
    iget v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mCheckSetId:I

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1090
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getIconUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1106
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mIconUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getIndicator(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x5

    .line 1171
    iget v3, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mDrawableResource:I

    if-nez v3, :cond_1

    .line 1172
    const/4 v1, 0x0

    .line 1191
    :cond_0
    :goto_0
    return-object v1

    .line 1174
    :cond_1
    iget-object v3, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mResourcePackageName:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 1175
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mDrawableResource:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0

    .line 1178
    :cond_2
    const/4 v1, 0x0

    .line 1180
    .local v1, "icon":Landroid/graphics/drawable/Drawable;
    :try_start_0
    iget-object v3, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mResourcePackageName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v2

    .line 1181
    .local v2, "packageContext":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mDrawableResource:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    goto :goto_0

    .line 1182
    .end local v2    # "packageContext":Landroid/content/Context;
    :catch_0
    move-exception v0

    .line 1183
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "Action"

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1184
    const-string v3, "Action"

    const-string v4, "No icon for this action."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1186
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 1187
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v3, "Action"

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1188
    const-string v3, "Action"

    const-string v4, "No icon for this action."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1082
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mKey:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public hasMultilineDescription()Z
    .locals 1

    .prologue
    .line 1128
    iget-boolean v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mMultilineDescription:Z

    return v0
.end method

.method public hasNext()Z
    .locals 1

    .prologue
    .line 1149
    iget-boolean v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mHasNext:Z

    return v0
.end method

.method public infoOnly()Z
    .locals 1

    .prologue
    .line 1159
    iget-boolean v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mInfoOnly:Z

    return v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 1098
    iget-boolean v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mChecked:Z

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 1132
    iget-boolean v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mEnabled:Z

    return v0
.end method

.method public setChecked(Z)V
    .locals 0
    .param p1, "checked"    # Z

    .prologue
    .line 1136
    iput-boolean p1, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mChecked:Z

    .line 1137
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1226
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1227
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1228
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1229
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mIntent:Landroid/content/Intent;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1230
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mResourcePackageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1231
    iget v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mDrawableResource:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1232
    iget-object v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mIconUri:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1233
    iget-boolean v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mChecked:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1234
    iget-boolean v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mMultilineDescription:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1235
    iget v0, p0, Lcom/google/android/recline/app/DialogFragment$Action;->mCheckSetId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1236
    return-void

    :cond_0
    move v0, v2

    .line 1233
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1234
    goto :goto_1
.end method
