.class Lcom/google/android/recline/widget/LeanbackCardView$2;
.super Ljava/lang/Object;
.source "LeanbackCardView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/recline/widget/LeanbackCardView;->animateInfoOffset(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/recline/widget/LeanbackCardView;


# direct methods
.method constructor <init>(Lcom/google/android/recline/widget/LeanbackCardView;)V
    .locals 0

    .prologue
    .line 604
    iput-object p1, p0, Lcom/google/android/recline/widget/LeanbackCardView$2;->this$0:Lcom/google/android/recline/widget/LeanbackCardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 611
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView$2;->this$0:Lcom/google/android/recline/widget/LeanbackCardView;

    # getter for: Lcom/google/android/recline/widget/LeanbackCardView;->mInfoOffset:F
    invoke-static {v1}, Lcom/google/android/recline/widget/LeanbackCardView;->access$100(Lcom/google/android/recline/widget/LeanbackCardView;)F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    .line 612
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView$2;->this$0:Lcom/google/android/recline/widget/LeanbackCardView;

    # getter for: Lcom/google/android/recline/widget/LeanbackCardView;->mExtraViewList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/recline/widget/LeanbackCardView;->access$200(Lcom/google/android/recline/widget/LeanbackCardView;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 613
    iget-object v1, p0, Lcom/google/android/recline/widget/LeanbackCardView$2;->this$0:Lcom/google/android/recline/widget/LeanbackCardView;

    # getter for: Lcom/google/android/recline/widget/LeanbackCardView;->mExtraViewList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/recline/widget/LeanbackCardView;->access$200(Lcom/google/android/recline/widget/LeanbackCardView;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 612
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 616
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 620
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 607
    return-void
.end method
