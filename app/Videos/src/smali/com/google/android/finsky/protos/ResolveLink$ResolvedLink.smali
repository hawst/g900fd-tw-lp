.class public final Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ResolveLink.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ResolveLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ResolvedLink"
.end annotation


# instance fields
.field public backend:I

.field public browseUrl:Ljava/lang/String;

.field public detailsUrl:Ljava/lang/String;

.field public directPurchase:Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;

.field public docid:Lcom/google/android/finsky/protos/Common$Docid;

.field public hasBackend:Z

.field public hasBrowseUrl:Z

.field public hasDetailsUrl:Z

.field public hasHomeUrl:Z

.field public hasMyAccountUrl:Z

.field public hasQuery:Z

.field public hasSearchUrl:Z

.field public hasServerLogsCookie:Z

.field public hasWishlistUrl:Z

.field public homeUrl:Ljava/lang/String;

.field public myAccountUrl:Ljava/lang/String;

.field public query:Ljava/lang/String;

.field public redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

.field public searchUrl:Ljava/lang/String;

.field public serverLogsCookie:[B

.field public wishlistUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->clear()Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    .line 72
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 75
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->detailsUrl:Ljava/lang/String;

    .line 76
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasDetailsUrl:Z

    .line 77
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->browseUrl:Ljava/lang/String;

    .line 78
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasBrowseUrl:Z

    .line 79
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->searchUrl:Ljava/lang/String;

    .line 80
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasSearchUrl:Z

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->wishlistUrl:Ljava/lang/String;

    .line 82
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasWishlistUrl:Z

    .line 83
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->myAccountUrl:Ljava/lang/String;

    .line 84
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasMyAccountUrl:Z

    .line 85
    iput-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->directPurchase:Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;

    .line 86
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->homeUrl:Ljava/lang/String;

    .line 87
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasHomeUrl:Z

    .line 88
    iput-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    .line 89
    iput-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 90
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->serverLogsCookie:[B

    .line 91
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasServerLogsCookie:Z

    .line 92
    iput v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->backend:I

    .line 93
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasBackend:Z

    .line 94
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->query:Ljava/lang/String;

    .line 95
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasQuery:Z

    .line 96
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->cachedSize:I

    .line 97
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 144
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 145
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasDetailsUrl:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->detailsUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 146
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->detailsUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasBrowseUrl:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->browseUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 150
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->browseUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasSearchUrl:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->searchUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 154
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->searchUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 157
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->directPurchase:Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;

    if-eqz v1, :cond_6

    .line 158
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->directPurchase:Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 161
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasHomeUrl:Z

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->homeUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 162
    :cond_7
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->homeUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    if-eqz v1, :cond_9

    .line 166
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 169
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasServerLogsCookie:Z

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->serverLogsCookie:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_b

    .line 170
    :cond_a
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->serverLogsCookie:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 173
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v1, :cond_c

    .line 174
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 177
    :cond_c
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasWishlistUrl:Z

    if-nez v1, :cond_d

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->wishlistUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 178
    :cond_d
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->wishlistUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 181
    :cond_e
    iget v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->backend:I

    if-nez v1, :cond_f

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasBackend:Z

    if-eqz v1, :cond_10

    .line 182
    :cond_f
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->backend:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 185
    :cond_10
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasQuery:Z

    if-nez v1, :cond_11

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->query:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 186
    :cond_11
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->query:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 189
    :cond_12
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasMyAccountUrl:Z

    if-nez v1, :cond_13

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->myAccountUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 190
    :cond_13
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->myAccountUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 193
    :cond_14
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 201
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 202
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 206
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 207
    :sswitch_0
    return-object p0

    .line 212
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->detailsUrl:Ljava/lang/String;

    .line 213
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasDetailsUrl:Z

    goto :goto_0

    .line 217
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->browseUrl:Ljava/lang/String;

    .line 218
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasBrowseUrl:Z

    goto :goto_0

    .line 222
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->searchUrl:Ljava/lang/String;

    .line 223
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasSearchUrl:Z

    goto :goto_0

    .line 227
    :sswitch_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->directPurchase:Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;

    if-nez v2, :cond_1

    .line 228
    new-instance v2, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->directPurchase:Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;

    .line 230
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->directPurchase:Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 234
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->homeUrl:Ljava/lang/String;

    .line 235
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasHomeUrl:Z

    goto :goto_0

    .line 239
    :sswitch_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    if-nez v2, :cond_2

    .line 240
    new-instance v2, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    .line 242
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 246
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->serverLogsCookie:[B

    .line 247
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasServerLogsCookie:Z

    goto :goto_0

    .line 251
    :sswitch_8
    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v2, :cond_3

    .line 252
    new-instance v2, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 254
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 258
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->wishlistUrl:Ljava/lang/String;

    .line 259
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasWishlistUrl:Z

    goto :goto_0

    .line 263
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 264
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 276
    :pswitch_1
    iput v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->backend:I

    .line 277
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasBackend:Z

    goto/16 :goto_0

    .line 283
    .end local v1    # "value":I
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->query:Ljava/lang/String;

    .line 284
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasQuery:Z

    goto/16 :goto_0

    .line 288
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->myAccountUrl:Ljava/lang/String;

    .line 289
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasMyAccountUrl:Z

    goto/16 :goto_0

    .line 202
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch

    .line 264
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasDetailsUrl:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->detailsUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 104
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->detailsUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 106
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasBrowseUrl:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->browseUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 107
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->browseUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 109
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasSearchUrl:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->searchUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 110
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->searchUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 112
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->directPurchase:Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;

    if-eqz v0, :cond_6

    .line 113
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->directPurchase:Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 115
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasHomeUrl:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->homeUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 116
    :cond_7
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->homeUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 118
    :cond_8
    iget-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    if-eqz v0, :cond_9

    .line 119
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 121
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasServerLogsCookie:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->serverLogsCookie:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_b

    .line 122
    :cond_a
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->serverLogsCookie:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 124
    :cond_b
    iget-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v0, :cond_c

    .line 125
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 127
    :cond_c
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasWishlistUrl:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->wishlistUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 128
    :cond_d
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->wishlistUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 130
    :cond_e
    iget v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->backend:I

    if-nez v0, :cond_f

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasBackend:Z

    if-eqz v0, :cond_10

    .line 131
    :cond_f
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->backend:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 133
    :cond_10
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasQuery:Z

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->query:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 134
    :cond_11
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->query:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 136
    :cond_12
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->hasMyAccountUrl:Z

    if-nez v0, :cond_13

    iget-object v0, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->myAccountUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 137
    :cond_13
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->myAccountUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 139
    :cond_14
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 140
    return-void
.end method
