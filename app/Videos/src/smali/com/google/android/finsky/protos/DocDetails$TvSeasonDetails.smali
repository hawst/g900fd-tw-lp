.class public final Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TvSeasonDetails"
.end annotation


# instance fields
.field public broadcaster:Ljava/lang/String;

.field public episodeCount:I

.field public expectedEpisodeCount:I

.field public hasBroadcaster:Z

.field public hasEpisodeCount:Z

.field public hasExpectedEpisodeCount:Z

.field public hasParentDetailsUrl:Z

.field public hasReleaseDate:Z

.field public hasSeasonIndex:Z

.field public parentDetailsUrl:Ljava/lang/String;

.field public releaseDate:Ljava/lang/String;

.field public seasonIndex:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4278
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4279
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->clear()Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;

    .line 4280
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4283
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->parentDetailsUrl:Ljava/lang/String;

    .line 4284
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasParentDetailsUrl:Z

    .line 4285
    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->seasonIndex:I

    .line 4286
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasSeasonIndex:Z

    .line 4287
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->releaseDate:Ljava/lang/String;

    .line 4288
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasReleaseDate:Z

    .line 4289
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->broadcaster:Ljava/lang/String;

    .line 4290
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasBroadcaster:Z

    .line 4291
    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->episodeCount:I

    .line 4292
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasEpisodeCount:Z

    .line 4293
    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->expectedEpisodeCount:I

    .line 4294
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasExpectedEpisodeCount:Z

    .line 4295
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->cachedSize:I

    .line 4296
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 4325
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4326
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasParentDetailsUrl:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->parentDetailsUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 4327
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->parentDetailsUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4330
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasSeasonIndex:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->seasonIndex:I

    if-eqz v1, :cond_3

    .line 4331
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->seasonIndex:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4334
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasReleaseDate:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->releaseDate:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 4335
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->releaseDate:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4338
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasBroadcaster:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->broadcaster:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 4339
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->broadcaster:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4342
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasEpisodeCount:Z

    if-nez v1, :cond_8

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->episodeCount:I

    if-eqz v1, :cond_9

    .line 4343
    :cond_8
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->episodeCount:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4346
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasExpectedEpisodeCount:Z

    if-nez v1, :cond_a

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->expectedEpisodeCount:I

    if-eqz v1, :cond_b

    .line 4347
    :cond_a
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->expectedEpisodeCount:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4350
    :cond_b
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 4358
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4359
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4363
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4364
    :sswitch_0
    return-object p0

    .line 4369
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->parentDetailsUrl:Ljava/lang/String;

    .line 4370
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasParentDetailsUrl:Z

    goto :goto_0

    .line 4374
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->seasonIndex:I

    .line 4375
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasSeasonIndex:Z

    goto :goto_0

    .line 4379
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->releaseDate:Ljava/lang/String;

    .line 4380
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasReleaseDate:Z

    goto :goto_0

    .line 4384
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->broadcaster:Ljava/lang/String;

    .line 4385
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasBroadcaster:Z

    goto :goto_0

    .line 4389
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->episodeCount:I

    .line 4390
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasEpisodeCount:Z

    goto :goto_0

    .line 4394
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->expectedEpisodeCount:I

    .line 4395
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasExpectedEpisodeCount:Z

    goto :goto_0

    .line 4359
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4237
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4302
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasParentDetailsUrl:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->parentDetailsUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4303
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->parentDetailsUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4305
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasSeasonIndex:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->seasonIndex:I

    if-eqz v0, :cond_3

    .line 4306
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->seasonIndex:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4308
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasReleaseDate:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->releaseDate:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 4309
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->releaseDate:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4311
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasBroadcaster:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->broadcaster:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 4312
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->broadcaster:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4314
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasEpisodeCount:Z

    if-nez v0, :cond_8

    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->episodeCount:I

    if-eqz v0, :cond_9

    .line 4315
    :cond_8
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->episodeCount:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4317
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasExpectedEpisodeCount:Z

    if-nez v0, :cond_a

    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->expectedEpisodeCount:I

    if-eqz v0, :cond_b

    .line 4318
    :cond_a
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->expectedEpisodeCount:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4320
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4321
    return-void
.end method
