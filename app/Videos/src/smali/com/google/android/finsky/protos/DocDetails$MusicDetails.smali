.class public final Lcom/google/android/finsky/protos/DocDetails$MusicDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MusicDetails"
.end annotation


# instance fields
.field public artist:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

.field public censoring:I

.field public durationSec:I

.field public genre:[Ljava/lang/String;

.field public hasCensoring:Z

.field public hasDurationSec:Z

.field public hasLabel:Z

.field public hasOriginalReleaseDate:Z

.field public hasReleaseDate:Z

.field public label:Ljava/lang/String;

.field public originalReleaseDate:Ljava/lang/String;

.field public releaseDate:Ljava/lang/String;

.field public releaseType:[I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1801
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1802
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->clear()Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    .line 1803
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$MusicDetails;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1806
    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->censoring:I

    .line 1807
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasCensoring:Z

    .line 1808
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    .line 1809
    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->durationSec:I

    .line 1810
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasDurationSec:Z

    .line 1811
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->originalReleaseDate:Ljava/lang/String;

    .line 1812
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasOriginalReleaseDate:Z

    .line 1813
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseDate:Ljava/lang/String;

    .line 1814
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasReleaseDate:Z

    .line 1815
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->label:Ljava/lang/String;

    .line 1816
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasLabel:Z

    .line 1817
    invoke-static {}, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->emptyArray()[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->artist:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    .line 1818
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->genre:[Ljava/lang/String;

    .line 1819
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->cachedSize:I

    .line 1820
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 1867
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 1868
    .local v4, "size":I
    iget v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->censoring:I

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasCensoring:Z

    if-eqz v5, :cond_1

    .line 1869
    :cond_0
    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->censoring:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 1872
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasDurationSec:Z

    if-nez v5, :cond_2

    iget v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->durationSec:I

    if-eqz v5, :cond_3

    .line 1873
    :cond_2
    const/4 v5, 0x2

    iget v6, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->durationSec:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 1876
    :cond_3
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasOriginalReleaseDate:Z

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->originalReleaseDate:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 1877
    :cond_4
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->originalReleaseDate:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1880
    :cond_5
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasLabel:Z

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->label:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 1881
    :cond_6
    const/4 v5, 0x4

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->label:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1884
    :cond_7
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->artist:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->artist:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    array-length v5, v5

    if-lez v5, :cond_9

    .line 1885
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->artist:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    array-length v5, v5

    if-ge v3, v5, :cond_9

    .line 1886
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->artist:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    aget-object v2, v5, v3

    .line 1887
    .local v2, "element":Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    if-eqz v2, :cond_8

    .line 1888
    const/4 v5, 0x5

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1885
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1893
    .end local v2    # "element":Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    .end local v3    # "i":I
    :cond_9
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->genre:[Ljava/lang/String;

    if-eqz v5, :cond_c

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->genre:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_c

    .line 1894
    const/4 v0, 0x0

    .line 1895
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 1896
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->genre:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_b

    .line 1897
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->genre:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 1898
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_a

    .line 1899
    add-int/lit8 v0, v0, 0x1

    .line 1900
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 1896
    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1904
    .end local v2    # "element":Ljava/lang/String;
    :cond_b
    add-int/2addr v4, v1

    .line 1905
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 1907
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_c
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasReleaseDate:Z

    if-nez v5, :cond_d

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseDate:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_e

    .line 1908
    :cond_d
    const/4 v5, 0x7

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseDate:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 1911
    :cond_e
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    if-eqz v5, :cond_10

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    array-length v5, v5

    if-lez v5, :cond_10

    .line 1912
    const/4 v1, 0x0

    .line 1913
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    array-length v5, v5

    if-ge v3, v5, :cond_f

    .line 1914
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    aget v2, v5, v3

    .line 1915
    .local v2, "element":I
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v1, v5

    .line 1913
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1918
    .end local v2    # "element":I
    :cond_f
    add-int/2addr v4, v1

    .line 1919
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    array-length v5, v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v4, v5

    .line 1921
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_10
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$MusicDetails;
    .locals 17
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1929
    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v9

    .line 1930
    .local v9, "tag":I
    sparse-switch v9, :sswitch_data_0

    .line 1934
    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v14

    if-nez v14, :cond_0

    .line 1935
    :sswitch_0
    return-object p0

    .line 1940
    :sswitch_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 1941
    .local v13, "value":I
    packed-switch v13, :pswitch_data_0

    goto :goto_0

    .line 1945
    :pswitch_0
    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->censoring:I

    .line 1946
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasCensoring:Z

    goto :goto_0

    .line 1952
    .end local v13    # "value":I
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->durationSec:I

    .line 1953
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasDurationSec:Z

    goto :goto_0

    .line 1957
    :sswitch_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->originalReleaseDate:Ljava/lang/String;

    .line 1958
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasOriginalReleaseDate:Z

    goto :goto_0

    .line 1962
    :sswitch_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->label:Ljava/lang/String;

    .line 1963
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasLabel:Z

    goto :goto_0

    .line 1967
    :sswitch_5
    const/16 v14, 0x2a

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v1

    .line 1969
    .local v1, "arrayLength":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->artist:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    if-nez v14, :cond_2

    const/4 v3, 0x0

    .line 1970
    .local v3, "i":I
    :goto_1
    add-int v14, v3, v1

    new-array v7, v14, [Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    .line 1972
    .local v7, "newArray":[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    if-eqz v3, :cond_1

    .line 1973
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->artist:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1975
    :cond_1
    :goto_2
    array-length v14, v7

    add-int/lit8 v14, v14, -0x1

    if-ge v3, v14, :cond_3

    .line 1976
    new-instance v14, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    invoke-direct {v14}, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;-><init>()V

    aput-object v14, v7, v3

    .line 1977
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1978
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1975
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1969
    .end local v3    # "i":I
    .end local v7    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->artist:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    array-length v3, v14

    goto :goto_1

    .line 1981
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    :cond_3
    new-instance v14, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    invoke-direct {v14}, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;-><init>()V

    aput-object v14, v7, v3

    .line 1982
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1983
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->artist:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    goto/16 :goto_0

    .line 1987
    .end local v1    # "arrayLength":I
    .end local v3    # "i":I
    .end local v7    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    :sswitch_6
    const/16 v14, 0x32

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v1

    .line 1989
    .restart local v1    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->genre:[Ljava/lang/String;

    if-nez v14, :cond_5

    const/4 v3, 0x0

    .line 1990
    .restart local v3    # "i":I
    :goto_3
    add-int v14, v3, v1

    new-array v7, v14, [Ljava/lang/String;

    .line 1991
    .local v7, "newArray":[Ljava/lang/String;
    if-eqz v3, :cond_4

    .line 1992
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->genre:[Ljava/lang/String;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1994
    :cond_4
    :goto_4
    array-length v14, v7

    add-int/lit8 v14, v14, -0x1

    if-ge v3, v14, :cond_6

    .line 1995
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v7, v3

    .line 1996
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1994
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 1989
    .end local v3    # "i":I
    .end local v7    # "newArray":[Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->genre:[Ljava/lang/String;

    array-length v3, v14

    goto :goto_3

    .line 1999
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[Ljava/lang/String;
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v7, v3

    .line 2000
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->genre:[Ljava/lang/String;

    goto/16 :goto_0

    .line 2004
    .end local v1    # "arrayLength":I
    .end local v3    # "i":I
    .end local v7    # "newArray":[Ljava/lang/String;
    :sswitch_7
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseDate:Ljava/lang/String;

    .line 2005
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasReleaseDate:Z

    goto/16 :goto_0

    .line 2009
    :sswitch_8
    const/16 v14, 0x40

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v5

    .line 2011
    .local v5, "length":I
    new-array v12, v5, [I

    .line 2012
    .local v12, "validValues":[I
    const/4 v10, 0x0

    .line 2013
    .local v10, "validCount":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    move v11, v10

    .end local v10    # "validCount":I
    .local v11, "validCount":I
    :goto_5
    if-ge v3, v5, :cond_8

    .line 2014
    if-eqz v3, :cond_7

    .line 2015
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2017
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 2018
    .restart local v13    # "value":I
    packed-switch v13, :pswitch_data_1

    move v10, v11

    .line 2013
    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    :goto_6
    add-int/lit8 v3, v3, 0x1

    move v11, v10

    .end local v10    # "validCount":I
    .restart local v11    # "validCount":I
    goto :goto_5

    .line 2022
    :pswitch_1
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    aput v13, v12, v11

    goto :goto_6

    .line 2026
    .end local v10    # "validCount":I
    .end local v13    # "value":I
    .restart local v11    # "validCount":I
    :cond_8
    if-eqz v11, :cond_0

    .line 2027
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    if-nez v14, :cond_9

    const/4 v3, 0x0

    .line 2028
    :goto_7
    if-nez v3, :cond_a

    array-length v14, v12

    if-ne v11, v14, :cond_a

    .line 2029
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    goto/16 :goto_0

    .line 2027
    :cond_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    array-length v3, v14

    goto :goto_7

    .line 2031
    :cond_a
    add-int v14, v3, v11

    new-array v7, v14, [I

    .line 2032
    .local v7, "newArray":[I
    if-eqz v3, :cond_b

    .line 2033
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2035
    :cond_b
    const/4 v14, 0x0

    invoke-static {v12, v14, v7, v3, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2036
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    goto/16 :goto_0

    .line 2042
    .end local v3    # "i":I
    .end local v5    # "length":I
    .end local v7    # "newArray":[I
    .end local v11    # "validCount":I
    .end local v12    # "validValues":[I
    :sswitch_9
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 2043
    .local v2, "bytes":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v6

    .line 2045
    .local v6, "limit":I
    const/4 v1, 0x0

    .line 2046
    .restart local v1    # "arrayLength":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v8

    .line 2047
    .local v8, "startPos":I
    :goto_8
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_c

    .line 2048
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    packed-switch v14, :pswitch_data_2

    goto :goto_8

    .line 2052
    :pswitch_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 2056
    :cond_c
    if-eqz v1, :cond_10

    .line 2057
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 2058
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    if-nez v14, :cond_e

    const/4 v3, 0x0

    .line 2059
    .restart local v3    # "i":I
    :goto_9
    add-int v14, v3, v1

    new-array v7, v14, [I

    .line 2060
    .restart local v7    # "newArray":[I
    if-eqz v3, :cond_d

    .line 2061
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2063
    :cond_d
    :goto_a
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_f

    .line 2064
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 2065
    .restart local v13    # "value":I
    packed-switch v13, :pswitch_data_3

    goto :goto_a

    .line 2069
    :pswitch_3
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    aput v13, v7, v3

    move v3, v4

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_a

    .line 2058
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    .end local v13    # "value":I
    :cond_e
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    array-length v3, v14

    goto :goto_9

    .line 2073
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[I
    :cond_f
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    .line 2075
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    :cond_10
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 1930
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x42 -> :sswitch_9
    .end sparse-switch

    .line 1941
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 2018
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 2048
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 2065
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1755
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1826
    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->censoring:I

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasCensoring:Z

    if-eqz v2, :cond_1

    .line 1827
    :cond_0
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->censoring:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1829
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasDurationSec:Z

    if-nez v2, :cond_2

    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->durationSec:I

    if-eqz v2, :cond_3

    .line 1830
    :cond_2
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->durationSec:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1832
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasOriginalReleaseDate:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->originalReleaseDate:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1833
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->originalReleaseDate:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1835
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasLabel:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->label:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1836
    :cond_6
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->label:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1838
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->artist:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->artist:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 1839
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->artist:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 1840
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->artist:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    aget-object v0, v2, v1

    .line 1841
    .local v0, "element":Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    if-eqz v0, :cond_8

    .line 1842
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1839
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1846
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    .end local v1    # "i":I
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->genre:[Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->genre:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_b

    .line 1847
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->genre:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_b

    .line 1848
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->genre:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 1849
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_a

    .line 1850
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1847
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1854
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasReleaseDate:Z

    if-nez v2, :cond_c

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseDate:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 1855
    :cond_c
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseDate:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1857
    :cond_d
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    array-length v2, v2

    if-lez v2, :cond_e

    .line 1858
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    array-length v2, v2

    if-ge v1, v2, :cond_e

    .line 1859
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    aget v3, v3, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1858
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1862
    .end local v1    # "i":I
    :cond_e
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1863
    return-void
.end method
