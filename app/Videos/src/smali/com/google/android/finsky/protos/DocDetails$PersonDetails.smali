.class public final Lcom/google/android/finsky/protos/DocDetails$PersonDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PersonDetails"
.end annotation


# instance fields
.field public hasPersonIsRequester:Z

.field public personIsRequester:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4558
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4559
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;->clear()Lcom/google/android/finsky/protos/DocDetails$PersonDetails;

    .line 4560
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$PersonDetails;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4563
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;->personIsRequester:Z

    .line 4564
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;->hasPersonIsRequester:Z

    .line 4565
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;->cachedSize:I

    .line 4566
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 4580
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4581
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;->hasPersonIsRequester:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;->personIsRequester:Z

    if-eqz v1, :cond_1

    .line 4582
    :cond_0
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;->personIsRequester:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4585
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$PersonDetails;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4593
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4594
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4598
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4599
    :sswitch_0
    return-object p0

    .line 4604
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;->personIsRequester:Z

    .line 4605
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;->hasPersonIsRequester:Z

    goto :goto_0

    .line 4594
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4537
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$PersonDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4572
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;->hasPersonIsRequester:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;->personIsRequester:Z

    if-eqz v0, :cond_1

    .line 4573
    :cond_0
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;->personIsRequester:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 4575
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4576
    return-void
.end method
