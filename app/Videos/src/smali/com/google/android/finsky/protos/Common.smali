.class public interface abstract Lcom/google/android/finsky/protos/Common;
.super Ljava/lang/Object;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/Common$Attribution;,
        Lcom/google/android/finsky/protos/Common$SignedData;,
        Lcom/google/android/finsky/protos/Common$Install;,
        Lcom/google/android/finsky/protos/Common$VoucherId;,
        Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;,
        Lcom/google/android/finsky/protos/Common$LicenseTerms;,
        Lcom/google/android/finsky/protos/Common$LicensedDocumentInfo;,
        Lcom/google/android/finsky/protos/Common$GroupLicenseKey;,
        Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;,
        Lcom/google/android/finsky/protos/Common$SubscriptionTerms;,
        Lcom/google/android/finsky/protos/Common$SeasonalSubscriptionInfo;,
        Lcom/google/android/finsky/protos/Common$MonthAndDay;,
        Lcom/google/android/finsky/protos/Common$TimePeriod;,
        Lcom/google/android/finsky/protos/Common$RentalTerms;,
        Lcom/google/android/finsky/protos/Common$Image;,
        Lcom/google/android/finsky/protos/Common$Offer;,
        Lcom/google/android/finsky/protos/Common$Docid;
    }
.end annotation
