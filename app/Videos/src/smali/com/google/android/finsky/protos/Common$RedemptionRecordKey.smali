.class public final Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RedemptionRecordKey"
.end annotation


# instance fields
.field public campaignId:J

.field public codeGroupId:J

.field public hasCampaignId:Z

.field public hasCodeGroupId:Z

.field public hasPublisherId:Z

.field public hasRecordId:Z

.field public publisherId:J

.field public recordId:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3997
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3998
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->clear()Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;

    .line 3999
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 4002
    iput-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->publisherId:J

    .line 4003
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->hasPublisherId:Z

    .line 4004
    iput-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->campaignId:J

    .line 4005
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->hasCampaignId:Z

    .line 4006
    iput-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->codeGroupId:J

    .line 4007
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->hasCodeGroupId:Z

    .line 4008
    iput-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->recordId:J

    .line 4009
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->hasRecordId:Z

    .line 4010
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->cachedSize:I

    .line 4011
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 4034
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4035
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->hasPublisherId:Z

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->publisherId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 4036
    :cond_0
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->publisherId:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4039
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->hasCampaignId:Z

    if-nez v1, :cond_2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->campaignId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 4040
    :cond_2
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->campaignId:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4043
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->hasCodeGroupId:Z

    if-nez v1, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->codeGroupId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 4044
    :cond_4
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->codeGroupId:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4047
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->hasRecordId:Z

    if-nez v1, :cond_6

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->recordId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 4048
    :cond_6
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->recordId:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4051
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 4059
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4060
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4064
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4065
    :sswitch_0
    return-object p0

    .line 4070
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->publisherId:J

    .line 4071
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->hasPublisherId:Z

    goto :goto_0

    .line 4075
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->campaignId:J

    .line 4076
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->hasCampaignId:Z

    goto :goto_0

    .line 4080
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->codeGroupId:J

    .line 4081
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->hasCodeGroupId:Z

    goto :goto_0

    .line 4085
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->recordId:J

    .line 4086
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->hasRecordId:Z

    goto :goto_0

    .line 4060
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3964
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 4017
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->hasPublisherId:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->publisherId:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 4018
    :cond_0
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->publisherId:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 4020
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->hasCampaignId:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->campaignId:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 4021
    :cond_2
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->campaignId:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 4023
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->hasCodeGroupId:Z

    if-nez v0, :cond_4

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->codeGroupId:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 4024
    :cond_4
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->codeGroupId:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 4026
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->hasRecordId:Z

    if-nez v0, :cond_6

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->recordId:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    .line 4027
    :cond_6
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;->recordId:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 4029
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4030
    return-void
.end method
