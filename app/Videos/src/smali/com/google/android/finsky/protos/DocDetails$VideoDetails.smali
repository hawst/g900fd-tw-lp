.class public final Lcom/google/android/finsky/protos/DocDetails$VideoDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoDetails"
.end annotation


# instance fields
.field public audioLanguage:[Ljava/lang/String;

.field public captionLanguage:[Ljava/lang/String;

.field public contentRating:Ljava/lang/String;

.field public credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

.field public dislikes:J

.field public duration:Ljava/lang/String;

.field public genre:[Ljava/lang/String;

.field public hasContentRating:Z

.field public hasDislikes:Z

.field public hasDuration:Z

.field public hasLikes:Z

.field public hasReleaseDate:Z

.field public likes:J

.field public releaseDate:Ljava/lang/String;

.field public rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

.field public trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2897
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2898
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->clear()Lcom/google/android/finsky/protos/DocDetails$VideoDetails;

    .line 2899
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$VideoDetails;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 2902
    invoke-static {}, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->emptyArray()[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    .line 2903
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->duration:Ljava/lang/String;

    .line 2904
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasDuration:Z

    .line 2905
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->releaseDate:Ljava/lang/String;

    .line 2906
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasReleaseDate:Z

    .line 2907
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->contentRating:Ljava/lang/String;

    .line 2908
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasContentRating:Z

    .line 2909
    iput-wide v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->likes:J

    .line 2910
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasLikes:Z

    .line 2911
    iput-wide v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->dislikes:J

    .line 2912
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasDislikes:Z

    .line 2913
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    .line 2914
    invoke-static {}, Lcom/google/android/finsky/protos/DocDetails$Trailer;->emptyArray()[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    .line 2915
    invoke-static {}, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;->emptyArray()[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    .line 2916
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    .line 2917
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    .line 2918
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->cachedSize:I

    .line 2919
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 2993
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 2994
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    array-length v5, v5

    if-lez v5, :cond_1

    .line 2995
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 2996
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    aget-object v2, v5, v3

    .line 2997
    .local v2, "element":Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    if-eqz v2, :cond_0

    .line 2998
    const/4 v5, 0x1

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 2995
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 3003
    .end local v2    # "element":Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    .end local v3    # "i":I
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasDuration:Z

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->duration:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 3004
    :cond_2
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->duration:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 3007
    :cond_3
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasReleaseDate:Z

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->releaseDate:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 3008
    :cond_4
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->releaseDate:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 3011
    :cond_5
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasContentRating:Z

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->contentRating:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 3012
    :cond_6
    const/4 v5, 0x4

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->contentRating:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 3015
    :cond_7
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasLikes:Z

    if-nez v5, :cond_8

    iget-wide v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->likes:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_9

    .line 3016
    :cond_8
    const/4 v5, 0x5

    iget-wide v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->likes:J

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 3019
    :cond_9
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasDislikes:Z

    if-nez v5, :cond_a

    iget-wide v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->dislikes:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_b

    .line 3020
    :cond_a
    const/4 v5, 0x6

    iget-wide v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->dislikes:J

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 3023
    :cond_b
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    if-eqz v5, :cond_e

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_e

    .line 3024
    const/4 v0, 0x0

    .line 3025
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 3026
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_d

    .line 3027
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 3028
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_c

    .line 3029
    add-int/lit8 v0, v0, 0x1

    .line 3030
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 3026
    :cond_c
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 3034
    .end local v2    # "element":Ljava/lang/String;
    :cond_d
    add-int/2addr v4, v1

    .line 3035
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 3037
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_e
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    if-eqz v5, :cond_10

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    array-length v5, v5

    if-lez v5, :cond_10

    .line 3038
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    array-length v5, v5

    if-ge v3, v5, :cond_10

    .line 3039
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    aget-object v2, v5, v3

    .line 3040
    .local v2, "element":Lcom/google/android/finsky/protos/DocDetails$Trailer;
    if-eqz v2, :cond_f

    .line 3041
    const/16 v5, 0x8

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 3038
    :cond_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 3046
    .end local v2    # "element":Lcom/google/android/finsky/protos/DocDetails$Trailer;
    .end local v3    # "i":I
    :cond_10
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    if-eqz v5, :cond_12

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    array-length v5, v5

    if-lez v5, :cond_12

    .line 3047
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    array-length v5, v5

    if-ge v3, v5, :cond_12

    .line 3048
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    aget-object v2, v5, v3

    .line 3049
    .local v2, "element":Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    if-eqz v2, :cond_11

    .line 3050
    const/16 v5, 0x9

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 3047
    :cond_11
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 3055
    .end local v2    # "element":Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    .end local v3    # "i":I
    :cond_12
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    if-eqz v5, :cond_15

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_15

    .line 3056
    const/4 v0, 0x0

    .line 3057
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 3058
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_14

    .line 3059
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 3060
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_13

    .line 3061
    add-int/lit8 v0, v0, 0x1

    .line 3062
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 3058
    :cond_13
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 3066
    .end local v2    # "element":Ljava/lang/String;
    :cond_14
    add-int/2addr v4, v1

    .line 3067
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 3069
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_15
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    if-eqz v5, :cond_18

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_18

    .line 3070
    const/4 v0, 0x0

    .line 3071
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 3072
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_17

    .line 3073
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 3074
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_16

    .line 3075
    add-int/lit8 v0, v0, 0x1

    .line 3076
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 3072
    :cond_16
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 3080
    .end local v2    # "element":Ljava/lang/String;
    :cond_17
    add-int/2addr v4, v1

    .line 3081
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 3083
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_18
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$VideoDetails;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 3091
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3092
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3096
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3097
    :sswitch_0
    return-object p0

    .line 3102
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3104
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    if-nez v5, :cond_2

    move v1, v4

    .line 3105
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    .line 3107
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    if-eqz v1, :cond_1

    .line 3108
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3110
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 3111
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;-><init>()V

    aput-object v5, v2, v1

    .line 3112
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3113
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3110
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3104
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    array-length v1, v5

    goto :goto_1

    .line 3116
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;-><init>()V

    aput-object v5, v2, v1

    .line 3117
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3118
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    goto :goto_0

    .line 3122
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->duration:Ljava/lang/String;

    .line 3123
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasDuration:Z

    goto :goto_0

    .line 3127
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->releaseDate:Ljava/lang/String;

    .line 3128
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasReleaseDate:Z

    goto :goto_0

    .line 3132
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->contentRating:Ljava/lang/String;

    .line 3133
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasContentRating:Z

    goto :goto_0

    .line 3137
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->likes:J

    .line 3138
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasLikes:Z

    goto :goto_0

    .line 3142
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->dislikes:J

    .line 3143
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasDislikes:Z

    goto :goto_0

    .line 3147
    :sswitch_7
    const/16 v5, 0x3a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3149
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    if-nez v5, :cond_5

    move v1, v4

    .line 3150
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 3151
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 3152
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3154
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 3155
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 3156
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3154
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 3149
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_3

    .line 3159
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 3160
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    goto/16 :goto_0

    .line 3164
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_8
    const/16 v5, 0x42

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3166
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    if-nez v5, :cond_8

    move v1, v4

    .line 3167
    .restart local v1    # "i":I
    :goto_5
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocDetails$Trailer;

    .line 3169
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocDetails$Trailer;
    if-eqz v1, :cond_7

    .line 3170
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3172
    :cond_7
    :goto_6
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_9

    .line 3173
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$Trailer;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$Trailer;-><init>()V

    aput-object v5, v2, v1

    .line 3174
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3175
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3172
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 3166
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$Trailer;
    :cond_8
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    array-length v1, v5

    goto :goto_5

    .line 3178
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$Trailer;
    :cond_9
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$Trailer;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$Trailer;-><init>()V

    aput-object v5, v2, v1

    .line 3179
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3180
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    goto/16 :goto_0

    .line 3184
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$Trailer;
    :sswitch_9
    const/16 v5, 0x4a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3186
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    if-nez v5, :cond_b

    move v1, v4

    .line 3187
    .restart local v1    # "i":I
    :goto_7
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    .line 3189
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    if-eqz v1, :cond_a

    .line 3190
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3192
    :cond_a
    :goto_8
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_c

    .line 3193
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;-><init>()V

    aput-object v5, v2, v1

    .line 3194
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3195
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3192
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 3186
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    :cond_b
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    array-length v1, v5

    goto :goto_7

    .line 3198
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    :cond_c
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;-><init>()V

    aput-object v5, v2, v1

    .line 3199
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3200
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    goto/16 :goto_0

    .line 3204
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    :sswitch_a
    const/16 v5, 0x52

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3206
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    if-nez v5, :cond_e

    move v1, v4

    .line 3207
    .restart local v1    # "i":I
    :goto_9
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 3208
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_d

    .line 3209
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3211
    :cond_d
    :goto_a
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_f

    .line 3212
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 3213
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3211
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 3206
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_e
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_9

    .line 3216
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 3217
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    goto/16 :goto_0

    .line 3221
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_b
    const/16 v5, 0x5a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3223
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    if-nez v5, :cond_11

    move v1, v4

    .line 3224
    .restart local v1    # "i":I
    :goto_b
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 3225
    .restart local v2    # "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_10

    .line 3226
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3228
    :cond_10
    :goto_c
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_12

    .line 3229
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 3230
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3228
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 3223
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_11
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_b

    .line 3233
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 3234
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    goto/16 :goto_0

    .line 3092
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2842
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$VideoDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 8
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 2925
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 2926
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 2927
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    aget-object v0, v2, v1

    .line 2928
    .local v0, "element":Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    if-eqz v0, :cond_0

    .line 2929
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2926
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2933
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasDuration:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->duration:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2934
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->duration:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2936
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasReleaseDate:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->releaseDate:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2937
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->releaseDate:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2939
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasContentRating:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->contentRating:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 2940
    :cond_6
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->contentRating:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2942
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasLikes:Z

    if-nez v2, :cond_8

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->likes:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_9

    .line 2943
    :cond_8
    const/4 v2, 0x5

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->likes:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 2945
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->hasDislikes:Z

    if-nez v2, :cond_a

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->dislikes:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_b

    .line 2946
    :cond_a
    const/4 v2, 0x6

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->dislikes:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 2948
    :cond_b
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_d

    .line 2949
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_d

    .line 2950
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->genre:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 2951
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_c

    .line 2952
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2949
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2956
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_d
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    array-length v2, v2

    if-lez v2, :cond_f

    .line 2957
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    array-length v2, v2

    if-ge v1, v2, :cond_f

    .line 2958
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->trailer:[Lcom/google/android/finsky/protos/DocDetails$Trailer;

    aget-object v0, v2, v1

    .line 2959
    .local v0, "element":Lcom/google/android/finsky/protos/DocDetails$Trailer;
    if-eqz v0, :cond_e

    .line 2960
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2957
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2964
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocDetails$Trailer;
    .end local v1    # "i":I
    :cond_f
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    array-length v2, v2

    if-lez v2, :cond_11

    .line 2965
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    array-length v2, v2

    if-ge v1, v2, :cond_11

    .line 2966
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->rentalTerm:[Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;

    aget-object v0, v2, v1

    .line 2967
    .local v0, "element":Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    if-eqz v0, :cond_10

    .line 2968
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2965
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2972
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocDetails$VideoRentalTerm;
    .end local v1    # "i":I
    :cond_11
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_13

    .line 2973
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_13

    .line 2974
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 2975
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_12

    .line 2976
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2973
    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2980
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_13
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_15

    .line 2981
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_15

    .line 2982
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 2983
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_14

    .line 2984
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2981
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 2988
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_15
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2989
    return-void
.end method
