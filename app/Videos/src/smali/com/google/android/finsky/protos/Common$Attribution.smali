.class public final Lcom/google/android/finsky/protos/Common$Attribution;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Attribution"
.end annotation


# instance fields
.field public hasLicenseTitle:Z

.field public hasLicenseUrl:Z

.field public hasSourceTitle:Z

.field public hasSourceUrl:Z

.field public licenseTitle:Ljava/lang/String;

.field public licenseUrl:Ljava/lang/String;

.field public sourceTitle:Ljava/lang/String;

.field public sourceUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5617
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 5618
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$Attribution;->clear()Lcom/google/android/finsky/protos/Common$Attribution;

    .line 5619
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$Attribution;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5622
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceTitle:Ljava/lang/String;

    .line 5623
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasSourceTitle:Z

    .line 5624
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceUrl:Ljava/lang/String;

    .line 5625
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasSourceUrl:Z

    .line 5626
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseTitle:Ljava/lang/String;

    .line 5627
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasLicenseTitle:Z

    .line 5628
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseUrl:Ljava/lang/String;

    .line 5629
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasLicenseUrl:Z

    .line 5630
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->cachedSize:I

    .line 5631
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 5654
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 5655
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasSourceTitle:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 5656
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceTitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5659
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasSourceUrl:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 5660
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5663
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasLicenseTitle:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 5664
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseTitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5667
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasLicenseUrl:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 5668
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5671
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Attribution;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 5679
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 5680
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 5684
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 5685
    :sswitch_0
    return-object p0

    .line 5690
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceTitle:Ljava/lang/String;

    .line 5691
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasSourceTitle:Z

    goto :goto_0

    .line 5695
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceUrl:Ljava/lang/String;

    .line 5696
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasSourceUrl:Z

    goto :goto_0

    .line 5700
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseTitle:Ljava/lang/String;

    .line 5701
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasLicenseTitle:Z

    goto :goto_0

    .line 5705
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseUrl:Ljava/lang/String;

    .line 5706
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasLicenseUrl:Z

    goto :goto_0

    .line 5680
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5584
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$Attribution;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Attribution;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5637
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasSourceTitle:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 5638
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5640
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasSourceUrl:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 5641
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->sourceUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5643
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasLicenseTitle:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 5644
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5646
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->hasLicenseUrl:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 5647
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$Attribution;->licenseUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5649
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 5650
    return-void
.end method
