.class public final Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
.super Lcom/google/protobuf/nano/MessageNano;
.source "FilterRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/FilterRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RuleEvaluation"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;


# instance fields
.field public actualBoolValue:[Z

.field public actualDoubleValue:[D

.field public actualLongValue:[J

.field public actualStringValue:[Ljava/lang/String;

.field public rule:Lcom/google/android/finsky/protos/FilterRules$Rule;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 714
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 715
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->clear()Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    .line 716
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
    .locals 2

    .prologue
    .line 688
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    if-nez v0, :cond_1

    .line 689
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 691
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    if-nez v0, :cond_0

    .line 692
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    sput-object v0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    .line 694
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 696
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->_emptyArray:[Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    return-object v0

    .line 694
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
    .locals 1

    .prologue
    .line 719
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 720
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    .line 721
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_LONG_ARRAY:[J

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    .line 722
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BOOLEAN_ARRAY:[Z

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    .line 723
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_DOUBLE_ARRAY:[D

    iput-object v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    .line 724
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->cachedSize:I

    .line 725
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    .line 762
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v5

    .line 763
    .local v5, "size":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-eqz v6, :cond_0

    .line 764
    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-static {v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v6

    add-int/2addr v5, v6

    .line 767
    :cond_0
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    array-length v6, v6

    if-lez v6, :cond_3

    .line 768
    const/4 v0, 0x0

    .line 769
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 770
    .local v1, "dataSize":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    array-length v6, v6

    if-ge v4, v6, :cond_2

    .line 771
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    aget-object v2, v6, v4

    .line 772
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 773
    add-int/lit8 v0, v0, 0x1

    .line 774
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v1, v6

    .line 770
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 778
    .end local v2    # "element":Ljava/lang/String;
    :cond_2
    add-int/2addr v5, v1

    .line 779
    mul-int/lit8 v6, v0, 0x1

    add-int/2addr v5, v6

    .line 781
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v4    # "i":I
    :cond_3
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v6, v6

    if-lez v6, :cond_5

    .line 782
    const/4 v1, 0x0

    .line 783
    .restart local v1    # "dataSize":I
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_1
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v6, v6

    if-ge v4, v6, :cond_4

    .line 784
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    aget-wide v2, v6, v4

    .line 785
    .local v2, "element":J
    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64SizeNoTag(J)I

    move-result v6

    add-int/2addr v1, v6

    .line 783
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 788
    .end local v2    # "element":J
    :cond_4
    add-int/2addr v5, v1

    .line 789
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    .line 791
    .end local v1    # "dataSize":I
    .end local v4    # "i":I
    :cond_5
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v6, v6

    if-lez v6, :cond_6

    .line 792
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v6, v6

    mul-int/lit8 v1, v6, 0x1

    .line 793
    .restart local v1    # "dataSize":I
    add-int/2addr v5, v1

    .line 794
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    .line 796
    .end local v1    # "dataSize":I
    :cond_6
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v6, v6

    if-lez v6, :cond_7

    .line 797
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v6, v6

    mul-int/lit8 v1, v6, 0x8

    .line 798
    .restart local v1    # "dataSize":I
    add-int/2addr v5, v1

    .line 799
    iget-object v6, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v6, v6

    mul-int/lit8 v6, v6, 0x1

    add-int/2addr v5, v6

    .line 801
    .end local v1    # "dataSize":I
    :cond_7
    return v5
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;
    .locals 10
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 809
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v6

    .line 810
    .local v6, "tag":I
    sparse-switch v6, :sswitch_data_0

    .line 814
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v8

    if-nez v8, :cond_0

    .line 815
    :sswitch_0
    return-object p0

    .line 820
    :sswitch_1
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-nez v8, :cond_1

    .line 821
    new-instance v8, Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/FilterRules$Rule;-><init>()V

    iput-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    .line 823
    :cond_1
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 827
    :sswitch_2
    const/16 v8, 0x12

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 829
    .local v0, "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    if-nez v8, :cond_3

    move v1, v7

    .line 830
    .local v1, "i":I
    :goto_1
    add-int v8, v1, v0

    new-array v4, v8, [Ljava/lang/String;

    .line 831
    .local v4, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 832
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 834
    :cond_2
    :goto_2
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_4

    .line 835
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v1

    .line 836
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 834
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 829
    .end local v1    # "i":I
    .end local v4    # "newArray":[Ljava/lang/String;
    :cond_3
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    array-length v1, v8

    goto :goto_1

    .line 839
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Ljava/lang/String;
    :cond_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v1

    .line 840
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    goto :goto_0

    .line 844
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[Ljava/lang/String;
    :sswitch_3
    const/16 v8, 0x18

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 846
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    if-nez v8, :cond_6

    move v1, v7

    .line 847
    .restart local v1    # "i":I
    :goto_3
    add-int v8, v1, v0

    new-array v4, v8, [J

    .line 848
    .local v4, "newArray":[J
    if-eqz v1, :cond_5

    .line 849
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 851
    :cond_5
    :goto_4
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_7

    .line 852
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 853
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 851
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 846
    .end local v1    # "i":I
    .end local v4    # "newArray":[J
    :cond_6
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v1, v8

    goto :goto_3

    .line 856
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[J
    :cond_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 857
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    goto/16 :goto_0

    .line 861
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[J
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 862
    .local v2, "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 864
    .local v3, "limit":I
    const/4 v0, 0x0

    .line 865
    .restart local v0    # "arrayLength":I
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v5

    .line 866
    .local v5, "startPos":I
    :goto_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v8

    if-lez v8, :cond_8

    .line 867
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    .line 868
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 870
    :cond_8
    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 871
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    if-nez v8, :cond_a

    move v1, v7

    .line 872
    .restart local v1    # "i":I
    :goto_6
    add-int v8, v1, v0

    new-array v4, v8, [J

    .line 873
    .restart local v4    # "newArray":[J
    if-eqz v1, :cond_9

    .line 874
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 876
    :cond_9
    :goto_7
    array-length v8, v4

    if-ge v1, v8, :cond_b

    .line 877
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 876
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 871
    .end local v1    # "i":I
    .end local v4    # "newArray":[J
    :cond_a
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v1, v8

    goto :goto_6

    .line 879
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[J
    :cond_b
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    .line 880
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 884
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "limit":I
    .end local v4    # "newArray":[J
    .end local v5    # "startPos":I
    :sswitch_5
    const/16 v8, 0x20

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 886
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    if-nez v8, :cond_d

    move v1, v7

    .line 887
    .restart local v1    # "i":I
    :goto_8
    add-int v8, v1, v0

    new-array v4, v8, [Z

    .line 888
    .local v4, "newArray":[Z
    if-eqz v1, :cond_c

    .line 889
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 891
    :cond_c
    :goto_9
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_e

    .line 892
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v8

    aput-boolean v8, v4, v1

    .line 893
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 891
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 886
    .end local v1    # "i":I
    .end local v4    # "newArray":[Z
    :cond_d
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v1, v8

    goto :goto_8

    .line 896
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Z
    :cond_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v8

    aput-boolean v8, v4, v1

    .line 897
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    goto/16 :goto_0

    .line 901
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[Z
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 902
    .restart local v2    # "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 904
    .restart local v3    # "limit":I
    const/4 v0, 0x0

    .line 905
    .restart local v0    # "arrayLength":I
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v5

    .line 906
    .restart local v5    # "startPos":I
    :goto_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v8

    if-lez v8, :cond_f

    .line 907
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    .line 908
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 910
    :cond_f
    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 911
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    if-nez v8, :cond_11

    move v1, v7

    .line 912
    .restart local v1    # "i":I
    :goto_b
    add-int v8, v1, v0

    new-array v4, v8, [Z

    .line 913
    .restart local v4    # "newArray":[Z
    if-eqz v1, :cond_10

    .line 914
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 916
    :cond_10
    :goto_c
    array-length v8, v4

    if-ge v1, v8, :cond_12

    .line 917
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v8

    aput-boolean v8, v4, v1

    .line 916
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 911
    .end local v1    # "i":I
    .end local v4    # "newArray":[Z
    :cond_11
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v1, v8

    goto :goto_b

    .line 919
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Z
    :cond_12
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    .line 920
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 924
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "limit":I
    .end local v4    # "newArray":[Z
    .end local v5    # "startPos":I
    :sswitch_7
    const/16 v8, 0x29

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 926
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    if-nez v8, :cond_14

    move v1, v7

    .line 927
    .restart local v1    # "i":I
    :goto_d
    add-int v8, v1, v0

    new-array v4, v8, [D

    .line 928
    .local v4, "newArray":[D
    if-eqz v1, :cond_13

    .line 929
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 931
    :cond_13
    :goto_e
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_15

    .line 932
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 933
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 931
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 926
    .end local v1    # "i":I
    .end local v4    # "newArray":[D
    :cond_14
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v1, v8

    goto :goto_d

    .line 936
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[D
    :cond_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 937
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    goto/16 :goto_0

    .line 941
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[D
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 942
    .restart local v2    # "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 943
    .restart local v3    # "limit":I
    div-int/lit8 v0, v2, 0x8

    .line 944
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    if-nez v8, :cond_17

    move v1, v7

    .line 945
    .restart local v1    # "i":I
    :goto_f
    add-int v8, v1, v0

    new-array v4, v8, [D

    .line 946
    .restart local v4    # "newArray":[D
    if-eqz v1, :cond_16

    .line 947
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 949
    :cond_16
    :goto_10
    array-length v8, v4

    if-ge v1, v8, :cond_18

    .line 950
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v8

    aput-wide v8, v4, v1

    .line 949
    add-int/lit8 v1, v1, 0x1

    goto :goto_10

    .line 944
    .end local v1    # "i":I
    .end local v4    # "newArray":[D
    :cond_17
    iget-object v8, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v1, v8

    goto :goto_f

    .line 952
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[D
    :cond_18
    iput-object v4, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    .line 953
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 810
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
        0x22 -> :sswitch_6
        0x29 -> :sswitch_7
        0x2a -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 682
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 731
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    if-eqz v2, :cond_0

    .line 732
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->rule:Lcom/google/android/finsky/protos/FilterRules$Rule;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 734
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 735
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 736
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualStringValue:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 737
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 738
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 735
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 742
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v2, v2

    if-lez v2, :cond_3

    .line 743
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 744
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualLongValue:[J

    aget-wide v4, v3, v1

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 743
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 747
    .end local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v2, v2

    if-lez v2, :cond_4

    .line 748
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 749
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualBoolValue:[Z

    aget-boolean v3, v3, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 748
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 752
    .end local v1    # "i":I
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v2, v2

    if-lez v2, :cond_5

    .line 753
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 754
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/FilterRules$RuleEvaluation;->actualDoubleValue:[D

    aget-wide v4, v3, v1

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 753
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 757
    .end local v1    # "i":I
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 758
    return-void
.end method
