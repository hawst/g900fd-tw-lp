.class public final Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RecommendationsContainerWithHeader"
.end annotation


# instance fields
.field public primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4484
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4485
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->clear()Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    .line 4486
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;
    .locals 1

    .prologue
    .line 4489
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 4490
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 4491
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->cachedSize:I

    .line 4492
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 4514
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 4515
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_0

    .line 4516
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4519
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 4520
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 4521
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v3, v1

    .line 4522
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_1

    .line 4523
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4520
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4528
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_2
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 4536
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 4537
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 4541
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 4542
    :sswitch_0
    return-object p0

    .line 4547
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_1

    .line 4548
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 4550
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 4554
    :sswitch_2
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 4556
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_3

    move v1, v4

    .line 4557
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 4559
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_2

    .line 4560
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4562
    :cond_2
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 4563
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 4564
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 4565
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 4562
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4556
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v1, v5

    goto :goto_1

    .line 4568
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_4
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 4569
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 4570
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    goto :goto_0

    .line 4537
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4461
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4498
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_0

    .line 4499
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4501
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 4502
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 4503
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v2, v1

    .line 4504
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_1

    .line 4505
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4502
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4509
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4510
    return-void
.end method
