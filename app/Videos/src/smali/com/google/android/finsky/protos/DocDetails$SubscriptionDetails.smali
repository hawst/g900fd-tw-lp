.class public final Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SubscriptionDetails"
.end annotation


# instance fields
.field public hasSubscriptionPeriod:Z

.field public subscriptionPeriod:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3880
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3881
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;->clear()Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;

    .line 3882
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;
    .locals 1

    .prologue
    .line 3885
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;->subscriptionPeriod:I

    .line 3886
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;->hasSubscriptionPeriod:Z

    .line 3887
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;->cachedSize:I

    .line 3888
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3902
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3903
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;->subscriptionPeriod:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;->hasSubscriptionPeriod:Z

    if-eqz v1, :cond_1

    .line 3904
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;->subscriptionPeriod:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3907
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3915
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3916
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3920
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3921
    :sswitch_0
    return-object p0

    .line 3926
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 3927
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 3933
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;->subscriptionPeriod:I

    .line 3934
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;->hasSubscriptionPeriod:Z

    goto :goto_0

    .line 3916
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch

    .line 3927
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3859
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 3894
    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;->subscriptionPeriod:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;->hasSubscriptionPeriod:Z

    if-eqz v0, :cond_1

    .line 3895
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/DocDetails$SubscriptionDetails;->subscriptionPeriod:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3897
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3898
    return-void
.end method
