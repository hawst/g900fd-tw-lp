.class public final Lcom/google/android/finsky/protos/Common$TimePeriod;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TimePeriod"
.end annotation


# instance fields
.field public count:I

.field public hasCount:Z

.field public hasUnit:Z

.field public unit:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2089
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2090
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$TimePeriod;->clear()Lcom/google/android/finsky/protos/Common$TimePeriod;

    .line 2091
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$TimePeriod;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2094
    iput v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->unit:I

    .line 2095
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->hasUnit:Z

    .line 2096
    iput v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->count:I

    .line 2097
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->hasCount:Z

    .line 2098
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->cachedSize:I

    .line 2099
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2116
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2117
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->unit:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->hasUnit:Z

    if-eqz v1, :cond_1

    .line 2118
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->unit:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2121
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->hasCount:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->count:I

    if-eqz v1, :cond_3

    .line 2122
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->count:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2125
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$TimePeriod;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 2133
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2134
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2138
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2139
    :sswitch_0
    return-object p0

    .line 2144
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 2145
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2154
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->unit:I

    .line 2155
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->hasUnit:Z

    goto :goto_0

    .line 2161
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->count:I

    .line 2162
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->hasCount:Z

    goto :goto_0

    .line 2134
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    .line 2145
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2054
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$TimePeriod;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$TimePeriod;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2105
    iget v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->unit:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->hasUnit:Z

    if-eqz v0, :cond_1

    .line 2106
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->unit:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2108
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->hasCount:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->count:I

    if-eqz v0, :cond_3

    .line 2109
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$TimePeriod;->count:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2111
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2112
    return-void
.end method
