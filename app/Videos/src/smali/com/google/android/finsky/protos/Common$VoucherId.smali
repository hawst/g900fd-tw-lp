.class public final Lcom/google/android/finsky/protos/Common$VoucherId;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VoucherId"
.end annotation


# instance fields
.field public key:Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;

.field public voucherDocid:Lcom/google/android/finsky/protos/Common$Docid;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4128
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4129
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$VoucherId;->clear()Lcom/google/android/finsky/protos/Common$VoucherId;

    .line 4130
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$VoucherId;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4133
    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$VoucherId;->voucherDocid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 4134
    iput-object v0, p0, Lcom/google/android/finsky/protos/Common$VoucherId;->key:Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;

    .line 4135
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$VoucherId;->cachedSize:I

    .line 4136
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 4153
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4154
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$VoucherId;->voucherDocid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v1, :cond_0

    .line 4155
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$VoucherId;->voucherDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4158
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$VoucherId;->key:Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;

    if-eqz v1, :cond_1

    .line 4159
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Common$VoucherId;->key:Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4162
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$VoucherId;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4170
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4171
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4175
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4176
    :sswitch_0
    return-object p0

    .line 4181
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$VoucherId;->voucherDocid:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v1, :cond_1

    .line 4182
    new-instance v1, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$VoucherId;->voucherDocid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 4184
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$VoucherId;->voucherDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 4188
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$VoucherId;->key:Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;

    if-nez v1, :cond_2

    .line 4189
    new-instance v1, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Common$VoucherId;->key:Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;

    .line 4191
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$VoucherId;->key:Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 4171
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4105
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$VoucherId;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$VoucherId;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4142
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$VoucherId;->voucherDocid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v0, :cond_0

    .line 4143
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$VoucherId;->voucherDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4145
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$VoucherId;->key:Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;

    if-eqz v0, :cond_1

    .line 4146
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Common$VoucherId;->key:Lcom/google/android/finsky/protos/Common$RedemptionRecordKey;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4148
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4149
    return-void
.end method
