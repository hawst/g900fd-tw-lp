.class public Lcom/google/android/libraries/bind/data/BoundHelper;
.super Ljava/lang/Object;
.source "BoundHelper.java"


# static fields
.field private static final LOGD:Lcom/google/android/libraries/bind/logging/Logd;


# instance fields
.field public final bindBackgroundKey:Ljava/lang/Integer;

.field public final bindContentDescriptionKey:Ljava/lang/Integer;

.field public final bindEnabledKey:Ljava/lang/Integer;

.field public final bindInvisibilityKey:Ljava/lang/Integer;

.field public final bindMinHeightKey:Ljava/lang/Integer;

.field public final bindOnClickListenerKey:Ljava/lang/Integer;

.field public final bindPaddingTopKey:Ljava/lang/Integer;

.field public final bindTransitionNameKey:Ljava/lang/Integer;

.field public final bindVisibilityKey:Ljava/lang/Integer;

.field protected final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/libraries/bind/data/BoundHelper;

    invoke-static {v0}, Lcom/google/android/libraries/bind/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/data/BoundHelper;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "view"    # Landroid/view/View;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p3, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->view:Landroid/view/View;

    .line 36
    sget-object v1, Lcom/google/android/libraries/bind/R$styleable;->BoundView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 38
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v1, Lcom/google/android/libraries/bind/R$styleable;->BoundView_bindBackground:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindBackgroundKey:Ljava/lang/Integer;

    .line 39
    sget v1, Lcom/google/android/libraries/bind/R$styleable;->BoundView_bindContentDescription:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindContentDescriptionKey:Ljava/lang/Integer;

    .line 40
    sget v1, Lcom/google/android/libraries/bind/R$styleable;->BoundView_bindEnabled:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindEnabledKey:Ljava/lang/Integer;

    .line 41
    sget v1, Lcom/google/android/libraries/bind/R$styleable;->BoundView_bindOnClickListener:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindOnClickListenerKey:Ljava/lang/Integer;

    .line 42
    sget v1, Lcom/google/android/libraries/bind/R$styleable;->BoundView_bindInvisibility:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindInvisibilityKey:Ljava/lang/Integer;

    .line 43
    sget v1, Lcom/google/android/libraries/bind/R$styleable;->BoundView_bindMinHeight:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindMinHeightKey:Ljava/lang/Integer;

    .line 44
    sget v1, Lcom/google/android/libraries/bind/R$styleable;->BoundView_bindPaddingTop:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindPaddingTopKey:Ljava/lang/Integer;

    .line 45
    sget v1, Lcom/google/android/libraries/bind/R$styleable;->BoundView_bindVisibility:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindVisibilityKey:Ljava/lang/Integer;

    .line 46
    sget v1, Lcom/google/android/libraries/bind/R$styleable;->BoundView_bindTransitionName:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/BoundHelper;->getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindTransitionNameKey:Ljava/lang/Integer;

    .line 48
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 49
    return-void
.end method

.method public static bindBackground(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 5
    .param p0, "view"    # Landroid/view/View;
    .param p1, "bindBackgroundKey"    # Ljava/lang/Integer;
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v4, 0x0

    .line 72
    if-eqz p1, :cond_0

    .line 73
    if-nez p2, :cond_1

    const/4 v0, 0x0

    .line 74
    .local v0, "background":Ljava/lang/Object;
    :goto_0
    if-nez v0, :cond_2

    .line 75
    invoke-virtual {p0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 84
    .end local v0    # "background":Ljava/lang/Object;
    :cond_0
    :goto_1
    return-void

    .line 73
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 76
    .restart local v0    # "background":Ljava/lang/Object;
    :cond_2
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 77
    check-cast v0, Ljava/lang/Integer;

    .end local v0    # "background":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    .line 78
    .restart local v0    # "background":Ljava/lang/Object;
    :cond_3
    instance-of v1, v0, Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_4

    .line 79
    check-cast v0, Landroid/graphics/drawable/Drawable;

    .end local v0    # "background":Ljava/lang/Object;
    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 81
    .restart local v0    # "background":Ljava/lang/Object;
    :cond_4
    sget-object v1, Lcom/google/android/libraries/bind/data/BoundHelper;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v2, "Unrecognized bound background for key: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/logging/Logd;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static bindContentDescription(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 2
    .param p0, "view"    # Landroid/view/View;
    .param p1, "bindContentDescriptionKey"    # Ljava/lang/Integer;
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 88
    if-eqz p1, :cond_0

    .line 89
    if-nez p2, :cond_1

    const/4 v0, 0x0

    .line 91
    .local v0, "contentDescription":Ljava/lang/CharSequence;
    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 93
    .end local v0    # "contentDescription":Ljava/lang/CharSequence;
    :cond_0
    return-void

    .line 89
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    move-object v0, v1

    goto :goto_0
.end method

.method public static bindEnabled(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "bindEnabledKey"    # Ljava/lang/Integer;
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 108
    if-eqz p1, :cond_0

    .line 109
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 111
    .local v0, "enabled":Z
    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 113
    .end local v0    # "enabled":Z
    :cond_0
    return-void

    .line 109
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static bindInvisibility(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "bindInvisibilityKey"    # Ljava/lang/Integer;
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 149
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/libraries/bind/data/BoundHelper;->bindVisibility(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;Z)V

    .line 150
    return-void
.end method

.method public static bindMinHeight(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "bindMinHeightKey"    # Ljava/lang/Integer;
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v0, 0x0

    .line 116
    if-eqz p1, :cond_0

    .line 117
    if-nez p2, :cond_1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 118
    .local v1, "optMinHeight":Ljava/lang/Number;
    :goto_0
    if-nez v1, :cond_2

    .line 119
    .local v0, "minHeight":I
    :goto_1
    invoke-virtual {p0, v0}, Landroid/view/View;->setMinimumHeight(I)V

    .line 121
    .end local v0    # "minHeight":I
    .end local v1    # "optMinHeight":Ljava/lang/Number;
    :cond_0
    return-void

    .line 117
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p2, v2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    move-object v1, v2

    goto :goto_0

    .line 118
    .restart local v1    # "optMinHeight":Ljava/lang/Number;
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v0

    goto :goto_1
.end method

.method public static bindOnClickListener(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "bindOnClickListenerKey"    # Ljava/lang/Integer;
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 124
    if-eqz p1, :cond_0

    .line 125
    if-nez p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    :cond_0
    return-void

    .line 125
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnClickListener;

    goto :goto_0
.end method

.method public static bindPaddingTop(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 9
    .param p0, "view"    # Landroid/view/View;
    .param p1, "bindPaddingTopKey"    # Ljava/lang/Integer;
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    const/4 v6, 0x0

    .line 132
    if-eqz p1, :cond_0

    .line 133
    if-nez p2, :cond_1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 134
    .local v0, "optPaddingTop":Ljava/lang/Number;
    :goto_0
    if-nez v0, :cond_2

    .line 135
    .local v6, "paddingTop":I
    :goto_1
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    .line 136
    .local v1, "paddingBottom":I
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x11

    if-lt v7, v8, :cond_3

    invoke-virtual {p0}, Landroid/view/View;->isPaddingRelative()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 137
    invoke-virtual {p0}, Landroid/view/View;->getPaddingStart()I

    move-result v5

    .line 138
    .local v5, "paddingStart":I
    invoke-virtual {p0}, Landroid/view/View;->getPaddingEnd()I

    move-result v2

    .line 139
    .local v2, "paddingEnd":I
    invoke-virtual {p0, v5, v6, v2, v1}, Landroid/view/View;->setPaddingRelative(IIII)V

    .line 146
    .end local v0    # "optPaddingTop":Ljava/lang/Number;
    .end local v1    # "paddingBottom":I
    .end local v2    # "paddingEnd":I
    .end local v5    # "paddingStart":I
    .end local v6    # "paddingTop":I
    :cond_0
    :goto_2
    return-void

    .line 133
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {p2, v7}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    move-object v0, v7

    goto :goto_0

    .line 134
    .restart local v0    # "optPaddingTop":Ljava/lang/Number;
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v6

    goto :goto_1

    .line 141
    .restart local v1    # "paddingBottom":I
    .restart local v6    # "paddingTop":I
    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    .line 142
    .local v3, "paddingLeft":I
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    .line 143
    .local v4, "paddingRight":I
    invoke-virtual {p0, v3, v6, v4, v1}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_2
.end method

.method public static bindTransitionName(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "bindTransitionNameKey"    # Ljava/lang/Integer;
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 97
    if-eqz p1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 98
    if-nez p2, :cond_1

    const/4 v0, 0x0

    .line 99
    .local v0, "transitionName":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 101
    .end local v0    # "transitionName":Ljava/lang/String;
    :cond_0
    return-void

    .line 98
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static bindVisibility(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "bindVisibilityKey"    # Ljava/lang/Integer;
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 157
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/libraries/bind/data/BoundHelper;->bindVisibility(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;Z)V

    .line 158
    return-void
.end method

.method private static bindVisibility(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;Z)V
    .locals 5
    .param p0, "view"    # Landroid/view/View;
    .param p1, "bindVisibilityKey"    # Ljava/lang/Integer;
    .param p2, "data"    # Lcom/google/android/libraries/bind/data/Data;
    .param p3, "negate"    # Z

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 166
    if-eqz p1, :cond_2

    .line 168
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p2, v2}, Lcom/google/android/libraries/bind/data/Data;->containsKey(I)Z

    move-result v2

    if-nez v2, :cond_3

    .line 169
    :cond_0
    const/16 v0, 0x8

    .line 176
    .local v0, "visibility":I
    :goto_0
    if-eqz p3, :cond_1

    .line 177
    if-nez v0, :cond_6

    move v0, v4

    .line 179
    :cond_1
    :goto_1
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 181
    .end local v0    # "visibility":I
    :cond_2
    return-void

    .line 170
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p2, v2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    .line 171
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p2, v2}, Lcom/google/android/libraries/bind/data/Data;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 172
    .local v1, "visible":Z
    if-eqz v1, :cond_4

    move v0, v3

    .line 173
    .restart local v0    # "visibility":I
    :goto_2
    goto :goto_0

    .end local v0    # "visibility":I
    :cond_4
    move v0, v4

    .line 172
    goto :goto_2

    .line 174
    .end local v1    # "visible":Z
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "visibility":I
    goto :goto_0

    :cond_6
    move v0, v3

    .line 177
    goto :goto_1
.end method

.method public static getInteger(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;
    .locals 2
    .param p0, "a"    # Landroid/content/res/TypedArray;
    .param p1, "attr"    # I

    .prologue
    const v1, 0x7fffffff

    .line 66
    invoke-virtual {p0, p1, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 67
    .local v0, "value":I
    if-ne v0, v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 2
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->view:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindBackgroundKey:Ljava/lang/Integer;

    invoke-static {v0, v1, p1}, Lcom/google/android/libraries/bind/data/BoundHelper;->bindBackground(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V

    .line 53
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->view:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindContentDescriptionKey:Ljava/lang/Integer;

    invoke-static {v0, v1, p1}, Lcom/google/android/libraries/bind/data/BoundHelper;->bindContentDescription(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V

    .line 54
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->view:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindEnabledKey:Ljava/lang/Integer;

    invoke-static {v0, v1, p1}, Lcom/google/android/libraries/bind/data/BoundHelper;->bindEnabled(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->view:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindInvisibilityKey:Ljava/lang/Integer;

    invoke-static {v0, v1, p1}, Lcom/google/android/libraries/bind/data/BoundHelper;->bindInvisibility(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->view:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindMinHeightKey:Ljava/lang/Integer;

    invoke-static {v0, v1, p1}, Lcom/google/android/libraries/bind/data/BoundHelper;->bindMinHeight(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->view:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindOnClickListenerKey:Ljava/lang/Integer;

    invoke-static {v0, v1, p1}, Lcom/google/android/libraries/bind/data/BoundHelper;->bindOnClickListener(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->view:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindPaddingTopKey:Ljava/lang/Integer;

    invoke-static {v0, v1, p1}, Lcom/google/android/libraries/bind/data/BoundHelper;->bindPaddingTop(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->view:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindTransitionNameKey:Ljava/lang/Integer;

    invoke-static {v0, v1, p1}, Lcom/google/android/libraries/bind/data/BoundHelper;->bindTransitionName(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->view:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/BoundHelper;->bindVisibilityKey:Ljava/lang/Integer;

    invoke-static {v0, v1, p1}, Lcom/google/android/libraries/bind/data/BoundHelper;->bindVisibility(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;)V

    .line 61
    return-void
.end method
