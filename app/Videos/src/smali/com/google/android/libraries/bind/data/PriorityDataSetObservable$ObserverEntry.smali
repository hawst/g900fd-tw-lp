.class Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;
.super Ljava/lang/Object;
.source "PriorityDataSetObservable.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/bind/data/PriorityDataSetObservable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ObserverEntry"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;",
        ">;"
    }
.end annotation


# instance fields
.field public final observer:Landroid/database/DataSetObserver;

.field public final priority:I


# direct methods
.method public constructor <init>(Landroid/database/DataSetObserver;I)V
    .locals 0
    .param p1, "observer"    # Landroid/database/DataSetObserver;
    .param p2, "priority"    # I

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;->observer:Landroid/database/DataSetObserver;

    .line 25
    iput p2, p0, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;->priority:I

    .line 26
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;)I
    .locals 2
    .param p1, "other"    # Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;

    .prologue
    .line 30
    iget v0, p0, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;->priority:I

    iget v1, p1, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;->priority:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;->priority:I

    iget v1, p1, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;->priority:I

    if-ge v0, v1, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 19
    check-cast p1, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;->compareTo(Lcom/google/android/libraries/bind/data/PriorityDataSetObservable$ObserverEntry;)I

    move-result v0

    return v0
.end method
