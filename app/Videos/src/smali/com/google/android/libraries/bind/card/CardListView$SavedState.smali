.class Lcom/google/android/libraries/bind/card/CardListView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "CardListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/bind/card/CardListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/libraries/bind/card/CardListView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final cardId:Ljava/lang/Object;

.field public final offsetFromTop:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 584
    new-instance v0, Lcom/google/android/libraries/bind/card/CardListView$SavedState$1;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/card/CardListView$SavedState$1;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 598
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 599
    const-class v0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/libraries/bind/util/ParcelUtil;->readObjectFromParcel(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->cardId:Ljava/lang/Object;

    .line 600
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->offsetFromTop:I

    .line 601
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/libraries/bind/card/CardListView$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/libraries/bind/card/CardListView$1;

    .prologue
    .line 561
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/card/CardListView$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Landroid/os/Parcelable;Ljava/lang/Object;I)V
    .locals 0
    .param p1, "superState"    # Landroid/os/Parcelable;
    .param p2, "cardId"    # Ljava/lang/Object;
    .param p3, "offsetFromTop"    # I

    .prologue
    .line 566
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 567
    iput-object p2, p0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->cardId:Ljava/lang/Object;

    .line 568
    iput p3, p0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->offsetFromTop:I

    .line 569
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->cardId:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 573
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 574
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->cardId:Ljava/lang/Object;

    invoke-static {v0, p1, p2}, Lcom/google/android/libraries/bind/util/ParcelUtil;->writeObjectToParcel(Ljava/lang/Object;Landroid/os/Parcel;I)V

    .line 575
    iget v0, p0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->offsetFromTop:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 576
    return-void
.end method
