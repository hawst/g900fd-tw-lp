.class public final Lcom/google/android/libraries/bind/collections/RingBuffer;
.super Ljava/lang/Object;
.source "RingBuffer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private elements:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TE;"
        }
    .end annotation
.end field

.field private head:I

.field private size:I


# direct methods
.method private constructor <init>(I)V
    .locals 0
    .param p1, "capacity"    # I

    .prologue
    .line 22
    .local p0, "this":Lcom/google/android/libraries/bind/collections/RingBuffer;, "Lcom/google/android/libraries/bind/collections/RingBuffer<TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/collections/RingBuffer;->clear(I)V

    .line 24
    return-void
.end method

.method private clear(I)V
    .locals 2
    .param p1, "capacity"    # I

    .prologue
    .local p0, "this":Lcom/google/android/libraries/bind/collections/RingBuffer;, "Lcom/google/android/libraries/bind/collections/RingBuffer<TE;>;"
    const/4 v1, 0x0

    .line 40
    new-array v0, p1, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    .line 41
    iput v1, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->head:I

    .line 42
    iput v1, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->size:I

    .line 43
    return-void
.end method

.method public static create()Lcom/google/android/libraries/bind/collections/RingBuffer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/libraries/bind/collections/RingBuffer",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/libraries/bind/collections/RingBuffer;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Lcom/google/android/libraries/bind/collections/RingBuffer;-><init>(I)V

    return-object v0
.end method

.method private indexOf(I)I
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 103
    .local p0, "this":Lcom/google/android/libraries/bind/collections/RingBuffer;, "Lcom/google/android/libraries/bind/collections/RingBuffer<TE;>;"
    iget v0, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->head:I

    add-int/2addr v0, p1

    iget-object v1, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    return v0
.end method

.method private maybeDoubleCapacity()V
    .locals 5

    .prologue
    .local p0, "this":Lcom/google/android/libraries/bind/collections/RingBuffer;, "Lcom/google/android/libraries/bind/collections/RingBuffer<TE;>;"
    const/4 v4, 0x0

    .line 107
    iget v2, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->size:I

    iget-object v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 119
    :goto_0
    return-void

    .line 112
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    array-length v2, v2

    shl-int/lit8 v2, v2, 0x1

    new-array v1, v2, [Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    .line 114
    .local v1, "newElements":[Ljava/lang/Object;, "[TE;"
    iget-object v2, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    array-length v2, v2

    iget v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->head:I

    sub-int v0, v2, v3

    .line 115
    .local v0, "fromHeadtoEnd":I
    iget-object v2, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->head:I

    invoke-static {v2, v3, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 116
    iget-object v2, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->size:I

    sub-int/2addr v3, v0

    invoke-static {v2, v4, v1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 117
    iput-object v1, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    .line 118
    iput v4, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->head:I

    goto :goto_0
.end method

.method private maybeHalveCapacity()V
    .locals 7

    .prologue
    .local p0, "this":Lcom/google/android/libraries/bind/collections/RingBuffer;, "Lcom/google/android/libraries/bind/collections/RingBuffer<TE;>;"
    const/4 v6, 0x0

    .line 122
    iget v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->size:I

    iget-object v4, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    array-length v4, v4

    shr-int/lit8 v4, v4, 0x2

    if-gt v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    array-length v3, v3

    const/16 v4, 0x10

    if-gt v3, v4, :cond_1

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    iget-object v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    array-length v3, v3

    shr-int/lit8 v3, v3, 0x1

    new-array v2, v3, [Ljava/lang/Object;

    check-cast v2, [Ljava/lang/Object;

    .line 129
    .local v2, "newElements":[Ljava/lang/Object;, "[TE;"
    iget v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->size:I

    add-int/lit8 v3, v3, -0x1

    invoke-direct {p0, v3}, Lcom/google/android/libraries/bind/collections/RingBuffer;->indexOf(I)I

    move-result v1

    .line 130
    .local v1, "lastIndex":I
    iget v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->head:I

    if-lt v1, v3, :cond_2

    .line 131
    iget-object v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->head:I

    iget v5, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->size:I

    invoke-static {v3, v4, v2, v6, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 137
    :goto_1
    iput-object v2, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    .line 138
    iput v6, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->head:I

    goto :goto_0

    .line 133
    :cond_2
    iget-object v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    array-length v3, v3

    iget v4, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->head:I

    sub-int v0, v3, v4

    .line 134
    .local v0, "fromHeadToEnd":I
    iget-object v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->head:I

    invoke-static {v3, v4, v2, v6, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 135
    iget-object v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->size:I

    sub-int/2addr v4, v0

    invoke-static {v3, v6, v2, v0, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1
.end method


# virtual methods
.method public addLast(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "this":Lcom/google/android/libraries/bind/collections/RingBuffer;, "Lcom/google/android/libraries/bind/collections/RingBuffer<TE;>;"
    .local p1, "e":Ljava/lang/Object;, "TE;"
    invoke-direct {p0}, Lcom/google/android/libraries/bind/collections/RingBuffer;->maybeDoubleCapacity()V

    .line 72
    iget-object v0, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    iget v1, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->size:I

    invoke-direct {p0, v1}, Lcom/google/android/libraries/bind/collections/RingBuffer;->indexOf(I)I

    move-result v1

    aput-object p1, v0, v1

    .line 73
    iget v0, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->size:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->size:I

    .line 74
    return-void
.end method

.method public get(I)Ljava/lang/Object;
    .locals 2
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 77
    .local p0, "this":Lcom/google/android/libraries/bind/collections/RingBuffer;, "Lcom/google/android/libraries/bind/collections/RingBuffer<TE;>;"
    iget-object v0, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/collections/RingBuffer;->indexOf(I)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 31
    .local p0, "this":Lcom/google/android/libraries/bind/collections/RingBuffer;, "Lcom/google/android/libraries/bind/collections/RingBuffer<TE;>;"
    iget v0, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->size:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove(I)Ljava/lang/Object;
    .locals 9
    .param p1, "i"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/libraries/bind/collections/RingBuffer;, "Lcom/google/android/libraries/bind/collections/RingBuffer<TE;>;"
    const/4 v8, 0x0

    .line 81
    iget v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->size:I

    add-int/lit8 v3, v3, -0x1

    invoke-direct {p0, v3}, Lcom/google/android/libraries/bind/collections/RingBuffer;->indexOf(I)I

    move-result v0

    .line 82
    .local v0, "lastIndex":I
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/collections/RingBuffer;->indexOf(I)I

    move-result v1

    .line 83
    .local v1, "removeIndex":I
    iget-object v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    aget-object v2, v3, v1

    .line 85
    .local v2, "result":Ljava/lang/Object;, "TE;"
    if-gt v1, v0, :cond_0

    .line 87
    iget-object v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    add-int/lit8 v4, v1, 0x1

    iget-object v5, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    sub-int v6, v0, v1

    invoke-static {v3, v4, v5, v1, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 88
    iget-object v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    aput-object v8, v3, v0

    .line 96
    :goto_0
    iget v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->size:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->size:I

    .line 97
    invoke-direct {p0}, Lcom/google/android/libraries/bind/collections/RingBuffer;->maybeHalveCapacity()V

    .line 98
    return-object v2

    .line 91
    :cond_0
    iget-object v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->head:I

    iget-object v5, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    iget v6, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->head:I

    add-int/lit8 v6, v6, 0x1

    iget v7, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->head:I

    sub-int v7, v1, v7

    invoke-static {v3, v4, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 92
    iget-object v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->elements:[Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->head:I

    aput-object v8, v3, v4

    .line 93
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/google/android/libraries/bind/collections/RingBuffer;->indexOf(I)I

    move-result v3

    iput v3, p0, Lcom/google/android/libraries/bind/collections/RingBuffer;->head:I

    goto :goto_0
.end method
