.class public Lcom/google/android/libraries/bind/async/AsyncUtil;
.super Ljava/lang/Object;
.source "AsyncUtil.java"


# static fields
.field private static immediateExecutor:Ljava/util/concurrent/Executor;

.field private static mainThreadExecutor:Ljava/util/concurrent/Executor;

.field private static final mainThreadHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/libraries/bind/async/AsyncUtil;->mainThreadHandler:Landroid/os/Handler;

    .line 15
    new-instance v0, Lcom/google/android/libraries/bind/async/AsyncUtil$1;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/async/AsyncUtil$1;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/async/AsyncUtil;->mainThreadExecutor:Ljava/util/concurrent/Executor;

    .line 22
    new-instance v0, Lcom/google/android/libraries/bind/async/AsyncUtil$2;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/async/AsyncUtil$2;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/async/AsyncUtil;->immediateExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic access$000()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/google/android/libraries/bind/async/AsyncUtil;->mainThreadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static checkMainThread()V
    .locals 2

    .prologue
    .line 30
    invoke-static {}, Lcom/google/android/libraries/bind/async/AsyncUtil;->isMainThread()Z

    move-result v0

    const-string v1, "Not on the main thread"

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(ZLjava/lang/String;)V

    .line 31
    return-void
.end method

.method public static immediateExecutor()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/libraries/bind/async/AsyncUtil;->immediateExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public static isMainThread()Z
    .locals 2

    .prologue
    .line 38
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static mainThreadExecutor()Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/google/android/libraries/bind/async/AsyncUtil;->mainThreadExecutor:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public static mainThreadHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/google/android/libraries/bind/async/AsyncUtil;->mainThreadHandler:Landroid/os/Handler;

    return-object v0
.end method
