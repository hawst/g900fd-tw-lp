.class public Lcom/google/android/libraries/bind/widget/BoundLinearLayout;
.super Landroid/widget/LinearLayout;
.source "BoundLinearLayout.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/Bound;


# instance fields
.field private final boundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;->makeBoundHelper(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/libraries/bind/data/BoundHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;->boundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    invoke-virtual {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;->makeBoundHelper(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/libraries/bind/data/BoundHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;->boundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;

    .line 32
    return-void
.end method


# virtual methods
.method protected makeBoundHelper(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/libraries/bind/data/BoundHelper;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/libraries/bind/data/BoundHelper;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/android/libraries/bind/data/BoundHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    return-object v0
.end method

.method public onRtlPropertiesChanged(I)V
    .locals 0
    .param p1, "layoutDirection"    # I

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRtlPropertiesChanged(I)V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;->requestLayout()V

    .line 49
    return-void
.end method

.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 1
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundLinearLayout;->boundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/BoundHelper;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 41
    return-void
.end method
