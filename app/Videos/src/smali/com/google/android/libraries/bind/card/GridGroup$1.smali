.class Lcom/google/android/libraries/bind/card/GridGroup$1;
.super Ljava/lang/Object;
.source "GridGroup.java"

# interfaces
.implements Lcom/google/android/libraries/bind/card/ViewGenerator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/libraries/bind/card/GridGroup;->makeRowViewGenerator(ILjava/util/List;I)Lcom/google/android/libraries/bind/card/ViewGenerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/bind/card/GridGroup;

.field final synthetic val$cards:Ljava/util/List;

.field final synthetic val$nColumns:I

.field final synthetic val$row:I


# direct methods
.method constructor <init>(Lcom/google/android/libraries/bind/card/GridGroup;Ljava/util/List;II)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/GridGroup$1;->this$0:Lcom/google/android/libraries/bind/card/GridGroup;

    iput-object p2, p0, Lcom/google/android/libraries/bind/card/GridGroup$1;->val$cards:Ljava/util/List;

    iput p3, p0, Lcom/google/android/libraries/bind/card/GridGroup$1;->val$nColumns:I

    iput p4, p0, Lcom/google/android/libraries/bind/card/GridGroup$1;->val$row:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getLayoutParamsForRow(II)Landroid/widget/LinearLayout$LayoutParams;
    .locals 3
    .param p1, "layoutRowHeight"    # I
    .param p2, "columnSpan"    # I

    .prologue
    .line 222
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    int-to-float v2, p2

    invoke-direct {v0, v1, p1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    return-object v0
.end method


# virtual methods
.method public getCardCount()I
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/GridGroup$1;->val$cards:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCardIds()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/libraries/bind/card/GridGroup$1;->val$cards:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 210
    .local v0, "cardIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/GridGroup$1;->val$cards:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 211
    .local v1, "cardIndex":Ljava/lang/Integer;
    iget-object v3, p0, Lcom/google/android/libraries/bind/card/GridGroup$1;->this$0:Lcom/google/android/libraries/bind/card/GridGroup;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/libraries/bind/card/GridGroup;->getCardId(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 213
    .end local v1    # "cardIndex":Ljava/lang/Integer;
    :cond_0
    return-object v0
.end method

.method public getViewResId()I
    .locals 1

    .prologue
    .line 170
    sget v0, Lcom/google/android/libraries/bind/R$layout;->bind__grid_group_row:I

    return v0
.end method

.method public makeView(Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;
    .locals 16
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;

    .prologue
    .line 175
    # getter for: Lcom/google/android/libraries/bind/card/GridGroup;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;
    invoke-static {}, Lcom/google/android/libraries/bind/card/GridGroup;->access$000()Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v11

    const-string v12, "Making views %d out of %d columns for row %d"

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/libraries/bind/card/GridGroup$1;->val$cards:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/libraries/bind/card/GridGroup$1;->val$nColumns:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/libraries/bind/card/GridGroup$1;->val$row:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 176
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/bind/card/GridGroup$1;->getViewResId()I

    move-result v11

    new-instance v12, Landroid/view/ViewGroup$LayoutParams;

    const/4 v13, -0x1

    const/4 v14, -0x2

    invoke-direct {v12, v13, v14}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-virtual {v0, v11, v1, v12}, Lcom/google/android/libraries/bind/view/ViewHeap;->get(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup;

    .line 178
    .local v10, "rowViewGroup":Landroid/view/ViewGroup;
    invoke-virtual {v10}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v11

    if-nez v11, :cond_0

    const/4 v11, 0x1

    :goto_0
    invoke-static {v11}, Lcom/google/android/libraries/bind/util/Util;->checkPrecondition(Z)V

    .line 180
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/libraries/bind/card/GridGroup$1;->this$0:Lcom/google/android/libraries/bind/card/GridGroup;

    iget v11, v11, Lcom/google/android/libraries/bind/card/GridGroup;->rowHeightPx:I

    if-lez v11, :cond_1

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/libraries/bind/card/GridGroup$1;->this$0:Lcom/google/android/libraries/bind/card/GridGroup;

    iget v8, v11, Lcom/google/android/libraries/bind/card/GridGroup;->rowHeightPx:I

    .line 182
    .local v8, "layoutRowHeight":I
    :goto_1
    const/4 v5, 0x0

    .line 183
    .local v5, "columnsUsedForRow":I
    const/4 v2, 0x0

    .local v2, "card":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/libraries/bind/card/GridGroup$1;->val$cards:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-ge v2, v11, :cond_2

    .line 184
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/libraries/bind/card/GridGroup$1;->this$0:Lcom/google/android/libraries/bind/card/GridGroup;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/libraries/bind/card/GridGroup$1;->val$cards:Ljava/util/List;

    invoke-interface {v11, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/libraries/bind/card/GridGroup$1;->val$nColumns:I

    invoke-virtual {v12, v11, v13}, Lcom/google/android/libraries/bind/card/GridGroup;->getColumnSpanForCard(II)I

    move-result v4

    .line 185
    .local v4, "columnSpanForCard":I
    add-int/2addr v5, v4

    .line 186
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v4}, Lcom/google/android/libraries/bind/card/GridGroup$1;->getLayoutParamsForRow(II)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v7

    .line 188
    .local v7, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/libraries/bind/card/GridGroup$1;->this$0:Lcom/google/android/libraries/bind/card/GridGroup;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/libraries/bind/card/GridGroup$1;->val$cards:Ljava/util/List;

    invoke-interface {v11, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    move-object/from16 v0, p3

    invoke-virtual {v12, v0, v11, v7}, Lcom/google/android/libraries/bind/card/GridGroup;->makeCardView(Lcom/google/android/libraries/bind/view/ViewHeap;ILandroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v3

    .line 189
    .local v3, "cardView":Landroid/view/View;
    invoke-virtual {v3, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 190
    invoke-virtual {v10, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 183
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 178
    .end local v2    # "card":I
    .end local v3    # "cardView":Landroid/view/View;
    .end local v4    # "columnSpanForCard":I
    .end local v5    # "columnsUsedForRow":I
    .end local v7    # "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v8    # "layoutRowHeight":I
    :cond_0
    const/4 v11, 0x0

    goto :goto_0

    .line 180
    :cond_1
    const/4 v8, -0x1

    goto :goto_1

    .line 195
    .restart local v2    # "card":I
    .restart local v5    # "columnsUsedForRow":I
    .restart local v8    # "layoutRowHeight":I
    :cond_2
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/libraries/bind/card/GridGroup$1;->val$nColumns:I

    sub-int v9, v11, v5

    .line 196
    .local v9, "remainingColumnSpan":I
    if-lez v9, :cond_3

    .line 197
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/google/android/libraries/bind/card/GridGroup$1;->getLayoutParamsForRow(II)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v7

    .line 199
    .restart local v7    # "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/libraries/bind/card/GridGroup$1;->this$0:Lcom/google/android/libraries/bind/card/GridGroup;

    move-object/from16 v0, p3

    # invokes: Lcom/google/android/libraries/bind/card/GridGroup;->makeEmptyView(Landroid/view/ViewGroup$LayoutParams;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;
    invoke-static {v11, v7, v0}, Lcom/google/android/libraries/bind/card/GridGroup;->access$100(Lcom/google/android/libraries/bind/card/GridGroup;Landroid/view/ViewGroup$LayoutParams;Lcom/google/android/libraries/bind/view/ViewHeap;)Landroid/view/View;

    move-result-object v6

    .line 200
    .local v6, "emptyView":Landroid/view/View;
    invoke-virtual {v6, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 201
    invoke-virtual {v10, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 204
    .end local v6    # "emptyView":Landroid/view/View;
    .end local v7    # "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_3
    return-object v10
.end method
