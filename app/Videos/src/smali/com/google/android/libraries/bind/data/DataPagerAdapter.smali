.class public abstract Lcom/google/android/libraries/bind/data/DataPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "DataPagerAdapter.java"

# interfaces
.implements Lcom/google/android/libraries/bind/bidi/BidiAwarePagerAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;
    }
.end annotation


# instance fields
.field private final errorPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

.field private isRtl:Z

.field private list:Lcom/google/android/libraries/bind/data/DataList;

.field private final loadingPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

.field private final observer:Lcom/google/android/libraries/bind/data/DataObserver;

.field private titleKey:Ljava/lang/Integer;

.field protected final viewHeap:Lcom/google/android/libraries/bind/view/ViewHeap;

.field private final viewMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/view/ViewHeap;)V
    .locals 2
    .param p1, "viewHeap"    # Lcom/google/android/libraries/bind/view/ViewHeap;

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->viewMap:Ljava/util/Map;

    .line 27
    new-instance v0, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    sget-object v1, Lcom/google/android/libraries/bind/data/ViewProvider;->DEFAULT_LOADING_VIEW_PROVIDER:Lcom/google/android/libraries/bind/data/ViewProvider;

    invoke-direct {v0, p0, v1}, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;-><init>(Lcom/google/android/libraries/bind/data/DataPagerAdapter;Lcom/google/android/libraries/bind/data/ViewProvider;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->loadingPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    .line 29
    new-instance v0, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    sget-object v1, Lcom/google/android/libraries/bind/data/ViewProvider;->DEFAULT_ERROR_VIEW_PROVIDER:Lcom/google/android/libraries/bind/data/ViewProvider;

    invoke-direct {v0, p0, v1}, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;-><init>(Lcom/google/android/libraries/bind/data/DataPagerAdapter;Lcom/google/android/libraries/bind/data/ViewProvider;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->errorPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    .line 37
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->viewHeap:Lcom/google/android/libraries/bind/view/ViewHeap;

    .line 38
    new-instance v0, Lcom/google/android/libraries/bind/data/DataPagerAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/data/DataPagerAdapter$1;-><init>(Lcom/google/android/libraries/bind/data/DataPagerAdapter;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->observer:Lcom/google/android/libraries/bind/data/DataObserver;

    .line 46
    return-void
.end method

.method private isInvalidPosition(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 106
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 131
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->loadingPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    if-ne p3, v1, :cond_0

    .line 132
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->loadingPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->destroyItem(Landroid/view/ViewGroup;)V

    .line 142
    :goto_0
    return-void

    .line 135
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->errorPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    if-ne p3, v1, :cond_1

    .line 136
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->errorPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->destroyItem(Landroid/view/ViewGroup;)V

    goto :goto_0

    .line 139
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->viewMap:Ljava/util/Map;

    invoke-interface {v1, p3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 140
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 141
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->onDestroyedView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 91
    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/DataList;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 176
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->getLogicalItemPosition(Ljava/lang/Object;)I

    move-result v0

    invoke-static {p0, v0}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getVisualPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v0

    return v0
.end method

.method public getList()Lcom/google/android/libraries/bind/data/DataList;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    return-object v0
.end method

.method public getLogicalItemPosition(Ljava/lang/Object;)I
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x2

    .line 187
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v3, :cond_1

    .line 201
    :cond_0
    :goto_0
    return v2

    .line 190
    :cond_1
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->loadingPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    if-ne p1, v3, :cond_3

    .line 191
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->didLastRefreshFail()Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    .line 193
    :cond_3
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->errorPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    if-ne p1, v3, :cond_5

    .line 194
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->didLastRefreshFail()Z

    move-result v3

    if-eqz v3, :cond_4

    :goto_2
    move v2, v1

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_2

    .line 196
    :cond_5
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1, p1}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v0

    .line 197
    .local v0, "positionForId":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    move v2, v0

    .line 201
    goto :goto_0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "visualPosition"    # I

    .prologue
    .line 223
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->titleKey:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->isInvalidPosition(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 224
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/view/PagerAdapter;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 227
    :goto_0
    return-object v1

    .line 226
    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v0

    .line 227
    .local v0, "logicalPosition":I
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->titleKey:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public abstract getView(Landroid/view/ViewGroup;ILcom/google/android/libraries/bind/data/Data;)Landroid/view/View;
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "visualPosition"    # I

    .prologue
    .line 111
    invoke-direct {p0, p2}, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->isInvalidPosition(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 112
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v3, :cond_0

    .line 113
    const/4 v0, 0x0

    .line 126
    :goto_0
    return-object v0

    .line 114
    :cond_0
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/DataList;->didLastRefreshFail()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 115
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->errorPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    invoke-virtual {v3, p1, p2}, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 117
    :cond_1
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->loadingPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    invoke-virtual {v3, p1, p2}, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 120
    :cond_2
    invoke-static {p0, p2}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v1

    .line 121
    .local v1, "logicalPosition":I
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3, v1}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    invoke-virtual {p0, p1, v1, v3}, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->getView(Landroid/view/ViewGroup;ILcom/google/android/libraries/bind/data/Data;)Landroid/view/View;

    move-result-object v2

    .line 122
    .local v2, "view":Landroid/view/View;
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 123
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v3, v1}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v0

    .line 124
    .local v0, "id":Ljava/lang/Object;
    sget v3, Lcom/google/android/libraries/bind/R$id;->bind__tagDataPagerAdapterObject:I

    invoke-virtual {v2, v3, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 125
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->viewMap:Ljava/util/Map;

    invoke-interface {v3, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public isRtl()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->isRtl:Z

    return v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->loadingPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    if-ne p2, v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->loadingPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->isViewFromObject(Landroid/view/View;)Z

    move-result v0

    .line 102
    :goto_0
    return v0

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->errorPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    if-ne p2, v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->errorPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->isViewFromObject(Landroid/view/View;)Z

    move-result v0

    goto :goto_0

    .line 102
    :cond_1
    sget v0, Lcom/google/android/libraries/bind/R$id;->bind__tagDataPagerAdapterObject:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/libraries/bind/util/Util;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public onDestroyedView(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 86
    return-void
.end method

.method public setList(Lcom/google/android/libraries/bind/data/DataList;)V
    .locals 2
    .param p1, "list"    # Lcom/google/android/libraries/bind/data/DataList;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->observer:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/DataList;->unregisterDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 76
    :cond_0
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    .line 77
    if-eqz p1, :cond_1

    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->observer:Lcom/google/android/libraries/bind/data/DataObserver;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/DataList;->registerDataObserver(Lcom/google/android/libraries/bind/data/DataObserver;)V

    .line 80
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->notifyDataSetChanged()V

    .line 81
    return-void
.end method

.method public setRtl(Z)V
    .locals 1
    .param p1, "isRtl"    # Z

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->isRtl:Z

    if-eq v0, p1, :cond_0

    .line 56
    iput-boolean p1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->isRtl:Z

    .line 57
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->notifyDataSetChanged()V

    .line 59
    :cond_0
    return-void
.end method

.method public tryGetViewAt(I)Landroid/view/View;
    .locals 2
    .param p1, "logicalPosition"    # I

    .prologue
    .line 205
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->isInvalidPosition(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 206
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    if-nez v1, :cond_0

    .line 207
    const/4 v1, 0x0

    .line 215
    :goto_0
    return-object v1

    .line 208
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->didLastRefreshFail()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 209
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->errorPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    # getter for: Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->view:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->access$000(Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 211
    :cond_1
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->loadingPagerHelper:Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;

    # getter for: Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->view:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;->access$000(Lcom/google/android/libraries/bind/data/DataPagerAdapter$ViewPagerHelper;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 214
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->list:Lcom/google/android/libraries/bind/data/DataList;

    invoke-virtual {v1, p1}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v0

    .line 215
    .local v0, "id":Ljava/lang/Object;
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/DataPagerAdapter;->viewMap:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    goto :goto_0
.end method
