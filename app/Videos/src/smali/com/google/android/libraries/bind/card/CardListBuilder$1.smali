.class Lcom/google/android/libraries/bind/card/CardListBuilder$1;
.super Lcom/google/android/libraries/bind/data/DataList;
.source "CardListBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/libraries/bind/card/CardListBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/libraries/bind/card/CardListBuilder;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/bind/card/CardListBuilder;I)V
    .locals 0
    .param p2, "x0"    # I

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardListBuilder$1;->this$0:Lcom/google/android/libraries/bind/card/CardListBuilder;

    invoke-direct {p0, p2}, Lcom/google/android/libraries/bind/data/DataList;-><init>(I)V

    return-void
.end method

.method private validateFilterIsImmediate(Lcom/google/android/libraries/bind/data/Filter;)V
    .locals 2
    .param p1, "filter"    # Lcom/google/android/libraries/bind/data/Filter;

    .prologue
    .line 50
    invoke-interface {p1}, Lcom/google/android/libraries/bind/data/Filter;->executor()Ljava/util/concurrent/Executor;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/bind/async/Queues;->BIND_IMMEDIATE:Lcom/google/android/libraries/bind/async/Queue;

    if-eq v0, v1, :cond_0

    .line 51
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 53
    :cond_0
    return-void
.end method


# virtual methods
.method public filter([ILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;
    .locals 1
    .param p1, "equalityFields"    # [I
    .param p2, "filter"    # Lcom/google/android/libraries/bind/data/Filter;

    .prologue
    .line 57
    invoke-direct {p0, p2}, Lcom/google/android/libraries/bind/card/CardListBuilder$1;->validateFilterIsImmediate(Lcom/google/android/libraries/bind/data/Filter;)V

    .line 58
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/bind/data/DataList;->filter([ILcom/google/android/libraries/bind/data/Filter;)Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v0

    return-object v0
.end method
