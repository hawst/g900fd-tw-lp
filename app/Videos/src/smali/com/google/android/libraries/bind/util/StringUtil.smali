.class public Lcom/google/android/libraries/bind/util/StringUtil;
.super Ljava/lang/Object;
.source "StringUtil.java"


# direct methods
.method public static getLongHash(Ljava/lang/String;)J
    .locals 8
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 8
    const-wide/16 v2, 0x0

    .line 9
    .local v2, "hash":J
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 10
    .local v0, "end":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 11
    const-wide/16 v4, 0x3f

    mul-long/2addr v4, v2

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    int-to-long v6, v6

    add-long v2, v4, v6

    .line 10
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 13
    :cond_0
    return-wide v2
.end method
