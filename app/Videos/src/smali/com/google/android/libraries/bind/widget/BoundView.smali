.class public Lcom/google/android/libraries/bind/widget/BoundView;
.super Landroid/view/View;
.source "BoundView.java"

# interfaces
.implements Lcom/google/android/libraries/bind/data/Bound;


# instance fields
.field private final boundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/bind/widget/BoundView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/bind/widget/BoundView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    new-instance v0, Lcom/google/android/libraries/bind/data/BoundHelper;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/android/libraries/bind/data/BoundHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundView;->boundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;

    .line 28
    return-void
.end method


# virtual methods
.method public updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 1
    .param p1, "data"    # Lcom/google/android/libraries/bind/data/Data;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundView;->boundHelper:Lcom/google/android/libraries/bind/data/BoundHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/BoundHelper;->updateBoundData(Lcom/google/android/libraries/bind/data/Data;)V

    .line 33
    return-void
.end method
