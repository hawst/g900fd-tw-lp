.class public final Lcom/google/android/repolib/common/LimitingIterator;
.super Ljava/lang/Object;
.source "LimitingIterator.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private current:I

.field private final iterator:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private final limit:I


# direct methods
.method private constructor <init>(Ljava/util/Iterator;I)V
    .locals 1
    .param p2, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<+TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p0, "this":Lcom/google/android/repolib/common/LimitingIterator;, "Lcom/google/android/repolib/common/LimitingIterator<TT;>;"
    .local p1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<+TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/google/android/repolib/common/LimitingIterator;->iterator:Ljava/util/Iterator;

    .line 16
    iput p2, p0, Lcom/google/android/repolib/common/LimitingIterator;->limit:I

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/repolib/common/LimitingIterator;->current:I

    .line 18
    return-void
.end method

.method public static limitingIterator(Ljava/util/Iterator;I)Ljava/util/Iterator;
    .locals 1
    .param p1, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Iterator",
            "<+TT;>;I)",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<+TT;>;"
    new-instance v0, Lcom/google/android/repolib/common/LimitingIterator;

    invoke-direct {v0, p0, p1}, Lcom/google/android/repolib/common/LimitingIterator;-><init>(Ljava/util/Iterator;I)V

    return-object v0
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 27
    .local p0, "this":Lcom/google/android/repolib/common/LimitingIterator;, "Lcom/google/android/repolib/common/LimitingIterator<TT;>;"
    iget v0, p0, Lcom/google/android/repolib/common/LimitingIterator;->current:I

    iget v1, p0, Lcom/google/android/repolib/common/LimitingIterator;->limit:I

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/repolib/common/LimitingIterator;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "this":Lcom/google/android/repolib/common/LimitingIterator;, "Lcom/google/android/repolib/common/LimitingIterator<TT;>;"
    iget v0, p0, Lcom/google/android/repolib/common/LimitingIterator;->current:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/repolib/common/LimitingIterator;->current:I

    iget v1, p0, Lcom/google/android/repolib/common/LimitingIterator;->limit:I

    if-lt v0, v1, :cond_0

    .line 33
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 35
    :cond_0
    iget-object v0, p0, Lcom/google/android/repolib/common/LimitingIterator;->iterator:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 40
    .local p0, "this":Lcom/google/android/repolib/common/LimitingIterator;, "Lcom/google/android/repolib/common/LimitingIterator<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
