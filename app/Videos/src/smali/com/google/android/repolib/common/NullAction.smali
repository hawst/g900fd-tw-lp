.class public final Lcom/google/android/repolib/common/NullAction;
.super Ljava/lang/Object;
.source "NullAction.java"

# interfaces
.implements Lcom/google/android/repolib/common/Action;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Action",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final NULL_ACTION:Lcom/google/android/repolib/common/NullAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/NullAction",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    new-instance v0, Lcom/google/android/repolib/common/NullAction;

    invoke-direct {v0}, Lcom/google/android/repolib/common/NullAction;-><init>()V

    sput-object v0, Lcom/google/android/repolib/common/NullAction;->NULL_ACTION:Lcom/google/android/repolib/common/NullAction;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/google/android/repolib/common/NullAction;, "Lcom/google/android/repolib/common/NullAction<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    return-void
.end method

.method public static nullAction()Lcom/google/android/repolib/common/Action;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/repolib/common/Action",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/repolib/common/NullAction;->NULL_ACTION:Lcom/google/android/repolib/common/NullAction;

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lcom/google/android/repolib/common/NullAction;, "Lcom/google/android/repolib/common/NullAction<TT;>;"
    .local p1, "object":Ljava/lang/Object;, "TT;"
    return-void
.end method
