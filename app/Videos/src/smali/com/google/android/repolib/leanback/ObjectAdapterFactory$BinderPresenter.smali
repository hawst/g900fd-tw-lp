.class final Lcom/google/android/repolib/leanback/ObjectAdapterFactory$BinderPresenter;
.super Landroid/support/v17/leanback/widget/Presenter;
.source "ObjectAdapterFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/repolib/leanback/ObjectAdapterFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "BinderPresenter"
.end annotation


# instance fields
.field private final binder:Lcom/google/android/repolib/common/Binder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Binder",
            "<*+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final viewType:I


# direct methods
.method constructor <init>(ILcom/google/android/repolib/common/Binder;)V
    .locals 1
    .param p1, "viewType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/repolib/common/Binder",
            "<*+",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 41
    .local p2, "binder":Lcom/google/android/repolib/common/Binder;, "Lcom/google/android/repolib/common/Binder<*+Landroid/view/View;>;"
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/Presenter;-><init>()V

    .line 42
    iput p1, p0, Lcom/google/android/repolib/leanback/ObjectAdapterFactory$BinderPresenter;->viewType:I

    .line 43
    invoke-static {p2}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Binder;

    iput-object v0, p0, Lcom/google/android/repolib/leanback/ObjectAdapterFactory$BinderPresenter;->binder:Lcom/google/android/repolib/common/Binder;

    .line 44
    return-void
.end method


# virtual methods
.method public onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V
    .locals 2
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/repolib/leanback/ObjectAdapterFactory$BinderPresenter;->binder:Lcom/google/android/repolib/common/Binder;

    iget-object v1, p1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    invoke-interface {v0, p2, v1}, Lcom/google/android/repolib/common/Binder;->bind(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 56
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .locals 4
    .param p1, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 48
    new-instance v0, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget v2, p0, Lcom/google/android/repolib/leanback/ObjectAdapterFactory$BinderPresenter;->viewType:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 0
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 60
    return-void
.end method

.method public setOnClickListener(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 64
    return-void
.end method
