.class final Lcom/google/android/repolib/leanback/ListRowRepository$EntityListRow;
.super Landroid/support/v17/leanback/widget/ListRow;
.source "ListRowRepository.java"

# interfaces
.implements Lcom/google/android/repolib/common/Entity;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/repolib/leanback/ListRowRepository;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "EntityListRow"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/support/v17/leanback/widget/ListRow;",
        "Lcom/google/android/repolib/common/Entity;"
    }
.end annotation


# instance fields
.field private final id:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 128
    .local p0, "this":Lcom/google/android/repolib/leanback/ListRowRepository$EntityListRow;, "Lcom/google/android/repolib/leanback/ListRowRepository$EntityListRow<TT;>;"
    .local p2, "adapter":Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;, "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter<+TT;>;"
    new-instance v1, Landroid/support/v17/leanback/widget/HeaderItem;

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Landroid/support/v17/leanback/widget/HeaderItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v1, p2}, Landroid/support/v17/leanback/widget/ListRow;-><init>(Landroid/support/v17/leanback/widget/HeaderItem;Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 129
    iput-object p1, p0, Lcom/google/android/repolib/leanback/ListRowRepository$EntityListRow;->id:Ljava/lang/String;

    .line 130
    return-void
.end method


# virtual methods
.method public final getEntityId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    .local p0, "this":Lcom/google/android/repolib/leanback/ListRowRepository$EntityListRow;, "Lcom/google/android/repolib/leanback/ListRowRepository$EntityListRow<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/leanback/ListRowRepository$EntityListRow;->id:Ljava/lang/String;

    return-object v0
.end method
