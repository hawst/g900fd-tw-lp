.class public final Lcom/google/android/repolib/common/LogErrorThrowableAction;
.super Ljava/lang/Object;
.source "LogErrorThrowableAction.java"

# interfaces
.implements Lcom/google/android/repolib/common/Action;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Action",
        "<",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field private final action:Lcom/google/android/repolib/common/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Action",
            "<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field private final tag:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/google/android/repolib/common/Action;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/repolib/common/Action",
            "<",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 15
    .local p2, "action":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<Ljava/lang/Throwable;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-static {p1}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/repolib/common/LogErrorThrowableAction;->tag:Ljava/lang/String;

    .line 17
    invoke-static {p2}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Action;

    iput-object v0, p0, Lcom/google/android/repolib/common/LogErrorThrowableAction;->action:Lcom/google/android/repolib/common/Action;

    .line 18
    return-void
.end method

.method public static logErrorThrowableAction(Ljava/lang/String;Lcom/google/android/repolib/common/Action;)Lcom/google/android/repolib/common/Action;
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/repolib/common/Action",
            "<",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lcom/google/android/repolib/common/Action",
            "<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    .local p1, "action":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<Ljava/lang/Throwable;>;"
    new-instance v0, Lcom/google/android/repolib/common/LogErrorThrowableAction;

    invoke-direct {v0, p0, p1}, Lcom/google/android/repolib/common/LogErrorThrowableAction;-><init>(Ljava/lang/String;Lcom/google/android/repolib/common/Action;)V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 8
    check-cast p1, Ljava/lang/Throwable;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/repolib/common/LogErrorThrowableAction;->apply(Ljava/lang/Throwable;)V

    return-void
.end method

.method public apply(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/repolib/common/LogErrorThrowableAction;->tag:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 34
    iget-object v0, p0, Lcom/google/android/repolib/common/LogErrorThrowableAction;->action:Lcom/google/android/repolib/common/Action;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/common/Action;->apply(Ljava/lang/Object;)V

    .line 35
    return-void
.end method
