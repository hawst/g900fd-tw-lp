.class public final Lcom/google/android/repolib/common/EmptyIndexer;
.super Ljava/lang/Object;
.source "EmptyIndexer.java"

# interfaces
.implements Lcom/google/android/repolib/common/Indexer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Indexer",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final EMPTY_INDEXER:Lcom/google/android/repolib/common/EmptyIndexer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    new-instance v0, Lcom/google/android/repolib/common/EmptyIndexer;

    invoke-direct {v0}, Lcom/google/android/repolib/common/EmptyIndexer;-><init>()V

    sput-object v0, Lcom/google/android/repolib/common/EmptyIndexer;->EMPTY_INDEXER:Lcom/google/android/repolib/common/EmptyIndexer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 8
    .local p0, "this":Lcom/google/android/repolib/common/EmptyIndexer;, "Lcom/google/android/repolib/common/EmptyIndexer<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    return-void
.end method

.method public static emptyIndexer()Lcom/google/android/repolib/common/Indexer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/repolib/common/Indexer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 13
    sget-object v0, Lcom/google/android/repolib/common/EmptyIndexer;->EMPTY_INDEXER:Lcom/google/android/repolib/common/EmptyIndexer;

    return-object v0
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lcom/google/android/repolib/common/EmptyIndexer;, "Lcom/google/android/repolib/common/EmptyIndexer<TT;>;"
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 24
    .local p0, "this":Lcom/google/android/repolib/common/EmptyIndexer;, "Lcom/google/android/repolib/common/EmptyIndexer<TT;>;"
    const/4 v0, 0x0

    return v0
.end method
