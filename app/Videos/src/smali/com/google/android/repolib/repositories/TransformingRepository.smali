.class public final Lcom/google/android/repolib/repositories/TransformingRepository;
.super Ljava/lang/Object;
.source "TransformingRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/repolib/repositories/TransformingRepository$1;,
        Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator;
    }
.end annotation


# direct methods
.method public static asyncTransformingRepository(Ljava/util/concurrent/Executor;Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Factory;)Lcom/google/android/repolib/repositories/Repository;
    .locals 2
    .param p0, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "F:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TF;>;",
            "Lcom/google/android/repolib/common/Factory",
            "<+TT;TF;>;)",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "fromRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<TF;>;"
    .local p2, "transformingFactory":Lcom/google/android/repolib/common/Factory;, "Lcom/google/android/repolib/common/Factory<+TT;TF;>;"
    new-instance v0, Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator;-><init>(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Factory;Lcom/google/android/repolib/repositories/TransformingRepository$1;)V

    invoke-static {p0, v0, p1}, Lcom/google/android/repolib/repositories/CreatorRepository;->asyncCreatorRepository(Ljava/util/concurrent/Executor;Lcom/google/android/repolib/common/Creator;Lcom/google/android/repolib/observers/Observable;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v0

    return-object v0
.end method

.method public static transformingRepository(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Factory;)Lcom/google/android/repolib/repositories/Repository;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "F:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TF;>;",
            "Lcom/google/android/repolib/common/Factory",
            "<+TT;TF;>;)",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "fromRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<TF;>;"
    .local p1, "transformingFactory":Lcom/google/android/repolib/common/Factory;, "Lcom/google/android/repolib/common/Factory<+TT;TF;>;"
    new-instance v0, Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator;-><init>(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Factory;Lcom/google/android/repolib/repositories/TransformingRepository$1;)V

    invoke-static {v0, p0}, Lcom/google/android/repolib/repositories/CreatorRepository;->creatorRepository(Lcom/google/android/repolib/common/Creator;Lcom/google/android/repolib/observers/Observable;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v0

    return-object v0
.end method
