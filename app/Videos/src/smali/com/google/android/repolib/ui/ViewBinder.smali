.class public final Lcom/google/android/repolib/ui/ViewBinder;
.super Ljava/lang/Object;
.source "ViewBinder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final binder:Lcom/google/android/repolib/common/Binder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Binder",
            "<+TT;+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final viewType:I


# direct methods
.method private constructor <init>(ILcom/google/android/repolib/common/Binder;)V
    .locals 1
    .param p1, "viewType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/repolib/common/Binder",
            "<+TT;+",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 17
    .local p0, "this":Lcom/google/android/repolib/ui/ViewBinder;, "Lcom/google/android/repolib/ui/ViewBinder<TT;>;"
    .local p2, "binder":Lcom/google/android/repolib/common/Binder;, "Lcom/google/android/repolib/common/Binder<+TT;+Landroid/view/View;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput p1, p0, Lcom/google/android/repolib/ui/ViewBinder;->viewType:I

    .line 19
    invoke-static {p2}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Binder;

    iput-object v0, p0, Lcom/google/android/repolib/ui/ViewBinder;->binder:Lcom/google/android/repolib/common/Binder;

    .line 20
    return-void
.end method

.method public static viewBinder(ILcom/google/android/repolib/common/Binder;)Lcom/google/android/repolib/ui/ViewBinder;
    .locals 1
    .param p0, "viewType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Lcom/google/android/repolib/common/Binder",
            "<+TT;+",
            "Landroid/view/View;",
            ">;)",
            "Lcom/google/android/repolib/ui/ViewBinder",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 25
    .local p1, "binder":Lcom/google/android/repolib/common/Binder;, "Lcom/google/android/repolib/common/Binder<+TT;+Landroid/view/View;>;"
    new-instance v0, Lcom/google/android/repolib/ui/ViewBinder;

    invoke-direct {v0, p0, p1}, Lcom/google/android/repolib/ui/ViewBinder;-><init>(ILcom/google/android/repolib/common/Binder;)V

    return-object v0
.end method


# virtual methods
.method public getBinder()Lcom/google/android/repolib/common/Binder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/repolib/common/Binder",
            "<+TT;+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "this":Lcom/google/android/repolib/ui/ViewBinder;, "Lcom/google/android/repolib/ui/ViewBinder<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/ui/ViewBinder;->binder:Lcom/google/android/repolib/common/Binder;

    return-object v0
.end method

.method public getViewType()I
    .locals 1

    .prologue
    .line 34
    .local p0, "this":Lcom/google/android/repolib/ui/ViewBinder;, "Lcom/google/android/repolib/ui/ViewBinder<TT;>;"
    iget v0, p0, Lcom/google/android/repolib/ui/ViewBinder;->viewType:I

    return v0
.end method
