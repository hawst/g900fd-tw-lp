.class public final Lcom/google/android/repolib/repositories/ConditionalRepository;
.super Ljava/lang/Object;
.source "ConditionalRepository.java"

# interfaces
.implements Lcom/google/android/repolib/repositories/Repository;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/repositories/Repository",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final condition:Lcom/google/android/repolib/common/Condition;

.field private final firstRepository:Lcom/google/android/repolib/repositories/Repository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final secondRepository:Lcom/google/android/repolib/repositories/Repository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/repolib/common/Condition;Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/repositories/Repository;)V
    .locals 1
    .param p1, "condition"    # Lcom/google/android/repolib/common/Condition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/common/Condition;",
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;",
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "this":Lcom/google/android/repolib/repositories/ConditionalRepository;, "Lcom/google/android/repolib/repositories/ConditionalRepository<TT;>;"
    .local p2, "firstRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    .local p3, "secondRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Condition;

    iput-object v0, p0, Lcom/google/android/repolib/repositories/ConditionalRepository;->condition:Lcom/google/android/repolib/common/Condition;

    .line 34
    invoke-static {p2}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/repositories/Repository;

    iput-object v0, p0, Lcom/google/android/repolib/repositories/ConditionalRepository;->firstRepository:Lcom/google/android/repolib/repositories/Repository;

    .line 35
    invoke-static {p3}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/repositories/Repository;

    iput-object v0, p0, Lcom/google/android/repolib/repositories/ConditionalRepository;->secondRepository:Lcom/google/android/repolib/repositories/Repository;

    .line 36
    return-void
.end method

.method private choose()Lcom/google/android/repolib/repositories/Repository;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 64
    .local p0, "this":Lcom/google/android/repolib/repositories/ConditionalRepository;, "Lcom/google/android/repolib/repositories/ConditionalRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ConditionalRepository;->condition:Lcom/google/android/repolib/common/Condition;

    invoke-interface {v0}, Lcom/google/android/repolib/common/Condition;->applies()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/repolib/repositories/ConditionalRepository;->firstRepository:Lcom/google/android/repolib/repositories/Repository;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ConditionalRepository;->secondRepository:Lcom/google/android/repolib/repositories/Repository;

    goto :goto_0
.end method

.method public static conditionalRepository(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Condition;)Lcom/google/android/repolib/repositories/Repository;
    .locals 1
    .param p2, "condition"    # Lcom/google/android/repolib/common/Condition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;",
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;",
            "Lcom/google/android/repolib/common/Condition;",
            ")",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 43
    .local p0, "firstRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    .local p1, "secondRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    new-instance v0, Lcom/google/android/repolib/repositories/ConditionalRepository;

    invoke-direct {v0, p2, p0, p1}, Lcom/google/android/repolib/repositories/ConditionalRepository;-><init>(Lcom/google/android/repolib/common/Condition;Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/repositories/Repository;)V

    return-object v0
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 48
    .local p0, "this":Lcom/google/android/repolib/repositories/ConditionalRepository;, "Lcom/google/android/repolib/repositories/ConditionalRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ConditionalRepository;->firstRepository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/repositories/Repository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 49
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ConditionalRepository;->secondRepository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/repositories/Repository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 50
    return-void
.end method

.method public indexer()Lcom/google/android/repolib/common/Indexer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/repolib/common/Indexer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 70
    .local p0, "this":Lcom/google/android/repolib/repositories/ConditionalRepository;, "Lcom/google/android/repolib/repositories/ConditionalRepository<TT;>;"
    invoke-direct {p0}, Lcom/google/android/repolib/repositories/ConditionalRepository;->choose()Lcom/google/android/repolib/repositories/Repository;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/repolib/repositories/Repository;->indexer()Lcom/google/android/repolib/common/Indexer;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 60
    .local p0, "this":Lcom/google/android/repolib/repositories/ConditionalRepository;, "Lcom/google/android/repolib/repositories/ConditionalRepository<TT;>;"
    invoke-direct {p0}, Lcom/google/android/repolib/repositories/ConditionalRepository;->choose()Lcom/google/android/repolib/repositories/Repository;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/repolib/repositories/Repository;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 54
    .local p0, "this":Lcom/google/android/repolib/repositories/ConditionalRepository;, "Lcom/google/android/repolib/repositories/ConditionalRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ConditionalRepository;->firstRepository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/repositories/Repository;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ConditionalRepository;->secondRepository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/repositories/Repository;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 56
    return-void
.end method
