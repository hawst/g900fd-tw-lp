.class public final Lcom/google/android/repolib/repositories/ArrayRepository;
.super Ljava/lang/Object;
.source "ArrayRepository.java"

# interfaces
.implements Lcom/google/android/repolib/repositories/Repository;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/repositories/Repository",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final items:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/util/List",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;


# direct methods
.method private constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p0, "this":Lcom/google/android/repolib/repositories/ArrayRepository;, "Lcom/google/android/repolib/repositories/ArrayRepository<TT;>;"
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher()Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/repolib/repositories/ArrayRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 30
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/repolib/repositories/ArrayRepository;->items:Ljava/util/concurrent/atomic/AtomicReference;

    .line 44
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ArrayRepository;->items:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 45
    return-void
.end method

.method public static arrayRepository()Lcom/google/android/repolib/repositories/ArrayRepository;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/repolib/repositories/ArrayRepository",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/repolib/repositories/ArrayRepository;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public static arrayRepository(Ljava/util/Collection;)Lcom/google/android/repolib/repositories/ArrayRepository;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<TT;>;)",
            "Lcom/google/android/repolib/repositories/ArrayRepository",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 40
    .local p0, "items":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;"
    new-instance v0, Lcom/google/android/repolib/repositories/ArrayRepository;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v0, v1}, Lcom/google/android/repolib/repositories/ArrayRepository;-><init>(Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 49
    .local p0, "this":Lcom/google/android/repolib/repositories/ArrayRepository;, "Lcom/google/android/repolib/repositories/ArrayRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ArrayRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 50
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 69
    .local p0, "this":Lcom/google/android/repolib/repositories/ArrayRepository;, "Lcom/google/android/repolib/repositories/ArrayRepository<TT;>;"
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/repolib/repositories/ArrayRepository;->setItems(Ljava/util/Collection;)V

    .line 70
    return-void
.end method

.method public indexer()Lcom/google/android/repolib/common/Indexer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/repolib/common/Indexer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 65
    .local p0, "this":Lcom/google/android/repolib/repositories/ArrayRepository;, "Lcom/google/android/repolib/repositories/ArrayRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ArrayRepository;->items:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/repolib/common/ListIndexer;->listIndexer(Ljava/util/List;)Lcom/google/android/repolib/common/Indexer;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 59
    .local p0, "this":Lcom/google/android/repolib/repositories/ArrayRepository;, "Lcom/google/android/repolib/repositories/ArrayRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ArrayRepository;->items:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 54
    .local p0, "this":Lcom/google/android/repolib/repositories/ArrayRepository;, "Lcom/google/android/repolib/repositories/ArrayRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ArrayRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 55
    return-void
.end method

.method public setItems(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p0, "this":Lcom/google/android/repolib/repositories/ArrayRepository;, "Lcom/google/android/repolib/repositories/ArrayRepository<TT;>;"
    .local p1, "items":Ljava/util/Collection;, "Ljava/util/Collection<+TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ArrayRepository;->items:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ArrayRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0}, Lcom/google/android/repolib/observers/UpdateDispatcher;->update()V

    .line 75
    return-void
.end method
