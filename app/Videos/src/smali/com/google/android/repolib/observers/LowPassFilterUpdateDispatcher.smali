.class public final Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;
.super Ljava/lang/Object;
.source "LowPassFilterUpdateDispatcher.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/repolib/observers/UpdatablesChanged;
.implements Lcom/google/android/repolib/observers/UpdateDispatcher;


# instance fields
.field private final handler:Landroid/os/Handler;

.field private lastUpdateTimestamp:J

.field private final shortestUpdateWindowMillis:I

.field private final updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;


# direct methods
.method private constructor <init>(I)V
    .locals 1
    .param p1, "shortestUpdateWindowMillis"    # I

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput p1, p0, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;->shortestUpdateWindowMillis:I

    .line 24
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;->handler:Landroid/os/Handler;

    .line 25
    invoke-static {p0}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher(Lcom/google/android/repolib/observers/UpdatablesChanged;)Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 26
    return-void
.end method

.method public static lowPassFilterUpdateDispatcher(I)Lcom/google/android/repolib/observers/UpdateDispatcher;
    .locals 1
    .param p0, "shortestUpdateWindowMillis"    # I

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;

    invoke-direct {v0, p0}, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 69
    return-void
.end method

.method public firstUpdatableAdded()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 8
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    .line 45
    iget v5, p1, Landroid/os/Message;->what:I

    if-nez v5, :cond_0

    .line 46
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 47
    .local v0, "elapsedRealtimeMillis":J
    iget-wide v6, p0, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;->lastUpdateTimestamp:J

    sub-long v2, v0, v6

    .line 48
    .local v2, "timeFromLastUpdate":J
    iget v5, p0, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;->shortestUpdateWindowMillis:I

    int-to-long v6, v5

    cmp-long v5, v2, v6

    if-ltz v5, :cond_1

    .line 49
    iput-wide v0, p0, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;->lastUpdateTimestamp:J

    .line 50
    iget-object v4, p0, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v4}, Lcom/google/android/repolib/observers/UpdateDispatcher;->update()V

    .line 56
    :goto_0
    const/4 v4, 0x1

    .line 58
    .end local v0    # "elapsedRealtimeMillis":J
    .end local v2    # "timeFromLastUpdate":J
    :cond_0
    return v4

    .line 52
    .restart local v0    # "elapsedRealtimeMillis":J
    .restart local v2    # "timeFromLastUpdate":J
    :cond_1
    iget-object v5, p0, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;->handler:Landroid/os/Handler;

    invoke-virtual {v5, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 53
    iget-object v5, p0, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;->handler:Landroid/os/Handler;

    iget v6, p0, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;->shortestUpdateWindowMillis:I

    int-to-long v6, v6

    sub-long/2addr v6, v2

    invoke-virtual {v5, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public lastUpdatableRemoved()V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 41
    return-void
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 74
    return-void
.end method

.method public update()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/repolib/observers/LowPassFilterUpdateDispatcher;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 64
    return-void
.end method
