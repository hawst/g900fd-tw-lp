.class public final Lcom/google/android/repolib/common/PostingAction;
.super Ljava/lang/Object;
.source "PostingAction.java"

# interfaces
.implements Lcom/google/android/repolib/common/Action;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/repolib/common/PostingAction$ActionRunnable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Action",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final action:Lcom/google/android/repolib/common/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Action",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final handler:Landroid/os/Handler;


# direct methods
.method private constructor <init>(Lcom/google/android/repolib/common/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/common/Action",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p0, "this":Lcom/google/android/repolib/common/PostingAction;, "Lcom/google/android/repolib/common/PostingAction<TT;>;"
    .local p1, "action":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/repolib/common/PostingAction;->handler:Landroid/os/Handler;

    .line 15
    invoke-static {p1}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Action;

    iput-object v0, p0, Lcom/google/android/repolib/common/PostingAction;->action:Lcom/google/android/repolib/common/Action;

    .line 16
    return-void
.end method

.method public static postingAction(Lcom/google/android/repolib/common/Action;)Lcom/google/android/repolib/common/Action;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/repolib/common/Action",
            "<TT;>;)",
            "Lcom/google/android/repolib/common/Action",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 20
    .local p0, "action":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<TT;>;"
    new-instance v0, Lcom/google/android/repolib/common/PostingAction;

    invoke-direct {v0, p0}, Lcom/google/android/repolib/common/PostingAction;-><init>(Lcom/google/android/repolib/common/Action;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "this":Lcom/google/android/repolib/common/PostingAction;, "Lcom/google/android/repolib/common/PostingAction<TT;>;"
    .local p1, "object":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/google/android/repolib/common/PostingAction;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/repolib/common/PostingAction$ActionRunnable;

    iget-object v2, p0, Lcom/google/android/repolib/common/PostingAction;->action:Lcom/google/android/repolib/common/Action;

    invoke-direct {v1, v2, p1}, Lcom/google/android/repolib/common/PostingAction$ActionRunnable;-><init>(Lcom/google/android/repolib/common/Action;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 26
    return-void
.end method
