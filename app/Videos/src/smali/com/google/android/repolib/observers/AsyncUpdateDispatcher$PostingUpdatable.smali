.class Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;
.super Ljava/lang/Object;
.source "AsyncUpdateDispatcher.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PostingUpdatable"
.end annotation


# instance fields
.field private final handler:Landroid/os/Handler;

.field private final updatable:Lcom/google/android/repolib/observers/Updatable;


# direct methods
.method public constructor <init>(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;->handler:Landroid/os/Handler;

    .line 114
    iput-object p1, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;->updatable:Lcom/google/android/repolib/observers/Updatable;

    .line 115
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 119
    return-void
.end method

.method public getKey()Lcom/google/android/repolib/observers/Updatable;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;->updatable:Lcom/google/android/repolib/observers/Updatable;

    return-object v0
.end method

.method public run()V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;->updatable:Lcom/google/android/repolib/observers/Updatable;

    invoke-interface {v0}, Lcom/google/android/repolib/observers/Updatable;->update()V

    .line 124
    return-void
.end method

.method public update()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;->handler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;->run()V

    .line 133
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;->handler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
