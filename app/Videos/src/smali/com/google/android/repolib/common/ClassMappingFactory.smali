.class public final Lcom/google/android/repolib/common/ClassMappingFactory;
.super Ljava/lang/Object;
.source "ClassMappingFactory.java"

# interfaces
.implements Lcom/google/android/repolib/common/Binder;
.implements Lcom/google/android/repolib/common/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "F:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Binder",
        "<",
        "Ljava/lang/Class",
        "<+TF;>;",
        "Lcom/google/android/repolib/common/Factory",
        "<+TT;+TF;>;>;",
        "Lcom/google/android/repolib/common/Factory",
        "<TT;TF;>;"
    }
.end annotation


# instance fields
.field private final defaultFactory:Lcom/google/android/repolib/common/Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Factory",
            "<TT;TF;>;"
        }
    .end annotation
.end field

.field private final factories:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/android/repolib/common/Factory",
            "<TT;TF;>;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/repolib/common/Factory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/common/Factory",
            "<+TT;+TF;>;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p0, "this":Lcom/google/android/repolib/common/ClassMappingFactory;, "Lcom/google/android/repolib/common/ClassMappingFactory<TT;TF;>;"
    .local p1, "defaultFactory":Lcom/google/android/repolib/common/Factory;, "Lcom/google/android/repolib/common/Factory<+TT;+TF;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/repolib/common/ClassMappingFactory;->factories:Ljava/util/Map;

    .line 17
    iput-object p1, p0, Lcom/google/android/repolib/common/ClassMappingFactory;->defaultFactory:Lcom/google/android/repolib/common/Factory;

    .line 18
    return-void
.end method

.method public static classMappingFactory(Lcom/google/android/repolib/common/Factory;)Lcom/google/android/repolib/common/ClassMappingFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "F:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/repolib/common/Factory",
            "<+TT;+TF;>;)",
            "Lcom/google/android/repolib/common/ClassMappingFactory",
            "<TT;TF;>;"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "defaultFactory":Lcom/google/android/repolib/common/Factory;, "Lcom/google/android/repolib/common/Factory<+TT;+TF;>;"
    new-instance v0, Lcom/google/android/repolib/common/ClassMappingFactory;

    invoke-direct {v0, p0}, Lcom/google/android/repolib/common/ClassMappingFactory;-><init>(Lcom/google/android/repolib/common/Factory;)V

    return-object v0
.end method


# virtual methods
.method public bind(Ljava/lang/Class;Lcom/google/android/repolib/common/Factory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+TF;>;",
            "Lcom/google/android/repolib/common/Factory",
            "<+TT;+TF;>;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p0, "this":Lcom/google/android/repolib/common/ClassMappingFactory;, "Lcom/google/android/repolib/common/ClassMappingFactory<TT;TF;>;"
    .local p1, "from":Ljava/lang/Class;, "Ljava/lang/Class<+TF;>;"
    .local p2, "to":Lcom/google/android/repolib/common/Factory;, "Lcom/google/android/repolib/common/Factory<+TT;+TF;>;"
    iget-object v0, p0, Lcom/google/android/repolib/common/ClassMappingFactory;->factories:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    return-void
.end method

.method public bridge synthetic bind(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 8
    .local p0, "this":Lcom/google/android/repolib/common/ClassMappingFactory;, "Lcom/google/android/repolib/common/ClassMappingFactory<TT;TF;>;"
    check-cast p1, Ljava/lang/Class;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/repolib/common/Factory;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/repolib/common/ClassMappingFactory;->bind(Ljava/lang/Class;Lcom/google/android/repolib/common/Factory;)V

    return-void
.end method

.method public createFrom(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TF;)TT;"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lcom/google/android/repolib/common/ClassMappingFactory;, "Lcom/google/android/repolib/common/ClassMappingFactory<TT;TF;>;"
    .local p1, "from":Ljava/lang/Object;, "TF;"
    iget-object v1, p0, Lcom/google/android/repolib/common/ClassMappingFactory;->factories:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Factory;

    .line 29
    .local v0, "factory":Lcom/google/android/repolib/common/Factory;, "Lcom/google/android/repolib/common/Factory<TT;TF;>;"
    if-eqz v0, :cond_0

    .end local v0    # "factory":Lcom/google/android/repolib/common/Factory;, "Lcom/google/android/repolib/common/Factory<TT;TF;>;"
    :goto_0
    invoke-interface {v0, p1}, Lcom/google/android/repolib/common/Factory;->createFrom(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    return-object v1

    .restart local v0    # "factory":Lcom/google/android/repolib/common/Factory;, "Lcom/google/android/repolib/common/Factory<TT;TF;>;"
    :cond_0
    iget-object v0, p0, Lcom/google/android/repolib/common/ClassMappingFactory;->defaultFactory:Lcom/google/android/repolib/common/Factory;

    goto :goto_0
.end method
