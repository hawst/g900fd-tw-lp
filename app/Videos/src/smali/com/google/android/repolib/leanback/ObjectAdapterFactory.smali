.class public final Lcom/google/android/repolib/leanback/ObjectAdapterFactory;
.super Ljava/lang/Object;
.source "ObjectAdapterFactory.java"

# interfaces
.implements Lcom/google/android/repolib/common/Binder;
.implements Lcom/google/android/repolib/common/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/repolib/leanback/ObjectAdapterFactory$BinderPresenter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Binder",
        "<",
        "Ljava/lang/Class",
        "<+TT;>;",
        "Lcom/google/android/repolib/ui/ViewBinder",
        "<+TT;>;>;",
        "Lcom/google/android/repolib/common/Factory",
        "<",
        "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter",
        "<+TT;>;",
        "Lcom/google/android/repolib/repositories/Repository",
        "<+TT;>;>;"
    }
.end annotation


# instance fields
.field private final presenterSelector:Landroid/support/v17/leanback/widget/ClassPresenterSelector;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    .local p0, "this":Lcom/google/android/repolib/leanback/ObjectAdapterFactory;, "Lcom/google/android/repolib/leanback/ObjectAdapterFactory<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Landroid/support/v17/leanback/widget/ClassPresenterSelector;

    invoke-direct {v0}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;-><init>()V

    iput-object v0, p0, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;->presenterSelector:Landroid/support/v17/leanback/widget/ClassPresenterSelector;

    .line 36
    return-void
.end method


# virtual methods
.method public bind(Ljava/lang/Class;Lcom/google/android/repolib/ui/ViewBinder;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+TT;>;",
            "Lcom/google/android/repolib/ui/ViewBinder",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "this":Lcom/google/android/repolib/leanback/ObjectAdapterFactory;, "Lcom/google/android/repolib/leanback/ObjectAdapterFactory<TT;>;"
    .local p1, "aClass":Ljava/lang/Class;, "Ljava/lang/Class<+TT;>;"
    .local p2, "viewBinder":Lcom/google/android/repolib/ui/ViewBinder;, "Lcom/google/android/repolib/ui/ViewBinder<+TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;->presenterSelector:Landroid/support/v17/leanback/widget/ClassPresenterSelector;

    new-instance v1, Lcom/google/android/repolib/leanback/ObjectAdapterFactory$BinderPresenter;

    invoke-virtual {p2}, Lcom/google/android/repolib/ui/ViewBinder;->getViewType()I

    move-result v2

    invoke-virtual {p2}, Lcom/google/android/repolib/ui/ViewBinder;->getBinder()Lcom/google/android/repolib/common/Binder;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/repolib/leanback/ObjectAdapterFactory$BinderPresenter;-><init>(ILcom/google/android/repolib/common/Binder;)V

    invoke-virtual {v0, p1, v1}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 27
    return-void
.end method

.method public bridge synthetic bind(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 17
    .local p0, "this":Lcom/google/android/repolib/leanback/ObjectAdapterFactory;, "Lcom/google/android/repolib/leanback/ObjectAdapterFactory<TT;>;"
    check-cast p1, Ljava/lang/Class;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/repolib/ui/ViewBinder;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;->bind(Ljava/lang/Class;Lcom/google/android/repolib/ui/ViewBinder;)V

    return-void
.end method

.method public createFrom(Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;)",
            "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "this":Lcom/google/android/repolib/leanback/ObjectAdapterFactory;, "Lcom/google/android/repolib/leanback/ObjectAdapterFactory<TT;>;"
    .local p1, "repository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    new-instance v0, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;

    iget-object v1, p0, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;->presenterSelector:Landroid/support/v17/leanback/widget/ClassPresenterSelector;

    invoke-direct {v0, v1, p1}, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;Lcom/google/android/repolib/repositories/Repository;)V

    return-object v0
.end method

.method public bridge synthetic createFrom(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 17
    .local p0, "this":Lcom/google/android/repolib/leanback/ObjectAdapterFactory;, "Lcom/google/android/repolib/leanback/ObjectAdapterFactory<TT;>;"
    check-cast p1, Lcom/google/android/repolib/repositories/Repository;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;->createFrom(Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;

    move-result-object v0

    return-object v0
.end method
