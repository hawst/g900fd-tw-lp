.class Lcom/google/android/repolib/repositories/CreatorRepository$CreatorUpdatable;
.super Ljava/lang/Object;
.source "CreatorRepository.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/repolib/repositories/CreatorRepository;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CreatorUpdatable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/observers/Updatable;"
    }
.end annotation


# instance fields
.field private final creator:Lcom/google/android/repolib/common/Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Creator",
            "<",
            "Ljava/util/List",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final repository:Lcom/google/android/repolib/repositories/ArrayRepository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/ArrayRepository",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/repolib/repositories/ArrayRepository;Lcom/google/android/repolib/common/Creator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/repositories/ArrayRepository",
            "<TT;>;",
            "Lcom/google/android/repolib/common/Creator",
            "<",
            "Ljava/util/List",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p0, "this":Lcom/google/android/repolib/repositories/CreatorRepository$CreatorUpdatable;, "Lcom/google/android/repolib/repositories/CreatorRepository$CreatorUpdatable<TT;>;"
    .local p1, "repository":Lcom/google/android/repolib/repositories/ArrayRepository;, "Lcom/google/android/repolib/repositories/ArrayRepository<TT;>;"
    .local p2, "creator":Lcom/google/android/repolib/common/Creator;, "Lcom/google/android/repolib/common/Creator<Ljava/util/List<TT;>;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    invoke-static {p1}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/repositories/ArrayRepository;

    iput-object v0, p0, Lcom/google/android/repolib/repositories/CreatorRepository$CreatorUpdatable;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    .line 107
    invoke-static {p2}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Creator;

    iput-object v0, p0, Lcom/google/android/repolib/repositories/CreatorRepository$CreatorUpdatable;->creator:Lcom/google/android/repolib/common/Creator;

    .line 108
    return-void
.end method


# virtual methods
.method public update()V
    .locals 2

    .prologue
    .line 112
    .local p0, "this":Lcom/google/android/repolib/repositories/CreatorRepository$CreatorUpdatable;, "Lcom/google/android/repolib/repositories/CreatorRepository$CreatorUpdatable<TT;>;"
    iget-object v1, p0, Lcom/google/android/repolib/repositories/CreatorRepository$CreatorUpdatable;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    iget-object v0, p0, Lcom/google/android/repolib/repositories/CreatorRepository$CreatorUpdatable;->creator:Lcom/google/android/repolib/common/Creator;

    invoke-interface {v0}, Lcom/google/android/repolib/common/Creator;->create()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {v1, v0}, Lcom/google/android/repolib/repositories/ArrayRepository;->setItems(Ljava/util/Collection;)V

    .line 113
    return-void
.end method
