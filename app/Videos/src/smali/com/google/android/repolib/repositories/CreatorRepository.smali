.class public final Lcom/google/android/repolib/repositories/CreatorRepository;
.super Ljava/lang/Object;
.source "CreatorRepository.java"

# interfaces
.implements Lcom/google/android/repolib/observers/UpdatablesChanged;
.implements Lcom/google/android/repolib/repositories/Repository;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/repolib/repositories/CreatorRepository$CreatorUpdatable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/observers/UpdatablesChanged;",
        "Lcom/google/android/repolib/repositories/Repository",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final observable:Lcom/google/android/repolib/observers/Observable;

.field private final repository:Lcom/google/android/repolib/repositories/Repository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final updatable:Lcom/google/android/repolib/observers/Updatable;

.field private final updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;


# direct methods
.method private constructor <init>(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p2, "observable"    # Lcom/google/android/repolib/observers/Observable;
    .param p3, "updatable"    # Lcom/google/android/repolib/observers/Updatable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;",
            "Lcom/google/android/repolib/observers/Observable;",
            "Lcom/google/android/repolib/observers/Updatable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    .local p0, "this":Lcom/google/android/repolib/repositories/CreatorRepository;, "Lcom/google/android/repolib/repositories/CreatorRepository<TT;>;"
    .local p1, "repository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-static {p3}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/observers/Updatable;

    iput-object v0, p0, Lcom/google/android/repolib/repositories/CreatorRepository;->updatable:Lcom/google/android/repolib/observers/Updatable;

    .line 59
    invoke-static {p1}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/repositories/Repository;

    iput-object v0, p0, Lcom/google/android/repolib/repositories/CreatorRepository;->repository:Lcom/google/android/repolib/repositories/Repository;

    .line 60
    invoke-static {p2}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/observers/Observable;

    iput-object v0, p0, Lcom/google/android/repolib/repositories/CreatorRepository;->observable:Lcom/google/android/repolib/observers/Observable;

    .line 61
    invoke-static {p0}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher(Lcom/google/android/repolib/observers/UpdatablesChanged;)Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/repolib/repositories/CreatorRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 62
    return-void
.end method

.method public static asyncCreatorRepository(Ljava/util/concurrent/Executor;Lcom/google/android/repolib/common/Creator;Lcom/google/android/repolib/observers/Observable;)Lcom/google/android/repolib/repositories/Repository;
    .locals 3
    .param p0, "executor"    # Ljava/util/concurrent/Executor;
    .param p2, "observable"    # Lcom/google/android/repolib/observers/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/repolib/common/Creator",
            "<",
            "Ljava/util/List",
            "<TT;>;>;",
            "Lcom/google/android/repolib/observers/Observable;",
            ")",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 43
    .local p1, "creator":Lcom/google/android/repolib/common/Creator;, "Lcom/google/android/repolib/common/Creator<Ljava/util/List<TT;>;>;"
    invoke-static {}, Lcom/google/android/repolib/repositories/ArrayRepository;->arrayRepository()Lcom/google/android/repolib/repositories/ArrayRepository;

    move-result-object v0

    .line 44
    .local v0, "repository":Lcom/google/android/repolib/repositories/ArrayRepository;, "Lcom/google/android/repolib/repositories/ArrayRepository<TT;>;"
    new-instance v1, Lcom/google/android/repolib/repositories/CreatorRepository;

    new-instance v2, Lcom/google/android/repolib/repositories/CreatorRepository$CreatorUpdatable;

    invoke-direct {v2, v0, p1}, Lcom/google/android/repolib/repositories/CreatorRepository$CreatorUpdatable;-><init>(Lcom/google/android/repolib/repositories/ArrayRepository;Lcom/google/android/repolib/common/Creator;)V

    invoke-static {p0, v2}, Lcom/google/android/repolib/observers/AsyncUpdatable;->asyncUpdatable(Ljava/util/concurrent/Executor;Lcom/google/android/repolib/observers/Updatable;)Lcom/google/android/repolib/observers/Updatable;

    move-result-object v2

    invoke-direct {v1, v0, p2, v2}, Lcom/google/android/repolib/repositories/CreatorRepository;-><init>(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/repolib/observers/Updatable;)V

    return-object v1
.end method

.method public static creatorRepository(Lcom/google/android/repolib/common/Creator;Lcom/google/android/repolib/observers/Observable;)Lcom/google/android/repolib/repositories/Repository;
    .locals 3
    .param p1, "observable"    # Lcom/google/android/repolib/observers/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/repolib/common/Creator",
            "<",
            "Ljava/util/List",
            "<TT;>;>;",
            "Lcom/google/android/repolib/observers/Observable;",
            ")",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "creator":Lcom/google/android/repolib/common/Creator;, "Lcom/google/android/repolib/common/Creator<Ljava/util/List<TT;>;>;"
    invoke-static {}, Lcom/google/android/repolib/repositories/ArrayRepository;->arrayRepository()Lcom/google/android/repolib/repositories/ArrayRepository;

    move-result-object v0

    .line 52
    .local v0, "repository":Lcom/google/android/repolib/repositories/ArrayRepository;, "Lcom/google/android/repolib/repositories/ArrayRepository<TT;>;"
    new-instance v1, Lcom/google/android/repolib/repositories/CreatorRepository;

    new-instance v2, Lcom/google/android/repolib/repositories/CreatorRepository$CreatorUpdatable;

    invoke-direct {v2, v0, p0}, Lcom/google/android/repolib/repositories/CreatorRepository$CreatorUpdatable;-><init>(Lcom/google/android/repolib/repositories/ArrayRepository;Lcom/google/android/repolib/common/Creator;)V

    invoke-direct {v1, v0, p1, v2}, Lcom/google/android/repolib/repositories/CreatorRepository;-><init>(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/repolib/observers/Updatable;)V

    return-object v1
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 79
    .local p0, "this":Lcom/google/android/repolib/repositories/CreatorRepository;, "Lcom/google/android/repolib/repositories/CreatorRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/CreatorRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 80
    return-void
.end method

.method public firstUpdatableAdded()V
    .locals 2

    .prologue
    .line 66
    .local p0, "this":Lcom/google/android/repolib/repositories/CreatorRepository;, "Lcom/google/android/repolib/repositories/CreatorRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/CreatorRepository;->repository:Lcom/google/android/repolib/repositories/Repository;

    iget-object v1, p0, Lcom/google/android/repolib/repositories/CreatorRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, v1}, Lcom/google/android/repolib/repositories/Repository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/repolib/repositories/CreatorRepository;->observable:Lcom/google/android/repolib/observers/Observable;

    iget-object v1, p0, Lcom/google/android/repolib/repositories/CreatorRepository;->updatable:Lcom/google/android/repolib/observers/Updatable;

    invoke-interface {v0, v1}, Lcom/google/android/repolib/observers/Observable;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/repolib/repositories/CreatorRepository;->updatable:Lcom/google/android/repolib/observers/Updatable;

    invoke-interface {v0}, Lcom/google/android/repolib/observers/Updatable;->update()V

    .line 69
    return-void
.end method

.method public indexer()Lcom/google/android/repolib/common/Indexer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/repolib/common/Indexer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 95
    .local p0, "this":Lcom/google/android/repolib/repositories/CreatorRepository;, "Lcom/google/android/repolib/repositories/CreatorRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/CreatorRepository;->repository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0}, Lcom/google/android/repolib/repositories/Repository;->indexer()Lcom/google/android/repolib/common/Indexer;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 89
    .local p0, "this":Lcom/google/android/repolib/repositories/CreatorRepository;, "Lcom/google/android/repolib/repositories/CreatorRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/CreatorRepository;->repository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0}, Lcom/google/android/repolib/repositories/Repository;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public lastUpdatableRemoved()V
    .locals 2

    .prologue
    .line 73
    .local p0, "this":Lcom/google/android/repolib/repositories/CreatorRepository;, "Lcom/google/android/repolib/repositories/CreatorRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/CreatorRepository;->observable:Lcom/google/android/repolib/observers/Observable;

    iget-object v1, p0, Lcom/google/android/repolib/repositories/CreatorRepository;->updatable:Lcom/google/android/repolib/observers/Updatable;

    invoke-interface {v0, v1}, Lcom/google/android/repolib/observers/Observable;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/repolib/repositories/CreatorRepository;->repository:Lcom/google/android/repolib/repositories/Repository;

    iget-object v1, p0, Lcom/google/android/repolib/repositories/CreatorRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, v1}, Lcom/google/android/repolib/repositories/Repository;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 75
    return-void
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 84
    .local p0, "this":Lcom/google/android/repolib/repositories/CreatorRepository;, "Lcom/google/android/repolib/repositories/CreatorRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/CreatorRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 85
    return-void
.end method
