.class public final Lcom/google/android/repolib/ui/TagActionOnClickListener;
.super Ljava/lang/Object;
.source "TagActionOnClickListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/repolib/common/Binder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/google/android/repolib/common/Binder",
        "<",
        "Landroid/view/View;",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final action:Lcom/google/android/repolib/common/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Action",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/repolib/common/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/common/Action",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p0, "this":Lcom/google/android/repolib/ui/TagActionOnClickListener;, "Lcom/google/android/repolib/ui/TagActionOnClickListener<TT;>;"
    .local p1, "action":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    invoke-static {p1}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Action;

    iput-object v0, p0, Lcom/google/android/repolib/ui/TagActionOnClickListener;->action:Lcom/google/android/repolib/common/Action;

    .line 18
    return-void
.end method

.method public static tagActionOnClickListener(Lcom/google/android/repolib/common/Action;)Lcom/google/android/repolib/common/Binder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/repolib/common/Action",
            "<TT;>;)",
            "Lcom/google/android/repolib/common/Binder",
            "<",
            "Landroid/view/View;",
            "TT;>;"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "action":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<TT;>;"
    new-instance v0, Lcom/google/android/repolib/ui/TagActionOnClickListener;

    invoke-direct {v0, p0}, Lcom/google/android/repolib/ui/TagActionOnClickListener;-><init>(Lcom/google/android/repolib/common/Action;)V

    return-object v0
.end method


# virtual methods
.method public bind(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lcom/google/android/repolib/ui/TagActionOnClickListener;, "Lcom/google/android/repolib/ui/TagActionOnClickListener<TT;>;"
    .local p2, "item":Ljava/lang/Object;, "TT;"
    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 29
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    return-void
.end method

.method public bridge synthetic bind(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 11
    .local p0, "this":Lcom/google/android/repolib/ui/TagActionOnClickListener;, "Lcom/google/android/repolib/ui/TagActionOnClickListener<TT;>;"
    check-cast p1, Landroid/view/View;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/repolib/ui/TagActionOnClickListener;->bind(Landroid/view/View;Ljava/lang/Object;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 35
    .local p0, "this":Lcom/google/android/repolib/ui/TagActionOnClickListener;, "Lcom/google/android/repolib/ui/TagActionOnClickListener<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/ui/TagActionOnClickListener;->action:Lcom/google/android/repolib/common/Action;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/repolib/common/Action;->apply(Ljava/lang/Object;)V

    .line 36
    return-void
.end method
