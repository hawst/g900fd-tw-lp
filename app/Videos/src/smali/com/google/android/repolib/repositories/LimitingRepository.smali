.class public final Lcom/google/android/repolib/repositories/LimitingRepository;
.super Ljava/lang/Object;
.source "LimitingRepository.java"

# interfaces
.implements Lcom/google/android/repolib/observers/UpdatablesChanged;
.implements Lcom/google/android/repolib/repositories/Repository;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/observers/UpdatablesChanged;",
        "Lcom/google/android/repolib/repositories/Repository",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final limit:I

.field private final repository:Lcom/google/android/repolib/repositories/Repository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;


# direct methods
.method private constructor <init>(Lcom/google/android/repolib/repositories/Repository;I)V
    .locals 1
    .param p2, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p0, "this":Lcom/google/android/repolib/repositories/LimitingRepository;, "Lcom/google/android/repolib/repositories/LimitingRepository<TT;>;"
    .local p1, "repository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/repolib/common/Preconditions;->checkArgument(Z)V

    .line 38
    invoke-static {p0}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher(Lcom/google/android/repolib/observers/UpdatablesChanged;)Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/repolib/repositories/LimitingRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 39
    iput-object p1, p0, Lcom/google/android/repolib/repositories/LimitingRepository;->repository:Lcom/google/android/repolib/repositories/Repository;

    .line 40
    iput p2, p0, Lcom/google/android/repolib/repositories/LimitingRepository;->limit:I

    .line 41
    return-void

    .line 37
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static limitingRepository(Lcom/google/android/repolib/repositories/Repository;I)Lcom/google/android/repolib/repositories/Repository;
    .locals 1
    .param p1, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;I)",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "repository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<TT;>;"
    new-instance v0, Lcom/google/android/repolib/repositories/LimitingRepository;

    invoke-direct {v0, p0, p1}, Lcom/google/android/repolib/repositories/LimitingRepository;-><init>(Lcom/google/android/repolib/repositories/Repository;I)V

    return-object v0
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 55
    .local p0, "this":Lcom/google/android/repolib/repositories/LimitingRepository;, "Lcom/google/android/repolib/repositories/LimitingRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/LimitingRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 56
    return-void
.end method

.method public firstUpdatableAdded()V
    .locals 2

    .prologue
    .line 45
    .local p0, "this":Lcom/google/android/repolib/repositories/LimitingRepository;, "Lcom/google/android/repolib/repositories/LimitingRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/LimitingRepository;->repository:Lcom/google/android/repolib/repositories/Repository;

    iget-object v1, p0, Lcom/google/android/repolib/repositories/LimitingRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, v1}, Lcom/google/android/repolib/repositories/Repository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 46
    return-void
.end method

.method public indexer()Lcom/google/android/repolib/common/Indexer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/repolib/common/Indexer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "this":Lcom/google/android/repolib/repositories/LimitingRepository;, "Lcom/google/android/repolib/repositories/LimitingRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/LimitingRepository;->repository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0}, Lcom/google/android/repolib/repositories/Repository;->indexer()Lcom/google/android/repolib/common/Indexer;

    move-result-object v0

    iget v1, p0, Lcom/google/android/repolib/repositories/LimitingRepository;->limit:I

    invoke-static {v0, v1}, Lcom/google/android/repolib/common/LimitingIndexer;->limitingIndexer(Lcom/google/android/repolib/common/Indexer;I)Lcom/google/android/repolib/common/Indexer;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 65
    .local p0, "this":Lcom/google/android/repolib/repositories/LimitingRepository;, "Lcom/google/android/repolib/repositories/LimitingRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/LimitingRepository;->repository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0}, Lcom/google/android/repolib/repositories/Repository;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/repolib/repositories/LimitingRepository;->limit:I

    invoke-static {v0, v1}, Lcom/google/android/repolib/common/LimitingIterator;->limitingIterator(Ljava/util/Iterator;I)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public lastUpdatableRemoved()V
    .locals 2

    .prologue
    .line 50
    .local p0, "this":Lcom/google/android/repolib/repositories/LimitingRepository;, "Lcom/google/android/repolib/repositories/LimitingRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/LimitingRepository;->repository:Lcom/google/android/repolib/repositories/Repository;

    iget-object v1, p0, Lcom/google/android/repolib/repositories/LimitingRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, v1}, Lcom/google/android/repolib/repositories/Repository;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 51
    return-void
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 60
    .local p0, "this":Lcom/google/android/repolib/repositories/LimitingRepository;, "Lcom/google/android/repolib/repositories/LimitingRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/LimitingRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 61
    return-void
.end method
