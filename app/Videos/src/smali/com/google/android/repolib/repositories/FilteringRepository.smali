.class public final Lcom/google/android/repolib/repositories/FilteringRepository;
.super Ljava/lang/Object;
.source "FilteringRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/repolib/repositories/FilteringRepository$1;,
        Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public static filteringRepository(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Predicate;)Lcom/google/android/repolib/repositories/Repository;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;",
            "Lcom/google/android/repolib/common/Predicate",
            "<TT;>;)",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "source":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<TT;>;"
    .local p1, "predicate":Lcom/google/android/repolib/common/Predicate;, "Lcom/google/android/repolib/common/Predicate<TT;>;"
    new-instance v0, Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator;-><init>(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Predicate;Lcom/google/android/repolib/repositories/FilteringRepository$1;)V

    invoke-static {v0, p0}, Lcom/google/android/repolib/repositories/CreatorRepository;->creatorRepository(Lcom/google/android/repolib/common/Creator;Lcom/google/android/repolib/observers/Observable;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v0

    return-object v0
.end method
