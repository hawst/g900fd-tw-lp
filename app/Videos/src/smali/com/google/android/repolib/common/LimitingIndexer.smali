.class public final Lcom/google/android/repolib/common/LimitingIndexer;
.super Ljava/lang/Object;
.source "LimitingIndexer.java"

# interfaces
.implements Lcom/google/android/repolib/common/Indexer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Indexer",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final indexer:Lcom/google/android/repolib/common/Indexer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Indexer",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final limit:I


# direct methods
.method private constructor <init>(Lcom/google/android/repolib/common/Indexer;I)V
    .locals 1
    .param p2, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/common/Indexer",
            "<TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 13
    .local p0, "this":Lcom/google/android/repolib/common/LimitingIndexer;, "Lcom/google/android/repolib/common/LimitingIndexer<TT;>;"
    .local p1, "indexer":Lcom/google/android/repolib/common/Indexer;, "Lcom/google/android/repolib/common/Indexer<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    invoke-static {p1}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Indexer;

    iput-object v0, p0, Lcom/google/android/repolib/common/LimitingIndexer;->indexer:Lcom/google/android/repolib/common/Indexer;

    .line 15
    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/repolib/common/Preconditions;->checkArgument(Z)V

    .line 16
    iput p2, p0, Lcom/google/android/repolib/common/LimitingIndexer;->limit:I

    .line 17
    return-void

    .line 15
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static limitingIndexer(Lcom/google/android/repolib/common/Indexer;I)Lcom/google/android/repolib/common/Indexer;
    .locals 1
    .param p1, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/repolib/common/Indexer",
            "<TT;>;I)",
            "Lcom/google/android/repolib/common/Indexer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 21
    .local p0, "indexer":Lcom/google/android/repolib/common/Indexer;, "Lcom/google/android/repolib/common/Indexer<TT;>;"
    new-instance v0, Lcom/google/android/repolib/common/LimitingIndexer;

    invoke-direct {v0, p0, p1}, Lcom/google/android/repolib/common/LimitingIndexer;-><init>(Lcom/google/android/repolib/common/Indexer;I)V

    return-object v0
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 27
    .local p0, "this":Lcom/google/android/repolib/common/LimitingIndexer;, "Lcom/google/android/repolib/common/LimitingIndexer<TT;>;"
    iget v0, p0, Lcom/google/android/repolib/common/LimitingIndexer;->limit:I

    if-le p1, v0, :cond_0

    .line 28
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/google/android/repolib/common/LimitingIndexer;->indexer:Lcom/google/android/repolib/common/Indexer;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/common/Indexer;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 35
    .local p0, "this":Lcom/google/android/repolib/common/LimitingIndexer;, "Lcom/google/android/repolib/common/LimitingIndexer<TT;>;"
    iget v0, p0, Lcom/google/android/repolib/common/LimitingIndexer;->limit:I

    iget-object v1, p0, Lcom/google/android/repolib/common/LimitingIndexer;->indexer:Lcom/google/android/repolib/common/Indexer;

    invoke-interface {v1}, Lcom/google/android/repolib/common/Indexer;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method
