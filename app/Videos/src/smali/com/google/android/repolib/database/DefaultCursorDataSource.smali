.class public final Lcom/google/android/repolib/database/DefaultCursorDataSource;
.super Ljava/lang/Object;
.source "DefaultCursorDataSource.java"

# interfaces
.implements Lcom/google/android/repolib/database/CursorDataSource;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/database/CursorDataSource",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final creator:Lcom/google/android/repolib/common/Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Creator",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final factory:Lcom/google/android/repolib/common/Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Factory",
            "<TT;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/repolib/common/Creator;Lcom/google/android/repolib/common/Factory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/common/Creator",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Lcom/google/android/repolib/common/Factory",
            "<TT;",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    .local p0, "this":Lcom/google/android/repolib/database/DefaultCursorDataSource;, "Lcom/google/android/repolib/database/DefaultCursorDataSource<TT;>;"
    .local p1, "creator":Lcom/google/android/repolib/common/Creator;, "Lcom/google/android/repolib/common/Creator<Landroid/database/Cursor;>;"
    .local p2, "factory":Lcom/google/android/repolib/common/Factory;, "Lcom/google/android/repolib/common/Factory<TT;Landroid/database/Cursor;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {p1}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Creator;

    iput-object v0, p0, Lcom/google/android/repolib/database/DefaultCursorDataSource;->creator:Lcom/google/android/repolib/common/Creator;

    .line 20
    invoke-static {p2}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Factory;

    iput-object v0, p0, Lcom/google/android/repolib/database/DefaultCursorDataSource;->factory:Lcom/google/android/repolib/common/Factory;

    .line 21
    return-void
.end method

.method public static defaultCursorDataSource(Lcom/google/android/repolib/common/Creator;Lcom/google/android/repolib/common/Factory;)Lcom/google/android/repolib/database/CursorDataSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/repolib/common/Creator",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Lcom/google/android/repolib/common/Factory",
            "<TT;",
            "Landroid/database/Cursor;",
            ">;)",
            "Lcom/google/android/repolib/database/CursorDataSource",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 27
    .local p0, "creator":Lcom/google/android/repolib/common/Creator;, "Lcom/google/android/repolib/common/Creator<Landroid/database/Cursor;>;"
    .local p1, "factory":Lcom/google/android/repolib/common/Factory;, "Lcom/google/android/repolib/common/Factory<TT;Landroid/database/Cursor;>;"
    new-instance v0, Lcom/google/android/repolib/database/DefaultCursorDataSource;

    invoke-direct {v0, p0, p1}, Lcom/google/android/repolib/database/DefaultCursorDataSource;-><init>(Lcom/google/android/repolib/common/Creator;Lcom/google/android/repolib/common/Factory;)V

    return-object v0
.end method


# virtual methods
.method public create()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 33
    .local p0, "this":Lcom/google/android/repolib/database/DefaultCursorDataSource;, "Lcom/google/android/repolib/database/DefaultCursorDataSource<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/database/DefaultCursorDataSource;->creator:Lcom/google/android/repolib/common/Creator;

    invoke-interface {v0}, Lcom/google/android/repolib/common/Creator;->create()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    return-object v0
.end method

.method public bridge synthetic create()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    .local p0, "this":Lcom/google/android/repolib/database/DefaultCursorDataSource;, "Lcom/google/android/repolib/database/DefaultCursorDataSource<TT;>;"
    invoke-virtual {p0}, Lcom/google/android/repolib/database/DefaultCursorDataSource;->create()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public createFrom(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "this":Lcom/google/android/repolib/database/DefaultCursorDataSource;, "Lcom/google/android/repolib/database/DefaultCursorDataSource<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/database/DefaultCursorDataSource;->factory:Lcom/google/android/repolib/common/Factory;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/common/Factory;->createFrom(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic createFrom(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 11
    .local p0, "this":Lcom/google/android/repolib/database/DefaultCursorDataSource;, "Lcom/google/android/repolib/database/DefaultCursorDataSource<TT;>;"
    check-cast p1, Landroid/database/Cursor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/repolib/database/DefaultCursorDataSource;->createFrom(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
