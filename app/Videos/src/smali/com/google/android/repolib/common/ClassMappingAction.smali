.class public final Lcom/google/android/repolib/common/ClassMappingAction;
.super Ljava/lang/Object;
.source "ClassMappingAction.java"

# interfaces
.implements Lcom/google/android/repolib/common/Action;
.implements Lcom/google/android/repolib/common/Binder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Action",
        "<TT;>;",
        "Lcom/google/android/repolib/common/Binder",
        "<",
        "Ljava/lang/Class",
        "<+TT;>;",
        "Lcom/google/android/repolib/common/Action",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private final actions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+TT;>;",
            "Lcom/google/android/repolib/common/Action",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final defaultAction:Lcom/google/android/repolib/common/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Action",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/repolib/common/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/common/Action",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 15
    .local p0, "this":Lcom/google/android/repolib/common/ClassMappingAction;, "Lcom/google/android/repolib/common/ClassMappingAction<TT;>;"
    .local p1, "defaultAction":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/repolib/common/ClassMappingAction;->actions:Ljava/util/Map;

    .line 16
    iput-object p1, p0, Lcom/google/android/repolib/common/ClassMappingAction;->defaultAction:Lcom/google/android/repolib/common/Action;

    .line 17
    return-void
.end method

.method public static classMappingAction(Lcom/google/android/repolib/common/Action;)Lcom/google/android/repolib/common/ClassMappingAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/repolib/common/Action",
            "<TT;>;)",
            "Lcom/google/android/repolib/common/ClassMappingAction",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "defaultAction":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<TT;>;"
    new-instance v0, Lcom/google/android/repolib/common/ClassMappingAction;

    invoke-direct {v0, p0}, Lcom/google/android/repolib/common/ClassMappingAction;-><init>(Lcom/google/android/repolib/common/Action;)V

    return-object v0
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "this":Lcom/google/android/repolib/common/ClassMappingAction;, "Lcom/google/android/repolib/common/ClassMappingAction<TT;>;"
    .local p1, "object":Ljava/lang/Object;, "TT;"
    iget-object v1, p0, Lcom/google/android/repolib/common/ClassMappingAction;->actions:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Action;

    .line 35
    .local v0, "typeAction":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<TT;>;"
    if-eqz v0, :cond_0

    .end local v0    # "typeAction":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<TT;>;"
    :goto_0
    invoke-interface {v0, p1}, Lcom/google/android/repolib/common/Action;->apply(Ljava/lang/Object;)V

    .line 36
    return-void

    .line 35
    .restart local v0    # "typeAction":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<TT;>;"
    :cond_0
    iget-object v0, p0, Lcom/google/android/repolib/common/ClassMappingAction;->defaultAction:Lcom/google/android/repolib/common/Action;

    goto :goto_0
.end method

.method public bind(Ljava/lang/Class;Lcom/google/android/repolib/common/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+TT;>;",
            "Lcom/google/android/repolib/common/Action",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lcom/google/android/repolib/common/ClassMappingAction;, "Lcom/google/android/repolib/common/ClassMappingAction<TT;>;"
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+TT;>;"
    .local p2, "action":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/common/ClassMappingAction;->actions:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    return-void
.end method

.method public bridge synthetic bind(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 8
    .local p0, "this":Lcom/google/android/repolib/common/ClassMappingAction;, "Lcom/google/android/repolib/common/ClassMappingAction<TT;>;"
    check-cast p1, Ljava/lang/Class;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/repolib/common/Action;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/repolib/common/ClassMappingAction;->bind(Ljava/lang/Class;Lcom/google/android/repolib/common/Action;)V

    return-void
.end method
