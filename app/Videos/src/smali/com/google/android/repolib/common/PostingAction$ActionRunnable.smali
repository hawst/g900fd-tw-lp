.class Lcom/google/android/repolib/common/PostingAction$ActionRunnable;
.super Ljava/lang/Object;
.source "PostingAction.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/repolib/common/PostingAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ActionRunnable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private final action:Lcom/google/android/repolib/common/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Action",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final object:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/repolib/common/Action;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/common/Action",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "this":Lcom/google/android/repolib/common/PostingAction$ActionRunnable;, "Lcom/google/android/repolib/common/PostingAction$ActionRunnable<TT;>;"
    .local p1, "action":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<TT;>;"
    .local p2, "object":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Action;

    iput-object v0, p0, Lcom/google/android/repolib/common/PostingAction$ActionRunnable;->action:Lcom/google/android/repolib/common/Action;

    .line 36
    invoke-static {p2}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/repolib/common/PostingAction$ActionRunnable;->object:Ljava/lang/Object;

    .line 37
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 41
    .local p0, "this":Lcom/google/android/repolib/common/PostingAction$ActionRunnable;, "Lcom/google/android/repolib/common/PostingAction$ActionRunnable<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/common/PostingAction$ActionRunnable;->action:Lcom/google/android/repolib/common/Action;

    iget-object v1, p0, Lcom/google/android/repolib/common/PostingAction$ActionRunnable;->object:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/repolib/common/Action;->apply(Ljava/lang/Object;)V

    .line 42
    return-void
.end method
