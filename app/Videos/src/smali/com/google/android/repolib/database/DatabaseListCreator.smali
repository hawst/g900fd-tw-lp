.class public final Lcom/google/android/repolib/database/DatabaseListCreator;
.super Ljava/lang/Object;
.source "DatabaseListCreator.java"

# interfaces
.implements Lcom/google/android/repolib/common/Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Creator",
        "<",
        "Ljava/util/List",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private final cursorDataSource:Lcom/google/android/repolib/database/CursorDataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/database/CursorDataSource",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private final errorHandler:Lcom/google/android/repolib/common/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Action",
            "<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/repolib/database/CursorDataSource;Lcom/google/android/repolib/common/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/database/CursorDataSource",
            "<+TT;>;",
            "Lcom/google/android/repolib/common/Action",
            "<",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "this":Lcom/google/android/repolib/database/DatabaseListCreator;, "Lcom/google/android/repolib/database/DatabaseListCreator<TT;>;"
    .local p1, "cursorDataSource":Lcom/google/android/repolib/database/CursorDataSource;, "Lcom/google/android/repolib/database/CursorDataSource<+TT;>;"
    .local p2, "errorHandler":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<Ljava/lang/Throwable;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p2}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Action;

    iput-object v0, p0, Lcom/google/android/repolib/database/DatabaseListCreator;->errorHandler:Lcom/google/android/repolib/common/Action;

    .line 24
    invoke-static {p1}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/database/CursorDataSource;

    iput-object v0, p0, Lcom/google/android/repolib/database/DatabaseListCreator;->cursorDataSource:Lcom/google/android/repolib/database/CursorDataSource;

    .line 25
    return-void
.end method

.method public static databaseListCreator(Lcom/google/android/repolib/database/CursorDataSource;Lcom/google/android/repolib/common/Action;)Lcom/google/android/repolib/common/Creator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/repolib/database/CursorDataSource",
            "<+TT;>;",
            "Lcom/google/android/repolib/common/Action",
            "<",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lcom/google/android/repolib/common/Creator",
            "<",
            "Ljava/util/List",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "cursorDataSource":Lcom/google/android/repolib/database/CursorDataSource;, "Lcom/google/android/repolib/database/CursorDataSource<+TT;>;"
    .local p1, "errorHandler":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<Ljava/lang/Throwable;>;"
    new-instance v0, Lcom/google/android/repolib/database/DatabaseListCreator;

    invoke-direct {v0, p0, p1}, Lcom/google/android/repolib/database/DatabaseListCreator;-><init>(Lcom/google/android/repolib/database/CursorDataSource;Lcom/google/android/repolib/common/Action;)V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic create()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    .local p0, "this":Lcom/google/android/repolib/database/DatabaseListCreator;, "Lcom/google/android/repolib/database/DatabaseListCreator<TT;>;"
    invoke-virtual {p0}, Lcom/google/android/repolib/database/DatabaseListCreator;->create()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public create()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "this":Lcom/google/android/repolib/database/DatabaseListCreator;, "Lcom/google/android/repolib/database/DatabaseListCreator<TT;>;"
    iget-object v3, p0, Lcom/google/android/repolib/database/DatabaseListCreator;->cursorDataSource:Lcom/google/android/repolib/database/CursorDataSource;

    invoke-interface {v3}, Lcom/google/android/repolib/database/CursorDataSource;->create()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 38
    .local v0, "cursor":Landroid/database/Cursor;
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 40
    .local v2, "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 41
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_0

    .line 42
    iget-object v3, p0, Lcom/google/android/repolib/database/DatabaseListCreator;->cursorDataSource:Lcom/google/android/repolib/database/CursorDataSource;

    invoke-interface {v3, v0}, Lcom/google/android/repolib/database/CursorDataSource;->createFrom(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 45
    :catch_0
    move-exception v1

    .line 46
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    iget-object v3, p0, Lcom/google/android/repolib/database/DatabaseListCreator;->errorHandler:Lcom/google/android/repolib/common/Action;

    invoke-interface {v3, v1}, Lcom/google/android/repolib/common/Action;->apply(Ljava/lang/Object;)V

    .line 47
    new-instance v2, Ljava/util/ArrayList;

    .end local v2    # "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 49
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 51
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-object v2

    .line 49
    .restart local v2    # "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .end local v2    # "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    :catchall_0
    move-exception v3

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v3
.end method
