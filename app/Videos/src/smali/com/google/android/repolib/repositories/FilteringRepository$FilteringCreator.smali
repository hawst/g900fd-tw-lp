.class Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator;
.super Ljava/lang/Object;
.source "FilteringRepository.java"

# interfaces
.implements Lcom/google/android/repolib/common/Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/repolib/repositories/FilteringRepository;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FilteringCreator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Creator",
        "<",
        "Ljava/util/List",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private final predicate:Lcom/google/android/repolib/common/Predicate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Predicate",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final source:Lcom/google/android/repolib/repositories/Repository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Predicate;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;",
            "Lcom/google/android/repolib/common/Predicate",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "this":Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator;, "Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator<TT;>;"
    .local p1, "source":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<TT;>;"
    .local p2, "predicate":Lcom/google/android/repolib/common/Predicate;, "Lcom/google/android/repolib/common/Predicate<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-static {p1}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/repositories/Repository;

    iput-object v0, p0, Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator;->source:Lcom/google/android/repolib/repositories/Repository;

    .line 44
    invoke-static {p2}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Predicate;

    iput-object v0, p0, Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator;->predicate:Lcom/google/android/repolib/common/Predicate;

    .line 45
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Predicate;Lcom/google/android/repolib/repositories/FilteringRepository$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/repolib/repositories/Repository;
    .param p2, "x1"    # Lcom/google/android/repolib/common/Predicate;
    .param p3, "x2"    # Lcom/google/android/repolib/repositories/FilteringRepository$1;

    .prologue
    .line 37
    .local p0, "this":Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator;, "Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator<TT;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator;-><init>(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Predicate;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic create()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    .local p0, "this":Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator;, "Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator<TT;>;"
    invoke-virtual {p0}, Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator;->create()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public create()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 50
    .local p0, "this":Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator;, "Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator<TT;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 51
    .local v2, "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    iget-object v3, p0, Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator;->source:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v3}, Lcom/google/android/repolib/repositories/Repository;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 52
    .local v1, "item":Ljava/lang/Object;, "TT;"
    iget-object v3, p0, Lcom/google/android/repolib/repositories/FilteringRepository$FilteringCreator;->predicate:Lcom/google/android/repolib/common/Predicate;

    invoke-interface {v3, v1}, Lcom/google/android/repolib/common/Predicate;->apply(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 53
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 56
    .end local v1    # "item":Ljava/lang/Object;, "TT;"
    :cond_1
    return-object v2
.end method
