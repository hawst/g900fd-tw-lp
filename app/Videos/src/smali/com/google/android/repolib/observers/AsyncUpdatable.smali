.class public final Lcom/google/android/repolib/observers/AsyncUpdatable;
.super Ljava/lang/Object;
.source "AsyncUpdatable.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/observers/Updatable;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field private final executor:Ljava/util/concurrent/Executor;

.field private final updatable:Lcom/google/android/repolib/observers/Updatable;


# direct methods
.method private constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "executor"    # Ljava/util/concurrent/Executor;
    .param p2, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 15
    .local p0, "this":Lcom/google/android/repolib/observers/AsyncUpdatable;, "Lcom/google/android/repolib/observers/AsyncUpdatable<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-static {p1}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdatable;->executor:Ljava/util/concurrent/Executor;

    .line 17
    invoke-static {p2}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/observers/Updatable;

    iput-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdatable;->updatable:Lcom/google/android/repolib/observers/Updatable;

    .line 18
    return-void
.end method

.method public static asyncUpdatable(Ljava/util/concurrent/Executor;Lcom/google/android/repolib/observers/Updatable;)Lcom/google/android/repolib/observers/Updatable;
    .locals 1
    .param p0, "executor"    # Ljava/util/concurrent/Executor;
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/repolib/observers/Updatable;",
            ")",
            "Lcom/google/android/repolib/observers/Updatable;"
        }
    .end annotation

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/repolib/observers/AsyncUpdatable;

    invoke-direct {v0, p0, p1}, Lcom/google/android/repolib/observers/AsyncUpdatable;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/repolib/observers/Updatable;)V

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 27
    .local p0, "this":Lcom/google/android/repolib/observers/AsyncUpdatable;, "Lcom/google/android/repolib/observers/AsyncUpdatable<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdatable;->updatable:Lcom/google/android/repolib/observers/Updatable;

    invoke-interface {v0}, Lcom/google/android/repolib/observers/Updatable;->update()V

    .line 28
    return-void
.end method

.method public update()V
    .locals 1

    .prologue
    .line 32
    .local p0, "this":Lcom/google/android/repolib/observers/AsyncUpdatable;, "Lcom/google/android/repolib/observers/AsyncUpdatable<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdatable;->executor:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 33
    return-void
.end method
