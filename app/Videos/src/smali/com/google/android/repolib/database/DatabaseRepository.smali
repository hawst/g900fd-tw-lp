.class public final Lcom/google/android/repolib/database/DatabaseRepository;
.super Ljava/lang/Object;
.source "DatabaseRepository.java"


# direct methods
.method public static databaseRepository(Ljava/util/concurrent/Executor;Lcom/google/android/repolib/database/CursorDataSource;Lcom/google/android/repolib/observers/Observable;Lcom/google/android/repolib/common/Action;)Lcom/google/android/repolib/repositories/Repository;
    .locals 1
    .param p0, "executor"    # Ljava/util/concurrent/Executor;
    .param p2, "observable"    # Lcom/google/android/repolib/observers/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/repolib/database/CursorDataSource",
            "<+TT;>;",
            "Lcom/google/android/repolib/observers/Observable;",
            "Lcom/google/android/repolib/common/Action",
            "<",
            "Ljava/lang/Throwable;",
            ">;)",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 23
    .local p1, "cursorDataSource":Lcom/google/android/repolib/database/CursorDataSource;, "Lcom/google/android/repolib/database/CursorDataSource<+TT;>;"
    .local p3, "errorHandler":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<Ljava/lang/Throwable;>;"
    invoke-static {p1, p3}, Lcom/google/android/repolib/database/DatabaseListCreator;->databaseListCreator(Lcom/google/android/repolib/database/CursorDataSource;Lcom/google/android/repolib/common/Action;)Lcom/google/android/repolib/common/Creator;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lcom/google/android/repolib/repositories/CreatorRepository;->asyncCreatorRepository(Ljava/util/concurrent/Executor;Lcom/google/android/repolib/common/Creator;Lcom/google/android/repolib/observers/Observable;)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v0

    return-object v0
.end method
