.class public final Lcom/google/android/repolib/leanback/RepositoryListRow;
.super Ljava/lang/Object;
.source "RepositoryListRow.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final repository:Lcom/google/android/repolib/repositories/Repository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private final title:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/google/android/repolib/repositories/Repository;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p0, "this":Lcom/google/android/repolib/leanback/RepositoryListRow;, "Lcom/google/android/repolib/leanback/RepositoryListRow<TT;>;"
    .local p2, "repository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p2, p0, Lcom/google/android/repolib/leanback/RepositoryListRow;->repository:Lcom/google/android/repolib/repositories/Repository;

    .line 16
    iput-object p1, p0, Lcom/google/android/repolib/leanback/RepositoryListRow;->title:Ljava/lang/String;

    .line 17
    return-void
.end method

.method public static repositoryListRow(Ljava/lang/String;Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/leanback/RepositoryListRow;
    .locals 1
    .param p0, "title"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;)",
            "Lcom/google/android/repolib/leanback/RepositoryListRow",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 20
    .local p1, "repository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    new-instance v0, Lcom/google/android/repolib/leanback/RepositoryListRow;

    invoke-direct {v0, p0, p1}, Lcom/google/android/repolib/leanback/RepositoryListRow;-><init>(Ljava/lang/String;Lcom/google/android/repolib/repositories/Repository;)V

    return-object v0
.end method


# virtual methods
.method public getRepository()Lcom/google/android/repolib/repositories/Repository;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "this":Lcom/google/android/repolib/leanback/RepositoryListRow;, "Lcom/google/android/repolib/leanback/RepositoryListRow<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/leanback/RepositoryListRow;->repository:Lcom/google/android/repolib/repositories/Repository;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    .local p0, "this":Lcom/google/android/repolib/leanback/RepositoryListRow;, "Lcom/google/android/repolib/leanback/RepositoryListRow<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/leanback/RepositoryListRow;->title:Ljava/lang/String;

    return-object v0
.end method
