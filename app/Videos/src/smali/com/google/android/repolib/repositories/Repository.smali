.class public interface abstract Lcom/google/android/repolib/repositories/Repository;
.super Ljava/lang/Object;
.source "Repository.java"

# interfaces
.implements Lcom/google/android/repolib/common/Indexable;
.implements Lcom/google/android/repolib/observers/Observable;
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Indexable",
        "<TT;>;",
        "Lcom/google/android/repolib/observers/Observable;",
        "Ljava/lang/Iterable",
        "<TT;>;"
    }
.end annotation
