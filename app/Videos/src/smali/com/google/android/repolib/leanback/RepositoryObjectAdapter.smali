.class public final Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;
.super Landroid/support/v17/leanback/widget/ObjectAdapter;
.source "RepositoryObjectAdapter.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/support/v17/leanback/widget/ObjectAdapter;",
        "Lcom/google/android/repolib/observers/Updatable;"
    }
.end annotation


# instance fields
.field private indexer:Lcom/google/android/repolib/common/Indexer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Indexer",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final itemIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final repository:Lcom/google/android/repolib/repositories/Repository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/v17/leanback/widget/PresenterSelector;Lcom/google/android/repolib/repositories/Repository;)V
    .locals 1
    .param p1, "presenterSelector"    # Landroid/support/v17/leanback/widget/PresenterSelector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v17/leanback/widget/PresenterSelector;",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "this":Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;, "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter<TT;>;"
    .local p2, "repository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<TT;>;"
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/ObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 16
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->itemIds:Ljava/util/Map;

    .line 26
    iput-object p2, p0, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->repository:Lcom/google/android/repolib/repositories/Repository;

    .line 27
    invoke-interface {p2}, Lcom/google/android/repolib/repositories/Repository;->indexer()Lcom/google/android/repolib/common/Indexer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->indexer:Lcom/google/android/repolib/common/Indexer;

    .line 28
    return-void
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 38
    .local p0, "this":Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;, "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->indexer:Lcom/google/android/repolib/common/Indexer;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/common/Indexer;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getId(I)J
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 43
    .local p0, "this":Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;, "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter<TT;>;"
    iget-object v4, p0, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->indexer:Lcom/google/android/repolib/common/Indexer;

    invoke-interface {v4, p1}, Lcom/google/android/repolib/common/Indexer;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 44
    .local v2, "item":Ljava/lang/Object;, "TT;"
    instance-of v4, v2, Lcom/google/android/repolib/common/Entity;

    if-eqz v4, :cond_1

    .line 45
    check-cast v2, Lcom/google/android/repolib/common/Entity;

    .end local v2    # "item":Ljava/lang/Object;, "TT;"
    invoke-interface {v2}, Lcom/google/android/repolib/common/Entity;->getEntityId()Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "entityId":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->itemIds:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 47
    .local v1, "id":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    .line 48
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v4, v4

    .line 54
    .end local v0    # "entityId":Ljava/lang/String;
    .end local v1    # "id":Ljava/lang/Integer;
    :goto_0
    return-wide v4

    .line 50
    .restart local v0    # "entityId":Ljava/lang/String;
    .restart local v1    # "id":Ljava/lang/Integer;
    :cond_0
    iget-object v4, p0, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->itemIds:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    add-int/lit8 v3, v4, 0x1

    .line 51
    .local v3, "nextId":I
    iget-object v4, p0, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->itemIds:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    int-to-long v4, v3

    goto :goto_0

    .line 54
    .end local v0    # "entityId":Ljava/lang/String;
    .end local v1    # "id":Ljava/lang/Integer;
    .end local v3    # "nextId":I
    .restart local v2    # "item":Ljava/lang/Object;, "TT;"
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    int-to-long v4, v4

    const/16 v6, 0x20

    shl-long/2addr v4, v6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v6

    int-to-long v6, v6

    or-long/2addr v4, v6

    goto :goto_0
.end method

.method public getRepository()Lcom/google/android/repolib/repositories/Repository;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 65
    .local p0, "this":Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;, "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->repository:Lcom/google/android/repolib/repositories/Repository;

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 32
    .local p0, "this":Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;, "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->indexer:Lcom/google/android/repolib/common/Indexer;

    invoke-interface {v0}, Lcom/google/android/repolib/common/Indexer;->size()I

    move-result v0

    return v0
.end method

.method public update()V
    .locals 1

    .prologue
    .line 59
    .local p0, "this":Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;, "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->repository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0}, Lcom/google/android/repolib/repositories/Repository;->indexer()Lcom/google/android/repolib/common/Indexer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->indexer:Lcom/google/android/repolib/common/Indexer;

    .line 60
    invoke-virtual {p0}, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->notifyChanged()V

    .line 61
    return-void
.end method
