.class public final Lcom/google/android/repolib/common/ConcatenatingIndexer;
.super Ljava/lang/Object;
.source "ConcatenatingIndexer.java"

# interfaces
.implements Lcom/google/android/repolib/common/Indexer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Indexer",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final firstIndexer:Lcom/google/android/repolib/common/Indexer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Indexer",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private final secondIndexer:Lcom/google/android/repolib/common/Indexer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Indexer",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/repolib/common/Indexer;Lcom/google/android/repolib/common/Indexer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/common/Indexer",
            "<+TT;>;",
            "Lcom/google/android/repolib/common/Indexer",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p0, "this":Lcom/google/android/repolib/common/ConcatenatingIndexer;, "Lcom/google/android/repolib/common/ConcatenatingIndexer<TT;>;"
    .local p1, "firstIndexer":Lcom/google/android/repolib/common/Indexer;, "Lcom/google/android/repolib/common/Indexer<+TT;>;"
    .local p2, "secondIndexer":Lcom/google/android/repolib/common/Indexer;, "Lcom/google/android/repolib/common/Indexer<+TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    invoke-static {p1}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Indexer;

    iput-object v0, p0, Lcom/google/android/repolib/common/ConcatenatingIndexer;->firstIndexer:Lcom/google/android/repolib/common/Indexer;

    .line 16
    invoke-static {p2}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Indexer;

    iput-object v0, p0, Lcom/google/android/repolib/common/ConcatenatingIndexer;->secondIndexer:Lcom/google/android/repolib/common/Indexer;

    .line 17
    return-void
.end method

.method public static concatenatingIndexer(Lcom/google/android/repolib/common/Indexer;Lcom/google/android/repolib/common/Indexer;)Lcom/google/android/repolib/common/Indexer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/repolib/common/Indexer",
            "<+TT;>;",
            "Lcom/google/android/repolib/common/Indexer",
            "<+TT;>;)",
            "Lcom/google/android/repolib/common/Indexer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "firstIndexer":Lcom/google/android/repolib/common/Indexer;, "Lcom/google/android/repolib/common/Indexer<+TT;>;"
    .local p1, "secondIndexer":Lcom/google/android/repolib/common/Indexer;, "Lcom/google/android/repolib/common/Indexer<+TT;>;"
    new-instance v0, Lcom/google/android/repolib/common/ConcatenatingIndexer;

    invoke-direct {v0, p0, p1}, Lcom/google/android/repolib/common/ConcatenatingIndexer;-><init>(Lcom/google/android/repolib/common/Indexer;Lcom/google/android/repolib/common/Indexer;)V

    return-object v0
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 3
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lcom/google/android/repolib/common/ConcatenatingIndexer;, "Lcom/google/android/repolib/common/ConcatenatingIndexer<TT;>;"
    iget-object v1, p0, Lcom/google/android/repolib/common/ConcatenatingIndexer;->firstIndexer:Lcom/google/android/repolib/common/Indexer;

    invoke-interface {v1}, Lcom/google/android/repolib/common/Indexer;->size()I

    move-result v0

    .line 29
    .local v0, "firstSize":I
    if-ge p1, v0, :cond_0

    iget-object v1, p0, Lcom/google/android/repolib/common/ConcatenatingIndexer;->firstIndexer:Lcom/google/android/repolib/common/Indexer;

    invoke-interface {v1, p1}, Lcom/google/android/repolib/common/Indexer;->get(I)Ljava/lang/Object;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/repolib/common/ConcatenatingIndexer;->secondIndexer:Lcom/google/android/repolib/common/Indexer;

    sub-int v2, p1, v0

    invoke-interface {v1, v2}, Lcom/google/android/repolib/common/Indexer;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public size()I
    .locals 2

    .prologue
    .line 35
    .local p0, "this":Lcom/google/android/repolib/common/ConcatenatingIndexer;, "Lcom/google/android/repolib/common/ConcatenatingIndexer<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/common/ConcatenatingIndexer;->firstIndexer:Lcom/google/android/repolib/common/Indexer;

    invoke-interface {v0}, Lcom/google/android/repolib/common/Indexer;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/repolib/common/ConcatenatingIndexer;->secondIndexer:Lcom/google/android/repolib/common/Indexer;

    invoke-interface {v1}, Lcom/google/android/repolib/common/Indexer;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
