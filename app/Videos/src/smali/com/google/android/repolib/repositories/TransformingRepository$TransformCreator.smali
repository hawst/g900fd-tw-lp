.class Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator;
.super Ljava/lang/Object;
.source "TransformingRepository.java"

# interfaces
.implements Lcom/google/android/repolib/common/Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/repolib/repositories/TransformingRepository;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TransformCreator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "F:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Creator",
        "<",
        "Ljava/util/List",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private final fromRepository:Lcom/google/android/repolib/repositories/Repository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/Repository",
            "<TF;>;"
        }
    .end annotation
.end field

.field private final transformingFactory:Lcom/google/android/repolib/common/Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Factory",
            "<+TT;TF;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Factory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TF;>;",
            "Lcom/google/android/repolib/common/Factory",
            "<+TT;TF;>;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p0, "this":Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator;, "Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator<TT;TF;>;"
    .local p1, "fromRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<TF;>;"
    .local p2, "transformingFactory":Lcom/google/android/repolib/common/Factory;, "Lcom/google/android/repolib/common/Factory<+TT;TF;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator;->fromRepository:Lcom/google/android/repolib/repositories/Repository;

    .line 49
    iput-object p2, p0, Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator;->transformingFactory:Lcom/google/android/repolib/common/Factory;

    .line 50
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Factory;Lcom/google/android/repolib/repositories/TransformingRepository$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/repolib/repositories/Repository;
    .param p2, "x1"    # Lcom/google/android/repolib/common/Factory;
    .param p3, "x2"    # Lcom/google/android/repolib/repositories/TransformingRepository$1;

    .prologue
    .line 42
    .local p0, "this":Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator;, "Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator<TT;TF;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator;-><init>(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/common/Factory;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic create()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    .local p0, "this":Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator;, "Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator<TT;TF;>;"
    invoke-virtual {p0}, Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator;->create()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public create()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "this":Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator;, "Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator<TT;TF;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 56
    .local v2, "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    iget-object v3, p0, Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator;->fromRepository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v3}, Lcom/google/android/repolib/repositories/Repository;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 57
    .local v0, "from":Ljava/lang/Object;, "TF;"
    iget-object v3, p0, Lcom/google/android/repolib/repositories/TransformingRepository$TransformCreator;->transformingFactory:Lcom/google/android/repolib/common/Factory;

    invoke-interface {v3, v0}, Lcom/google/android/repolib/common/Factory;->createFrom(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 59
    .end local v0    # "from":Ljava/lang/Object;, "TF;"
    :cond_0
    return-object v2
.end method
