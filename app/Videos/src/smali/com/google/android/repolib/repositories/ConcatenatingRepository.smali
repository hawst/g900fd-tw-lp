.class public final Lcom/google/android/repolib/repositories/ConcatenatingRepository;
.super Ljava/lang/Object;
.source "ConcatenatingRepository.java"

# interfaces
.implements Lcom/google/android/repolib/repositories/Repository;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/repositories/Repository",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final firstRepository:Lcom/google/android/repolib/repositories/Repository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private final secondRepository:Lcom/google/android/repolib/repositories/Repository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/repositories/Repository;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;",
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "this":Lcom/google/android/repolib/repositories/ConcatenatingRepository;, "Lcom/google/android/repolib/repositories/ConcatenatingRepository<TT;>;"
    .local p1, "firstRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    .local p2, "secondRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/repositories/Repository;

    iput-object v0, p0, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->firstRepository:Lcom/google/android/repolib/repositories/Repository;

    .line 32
    invoke-static {p2}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/repositories/Repository;

    iput-object v0, p0, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->secondRepository:Lcom/google/android/repolib/repositories/Repository;

    .line 33
    return-void
.end method

.method private static buildTree([Lcom/google/android/repolib/repositories/Repository;II)Lcom/google/android/repolib/repositories/Repository;
    .locals 4
    .param p1, "from"    # I
    .param p2, "to"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([",
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;II)",
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 54
    .local p0, "repositories":[Lcom/google/android/repolib/repositories/Repository;, "[Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    add-int/lit8 v1, p1, 0x1

    if-ne v1, p2, :cond_0

    .line 55
    aget-object v1, p0, p1

    .line 58
    :goto_0
    return-object v1

    .line 57
    :cond_0
    add-int v1, p1, p2

    div-int/lit8 v0, v1, 0x2

    .line 58
    .local v0, "split":I
    new-instance v1, Lcom/google/android/repolib/repositories/ConcatenatingRepository;

    invoke-static {p0, p1, v0}, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->buildTree([Lcom/google/android/repolib/repositories/Repository;II)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v2

    invoke-static {p0, v0, p2}, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->buildTree([Lcom/google/android/repolib/repositories/Repository;II)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/repolib/repositories/ConcatenatingRepository;-><init>(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/repositories/Repository;)V

    goto :goto_0
.end method

.method public static varargs concatenatingRepository(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/repositories/Repository;[Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/repositories/Repository;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;",
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;[",
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;)",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    .prologue
    .line 42
    .local p0, "firstRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    .local p1, "secondRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    .local p2, "nextRepositories":[Lcom/google/android/repolib/repositories/Repository;, "[Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    array-length v1, p2

    if-nez v1, :cond_0

    move-object v0, p1

    .line 47
    .local v0, "secondNode":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    :goto_0
    new-instance v1, Lcom/google/android/repolib/repositories/ConcatenatingRepository;

    invoke-direct {v1, p0, v0}, Lcom/google/android/repolib/repositories/ConcatenatingRepository;-><init>(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/repositories/Repository;)V

    return-object v1

    .line 42
    .end local v0    # "secondNode":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    :cond_0
    new-instance v0, Lcom/google/android/repolib/repositories/ConcatenatingRepository;

    const/4 v1, 0x0

    array-length v2, p2

    invoke-static {p2, v1, v2}, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->buildTree([Lcom/google/android/repolib/repositories/Repository;II)Lcom/google/android/repolib/repositories/Repository;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/android/repolib/repositories/ConcatenatingRepository;-><init>(Lcom/google/android/repolib/repositories/Repository;Lcom/google/android/repolib/repositories/Repository;)V

    goto :goto_0
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 67
    .local p0, "this":Lcom/google/android/repolib/repositories/ConcatenatingRepository;, "Lcom/google/android/repolib/repositories/ConcatenatingRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->firstRepository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/repositories/Repository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->secondRepository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/repositories/Repository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 69
    return-void
.end method

.method public indexer()Lcom/google/android/repolib/common/Indexer;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/repolib/common/Indexer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 85
    .local p0, "this":Lcom/google/android/repolib/repositories/ConcatenatingRepository;, "Lcom/google/android/repolib/repositories/ConcatenatingRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->firstRepository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0}, Lcom/google/android/repolib/repositories/Repository;->indexer()Lcom/google/android/repolib/common/Indexer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->secondRepository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v1}, Lcom/google/android/repolib/repositories/Repository;->indexer()Lcom/google/android/repolib/common/Indexer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/repolib/common/ConcatenatingIndexer;->concatenatingIndexer(Lcom/google/android/repolib/common/Indexer;Lcom/google/android/repolib/common/Indexer;)Lcom/google/android/repolib/common/Indexer;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 79
    .local p0, "this":Lcom/google/android/repolib/repositories/ConcatenatingRepository;, "Lcom/google/android/repolib/repositories/ConcatenatingRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->firstRepository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0}, Lcom/google/android/repolib/repositories/Repository;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->secondRepository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v1}, Lcom/google/android/repolib/repositories/Repository;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/repolib/common/ConcatenatingIterator;->concatenatingIterator(Ljava/util/Iterator;Ljava/util/Iterator;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 73
    .local p0, "this":Lcom/google/android/repolib/repositories/ConcatenatingRepository;, "Lcom/google/android/repolib/repositories/ConcatenatingRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->firstRepository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/repositories/Repository;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/repolib/repositories/ConcatenatingRepository;->secondRepository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/repositories/Repository;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 75
    return-void
.end method
