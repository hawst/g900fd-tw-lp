.class public final Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;
.super Ljava/lang/Object;
.source "AsyncUpdateDispatcher.java"

# interfaces
.implements Lcom/google/android/repolib/observers/UpdateDispatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;
    }
.end annotation


# instance fields
.field private final dispatchUpdatesRunnable:Ljava/lang/Runnable;

.field private final firstUpdatableAddedRunnable:Ljava/lang/Runnable;

.field private final handler:Landroid/os/Handler;

.field private final lastUpdatableRemovedRunnable:Ljava/lang/Runnable;

.field private final updatables:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/repolib/observers/Updatable;",
            "Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/repolib/observers/UpdatablesChanged;)V
    .locals 1
    .param p1, "updatablesChanged"    # Lcom/google/android/repolib/observers/UpdatablesChanged;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->updatables:Ljava/util/Map;

    .line 45
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->handler:Landroid/os/Handler;

    .line 46
    new-instance v0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$1;-><init>(Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;Lcom/google/android/repolib/observers/UpdatablesChanged;)V

    iput-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->firstUpdatableAddedRunnable:Ljava/lang/Runnable;

    .line 52
    new-instance v0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$2;-><init>(Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;Lcom/google/android/repolib/observers/UpdatablesChanged;)V

    iput-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->lastUpdatableRemovedRunnable:Ljava/lang/Runnable;

    .line 58
    new-instance v0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$3;

    invoke-direct {v0, p0}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$3;-><init>(Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;)V

    iput-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->dispatchUpdatesRunnable:Ljava/lang/Runnable;

    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->dispatchUpdates()V

    return-void
.end method

.method public static asyncUpdateDispatcher()Lcom/google/android/repolib/observers/UpdateDispatcher;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;

    invoke-static {}, Lcom/google/android/repolib/observers/NullUpdatablesChanged;->nullUpdatablesChanged()Lcom/google/android/repolib/observers/UpdatablesChanged;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;-><init>(Lcom/google/android/repolib/observers/UpdatablesChanged;)V

    return-object v0
.end method

.method public static asyncUpdateDispatcher(Lcom/google/android/repolib/observers/UpdatablesChanged;)Lcom/google/android/repolib/observers/UpdateDispatcher;
    .locals 1
    .param p0, "observable"    # Lcom/google/android/repolib/observers/UpdatablesChanged;

    .prologue
    .line 74
    new-instance v0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;

    invoke-direct {v0, p0}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;-><init>(Lcom/google/android/repolib/observers/UpdatablesChanged;)V

    return-object v0
.end method

.method private declared-synchronized dispatchUpdates()V
    .locals 4

    .prologue
    .line 101
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->updatables:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;

    .line 102
    .local v1, "updatable":Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;
    iget-object v2, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->updatables:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;->getKey()Lcom/google/android/repolib/observers/Updatable;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 103
    invoke-virtual {v1}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;->update()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 101
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "updatable":Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 106
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public declared-synchronized addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 2
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->updatables:Ljava/util/Map;

    new-instance v1, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;

    invoke-direct {v1, p1}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;-><init>(Lcom/google/android/repolib/observers/Updatable;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    iget-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->updatables:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->firstUpdatableAddedRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    :cond_0
    monitor-exit p0

    return-void

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 3
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 87
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->updatables:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;

    .line 88
    .local v0, "postingUpdatable":Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Updatable not added, cannot remove."

    invoke-static {v1, v2}, Lcom/google/android/repolib/common/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 89
    invoke-virtual {v0}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;->cancel()V

    .line 90
    iget-object v1, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->updatables:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    iget-object v1, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->lastUpdatableRemovedRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :cond_0
    monitor-exit p0

    return-void

    .line 88
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 87
    .end local v0    # "postingUpdatable":Lcom/google/android/repolib/observers/AsyncUpdateDispatcher$PostingUpdatable;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public update()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->dispatchUpdatesRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 98
    return-void
.end method
