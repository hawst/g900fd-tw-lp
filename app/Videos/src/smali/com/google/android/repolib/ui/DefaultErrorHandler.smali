.class public final Lcom/google/android/repolib/ui/DefaultErrorHandler;
.super Ljava/lang/Object;
.source "DefaultErrorHandler.java"

# interfaces
.implements Lcom/google/android/repolib/common/Action;
.implements Lcom/google/android/repolib/common/Binder;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Action",
        "<",
        "Ljava/lang/Throwable;",
        ">;",
        "Lcom/google/android/repolib/common/Binder",
        "<",
        "Ljava/lang/Class",
        "<+",
        "Ljava/lang/Throwable;",
        ">;",
        "Lcom/google/android/repolib/common/Action",
        "<",
        "Ljava/lang/Throwable;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final errorHandler:Lcom/google/android/repolib/common/Action;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/Action",
            "<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field

.field private final innerErrorHandler:Lcom/google/android/repolib/common/ClassMappingAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/common/ClassMappingAction",
            "<",
            "Ljava/lang/Throwable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/google/android/repolib/common/Action;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/repolib/common/Action",
            "<",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p2, "throwableAction":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<Ljava/lang/Throwable;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {p2}, Lcom/google/android/repolib/common/ClassMappingAction;->classMappingAction(Lcom/google/android/repolib/common/Action;)Lcom/google/android/repolib/common/ClassMappingAction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/repolib/ui/DefaultErrorHandler;->innerErrorHandler:Lcom/google/android/repolib/common/ClassMappingAction;

    .line 25
    iget-object v0, p0, Lcom/google/android/repolib/ui/DefaultErrorHandler;->innerErrorHandler:Lcom/google/android/repolib/common/ClassMappingAction;

    invoke-static {p1, v0}, Lcom/google/android/repolib/common/LogErrorThrowableAction;->logErrorThrowableAction(Ljava/lang/String;Lcom/google/android/repolib/common/Action;)Lcom/google/android/repolib/common/Action;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/repolib/common/PostingAction;->postingAction(Lcom/google/android/repolib/common/Action;)Lcom/google/android/repolib/common/Action;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/repolib/ui/DefaultErrorHandler;->errorHandler:Lcom/google/android/repolib/common/Action;

    .line 26
    return-void
.end method

.method public static errorHandler(Ljava/lang/String;)Lcom/google/android/repolib/ui/DefaultErrorHandler;
    .locals 2
    .param p0, "tag"    # Ljava/lang/String;

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/repolib/ui/DefaultErrorHandler;

    invoke-static {}, Lcom/google/android/repolib/common/NullAction;->nullAction()Lcom/google/android/repolib/common/Action;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/repolib/ui/DefaultErrorHandler;-><init>(Ljava/lang/String;Lcom/google/android/repolib/common/Action;)V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 15
    check-cast p1, Ljava/lang/Throwable;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/repolib/ui/DefaultErrorHandler;->apply(Ljava/lang/Throwable;)V

    return-void
.end method

.method public apply(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "throwable"    # Ljava/lang/Throwable;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/repolib/ui/DefaultErrorHandler;->errorHandler:Lcom/google/android/repolib/common/Action;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/common/Action;->apply(Ljava/lang/Object;)V

    .line 48
    return-void
.end method

.method public bind(Ljava/lang/Class;Lcom/google/android/repolib/common/Action;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/Throwable;",
            ">;",
            "Lcom/google/android/repolib/common/Action",
            "<",
            "Ljava/lang/Throwable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Throwable;>;"
    .local p2, "throwableAction":Lcom/google/android/repolib/common/Action;, "Lcom/google/android/repolib/common/Action<Ljava/lang/Throwable;>;"
    iget-object v0, p0, Lcom/google/android/repolib/ui/DefaultErrorHandler;->innerErrorHandler:Lcom/google/android/repolib/common/ClassMappingAction;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/repolib/common/ClassMappingAction;->bind(Ljava/lang/Class;Lcom/google/android/repolib/common/Action;)V

    .line 43
    return-void
.end method

.method public bridge synthetic bind(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 15
    check-cast p1, Ljava/lang/Class;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/repolib/common/Action;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/repolib/ui/DefaultErrorHandler;->bind(Ljava/lang/Class;Lcom/google/android/repolib/common/Action;)V

    return-void
.end method
