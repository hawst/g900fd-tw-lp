.class public final Lcom/google/android/repolib/common/ListIndexer;
.super Ljava/lang/Object;
.source "ListIndexer.java"

# interfaces
.implements Lcom/google/android/repolib/common/Indexer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Indexer",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 13
    .local p0, "this":Lcom/google/android/repolib/common/ListIndexer;, "Lcom/google/android/repolib/common/ListIndexer<TT;>;"
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    invoke-static {p1}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/repolib/common/ListIndexer;->list:Ljava/util/List;

    .line 15
    return-void
.end method

.method public static listIndexer(Ljava/util/List;)Lcom/google/android/repolib/common/Indexer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)",
            "Lcom/google/android/repolib/common/Indexer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 18
    .local p0, "items":Ljava/util/List;, "Ljava/util/List<TT;>;"
    new-instance v0, Lcom/google/android/repolib/common/ListIndexer;

    invoke-direct {v0, p0}, Lcom/google/android/repolib/common/ListIndexer;-><init>(Ljava/util/List;)V

    return-object v0
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "this":Lcom/google/android/repolib/common/ListIndexer;, "Lcom/google/android/repolib/common/ListIndexer<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/common/ListIndexer;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 29
    .local p0, "this":Lcom/google/android/repolib/common/ListIndexer;, "Lcom/google/android/repolib/common/ListIndexer<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/common/ListIndexer;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
