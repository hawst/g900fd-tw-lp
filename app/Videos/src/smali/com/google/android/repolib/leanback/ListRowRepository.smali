.class public final Lcom/google/android/repolib/leanback/ListRowRepository;
.super Ljava/lang/Object;
.source "ListRowRepository.java"

# interfaces
.implements Lcom/google/android/repolib/observers/UpdatablesChanged;
.implements Lcom/google/android/repolib/repositories/Repository;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/repolib/leanback/ListRowRepository$EntityListRow;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/observers/UpdatablesChanged;",
        "Lcom/google/android/repolib/repositories/Repository",
        "<",
        "Landroid/support/v17/leanback/widget/ListRow;",
        ">;"
    }
.end annotation


# instance fields
.field private final adapterFactory:Lcom/google/android/repolib/leanback/ObjectAdapterFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/leanback/ObjectAdapterFactory",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final repository:Lcom/google/android/repolib/repositories/ArrayRepository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/ArrayRepository",
            "<",
            "Landroid/support/v17/leanback/widget/ListRow;",
            ">;"
        }
    .end annotation
.end field

.field private final repositoryListRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/repolib/leanback/RepositoryListRow",
            "<+TT;>;>;"
        }
    .end annotation
.end field

.field private final updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;


# direct methods
.method private varargs constructor <init>(Lcom/google/android/repolib/leanback/ObjectAdapterFactory;[Lcom/google/android/repolib/leanback/RepositoryListRow;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/leanback/ObjectAdapterFactory",
            "<TT;>;[",
            "Lcom/google/android/repolib/leanback/RepositoryListRow",
            "<+TT;>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    .prologue
    .line 33
    .local p0, "this":Lcom/google/android/repolib/leanback/ListRowRepository;, "Lcom/google/android/repolib/leanback/ListRowRepository<TT;>;"
    .local p1, "adapterFactory":Lcom/google/android/repolib/leanback/ObjectAdapterFactory;, "Lcom/google/android/repolib/leanback/ObjectAdapterFactory<TT;>;"
    .local p2, "repositoryListRows":[Lcom/google/android/repolib/leanback/RepositoryListRow;, "[Lcom/google/android/repolib/leanback/RepositoryListRow<+TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {}, Lcom/google/android/repolib/common/Preconditions;->checkMainThread()V

    .line 35
    iput-object p1, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->adapterFactory:Lcom/google/android/repolib/leanback/ObjectAdapterFactory;

    .line 36
    invoke-static {}, Lcom/google/android/repolib/repositories/ArrayRepository;->arrayRepository()Lcom/google/android/repolib/repositories/ArrayRepository;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->repositoryListRows:Ljava/util/List;

    .line 38
    invoke-static {p0}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher(Lcom/google/android/repolib/observers/UpdatablesChanged;)Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 39
    return-void
.end method

.method private addUpdatables()V
    .locals 5

    .prologue
    .line 97
    .local p0, "this":Lcom/google/android/repolib/leanback/ListRowRepository;, "Lcom/google/android/repolib/leanback/ListRowRepository<TT;>;"
    iget-object v4, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-virtual {v4}, Lcom/google/android/repolib/repositories/ArrayRepository;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/ListRow;

    .line 98
    .local v1, "listRow":Landroid/support/v17/leanback/widget/ListRow;
    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ListRow;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    check-cast v2, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;

    .line 100
    .local v2, "repositoryAdapter":Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;, "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter<+TT;>;"
    invoke-virtual {v2}, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->getRepository()Lcom/google/android/repolib/repositories/Repository;

    move-result-object v3

    .line 101
    .local v3, "rowRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    invoke-interface {v3, v2}, Lcom/google/android/repolib/repositories/Repository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 102
    invoke-virtual {v2}, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->update()V

    goto :goto_0

    .line 104
    .end local v1    # "listRow":Landroid/support/v17/leanback/widget/ListRow;
    .end local v2    # "repositoryAdapter":Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;, "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter<+TT;>;"
    .end local v3    # "rowRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    :cond_0
    return-void
.end method

.method public static varargs listRowRepository(Lcom/google/android/repolib/leanback/ObjectAdapterFactory;[Lcom/google/android/repolib/leanback/RepositoryListRow;)Lcom/google/android/repolib/leanback/ListRowRepository;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/repolib/leanback/ObjectAdapterFactory",
            "<TT;>;[",
            "Lcom/google/android/repolib/leanback/RepositoryListRow",
            "<+TT;>;)",
            "Lcom/google/android/repolib/leanback/ListRowRepository",
            "<TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    .prologue
    .line 45
    .local p0, "adapterFactory":Lcom/google/android/repolib/leanback/ObjectAdapterFactory;, "Lcom/google/android/repolib/leanback/ObjectAdapterFactory<TT;>;"
    .local p1, "repositoryListRows":[Lcom/google/android/repolib/leanback/RepositoryListRow;, "[Lcom/google/android/repolib/leanback/RepositoryListRow<+TT;>;"
    new-instance v0, Lcom/google/android/repolib/leanback/ListRowRepository;

    invoke-direct {v0, p0, p1}, Lcom/google/android/repolib/leanback/ListRowRepository;-><init>(Lcom/google/android/repolib/leanback/ObjectAdapterFactory;[Lcom/google/android/repolib/leanback/RepositoryListRow;)V

    return-object v0
.end method

.method private removeUpdatables()V
    .locals 4

    .prologue
    .line 88
    .local p0, "this":Lcom/google/android/repolib/leanback/ListRowRepository;, "Lcom/google/android/repolib/leanback/ListRowRepository<TT;>;"
    iget-object v3, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-virtual {v3}, Lcom/google/android/repolib/repositories/ArrayRepository;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/ListRow;

    .line 89
    .local v1, "listRow":Landroid/support/v17/leanback/widget/ListRow;
    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ListRow;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    check-cast v2, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;

    .line 91
    .local v2, "repositoryAdapter":Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;, "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter<+TT;>;"
    invoke-virtual {v2}, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->getRepository()Lcom/google/android/repolib/repositories/Repository;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/google/android/repolib/repositories/Repository;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    goto :goto_0

    .line 93
    .end local v1    # "listRow":Landroid/support/v17/leanback/widget/ListRow;
    .end local v2    # "repositoryAdapter":Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;, "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter<+TT;>;"
    :cond_0
    return-void
.end method

.method private update()V
    .locals 7

    .prologue
    .line 71
    .local p0, "this":Lcom/google/android/repolib/leanback/ListRowRepository;, "Lcom/google/android/repolib/leanback/ListRowRepository<TT;>;"
    invoke-direct {p0}, Lcom/google/android/repolib/leanback/ListRowRepository;->removeUpdatables()V

    .line 72
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 73
    .local v1, "entityListRows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/repolib/leanback/ListRowRepository$EntityListRow<TT;>;>;"
    iget-object v5, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->repositoryListRows:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/repolib/leanback/RepositoryListRow;

    .line 74
    .local v4, "repositoryListRow":Lcom/google/android/repolib/leanback/RepositoryListRow;, "Lcom/google/android/repolib/leanback/RepositoryListRow<+TT;>;"
    invoke-virtual {v4}, Lcom/google/android/repolib/leanback/RepositoryListRow;->getRepository()Lcom/google/android/repolib/repositories/Repository;

    move-result-object v3

    .line 75
    .local v3, "leanBackRowRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    iget-object v5, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->adapterFactory:Lcom/google/android/repolib/leanback/ObjectAdapterFactory;

    invoke-virtual {v5, v3}, Lcom/google/android/repolib/leanback/ObjectAdapterFactory;->createFrom(Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;

    move-result-object v0

    .line 77
    .local v0, "adapter":Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;, "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter<+TT;>;"
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;->setHasStableIds(Z)V

    .line 78
    new-instance v5, Lcom/google/android/repolib/leanback/ListRowRepository$EntityListRow;

    invoke-virtual {v4}, Lcom/google/android/repolib/leanback/RepositoryListRow;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v0}, Lcom/google/android/repolib/leanback/ListRowRepository$EntityListRow;-><init>(Ljava/lang/String;Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    invoke-interface {v3, v0}, Lcom/google/android/repolib/repositories/Repository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    goto :goto_0

    .line 81
    .end local v0    # "adapter":Lcom/google/android/repolib/leanback/RepositoryObjectAdapter;, "Lcom/google/android/repolib/leanback/RepositoryObjectAdapter<+TT;>;"
    .end local v3    # "leanBackRowRepository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    .end local v4    # "repositoryListRow":Lcom/google/android/repolib/leanback/RepositoryListRow;, "Lcom/google/android/repolib/leanback/RepositoryListRow<+TT;>;"
    :cond_0
    iget-object v5, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-virtual {v5, v1}, Lcom/google/android/repolib/repositories/ArrayRepository;->setItems(Ljava/util/Collection;)V

    .line 82
    invoke-direct {p0}, Lcom/google/android/repolib/leanback/ListRowRepository;->addUpdatables()V

    .line 83
    iget-object v5, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v5}, Lcom/google/android/repolib/observers/UpdateDispatcher;->update()V

    .line 84
    return-void
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 62
    .local p0, "this":Lcom/google/android/repolib/leanback/ListRowRepository;, "Lcom/google/android/repolib/leanback/ListRowRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 63
    return-void
.end method

.method public firstUpdatableAdded()V
    .locals 2

    .prologue
    .line 50
    .local p0, "this":Lcom/google/android/repolib/leanback/ListRowRepository;, "Lcom/google/android/repolib/leanback/ListRowRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    iget-object v1, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/repolib/repositories/ArrayRepository;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 51
    invoke-direct {p0}, Lcom/google/android/repolib/leanback/ListRowRepository;->update()V

    .line 52
    return-void
.end method

.method public indexer()Lcom/google/android/repolib/common/Indexer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/repolib/common/Indexer",
            "<",
            "Landroid/support/v17/leanback/widget/ListRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    .local p0, "this":Lcom/google/android/repolib/leanback/ListRowRepository;, "Lcom/google/android/repolib/leanback/ListRowRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-virtual {v0}, Lcom/google/android/repolib/repositories/ArrayRepository;->indexer()Lcom/google/android/repolib/common/Indexer;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Landroid/support/v17/leanback/widget/ListRow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    .local p0, "this":Lcom/google/android/repolib/leanback/ListRowRepository;, "Lcom/google/android/repolib/leanback/ListRowRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    invoke-virtual {v0}, Lcom/google/android/repolib/repositories/ArrayRepository;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public lastUpdatableRemoved()V
    .locals 2

    .prologue
    .line 56
    .local p0, "this":Lcom/google/android/repolib/leanback/ListRowRepository;, "Lcom/google/android/repolib/leanback/ListRowRepository<TT;>;"
    invoke-direct {p0}, Lcom/google/android/repolib/leanback/ListRowRepository;->removeUpdatables()V

    .line 57
    iget-object v0, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->repository:Lcom/google/android/repolib/repositories/ArrayRepository;

    iget-object v1, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/repolib/repositories/ArrayRepository;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 58
    return-void
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 67
    .local p0, "this":Lcom/google/android/repolib/leanback/ListRowRepository;, "Lcom/google/android/repolib/leanback/ListRowRepository<TT;>;"
    iget-object v0, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 68
    return-void
.end method

.method public setItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/repolib/leanback/RepositoryListRow",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 107
    .local p0, "this":Lcom/google/android/repolib/leanback/ListRowRepository;, "Lcom/google/android/repolib/leanback/ListRowRepository<TT;>;"
    .local p1, "repositoryListRows":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/repolib/leanback/RepositoryListRow<TT;>;>;"
    invoke-static {}, Lcom/google/android/repolib/common/Preconditions;->checkMainThread()V

    .line 108
    iget-object v0, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->repositoryListRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 109
    iget-object v0, p0, Lcom/google/android/repolib/leanback/ListRowRepository;->repositoryListRows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 110
    invoke-direct {p0}, Lcom/google/android/repolib/leanback/ListRowRepository;->update()V

    .line 111
    return-void
.end method
