.class public final Lcom/google/android/repolib/repositories/EmptyRepository;
.super Ljava/lang/Object;
.source "EmptyRepository.java"

# interfaces
.implements Lcom/google/android/repolib/repositories/Repository;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/repositories/Repository",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final EMPTY_REPOSITORY:Lcom/google/android/repolib/repositories/EmptyRepository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/EmptyRepository",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/google/android/repolib/repositories/EmptyRepository;

    invoke-direct {v0}, Lcom/google/android/repolib/repositories/EmptyRepository;-><init>()V

    sput-object v0, Lcom/google/android/repolib/repositories/EmptyRepository;->EMPTY_REPOSITORY:Lcom/google/android/repolib/repositories/EmptyRepository;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 17
    .local p0, "this":Lcom/google/android/repolib/repositories/EmptyRepository;, "Lcom/google/android/repolib/repositories/EmptyRepository<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method public static emptyRepository()Lcom/google/android/repolib/repositories/Repository;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/repolib/repositories/EmptyRepository;->EMPTY_REPOSITORY:Lcom/google/android/repolib/repositories/EmptyRepository;

    return-object v0
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 0
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 39
    .local p0, "this":Lcom/google/android/repolib/repositories/EmptyRepository;, "Lcom/google/android/repolib/repositories/EmptyRepository<TT;>;"
    return-void
.end method

.method public indexer()Lcom/google/android/repolib/common/Indexer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/repolib/common/Indexer",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "this":Lcom/google/android/repolib/repositories/EmptyRepository;, "Lcom/google/android/repolib/repositories/EmptyRepository<TT;>;"
    invoke-static {}, Lcom/google/android/repolib/common/EmptyIndexer;->emptyIndexer()Lcom/google/android/repolib/common/Indexer;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lcom/google/android/repolib/repositories/EmptyRepository;, "Lcom/google/android/repolib/repositories/EmptyRepository<TT;>;"
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 0
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 43
    .local p0, "this":Lcom/google/android/repolib/repositories/EmptyRepository;, "Lcom/google/android/repolib/repositories/EmptyRepository<TT;>;"
    return-void
.end method
