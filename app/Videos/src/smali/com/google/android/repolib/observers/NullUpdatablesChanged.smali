.class public final Lcom/google/android/repolib/observers/NullUpdatablesChanged;
.super Ljava/lang/Object;
.source "NullUpdatablesChanged.java"

# interfaces
.implements Lcom/google/android/repolib/observers/UpdatablesChanged;


# static fields
.field private static final NULL_UPDATABLES_CHANGED:Lcom/google/android/repolib/observers/NullUpdatablesChanged;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4
    new-instance v0, Lcom/google/android/repolib/observers/NullUpdatablesChanged;

    invoke-direct {v0}, Lcom/google/android/repolib/observers/NullUpdatablesChanged;-><init>()V

    sput-object v0, Lcom/google/android/repolib/observers/NullUpdatablesChanged;->NULL_UPDATABLES_CHANGED:Lcom/google/android/repolib/observers/NullUpdatablesChanged;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static nullUpdatablesChanged()Lcom/google/android/repolib/observers/UpdatablesChanged;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/google/android/repolib/observers/NullUpdatablesChanged;->NULL_UPDATABLES_CHANGED:Lcom/google/android/repolib/observers/NullUpdatablesChanged;

    return-object v0
.end method


# virtual methods
.method public firstUpdatableAdded()V
    .locals 0

    .prologue
    .line 12
    return-void
.end method

.method public lastUpdatableRemoved()V
    .locals 0

    .prologue
    .line 16
    return-void
.end method
