.class public final Lcom/google/android/repolib/common/EntityNotInRepositoryPredicate;
.super Ljava/lang/Object;
.source "EntityNotInRepositoryPredicate.java"

# interfaces
.implements Lcom/google/android/repolib/common/Predicate;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/repolib/common/Entity;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Predicate",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final repository:Lcom/google/android/repolib/repositories/Repository;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/repolib/repositories/Repository;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/repolib/repositories/Repository",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 17
    .local p0, "this":Lcom/google/android/repolib/common/EntityNotInRepositoryPredicate;, "Lcom/google/android/repolib/common/EntityNotInRepositoryPredicate<TT;>;"
    .local p1, "repository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<+TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {p1}, Lcom/google/android/repolib/common/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/repositories/Repository;

    iput-object v0, p0, Lcom/google/android/repolib/common/EntityNotInRepositoryPredicate;->repository:Lcom/google/android/repolib/repositories/Repository;

    .line 19
    return-void
.end method

.method public static entityNotInRepositoryPredicate(Lcom/google/android/repolib/repositories/Repository;)Lcom/google/android/repolib/common/Predicate;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/repolib/common/Entity;",
            ">(",
            "Lcom/google/android/repolib/repositories/Repository",
            "<TT;>;)",
            "Lcom/google/android/repolib/common/Predicate",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "repository":Lcom/google/android/repolib/repositories/Repository;, "Lcom/google/android/repolib/repositories/Repository<TT;>;"
    new-instance v0, Lcom/google/android/repolib/common/EntityNotInRepositoryPredicate;

    invoke-direct {v0, p0}, Lcom/google/android/repolib/common/EntityNotInRepositoryPredicate;-><init>(Lcom/google/android/repolib/repositories/Repository;)V

    return-object v0
.end method


# virtual methods
.method public apply(Lcom/google/android/repolib/common/Entity;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "this":Lcom/google/android/repolib/common/EntityNotInRepositoryPredicate;, "Lcom/google/android/repolib/common/EntityNotInRepositoryPredicate<TT;>;"
    .local p1, "input":Lcom/google/android/repolib/common/Entity;, "TT;"
    invoke-interface {p1}, Lcom/google/android/repolib/common/Entity;->getEntityId()Ljava/lang/String;

    move-result-object v2

    .line 30
    .local v2, "inputEntity":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/repolib/common/EntityNotInRepositoryPredicate;->repository:Lcom/google/android/repolib/repositories/Repository;

    invoke-interface {v3}, Lcom/google/android/repolib/repositories/Repository;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/repolib/common/Entity;

    .line 31
    .local v0, "element":Lcom/google/android/repolib/common/Entity;, "TT;"
    invoke-interface {v0}, Lcom/google/android/repolib/common/Entity;->getEntityId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 32
    const/4 v3, 0x0

    .line 35
    .end local v0    # "element":Lcom/google/android/repolib/common/Entity;, "TT;"
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 12
    .local p0, "this":Lcom/google/android/repolib/common/EntityNotInRepositoryPredicate;, "Lcom/google/android/repolib/common/EntityNotInRepositoryPredicate<TT;>;"
    check-cast p1, Lcom/google/android/repolib/common/Entity;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/repolib/common/EntityNotInRepositoryPredicate;->apply(Lcom/google/android/repolib/common/Entity;)Z

    move-result v0

    return v0
.end method
