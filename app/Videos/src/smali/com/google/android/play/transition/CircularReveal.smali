.class public Lcom/google/android/play/transition/CircularReveal;
.super Lcom/google/android/play/transition/ForcedVisibilityTransition;
.source "CircularReveal.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/transition/CircularReveal$KeyFrame;
    }
.end annotation


# instance fields
.field private mKeyFrames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/transition/CircularReveal$KeyFrame;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 55
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/play/transition/CircularReveal;-><init>(IFF)V

    .line 56
    return-void
.end method

.method public constructor <init>(IFF)V
    .locals 4
    .param p1, "mode"    # I
    .param p2, "startRadiusPct"    # F
    .param p3, "endRadiusPct"    # F

    .prologue
    const/4 v3, 0x0

    .line 65
    invoke-direct {p0, p1}, Lcom/google/android/play/transition/ForcedVisibilityTransition;-><init>(I)V

    .line 66
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/transition/CircularReveal;->mKeyFrames:Ljava/util/List;

    .line 67
    iget-object v0, p0, Lcom/google/android/play/transition/CircularReveal;->mKeyFrames:Ljava/util/List;

    new-instance v1, Lcom/google/android/play/transition/CircularReveal$KeyFrame;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p2, v3}, Lcom/google/android/play/transition/CircularReveal$KeyFrame;-><init>(FFLandroid/animation/TimeInterpolator;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    iget-object v0, p0, Lcom/google/android/play/transition/CircularReveal;->mKeyFrames:Ljava/util/List;

    new-instance v1, Lcom/google/android/play/transition/CircularReveal$KeyFrame;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, p3, v3}, Lcom/google/android/play/transition/CircularReveal$KeyFrame;-><init>(FFLandroid/animation/TimeInterpolator;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "startCenterOn"    # Landroid/graphics/Rect;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 46
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/play/transition/CircularReveal;-><init>(I)V

    .line 47
    const-string v0, "CircularReveal"

    const-string v1, "This constructor - CircularReveal(Rect) - is deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    return-void
.end method

.method private calculateDuration(Lcom/google/android/play/transition/CircularReveal$KeyFrame;Lcom/google/android/play/transition/CircularReveal$KeyFrame;J)J
    .locals 3
    .param p1, "prev"    # Lcom/google/android/play/transition/CircularReveal$KeyFrame;
    .param p2, "current"    # Lcom/google/android/play/transition/CircularReveal$KeyFrame;
    .param p3, "totalDuration"    # J

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 153
    invoke-virtual {p0}, Lcom/google/android/play/transition/CircularReveal;->isRevealing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p2, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mTimePct:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    long-to-float v1, p3

    mul-float/2addr v0, v1

    iget-object v1, p1, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mTimePct:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    long-to-float v2, p3

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-long v0, v0

    .line 157
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p2, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mTimePct:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sub-float v0, v2, v0

    long-to-float v1, p3

    mul-float/2addr v0, v1

    iget-object v1, p1, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mTimePct:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    sub-float v1, v2, v1

    long-to-float v2, p3

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-long v0, v0

    goto :goto_0
.end method

.method private static calculateMaxRadius(Landroid/view/View;)F
    .locals 4
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 181
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    int-to-float v1, v2

    .line 182
    .local v1, "widthSquared":F
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v3

    mul-int/2addr v2, v3

    int-to-float v0, v2

    .line 183
    .local v0, "heightSquared":F
    add-float v2, v1, v0

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    return v2
.end method


# virtual methods
.method public addKeyFrame(FF)Lcom/google/android/play/transition/CircularReveal;
    .locals 1
    .param p1, "time"    # F
    .param p2, "amount"    # F

    .prologue
    .line 77
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/play/transition/CircularReveal;->addKeyFrame(FFLandroid/animation/TimeInterpolator;)Lcom/google/android/play/transition/CircularReveal;

    .line 78
    return-object p0
.end method

.method public addKeyFrame(FFLandroid/animation/TimeInterpolator;)Lcom/google/android/play/transition/CircularReveal;
    .locals 2
    .param p1, "time"    # F
    .param p2, "amount"    # F
    .param p3, "interpolator"    # Landroid/animation/TimeInterpolator;

    .prologue
    .line 91
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/play/transition/CircularReveal;->mKeyFrames:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 93
    iget-object v0, p0, Lcom/google/android/play/transition/CircularReveal;->mKeyFrames:Ljava/util/List;

    new-instance v1, Lcom/google/android/play/transition/CircularReveal$KeyFrame;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/play/transition/CircularReveal$KeyFrame;-><init>(FFLandroid/animation/TimeInterpolator;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    :goto_0
    iget-object v0, p0, Lcom/google/android/play/transition/CircularReveal;->mKeyFrames:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 101
    return-object p0

    .line 94
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/google/android/play/transition/CircularReveal;->mKeyFrames:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/play/transition/CircularReveal;->mKeyFrames:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 96
    iget-object v0, p0, Lcom/google/android/play/transition/CircularReveal;->mKeyFrames:Ljava/util/List;

    new-instance v1, Lcom/google/android/play/transition/CircularReveal$KeyFrame;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/play/transition/CircularReveal$KeyFrame;-><init>(FFLandroid/animation/TimeInterpolator;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/transition/CircularReveal;->mKeyFrames:Ljava/util/List;

    new-instance v1, Lcom/google/android/play/transition/CircularReveal$KeyFrame;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/play/transition/CircularReveal$KeyFrame;-><init>(FFLandroid/animation/TimeInterpolator;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 12
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 107
    iget-object v9, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 108
    .local v9, "view":Landroid/view/View;
    invoke-static {v9}, Lcom/google/android/play/transition/CircularReveal;->calculateMaxRadius(Landroid/view/View;)F

    move-result v6

    .line 109
    .local v6, "maxRadius":F
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    div-int/lit8 v2, v10, 0x2

    .line 110
    .local v2, "centerX":I
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    div-int/lit8 v3, v10, 0x2

    .line 113
    .local v3, "centerY":I
    invoke-virtual {p0}, Lcom/google/android/play/transition/CircularReveal;->isRevealing()Z

    move-result v10

    if-nez v10, :cond_0

    .line 114
    iget-object v10, p0, Lcom/google/android/play/transition/CircularReveal;->mKeyFrames:Ljava/util/List;

    invoke-static {v10}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 118
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/google/android/play/transition/CircularReveal;->mKeyFrames:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-direct {v1, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 120
    .local v1, "animators":Ljava/util/List;, "Ljava/util/List<Landroid/animation/Animator;>;"
    iget-object v10, p0, Lcom/google/android/play/transition/CircularReveal;->mKeyFrames:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 121
    .local v5, "keyFramesIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/play/transition/CircularReveal$KeyFrame;>;"
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/play/transition/CircularReveal$KeyFrame;

    .line 123
    .local v7, "prevFrame":Lcom/google/android/play/transition/CircularReveal$KeyFrame;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 124
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/play/transition/CircularReveal$KeyFrame;

    .line 126
    .local v4, "currFrame":Lcom/google/android/play/transition/CircularReveal$KeyFrame;
    iget-object v10, v7, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mAmountPct:Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    mul-float/2addr v10, v6

    iget-object v11, v4, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mAmountPct:Ljava/lang/Float;

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v11

    mul-float/2addr v11, v6

    invoke-static {v9, v2, v3, v10, v11}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v0

    .line 128
    .local v0, "animator":Landroid/animation/Animator;
    invoke-virtual {p0}, Lcom/google/android/play/transition/CircularReveal;->getDuration()J

    move-result-wide v10

    invoke-direct {p0, v7, v4, v10, v11}, Lcom/google/android/play/transition/CircularReveal;->calculateDuration(Lcom/google/android/play/transition/CircularReveal$KeyFrame;Lcom/google/android/play/transition/CircularReveal$KeyFrame;J)J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 129
    invoke-virtual {p0}, Lcom/google/android/play/transition/CircularReveal;->isRevealing()Z

    move-result v10

    if-eqz v10, :cond_1

    iget-object v10, v4, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mInterpolator:Landroid/animation/TimeInterpolator;

    :goto_1
    invoke-virtual {v0, v10}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 131
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    move-object v7, v4

    .line 134
    goto :goto_0

    .line 129
    :cond_1
    iget-object v10, v7, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mInterpolator:Landroid/animation/TimeInterpolator;

    goto :goto_1

    .line 136
    .end local v0    # "animator":Landroid/animation/Animator;
    .end local v4    # "currFrame":Lcom/google/android/play/transition/CircularReveal$KeyFrame;
    :cond_2
    new-instance v8, Landroid/animation/AnimatorSet;

    invoke-direct {v8}, Landroid/animation/AnimatorSet;-><init>()V

    .line 137
    .local v8, "set":Landroid/animation/AnimatorSet;
    invoke-virtual {v8, v1}, Landroid/animation/AnimatorSet;->playSequentially(Ljava/util/List;)V

    .line 138
    invoke-virtual {p0}, Lcom/google/android/play/transition/CircularReveal;->getDuration()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 139
    new-instance v10, Lcom/google/android/play/transition/CircularReveal$1;

    invoke-direct {v10, p0, v9}, Lcom/google/android/play/transition/CircularReveal$1;-><init>(Lcom/google/android/play/transition/CircularReveal;Landroid/view/View;)V

    invoke-virtual {v8, v10}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 149
    new-instance v10, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;

    invoke-direct {v10, v8}, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;-><init>(Landroid/animation/Animator;)V

    return-object v10
.end method

.method public getInterpolator()Landroid/animation/TimeInterpolator;
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/play/transition/CircularReveal;->mKeyFrames:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/play/transition/CircularReveal;->mKeyFrames:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/transition/CircularReveal$KeyFrame;

    iget-object v0, v0, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mInterpolator:Landroid/animation/TimeInterpolator;

    return-object v0
.end method

.method public setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;
    .locals 2
    .param p1, "interpolator"    # Landroid/animation/TimeInterpolator;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/play/transition/CircularReveal;->mKeyFrames:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/play/transition/CircularReveal;->mKeyFrames:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/transition/CircularReveal$KeyFrame;

    iput-object p1, v0, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mInterpolator:Landroid/animation/TimeInterpolator;

    .line 169
    return-object p0
.end method
