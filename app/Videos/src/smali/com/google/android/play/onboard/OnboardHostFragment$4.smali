.class Lcom/google/android/play/onboard/OnboardHostFragment$4;
.super Lcom/google/android/play/widget/UserAwareViewPager$UserAwareOnPageChangeListener;
.source "OnboardHostFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/onboard/OnboardHostFragment;->setUpPager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/onboard/OnboardHostFragment;


# direct methods
.method constructor <init>(Lcom/google/android/play/onboard/OnboardHostFragment;Lcom/google/android/play/widget/UserAwareViewPager;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/play/widget/UserAwareViewPager;

    .prologue
    .line 256
    iput-object p1, p0, Lcom/google/android/play/onboard/OnboardHostFragment$4;->this$0:Lcom/google/android/play/onboard/OnboardHostFragment;

    invoke-direct {p0, p2}, Lcom/google/android/play/widget/UserAwareViewPager$UserAwareOnPageChangeListener;-><init>(Lcom/google/android/play/widget/UserAwareViewPager;)V

    return-void
.end method


# virtual methods
.method public onPageScrolled(IFI)V
    .locals 1
    .param p1, "visualPosition"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 267
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/play/widget/UserAwareViewPager$UserAwareOnPageChangeListener;->onPageScrolled(IFI)V

    .line 268
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment$4;->this$0:Lcom/google/android/play/onboard/OnboardHostFragment;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/play/onboard/OnboardHostFragment;->onPageScrolled(IFI)V

    .line 270
    return-void
.end method

.method public onPageSelected(IZ)V
    .locals 2
    .param p1, "visualPosition"    # I
    .param p2, "byUser"    # Z

    .prologue
    .line 259
    invoke-super {p0, p1, p2}, Lcom/google/android/play/widget/UserAwareViewPager$UserAwareOnPageChangeListener;->onPageSelected(IZ)V

    .line 260
    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardHostFragment$4;->this$0:Lcom/google/android/play/onboard/OnboardHostFragment;

    iget-object v1, v1, Lcom/google/android/play/onboard/OnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    invoke-static {v1, p1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v0

    .line 261
    .local v0, "logicalPosition":I
    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardHostFragment$4;->this$0:Lcom/google/android/play/onboard/OnboardHostFragment;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/play/onboard/OnboardHostFragment;->onPageSelected(IZ)V

    .line 262
    return-void
.end method
