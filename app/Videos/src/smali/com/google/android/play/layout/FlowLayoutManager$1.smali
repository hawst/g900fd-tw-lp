.class Lcom/google/android/play/layout/FlowLayoutManager$1;
.super Landroid/support/v7/widget/LinearSmoothScroller;
.source "FlowLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/layout/FlowLayoutManager;->smoothScrollToPosition(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/layout/FlowLayoutManager;


# direct methods
.method constructor <init>(Lcom/google/android/play/layout/FlowLayoutManager;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 3456
    iput-object p1, p0, Lcom/google/android/play/layout/FlowLayoutManager$1;->this$0:Lcom/google/android/play/layout/FlowLayoutManager;

    invoke-direct {p0, p2}, Landroid/support/v7/widget/LinearSmoothScroller;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public computeScrollVectorForPosition(I)Landroid/graphics/PointF;
    .locals 5
    .param p1, "targetPosition"    # I

    .prologue
    .line 3459
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager$1;->getChildCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 3460
    const/4 v2, 0x0

    .line 3464
    :goto_0
    return-object v2

    .line 3462
    :cond_0
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager$1;->this$0:Lcom/google/android/play/layout/FlowLayoutManager;

    iget-object v3, p0, Lcom/google/android/play/layout/FlowLayoutManager$1;->this$0:Lcom/google/android/play/layout/FlowLayoutManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v1

    .line 3463
    .local v1, "firstChildPos":I
    if-ge p1, v1, :cond_1

    const/4 v0, -0x1

    .line 3464
    .local v0, "direction":I
    :goto_1
    new-instance v2, Landroid/graphics/PointF;

    const/4 v3, 0x0

    int-to-float v4, v0

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_0

    .line 3463
    .end local v0    # "direction":I
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method
