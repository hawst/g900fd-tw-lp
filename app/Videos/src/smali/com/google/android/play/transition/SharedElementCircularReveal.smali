.class public Lcom/google/android/play/transition/SharedElementCircularReveal;
.super Landroid/transition/ChangeBounds;
.source "SharedElementCircularReveal.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CONTRACT:I = 0x2

.field public static final EXPAND:I = 0x1


# instance fields
.field private mMode:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/transition/ChangeBounds;-><init>()V

    .line 39
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/SharedElementCircularReveal;->setMode(I)V

    .line 40
    return-void
.end method

.method private isExpanding()Z
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/google/android/play/transition/SharedElementCircularReveal;->mMode:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private revealAnimator(Landroid/view/View;IIZ)Landroid/animation/Animator;
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "isExpanding"    # Z

    .prologue
    const/high16 v5, 0x40800000    # 4.0f

    const/4 v2, 0x0

    .line 87
    mul-int v3, p2, p2

    int-to-float v3, v3

    div-float/2addr v3, v5

    mul-int v4, p3, p3

    int-to-float v4, v4

    div-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v1, v4

    .line 88
    .local v1, "fullRadius":F
    if-eqz p4, :cond_0

    move v0, v1

    .line 89
    .local v0, "endRadius":F
    :goto_0
    if-eqz p4, :cond_1

    .line 90
    .local v2, "startRadius":F
    :goto_1
    div-int/lit8 v3, p2, 0x2

    div-int/lit8 v4, p3, 0x2

    invoke-static {p1, v3, v4, v2, v0}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v3

    return-object v3

    .end local v0    # "endRadius":F
    .end local v2    # "startRadius":F
    :cond_0
    move v0, v2

    .line 88
    goto :goto_0

    .restart local v0    # "endRadius":F
    :cond_1
    move v2, v1

    .line 89
    goto :goto_1
.end method


# virtual methods
.method public createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 8
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 46
    iget-object v4, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 49
    .local v4, "view":Landroid/view/View;
    invoke-super {p0, p1, p2, p3}, Landroid/transition/ChangeBounds;->createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;

    move-result-object v1

    .line 50
    .local v1, "boundsAnimator":Landroid/animation/Animator;
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    invoke-direct {p0}, Lcom/google/android/play/transition/SharedElementCircularReveal;->isExpanding()Z

    move-result v7

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/google/android/play/transition/SharedElementCircularReveal;->revealAnimator(Landroid/view/View;IIZ)Landroid/animation/Animator;

    move-result-object v0

    .line 57
    .local v0, "animator":Landroid/animation/Animator;
    new-instance v2, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;

    invoke-direct {v2, v0}, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;-><init>(Landroid/animation/Animator;)V

    .line 59
    .local v2, "circularRevealAnimator":Landroid/animation/Animator;
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 60
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 61
    .local v3, "set":Landroid/animation/AnimatorSet;
    const/4 v5, 0x2

    new-array v5, v5, [Landroid/animation/Animator;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    const/4 v6, 0x1

    aput-object v2, v5, v6

    invoke-virtual {v3, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 64
    .end local v3    # "set":Landroid/animation/AnimatorSet;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public setMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 80
    and-int/lit8 v0, p1, -0x4

    if-eqz v0, :cond_0

    .line 81
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only EXPAND and CONTRACT flags are allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_0
    iput p1, p0, Lcom/google/android/play/transition/SharedElementCircularReveal;->mMode:I

    .line 84
    return-void
.end method
