.class public Lcom/google/android/play/layout/PlayCardThumbnail;
.super Landroid/view/ViewGroup;
.source "PlayCardThumbnail.java"


# instance fields
.field private final mAppThumbnailPadding:I

.field private mCoverPadding:I

.field private final mPersonThumbnailPadding:I

.field private mThumbnail:Landroid/widget/ImageView;

.field private final mThumbnailId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayCardThumbnail;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 48
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    sget-object v1, Lcom/google/android/play/R$styleable;->PlayCardThumbnail:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 52
    .local v0, "viewAttrs":Landroid/content/res/TypedArray;
    sget v1, Lcom/google/android/play/R$styleable;->PlayCardThumbnail_thumbnail_id:I

    sget v2, Lcom/google/android/play/R$id;->li_thumbnail:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mThumbnailId:I

    .line 54
    sget v1, Lcom/google/android/play/R$styleable;->PlayCardThumbnail_app_thumbnail_padding:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mAppThumbnailPadding:I

    .line 56
    sget v1, Lcom/google/android/play/R$styleable;->PlayCardThumbnail_person_thumbnail_padding:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mPersonThumbnailPadding:I

    .line 58
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    return-void
.end method


# virtual methods
.method public getBaseline()I
    .locals 2

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mCoverPadding:I

    iget-object v1, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getCoverPadding()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mCoverPadding:I

    return v0
.end method

.method public getImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mThumbnail:Landroid/widget/ImageView;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 65
    iget v0, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mThumbnailId:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayCardThumbnail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mThumbnail:Landroid/widget/ImageView;

    .line 66
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mThumbnail:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mCoverPadding:I

    iget v2, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mCoverPadding:I

    iget v3, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mCoverPadding:I

    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mCoverPadding:I

    iget-object v5, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    .line 118
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x0

    .line 102
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 103
    .local v3, "width":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 105
    .local v0, "height":I
    iget v4, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mCoverPadding:I

    mul-int/lit8 v4, v4, 0x2

    sub-int v4, v3, v4

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 106
    .local v2, "thumbnailWidth":I
    iget v4, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mCoverPadding:I

    mul-int/lit8 v4, v4, 0x2

    sub-int v4, v0, v4

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 107
    .local v1, "thumbnailHeight":I
    iget-object v4, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mThumbnail:Landroid/widget/ImageView;

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/ImageView;->measure(II)V

    .line 110
    invoke-virtual {p0, v3, v0}, Lcom/google/android/play/layout/PlayCardThumbnail;->setMeasuredDimension(II)V

    .line 111
    return-void
.end method

.method public updateCoverPadding(I)V
    .locals 2
    .param p1, "corpus"    # I

    .prologue
    .line 79
    iget v0, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mCoverPadding:I

    .line 80
    .local v0, "currCoverPadding":I
    const/4 v1, 0x3

    if-ne p1, v1, :cond_1

    .line 82
    iget v1, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mAppThumbnailPadding:I

    iput v1, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mCoverPadding:I

    .line 90
    :goto_0
    iget v1, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mCoverPadding:I

    if-eq v0, v1, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayCardThumbnail;->requestLayout()V

    .line 94
    :cond_0
    return-void

    .line 83
    :cond_1
    const/16 v1, 0x9

    if-ne p1, v1, :cond_2

    .line 85
    iget v1, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mPersonThumbnailPadding:I

    iput v1, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mCoverPadding:I

    goto :goto_0

    .line 88
    :cond_2
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/play/layout/PlayCardThumbnail;->mCoverPadding:I

    goto :goto_0
.end method
