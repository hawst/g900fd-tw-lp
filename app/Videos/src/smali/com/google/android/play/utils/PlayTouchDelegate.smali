.class public Lcom/google/android/play/utils/PlayTouchDelegate;
.super Landroid/view/TouchDelegate;
.source "PlayTouchDelegate.java"


# instance fields
.field private mPlayBounds:Landroid/graphics/Rect;

.field private mPlayDelegateTargeted:Z

.field private mPlayDelegateView:Landroid/view/View;

.field private mPlaySlop:I

.field private mPlaySlopBounds:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/graphics/Rect;Landroid/view/View;)V
    .locals 3
    .param p1, "bounds"    # Landroid/graphics/Rect;
    .param p2, "delegateView"    # Landroid/view/View;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    .line 54
    iput-object p1, p0, Lcom/google/android/play/utils/PlayTouchDelegate;->mPlayBounds:Landroid/graphics/Rect;

    .line 56
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/utils/PlayTouchDelegate;->mPlaySlop:I

    .line 57
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lcom/google/android/play/utils/PlayTouchDelegate;->mPlaySlopBounds:Landroid/graphics/Rect;

    .line 58
    iget-object v0, p0, Lcom/google/android/play/utils/PlayTouchDelegate;->mPlaySlopBounds:Landroid/graphics/Rect;

    iget v1, p0, Lcom/google/android/play/utils/PlayTouchDelegate;->mPlaySlop:I

    neg-int v1, v1

    iget v2, p0, Lcom/google/android/play/utils/PlayTouchDelegate;->mPlaySlop:I

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 59
    iput-object p2, p0, Lcom/google/android/play/utils/PlayTouchDelegate;->mPlayDelegateView:Landroid/view/View;

    .line 60
    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 71
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    float-to-int v7, v9

    .line 72
    .local v7, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v8, v9

    .line 73
    .local v8, "y":I
    const/4 v4, 0x0

    .line 74
    .local v4, "sendToDelegate":Z
    const/4 v3, 0x1

    .line 75
    .local v3, "hit":Z
    const/4 v2, 0x0

    .line 77
    .local v2, "handled":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    .line 105
    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    .line 106
    iget-object v1, p0, Lcom/google/android/play/utils/PlayTouchDelegate;->mPlayDelegateView:Landroid/view/View;

    .line 108
    .local v1, "delegateView":Landroid/view/View;
    if-eqz v3, :cond_3

    .line 110
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    int-to-float v9, v9

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    invoke-virtual {p1, v9, v10}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 117
    :goto_1
    invoke-virtual {v1, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 119
    .end local v1    # "delegateView":Landroid/view/View;
    :cond_1
    return v2

    .line 79
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/play/utils/PlayTouchDelegate;->mPlayBounds:Landroid/graphics/Rect;

    .line 81
    .local v0, "bounds":Landroid/graphics/Rect;
    invoke-virtual {v0, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 82
    iput-boolean v11, p0, Lcom/google/android/play/utils/PlayTouchDelegate;->mPlayDelegateTargeted:Z

    .line 83
    const/4 v4, 0x1

    goto :goto_0

    .line 88
    .end local v0    # "bounds":Landroid/graphics/Rect;
    :pswitch_1
    iget-boolean v4, p0, Lcom/google/android/play/utils/PlayTouchDelegate;->mPlayDelegateTargeted:Z

    .line 89
    if-eqz v4, :cond_2

    .line 90
    iget-object v6, p0, Lcom/google/android/play/utils/PlayTouchDelegate;->mPlaySlopBounds:Landroid/graphics/Rect;

    .line 91
    .local v6, "slopBounds":Landroid/graphics/Rect;
    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->contains(II)Z

    move-result v9

    if-nez v9, :cond_2

    .line 92
    const/4 v3, 0x0

    .line 96
    .end local v6    # "slopBounds":Landroid/graphics/Rect;
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    if-ne v9, v11, :cond_0

    .line 97
    iput-boolean v10, p0, Lcom/google/android/play/utils/PlayTouchDelegate;->mPlayDelegateTargeted:Z

    goto :goto_0

    .line 101
    :pswitch_2
    iget-boolean v4, p0, Lcom/google/android/play/utils/PlayTouchDelegate;->mPlayDelegateTargeted:Z

    .line 102
    iput-boolean v10, p0, Lcom/google/android/play/utils/PlayTouchDelegate;->mPlayDelegateTargeted:Z

    goto :goto_0

    .line 114
    .restart local v1    # "delegateView":Landroid/view/View;
    :cond_3
    iget v5, p0, Lcom/google/android/play/utils/PlayTouchDelegate;->mPlaySlop:I

    .line 115
    .local v5, "slop":I
    mul-int/lit8 v9, v5, 0x2

    neg-int v9, v9

    int-to-float v9, v9

    mul-int/lit8 v10, v5, 0x2

    neg-int v10, v10

    int-to-float v10, v10

    invoke-virtual {p1, v9, v10}, Landroid/view/MotionEvent;->setLocation(FF)V

    goto :goto_1

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
