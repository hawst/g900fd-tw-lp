.class public abstract Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;
.super Ljava/lang/Object;
.source "PlayHeaderListLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/headerlist/PlayHeaderListLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PullToRefreshProvider"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 950
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getOffsetMode()I
    .locals 1

    .prologue
    .line 1036
    const/4 v0, 0x0

    return v0
.end method

.method public getPositionMode()I
    .locals 1

    .prologue
    .line 1047
    const/4 v0, 0x0

    return v0
.end method

.method public isPageRefreshing(I)Z
    .locals 1
    .param p1, "pagerAdapterIndex"    # I

    .prologue
    .line 1022
    const/4 v0, 0x0

    return v0
.end method

.method public onPullToRefreshDisplayedPageChanged(Landroid/support/v4/widget/SwipeRefreshLayout;I)V
    .locals 0
    .param p1, "swipeRefreshLayout"    # Landroid/support/v4/widget/SwipeRefreshLayout;
    .param p2, "pagerAdapterIndex"    # I

    .prologue
    .line 997
    return-void
.end method

.method public abstract onPulledToRefresh()V
.end method

.method public supportsPullToRefresh(I)Z
    .locals 1
    .param p1, "pagerAdapterIndex"    # I

    .prologue
    .line 1008
    const/4 v0, 0x1

    return v0
.end method
