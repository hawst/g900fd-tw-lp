.class public Lcom/google/android/play/headerlist/PlayHeaderListLayout;
.super Landroid/widget/FrameLayout;
.source "PlayHeaderListLayout.java"

# interfaces
.implements Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;,
        Lcom/google/android/play/headerlist/PlayHeaderListLayout$ActionBarTitleAlphaAnimation;,
        Lcom/google/android/play/headerlist/PlayHeaderListLayout$FloatingFractionAnimation;,
        Lcom/google/android/play/headerlist/PlayHeaderListLayout$FloatAnimation;,
        Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;,
        Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;,
        Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;,
        Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;,
        Lcom/google/android/play/headerlist/PlayHeaderListLayout$LayoutListener;
    }
.end annotation


# static fields
.field public static final CONTENT_PROTECTION_MODE_ENABLED:I = 0x1

.field public static final CONTENT_PROTECTION_MODE_NONE:I = 0x0

.field public static final HEADER_MODE_EVERYTHING_SCROLLS_OFF:I = 0x0

.field public static final HEADER_MODE_STICKY_TABS:I = 0x1

.field public static final HEADER_MODE_STICKY_TABS_AND_TOOLBAR:I = 0x3

.field public static final HEADER_MODE_STICKY_TOOLBAR:I = 0x2

.field public static final HEADER_SHADOW_MODE_ALWAYS_OFF:I = 0x2

.field public static final HEADER_SHADOW_MODE_ALWAYS_ON:I = 0x3

.field public static final HEADER_SHADOW_MODE_ALWAYS_WHEN_FLOATING:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final HEADER_SHADOW_MODE_WHEN_FLOATING:I = 0x1

.field public static final HEADER_SHADOW_MODE_WHEN_FLOATING_AND_BACKGROUND_NOT_VISIBLE:I = 0x0

.field public static final HERO_ANIMATION_MODE_FADE:I = 0x1

.field public static final HERO_ANIMATION_MODE_NONE:I = 0x3

.field public static final HERO_ANIMATION_MODE_SCALE:I = 0x0

.field public static final HERO_ANIMATION_MODE_SCROLL_FADE:I = 0x2

.field public static final POSITION_NOT_AVAILABLE:I = -0x1

.field private static final SUPPORT_ELEVATION:Z

.field public static final TAB_A11Y_MODE_HIGH_CONTRAST_ALWAYS:I = 0x3

.field public static final TAB_A11Y_MODE_HIGH_CONTRAST_NEVER:I = 0x0

.field public static final TAB_A11Y_MODE_HIGH_CONTRAST_WHEN_FLOATING:I = 0x1

.field public static final TAB_A11Y_MODE_HIGH_CONTRAST_WHEN_NOT_FLOATING:I = 0x2

.field public static final TAB_MODE_MANY:I = 0x0

.field public static final TAB_MODE_NONE:I = 0x2

.field public static final TAB_MODE_SINGLE:I = 0x1

.field public static final TAB_PADDING_MODE_ALWAYS_CENTERED:I = 0x2

.field public static final TAB_PADDING_MODE_ALWAYS_REDUCED:I = 0x1

.field public static final TAB_PADDING_MODE_REDUCED_WHEN_FLOATING:I = 0x0

.field public static final TOOLBAR_TITLE_MODE_HIDE_AT_TOP:I = 0x0

.field public static final TOOLBAR_TITLE_MODE_NEVER_HIDE:I = 0x1

.field private static final USE_ANIMATIONS:Z

.field private static sActionbarAttachedCount:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAbsoluteY:I

.field private mActionBarCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

.field private mActionBarTitleAlpha:F

.field private mActionBarView:Landroid/view/View;

.field private mAltPlayBackground:Landroid/view/View;

.field private mAltPlayBackgroundCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

.field private mAlwaysUseFloatingBackground:Z

.field private mAnimatorMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/animation/ObjectAnimator;",
            ">;"
        }
    .end annotation
.end field

.field mAppContentViewOnScrollListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

.field mAppListViewOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

.field mAppRecyclerViewOnScrollListener:Landroid/support/v7/widget/RecyclerView$OnScrollListener;

.field private mAttached:Z

.field private mAutoHideToolbarTitle:Z

.field private mBackgroundContainer:Landroid/widget/FrameLayout;

.field private mBackgroundContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

.field private final mBackgroundFadeInOffsetThresholdPx:F

.field private mBackgroundParallaxRatio:F

.field private mBannerFraction:F

.field private mBannerText:Ljava/lang/CharSequence;

.field private mBannerTextView:Landroid/widget/TextView;

.field private mBannerTextViewCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

.field private mBannerVisibleGB:Z

.field private mContentContainer:Landroid/view/View;

.field private mContentContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

.field private mContentProtectionEnabled:Z

.field private mControlsAreFloating:Z

.field private mControlsContainer:Landroid/view/ViewGroup;

.field private mControlsContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

.field private mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private mFloatingControlsBackground:Landroid/graphics/drawable/Drawable;

.field private mFloatingFraction:F

.field private final mHandler:Landroid/os/Handler;

.field private mHasPullToRefresh:Z

.field private mHasViewPager:Z

.field private mHeaderBottomMargin:I

.field private mHeaderHeight:I

.field private mHeaderMode:I

.field private mHeaderShadow:Landroid/view/View;

.field private mHeaderShadowCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

.field private mHeaderShadowMode:I

.field private mHeaderShadowVisible:Z

.field private mHeroAnimationMode:I

.field private mHeroContainer:Landroid/widget/FrameLayout;

.field private mHeroContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

.field private mHeroVisible:Z

.field private mLastScrollWasDown:Z

.field mLayoutListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$LayoutListener;

.field private mListViewId:I

.field private mLockHeader:Z

.field private final mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private mPendingListSync:I

.field private mPendingSavedState:Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

.field private mPullToRefreshAdapterPage:I

.field mPullToRefreshProvider:Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;

.field private mScrollProxyView:Lcom/google/android/play/widget/ScrollProxyView;

.field private final mSnapControlsDownIfNeededRunnable:Ljava/lang/Runnable;

.field private final mSnapControlsUpIfNeededRunnable:Ljava/lang/Runnable;

.field private mSpacerId:I

.field private mSuppressIdleOnScroll:Z

.field private mSwipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

.field private mSwipeRefreshLayoutCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

.field private mTabA11yMode:I

.field private mTabBar:Landroid/view/View;

.field private mTabBarTitleView:Landroid/widget/TextView;

.field private mTabMode:I

.field private mTabPaddingMode:I

.field private mTabStrip:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

.field private mTemporaryBannerGoneRunnable:Ljava/lang/Runnable;

.field private final mTemporaryBannerTimeoutRunnable:Ljava/lang/Runnable;

.field private mToolbar:Landroid/support/v7/widget/Toolbar;

.field private final mTrackedListContentViewScrollListener:Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;

.field private final mTrackedListRecyclerViewScrollListener:Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;

.field private final mTrackedListScrollListener:Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;

.field private mTrackedListView:Landroid/view/ViewGroup;

.field private mUpdateContentPositionOnLayout:Z

.field private mUseBuiltInToolbar:Z

.field private mViewPager:Landroid/support/v4/view/ViewPager;

.field private mViewPagerId:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 102
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xa

    if-le v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->USE_ANIMATIONS:Z

    .line 103
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->SUPPORT_ELEVATION:Z

    .line 427
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->sActionbarAttachedCount:Ljava/util/Map;

    return-void

    :cond_0
    move v0, v2

    .line 102
    goto :goto_0

    :cond_1
    move v1, v2

    .line 103
    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1122
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1123
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 1126
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1127
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    const/4 v1, 0x1

    .line 1130
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 319
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHandler:Landroid/os/Handler;

    .line 325
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$1;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 515
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroAnimationMode:I

    .line 571
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshAdapterPage:I

    .line 580
    iput-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroVisible:Z

    .line 590
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mActionBarTitleAlpha:F

    .line 592
    iput-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mLastScrollWasDown:Z

    .line 616
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListScrollListener:Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;

    .line 623
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListRecyclerViewScrollListener:Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;

    .line 631
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListContentViewScrollListener:Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;

    .line 641
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAnimatorMap:Ljava/util/Map;

    .line 644
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$2;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$2;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSnapControlsUpIfNeededRunnable:Ljava/lang/Runnable;

    .line 652
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$3;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$3;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSnapControlsDownIfNeededRunnable:Ljava/lang/Runnable;

    .line 660
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$4;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$4;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTemporaryBannerTimeoutRunnable:Ljava/lang/Runnable;

    .line 1131
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41a00000    # 20.0f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundFadeInOffsetThresholdPx:F

    .line 1134
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/view/ViewPager$OnPageChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 99
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderMode:I

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)F
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 99
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingFraction:F

    return v0
.end method

.method static synthetic access$1300()Z
    .locals 1

    .prologue
    .line 99
    sget-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->SUPPORT_ELEVATION:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/play/headerlist/PlayHeaderListLayout;ZZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 99
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->snapControlsIfNeeded(ZZZ)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateActiveListView()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/widget/SwipeRefreshLayout;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 99
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshAdapterPage:I

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/play/headerlist/PlayHeaderListLayout;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .param p1, "x1"    # I

    .prologue
    .line 99
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshAdapterPage:I

    return p1
.end method

.method static synthetic access$600()Z
    .locals 1

    .prologue
    .line 99
    sget-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->USE_ANIMATIONS:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayoutCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/view/ViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTemporaryBannerGoneRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/play/headerlist/PlayHeaderListLayout;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTemporaryBannerGoneRunnable:Ljava/lang/Runnable;

    return-object p1
.end method

.method private attachIfNeeded()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1840
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAttached:Z

    if-eqz v2, :cond_0

    .line 1864
    :goto_0
    return-void

    .line 1843
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAttached:Z

    .line 1844
    sget-object v2, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->sActionbarAttachedCount:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1845
    .local v0, "currentAttachCount":Ljava/lang/Integer;
    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1846
    sget-object v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->sActionbarAttachedCount:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1851
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setupViewPagerIfNeeded()V

    .line 1852
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateHeaderShadow()V

    .line 1854
    iget-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAutoHideToolbarTitle:Z

    if-eqz v1, :cond_2

    .line 1855
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setActionBarTitleAlpha(F)V

    .line 1863
    :goto_2
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateActiveListView()V

    goto :goto_0

    .line 1845
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1857
    :cond_2
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setActionBarTitleAlpha(F)V

    goto :goto_2
.end method

.method private detachIfNeeded()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 1874
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAttached:Z

    if-nez v2, :cond_0

    .line 1897
    :goto_0
    return-void

    .line 1877
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAttached:Z

    .line 1878
    sget-object v2, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->sActionbarAttachedCount:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1880
    .local v0, "currentAttachCount":Ljava/lang/Integer;
    if-nez v0, :cond_2

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1881
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_3

    .line 1882
    sget-object v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->sActionbarAttachedCount:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1890
    :goto_2
    invoke-direct {p0, v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setActiveListView(Landroid/view/ViewGroup;)Z

    .line 1891
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1892
    iget-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mUseBuiltInToolbar:Z

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_1

    .line 1894
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mActionBarCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setTranslationY(F)V

    .line 1896
    :cond_1
    iput-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTemporaryBannerGoneRunnable:Ljava/lang/Runnable;

    goto :goto_0

    .line 1880
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 1884
    :cond_3
    sget-object v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->sActionbarAttachedCount:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method

.method private static getActionBarHeight(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2129
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$dimen;->abc_action_bar_default_height_material:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method private static getAdapterCount(Landroid/view/ViewGroup;)I
    .locals 5
    .param p0, "view"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v2, 0x0

    .line 3248
    instance-of v3, p0, Landroid/widget/ListView;

    if-eqz v3, :cond_2

    .line 3249
    check-cast p0, Landroid/widget/ListView;

    .end local p0    # "view":Landroid/view/ViewGroup;
    invoke-virtual {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 3250
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-nez v0, :cond_1

    .line 3256
    .local v0, "adapter":Landroid/widget/Adapter;
    :cond_0
    :goto_0
    return v2

    .line 3250
    .local v0, "adapter":Landroid/widget/ListAdapter;
    :cond_1
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    goto :goto_0

    .line 3251
    .end local v0    # "adapter":Landroid/widget/ListAdapter;
    .restart local p0    # "view":Landroid/view/ViewGroup;
    :cond_2
    instance-of v3, p0, Landroid/support/v7/widget/RecyclerView;

    if-eqz v3, :cond_3

    .line 3252
    check-cast p0, Landroid/support/v7/widget/RecyclerView;

    .end local p0    # "view":Landroid/view/ViewGroup;
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v1

    .line 3253
    .local v1, "adapter":Landroid/support/v7/widget/RecyclerView$Adapter;, "Landroid/support/v7/widget/RecyclerView$Adapter<*>;"
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v2

    goto :goto_0

    .line 3254
    .end local v1    # "adapter":Landroid/support/v7/widget/RecyclerView$Adapter;, "Landroid/support/v7/widget/RecyclerView$Adapter<*>;"
    .restart local p0    # "view":Landroid/view/ViewGroup;
    :cond_3
    instance-of v3, p0, Lcom/google/android/play/headerlist/PlayScrollableContentView;

    if-eqz v3, :cond_4

    .line 3255
    check-cast p0, Lcom/google/android/play/headerlist/PlayScrollableContentView;

    .end local p0    # "view":Landroid/view/ViewGroup;
    invoke-interface {p0}, Lcom/google/android/play/headerlist/PlayScrollableContentView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 3256
    .local v0, "adapter":Landroid/widget/Adapter;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v2

    goto :goto_0

    .line 3258
    .end local v0    # "adapter":Landroid/widget/Adapter;
    .restart local p0    # "view":Landroid/view/ViewGroup;
    :cond_4
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected listview type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private getContentPosition()F
    .locals 3

    .prologue
    .line 2068
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAbsoluteY:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderBottomMargin:I

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderBottomMargin:I

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderHeight:I

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAbsoluteY:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    goto :goto_0
.end method

.method private getDesiredContentTop()I
    .locals 4

    .prologue
    .line 3203
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderMode:I

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderMode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 3206
    .local v0, "alignToFloatingHeight":Z
    :goto_0
    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getFullFloatingHeaderHeight()F

    move-result v2

    :goto_1
    float-to-int v1, v2

    .line 3208
    .local v1, "desiredContentTop":I
    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderBottomMargin:I

    add-int/2addr v1, v2

    .line 3209
    return v1

    .line 3203
    .end local v0    # "alignToFloatingHeight":Z
    .end local v1    # "desiredContentTop":I
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 3206
    .restart local v0    # "alignToFloatingHeight":Z
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getVisibleHeaderHeight()F

    move-result v2

    goto :goto_1
.end method

.method private getFloatAnimator(Ljava/lang/String;FF)Landroid/animation/ObjectAnimator;
    .locals 3
    .param p1, "propertyName"    # Ljava/lang/String;
    .param p2, "startValue"    # F
    .param p3, "endValue"    # F

    .prologue
    .line 3362
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAnimatorMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ObjectAnimator;

    .line 3363
    .local v0, "animator":Landroid/animation/ObjectAnimator;
    if-eqz v0, :cond_0

    .line 3364
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 3366
    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p2, v1, v2

    const/4 v2, 0x1

    aput p3, v1, v2

    invoke-static {p0, p1, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 3367
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAnimatorMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3368
    return-object v0
.end method

.method private getFloatingHeaderElevation()F
    .locals 2

    .prologue
    .line 2144
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$dimen;->play_header_list_floating_elevation:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method private getFullFloatingHeaderHeight()F
    .locals 2

    .prologue
    .line 2076
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getVisibleTabBarHeight()F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method private getListView(I)Landroid/view/ViewGroup;
    .locals 3
    .param p1, "relativePosition"    # I

    .prologue
    .line 2040
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 2041
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mContentContainer:Landroid/view/View;

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mListViewId:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->validateListView(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v1

    .line 2047
    :goto_0
    return-object v1

    .line 2043
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getViewPagerView(I)Landroid/view/View;

    move-result-object v0

    .line 2044
    .local v0, "currentPagerView":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 2045
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mListViewId:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->validateListView(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v1

    goto :goto_0

    .line 2047
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getMinimumHeaderHeight(Landroid/content/Context;I)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "tabMode"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1102
    invoke-static {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarHeight(Landroid/content/Context;)I

    move-result v0

    invoke-static {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getTabBarHeight(Landroid/content/Context;I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static getMinimumHeaderHeight(Landroid/content/Context;II)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "tabMode"    # I
    .param p2, "headerBottomMargin"    # I

    .prologue
    .line 1118
    invoke-static {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;I)I

    move-result v0

    add-int/2addr v0, p2

    return v0
.end method

.method private getNonScrollingFloatingHeaderHeight()F
    .locals 3

    .prologue
    .line 2081
    iget-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mLockHeader:Z

    if-eqz v1, :cond_0

    .line 2082
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getVisibleTabBarHeight()F

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarHeight()I

    move-result v2

    int-to-float v2, v2

    add-float v0, v1, v2

    .line 2098
    :goto_0
    return v0

    .line 2084
    :cond_0
    const/4 v0, 0x0

    .line 2085
    .local v0, "height":F
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderMode:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2087
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getVisibleTabBarHeight()F

    move-result v1

    add-float/2addr v0, v1

    .line 2088
    goto :goto_0

    .line 2091
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarHeight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    .line 2092
    goto :goto_0

    .line 2095
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getVisibleTabBarHeight()F

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarHeight()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    add-float/2addr v0, v1

    goto :goto_0

    .line 2085
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getScrollingFloatingHeaderHeight()F
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2103
    iget-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mLockHeader:Z

    if-eqz v1, :cond_0

    .line 2114
    :goto_0
    :pswitch_0
    return v0

    .line 2106
    :cond_0
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderMode:I

    packed-switch v1, :pswitch_data_0

    .line 2116
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 2108
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getVisibleTabBarHeight()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarHeight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    goto :goto_0

    .line 2110
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarHeight()I

    move-result v0

    int-to-float v0, v0

    goto :goto_0

    .line 2112
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getVisibleTabBarHeight()F

    move-result v0

    goto :goto_0

    .line 2106
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private static getTabBarHeight(Landroid/content/Context;I)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "tabMode"    # I

    .prologue
    .line 2151
    packed-switch p1, :pswitch_data_0

    .line 2159
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 2154
    :pswitch_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$dimen;->play_header_list_tab_strip_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2157
    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2151
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getToolbar()Landroid/support/v7/widget/Toolbar;
    .locals 3

    .prologue
    .line 3031
    iget-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mUseBuiltInToolbar:Z

    if-eqz v1, :cond_0

    .line 3032
    sget v1, Lcom/google/android/play/R$id;->play_header_toolbar:I

    invoke-virtual {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 3033
    .local v0, "toolbar":Landroid/support/v7/widget/Toolbar;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setVisibility(I)V

    .line 3034
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v1, v0}, Landroid/support/v7/app/ActionBarActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 3038
    :goto_0
    return-object v0

    .line 3036
    .end local v0    # "toolbar":Landroid/support/v7/widget/Toolbar;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    sget v2, Lcom/google/android/play/R$id;->action_bar:I

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .restart local v0    # "toolbar":Landroid/support/v7/widget/Toolbar;
    goto :goto_0
.end method

.method private getViewPagerView(I)Landroid/view/View;
    .locals 6
    .param p1, "relativePosition"    # I

    .prologue
    const/4 v4, 0x0

    .line 2011
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->relativeToAbsolute(I)I

    move-result v0

    .line 2012
    .local v0, "absolutePosition":I
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v5}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v5

    if-eqz v5, :cond_0

    if-ltz v0, :cond_0

    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v5}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v5

    if-lt v0, v5, :cond_1

    :cond_0
    move-object v3, v4

    .line 2025
    :goto_0
    return-object v3

    .line 2016
    :cond_1
    const/4 v1, 0x0

    .local v1, "child":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v5}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v5

    if-ge v1, v5, :cond_4

    .line 2017
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v5, v1}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2018
    .local v3, "view":Landroid/view/View;
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-static {v5, v3}, Landroid/support/v4/view/ViewPagerHelper;->getChildViewPosition(Landroid/support/v4/view/ViewPager;Landroid/view/View;)Ljava/lang/Integer;

    move-result-object v2

    .line 2019
    .local v2, "position":Ljava/lang/Integer;
    if-nez v2, :cond_3

    .line 2016
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2021
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v0, :cond_2

    goto :goto_0

    .end local v2    # "position":Ljava/lang/Integer;
    .end local v3    # "view":Landroid/view/View;
    :cond_4
    move-object v3, v4

    .line 2025
    goto :goto_0
.end method

.method private getVisibleTabBarHeight()F
    .locals 2

    .prologue
    .line 2121
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabMode:I

    invoke-static {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getTabBarHeight(Landroid/content/Context;I)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method private hideBanner(Z)V
    .locals 3
    .param p1, "animate"    # Z

    .prologue
    const/4 v2, 0x0

    .line 3281
    sget-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->USE_ANIMATIONS:Z

    if-eqz v0, :cond_1

    .line 3282
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setBannerFraction(FZ)V

    .line 3283
    invoke-direct {p0, v2, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setContentContainerMargins(II)V

    .line 3291
    :cond_0
    :goto_0
    return-void

    .line 3285
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerVisibleGB:Z

    if-eqz v0, :cond_0

    .line 3286
    iput-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerVisibleGB:Z

    .line 3287
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3288
    invoke-direct {p0, v2, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setContentContainerMargins(II)V

    goto :goto_0
.end method

.method private isBackgroundVisible()Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 2946
    sget-boolean v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->USE_ANIMATIONS:Z

    if-nez v5, :cond_1

    move v3, v4

    .line 2966
    :cond_0
    :goto_0
    return v3

    .line 2949
    :cond_1
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v5

    if-eqz v5, :cond_2

    move v3, v4

    .line 2950
    goto :goto_0

    .line 2952
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMeasuredHeight()I

    move-result v5

    if-eqz v5, :cond_0

    .line 2958
    iget-boolean v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-nez v5, :cond_3

    iget-boolean v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAlwaysUseFloatingBackground:Z

    if-eqz v5, :cond_0

    .line 2959
    :cond_3
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v0

    .line 2960
    .local v0, "backgroundHeight":I
    int-to-float v5, v0

    iget-object v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    invoke-virtual {v6}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->getTranslationY()F

    move-result v6

    add-float/2addr v5, v6

    invoke-static {v7, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 2962
    .local v1, "onScreenBackgroundHeight":F
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getVisibleHeaderHeight()F

    move-result v5

    sub-float v5, v1, v5

    invoke-static {v7, v5}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 2964
    .local v2, "visibleBackgroundHeight":F
    cmpl-float v5, v2, v7

    if-gtz v5, :cond_0

    move v3, v4

    goto :goto_0
.end method

.method private isPullToRefreshEnabled()Z
    .locals 1

    .prologue
    .line 1385
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mScrollProxyView:Lcom/google/android/play/widget/ScrollProxyView;

    invoke-virtual {v0}, Lcom/google/android/play/widget/ScrollProxyView;->getScrollY()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isTrackedListViewReady()Z
    .locals 1

    .prologue
    .line 2567
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->isListViewReady(Landroid/view/ViewGroup;)Z

    move-result v0

    return v0
.end method

.method private layout()V
    .locals 23

    .prologue
    .line 2321
    sget-boolean v20, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->USE_ANIMATIONS:Z

    if-nez v20, :cond_1

    .line 2457
    :cond_0
    :goto_0
    return-void

    .line 2327
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerFraction:F

    move/from16 v20, v0

    const/high16 v21, 0x3f800000    # 1.0f

    sub-float v20, v20, v21

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getBannerHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    mul-float v6, v20, v21

    .line 2328
    .local v6, "bannerTop":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerTextViewCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setTranslationY(F)V

    .line 2330
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getBannerHeight()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    add-float v5, v20, v6

    .line 2334
    .local v5, "bannerOffset":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mContentContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setTranslationY(F)V

    .line 2338
    move v9, v5

    .line 2339
    .local v9, "controlTop":F
    invoke-direct/range {p0 .. p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContentPosition()F

    move-result v7

    .line 2340
    .local v7, "contentPosition":F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    move/from16 v20, v0

    if-eqz v20, :cond_7

    .line 2342
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderHeight:I

    move/from16 v20, v0

    move/from16 v0, v20

    neg-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getNonScrollingFloatingHeaderHeight()F

    move-result v21

    add-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingFraction:F

    move/from16 v21, v0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getScrollingFloatingHeaderHeight()F

    move-result v22

    mul-float v21, v21, v22

    add-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderBottomMargin:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    add-float v20, v20, v21

    add-float v9, v9, v20

    .line 2351
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setTranslationY(F)V

    .line 2354
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderShadowCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setTranslationY(F)V

    .line 2359
    move v3, v5

    .line 2360
    .local v3, "actionBarTop":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderMode:I

    move/from16 v20, v0

    if-eqz v20, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderMode:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_3

    .line 2362
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    move/from16 v20, v0

    if-eqz v20, :cond_8

    .line 2364
    const/high16 v20, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingFraction:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-direct/range {p0 .. p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getScrollingFloatingHeaderHeight()F

    move-result v21

    mul-float v20, v20, v21

    sub-float v3, v3, v20

    .line 2373
    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mActionBarCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setTranslationY(F)V

    .line 2376
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroContainer:Landroid/widget/FrameLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v20

    move/from16 v0, v20

    int-to-float v12, v0

    .line 2377
    .local v12, "heroHeight":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderHeight:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v20, v20, v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderBottomMargin:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    sub-float v20, v20, v21

    const/high16 v21, 0x3f000000    # 0.5f

    mul-float v16, v20, v21

    .line 2378
    .local v16, "normalHeroTop":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroAnimationMode:I

    move/from16 v20, v0

    packed-switch v20, :pswitch_data_0

    .line 2404
    :goto_3
    move/from16 v17, v5

    .line 2405
    .local v17, "playBackgroundTranslation":F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mContentProtectionEnabled:Z

    move/from16 v20, v0

    if-eqz v20, :cond_5

    .line 2407
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    move-object/from16 v20, v0

    if-eqz v20, :cond_4

    .line 2408
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->tryGetContentTop(Landroid/view/ViewGroup;)I

    move-result v8

    .line 2409
    .local v8, "contentTop":I
    const/16 v20, -0x1

    move/from16 v0, v20

    if-eq v8, v0, :cond_4

    .line 2410
    int-to-float v0, v8

    move/from16 v20, v0

    add-float v17, v17, v20

    .line 2413
    .end local v8    # "contentTop":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAltPlayBackgroundCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setTranslationY(F)V

    .line 2419
    :cond_5
    move v4, v5

    .line 2420
    .local v4, "backgroundTop":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAbsoluteY:I

    move/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_b

    .line 2421
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundContainer:Landroid/widget/FrameLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2452
    :cond_6
    :goto_4
    invoke-direct/range {p0 .. p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateHeaderShadow()V

    .line 2454
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mLayoutListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$LayoutListener;

    move-object/from16 v20, v0

    if-eqz v20, :cond_0

    .line 2455
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mLayoutListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$LayoutListener;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$LayoutListener;->onPlayHeaderListLayoutChanged()V

    goto/16 :goto_0

    .line 2349
    .end local v3    # "actionBarTop":F
    .end local v4    # "backgroundTop":F
    .end local v12    # "heroHeight":F
    .end local v16    # "normalHeroTop":F
    .end local v17    # "playBackgroundTranslation":F
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderHeight:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v20, v7, v20

    add-float v9, v9, v20

    goto/16 :goto_1

    .line 2367
    .restart local v3    # "actionBarTop":F
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderBottomMargin:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    sub-float v20, v7, v20

    add-float v20, v20, v5

    invoke-direct/range {p0 .. p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getVisibleTabBarHeight()F

    move-result v21

    sub-float v20, v20, v21

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    sub-float v14, v20, v21

    .line 2370
    .local v14, "maxActionBarTop":F
    invoke-static {v3, v14}, Ljava/lang/Math;->min(FF)F

    move-result v3

    goto/16 :goto_2

    .line 2383
    .end local v14    # "maxActionBarTop":F
    .restart local v12    # "heroHeight":F
    .restart local v16    # "normalHeroTop":F
    :pswitch_0
    add-float v13, v9, v16

    .line 2384
    .local v13, "heroTop":F
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarHeight()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    add-float v2, v3, v20

    .line 2385
    .local v2, "actionBarBottom":F
    cmpl-float v20, v13, v2

    if-ltz v20, :cond_9

    const/4 v11, 0x1

    .line 2386
    .local v11, "heroElementShouldBeVisible":Z
    :goto_5
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v11, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setHeroElementVisible(ZZ)V

    goto/16 :goto_3

    .line 2385
    .end local v11    # "heroElementShouldBeVisible":Z
    :cond_9
    const/4 v11, 0x0

    goto :goto_5

    .line 2390
    .end local v2    # "actionBarBottom":F
    .end local v13    # "heroTop":F
    :pswitch_1
    const/16 v20, 0x0

    add-float v21, v12, v9

    div-float v21, v21, v12

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->max(FF)F

    move-result v10

    .line 2395
    .local v10, "heroAlpha":F
    const/16 v20, 0x0

    cmpl-float v20, v10, v20

    if-lez v20, :cond_a

    const/16 v20, 0x1

    :goto_6
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroVisible:Z

    .line 2396
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v10, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setHeroAnimationValue(FZ)V

    goto/16 :goto_3

    .line 2395
    :cond_a
    const/16 v20, 0x0

    goto :goto_6

    .line 2423
    .end local v10    # "heroAlpha":F
    .restart local v4    # "backgroundTop":F
    .restart local v17    # "playBackgroundTranslation":F
    :cond_b
    const/16 v19, 0x0

    .line 2424
    .local v19, "wasInvisible":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundContainer:Landroid/widget/FrameLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v20

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_c

    .line 2425
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundContainer:Landroid/widget/FrameLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2426
    const/16 v19, 0x1

    .line 2428
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundContainer:Landroid/widget/FrameLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v20

    move/from16 v0, v20

    neg-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundParallaxRatio:F

    move/from16 v21, v0

    div-float v15, v20, v21

    .line 2430
    .local v15, "minHeaderTranslation":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAbsoluteY:I

    move/from16 v20, v0

    move/from16 v0, v20

    neg-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundParallaxRatio:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    add-float v4, v4, v20

    .line 2431
    invoke-static {v15, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 2432
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setTranslationY(F)V

    .line 2435
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundContainer:Landroid/widget/FrameLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    add-float v20, v20, v4

    sub-float v18, v20, v5

    .line 2438
    .local v18, "visibleBackgroundHeight":F
    if-eqz v19, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundFadeInOffsetThresholdPx:F

    move/from16 v20, v0

    cmpl-float v20, v18, v20

    if-lez v20, :cond_6

    .line 2439
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setAlpha(F)V

    .line 2440
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    move-object/from16 v20, v0

    const/high16 v21, 0x3f800000    # 1.0f

    const/16 v22, 0xc8

    invoke-virtual/range {v20 .. v22}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->animateAlpha(FI)V

    goto/16 :goto_4

    .line 2378
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private makeNonFloatingBackground()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 2819
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    return-object v0
.end method

.method private relativeToAbsolute(I)I
    .locals 2
    .param p1, "relativePosition"    # I

    .prologue
    .line 1991
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 1992
    .local v0, "position":I
    if-nez p1, :cond_0

    .line 1993
    add-int/lit8 v0, v0, -0x1

    .line 1995
    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 1996
    add-int/lit8 v0, v0, 0x1

    .line 1998
    :cond_1
    return v0
.end method

.method private static replaceTabStrip(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)V
    .locals 5
    .param p0, "stripToReplace"    # Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    .param p1, "strip"    # Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    .prologue
    const/4 v4, 0x0

    .line 1264
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1265
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 1266
    .local v1, "parent":Landroid/view/ViewGroup;
    invoke-virtual {v1, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v3

    .line 1267
    .local v3, "viewIndex":I
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 1268
    invoke-virtual {v1, p1, v3, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1269
    invoke-virtual {p0, v4}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1270
    .local v2, "stripToReplaceChild":Landroid/view/View;
    invoke-virtual {p0, v4}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->removeViewAt(I)V

    .line 1271
    invoke-virtual {p1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->addView(Landroid/view/View;)V

    .line 1272
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getSubViewReferences()V

    .line 1273
    return-void
.end method

.method private restorePendingSavedStateIfNeeded()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1293
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPendingSavedState:Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1298
    invoke-direct {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateContentPosition(Z)V

    .line 1299
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPendingSavedState:Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

    iget v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;->mFloatingFraction:F

    invoke-direct {p0, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingFraction(FZ)V

    .line 1303
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->layout()V

    .line 1304
    invoke-direct {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->syncListViews(Z)V

    .line 1305
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPendingSavedState:Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

    .line 1306
    iput-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSuppressIdleOnScroll:Z

    .line 1308
    :cond_0
    return-void
.end method

.method private static scrollListViewBy(Landroid/view/ViewGroup;I)V
    .locals 6
    .param p0, "view"    # Landroid/view/ViewGroup;
    .param p1, "scrollBy"    # I

    .prologue
    const/4 v5, 0x0

    .line 3215
    instance-of v3, p0, Landroid/widget/ListView;

    if-eqz v3, :cond_2

    move-object v1, p0

    .line 3216
    check-cast v1, Landroid/widget/ListView;

    .line 3218
    .local v1, "listView":Landroid/widget/ListView;
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-lt v3, v4, :cond_1

    .line 3219
    invoke-virtual {v1, p1}, Landroid/widget/ListView;->scrollListBy(I)V

    .line 3230
    .end local v1    # "listView":Landroid/widget/ListView;
    :cond_0
    :goto_0
    return-void

    .line 3221
    .restart local v1    # "listView":Landroid/widget/ListView;
    :cond_1
    invoke-virtual {v1, p1, v5}, Landroid/widget/ListView;->smoothScrollBy(II)V

    goto :goto_0

    .line 3223
    .end local v1    # "listView":Landroid/widget/ListView;
    :cond_2
    instance-of v3, p0, Landroid/support/v7/widget/RecyclerView;

    if-eqz v3, :cond_3

    move-object v2, p0

    .line 3224
    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    .line 3225
    .local v2, "recyclerView":Landroid/support/v7/widget/RecyclerView;
    invoke-virtual {v2, v5, p1}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    goto :goto_0

    .line 3226
    .end local v2    # "recyclerView":Landroid/support/v7/widget/RecyclerView;
    :cond_3
    instance-of v3, p0, Lcom/google/android/play/headerlist/PlayScrollableContentView;

    if-eqz v3, :cond_0

    move-object v0, p0

    .line 3227
    check-cast v0, Lcom/google/android/play/headerlist/PlayScrollableContentView;

    .line 3228
    .local v0, "contentView":Lcom/google/android/play/headerlist/PlayScrollableContentView;
    invoke-interface {v0, v5, p1}, Lcom/google/android/play/headerlist/PlayScrollableContentView;->scrollBy(II)V

    goto :goto_0
.end method

.method private static scrollListViewToTop(Landroid/view/ViewGroup;)V
    .locals 5
    .param p0, "view"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 3234
    instance-of v3, p0, Landroid/widget/ListView;

    if-eqz v3, :cond_1

    move-object v1, p0

    .line 3235
    check-cast v1, Landroid/widget/ListView;

    .line 3236
    .local v1, "listView":Landroid/widget/ListView;
    invoke-virtual {v1, v4, v4}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 3244
    .end local v1    # "listView":Landroid/widget/ListView;
    :cond_0
    :goto_0
    return-void

    .line 3237
    :cond_1
    instance-of v3, p0, Landroid/support/v7/widget/RecyclerView;

    if-eqz v3, :cond_2

    move-object v2, p0

    .line 3238
    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    .line 3239
    .local v2, "recyclerView":Landroid/support/v7/widget/RecyclerView;
    invoke-virtual {v2, v4}, Landroid/support/v7/widget/RecyclerView;->scrollToPosition(I)V

    goto :goto_0

    .line 3240
    .end local v2    # "recyclerView":Landroid/support/v7/widget/RecyclerView;
    :cond_2
    instance-of v3, p0, Lcom/google/android/play/headerlist/PlayScrollableContentView;

    if-eqz v3, :cond_0

    move-object v0, p0

    .line 3241
    check-cast v0, Lcom/google/android/play/headerlist/PlayScrollableContentView;

    .line 3242
    .local v0, "contentView":Lcom/google/android/play/headerlist/PlayScrollableContentView;
    invoke-interface {v0, v4, v4}, Lcom/google/android/play/headerlist/PlayScrollableContentView;->scrollTo(II)V

    goto :goto_0
.end method

.method private setActionBarTitleVisibility(ZZ)V
    .locals 6
    .param p1, "visible"    # Z
    .param p2, "animate"    # Z

    .prologue
    const-wide/16 v4, 0xc8

    .line 3044
    if-eqz p1, :cond_1

    const/high16 v1, 0x3f800000    # 1.0f

    .line 3045
    .local v1, "toAlpha":F
    :goto_0
    if-eqz p2, :cond_3

    .line 3046
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarTitleAlpha()F

    move-result v2

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_0

    .line 3047
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_2

    .line 3048
    const-string v2, "actionBarTitleAlpha"

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarTitleAlpha()F

    move-result v3

    invoke-direct {p0, v2, v3, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getFloatAnimator(Ljava/lang/String;FF)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 3061
    :cond_0
    :goto_1
    return-void

    .line 3044
    .end local v1    # "toAlpha":F
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 3052
    .restart local v1    # "toAlpha":F
    :cond_2
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$ActionBarTitleAlphaAnimation;

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarTitleAlpha()F

    move-result v2

    invoke-direct {v0, p0, v2, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$ActionBarTitleAlphaAnimation;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;FF)V

    .line 3054
    .local v0, "anim":Lcom/google/android/play/headerlist/PlayHeaderListLayout$ActionBarTitleAlphaAnimation;
    invoke-virtual {v0, v4, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$ActionBarTitleAlphaAnimation;->setDuration(J)V

    .line 3055
    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1

    .line 3059
    .end local v0    # "anim":Lcom/google/android/play/headerlist/PlayHeaderListLayout$ActionBarTitleAlphaAnimation;
    :cond_3
    invoke-virtual {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setActionBarTitleAlpha(F)V

    goto :goto_1
.end method

.method private setActiveListView(Landroid/view/ViewGroup;)Z
    .locals 6
    .param p1, "listView"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1909
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    if-ne v2, p1, :cond_1

    .line 1910
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    move v2, v3

    .line 1971
    :goto_0
    return v2

    :cond_0
    move v2, v4

    .line 1910
    goto :goto_0

    .line 1912
    :cond_1
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    if-eqz v2, :cond_8

    move v0, v3

    .line 1913
    .local v0, "hadTrackedListView":Z
    :goto_1
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    if-eqz v2, :cond_3

    .line 1914
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    instance-of v2, v2, Landroid/widget/ListView;

    if-eqz v2, :cond_9

    .line 1915
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    check-cast v2, Landroid/widget/ListView;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1916
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListScrollListener:Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;

    invoke-virtual {v2}, Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;->reset()V

    .line 1927
    :cond_2
    :goto_2
    iput-boolean v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSuppressIdleOnScroll:Z

    .line 1929
    :cond_3
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    .line 1930
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    if-eqz v2, :cond_e

    .line 1931
    iget-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSuppressIdleOnScroll:Z

    .line 1932
    .local v1, "oldSuppressIdleOnScroll":Z
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSuppressIdleOnScroll:Z

    if-nez v2, :cond_4

    .line 1938
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->isLayoutRequested()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSuppressIdleOnScroll:Z

    .line 1944
    :cond_4
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    instance-of v2, v2, Landroid/widget/ListView;

    if-eqz v2, :cond_b

    .line 1945
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    check-cast v2, Landroid/widget/ListView;

    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListScrollListener:Lcom/google/android/play/headerlist/PlayHeaderListOnScrollListener;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1953
    :cond_5
    :goto_3
    iput-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSuppressIdleOnScroll:Z

    .line 1954
    if-eqz v0, :cond_6

    .line 1958
    invoke-direct {p0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->syncListViews(Z)V

    .line 1962
    :cond_6
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshProvider:Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;

    if-eqz v2, :cond_d

    .line 1963
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshProvider:Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;

    iget-boolean v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHasViewPager:Z

    if-eqz v5, :cond_7

    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v4

    :cond_7
    invoke-virtual {v2, v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;->supportsPullToRefresh(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHasPullToRefresh:Z

    :goto_4
    move v2, v3

    .line 1969
    goto :goto_0

    .end local v0    # "hadTrackedListView":Z
    .end local v1    # "oldSuppressIdleOnScroll":Z
    :cond_8
    move v0, v4

    .line 1912
    goto :goto_1

    .line 1917
    .restart local v0    # "hadTrackedListView":Z
    :cond_9
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    instance-of v2, v2, Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_a

    .line 1918
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v5}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 1919
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListRecyclerViewScrollListener:Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;

    invoke-virtual {v2}, Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;->reset()V

    goto :goto_2

    .line 1920
    :cond_a
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    instance-of v2, v2, Lcom/google/android/play/headerlist/PlayScrollableContentView;

    if-eqz v2, :cond_2

    .line 1921
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    check-cast v2, Lcom/google/android/play/headerlist/PlayScrollableContentView;

    invoke-interface {v2, v5}, Lcom/google/android/play/headerlist/PlayScrollableContentView;->setOnScrollListener(Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;)V

    .line 1922
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListContentViewScrollListener:Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;

    invoke-virtual {v2}, Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;->reset()V

    goto :goto_2

    .line 1946
    .restart local v1    # "oldSuppressIdleOnScroll":Z
    :cond_b
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    instance-of v2, v2, Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_c

    .line 1947
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListRecyclerViewScrollListener:Lcom/google/android/play/headerlist/PlayHeaderListRecyclerViewListener;

    invoke-virtual {v2, v5}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    goto :goto_3

    .line 1949
    :cond_c
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    instance-of v2, v2, Lcom/google/android/play/headerlist/PlayScrollableContentView;

    if-eqz v2, :cond_5

    .line 1950
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    check-cast v2, Lcom/google/android/play/headerlist/PlayScrollableContentView;

    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListContentViewScrollListener:Lcom/google/android/play/headerlist/PlayHeaderScrollableContentListener;

    invoke-interface {v2, v5}, Lcom/google/android/play/headerlist/PlayScrollableContentView;->setOnScrollListener(Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;)V

    goto :goto_3

    .line 1967
    :cond_d
    iput-boolean v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHasPullToRefresh:Z

    goto :goto_4

    .end local v1    # "oldSuppressIdleOnScroll":Z
    :cond_e
    move v2, v4

    .line 1971
    goto/16 :goto_0
.end method

.method private setBannerFraction(FZ)V
    .locals 4
    .param p1, "bannerFraction"    # F
    .param p2, "animate"    # Z

    .prologue
    .line 3296
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerFraction:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    .line 3311
    :goto_0
    return-void

    .line 3300
    :cond_0
    if-eqz p2, :cond_1

    sget-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->USE_ANIMATIONS:Z

    if-eqz v0, :cond_1

    .line 3301
    const-string v0, "bannerFraction"

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getBannerFraction()F

    move-result v1

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getFloatAnimator(Ljava/lang/String;FF)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    .line 3305
    :cond_1
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerFraction:F

    .line 3309
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->layout()V

    goto :goto_0
.end method

.method private setContentContainerMargins(II)V
    .locals 3
    .param p1, "topMargin"    # I
    .param p2, "bottomMargin"    # I

    .prologue
    const/4 v2, 0x0

    .line 3349
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mContentContainer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 3351
    .local v0, "lp":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v0, v2, p1, v2, p2}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 3352
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mContentContainer:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3353
    return-void
.end method

.method private setControlsBackground(Landroid/graphics/drawable/Drawable;Z)V
    .locals 5
    .param p1, "toDrawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "animate"    # Z

    .prologue
    const/4 v4, 0x1

    .line 2915
    if-eqz p2, :cond_3

    .line 2916
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2917
    .local v0, "fromDrawable":Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_0

    .line 2918
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->makeNonFloatingBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2920
    :cond_0
    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->makeNonFloatingBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 2921
    :cond_1
    if-ne v0, p1, :cond_2

    .line 2940
    .end local v0    # "fromDrawable":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-void

    .line 2924
    .restart local v0    # "fromDrawable":Landroid/graphics/drawable/Drawable;
    :cond_2
    new-instance v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout$5;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    aput-object p1, v2, v4

    invoke-direct {v1, p0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$5;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;[Landroid/graphics/drawable/Drawable;)V

    .line 2934
    .local v1, "newBackground":Landroid/graphics/drawable/TransitionDrawable;
    invoke-virtual {v1, v4}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 2935
    const/16 v2, 0x12c

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 2936
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2938
    .end local v0    # "fromDrawable":Landroid/graphics/drawable/Drawable;
    .end local v1    # "newBackground":Landroid/graphics/drawable/TransitionDrawable;
    :cond_3
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, p1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private setControlsContainerHeight(I)V
    .locals 2
    .param p1, "height"    # I

    .prologue
    .line 3325
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 3327
    .local v0, "lp":Landroid/widget/FrameLayout$LayoutParams;
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 3328
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3329
    return-void
.end method

.method private setControlsFloating(ZZ)V
    .locals 6
    .param p1, "floating"    # Z
    .param p2, "animate"    # Z

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 2828
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-ne v2, p1, :cond_0

    .line 2862
    :goto_0
    return-void

    .line 2832
    :cond_0
    if-eqz p1, :cond_4

    .line 2833
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getVisibleHeaderHeight()F

    move-result v2

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getNonScrollingFloatingHeaderHeight()F

    move-result v3

    sub-float v1, v2, v3

    .line 2835
    .local v1, "visibleScrollingControlHeight":F
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getScrollingFloatingHeaderHeight()F

    move-result v0

    .line 2836
    .local v0, "scrollingFloatingHeaderHeight":F
    cmpl-float v2, v0, v4

    if-nez v2, :cond_3

    .line 2837
    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingFraction:F

    .line 2845
    .end local v0    # "scrollingFloatingHeaderHeight":F
    .end local v1    # "visibleScrollingControlHeight":F
    :goto_1
    iput-boolean p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    .line 2848
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAlwaysUseFloatingBackground:Z

    if-nez v2, :cond_1

    .line 2849
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-eqz v2, :cond_5

    .line 2850
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingControlsBackground:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v2, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setControlsBackground(Landroid/graphics/drawable/Drawable;Z)V

    .line 2856
    :cond_1
    :goto_2
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAutoHideToolbarTitle:Z

    if-eqz v2, :cond_2

    .line 2857
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    invoke-direct {p0, v2, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setActionBarTitleVisibility(ZZ)V

    .line 2859
    :cond_2
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateHeaderShadow()V

    .line 2860
    invoke-direct {p0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateTabPadding(Z)V

    .line 2861
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateTabContrast()V

    goto :goto_0

    .line 2839
    .restart local v0    # "scrollingFloatingHeaderHeight":F
    .restart local v1    # "visibleScrollingControlHeight":F
    :cond_3
    div-float v2, v1, v0

    invoke-static {v5, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingFraction:F

    goto :goto_1

    .line 2843
    .end local v0    # "scrollingFloatingHeaderHeight":F
    .end local v1    # "visibleScrollingControlHeight":F
    :cond_4
    iput v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingFraction:F

    goto :goto_1

    .line 2852
    :cond_5
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->makeNonFloatingBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setControlsBackground(Landroid/graphics/drawable/Drawable;Z)V

    goto :goto_2
.end method

.method private final setFloatingFraction(FZ)V
    .locals 1
    .param p1, "fraction"    # F
    .param p2, "layout"    # Z

    .prologue
    .line 2793
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingFraction:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 2794
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingFraction:F

    .line 2795
    if-eqz p2, :cond_0

    .line 2799
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->layout()V

    .line 2800
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->syncListViews(Z)V

    .line 2803
    :cond_0
    return-void
.end method

.method private setHeaderShadowTopMargin(I)V
    .locals 3
    .param p1, "topMargin"    # I

    .prologue
    const/4 v2, 0x0

    .line 3342
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderShadow:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 3343
    .local v0, "lp":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v0, v2, p1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 3344
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderShadow:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3345
    return-void
.end method

.method private setHeaderShadowVisible(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    const/4 v1, 0x0

    .line 3002
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderShadowVisible:Z

    if-eq v0, p1, :cond_1

    .line 3003
    iput-boolean p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderShadowVisible:Z

    .line 3004
    sget-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->SUPPORT_ELEVATION:Z

    if-eqz v0, :cond_4

    .line 3005
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getFloatingHeaderElevation()F

    move-result v0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setZ(F)V

    .line 3008
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mActionBarCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    if-eqz p1, :cond_3

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getFloatingHeaderElevation()F

    move-result v0

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setZ(F)V

    .line 3009
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerTextView:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getFloatingHeaderElevation()F

    move-result v1

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setZ(F)V

    .line 3014
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v1

    .line 3005
    goto :goto_0

    :cond_3
    move v0, v1

    .line 3008
    goto :goto_1

    .line 3011
    :cond_4
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderShadow:Landroid/view/View;

    if-eqz p1, :cond_5

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_5
    const/4 v0, 0x4

    goto :goto_3
.end method

.method private setHeroAnimationValue(FZ)V
    .locals 2
    .param p1, "value"    # F
    .param p2, "animate"    # Z

    .prologue
    .line 2692
    if-eqz p2, :cond_0

    .line 2693
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroAnimationMode:I

    packed-switch v0, :pswitch_data_0

    .line 2722
    :goto_0
    return-void

    .line 2695
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    const/16 v1, 0x64

    invoke-virtual {v0, p1, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->animateScale(FI)V

    goto :goto_0

    .line 2701
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    const/16 v1, 0xc8

    invoke-virtual {v0, p1, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->animateAlpha(FI)V

    goto :goto_0

    .line 2709
    :cond_0
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroAnimationMode:I

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 2711
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    invoke-virtual {v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setScale(F)V

    goto :goto_0

    .line 2716
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    invoke-virtual {v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setAlpha(F)V

    goto :goto_0

    .line 2693
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 2709
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private setHeroElementVisible(ZZ)V
    .locals 1
    .param p1, "visible"    # Z
    .param p2, "animate"    # Z

    .prologue
    .line 2671
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroVisible:Z

    if-eq v0, p1, :cond_0

    .line 2672
    iput-boolean p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroVisible:Z

    .line 2673
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroVisible:Z

    if-eqz v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-direct {p0, v0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setHeroAnimationValue(FZ)V

    .line 2675
    :cond_0
    return-void

    .line 2673
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setPullToRefreshEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1390
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->isPullToRefreshEnabled()Z

    move-result v0

    if-eq p1, v0, :cond_0

    .line 1395
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mScrollProxyView:Lcom/google/android/play/widget/ScrollProxyView;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v1, v0}, Lcom/google/android/play/widget/ScrollProxyView;->scrollTo(II)V

    .line 1397
    :cond_0
    return-void

    .line 1395
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setupBackground(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V
    .locals 2
    .param p1, "configurator"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;

    .prologue
    .line 2205
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mContentProtectionEnabled:Z

    if-eqz v0, :cond_0

    .line 2206
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAltPlayBackground:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->getContentProtectionBackground(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2208
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAltPlayBackground:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2210
    :cond_0
    return-void
.end method

.method private setupViewPagerIfNeeded()V
    .locals 3

    .prologue
    .line 2214
    iget-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHasViewPager:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v1, :cond_0

    .line 2215
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mContentContainer:Landroid/view/View;

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPagerId:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 2216
    .local v0, "viewPager":Landroid/support/v4/view/ViewPager;
    if-nez v0, :cond_1

    .line 2229
    .end local v0    # "viewPager":Landroid/support/v4/view/ViewPager;
    :cond_0
    :goto_0
    return-void

    .line 2226
    .restart local v0    # "viewPager":Landroid/support/v4/view/ViewPager;
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    goto :goto_0
.end method

.method private shouldFloat()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v7, -0x1

    .line 2523
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    .line 2524
    .local v0, "shouldFloat":Z
    iget-boolean v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-eqz v5, :cond_2

    .line 2530
    iget v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderHeight:I

    int-to-float v5, v5

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getFullFloatingHeaderHeight()F

    move-result v6

    sub-float/2addr v5, v6

    iget v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderBottomMargin:I

    int-to-float v6, v6

    sub-float v2, v5, v6

    .line 2532
    .local v2, "snapThreshold":F
    iget v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAbsoluteY:I

    if-eq v5, v7, :cond_0

    iget v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAbsoluteY:I

    int-to-float v5, v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v6

    if-le v5, v6, :cond_1

    :cond_0
    move v0, v4

    .line 2541
    .end local v2    # "snapThreshold":F
    :goto_0
    return v0

    .restart local v2    # "snapThreshold":F
    :cond_1
    move v0, v3

    .line 2532
    goto :goto_0

    .line 2536
    .end local v2    # "snapThreshold":F
    :cond_2
    iget v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderHeight:I

    iget v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderBottomMargin:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getNonScrollingFloatingHeaderHeight()F

    move-result v6

    sub-float v1, v5, v6

    .line 2538
    .local v1, "shouldFloatThreshold":F
    iget v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAbsoluteY:I

    int-to-float v5, v5

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v6

    if-ge v5, v6, :cond_3

    iget v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAbsoluteY:I

    if-ne v5, v7, :cond_4

    :cond_3
    move v0, v4

    :goto_1
    goto :goto_0

    :cond_4
    move v0, v3

    goto :goto_1
.end method

.method private showBanner(Z)V
    .locals 2
    .param p1, "animate"    # Z

    .prologue
    const/4 v1, 0x0

    .line 3265
    sget-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->USE_ANIMATIONS:Z

    if-eqz v0, :cond_1

    .line 3266
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setBannerFraction(FZ)V

    .line 3267
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getBannerHeight()I

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setContentContainerMargins(II)V

    .line 3275
    :cond_0
    :goto_0
    return-void

    .line 3269
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerVisibleGB:Z

    if-nez v0, :cond_0

    .line 3270
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerVisibleGB:Z

    .line 3271
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3272
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getBannerHeight()I

    move-result v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setContentContainerMargins(II)V

    goto :goto_0
.end method

.method private snapControlsIfNeeded(ZZZ)V
    .locals 10
    .param p1, "snapDown"    # Z
    .param p2, "snapEvenIfInvisible"    # Z
    .param p3, "animate"    # Z

    .prologue
    const-wide/16 v8, 0xc8

    const/4 v7, 0x1

    .line 2735
    iget v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderMode:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_1

    .line 2780
    :cond_0
    :goto_0
    return-void

    .line 2738
    :cond_1
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSnapControlsUpIfNeededRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2739
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSnapControlsDownIfNeededRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2741
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getVisibleHeaderHeight()F

    move-result v4

    .line 2742
    .local v4, "visibleControlHeight":F
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getNonScrollingFloatingHeaderHeight()F

    move-result v2

    .line 2743
    .local v2, "minVisibleControlHeightForSnap":F
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getFullFloatingHeaderHeight()F

    move-result v1

    .line 2744
    .local v1, "maxVisibleControlHeightForSnap":F
    if-nez p2, :cond_2

    cmpl-float v5, v4, v2

    if-lez v5, :cond_0

    :cond_2
    cmpg-float v5, v4, v1

    if-gez v5, :cond_0

    .line 2746
    if-eqz p1, :cond_4

    const/high16 v3, 0x3f800000    # 1.0f

    .line 2748
    .local v3, "newFloatingFraction":F
    :goto_1
    iget-boolean v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-nez v5, :cond_3

    .line 2750
    cmpl-float v5, v4, v1

    if-gez v5, :cond_0

    .line 2761
    invoke-direct {p0, v7, p3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setControlsFloating(ZZ)V

    .line 2764
    :cond_3
    if-eqz p3, :cond_6

    .line 2765
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-lt v5, v6, :cond_5

    .line 2766
    const-string v5, "floatingFraction"

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getFloatingFraction()F

    move-result v6

    invoke-direct {p0, v5, v6, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getFloatAnimator(Ljava/lang/String;FF)Landroid/animation/ObjectAnimator;

    move-result-object v5

    invoke-virtual {v5, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v5

    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    .line 2746
    .end local v3    # "newFloatingFraction":F
    :cond_4
    const/4 v3, 0x0

    goto :goto_1

    .line 2770
    .restart local v3    # "newFloatingFraction":F
    :cond_5
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$FloatingFractionAnimation;

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getFloatingFraction()F

    move-result v5

    invoke-direct {v0, p0, v5, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$FloatingFractionAnimation;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;FF)V

    .line 2773
    .local v0, "animation":Lcom/google/android/play/headerlist/PlayHeaderListLayout$FloatingFractionAnimation;
    invoke-virtual {v0, v8, v9}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$FloatingFractionAnimation;->setDuration(J)V

    .line 2774
    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 2777
    .end local v0    # "animation":Lcom/google/android/play/headerlist/PlayHeaderListLayout$FloatingFractionAnimation;
    :cond_6
    invoke-direct {p0, v3, v7}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingFraction(FZ)V

    goto :goto_0
.end method

.method private syncListView(Landroid/view/ViewGroup;Z)Z
    .locals 6
    .param p1, "listView"    # Landroid/view/ViewGroup;
    .param p2, "isCurrent"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 3121
    invoke-virtual {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->isListViewReady(Landroid/view/ViewGroup;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3191
    :goto_0
    return v3

    .line 3127
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->tryGetContentTop(Landroid/view/ViewGroup;)I

    move-result v0

    .line 3128
    .local v0, "contentTop":I
    const/4 v5, -0x1

    if-ne v0, v5, :cond_2

    .line 3131
    iget-boolean v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-eqz v5, :cond_1

    move v3, v4

    .line 3137
    goto :goto_0

    .line 3147
    :cond_1
    iput-boolean v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSuppressIdleOnScroll:Z

    .line 3148
    invoke-static {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->scrollListViewToTop(Landroid/view/ViewGroup;)V

    .line 3149
    iput-boolean v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSuppressIdleOnScroll:Z

    goto :goto_0

    .line 3152
    :cond_2
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getDesiredContentTop()I

    move-result v1

    .line 3153
    .local v1, "desiredContentTop":I
    sub-int v2, v0, v1

    .line 3154
    .local v2, "scrollBy":I
    iget-boolean v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-eqz v5, :cond_3

    if-gez v2, :cond_3

    move v3, v4

    .line 3163
    goto :goto_0

    .line 3165
    :cond_3
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-lt v5, v3, :cond_5

    invoke-static {p1, v2}, Landroid/support/v4/view/ViewCompat;->canScrollVertically(Landroid/view/View;I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 3171
    iput-boolean v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSuppressIdleOnScroll:Z

    .line 3172
    invoke-static {p1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->scrollListViewBy(Landroid/view/ViewGroup;I)V

    .line 3173
    iput-boolean v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSuppressIdleOnScroll:Z

    .line 3174
    if-eqz p2, :cond_4

    .line 3180
    iput-boolean v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mUpdateContentPositionOnLayout:Z

    :cond_4
    :goto_1
    move v3, v4

    .line 3191
    goto :goto_0

    .line 3184
    :cond_5
    if-eqz p2, :cond_4

    .line 3188
    invoke-direct {p0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateContentPosition(Z)V

    goto :goto_1
.end method

.method private syncListViews(Z)V
    .locals 6
    .param p1, "includeCurrent"    # Z

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 3069
    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v4, :cond_0

    .line 3096
    :goto_0
    return-void

    .line 3073
    :cond_0
    const/4 v1, 0x0

    .line 3074
    .local v1, "retryCurrent":Z
    if-eqz p1, :cond_1

    .line 3075
    invoke-direct {p0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->syncViewPagerListView(I)Z

    move-result v1

    .line 3076
    if-nez v1, :cond_1

    .line 3081
    iput-boolean v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSuppressIdleOnScroll:Z

    .line 3084
    :cond_1
    move v0, v1

    .line 3085
    .local v0, "retry":Z
    invoke-direct {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->syncViewPagerListView(I)Z

    move-result v4

    or-int/2addr v0, v4

    .line 3086
    invoke-direct {p0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->syncViewPagerListView(I)Z

    move-result v4

    or-int/2addr v0, v4

    .line 3087
    if-eqz v0, :cond_3

    .line 3092
    if-eqz v1, :cond_2

    :goto_1
    iput v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPendingListSync:I

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1

    .line 3094
    :cond_3
    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPendingListSync:I

    goto :goto_0
.end method

.method private syncViewPagerListView(I)Z
    .locals 5
    .param p1, "relativePosition"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3103
    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v4

    if-nez v4, :cond_1

    .line 3112
    :cond_0
    :goto_0
    return v3

    .line 3107
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->relativeToAbsolute(I)I

    move-result v1

    .line 3108
    .local v1, "position":I
    if-ltz v1, :cond_0

    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 3111
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getListView(I)Landroid/view/ViewGroup;

    move-result-object v0

    .line 3112
    .local v0, "listView":Landroid/view/ViewGroup;
    if-ne p1, v2, :cond_2

    :goto_1
    invoke-direct {p0, v0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->syncListView(Landroid/view/ViewGroup;Z)Z

    move-result v3

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1
.end method

.method private updateActiveListView()V
    .locals 2

    .prologue
    .line 1976
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getListView(I)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setActiveListView(Landroid/view/ViewGroup;)Z

    move-result v0

    .line 1977
    .local v0, "hasActiveList":Z
    if-nez v0, :cond_0

    .line 1984
    :cond_0
    return-void
.end method

.method private updateContentPosition(Z)V
    .locals 1
    .param p1, "doLayout"    # Z

    .prologue
    .line 2599
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->isTrackedListViewReady()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2621
    :cond_0
    :goto_0
    return-void

    .line 2605
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTrackedListView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->tryGetCollectionViewAbsoluteY(Landroid/view/ViewGroup;)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAbsoluteY:I

    .line 2606
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshProvider:Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;

    if-eqz v0, :cond_2

    .line 2607
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAbsoluteY:I

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setPullToRefreshEnabled(Z)V

    .line 2614
    :cond_2
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateFloatingState()Z

    .line 2618
    if-eqz p1, :cond_0

    .line 2619
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->layout()V

    goto :goto_0

    .line 2607
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private updateFloatingState()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2550
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->shouldFloat()Z

    move-result v0

    .line 2551
    .local v0, "shouldFloat":Z
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-eq v0, v2, :cond_0

    .line 2554
    invoke-direct {p0, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setControlsFloating(ZZ)V

    .line 2560
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateHeaderShadow()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2975
    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderShadowMode:I

    packed-switch v2, :pswitch_data_0

    .line 2989
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAlwaysUseFloatingBackground:Z

    if-eqz v2, :cond_3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getVisibleHeaderHeight()F

    move-result v2

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->isBackgroundVisible()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2995
    .local v0, "headerShadowShouldBeVisible":Z
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setHeaderShadowVisible(Z)V

    .line 2996
    return-void

    .line 2977
    .end local v0    # "headerShadowShouldBeVisible":Z
    :pswitch_0
    const/4 v0, 0x0

    .line 2978
    .restart local v0    # "headerShadowShouldBeVisible":Z
    goto :goto_0

    .line 2980
    .end local v0    # "headerShadowShouldBeVisible":Z
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getVisibleHeaderHeight()F

    move-result v2

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 2981
    .restart local v0    # "headerShadowShouldBeVisible":Z
    :goto_1
    goto :goto_0

    .end local v0    # "headerShadowShouldBeVisible":Z
    :cond_1
    move v0, v1

    .line 2980
    goto :goto_1

    .line 2983
    :pswitch_2
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getVisibleHeaderHeight()F

    move-result v2

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    .line 2985
    .restart local v0    # "headerShadowShouldBeVisible":Z
    :goto_2
    goto :goto_0

    .end local v0    # "headerShadowShouldBeVisible":Z
    :cond_2
    move v0, v1

    .line 2983
    goto :goto_2

    :cond_3
    move v0, v1

    .line 2989
    goto :goto_0

    .line 2975
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateHeaderShadowTopMargin()V
    .locals 2

    .prologue
    .line 3337
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderHeight:I

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderBottomMargin:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setHeaderShadowTopMargin(I)V

    .line 3338
    return-void
.end method

.method private updateTabBarVisibility()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 2178
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabMode:I

    packed-switch v0, :pswitch_data_0

    .line 2195
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected tab mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2180
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabBar:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2181
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabBarTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2182
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabStrip:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->setVisibility(I)V

    .line 2197
    :goto_0
    return-void

    .line 2185
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabBar:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2186
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabBarTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2187
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabStrip:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-virtual {v0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->setVisibility(I)V

    goto :goto_0

    .line 2190
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabBar:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2191
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabBarTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2192
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabStrip:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->setVisibility(I)V

    goto :goto_0

    .line 2178
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updateTabContrast()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2891
    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabA11yMode:I

    packed-switch v2, :pswitch_data_0

    .line 2903
    const/4 v0, 0x0

    .line 2906
    .local v0, "useHighContrast":Z
    :goto_0
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabStrip:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-virtual {v1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->setUseHighContrast(Z)V

    .line 2907
    return-void

    .line 2893
    .end local v0    # "useHighContrast":Z
    :pswitch_0
    const/4 v0, 0x1

    .line 2894
    .restart local v0    # "useHighContrast":Z
    goto :goto_0

    .line 2896
    .end local v0    # "useHighContrast":Z
    :pswitch_1
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAlwaysUseFloatingBackground:Z

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 2897
    .restart local v0    # "useHighContrast":Z
    :cond_1
    goto :goto_0

    .line 2899
    .end local v0    # "useHighContrast":Z
    :pswitch_2
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAlwaysUseFloatingBackground:Z

    if-nez v2, :cond_2

    move v0, v1

    .line 2900
    .restart local v0    # "useHighContrast":Z
    :cond_2
    goto :goto_0

    .line 2891
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private updateTabPadding(Z)V
    .locals 2
    .param p1, "animate"    # Z

    .prologue
    .line 2868
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabMode:I

    if-nez v1, :cond_0

    .line 2870
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabPaddingMode:I

    packed-switch v1, :pswitch_data_0

    .line 2879
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    .line 2882
    .local v0, "useFloatingTabPadding":Z
    :goto_0
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabStrip:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->setUseFloatingTabPadding(ZZ)V

    .line 2884
    .end local v0    # "useFloatingTabPadding":Z
    :cond_0
    return-void

    .line 2872
    :pswitch_0
    const/4 v0, 0x1

    .line 2873
    .restart local v0    # "useFloatingTabPadding":Z
    goto :goto_0

    .line 2875
    .end local v0    # "useFloatingTabPadding":Z
    :pswitch_1
    const/4 v0, 0x0

    .line 2876
    .restart local v0    # "useFloatingTabPadding":Z
    goto :goto_0

    .line 2870
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static validateListView(Landroid/view/View;)Landroid/view/ViewGroup;
    .locals 2
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 2055
    if-eqz p0, :cond_0

    instance-of v0, p0, Landroid/widget/ListView;

    if-nez v0, :cond_0

    instance-of v0, p0, Landroid/support/v7/widget/RecyclerView;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/google/android/play/headerlist/PlayScrollableContentView;

    if-eqz v0, :cond_1

    .line 2057
    :cond_0
    check-cast p0, Landroid/view/ViewGroup;

    .end local p0    # "view":Landroid/view/View;
    return-object p0

    .line 2059
    .restart local p0    # "view":Landroid/view/View;
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Found a view that isn\'t a ListView or a RecyclerView or a PlayScrollableContentView implementation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public activateBuiltInToolbar()V
    .locals 2

    .prologue
    .line 1793
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mUseBuiltInToolbar:Z

    if-eqz v0, :cond_0

    .line 1794
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/ActionBarActivity;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 1796
    :cond_0
    return-void
.end method

.method protected applyActionBarTitleAlpha(Landroid/support/v7/widget/Toolbar;F)V
    .locals 5
    .param p1, "toolbar"    # Landroid/support/v7/widget/Toolbar;
    .param p2, "alpha"    # F

    .prologue
    .line 1819
    const/4 v2, 0x0

    const/16 v3, 0xff

    const/high16 v4, 0x437f0000    # 255.0f

    mul-float/2addr v4, p2

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1820
    .local v0, "alphaInt":I
    const v2, 0xffffff

    shl-int/lit8 v3, v0, 0x18

    or-int v1, v2, v3

    .line 1821
    .local v1, "titleColor":I
    invoke-virtual {p1, v1}, Landroid/support/v7/widget/Toolbar;->setTitleTextColor(I)V

    .line 1822
    invoke-virtual {p1, v1}, Landroid/support/v7/widget/Toolbar;->setSubtitleTextColor(I)V

    .line 1823
    return-void
.end method

.method public configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V
    .locals 9
    .param p1, "configurator"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1142
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->getBackgroundViewParallaxRatio()F

    move-result v5

    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundParallaxRatio:F

    .line 1143
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->getListViewId()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mListViewId:I

    .line 1144
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->getViewPagerId()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPagerId:I

    .line 1145
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->getHeaderSpacerId()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSpacerId:I

    .line 1146
    iget v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSpacerId:I

    if-nez v5, :cond_5

    sget v5, Lcom/google/android/play/R$id;->play_header_spacer:I

    :goto_0
    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSpacerId:I

    .line 1147
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->useBuiltInActionBar()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mUseBuiltInToolbar:Z

    .line 1148
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->hasViewPager()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHasViewPager:Z

    .line 1149
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->getTabMode()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabMode:I

    .line 1150
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->getContentProtectionMode()I

    move-result v5

    if-ne v5, v6, :cond_6

    move v5, v6

    :goto_1
    iput-boolean v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mContentProtectionEnabled:Z

    .line 1152
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->getHeroAnimationMode()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroAnimationMode:I

    .line 1153
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->getToolbarTitleMode()I

    move-result v5

    if-nez v5, :cond_7

    move v5, v6

    :goto_2
    iput-boolean v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAutoHideToolbarTitle:Z

    .line 1155
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->getHeaderMode()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderMode:I

    .line 1156
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->getHeaderShadowMode()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderShadowMode:I

    .line 1157
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->alwaysUseFloatingBackground()Z

    move-result v5

    if-nez v5, :cond_0

    sget-boolean v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->USE_ANIMATIONS:Z

    if-nez v5, :cond_8

    :cond_0
    move v5, v6

    :goto_3
    iput-boolean v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAlwaysUseFloatingBackground:Z

    .line 1159
    sget-boolean v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->USE_ANIMATIONS:Z

    if-eqz v5, :cond_9

    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->getTabPaddingMode()I

    move-result v5

    :goto_4
    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabPaddingMode:I

    .line 1161
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->getTabAccessibilityMode()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabA11yMode:I

    .line 1162
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 1163
    .local v4, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {p1, v5, v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->getCustomTabStrip(Landroid/content/Context;Landroid/view/LayoutInflater;)Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    move-result-object v3

    .line 1167
    .local v3, "customTabStrip":Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    sget-boolean v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->USE_ANIMATIONS:Z

    if-eqz v5, :cond_a

    sget v5, Lcom/google/android/play/R$layout;->play_header_list_layout:I

    :goto_5
    invoke-virtual {v4, v5, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1169
    sget v5, Lcom/google/android/play/R$id;->background_container:I

    invoke-virtual {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout;

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundContainer:Landroid/widget/FrameLayout;

    .line 1170
    new-instance v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundContainer:Landroid/widget/FrameLayout;

    invoke-direct {v5, v8}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;-><init>(Landroid/view/View;)V

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    .line 1171
    sget v5, Lcom/google/android/play/R$id;->alt_play_background:I

    invoke-virtual {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAltPlayBackground:Landroid/view/View;

    .line 1172
    new-instance v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAltPlayBackground:Landroid/view/View;

    invoke-direct {v5, v8}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;-><init>(Landroid/view/View;)V

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAltPlayBackgroundCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    .line 1173
    sget v5, Lcom/google/android/play/R$id;->content_container:I

    invoke-virtual {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mContentContainer:Landroid/view/View;

    .line 1174
    new-instance v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mContentContainer:Landroid/view/View;

    invoke-direct {v5, v8}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;-><init>(Landroid/view/View;)V

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mContentContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    .line 1175
    sget v5, Lcom/google/android/play/R$id;->controls_container:I

    invoke-virtual {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsContainer:Landroid/view/ViewGroup;

    .line 1176
    new-instance v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsContainer:Landroid/view/ViewGroup;

    invoke-direct {v5, v8}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;-><init>(Landroid/view/View;)V

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    .line 1177
    sget v5, Lcom/google/android/play/R$id;->header_shadow:I

    invoke-virtual {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderShadow:Landroid/view/View;

    .line 1178
    new-instance v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderShadow:Landroid/view/View;

    invoke-direct {v5, v8}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;-><init>(Landroid/view/View;)V

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderShadowCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    .line 1179
    sget v5, Lcom/google/android/play/R$id;->hero_container:I

    invoke-virtual {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout;

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroContainer:Landroid/widget/FrameLayout;

    .line 1180
    new-instance v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroContainer:Landroid/widget/FrameLayout;

    invoke-direct {v5, v8}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;-><init>(Landroid/view/View;)V

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    .line 1181
    sget v5, Lcom/google/android/play/R$id;->tab_bar:I

    invoke-virtual {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabBar:Landroid/view/View;

    .line 1182
    sget v5, Lcom/google/android/play/R$id;->pager_tab_strip:I

    invoke-virtual {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabStrip:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    .line 1183
    if-eqz v3, :cond_1

    .line 1184
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabStrip:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-static {v5, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->replaceTabStrip(Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;)V

    .line 1185
    iput-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabStrip:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    .line 1187
    :cond_1
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabStrip:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mOnPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-virtual {v5, v8}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->setExternalOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 1188
    sget v5, Lcom/google/android/play/R$id;->tab_bar_title:I

    invoke-virtual {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabBarTitleView:Landroid/widget/TextView;

    .line 1190
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->getHeaderHeight()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderHeight:I

    .line 1191
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->getHeaderBottomMargin()I

    move-result v5

    iput v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderBottomMargin:I

    .line 1192
    iget v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderHeight:I

    iget v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderBottomMargin:I

    sub-int/2addr v5, v8

    invoke-direct {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setControlsContainerHeight(I)V

    .line 1193
    sget-boolean v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->SUPPORT_ELEVATION:Z

    if-eqz v5, :cond_b

    .line 1199
    :goto_6
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getToolbar()Landroid/support/v7/widget/Toolbar;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mToolbar:Landroid/support/v7/widget/Toolbar;

    .line 1200
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarView()Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mActionBarView:Landroid/view/View;

    .line 1201
    new-instance v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mActionBarView:Landroid/view/View;

    invoke-direct {v5, v8}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;-><init>(Landroid/view/View;)V

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mActionBarCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    .line 1202
    sget v5, Lcom/google/android/play/R$id;->play_header_banner:I

    invoke-virtual {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerTextView:Landroid/widget/TextView;

    .line 1203
    new-instance v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerTextView:Landroid/widget/TextView;

    invoke-direct {v5, v8}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;-><init>(Landroid/view/View;)V

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerTextViewCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    .line 1206
    sget v5, Lcom/google/android/play/R$id;->swipe_refresh_layout:I

    invoke-virtual {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 1207
    new-instance v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    iget-object v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-direct {v5, v8}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;-><init>(Landroid/view/View;)V

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayoutCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    .line 1208
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v5, p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;)V

    .line 1209
    sget v5, Lcom/google/android/play/R$id;->scroll_proxy:I

    invoke-virtual {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/play/widget/ScrollProxyView;

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mScrollProxyView:Lcom/google/android/play/widget/ScrollProxyView;

    .line 1211
    invoke-direct {p0, v7}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setPullToRefreshEnabled(Z)V

    .line 1214
    sget-boolean v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->USE_ANIMATIONS:Z

    if-eqz v5, :cond_2

    .line 1215
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBackgroundContainer:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v4, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->addBackgroundView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 1216
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroContainer:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v4, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->addHeroView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 1218
    :cond_2
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mContentContainer:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    .line 1219
    .local v0, "contentContainer":Landroid/view/ViewGroup;
    invoke-virtual {p1, v4, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;->addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 1221
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-ne v5, v6, :cond_3

    .line 1222
    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1223
    .local v2, "contentView":Landroid/view/View;
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mContentContainer:Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->indexOfChild(Landroid/view/View;)I

    move-result v1

    .line 1224
    .local v1, "contentContainerIndex":I
    invoke-virtual {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->removeViewAt(I)V

    .line 1225
    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 1226
    invoke-virtual {p0, v2, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->addView(Landroid/view/View;I)V

    .line 1227
    iput-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mContentContainer:Landroid/view/View;

    .line 1228
    new-instance v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    iget-object v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mContentContainer:Landroid/view/View;

    invoke-direct {v5, v6}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;-><init>(Landroid/view/View;)V

    iput-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mContentContainerCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    .line 1232
    .end local v1    # "contentContainerIndex":I
    .end local v2    # "contentView":Landroid/view/View;
    :cond_3
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateTabBarVisibility()V

    .line 1233
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setupBackground(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 1236
    iget-boolean v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAlwaysUseFloatingBackground:Z

    if-eqz v5, :cond_4

    .line 1237
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingControlsBackground:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v5, v7}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setControlsBackground(Landroid/graphics/drawable/Drawable;Z)V

    .line 1240
    :cond_4
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerTextViewCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getBannerHeight()I

    move-result v6

    neg-int v6, v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setTranslationY(F)V

    .line 1243
    iget v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerFraction:F

    invoke-virtual {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setBannerFraction(F)V

    .line 1244
    invoke-direct {p0, v7}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateTabPadding(Z)V

    .line 1245
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateTabContrast()V

    .line 1246
    return-void

    .line 1146
    .end local v0    # "contentContainer":Landroid/view/ViewGroup;
    .end local v3    # "customTabStrip":Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    :cond_5
    iget v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSpacerId:I

    goto/16 :goto_0

    :cond_6
    move v5, v7

    .line 1150
    goto/16 :goto_1

    :cond_7
    move v5, v7

    .line 1153
    goto/16 :goto_2

    :cond_8
    move v5, v7

    .line 1157
    goto/16 :goto_3

    :cond_9
    move v5, v6

    .line 1159
    goto/16 :goto_4

    .line 1167
    .restart local v3    # "customTabStrip":Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    .restart local v4    # "inflater":Landroid/view/LayoutInflater;
    :cond_a
    sget v5, Lcom/google/android/play/R$layout;->play_header_list_layout_gb:I

    goto/16 :goto_5

    .line 1197
    :cond_b
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateHeaderShadowTopMargin()V

    goto/16 :goto_6
.end method

.method public detach()V
    .locals 0

    .prologue
    .line 1904
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->detachIfNeeded()V

    .line 1905
    return-void
.end method

.method public getActionBarHeight()I
    .locals 1

    .prologue
    .line 2137
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarHeight(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected final getActionBarTitleAlpha()F
    .locals 1

    .prologue
    .line 1829
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mActionBarTitleAlpha:F

    return v0
.end method

.method public getActionBarView()Landroid/view/View;
    .locals 2

    .prologue
    .line 3018
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mActionBarView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 3019
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mActionBarView:Landroid/view/View;

    .line 3024
    :goto_0
    return-object v0

    .line 3021
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mUseBuiltInToolbar:Z

    if-eqz v0, :cond_1

    .line 3022
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mToolbar:Landroid/support/v7/widget/Toolbar;

    goto :goto_0

    .line 3024
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$id;->action_bar_container:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method protected final getBannerFraction()F
    .locals 1

    .prologue
    .line 3320
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerFraction:F

    return v0
.end method

.method public getBannerHeight()I
    .locals 2

    .prologue
    .line 2171
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$dimen;->play_header_list_banner_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public getBannerText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1664
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getCurrentListView()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 2032
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getListView(I)Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method protected final getFloatingFraction()F
    .locals 1

    .prologue
    .line 2809
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-eqz v0, :cond_0

    .line 2810
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingFraction:F

    .line 2812
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHeaderHeight()I
    .locals 1

    .prologue
    .line 1759
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderHeight:I

    return v0
.end method

.method public getHeroElementVisible()Z
    .locals 1

    .prologue
    .line 2681
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeroVisible:Z

    return v0
.end method

.method public getSwipeRefreshLayout()Landroid/support/v4/widget/SwipeRefreshLayout;
    .locals 1

    .prologue
    .line 1472
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    return-object v0
.end method

.method public getTabBarHeight()I
    .locals 2

    .prologue
    .line 2164
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabMode:I

    invoke-static {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getTabBarHeight(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method public getTabMode()I
    .locals 1

    .prologue
    .line 1754
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabMode:I

    return v0
.end method

.method public getVisibleHeaderHeight()F
    .locals 5

    .prologue
    .line 1770
    iget-boolean v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-eqz v3, :cond_0

    .line 1771
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getNonScrollingFloatingHeaderHeight()F

    move-result v0

    .line 1772
    .local v0, "nonScrollingHeaderHeight":F
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getScrollingFloatingHeaderHeight()F

    move-result v1

    .line 1773
    .local v1, "scrollingHeaderHeight":F
    iget v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingFraction:F

    mul-float/2addr v3, v1

    add-float v2, v0, v3

    .line 1777
    .end local v0    # "nonScrollingHeaderHeight":F
    .end local v1    # "scrollingHeaderHeight":F
    .local v2, "visibleHeight":F
    :goto_0
    return v2

    .line 1775
    .end local v2    # "visibleHeight":F
    :cond_0
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContentPosition()F

    move-result v3

    iget v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderBottomMargin:I

    int-to-float v4, v4

    sub-float v2, v3, v4

    .restart local v2    # "visibleHeight":F
    goto :goto_0
.end method

.method public isBannerShowing()Z
    .locals 1

    .prologue
    .line 1672
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHeaderFloating()Z
    .locals 1

    .prologue
    .line 1782
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAlwaysUseFloatingBackground:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHeaderLocked()Z
    .locals 1

    .prologue
    .line 1707
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mLockHeader:Z

    return v0
.end method

.method protected isListViewReady(Landroid/view/ViewGroup;)Z
    .locals 4
    .param p1, "listView"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2576
    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getAdapterCount(Landroid/view/ViewGroup;)I

    move-result v2

    if-gt v2, v0, :cond_2

    :cond_0
    move v0, v1

    .line 2591
    :cond_1
    :goto_0
    return v0

    .line 2581
    :cond_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-gt v2, v0, :cond_1

    .line 2585
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ne v2, v0, :cond_3

    .line 2588
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    iget v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSpacerId:I

    if-ne v2, v3, :cond_1

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2591
    goto :goto_0
.end method

.method public lockHeader(Z)V
    .locals 1
    .param p1, "animate"    # Z

    .prologue
    .line 1691
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mLockHeader:Z

    .line 1692
    invoke-virtual {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->showActionBar(Z)V

    .line 1693
    return-void
.end method

.method public notifyPagerAdapterChanged()V
    .locals 1

    .prologue
    .line 2241
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabStrip:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->notifyPagerAdapterChanged()V

    .line 2242
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 0

    .prologue
    .line 1834
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 1835
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->attachIfNeeded()V

    .line 1836
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 1868
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->detachIfNeeded()V

    .line 1869
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 1870
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1359
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHasPullToRefresh:Z

    if-eqz v2, :cond_1

    .line 1360
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1361
    .local v0, "cloneEvent":Landroid/view/MotionEvent;
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v2, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 1362
    .local v1, "intercept":Z
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 1364
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayoutCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    invoke-virtual {v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->getAlpha()F

    move-result v2

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 1365
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayoutCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    invoke-virtual {v2, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setAlpha(F)V

    .line 1369
    .end local v0    # "cloneEvent":Landroid/view/MotionEvent;
    .end local v1    # "intercept":Z
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1312
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 1313
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setupViewPagerIfNeeded()V

    .line 1316
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateActiveListView()V

    .line 1317
    iget-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPendingSavedState:Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

    if-eqz v3, :cond_3

    move v0, v1

    .line 1318
    .local v0, "hadPendingState":Z
    :goto_0
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->restorePendingSavedStateIfNeeded()V

    .line 1319
    if-nez v0, :cond_1

    .line 1320
    if-eqz p1, :cond_0

    .line 1324
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->syncCurrentListViewOnNextScroll()V

    .line 1327
    :cond_0
    iget-boolean v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mUpdateContentPositionOnLayout:Z

    if-eqz v3, :cond_1

    .line 1331
    invoke-direct {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateContentPosition(Z)V

    .line 1332
    iput-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mUpdateContentPositionOnLayout:Z

    .line 1335
    :cond_1
    iget v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPendingListSync:I

    packed-switch v3, :pswitch_data_0

    .line 1349
    :goto_1
    if-eqz p1, :cond_2

    .line 1352
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateHeaderShadow()V

    .line 1354
    :cond_2
    return-void

    .end local v0    # "hadPendingState":Z
    :cond_3
    move v0, v2

    .line 1317
    goto :goto_0

    .line 1337
    .restart local v0    # "hadPendingState":Z
    :pswitch_0
    invoke-direct {p0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->syncListViews(Z)V

    goto :goto_1

    .line 1343
    :pswitch_1
    invoke-direct {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->syncListViews(Z)V

    goto :goto_1

    .line 1335
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onRefresh()V
    .locals 1

    .prologue
    .line 1404
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshProvider:Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;

    if-eqz v0, :cond_0

    .line 1405
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshProvider:Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;->onPulledToRefresh()V

    .line 1407
    :cond_0
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 1282
    instance-of v1, p1, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 1283
    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

    .line 1284
    .local v0, "savedState":Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;
    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1285
    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPendingSavedState:Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

    .line 1289
    .end local v0    # "savedState":Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;
    :goto_0
    return-void

    .line 1287
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1277
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;-><init>(Landroid/os/Parcelable;Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    return-object v0
.end method

.method onScroll(III)V
    .locals 5
    .param p1, "scrollState"    # I
    .param p2, "deltaY"    # I
    .param p3, "absoluteY"    # I

    .prologue
    const/4 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2470
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshProvider:Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;

    if-eqz v0, :cond_0

    .line 2472
    if-nez p3, :cond_3

    move v0, v1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setPullToRefreshEnabled(Z)V

    .line 2476
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSuppressIdleOnScroll:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_2

    :cond_1
    sget-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->USE_ANIMATIONS:Z

    if-nez v0, :cond_4

    .line 2520
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v2

    .line 2472
    goto :goto_0

    .line 2486
    :cond_4
    iput p3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAbsoluteY:I

    .line 2487
    packed-switch p1, :pswitch_data_0

    .line 2504
    :goto_2
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateFloatingState()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2507
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-eqz v0, :cond_5

    .line 2508
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getScrollingFloatingHeaderHeight()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_7

    .line 2509
    iput v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingFraction:F

    .line 2519
    :cond_5
    :goto_3
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->layout()V

    goto :goto_1

    .line 2489
    :pswitch_0
    iput-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mLastScrollWasDown:Z

    goto :goto_2

    .line 2492
    :pswitch_1
    int-to-float v0, p2

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_6

    :goto_4
    iput-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mLastScrollWasDown:Z

    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_4

    .line 2511
    :cond_7
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingFraction:F

    int-to-float v1, p2

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getScrollingFloatingHeaderHeight()F

    move-result v2

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingFraction:F

    .line 2512
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingFraction:F

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingFraction:F

    goto :goto_3

    .line 2487
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method onScrollStateChanged(I)V
    .locals 10
    .param p1, "newState"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2631
    iget-object v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSnapControlsUpIfNeededRunnable:Ljava/lang/Runnable;

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2632
    iget-object v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSnapControlsDownIfNeededRunnable:Ljava/lang/Runnable;

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2633
    if-nez p1, :cond_1

    .line 2637
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContentPosition()F

    move-result v6

    iget v7, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderBottomMargin:I

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-lez v6, :cond_2

    move v0, v4

    .line 2639
    .local v0, "mustSnapDown":Z
    :goto_0
    iget-boolean v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mLastScrollWasDown:Z

    if-eqz v6, :cond_4

    .line 2642
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarHeight()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x3f000000    # 0.5f

    mul-float v1, v6, v7

    .line 2643
    .local v1, "scrollUpThreshold":F
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getVisibleHeaderHeight()F

    move-result v3

    .line 2644
    .local v3, "visibleHeaderHeight":F
    if-nez v0, :cond_0

    cmpl-float v6, v3, v1

    if-ltz v6, :cond_3

    :cond_0
    move v2, v4

    .line 2652
    .end local v1    # "scrollUpThreshold":F
    .end local v3    # "visibleHeaderHeight":F
    .local v2, "snapDown":Z
    :goto_1
    iget-object v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_5

    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSnapControlsDownIfNeededRunnable:Ljava/lang/Runnable;

    :goto_2
    const-wide/16 v8, 0x32

    invoke-virtual {v6, v4, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2660
    invoke-direct {p0, v5}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->syncListViews(Z)V

    .line 2662
    .end local v0    # "mustSnapDown":Z
    .end local v2    # "snapDown":Z
    :cond_1
    return-void

    :cond_2
    move v0, v5

    .line 2637
    goto :goto_0

    .restart local v0    # "mustSnapDown":Z
    .restart local v1    # "scrollUpThreshold":F
    .restart local v3    # "visibleHeaderHeight":F
    :cond_3
    move v2, v5

    .line 2644
    goto :goto_1

    .line 2646
    .end local v1    # "scrollUpThreshold":F
    .end local v3    # "visibleHeaderHeight":F
    :cond_4
    move v2, v0

    .restart local v2    # "snapDown":Z
    goto :goto_1

    .line 2652
    :cond_5
    iget-object v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSnapControlsUpIfNeededRunnable:Ljava/lang/Runnable;

    goto :goto_2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 1374
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHasPullToRefresh:Z

    if-eqz v2, :cond_0

    .line 1375
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1376
    .local v0, "cloneEvent":Landroid/view/MotionEvent;
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v2, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 1377
    .local v1, "result":Z
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 1380
    .end local v0    # "cloneEvent":Landroid/view/MotionEvent;
    .end local v1    # "result":Z
    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method protected final setActionBarTitleAlpha(F)V
    .locals 1
    .param p1, "alpha"    # F

    .prologue
    .line 1804
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mActionBarTitleAlpha:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 1805
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mActionBarTitleAlpha:F

    .line 1806
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mToolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->applyActionBarTitleAlpha(Landroid/support/v7/widget/Toolbar;F)V

    .line 1808
    :cond_0
    return-void
.end method

.method protected final setBannerFraction(F)V
    .locals 1
    .param p1, "bannerFraction"    # F

    .prologue
    .line 3315
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setBannerFraction(FZ)V

    .line 3316
    return-void
.end method

.method public setBannerOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "onClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 1656
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1657
    return-void
.end method

.method public setBannerText(I)V
    .locals 1
    .param p1, "textResId"    # I

    .prologue
    .line 1623
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setBannerText(IZ)V

    .line 1624
    return-void
.end method

.method public setBannerText(IZ)V
    .locals 1
    .param p1, "textResId"    # I
    .param p2, "animate"    # Z

    .prologue
    .line 1612
    if-nez p1, :cond_0

    .line 1613
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setBannerText(Ljava/lang/CharSequence;)V

    .line 1617
    :goto_0
    return-void

    .line 1616
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setBannerText(Ljava/lang/CharSequence;Z)V

    goto :goto_0
.end method

.method public setBannerText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "bannerText"    # Ljava/lang/CharSequence;

    .prologue
    .line 1605
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setBannerText(Ljava/lang/CharSequence;Z)V

    .line 1606
    return-void
.end method

.method public setBannerText(Ljava/lang/CharSequence;Z)V
    .locals 2
    .param p1, "bannerText"    # Ljava/lang/CharSequence;
    .param p2, "animate"    # Z

    .prologue
    .line 1584
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTemporaryBannerTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1585
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTemporaryBannerGoneRunnable:Ljava/lang/Runnable;

    .line 1586
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerText:Ljava/lang/CharSequence;

    .line 1587
    if-nez p1, :cond_0

    .line 1590
    invoke-direct {p0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->hideBanner(Z)V

    .line 1595
    :goto_0
    return-void

    .line 1592
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mBannerTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1593
    invoke-direct {p0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->showBanner(Z)V

    goto :goto_0
.end method

.method public setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 1483
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;Z)V

    .line 1484
    return-void
.end method

.method public setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;Z)V
    .locals 1
    .param p1, "background"    # Landroid/graphics/drawable/Drawable;
    .param p2, "animate"    # Z

    .prologue
    .line 1495
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingControlsBackground:Landroid/graphics/drawable/Drawable;

    .line 1496
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsContainer:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mControlsAreFloating:Z

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAlwaysUseFloatingBackground:Z

    if-eqz v0, :cond_2

    .line 1498
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mFloatingControlsBackground:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setControlsBackground(Landroid/graphics/drawable/Drawable;Z)V

    .line 1500
    :cond_2
    return-void
.end method

.method protected setFloatingFraction(F)V
    .locals 1
    .param p1, "fraction"    # F

    .prologue
    .line 2786
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingFraction(FZ)V

    .line 2787
    return-void
.end method

.method public setHeaderShadowMode(I)V
    .locals 1
    .param p1, "headerShadowMode"    # I

    .prologue
    .line 1715
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderShadowMode:I

    if-eq v0, p1, :cond_0

    .line 1716
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderShadowMode:I

    .line 1717
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->layout()V

    .line 1719
    :cond_0
    return-void
.end method

.method public setOnLayoutChangedListener(Lcom/google/android/play/headerlist/PlayHeaderListLayout$LayoutListener;)V
    .locals 0
    .param p1, "layoutListener"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout$LayoutListener;

    .prologue
    .line 1565
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mLayoutListener:Lcom/google/android/play/headerlist/PlayHeaderListLayout$LayoutListener;

    .line 1566
    return-void
.end method

.method public setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .locals 0
    .param p1, "onPageChangeListener"    # Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .prologue
    .line 1533
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mExternalPageChangeListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 1534
    return-void
.end method

.method public setOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V
    .locals 0
    .param p1, "onScrollListener"    # Landroid/support/v7/widget/RecyclerView$OnScrollListener;

    .prologue
    .line 1549
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAppRecyclerViewOnScrollListener:Landroid/support/v7/widget/RecyclerView$OnScrollListener;

    .line 1550
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0
    .param p1, "onScrollListener"    # Landroid/widget/AbsListView$OnScrollListener;

    .prologue
    .line 1541
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAppListViewOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    .line 1542
    return-void
.end method

.method public setOnScrollListener(Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;)V
    .locals 0
    .param p1, "onScrollListener"    # Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    .prologue
    .line 1557
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAppContentViewOnScrollListener:Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;

    .line 1558
    return-void
.end method

.method public setOnTabSelectedListener(Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;

    .prologue
    .line 1573
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabStrip:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-virtual {v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->setOnTabSelectedListener(Lcom/google/android/play/headerlist/PlayHeaderListLayout$OnTabSelectedListener;)V

    .line 1574
    return-void
.end method

.method public setPullToRefreshProvider(Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;)V
    .locals 5
    .param p1, "pullToRefreshProvider"    # Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;

    .prologue
    const/4 v3, 0x0

    .line 1417
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-nez v2, :cond_0

    .line 1418
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Cannot initialize pull to refresh before HeaderListLayout has been configured"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1421
    :cond_0
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v2, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 1422
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayoutCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v2, v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setAlpha(F)V

    .line 1423
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayoutCompat:Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$AnimationCompat;->setTranslationY(F)V

    .line 1424
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshProvider:Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;

    .line 1427
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateActiveListView()V

    .line 1429
    sget v2, Lcom/google/android/play/R$id;->swipe_refresh_layout_parent:I

    invoke-virtual {p0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1430
    .local v1, "swipeRefreshParent":Landroid/view/View;
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshProvider:Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;

    if-eqz v2, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1432
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshProvider:Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;

    if-eqz v2, :cond_3

    .line 1434
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshProvider:Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;

    invoke-virtual {v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;->getOffsetMode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1446
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Illegal offset mode specified in PullToRefreshProvider.getOffsetMode()"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1430
    :cond_1
    const/16 v2, 0x8

    goto :goto_0

    .line 1436
    :pswitch_0
    const/4 v0, 0x0

    .line 1449
    .local v0, "computedTopMargin":I
    :goto_1
    invoke-virtual {v1, v3, v0, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 1450
    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mAbsoluteY:I

    if-nez v2, :cond_2

    const/4 v3, 0x1

    :cond_2
    invoke-direct {p0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setPullToRefreshEnabled(Z)V

    .line 1455
    .end local v0    # "computedTopMargin":I
    :goto_2
    return-void

    .line 1439
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarHeight()I

    move-result v0

    .line 1440
    .restart local v0    # "computedTopMargin":I
    goto :goto_1

    .line 1443
    .end local v0    # "computedTopMargin":I
    :pswitch_2
    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderHeight:I

    iget v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderBottomMargin:I

    sub-int/2addr v2, v4

    add-int/lit8 v0, v2, -0x1

    .line 1444
    .restart local v0    # "computedTopMargin":I
    goto :goto_1

    .line 1452
    .end local v0    # "computedTopMargin":I
    :cond_3
    invoke-direct {p0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setPullToRefreshEnabled(Z)V

    goto :goto_2

    .line 1434
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setRefreshing(IZ)V
    .locals 1
    .param p1, "pagerAdapterIndex"    # I
    .param p2, "isRefreshing"    # Z

    .prologue
    .line 1463
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPullToRefreshAdapterPage:I

    if-eq p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHasViewPager:Z

    if-nez v0, :cond_1

    .line 1464
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSwipeRefreshLayout:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, p2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 1466
    :cond_1
    return-void
.end method

.method public setSingleTabContentDescription(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "contentDescription"    # Ljava/lang/CharSequence;

    .prologue
    .line 1520
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabBarTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1521
    return-void
.end method

.method public setSingleTabTitle(I)V
    .locals 1
    .param p1, "titleResId"    # I

    .prologue
    .line 1513
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabBarTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 1514
    return-void
.end method

.method public setSingleTabTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 1506
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabBarTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1507
    return-void
.end method

.method public setTabMode(II)V
    .locals 3
    .param p1, "tabMode"    # I
    .param p2, "headerHeight"    # I

    .prologue
    .line 1728
    const/4 v0, 0x0

    .line 1729
    .local v0, "syncListViews":Z
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabMode:I

    if-eq v1, p1, :cond_0

    .line 1730
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabMode:I

    .line 1731
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateTabBarVisibility()V

    .line 1732
    const/4 v0, 0x1

    .line 1734
    :cond_0
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderHeight:I

    if-eq p2, v1, :cond_1

    .line 1735
    iput p2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderHeight:I

    .line 1736
    const/4 v0, 0x1

    .line 1737
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderHeight:I

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHeaderBottomMargin:I

    sub-int/2addr v1, v2

    invoke-direct {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setControlsContainerHeight(I)V

    .line 1738
    sget-boolean v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->SUPPORT_ELEVATION:Z

    if-nez v1, :cond_1

    .line 1739
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateHeaderShadowTopMargin()V

    .line 1742
    :cond_1
    if-eqz v0, :cond_2

    .line 1746
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->syncListViews(Z)V

    .line 1748
    :cond_2
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->updateTabPadding(Z)V

    .line 1749
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->layout()V

    .line 1750
    return-void
.end method

.method public setTemporaryBannerText(ILjava/lang/Runnable;)V
    .locals 4
    .param p1, "textResId"    # I
    .param p2, "bannerGoneCallback"    # Ljava/lang/Runnable;

    .prologue
    .line 1647
    invoke-virtual {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setBannerText(I)V

    .line 1648
    iput-object p2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTemporaryBannerGoneRunnable:Ljava/lang/Runnable;

    .line 1649
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTemporaryBannerTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1650
    return-void
.end method

.method public setTemporaryBannerText(Ljava/lang/CharSequence;Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "bannerText"    # Ljava/lang/CharSequence;
    .param p2, "bannerGoneCallback"    # Ljava/lang/Runnable;

    .prologue
    .line 1634
    invoke-virtual {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setBannerText(Ljava/lang/CharSequence;)V

    .line 1635
    iput-object p2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTemporaryBannerGoneRunnable:Ljava/lang/Runnable;

    .line 1636
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTemporaryBannerTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1637
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 1
    .param p1, "viewPager"    # Landroid/support/v4/view/ViewPager;

    .prologue
    .line 2233
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 2234
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mTabStrip:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-virtual {v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 2235
    return-void
.end method

.method public showActionBar(Z)V
    .locals 1
    .param p1, "animate"    # Z

    .prologue
    const/4 v0, 0x1

    .line 1680
    invoke-direct {p0, v0, v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->snapControlsIfNeeded(ZZZ)V

    .line 1681
    return-void
.end method

.method syncCurrentListViewOnNextScroll()V
    .locals 1

    .prologue
    .line 2313
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mPendingListSync:I

    .line 2314
    return-void
.end method

.method protected final tryFindHeaderSpacerView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "listView"    # Landroid/view/ViewGroup;

    .prologue
    .line 2253
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 2254
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2255
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    iget v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mSpacerId:I

    if-ne v2, v3, :cond_0

    .line 2267
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return-object v0

    .line 2253
    .restart local v0    # "child":Landroid/view/View;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2267
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public tryGetCollectionViewAbsoluteY(Landroid/view/ViewGroup;)I
    .locals 6
    .param p1, "listOrRecyclerView"    # Landroid/view/ViewGroup;

    .prologue
    .line 2277
    const/4 v0, -0x1

    .line 2278
    .local v0, "absoluteY":I
    instance-of v5, p1, Landroid/widget/ListView;

    if-eqz v5, :cond_1

    move-object v3, p1

    .line 2281
    check-cast v3, Landroid/widget/ListView;

    .line 2282
    .local v3, "listView":Landroid/widget/ListView;
    invoke-virtual {v3}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    .line 2283
    .local v1, "firstVisibleItem":I
    invoke-virtual {v3}, Landroid/widget/ListView;->getChildCount()I

    move-result v4

    .line 2284
    .local v4, "visibleItemCount":I
    if-nez v1, :cond_0

    if-lez v4, :cond_0

    .line 2285
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    neg-int v0, v5

    .line 2295
    .end local v1    # "firstVisibleItem":I
    .end local v3    # "listView":Landroid/widget/ListView;
    .end local v4    # "visibleItemCount":I
    :cond_0
    :goto_0
    return v0

    .line 2287
    :cond_1
    instance-of v5, p1, Landroid/support/v7/widget/RecyclerView;

    if-nez v5, :cond_2

    instance-of v5, p1, Lcom/google/android/play/headerlist/PlayScrollableContentView;

    if-eqz v5, :cond_0

    .line 2290
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->tryFindHeaderSpacerView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2291
    .local v2, "headerSpacer":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 2292
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v5

    neg-int v0, v5

    goto :goto_0
.end method

.method public tryGetContentTop(Landroid/view/ViewGroup;)I
    .locals 2
    .param p1, "listOrRecyclerView"    # Landroid/view/ViewGroup;

    .prologue
    .line 2303
    const/4 v0, -0x1

    .line 2304
    .local v0, "contentTop":I
    invoke-virtual {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->tryFindHeaderSpacerView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2305
    .local v1, "headerSpacer":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 2306
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 2308
    :cond_0
    return v0
.end method

.method public unlockHeader()V
    .locals 1

    .prologue
    .line 1700
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->mLockHeader:Z

    .line 1701
    return-void
.end method
