.class public Lcom/google/android/play/search/ArrowOrBurgerDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "ArrowOrBurgerDrawable.java"


# static fields
.field private static final HALF_SQRT_2:F


# instance fields
.field private mArrowness:F

.field private final mIsRtl:Z

.field private final mPaint:Landroid/graphics/Paint;

.field private mPreDrawScale:F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 33
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->HALF_SQRT_2:F

    return-void
.end method

.method public constructor <init>(IZ)V
    .locals 2
    .param p1, "color"    # I
    .param p2, "isRtl"    # Z

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 39
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mPaint:Landroid/graphics/Paint;

    .line 42
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mArrowness:F

    .line 46
    iget-object v0, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 47
    iget-object v0, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 48
    iget-object v0, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mPaint:Landroid/graphics/Paint;

    const v1, 0x3e926e98    # 0.286f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 49
    iget-object v0, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 50
    iput-boolean p2, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mIsRtl:Z

    .line 51
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 61
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 64
    invoke-virtual {p0}, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 65
    iget v0, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mPreDrawScale:F

    iget v1, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mPreDrawScale:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 67
    iget v0, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mArrowness:F

    sub-float v0, v4, v0

    const/high16 v1, 0x43340000    # 180.0f

    mul-float/2addr v1, v0

    iget-boolean v0, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mIsRtl:Z

    if-eqz v0, :cond_0

    const/16 v0, 0xb4

    :goto_0
    int-to-float v0, v0

    add-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 72
    const/high16 v0, 0x3f000000    # 0.5f

    iget v1, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mArrowness:F

    sub-float/2addr v0, v1

    const v1, 0x3e926e98    # 0.286f

    mul-float/2addr v0, v1

    add-float v3, v4, v0

    .line 73
    .local v3, "burger_length":F
    neg-float v1, v3

    iget-object v5, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 76
    const/high16 v0, 0x42340000    # 45.0f

    iget v1, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mArrowness:F

    mul-float v10, v0, v1

    .line 78
    .local v10, "rotation":F
    iget v0, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mArrowness:F

    sget v1, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->HALF_SQRT_2:F

    sub-float/2addr v1, v3

    mul-float/2addr v0, v1

    add-float v7, v3, v0

    .line 80
    .local v7, "length":F
    invoke-virtual {p1, v10}, Landroid/graphics/Canvas;->rotate(F)V

    .line 81
    neg-float v5, v7

    sget v6, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->HALF_SQRT_2:F

    sget v8, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->HALF_SQRT_2:F

    iget-object v9, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mPaint:Landroid/graphics/Paint;

    move-object v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 82
    const/high16 v0, -0x40000000    # -2.0f

    mul-float/2addr v0, v10

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 83
    neg-float v5, v7

    sget v0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->HALF_SQRT_2:F

    neg-float v6, v0

    sget v0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->HALF_SQRT_2:F

    neg-float v8, v0

    iget-object v9, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mPaint:Landroid/graphics/Paint;

    move-object v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 85
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 86
    return-void

    .line 67
    .end local v3    # "burger_length":F
    .end local v7    # "length":F
    .end local v10    # "rotation":F
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 114
    const/4 v0, -0x2

    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "bounds"    # Landroid/graphics/Rect;

    .prologue
    .line 55
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    const v1, 0x40249ba6    # 2.572f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mPreDrawScale:F

    .line 56
    iget-object v0, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mPaint:Landroid/graphics/Paint;

    const v1, 0x3e926e98    # 0.286f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 57
    return-void
.end method

.method public setAlpha(I)V
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 105
    return-void
.end method

.method public setAsBurger(Z)V
    .locals 1
    .param p1, "burger"    # Z

    .prologue
    .line 99
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->setHowArrowIsTheBurger(F)V

    .line 100
    return-void

    .line 99
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1
    .param p1, "cf"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 110
    return-void
.end method

.method public setHowArrowIsTheBurger(F)V
    .locals 0
    .param p1, "howMuch"    # F

    .prologue
    .line 93
    iput p1, p0, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->mArrowness:F

    .line 94
    invoke-virtual {p0}, Lcom/google/android/play/search/ArrowOrBurgerDrawable;->invalidateSelf()V

    .line 95
    return-void
.end method
