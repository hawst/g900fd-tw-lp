.class public final Lcom/google/android/play/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final account_info_container:I = 0x7f0f01b5

.field public static final account_name:I = 0x7f0f01ae

.field public static final action_bar:I = 0x7f0f00b2

.field public static final action_bar_container:I = 0x7f0f00b1

.field public static final action_button:I = 0x7f0f01dc

.field public static final action_text:I = 0x7f0f01b0

.field public static final alt_play_background:I = 0x7f0f01b9

.field public static final avatar:I = 0x7f0f01ad

.field public static final background_container:I = 0x7f0f01b8

.field public static final caption:I = 0x7f0f01c9

.field public static final content_container:I = 0x7f0f01ba

.field public static final controls_container:I = 0x7f0f015c

.field public static final cover_photo:I = 0x7f0f01b2

.field public static final display_name:I = 0x7f0f01b6

.field public static final end_button:I = 0x7f0f01cc

.field public static final expando_hero:I = 0x7f0f0026

.field public static final expando_logo:I = 0x7f0f0028

.field public static final expando_splash:I = 0x7f0f0027

.field public static final flm_paddingEnd:I = 0x7f0f0024

.field public static final flm_paddingStart:I = 0x7f0f0023

.field public static final flm_width:I = 0x7f0f0025

.field public static final header_shadow:I = 0x7f0f01c0

.field public static final hero_container:I = 0x7f0f01bb

.field public static final icon:I = 0x7f0f008a

.field public static final li_badge:I = 0x7f0f01aa

.field public static final li_description:I = 0x7f0f01a7

.field public static final li_label:I = 0x7f0f01a3

.field public static final li_overflow:I = 0x7f0f019e

.field public static final li_rating:I = 0x7f0f01a2

.field public static final li_snippet_1:I = 0x7f0f01a6

.field public static final li_snippet_2:I = 0x7f0f01a5

.field public static final li_snippet_avatar:I = 0x7f0f01ab

.field public static final li_snippet_text:I = 0x7f0f01ac

.field public static final li_subtitle:I = 0x7f0f01a4

.field public static final li_thumbnail:I = 0x7f0f01a0

.field public static final li_thumbnail_frame:I = 0x7f0f019f

.field public static final li_title:I = 0x7f0f01a1

.field public static final loading_progress_bar:I = 0x7f0f01a8

.field public static final navigation_button:I = 0x7f0f01d8

.field public static final page_indicator:I = 0x7f0f01cb

.field public static final pager_tab_strip:I = 0x7f0f00c3

.field public static final play_drawer_list:I = 0x7f0f01af

.field public static final play_header_banner:I = 0x7f0f01c2

.field public static final play_header_list_tab_container:I = 0x7f0f01be

.field public static final play_header_list_tab_scroll:I = 0x7f0f01bd

.field public static final play_header_listview:I = 0x7f0f0029

.field public static final play_header_spacer:I = 0x7f0f002b

.field public static final play_header_toolbar:I = 0x7f0f01c1

.field public static final play_header_viewpager:I = 0x7f0f002a

.field public static final play_onboard__OnboardPage_pageId:I = 0x7f0f002d

.field public static final play_onboard__OnboardPage_pageInfo:I = 0x7f0f002e

.field public static final play_onboard__OnboardPagerAdapter_pageGenerator:I = 0x7f0f002c

.field public static final play_onboard__OnboardSimpleQuizPage_title:I = 0x7f0f0033

.field public static final play_onboard__OnboardTutorialPage_backgroundColor:I = 0x7f0f002f

.field public static final play_onboard__OnboardTutorialPage_bodyText:I = 0x7f0f0031

.field public static final play_onboard__OnboardTutorialPage_iconDrawableId:I = 0x7f0f0032

.field public static final play_onboard__OnboardTutorialPage_titleText:I = 0x7f0f0030

.field public static final play_onboard_background:I = 0x7f0f0034

.field public static final play_onboard_drops:I = 0x7f0f01c8

.field public static final play_onboard_footer:I = 0x7f0f01c7

.field public static final play_onboard_pager:I = 0x7f0f01c6

.field public static final play_search_plate:I = 0x7f0f01d3

.field public static final play_search_suggestions_list:I = 0x7f0f01d4

.field public static final rating_badge_container:I = 0x7f0f01a9

.field public static final scroll_proxy:I = 0x7f0f01c5

.field public static final search_box_idle_text:I = 0x7f0f01da

.field public static final search_box_text_input:I = 0x7f0f01db

.field public static final secondary_avatar_left:I = 0x7f0f01b3

.field public static final secondary_avatar_right:I = 0x7f0f01b4

.field public static final shortcut:I = 0x7f0f00ab

.field public static final splash:I = 0x7f0f01ce

.field public static final start_button:I = 0x7f0f01ca

.field public static final suggest_text:I = 0x7f0f01d6

.field public static final suggestion_divider:I = 0x7f0f01d7

.field public static final suggestion_list_recycler_view:I = 0x7f0f01df

.field public static final swipe_refresh_layout:I = 0x7f0f01c4

.field public static final swipe_refresh_layout_parent:I = 0x7f0f01c3

.field public static final switch_button:I = 0x7f0f01b1

.field public static final tab_bar:I = 0x7f0f01bc

.field public static final tab_bar_title:I = 0x7f0f01bf

.field public static final text_container:I = 0x7f0f01d9

.field public static final title:I = 0x7f0f00a8

.field public static final toggle_account_list_button:I = 0x7f0f01b7
