.class public abstract Lcom/google/android/play/onboard/OnboardHostFragment;
.super Lcom/google/android/play/onboard/OnboardBaseFragment;
.source "OnboardHostFragment.java"

# interfaces
.implements Lcom/google/android/play/onboard/OnboardHostControl;


# static fields
.field private static final LOGD:Lcom/google/android/libraries/bind/logging/Logd;

.field protected static final STATE_ONBOARD_BUNDLE:Ljava/lang/String;

.field private static final STATE_PREFIX:Ljava/lang/String;

.field protected static final STATE_SELECTED_PAGE_ID:Ljava/lang/String;

.field protected static final STATE_SHOWED_SPLASH:Ljava/lang/String;

.field protected static final STATE_SHOWING_INTERSTITIAL_OVERLAY:Ljava/lang/String;

.field protected static final STATE_SHOWING_LOADING_OVERLAY:Ljava/lang/String;


# instance fields
.field protected mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

.field protected mDrops:Landroid/widget/ImageView;

.field protected mLastSelectedPageId:Ljava/lang/String;

.field protected mNavFooter:Lcom/google/android/play/onboard/OnboardNavFooter;

.field protected mOnboardBundle:Landroid/os/Bundle;

.field protected mPager:Lcom/google/android/play/onboard/OnboardPager;

.field protected mRootView:Landroid/widget/FrameLayout;

.field protected mShowedSplash:Z

.field protected mSplash:Landroid/view/View;

.field private final mUpdateControlsRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 105
    const-class v0, Lcom/google/android/play/onboard/OnboardHostFragment;

    invoke-static {v0}, Lcom/google/android/libraries/bind/logging/Logd;->get(Ljava/lang/Class;)Lcom/google/android/libraries/bind/logging/Logd;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/onboard/OnboardHostFragment;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/play/onboard/OnboardHostFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/onboard/OnboardHostFragment;->STATE_PREFIX:Ljava/lang/String;

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/play/onboard/OnboardHostFragment;->STATE_PREFIX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "onboardBundle"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/onboard/OnboardHostFragment;->STATE_ONBOARD_BUNDLE:Ljava/lang/String;

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/play/onboard/OnboardHostFragment;->STATE_PREFIX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "showedSplash"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/onboard/OnboardHostFragment;->STATE_SHOWED_SPLASH:Ljava/lang/String;

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/play/onboard/OnboardHostFragment;->STATE_PREFIX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "selectedPageId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/onboard/OnboardHostFragment;->STATE_SELECTED_PAGE_ID:Ljava/lang/String;

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/play/onboard/OnboardHostFragment;->STATE_PREFIX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "showingLoadingOverlay"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/onboard/OnboardHostFragment;->STATE_SHOWING_LOADING_OVERLAY:Ljava/lang/String;

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/play/onboard/OnboardHostFragment;->STATE_PREFIX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "showingInterstitialOverlay"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/onboard/OnboardHostFragment;->STATE_SHOWING_INTERSTITIAL_OVERLAY:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .locals 3

    .prologue
    .line 149
    sget v0, Lcom/google/android/play/R$layout;->play_onboard_host_fragment:I

    invoke-direct {p0, v0}, Lcom/google/android/play/onboard/OnboardBaseFragment;-><init>(I)V

    .line 140
    new-instance v0, Lcom/google/android/libraries/bind/async/DelayedRunnable;

    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/play/onboard/OnboardHostFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/play/onboard/OnboardHostFragment$1;-><init>(Lcom/google/android/play/onboard/OnboardHostFragment;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/bind/async/DelayedRunnable;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mUpdateControlsRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    .line 150
    return-void
.end method

.method static synthetic access$000()Lcom/google/android/libraries/bind/logging/Logd;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/google/android/play/onboard/OnboardHostFragment;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    return-object v0
.end method

.method private replaceViewByIdAtIndex(Landroid/view/View;II)V
    .locals 5
    .param p1, "newView"    # Landroid/view/View;
    .param p2, "id"    # I
    .param p3, "optIndex"    # I

    .prologue
    const/4 v4, -0x1

    .line 587
    iget-object v2, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mRootView:Landroid/widget/FrameLayout;

    invoke-virtual {v2, p2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 588
    .local v1, "oldView":Landroid/view/View;
    if-eq v1, p1, :cond_1

    .line 589
    if-eqz v1, :cond_0

    .line 590
    iget-object v2, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mRootView:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 592
    :cond_0
    if-eqz p1, :cond_1

    .line 593
    invoke-virtual {p1, p2}, Landroid/view/View;->setId(I)V

    .line 594
    if-ltz p3, :cond_2

    move v0, p3

    .line 595
    .local v0, "index":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mRootView:Landroid/widget/FrameLayout;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, p1, v0, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 600
    .end local v0    # "index":I
    :cond_1
    return-void

    .line 594
    :cond_2
    iget-object v2, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mRootView:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method protected adjustPagePaddingToFitNavFooter(Landroid/view/View;Z)V
    .locals 4
    .param p1, "pageView"    # Landroid/view/View;
    .param p2, "showingFooter"    # Z

    .prologue
    .line 276
    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/play/R$dimen;->play_onboard__onboard_nav_footer_height:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 280
    .local v0, "paddingBottomPx":I
    :goto_0
    invoke-static {p1}, Landroid/support/v4/view/ViewCompat;->getPaddingStart(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-static {p1}, Landroid/support/v4/view/ViewCompat;->getPaddingEnd(Landroid/view/View;)I

    move-result v3

    invoke-static {p1, v1, v2, v3, v0}, Landroid/support/v4/view/ViewCompat;->setPaddingRelative(Landroid/view/View;IIII)V

    .line 282
    return-void

    .line 276
    .end local v0    # "paddingBottomPx":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public finishOnboardFlow()V
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mOnboardBundle:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardHostFragment;->saveOnboardState(Landroid/os/Bundle;)V

    .line 517
    return-void
.end method

.method public getAppColor()I
    .locals 2

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$color;->play_onboard_app_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public getCurrentPageId()Ljava/lang/String;
    .locals 5

    .prologue
    .line 421
    const/4 v2, 0x0

    .line 422
    .local v2, "id":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getCurrentPageIndex()I

    move-result v3

    .line 423
    .local v3, "index":I
    iget-object v4, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    if-eqz v4, :cond_0

    .line 424
    iget-object v4, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    invoke-virtual {v4}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->getList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    .line 425
    .local v1, "dataList":Lcom/google/android/libraries/bind/data/DataList;
    if-eqz v1, :cond_0

    .line 426
    invoke-virtual {v1, v3}, Lcom/google/android/libraries/bind/data/DataList;->getData(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 427
    .local v0, "data":Lcom/google/android/libraries/bind/data/Data;
    if-eqz v0, :cond_0

    .line 428
    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/DataList;->primaryKey()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/bind/data/Data;->getAsString(I)Ljava/lang/String;

    move-result-object v2

    .line 432
    .end local v0    # "data":Lcom/google/android/libraries/bind/data/Data;
    .end local v1    # "dataList":Lcom/google/android/libraries/bind/data/DataList;
    :cond_0
    return-object v2
.end method

.method public getCurrentPageIndex()I
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mPager:Lcom/google/android/play/onboard/OnboardPager;

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mPager:Lcom/google/android/play/onboard/OnboardPager;

    invoke-virtual {v0}, Lcom/google/android/play/onboard/OnboardPager;->getCurrentLogicalItem()I

    move-result v0

    .line 443
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected getDropAnimationDurationMs()J
    .locals 2

    .prologue
    .line 684
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$integer;->play_onboard__drop_duration_ms:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method protected getDropCount()I
    .locals 2

    .prologue
    .line 691
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$integer;->play_onboard__drop_count:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method public getInitialPageId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 296
    .local v0, "args":Landroid/os/Bundle;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    const-string v1, "arg_initialPageId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public abstract getPageList()Lcom/google/android/libraries/bind/data/DataList;
.end method

.method protected getSplashDurationMs()J
    .locals 2

    .prologue
    .line 677
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$integer;->play_onboard__splash_duration_ms:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method protected getViewHeap()Lcom/google/android/libraries/bind/view/ViewHeap;
    .locals 2

    .prologue
    .line 363
    new-instance v0, Lcom/google/android/libraries/bind/view/ViewHeap;

    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/bind/view/ViewHeap;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected goToInitialOrRestoredPage()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 304
    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mOnboardBundle:Landroid/os/Bundle;

    sget-object v2, Lcom/google/android/play/onboard/OnboardHostFragment;->STATE_SELECTED_PAGE_ID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 305
    .local v0, "pageId":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 306
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getInitialPageId()Ljava/lang/String;

    move-result-object v0

    .line 308
    :cond_0
    if-eqz v0, :cond_1

    .line 309
    invoke-virtual {p0, v0, v3}, Lcom/google/android/play/onboard/OnboardHostFragment;->goToPage(Ljava/lang/String;Z)V

    .line 315
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getCurrentPageIndex()I

    move-result v1

    invoke-virtual {p0, v1, v3}, Lcom/google/android/play/onboard/OnboardHostFragment;->onPageSelected(IZ)V

    .line 316
    return-void

    .line 311
    :cond_1
    invoke-virtual {p0, v3, v3}, Lcom/google/android/play/onboard/OnboardHostFragment;->goToPage(IZ)V

    goto :goto_0
.end method

.method public goToNextPage()V
    .locals 1

    .prologue
    .line 469
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardHostFragment;->incrementPage(I)V

    .line 470
    return-void
.end method

.method public goToPage(IZ)V
    .locals 2
    .param p1, "logicalPosition"    # I
    .param p2, "smoothScroll"    # Z

    .prologue
    .line 495
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mPager:Lcom/google/android/play/onboard/OnboardPager;

    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    invoke-static {v1, p1}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getVisualPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/play/onboard/OnboardPager;->setCurrentItem(IZ)V

    .line 497
    return-void
.end method

.method public goToPage(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "pageId"    # Ljava/lang/String;
    .param p2, "smoothScroll"    # Z

    .prologue
    .line 504
    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->getLogicalItemPosition(Ljava/lang/Object;)I

    move-result v0

    .line 505
    .local v0, "logicalPosition":I
    if-ltz v0, :cond_0

    .line 506
    invoke-virtual {p0, v0, p2}, Lcom/google/android/play/onboard/OnboardHostFragment;->goToPage(IZ)V

    .line 508
    :cond_0
    return-void
.end method

.method public goToPreviousPage()V
    .locals 1

    .prologue
    .line 477
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardHostFragment;->incrementPage(I)V

    .line 478
    return-void
.end method

.method protected incrementPage(I)V
    .locals 2
    .param p1, "increment"    # I

    .prologue
    .line 484
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getCurrentPageIndex()I

    move-result v1

    add-int v0, v1, p1

    .line 485
    .local v0, "targetIndex":I
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    invoke-virtual {v1}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 486
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/play/onboard/OnboardHostFragment;->goToPage(IZ)V

    .line 488
    :cond_0
    return-void
.end method

.method public final invalidateControls()V
    .locals 4

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mUpdateControlsRunnable:Lcom/google/android/libraries/bind/async/DelayedRunnable;

    const-wide/16 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/libraries/bind/async/DelayedRunnable;->postDelayed(JI)Z

    .line 549
    return-void
.end method

.method protected onPageScrolled(IFI)V
    .locals 0
    .param p1, "visualPosition"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 415
    return-void
.end method

.method protected onPageSelected(IZ)V
    .locals 8
    .param p1, "logicalPosition"    # I
    .param p2, "byUser"    # Z

    .prologue
    .line 379
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getPageList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/google/android/libraries/bind/data/DataList;->getItemId(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 380
    .local v4, "selectedPageId":Ljava/lang/String;
    const/4 v3, 0x0

    .line 381
    .local v3, "selectedPage":Lcom/google/android/play/onboard/OnboardPage;
    iget-object v6, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    invoke-virtual {v6, p1}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->tryGetViewAt(I)Landroid/view/View;

    move-result-object v5

    .line 382
    .local v5, "selectedView":Landroid/view/View;
    instance-of v6, v5, Lcom/google/android/play/onboard/OnboardPage;

    if-eqz v6, :cond_0

    move-object v3, v5

    .line 383
    check-cast v3, Lcom/google/android/play/onboard/OnboardPage;

    .line 386
    :cond_0
    iget-object v6, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mLastSelectedPageId:Ljava/lang/String;

    invoke-static {v4, v6}, Lcom/google/android/libraries/bind/util/Util;->objectsEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 387
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getPageList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mLastSelectedPageId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/libraries/bind/data/DataList;->findPositionForId(Ljava/lang/Object;)I

    move-result v1

    .line 389
    .local v1, "lastSelectedPosition":I
    const/4 v6, -0x1

    if-eq v1, v6, :cond_1

    if-le p1, v1, :cond_4

    :cond_1
    const/4 v2, 0x1

    .line 391
    .local v2, "movingTowardsEnd":Z
    :goto_0
    iget-object v6, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    invoke-virtual {v6, v1}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->tryGetViewAt(I)Landroid/view/View;

    move-result-object v0

    .line 392
    .local v0, "lastSelectedPageView":Landroid/view/View;
    instance-of v6, v0, Lcom/google/android/play/onboard/OnboardPage;

    if-eqz v6, :cond_2

    .line 393
    check-cast v0, Lcom/google/android/play/onboard/OnboardPage;

    .end local v0    # "lastSelectedPageView":Landroid/view/View;
    invoke-interface {v0, v2}, Lcom/google/android/play/onboard/OnboardPage;->onExitPage(Z)V

    .line 395
    :cond_2
    iput-object v4, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mLastSelectedPageId:Ljava/lang/String;

    .line 396
    if-eqz v3, :cond_3

    .line 397
    invoke-interface {v3, v2}, Lcom/google/android/play/onboard/OnboardPage;->onEnterPage(Z)V

    .line 401
    .end local v1    # "lastSelectedPosition":I
    .end local v2    # "movingTowardsEnd":Z
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->invalidateControls()V

    .line 402
    return-void

    .line 389
    .restart local v1    # "lastSelectedPosition":I
    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 182
    invoke-super {p0}, Lcom/google/android/play/onboard/OnboardBaseFragment;->onResume()V

    .line 183
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->startOrHideSplash()V

    .line 184
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 188
    invoke-super {p0, p1}, Lcom/google/android/play/onboard/OnboardBaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 189
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mOnboardBundle:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardHostFragment;->saveOnboardState(Landroid/os/Bundle;)V

    .line 190
    sget-object v0, Lcom/google/android/play/onboard/OnboardHostFragment;->STATE_ONBOARD_BUNDLE:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mOnboardBundle:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 191
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 154
    invoke-super {p0, p1, p2}, Lcom/google/android/play/onboard/OnboardBaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    move-object v0, p1

    .line 155
    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mRootView:Landroid/widget/FrameLayout;

    .line 156
    sget v0, Lcom/google/android/play/R$id;->play_onboard_pager:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/onboard/OnboardPager;

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mPager:Lcom/google/android/play/onboard/OnboardPager;

    .line 157
    sget v0, Lcom/google/android/play/R$id;->play_onboard_footer:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/onboard/OnboardNavFooter;

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mNavFooter:Lcom/google/android/play/onboard/OnboardNavFooter;

    .line 159
    sget v0, Lcom/google/android/play/R$id;->splash:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mSplash:Landroid/view/View;

    .line 160
    sget v0, Lcom/google/android/play/R$id;->play_onboard_drops:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mDrops:Landroid/widget/ImageView;

    .line 162
    if-eqz p2, :cond_0

    .line 163
    sget-object v0, Lcom/google/android/play/onboard/OnboardHostFragment;->STATE_ONBOARD_BUNDLE:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mOnboardBundle:Landroid/os/Bundle;

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mOnboardBundle:Landroid/os/Bundle;

    if-nez v0, :cond_1

    .line 166
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mOnboardBundle:Landroid/os/Bundle;

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mOnboardBundle:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardHostFragment;->restoreOnboardState(Landroid/os/Bundle;)V

    .line 170
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->setUpPager()V

    .line 172
    new-instance v0, Lcom/google/android/play/onboard/OnboardHostFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/play/onboard/OnboardHostFragment$2;-><init>(Lcom/google/android/play/onboard/OnboardHostFragment;)V

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardHostFragment;->safelyPost(Ljava/lang/Runnable;)V

    .line 178
    return-void
.end method

.method protected restoreOnboardState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 221
    sget-object v0, Lcom/google/android/play/onboard/OnboardHostFragment;->STATE_SHOWED_SPLASH:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mShowedSplash:Z

    .line 222
    return-void
.end method

.method protected saveOnboardState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 197
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mPager:Lcom/google/android/play/onboard/OnboardPager;

    invoke-virtual {v2}, Lcom/google/android/play/onboard/OnboardPager;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 198
    iget-object v2, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mPager:Lcom/google/android/play/onboard/OnboardPager;

    invoke-virtual {v2, v1}, Lcom/google/android/play/onboard/OnboardPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 199
    .local v0, "child":Landroid/view/View;
    instance-of v2, v0, Lcom/google/android/play/onboard/OnboardPage;

    if-eqz v2, :cond_0

    .line 200
    check-cast v0, Lcom/google/android/play/onboard/OnboardPage;

    .end local v0    # "child":Landroid/view/View;
    invoke-interface {v0, p1}, Lcom/google/android/play/onboard/OnboardPage;->saveOnboardState(Landroid/os/Bundle;)V

    .line 197
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 203
    :cond_1
    sget-object v2, Lcom/google/android/play/onboard/OnboardHostFragment;->STATE_SELECTED_PAGE_ID:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getCurrentPageId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    sget-object v2, Lcom/google/android/play/onboard/OnboardHostFragment;->STATE_SHOWED_SPLASH:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mShowedSplash:Z

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 205
    return-void
.end method

.method public setBackgroundView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 607
    sget v0, Lcom/google/android/play/R$id;->play_onboard_background:I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/play/onboard/OnboardHostFragment;->replaceViewByIdAtIndex(Landroid/view/View;II)V

    .line 608
    return-void
.end method

.method protected setUpPager()V
    .locals 3

    .prologue
    .line 226
    new-instance v0, Lcom/google/android/play/onboard/OnboardHostFragment$3;

    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getViewHeap()Lcom/google/android/libraries/bind/view/ViewHeap;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/play/onboard/OnboardHostFragment$3;-><init>(Lcom/google/android/play/onboard/OnboardHostFragment;Lcom/google/android/libraries/bind/view/ViewHeap;)V

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    .line 254
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getPageList()Lcom/google/android/libraries/bind/data/DataList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->setList(Lcom/google/android/libraries/bind/data/DataList;)V

    .line 256
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mPager:Lcom/google/android/play/onboard/OnboardPager;

    new-instance v1, Lcom/google/android/play/onboard/OnboardHostFragment$4;

    iget-object v2, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mPager:Lcom/google/android/play/onboard/OnboardPager;

    invoke-direct {v1, p0, v2}, Lcom/google/android/play/onboard/OnboardHostFragment$4;-><init>(Lcom/google/android/play/onboard/OnboardHostFragment;Lcom/google/android/play/widget/UserAwareViewPager;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 272
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mPager:Lcom/google/android/play/onboard/OnboardPager;

    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 273
    return-void
.end method

.method protected startOrHideSplash()V
    .locals 6

    .prologue
    const/16 v2, 0x8

    .line 700
    iget-boolean v1, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mShowedSplash:Z

    if-eqz v1, :cond_0

    .line 701
    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mSplash:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 702
    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mDrops:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 745
    :goto_0
    return-void

    .line 706
    :cond_0
    new-instance v0, Lcom/google/android/play/widget/RaindropMaskDrawable;

    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getAppColor()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getDropCount()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getDropAnimationDurationMs()J

    move-result-wide v4

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/google/android/play/widget/RaindropMaskDrawable;-><init>(IIJ)V

    .line 708
    .local v0, "dropsDrawable":Lcom/google/android/play/widget/RaindropMaskDrawable;
    new-instance v1, Lcom/google/android/play/onboard/OnboardHostFragment$6;

    invoke-direct {v1, p0}, Lcom/google/android/play/onboard/OnboardHostFragment$6;-><init>(Lcom/google/android/play/onboard/OnboardHostFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/widget/RaindropMaskDrawable;->setStoppedRunnable(Ljava/lang/Runnable;)V

    .line 714
    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mDrops:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 716
    sget-object v1, Lcom/google/android/play/onboard/OnboardHostFragment;->LOGD:Lcom/google/android/libraries/bind/logging/Logd;

    const-string v2, "Started showing splash"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/logging/Logd;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 717
    new-instance v1, Lcom/google/android/play/onboard/OnboardHostFragment$7;

    invoke-direct {v1, p0, v0}, Lcom/google/android/play/onboard/OnboardHostFragment$7;-><init>(Lcom/google/android/play/onboard/OnboardHostFragment;Lcom/google/android/play/widget/RaindropMaskDrawable;)V

    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getSplashDurationMs()J

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/play/onboard/OnboardHostFragment;->safelyPostDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method

.method protected updateControls()V
    .locals 3

    .prologue
    .line 559
    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mAdapter:Lcom/google/android/play/onboard/OnboardPagerAdapter;

    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardHostFragment;->getCurrentPageIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->getPageInfo(I)Lcom/google/android/play/onboard/OnboardPageInfo;

    move-result-object v0

    .line 560
    .local v0, "currentPageInfo":Lcom/google/android/play/onboard/OnboardPageInfo;
    if-eqz v0, :cond_0

    .line 561
    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardHostFragment;->updateUiForPage(Lcom/google/android/play/onboard/OnboardPageInfo;)V

    .line 564
    :cond_0
    return-void
.end method

.method protected updateUiForPage(Lcom/google/android/play/onboard/OnboardPageInfo;)V
    .locals 2
    .param p1, "pageInfo"    # Lcom/google/android/play/onboard/OnboardPageInfo;

    .prologue
    .line 572
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mNavFooter:Lcom/google/android/play/onboard/OnboardNavFooter;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/play/onboard/OnboardNavFooter;->updatePageInfo(Lcom/google/android/play/onboard/OnboardHostControl;Lcom/google/android/play/onboard/OnboardPageInfo;)V

    .line 573
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mPager:Lcom/google/android/play/onboard/OnboardPager;

    invoke-interface {p1, p0}, Lcom/google/android/play/onboard/OnboardPageInfo;->allowSwipeToNext(Lcom/google/android/play/onboard/OnboardHostControl;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardPager;->setAllowSwipeToNext(Z)V

    .line 574
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment;->mPager:Lcom/google/android/play/onboard/OnboardPager;

    invoke-interface {p1, p0}, Lcom/google/android/play/onboard/OnboardPageInfo;->allowSwipeToPrevious(Lcom/google/android/play/onboard/OnboardHostControl;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardPager;->setAllowSwipeToPrevious(Z)V

    .line 575
    return-void
.end method
