.class public Lcom/google/android/play/layout/PlayRatingBadgeContainer;
.super Landroid/widget/FrameLayout;
.source "PlayRatingBadgeContainer.java"


# instance fields
.field private mBadge:Landroid/view/View;

.field private mRatingBar:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayRatingBadgeContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method


# virtual methods
.method public getBaseline()I
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 42
    iget-object v0, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mBadge:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_0

    .line 43
    iget-object v0, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mBadge:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBaseline()I

    move-result v0

    .line 48
    :goto_0
    return v0

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mRatingBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 46
    iget-object v0, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mRatingBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBaseline()I

    move-result v0

    goto :goto_0

    .line 48
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->getMeasuredHeight()I

    move-result v0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 36
    sget v0, Lcom/google/android/play/R$id;->li_rating:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mRatingBar:Landroid/view/View;

    .line 37
    sget v0, Lcom/google/android/play/R$id;->li_badge:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mBadge:Landroid/view/View;

    .line 38
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 84
    iget-object v0, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mRatingBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v4, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mRatingBar:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mRatingBar:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mRatingBar:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mBadge:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v4, :cond_1

    .line 88
    iget-object v0, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mBadge:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mBadge:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mBadge:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 90
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/16 v5, 0x8

    const/4 v6, 0x0

    .line 53
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 55
    .local v0, "availableWidth":I
    const/4 v2, 0x0

    .line 56
    .local v2, "width":I
    const/4 v1, 0x0

    .line 58
    .local v1, "height":I
    iget-object v4, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mRatingBar:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eq v4, v5, :cond_1

    .line 60
    iget-object v4, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mRatingBar:Landroid/view/View;

    invoke-virtual {v4, v6, v6}, Landroid/view/View;->measure(II)V

    .line 61
    iget-object v4, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mRatingBar:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    if-le v4, v0, :cond_0

    .line 64
    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v6, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 65
    .local v3, "zeroSpec":I
    iget-object v4, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mRatingBar:Landroid/view/View;

    invoke-virtual {v4, v3, v3}, Landroid/view/View;->measure(II)V

    .line 68
    .end local v3    # "zeroSpec":I
    :cond_0
    iget-object v4, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mRatingBar:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 69
    iget-object v4, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mRatingBar:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 72
    :cond_1
    iget-object v4, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mBadge:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eq v4, v5, :cond_2

    .line 73
    iget-object v4, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mBadge:Landroid/view/View;

    const/high16 v5, -0x80000000

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v4, v5, v6}, Landroid/view/View;->measure(II)V

    .line 75
    iget-object v4, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mBadge:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 76
    iget-object v4, p0, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->mBadge:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 79
    :cond_2
    invoke-virtual {p0, v2, v1}, Lcom/google/android/play/layout/PlayRatingBadgeContainer;->setMeasuredDimension(II)V

    .line 80
    return-void
.end method
