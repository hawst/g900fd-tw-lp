.class public final Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ClientAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/ClientAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppUsage1pLogEvent"
.end annotation


# instance fields
.field public androidPackageName:Ljava/lang/String;

.field public appType:I

.field public version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1756
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1757
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->clear()Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;

    .line 1758
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;
    .locals 1

    .prologue
    .line 1761
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->appType:I

    .line 1762
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->androidPackageName:Ljava/lang/String;

    .line 1763
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->version:Ljava/lang/String;

    .line 1764
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->cachedSize:I

    .line 1765
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1785
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1786
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->appType:I

    if-eqz v1, :cond_0

    .line 1787
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->appType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1790
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->androidPackageName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1791
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->androidPackageName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1794
    :cond_1
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->version:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1795
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->version:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1798
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1806
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1807
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1811
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1812
    :sswitch_0
    return-object p0

    .line 1817
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1818
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1843
    :pswitch_0
    iput v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->appType:I

    goto :goto_0

    .line 1849
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->androidPackageName:Ljava/lang/String;

    goto :goto_0

    .line 1853
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->version:Ljava/lang/String;

    goto :goto_0

    .line 1807
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    .line 1818
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1704
    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1771
    iget v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->appType:I

    if-eqz v0, :cond_0

    .line 1772
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->appType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1774
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->androidPackageName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1775
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->androidPackageName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1777
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->version:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1778
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$AppUsage1pLogEvent;->version:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1780
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1781
    return-void
.end method
