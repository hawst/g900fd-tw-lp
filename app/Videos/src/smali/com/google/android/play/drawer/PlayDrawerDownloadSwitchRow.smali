.class Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;
.super Landroid/widget/RelativeLayout;
.source "PlayDrawerDownloadSwitchRow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/Checkable;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow$OnCheckedChangeListener;
    }
.end annotation


# static fields
.field private static final SUPPORTS_STYLED_SWITCH:Z


# instance fields
.field private mActionTextView:Landroid/widget/TextView;

.field private mBroadcasting:Z

.field private mChecked:Z

.field private mCheckedTextColor:I

.field private mListener:Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow$OnCheckedChangeListener;

.field private mSwitch:Landroid/widget/Switch;

.field private final mSwitchTouchListener:Landroid/view/View$OnTouchListener;

.field private mUncheckedTextColor:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->SUPPORTS_STYLED_SWITCH:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    new-instance v0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow$1;

    invoke-direct {v0, p0}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow$1;-><init>(Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;)V

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mSwitchTouchListener:Landroid/view/View$OnTouchListener;

    .line 58
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/R$color;->play_fg_primary:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mUncheckedTextColor:I

    .line 59
    return-void
.end method


# virtual methods
.method public configure(Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;)V
    .locals 4
    .param p1, "config"    # Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;

    .prologue
    .line 63
    iget v2, p1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;->checkedTextColor:I

    iput v2, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mCheckedTextColor:I

    .line 64
    iget v0, p1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;->thumbDrawableId:I

    .line 65
    .local v0, "switchThumbDrawableId":I
    iget v1, p1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;->trackDrawableId:I

    .line 66
    .local v1, "switchTrackDrawableId":I
    sget-boolean v2, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->SUPPORTS_STYLED_SWITCH:Z

    if-eqz v2, :cond_0

    .line 67
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v2, v1}, Landroid/widget/Switch;->setTrackResource(I)V

    .line 68
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v2, v0}, Landroid/widget/Switch;->setThumbResource(I)V

    .line 69
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mSwitch:Landroid/widget/Switch;

    iget-object v3, p1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;->actionText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/Switch;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 71
    :cond_0
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mActionTextView:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/google/android/play/drawer/PlayDrawerLayout$PlayDrawerDownloadSwitchConfig;->actionText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    return-void
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mChecked:Z

    return v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mChecked:Z

    if-eq p2, v0, :cond_0

    .line 149
    invoke-virtual {p0, p2}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->setChecked(Z)V

    .line 151
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->toggle()V

    .line 144
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 4
    .param p1, "extraSpace"    # I

    .prologue
    .line 130
    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Landroid/widget/RelativeLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 131
    .local v0, "superstate":[I
    iget-boolean v1, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mChecked:Z

    if-eqz v1, :cond_0

    .line 132
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const v3, 0x1010106

    aput v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mergeDrawableStates([I[I)[I

    .line 134
    :cond_0
    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 76
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 77
    sget v1, Lcom/google/android/play/R$id;->action_text:I

    invoke-virtual {p0, v1}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mActionTextView:Landroid/widget/TextView;

    .line 78
    invoke-virtual {p0, p0}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    sget v1, Lcom/google/android/play/R$id;->switch_button:I

    invoke-virtual {p0, v1}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 80
    .local v0, "switchView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 81
    check-cast v0, Landroid/widget/Switch;

    .end local v0    # "switchView":Landroid/view/View;
    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mSwitch:Landroid/widget/Switch;

    .line 82
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v1, p0}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 83
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mSwitch:Landroid/widget/Switch;

    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mSwitchTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 85
    :cond_0
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 168
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 169
    const-class v0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 170
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mChecked:Z

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    .line 171
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 160
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 161
    const-class v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 162
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    .line 163
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mChecked:Z

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    .line 164
    return-void
.end method

.method public setChecked(Z)V
    .locals 2
    .param p1, "checked"    # Z

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mChecked:Z

    if-eq v0, p1, :cond_0

    .line 92
    invoke-virtual {p0, p1}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->setCheckedNoCallbacks(Z)V

    .line 93
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mBroadcasting:Z

    if-eqz v0, :cond_1

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_2

    .line 97
    const/16 v0, 0x800

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->sendAccessibilityEvent(I)V

    .line 100
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mBroadcasting:Z

    .line 101
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mListener:Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow$OnCheckedChangeListener;

    if-eqz v0, :cond_3

    .line 102
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mListener:Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow$OnCheckedChangeListener;

    invoke-interface {v0, p0, p1}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow$OnCheckedChangeListener;->onCheckedChanged(Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;Z)V

    .line 104
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mBroadcasting:Z

    goto :goto_0
.end method

.method public setCheckedNoCallbacks(Z)V
    .locals 2
    .param p1, "checked"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mChecked:Z

    .line 111
    sget-boolean v0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->SUPPORTS_STYLED_SWITCH:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mSwitch:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 113
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->refreshDrawableState()V

    .line 115
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mActionTextView:Landroid/widget/TextView;

    if-eqz p1, :cond_1

    iget v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mCheckedTextColor:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 116
    return-void

    .line 115
    :cond_1
    iget v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mUncheckedTextColor:I

    goto :goto_0
.end method

.method public setOnCheckedChangeListener(Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow$OnCheckedChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow$OnCheckedChangeListener;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mListener:Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow$OnCheckedChangeListener;

    .line 139
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->mChecked:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerDownloadSwitchRow;->setChecked(Z)V

    .line 126
    return-void

    .line 125
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
