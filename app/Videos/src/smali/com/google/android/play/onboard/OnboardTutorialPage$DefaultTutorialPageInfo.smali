.class public Lcom/google/android/play/onboard/OnboardTutorialPage$DefaultTutorialPageInfo;
.super Ljava/lang/Object;
.source "OnboardTutorialPage.java"

# interfaces
.implements Lcom/google/android/play/onboard/OnboardPageInfo;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/onboard/OnboardTutorialPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DefaultTutorialPageInfo"
.end annotation


# instance fields
.field protected final mContext:Landroid/content/Context;

.field private final mPageCount:I

.field private final mPageIndex:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "groupPageIndex"    # I
    .param p3, "groupPageCount"    # I

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$DefaultTutorialPageInfo;->mContext:Landroid/content/Context;

    .line 93
    iput p2, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$DefaultTutorialPageInfo;->mPageIndex:I

    .line 94
    iput p3, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$DefaultTutorialPageInfo;->mPageCount:I

    .line 95
    return-void
.end method


# virtual methods
.method public allowSwipeToNext(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 147
    const/4 v0, 0x1

    return v0
.end method

.method public allowSwipeToPrevious(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 142
    const/4 v0, 0x1

    return v0
.end method

.method public getEndButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 3
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 124
    new-instance v0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    invoke-direct {v0}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;-><init>()V

    sget v1, Lcom/google/android/play/R$drawable;->ic_chevron_end_wht_24dp:I

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setIconResId(I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$DefaultTutorialPageInfo;->mContext:Landroid/content/Context;

    sget v2, Lcom/google/android/play/R$string;->play_onboard_button_next:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setLabel(Landroid/content/Context;I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    new-instance v1, Lcom/google/android/play/onboard/OnboardTutorialPage$DefaultTutorialPageInfo$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/play/onboard/OnboardTutorialPage$DefaultTutorialPageInfo$1;-><init>(Lcom/google/android/play/onboard/OnboardTutorialPage$DefaultTutorialPageInfo;Lcom/google/android/play/onboard/OnboardHostControl;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setClickRunnable(Ljava/lang/Runnable;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    return-object v0
.end method

.method public getGroupPageCount(Lcom/google/android/play/onboard/OnboardHostControl;)I
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 109
    iget v0, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$DefaultTutorialPageInfo;->mPageCount:I

    return v0
.end method

.method public getGroupPageIndex(Lcom/google/android/play/onboard/OnboardHostControl;)I
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 114
    iget v0, p0, Lcom/google/android/play/onboard/OnboardTutorialPage$DefaultTutorialPageInfo;->mPageIndex:I

    return v0
.end method

.method public getStartButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 2
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 119
    new-instance v0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    invoke-direct {v0}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setVisible(Z)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    return-object v0
.end method

.method public isNavFooterVisible(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 99
    const/4 v0, 0x1

    return v0
.end method

.method public shouldAdjustPagePaddingToFitNavFooter(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 104
    const/4 v0, 0x1

    return v0
.end method
