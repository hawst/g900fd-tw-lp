.class public Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;
.super Ljava/lang/Object;
.source "PlayPopupMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/layout/PlayPopupMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "PopupAction"
.end annotation


# instance fields
.field public final mActionListener:Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final mId:I

.field public final mIsEnabled:Z

.field public final mPopupActionListener:Lcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;

.field public final mTitle:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(ILjava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "title"    # Ljava/lang/CharSequence;
    .param p3, "isEnabled"    # Z
    .param p4, "popupActionListener"    # Lcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput p1, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mId:I

    .line 108
    iput-object p2, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mTitle:Ljava/lang/CharSequence;

    .line 109
    iput-boolean p3, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mIsEnabled:Z

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mActionListener:Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;

    .line 111
    iput-object p4, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mPopupActionListener:Lcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;

    .line 112
    return-void
.end method


# virtual methods
.method public onSelect()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mPopupActionListener:Lcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mPopupActionListener:Lcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;

    iget v1, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mId:I

    invoke-interface {v0, v1}, Lcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;->onActionSelected(I)V

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mActionListener:Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;->mActionListener:Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;

    invoke-interface {v0}, Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;->onActionSelected()V

    goto :goto_0
.end method
