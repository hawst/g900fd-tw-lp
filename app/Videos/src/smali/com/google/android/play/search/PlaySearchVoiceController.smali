.class public final Lcom/google/android/play/search/PlaySearchVoiceController;
.super Landroid/content/BroadcastReceiver;
.source "PlaySearchVoiceController.java"


# static fields
.field private static final VOICE_SEARCH_RESULT_ACTION:Ljava/lang/String;

.field private static final sVoiceIntent:Landroid/content/Intent;


# instance fields
.field private final mController:Lcom/google/android/play/search/PlaySearchController;

.field private mRegistered:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/play/search/PlaySearchVoiceController;

    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "VOICE_SEARCH_RESULT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/search/PlaySearchVoiceController;->VOICE_SEARCH_RESULT_ACTION:Ljava/lang/String;

    .line 22
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/play/search/PlaySearchVoiceController;->sVoiceIntent:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/play/search/PlaySearchController;)V
    .locals 0
    .param p1, "controller"    # Lcom/google/android/play/search/PlaySearchController;

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/play/search/PlaySearchVoiceController;->mController:Lcom/google/android/play/search/PlaySearchController;

    .line 36
    return-void
.end method

.method public static canPerformVoiceSearch(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 31
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    sget-object v2, Lcom/google/android/play/search/PlaySearchVoiceController;->sVoiceIntent:Landroid/content/Intent;

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private register(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/play/search/PlaySearchVoiceController;->mRegistered:Z

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Landroid/content/IntentFilter;

    sget-object v1, Lcom/google/android/play/search/PlaySearchVoiceController;->VOICE_SEARCH_RESULT_ACTION:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/search/PlaySearchVoiceController;->mRegistered:Z

    .line 65
    :cond_0
    return-void
.end method


# virtual methods
.method cancelPendingVoiceSearch(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/google/android/play/search/PlaySearchVoiceController;->mRegistered:Z

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/search/PlaySearchVoiceController;->mRegistered:Z

    .line 75
    :cond_0
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 79
    const-string v1, "android.speech.extra.RESULTS"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 81
    .local v0, "matches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/play/search/PlaySearchVoiceController;->mController:Lcom/google/android/play/search/PlaySearchController;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/play/search/PlaySearchController;->setMode(I)V

    .line 82
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 84
    iget-object v2, p0, Lcom/google/android/play/search/PlaySearchVoiceController;->mController:Lcom/google/android/play/search/PlaySearchController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/google/android/play/search/PlaySearchController;->setQuery(Ljava/lang/String;)V

    .line 86
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/play/search/PlaySearchVoiceController;->cancelPendingVoiceSearch(Landroid/content/Context;)V

    .line 87
    return-void
.end method

.method requestVoiceSearch(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    new-instance v1, Landroid/content/Intent;

    sget-object v3, Lcom/google/android/play/search/PlaySearchVoiceController;->VOICE_SEARCH_RESULT_ACTION:Ljava/lang/String;

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 44
    .local v1, "result":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 46
    const/4 v3, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {p1, v3, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 48
    .local v0, "pending":Landroid/app/PendingIntent;
    new-instance v2, Landroid/content/Intent;

    sget-object v3, Lcom/google/android/play/search/PlaySearchVoiceController;->sVoiceIntent:Landroid/content/Intent;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 49
    .local v2, "voiceSearchIntent":Landroid/content/Intent;
    const-string v3, "android.speech.extra.LANGUAGE_MODEL"

    const-string v4, "free_form"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    const-string v3, "android.speech.extra.RESULTS_PENDINGINTENT"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/play/search/PlaySearchVoiceController;->register(Landroid/content/Context;)V

    .line 53
    invoke-virtual {p1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 54
    return-void
.end method
