.class final Lcom/google/android/play/animation/PlayInterpolators$1;
.super Ljava/lang/Object;
.source "PlayInterpolators.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/animation/PlayInterpolators;->slowOutFastIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private inverse:Landroid/view/animation/Interpolator;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/play/animation/PlayInterpolators$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iget-object v0, p0, Lcom/google/android/play/animation/PlayInterpolators$1;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/play/animation/PlayInterpolators;->fastOutSlowIn(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/animation/PlayInterpolators$1;->inverse:Landroid/view/animation/Interpolator;

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 3
    .param p1, "input"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 44
    iget-object v0, p0, Lcom/google/android/play/animation/PlayInterpolators$1;->inverse:Landroid/view/animation/Interpolator;

    sub-float v1, v2, p1

    invoke-interface {v0, v1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    sub-float v0, v2, v0

    return v0
.end method
