.class Lcom/google/android/play/layout/FlowLayoutManager$SavedState;
.super Ljava/lang/Object;
.source "FlowLayoutManager.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/layout/FlowLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/play/layout/FlowLayoutManager$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mReferenceOffset:F

.field mReferencePosition:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3537
    new-instance v0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState$1;

    invoke-direct {v0}, Lcom/google/android/play/layout/FlowLayoutManager$SavedState$1;-><init>()V

    sput-object v0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 3515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3516
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->mReferencePosition:I

    .line 3517
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->mReferenceOffset:F

    .line 3518
    return-void
.end method

.method public constructor <init>(Lcom/google/android/play/layout/FlowLayoutManager$SavedState;)V
    .locals 1
    .param p1, "other"    # Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    .prologue
    .line 3520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3521
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->mReferencePosition:I

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->mReferencePosition:I

    .line 3522
    iget v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->mReferenceOffset:F

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->mReferenceOffset:F

    .line 3523
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 3527
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 3532
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->mReferencePosition:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3533
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->mReferenceOffset:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 3534
    return-void
.end method
