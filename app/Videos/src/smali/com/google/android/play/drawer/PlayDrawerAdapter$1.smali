.class Lcom/google/android/play/drawer/PlayDrawerAdapter$1;
.super Ljava/lang/Object;
.source "PlayDrawerAdapter.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/drawer/PlayDrawerAdapter;->getProfileInfoView(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

.field final synthetic val$finalCurrentAccount:Landroid/accounts/Account;

.field final synthetic val$finalCurrentAccountName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/play/drawer/PlayDrawerAdapter;Ljava/lang/String;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    iput-object p2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->val$finalCurrentAccountName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->val$finalCurrentAccount:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;)V
    .locals 7
    .param p1, "plusProfileResponse"    # Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    .prologue
    .line 363
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$000(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->val$finalCurrentAccountName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 367
    .local v1, "prevDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    iget-object v0, p1, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;->partialUserProfile:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 368
    .local v0, "newDocV2":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$000(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->val$finalCurrentAccountName:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mIsAccountDocLoaded:Ljava/util/Set;
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$100(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Set;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->val$finalCurrentAccountName:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 374
    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->val$finalCurrentAccountName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mCurrentAccount:Landroid/accounts/Account;
    invoke-static {v3}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$200(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Landroid/accounts/Account;

    move-result-object v3

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 376
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # invokes: Lcom/google/android/play/drawer/PlayDrawerAdapter;->isProfileContainerVisible()Z
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$300(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 377
    iget-object v2, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mProfileInfoView:Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;
    invoke-static {v2}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$600(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->val$finalCurrentAccount:Landroid/accounts/Account;

    iget-object v4, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mNonCurrentAccounts:[Landroid/accounts/Account;
    invoke-static {v4}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$400(Lcom/google/android/play/drawer/PlayDrawerAdapter;)[Landroid/accounts/Account;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mAccountDocV2s:Ljava/util/Map;
    invoke-static {v5}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$000(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Ljava/util/Map;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->this$0:Lcom/google/android/play/drawer/PlayDrawerAdapter;

    # getter for: Lcom/google/android/play/drawer/PlayDrawerAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;
    invoke-static {v6}, Lcom/google/android/play/drawer/PlayDrawerAdapter;->access$500(Lcom/google/android/play/drawer/PlayDrawerAdapter;)Lcom/google/android/play/image/BitmapLoader;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->configure(Landroid/accounts/Account;[Landroid/accounts/Account;Ljava/util/Map;Lcom/google/android/play/image/BitmapLoader;)V

    .line 386
    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 360
    check-cast p1, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/play/drawer/PlayDrawerAdapter$1;->onResponse(Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;)V

    return-void
.end method
