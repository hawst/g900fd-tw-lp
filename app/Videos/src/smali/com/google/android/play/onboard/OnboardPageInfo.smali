.class public interface abstract Lcom/google/android/play/onboard/OnboardPageInfo;
.super Ljava/lang/Object;
.source "OnboardPageInfo.java"


# virtual methods
.method public abstract allowSwipeToNext(Lcom/google/android/play/onboard/OnboardHostControl;)Z
.end method

.method public abstract allowSwipeToPrevious(Lcom/google/android/play/onboard/OnboardHostControl;)Z
.end method

.method public abstract getEndButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
.end method

.method public abstract getGroupPageCount(Lcom/google/android/play/onboard/OnboardHostControl;)I
.end method

.method public abstract getGroupPageIndex(Lcom/google/android/play/onboard/OnboardHostControl;)I
.end method

.method public abstract getStartButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
.end method

.method public abstract isNavFooterVisible(Lcom/google/android/play/onboard/OnboardHostControl;)Z
.end method

.method public abstract shouldAdjustPagePaddingToFitNavFooter(Lcom/google/android/play/onboard/OnboardHostControl;)Z
.end method
