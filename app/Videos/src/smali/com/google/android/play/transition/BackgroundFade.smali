.class public Lcom/google/android/play/transition/BackgroundFade;
.super Landroid/transition/Visibility;
.source "BackgroundFade.java"


# instance fields
.field private mEndColor:I

.field private mStartColor:I


# direct methods
.method public constructor <init>(II)V
    .locals 0
    .param p1, "startColor"    # I
    .param p2, "endColor"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/transition/Visibility;-><init>()V

    .line 25
    iput p1, p0, Lcom/google/android/play/transition/BackgroundFade;->mStartColor:I

    .line 26
    iput p2, p0, Lcom/google/android/play/transition/BackgroundFade;->mEndColor:I

    .line 27
    return-void
.end method


# virtual methods
.method public onAppear(Landroid/view/ViewGroup;Landroid/view/View;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 5
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "startValues"    # Landroid/transition/TransitionValues;
    .param p4, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 32
    const-string v1, "backgroundColor"

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/play/transition/BackgroundFade;->mStartColor:I

    aput v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/play/transition/BackgroundFade;->mEndColor:I

    aput v4, v2, v3

    invoke-static {p2, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 34
    .local v0, "animator":Landroid/animation/ObjectAnimator;
    new-instance v1, Landroid/animation/ArgbEvaluator;

    invoke-direct {v1}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 35
    return-object v0
.end method

.method public onDisappear(Landroid/view/ViewGroup;Landroid/view/View;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 5
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "startValues"    # Landroid/transition/TransitionValues;
    .param p4, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 41
    const-string v1, "backgroundColor"

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/play/transition/BackgroundFade;->mEndColor:I

    aput v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/play/transition/BackgroundFade;->mStartColor:I

    aput v4, v2, v3

    invoke-static {p2, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 43
    .local v0, "animator":Landroid/animation/ObjectAnimator;
    new-instance v1, Landroid/animation/ArgbEvaluator;

    invoke-direct {v1}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 44
    return-object v0
.end method
