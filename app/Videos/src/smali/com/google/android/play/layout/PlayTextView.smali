.class public Lcom/google/android/play/layout/PlayTextView;
.super Landroid/widget/TextView;
.source "PlayTextView.java"


# static fields
.field private static final RESPECT_ORIGINAL_LINE_SPACING:Z


# instance fields
.field private mBorderPaint:Landroid/graphics/Paint;

.field private mBorderThickness:F

.field private final mCompactFactor:F

.field private mDecorationPosition:I

.field private mDrawBorder:Z

.field private mLastLineFadeOutDrawable:Landroid/graphics/drawable/GradientDrawable;

.field private mLastLineFadeOutHintMargin:I

.field private mLastLineFadeOutSize:I

.field private final mLastLineOverdrawHint:Ljava/lang/String;

.field private mLastLineOverdrawHintPaint:Landroid/graphics/Paint;

.field private mLastLineOverdrawPaint:Landroid/graphics/Paint;

.field private mToDrawOverLastLineIfNecessary:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 57
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/layout/PlayTextView;->RESPECT_ORIGINAL_LINE_SPACING:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 94
    invoke-direct/range {p0 .. p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 96
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 98
    .local v9, "res":Landroid/content/res/Resources;
    sget-object v12, Lcom/google/android/play/R$styleable;->PlayTextView:[I

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v12}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v11

    .line 100
    .local v11, "viewAttrs":Landroid/content/res/TypedArray;
    sget v12, Lcom/google/android/play/R$styleable;->PlayTextView_lastLineOverdrawColor:I

    invoke-virtual {v11, v12}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v12

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/google/android/play/layout/PlayTextView;->mToDrawOverLastLineIfNecessary:Z

    .line 102
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/play/layout/PlayTextView;->mToDrawOverLastLineIfNecessary:Z

    if-eqz v12, :cond_0

    .line 103
    sget v12, Lcom/google/android/play/R$styleable;->PlayTextView_lastLineOverdrawColor:I

    sget v13, Lcom/google/android/play/R$color;->play_white:I

    invoke-virtual {v9, v13}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    invoke-virtual {v11, v12, v13}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v8

    .line 105
    .local v8, "overdrawColor":I
    new-instance v12, Landroid/graphics/Paint;

    invoke-direct {v12}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawPaint:Landroid/graphics/Paint;

    .line 106
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawPaint:Landroid/graphics/Paint;

    invoke-virtual {v12, v8}, Landroid/graphics/Paint;->setColor(I)V

    .line 107
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawPaint:Landroid/graphics/Paint;

    sget-object v13, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 109
    sget v12, Lcom/google/android/play/R$dimen;->play_text_view_fadeout:I

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineFadeOutSize:I

    .line 112
    new-instance v12, Landroid/graphics/drawable/GradientDrawable;

    sget-object v13, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v14, 0x2

    new-array v14, v14, [I

    const/4 v15, 0x0

    const v16, 0xffffff

    and-int v16, v16, v8

    aput v16, v14, v15

    const/4 v15, 0x1

    aput v8, v14, v15

    invoke-direct {v12, v13, v14}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineFadeOutDrawable:Landroid/graphics/drawable/GradientDrawable;

    .line 114
    sget v12, Lcom/google/android/play/R$dimen;->play_text_view_fadeout_hint_margin:I

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineFadeOutHintMargin:I

    .line 117
    .end local v8    # "overdrawColor":I
    :cond_0
    sget v12, Lcom/google/android/play/R$styleable;->PlayTextView_lastLineOverdrawHint:I

    invoke-virtual {v11, v12}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawHint:Ljava/lang/String;

    .line 118
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawHint:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 119
    new-instance v12, Landroid/graphics/Paint;

    invoke-direct {v12}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawHintPaint:Landroid/graphics/Paint;

    .line 120
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawHintPaint:Landroid/graphics/Paint;

    sget v13, Lcom/google/android/play/R$styleable;->PlayTextView_lastLineOverdrawHintColor:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayTextView;->getCurrentTextColor()I

    move-result v14

    invoke-virtual {v11, v13, v14}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 123
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawHintPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayTextView;->getTextSize()F

    move-result v13

    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 124
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawHintPaint:Landroid/graphics/Paint;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 125
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawHintPaint:Landroid/graphics/Paint;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 127
    :cond_1
    sget v12, Lcom/google/android/play/R$styleable;->PlayTextView_decoration_position:I

    const/4 v13, 0x1

    invoke-virtual {v11, v12, v13}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/play/layout/PlayTextView;->mDecorationPosition:I

    .line 130
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayTextView;->getTextSize()F

    move-result v10

    .line 131
    .local v10, "textSize":F
    sget v12, Lcom/google/android/play/R$bool;->play_text_compact_mode_enable:I

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    .line 132
    .local v4, "isGlobalCompactModeEnabled":Z
    sget v12, Lcom/google/android/play/R$styleable;->PlayTextView_allowsCompactLineSpacing:I

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v5

    .line 134
    .local v5, "isLocalCompactModeEnabled":Z
    if-eqz v4, :cond_4

    if-eqz v5, :cond_4

    .line 138
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v12

    invoke-virtual {v12}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v7

    .line 139
    .local v7, "metrics":Landroid/graphics/Paint$FontMetrics;
    const v12, 0x3f960419    # 1.172f

    mul-float/2addr v12, v10

    iget v13, v7, Landroid/graphics/Paint$FontMetrics;->ascent:F

    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v13

    iget v14, v7, Landroid/graphics/Paint$FontMetrics;->descent:F

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v14

    add-float/2addr v13, v14

    div-float v6, v12, v13

    .line 141
    .local v6, "lineSpacing":F
    const/4 v12, 0x0

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float/2addr v13, v6

    invoke-static {v12, v13}, Ljava/lang/Math;->max(FF)F

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/play/layout/PlayTextView;->mCompactFactor:F

    .line 145
    .end local v6    # "lineSpacing":F
    .end local v7    # "metrics":Landroid/graphics/Paint$FontMetrics;
    :goto_0
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/play/layout/PlayTextView;->mCompactFactor:F

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-lez v12, :cond_3

    .line 146
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/play/layout/PlayTextView;->mCompactFactor:F

    neg-float v12, v12

    mul-float v2, v12, v10

    .line 147
    .local v2, "compactedLineSpacing":F
    sget-boolean v12, Lcom/google/android/play/layout/PlayTextView;->RESPECT_ORIGINAL_LINE_SPACING:Z

    if-eqz v12, :cond_2

    .line 150
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayTextView;->getLineHeight()I

    move-result v12

    int-to-float v12, v12

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayTextView;->getLineSpacingMultiplier()F

    move-result v13

    const/high16 v14, 0x3f800000    # 1.0f

    sub-float/2addr v13, v14

    mul-float v3, v12, v13

    .line 151
    .local v3, "extraLineSpacing":F
    add-float/2addr v2, v3

    .line 153
    .end local v3    # "extraLineSpacing":F
    :cond_2
    const/high16 v12, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v12}, Lcom/google/android/play/layout/PlayTextView;->setLineSpacing(FF)V

    .line 155
    .end local v2    # "compactedLineSpacing":F
    :cond_3
    invoke-virtual {v11}, Landroid/content/res/TypedArray;->recycle()V

    .line 157
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    sget v13, Lcom/google/android/play/R$dimen;->play_text_view_outline:I

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/play/layout/PlayTextView;->mBorderThickness:F

    .line 158
    new-instance v12, Landroid/graphics/Paint;

    invoke-direct {v12}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/play/layout/PlayTextView;->mBorderPaint:Landroid/graphics/Paint;

    .line 159
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/play/layout/PlayTextView;->mBorderPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/play/layout/PlayTextView;->mBorderThickness:F

    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 160
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/play/layout/PlayTextView;->mBorderPaint:Landroid/graphics/Paint;

    sget-object v13, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v12, v13}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 162
    const/4 v12, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/google/android/play/layout/PlayTextView;->setWillNotDraw(Z)V

    .line 163
    return-void

    .line 143
    :cond_4
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/android/play/layout/PlayTextView;->mCompactFactor:F

    goto :goto_0
.end method

.method private getLineNonWhiteSpaceCharacterEnd(I)I
    .locals 5
    .param p1, "line"    # I

    .prologue
    .line 326
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 327
    .local v1, "layout":Landroid/text/Layout;
    invoke-virtual {v1}, Landroid/text/Layout;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    .line 328
    .local v3, "text":Ljava/lang/CharSequence;
    invoke-virtual {v1, p1}, Landroid/text/Layout;->getLineStart(I)I

    move-result v2

    .line 329
    .local v2, "start":I
    invoke-virtual {v1, p1}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v0

    .line 331
    .local v0, "end":I
    :goto_0
    if-le v0, v2, :cond_1

    .line 332
    add-int/lit8 v4, v0, -0x1

    invoke-interface {v3, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v4

    if-nez v4, :cond_0

    .line 336
    .end local v0    # "end":I
    :goto_1
    return v0

    .line 331
    .restart local v0    # "end":I
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 336
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 25
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 237
    invoke-super/range {p0 .. p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 239
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/play/layout/PlayTextView;->mDrawBorder:Z

    if-eqz v2, :cond_0

    .line 246
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/play/layout/PlayTextView;->mBorderThickness:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v14, v2

    .line 247
    .local v14, "inset":I
    int-to-float v3, v14

    int-to-float v4, v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayTextView;->getWidth()I

    move-result v2

    sub-int/2addr v2, v14

    int-to-float v5, v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayTextView;->getHeight()I

    move-result v2

    sub-int/2addr v2, v14

    int-to-float v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/play/layout/PlayTextView;->mBorderPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 250
    .end local v14    # "inset":I
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/play/layout/PlayTextView;->mToDrawOverLastLineIfNecessary:Z

    if-nez v2, :cond_2

    .line 319
    :cond_1
    :goto_0
    return-void

    .line 254
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayTextView;->getHeight()I

    move-result v11

    .line 255
    .local v11, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayTextView;->getWidth()I

    move-result v24

    .line 256
    .local v24, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayTextView;->getLayout()Landroid/text/Layout;

    move-result-object v15

    .line 257
    .local v15, "layout":Landroid/text/Layout;
    if-eqz v15, :cond_1

    .line 260
    const/16 v22, -0x1

    .line 261
    .local v22, "prevLineTop":I
    const/16 v20, -0x1

    .line 262
    .local v20, "prevLineBottom":I
    const/16 v16, 0x0

    .local v16, "line":I
    :goto_1
    invoke-virtual {v15}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    move/from16 v0, v16

    if-ge v0, v2, :cond_1

    .line 263
    invoke-virtual/range {v15 .. v16}, Landroid/text/Layout;->getLineTop(I)I

    move-result v9

    .line 264
    .local v9, "currLineTop":I
    invoke-virtual/range {v15 .. v16}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v8

    .line 265
    .local v8, "currLineBottom":I
    if-gt v9, v11, :cond_8

    if-le v8, v11, :cond_8

    .line 268
    const/4 v3, 0x0

    int-to-float v4, v9

    move/from16 v0, v24

    int-to-float v5, v0

    int-to-float v6, v11

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 269
    if-lez v16, :cond_1

    .line 274
    move/from16 v23, v16

    .line 276
    .local v23, "previousNonWhiteSpaceLine":I
    :cond_3
    add-int/lit8 v23, v23, -0x1

    .line 277
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/google/android/play/layout/PlayTextView;->getLineNonWhiteSpaceCharacterEnd(I)I

    move-result v17

    .line 279
    .local v17, "nonWhiteSpaceCharOffset":I
    const/4 v2, -0x1

    move/from16 v0, v17

    if-eq v0, v2, :cond_7

    .line 284
    :goto_2
    const/4 v2, -0x1

    move/from16 v0, v17

    if-ne v0, v2, :cond_4

    .line 285
    const/16 v17, 0x0

    .line 287
    :cond_4
    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/text/Layout;->getLineTop(I)I

    move-result v22

    .line 288
    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v20

    .line 290
    move/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v2

    float-to-int v0, v2

    move/from16 v21, v0

    .line 291
    .local v21, "prevLineEnd":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayTextView;->getPaddingLeft()I

    move-result v18

    .line 292
    .local v18, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/PlayTextView;->getPaddingRight()I

    move-result v19

    .line 294
    .local v19, "paddingRight":I
    add-int v10, v18, v21

    .line 295
    .local v10, "fadeOutRight":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawHint:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 296
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawHintPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawHint:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-int v13, v2

    .line 298
    .local v13, "hintWidth":I
    sub-int v2, v24, v19

    sub-int v12, v2, v13

    .line 299
    .local v12, "hintLeft":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineFadeOutHintMargin:I

    sub-int v2, v12, v2

    move/from16 v0, v21

    if-ge v2, v0, :cond_5

    .line 300
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineFadeOutHintMargin:I

    sub-int v10, v12, v2

    .line 302
    int-to-float v3, v10

    move/from16 v0, v22

    int-to-float v4, v0

    sub-int v2, v24, v19

    int-to-float v5, v2

    move/from16 v0, v20

    int-to-float v6, v0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 305
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawHint:Ljava/lang/String;

    int-to-float v3, v12

    add-int/lit8 v4, v16, -0x1

    invoke-virtual {v15, v4}, Landroid/text/Layout;->getLineBaseline(I)I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawHintPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 309
    .end local v12    # "hintLeft":I
    .end local v13    # "hintWidth":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineFadeOutDrawable:Landroid/graphics/drawable/GradientDrawable;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineFadeOutSize:I

    sub-int v3, v10, v3

    move/from16 v0, v22

    move/from16 v1, v20

    invoke-virtual {v2, v3, v0, v10, v1}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    .line 314
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/PlayTextView;->mLastLineFadeOutDrawable:Landroid/graphics/drawable/GradientDrawable;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    .line 282
    .end local v10    # "fadeOutRight":I
    .end local v18    # "paddingLeft":I
    .end local v19    # "paddingRight":I
    .end local v21    # "prevLineEnd":I
    :cond_7
    if-gtz v23, :cond_3

    goto/16 :goto_2

    .line 262
    .end local v17    # "nonWhiteSpaceCharOffset":I
    .end local v23    # "previousNonWhiteSpaceLine":I
    :cond_8
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1
.end method

.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 167
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    .line 169
    iget v0, p0, Lcom/google/android/play/layout/PlayTextView;->mCompactFactor:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    if-eq v0, v1, :cond_0

    .line 178
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v1

    iget v2, p0, Lcom/google/android/play/layout/PlayTextView;->mCompactFactor:F

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getLineHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/play/layout/PlayTextView;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public setContentColorId(IZ)V
    .locals 2
    .param p1, "contentColorId"    # I
    .param p2, "drawBorder"    # Z

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 197
    .local v0, "color":I
    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayTextView;->setTextColor(I)V

    .line 198
    iput-boolean p2, p0, Lcom/google/android/play/layout/PlayTextView;->mDrawBorder:Z

    .line 199
    iget-boolean v1, p0, Lcom/google/android/play/layout/PlayTextView;->mDrawBorder:Z

    if-eqz v1, :cond_0

    .line 200
    iget-object v1, p0, Lcom/google/android/play/layout/PlayTextView;->mBorderPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 202
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->invalidate()V

    .line 203
    return-void
.end method

.method public setContentColorStateListId(IZ)V
    .locals 3
    .param p1, "contentColorStateListId"    # I
    .param p2, "drawBorder"    # Z

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 208
    .local v0, "contentColorStateList":Landroid/content/res/ColorStateList;
    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 209
    iput-boolean p2, p0, Lcom/google/android/play/layout/PlayTextView;->mDrawBorder:Z

    .line 210
    iget-boolean v1, p0, Lcom/google/android/play/layout/PlayTextView;->mDrawBorder:Z

    if-eqz v1, :cond_0

    .line 211
    iget-object v1, p0, Lcom/google/android/play/layout/PlayTextView;->mBorderPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 213
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->invalidate()V

    .line 214
    return-void
.end method

.method public setDecorationBitmap(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "decorationBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v2, 0x0

    .line 183
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 184
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    iget v1, p0, Lcom/google/android/play/layout/PlayTextView;->mDecorationPosition:I

    packed-switch v1, :pswitch_data_0

    .line 192
    :goto_0
    return-void

    .line 186
    :pswitch_0
    invoke-virtual {p0, v0, v2, v2, v2}, Lcom/google/android/play/layout/PlayTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 189
    :pswitch_1
    invoke-virtual {p0, v2, v2, v0, v2}, Lcom/google/android/play/layout/PlayTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 184
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setLastLineOverdrawColor(I)V
    .locals 7
    .param p1, "lastLineOverdrawColor"    # I

    .prologue
    const/4 v6, 0x1

    .line 217
    iget-boolean v1, p0, Lcom/google/android/play/layout/PlayTextView;->mToDrawOverLastLineIfNecessary:Z

    if-nez v1, :cond_0

    .line 218
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawPaint:Landroid/graphics/Paint;

    .line 219
    iget-object v1, p0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 221
    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 222
    .local v0, "res":Landroid/content/res/Resources;
    sget v1, Lcom/google/android/play/R$dimen;->play_text_view_fadeout:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayTextView;->mLastLineFadeOutSize:I

    .line 223
    sget v1, Lcom/google/android/play/R$dimen;->play_text_view_fadeout_hint_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/PlayTextView;->mLastLineFadeOutHintMargin:I

    .line 226
    .end local v0    # "res":Landroid/content/res/Resources;
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/layout/PlayTextView;->mLastLineOverdrawPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 229
    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    sget-object v2, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v3, 0x2

    new-array v3, v3, [I

    const/4 v4, 0x0

    const v5, 0xffffff

    and-int/2addr v5, p1

    aput v5, v3, v4

    aput p1, v3, v6

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    iput-object v1, p0, Lcom/google/android/play/layout/PlayTextView;->mLastLineFadeOutDrawable:Landroid/graphics/drawable/GradientDrawable;

    .line 232
    iput-boolean v6, p0, Lcom/google/android/play/layout/PlayTextView;->mToDrawOverLastLineIfNecessary:Z

    .line 233
    return-void
.end method
