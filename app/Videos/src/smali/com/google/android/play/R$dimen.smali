.class public final Lcom/google/android/play/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abc_action_bar_default_height_material:I = 0x7f0e0014

.field public static final play_avatar_decoration_threshold_max:I = 0x7f0e00e7

.field public static final play_avatar_decoration_threshold_min:I = 0x7f0e00e6

.field public static final play_avatar_drop_shadow_max:I = 0x7f0e00eb

.field public static final play_avatar_drop_shadow_min:I = 0x7f0e00ea

.field public static final play_avatar_noring_outline:I = 0x7f0e00ec

.field public static final play_avatar_ring_size_max:I = 0x7f0e00e9

.field public static final play_avatar_ring_size_min:I = 0x7f0e00e8

.field public static final play_card_default_inset:I = 0x7f0e0106

.field public static final play_card_extra_vspace:I = 0x7f0e00cb

.field public static final play_card_label_icon_gap:I = 0x7f0e00da

.field public static final play_card_label_texts_gap:I = 0x7f0e00db

.field public static final play_card_overflow_touch_extend:I = 0x7f0e00ce

.field public static final play_card_snippet_avatar_large_size:I = 0x7f0e00d2

.field public static final play_card_snippet_avatar_size:I = 0x7f0e00d1

.field public static final play_card_snippet_text_extra_margin_left:I = 0x7f0e00d6

.field public static final play_download_button_asset_inset:I = 0x7f0e00ed

.field public static final play_drawer_max_width:I = 0x7f0e00e3

.field public static final play_hairline_separator_thickness:I = 0x7f0e00c6

.field public static final play_header_list_banner_height:I = 0x7f0e00ee

.field public static final play_header_list_floating_elevation:I = 0x7f0e00f4

.field public static final play_header_list_tab_floating_padding:I = 0x7f0e00f6

.field public static final play_header_list_tab_strip_height:I = 0x7f0e00ef

.field public static final play_header_list_tab_strip_selected_underline_height:I = 0x7f0e00f0

.field public static final play_medium_size:I = 0x7f0e011a

.field public static final play_mini_card_content_height:I = 0x7f0e00d8

.field public static final play_mini_card_label_threshold:I = 0x7f0e00d9

.field public static final play_onboard__onboard_nav_footer_height:I = 0x7f0e013b

.field public static final play_onboard__onboard_simple_quiz_row_spacing:I = 0x7f0e0141

.field public static final play_onboard__page_indicator_dot_diameter:I = 0x7f0e013d

.field public static final play_search_one_suggestion_height:I = 0x7f0e0113

.field public static final play_small_card_content_min_height:I = 0x7f0e00d7

.field public static final play_snippet_large_size:I = 0x7f0e0121

.field public static final play_snippet_regular_size:I = 0x7f0e0120

.field public static final play_star_height_default:I = 0x7f0e0103

.field public static final play_tab_strip_selected_underline_height:I = 0x7f0e00c1

.field public static final play_tab_strip_title_offset:I = 0x7f0e00c4

.field public static final play_text_view_fadeout:I = 0x7f0e00c7

.field public static final play_text_view_fadeout_hint_margin:I = 0x7f0e00c8

.field public static final play_text_view_outline:I = 0x7f0e00c9
