.class Lcom/google/android/play/onboard/OnboardHostFragment$3;
.super Lcom/google/android/play/onboard/OnboardPagerAdapter;
.source "OnboardHostFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/onboard/OnboardHostFragment;->setUpPager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/onboard/OnboardHostFragment;


# direct methods
.method constructor <init>(Lcom/google/android/play/onboard/OnboardHostFragment;Lcom/google/android/libraries/bind/view/ViewHeap;)V
    .locals 0
    .param p2, "x0"    # Lcom/google/android/libraries/bind/view/ViewHeap;

    .prologue
    .line 226
    iput-object p1, p0, Lcom/google/android/play/onboard/OnboardHostFragment$3;->this$0:Lcom/google/android/play/onboard/OnboardHostFragment;

    invoke-direct {p0, p2}, Lcom/google/android/play/onboard/OnboardPagerAdapter;-><init>(Lcom/google/android/libraries/bind/view/ViewHeap;)V

    return-void
.end method


# virtual methods
.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 7
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "visualPosition"    # I

    .prologue
    .line 229
    invoke-super {p0, p1, p2}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v1

    .line 230
    .local v1, "id":Ljava/lang/Object;
    invoke-static {p0, p2}, Lcom/google/android/libraries/bind/bidi/BidiPagingHelper;->getLogicalPosition(Landroid/support/v4/view/PagerAdapter;I)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/google/android/play/onboard/OnboardHostFragment$3;->tryGetViewAt(I)Landroid/view/View;

    move-result-object v4

    .line 231
    .local v4, "view":Landroid/view/View;
    instance-of v5, v4, Lcom/google/android/play/onboard/OnboardPage;

    if-eqz v5, :cond_0

    move-object v2, v4

    .line 232
    check-cast v2, Lcom/google/android/play/onboard/OnboardPage;

    .line 233
    .local v2, "page":Lcom/google/android/play/onboard/OnboardPage;
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment$3;->this$0:Lcom/google/android/play/onboard/OnboardHostFragment;

    .line 234
    .local v0, "hostControl":Lcom/google/android/play/onboard/OnboardHostControl;
    invoke-interface {v2, v0}, Lcom/google/android/play/onboard/OnboardPage;->setOnboardHostControl(Lcom/google/android/play/onboard/OnboardHostControl;)V

    .line 235
    iget-object v5, p0, Lcom/google/android/play/onboard/OnboardHostFragment$3;->this$0:Lcom/google/android/play/onboard/OnboardHostFragment;

    iget-object v5, v5, Lcom/google/android/play/onboard/OnboardHostFragment;->mOnboardBundle:Landroid/os/Bundle;

    invoke-interface {v2, v5}, Lcom/google/android/play/onboard/OnboardPage;->restoreOnboardState(Landroid/os/Bundle;)V

    .line 236
    invoke-interface {v2}, Lcom/google/android/play/onboard/OnboardPage;->getPageInfo()Lcom/google/android/play/onboard/OnboardPageInfo;

    move-result-object v3

    .line 237
    .local v3, "pageInfo":Lcom/google/android/play/onboard/OnboardPageInfo;
    invoke-interface {v3, v0}, Lcom/google/android/play/onboard/OnboardPageInfo;->shouldAdjustPagePaddingToFitNavFooter(Lcom/google/android/play/onboard/OnboardHostControl;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 238
    iget-object v5, p0, Lcom/google/android/play/onboard/OnboardHostFragment$3;->this$0:Lcom/google/android/play/onboard/OnboardHostFragment;

    invoke-interface {v3, v0}, Lcom/google/android/play/onboard/OnboardPageInfo;->isNavFooterVisible(Lcom/google/android/play/onboard/OnboardHostControl;)Z

    move-result v6

    invoke-virtual {v5, v4, v6}, Lcom/google/android/play/onboard/OnboardHostFragment;->adjustPagePaddingToFitNavFooter(Landroid/view/View;Z)V

    .line 242
    .end local v0    # "hostControl":Lcom/google/android/play/onboard/OnboardHostControl;
    .end local v2    # "page":Lcom/google/android/play/onboard/OnboardPage;
    .end local v3    # "pageInfo":Lcom/google/android/play/onboard/OnboardPageInfo;
    :cond_0
    return-object v1
.end method

.method public onDestroyedView(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 247
    invoke-super {p0, p1}, Lcom/google/android/play/onboard/OnboardPagerAdapter;->onDestroyedView(Landroid/view/View;)V

    .line 249
    instance-of v0, p1, Lcom/google/android/play/onboard/OnboardPage;

    if-eqz v0, :cond_0

    .line 250
    check-cast p1, Lcom/google/android/play/onboard/OnboardPage;

    .end local p1    # "view":Landroid/view/View;
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardHostFragment$3;->this$0:Lcom/google/android/play/onboard/OnboardHostFragment;

    iget-object v0, v0, Lcom/google/android/play/onboard/OnboardHostFragment;->mOnboardBundle:Landroid/os/Bundle;

    invoke-interface {p1, v0}, Lcom/google/android/play/onboard/OnboardPage;->saveOnboardState(Landroid/os/Bundle;)V

    .line 252
    :cond_0
    return-void
.end method
