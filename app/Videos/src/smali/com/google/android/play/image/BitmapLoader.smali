.class public Lcom/google/android/play/image/BitmapLoader;
.super Ljava/lang/Object;
.source "BitmapLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;,
        Lcom/google/android/play/image/BitmapLoader$BitmapContainer;,
        Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;,
        Lcom/google/android/play/image/BitmapLoader$DebugImageRequest;,
        Lcom/google/android/play/image/BitmapLoader$RemoteRequestCreator;
    }
.end annotation


# static fields
.field private static MIN_CACHE_SIZE_BYTES:I

.field private static MIN_NUM_IMAGES_IN_CACHE:I


# instance fields
.field private final mBatchedResponses:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private final mCachedRemoteImages:Lcom/google/android/play/image/BitmapCache;

.field private final mHandler:Landroid/os/Handler;

.field private final mInFlightRequests:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private final mMaxImageSizeToCacheInBytes:I

.field private final mRequestQueue:Lcom/android/volley/RequestQueue;

.field private mRunnable:Ljava/lang/Runnable;

.field private mTentativeGcRunner:Lcom/google/android/play/image/TentativeGcRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/high16 v0, 0x300000

    sput v0, Lcom/google/android/play/image/BitmapLoader;->MIN_CACHE_SIZE_BYTES:I

    .line 51
    const/4 v0, 0x6

    sput v0, Lcom/google/android/play/image/BitmapLoader;->MIN_NUM_IMAGES_IN_CACHE:I

    return-void
.end method

.method public constructor <init>(Lcom/android/volley/RequestQueue;IILcom/google/android/play/image/TentativeGcRunner;)V
    .locals 5
    .param p1, "queue"    # Lcom/android/volley/RequestQueue;
    .param p2, "screenWidthPx"    # I
    .param p3, "screenHeightPx"    # I
    .param p4, "tentativeGcRunner"    # Lcom/google/android/play/image/TentativeGcRunner;

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-static {}, Lcom/google/android/play/utils/collections/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/play/image/BitmapLoader;->mInFlightRequests:Ljava/util/HashMap;

    .line 63
    invoke-static {}, Lcom/google/android/play/utils/collections/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/play/image/BitmapLoader;->mBatchedResponses:Ljava/util/HashMap;

    .line 66
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/google/android/play/image/BitmapLoader;->mHandler:Landroid/os/Handler;

    .line 79
    iput-object p1, p0, Lcom/google/android/play/image/BitmapLoader;->mRequestQueue:Lcom/android/volley/RequestQueue;

    .line 80
    sget-object v2, Lcom/google/android/play/utils/config/PlayG;->bitmapLoaderCacheSizeOverrideMb:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 83
    .local v1, "gServicesCacheSizeMb":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 86
    sget v3, Lcom/google/android/play/image/BitmapLoader;->MIN_CACHE_SIZE_BYTES:I

    mul-int v2, p2, p3

    mul-int/lit8 v2, v2, 0x4

    int-to-float v4, v2

    sget-object v2, Lcom/google/android/play/utils/config/PlayG;->bitmapLoaderCacheSizeRatioToScreen:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    mul-float/2addr v2, v4

    float-to-int v2, v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 93
    .local v0, "cacheSizeInBytes":I
    :goto_0
    sget-object v2, Lcom/google/android/play/utils/config/PlayG;->minImageSizeLimitInLRUCacheBytes:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget v3, Lcom/google/android/play/image/BitmapLoader;->MIN_NUM_IMAGES_IN_CACHE:I

    div-int v3, v0, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/play/image/BitmapLoader;->mMaxImageSizeToCacheInBytes:I

    .line 95
    new-instance v2, Lcom/google/android/play/image/BitmapCache;

    invoke-direct {v2, v0}, Lcom/google/android/play/image/BitmapCache;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/play/image/BitmapLoader;->mCachedRemoteImages:Lcom/google/android/play/image/BitmapCache;

    .line 96
    iput-object p4, p0, Lcom/google/android/play/image/BitmapLoader;->mTentativeGcRunner:Lcom/google/android/play/image/TentativeGcRunner;

    .line 97
    return-void

    .line 89
    .end local v0    # "cacheSizeInBytes":I
    :cond_0
    mul-int/lit16 v2, v1, 0x400

    mul-int/lit16 v0, v2, 0x400

    .restart local v0    # "cacheSizeInBytes":I
    goto :goto_0
.end method

.method static synthetic access$100(Lcom/google/android/play/image/BitmapLoader;)Lcom/google/android/play/image/TentativeGcRunner;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/image/BitmapLoader;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader;->mTentativeGcRunner:Lcom/google/android/play/image/TentativeGcRunner;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/play/image/BitmapLoader;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/image/BitmapLoader;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/android/play/image/BitmapLoader;->mRunnable:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/play/image/BitmapLoader;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/image/BitmapLoader;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I
    .param p4, "x4"    # I
    .param p5, "x5"    # Landroid/graphics/Bitmap;

    .prologue
    .line 37
    invoke-direct/range {p0 .. p5}, Lcom/google/android/play/image/BitmapLoader;->onGetImageSuccess(Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/play/image/BitmapLoader;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/play/image/BitmapLoader;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/play/image/BitmapLoader;->onGetImageError(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/play/image/BitmapLoader;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/image/BitmapLoader;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader;->mInFlightRequests:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/play/image/BitmapLoader;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/play/image/BitmapLoader;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader;->mBatchedResponses:Ljava/util/HashMap;

    return-object v0
.end method

.method private batchResponse(Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;)V
    .locals 4
    .param p1, "modifiedUrl"    # Ljava/lang/String;
    .param p2, "wrapper"    # Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader;->mBatchedResponses:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 530
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader;->mRunnable:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 531
    new-instance v0, Lcom/google/android/play/image/BitmapLoader$2;

    invoke-direct {v0, p0}, Lcom/google/android/play/image/BitmapLoader$2;-><init>(Lcom/google/android/play/image/BitmapLoader;)V

    iput-object v0, p0, Lcom/google/android/play/image/BitmapLoader;->mRunnable:Ljava/lang/Runnable;

    .line 549
    iget-object v0, p0, Lcom/google/android/play/image/BitmapLoader;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/play/image/BitmapLoader;->mRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 551
    :cond_0
    return-void
.end method

.method private get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLcom/google/android/play/image/BitmapLoader$RemoteRequestCreator;Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    .locals 14
    .param p1, "requestUrl"    # Ljava/lang/String;
    .param p2, "modifiedUrl"    # Ljava/lang/String;
    .param p3, "cacheKey"    # Ljava/lang/String;
    .param p4, "requestWidth"    # I
    .param p5, "requestHeight"    # I
    .param p6, "useCachedPlaceholder"    # Z
    .param p7, "remoteRequestCreator"    # Lcom/google/android/play/image/BitmapLoader$RemoteRequestCreator;
    .param p8, "bitmapLoadedHandler"    # Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;

    .prologue
    .line 136
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 137
    new-instance v3, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v10, 0x0

    move-object v4, p0

    move/from16 v8, p4

    move/from16 v9, p5

    invoke-direct/range {v3 .. v10}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;-><init>(Lcom/google/android/play/image/BitmapLoader;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)V

    .line 178
    :goto_0
    return-object v3

    .line 141
    :cond_0
    iget-object v4, p0, Lcom/google/android/play/image/BitmapLoader;->mCachedRemoteImages:Lcom/google/android/play/image/BitmapCache;

    move-object/from16 v0, p3

    move/from16 v1, p4

    move/from16 v2, p5

    invoke-virtual {v4, v0, v1, v2}, Lcom/google/android/play/image/BitmapCache;->get(Ljava/lang/String;II)Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;

    move-result-object v11

    .line 143
    .local v11, "bitmapCacheEntry":Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;
    if-eqz v11, :cond_1

    iget v4, v11, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;->requestedWidth:I

    move/from16 v0, p4

    if-ne v4, v0, :cond_1

    iget v4, v11, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;->requestedHeight:I

    move/from16 v0, p5

    if-ne v4, v0, :cond_1

    .line 146
    new-instance v3, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    iget-object v5, v11, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;->bitmap:Landroid/graphics/Bitmap;

    const/4 v10, 0x0

    move-object v4, p0

    move-object v6, p1

    move-object/from16 v7, p2

    move/from16 v8, p4

    move/from16 v9, p5

    invoke-direct/range {v3 .. v10}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;-><init>(Lcom/google/android/play/image/BitmapLoader;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)V

    goto :goto_0

    .line 153
    :cond_1
    const/4 v5, 0x0

    .line 154
    .local v5, "placeholderBitmap":Landroid/graphics/Bitmap;
    if-eqz p6, :cond_2

    if-eqz v11, :cond_2

    .line 155
    iget-object v5, v11, Lcom/google/android/play/image/BitmapCache$BitmapCacheEntry;->bitmap:Landroid/graphics/Bitmap;

    .line 157
    :cond_2
    new-instance v3, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    move-object v4, p0

    move-object v6, p1

    move-object/from16 v7, p2

    move/from16 v8, p4

    move/from16 v9, p5

    move-object/from16 v10, p8

    invoke-direct/range {v3 .. v10}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;-><init>(Lcom/google/android/play/image/BitmapLoader;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)V

    .line 162
    .local v3, "bitmapContainer":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    iget-object v4, p0, Lcom/google/android/play/image/BitmapLoader;->mInFlightRequests:Ljava/util/HashMap;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;

    .line 163
    .local v13, "wrapper":Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;
    if-eqz v13, :cond_3

    .line 165
    invoke-virtual {v13, v3}, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->addHandler(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V

    goto :goto_0

    .line 170
    :cond_3
    invoke-interface/range {p7 .. p7}, Lcom/google/android/play/image/BitmapLoader$RemoteRequestCreator;->create()Lcom/android/volley/Request;

    move-result-object v12

    .line 172
    .local v12, "newRequest":Lcom/android/volley/Request;, "Lcom/android/volley/Request<*>;"
    iget-object v4, p0, Lcom/google/android/play/image/BitmapLoader;->mRequestQueue:Lcom/android/volley/RequestQueue;

    invoke-virtual {v4, v12}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 175
    iget-object v4, p0, Lcom/google/android/play/image/BitmapLoader;->mInFlightRequests:Ljava/util/HashMap;

    new-instance v6, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;

    invoke-direct {v6, p0, v12, v3}, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;-><init>(Lcom/google/android/play/image/BitmapLoader;Lcom/android/volley/Request;Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V

    move-object/from16 v0, p2

    invoke-virtual {v4, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private onGetImageError(Ljava/lang/String;)V
    .locals 5
    .param p1, "modifiedUrl"    # Ljava/lang/String;

    .prologue
    .line 364
    iget-object v2, p0, Lcom/google/android/play/image/BitmapLoader;->mInFlightRequests:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;

    .line 367
    .local v1, "wrapper":Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;
    if-eqz v1, :cond_0

    .line 369
    invoke-direct {p0, p1, v1}, Lcom/google/android/play/image/BitmapLoader;->batchResponse(Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;)V

    .line 370
    # getter for: Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->request:Lcom/android/volley/Request;
    invoke-static {v1}, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->access$000(Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;)Lcom/android/volley/Request;

    move-result-object v2

    if-eqz v2, :cond_1

    # getter for: Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->request:Lcom/android/volley/Request;
    invoke-static {v1}, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->access$000(Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;)Lcom/android/volley/Request;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/volley/Request;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 371
    .local v0, "url":Ljava/lang/String;
    :goto_0
    const-string v2, "Bitmap error %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/play/utils/PlayCommonLog;->logTiming(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 373
    .end local v0    # "url":Ljava/lang/String;
    :cond_0
    return-void

    .line 370
    :cond_1
    const-string v0, "<null request>"

    goto :goto_0
.end method

.method private onGetImageSuccess(Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/Bitmap;)V
    .locals 5
    .param p1, "modifiedUrl"    # Ljava/lang/String;
    .param p2, "cacheKey"    # Ljava/lang/String;
    .param p3, "requestWidth"    # I
    .param p4, "requestHeight"    # I
    .param p5, "response"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 343
    invoke-virtual {p5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p5}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v2

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/play/image/BitmapLoader;->mMaxImageSizeToCacheInBytes:I

    if-gt v1, v2, :cond_1

    .line 344
    iget-object v1, p0, Lcom/google/android/play/image/BitmapLoader;->mCachedRemoteImages:Lcom/google/android/play/image/BitmapCache;

    invoke-virtual {v1, p2, p3, p4, p5}, Lcom/google/android/play/image/BitmapCache;->put(Ljava/lang/String;IILandroid/graphics/Bitmap;)V

    .line 349
    :goto_0
    iget-object v1, p0, Lcom/google/android/play/image/BitmapLoader;->mInFlightRequests:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;

    .line 351
    .local v0, "wrapper":Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;
    if-eqz v0, :cond_0

    .line 353
    # setter for: Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->responseBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0, p5}, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->access$402(Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 356
    invoke-direct {p0, p1, v0}, Lcom/google/android/play/image/BitmapLoader;->batchResponse(Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;)V

    .line 357
    const-string v1, "Loaded bitmap %s"

    new-array v2, v3, [Ljava/lang/Object;

    # getter for: Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->request:Lcom/android/volley/Request;
    invoke-static {v0}, Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;->access$000(Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;)Lcom/android/volley/Request;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/volley/Request;->getUrl()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/play/utils/PlayCommonLog;->logTiming(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 359
    :cond_0
    return-void

    .line 346
    .end local v0    # "wrapper":Lcom/google/android/play/image/BitmapLoader$RequestListenerWrapper;
    :cond_1
    const-string v1, "%s is too large to cache"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/play/utils/PlayCommonLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method protected createImageRequest(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/google/android/play/image/BitmapLoader$DebugImageRequest;
    .locals 7
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "requestWidth"    # I
    .param p3, "requestHeight"    # I
    .param p4, "decodeConfig"    # Landroid/graphics/Bitmap$Config;
    .param p6, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Landroid/graphics/Bitmap$Config;",
            "Lcom/android/volley/Response$Listener",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")",
            "Lcom/google/android/play/image/BitmapLoader$DebugImageRequest;"
        }
    .end annotation

    .prologue
    .line 326
    .local p5, "bitmapListener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<Landroid/graphics/Bitmap;>;"
    new-instance v0, Lcom/google/android/play/image/BitmapLoader$DebugImageRequest;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/play/image/BitmapLoader$DebugImageRequest;-><init>(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    return-object v0
.end method

.method public get(Ljava/lang/String;IILcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    .locals 6
    .param p1, "requestUrl"    # Ljava/lang/String;
    .param p2, "requestWidth"    # I
    .param p3, "requestHeight"    # I
    .param p4, "bitmapLoadedHandler"    # Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;

    .prologue
    .line 336
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/play/image/BitmapLoader;->get(Ljava/lang/String;IIZLcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/String;IIZLcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    .locals 12
    .param p1, "requestUrl"    # Ljava/lang/String;
    .param p2, "requestWidth"    # I
    .param p3, "requestHeight"    # I
    .param p4, "useCachedPlaceholder"    # Z
    .param p5, "bitmapLoadedHandler"    # Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;

    .prologue
    .line 288
    move-object v11, p1

    .line 289
    .local v11, "modifiedUrl":Ljava/lang/String;
    if-gtz p2, :cond_0

    if-lez p3, :cond_1

    .line 290
    :cond_0
    invoke-static {p1, p2, p3}, Lcom/google/android/play/image/FifeUrlUtils;->buildFifeUrl(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v11

    .line 292
    :cond_1
    move-object v5, p1

    .line 293
    .local v5, "cacheKey":Ljava/lang/String;
    move-object v4, v11

    .line 295
    .local v4, "finalModifiedUrl":Ljava/lang/String;
    new-instance v0, Lcom/google/android/play/image/BitmapLoader$1;

    move-object v1, p0

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/image/BitmapLoader$1;-><init>(Lcom/google/android/play/image/BitmapLoader;IILjava/lang/String;Ljava/lang/String;)V

    .local v0, "remoteRequestCreator":Lcom/google/android/play/image/BitmapLoader$RemoteRequestCreator;
    move-object v2, p0

    move-object v3, p1

    move v6, p2

    move v7, p3

    move/from16 v8, p4

    move-object v9, v0

    move-object/from16 v10, p5

    .line 315
    invoke-direct/range {v2 .. v10}, Lcom/google/android/play/image/BitmapLoader;->get(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLcom/google/android/play/image/BitmapLoader$RemoteRequestCreator;Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    move-result-object v1

    return-object v1
.end method
