.class Lcom/google/android/play/transition/Expando$2;
.super Lcom/google/android/play/animation/BaseAnimatorListener;
.source "Expando.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/play/transition/Expando;->splashAnimator(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;JZ)Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/play/transition/Expando;

.field final synthetic val$isExpando:Z

.field final synthetic val$splashSnapshot:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/play/transition/Expando;ZLandroid/view/View;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcom/google/android/play/transition/Expando$2;->this$0:Lcom/google/android/play/transition/Expando;

    iput-boolean p2, p0, Lcom/google/android/play/transition/Expando$2;->val$isExpando:Z

    iput-object p3, p0, Lcom/google/android/play/transition/Expando$2;->val$splashSnapshot:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/play/animation/BaseAnimatorListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 318
    iget-boolean v0, p0, Lcom/google/android/play/transition/Expando$2;->val$isExpando:Z

    if-nez v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/google/android/play/transition/Expando$2;->val$splashSnapshot:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 321
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 311
    iget-boolean v0, p0, Lcom/google/android/play/transition/Expando$2;->val$isExpando:Z

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/google/android/play/transition/Expando$2;->val$splashSnapshot:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 314
    :cond_0
    return-void
.end method
