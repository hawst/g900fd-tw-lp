.class final Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;
.super Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;
.source "FlowLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/layout/FlowLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "InlineFlowLineInfo"
.end annotation


# static fields
.field private static final sPool:Landroid/support/v4/util/Pools$Pool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/Pools$Pool",
            "<",
            "Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;",
            ">;"
        }
    .end annotation
.end field

.field public mLineWidth:I

.field public mUsedWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1130
    new-instance v0, Landroid/support/v4/util/Pools$SimplePool;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/support/v4/util/Pools$SimplePool;-><init>(I)V

    sput-object v0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->sPool:Landroid/support/v4/util/Pools$Pool;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1162
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;-><init>(Lcom/google/android/play/layout/FlowLayoutManager$1;)V

    .line 1160
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    .line 1163
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->reset()V

    .line 1164
    return-void
.end method

.method private normalizeOffsetsAndAddMargins(ZZ)I
    .locals 10
    .param p1, "isFirstLine"    # Z
    .param p2, "isLastLine"    # Z

    .prologue
    const/4 v8, 0x0

    .line 1266
    iget-object v7, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    .line 1267
    .local v3, "itemCount":I
    if-nez v3, :cond_0

    move v7, v8

    .line 1298
    :goto_0
    return v7

    .line 1274
    :cond_0
    const v6, 0x7fffffff

    .line 1275
    .local v6, "lineTop":I
    const/high16 v5, -0x80000000

    .line 1276
    .local v5, "lineBottom":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v3, :cond_5

    .line 1277
    iget-object v7, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .line 1278
    .local v1, "item":Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    iget v9, v1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mTopOffset:I

    if-eqz p1, :cond_3

    iget v7, v1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginTopForFirstLine:I

    :goto_2
    sub-int v4, v9, v7

    .line 1280
    .local v4, "itemTop":I
    if-ge v4, v6, :cond_1

    .line 1281
    move v6, v4

    .line 1283
    :cond_1
    iget v7, v1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mTopOffset:I

    iget v9, v1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedHeight:I

    add-int/2addr v9, v7

    if-eqz p2, :cond_4

    iget v7, v1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginBottomForLastLine:I

    :goto_3
    add-int v2, v9, v7

    .line 1285
    .local v2, "itemBottom":I
    if-le v2, v5, :cond_2

    .line 1286
    move v5, v2

    .line 1276
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1278
    .end local v2    # "itemBottom":I
    .end local v4    # "itemTop":I
    :cond_3
    iget v7, v1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginTop:I

    goto :goto_2

    .line 1283
    .restart local v4    # "itemTop":I
    :cond_4
    iget v7, v1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginBottom:I

    goto :goto_3

    .line 1291
    .end local v1    # "item":Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    .end local v4    # "itemTop":I
    :cond_5
    if-eqz v6, :cond_7

    .line 1292
    const/4 v0, 0x0

    :goto_4
    if-ge v0, v3, :cond_6

    .line 1293
    iget-object v7, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v9, v7, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mTopOffset:I

    sub-int/2addr v9, v6

    iput v9, v7, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mTopOffset:I

    .line 1292
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1295
    :cond_6
    sub-int/2addr v5, v6

    .line 1298
    :cond_7
    invoke-static {v8, v5}, Ljava/lang/Math;->max(II)I

    move-result v7

    goto :goto_0
.end method

.method public static obtain(IIILcom/google/android/play/layout/FlowLayoutManager$ItemInfo;)Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;
    .locals 2
    .param p0, "positionStart"    # I
    .param p1, "lineWidth"    # I
    .param p2, "offsetStart"    # I
    .param p3, "measuredFirstItem"    # Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .prologue
    .line 1135
    sget-object v1, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->sPool:Landroid/support/v4/util/Pools$Pool;

    invoke-interface {v1}, Landroid/support/v4/util/Pools$Pool;->acquire()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;

    .line 1136
    .local v0, "line":Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;
    if-nez v0, :cond_0

    .line 1137
    new-instance v0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;

    .end local v0    # "line":Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;
    invoke-direct {v0}, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;-><init>()V

    .line 1139
    .restart local v0    # "line":Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;
    :cond_0
    iput p0, v0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mPositionStart:I

    .line 1140
    iput p2, v0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mOffsetStart:I

    .line 1141
    iput p1, v0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mLineWidth:I

    .line 1142
    invoke-virtual {v0, p3}, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->addMeasuredItem(Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;)V

    .line 1143
    return-object v0
.end method

.method private verticallyAlignItems()V
    .locals 12

    .prologue
    .line 1202
    iget-object v9, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v4

    .line 1205
    .local v4, "itemCount":I
    const/4 v7, 0x0

    .line 1206
    .local v7, "lineTop":I
    const/4 v6, 0x0

    .line 1207
    .local v6, "lineBottom":I
    const/high16 v8, -0x80000000

    .line 1208
    .local v8, "nonBaselineAlignedMaxHeight":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_3

    .line 1209
    iget-object v9, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .line 1210
    .local v2, "item":Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    iget v9, v2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mVAlign:I

    if-nez v9, :cond_2

    .line 1211
    iget v9, v2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mBaseline:I

    neg-int v5, v9

    .line 1212
    .local v5, "itemTop":I
    iput v5, v2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mTopOffset:I

    .line 1213
    if-ge v5, v7, :cond_0

    .line 1214
    move v7, v5

    .line 1216
    :cond_0
    iget v9, v2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedHeight:I

    add-int v3, v5, v9

    .line 1217
    .local v3, "itemBottom":I
    if-le v3, v6, :cond_1

    .line 1218
    move v6, v3

    .line 1208
    .end local v3    # "itemBottom":I
    .end local v5    # "itemTop":I
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1221
    :cond_2
    iget v9, v2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedHeight:I

    if-le v9, v8, :cond_1

    .line 1222
    iget v8, v2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedHeight:I

    goto :goto_1

    .line 1227
    .end local v2    # "item":Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    :cond_3
    const/high16 v9, -0x80000000

    if-ne v8, v9, :cond_5

    .line 1252
    :cond_4
    return-void

    .line 1234
    :cond_5
    sub-int v0, v6, v7

    .line 1235
    .local v0, "baselineLineHeight":I
    if-ge v0, v8, :cond_6

    .line 1236
    if-nez v0, :cond_7

    const/4 v7, 0x0

    .line 1238
    :goto_2
    add-int v6, v7, v8

    .line 1241
    :cond_6
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v4, :cond_4

    .line 1242
    iget-object v9, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .line 1243
    .restart local v2    # "item":Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    iget v9, v2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mVAlign:I

    packed-switch v9, :pswitch_data_0

    .line 1241
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1236
    .end local v2    # "item":Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    :cond_7
    int-to-float v9, v8

    int-to-float v10, v7

    int-to-float v11, v0

    div-float/2addr v10, v11

    mul-float/2addr v9, v10

    float-to-int v7, v9

    goto :goto_2

    .line 1245
    .restart local v2    # "item":Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    :pswitch_0
    iput v7, v2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mTopOffset:I

    goto :goto_4

    .line 1248
    :pswitch_1
    iget v9, v2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedHeight:I

    sub-int v9, v6, v9

    iput v9, v2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mTopOffset:I

    goto :goto_4

    .line 1243
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public addMeasuredItem(Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;)V
    .locals 3
    .param p1, "item"    # Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .prologue
    .line 1174
    iget-boolean v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMeasuredInCurrentPass:Z

    if-nez v0, :cond_0

    .line 1175
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Item not measured"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1177
    :cond_0
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mUsedWidth:I

    iget v1, p1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedWidth:I

    iget v2, p1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginStart:I

    add-int/2addr v1, v2

    iget v2, p1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginEnd:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mUsedWidth:I

    .line 1178
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->invalidateHeight()V

    .line 1180
    return-void
.end method

.method public clearMeasuredInCurrentPass()V
    .locals 3

    .prologue
    .line 1168
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 1169
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMeasuredInCurrentPass:Z

    .line 1168
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1171
    :cond_0
    return-void
.end method

.method debugPrint(ILjava/lang/StringBuilder;)V
    .locals 2
    .param p1, "lineIndex"    # I
    .param p2, "output"    # Ljava/lang/StringBuilder;

    .prologue
    .line 1343
    const/16 v0, 0x40

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mPositionStart:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->validPositionEnd()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1344
    return-void
.end method

.method public getItemTopOffset(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1303
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mPositionStart:I

    sub-int v1, p1, v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v0, v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mTopOffset:I

    return v0
.end method

.method protected invalidateFromInternal(I)Z
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 1308
    iget-object v4, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 1309
    .local v3, "itemCount":I
    iget v4, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mPositionStart:I

    add-int/2addr v4, v3

    if-gt v4, p1, :cond_0

    .line 1311
    const/4 v4, 0x0

    .line 1321
    :goto_0
    return v4

    .line 1314
    :cond_0
    iget v4, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mPositionStart:I

    sub-int v0, p1, v4

    .line 1315
    .local v0, "firstInvalidatedIndex":I
    add-int/lit8 v1, v3, -0x1

    .local v1, "i":I
    :goto_1
    if-lt v1, v0, :cond_1

    .line 1316
    iget-object v4, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .line 1318
    .local v2, "item":Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    iget v4, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mUsedWidth:I

    iget v5, v2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedWidth:I

    iget v6, v2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginStart:I

    add-int/2addr v5, v6

    iget v6, v2, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginEnd:I

    add-int/2addr v5, v6

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mUsedWidth:I

    .line 1319
    invoke-virtual {v2}, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->recycle()V

    .line 1315
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 1321
    .end local v2    # "item":Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    :cond_1
    const/4 v4, 0x1

    goto :goto_0
.end method

.method protected onArrange(ZI)I
    .locals 6
    .param p1, "fullArrangementRequired"    # Z
    .param p2, "totalItemCount"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1189
    if-eqz p1, :cond_0

    .line 1190
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->verticallyAlignItems()V

    .line 1192
    :cond_0
    iget v4, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mPositionStart:I

    if-nez v4, :cond_1

    move v0, v2

    .line 1193
    .local v0, "isFirstLine":Z
    :goto_0
    iget v4, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mPositionStart:I

    iget-object v5, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/2addr v4, v5

    if-ne v4, p2, :cond_2

    move v1, v2

    .line 1194
    .local v1, "isLastLine":Z
    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->normalizeOffsetsAndAddMargins(ZZ)I

    move-result v2

    return v2

    .end local v0    # "isFirstLine":Z
    .end local v1    # "isLastLine":Z
    :cond_1
    move v0, v3

    .line 1192
    goto :goto_0

    .restart local v0    # "isFirstLine":Z
    :cond_2
    move v1, v3

    .line 1193
    goto :goto_1
.end method

.method public recycle()V
    .locals 1

    .prologue
    .line 1326
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->reset()V

    .line 1327
    sget-object v0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->sPool:Landroid/support/v4/util/Pools$Pool;

    invoke-interface {v0, p0}, Landroid/support/v4/util/Pools$Pool;->release(Ljava/lang/Object;)Z

    .line 1328
    return-void
.end method

.method protected reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1332
    invoke-super {p0}, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->reset()V

    .line 1333
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mLineWidth:I

    .line 1334
    iput v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mUsedWidth:I

    .line 1335
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 1336
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    invoke-virtual {v1}, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->recycle()V

    .line 1335
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1338
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1339
    return-void
.end method

.method public validPositionEnd()I
    .locals 2

    .prologue
    .line 1184
    iget v0, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mPositionStart:I

    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
