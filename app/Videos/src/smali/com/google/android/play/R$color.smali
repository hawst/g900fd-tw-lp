.class public final Lcom/google/android/play/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final play_action_button_text:I = 0x7f0a00fc

.field public static final play_apps_primary_text:I = 0x7f0a00fd

.field public static final play_avatar_focused_outline:I = 0x7f0a0098

.field public static final play_avatar_outline:I = 0x7f0a0095

.field public static final play_avatar_pressed_fill:I = 0x7f0a0096

.field public static final play_avatar_pressed_outline:I = 0x7f0a0097

.field public static final play_books_primary_text:I = 0x7f0a00ff

.field public static final play_card_shadow_end_color:I = 0x7f0a00a2

.field public static final play_card_shadow_start_color:I = 0x7f0a00a1

.field public static final play_default_download_arc_color_offline:I = 0x7f0a0099

.field public static final play_disabled_grey:I = 0x7f0a0067

.field public static final play_dismissed_overlay:I = 0x7f0a008f

.field public static final play_fg_primary:I = 0x7f0a0071

.field public static final play_fg_secondary:I = 0x7f0a0072

.field public static final play_header_list_tab_text_color:I = 0x7f0a0101

.field public static final play_header_list_tab_underline_color:I = 0x7f0a009a

.field public static final play_main_background:I = 0x7f0a006b

.field public static final play_movies_primary_text:I = 0x7f0a0102

.field public static final play_multi_primary_text:I = 0x7f0a0104

.field public static final play_music_primary_text:I = 0x7f0a0106

.field public static final play_newsstand_primary_text:I = 0x7f0a0108

.field public static final play_onboard__page_indicator_dot_active:I = 0x7f0a00a8

.field public static final play_onboard__page_indicator_dot_inactive:I = 0x7f0a00a9

.field public static final play_onboard_accent_color_a:I = 0x7f0a00ae

.field public static final play_onboard_accent_color_b:I = 0x7f0a00af

.field public static final play_onboard_accent_color_c:I = 0x7f0a00b0

.field public static final play_onboard_accent_color_d:I = 0x7f0a00b1

.field public static final play_onboard_app_color:I = 0x7f0a00a6

.field public static final play_onboard_app_color_dark:I = 0x7f0a00a7

.field public static final play_onboard_simple_quiz_background:I = 0x7f0a00ad

.field public static final play_reason_separator:I = 0x7f0a006c

.field public static final play_search_plate_navigation_button_color:I = 0x7f0a00a4

.field public static final play_tab_strip_text_selected:I = 0x7f0a0070

.field public static final play_transparent:I = 0x7f0a0068

.field public static final play_white:I = 0x7f0a0063
