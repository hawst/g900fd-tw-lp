.class public Lcom/google/android/play/layout/PlayPopupMenu;
.super Ljava/lang/Object;
.source "PlayPopupMenu.java"

# interfaces
.implements Landroid/widget/PopupWindow$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;,
        Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;,
        Lcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;,
        Lcom/google/android/play/layout/PlayPopupMenu$OnActionSelectedListener;
    }
.end annotation


# instance fields
.field protected final mAnchor:Landroid/view/View;

.field protected final mContext:Landroid/content/Context;

.field private mOnPopupDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

.field protected final mPopupActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;",
            ">;"
        }
    .end annotation
.end field

.field protected mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "anchor"    # Landroid/view/View;

    .prologue
    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    iput-object p1, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mContext:Landroid/content/Context;

    .line 185
    iput-object p2, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mAnchor:Landroid/view/View;

    .line 186
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mPopupActions:Ljava/util/List;

    .line 187
    return-void
.end method

.method protected static measureContentWidth(Landroid/widget/ListAdapter;Landroid/content/Context;)I
    .locals 10
    .param p0, "listAdapter"    # Landroid/widget/ListAdapter;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x0

    .line 254
    const/4 v7, 0x0

    .line 255
    .local v7, "width":I
    const/4 v4, 0x0

    .line 256
    .local v4, "itemView":Landroid/view/View;
    const/4 v3, 0x0

    .line 257
    .local v3, "itemType":I
    new-instance v5, Landroid/widget/FrameLayout;

    invoke-direct {v5, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 258
    .local v5, "measureParent":Landroid/view/ViewGroup;
    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 260
    .local v8, "widthMeasureSpec":I
    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 262
    .local v1, "heightMeasureSpec":I
    invoke-interface {p0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    .line 263
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 264
    invoke-interface {p0, v2}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v6

    .line 265
    .local v6, "positionType":I
    if-eq v6, v3, :cond_0

    .line 266
    move v3, v6

    .line 267
    const/4 v4, 0x0

    .line 269
    :cond_0
    invoke-interface {p0, v2, v4, v5}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 270
    invoke-virtual {v4, v8, v1}, Landroid/view/View;->measure(II)V

    .line 271
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    invoke-static {v7, v9}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 263
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 273
    .end local v6    # "positionType":I
    :cond_1
    return v7
.end method


# virtual methods
.method public addMenuItem(ILjava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "title"    # Ljava/lang/CharSequence;
    .param p3, "isEnabled"    # Z
    .param p4, "onPopupActionSelectedListener"    # Lcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mPopupActions:Ljava/util/List;

    new-instance v1, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/android/play/layout/PlayPopupMenu$PopupAction;-><init>(ILjava/lang/CharSequence;ZLcom/google/android/play/layout/PlayPopupMenu$OnPopupActionSelectedListener;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/support/v7/widget/ListPopupWindow;->dismiss()V

    .line 298
    :cond_0
    return-void
.end method

.method public onDismiss()V
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    .line 280
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mOnPopupDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mOnPopupDismissListener:Landroid/widget/PopupWindow$OnDismissListener;

    invoke-interface {v0}, Landroid/widget/PopupWindow$OnDismissListener;->onDismiss()V

    .line 283
    :cond_0
    return-void
.end method

.method public show()V
    .locals 4

    .prologue
    .line 214
    new-instance v2, Landroid/support/v7/widget/ListPopupWindow;

    iget-object v3, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/support/v7/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    .line 216
    new-instance v0, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;

    iget-object v2, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mPopupActions:Ljava/util/List;

    invoke-direct {v0, v2, v3}, Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 217
    .local v0, "popupListAdapter":Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;
    iget-object v2, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 219
    iget-object v2, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    invoke-virtual {v2, p0}, Landroid/support/v7/widget/ListPopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 220
    iget-object v2, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    new-instance v3, Lcom/google/android/play/layout/PlayPopupMenu$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/play/layout/PlayPopupMenu$1;-><init>(Lcom/google/android/play/layout/PlayPopupMenu;Lcom/google/android/play/layout/PlayPopupMenu$PopupListAdapter;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 235
    iget-object v2, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    iget-object v3, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mAnchor:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 237
    iget-object v2, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v1, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 238
    .local v1, "screenWidth":I
    iget-object v2, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    iget-object v3, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mContext:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/google/android/play/layout/PlayPopupMenu;->measureContentWidth(Landroid/widget/ListAdapter;Landroid/content/Context;)I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/ListPopupWindow;->setContentWidth(I)V

    .line 241
    iget-object v2, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/ListPopupWindow;->setModal(Z)V

    .line 243
    iget-object v2, p0, Lcom/google/android/play/layout/PlayPopupMenu;->mPopupWindow:Landroid/support/v7/widget/ListPopupWindow;

    invoke-virtual {v2}, Landroid/support/v7/widget/ListPopupWindow;->show()V

    .line 244
    return-void
.end method
