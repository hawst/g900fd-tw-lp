.class public Lcom/google/android/play/utils/TextViewUtils;
.super Ljava/lang/Object;
.source "TextViewUtils.java"


# direct methods
.method private static setDrawable(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Z)V
    .locals 7
    .param p0, "view"    # Landroid/widget/TextView;
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "isStart"    # Z

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 37
    invoke-virtual {p0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 38
    .local v0, "drawables":[Landroid/graphics/drawable/Drawable;
    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->getLayoutDirection(Landroid/view/View;)I

    move-result v6

    if-nez v6, :cond_0

    move v2, v5

    .line 39
    .local v2, "isLtr":Z
    :goto_0
    if-ne v2, p2, :cond_1

    move v1, v3

    .line 40
    .local v1, "index":I
    :goto_1
    aput-object p1, v0, v1

    .line 41
    aget-object v3, v0, v3

    aget-object v5, v0, v5

    aget-object v4, v0, v4

    const/4 v6, 0x3

    aget-object v6, v0, v6

    invoke-virtual {p0, v3, v5, v4, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 43
    return-void

    .end local v1    # "index":I
    .end local v2    # "isLtr":Z
    :cond_0
    move v2, v3

    .line 38
    goto :goto_0

    .restart local v2    # "isLtr":Z
    :cond_1
    move v1, v4

    .line 39
    goto :goto_1
.end method

.method public static setDrawableEnd(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p0, "view"    # Landroid/widget/TextView;
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/play/utils/TextViewUtils;->setDrawable(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Z)V

    .line 34
    return-void
.end method

.method public static setDrawableStart(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p0, "view"    # Landroid/widget/TextView;
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 22
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/google/android/play/utils/TextViewUtils;->setDrawable(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Z)V

    .line 23
    return-void
.end method
