.class public Lcom/google/android/play/widget/UserAwareViewPager;
.super Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;
.source "UserAwareViewPager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/widget/UserAwareViewPager$UserAwareOnPageChangeListener;
    }
.end annotation


# instance fields
.field protected mFirstLayout:Z

.field private mIsSettingCurrentItem:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;-><init>(Landroid/content/Context;)V

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/widget/UserAwareViewPager;->mFirstLayout:Z

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/widget/UserAwareViewPager;->mFirstLayout:Z

    .line 30
    return-void
.end method


# virtual methods
.method public isSettingCurrentItem()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/android/play/widget/UserAwareViewPager;->mFirstLayout:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/play/widget/UserAwareViewPager;->mIsSettingCurrentItem:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->onAttachedToWindow()V

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/widget/UserAwareViewPager;->mFirstLayout:Z

    .line 36
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 46
    invoke-super/range {p0 .. p5}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->onLayout(ZIIII)V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/widget/UserAwareViewPager;->mFirstLayout:Z

    .line 48
    return-void
.end method

.method public setAdapter(Landroid/support/v4/view/PagerAdapter;)V
    .locals 1
    .param p1, "adapter"    # Landroid/support/v4/view/PagerAdapter;

    .prologue
    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/widget/UserAwareViewPager;->mFirstLayout:Z

    .line 41
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 42
    return-void
.end method

.method public setCurrentItem(I)V
    .locals 1
    .param p1, "item"    # I

    .prologue
    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/widget/UserAwareViewPager;->mIsSettingCurrentItem:Z

    .line 53
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->setCurrentItem(I)V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/widget/UserAwareViewPager;->mIsSettingCurrentItem:Z

    .line 55
    return-void
.end method

.method public setCurrentItem(IZ)V
    .locals 1
    .param p1, "item"    # I
    .param p2, "smoothScroll"    # Z

    .prologue
    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/widget/UserAwareViewPager;->mIsSettingCurrentItem:Z

    .line 60
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/bind/bidi/BidiAwareViewPager;->setCurrentItem(IZ)V

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/widget/UserAwareViewPager;->mIsSettingCurrentItem:Z

    .line 62
    return-void
.end method
