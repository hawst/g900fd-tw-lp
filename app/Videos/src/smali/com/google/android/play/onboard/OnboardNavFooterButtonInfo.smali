.class public Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
.super Ljava/lang/Object;
.source "OnboardNavFooterButtonInfo.java"


# instance fields
.field protected mClickRunnable:Ljava/lang/Runnable;

.field protected mEnabled:Z

.field protected mIconResId:I

.field protected mLabel:Ljava/lang/String;

.field protected mVisible:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-boolean v0, p0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->mVisible:Z

    .line 15
    iput-boolean v0, p0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->mEnabled:Z

    return-void
.end method


# virtual methods
.method public clone()Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 2

    .prologue
    .line 83
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 84
    :catch_0
    move-exception v0

    .line 85
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 12
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->clone()Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v0

    return-object v0
.end method

.method public getClickRunnable()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->mClickRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method public getIconResId()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->mIconResId:I

    return v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->mLabel:Ljava/lang/String;

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->mEnabled:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->mVisible:Z

    return v0
.end method

.method public setClickRunnable(Ljava/lang/Runnable;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 0
    .param p1, "clickRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->mClickRunnable:Ljava/lang/Runnable;

    .line 73
    return-object p0
.end method

.method public setIconResId(I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 0
    .param p1, "iconResId"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->mIconResId:I

    .line 64
    return-object p0
.end method

.method public setLabel(Landroid/content/Context;I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "stringResId"    # I

    .prologue
    .line 50
    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 51
    .local v0, "string":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setLabel(Ljava/lang/String;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    move-result-object v1

    return-object v1

    .line 50
    .end local v0    # "string":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setLabel(Ljava/lang/String;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 0
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->mLabel:Ljava/lang/String;

    .line 46
    return-object p0
.end method

.method public setVisible(Z)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 21
    iput-boolean p1, p0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->mVisible:Z

    .line 22
    return-object p0
.end method
