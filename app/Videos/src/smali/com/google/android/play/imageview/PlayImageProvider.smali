.class public Lcom/google/android/play/imageview/PlayImageProvider;
.super Ljava/lang/Object;
.source "PlayImageProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;,
        Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/google/android/play/imageview/PlayImageProvider;


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mManagedBitmaps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;",
            "Lcom/google/android/play/imageview/PlayManagedBitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mProviderPlugin:Lcom/google/android/play/imageview/PlayImageLoader;

.field private final mRequestListenerMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/imageview/PlayImageProvider;->mRequestListenerMap:Ljava/util/Map;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/imageview/PlayImageProvider;->mManagedBitmaps:Ljava/util/Map;

    .line 68
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 69
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/imageview/PlayImageProvider;->mHandler:Landroid/os/Handler;

    .line 74
    return-void

    .line 71
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PlayImageProvider must be initialized on the main thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private assertPluginConfigured()V
    .locals 2

    .prologue
    .line 106
    iget-object v1, p0, Lcom/google/android/play/imageview/PlayImageProvider;->mProviderPlugin:Lcom/google/android/play/imageview/PlayImageLoader;

    if-nez v1, :cond_0

    .line 107
    const-string v0, "PlayImageProviderPlugin must be configured before calls to PlayImageProvider can be completed"

    .line 109
    .local v0, "message":Ljava/lang/String;
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/play/utils/PlayCommonLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 112
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private cancelRequestCore(Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;)V
    .locals 4
    .param p1, "key"    # Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;
    .param p2, "requester"    # Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;

    .prologue
    .line 182
    sget-boolean v1, Lcom/google/android/play/utils/PlayCommonLog;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 183
    const-string v1, "requester=%s, key=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/play/utils/PlayCommonLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 185
    :cond_0
    monitor-enter p0

    .line 186
    :try_start_0
    iget-object v1, p0, Lcom/google/android/play/imageview/PlayImageProvider;->mRequestListenerMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 187
    invoke-direct {p0, p1}, Lcom/google/android/play/imageview/PlayImageProvider;->getListenersForKey(Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;)Ljava/util/List;

    move-result-object v0

    .line 188
    .local v0, "requestListeners":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;>;"
    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 189
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 190
    iget-object v1, p0, Lcom/google/android/play/imageview/PlayImageProvider;->mProviderPlugin:Lcom/google/android/play/imageview/PlayImageLoader;

    iget-object v2, p1, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;->imageToken:Ljava/lang/Object;

    invoke-interface {v1, v2}, Lcom/google/android/play/imageview/PlayImageLoader;->cancelRequest(Ljava/lang/Object;)V

    .line 193
    .end local v0    # "requestListeners":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;>;"
    :cond_1
    monitor-exit p0

    .line 194
    return-void

    .line 193
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static get()Lcom/google/android/play/imageview/PlayImageProvider;
    .locals 2

    .prologue
    .line 80
    sget-object v0, Lcom/google/android/play/imageview/PlayImageProvider;->sInstance:Lcom/google/android/play/imageview/PlayImageProvider;

    if-nez v0, :cond_1

    .line 81
    const-class v1, Lcom/google/android/play/imageview/PlayImageProvider;

    monitor-enter v1

    .line 82
    :try_start_0
    sget-object v0, Lcom/google/android/play/imageview/PlayImageProvider;->sInstance:Lcom/google/android/play/imageview/PlayImageProvider;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Lcom/google/android/play/imageview/PlayImageProvider;

    invoke-direct {v0}, Lcom/google/android/play/imageview/PlayImageProvider;-><init>()V

    sput-object v0, Lcom/google/android/play/imageview/PlayImageProvider;->sInstance:Lcom/google/android/play/imageview/PlayImageProvider;

    .line 85
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    :cond_1
    sget-object v0, Lcom/google/android/play/imageview/PlayImageProvider;->sInstance:Lcom/google/android/play/imageview/PlayImageProvider;

    return-object v0

    .line 85
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private getListenersForKey(Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;)Ljava/util/List;
    .locals 2
    .param p1, "key"    # Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v1, p0, Lcom/google/android/play/imageview/PlayImageProvider;->mRequestListenerMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 98
    .local v0, "listeners":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;>;"
    if-nez v0, :cond_0

    .line 99
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 100
    iget-object v1, p0, Lcom/google/android/play/imageview/PlayImageProvider;->mRequestListenerMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    :cond_0
    return-object v0
.end method

.method private loadImageCore(Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;)Z
    .locals 5
    .param p1, "key"    # Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;
    .param p2, "requester"    # Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;

    .prologue
    .line 115
    monitor-enter p0

    .line 116
    :try_start_0
    iget-object v3, p0, Lcom/google/android/play/imageview/PlayImageProvider;->mManagedBitmaps:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 117
    iget-object v3, p0, Lcom/google/android/play/imageview/PlayImageProvider;->mManagedBitmaps:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/imageview/PlayManagedBitmap;

    .line 118
    .local v0, "bitmap":Lcom/google/android/play/imageview/PlayManagedBitmap;
    invoke-virtual {v0}, Lcom/google/android/play/imageview/PlayManagedBitmap;->retain()V

    .line 119
    iget-object v3, p0, Lcom/google/android/play/imageview/PlayImageProvider;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/play/imageview/PlayImageProvider$1;

    invoke-direct {v4, p0, p2, v0}, Lcom/google/android/play/imageview/PlayImageProvider$1;-><init>(Lcom/google/android/play/imageview/PlayImageProvider;Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;Lcom/google/android/play/imageview/PlayManagedBitmap;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 125
    const/4 v1, 0x0

    monitor-exit p0

    .line 134
    .end local v0    # "bitmap":Lcom/google/android/play/imageview/PlayManagedBitmap;
    :goto_0
    return v1

    .line 127
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/play/imageview/PlayImageProvider;->getListenersForKey(Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;)Ljava/util/List;

    move-result-object v2

    .line 132
    .local v2, "requestListeners":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    .line 133
    .local v1, "loadNeeded":Z
    invoke-interface {v2, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    monitor-exit p0

    goto :goto_0

    .line 135
    .end local v1    # "loadNeeded":Z
    .end local v2    # "requestListeners":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;>;"
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method


# virtual methods
.method public cancelRequest(Ljava/lang/Object;Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;)V
    .locals 3
    .param p1, "imageToken"    # Ljava/lang/Object;
    .param p2, "requester"    # Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;

    .prologue
    const/4 v2, 0x0

    .line 211
    invoke-direct {p0}, Lcom/google/android/play/imageview/PlayImageProvider;->assertPluginConfigured()V

    .line 212
    new-instance v0, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v2, v2, v1}, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;-><init>(Ljava/lang/Object;IILcom/google/android/play/imageview/PlayImageProvider$1;)V

    .line 213
    .local v0, "key":Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;
    invoke-direct {p0, v0, p2}, Lcom/google/android/play/imageview/PlayImageProvider;->cancelRequestCore(Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;)V

    .line 214
    return-void
.end method

.method public loadImage(Ljava/lang/Object;Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;)V
    .locals 6
    .param p1, "imageToken"    # Ljava/lang/Object;
    .param p2, "requester"    # Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;

    .prologue
    const/4 v5, 0x0

    .line 145
    invoke-direct {p0}, Lcom/google/android/play/imageview/PlayImageProvider;->assertPluginConfigured()V

    .line 146
    sget-boolean v2, Lcom/google/android/play/utils/PlayCommonLog;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 147
    const-string v2, "imageToken=%s, requester=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v5

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/play/utils/PlayCommonLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    :cond_0
    new-instance v1, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v5, v5, v2}, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;-><init>(Ljava/lang/Object;IILcom/google/android/play/imageview/PlayImageProvider$1;)V

    .line 150
    .local v1, "key":Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;
    invoke-direct {p0, v1, p2}, Lcom/google/android/play/imageview/PlayImageProvider;->loadImageCore(Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;)Z

    move-result v0

    .line 151
    .local v0, "doLoad":Z
    if-eqz v0, :cond_1

    .line 152
    iget-object v2, p0, Lcom/google/android/play/imageview/PlayImageProvider;->mProviderPlugin:Lcom/google/android/play/imageview/PlayImageLoader;

    invoke-interface {v2, p1, v5, v5}, Lcom/google/android/play/imageview/PlayImageLoader;->loadImage(Ljava/lang/Object;II)V

    .line 154
    :cond_1
    return-void
.end method

.method public releaseImage(Lcom/google/android/play/imageview/PlayManagedBitmap;)V
    .locals 9
    .param p1, "managedBitmap"    # Lcom/google/android/play/imageview/PlayManagedBitmap;

    .prologue
    .line 251
    invoke-direct {p0}, Lcom/google/android/play/imageview/PlayImageProvider;->assertPluginConfigured()V

    .line 252
    monitor-enter p0

    .line 253
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/play/imageview/PlayManagedBitmap;->release()Z

    move-result v4

    .line 254
    .local v4, "shouldRecycle":Z
    sget-boolean v5, Lcom/google/android/play/utils/PlayCommonLog;->DEBUG:Z

    if-eqz v5, :cond_0

    .line 255
    const-string v5, "managedBitmap=%s, will recycle? %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/play/utils/PlayCommonLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 257
    :cond_0
    if-eqz v4, :cond_3

    .line 258
    new-instance v3, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;

    const/4 v5, 0x0

    invoke-direct {v3, p1, v5}, Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;-><init>(Lcom/google/android/play/imageview/PlayManagedBitmap;Lcom/google/android/play/imageview/PlayImageProvider$1;)V

    .line 259
    .local v3, "key":Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;
    iget-object v5, p0, Lcom/google/android/play/imageview/PlayImageProvider;->mManagedBitmaps:Ljava/util/Map;

    invoke-interface {v5, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/imageview/PlayManagedBitmap;

    .line 260
    .local v0, "bitmap":Lcom/google/android/play/imageview/PlayManagedBitmap;
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 261
    const-string v5, "managedBitmap != mManagedBitmaps.get(key). Recycling both. managedBitmap=%s, bitmap=%s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/play/utils/PlayCommonLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 264
    iget-object v5, p0, Lcom/google/android/play/imageview/PlayImageProvider;->mProviderPlugin:Lcom/google/android/play/imageview/PlayImageLoader;

    iget-object v6, v0, Lcom/google/android/play/imageview/PlayManagedBitmap;->imageToken:Ljava/lang/Object;

    iget-object v7, v0, Lcom/google/android/play/imageview/PlayManagedBitmap;->bitmap:Landroid/graphics/Bitmap;

    invoke-interface {v5, v6, v7}, Lcom/google/android/play/imageview/PlayImageLoader;->recycleImage(Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    .line 266
    :cond_1
    iget-object v5, v0, Lcom/google/android/play/imageview/PlayManagedBitmap;->relatedBitmaps:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    .local v1, "i":I
    move v2, v1

    .line 267
    .end local v1    # "i":I
    .local v2, "i":I
    :goto_0
    iget-object v5, v0, Lcom/google/android/play/imageview/PlayManagedBitmap;->relatedBitmaps:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 268
    iget-object v5, v0, Lcom/google/android/play/imageview/PlayManagedBitmap;->relatedBitmaps:Ljava/util/List;

    add-int/lit8 v1, v2, -0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-interface {v5, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    move v2, v1

    .line 274
    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 275
    :cond_2
    iget-object v5, p0, Lcom/google/android/play/imageview/PlayImageProvider;->mProviderPlugin:Lcom/google/android/play/imageview/PlayImageLoader;

    iget-object v6, p1, Lcom/google/android/play/imageview/PlayManagedBitmap;->imageToken:Ljava/lang/Object;

    iget-object v7, p1, Lcom/google/android/play/imageview/PlayManagedBitmap;->bitmap:Landroid/graphics/Bitmap;

    invoke-interface {v5, v6, v7}, Lcom/google/android/play/imageview/PlayImageLoader;->recycleImage(Ljava/lang/Object;Landroid/graphics/Bitmap;)V

    .line 277
    .end local v0    # "bitmap":Lcom/google/android/play/imageview/PlayManagedBitmap;
    .end local v2    # "i":I
    .end local v3    # "key":Lcom/google/android/play/imageview/PlayImageProvider$ManagedBitmapsKey;
    :cond_3
    monitor-exit p0

    .line 278
    return-void

    .line 277
    .end local v4    # "shouldRecycle":Z
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method
