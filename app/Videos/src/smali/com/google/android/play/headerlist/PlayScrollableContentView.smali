.class public interface abstract Lcom/google/android/play/headerlist/PlayScrollableContentView;
.super Ljava/lang/Object;
.source "PlayScrollableContentView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;
    }
.end annotation


# virtual methods
.method public abstract getAdapter()Landroid/widget/Adapter;
.end method

.method public abstract scrollBy(II)V
.end method

.method public abstract scrollTo(II)V
.end method

.method public abstract setOnScrollListener(Lcom/google/android/play/headerlist/PlayScrollableContentView$OnScrollListener;)V
.end method
