.class public final Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ClientAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/analytics/ClientAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LogResponse"
.end annotation


# instance fields
.field public experiments:Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

.field public nextRequestWaitMillis:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2240
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2241
    invoke-virtual {p0}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->clear()Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;

    .line 2242
    return-void
.end method

.method public static parseFrom([B)Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 2309
    new-instance v0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;

    invoke-direct {v0}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;

    return-object v0
.end method


# virtual methods
.method public clear()Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;
    .locals 2

    .prologue
    .line 2245
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->nextRequestWaitMillis:J

    .line 2246
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->experiments:Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    .line 2247
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->cachedSize:I

    .line 2248
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 2265
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2266
    .local v0, "size":I
    iget-wide v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->nextRequestWaitMillis:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 2267
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->nextRequestWaitMillis:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2270
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->experiments:Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    if-eqz v1, :cond_1

    .line 2271
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->experiments:Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2274
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2282
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2283
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2287
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2288
    :sswitch_0
    return-object p0

    .line 2293
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->nextRequestWaitMillis:J

    goto :goto_0

    .line 2297
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->experiments:Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    if-nez v1, :cond_1

    .line 2298
    new-instance v1, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    invoke-direct {v1}, Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;-><init>()V

    iput-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->experiments:Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    .line 2300
    :cond_1
    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->experiments:Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2283
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2217
    invoke-virtual {p0, p1}, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2254
    iget-wide v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->nextRequestWaitMillis:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 2255
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->nextRequestWaitMillis:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 2257
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->experiments:Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    if-eqz v0, :cond_1

    .line 2258
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/play/analytics/ClientAnalytics$LogResponse;->experiments:Lcom/google/android/play/analytics/ClientAnalytics$ExperimentIdList;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2260
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2261
    return-void
.end method
