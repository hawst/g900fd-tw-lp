.class public Lcom/google/android/play/layout/FlowLayoutManager;
.super Landroid/support/v7/widget/RecyclerView$LayoutManager;
.source "FlowLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/play/layout/FlowLayoutManager$SavedState;,
        Lcom/google/android/play/layout/FlowLayoutManager$AutoRegisteredOnViewRenderedListener;,
        Lcom/google/android/play/layout/FlowLayoutManager$OnViewRenderedListener;,
        Lcom/google/android/play/layout/FlowLayoutManager$FillState;,
        Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;,
        Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;,
        Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;,
        Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;,
        Lcom/google/android/play/layout/FlowLayoutManager$MultiItemInfo;,
        Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;,
        Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    }
.end annotation


# static fields
.field private static final sDecorInsets:Landroid/graphics/Rect;


# instance fields
.field private mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

.field private mItemChangesAffectFlow:Z

.field private mPendingSavedState:Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

.field private mPendingScrollPosition:I

.field private mPendingScrollPositionOffset:I

.field private final mSections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mTrueAnchorAtPositionZero:Z

.field private mViewRenderedListener:Lcom/google/android/play/layout/FlowLayoutManager$OnViewRenderedListener;

.field private mWasViewRenderedListenerAutoRegistered:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2214
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/play/layout/FlowLayoutManager;->sDecorInsets:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$LayoutManager;-><init>()V

    .line 1771
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    .line 3426
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPosition:I

    .line 3431
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPositionOffset:I

    .line 3552
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingSavedState:Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    return-void
.end method

.method private addLineToParagraph(Landroid/support/v7/widget/RecyclerView$Recycler;Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;IIIIIZI)I
    .locals 15
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "paragraph"    # Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    .param p3, "nextSectionStart"    # I
    .param p4, "totalItemCount"    # I
    .param p5, "fullContextWidth"    # I
    .param p6, "lineWidth"    # I
    .param p7, "lineOffsetStart"    # I
    .param p8, "inNestedFlow"    # Z
    .param p9, "remainingFlowHeight"    # I

    .prologue
    .line 2699
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->validPositionEnd()I

    move-result v4

    .line 2700
    .local v4, "nextPosition":I
    move/from16 v0, p3

    if-lt v4, v0, :cond_1

    .line 2701
    move/from16 v0, p3

    if-le v4, v0, :cond_0

    .line 2702
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\u00b6@["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    iget v5, v0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ","

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ") should not cover nextSectionStart@"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    move v14, v4

    .line 2735
    .end local v4    # "nextPosition":I
    .local v14, "nextPosition":I
    :goto_0
    return v14

    .line 2708
    .end local v14    # "nextPosition":I
    .restart local v4    # "nextPosition":I
    :cond_1
    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    if-ne v2, v4, :cond_2

    const/4 v10, 0x1

    :goto_1
    move-object v2, p0

    move-object/from16 v3, p1

    move/from16 v5, p5

    move/from16 v7, p6

    move/from16 v9, p7

    move/from16 v11, p8

    move/from16 v12, p9

    invoke-direct/range {v2 .. v12}, Lcom/google/android/play/layout/FlowLayoutManager;->measureNextItem(Landroid/support/v7/widget/RecyclerView$Recycler;IILjava/util/List;IIIZZI)Z

    move-result v2

    if-nez v2, :cond_3

    move v14, v4

    .line 2711
    .end local v4    # "nextPosition":I
    .restart local v14    # "nextPosition":I
    goto :goto_0

    .line 2708
    .end local v14    # "nextPosition":I
    .restart local v4    # "nextPosition":I
    :cond_2
    const/4 v10, 0x0

    goto :goto_1

    .line 2715
    :cond_3
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget-object v13, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItemLayoutParams:Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    .line 2716
    .local v13, "layoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    iget v2, v13, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flow:I

    if-nez v2, :cond_4

    .line 2718
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    invoke-virtual {v2}, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->takeNextItem()Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    move-result-object v2

    move/from16 v0, p6

    move/from16 v1, p7

    invoke-static {v4, v0, v1, v2}, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->obtain(IIILcom/google/android/play/layout/FlowLayoutManager$ItemInfo;)Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;

    move-result-object v7

    .local v7, "line":Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;
    move-object v5, p0

    move-object/from16 v6, p1

    move/from16 v8, p3

    move/from16 v9, p5

    move/from16 v10, p8

    move/from16 v11, p9

    .line 2720
    invoke-direct/range {v5 .. v11}, Lcom/google/android/play/layout/FlowLayoutManager;->fillInlineFlowLine(Landroid/support/v7/widget/RecyclerView$Recycler;Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;IIZI)I

    move-result v4

    .line 2722
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->addLine(Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;)V

    .end local v7    # "line":Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;
    :goto_2
    move v14, v4

    .line 2735
    .end local v4    # "nextPosition":I
    .restart local v14    # "nextPosition":I
    goto :goto_0

    .line 2725
    .end local v14    # "nextPosition":I
    .restart local v4    # "nextPosition":I
    :cond_4
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    invoke-virtual {v2}, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->takeNextItem()Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget-object v3, v3, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItemLayoutParams:Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    move/from16 v0, p6

    move/from16 v1, p7

    invoke-static {v4, v0, v1, v2, v3}, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->obtain(IIILcom/google/android/play/layout/FlowLayoutManager$ItemInfo;Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;)Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;

    move-result-object v7

    .local v7, "line":Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;
    move-object v5, p0

    move-object/from16 v6, p1

    move/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    .line 2727
    invoke-direct/range {v5 .. v10}, Lcom/google/android/play/layout/FlowLayoutManager;->fillNestedFlowLine(Landroid/support/v7/widget/RecyclerView$Recycler;Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;III)I

    move-result v4

    .line 2729
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->addLine(Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;)V

    goto :goto_2
.end method

.method private debugPrintChildren()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 2190
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildCount()I

    move-result v1

    .line 2191
    .local v1, "childCount":I
    const-string v5, "FlowLayoutManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "current child list: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " child(ren)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2192
    if-lez v1, :cond_0

    .line 2193
    invoke-virtual {p0, v8}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/support/v7/widget/RecyclerView;

    .line 2194
    .local v4, "recyclerView":Landroid/support/v7/widget/RecyclerView;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2195
    .local v3, "output":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 2196
    const-string v5, "  #"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x40

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2197
    invoke-virtual {p0, v2}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2198
    .local v0, "child":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x2c

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2200
    const-string v5, "FlowLayoutManager"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2201
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2195
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2204
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "i":I
    .end local v3    # "output":Ljava/lang/StringBuilder;
    .end local v4    # "recyclerView":Landroid/support/v7/widget/RecyclerView;
    :cond_0
    return-void
.end method

.method private debugPrintSections()V
    .locals 9

    .prologue
    const/16 v8, 0x3a

    const/4 v7, 0x0

    .line 1951
    iget-object v4, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 1952
    .local v3, "sectionCount":I
    const-string v4, "FlowLayoutManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Layout in bookkeeping: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " section(s)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1953
    if-lez v3, :cond_2

    .line 1954
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1955
    .local v1, "output":Ljava/lang/StringBuilder;
    iget-object v4, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    .line 1956
    .local v2, "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    const-string v4, "  \u00a70@"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1957
    iget v4, v2, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    if-nez v4, :cond_0

    .line 1958
    iget-boolean v4, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mTrueAnchorAtPositionZero:Z

    if-eqz v4, :cond_1

    const-string v4, "(real)"

    :goto_0
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1960
    :cond_0
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1961
    invoke-virtual {v2, v1}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->debugPrint(Ljava/lang/StringBuilder;)V

    .line 1962
    const-string v4, "FlowLayoutManager"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1963
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    if-ge v0, v3, :cond_2

    .line 1964
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1965
    iget-object v4, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    check-cast v2, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    .line 1966
    .restart local v2    # "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    const-string v4, "  \u00a7"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x40

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1968
    invoke-virtual {v2, v1}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->debugPrint(Ljava/lang/StringBuilder;)V

    .line 1969
    const-string v4, "FlowLayoutManager"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1963
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1958
    .end local v0    # "i":I
    :cond_1
    const-string v4, "(fake)"

    goto :goto_0

    .line 1972
    .end local v1    # "output":Ljava/lang/StringBuilder;
    .end local v2    # "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :cond_2
    return-void
.end method

.method private fillDownForHeight(Landroid/support/v7/widget/RecyclerView$Recycler;III)I
    .locals 13
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "startSectionIndex"    # I
    .param p3, "height"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 2436
    move v9, p2

    .line 2437
    .local v9, "lastSectionIndex":I
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    .line 2438
    .local v3, "lastSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    move/from16 v5, p3

    .line 2439
    .local v5, "remainingHeight":I
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->resetFillState()V

    .line 2441
    :goto_0
    if-lez v5, :cond_3

    .line 2442
    add-int/lit8 v1, v9, 0x1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v11, 0x0

    .line 2444
    .local v11, "nextSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :goto_1
    if-nez v11, :cond_1

    move/from16 v6, p4

    .line 2446
    .local v6, "nextSectionStart":I
    :goto_2
    const/4 v4, -0x1

    move-object v1, p0

    move-object v2, p1

    move/from16 v7, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/play/layout/FlowLayoutManager;->fillSection(Landroid/support/v7/widget/RecyclerView$Recycler;Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;IIII)V

    .line 2448
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget v1, v1, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mHeightFilled:I

    sub-int/2addr v5, v1

    .line 2449
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget v10, v1, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextAnchorPosition:I

    .line 2450
    .local v10, "nextAnchorPosition":I
    const/4 v1, -0x1

    if-eq v10, v1, :cond_2

    .line 2452
    invoke-static {v10}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->obtain(I)Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    move-result-object v3

    .line 2453
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    add-int/lit8 v9, v9, 0x1

    invoke-interface {v1, v9, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2467
    .end local v6    # "nextSectionStart":I
    .end local v10    # "nextAnchorPosition":I
    .end local v11    # "nextSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :catch_0
    move-exception v8

    .line 2469
    .local v8, "e":Ljava/lang/RuntimeException;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fillDownForHeight() states at exception:\n\t startSectionIndex="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\t height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\t totalItemCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\t remainingHeight="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\t lastSectionIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\t lastSection="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 2477
    .local v12, "output":Ljava/lang/StringBuilder;
    if-nez v3, :cond_5

    .line 2478
    const-string v1, "null"

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2482
    :goto_3
    const-string v1, "\n\t mFillState="

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2483
    const-string v1, "FlowLayoutManager"

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2485
    throw v8

    .line 2442
    .end local v8    # "e":Ljava/lang/RuntimeException;
    .end local v12    # "output":Ljava/lang/StringBuilder;
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    add-int/lit8 v2, v9, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    move-object v11, v1

    goto/16 :goto_1

    .line 2444
    .restart local v11    # "nextSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :cond_1
    iget v6, v11, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    goto/16 :goto_2

    .line 2458
    .restart local v6    # "nextSectionStart":I
    .restart local v10    # "nextAnchorPosition":I
    :cond_2
    invoke-virtual {v3}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->validPositionEnd()I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v1

    move/from16 v0, p4

    if-ne v1, v0, :cond_4

    .line 2487
    .end local v6    # "nextSectionStart":I
    .end local v10    # "nextAnchorPosition":I
    .end local v11    # "nextSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :cond_3
    return v5

    .line 2463
    .restart local v6    # "nextSectionStart":I
    .restart local v10    # "nextAnchorPosition":I
    .restart local v11    # "nextSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :cond_4
    add-int/lit8 v9, v9, 0x1

    .line 2464
    move-object v3, v11

    goto/16 :goto_0

    .line 2480
    .end local v6    # "nextSectionStart":I
    .end local v10    # "nextAnchorPosition":I
    .end local v11    # "nextSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    .restart local v8    # "e":Ljava/lang/RuntimeException;
    .restart local v12    # "output":Ljava/lang/StringBuilder;
    :cond_5
    invoke-virtual {v3, v12}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->debugPrint(Ljava/lang/StringBuilder;)V

    goto :goto_3
.end method

.method private fillExistingLine(Landroid/support/v7/widget/RecyclerView$Recycler;Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;IIIZI)I
    .locals 7
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "line"    # Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;
    .param p3, "nextSectionStart"    # I
    .param p4, "totalItemCount"    # I
    .param p5, "fullContextWidth"    # I
    .param p6, "inNestedFlow"    # Z
    .param p7, "remainingFlowHeight"    # I

    .prologue
    .line 2585
    instance-of v0, p2, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;

    if-eqz v0, :cond_0

    move-object v2, p2

    .line 2586
    check-cast v2, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p5

    move v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/play/layout/FlowLayoutManager;->fillInlineFlowLine(Landroid/support/v7/widget/RecyclerView$Recycler;Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;IIZI)I

    move-result v0

    .line 2589
    :goto_0
    return v0

    :cond_0
    move-object v2, p2

    check-cast v2, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/layout/FlowLayoutManager;->fillNestedFlowLine(Landroid/support/v7/widget/RecyclerView$Recycler;Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;III)I

    move-result v0

    goto :goto_0
.end method

.method private fillInlineFlowLine(Landroid/support/v7/widget/RecyclerView$Recycler;Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;IIZI)I
    .locals 11
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "line"    # Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;
    .param p3, "nextSectionStart"    # I
    .param p4, "fullContextWidth"    # I
    .param p5, "inNestedFlow"    # Z
    .param p6, "remainingFlowHeight"    # I

    .prologue
    .line 2597
    iget-object v0, p2, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2598
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Line must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2600
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->validPositionEnd()I

    move-result v2

    .line 2602
    .local v2, "nextPosition":I
    :goto_0
    if-ge v2, p3, :cond_1

    iget v0, p2, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mLineWidth:I

    iget v1, p2, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mUsedWidth:I

    sub-int/2addr v0, v1

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 2603
    iget-object v4, p2, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    iget v5, p2, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mLineWidth:I

    iget v6, p2, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mUsedWidth:I

    iget v7, p2, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mOffsetStart:I

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p4

    move/from16 v9, p5

    move/from16 v10, p6

    invoke-direct/range {v0 .. v10}, Lcom/google/android/play/layout/FlowLayoutManager;->measureNextItem(Landroid/support/v7/widget/RecyclerView$Recycler;IILjava/util/List;IIIZZI)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2611
    :cond_1
    return v2

    .line 2608
    :cond_2
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    invoke-virtual {v0}, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->takeNextItem()Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->addMeasuredItem(Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;)V

    .line 2609
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private fillNestedFlowLine(Landroid/support/v7/widget/RecyclerView$Recycler;Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;III)I
    .locals 23
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "line"    # Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;
    .param p3, "nextSectionStart"    # I
    .param p4, "totalItemCount"    # I
    .param p5, "fullContextWidth"    # I

    .prologue
    .line 2616
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    if-nez v2, :cond_0

    .line 2617
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Line must not be empty"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2620
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->validPositionEnd()I

    move-result v21

    .line 2621
    .local v21, "oldValidPositionEnd":I
    move/from16 v18, v21

    .line 2622
    .local v18, "nextPosition":I
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    .line 2623
    .local v4, "nestedParagraph":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    if-nez v4, :cond_6

    .line 2628
    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowWidth:I

    if-eqz v2, :cond_1

    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowHeight:I

    if-nez v2, :cond_2

    :cond_1
    move/from16 v19, v18

    .line 2683
    .end local v18    # "nextPosition":I
    .local v19, "nextPosition":I
    :goto_0
    return v19

    .line 2635
    .end local v19    # "nextPosition":I
    .restart local v18    # "nextPosition":I
    :cond_2
    invoke-static/range {v18 .. v18}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->obtain(I)Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    move-result-object v4

    .line 2636
    move-object/from16 v0, p2

    iget v8, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowWidth:I

    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mOffsetStart:I

    move-object/from16 v0, p2

    iget v3, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowStartOffset:I

    add-int v9, v2, v3

    const/4 v10, 0x1

    move-object/from16 v0, p2

    iget v11, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowHeight:I

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    invoke-direct/range {v2 .. v11}, Lcom/google/android/play/layout/FlowLayoutManager;->addLineToParagraph(Landroid/support/v7/widget/RecyclerView$Recycler;Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;IIIIIZI)I

    move-result v18

    .line 2641
    iget v2, v4, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    move/from16 v0, v18

    if-ne v0, v2, :cond_3

    .line 2642
    invoke-virtual {v4}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->recycle()V

    move/from16 v19, v18

    .line 2646
    .end local v18    # "nextPosition":I
    .restart local v19    # "nextPosition":I
    goto :goto_0

    .line 2648
    .end local v19    # "nextPosition":I
    .restart local v18    # "nextPosition":I
    :cond_3
    move-object/from16 v0, p2

    iput-object v4, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    .line 2667
    :cond_4
    :goto_1
    move/from16 v20, v18

    .line 2668
    .local v20, "oldNextPosition":I
    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowHeight:I

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->arrangeIfNecessary(I)I

    move-result v3

    sub-int v17, v2, v3

    .line 2670
    .local v17, "remainingFlowHeight":I
    move-object/from16 v0, p2

    iget v14, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowWidth:I

    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mOffsetStart:I

    move-object/from16 v0, p2

    iget v3, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowStartOffset:I

    add-int v15, v2, v3

    const/16 v16, 0x1

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object v10, v4

    move/from16 v11, p3

    move/from16 v12, p4

    move/from16 v13, p5

    invoke-direct/range {v8 .. v17}, Lcom/google/android/play/layout/FlowLayoutManager;->addLineToParagraph(Landroid/support/v7/widget/RecyclerView$Recycler;Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;IIIIIZI)I

    move-result v18

    .line 2673
    move/from16 v0, v18

    move/from16 v1, v20

    if-gt v0, v1, :cond_4

    .line 2679
    move/from16 v0, v18

    move/from16 v1, v21

    if-le v0, v1, :cond_5

    .line 2680
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->invalidateHeight()V

    :cond_5
    move/from16 v19, v18

    .line 2683
    .end local v18    # "nextPosition":I
    .restart local v19    # "nextPosition":I
    goto :goto_0

    .line 2651
    .end local v17    # "remainingFlowHeight":I
    .end local v19    # "nextPosition":I
    .end local v20    # "oldNextPosition":I
    .restart local v18    # "nextPosition":I
    :cond_6
    move/from16 v0, p4

    invoke-virtual {v4, v0}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->arrangeIfNecessary(I)I

    move-result v22

    .line 2652
    .local v22, "paragraphHeight":I
    invoke-virtual {v4}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->getLastLine()Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;

    move-result-object v7

    .line 2653
    .local v7, "lastLine":Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;
    if-nez v7, :cond_7

    .line 2654
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Empty nested paragraph found!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2656
    :cond_7
    iget v2, v7, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->mTotalHeight:I

    sub-int v22, v22, v2

    .line 2657
    const/4 v11, 0x1

    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowHeight:I

    sub-int v12, v2, v22

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v8, p3

    move/from16 v9, p4

    move/from16 v10, p5

    invoke-direct/range {v5 .. v12}, Lcom/google/android/play/layout/FlowLayoutManager;->fillExistingLine(Landroid/support/v7/widget/RecyclerView$Recycler;Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;IIIZI)I

    move-result v18

    .line 2659
    move/from16 v0, v18

    move/from16 v1, v21

    if-le v0, v1, :cond_4

    .line 2660
    invoke-virtual {v4}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->invalidateHeight()V

    goto :goto_1
.end method

.method private fillSection(Landroid/support/v7/widget/RecyclerView$Recycler;Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;IIII)V
    .locals 20
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "section"    # Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    .param p3, "positionToCover"    # I
    .param p4, "minHeight"    # I
    .param p5, "nextSectionStart"    # I
    .param p6, "totalItemCount"    # I

    .prologue
    .line 2517
    const/4 v2, -0x1

    move/from16 v0, p3

    if-ne v0, v2, :cond_0

    if-gtz p4, :cond_0

    .line 2518
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Both criteria met before any processing"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2519
    :cond_0
    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    move/from16 v0, p5

    if-lt v2, v0, :cond_1

    .line 2520
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Section started after limit"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2521
    :cond_1
    move/from16 v0, p3

    move/from16 v1, p5

    if-ge v0, v1, :cond_2

    move/from16 v0, p5

    move/from16 v1, p6

    if-le v0, v1, :cond_3

    .line 2522
    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "positionToCover < nextSectionStart <= totalItemCount does not hold"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2532
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getWidth()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getPaddingRight()I

    move-result v3

    sub-int v7, v2, v3

    .line 2533
    .local v7, "fullContextWidth":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    const/4 v3, -0x1

    iput v3, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextAnchorPosition:I

    .line 2536
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    move-object/from16 v0, p2

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->arrangeIfNecessary(I)I

    move-result v3

    iput v3, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mHeightFilled:I

    .line 2537
    move-object/from16 v0, p2

    iget v0, v0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    move/from16 v19, v0

    .line 2538
    .local v19, "nextPosition":I
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->getLastLine()Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;

    move-result-object v4

    .line 2539
    .local v4, "lastLine":Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;
    if-eqz v4, :cond_7

    .line 2540
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget v3, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mHeightFilled:I

    iget v5, v4, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->mTotalHeight:I

    sub-int/2addr v3, v5

    iput v3, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mHeightFilled:I

    .line 2541
    iget v0, v4, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->mPositionStart:I

    move/from16 v19, v0

    .line 2543
    move/from16 v0, v19

    move/from16 v1, p3

    if-le v0, v1, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget v2, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mHeightFilled:I

    move/from16 v0, p4

    if-lt v2, v0, :cond_5

    .line 2575
    :cond_4
    :goto_0
    return-void

    .line 2547
    :cond_5
    invoke-virtual {v4}, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->validPositionEnd()I

    move-result v18

    .line 2548
    .local v18, "lastValidPositionEnd":I
    const/4 v8, 0x0

    const/4 v9, -0x1

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v5, p5

    move/from16 v6, p6

    invoke-direct/range {v2 .. v9}, Lcom/google/android/play/layout/FlowLayoutManager;->fillExistingLine(Landroid/support/v7/widget/RecyclerView$Recycler;Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;IIIZI)I

    move-result v19

    .line 2550
    move/from16 v0, v19

    move/from16 v1, v18

    if-eq v0, v1, :cond_6

    .line 2552
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->invalidateHeight()V

    .line 2558
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    move-object/from16 v0, p2

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->arrangeIfNecessary(I)I

    move-result v3

    iput v3, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mHeightFilled:I

    .line 2563
    .end local v18    # "lastValidPositionEnd":I
    :cond_7
    :goto_1
    move/from16 v0, v19

    move/from16 v1, p3

    if-le v0, v1, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget v2, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mHeightFilled:I

    move/from16 v0, p4

    if-ge v2, v0, :cond_9

    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget v2, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextAnchorPosition:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_9

    move/from16 v0, v19

    move/from16 v1, p5

    if-ge v0, v1, :cond_9

    .line 2564
    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, -0x1

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move/from16 v11, p5

    move/from16 v12, p6

    move v13, v7

    move v14, v7

    invoke-direct/range {v8 .. v17}, Lcom/google/android/play/layout/FlowLayoutManager;->addLineToParagraph(Landroid/support/v7/widget/RecyclerView$Recycler;Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;IIIIIZI)I

    move-result v19

    .line 2566
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    move-object/from16 v0, p2

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->arrangeIfNecessary(I)I

    move-result v3

    iput v3, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mHeightFilled:I

    goto :goto_1

    .line 2571
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget v2, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextAnchorPosition:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4

    move/from16 v0, v19

    move/from16 v1, p3

    if-le v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget v2, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mHeightFilled:I

    move/from16 v0, p4

    if-lt v2, v0, :cond_4

    .line 2573
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    const/4 v3, -0x1

    iput v3, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextAnchorPosition:I

    goto/16 :goto_0
.end method

.method private fillUpForPosition(Landroid/support/v7/widget/RecyclerView$Recycler;III)I
    .locals 19
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "sectionIndex"    # I
    .param p3, "position"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 2340
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_0

    const/4 v4, 0x0

    .line 2341
    .local v4, "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :goto_0
    if-eqz v4, :cond_1

    iget v2, v4, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    move/from16 v0, p3

    if-le v2, v0, :cond_1

    .line 2342
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Section at "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " impossible to cover position "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2340
    .end local v4    # "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    move/from16 v0, p2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    move-object v4, v2

    goto :goto_0

    .line 2345
    .restart local v4    # "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :cond_1
    add-int/lit8 v2, p2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    add-int/lit8 v3, p2, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    iget v2, v2, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    move/from16 v0, p3

    if-lt v0, v2, :cond_2

    .line 2347
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Section at "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " impossible to cover position "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2351
    :cond_2
    if-nez v4, :cond_3

    const/16 v18, 0x0

    .line 2352
    .local v18, "validPositionEnd":I
    :goto_1
    move/from16 v0, v18

    move/from16 v1, p3

    if-le v0, v1, :cond_4

    .line 2353
    move/from16 v0, p4

    invoke-virtual {v4, v0}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->arrangeIfNecessary(I)I

    move/from16 v17, p2

    .line 2410
    .end local p2    # "sectionIndex":I
    .local v17, "sectionIndex":I
    :goto_2
    return v17

    .line 2351
    .end local v17    # "sectionIndex":I
    .end local v18    # "validPositionEnd":I
    .restart local p2    # "sectionIndex":I
    :cond_3
    invoke-virtual {v4}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->validPositionEnd()I

    move-result v18

    goto :goto_1

    .line 2362
    .restart local v18    # "validPositionEnd":I
    :cond_4
    const/4 v15, -0x1

    .line 2363
    .local v15, "positionHint":I
    const/4 v10, -0x1

    .line 2364
    .local v10, "childIndex":I
    const/16 v16, 0x1

    .line 2365
    .local v16, "reusingExistingSection":Z
    move/from16 v12, p3

    .local v12, "childPosition":I
    :goto_3
    move/from16 v0, v18

    if-lt v12, v0, :cond_6

    .line 2366
    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v12, v15, v10}, Lcom/google/android/play/layout/FlowLayoutManager;->getOrAddChildWithHint(Landroid/support/v7/widget/RecyclerView$Recycler;III)I

    move-result v10

    .line 2368
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 2370
    .local v9, "child":Landroid/view/View;
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    .line 2371
    .local v11, "childLayoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    invoke-virtual {v11}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->isAnchorCandidate()Z

    move-result v14

    .line 2372
    .local v14, "isAnchor":Z
    if-nez v14, :cond_5

    if-nez v12, :cond_7

    .line 2374
    :cond_5
    invoke-static {v12}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->obtain(I)Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    move-result-object v4

    .line 2375
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    add-int/lit8 p2, p2, 0x1

    move/from16 v0, p2

    invoke-interface {v2, v0, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2376
    const/16 v16, 0x0

    .line 2381
    if-nez v12, :cond_6

    .line 2382
    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mTrueAnchorAtPositionZero:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2406
    .end local v9    # "child":Landroid/view/View;
    .end local v11    # "childLayoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    .end local v14    # "isAnchor":Z
    :cond_6
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->resetFillState()V

    .line 2407
    add-int/lit8 v2, p2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v2, v3, :cond_8

    move/from16 v7, p4

    .line 2409
    .local v7, "nextSectionStart":I
    :goto_4
    const/4 v6, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v5, p3

    move/from16 v8, p4

    invoke-direct/range {v2 .. v8}, Lcom/google/android/play/layout/FlowLayoutManager;->fillSection(Landroid/support/v7/widget/RecyclerView$Recycler;Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;IIII)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    move/from16 v17, p2

    .line 2410
    .end local p2    # "sectionIndex":I
    .restart local v17    # "sectionIndex":I
    goto :goto_2

    .line 2386
    .end local v7    # "nextSectionStart":I
    .end local v17    # "sectionIndex":I
    .restart local v9    # "child":Landroid/view/View;
    .restart local v11    # "childLayoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    .restart local v14    # "isAnchor":Z
    .restart local p2    # "sectionIndex":I
    :cond_7
    move v15, v12

    .line 2365
    add-int/lit8 v12, v12, -0x1

    goto :goto_3

    .line 2394
    .end local v9    # "child":Landroid/view/View;
    .end local v11    # "childLayoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    .end local v14    # "isAnchor":Z
    :catch_0
    move-exception v13

    .line 2396
    .local v13, "e":Ljava/lang/RuntimeException;
    const-string v2, "FlowLayoutManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fillUpForPosition() state at exception: finding anchor\n\r sectionIndex="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\n\r position="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\n\r totalItemCount="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2401
    throw v13

    .line 2407
    .end local v13    # "e":Ljava/lang/RuntimeException;
    :cond_8
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    add-int/lit8 v3, p2, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    iget v7, v2, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_4

    .line 2411
    :catch_1
    move-exception v13

    .line 2413
    .restart local v13    # "e":Ljava/lang/RuntimeException;
    const-string v2, "FlowLayoutManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fillUpForPosition() state at exception: filling section\n\r sectionIndex="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\n\r position="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\n\r totalItemCount="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\n\r mFillState="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2419
    throw v13
.end method

.method private findChildIndexByPosition(I)I
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 1986
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildCount()I

    move-result v0

    .line 1987
    .local v0, "childCount":I
    if-eqz v0, :cond_0

    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/play/layout/FlowLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v5

    if-le v5, p1, :cond_2

    .line 1988
    :cond_0
    const/4 v2, -0x1

    .line 2006
    :cond_1
    :goto_0
    return v2

    .line 1989
    :cond_2
    add-int/lit8 v5, v0, -0x1

    invoke-virtual {p0, v5}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/play/layout/FlowLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v5

    if-ge v5, p1, :cond_3

    .line 1990
    xor-int/lit8 v2, v0, -0x1

    goto :goto_0

    .line 1993
    :cond_3
    const/4 v4, 0x0

    .line 1994
    .local v4, "startIndex":I
    move v1, v0

    .line 1995
    .local v1, "endIndex":I
    :goto_1
    if-ge v4, v1, :cond_5

    .line 1996
    add-int v5, v4, v1

    div-int/lit8 v2, v5, 0x2

    .line 1997
    .local v2, "midIndex":I
    invoke-virtual {p0, v2}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/play/layout/FlowLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v3

    .line 1998
    .local v3, "midPosition":I
    if-eq v3, p1, :cond_1

    .line 2000
    if-ge v3, p1, :cond_4

    .line 2001
    add-int/lit8 v4, v2, 0x1

    goto :goto_1

    .line 2003
    :cond_4
    move v1, v2

    goto :goto_1

    .line 2006
    .end local v2    # "midIndex":I
    .end local v3    # "midPosition":I
    :cond_5
    xor-int/lit8 v2, v4, -0x1

    goto :goto_0
.end method

.method private findSectionIndexByPosition(I)I
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 1789
    iget-object v5, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    .line 1791
    .local v3, "sectionCount":I
    if-eqz v3, :cond_0

    iget-object v5, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    iget v5, v5, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    if-le v5, p1, :cond_2

    .line 1792
    :cond_0
    const/4 v1, -0x1

    .line 1810
    :cond_1
    :goto_0
    return v1

    .line 1793
    :cond_2
    iget-object v5, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    add-int/lit8 v6, v3, -0x1

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    invoke-virtual {v5}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->validPositionEnd()I

    move-result v5

    if-gt v5, p1, :cond_3

    .line 1794
    xor-int/lit8 v1, v3, -0x1

    goto :goto_0

    .line 1797
    :cond_3
    const/4 v4, 0x0

    .line 1798
    .local v4, "startIndex":I
    move v0, v3

    .line 1799
    .local v0, "endIndex":I
    :goto_1
    if-ge v4, v0, :cond_5

    .line 1800
    add-int v5, v4, v0

    div-int/lit8 v1, v5, 0x2

    .line 1801
    .local v1, "midIndex":I
    iget-object v5, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    .line 1802
    .local v2, "midSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    iget v5, v2, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    if-ge p1, v5, :cond_4

    .line 1803
    move v0, v1

    goto :goto_1

    .line 1804
    :cond_4
    invoke-virtual {v2}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->validPositionEnd()I

    move-result v5

    if-lt p1, v5, :cond_1

    .line 1805
    add-int/lit8 v4, v1, 0x1

    goto :goto_1

    .line 1810
    .end local v1    # "midIndex":I
    .end local v2    # "midSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :cond_5
    xor-int/lit8 v1, v4, -0x1

    goto :goto_0
.end method

.method private getNextChildIntoFillState(Landroid/support/v7/widget/RecyclerView$Recycler;II)Landroid/view/View;
    .locals 4
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "position"    # I
    .param p3, "fullContextWidth"    # I

    .prologue
    .line 2293
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget v2, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItemPosition:I

    iget-object v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget v3, v3, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItemChildIndex:I

    invoke-direct {p0, p1, p2, v2, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->getOrAddChildWithHint(Landroid/support/v7/widget/RecyclerView$Recycler;III)I

    move-result v1

    .line 2295
    .local v1, "childIndex":I
    invoke-virtual {p0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2296
    .local v0, "child":Landroid/view/View;
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget v2, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItemPosition:I

    if-ne v2, p2, :cond_1

    .line 2298
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget v2, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItemChildIndex:I

    if-eq v2, v1, :cond_0

    .line 2299
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Cached next child index incorrect"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2300
    :cond_0
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget-object v2, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItem:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    if-nez v2, :cond_3

    .line 2301
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Cached next child missing ItemInfo"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2304
    :cond_1
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iput p2, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItemPosition:I

    .line 2305
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iput v1, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItemChildIndex:I

    .line 2306
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget-object v2, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItem:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    if-eqz v2, :cond_2

    .line 2308
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Did not consume previous ItemInfo"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2313
    :cond_2
    iget-object v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    iput-object v2, v3, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItemLayoutParams:Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    .line 2314
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    invoke-static {}, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->obtain()Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItem:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .line 2315
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget-object v2, v2, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItem:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget-object v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    iget-object v3, v3, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItemLayoutParams:Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    invoke-virtual {v2, v3, p3}, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->loadParams(Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;I)V

    .line 2317
    :cond_3
    return-object v0
.end method

.method private getOrAddChildWithHint(Landroid/support/v7/widget/RecyclerView$Recycler;III)I
    .locals 21
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "position"    # I
    .param p3, "positionHint"    # I
    .param p4, "indexHint"    # I

    .prologue
    .line 2037
    sub-int v18, p2, p3

    packed-switch v18, :pswitch_data_0

    .line 2062
    const/4 v11, -0x1

    .line 2063
    .local v11, "potentialIndex":I
    const/4 v10, -0x1

    .line 2066
    .local v10, "insertIndex":I
    :goto_0
    if-ltz v11, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildCount()I

    move-result v18

    move/from16 v0, v18

    if-ge v11, v0, :cond_3

    .line 2067
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v14

    .line 2068
    .local v14, "realChildPosition":I
    move/from16 v0, p2

    if-ne v14, v0, :cond_0

    move/from16 v18, v11

    .line 2170
    .end local v14    # "realChildPosition":I
    :goto_1
    return v18

    .line 2042
    .end local v10    # "insertIndex":I
    .end local v11    # "potentialIndex":I
    :pswitch_0
    move/from16 v11, p4

    .line 2043
    .restart local v11    # "potentialIndex":I
    move/from16 v10, p4

    .line 2044
    .restart local v10    # "insertIndex":I
    goto :goto_0

    .line 2049
    .end local v10    # "insertIndex":I
    .end local v11    # "potentialIndex":I
    :pswitch_1
    add-int/lit8 v11, p4, -0x1

    .line 2050
    .restart local v11    # "potentialIndex":I
    move/from16 v10, p4

    .line 2051
    .restart local v10    # "insertIndex":I
    goto :goto_0

    .line 2057
    .end local v10    # "insertIndex":I
    .end local v11    # "potentialIndex":I
    :pswitch_2
    add-int/lit8 v11, p4, 0x1

    .line 2058
    .restart local v11    # "potentialIndex":I
    add-int/lit8 v10, p4, 0x1

    .line 2059
    .restart local v10    # "insertIndex":I
    goto :goto_0

    .line 2077
    .restart local v14    # "realChildPosition":I
    :cond_0
    if-ne v11, v10, :cond_1

    const/16 v18, 0x1

    move/from16 v19, v18

    :goto_2
    move/from16 v0, p2

    if-le v14, v0, :cond_2

    const/16 v18, 0x1

    :goto_3
    move/from16 v0, v19

    move/from16 v1, v18

    if-eq v0, v1, :cond_3

    .line 2079
    new-instance v18, Ljava/lang/IllegalArgumentException;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Wrong hint precondition.\n\t position="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t positionHint="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t indexHint="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t potentialIndex="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t insertIndex="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t realChildPosition(potentialIndex)="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 2077
    :cond_1
    const/16 v18, 0x0

    move/from16 v19, v18

    goto :goto_2

    :cond_2
    const/16 v18, 0x0

    goto :goto_3

    .line 2093
    .end local v14    # "realChildPosition":I
    :cond_3
    const/16 v16, 0x0

    .line 2094
    .local v16, "usedBinarySearch":Z
    if-gez v10, :cond_5

    .line 2095
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->findChildIndexByPosition(I)I

    move-result v11

    .line 2096
    const/16 v16, 0x1

    .line 2097
    if-ltz v11, :cond_4

    move/from16 v18, v11

    .line 2101
    goto/16 :goto_1

    .line 2103
    :cond_4
    xor-int/lit8 v10, v11, -0x1

    .line 2106
    :cond_5
    invoke-virtual/range {p1 .. p2}, Landroid/support/v7/widget/RecyclerView$Recycler;->getViewForPosition(I)Landroid/view/View;

    move-result-object v3

    .line 2109
    .local v3, "child":Landroid/view/View;
    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v2

    .line 2110
    .local v2, "actualPosition":I
    move/from16 v0, p2

    if-eq v2, v0, :cond_6

    .line 2111
    new-instance v18, Ljava/lang/IllegalStateException;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Recycler.getViewForPosition("

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ") returned a view @"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v18
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2116
    .end local v2    # "actualPosition":I
    :catch_0
    move-exception v7

    .line 2119
    .local v7, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    .line 2120
    .local v4, "childLayoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    if-nez v4, :cond_7

    const-string v17, "failed: no LayoutParams"

    .line 2122
    .local v17, "viewHolderDump":Ljava/lang/String;
    :goto_4
    const-string v18, "FlowLayoutManager"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "getOrAddChildWithHint() states at exception:\n\t position="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t positionHint="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t indexHint="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t potentialIndex="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t insertIndex="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t usedBinarySearch="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t child\'s viewHolderDump="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2131
    throw v7

    .line 2115
    .end local v4    # "childLayoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    .end local v7    # "e":Ljava/lang/RuntimeException;
    .end local v17    # "viewHolderDump":Ljava/lang/String;
    .restart local v2    # "actualPosition":I
    :cond_6
    :try_start_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v10}, Lcom/google/android/play/layout/FlowLayoutManager;->addView(Landroid/view/View;I)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2136
    const/16 v18, 0x0

    add-int/lit8 v19, v10, -0x1

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->max(II)I

    move-result v15

    .line 2137
    .local v15, "startIndex":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2138
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v12

    .line 2139
    .local v12, "previousPosition":I
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    check-cast v18, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->debugGetViewHolderDump()Ljava/lang/String;

    move-result-object v13

    .line 2141
    .local v13, "previousViewHolderDump":Ljava/lang/String;
    add-int/lit8 v18, v10, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildCount()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 2142
    .local v8, "endIndex":I
    add-int/lit8 v9, v15, 0x1

    .local v9, "i":I
    :goto_5
    if-gt v9, v8, :cond_9

    .line 2143
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 2144
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v5

    .line 2145
    .local v5, "currentPosition":I
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    check-cast v18, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->debugGetViewHolderDump()Ljava/lang/String;

    move-result-object v6

    .line 2147
    .local v6, "currentViewHolderDump":Ljava/lang/String;
    if-gt v5, v12, :cond_8

    .line 2148
    new-instance v18, Ljava/lang/IllegalStateException;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Index/position monotonicity broken!\n\t position="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t positionHint="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t indexHint="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t potentialIndex="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t insertIndex="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t usedBinarySearch="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t p(childAt("

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    add-int/lit8 v20, v9, -0x1

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "))="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t   viewHolderDump="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t p(childAt("

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "))="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "\n\t   viewHolderDump="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 2120
    .end local v2    # "actualPosition":I
    .end local v5    # "currentPosition":I
    .end local v6    # "currentViewHolderDump":Ljava/lang/String;
    .end local v8    # "endIndex":I
    .end local v9    # "i":I
    .end local v12    # "previousPosition":I
    .end local v13    # "previousViewHolderDump":Ljava/lang/String;
    .end local v15    # "startIndex":I
    .restart local v4    # "childLayoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    .restart local v7    # "e":Ljava/lang/RuntimeException;
    :cond_7
    invoke-virtual {v4}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->debugGetViewHolderDump()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_4

    .line 2160
    .end local v4    # "childLayoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    .end local v7    # "e":Ljava/lang/RuntimeException;
    .restart local v2    # "actualPosition":I
    .restart local v5    # "currentPosition":I
    .restart local v6    # "currentViewHolderDump":Ljava/lang/String;
    .restart local v8    # "endIndex":I
    .restart local v9    # "i":I
    .restart local v12    # "previousPosition":I
    .restart local v13    # "previousViewHolderDump":Ljava/lang/String;
    .restart local v15    # "startIndex":I
    :cond_8
    move v12, v5

    .line 2161
    move-object v13, v6

    .line 2142
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_5

    .end local v5    # "currentPosition":I
    .end local v6    # "currentViewHolderDump":Ljava/lang/String;
    :cond_9
    move/from16 v18, v10

    .line 2170
    goto/16 :goto_1

    .line 2037
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private getReferenceChild()Landroid/view/View;
    .locals 11

    .prologue
    .line 3563
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getHeight()I

    move-result v7

    .line 3564
    .local v7, "height":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildCount()I

    move-result v4

    .line 3565
    .local v4, "childCount":I
    const/4 v0, 0x0

    .line 3566
    .local v0, "alternative":Landroid/view/View;
    const v6, 0x7fffffff

    .line 3567
    .local v6, "distanceOfAlternative":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v4, :cond_4

    .line 3568
    invoke-virtual {p0, v8}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 3569
    .local v3, "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    invoke-virtual {v10}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->isItemRemoved()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 3567
    :cond_0
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 3572
    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->getDecoratedTop(Landroid/view/View;)I

    move-result v9

    .line 3573
    .local v9, "top":I
    invoke-virtual {p0, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->getDecoratedBottom(Landroid/view/View;)I

    move-result v1

    .line 3574
    .local v1, "bottom":I
    add-int v10, v9, v1

    div-int/lit8 v2, v10, 0x2

    .line 3575
    .local v2, "center":I
    if-ltz v2, :cond_2

    if-gt v2, v7, :cond_2

    .line 3597
    .end local v1    # "bottom":I
    .end local v2    # "center":I
    .end local v3    # "child":Landroid/view/View;
    .end local v9    # "top":I
    :goto_2
    return-object v3

    .line 3582
    .restart local v1    # "bottom":I
    .restart local v2    # "center":I
    .restart local v3    # "child":Landroid/view/View;
    .restart local v9    # "top":I
    :cond_2
    if-gez v2, :cond_3

    neg-int v5, v2

    .line 3583
    .local v5, "distance":I
    :goto_3
    if-ge v5, v6, :cond_0

    .line 3584
    move-object v0, v3

    .line 3585
    move v6, v5

    goto :goto_1

    .line 3582
    .end local v5    # "distance":I
    :cond_3
    sub-int v5, v2, v7

    goto :goto_3

    .end local v1    # "bottom":I
    .end local v2    # "center":I
    .end local v3    # "child":Landroid/view/View;
    .end local v9    # "top":I
    :cond_4
    move-object v3, v0

    .line 3597
    goto :goto_2
.end method

.method private handleAutoRegisteredOnViewRenderedLister(Landroid/support/v7/widget/RecyclerView$Adapter;)V
    .locals 1
    .param p1, "newAdapter"    # Landroid/support/v7/widget/RecyclerView$Adapter;

    .prologue
    .line 2999
    iget-boolean v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mWasViewRenderedListenerAutoRegistered:Z

    if-eqz v0, :cond_0

    .line 3000
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->setOnViewRenderedListener(Lcom/google/android/play/layout/FlowLayoutManager$OnViewRenderedListener;)Lcom/google/android/play/layout/FlowLayoutManager;

    .line 3002
    :cond_0
    instance-of v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$AutoRegisteredOnViewRenderedListener;

    if-eqz v0, :cond_1

    .line 3003
    check-cast p1, Lcom/google/android/play/layout/FlowLayoutManager$AutoRegisteredOnViewRenderedListener;

    .end local p1    # "newAdapter":Landroid/support/v7/widget/RecyclerView$Adapter;
    iput-object p1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mViewRenderedListener:Lcom/google/android/play/layout/FlowLayoutManager$OnViewRenderedListener;

    .line 3004
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mWasViewRenderedListenerAutoRegistered:Z

    .line 3006
    :cond_1
    return-void
.end method

.method private invalidateAndOffsetPositions(III)V
    .locals 6
    .param p1, "positionStart"    # I
    .param p2, "positionEnd"    # I
    .param p3, "offset"    # I

    .prologue
    .line 1901
    iget-object v4, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1948
    :cond_0
    :goto_0
    return-void

    .line 1907
    :cond_1
    const/4 v1, 0x0

    .line 1908
    .local v1, "lastOffsetSectionIndex":I
    iget-object v4, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_4

    .line 1909
    iget-object v4, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    .line 1910
    .local v3, "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    iget v4, v3, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    if-lt v4, p2, :cond_3

    iget v4, v3, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    if-gtz v4, :cond_2

    iget-boolean v4, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mTrueAnchorAtPositionZero:Z

    if-eqz v4, :cond_3

    .line 1912
    :cond_2
    invoke-virtual {v3, p3}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->offsetPositions(I)V

    .line 1908
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 1914
    :cond_3
    add-int/lit8 v1, v0, 0x1

    .line 1928
    .end local v3    # "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :cond_4
    const/4 v2, 0x0

    .line 1929
    .local v2, "lastRemovedSectionIndex":I
    add-int/lit8 v0, v1, -0x1

    :goto_2
    if-ltz v0, :cond_0

    .line 1930
    iget-object v4, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    .line 1931
    .restart local v3    # "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    invoke-virtual {v3, p1}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->invalidateFrom(I)I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_5

    .line 1932
    invoke-direct {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->removeSectionAt(I)V

    .line 1929
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 1934
    :cond_5
    add-int/lit8 v2, v0, 0x1

    .line 1939
    goto :goto_0
.end method

.method private layoutViewport(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;II)I
    .locals 38
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "state"    # Landroid/support/v7/widget/RecyclerView$State;
    .param p3, "referencePosition"    # I
    .param p4, "referenceOffset"    # I

    .prologue
    .line 3077
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v32

    .line 3079
    .local v32, "totalItemCount":I
    if-nez v32, :cond_0

    .line 3080
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/play/layout/FlowLayoutManager;->removeAndRecycleAllViews(Landroid/support/v7/widget/RecyclerView$Recycler;)V

    .line 3081
    invoke-direct/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->recycleAllSections()V

    .line 3082
    const/4 v4, 0x0

    .line 3251
    :goto_0
    return v4

    .line 3087
    :cond_0
    if-ltz p3, :cond_3

    move/from16 v0, p3

    move/from16 v1, v32

    if-ge v0, v1, :cond_3

    const/16 v33, 0x1

    .line 3090
    .local v33, "usingReference":Z
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getPaddingTop()I

    move-result v36

    .line 3091
    .local v36, "viewportTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getHeight()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getPaddingBottom()I

    move-result v5

    sub-int v34, v4, v5

    .line 3095
    .local v34, "viewportBottom":I
    sub-int v4, v34, v36

    div-int/lit8 v35, v4, 0x2

    .line 3096
    .local v35, "viewportHalfHeight":I
    if-eqz v33, :cond_4

    sub-int v6, v36, v35

    .line 3098
    .local v6, "fillExtentTop":I
    :goto_2
    add-int v12, v34, v35

    .line 3102
    .local v12, "fillExtentBottom":I
    invoke-virtual/range {p2 .. p2}, Landroid/support/v7/widget/RecyclerView$State;->didStructureChange()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 3103
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/play/layout/FlowLayoutManager;->detachAndScrapAttachedViews(Landroid/support/v7/widget/RecyclerView$Recycler;)V

    .line 3109
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildCount()I

    move-result v4

    add-int/lit8 v17, v4, -0x1

    .local v17, "i":I
    :goto_3
    if-ltz v17, :cond_5

    .line 3110
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 3111
    .local v10, "child":Landroid/view/View;
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    check-cast v20, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    .line 3112
    .local v20, "layoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->viewNeedsUpdate()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 3117
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v10, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->detachAndScrapView(Landroid/view/View;Landroid/support/v7/widget/RecyclerView$Recycler;)V

    .line 3109
    :cond_2
    add-int/lit8 v17, v17, -0x1

    goto :goto_3

    .line 3087
    .end local v6    # "fillExtentTop":I
    .end local v10    # "child":Landroid/view/View;
    .end local v12    # "fillExtentBottom":I
    .end local v17    # "i":I
    .end local v20    # "layoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    .end local v33    # "usingReference":Z
    .end local v34    # "viewportBottom":I
    .end local v35    # "viewportHalfHeight":I
    .end local v36    # "viewportTop":I
    :cond_3
    const/16 v33, 0x0

    goto :goto_1

    .restart local v33    # "usingReference":Z
    .restart local v34    # "viewportBottom":I
    .restart local v35    # "viewportHalfHeight":I
    .restart local v36    # "viewportTop":I
    :cond_4
    move/from16 v6, v36

    .line 3096
    goto :goto_2

    .line 3122
    .restart local v6    # "fillExtentTop":I
    .restart local v12    # "fillExtentBottom":I
    .restart local v17    # "i":I
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getWidth()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v37

    .line 3123
    .local v37, "width":Ljava/lang/Integer;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getPaddingStart()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    .line 3124
    .local v23, "paddingStart":Ljava/lang/Integer;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getPaddingEnd()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    .line 3125
    .local v22, "paddingEnd":Ljava/lang/Integer;
    sget v4, Lcom/google/android/play/R$id;->flm_width:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView$State;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    sget v4, Lcom/google/android/play/R$id;->flm_paddingStart:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView$State;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    sget v4, Lcom/google/android/play/R$id;->flm_paddingEnd:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView$State;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 3128
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->recycleAllSections()V

    .line 3129
    sget v4, Lcom/google/android/play/R$id;->flm_width:I

    move-object/from16 v0, p2

    move-object/from16 v1, v37

    invoke-virtual {v0, v4, v1}, Landroid/support/v7/widget/RecyclerView$State;->put(ILjava/lang/Object;)V

    .line 3130
    sget v4, Lcom/google/android/play/R$id;->flm_paddingStart:I

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v4, v1}, Landroid/support/v7/widget/RecyclerView$State;->put(ILjava/lang/Object;)V

    .line 3131
    sget v4, Lcom/google/android/play/R$id;->flm_paddingEnd:I

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v4, v1}, Landroid/support/v7/widget/RecyclerView$State;->put(ILjava/lang/Object;)V

    .line 3138
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v17, v4, -0x1

    :goto_4
    if-ltz v17, :cond_8

    .line 3139
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    move/from16 v0, v17

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    invoke-virtual {v4}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->clearMeasuredInCurrentPass()V

    .line 3138
    add-int/lit8 v17, v17, -0x1

    goto :goto_4

    .line 3144
    :cond_8
    if-eqz v33, :cond_b

    move/from16 v13, p3

    .line 3145
    .local v13, "fillReferencePosition":I
    :goto_5
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/android/play/layout/FlowLayoutManager;->findSectionIndexByPosition(I)I

    move-result v26

    .line 3146
    .local v26, "referenceOrPreviousSectionIndex":I
    if-gez v26, :cond_9

    .line 3147
    xor-int/lit8 v4, v26, -0x1

    add-int/lit8 v26, v4, -0x1

    .line 3149
    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v26

    move/from16 v3, v32

    invoke-direct {v0, v1, v2, v13, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->fillUpForPosition(Landroid/support/v7/widget/RecyclerView$Recycler;III)I

    move-result v28

    .line 3152
    .local v28, "referenceSectionIndex":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    move/from16 v0, v28

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    .line 3153
    .local v27, "referenceSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    if-eqz v33, :cond_c

    move-object/from16 v0, v27

    invoke-virtual {v0, v13}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->getItemTopOffset(I)I

    move-result v4

    sub-int v29, p4, v4

    .line 3158
    .local v29, "referenceSectionTop":I
    :goto_6
    sub-int v4, v12, v29

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v28

    move/from16 v3, v32

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->fillDownForHeight(Landroid/support/v7/widget/RecyclerView$Recycler;III)I

    move-result v30

    .line 3160
    .local v30, "remainingHeight":I
    sub-int v18, v12, v30

    .line 3165
    .local v18, "lastLineBottom":I
    const/16 v24, 0x0

    .line 3166
    .local v24, "positiveOffset":I
    if-eqz v33, :cond_a

    .line 3167
    const/4 v4, 0x0

    sub-int v5, v34, v18

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v24

    .line 3168
    add-int v29, v29, v24

    .line 3169
    add-int v18, v18, v24

    .line 3176
    :cond_a
    move/from16 v8, v28

    .line 3177
    .local v8, "firstSectionIndex":I
    move/from16 v9, v29

    .line 3178
    .local v9, "firstSectionTop":I
    move-object/from16 v15, v27

    .line 3179
    .local v15, "firstSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :goto_7
    if-le v9, v6, :cond_e

    iget v4, v15, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    if-lez v4, :cond_e

    .line 3180
    add-int/lit8 v4, v8, -0x1

    iget v5, v15, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    add-int/lit8 v5, v5, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v32

    invoke-direct {v0, v1, v4, v5, v2}, Lcom/google/android/play/layout/FlowLayoutManager;->fillUpForPosition(Landroid/support/v7/widget/RecyclerView$Recycler;III)I

    move-result v25

    .line 3182
    .local v25, "previousSectionIndex":I
    move/from16 v0, v25

    if-ne v0, v8, :cond_d

    .line 3184
    add-int/lit8 v28, v28, 0x1

    .line 3188
    :goto_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "firstSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    check-cast v15, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    .line 3189
    .restart local v15    # "firstSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    iget v4, v15, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mTotalHeight:I

    sub-int/2addr v9, v4

    .line 3190
    goto :goto_7

    .line 3144
    .end local v8    # "firstSectionIndex":I
    .end local v9    # "firstSectionTop":I
    .end local v13    # "fillReferencePosition":I
    .end local v15    # "firstSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    .end local v18    # "lastLineBottom":I
    .end local v24    # "positiveOffset":I
    .end local v25    # "previousSectionIndex":I
    .end local v26    # "referenceOrPreviousSectionIndex":I
    .end local v27    # "referenceSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    .end local v28    # "referenceSectionIndex":I
    .end local v29    # "referenceSectionTop":I
    .end local v30    # "remainingHeight":I
    :cond_b
    const/4 v13, 0x0

    goto :goto_5

    .restart local v13    # "fillReferencePosition":I
    .restart local v26    # "referenceOrPreviousSectionIndex":I
    .restart local v27    # "referenceSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    .restart local v28    # "referenceSectionIndex":I
    :cond_c
    move/from16 v29, v36

    .line 3153
    goto :goto_6

    .line 3186
    .restart local v8    # "firstSectionIndex":I
    .restart local v9    # "firstSectionTop":I
    .restart local v15    # "firstSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    .restart local v18    # "lastLineBottom":I
    .restart local v24    # "positiveOffset":I
    .restart local v25    # "previousSectionIndex":I
    .restart local v29    # "referenceSectionTop":I
    .restart local v30    # "remainingHeight":I
    :cond_d
    move/from16 v8, v25

    goto :goto_8

    .line 3194
    .end local v25    # "previousSectionIndex":I
    :cond_e
    const/16 v21, 0x0

    .line 3195
    .local v21, "negativeOffset":I
    if-eqz v33, :cond_f

    .line 3197
    const/4 v4, 0x0

    sub-int v5, v9, v36

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v21

    .line 3198
    sub-int v9, v9, v21

    .line 3199
    sub-int v29, v29, v21

    .line 3200
    sub-int v18, v18, v21

    .line 3205
    if-lez v21, :cond_f

    if-nez v24, :cond_f

    move/from16 v0, v18

    if-ge v0, v12, :cond_f

    .line 3207
    sub-int v4, v12, v29

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v28

    move/from16 v3, v32

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->fillDownForHeight(Landroid/support/v7/widget/RecyclerView$Recycler;III)I

    move-result v30

    .line 3209
    sub-int v18, v12, v30

    .line 3220
    :cond_f
    move/from16 v0, v18

    invoke-static {v0, v12}, Ljava/lang/Math;->min(II)I

    move-result v7

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-direct/range {v4 .. v9}, Lcom/google/android/play/layout/FlowLayoutManager;->renderAndRecycleViews(Landroid/support/v7/widget/RecyclerView$Recycler;IIII)I

    move-result v16

    .line 3229
    .local v16, "firstUnusedSectionIndex":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v16

    if-ge v0, v4, :cond_10

    .line 3230
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    move/from16 v0, v16

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    iget v4, v4, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    add-int/lit8 v14, v4, 0x5

    .line 3232
    .local v14, "firstPositionToRecycle":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v17, v4, -0x1

    .line 3233
    :goto_9
    add-int/lit8 v4, v16, 0x2

    move/from16 v0, v17

    if-lt v0, v4, :cond_10

    .line 3234
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    move/from16 v0, v17

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    .line 3235
    .local v31, "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    move-object/from16 v0, v31

    iget v4, v0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    if-lt v4, v14, :cond_10

    .line 3236
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->removeSectionAt(I)V

    .line 3233
    add-int/lit8 v17, v17, -0x1

    goto :goto_9

    .line 3242
    .end local v14    # "firstPositionToRecycle":I
    .end local v31    # "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :cond_10
    iget v4, v15, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    add-int/lit8 v19, v4, -0x5

    .line 3243
    .local v19, "lastPositionToRecycle":I
    add-int/lit8 v4, v8, -0x2

    add-int/lit8 v17, v4, -0x1

    :goto_a
    if-ltz v17, :cond_12

    .line 3244
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    move/from16 v0, v17

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    .line 3245
    .restart local v31    # "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    move-object/from16 v0, v31

    iget v4, v0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mPositionStart:I

    move/from16 v0, v19

    if-ge v4, v0, :cond_11

    .line 3246
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->removeSectionAt(I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3243
    :cond_11
    add-int/lit8 v17, v17, -0x1

    goto :goto_a

    .line 3251
    .end local v31    # "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :cond_12
    sub-int v4, v24, v21

    goto/16 :goto_0

    .line 3252
    .end local v6    # "fillExtentTop":I
    .end local v8    # "firstSectionIndex":I
    .end local v9    # "firstSectionTop":I
    .end local v12    # "fillExtentBottom":I
    .end local v13    # "fillReferencePosition":I
    .end local v15    # "firstSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    .end local v16    # "firstUnusedSectionIndex":I
    .end local v17    # "i":I
    .end local v18    # "lastLineBottom":I
    .end local v19    # "lastPositionToRecycle":I
    .end local v21    # "negativeOffset":I
    .end local v22    # "paddingEnd":Ljava/lang/Integer;
    .end local v23    # "paddingStart":Ljava/lang/Integer;
    .end local v24    # "positiveOffset":I
    .end local v26    # "referenceOrPreviousSectionIndex":I
    .end local v27    # "referenceSection":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    .end local v28    # "referenceSectionIndex":I
    .end local v29    # "referenceSectionTop":I
    .end local v30    # "remainingHeight":I
    .end local v32    # "totalItemCount":I
    .end local v33    # "usingReference":Z
    .end local v34    # "viewportBottom":I
    .end local v35    # "viewportHalfHeight":I
    .end local v36    # "viewportTop":I
    .end local v37    # "width":Ljava/lang/Integer;
    :catch_0
    move-exception v11

    .line 3254
    .local v11, "e":Ljava/lang/RuntimeException;
    const-string v4, "FlowLayoutManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "layoutViewport() state at exception:\n\t referencePosition="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\n\t referenceOffset="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p4

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\n\t state.getItemCount()="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\n\t didStructureChange="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Landroid/support/v7/widget/RecyclerView$State;->didStructureChange()Z

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3259
    invoke-direct/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->debugPrintSections()V

    .line 3260
    invoke-direct/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->debugPrintChildren()V

    .line 3262
    throw v11
.end method

.method private measureDecorated(Landroid/view/View;II)V
    .locals 5
    .param p1, "child"    # Landroid/view/View;
    .param p2, "exactDecoratedWidth"    # I
    .param p3, "exactDecoratedHeight"    # I

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 3407
    sget-object v2, Lcom/google/android/play/layout/FlowLayoutManager;->sDecorInsets:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v2}, Lcom/google/android/play/layout/FlowLayoutManager;->calculateItemDecorationsForChild(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 3408
    sget-object v2, Lcom/google/android/play/layout/FlowLayoutManager;->sDecorInsets:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int v2, p2, v2

    sget-object v3, Lcom/google/android/play/layout/FlowLayoutManager;->sDecorInsets:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v3

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 3410
    .local v1, "widthMeasureSpec":I
    sget-object v2, Lcom/google/android/play/layout/FlowLayoutManager;->sDecorInsets:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int v2, p3, v2

    sget-object v3, Lcom/google/android/play/layout/FlowLayoutManager;->sDecorInsets:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, v3

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 3412
    .local v0, "heightMeasureSpec":I
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 3413
    return-void
.end method

.method private measureNextItem(Landroid/support/v7/widget/RecyclerView$Recycler;IILjava/util/List;IIIZZI)Z
    .locals 20
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "nextPosition"    # I
    .param p3, "fullContextWidth"    # I
    .param p5, "lineWidth"    # I
    .param p6, "usedLineWidth"    # I
    .param p7, "lineOffsetStart"    # I
    .param p8, "atParagraphStart"    # Z
    .param p9, "inNestedFlow"    # Z
    .param p10, "remainingFlowHeight"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v7/widget/RecyclerView$Recycler;",
            "II",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;",
            ">;IIIZZI)Z"
        }
    .end annotation

    .prologue
    .line 2750
    .local p4, "previousItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;>;"
    invoke-direct/range {p0 .. p3}, Lcom/google/android/play/layout/FlowLayoutManager;->getNextChildIntoFillState(Landroid/support/v7/widget/RecyclerView$Recycler;II)Landroid/view/View;

    move-result-object v4

    .line 2751
    .local v4, "child":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v10, v0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItemLayoutParams:Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    .line 2752
    .local v10, "layoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    const/4 v2, 0x1

    .line 2753
    .local v2, "allowSoftWrap":Z
    if-eqz p4, :cond_0

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->isEmpty()Z

    move-result v17

    if-eqz v17, :cond_2

    :cond_0
    const/4 v3, 0x1

    .line 2756
    .local v3, "atLineStart":Z
    :goto_0
    invoke-virtual {v10}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->isAnchorCandidate()Z

    move-result v17

    if-eqz v17, :cond_1

    .line 2757
    if-nez p9, :cond_3

    if-eqz p8, :cond_3

    .line 2758
    const/4 v2, 0x0

    .line 2769
    :cond_1
    invoke-virtual {v10}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getLineWrapFlow()I

    move-result v13

    .line 2770
    .local v13, "lineWrapFlow":I
    if-eqz p9, :cond_4

    .line 2771
    sparse-switch v13, :sswitch_data_0

    .line 2789
    if-eqz v3, :cond_4

    const/16 v17, 0x1

    move/from16 v0, p10

    move/from16 v1, v17

    if-gt v0, v1, :cond_4

    .line 2793
    const/16 v17, 0x0

    .line 2939
    .end local v13    # "lineWrapFlow":I
    :goto_1
    return v17

    .line 2753
    .end local v3    # "atLineStart":Z
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 2760
    .restart local v3    # "atLineStart":Z
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    move-object/from16 v17, v0

    move/from16 v0, p2

    move-object/from16 v1, v17

    iput v0, v1, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextAnchorPosition:I

    .line 2764
    const/16 v17, 0x0

    goto :goto_1

    .line 2776
    .restart local v13    # "lineWrapFlow":I
    :sswitch_0
    const/16 v17, 0x0

    goto :goto_1

    .line 2783
    :sswitch_1
    if-eqz v3, :cond_4

    .line 2784
    const/4 v2, 0x0

    .line 2800
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v9, v0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->mNextItem:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .line 2801
    .local v9, "item":Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    invoke-virtual {v10}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->getEffectiveLineWrapAction()I

    move-result v12

    .line 2802
    .local v12, "lineWrapAction":I
    if-nez v3, :cond_5

    .line 2803
    packed-switch v12, :pswitch_data_0

    .line 2815
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v17

    add-int/lit8 v17, v17, -0x1

    move-object/from16 v0, p4

    move/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->hasSameGridAs(Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;)Z

    move-result v17

    if-nez v17, :cond_5

    .line 2819
    const/16 v17, 0x0

    goto :goto_1

    .line 2805
    :pswitch_0
    const/4 v2, 0x0

    .line 2825
    :cond_5
    :pswitch_1
    iget v0, v10, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->widthCompound:I

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_6

    .line 2826
    move/from16 v0, p5

    move/from16 v1, p3

    if-ge v0, v1, :cond_6

    .line 2831
    const/16 v17, 0x0

    goto :goto_1

    .line 2811
    :pswitch_2
    const/16 v17, 0x0

    goto :goto_1

    .line 2839
    :cond_6
    if-eqz v2, :cond_7

    if-eqz v3, :cond_7

    move/from16 v0, p5

    move/from16 v1, p3

    if-lt v0, v1, :cond_7

    .line 2840
    const/4 v2, 0x0

    .line 2845
    :cond_7
    const/16 v17, 0x0

    sub-int v18, p5, p6

    iget v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginStart:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    iget v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginEnd:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(II)I

    move-result v14

    .line 2850
    .local v14, "remainingWidthAfterMargin":I
    const/4 v6, 0x0

    .line 2851
    .local v6, "extraMarginStart":I
    const/4 v5, 0x0

    .line 2852
    .local v5, "extraMarginEnd":I
    iget v0, v10, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->widthCompound:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/play/utils/Compound;->isCompoundFloat(I)Z

    move-result v17

    if-eqz v17, :cond_b

    iget v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetStart:I

    move/from16 v17, v0

    if-nez v17, :cond_8

    iget v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetEnd:I

    move/from16 v17, v0

    if-eqz v17, :cond_b

    .line 2854
    :cond_8
    iget v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetStart:I

    move/from16 v17, v0

    if-lez v17, :cond_d

    .line 2856
    const/16 v17, 0x0

    iget v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetStart:I

    move/from16 v18, v0

    sub-int v18, v18, p7

    sub-int v18, v18, p6

    iget v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginStart:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 2863
    :cond_9
    :goto_2
    sub-int v17, p3, p7

    sub-int v11, v17, p5

    .line 2864
    .local v11, "lineOffsetEnd":I
    iget v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetEnd:I

    move/from16 v17, v0

    if-lez v17, :cond_e

    .line 2866
    const/16 v17, 0x0

    iget v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetEnd:I

    move/from16 v18, v0

    sub-int v18, v18, v11

    iget v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginEnd:I

    move/from16 v19, v0

    sub-int v18, v18, v19

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 2875
    :cond_a
    :goto_3
    const/16 v17, 0x0

    sub-int v18, v14, v6

    sub-int v18, v18, v5

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(II)I

    move-result v14

    .line 2880
    .end local v11    # "lineOffsetEnd":I
    :cond_b
    sget-object v17, Lcom/google/android/play/layout/FlowLayoutManager;->sDecorInsets:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v4, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->calculateItemDecorationsForChild(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2881
    sget-object v17, Lcom/google/android/play/layout/FlowLayoutManager;->sDecorInsets:Landroid/graphics/Rect;

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    sget-object v18, Lcom/google/android/play/layout/FlowLayoutManager;->sDecorInsets:Landroid/graphics/Rect;

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    add-int v8, v17, v18

    .line 2882
    .local v8, "horizontalDecorationSize":I
    sget-object v17, Lcom/google/android/play/layout/FlowLayoutManager;->sDecorInsets:Landroid/graphics/Rect;

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    sget-object v18, Lcom/google/android/play/layout/FlowLayoutManager;->sDecorInsets:Landroid/graphics/Rect;

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    add-int v15, v17, v18

    .line 2883
    .local v15, "verticalDecorationSize":I
    const/16 v17, 0x0

    sub-int v18, v14, v8

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(II)I

    move-result v14

    .line 2887
    iget v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v10, v0, v14, v8}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->resolveWidthAndMakeMeasureSpec(FII)I

    move-result v16

    .line 2889
    .local v16, "widthMeasureSpec":I
    iget v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridCellSize:F

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v10, v0, v15, v1}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->resolveHeightAndMakeMeasureSpec(FILcom/google/android/play/layout/FlowLayoutManager;)I

    move-result v7

    .line 2894
    .local v7, "heightMeasureSpec":I
    if-eqz v2, :cond_f

    if-eqz v14, :cond_c

    invoke-static/range {v16 .. v16}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v17

    move/from16 v0, v17

    if-le v0, v14, :cond_f

    .line 2901
    :cond_c
    const/16 v17, 0x0

    goto/16 :goto_1

    .line 2858
    .end local v7    # "heightMeasureSpec":I
    .end local v8    # "horizontalDecorationSize":I
    .end local v15    # "verticalDecorationSize":I
    .end local v16    # "widthMeasureSpec":I
    :cond_d
    if-eqz v3, :cond_9

    if-nez p7, :cond_9

    iget v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginStart:I

    move/from16 v17, v0

    if-nez v17, :cond_9

    .line 2861
    iget v6, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetStart:I

    goto/16 :goto_2

    .line 2868
    .restart local v11    # "lineOffsetEnd":I
    :cond_e
    if-nez v11, :cond_a

    iget v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginEnd:I

    move/from16 v17, v0

    if-nez v17, :cond_a

    .line 2873
    iget v5, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mGridInsetEnd:I

    goto :goto_3

    .line 2905
    .end local v11    # "lineOffsetEnd":I
    .restart local v7    # "heightMeasureSpec":I
    .restart local v8    # "horizontalDecorationSize":I
    .restart local v15    # "verticalDecorationSize":I
    .restart local v16    # "widthMeasureSpec":I
    :cond_f
    move/from16 v0, v16

    invoke-virtual {v4, v0, v7}, Landroid/view/View;->measure(II)V

    .line 2906
    if-eqz v2, :cond_11

    .line 2908
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v17

    move/from16 v0, v17

    if-le v0, v14, :cond_10

    .line 2914
    const/16 v17, 0x0

    goto/16 :goto_1

    .line 2918
    :cond_10
    invoke-static {v4}, Landroid/support/v4/view/ViewCompat;->getMeasuredWidthAndState(Landroid/view/View;)I

    move-result v17

    const/high16 v18, 0x1000000

    and-int v17, v17, v18

    if-eqz v17, :cond_11

    .line 2924
    const/16 v17, 0x0

    goto/16 :goto_1

    .line 2933
    :cond_11
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v9, v0, v4, v1}, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->loadMeasurements(Lcom/google/android/play/layout/FlowLayoutManager;Landroid/view/View;Z)V

    .line 2934
    iget v0, v10, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->flow:I

    move/from16 v17, v0

    and-int/lit8 v17, v17, 0x2

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_12

    .line 2935
    iget v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginEnd:I

    move/from16 v17, v0

    add-int v17, v17, v5

    move/from16 v0, v17

    iput v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginEnd:I

    .line 2939
    :goto_4
    const/16 v17, 0x1

    goto/16 :goto_1

    .line 2937
    :cond_12
    iget v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginStart:I

    move/from16 v17, v0

    add-int v17, v17, v6

    move/from16 v0, v17

    iput v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginStart:I

    goto :goto_4

    .line 2771
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x8 -> :sswitch_0
    .end sparse-switch

    .line 2803
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private recycleAllSections()V
    .locals 2

    .prologue
    .line 1830
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 1831
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    invoke-virtual {v1}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->recycle()V

    .line 1830
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1833
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1834
    return-void
.end method

.method private recycleViewsAtOrAfter(Landroid/support/v7/widget/RecyclerView$Recycler;I)V
    .locals 2
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "childIndex"    # I

    .prologue
    .line 2184
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildCount()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    if-lt v0, p2, :cond_0

    .line 2185
    invoke-virtual {p0, v0, p1}, Lcom/google/android/play/layout/FlowLayoutManager;->removeAndRecycleViewAt(ILandroid/support/v7/widget/RecyclerView$Recycler;)V

    .line 2184
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 2187
    :cond_0
    return-void
.end method

.method private recycleViewsBeforePosition(Landroid/support/v7/widget/RecyclerView$Recycler;I)V
    .locals 2
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "position"    # I

    .prologue
    .line 2174
    invoke-direct {p0, p2}, Lcom/google/android/play/layout/FlowLayoutManager;->findChildIndexByPosition(I)I

    move-result v0

    .line 2175
    .local v0, "childIndex":I
    if-gez v0, :cond_0

    .line 2176
    xor-int/lit8 v0, v0, -0x1

    .line 2178
    :cond_0
    add-int/lit8 v1, v0, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_1

    .line 2179
    invoke-virtual {p0, v1, p1}, Lcom/google/android/play/layout/FlowLayoutManager;->removeAndRecycleViewAt(ILandroid/support/v7/widget/RecyclerView$Recycler;)V

    .line 2178
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 2181
    :cond_1
    return-void
.end method

.method private removeSectionAt(I)V
    .locals 1
    .param p1, "sectionIndex"    # I

    .prologue
    .line 1814
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    invoke-virtual {v0}, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->recycle()V

    .line 1815
    if-nez p1, :cond_0

    .line 1820
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mTrueAnchorAtPositionZero:Z

    .line 1822
    :cond_0
    return-void
.end method

.method private renderAndRecycleViews(Landroid/support/v7/widget/RecyclerView$Recycler;IIII)I
    .locals 11
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "renderTop"    # I
    .param p3, "renderBottom"    # I
    .param p4, "firstSectionIndex"    # I
    .param p5, "firstSectionTop"    # I

    .prologue
    .line 3282
    move/from16 v1, p5

    .line 3283
    .local v1, "lineTop":I
    const/4 v3, -0x1

    .line 3284
    .local v3, "nextChildIndex":I
    move v10, p4

    .line 3285
    .local v10, "sectionIndex":I
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getLayoutDirection()I

    move-result v0

    const/4 v4, 0x1

    if-ne v0, v4, :cond_2

    const/4 v5, 0x1

    .line 3286
    .local v5, "isRtl":Z
    :goto_0
    if-ge v1, p3, :cond_4

    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v10, v0, :cond_4

    .line 3287
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mSections:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    .line 3288
    .local v9, "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    iget-object v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    .line 3289
    .local v6, "lineCount":I
    const/4 v7, 0x0

    .local v7, "lineIndex":I
    :goto_1
    if-ge v1, p3, :cond_3

    if-ge v7, v6, :cond_3

    .line 3290
    iget-object v0, v9, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;

    .line 3291
    .local v2, "line":Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;
    iget v0, v2, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->mTotalHeight:I

    add-int v8, v1, v0

    .line 3292
    .local v8, "newLineTop":I
    const/4 v0, -0x1

    if-ne v3, v0, :cond_0

    if-le v8, p2, :cond_0

    .line 3293
    iget v0, v2, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->mPositionStart:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->recycleViewsBeforePosition(Landroid/support/v7/widget/RecyclerView$Recycler;I)V

    .line 3294
    const/4 v3, 0x0

    .line 3296
    :cond_0
    const/4 v0, -0x1

    if-eq v3, v0, :cond_1

    move-object v0, p0

    move-object v4, p1

    .line 3297
    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/layout/FlowLayoutManager;->renderLineAt(ILcom/google/android/play/layout/FlowLayoutManager$LineInfo;ILandroid/support/v7/widget/RecyclerView$Recycler;Z)I

    move-result v3

    .line 3299
    :cond_1
    move v1, v8

    .line 3289
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 3285
    .end local v2    # "line":Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;
    .end local v5    # "isRtl":Z
    .end local v6    # "lineCount":I
    .end local v7    # "lineIndex":I
    .end local v8    # "newLineTop":I
    .end local v9    # "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 3286
    .restart local v5    # "isRtl":Z
    .restart local v6    # "lineCount":I
    .restart local v7    # "lineIndex":I
    .restart local v9    # "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 3302
    .end local v6    # "lineCount":I
    .end local v7    # "lineIndex":I
    .end local v9    # "section":Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;
    :cond_4
    invoke-direct {p0, p1, v3}, Lcom/google/android/play/layout/FlowLayoutManager;->recycleViewsAtOrAfter(Landroid/support/v7/widget/RecyclerView$Recycler;I)V

    .line 3303
    return v10
.end method

.method private renderInlineFlowLineAt(ILcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;ILandroid/support/v7/widget/RecyclerView$Recycler;Z)I
    .locals 11
    .param p1, "lineTop"    # I
    .param p2, "line"    # Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;
    .param p3, "nextChildIndex"    # I
    .param p4, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p5, "isRtl"    # Z

    .prologue
    .line 3328
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getPaddingStart()I

    move-result v0

    iget v1, p2, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mOffsetStart:I

    add-int v2, v0, v1

    .line 3329
    .local v2, "itemStart":I
    iget-object v0, p2, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v10

    .line 3330
    .local v10, "itemCount":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-ge v9, v10, :cond_0

    .line 3331
    iget-object v0, p2, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mItems:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    .line 3332
    .local v3, "item":Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    iget v0, p2, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;->mPositionStart:I

    add-int v4, v0, v9

    const/4 v8, 0x0

    move-object v0, p0

    move v1, p1

    move v5, p3

    move-object v6, p4

    move/from16 v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/play/layout/FlowLayoutManager;->renderItemAt(IILcom/google/android/play/layout/FlowLayoutManager$ItemInfo;IILandroid/support/v7/widget/RecyclerView$Recycler;ZLcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;)I

    move-result v0

    add-int/lit8 p3, v0, 0x1

    .line 3334
    iget v0, v3, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginStart:I

    iget v1, v3, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedWidth:I

    add-int/2addr v0, v1

    iget v1, v3, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginEnd:I

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 3330
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 3336
    .end local v3    # "item":Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    :cond_0
    return p3
.end method

.method private renderItemAt(IILcom/google/android/play/layout/FlowLayoutManager$ItemInfo;IILandroid/support/v7/widget/RecyclerView$Recycler;ZLcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;)I
    .locals 17
    .param p1, "itemTop"    # I
    .param p2, "itemStart"    # I
    .param p3, "item"    # Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;
    .param p4, "position"    # I
    .param p5, "childIndex"    # I
    .param p6, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p7, "isRtl"    # Z
    .param p8, "itemLine"    # Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;

    .prologue
    .line 3365
    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move/from16 v2, p4

    move/from16 v3, p4

    move/from16 v4, p5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/play/layout/FlowLayoutManager;->getOrAddChildWithHint(Landroid/support/v7/widget/RecyclerView$Recycler;III)I

    move-result p5

    .line 3366
    move-object/from16 v0, p0

    move/from16 v1, p5

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 3367
    .local v6, "child":Landroid/view/View;
    move-object/from16 v0, p3

    iget v14, v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedHeight:I

    .line 3368
    .local v14, "itemDecoratedHeight":I
    if-eqz p8, :cond_2

    move-object/from16 v0, p8

    iget-boolean v5, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreatorHeightWrapsChildFlow:Z

    if-eqz v5, :cond_2

    move-object/from16 v0, p8

    iget v5, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mExtraHeight:I

    if-lez v5, :cond_2

    .line 3377
    move-object/from16 v0, p3

    iget v5, v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedWidth:I

    move-object/from16 v0, p3

    iget v15, v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedHeight:I

    move-object/from16 v0, p8

    iget v0, v0, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mExtraHeight:I

    move/from16 v16, v0

    add-int v15, v15, v16

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v5, v15}, Lcom/google/android/play/layout/FlowLayoutManager;->measureDecorated(Landroid/view/View;II)V

    .line 3379
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/play/layout/FlowLayoutManager;->getDecoratedMeasuredHeight(Landroid/view/View;)I

    move-result v14

    .line 3387
    :cond_0
    :goto_0
    move-object/from16 v0, p3

    iget v5, v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mTopOffset:I

    add-int v8, p1, v5

    .line 3388
    .local v8, "childTop":I
    add-int v10, v8, v14

    .line 3389
    .local v10, "childBottom":I
    move-object/from16 v0, p3

    iget v5, v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMarginStart:I

    add-int v12, p2, v5

    .line 3390
    .local v12, "childStart":I
    move-object/from16 v0, p3

    iget v5, v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedWidth:I

    add-int v11, v12, v5

    .line 3391
    .local v11, "childEnd":I
    if-eqz p7, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getWidth()I

    move-result v5

    sub-int v7, v5, v11

    .line 3392
    .local v7, "childLeft":I
    :goto_1
    if-eqz p7, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getWidth()I

    move-result v5

    sub-int v9, v5, v12

    .local v9, "childRight":I
    :goto_2
    move-object/from16 v5, p0

    .line 3393
    invoke-virtual/range {v5 .. v10}, Lcom/google/android/play/layout/FlowLayoutManager;->layoutDecorated(Landroid/view/View;IIII)V

    .line 3395
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mViewRenderedListener:Lcom/google/android/play/layout/FlowLayoutManager$OnViewRenderedListener;

    if-eqz v5, :cond_1

    .line 3398
    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/RecyclerView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v13

    .line 3400
    .local v13, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/play/layout/FlowLayoutManager;->mViewRenderedListener:Lcom/google/android/play/layout/FlowLayoutManager$OnViewRenderedListener;

    invoke-interface {v5, v13}, Lcom/google/android/play/layout/FlowLayoutManager$OnViewRenderedListener;->onViewRendered(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 3403
    .end local v13    # "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_1
    return p5

    .line 3380
    .end local v7    # "childLeft":I
    .end local v8    # "childTop":I
    .end local v9    # "childRight":I
    .end local v10    # "childBottom":I
    .end local v11    # "childEnd":I
    .end local v12    # "childStart":I
    :cond_2
    move-object/from16 v0, p3

    iget-boolean v5, v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mMeasuredInCurrentPass:Z

    if-nez v5, :cond_0

    .line 3383
    move-object/from16 v0, p3

    iget v5, v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedWidth:I

    move-object/from16 v0, p3

    iget v15, v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedHeight:I

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v5, v15}, Lcom/google/android/play/layout/FlowLayoutManager;->measureDecorated(Landroid/view/View;II)V

    .line 3384
    const/4 v5, 0x1

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v6, v5}, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->loadMeasurements(Lcom/google/android/play/layout/FlowLayoutManager;Landroid/view/View;Z)V

    .line 3385
    move-object/from16 v0, p3

    iget v14, v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mDecoratedHeight:I

    goto :goto_0

    .restart local v8    # "childTop":I
    .restart local v10    # "childBottom":I
    .restart local v11    # "childEnd":I
    .restart local v12    # "childStart":I
    :cond_3
    move v7, v12

    .line 3391
    goto :goto_1

    .restart local v7    # "childLeft":I
    :cond_4
    move v9, v11

    .line 3392
    goto :goto_2
.end method

.method private renderLineAt(ILcom/google/android/play/layout/FlowLayoutManager$LineInfo;ILandroid/support/v7/widget/RecyclerView$Recycler;Z)I
    .locals 6
    .param p1, "lineTop"    # I
    .param p2, "line"    # Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;
    .param p3, "nextChildIndex"    # I
    .param p4, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p5, "isRtl"    # Z

    .prologue
    .line 3317
    instance-of v0, p2, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;

    if-eqz v0, :cond_0

    move-object v2, p2

    .line 3318
    check-cast v2, Lcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;

    move-object v0, p0

    move v1, p1

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/layout/FlowLayoutManager;->renderInlineFlowLineAt(ILcom/google/android/play/layout/FlowLayoutManager$InlineFlowLineInfo;ILandroid/support/v7/widget/RecyclerView$Recycler;Z)I

    move-result v0

    .line 3321
    :goto_0
    return v0

    :cond_0
    move-object v2, p2

    check-cast v2, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;

    move-object v0, p0

    move v1, p1

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/play/layout/FlowLayoutManager;->renderNestedFlowLineAt(ILcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;ILandroid/support/v7/widget/RecyclerView$Recycler;Z)I

    move-result v0

    goto :goto_0
.end method

.method private renderNestedFlowLineAt(ILcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;ILandroid/support/v7/widget/RecyclerView$Recycler;Z)I
    .locals 11
    .param p1, "lineTop"    # I
    .param p2, "line"    # Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;
    .param p3, "nextChildIndex"    # I
    .param p4, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p5, "isRtl"    # Z

    .prologue
    .line 3341
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getPaddingStart()I

    move-result v0

    iget v1, p2, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mOffsetStart:I

    add-int v2, v0, v1

    .line 3342
    .local v2, "lineStart":I
    iget-object v3, p2, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v4, p2, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mPositionStart:I

    move-object v0, p0

    move v1, p1

    move v5, p3

    move-object v6, p4

    move/from16 v7, p5

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/play/layout/FlowLayoutManager;->renderItemAt(IILcom/google/android/play/layout/FlowLayoutManager$ItemInfo;IILandroid/support/v7/widget/RecyclerView$Recycler;ZLcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;)I

    move-result v0

    add-int/lit8 p3, v0, 0x1

    .line 3344
    iget-object v0, p2, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    if-nez v0, :cond_0

    const/4 v10, 0x0

    .line 3345
    .local v10, "nestedLineCount":I
    :goto_0
    iget-object v0, p2, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mCreator:Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;

    iget v0, v0, Lcom/google/android/play/layout/FlowLayoutManager$ItemInfo;->mTopOffset:I

    add-int/2addr v0, p1

    iget v1, p2, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mFlowInsetTop:I

    add-int v4, v0, v1

    .line 3346
    .local v4, "nestedLineTop":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    if-ge v9, v10, :cond_1

    .line 3347
    iget-object v0, p2, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    iget-object v0, v0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;

    .local v5, "nestedLine":Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;
    move-object v3, p0

    move v6, p3

    move-object v7, p4

    move/from16 v8, p5

    .line 3348
    invoke-direct/range {v3 .. v8}, Lcom/google/android/play/layout/FlowLayoutManager;->renderLineAt(ILcom/google/android/play/layout/FlowLayoutManager$LineInfo;ILandroid/support/v7/widget/RecyclerView$Recycler;Z)I

    move-result p3

    .line 3350
    iget v0, v5, Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;->mTotalHeight:I

    add-int/2addr v4, v0

    .line 3346
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 3344
    .end local v4    # "nestedLineTop":I
    .end local v5    # "nestedLine":Lcom/google/android/play/layout/FlowLayoutManager$LineInfo;
    .end local v9    # "i":I
    .end local v10    # "nestedLineCount":I
    :cond_0
    iget-object v0, p2, Lcom/google/android/play/layout/FlowLayoutManager$NestedFlowLineInfo;->mParagraph:Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;

    iget-object v0, v0, Lcom/google/android/play/layout/FlowLayoutManager$ParagraphInfo;->mLines:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v10

    goto :goto_0

    .line 3352
    .restart local v4    # "nestedLineTop":I
    .restart local v9    # "i":I
    .restart local v10    # "nestedLineCount":I
    :cond_1
    return p3
.end method

.method private resetFillState()V
    .locals 2

    .prologue
    .line 2286
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    if-nez v0, :cond_0

    .line 2287
    new-instance v0, Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager$FillState;-><init>(Lcom/google/android/play/layout/FlowLayoutManager$1;)V

    iput-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    .line 2289
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mFillState:Lcom/google/android/play/layout/FlowLayoutManager$FillState;

    invoke-virtual {v0}, Lcom/google/android/play/layout/FlowLayoutManager$FillState;->reset()V

    .line 2290
    return-void
.end method


# virtual methods
.method public canScrollVertically()Z
    .locals 1

    .prologue
    .line 3435
    const/4 v0, 0x1

    return v0
.end method

.method public checkLayoutParams(Landroid/support/v7/widget/RecyclerView$LayoutParams;)Z
    .locals 1
    .param p1, "layoutParams"    # Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .prologue
    .line 782
    instance-of v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    return v0
.end method

.method public computeVerticalScrollExtent(Landroid/support/v7/widget/RecyclerView$State;)I
    .locals 1
    .param p1, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    .line 3450
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildCount()I

    move-result v0

    return v0
.end method

.method public computeVerticalScrollOffset(Landroid/support/v7/widget/RecyclerView$State;)I
    .locals 2
    .param p1, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    const/4 v0, 0x0

    .line 3445
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildCount()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method public computeVerticalScrollRange(Landroid/support/v7/widget/RecyclerView$State;)I
    .locals 1
    .param p1, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    .line 3440
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v0

    return v0
.end method

.method public findViewByPosition(I)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 2011
    invoke-direct {p0, p1}, Lcom/google/android/play/layout/FlowLayoutManager;->findChildIndexByPosition(I)I

    move-result v0

    .line 2012
    .local v0, "childIndex":I
    if-gez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public bridge synthetic generateDefaultLayoutParams()Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->generateDefaultLayoutParams()Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateDefaultLayoutParams()Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 787
    new-instance v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 1
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Landroid/util/AttributeSet;

    .prologue
    .line 50
    invoke-virtual {p0, p1, p2}, Lcom/google/android/play/layout/FlowLayoutManager;->generateLayoutParams(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 1
    .param p1, "x0"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/FlowLayoutManager;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 793
    new-instance v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    invoke-direct {v0, p1, p2}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    .locals 1
    .param p1, "layoutParams"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 798
    instance-of v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    if-eqz v0, :cond_0

    .line 799
    new-instance v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    check-cast p1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    .end local p1    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v0, p1}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;-><init>(Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;)V

    .line 803
    :goto_0
    return-object v0

    .line 800
    .restart local p1    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    .line 801
    new-instance v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    .end local p1    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v0, p1}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_0

    .line 803
    .restart local p1    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_1
    new-instance v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public measureChildWithMargins(Landroid/view/View;II)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "widthUsed"    # I
    .param p3, "heightUsed"    # I

    .prologue
    .line 818
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    if-eqz v0, :cond_0

    .line 819
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Views using FlowLayoutManager.LayoutParams should not be measured with measureChildWithMargins()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 822
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->measureChildWithMargins(Landroid/view/View;II)V

    .line 823
    return-void
.end method

.method public onAdapterChanged(Landroid/support/v7/widget/RecyclerView$Adapter;Landroid/support/v7/widget/RecyclerView$Adapter;)V
    .locals 0
    .param p1, "oldAdapter"    # Landroid/support/v7/widget/RecyclerView$Adapter;
    .param p2, "newAdapter"    # Landroid/support/v7/widget/RecyclerView$Adapter;

    .prologue
    .line 1845
    invoke-direct {p0, p2}, Lcom/google/android/play/layout/FlowLayoutManager;->handleAutoRegisteredOnViewRenderedLister(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 1846
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->recycleAllSections()V

    .line 1847
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->onAdapterChanged(Landroid/support/v7/widget/RecyclerView$Adapter;Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 1848
    return-void
.end method

.method public onDetachedFromWindow(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$Recycler;)V
    .locals 0
    .param p1, "view"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;

    .prologue
    .line 1838
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->recycleAllSections()V

    .line 1839
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->onDetachedFromWindow(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$Recycler;)V

    .line 1840
    return-void
.end method

.method public onItemsAdded(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "positionStart"    # I
    .param p3, "itemCount"    # I

    .prologue
    .line 1861
    invoke-direct {p0, p2, p2, p3}, Lcom/google/android/play/layout/FlowLayoutManager;->invalidateAndOffsetPositions(III)V

    .line 1862
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->onItemsAdded(Landroid/support/v7/widget/RecyclerView;II)V

    .line 1863
    return-void
.end method

.method public onItemsChanged(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 1852
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->recycleAllSections()V

    .line 1853
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->onItemsChanged(Landroid/support/v7/widget/RecyclerView;)V

    .line 1854
    return-void
.end method

.method public onItemsMoved(Landroid/support/v7/widget/RecyclerView;III)V
    .locals 5
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "from"    # I
    .param p3, "to"    # I
    .param p4, "itemCount"    # I

    .prologue
    .line 1879
    add-int v0, p2, p4

    .line 1880
    .local v0, "fromEnd":I
    add-int v1, p3, p4

    .line 1881
    .local v1, "toEnd":I
    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/google/android/play/layout/FlowLayoutManager;->invalidateAndOffsetPositions(III)V

    .line 1882
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->onItemsMoved(Landroid/support/v7/widget/RecyclerView;III)V

    .line 1883
    return-void
.end method

.method public onItemsRemoved(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "positionStart"    # I
    .param p3, "itemCount"    # I

    .prologue
    .line 1870
    add-int v0, p2, p3

    neg-int v1, p3

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->invalidateAndOffsetPositions(III)V

    .line 1871
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->onItemsRemoved(Landroid/support/v7/widget/RecyclerView;II)V

    .line 1872
    return-void
.end method

.method public onItemsUpdated(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "positionStart"    # I
    .param p3, "itemCount"    # I

    .prologue
    .line 1887
    iget-boolean v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mItemChangesAffectFlow:Z

    if-eqz v0, :cond_0

    .line 1891
    add-int v0, p2, p3

    const/4 v1, 0x0

    invoke-direct {p0, p2, v0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->invalidateAndOffsetPositions(III)V

    .line 1893
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->onItemsUpdated(Landroid/support/v7/widget/RecyclerView;II)V

    .line 1894
    return-void
.end method

.method public onLayoutChildren(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 7
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    const/high16 v6, -0x80000000

    const/4 v5, -0x1

    .line 3011
    iget-object v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingSavedState:Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    if-eqz v3, :cond_0

    .line 3012
    iget-object v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingSavedState:Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    iget v3, v3, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->mReferencePosition:I

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPosition:I

    .line 3013
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingSavedState:Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    iget v4, v4, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->mReferenceOffset:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPositionOffset:I

    .line 3015
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingSavedState:Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    .line 3017
    :cond_0
    iget v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPosition:I

    if-eq v3, v5, :cond_2

    .line 3018
    iget v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPosition:I

    if-ltz v3, :cond_1

    iget v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPosition:I

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v4

    if-lt v3, v4, :cond_3

    .line 3019
    :cond_1
    iput v5, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPosition:I

    .line 3020
    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPositionOffset:I

    .line 3030
    :cond_2
    :goto_0
    iget v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPosition:I

    if-eq v3, v5, :cond_4

    .line 3031
    iget v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPosition:I

    .line 3032
    .local v2, "referencePosition":I
    iget v1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPositionOffset:I

    .line 3034
    .local v1, "referenceOffset":I
    iput v5, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPosition:I

    .line 3035
    iput v6, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPositionOffset:I

    .line 3048
    :goto_1
    invoke-direct {p0, p1, p2, v2, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->layoutViewport(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;II)I

    .line 3049
    return-void

    .line 3021
    .end local v1    # "referenceOffset":I
    .end local v2    # "referencePosition":I
    :cond_3
    iget v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPositionOffset:I

    if-ne v3, v6, :cond_2

    .line 3023
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getPaddingTop()I

    move-result v3

    iput v3, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPositionOffset:I

    goto :goto_0

    .line 3037
    :cond_4
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getReferenceChild()Landroid/view/View;

    move-result-object v0

    .line 3038
    .local v0, "referenceChild":Landroid/view/View;
    if-eqz v0, :cond_5

    .line 3039
    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v2

    .line 3040
    .restart local v2    # "referencePosition":I
    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->getDecoratedTop(Landroid/view/View;)I

    move-result v1

    .restart local v1    # "referenceOffset":I
    goto :goto_1

    .line 3042
    .end local v1    # "referenceOffset":I
    .end local v2    # "referencePosition":I
    :cond_5
    const/4 v2, -0x1

    .line 3043
    .restart local v2    # "referencePosition":I
    const/4 v1, 0x0

    .restart local v1    # "referenceOffset":I
    goto :goto_1
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 3619
    instance-of v0, p1, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    if-eqz v0, :cond_0

    .line 3620
    check-cast p1, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    .end local p1    # "state":Landroid/os/Parcelable;
    iput-object p1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingSavedState:Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    .line 3621
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->requestLayout()V

    .line 3623
    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 3602
    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingSavedState:Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    if-eqz v2, :cond_0

    .line 3603
    new-instance v1, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    iget-object v2, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingSavedState:Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    invoke-direct {v1, v2}, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;-><init>(Lcom/google/android/play/layout/FlowLayoutManager$SavedState;)V

    .line 3614
    :goto_0
    return-object v1

    .line 3605
    :cond_0
    new-instance v1, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;

    invoke-direct {v1}, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;-><init>()V

    .line 3606
    .local v1, "state":Lcom/google/android/play/layout/FlowLayoutManager$SavedState;
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getReferenceChild()Landroid/view/View;

    move-result-object v0

    .line 3607
    .local v0, "referenceChild":Landroid/view/View;
    if-nez v0, :cond_1

    .line 3608
    const/4 v2, -0x1

    iput v2, v1, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->mReferencePosition:I

    .line 3609
    const/4 v2, 0x0

    iput v2, v1, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->mReferenceOffset:F

    goto :goto_0

    .line 3611
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v2

    iput v2, v1, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->mReferencePosition:I

    .line 3612
    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->getDecoratedTop(Landroid/view/View;)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    iput v2, v1, Lcom/google/android/play/layout/FlowLayoutManager$SavedState;->mReferenceOffset:F

    goto :goto_0
.end method

.method public scrollToPosition(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 3473
    iput p1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPosition:I

    .line 3474
    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mPendingScrollPositionOffset:I

    .line 3475
    invoke-virtual {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->requestLayout()V

    .line 3476
    return-void
.end method

.method public scrollVerticallyBy(ILandroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)I
    .locals 5
    .param p1, "dy"    # I
    .param p2, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p3, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    .line 3486
    invoke-direct {p0}, Lcom/google/android/play/layout/FlowLayoutManager;->getReferenceChild()Landroid/view/View;

    move-result-object v1

    .line 3487
    .local v1, "referenceChild":Landroid/view/View;
    if-nez v1, :cond_0

    .line 3488
    const/4 v4, 0x0

    .line 3495
    :goto_0
    return v4

    .line 3490
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v3

    .line 3492
    .local v3, "referencePosition":I
    invoke-virtual {p0, v1}, Lcom/google/android/play/layout/FlowLayoutManager;->getDecoratedTop(Landroid/view/View;)I

    move-result v4

    sub-int v2, v4, p1

    .line 3493
    .local v2, "referenceOffset":I
    invoke-direct {p0, p2, p3, v3, v2}, Lcom/google/android/play/layout/FlowLayoutManager;->layoutViewport(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;II)I

    move-result v0

    .line 3495
    .local v0, "offset":I
    sub-int v4, p1, v0

    goto :goto_0
.end method

.method public setItemChangesAffectFlow(Z)Lcom/google/android/play/layout/FlowLayoutManager;
    .locals 0
    .param p1, "itemChangesAffectFlow"    # Z

    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mItemChangesAffectFlow:Z

    .line 134
    return-object p0
.end method

.method public setOnViewRenderedListener(Lcom/google/android/play/layout/FlowLayoutManager$OnViewRenderedListener;)Lcom/google/android/play/layout/FlowLayoutManager;
    .locals 1
    .param p1, "listener"    # Lcom/google/android/play/layout/FlowLayoutManager$OnViewRenderedListener;

    .prologue
    .line 2992
    iput-object p1, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mViewRenderedListener:Lcom/google/android/play/layout/FlowLayoutManager$OnViewRenderedListener;

    .line 2993
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/layout/FlowLayoutManager;->mWasViewRenderedListenerAutoRegistered:Z

    .line 2994
    return-object p0
.end method

.method public smoothScrollToPosition(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;I)V
    .locals 2
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "state"    # Landroid/support/v7/widget/RecyclerView$State;
    .param p3, "position"    # I

    .prologue
    .line 3455
    new-instance v0, Lcom/google/android/play/layout/FlowLayoutManager$1;

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/play/layout/FlowLayoutManager$1;-><init>(Lcom/google/android/play/layout/FlowLayoutManager;Landroid/content/Context;)V

    .line 3467
    .local v0, "linearSmoothScroller":Landroid/support/v7/widget/LinearSmoothScroller;
    invoke-virtual {v0, p3}, Landroid/support/v7/widget/LinearSmoothScroller;->setTargetPosition(I)V

    .line 3468
    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/FlowLayoutManager;->startSmoothScroll(Landroid/support/v7/widget/RecyclerView$SmoothScroller;)V

    .line 3469
    return-void
.end method
