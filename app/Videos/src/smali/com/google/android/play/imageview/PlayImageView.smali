.class public Lcom/google/android/play/imageview/PlayImageView;
.super Landroid/widget/ImageView;
.source "PlayImageView.java"

# interfaces
.implements Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;


# instance fields
.field private mBlockRequestLayout:Z

.field private mCurrentImageToken:Ljava/lang/Object;

.field private mCurrentManagedBitmap:Lcom/google/android/play/imageview/PlayManagedBitmap;

.field private mCurrentManagedDrawable:Landroid/graphics/drawable/Drawable;

.field private mDefaultZoom:F

.field private mFlushedImageTemporarily:Z

.field private final mFocusPoint:Landroid/graphics/PointF;

.field private mFocusedDrawable:Landroid/graphics/drawable/Drawable;

.field private mHasDefaultZoom:Z

.field private mHasFixedBounds:Z

.field private final mMatrix:Landroid/graphics/Matrix;

.field private mPressedDrawable:Landroid/graphics/drawable/Drawable;

.field private mProviderRequestInProgress:Z

.field private final mResName:Ljava/lang/String;

.field private mTransform:Lcom/google/android/play/imageview/PlayImageViewTransform;

.field private mTransformedBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 99
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/imageview/PlayImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 100
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 103
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/imageview/PlayImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 107
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 89
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3, v4, v4}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mFocusPoint:Landroid/graphics/PointF;

    .line 90
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    iput-object v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mMatrix:Landroid/graphics/Matrix;

    .line 110
    sget-boolean v3, Lcom/google/android/play/utils/PlayCommonLog;->DEBUG:Z

    if-eqz v3, :cond_3

    .line 111
    const/4 v1, 0x0

    .line 113
    .local v1, "resName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 117
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 118
    iput-object v1, p0, Lcom/google/android/play/imageview/PlayImageView;->mResName:Ljava/lang/String;

    .line 126
    .end local v1    # "resName":Ljava/lang/String;
    :goto_1
    sget-object v3, Lcom/google/android/play/R$styleable;->PlayImageView:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 127
    .local v2, "viewAttrs":Landroid/content/res/TypedArray;
    sget v3, Lcom/google/android/play/R$styleable;->PlayImageView_fixed_bounds:I

    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mHasFixedBounds:Z

    .line 128
    sget v3, Lcom/google/android/play/R$styleable;->PlayImageView_zoom:I

    invoke-virtual {v2, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mHasDefaultZoom:Z

    .line 129
    iget-boolean v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mHasDefaultZoom:Z

    if-eqz v3, :cond_4

    .line 130
    sget v3, Lcom/google/android/play/R$styleable;->PlayImageView_zoom:I

    invoke-virtual {v2, v3, v7, v7, v5}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v3

    iput v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mDefaultZoom:F

    .line 132
    sget-object v3, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-super {p0, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 136
    :goto_2
    sget v3, Lcom/google/android/play/R$styleable;->PlayImageView_is_avatar:I

    invoke-virtual {v2, v3, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 138
    .local v0, "isAvatar":Z
    sget-boolean v3, Lcom/google/android/play/utils/PlayCommonLog;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 139
    const-string v3, "init: isAvatar=%s fixedBounds=%s hasDefaultZoom=%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v6

    iget-boolean v5, p0, Lcom/google/android/play/imageview/PlayImageView;->mHasFixedBounds:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x2

    iget-boolean v6, p0, Lcom/google/android/play/imageview/PlayImageView;->mHasDefaultZoom:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/play/utils/PlayCommonLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 143
    :cond_0
    if-eqz v0, :cond_1

    .line 144
    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/play/imageview/AvatarTransform;->getInstance(Landroid/content/res/Resources;)Lcom/google/android/play/imageview/AvatarTransform;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mTransform:Lcom/google/android/play/imageview/PlayImageViewTransform;

    .line 146
    :cond_1
    return-void

    .line 120
    .end local v0    # "isAvatar":Z
    .end local v2    # "viewAttrs":Landroid/content/res/TypedArray;
    .restart local v1    # "resName":Ljava/lang/String;
    :cond_2
    const-string v3, ""

    iput-object v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mResName:Ljava/lang/String;

    goto :goto_1

    .line 123
    .end local v1    # "resName":Ljava/lang/String;
    :cond_3
    const-string v3, ""

    iput-object v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mResName:Ljava/lang/String;

    goto :goto_1

    .line 134
    .restart local v2    # "viewAttrs":Landroid/content/res/TypedArray;
    :cond_4
    iput v5, p0, Lcom/google/android/play/imageview/PlayImageView;->mDefaultZoom:F

    goto :goto_2

    .line 114
    .end local v2    # "viewAttrs":Landroid/content/res/TypedArray;
    .restart local v1    # "resName":Ljava/lang/String;
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method private clearManagedBitmap(Z)V
    .locals 5
    .param p1, "force"    # Z

    .prologue
    const/4 v4, 0x0

    .line 193
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentManagedBitmap:Lcom/google/android/play/imageview/PlayManagedBitmap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentImageToken:Ljava/lang/Object;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentImageToken:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentManagedBitmap:Lcom/google/android/play/imageview/PlayManagedBitmap;

    iget-object v1, v1, Lcom/google/android/play/imageview/PlayManagedBitmap;->imageToken:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 197
    :cond_0
    sget-boolean v0, Lcom/google/android/play/utils/PlayCommonLog;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 198
    const-string v0, "%s clearing managed bitmap"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mResName:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/play/utils/PlayCommonLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 200
    :cond_1
    invoke-super {p0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 201
    iget-object v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentManagedBitmap:Lcom/google/android/play/imageview/PlayManagedBitmap;

    if-eqz v0, :cond_2

    .line 202
    invoke-static {}, Lcom/google/android/play/imageview/PlayImageProvider;->get()Lcom/google/android/play/imageview/PlayImageProvider;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentManagedBitmap:Lcom/google/android/play/imageview/PlayManagedBitmap;

    invoke-virtual {v0, v1}, Lcom/google/android/play/imageview/PlayImageProvider;->releaseImage(Lcom/google/android/play/imageview/PlayManagedBitmap;)V

    .line 203
    iput-object v4, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentManagedBitmap:Lcom/google/android/play/imageview/PlayManagedBitmap;

    .line 205
    :cond_2
    iput-object v4, p0, Lcom/google/android/play/imageview/PlayImageView;->mTransformedBitmap:Landroid/graphics/Bitmap;

    .line 207
    :cond_3
    return-void
.end method

.method private releaseManagedBitmap()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 149
    iget-object v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentManagedBitmap:Lcom/google/android/play/imageview/PlayManagedBitmap;

    if-eqz v0, :cond_0

    .line 150
    invoke-static {}, Lcom/google/android/play/imageview/PlayImageProvider;->get()Lcom/google/android/play/imageview/PlayImageProvider;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentManagedBitmap:Lcom/google/android/play/imageview/PlayManagedBitmap;

    invoke-virtual {v0, v1}, Lcom/google/android/play/imageview/PlayImageProvider;->releaseImage(Lcom/google/android/play/imageview/PlayManagedBitmap;)V

    .line 151
    iput-object v2, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentImageToken:Ljava/lang/Object;

    .line 152
    iput-object v2, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentManagedDrawable:Landroid/graphics/drawable/Drawable;

    .line 154
    :cond_0
    return-void
.end method

.method private requestBlockLayout()V
    .locals 1

    .prologue
    .line 382
    iget-boolean v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mHasFixedBounds:Z

    if-eqz v0, :cond_0

    .line 383
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mBlockRequestLayout:Z

    .line 385
    :cond_0
    return-void
.end method

.method private updateMatrix()V
    .locals 26

    .prologue
    .line 404
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/imageview/PlayImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v11

    .line 405
    .local v11, "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/imageview/PlayImageView;->getWidth()I

    move-result v21

    move/from16 v0, v21

    int-to-float v14, v0

    .line 406
    .local v14, "dstWidth":F
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/imageview/PlayImageView;->getHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v12, v0

    .line 407
    .local v12, "dstHeight":F
    if-eqz v11, :cond_0

    const/16 v21, 0x0

    cmpl-float v21, v14, v21

    if-eqz v21, :cond_0

    const/16 v21, 0x0

    cmpl-float v21, v12, v21

    if-nez v21, :cond_1

    .line 445
    :cond_0
    :goto_0
    return-void

    .line 411
    :cond_1
    invoke-virtual {v11}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v20, v0

    .line 412
    .local v20, "srcWidth":F
    invoke-virtual {v11}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v18, v0

    .line 413
    .local v18, "srcHeight":F
    const/16 v21, 0x0

    cmpg-float v21, v20, v21

    if-lez v21, :cond_2

    const/16 v21, 0x0

    cmpg-float v21, v18, v21

    if-gtz v21, :cond_3

    .line 416
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/imageview/PlayImageView;->mMatrix:Landroid/graphics/Matrix;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Matrix;->reset()V

    .line 444
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/imageview/PlayImageView;->mMatrix:Landroid/graphics/Matrix;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-super {v0, v1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 420
    :cond_3
    div-float v21, v14, v20

    div-float v22, v12, v18

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->max(FF)F

    move-result v17

    .line 425
    .local v17, "scale":F
    div-float v21, v14, v17

    sub-float v21, v20, v21

    const/16 v22, 0x0

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->max(FF)F

    move-result v9

    .line 426
    .local v9, "cropX":F
    div-float v21, v12, v17

    sub-float v21, v18, v21

    const/16 v22, 0x0

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->max(FF)F

    move-result v10

    .line 429
    .local v10, "cropY":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/imageview/PlayImageView;->mFocusPoint:Landroid/graphics/PointF;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v21, v0

    mul-float v6, v9, v21

    .line 430
    .local v6, "cropL":F
    sub-float v7, v9, v6

    .line 431
    .local v7, "cropR":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/imageview/PlayImageView;->mFocusPoint:Landroid/graphics/PointF;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    mul-float v8, v10, v21

    .line 432
    .local v8, "cropT":F
    sub-float v5, v10, v8

    .line 435
    .local v5, "cropB":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/imageview/PlayImageView;->mDefaultZoom:F

    move/from16 v21, v0

    const/high16 v22, 0x3f800000    # 1.0f

    sub-float v21, v21, v22

    mul-float v16, v20, v21

    .line 436
    .local v16, "extraSrcWidth":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/play/imageview/PlayImageView;->mDefaultZoom:F

    move/from16 v21, v0

    const/high16 v22, 0x3f800000    # 1.0f

    sub-float v21, v21, v22

    mul-float v15, v18, v21

    .line 437
    .local v15, "extraSrcHeight":F
    new-instance v19, Landroid/graphics/RectF;

    const/high16 v21, 0x40000000    # 2.0f

    div-float v21, v16, v21

    add-float v21, v21, v6

    const/high16 v22, 0x40000000    # 2.0f

    div-float v22, v15, v22

    add-float v22, v22, v8

    sub-float v23, v20, v7

    const/high16 v24, 0x40000000    # 2.0f

    div-float v24, v16, v24

    sub-float v23, v23, v24

    sub-float v24, v18, v5

    const/high16 v25, 0x40000000    # 2.0f

    div-float v25, v15, v25

    sub-float v24, v24, v25

    move-object/from16 v0, v19

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 440
    .local v19, "srcRect":Landroid/graphics/RectF;
    new-instance v13, Landroid/graphics/RectF;

    const/16 v21, 0x0

    const/16 v22, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-direct {v13, v0, v1, v14, v12}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 442
    .local v13, "dstRect":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/play/imageview/PlayImageView;->mMatrix:Landroid/graphics/Matrix;

    move-object/from16 v21, v0

    sget-object v22, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v13, v2}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    goto/16 :goto_1
.end method


# virtual methods
.method public clearLoadedImage()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 283
    iget-object v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentManagedBitmap:Lcom/google/android/play/imageview/PlayManagedBitmap;

    if-eqz v0, :cond_1

    .line 284
    sget-boolean v0, Lcom/google/android/play/utils/PlayCommonLog;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 285
    const-string v0, "%s beginning temporary clear for token:"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mResName:Ljava/lang/String;

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentImageToken:Ljava/lang/Object;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/play/utils/PlayCommonLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 288
    :cond_0
    invoke-direct {p0, v4}, Lcom/google/android/play/imageview/PlayImageView;->clearManagedBitmap(Z)V

    .line 289
    iput-boolean v4, p0, Lcom/google/android/play/imageview/PlayImageView;->mFlushedImageTemporarily:Z

    .line 292
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mProviderRequestInProgress:Z

    if-eqz v0, :cond_2

    .line 293
    invoke-static {}, Lcom/google/android/play/imageview/PlayImageProvider;->get()Lcom/google/android/play/imageview/PlayImageProvider;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentImageToken:Ljava/lang/Object;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/play/imageview/PlayImageProvider;->cancelRequest(Ljava/lang/Object;Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;)V

    .line 294
    iput-boolean v4, p0, Lcom/google/android/play/imageview/PlayImageView;->mFlushedImageTemporarily:Z

    .line 298
    :cond_2
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 358
    invoke-super {p0}, Landroid/widget/ImageView;->drawableStateChanged()V

    .line 359
    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->invalidate()V

    .line 360
    return-void
.end method

.method public loadImage(Ljava/lang/Object;)V
    .locals 5
    .param p1, "imageToken"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    .line 164
    sget-boolean v0, Lcom/google/android/play/utils/PlayCommonLog;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 165
    const-string v0, "%s imageToken=%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mResName:Ljava/lang/String;

    aput-object v3, v1, v2

    aput-object p1, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/play/utils/PlayCommonLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentImageToken:Ljava/lang/Object;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentImageToken:Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 168
    iget-boolean v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mProviderRequestInProgress:Z

    if-eqz v0, :cond_1

    .line 169
    invoke-static {}, Lcom/google/android/play/imageview/PlayImageProvider;->get()Lcom/google/android/play/imageview/PlayImageProvider;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentImageToken:Ljava/lang/Object;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/play/imageview/PlayImageProvider;->cancelRequest(Ljava/lang/Object;Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;)V

    .line 171
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentImageToken:Ljava/lang/Object;

    .line 176
    :cond_2
    iget-object v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentImageToken:Ljava/lang/Object;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mFlushedImageTemporarily:Z

    if-eqz v0, :cond_4

    .line 177
    :cond_3
    iput-object p1, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentImageToken:Ljava/lang/Object;

    .line 178
    invoke-static {}, Lcom/google/android/play/imageview/PlayImageProvider;->get()Lcom/google/android/play/imageview/PlayImageProvider;

    move-result-object v0

    invoke-virtual {v0, p1, p0}, Lcom/google/android/play/imageview/PlayImageProvider;->loadImage(Ljava/lang/Object;Lcom/google/android/play/imageview/PlayImageProvider$RequestListener;)V

    .line 179
    iput-boolean v4, p0, Lcom/google/android/play/imageview/PlayImageView;->mProviderRequestInProgress:Z

    .line 181
    :cond_4
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 364
    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->clearLoadedImage()V

    .line 365
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 366
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v3, 0x0

    .line 317
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 319
    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->getWidth()I

    move-result v2

    .line 320
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->getHeight()I

    move-result v1

    .line 322
    .local v1, "height":I
    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->isPressed()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->isDuplicateParentStateEnabled()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->isClickable()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 323
    .local v0, "drawPressed":Z
    :goto_0
    if-eqz v0, :cond_5

    .line 324
    iget-object v4, p0, Lcom/google/android/play/imageview/PlayImageView;->mTransform:Lcom/google/android/play/imageview/PlayImageViewTransform;

    if-eqz v4, :cond_3

    .line 326
    iget-object v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mTransform:Lcom/google/android/play/imageview/PlayImageViewTransform;

    invoke-interface {v3, p1, v2, v1}, Lcom/google/android/play/imageview/PlayImageViewTransform;->drawPressedOverlay(Landroid/graphics/Canvas;II)V

    .line 354
    :cond_1
    :goto_1
    return-void

    .end local v0    # "drawPressed":Z
    :cond_2
    move v0, v3

    .line 322
    goto :goto_0

    .line 330
    .restart local v0    # "drawPressed":Z
    :cond_3
    iget-object v4, p0, Lcom/google/android/play/imageview/PlayImageView;->mPressedDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v4, :cond_4

    .line 331
    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/play/R$drawable;->play_overlay_pressed_dark:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/play/imageview/PlayImageView;->mPressedDrawable:Landroid/graphics/drawable/Drawable;

    .line 334
    :cond_4
    iget-object v4, p0, Lcom/google/android/play/imageview/PlayImageView;->mPressedDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v3, v3, v2, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 335
    iget-object v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mPressedDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 340
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->isFocused()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 341
    iget-object v4, p0, Lcom/google/android/play/imageview/PlayImageView;->mTransform:Lcom/google/android/play/imageview/PlayImageViewTransform;

    if-eqz v4, :cond_6

    .line 343
    iget-object v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mTransform:Lcom/google/android/play/imageview/PlayImageViewTransform;

    invoke-interface {v3, p1, v2, v1}, Lcom/google/android/play/imageview/PlayImageViewTransform;->drawFocusedOverlay(Landroid/graphics/Canvas;II)V

    goto :goto_1

    .line 347
    :cond_6
    iget-object v4, p0, Lcom/google/android/play/imageview/PlayImageView;->mFocusedDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v4, :cond_7

    .line 348
    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/play/R$drawable;->overlay_focused_blue:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/play/imageview/PlayImageView;->mFocusedDrawable:Landroid/graphics/drawable/Drawable;

    .line 350
    :cond_7
    iget-object v4, p0, Lcom/google/android/play/imageview/PlayImageView;->mFocusedDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v3, v3, v2, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 351
    iget-object v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mFocusedDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1
.end method

.method public onImageFailure(Ljava/lang/Object;)V
    .locals 1
    .param p1, "imageToken"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 239
    invoke-direct {p0, v0}, Lcom/google/android/play/imageview/PlayImageView;->clearManagedBitmap(Z)V

    .line 240
    iput-boolean v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mFlushedImageTemporarily:Z

    .line 241
    iput-boolean v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mProviderRequestInProgress:Z

    .line 242
    return-void
.end method

.method public onImageLoad(Lcom/google/android/play/imageview/PlayManagedBitmap;)V
    .locals 6
    .param p1, "bitmap"    # Lcom/google/android/play/imageview/PlayManagedBitmap;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 211
    iget-object v1, p1, Lcom/google/android/play/imageview/PlayManagedBitmap;->imageToken:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentImageToken:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 212
    sget-boolean v1, Lcom/google/android/play/utils/PlayCommonLog;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 213
    const-string v1, "%s bitmap=mCurrentImageToken width=%d mTransform=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mResName:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->getWidth()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/play/imageview/PlayImageView;->mTransform:Lcom/google/android/play/imageview/PlayImageViewTransform;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/play/utils/PlayCommonLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 216
    :cond_0
    invoke-direct {p0, v5}, Lcom/google/android/play/imageview/PlayImageView;->clearManagedBitmap(Z)V

    .line 217
    iput-object p1, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentManagedBitmap:Lcom/google/android/play/imageview/PlayManagedBitmap;

    .line 218
    iget-object v1, p0, Lcom/google/android/play/imageview/PlayImageView;->mTransform:Lcom/google/android/play/imageview/PlayImageViewTransform;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->getWidth()I

    move-result v1

    if-lez v1, :cond_1

    .line 219
    iget-object v1, p0, Lcom/google/android/play/imageview/PlayImageView;->mTransform:Lcom/google/android/play/imageview/PlayImageViewTransform;

    iget-object v2, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentManagedBitmap:Lcom/google/android/play/imageview/PlayManagedBitmap;

    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->getHeight()I

    move-result v4

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/play/imageview/PlayImageViewTransform;->transform(Lcom/google/android/play/imageview/PlayManagedBitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 221
    .local v0, "transformed":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentManagedDrawable:Landroid/graphics/drawable/Drawable;

    .line 225
    .end local v0    # "transformed":Landroid/graphics/Bitmap;
    :goto_0
    iget-object v1, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentManagedDrawable:Landroid/graphics/drawable/Drawable;

    invoke-super {p0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 233
    :goto_1
    iput-boolean v5, p0, Lcom/google/android/play/imageview/PlayImageView;->mFlushedImageTemporarily:Z

    .line 234
    iput-boolean v5, p0, Lcom/google/android/play/imageview/PlayImageView;->mProviderRequestInProgress:Z

    .line 235
    return-void

    .line 223
    :cond_1
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/play/imageview/PlayManagedBitmap;->bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentManagedDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 228
    :cond_2
    sget-boolean v1, Lcom/google/android/play/utils/PlayCommonLog;->DEBUG:Z

    if-eqz v1, :cond_3

    .line 229
    const-string v1, "%s bitmap!=mCurrentImageToken, releasing bitmap"

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mResName:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/play/utils/PlayCommonLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    :cond_3
    invoke-static {}, Lcom/google/android/play/imageview/PlayImageProvider;->get()Lcom/google/android/play/imageview/PlayImageProvider;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/play/imageview/PlayImageProvider;->releaseImage(Lcom/google/android/play/imageview/PlayManagedBitmap;)V

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 377
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 378
    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->restoreLoadedImage()V

    .line 379
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 269
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 270
    iget-boolean v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mHasDefaultZoom:Z

    if-eqz v0, :cond_0

    .line 271
    invoke-direct {p0}, Lcom/google/android/play/imageview/PlayImageView;->updateMatrix()V

    .line 273
    :cond_0
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 370
    iget-boolean v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mBlockRequestLayout:Z

    if-nez v0, :cond_0

    .line 371
    invoke-super {p0}, Landroid/widget/ImageView;->requestLayout()V

    .line 373
    :cond_0
    return-void
.end method

.method public restoreLoadedImage()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 305
    iget-boolean v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mFlushedImageTemporarily:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentImageToken:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 306
    sget-boolean v0, Lcom/google/android/play/utils/PlayCommonLog;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 307
    const-string v0, "%s mCurrentImageToken=%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/play/imageview/PlayImageView;->mResName:Ljava/lang/String;

    aput-object v2, v1, v4

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentImageToken:Ljava/lang/Object;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/play/utils/PlayCommonLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentImageToken:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/google/android/play/imageview/PlayImageView;->loadImage(Ljava/lang/Object;)V

    .line 311
    iput-boolean v4, p0, Lcom/google/android/play/imageview/PlayImageView;->mFlushedImageTemporarily:Z

    .line 313
    :cond_1
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 5
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 249
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/play/imageview/PlayImageView;->mTransformedBitmap:Landroid/graphics/Bitmap;

    .line 250
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/google/android/play/imageview/PlayImageView;->mCurrentManagedDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 251
    invoke-direct {p0}, Lcom/google/android/play/imageview/PlayImageView;->releaseManagedBitmap()V

    .line 252
    instance-of v2, p1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/play/imageview/PlayImageView;->mTransform:Lcom/google/android/play/imageview/PlayImageViewTransform;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 253
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 254
    .local v0, "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 255
    .local v1, "bmp":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/google/android/play/imageview/PlayImageView;->mTransform:Lcom/google/android/play/imageview/PlayImageViewTransform;

    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/play/imageview/PlayImageView;->getHeight()I

    move-result v4

    invoke-interface {v2, v1, v3, v4}, Lcom/google/android/play/imageview/PlayImageViewTransform;->transform(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/play/imageview/PlayImageView;->mTransformedBitmap:Landroid/graphics/Bitmap;

    .line 256
    new-instance p1, Landroid/graphics/drawable/BitmapDrawable;

    .end local p1    # "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v2, p0, Lcom/google/android/play/imageview/PlayImageView;->mTransformedBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 259
    .end local v0    # "bitmapDrawable":Landroid/graphics/drawable/BitmapDrawable;
    .end local v1    # "bmp":Landroid/graphics/Bitmap;
    .restart local p1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-direct {p0}, Lcom/google/android/play/imageview/PlayImageView;->requestBlockLayout()V

    .line 260
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 261
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/play/imageview/PlayImageView;->mBlockRequestLayout:Z

    .line 262
    iget-boolean v2, p0, Lcom/google/android/play/imageview/PlayImageView;->mHasDefaultZoom:Z

    if-eqz v2, :cond_1

    .line 263
    invoke-direct {p0}, Lcom/google/android/play/imageview/PlayImageView;->updateMatrix()V

    .line 265
    :cond_1
    return-void
.end method

.method public setImageMatrix(Landroid/graphics/Matrix;)V
    .locals 2
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 397
    iget-boolean v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mHasDefaultZoom:Z

    if-eqz v0, :cond_0

    .line 398
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Can\'t mix scale type and custom zoom"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 400
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 401
    return-void
.end method

.method public setScaleType(Landroid/widget/ImageView$ScaleType;)V
    .locals 2
    .param p1, "scaleType"    # Landroid/widget/ImageView$ScaleType;

    .prologue
    .line 389
    iget-boolean v0, p0, Lcom/google/android/play/imageview/PlayImageView;->mHasDefaultZoom:Z

    if-eqz v0, :cond_0

    .line 390
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Can\'t mix scale type and custom zoom"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 392
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 393
    return-void
.end method
