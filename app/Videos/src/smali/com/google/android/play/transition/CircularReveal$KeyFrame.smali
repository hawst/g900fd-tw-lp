.class Lcom/google/android/play/transition/CircularReveal$KeyFrame;
.super Ljava/lang/Object;
.source "CircularReveal.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/play/transition/CircularReveal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "KeyFrame"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/play/transition/CircularReveal$KeyFrame;",
        ">;"
    }
.end annotation


# instance fields
.field public mAmountPct:Ljava/lang/Float;

.field public mInterpolator:Landroid/animation/TimeInterpolator;

.field public mTimePct:Ljava/lang/Float;


# direct methods
.method public constructor <init>(FFLandroid/animation/TimeInterpolator;)V
    .locals 3
    .param p1, "timePct"    # F
    .param p2, "amountPct"    # F
    .param p3, "interpolator"    # Landroid/animation/TimeInterpolator;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    cmpg-float v0, p1, v1

    if-ltz v0, :cond_0

    cmpl-float v0, p1, v2

    if-lez v0, :cond_1

    .line 198
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Time value must be between [0,1]"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 199
    :cond_1
    cmpg-float v0, p2, v1

    if-ltz v0, :cond_2

    cmpl-float v0, p2, v2

    if-lez v0, :cond_3

    .line 200
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value percentage must be between [0,1]"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_3
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mTimePct:Ljava/lang/Float;

    .line 203
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mAmountPct:Ljava/lang/Float;

    .line 204
    iput-object p3, p0, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mInterpolator:Landroid/animation/TimeInterpolator;

    .line 205
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/android/play/transition/CircularReveal$KeyFrame;)I
    .locals 2
    .param p1, "another"    # Lcom/google/android/play/transition/CircularReveal$KeyFrame;

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mTimePct:Ljava/lang/Float;

    iget-object v1, p1, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mTimePct:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/Float;->compareTo(Ljava/lang/Float;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 190
    check-cast p1, Lcom/google/android/play/transition/CircularReveal$KeyFrame;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->compareTo(Lcom/google/android/play/transition/CircularReveal$KeyFrame;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 214
    instance-of v0, p1, Lcom/google/android/play/transition/CircularReveal$KeyFrame;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mTimePct:Ljava/lang/Float;

    check-cast p1, Lcom/google/android/play/transition/CircularReveal$KeyFrame;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v1, p1, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mTimePct:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/play/transition/CircularReveal$KeyFrame;->mTimePct:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    return v0
.end method
