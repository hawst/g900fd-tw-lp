.class public Lcom/google/android/play/transition/Expando;
.super Landroid/transition/Transition;
.source "Expando.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final mHeroId:I

.field private final mLogoId:I

.field private final mSplashId:I

.field private mTempLocation:[I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 70
    sget v0, Lcom/google/android/play/R$id;->expando_hero:I

    sget v1, Lcom/google/android/play/R$id;->expando_splash:I

    sget v2, Lcom/google/android/play/R$id;->expando_logo:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/play/transition/Expando;-><init>(III)V

    .line 71
    return-void
.end method

.method public constructor <init>(III)V
    .locals 1
    .param p1, "heroId"    # I
    .param p2, "splashId"    # I
    .param p3, "logoId"    # I

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/transition/Transition;-><init>()V

    .line 44
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/play/transition/Expando;->mTempLocation:[I

    .line 61
    iput p1, p0, Lcom/google/android/play/transition/Expando;->mHeroId:I

    .line 62
    iput p2, p0, Lcom/google/android/play/transition/Expando;->mSplashId:I

    .line 63
    iput p3, p0, Lcom/google/android/play/transition/Expando;->mLogoId:I

    .line 64
    return-void
.end method

.method private captureValues(Landroid/transition/TransitionValues;)V
    .locals 14
    .param p1, "values"    # Landroid/transition/TransitionValues;

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 74
    iget-object v7, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 75
    .local v7, "view":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getId()I

    move-result v8

    iget v9, p0, Lcom/google/android/play/transition/Expando;->mHeroId:I

    if-ne v8, v9, :cond_2

    .line 76
    iget v8, p0, Lcom/google/android/play/transition/Expando;->mLogoId:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 77
    .local v3, "logo":Landroid/view/View;
    if-eqz v3, :cond_0

    .line 78
    new-instance v4, Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v8

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v9

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v10

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v11

    invoke-direct {v4, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 80
    .local v4, "logoBounds":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/google/android/play/transition/Expando;->mTempLocation:[I

    invoke-virtual {v3, v8}, Landroid/view/View;->getLocationInWindow([I)V

    .line 81
    iget-object v8, p0, Lcom/google/android/play/transition/Expando;->mTempLocation:[I

    aget v8, v8, v12

    iget-object v9, p0, Lcom/google/android/play/transition/Expando;->mTempLocation:[I

    aget v9, v9, v13

    invoke-virtual {v4, v8, v9}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 82
    iget-object v8, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v9, "android:expando:logoBounds"

    invoke-interface {v8, v9, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v8

    if-lez v8, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v8

    if-lez v8, :cond_0

    .line 84
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v8

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v9

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 86
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 87
    .local v2, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v3, v2}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 88
    iget-object v8, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v9, "android:expando:logoSnapshot"

    invoke-interface {v8, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "canvas":Landroid/graphics/Canvas;
    .end local v4    # "logoBounds":Landroid/graphics/Rect;
    :cond_0
    iget v8, p0, Lcom/google/android/play/transition/Expando;->mSplashId:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 92
    .local v5, "splash":Landroid/view/View;
    if-eqz v5, :cond_1

    .line 93
    new-instance v6, Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v8

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v9

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v10

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v11

    invoke-direct {v6, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 95
    .local v6, "splashBounds":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/google/android/play/transition/Expando;->mTempLocation:[I

    invoke-virtual {v5, v8}, Landroid/view/View;->getLocationInWindow([I)V

    .line 96
    iget-object v8, p0, Lcom/google/android/play/transition/Expando;->mTempLocation:[I

    aget v8, v8, v12

    iget-object v9, p0, Lcom/google/android/play/transition/Expando;->mTempLocation:[I

    aget v9, v9, v13

    invoke-virtual {v6, v8, v9}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 97
    iget-object v8, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v9, "android:expando:splashBounds"

    invoke-interface {v8, v9, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v8

    if-lez v8, :cond_1

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v8

    if-lez v8, :cond_1

    .line 99
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v8

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v9

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v8, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 101
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 102
    .restart local v2    # "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v5, v2}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 103
    iget-object v8, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v9, "android:expando:splashSnapshot"

    invoke-interface {v8, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "canvas":Landroid/graphics/Canvas;
    .end local v6    # "splashBounds":Landroid/graphics/Rect;
    :cond_1
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v8

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v9

    invoke-virtual {v7}, Landroid/view/View;->getRight()I

    move-result v10

    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v11

    invoke-direct {v1, v8, v9, v10, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 109
    .local v1, "bounds":Landroid/graphics/Rect;
    iget-object v8, p0, Lcom/google/android/play/transition/Expando;->mTempLocation:[I

    invoke-virtual {v7, v8}, Landroid/view/View;->getLocationInWindow([I)V

    .line 110
    iget-object v8, p0, Lcom/google/android/play/transition/Expando;->mTempLocation:[I

    aget v8, v8, v12

    iget-object v9, p0, Lcom/google/android/play/transition/Expando;->mTempLocation:[I

    aget v9, v9, v13

    invoke-virtual {v1, v8, v9}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 111
    iget-object v8, p1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v9, "android:expando:bounds"

    invoke-interface {v8, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    .end local v1    # "bounds":Landroid/graphics/Rect;
    .end local v3    # "logo":Landroid/view/View;
    .end local v5    # "splash":Landroid/view/View;
    :cond_2
    return-void
.end method

.method private fadeOutAnimator(Landroid/view/View;Landroid/view/ViewGroup;J)Landroid/animation/ObjectAnimator;
    .locals 3
    .param p1, "snapshot"    # Landroid/view/View;
    .param p2, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p3, "duration"    # J

    .prologue
    .line 398
    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 399
    .local v0, "fade":Landroid/animation/ObjectAnimator;
    new-instance v1, Lcom/google/android/play/transition/Expando$3;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/play/transition/Expando$3;-><init>(Lcom/google/android/play/transition/Expando;Landroid/view/ViewGroup;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 405
    invoke-virtual {v0, p3, p4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 406
    return-object v0

    .line 398
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private getHeroAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 44
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 138
    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    move-object/from16 v25, v0

    .line 139
    .local v25, "heroView":Landroid/view/View;
    move-object/from16 v0, p2

    iget-object v4, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v12, "android:expando:bounds"

    invoke-interface {v4, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Landroid/graphics/Rect;

    .line 140
    .local v41, "startBounds":Landroid/graphics/Rect;
    move-object/from16 v0, p3

    iget-object v4, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v12, "android:expando:bounds"

    invoke-interface {v4, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/graphics/Rect;

    .line 141
    .local v20, "endBounds":Landroid/graphics/Rect;
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual/range {v41 .. v41}, Landroid/graphics/Rect;->width()I

    move-result v12

    if-le v4, v12, :cond_8

    const/4 v10, 0x1

    .line 143
    .local v10, "isExpando":Z
    :goto_0
    move-object/from16 v0, p2

    iget-object v4, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v12, "android:expando:logoBounds"

    invoke-interface {v4, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/graphics/Rect;

    .line 144
    .local v14, "logoStartBounds":Landroid/graphics/Rect;
    move-object/from16 v0, p3

    iget-object v4, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v12, "android:expando:logoBounds"

    invoke-interface {v4, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/graphics/Rect;

    .line 145
    .local v15, "logoEndBounds":Landroid/graphics/Rect;
    const-string v4, "android:expando:logoSnapshot"

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v10, v1, v2, v4}, Lcom/google/android/play/transition/Expando;->getSnapshot(ZLandroid/transition/TransitionValues;Landroid/transition/TransitionValues;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v29

    .line 147
    .local v29, "logoBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p2

    iget-object v4, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v12, "android:expando:splashBounds"

    invoke-interface {v4, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Rect;

    .line 148
    .local v6, "splashStartBounds":Landroid/graphics/Rect;
    move-object/from16 v0, p3

    iget-object v4, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v12, "android:expando:splashBounds"

    invoke-interface {v4, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/Rect;

    .line 149
    .local v7, "splashEndBounds":Landroid/graphics/Rect;
    const-string v4, "android:expando:splashSnapshot"

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v10, v1, v2, v4}, Lcom/google/android/play/transition/Expando;->getSnapshot(ZLandroid/transition/TransitionValues;Landroid/transition/TransitionValues;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v40

    .line 152
    .local v40, "splashBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/transition/Expando;->mTempLocation:[I

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    .line 153
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/transition/Expando;->mTempLocation:[I

    const/4 v12, 0x0

    aget v31, v4, v12

    .line 154
    .local v31, "offsetX":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/play/transition/Expando;->mTempLocation:[I

    const/4 v12, 0x1

    aget v34, v4, v12

    .line 157
    .local v34, "offsetY":I
    const-wide/16 v32, 0x1f4

    .line 158
    .local v32, "minDuration":J
    const-wide/16 v36, 0x1f4

    .line 159
    .local v36, "scalingDuration":J
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    invoke-virtual/range {v41 .. v41}, Landroid/graphics/Rect;->centerY()I

    move-result v12

    sub-int/2addr v4, v12

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v12

    int-to-float v12, v12

    div-float v35, v4, v12

    .line 161
    .local v35, "percentage":F
    move-wide/from16 v0, v32

    long-to-float v4, v0

    move-wide/from16 v0, v36

    long-to-float v12, v0

    mul-float v12, v12, v35

    add-float/2addr v4, v12

    float-to-long v8, v4

    .line 162
    .local v8, "moveDuration":J
    add-long v16, v32, v36

    sub-long v16, v16, v8

    const-wide/16 v42, 0xc8

    move-wide/from16 v0, v16

    move-wide/from16 v2, v42

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v22

    .line 165
    .local v22, "fadeDuration":J
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/play/transition/Expando;->mSplashId:I

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v38

    .line 166
    .local v38, "splash":Landroid/view/View;
    const/16 v39, 0x0

    .line 167
    .local v39, "splashAnimator":Landroid/animation/Animator;
    const/4 v5, 0x0

    .line 168
    .local v5, "splashSnapshot":Landroid/view/View;
    if-eqz v38, :cond_0

    .line 174
    if-eqz v10, :cond_9

    .line 175
    move-object/from16 v0, p0

    move-object/from16 v1, v41

    invoke-direct {v0, v1, v7}, Lcom/google/android/play/transition/Expando;->resizeBounds(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v6

    .line 179
    :goto_1
    move/from16 v0, v31

    move/from16 v1, v34

    invoke-virtual {v6, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 180
    move/from16 v0, v31

    move/from16 v1, v34

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 181
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2, v6}, Lcom/google/android/play/transition/Expando;->makeSnapshot(Landroid/view/ViewGroup;Landroid/graphics/Bitmap;Landroid/graphics/Rect;)Landroid/view/View;

    move-result-object v5

    move-object/from16 v4, p0

    .line 182
    invoke-direct/range {v4 .. v10}, Lcom/google/android/play/transition/Expando;->splashAnimator(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;JZ)Landroid/animation/Animator;

    move-result-object v39

    .line 190
    :cond_0
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/play/transition/Expando;->mLogoId:I

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    .line 191
    .local v27, "logo":Landroid/view/View;
    const/16 v28, 0x0

    .line 192
    .local v28, "logoAnimator":Landroid/animation/Animator;
    const/4 v13, 0x0

    .line 193
    .local v13, "logoSnapshot":Landroid/view/View;
    if-eqz v27, :cond_3

    .line 194
    if-eqz v10, :cond_a

    .line 195
    new-instance v14, Landroid/graphics/Rect;

    .end local v14    # "logoStartBounds":Landroid/graphics/Rect;
    move-object/from16 v0, v41

    invoke-direct {v14, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 199
    .restart local v14    # "logoStartBounds":Landroid/graphics/Rect;
    :goto_2
    invoke-virtual {v15}, Landroid/graphics/Rect;->width()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v15}, Landroid/graphics/Rect;->height()I

    move-result v4

    if-nez v4, :cond_2

    .line 200
    :cond_1
    new-instance v30, Landroid/graphics/Rect;

    invoke-virtual {v15}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    sub-int/2addr v4, v12

    invoke-virtual {v15}, Landroid/graphics/Rect;->centerY()I

    move-result v12

    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v16

    div-int/lit8 v16, v16, 0x2

    sub-int v12, v12, v16

    invoke-virtual {v15}, Landroid/graphics/Rect;->centerX()I

    move-result v16

    invoke-virtual {v14}, Landroid/graphics/Rect;->width()I

    move-result v17

    div-int/lit8 v17, v17, 0x2

    add-int v16, v16, v17

    invoke-virtual {v15}, Landroid/graphics/Rect;->centerY()I

    move-result v17

    invoke-virtual {v14}, Landroid/graphics/Rect;->height()I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    add-int v17, v17, v18

    move-object/from16 v0, v30

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-direct {v0, v4, v12, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .end local v15    # "logoEndBounds":Landroid/graphics/Rect;
    .local v30, "logoEndBounds":Landroid/graphics/Rect;
    move-object/from16 v15, v30

    .line 205
    .end local v30    # "logoEndBounds":Landroid/graphics/Rect;
    .restart local v15    # "logoEndBounds":Landroid/graphics/Rect;
    :cond_2
    move/from16 v0, v31

    move/from16 v1, v34

    invoke-virtual {v14, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 206
    move/from16 v0, v31

    move/from16 v1, v34

    invoke-virtual {v15, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 207
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v29

    invoke-direct {v0, v1, v2, v14}, Lcom/google/android/play/transition/Expando;->makeSnapshot(Landroid/view/ViewGroup;Landroid/graphics/Bitmap;Landroid/graphics/Rect;)Landroid/view/View;

    move-result-object v13

    move-object/from16 v12, p0

    move-wide/from16 v16, v8

    move/from16 v18, v10

    .line 208
    invoke-direct/range {v12 .. v18}, Lcom/google/android/play/transition/Expando;->logoAnimator(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;JZ)Landroid/animation/Animator;

    move-result-object v28

    .line 216
    :cond_3
    const-string v4, "alpha"

    const/4 v12, 0x2

    new-array v12, v12, [F

    fill-array-data v12, :array_0

    move-object/from16 v0, v25

    invoke-static {v0, v4, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v24

    .line 217
    .local v24, "fadeOut":Landroid/animation/Animator;
    const-wide/16 v16, 0x0

    move-object/from16 v0, v24

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 219
    new-instance v11, Landroid/animation/AnimatorSet;

    invoke-direct {v11}, Landroid/animation/AnimatorSet;-><init>()V

    .line 220
    .local v11, "animSet":Landroid/animation/AnimatorSet;
    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v19

    .line 221
    .local v19, "builder":Landroid/animation/AnimatorSet$Builder;
    move-object/from16 v26, v24

    .line 222
    .local v26, "lastAnimator":Landroid/animation/Animator;
    if-eqz v39, :cond_4

    .line 223
    move-object/from16 v0, v19

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 224
    move-object/from16 v26, v39

    .line 226
    :cond_4
    if-eqz v28, :cond_5

    .line 227
    move-object/from16 v0, v19

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 228
    move-object/from16 v26, v28

    .line 231
    :cond_5
    if-eqz v10, :cond_b

    .line 232
    const-string v4, "alpha"

    const/4 v12, 0x2

    new-array v12, v12, [F

    fill-array-data v12, :array_1

    move-object/from16 v0, v25

    invoke-static {v0, v4, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v21

    .line 233
    .local v21, "fadeIn":Landroid/animation/Animator;
    new-instance v4, Lcom/google/android/play/transition/Expando$1;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v4, v0, v1}, Lcom/google/android/play/transition/Expando$1;-><init>(Lcom/google/android/play/transition/Expando;Landroid/view/View;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 240
    const-wide/16 v16, 0x0

    move-object/from16 v0, v21

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 241
    move-object/from16 v0, v26

    invoke-virtual {v11, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v19

    .line 242
    if-eqz v13, :cond_6

    .line 243
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, v22

    invoke-direct {v0, v13, v1, v2, v3}, Lcom/google/android/play/transition/Expando;->fadeOutAnimator(Landroid/view/View;Landroid/view/ViewGroup;J)Landroid/animation/ObjectAnimator;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 245
    :cond_6
    if-eqz v5, :cond_7

    .line 246
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, v22

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/google/android/play/transition/Expando;->fadeOutAnimator(Landroid/view/View;Landroid/view/ViewGroup;J)Landroid/animation/ObjectAnimator;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 256
    .end local v21    # "fadeIn":Landroid/animation/Animator;
    :cond_7
    :goto_3
    return-object v11

    .line 141
    .end local v5    # "splashSnapshot":Landroid/view/View;
    .end local v6    # "splashStartBounds":Landroid/graphics/Rect;
    .end local v7    # "splashEndBounds":Landroid/graphics/Rect;
    .end local v8    # "moveDuration":J
    .end local v10    # "isExpando":Z
    .end local v11    # "animSet":Landroid/animation/AnimatorSet;
    .end local v13    # "logoSnapshot":Landroid/view/View;
    .end local v14    # "logoStartBounds":Landroid/graphics/Rect;
    .end local v15    # "logoEndBounds":Landroid/graphics/Rect;
    .end local v19    # "builder":Landroid/animation/AnimatorSet$Builder;
    .end local v22    # "fadeDuration":J
    .end local v24    # "fadeOut":Landroid/animation/Animator;
    .end local v26    # "lastAnimator":Landroid/animation/Animator;
    .end local v27    # "logo":Landroid/view/View;
    .end local v28    # "logoAnimator":Landroid/animation/Animator;
    .end local v29    # "logoBitmap":Landroid/graphics/Bitmap;
    .end local v31    # "offsetX":I
    .end local v32    # "minDuration":J
    .end local v34    # "offsetY":I
    .end local v35    # "percentage":F
    .end local v36    # "scalingDuration":J
    .end local v38    # "splash":Landroid/view/View;
    .end local v39    # "splashAnimator":Landroid/animation/Animator;
    .end local v40    # "splashBitmap":Landroid/graphics/Bitmap;
    :cond_8
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 177
    .restart local v5    # "splashSnapshot":Landroid/view/View;
    .restart local v6    # "splashStartBounds":Landroid/graphics/Rect;
    .restart local v7    # "splashEndBounds":Landroid/graphics/Rect;
    .restart local v8    # "moveDuration":J
    .restart local v10    # "isExpando":Z
    .restart local v14    # "logoStartBounds":Landroid/graphics/Rect;
    .restart local v15    # "logoEndBounds":Landroid/graphics/Rect;
    .restart local v22    # "fadeDuration":J
    .restart local v29    # "logoBitmap":Landroid/graphics/Bitmap;
    .restart local v31    # "offsetX":I
    .restart local v32    # "minDuration":J
    .restart local v34    # "offsetY":I
    .restart local v35    # "percentage":F
    .restart local v36    # "scalingDuration":J
    .restart local v38    # "splash":Landroid/view/View;
    .restart local v39    # "splashAnimator":Landroid/animation/Animator;
    .restart local v40    # "splashBitmap":Landroid/graphics/Bitmap;
    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v6}, Lcom/google/android/play/transition/Expando;->resizeBounds(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v7

    goto/16 :goto_1

    .line 197
    .restart local v13    # "logoSnapshot":Landroid/view/View;
    .restart local v27    # "logo":Landroid/view/View;
    .restart local v28    # "logoAnimator":Landroid/animation/Animator;
    :cond_a
    new-instance v15, Landroid/graphics/Rect;

    .end local v15    # "logoEndBounds":Landroid/graphics/Rect;
    move-object/from16 v0, v20

    invoke-direct {v15, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .restart local v15    # "logoEndBounds":Landroid/graphics/Rect;
    goto/16 :goto_2

    .line 249
    .restart local v11    # "animSet":Landroid/animation/AnimatorSet;
    .restart local v19    # "builder":Landroid/animation/AnimatorSet$Builder;
    .restart local v24    # "fadeOut":Landroid/animation/Animator;
    .restart local v26    # "lastAnimator":Landroid/animation/Animator;
    :cond_b
    if-eqz v38, :cond_c

    .line 250
    const/4 v4, 0x4

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 252
    :cond_c
    if-eqz v27, :cond_7

    .line 253
    const/4 v4, 0x4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 216
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 232
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private getSnapshot(ZLandroid/transition/TransitionValues;Landroid/transition/TransitionValues;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "isExpando"    # Z
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;
    .param p4, "key"    # Ljava/lang/String;

    .prologue
    .line 261
    if-eqz p1, :cond_1

    move-object v1, p3

    :goto_0
    iget-object v1, v1, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    invoke-interface {v1, p4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 264
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 265
    if-eqz p1, :cond_2

    .end local p2    # "startValues":Landroid/transition/TransitionValues;
    :goto_1
    iget-object v1, p2, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    invoke-interface {v1, p4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    check-cast v0, Landroid/graphics/Bitmap;

    .line 267
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    return-object v0

    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local p2    # "startValues":Landroid/transition/TransitionValues;
    :cond_1
    move-object v1, p2

    .line 261
    goto :goto_0

    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    move-object p2, p3

    .line 265
    goto :goto_1
.end method

.method private logoAnimator(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;JZ)Landroid/animation/Animator;
    .locals 14
    .param p1, "logoSnapshot"    # Landroid/view/View;
    .param p2, "startBounds"    # Landroid/graphics/Rect;
    .param p3, "endBounds"    # Landroid/graphics/Rect;
    .param p4, "moveDuration"    # J
    .param p6, "isExpando"    # Z

    .prologue
    .line 338
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->width()I

    move-result v10

    int-to-float v10, v10

    div-float v6, v9, v10

    .line 339
    .local v6, "scaleXStart":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->height()I

    move-result v10

    int-to-float v10, v10

    div-float v8, v9, v10

    .line 340
    .local v8, "scaleYStart":F
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Landroid/view/View;->setPivotX(F)V

    .line 341
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Landroid/view/View;->setPivotY(F)V

    .line 342
    if-eqz p6, :cond_0

    .line 343
    invoke-virtual {p1, v6}, Landroid/view/View;->setScaleX(F)V

    .line 344
    invoke-virtual {p1, v8}, Landroid/view/View;->setScaleY(F)V

    .line 346
    :cond_0
    move-object/from16 v0, p2

    iget v9, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p2

    iget v10, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p2

    iget v11, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->width()I

    move-result v12

    add-int/2addr v11, v12

    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->height()I

    move-result v13

    add-int/2addr v12, v13

    invoke-virtual {p1, v9, v10, v11, v12}, Landroid/view/View;->layout(IIII)V

    .line 351
    invoke-direct/range {p0 .. p5}, Lcom/google/android/play/transition/Expando;->pathAnimator(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;J)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 355
    .local v4, "pathAnimator":Landroid/animation/ObjectAnimator;
    const-string v9, "scaleX"

    const/4 v10, 0x2

    new-array v10, v10, [F

    const/4 v11, 0x0

    aput v6, v10, v11

    const/4 v11, 0x1

    const/high16 v12, 0x3f800000    # 1.0f

    aput v12, v10, v11

    invoke-static {p1, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 356
    .local v5, "scaleX":Landroid/animation/ObjectAnimator;
    const-string v9, "scaleY"

    const/4 v10, 0x2

    new-array v10, v10, [F

    const/4 v11, 0x0

    aput v8, v10, v11

    const/4 v11, 0x1

    const/high16 v12, 0x3f800000    # 1.0f

    aput v12, v10, v11

    invoke-static {p1, v9, v10}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    .line 357
    .local v7, "scaleY":Landroid/animation/ObjectAnimator;
    move-wide/from16 v0, p4

    invoke-virtual {v5, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 358
    move-wide/from16 v0, p4

    invoke-virtual {v7, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 361
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 362
    .local v3, "logoAnimator":Landroid/animation/AnimatorSet;
    invoke-virtual {v3, v5}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    .line 363
    .local v2, "builder":Landroid/animation/AnimatorSet$Builder;
    if-eqz v4, :cond_1

    .line 364
    invoke-virtual {v2, v4}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 366
    :cond_1
    return-object v3
.end method

.method private makeSnapshot(Landroid/view/ViewGroup;Landroid/graphics/Bitmap;Landroid/graphics/Rect;)Landroid/view/View;
    .locals 6
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "bitmapSnapshot"    # Landroid/graphics/Bitmap;
    .param p3, "bounds"    # Landroid/graphics/Rect;

    .prologue
    .line 272
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 273
    .local v1, "snapshot":Landroid/widget/ImageView;
    if-nez p2, :cond_0

    .line 274
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    sget v3, Lcom/google/android/play/R$color;->play_disabled_grey:I

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 279
    :goto_0
    iget v2, p3, Landroid/graphics/Rect;->left:I

    iget v3, p3, Landroid/graphics/Rect;->top:I

    iget v4, p3, Landroid/graphics/Rect;->right:I

    iget v5, p3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/ImageView;->layout(IIII)V

    .line 280
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getOverlay()Landroid/view/ViewGroupOverlay;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewGroupOverlay;->add(Landroid/view/View;)V

    .line 281
    return-object v1

    .line 276
    :cond_0
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 277
    .local v0, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private pathAnimator(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;J)Landroid/animation/ObjectAnimator;
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "startBounds"    # Landroid/graphics/Rect;
    .param p3, "endBounds"    # Landroid/graphics/Rect;
    .param p4, "duration"    # J

    .prologue
    .line 384
    invoke-virtual {p2, p3}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 385
    const/4 v1, 0x0

    .line 393
    :goto_0
    return-object v1

    .line 387
    :cond_0
    new-instance v2, Landroid/transition/ArcMotion;

    invoke-direct {v2}, Landroid/transition/ArcMotion;-><init>()V

    iget v3, p2, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget v4, p2, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    iget v5, p3, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    iget v6, p3, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/transition/ArcMotion;->getPath(FFFF)Landroid/graphics/Path;

    move-result-object v0

    .line 389
    .local v0, "path":Landroid/graphics/Path;
    const-string v2, "x"

    const-string v3, "y"

    invoke-static {p1, v2, v3, v0}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Path;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 390
    .local v1, "pathAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x10c000d

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 392
    invoke-virtual {v1, p4, p5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    goto :goto_0
.end method

.method private resizeBounds(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 6
    .param p1, "locationRect"    # Landroid/graphics/Rect;
    .param p2, "sizeRect"    # Landroid/graphics/Rect;

    .prologue
    .line 285
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method private revealAnimator(Landroid/view/View;JZ)Landroid/animation/Animator;
    .locals 10
    .param p1, "snapshot"    # Landroid/view/View;
    .param p2, "duration"    # J
    .param p4, "isExpando"    # Z

    .prologue
    const/high16 v8, 0x40800000    # 4.0f

    const/4 v4, 0x0

    .line 370
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    .line 371
    .local v5, "width":I
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 372
    .local v2, "height":I
    mul-int v6, v5, v5

    int-to-float v6, v6

    div-float/2addr v6, v8

    mul-int v7, v2, v2

    int-to-float v7, v7

    div-float/2addr v7, v8

    add-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-float v1, v6

    .line 373
    .local v1, "fullRadius":F
    if-eqz p4, :cond_0

    move v0, v1

    .line 374
    .local v0, "endRadius":F
    :goto_0
    if-eqz p4, :cond_1

    .line 375
    .local v4, "startRadius":F
    :goto_1
    div-int/lit8 v6, v5, 0x2

    div-int/lit8 v7, v2, 0x2

    invoke-static {p1, v6, v7, v4, v0}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v3

    .line 377
    .local v3, "reveal":Landroid/animation/Animator;
    invoke-virtual {v3, p2, p3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 378
    return-object v3

    .end local v0    # "endRadius":F
    .end local v3    # "reveal":Landroid/animation/Animator;
    .end local v4    # "startRadius":F
    :cond_0
    move v0, v4

    .line 373
    goto :goto_0

    .restart local v0    # "endRadius":F
    :cond_1
    move v4, v1

    .line 374
    goto :goto_1
.end method

.method private splashAnimator(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;JZ)Landroid/animation/Animator;
    .locals 6
    .param p1, "splashSnapshot"    # Landroid/view/View;
    .param p2, "startBounds"    # Landroid/graphics/Rect;
    .param p3, "endBounds"    # Landroid/graphics/Rect;
    .param p4, "moveDuration"    # J
    .param p6, "isExpando"    # Z

    .prologue
    .line 298
    if-eqz p6, :cond_0

    .line 299
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 303
    :cond_0
    invoke-direct/range {p0 .. p5}, Lcom/google/android/play/transition/Expando;->pathAnimator(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 307
    .local v1, "pathAnimator":Landroid/animation/ObjectAnimator;
    invoke-direct {p0, p1, p4, p5, p6}, Lcom/google/android/play/transition/Expando;->revealAnimator(Landroid/view/View;JZ)Landroid/animation/Animator;

    move-result-object v2

    .line 308
    .local v2, "reveal":Landroid/animation/Animator;
    new-instance v4, Lcom/google/android/play/transition/Expando$2;

    invoke-direct {v4, p0, p6, p1}, Lcom/google/android/play/transition/Expando$2;-><init>(Lcom/google/android/play/transition/Expando;ZLandroid/view/View;)V

    invoke-virtual {v2, v4}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 325
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 326
    .local v3, "splashAnimator":Landroid/animation/AnimatorSet;
    invoke-virtual {v3, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v0

    .line 327
    .local v0, "builder":Landroid/animation/AnimatorSet$Builder;
    if-eqz v1, :cond_1

    .line 328
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 330
    :cond_1
    return-object v3
.end method


# virtual methods
.method public captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 0
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 117
    invoke-direct {p0, p1}, Lcom/google/android/play/transition/Expando;->captureValues(Landroid/transition/TransitionValues;)V

    .line 118
    return-void
.end method

.method public captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 0
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/google/android/play/transition/Expando;->captureValues(Landroid/transition/TransitionValues;)V

    .line 123
    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 2
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 128
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    iget-object v0, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    iget v1, p0, Lcom/google/android/play/transition/Expando;->mHeroId:I

    if-ne v0, v1, :cond_0

    .line 129
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/transition/Expando;->getHeroAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;

    move-result-object v0

    .line 131
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
