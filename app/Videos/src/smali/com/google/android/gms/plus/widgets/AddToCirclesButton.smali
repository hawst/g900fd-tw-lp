.class public final Lcom/google/android/gms/plus/widgets/AddToCirclesButton;
.super Landroid/widget/FrameLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/widgets/AddToCirclesButton$1;,
        Lcom/google/android/gms/plus/widgets/AddToCirclesButton$a;,
        Lcom/google/android/gms/plus/widgets/AddToCirclesButton$c;,
        Lcom/google/android/gms/plus/widgets/AddToCirclesButton$b;
    }
.end annotation


# static fields
.field protected static final PACKAGE_IMPLEMENTATION_CLASS_NAME:Ljava/lang/String; = "com.google.android.gms.plus.circlesbutton.AddToCirclesButtonImpl$DynamiteHost"

.field public static final SIZE_CUSTOM:I = 0x0

.field public static final SIZE_LARGE:I = 0x3

.field public static final SIZE_MEDIUM:I = 0x2

.field public static final SIZE_SMALL:I = 0x1

.field private static TE:Landroid/content/Context; = null

.field public static final TYPE_ADD:I = 0x0

.field public static final TYPE_BLOCKED:I = 0x3

.field public static final TYPE_FOLLOW:I = 0x1

.field public static final TYPE_ONE_CLICK_FOLLOW:I = 0x2


# instance fields
.field private Jo:Landroid/view/View;

.field private aHG:I

.field private final aKf:Lcom/google/android/gms/internal/th;

.field private final aKg:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->aKg:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->I(Landroid/content/Context;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/internal/th;

    iput-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->aKf:Lcom/google/android/gms/internal/th;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->aKf:Lcom/google/android/gms/internal/th;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/dynamic/e;->q(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v2

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-static {v1}, Lcom/google/android/gms/dynamic/e;->q(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/gms/dynamic/e;->q(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v3

    invoke-interface {v0, v2, v1, v3}, Lcom/google/android/gms/internal/th;->a(Lcom/google/android/gms/dynamic/d;Lcom/google/android/gms/dynamic/d;Lcom/google/android/gms/dynamic/d;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->aKf:Lcom/google/android/gms/internal/th;

    invoke-interface {v0}, Lcom/google/android/gms/internal/th;->getView()Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->i(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->Jo:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->Jo:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->addView(Landroid/view/View;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static I(Landroid/content/Context;)Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/gms/internal/th;",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->TE:Landroid/content/Context;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->getRemoteContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->TE:Landroid/content/Context;

    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->TE:Landroid/content/Context;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->TE:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    :try_start_0
    const-string v1, "com.google.android.gms.plus.circlesbutton.AddToCirclesButtonImpl$DynamiteHost"

    invoke-virtual {v0, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    new-instance v1, Landroid/util/Pair;

    invoke-static {v0}, Lcom/google/android/gms/internal/th$a;->dx(Landroid/os/IBinder;)Lcom/google/android/gms/internal/th;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->TE:Landroid/content/Context;

    invoke-direct {v1, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :goto_1
    if-eqz v0, :cond_1

    const-string v1, "AddToCirclesButton"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "AddToCirclesButton"

    const-string v2, "Can\'t load com.google.android.gms.plus.circlesbutton.AddToCirclesButtonImpl$DynamiteHost"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    new-instance v0, Landroid/util/Pair;

    new-instance v1, Lcom/google/android/gms/plus/widgets/AddToCirclesButton$a;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton$a;-><init>(Lcom/google/android/gms/plus/widgets/AddToCirclesButton$1;)V

    invoke-direct {v0, v1, p0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/plus/widgets/AddToCirclesButton;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->aKg:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/widgets/AddToCirclesButton;)I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->aHG:I

    return v0
.end method


# virtual methods
.method public configure(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/people/data/AudienceMember;ILjava/lang/String;)V
    .locals 7
    .param p1, "ownerAccount"    # Ljava/lang/String;
    .param p2, "ownerPageId"    # Ljava/lang/String;
    .param p3, "targetMember"    # Lcom/google/android/gms/common/people/data/AudienceMember;
    .param p4, "activityRequestCode"    # I
    .param p5, "applicationId"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->configure(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/people/data/AudienceMember;ILjava/lang/String;Lcom/google/android/gms/plus/widgets/AddToCirclesButton$c;)V

    return-void
.end method

.method public configure(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/people/data/AudienceMember;ILjava/lang/String;Lcom/google/android/gms/plus/widgets/AddToCirclesButton$c;)V
    .locals 6
    .param p1, "ownerAccount"    # Ljava/lang/String;
    .param p2, "ownerPageId"    # Ljava/lang/String;
    .param p3, "targetMember"    # Lcom/google/android/gms/common/people/data/AudienceMember;
    .param p4, "activityRequestCode"    # I
    .param p5, "applicationId"    # Ljava/lang/String;
    .param p6, "listener"    # Lcom/google/android/gms/plus/widgets/AddToCirclesButton$c;

    .prologue
    invoke-static {p1}, Lcom/google/android/gms/internal/kn;->k(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/gms/internal/kn;->k(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    const-string v1, "The AddToCirclesButton must be placed in an Activity."

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/kn;->a(ZLjava/lang/Object;)V

    iput p4, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->aHG:I

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->aKf:Lcom/google/android/gms/internal/th;

    new-instance v5, Lcom/google/android/gms/plus/widgets/AddToCirclesButton$b;

    invoke-direct {v5, p0, p6}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton$b;-><init>(Lcom/google/android/gms/plus/widgets/AddToCirclesButton;Lcom/google/android/gms/plus/widgets/AddToCirclesButton$c;)V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/internal/th;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/people/data/AudienceMember;Ljava/lang/String;Lcom/google/android/gms/internal/ti;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AddToCirclesButton"

    const-string v1, "Add to circles button failed to configure."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getContentView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->Jo:Landroid/view/View;

    return-object v0
.end method

.method public refreshButton()V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->setShowProgressIndicator(Z)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->aKf:Lcom/google/android/gms/internal/th;

    invoke-interface {v0}, Lcom/google/android/gms/internal/th;->refreshButton()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AddToCirclesButton"

    const-string v1, "Add to circles button failed to refreshButton."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setAnalyticsStartView(Ljava/lang/String;I)V
    .locals 2
    .param p1, "startViewNamespace"    # Ljava/lang/String;
    .param p2, "startViewTypeNum"    # I

    .prologue
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->aKf:Lcom/google/android/gms/internal/th;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/internal/th;->setAnalyticsStartView(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AddToCirclesButton"

    const-string v1, "Add to circles button failed to setAnalyticsStartView."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setShowProgressIndicator(Z)V
    .locals 2
    .param p1, "showProgress"    # Z

    .prologue
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->aKf:Lcom/google/android/gms/internal/th;

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/th;->setShowProgressIndicator(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AddToCirclesButton"

    const-string v1, "Add to circles button failed to setShowProgressIndicator."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setSize(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->aKf:Lcom/google/android/gms/internal/th;

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/th;->setSize(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AddToCirclesButton"

    const-string v1, "Add to circles button failed to setSize."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->aKf:Lcom/google/android/gms/internal/th;

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/th;->setType(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AddToCirclesButton"

    const-string v1, "Add to circles button failed to setType."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
