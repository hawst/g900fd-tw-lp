.class Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$2;
.super Ljava/lang/Object;
.source "AppDataSearchProvider.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->registerCorpora([Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;

.field final synthetic val$appInfo:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

.field final synthetic val$corpora:Ljava/util/Set;

.field final synthetic val$tables:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;Ljava/util/Set;Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$2;->this$0:Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$2;->val$corpora:Ljava/util/Set;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$2;->val$appInfo:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$2;->val$tables:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Integer;
    .locals 5

    .prologue
    .line 160
    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$2;->this$0:Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->mSearchClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    iget-object v4, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$2;->val$corpora:Ljava/util/Set;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->setRegisteredCorpora(Ljava/util/Collection;)Z

    move-result v1

    .line 161
    .local v1, "success":Z
    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$2;->val$appInfo:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    if-eqz v3, :cond_0

    .line 162
    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$2;->this$0:Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->mSearchClient:Lcom/google/android/gms/appdatasearch/AppDataSearchClient;

    iget-object v4, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$2;->val$appInfo:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/appdatasearch/AppDataSearchClient;->registerGlobalSearchApplication(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)Z

    move-result v3

    and-int/2addr v1, v3

    .line 164
    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$2;->val$tables:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    .line 165
    .local v2, "table":Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;
    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$2;->this$0:Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->requestIndexingIfRequired(Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;)Z

    goto :goto_0

    .line 167
    .end local v2    # "table":Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;
    :cond_1
    if-eqz v1, :cond_2

    const/4 v3, 0x0

    :goto_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    return-object v3

    :cond_2
    const/16 v3, 0x8

    goto :goto_1
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$2;->call()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
