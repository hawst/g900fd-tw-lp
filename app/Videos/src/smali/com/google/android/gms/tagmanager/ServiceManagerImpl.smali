.class Lcom/google/android/gms/tagmanager/ServiceManagerImpl;
.super Lcom/google/android/gms/tagmanager/cu;


# static fields
.field private static aPX:Lcom/google/android/gms/tagmanager/ServiceManagerImpl;

.field private static final yS:Ljava/lang/Object;


# instance fields
.field private aPO:Lcom/google/android/gms/tagmanager/as;

.field private volatile aPP:Lcom/google/android/gms/tagmanager/aq;

.field private aPQ:I

.field private aPR:Z

.field private aPS:Z

.field private aPT:Z

.field private aPU:Lcom/google/android/gms/tagmanager/at;

.field private aPW:Z

.field private connected:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->yS:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/gms/tagmanager/cu;-><init>()V

    const v0, 0x1b7740

    iput v0, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aPQ:I

    iput-boolean v1, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aPR:Z

    iput-boolean v2, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aPS:Z

    iput-boolean v1, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->connected:Z

    iput-boolean v1, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aPT:Z

    new-instance v0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/tagmanager/ServiceManagerImpl$1;-><init>(Lcom/google/android/gms/tagmanager/ServiceManagerImpl;)V

    iput-object v0, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aPU:Lcom/google/android/gms/tagmanager/at;

    iput-boolean v2, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aPW:Z

    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/tagmanager/ServiceManagerImpl;)Lcom/google/android/gms/tagmanager/as;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aPO:Lcom/google/android/gms/tagmanager/as;

    return-object v0
.end method

.method public static getInstance()Lcom/google/android/gms/tagmanager/ServiceManagerImpl;
    .locals 1

    sget-object v0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aPX:Lcom/google/android/gms/tagmanager/ServiceManagerImpl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;

    invoke-direct {v0}, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;-><init>()V

    sput-object v0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aPX:Lcom/google/android/gms/tagmanager/ServiceManagerImpl;

    :cond_0
    sget-object v0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aPX:Lcom/google/android/gms/tagmanager/ServiceManagerImpl;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized dispatch()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aPS:Z

    if-nez v0, :cond_0

    const-string v0, "Dispatch call queued. Dispatch will run once initialization is complete."

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/bg;->v(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aPR:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/tagmanager/ServiceManagerImpl;->aPP:Lcom/google/android/gms/tagmanager/aq;

    new-instance v1, Lcom/google/android/gms/tagmanager/ServiceManagerImpl$3;

    invoke-direct {v1, p0}, Lcom/google/android/gms/tagmanager/ServiceManagerImpl$3;-><init>(Lcom/google/android/gms/tagmanager/ServiceManagerImpl;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/tagmanager/aq;->b(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
