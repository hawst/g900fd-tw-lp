.class public final Lcom/google/android/gms/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final common_android_wear_notification_needs_update_text:I = 0x7f0b0003

.field public static final common_android_wear_update_text:I = 0x7f0b0010

.field public static final common_android_wear_update_title:I = 0x7f0b000e

.field public static final common_google_play_services_enable_button:I = 0x7f0b000c

.field public static final common_google_play_services_enable_text:I = 0x7f0b000b

.field public static final common_google_play_services_enable_title:I = 0x7f0b000a

.field public static final common_google_play_services_error_notification_requested_by_msg:I = 0x7f0b0005

.field public static final common_google_play_services_install_button:I = 0x7f0b0009

.field public static final common_google_play_services_install_text_phone:I = 0x7f0b0007

.field public static final common_google_play_services_install_text_tablet:I = 0x7f0b0008

.field public static final common_google_play_services_install_title:I = 0x7f0b0006

.field public static final common_google_play_services_invalid_account_text:I = 0x7f0b0014

.field public static final common_google_play_services_invalid_account_title:I = 0x7f0b0013

.field public static final common_google_play_services_needs_enabling_title:I = 0x7f0b0004

.field public static final common_google_play_services_network_error_text:I = 0x7f0b0012

.field public static final common_google_play_services_network_error_title:I = 0x7f0b0011

.field public static final common_google_play_services_notification_needs_installation_title:I = 0x7f0b0001

.field public static final common_google_play_services_notification_needs_update_title:I = 0x7f0b0002

.field public static final common_google_play_services_notification_ticker:I = 0x7f0b0000

.field public static final common_google_play_services_unknown_issue:I = 0x7f0b0015

.field public static final common_google_play_services_unsupported_text:I = 0x7f0b0017

.field public static final common_google_play_services_unsupported_title:I = 0x7f0b0016

.field public static final common_google_play_services_update_button:I = 0x7f0b0018

.field public static final common_google_play_services_update_text:I = 0x7f0b000f

.field public static final common_google_play_services_update_title:I = 0x7f0b000d

.field public static final common_open_on_phone:I = 0x7f0b001b

.field public static final common_signin_button_text:I = 0x7f0b0019

.field public static final common_signin_button_text_long:I = 0x7f0b001a
