.class public final Lcom/google/android/gms/googlehelp/GoogleHelp;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/googlehelp/GoogleHelp;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private afS:Landroid/graphics/Bitmap;

.field arN:Ljava/lang/String;

.field arO:Landroid/accounts/Account;

.field arP:Z

.field arQ:Z

.field arR:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field arS:Landroid/os/Bundle;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field arT:Landroid/graphics/Bitmap;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field arU:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field arV:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field arW:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field arX:Ljava/lang/String;

.field arY:Landroid/net/Uri;

.field arZ:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/googlehelp/internal/common/OverflowMenuItem;",
            ">;"
        }
    .end annotation
.end field

.field asa:I

.field asb:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/googlehelp/OfflineSuggestion;",
            ">;"
        }
    .end annotation
.end field

.field asc:Z

.field asd:Lcom/google/android/gms/feedback/ErrorReport;

.field mPsdBundle:Landroid/os/Bundle;

.field final mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/googlehelp/a;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/googlehelp/GoogleHelp;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;ZZLjava/util/List;Landroid/os/Bundle;Landroid/graphics/Bitmap;[BIILjava/lang/String;Landroid/net/Uri;Ljava/util/List;ILjava/util/List;ZLcom/google/android/gms/feedback/ErrorReport;)V
    .locals 2
    .param p1, "versionCode"    # I
    .param p2, "helpCenterContext"    # Ljava/lang/String;
    .param p3, "googleAccount"    # Landroid/accounts/Account;
    .param p4, "psdBundle"    # Landroid/os/Bundle;
    .param p5, "searchEnabled"    # Z
    .param p6, "metricsReportingEnabled"    # Z
    .param p8, "feedbackPsdBundle"    # Landroid/os/Bundle;
    .param p9, "backupScreenshot"    # Landroid/graphics/Bitmap;
    .param p10, "screenshotBytes"    # [B
    .param p11, "screenshotWidth"    # I
    .param p12, "screenshotHeight"    # I
    .param p13, "apiDebugOption"    # Ljava/lang/String;
    .param p14, "fallbackSupportUri"    # Landroid/net/Uri;
    .param p16, "helpActivityTheme"    # I
    .param p18, "showContactCardFirst"    # Z
    .param p19, "feedbackErrorReport"    # Lcom/google/android/gms/feedback/ErrorReport;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Landroid/accounts/Account;",
            "Landroid/os/Bundle;",
            "ZZ",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/os/Bundle;",
            "Landroid/graphics/Bitmap;",
            "[BII",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/googlehelp/internal/common/OverflowMenuItem;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/googlehelp/OfflineSuggestion;",
            ">;Z",
            "Lcom/google/android/gms/feedback/ErrorReport;",
            ")V"
        }
    .end annotation

    .prologue
    .local p7, "supportPhoneNumbers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p15, "overflowMenuItems":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/googlehelp/internal/common/OverflowMenuItem;>;"
    .local p17, "offlineSuggestions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/googlehelp/OfflineSuggestion;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v1}, Lcom/google/android/gms/feedback/ErrorReport;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->asd:Lcom/google/android/gms/feedback/ErrorReport;

    iput p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->mVersionCode:I

    iput-object p2, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->arN:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->arO:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->mPsdBundle:Landroid/os/Bundle;

    iput-boolean p5, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->arP:Z

    iput-boolean p6, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->arQ:Z

    iput-object p7, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->arR:Ljava/util/List;

    iput-object p8, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->arS:Landroid/os/Bundle;

    iput-object p9, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->arT:Landroid/graphics/Bitmap;

    iput-object p10, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->arU:[B

    iput p11, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->arV:I

    iput p12, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->arW:I

    iput-object p13, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->arX:Ljava/lang/String;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->arY:Landroid/net/Uri;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->arZ:Ljava/util/List;

    move/from16 v0, p16

    iput v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->asa:I

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->asb:Ljava/util/List;

    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->asc:Z

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->asd:Lcom/google/android/gms/feedback/ErrorReport;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 20
    .param p1, "helpcenterContext"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    const/16 v16, 0x0

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    const/16 v18, 0x0

    new-instance v19, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct/range {v19 .. v19}, Lcom/google/android/gms/feedback/ErrorReport;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    invoke-direct/range {v0 .. v19}, Lcom/google/android/gms/googlehelp/GoogleHelp;-><init>(ILjava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;ZZLjava/util/List;Landroid/os/Bundle;Landroid/graphics/Bitmap;[BIILjava/lang/String;Landroid/net/Uri;Ljava/util/List;ILjava/util/List;ZLcom/google/android/gms/feedback/ErrorReport;)V

    return-void
.end method

.method public static getScreenshot(Landroid/app/Activity;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public buildHelpIntent()Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.googlehelp.HELP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_GOOGLE_HELP"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public buildHelpIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->buildHelpIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getFallbackSupportUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->arY:Landroid/net/Uri;

    return-object v0
.end method

.method public setFallbackSupportUri(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 0
    .param p1, "fallbackSupportUri"    # Landroid/net/Uri;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->arY:Landroid/net/Uri;

    return-object p0
.end method

.method public setGoogleAccount(Landroid/accounts/Account;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 0
    .param p1, "googleAccount"    # Landroid/accounts/Account;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->arO:Landroid/accounts/Account;

    return-object p0
.end method

.method public setScreenshot(Landroid/graphics/Bitmap;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 0
    .param p1, "screenshot"    # Landroid/graphics/Bitmap;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->afS:Landroid/graphics/Bitmap;

    return-object p0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->afS:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->asd:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->afS:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/feedback/ErrorReport;->setScreenshot(Landroid/graphics/Bitmap;)V

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/googlehelp/a;->a(Lcom/google/android/gms/googlehelp/GoogleHelp;Landroid/os/Parcel;I)V

    return-void
.end method
