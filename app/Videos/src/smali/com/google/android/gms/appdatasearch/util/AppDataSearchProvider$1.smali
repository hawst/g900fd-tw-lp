.class Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$1;
.super Landroid/os/AsyncTask;
.source "AppDataSearchProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->registerCorporaAsync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;


# direct methods
.method constructor <init>(Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$1;->this$0:Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$1;->this$0:Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$1;->this$0:Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->getDataManager()Lcom/google/android/gms/appdatasearch/util/AppDataSearchDataManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchDataManager;->getTableMappings()[Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->registerCorpora([Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 115
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 123
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$1;->this$0:Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider;->onError(I)V

    .line 126
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 115
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/gms/appdatasearch/util/AppDataSearchProvider$1;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
