.class Lcom/google/android/gms/internal/nr$1;
.super Lcom/google/android/gms/internal/nr$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/internal/nr;->a(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/PendingResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic ash:Ljava/lang/Integer;

.field final synthetic asi:Ljava/lang/Long;

.field final synthetic asj:Ljava/lang/Integer;

.field final synthetic ask:Ljava/lang/Integer;

.field final synthetic asl:Landroid/os/Bundle;

.field final synthetic asm:Lcom/google/android/gms/internal/nr;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/nr;Lcom/google/android/gms/common/api/Api$c;Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/nr$1;->asm:Lcom/google/android/gms/internal/nr;

    iput-object p4, p0, Lcom/google/android/gms/internal/nr$1;->ash:Ljava/lang/Integer;

    iput-object p5, p0, Lcom/google/android/gms/internal/nr$1;->asi:Ljava/lang/Long;

    iput-object p6, p0, Lcom/google/android/gms/internal/nr$1;->asj:Ljava/lang/Integer;

    iput-object p7, p0, Lcom/google/android/gms/internal/nr$1;->ask:Ljava/lang/Integer;

    iput-object p8, p0, Lcom/google/android/gms/internal/nr$1;->asl:Landroid/os/Bundle;

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/internal/nr$a;-><init>(Lcom/google/android/gms/common/api/Api$c;Lcom/google/android/gms/common/api/GoogleApiClient;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Lcom/google/android/gms/common/api/Api$a;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    check-cast p1, Lcom/google/android/gms/internal/nq;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/nr$1;->a(Lcom/google/android/gms/internal/nq;)V

    return-void
.end method

.method protected a(Lcom/google/android/gms/internal/nq;)V
    .locals 4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/internal/nr$1;->ash:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, "latency_micros"

    iget-object v2, p0, Lcom/google/android/gms/internal/nr$1;->ash:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/internal/nr$1;->asi:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, "latency_bps"

    iget-object v2, p0, Lcom/google/android/gms/internal/nr$1;->asi:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/internal/nr$1;->asj:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, "latitude_e6"

    iget-object v2, p0, Lcom/google/android/gms/internal/nr$1;->asj:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/internal/nr$1;->ask:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, "longitude_e6"

    iget-object v2, p0, Lcom/google/android/gms/internal/nr$1;->ask:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_3
    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/internal/nr$1;->asl:Landroid/os/Bundle;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/internal/nr$1;->asl:Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/internal/nr$1;->asl:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/nq;->b(Landroid/os/Bundle;Landroid/os/Bundle;)V

    :cond_5
    sget-object v0, Lcom/google/android/gms/common/api/Status;->Th:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/nr$1;->b(Lcom/google/android/gms/common/api/Result;)V

    return-void
.end method
