.class public final Lcom/google/android/gms/mdm/NetworkQualityUploader;
.super Ljava/lang/Object;


# static fields
.field private static aAT:Z

.field private static final aAU:Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/mdm/NetworkQualityUploader;->aAT:Z

    new-instance v0, Lcom/google/android/gms/mdm/NetworkQualityUploader$1;

    invoke-direct {v0}, Lcom/google/android/gms/mdm/NetworkQualityUploader$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/mdm/NetworkQualityUploader;->aAU:Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;

    return-void
.end method

.method public static logNetworkStats(Landroid/content/Context;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/os/Bundle;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "latencyMicros"    # Ljava/lang/Integer;
    .param p2, "throughputBps"    # Ljava/lang/Long;
    .param p3, "latitudeE6"    # Ljava/lang/Integer;
    .param p4, "longitudeE6"    # Ljava/lang/Integer;
    .param p5, "customNetStats"    # Landroid/os/Bundle;

    .prologue
    sget-boolean v0, Lcom/google/android/gms/mdm/NetworkQualityUploader;->aAT:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/mdm/NetworkQualityUploader;->aAU:Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addOnConnectionFailedListener(Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/internal/no;->API:Lcom/google/android/gms/common/api/Api;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V

    sget-object v0, Lcom/google/android/gms/internal/no;->asf:Lcom/google/android/gms/internal/np;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/internal/np;->a(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/mdm/NetworkQualityUploader$2;

    invoke-direct {v2, v1}, Lcom/google/android/gms/mdm/NetworkQualityUploader$2;-><init>(Lcom/google/android/gms/common/api/GoogleApiClient;)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    goto :goto_0
.end method

.method static synthetic y(Z)Z
    .locals 0

    sput-boolean p0, Lcom/google/android/gms/mdm/NetworkQualityUploader;->aAT:Z

    return p0
.end method
