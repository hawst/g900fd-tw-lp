.class public Lcom/google/android/gms/plus/internal/i;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/internal/k;


# instance fields
.field private final Fl:Ljava/lang/String;

.field private final aHX:[Ljava/lang/String;

.field private final aHY:[Ljava/lang/String;

.field private final aHZ:[Ljava/lang/String;

.field private final aIa:Ljava/lang/String;

.field private final aIb:Ljava/lang/String;

.field private final aIc:Ljava/lang/String;

.field private final aId:Ljava/lang/String;

.field private final aIe:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

.field private final mVersionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/plus/internal/k;

    invoke-direct {v0}, Lcom/google/android/gms/plus/internal/k;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/internal/i;->CREATOR:Lcom/google/android/gms/plus/internal/k;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/plus/internal/i;->mVersionCode:I

    iput-object p2, p0, Lcom/google/android/gms/plus/internal/i;->Fl:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/plus/internal/i;->aHX:[Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/plus/internal/i;->aHY:[Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/plus/internal/i;->aHZ:[Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/plus/internal/i;->aIa:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/plus/internal/i;->aIb:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/gms/plus/internal/i;->aIc:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/gms/plus/internal/i;->aId:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/gms/plus/internal/i;->aIe:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/android/gms/plus/internal/i;

    if-nez v1, :cond_1

    .end local p1    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/internal/i;

    .end local p1    # "obj":Ljava/lang/Object;
    iget v1, p0, Lcom/google/android/gms/plus/internal/i;->mVersionCode:I

    iget v2, p1, Lcom/google/android/gms/plus/internal/i;->mVersionCode:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/i;->Fl:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/i;->Fl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/kl;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/i;->aHX:[Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/i;->aHX:[Ljava/lang/String;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/i;->aHY:[Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/i;->aHY:[Ljava/lang/String;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/i;->aHZ:[Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/i;->aHZ:[Ljava/lang/String;

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/i;->aIa:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/i;->aIa:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/kl;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/i;->aIb:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/i;->aIb:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/kl;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/i;->aIc:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/i;->aIc:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/kl;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/i;->aId:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/i;->aId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/kl;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/i;->aIe:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/i;->aIe:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/kl;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/i;->Fl:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/plus/internal/i;->mVersionCode:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/plus/internal/i;->mVersionCode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/i;->Fl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/i;->aHX:[Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/i;->aHY:[Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/i;->aHZ:[Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/i;->aIa:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/i;->aIb:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/i;->aIc:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/i;->aId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/i;->aIe:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/internal/kl;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public rA()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/i;->aHY:[Ljava/lang/String;

    return-object v0
.end method

.method public rB()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/i;->aHZ:[Ljava/lang/String;

    return-object v0
.end method

.method public rC()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/i;->aIa:Ljava/lang/String;

    return-object v0
.end method

.method public rD()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/i;->aIb:Ljava/lang/String;

    return-object v0
.end method

.method public rE()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/i;->aIc:Ljava/lang/String;

    return-object v0
.end method

.method public rF()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/i;->aId:Ljava/lang/String;

    return-object v0
.end method

.method public rG()Lcom/google/android/gms/plus/internal/PlusCommonExtras;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/i;->aIe:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    return-object v0
.end method

.method public rz()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/i;->aHX:[Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/google/android/gms/internal/kl;->j(Ljava/lang/Object;)Lcom/google/android/gms/internal/kl$a;

    move-result-object v0

    const-string v1, "versionCode"

    iget v2, p0, Lcom/google/android/gms/plus/internal/i;->mVersionCode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/kl$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/kl$a;

    move-result-object v0

    const-string v1, "accountName"

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/i;->Fl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/kl$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/kl$a;

    move-result-object v0

    const-string v1, "requestedScopes"

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/i;->aHX:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/kl$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/kl$a;

    move-result-object v0

    const-string v1, "visibleActivities"

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/i;->aHY:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/kl$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/kl$a;

    move-result-object v0

    const-string v1, "requiredFeatures"

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/i;->aHZ:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/kl$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/kl$a;

    move-result-object v0

    const-string v1, "packageNameForAuth"

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/i;->aIa:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/kl$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/kl$a;

    move-result-object v0

    const-string v1, "callingPackageName"

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/i;->aIb:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/kl$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/kl$a;

    move-result-object v0

    const-string v1, "applicationName"

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/i;->aIc:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/kl$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/kl$a;

    move-result-object v0

    const-string v1, "extra"

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/i;->aIe:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/kl$a;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/internal/kl$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/kl$a;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/internal/k;->a(Lcom/google/android/gms/plus/internal/i;Landroid/os/Parcel;I)V

    return-void
.end method
