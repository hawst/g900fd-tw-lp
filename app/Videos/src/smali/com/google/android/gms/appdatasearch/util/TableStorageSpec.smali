.class public final Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;
.super Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;
.source "TableStorageSpec.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;
    }
.end annotation


# instance fields
.field private final mAllowShortcuts:Z

.field private final mGlobalSearchTemplates:[I

.field private final mSections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mTrimmable:Z

.field private final mVersion:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/Map;Ljava/lang/String;[IZZZ)V
    .locals 10
    .param p1, "tableName"    # Ljava/lang/String;
    .param p2, "uriCol"    # Ljava/lang/String;
    .param p3, "score"    # Ljava/lang/String;
    .param p4, "createdTimestampMsSpec"    # Ljava/lang/String;
    .param p5, "version"    # Ljava/lang/String;
    .param p8, "condition"    # Ljava/lang/String;
    .param p9, "globalSearchSectionSpecs"    # [I
    .param p10, "trimmable"    # Z
    .param p11, "allowShortcuts"    # Z
    .param p12, "isUriColumnUnique"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "[IZZZ)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p6, "sections":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;>;"
    .local p7, "sectionNamesToColumnNames":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {p1, p2}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->getCorpusName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p12

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/appdatasearch/util/CorpusTableMapping;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Z)V

    .line 46
    if-nez p2, :cond_0

    .line 47
    new-instance v1, Ljava/lang/NullPointerException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "A URI column must be specified for table "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 49
    :cond_0
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 50
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "At least one section must be specified for table "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 53
    :cond_1
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0x10

    if-le v1, v2, :cond_2

    .line 54
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Too many sections for table "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; max is 16"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 57
    :cond_2
    if-nez p5, :cond_3

    const-string p5, "1"

    .end local p5    # "version":Ljava/lang/String;
    :cond_3
    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->mVersion:Ljava/lang/String;

    .line 58
    new-instance v1, Ljava/util/ArrayList;

    move-object/from16 v0, p6

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->mSections:Ljava/util/List;

    .line 59
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->mGlobalSearchTemplates:[I

    .line 60
    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->mTrimmable:Z

    .line 61
    move/from16 v0, p11

    iput-boolean v0, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->mAllowShortcuts:Z

    .line 62
    return-void
.end method

.method public static builder(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;
    .locals 1
    .param p0, "tableName"    # Ljava/lang/String;

    .prologue
    .line 97
    new-instance v0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec$Builder;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static getCorpusName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "tableName"    # Ljava/lang/String;
    .param p1, "uriColumn"    # Ljava/lang/String;

    .prologue
    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 112
    instance-of v1, p1, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    if-nez v1, :cond_0

    .line 113
    const/4 v1, 0x0

    .line 116
    :goto_0
    return v1

    :cond_0
    move-object v0, p1

    .line 115
    check-cast v0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;

    .line 116
    .local v0, "otherTableSpec":Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->mTableName:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->mTableName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getAllowShortcuts()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->mAllowShortcuts:Z

    return v0
.end method

.method public getGlobalSearchSectionTemplates()[I
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->mGlobalSearchTemplates:[I

    return-object v0
.end method

.method public getSections()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->mSections:Ljava/util/List;

    return-object v0
.end method

.method public getTrimmable()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->mTrimmable:Z

    return v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->mVersion:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->mTableName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TableStorageSpec["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/util/TableStorageSpec;->mTableName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
