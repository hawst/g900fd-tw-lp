.class public abstract Lcom/google/android/gms/internal/ye;
.super Ljava/lang/Object;


# instance fields
.field protected volatile aYu:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/ye;->aYu:I

    return-void
.end method


# virtual methods
.method protected c()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCachedSize()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/ye;->aYu:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/ye;->getSerializedSize()I

    :cond_0
    iget v0, p0, Lcom/google/android/gms/internal/ye;->aYu:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/ye;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/ye;->aYu:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/google/android/gms/internal/yf;->f(Lcom/google/android/gms/internal/ye;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/android/gms/internal/xx;)V
    .locals 0
    .param p1, "output"    # Lcom/google/android/gms/internal/xx;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    return-void
.end method
