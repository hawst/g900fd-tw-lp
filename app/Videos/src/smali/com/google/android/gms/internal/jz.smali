.class public Lcom/google/android/gms/internal/jz;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/gms/internal/jz;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final VH:I

.field VI:I

.field VJ:Ljava/lang/String;

.field VK:Landroid/os/IBinder;

.field VL:[Lcom/google/android/gms/common/api/Scope;

.field VM:Landroid/os/Bundle;

.field final version:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/ka;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ka;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/jz;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/jz;->version:I

    const v0, 0x648278

    iput v0, p0, Lcom/google/android/gms/internal/jz;->VI:I

    iput p1, p0, Lcom/google/android/gms/internal/jz;->VH:I

    return-void
.end method

.method constructor <init>(IIILjava/lang/String;Landroid/os/IBinder;[Lcom/google/android/gms/common/api/Scope;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/jz;->version:I

    iput p2, p0, Lcom/google/android/gms/internal/jz;->VH:I

    iput p3, p0, Lcom/google/android/gms/internal/jz;->VI:I

    iput-object p4, p0, Lcom/google/android/gms/internal/jz;->VJ:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/internal/jz;->VK:Landroid/os/IBinder;

    iput-object p6, p0, Lcom/google/android/gms/internal/jz;->VL:[Lcom/google/android/gms/common/api/Scope;

    iput-object p7, p0, Lcom/google/android/gms/internal/jz;->VM:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public bg(Ljava/lang/String;)Lcom/google/android/gms/internal/jz;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/jz;->VJ:Ljava/lang/String;

    return-object p0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/ka;->a(Lcom/google/android/gms/internal/jz;Landroid/os/Parcel;I)V

    return-void
.end method
