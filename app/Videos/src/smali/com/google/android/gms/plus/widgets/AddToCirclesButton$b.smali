.class public Lcom/google/android/gms/plus/widgets/AddToCirclesButton$b;
.super Lcom/google/android/gms/internal/ti$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/widgets/AddToCirclesButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field private final aKh:Lcom/google/android/gms/plus/widgets/AddToCirclesButton$c;

.field final synthetic aKi:Lcom/google/android/gms/plus/widgets/AddToCirclesButton;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/widgets/AddToCirclesButton;Lcom/google/android/gms/plus/widgets/AddToCirclesButton$c;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton$b;->aKi:Lcom/google/android/gms/plus/widgets/AddToCirclesButton;

    invoke-direct {p0}, Lcom/google/android/gms/internal/ti$a;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton$b;->aKh:Lcom/google/android/gms/plus/widgets/AddToCirclesButton$c;

    return-void
.end method

.method private s(Landroid/content/Intent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton$b;->aKi:Lcom/google/android/gms/plus/widgets/AddToCirclesButton;

    invoke-static {v0}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a(Lcom/google/android/gms/plus/widgets/AddToCirclesButton;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton$b;->aKi:Lcom/google/android/gms/plus/widgets/AddToCirclesButton;

    invoke-static {v0}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->a(Lcom/google/android/gms/plus/widgets/AddToCirclesButton;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton$b;->aKi:Lcom/google/android/gms/plus/widgets/AddToCirclesButton;

    invoke-static {v1}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton;->b(Lcom/google/android/gms/plus/widgets/AddToCirclesButton;)I

    move-result v1

    invoke-virtual {v0, p1, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public r(Landroid/content/Intent;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton$b;->aKh:Lcom/google/android/gms/plus/widgets/AddToCirclesButton$c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton$b;->aKh:Lcom/google/android/gms/plus/widgets/AddToCirclesButton$c;

    invoke-interface {v0, p1}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton$c;->r(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton$b;->s(Landroid/content/Intent;)V

    goto :goto_0
.end method
