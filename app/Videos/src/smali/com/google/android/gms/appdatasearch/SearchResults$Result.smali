.class public Lcom/google/android/gms/appdatasearch/SearchResults$Result;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/appdatasearch/SearchResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Result"
.end annotation


# instance fields
.field private final Es:Lcom/google/android/gms/appdatasearch/SearchResults$ResultIterator;

.field private final Et:I

.field final synthetic Eu:Lcom/google/android/gms/appdatasearch/SearchResults;


# direct methods
.method constructor <init>(Lcom/google/android/gms/appdatasearch/SearchResults;ILcom/google/android/gms/appdatasearch/SearchResults$ResultIterator;)V
    .locals 0
    .param p2, "idx"    # I
    .param p3, "it"    # Lcom/google/android/gms/appdatasearch/SearchResults$ResultIterator;

    .prologue
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->Eu:Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->Es:Lcom/google/android/gms/appdatasearch/SearchResults$ResultIterator;

    iput p2, p0, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->Et:I

    return-void
.end method

.method private ar(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults$a;
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->Es:Lcom/google/android/gms/appdatasearch/SearchResults$ResultIterator;

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/SearchResults$ResultIterator;->b(Lcom/google/android/gms/appdatasearch/SearchResults$ResultIterator;)[Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->Eu:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v1, v1, Lcom/google/android/gms/appdatasearch/SearchResults;->El:[I

    iget v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->Et:I

    aget v1, v1, v2

    aget-object v0, v0, v1

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->Es:Lcom/google/android/gms/appdatasearch/SearchResults$ResultIterator;

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/SearchResults$ResultIterator;->b(Lcom/google/android/gms/appdatasearch/SearchResults$ResultIterator;)[Ljava/util/Map;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->Eu:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/SearchResults;->El:[I

    iget v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->Et:I

    aget v2, v0, v2

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    aput-object v0, v1, v2

    move-object v1, v0

    :goto_0
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/SearchResults$a;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->Eu:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/SearchResults;->Ei:[Landroid/os/Bundle;

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->Eu:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v2, v2, Lcom/google/android/gms/appdatasearch/SearchResults;->El:[I

    iget v3, p0, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->Et:I

    aget v2, v2, v3

    aget-object v0, v0, v2

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->Eu:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/SearchResults;->Ej:[Landroid/os/Bundle;

    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->Eu:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/SearchResults;->El:[I

    iget v4, p0, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->Et:I

    aget v3, v3, v4

    aget-object v0, v0, v3

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    if-eqz v2, :cond_0

    if-nez v3, :cond_2

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_1
    return-object v0

    :cond_2
    new-instance v0, Lcom/google/android/gms/appdatasearch/SearchResults$a;

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/appdatasearch/SearchResults$a;-><init>([I[B)V

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method


# virtual methods
.method public getSection(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "sectionName"    # Ljava/lang/String;

    .prologue
    invoke-direct {p0, p1}, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->ar(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults$a;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget v1, p0, Lcom/google/android/gms/appdatasearch/SearchResults$Result;->Et:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/appdatasearch/SearchResults$a;->ai(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
