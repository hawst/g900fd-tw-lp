.class Lcom/google/android/gms/internal/is$a;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/is;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic Qw:Lcom/google/android/gms/internal/is;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/internal/is;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/is$a;->Qw:Lcom/google/android/gms/internal/is;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/internal/is;Lcom/google/android/gms/internal/is$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/is$a;-><init>(Lcom/google/android/gms/internal/is;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/is$a;->Qw:Lcom/google/android/gms/internal/is;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/is;->a(Lcom/google/android/gms/internal/is;Z)Z

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/gms/internal/is$a;->Qw:Lcom/google/android/gms/internal/is;

    invoke-static {v0}, Lcom/google/android/gms/internal/is;->a(Lcom/google/android/gms/internal/is;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/iv;

    const/16 v5, 0x836

    invoke-virtual {v0, v2, v3, v5}, Lcom/google/android/gms/internal/iv;->e(JI)Z

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/google/android/gms/internal/iv;->QB:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/is$a;->Qw:Lcom/google/android/gms/internal/is;

    invoke-static {v0}, Lcom/google/android/gms/internal/is;->a(Lcom/google/android/gms/internal/is;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/iv;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/iv;->hM()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/internal/is$a;->Qw:Lcom/google/android/gms/internal/is;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/is;->b(Lcom/google/android/gms/internal/is;Z)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move v0, v1

    goto :goto_2
.end method
