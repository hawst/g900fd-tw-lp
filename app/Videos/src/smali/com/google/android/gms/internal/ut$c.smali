.class public Lcom/google/android/gms/internal/ut$c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/Result;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/ut;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/uw;


# instance fields
.field public aLE:[Lcom/google/android/gms/internal/ut$a;

.field public aLF:J

.field public aLG:J

.field public aLH:J

.field final mVersionCode:I

.field public status:Lcom/google/android/gms/common/api/Status;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/uw;

    invoke-direct {v0}, Lcom/google/android/gms/internal/uw;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ut$c;->CREATOR:Lcom/google/android/gms/internal/uw;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/ut$c;->mVersionCode:I

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/api/Status;[Lcom/google/android/gms/internal/ut$a;JJJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/ut$c;->mVersionCode:I

    iput-object p2, p0, Lcom/google/android/gms/internal/ut$c;->status:Lcom/google/android/gms/common/api/Status;

    iput-object p3, p0, Lcom/google/android/gms/internal/ut$c;->aLE:[Lcom/google/android/gms/internal/ut$a;

    iput-wide p4, p0, Lcom/google/android/gms/internal/ut$c;->aLF:J

    iput-wide p6, p0, Lcom/google/android/gms/internal/ut$c;->aLG:J

    iput-wide p8, p0, Lcom/google/android/gms/internal/ut$c;->aLH:J

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/ut$c;->CREATOR:Lcom/google/android/gms/internal/uw;

    const/4 v0, 0x0

    return v0
.end method

.method public getStatus()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ut$c;->status:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    sget-object v0, Lcom/google/android/gms/internal/ut$c;->CREATOR:Lcom/google/android/gms/internal/uw;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/uw;->a(Lcom/google/android/gms/internal/ut$c;Landroid/os/Parcel;I)V

    return-void
.end method
