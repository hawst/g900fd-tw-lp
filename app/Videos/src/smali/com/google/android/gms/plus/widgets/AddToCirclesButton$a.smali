.class Lcom/google/android/gms/plus/widgets/AddToCirclesButton$a;
.super Lcom/google/android/gms/internal/th$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/plus/widgets/AddToCirclesButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private TL:Landroid/widget/TextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/th$a;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/widgets/AddToCirclesButton$1;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/plus/widgets/AddToCirclesButton$a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/dynamic/d;Lcom/google/android/gms/dynamic/d;Lcom/google/android/gms/dynamic/d;)V
    .locals 2

    new-instance v1, Landroid/widget/TextView;

    invoke-static {p1}, Lcom/google/android/gms/dynamic/e;->i(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton$a;->TL:Landroid/widget/TextView;

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/people/data/AudienceMember;Ljava/lang/String;Lcom/google/android/gms/internal/ti;)V
    .locals 0

    return-void
.end method

.method public getView()Lcom/google/android/gms/dynamic/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/widgets/AddToCirclesButton$a;->TL:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->q(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/d;

    move-result-object v0

    return-object v0
.end method

.method public refreshButton()V
    .locals 0

    return-void
.end method

.method public setAnalyticsStartView(Ljava/lang/String;I)V
    .locals 0
    .param p1, "startViewNamespace"    # Ljava/lang/String;
    .param p2, "startViewTypeNum"    # I

    .prologue
    return-void
.end method

.method public setShowProgressIndicator(Z)V
    .locals 0
    .param p1, "showProgress"    # Z

    .prologue
    return-void
.end method

.method public setSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    return-void
.end method
