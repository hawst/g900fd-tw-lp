.class public final Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
.super Lcom/google/protobuf/nano/MessageNano;
.source "UserFeedbackCommonProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/userfeedback/UserFeedbackCommonProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProductSpecificBinaryData"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;


# instance fields
.field public data:[B

.field public mimeType:Ljava/lang/String;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 579
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 580
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->clear()Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    .line 581
    return-void
.end method

.method public static emptyArray()[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    .locals 2

    .prologue
    .line 559
    sget-object v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->_emptyArray:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    if-nez v0, :cond_1

    .line 560
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 562
    :try_start_0
    sget-object v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->_emptyArray:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    if-nez v0, :cond_0

    .line 563
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    sput-object v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->_emptyArray:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    .line 565
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 567
    :cond_1
    sget-object v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->_emptyArray:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    return-object v0

    .line 565
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    .locals 1

    .prologue
    .line 584
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->name:Ljava/lang/String;

    .line 585
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->mimeType:Ljava/lang/String;

    .line 586
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->data:[B

    .line 587
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->cachedSize:I

    .line 588
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 646
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 647
    .local v0, "size":I
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 649
    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->mimeType:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 650
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->mimeType:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 653
    :cond_0
    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->data:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    .line 654
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->data:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 657
    :cond_1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 593
    if-ne p1, p0, :cond_1

    .line 617
    :cond_0
    :goto_0
    return v1

    .line 596
    :cond_1
    instance-of v3, p1, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    if-nez v3, :cond_2

    move v1, v2

    .line 597
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 599
    check-cast v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    .line 600
    .local v0, "other":Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->name:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 601
    iget-object v3, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->name:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 602
    goto :goto_0

    .line 604
    :cond_3
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 605
    goto :goto_0

    .line 607
    :cond_4
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->mimeType:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 608
    iget-object v3, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->mimeType:Ljava/lang/String;

    if-eqz v3, :cond_6

    move v1, v2

    .line 609
    goto :goto_0

    .line 611
    :cond_5
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->mimeType:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->mimeType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 612
    goto :goto_0

    .line 614
    :cond_6
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->data:[B

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->data:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 615
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 622
    const/16 v0, 0x11

    .line 623
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->name:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 625
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->mimeType:Ljava/lang/String;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 627
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->data:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int v0, v1, v2

    .line 628
    return v0

    .line 623
    :cond_0
    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 625
    :cond_1
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->mimeType:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 553
    invoke-virtual {p0, p1}, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 665
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 666
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 670
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 671
    :sswitch_0
    return-object p0

    .line 676
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->name:Ljava/lang/String;

    goto :goto_0

    .line 680
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->mimeType:Ljava/lang/String;

    goto :goto_0

    .line 684
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->data:[B

    goto :goto_0

    .line 666
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 634
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 635
    iget-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->mimeType:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 636
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->mimeType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 638
    :cond_0
    iget-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->data:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 639
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->data:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 641
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 642
    return-void
.end method
