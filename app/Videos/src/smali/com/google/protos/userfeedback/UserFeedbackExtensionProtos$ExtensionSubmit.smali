.class public final Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;
.super Lcom/google/protobuf/nano/MessageNano;
.source "UserFeedbackExtensionProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ExtensionSubmit"
.end annotation


# instance fields
.field public bucket:Ljava/lang/String;

.field public commonData:Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

.field public productId:I

.field public productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

.field public typeId:I

.field public webData:Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 44
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->clear()Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;

    .line 45
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 48
    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->commonData:Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    .line 49
    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->webData:Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;

    .line 50
    iput v1, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->typeId:I

    .line 51
    invoke-static {}, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->emptyArray()[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    .line 52
    iput v1, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productId:I

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->bucket:Ljava/lang/String;

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->cachedSize:I

    .line 55
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 152
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 153
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->commonData:Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    if-eqz v3, :cond_0

    .line 154
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->commonData:Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 157
    :cond_0
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->webData:Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;

    if-eqz v3, :cond_1

    .line 158
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->webData:Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 161
    :cond_1
    iget v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->typeId:I

    if-eqz v3, :cond_2

    .line 162
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->typeId:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 165
    :cond_2
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    array-length v3, v3

    if-lez v3, :cond_4

    .line 166
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    array-length v3, v3

    if-ge v1, v3, :cond_4

    .line 167
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    aget-object v0, v3, v1

    .line 168
    .local v0, "element":Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    if-eqz v0, :cond_3

    .line 169
    const/16 v3, 0xf

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 166
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 174
    .end local v0    # "element":Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    .end local v1    # "i":I
    :cond_4
    iget v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productId:I

    if-eqz v3, :cond_5

    .line 175
    const/16 v3, 0x11

    iget v4, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productId:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 178
    :cond_5
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->bucket:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 179
    const/16 v3, 0x12

    iget-object v4, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->bucket:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 182
    :cond_6
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60
    if-ne p1, p0, :cond_1

    .line 102
    :cond_0
    :goto_0
    return v1

    .line 63
    :cond_1
    instance-of v3, p1, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;

    if-nez v3, :cond_2

    move v1, v2

    .line 64
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 66
    check-cast v0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;

    .line 67
    .local v0, "other":Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->commonData:Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    if-nez v3, :cond_3

    .line 68
    iget-object v3, v0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->commonData:Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    if-eqz v3, :cond_4

    move v1, v2

    .line 69
    goto :goto_0

    .line 72
    :cond_3
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->commonData:Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->commonData:Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    invoke-virtual {v3, v4}, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 73
    goto :goto_0

    .line 76
    :cond_4
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->webData:Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;

    if-nez v3, :cond_5

    .line 77
    iget-object v3, v0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->webData:Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;

    if-eqz v3, :cond_6

    move v1, v2

    .line 78
    goto :goto_0

    .line 81
    :cond_5
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->webData:Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->webData:Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 82
    goto :goto_0

    .line 85
    :cond_6
    iget v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->typeId:I

    iget v4, v0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->typeId:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 86
    goto :goto_0

    .line 88
    :cond_7
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 90
    goto :goto_0

    .line 92
    :cond_8
    iget v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productId:I

    iget v4, v0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productId:I

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 93
    goto :goto_0

    .line 95
    :cond_9
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->bucket:Ljava/lang/String;

    if-nez v3, :cond_a

    .line 96
    iget-object v3, v0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->bucket:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 97
    goto :goto_0

    .line 99
    :cond_a
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->bucket:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->bucket:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 100
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 107
    const/16 v0, 0x11

    .line 108
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->commonData:Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 110
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->webData:Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 112
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->typeId:I

    add-int v0, v1, v3

    .line 113
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 115
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productId:I

    add-int v0, v1, v3

    .line 116
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->bucket:Ljava/lang/String;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 118
    return v0

    .line 108
    :cond_0
    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->commonData:Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    invoke-virtual {v1}, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->hashCode()I

    move-result v1

    goto :goto_0

    .line 110
    :cond_1
    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->webData:Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_1

    .line 116
    :cond_2
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->bucket:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 190
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 191
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 195
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 196
    :sswitch_0
    return-object p0

    .line 201
    :sswitch_1
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->commonData:Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    if-nez v5, :cond_1

    .line 202
    new-instance v5, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    invoke-direct {v5}, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;-><init>()V

    iput-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->commonData:Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    .line 204
    :cond_1
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->commonData:Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 208
    :sswitch_2
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->webData:Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;

    if-nez v5, :cond_2

    .line 209
    new-instance v5, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;

    invoke-direct {v5}, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;-><init>()V

    iput-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->webData:Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;

    .line 211
    :cond_2
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->webData:Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 215
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->typeId:I

    goto :goto_0

    .line 219
    :sswitch_4
    const/16 v5, 0x7a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 221
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    if-nez v5, :cond_4

    move v1, v4

    .line 222
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    .line 224
    .local v2, "newArray":[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    if-eqz v1, :cond_3

    .line 225
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 227
    :cond_3
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_5

    .line 228
    new-instance v5, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    invoke-direct {v5}, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;-><init>()V

    aput-object v5, v2, v1

    .line 229
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 230
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 227
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 221
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    :cond_4
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    array-length v1, v5

    goto :goto_1

    .line 233
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    :cond_5
    new-instance v5, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    invoke-direct {v5}, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;-><init>()V

    aput-object v5, v2, v1

    .line 234
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 235
    iput-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    goto :goto_0

    .line 239
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productId:I

    goto :goto_0

    .line 243
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->bucket:Ljava/lang/String;

    goto/16 :goto_0

    .line 191
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x7a -> :sswitch_4
        0x88 -> :sswitch_5
        0x92 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->commonData:Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    if-eqz v2, :cond_0

    .line 125
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->commonData:Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 127
    :cond_0
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->webData:Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;

    if-eqz v2, :cond_1

    .line 128
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->webData:Lcom/google/protos/userfeedback/UserFeedbackWebProtos$WebData;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 130
    :cond_1
    iget v2, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->typeId:I

    if-eqz v2, :cond_2

    .line 131
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->typeId:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 133
    :cond_2
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 134
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 135
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    aget-object v0, v2, v1

    .line 136
    .local v0, "element":Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    if-eqz v0, :cond_3

    .line 137
    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 134
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 141
    .end local v0    # "element":Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    .end local v1    # "i":I
    :cond_4
    iget v2, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productId:I

    if-eqz v2, :cond_5

    .line 142
    const/16 v2, 0x11

    iget v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->productId:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 144
    :cond_5
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->bucket:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 145
    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackExtensionProtos$ExtensionSubmit;->bucket:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 147
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 148
    return-void
.end method
