.class public final Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;
.super Lcom/google/protobuf/nano/MessageNano;
.source "UserFeedbackCommonProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/userfeedback/UserFeedbackCommonProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CommonData"
.end annotation


# instance fields
.field public additionalFormContent:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

.field public description:Ljava/lang/String;

.field public descriptionTranslated:Ljava/lang/String;

.field public gaiaId:J

.field public productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

.field public productSpecificBinaryDataName:[Ljava/lang/String;

.field public productSpecificContext:Ljava/lang/String;

.field public productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

.field public productVersion:Ljava/lang/String;

.field public reportType:I

.field public sourceDescriptionLanguage:Ljava/lang/String;

.field public spellingErrorCount:I

.field public uiLanguage:Ljava/lang/String;

.field public userEmail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 89
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->clear()Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    .line 90
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 93
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->gaiaId:J

    .line 94
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->description:Ljava/lang/String;

    .line 95
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->descriptionTranslated:Ljava/lang/String;

    .line 96
    iput v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->spellingErrorCount:I

    .line 97
    const-string v0, "en"

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->sourceDescriptionLanguage:Ljava/lang/String;

    .line 98
    const-string v0, "en_US"

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->uiLanguage:Ljava/lang/String;

    .line 99
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->userEmail:Ljava/lang/String;

    .line 100
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryDataName:[Ljava/lang/String;

    .line 101
    invoke-static {}, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;->emptyArray()[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    .line 102
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productVersion:Ljava/lang/String;

    .line 103
    invoke-static {}, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->emptyArray()[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    .line 104
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificContext:Ljava/lang/String;

    .line 105
    invoke-static {}, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->emptyArray()[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->additionalFormContent:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    .line 106
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->reportType:I

    .line 107
    iput v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->cachedSize:I

    .line 108
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 299
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 300
    .local v4, "size":I
    iget-wide v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->gaiaId:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 301
    iget-wide v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->gaiaId:J

    invoke-static {v10, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFixed64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 304
    :cond_0
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->description:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 305
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->description:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 308
    :cond_1
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->userEmail:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 309
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->userEmail:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 312
    :cond_2
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->descriptionTranslated:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 313
    const/4 v5, 0x4

    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->descriptionTranslated:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 316
    :cond_3
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->sourceDescriptionLanguage:Ljava/lang/String;

    const-string v6, "en"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 317
    const/4 v5, 0x5

    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->sourceDescriptionLanguage:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 320
    :cond_4
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->uiLanguage:Ljava/lang/String;

    const-string v6, "en_US"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 321
    const/4 v5, 0x6

    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->uiLanguage:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 324
    :cond_5
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryDataName:[Ljava/lang/String;

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryDataName:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_8

    .line 325
    const/4 v0, 0x0

    .line 326
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 327
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryDataName:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_7

    .line 328
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryDataName:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 329
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_6

    .line 330
    add-int/lit8 v0, v0, 0x1

    .line 331
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 327
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 335
    .end local v2    # "element":Ljava/lang/String;
    :cond_7
    add-int/2addr v4, v1

    .line 336
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 338
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_8
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    array-length v5, v5

    if-lez v5, :cond_a

    .line 339
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    array-length v5, v5

    if-ge v3, v5, :cond_a

    .line 340
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    aget-object v2, v5, v3

    .line 341
    .local v2, "element":Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    if-eqz v2, :cond_9

    .line 342
    const/16 v5, 0x9

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 339
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 347
    .end local v2    # "element":Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    .end local v3    # "i":I
    :cond_a
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productVersion:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 348
    const/16 v5, 0xa

    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productVersion:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 351
    :cond_b
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    if-eqz v5, :cond_d

    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    array-length v5, v5

    if-lez v5, :cond_d

    .line 352
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    array-length v5, v5

    if-ge v3, v5, :cond_d

    .line 353
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    aget-object v2, v5, v3

    .line 354
    .local v2, "element":Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    if-eqz v2, :cond_c

    .line 355
    const/16 v5, 0xb

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 352
    :cond_c
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 360
    .end local v2    # "element":Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    .end local v3    # "i":I
    :cond_d
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificContext:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_e

    .line 361
    const/16 v5, 0xf

    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificContext:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 364
    :cond_e
    iget v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->spellingErrorCount:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_f

    .line 365
    const/16 v5, 0x11

    iget v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->spellingErrorCount:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 368
    :cond_f
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->additionalFormContent:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    if-eqz v5, :cond_11

    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->additionalFormContent:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    array-length v5, v5

    if-lez v5, :cond_11

    .line 369
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->additionalFormContent:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    array-length v5, v5

    if-ge v3, v5, :cond_11

    .line 370
    iget-object v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->additionalFormContent:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    aget-object v2, v5, v3

    .line 371
    .restart local v2    # "element":Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    if-eqz v2, :cond_10

    .line 372
    const/16 v5, 0x13

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 369
    :cond_10
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 377
    .end local v2    # "element":Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    .end local v3    # "i":I
    :cond_11
    iget v5, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->reportType:I

    if-eq v5, v10, :cond_12

    .line 378
    const/16 v5, 0x15

    iget v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->reportType:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 381
    :cond_12
    return v4
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 113
    if-ne p1, p0, :cond_1

    .line 194
    :cond_0
    :goto_0
    return v1

    .line 116
    :cond_1
    instance-of v3, p1, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    if-nez v3, :cond_2

    move v1, v2

    .line 117
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 119
    check-cast v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    .line 120
    .local v0, "other":Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;
    iget-wide v4, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->gaiaId:J

    iget-wide v6, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->gaiaId:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    move v1, v2

    .line 121
    goto :goto_0

    .line 123
    :cond_3
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->description:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 124
    iget-object v3, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->description:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    .line 125
    goto :goto_0

    .line 127
    :cond_4
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->description:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->description:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 128
    goto :goto_0

    .line 130
    :cond_5
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->descriptionTranslated:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 131
    iget-object v3, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->descriptionTranslated:Ljava/lang/String;

    if-eqz v3, :cond_7

    move v1, v2

    .line 132
    goto :goto_0

    .line 134
    :cond_6
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->descriptionTranslated:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->descriptionTranslated:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 135
    goto :goto_0

    .line 137
    :cond_7
    iget v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->spellingErrorCount:I

    iget v4, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->spellingErrorCount:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 138
    goto :goto_0

    .line 140
    :cond_8
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->sourceDescriptionLanguage:Ljava/lang/String;

    if-nez v3, :cond_9

    .line 141
    iget-object v3, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->sourceDescriptionLanguage:Ljava/lang/String;

    if-eqz v3, :cond_a

    move v1, v2

    .line 142
    goto :goto_0

    .line 144
    :cond_9
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->sourceDescriptionLanguage:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->sourceDescriptionLanguage:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 145
    goto :goto_0

    .line 147
    :cond_a
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->uiLanguage:Ljava/lang/String;

    if-nez v3, :cond_b

    .line 148
    iget-object v3, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->uiLanguage:Ljava/lang/String;

    if-eqz v3, :cond_c

    move v1, v2

    .line 149
    goto :goto_0

    .line 151
    :cond_b
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->uiLanguage:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->uiLanguage:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    move v1, v2

    .line 152
    goto :goto_0

    .line 154
    :cond_c
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->userEmail:Ljava/lang/String;

    if-nez v3, :cond_d

    .line 155
    iget-object v3, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->userEmail:Ljava/lang/String;

    if-eqz v3, :cond_e

    move v1, v2

    .line 156
    goto :goto_0

    .line 158
    :cond_d
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->userEmail:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->userEmail:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    move v1, v2

    .line 159
    goto/16 :goto_0

    .line 161
    :cond_e
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryDataName:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryDataName:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    move v1, v2

    .line 163
    goto/16 :goto_0

    .line 165
    :cond_f
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    move v1, v2

    .line 167
    goto/16 :goto_0

    .line 169
    :cond_10
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productVersion:Ljava/lang/String;

    if-nez v3, :cond_11

    .line 170
    iget-object v3, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productVersion:Ljava/lang/String;

    if-eqz v3, :cond_12

    move v1, v2

    .line 171
    goto/16 :goto_0

    .line 173
    :cond_11
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productVersion:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    move v1, v2

    .line 174
    goto/16 :goto_0

    .line 176
    :cond_12
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    move v1, v2

    .line 178
    goto/16 :goto_0

    .line 180
    :cond_13
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificContext:Ljava/lang/String;

    if-nez v3, :cond_14

    .line 181
    iget-object v3, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificContext:Ljava/lang/String;

    if-eqz v3, :cond_15

    move v1, v2

    .line 182
    goto/16 :goto_0

    .line 184
    :cond_14
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificContext:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificContext:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_15

    move v1, v2

    .line 185
    goto/16 :goto_0

    .line 187
    :cond_15
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->additionalFormContent:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->additionalFormContent:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16

    move v1, v2

    .line 189
    goto/16 :goto_0

    .line 191
    :cond_16
    iget v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->reportType:I

    iget v4, v0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->reportType:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 192
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 199
    const/16 v0, 0x11

    .line 200
    .local v0, "result":I
    iget-wide v4, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->gaiaId:J

    iget-wide v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->gaiaId:J

    const/16 v1, 0x20

    ushr-long/2addr v6, v1

    xor-long/2addr v4, v6

    long-to-int v1, v4

    add-int/lit16 v0, v1, 0x20f

    .line 202
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->description:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 204
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->descriptionTranslated:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 206
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->spellingErrorCount:I

    add-int v0, v1, v3

    .line 207
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->sourceDescriptionLanguage:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 209
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->uiLanguage:Ljava/lang/String;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v3, v1

    .line 211
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->userEmail:Ljava/lang/String;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    add-int v0, v3, v1

    .line 213
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryDataName:[Ljava/lang/String;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 215
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 217
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productVersion:Ljava/lang/String;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    add-int v0, v3, v1

    .line 219
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 221
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificContext:Ljava/lang/String;

    if-nez v3, :cond_6

    :goto_6
    add-int v0, v1, v2

    .line 223
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->additionalFormContent:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 225
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->reportType:I

    add-int v0, v1, v2

    .line 226
    return v0

    .line 202
    :cond_0
    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->description:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 204
    :cond_1
    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->descriptionTranslated:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 207
    :cond_2
    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->sourceDescriptionLanguage:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 209
    :cond_3
    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->uiLanguage:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    .line 211
    :cond_4
    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->userEmail:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    .line 217
    :cond_5
    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productVersion:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    .line 221
    :cond_6
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificContext:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_6
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 389
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 390
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 394
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 395
    :sswitch_0
    return-object p0

    .line 400
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFixed64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->gaiaId:J

    goto :goto_0

    .line 404
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->description:Ljava/lang/String;

    goto :goto_0

    .line 408
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->userEmail:Ljava/lang/String;

    goto :goto_0

    .line 412
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->descriptionTranslated:Ljava/lang/String;

    goto :goto_0

    .line 416
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->sourceDescriptionLanguage:Ljava/lang/String;

    goto :goto_0

    .line 420
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->uiLanguage:Ljava/lang/String;

    goto :goto_0

    .line 424
    :sswitch_7
    const/16 v6, 0x42

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 426
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryDataName:[Ljava/lang/String;

    if-nez v6, :cond_2

    move v1, v5

    .line 427
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 428
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 429
    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryDataName:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 431
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 432
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 433
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 431
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 426
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryDataName:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_1

    .line 436
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 437
    iput-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryDataName:[Ljava/lang/String;

    goto :goto_0

    .line 441
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_8
    const/16 v6, 0x4a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 443
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    if-nez v6, :cond_5

    move v1, v5

    .line 444
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    .line 446
    .local v2, "newArray":[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    if-eqz v1, :cond_4

    .line 447
    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 449
    :cond_4
    :goto_4
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_6

    .line 450
    new-instance v6, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    invoke-direct {v6}, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;-><init>()V

    aput-object v6, v2, v1

    .line 451
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 452
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 449
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 443
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    :cond_5
    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    array-length v1, v6

    goto :goto_3

    .line 455
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    :cond_6
    new-instance v6, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    invoke-direct {v6}, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;-><init>()V

    aput-object v6, v2, v1

    .line 456
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 457
    iput-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    goto/16 :goto_0

    .line 461
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productVersion:Ljava/lang/String;

    goto/16 :goto_0

    .line 465
    :sswitch_a
    const/16 v6, 0x5a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 467
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    if-nez v6, :cond_8

    move v1, v5

    .line 468
    .restart local v1    # "i":I
    :goto_5
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    .line 470
    .local v2, "newArray":[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    if-eqz v1, :cond_7

    .line 471
    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 473
    :cond_7
    :goto_6
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_9

    .line 474
    new-instance v6, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    invoke-direct {v6}, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;-><init>()V

    aput-object v6, v2, v1

    .line 475
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 476
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 473
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 467
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    :cond_8
    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    array-length v1, v6

    goto :goto_5

    .line 479
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    :cond_9
    new-instance v6, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    invoke-direct {v6}, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;-><init>()V

    aput-object v6, v2, v1

    .line 480
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 481
    iput-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    goto/16 :goto_0

    .line 485
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificContext:Ljava/lang/String;

    goto/16 :goto_0

    .line 489
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->spellingErrorCount:I

    goto/16 :goto_0

    .line 493
    :sswitch_d
    const/16 v6, 0x9a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 495
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->additionalFormContent:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    if-nez v6, :cond_b

    move v1, v5

    .line 496
    .restart local v1    # "i":I
    :goto_7
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    .line 498
    .restart local v2    # "newArray":[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    if-eqz v1, :cond_a

    .line 499
    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->additionalFormContent:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 501
    :cond_a
    :goto_8
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_c

    .line 502
    new-instance v6, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    invoke-direct {v6}, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;-><init>()V

    aput-object v6, v2, v1

    .line 503
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 504
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 501
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 495
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    :cond_b
    iget-object v6, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->additionalFormContent:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    array-length v1, v6

    goto :goto_7

    .line 507
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    :cond_c
    new-instance v6, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    invoke-direct {v6}, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;-><init>()V

    aput-object v6, v2, v1

    .line 508
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 509
    iput-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->additionalFormContent:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    goto/16 :goto_0

    .line 513
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 514
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto/16 :goto_0

    .line 532
    :pswitch_0
    iput v4, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->reportType:I

    goto/16 :goto_0

    .line 390
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x7a -> :sswitch_b
        0x88 -> :sswitch_c
        0x9a -> :sswitch_d
        0xa8 -> :sswitch_e
    .end sparse-switch

    .line 514
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 7
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 232
    iget-wide v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->gaiaId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 233
    iget-wide v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->gaiaId:J

    invoke-virtual {p1, v6, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFixed64(IJ)V

    .line 235
    :cond_0
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->description:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 236
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->description:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 238
    :cond_1
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->userEmail:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 239
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->userEmail:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 241
    :cond_2
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->descriptionTranslated:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 242
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->descriptionTranslated:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 244
    :cond_3
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->sourceDescriptionLanguage:Ljava/lang/String;

    const-string v3, "en"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 245
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->sourceDescriptionLanguage:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 247
    :cond_4
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->uiLanguage:Ljava/lang/String;

    const-string v3, "en_US"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 248
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->uiLanguage:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 250
    :cond_5
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryDataName:[Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryDataName:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 251
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryDataName:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 252
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryDataName:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 253
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_6

    .line 254
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 251
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 258
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_7
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 259
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 260
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificBinaryData:[Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;

    aget-object v0, v2, v1

    .line 261
    .local v0, "element":Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    if-eqz v0, :cond_8

    .line 262
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 259
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 266
    .end local v0    # "element":Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$ProductSpecificBinaryData;
    .end local v1    # "i":I
    :cond_9
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productVersion:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 267
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productVersion:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 269
    :cond_a
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    array-length v2, v2

    if-lez v2, :cond_c

    .line 270
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    array-length v2, v2

    if-ge v1, v2, :cond_c

    .line 271
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificData:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    aget-object v0, v2, v1

    .line 272
    .local v0, "element":Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    if-eqz v0, :cond_b

    .line 273
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 270
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 277
    .end local v0    # "element":Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    .end local v1    # "i":I
    :cond_c
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificContext:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 278
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->productSpecificContext:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 280
    :cond_d
    iget v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->spellingErrorCount:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_e

    .line 281
    const/16 v2, 0x11

    iget v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->spellingErrorCount:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 283
    :cond_e
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->additionalFormContent:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->additionalFormContent:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    array-length v2, v2

    if-lez v2, :cond_10

    .line 284
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->additionalFormContent:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    array-length v2, v2

    if-ge v1, v2, :cond_10

    .line 285
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->additionalFormContent:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    aget-object v0, v2, v1

    .line 286
    .restart local v0    # "element":Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    if-eqz v0, :cond_f

    .line 287
    const/16 v2, 0x13

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 284
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 291
    .end local v0    # "element":Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    .end local v1    # "i":I
    :cond_10
    iget v2, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->reportType:I

    if-eq v2, v6, :cond_11

    .line 292
    const/16 v2, 0x15

    iget v3, p0, Lcom/google/protos/userfeedback/UserFeedbackCommonProtos$CommonData;->reportType:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 294
    :cond_11
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 295
    return-void
.end method
