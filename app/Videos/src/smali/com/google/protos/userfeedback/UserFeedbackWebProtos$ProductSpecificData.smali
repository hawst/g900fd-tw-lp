.class public final Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
.super Lcom/google/protobuf/nano/MessageNano;
.source "UserFeedbackWebProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/protos/userfeedback/UserFeedbackWebProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProductSpecificData"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;


# instance fields
.field public key:Ljava/lang/String;

.field public type:I

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 99
    invoke-virtual {p0}, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->clear()Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    .line 100
    return-void
.end method

.method public static emptyArray()[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    .locals 2

    .prologue
    .line 78
    sget-object v0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->_emptyArray:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    if-nez v0, :cond_1

    .line 79
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 81
    :try_start_0
    sget-object v0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->_emptyArray:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    if-nez v0, :cond_0

    .line 82
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    sput-object v0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->_emptyArray:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    .line 84
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    :cond_1
    sget-object v0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->_emptyArray:[Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    return-object v0

    .line 84
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    .locals 1

    .prologue
    .line 103
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->key:Ljava/lang/String;

    .line 104
    const-string v0, ""

    iput-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->value:Ljava/lang/String;

    .line 105
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->type:I

    .line 106
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->cachedSize:I

    .line 107
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 165
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 166
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->key:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->value:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 169
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->value:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    :cond_0
    iget v1, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->type:I

    if-eq v1, v3, :cond_1

    .line 173
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->type:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    :cond_1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 112
    if-ne p1, p0, :cond_1

    .line 136
    :cond_0
    :goto_0
    return v1

    .line 115
    :cond_1
    instance-of v3, p1, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    if-nez v3, :cond_2

    move v1, v2

    .line 116
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 118
    check-cast v0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    .line 119
    .local v0, "other":Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->key:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 120
    iget-object v3, v0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->key:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 121
    goto :goto_0

    .line 123
    :cond_3
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->key:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->key:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 124
    goto :goto_0

    .line 126
    :cond_4
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->value:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 127
    iget-object v3, v0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->value:Ljava/lang/String;

    if-eqz v3, :cond_6

    move v1, v2

    .line 128
    goto :goto_0

    .line 130
    :cond_5
    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->value:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->value:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 131
    goto :goto_0

    .line 133
    :cond_6
    iget v3, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->type:I

    iget v4, v0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->type:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 134
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 141
    const/16 v0, 0x11

    .line 142
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->key:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 144
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->value:Ljava/lang/String;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 146
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->type:I

    add-int v0, v1, v2

    .line 147
    return v0

    .line 142
    :cond_0
    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->key:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 144
    :cond_1
    iget-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->value:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    invoke-virtual {p0, p1}, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 185
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 189
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 190
    :sswitch_0
    return-object p0

    .line 195
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->key:Ljava/lang/String;

    goto :goto_0

    .line 199
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->value:Ljava/lang/String;

    goto :goto_0

    .line 203
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 204
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 208
    :pswitch_0
    iput v1, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->type:I

    goto :goto_0

    .line 185
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    .line 204
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 153
    iget-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->key:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->value:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->value:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 157
    :cond_0
    iget v0, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->type:I

    if-eq v0, v2, :cond_1

    .line 158
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/protos/userfeedback/UserFeedbackWebProtos$ProductSpecificData;->type:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 160
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 161
    return-void
.end method
