.class public final Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DeviceGcmRegisterRequest.java"


# instance fields
.field public registrationId:Ljava/lang/String;

.field public resourceId:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 30
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->clear()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    .line 31
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;
    .locals 1

    .prologue
    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->registrationId:Ljava/lang/String;

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->cachedSize:I

    .line 37
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 92
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 93
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->registrationId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 94
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->registrationId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-eqz v1, :cond_1

    .line 98
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    if-ne p1, p0, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v1

    .line 45
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    if-nez v3, :cond_2

    move v1, v2

    .line 46
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 48
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    .line 49
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->registrationId:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 50
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->registrationId:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 51
    goto :goto_0

    .line 53
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->registrationId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->registrationId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 54
    goto :goto_0

    .line 56
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-nez v3, :cond_5

    .line 57
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-eqz v3, :cond_0

    move v1, v2

    .line 58
    goto :goto_0

    .line 61
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 62
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 70
    const/16 v0, 0x11

    .line 71
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->registrationId:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 73
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 75
    return v0

    .line 71
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->registrationId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 73
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 110
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 114
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 115
    :sswitch_0
    return-object p0

    .line 120
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->registrationId:Ljava/lang/String;

    goto :goto_0

    .line 124
    :sswitch_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-nez v1, :cond_1

    .line 125
    new-instance v1, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    invoke-direct {v1}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    .line 127
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 110
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->registrationId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->registrationId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-eqz v0, :cond_1

    .line 85
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/DeviceGcmRegisterRequest;->resourceId:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 87
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 88
    return-void
.end method
