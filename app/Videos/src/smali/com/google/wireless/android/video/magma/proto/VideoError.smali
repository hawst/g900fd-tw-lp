.class public final Lcom/google/wireless/android/video/magma/proto/VideoError;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VideoError.java"


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoError;


# instance fields
.field public assetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 36
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoError;->clear()Lcom/google/wireless/android/video/magma/proto/VideoError;

    .line 37
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/VideoError;
    .locals 2

    .prologue
    .line 18
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoError;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    if-nez v0, :cond_1

    .line 19
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 21
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoError;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    if-nez v0, :cond_0

    .line 22
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/VideoError;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/VideoError;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    .line 24
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 26
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoError;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    return-object v0

    .line 24
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/VideoError;
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->type:I

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->assetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->cachedSize:I

    .line 43
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 93
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 94
    .local v0, "size":I
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->type:I

    if-eq v1, v2, :cond_0

    .line 95
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->type:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->assetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v1, :cond_1

    .line 99
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->assetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    if-ne p1, p0, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v1

    .line 51
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/VideoError;

    if-nez v3, :cond_2

    move v1, v2

    .line 52
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 54
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/VideoError;

    .line 55
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/VideoError;
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->type:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoError;->type:I

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 56
    goto :goto_0

    .line 58
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->assetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v3, :cond_4

    .line 59
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/VideoError;->assetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v3, :cond_0

    move v1, v2

    .line 60
    goto :goto_0

    .line 63
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->assetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoError;->assetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 64
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 72
    const/16 v0, 0x11

    .line 73
    .local v0, "result":I
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->type:I

    add-int/lit16 v0, v1, 0x20f

    .line 74
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->assetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 76
    return v0

    .line 74
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->assetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/VideoError;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/VideoError;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/VideoError;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 111
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 115
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 116
    :sswitch_0
    return-object p0

    .line 121
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 122
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 125
    :pswitch_0
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->type:I

    goto :goto_0

    .line 131
    .end local v1    # "value":I
    :sswitch_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->assetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v2, :cond_1

    .line 132
    new-instance v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-direct {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->assetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 134
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->assetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 111
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    .line 122
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 82
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->type:I

    if-eq v0, v1, :cond_0

    .line 83
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->type:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->assetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v0, :cond_1

    .line 86
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoError;->assetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 88
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 89
    return-void
.end method
