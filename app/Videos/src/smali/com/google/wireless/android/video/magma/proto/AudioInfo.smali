.class public final Lcom/google/wireless/android/video/magma/proto/AudioInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AudioInfo.java"


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;


# instance fields
.field public audio51:Z

.field public audioSourceType:I

.field public dEPRECATEDIsDubbed:Z

.field public dEPRECATEDIsOriginal:Z

.field public language:Ljava/lang/String;

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 56
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->clear()Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 57
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .locals 2

    .prologue
    .line 26
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-nez v0, :cond_1

    .line 27
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 29
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-nez v0, :cond_0

    .line 30
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 32
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    return-object v0

    .line 32
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    .line 61
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->type:I

    .line 62
    iput-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsOriginal:Z

    .line 63
    iput-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsDubbed:Z

    .line 64
    iput-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audio51:Z

    .line 65
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audioSourceType:I

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->cachedSize:I

    .line 67
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 143
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 144
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 145
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 148
    :cond_0
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->type:I

    if-eq v1, v3, :cond_1

    .line 149
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->type:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 152
    :cond_1
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsOriginal:Z

    if-eqz v1, :cond_2

    .line 153
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsOriginal:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 156
    :cond_2
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsDubbed:Z

    if-eqz v1, :cond_3

    .line 157
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsDubbed:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 160
    :cond_3
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audio51:Z

    if-eqz v1, :cond_4

    .line 161
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audio51:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 164
    :cond_4
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audioSourceType:I

    if-eqz v1, :cond_5

    .line 165
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audioSourceType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    :cond_5
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 72
    if-ne p1, p0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v1

    .line 75
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-nez v3, :cond_2

    move v1, v2

    .line 76
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 78
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 79
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 80
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 81
    goto :goto_0

    .line 83
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 84
    goto :goto_0

    .line 86
    :cond_4
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->type:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->type:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 87
    goto :goto_0

    .line 89
    :cond_5
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsOriginal:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsOriginal:Z

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 90
    goto :goto_0

    .line 92
    :cond_6
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsDubbed:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsDubbed:Z

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 93
    goto :goto_0

    .line 95
    :cond_7
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audio51:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audio51:Z

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 96
    goto :goto_0

    .line 98
    :cond_8
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audioSourceType:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audioSourceType:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 99
    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    .line 106
    const/16 v0, 0x11

    .line 107
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 109
    mul-int/lit8 v1, v0, 0x1f

    iget v4, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->type:I

    add-int v0, v1, v4

    .line 110
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsOriginal:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v4, v1

    .line 111
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsDubbed:Z

    if-eqz v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v4, v1

    .line 112
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audio51:Z

    if-eqz v4, :cond_3

    :goto_3
    add-int v0, v1, v2

    .line 113
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audioSourceType:I

    add-int v0, v1, v2

    .line 114
    return v0

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    move v1, v3

    .line 110
    goto :goto_1

    :cond_2
    move v1, v3

    .line 111
    goto :goto_2

    :cond_3
    move v2, v3

    .line 112
    goto :goto_3
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 177
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 181
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 182
    :sswitch_0
    return-object p0

    .line 187
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    goto :goto_0

    .line 191
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 192
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 196
    :pswitch_0
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->type:I

    goto :goto_0

    .line 202
    .end local v1    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsOriginal:Z

    goto :goto_0

    .line 206
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsDubbed:Z

    goto :goto_0

    .line 210
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audio51:Z

    goto :goto_0

    .line 214
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 215
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 219
    :pswitch_1
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audioSourceType:I

    goto :goto_0

    .line 177
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch

    .line 192
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 215
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 120
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 123
    :cond_0
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->type:I

    if-eq v0, v2, :cond_1

    .line 124
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->type:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 126
    :cond_1
    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsOriginal:Z

    if-eqz v0, :cond_2

    .line 127
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsOriginal:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 129
    :cond_2
    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsDubbed:Z

    if-eqz v0, :cond_3

    .line 130
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->dEPRECATEDIsDubbed:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 132
    :cond_3
    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audio51:Z

    if-eqz v0, :cond_4

    .line 133
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audio51:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 135
    :cond_4
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audioSourceType:I

    if-eqz v0, :cond_5

    .line 136
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->audioSourceType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 138
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 139
    return-void
.end method
