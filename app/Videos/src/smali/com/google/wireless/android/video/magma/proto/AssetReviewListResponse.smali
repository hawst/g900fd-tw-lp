.class public final Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AssetReviewListResponse.java"


# instance fields
.field public pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

.field public resource:[Lcom/google/wireless/android/video/magma/proto/Review;

.field public tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 33
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->clear()Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;

    .line 34
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    .line 38
    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    .line 39
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/Review;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/Review;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/Review;

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->cachedSize:I

    .line 41
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 112
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 113
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    if-eqz v3, :cond_0

    .line 114
    const/16 v3, 0x3ec

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 117
    :cond_0
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    if-eqz v3, :cond_1

    .line 118
    const/16 v3, 0x3ee

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 121
    :cond_1
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/Review;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/Review;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 122
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/Review;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 123
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/Review;

    aget-object v0, v3, v1

    .line 124
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/Review;
    if-eqz v0, :cond_2

    .line 125
    const/16 v3, 0x3ef

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 122
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 130
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/Review;
    .end local v1    # "i":I
    :cond_3
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 46
    if-ne p1, p0, :cond_1

    .line 75
    :cond_0
    :goto_0
    return v1

    .line 49
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;

    if-nez v3, :cond_2

    move v1, v2

    .line 50
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 52
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;

    .line 53
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    if-nez v3, :cond_3

    .line 54
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    if-eqz v3, :cond_4

    move v1, v2

    .line 55
    goto :goto_0

    .line 58
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 59
    goto :goto_0

    .line 62
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    if-nez v3, :cond_5

    .line 63
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    if-eqz v3, :cond_6

    move v1, v2

    .line 64
    goto :goto_0

    .line 67
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 68
    goto :goto_0

    .line 71
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/Review;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/Review;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 73
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 80
    const/16 v0, 0x11

    .line 81
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 83
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 85
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/Review;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 87
    return v0

    .line 81
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->hashCode()I

    move-result v1

    goto :goto_0

    .line 83
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 138
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 139
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 143
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 144
    :sswitch_0
    return-object p0

    .line 149
    :sswitch_1
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    if-nez v5, :cond_1

    .line 150
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;-><init>()V

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    .line 152
    :cond_1
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 156
    :sswitch_2
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    if-nez v5, :cond_2

    .line 157
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;-><init>()V

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    .line 159
    :cond_2
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 163
    :sswitch_3
    const/16 v5, 0x1f7a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 165
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/Review;

    if-nez v5, :cond_4

    move v1, v4

    .line 166
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/Review;

    .line 168
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/Review;
    if-eqz v1, :cond_3

    .line 169
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/Review;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 171
    :cond_3
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_5

    .line 172
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/Review;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/Review;-><init>()V

    aput-object v5, v2, v1

    .line 173
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 174
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 171
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 165
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/Review;
    :cond_4
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/Review;

    array-length v1, v5

    goto :goto_1

    .line 177
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/Review;
    :cond_5
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/Review;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/Review;-><init>()V

    aput-object v5, v2, v1

    .line 178
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 179
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/Review;

    goto :goto_0

    .line 139
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1f62 -> :sswitch_1
        0x1f72 -> :sswitch_2
        0x1f7a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    if-eqz v2, :cond_0

    .line 94
    const/16 v2, 0x3ec

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 96
    :cond_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    if-eqz v2, :cond_1

    .line 97
    const/16 v2, 0x3ee

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 99
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/Review;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/Review;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 100
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/Review;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 101
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/Review;

    aget-object v0, v2, v1

    .line 102
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/Review;
    if-eqz v0, :cond_2

    .line 103
    const/16 v2, 0x3ef

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 100
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 107
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/Review;
    .end local v1    # "i":I
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 108
    return-void
.end method
