.class public final Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "MpdUrlGetResponse.java"


# instance fields
.field public resource:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 27
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;->clear()Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;

    .line 28
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;
    .locals 1

    .prologue
    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;->resource:Ljava/lang/String;

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;->cachedSize:I

    .line 33
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 74
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 75
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;->resource:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 76
    const/16 v1, 0x3ef

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;->resource:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_0
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 38
    if-ne p1, p0, :cond_1

    .line 52
    :cond_0
    :goto_0
    return v1

    .line 41
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;

    if-nez v3, :cond_2

    move v1, v2

    .line 42
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 44
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;

    .line 45
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;->resource:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 46
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;->resource:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 47
    goto :goto_0

    .line 49
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;->resource:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;->resource:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 50
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 57
    const/16 v0, 0x11

    .line 58
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;->resource:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 60
    return v0

    .line 58
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;->resource:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 88
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 92
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 93
    :sswitch_0
    return-object p0

    .line 98
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;->resource:Ljava/lang/String;

    goto :goto_0

    .line 88
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1f7a -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;->resource:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    const/16 v0, 0x3ef

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;->resource:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 69
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 70
    return-void
.end method
