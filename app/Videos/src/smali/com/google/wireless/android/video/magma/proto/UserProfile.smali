.class public final Lcom/google/wireless/android/video/magma/proto/UserProfile;
.super Lcom/google/protobuf/nano/MessageNano;
.source "UserProfile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;
    }
.end annotation


# instance fields
.field public profileUrl:Ljava/lang/String;

.field public type:I

.field public userImage:Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

.field public userName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 153
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/UserProfile;->clear()Lcom/google/wireless/android/video/magma/proto/UserProfile;

    .line 154
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/UserProfile;
    .locals 1

    .prologue
    .line 157
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userName:Ljava/lang/String;

    .line 158
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->type:I

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userImage:Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    .line 160
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->profileUrl:Ljava/lang/String;

    .line 161
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->cachedSize:I

    .line 162
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 236
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 237
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 238
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userName:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 241
    :cond_0
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->type:I

    if-eq v1, v3, :cond_1

    .line 242
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->type:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 245
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userImage:Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    if-eqz v1, :cond_2

    .line 246
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userImage:Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 249
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->profileUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 250
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->profileUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 253
    :cond_3
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 167
    if-ne p1, p0, :cond_1

    .line 200
    :cond_0
    :goto_0
    return v1

    .line 170
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/UserProfile;

    if-nez v3, :cond_2

    move v1, v2

    .line 171
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 173
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/UserProfile;

    .line 174
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/UserProfile;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userName:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 175
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userName:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 176
    goto :goto_0

    .line 178
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userName:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 179
    goto :goto_0

    .line 181
    :cond_4
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->type:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->type:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 182
    goto :goto_0

    .line 184
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userImage:Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    if-nez v3, :cond_6

    .line 185
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userImage:Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    if-eqz v3, :cond_7

    move v1, v2

    .line 186
    goto :goto_0

    .line 189
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userImage:Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userImage:Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 190
    goto :goto_0

    .line 193
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->profileUrl:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 194
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->profileUrl:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 195
    goto :goto_0

    .line 197
    :cond_8
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->profileUrl:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->profileUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 198
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 205
    const/16 v0, 0x11

    .line 206
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userName:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 208
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->type:I

    add-int v0, v1, v3

    .line 209
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userImage:Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 211
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->profileUrl:Ljava/lang/String;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 213
    return v0

    .line 206
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 209
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userImage:Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->hashCode()I

    move-result v1

    goto :goto_1

    .line 211
    :cond_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->profileUrl:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/UserProfile;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/UserProfile;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/UserProfile;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 261
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 262
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 266
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 267
    :sswitch_0
    return-object p0

    .line 272
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userName:Ljava/lang/String;

    goto :goto_0

    .line 276
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 277
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 280
    :pswitch_0
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->type:I

    goto :goto_0

    .line 286
    .end local v1    # "value":I
    :sswitch_3
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userImage:Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    if-nez v2, :cond_1

    .line 287
    new-instance v2, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    invoke-direct {v2}, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userImage:Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    .line 289
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userImage:Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 293
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->profileUrl:Ljava/lang/String;

    goto :goto_0

    .line 262
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    .line 277
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 219
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userName:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 222
    :cond_0
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->type:I

    if-eq v0, v2, :cond_1

    .line 223
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->type:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 225
    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userImage:Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    if-eqz v0, :cond_2

    .line 226
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userImage:Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 228
    :cond_2
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->profileUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 229
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->profileUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 231
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 232
    return-void
.end method
