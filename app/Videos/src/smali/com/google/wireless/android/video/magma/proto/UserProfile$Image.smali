.class public final Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;
.super Lcom/google/protobuf/nano/MessageNano;
.source "UserProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/UserProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Image"
.end annotation


# instance fields
.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 36
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->clear()Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    .line 37
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;
    .locals 1

    .prologue
    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->url:Ljava/lang/String;

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->cachedSize:I

    .line 42
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 83
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 84
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->url:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 85
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->url:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_0
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 47
    if-ne p1, p0, :cond_1

    .line 61
    :cond_0
    :goto_0
    return v1

    .line 50
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    if-nez v3, :cond_2

    move v1, v2

    .line 51
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 53
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    .line 54
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->url:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 55
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->url:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 56
    goto :goto_0

    .line 58
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->url:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->url:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 59
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 66
    const/16 v0, 0x11

    .line 67
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->url:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 69
    return v0

    .line 67
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->url:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 97
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 101
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 102
    :sswitch_0
    return-object p0

    .line 107
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->url:Ljava/lang/String;

    goto :goto_0

    .line 97
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->url:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->url:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 78
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 79
    return-void
.end method
