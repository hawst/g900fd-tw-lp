.class public final Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VideoResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/VideoResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Playback"
.end annotation


# instance fields
.field public device:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

.field public positionMsec:J

.field public startTimestampMsec:J

.field public stopTimestampMsec:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 169
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->clear()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    .line 170
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 173
    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec:J

    .line 174
    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    .line 175
    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec:J

    .line 176
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    .line 177
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->cachedSize:I

    .line 178
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 245
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 246
    .local v0, "size":I
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 247
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 250
    :cond_0
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 251
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 254
    :cond_1
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 255
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-eqz v1, :cond_3

    .line 259
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 262
    :cond_3
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 183
    if-ne p1, p0, :cond_1

    .line 208
    :cond_0
    :goto_0
    return v1

    .line 186
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    if-nez v3, :cond_2

    move v1, v2

    .line 187
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 189
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    .line 190
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    move v1, v2

    .line 191
    goto :goto_0

    .line 193
    :cond_3
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    move v1, v2

    .line 194
    goto :goto_0

    .line 196
    :cond_4
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_5

    move v1, v2

    .line 197
    goto :goto_0

    .line 199
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-nez v3, :cond_6

    .line 200
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-eqz v3, :cond_0

    move v1, v2

    .line 201
    goto :goto_0

    .line 204
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 205
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 213
    const/16 v0, 0x11

    .line 214
    .local v0, "result":I
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/lit16 v0, v1, 0x20f

    .line 216
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 218
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 220
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 222
    return v0

    .line 220
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 139
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 270
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 271
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 275
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 276
    :sswitch_0
    return-object p0

    .line 281
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec:J

    goto :goto_0

    .line 285
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    goto :goto_0

    .line 289
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec:J

    goto :goto_0

    .line 293
    :sswitch_4
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-nez v1, :cond_1

    .line 294
    new-instance v1, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    invoke-direct {v1}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    .line 296
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 271
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 228
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 229
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 231
    :cond_0
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 232
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 234
    :cond_1
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 235
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 237
    :cond_2
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-eqz v0, :cond_3

    .line 238
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->device:Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 240
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 241
    return-void
.end method
