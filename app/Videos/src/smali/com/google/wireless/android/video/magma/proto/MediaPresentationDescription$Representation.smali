.class public final Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
.super Lcom/google/protobuf/nano/MessageNano;
.source "MediaPresentationDescription.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Representation"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;


# instance fields
.field public audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

.field public audioLang:Ljava/lang/String;

.field public bitrate:J

.field public codec:Ljava/lang/String;

.field public duration:D

.field public fileSize:J

.field public format:I

.field public height:I

.field public index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

.field public init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

.field public playbackUrl:Ljava/lang/String;

.field public timestampMillis:J

.field public width:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 66
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->clear()Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    .line 67
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    .locals 2

    .prologue
    .line 15
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    if-nez v0, :cond_1

    .line 16
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 18
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    if-nez v0, :cond_0

    .line 19
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    .line 21
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    return-object v0

    .line 21
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 70
    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->format:I

    .line 71
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->playbackUrl:Ljava/lang/String;

    .line 72
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->codec:Ljava/lang/String;

    .line 73
    iput-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    .line 74
    iput-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    .line 75
    iput-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->fileSize:J

    .line 76
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->duration:D

    .line 77
    iput-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->bitrate:J

    .line 78
    iput-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->timestampMillis:J

    .line 79
    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->width:I

    .line 80
    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->height:I

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioLang:Ljava/lang/String;

    .line 82
    iput-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 83
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->cachedSize:I

    .line 84
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 250
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 251
    .local v0, "size":I
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->format:I

    if-eqz v1, :cond_0

    .line 252
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->format:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 255
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->playbackUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 256
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->playbackUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 259
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->codec:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 260
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->codec:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 263
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    if-eqz v1, :cond_3

    .line 264
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 267
    :cond_3
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    if-eqz v1, :cond_4

    .line 268
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 271
    :cond_4
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->fileSize:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_5

    .line 272
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->fileSize:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 275
    :cond_5
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->duration:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_6

    .line 277
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->duration:D

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v1

    add-int/2addr v0, v1

    .line 280
    :cond_6
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->bitrate:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_7

    .line 281
    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->bitrate:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 284
    :cond_7
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->timestampMillis:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_8

    .line 285
    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->timestampMillis:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 288
    :cond_8
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->width:I

    if-eqz v1, :cond_9

    .line 289
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->width:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 292
    :cond_9
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->height:I

    if-eqz v1, :cond_a

    .line 293
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->height:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 296
    :cond_a
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioLang:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 297
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioLang:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 300
    :cond_b
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-eqz v1, :cond_c

    .line 301
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 304
    :cond_c
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 10
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 89
    if-ne p1, p0, :cond_1

    .line 168
    :cond_0
    :goto_0
    return v3

    .line 92
    :cond_1
    instance-of v5, p1, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    if-nez v5, :cond_2

    move v3, v4

    .line 93
    goto :goto_0

    :cond_2
    move-object v2, p1

    .line 95
    check-cast v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    .line 96
    .local v2, "other":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->format:I

    iget v6, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->format:I

    if-eq v5, v6, :cond_3

    move v3, v4

    .line 97
    goto :goto_0

    .line 99
    :cond_3
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->playbackUrl:Ljava/lang/String;

    if-nez v5, :cond_4

    .line 100
    iget-object v5, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->playbackUrl:Ljava/lang/String;

    if-eqz v5, :cond_5

    move v3, v4

    .line 101
    goto :goto_0

    .line 103
    :cond_4
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->playbackUrl:Ljava/lang/String;

    iget-object v6, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->playbackUrl:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    move v3, v4

    .line 104
    goto :goto_0

    .line 106
    :cond_5
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->codec:Ljava/lang/String;

    if-nez v5, :cond_6

    .line 107
    iget-object v5, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->codec:Ljava/lang/String;

    if-eqz v5, :cond_7

    move v3, v4

    .line 108
    goto :goto_0

    .line 110
    :cond_6
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->codec:Ljava/lang/String;

    iget-object v6, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->codec:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    move v3, v4

    .line 111
    goto :goto_0

    .line 113
    :cond_7
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    if-nez v5, :cond_8

    .line 114
    iget-object v5, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    if-eqz v5, :cond_9

    move v3, v4

    .line 115
    goto :goto_0

    .line 118
    :cond_8
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    iget-object v6, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    invoke-virtual {v5, v6}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    move v3, v4

    .line 119
    goto :goto_0

    .line 122
    :cond_9
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    if-nez v5, :cond_a

    .line 123
    iget-object v5, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    if-eqz v5, :cond_b

    move v3, v4

    .line 124
    goto :goto_0

    .line 127
    :cond_a
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    iget-object v6, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    invoke-virtual {v5, v6}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    move v3, v4

    .line 128
    goto :goto_0

    .line 131
    :cond_b
    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->fileSize:J

    iget-wide v8, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->fileSize:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_c

    move v3, v4

    .line 132
    goto :goto_0

    .line 135
    :cond_c
    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->duration:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    .line 136
    .local v0, "bits":J
    iget-wide v6, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->duration:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    cmp-long v5, v0, v6

    if-eqz v5, :cond_d

    move v3, v4

    .line 137
    goto/16 :goto_0

    .line 140
    :cond_d
    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->bitrate:J

    iget-wide v8, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->bitrate:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_e

    move v3, v4

    .line 141
    goto/16 :goto_0

    .line 143
    :cond_e
    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->timestampMillis:J

    iget-wide v8, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->timestampMillis:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_f

    move v3, v4

    .line 144
    goto/16 :goto_0

    .line 146
    :cond_f
    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->width:I

    iget v6, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->width:I

    if-eq v5, v6, :cond_10

    move v3, v4

    .line 147
    goto/16 :goto_0

    .line 149
    :cond_10
    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->height:I

    iget v6, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->height:I

    if-eq v5, v6, :cond_11

    move v3, v4

    .line 150
    goto/16 :goto_0

    .line 152
    :cond_11
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioLang:Ljava/lang/String;

    if-nez v5, :cond_12

    .line 153
    iget-object v5, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioLang:Ljava/lang/String;

    if-eqz v5, :cond_13

    move v3, v4

    .line 154
    goto/16 :goto_0

    .line 156
    :cond_12
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioLang:Ljava/lang/String;

    iget-object v6, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioLang:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_13

    move v3, v4

    .line 157
    goto/16 :goto_0

    .line 159
    :cond_13
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-nez v5, :cond_14

    .line 160
    iget-object v5, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-eqz v5, :cond_0

    move v3, v4

    .line 161
    goto/16 :goto_0

    .line 164
    :cond_14
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    iget-object v6, v2, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-virtual {v5, v6}, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    move v3, v4

    .line 165
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 11

    .prologue
    const/16 v10, 0x20

    const/4 v4, 0x0

    .line 173
    const/16 v0, 0x11

    .line 174
    .local v0, "result":I
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->format:I

    add-int/lit16 v0, v1, 0x20f

    .line 175
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->playbackUrl:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v4

    :goto_0
    add-int v0, v5, v1

    .line 177
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->codec:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v4

    :goto_1
    add-int v0, v5, v1

    .line 179
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    if-nez v1, :cond_2

    move v1, v4

    :goto_2
    add-int v0, v5, v1

    .line 181
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    if-nez v1, :cond_3

    move v1, v4

    :goto_3
    add-int v0, v5, v1

    .line 183
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->fileSize:J

    iget-wide v8, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->fileSize:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v5, v6

    add-int v0, v1, v5

    .line 186
    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->duration:D

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 187
    .local v2, "v":J
    mul-int/lit8 v1, v0, 0x1f

    ushr-long v6, v2, v10

    xor-long/2addr v6, v2

    long-to-int v5, v6

    add-int v0, v1, v5

    .line 189
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->bitrate:J

    iget-wide v8, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->bitrate:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v5, v6

    add-int v0, v1, v5

    .line 191
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->timestampMillis:J

    iget-wide v8, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->timestampMillis:J

    ushr-long/2addr v8, v10

    xor-long/2addr v6, v8

    long-to-int v5, v6

    add-int v0, v1, v5

    .line 193
    mul-int/lit8 v1, v0, 0x1f

    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->width:I

    add-int v0, v1, v5

    .line 194
    mul-int/lit8 v1, v0, 0x1f

    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->height:I

    add-int v0, v1, v5

    .line 195
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioLang:Ljava/lang/String;

    if-nez v1, :cond_4

    move v1, v4

    :goto_4
    add-int v0, v5, v1

    .line 197
    mul-int/lit8 v1, v0, 0x1f

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-nez v5, :cond_5

    :goto_5
    add-int v0, v1, v4

    .line 199
    return v0

    .line 175
    .end local v2    # "v":J
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->playbackUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 177
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->codec:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 179
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->hashCode()I

    move-result v1

    goto :goto_2

    .line 181
    :cond_3
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->hashCode()I

    move-result v1

    goto :goto_3

    .line 195
    .restart local v2    # "v":J
    :cond_4
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioLang:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    .line 197
    :cond_5
    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-virtual {v4}, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->hashCode()I

    move-result v4

    goto :goto_5
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 312
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 313
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 317
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 318
    :sswitch_0
    return-object p0

    .line 323
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->format:I

    goto :goto_0

    .line 327
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->playbackUrl:Ljava/lang/String;

    goto :goto_0

    .line 331
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->codec:Ljava/lang/String;

    goto :goto_0

    .line 335
    :sswitch_4
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    if-nez v1, :cond_1

    .line 336
    new-instance v1, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    invoke-direct {v1}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    .line 338
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 342
    :sswitch_5
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    if-nez v1, :cond_2

    .line 343
    new-instance v1, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    invoke-direct {v1}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    .line 345
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 349
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->fileSize:J

    goto :goto_0

    .line 353
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->duration:D

    goto :goto_0

    .line 357
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->bitrate:J

    goto :goto_0

    .line 361
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->timestampMillis:J

    goto :goto_0

    .line 365
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->width:I

    goto :goto_0

    .line 369
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->height:I

    goto :goto_0

    .line 373
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioLang:Ljava/lang/String;

    goto :goto_0

    .line 377
    :sswitch_d
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-nez v1, :cond_3

    .line 378
    new-instance v1, Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-direct {v1}, Lcom/google/wireless/android/video/magma/proto/AudioInfo;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 380
    :cond_3
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 313
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x39 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 205
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->format:I

    if-eqz v0, :cond_0

    .line 206
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->format:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->playbackUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 209
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->playbackUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 211
    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->codec:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 212
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->codec:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 214
    :cond_2
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    if-eqz v0, :cond_3

    .line 215
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->init:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 217
    :cond_3
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    if-eqz v0, :cond_4

    .line 218
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->index:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 220
    :cond_4
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->fileSize:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 221
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->fileSize:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 223
    :cond_5
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->duration:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_6

    .line 225
    const/4 v0, 0x7

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->duration:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 227
    :cond_6
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->bitrate:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    .line 228
    const/16 v0, 0x8

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->bitrate:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 230
    :cond_7
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->timestampMillis:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_8

    .line 231
    const/16 v0, 0x9

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->timestampMillis:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 233
    :cond_8
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->width:I

    if-eqz v0, :cond_9

    .line 234
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->width:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 236
    :cond_9
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->height:I

    if-eqz v0, :cond_a

    .line 237
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->height:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 239
    :cond_a
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioLang:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 240
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioLang:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 242
    :cond_b
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-eqz v0, :cond_c

    .line 243
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->audioInfo:Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 245
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 246
    return-void
.end method
