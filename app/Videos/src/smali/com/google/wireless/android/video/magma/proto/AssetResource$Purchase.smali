.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Purchase"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;


# instance fields
.field public isPreferred:Z

.field public purchaseFormatType:I

.field public purchaseStatus:I

.field public purchaseTimestampSec:J

.field public purchaseToken:Ljava/lang/String;

.field public purchaseType:I

.field public rentalExpirationTimestampSec:J

.field public rentalLongTimerSec:I

.field public rentalPolicy:I

.field public rentalShortTimerSec:I

.field public visible:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1275
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1276
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->clear()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    .line 1277
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .locals 2

    .prologue
    .line 1231
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    if-nez v0, :cond_1

    .line 1232
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1234
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    if-nez v0, :cond_0

    .line 1235
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    .line 1237
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1239
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    return-object v0

    .line 1237
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1280
    iput-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->isPreferred:Z

    .line 1281
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType:I

    .line 1282
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseStatus:I

    .line 1283
    iput-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->visible:Z

    .line 1284
    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec:J

    .line 1285
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalLongTimerSec:I

    .line 1286
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalShortTimerSec:I

    .line 1287
    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec:J

    .line 1288
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalPolicy:I

    .line 1289
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseToken:Ljava/lang/String;

    .line 1290
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseFormatType:I

    .line 1291
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->cachedSize:I

    .line 1292
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    .line 1405
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1406
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->isPreferred:Z

    if-eqz v1, :cond_0

    .line 1407
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->isPreferred:Z

    invoke-static {v4, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1410
    :cond_0
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType:I

    if-eq v1, v4, :cond_1

    .line 1411
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1414
    :cond_1
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseStatus:I

    if-eqz v1, :cond_2

    .line 1415
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseStatus:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1418
    :cond_2
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_3

    .line 1419
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1422
    :cond_3
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalLongTimerSec:I

    if-eqz v1, :cond_4

    .line 1423
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalLongTimerSec:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1426
    :cond_4
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalShortTimerSec:I

    if-eqz v1, :cond_5

    .line 1427
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalShortTimerSec:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1430
    :cond_5
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_6

    .line 1431
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1434
    :cond_6
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseToken:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1435
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseToken:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1438
    :cond_7
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseFormatType:I

    if-eq v1, v4, :cond_8

    .line 1439
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseFormatType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1442
    :cond_8
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalPolicy:I

    if-eq v1, v4, :cond_9

    .line 1443
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalPolicy:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1446
    :cond_9
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->visible:Z

    if-eqz v1, :cond_a

    .line 1447
    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->visible:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1450
    :cond_a
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1297
    if-ne p1, p0, :cond_1

    .line 1341
    :cond_0
    :goto_0
    return v1

    .line 1300
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    if-nez v3, :cond_2

    move v1, v2

    .line 1301
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1303
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    .line 1304
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->isPreferred:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->isPreferred:Z

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 1305
    goto :goto_0

    .line 1307
    :cond_3
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 1308
    goto :goto_0

    .line 1310
    :cond_4
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseStatus:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseStatus:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 1311
    goto :goto_0

    .line 1313
    :cond_5
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->visible:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->visible:Z

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 1314
    goto :goto_0

    .line 1316
    :cond_6
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_7

    move v1, v2

    .line 1317
    goto :goto_0

    .line 1319
    :cond_7
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalLongTimerSec:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalLongTimerSec:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 1320
    goto :goto_0

    .line 1322
    :cond_8
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalShortTimerSec:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalShortTimerSec:I

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 1323
    goto :goto_0

    .line 1325
    :cond_9
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_a

    move v1, v2

    .line 1326
    goto :goto_0

    .line 1328
    :cond_a
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalPolicy:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalPolicy:I

    if-eq v3, v4, :cond_b

    move v1, v2

    .line 1329
    goto :goto_0

    .line 1331
    :cond_b
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseToken:Ljava/lang/String;

    if-nez v3, :cond_c

    .line 1332
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseToken:Ljava/lang/String;

    if-eqz v3, :cond_d

    move v1, v2

    .line 1333
    goto :goto_0

    .line 1335
    :cond_c
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseToken:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseToken:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    move v1, v2

    .line 1336
    goto :goto_0

    .line 1338
    :cond_d
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseFormatType:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseFormatType:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 1339
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/16 v6, 0x20

    .line 1346
    const/16 v0, 0x11

    .line 1347
    .local v0, "result":I
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->isPreferred:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 1348
    mul-int/lit8 v1, v0, 0x1f

    iget v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType:I

    add-int v0, v1, v4

    .line 1349
    mul-int/lit8 v1, v0, 0x1f

    iget v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseStatus:I

    add-int v0, v1, v4

    .line 1350
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->visible:Z

    if-eqz v4, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 1351
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 1353
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalLongTimerSec:I

    add-int v0, v1, v2

    .line 1354
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalShortTimerSec:I

    add-int v0, v1, v2

    .line 1355
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 1357
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalPolicy:I

    add-int v0, v1, v2

    .line 1358
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseToken:Ljava/lang/String;

    if-nez v1, :cond_2

    const/4 v1, 0x0

    :goto_2
    add-int v0, v2, v1

    .line 1360
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseFormatType:I

    add-int v0, v1, v2

    .line 1361
    return v0

    :cond_0
    move v1, v3

    .line 1347
    goto :goto_0

    :cond_1
    move v2, v3

    .line 1350
    goto :goto_1

    .line 1358
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseToken:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1206
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1458
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1459
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1463
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1464
    :sswitch_0
    return-object p0

    .line 1469
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->isPreferred:Z

    goto :goto_0

    .line 1473
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1474
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1477
    :pswitch_0
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType:I

    goto :goto_0

    .line 1483
    .end local v1    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1484
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 1492
    :pswitch_1
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseStatus:I

    goto :goto_0

    .line 1498
    .end local v1    # "value":I
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec:J

    goto :goto_0

    .line 1502
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalLongTimerSec:I

    goto :goto_0

    .line 1506
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalShortTimerSec:I

    goto :goto_0

    .line 1510
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec:J

    goto :goto_0

    .line 1514
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseToken:Ljava/lang/String;

    goto :goto_0

    .line 1518
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1519
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_2

    goto :goto_0

    .line 1522
    :pswitch_2
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseFormatType:I

    goto :goto_0

    .line 1528
    .end local v1    # "value":I
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1529
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_3

    goto :goto_0

    .line 1533
    :pswitch_3
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalPolicy:I

    goto :goto_0

    .line 1539
    .end local v1    # "value":I
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->visible:Z

    goto :goto_0

    .line 1459
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch

    .line 1474
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 1484
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 1519
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 1529
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 8
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    .line 1367
    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->isPreferred:Z

    if-eqz v0, :cond_0

    .line 1368
    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->isPreferred:Z

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1370
    :cond_0
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType:I

    if-eq v0, v4, :cond_1

    .line 1371
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1373
    :cond_1
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseStatus:I

    if-eqz v0, :cond_2

    .line 1374
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseStatus:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1376
    :cond_2
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_3

    .line 1377
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1379
    :cond_3
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalLongTimerSec:I

    if-eqz v0, :cond_4

    .line 1380
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalLongTimerSec:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1382
    :cond_4
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalShortTimerSec:I

    if-eqz v0, :cond_5

    .line 1383
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalShortTimerSec:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1385
    :cond_5
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_6

    .line 1386
    const/4 v0, 0x7

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1388
    :cond_6
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseToken:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1389
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseToken:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1391
    :cond_7
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseFormatType:I

    if-eq v0, v4, :cond_8

    .line 1392
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseFormatType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1394
    :cond_8
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalPolicy:I

    if-eq v0, v4, :cond_9

    .line 1395
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalPolicy:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1397
    :cond_9
    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->visible:Z

    if-eqz v0, :cond_a

    .line 1398
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->visible:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1400
    :cond_a
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1401
    return-void
.end method
