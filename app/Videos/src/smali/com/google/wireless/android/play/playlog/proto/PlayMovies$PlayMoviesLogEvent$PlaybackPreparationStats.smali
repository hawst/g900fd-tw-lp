.class public final Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayMovies.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlaybackPreparationStats"
.end annotation


# instance fields
.field public buildRenderers:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

.field public getKeyRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

.field public localPlayerLoad:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

.field public possiblyInaccurate:Z

.field public provideKeyResponse:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

.field public purchasesRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

.field public trailerAssetsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

.field public videoGetRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

.field public videoStreamsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4244
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4245
    invoke-virtual {p0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    .line 4246
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4249
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->possiblyInaccurate:Z

    .line 4250
    iput-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoGetRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    .line 4251
    iput-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoStreamsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    .line 4252
    iput-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->purchasesRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    .line 4253
    iput-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->trailerAssetsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    .line 4254
    iput-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->localPlayerLoad:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    .line 4255
    iput-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->buildRenderers:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    .line 4256
    iput-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->getKeyRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    .line 4257
    iput-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->provideKeyResponse:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    .line 4258
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->cachedSize:I

    .line 4259
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 4407
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4408
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->possiblyInaccurate:Z

    if-eqz v1, :cond_0

    .line 4409
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->possiblyInaccurate:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4412
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoGetRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v1, :cond_1

    .line 4413
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoGetRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4416
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoStreamsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v1, :cond_2

    .line 4417
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoStreamsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4420
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->purchasesRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v1, :cond_3

    .line 4421
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->purchasesRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4424
    :cond_3
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->trailerAssetsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v1, :cond_4

    .line 4425
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->trailerAssetsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4428
    :cond_4
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->localPlayerLoad:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v1, :cond_5

    .line 4429
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->localPlayerLoad:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4432
    :cond_5
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->buildRenderers:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v1, :cond_6

    .line 4433
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->buildRenderers:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4436
    :cond_6
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->getKeyRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v1, :cond_7

    .line 4437
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->getKeyRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4440
    :cond_7
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->provideKeyResponse:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v1, :cond_8

    .line 4441
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->provideKeyResponse:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4444
    :cond_8
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 4264
    if-ne p1, p0, :cond_1

    .line 4346
    :cond_0
    :goto_0
    return v1

    .line 4267
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    if-nez v3, :cond_2

    move v1, v2

    .line 4268
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 4270
    check-cast v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    .line 4271
    .local v0, "other":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;
    iget-boolean v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->possiblyInaccurate:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->possiblyInaccurate:Z

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 4272
    goto :goto_0

    .line 4274
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoGetRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v3, :cond_4

    .line 4275
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoGetRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v3, :cond_5

    move v1, v2

    .line 4276
    goto :goto_0

    .line 4279
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoGetRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoGetRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 4280
    goto :goto_0

    .line 4283
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoStreamsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v3, :cond_6

    .line 4284
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoStreamsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v3, :cond_7

    move v1, v2

    .line 4285
    goto :goto_0

    .line 4288
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoStreamsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoStreamsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 4289
    goto :goto_0

    .line 4292
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->purchasesRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v3, :cond_8

    .line 4293
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->purchasesRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v3, :cond_9

    move v1, v2

    .line 4294
    goto :goto_0

    .line 4297
    :cond_8
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->purchasesRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->purchasesRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 4298
    goto :goto_0

    .line 4301
    :cond_9
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->trailerAssetsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v3, :cond_a

    .line 4302
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->trailerAssetsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v3, :cond_b

    move v1, v2

    .line 4303
    goto :goto_0

    .line 4306
    :cond_a
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->trailerAssetsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->trailerAssetsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    move v1, v2

    .line 4307
    goto :goto_0

    .line 4310
    :cond_b
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->localPlayerLoad:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v3, :cond_c

    .line 4311
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->localPlayerLoad:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v3, :cond_d

    move v1, v2

    .line 4312
    goto :goto_0

    .line 4315
    :cond_c
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->localPlayerLoad:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->localPlayerLoad:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    move v1, v2

    .line 4316
    goto :goto_0

    .line 4319
    :cond_d
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->buildRenderers:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v3, :cond_e

    .line 4320
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->buildRenderers:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v3, :cond_f

    move v1, v2

    .line 4321
    goto/16 :goto_0

    .line 4324
    :cond_e
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->buildRenderers:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->buildRenderers:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    move v1, v2

    .line 4325
    goto/16 :goto_0

    .line 4328
    :cond_f
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->getKeyRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v3, :cond_10

    .line 4329
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->getKeyRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v3, :cond_11

    move v1, v2

    .line 4330
    goto/16 :goto_0

    .line 4333
    :cond_10
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->getKeyRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->getKeyRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    move v1, v2

    .line 4334
    goto/16 :goto_0

    .line 4337
    :cond_11
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->provideKeyResponse:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v3, :cond_12

    .line 4338
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->provideKeyResponse:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v3, :cond_0

    move v1, v2

    .line 4339
    goto/16 :goto_0

    .line 4342
    :cond_12
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->provideKeyResponse:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->provideKeyResponse:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 4343
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 4351
    const/16 v0, 0x11

    .line 4352
    .local v0, "result":I
    iget-boolean v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->possiblyInaccurate:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x4cf

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 4353
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoGetRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 4355
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoStreamsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 4357
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->purchasesRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v3, v1

    .line 4359
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->trailerAssetsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    add-int v0, v3, v1

    .line 4361
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->localPlayerLoad:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    add-int v0, v3, v1

    .line 4363
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->buildRenderers:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v1, :cond_6

    move v1, v2

    :goto_6
    add-int v0, v3, v1

    .line 4365
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->getKeyRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v1, :cond_7

    move v1, v2

    :goto_7
    add-int v0, v3, v1

    .line 4367
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->provideKeyResponse:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v3, :cond_8

    :goto_8
    add-int v0, v1, v2

    .line 4369
    return v0

    .line 4352
    :cond_0
    const/16 v1, 0x4d5

    goto :goto_0

    .line 4353
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoGetRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->hashCode()I

    move-result v1

    goto :goto_1

    .line 4355
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoStreamsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->hashCode()I

    move-result v1

    goto :goto_2

    .line 4357
    :cond_3
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->purchasesRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->hashCode()I

    move-result v1

    goto :goto_3

    .line 4359
    :cond_4
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->trailerAssetsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->hashCode()I

    move-result v1

    goto :goto_4

    .line 4361
    :cond_5
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->localPlayerLoad:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->hashCode()I

    move-result v1

    goto :goto_5

    .line 4363
    :cond_6
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->buildRenderers:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->hashCode()I

    move-result v1

    goto :goto_6

    .line 4365
    :cond_7
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->getKeyRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->hashCode()I

    move-result v1

    goto :goto_7

    .line 4367
    :cond_8
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->provideKeyResponse:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->hashCode()I

    move-result v2

    goto :goto_8
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4200
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4452
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4453
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4457
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4458
    :sswitch_0
    return-object p0

    .line 4463
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->possiblyInaccurate:Z

    goto :goto_0

    .line 4467
    :sswitch_2
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoGetRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v1, :cond_1

    .line 4468
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoGetRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    .line 4470
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoGetRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 4474
    :sswitch_3
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoStreamsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v1, :cond_2

    .line 4475
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoStreamsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    .line 4477
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoStreamsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 4481
    :sswitch_4
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->purchasesRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v1, :cond_3

    .line 4482
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->purchasesRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    .line 4484
    :cond_3
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->purchasesRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 4488
    :sswitch_5
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->trailerAssetsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v1, :cond_4

    .line 4489
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->trailerAssetsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    .line 4491
    :cond_4
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->trailerAssetsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 4495
    :sswitch_6
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->localPlayerLoad:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v1, :cond_5

    .line 4496
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->localPlayerLoad:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    .line 4498
    :cond_5
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->localPlayerLoad:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 4502
    :sswitch_7
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->buildRenderers:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v1, :cond_6

    .line 4503
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->buildRenderers:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    .line 4505
    :cond_6
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->buildRenderers:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 4509
    :sswitch_8
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->getKeyRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v1, :cond_7

    .line 4510
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->getKeyRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    .line 4512
    :cond_7
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->getKeyRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 4516
    :sswitch_9
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->provideKeyResponse:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-nez v1, :cond_8

    .line 4517
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->provideKeyResponse:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    .line 4519
    :cond_8
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->provideKeyResponse:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 4453
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4375
    iget-boolean v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->possiblyInaccurate:Z

    if-eqz v0, :cond_0

    .line 4376
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->possiblyInaccurate:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 4378
    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoGetRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v0, :cond_1

    .line 4379
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoGetRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4381
    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoStreamsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v0, :cond_2

    .line 4382
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoStreamsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4384
    :cond_2
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->purchasesRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v0, :cond_3

    .line 4385
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->purchasesRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4387
    :cond_3
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->trailerAssetsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v0, :cond_4

    .line 4388
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->trailerAssetsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4390
    :cond_4
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->localPlayerLoad:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v0, :cond_5

    .line 4391
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->localPlayerLoad:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4393
    :cond_5
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->buildRenderers:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v0, :cond_6

    .line 4394
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->buildRenderers:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4396
    :cond_6
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->getKeyRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v0, :cond_7

    .line 4397
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->getKeyRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4399
    :cond_7
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->provideKeyResponse:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    if-eqz v0, :cond_8

    .line 4400
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->provideKeyResponse:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4402
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4403
    return-void
.end method
