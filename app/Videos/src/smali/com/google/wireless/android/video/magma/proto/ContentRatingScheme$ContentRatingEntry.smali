.class public final Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ContentRatingScheme.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContentRatingEntry"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;


# instance fields
.field public contentRatingId:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public shortName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 36
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->clear()Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    .line 37
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;
    .locals 2

    .prologue
    .line 15
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    if-nez v0, :cond_1

    .line 16
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 18
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    if-nez v0, :cond_0

    .line 19
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    .line 21
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    return-object v0

    .line 21
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;
    .locals 1

    .prologue
    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->name:Ljava/lang/String;

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->contentRatingId:Ljava/lang/String;

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->shortName:Ljava/lang/String;

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->cachedSize:I

    .line 44
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 109
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 110
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 111
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 114
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->contentRatingId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 115
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->contentRatingId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->shortName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 119
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->shortName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 122
    :cond_2
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    if-ne p1, p0, :cond_1

    .line 77
    :cond_0
    :goto_0
    return v1

    .line 52
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    if-nez v3, :cond_2

    move v1, v2

    .line 53
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 55
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    .line 56
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->name:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 57
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->name:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 58
    goto :goto_0

    .line 60
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 61
    goto :goto_0

    .line 63
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->contentRatingId:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 64
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->contentRatingId:Ljava/lang/String;

    if-eqz v3, :cond_6

    move v1, v2

    .line 65
    goto :goto_0

    .line 67
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->contentRatingId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->contentRatingId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 68
    goto :goto_0

    .line 70
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->shortName:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 71
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->shortName:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 72
    goto :goto_0

    .line 74
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->shortName:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->shortName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 75
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 82
    const/16 v0, 0x11

    .line 83
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->name:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 85
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->contentRatingId:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 87
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->shortName:Ljava/lang/String;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 89
    return v0

    .line 83
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 85
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->contentRatingId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 87
    :cond_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->shortName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 130
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 131
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 135
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 136
    :sswitch_0
    return-object p0

    .line 141
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->name:Ljava/lang/String;

    goto :goto_0

    .line 145
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->contentRatingId:Ljava/lang/String;

    goto :goto_0

    .line 149
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->shortName:Ljava/lang/String;

    goto :goto_0

    .line 131
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->contentRatingId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 99
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->contentRatingId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->shortName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 102
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->shortName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 104
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 105
    return-void
.end method
