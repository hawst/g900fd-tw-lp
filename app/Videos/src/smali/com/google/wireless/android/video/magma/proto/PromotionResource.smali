.class public final Lcom/google/wireless/android/video/magma/proto/PromotionResource;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PromotionResource.java"


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/PromotionResource;


# instance fields
.field public asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

.field public promotionCode:Ljava/lang/String;

.field public promotionType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 38
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->clear()Lcom/google/wireless/android/video/magma/proto/PromotionResource;

    .line 39
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/PromotionResource;
    .locals 2

    .prologue
    .line 17
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/PromotionResource;

    if-nez v0, :cond_1

    .line 18
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 20
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/PromotionResource;

    if-nez v0, :cond_0

    .line 21
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/PromotionResource;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/PromotionResource;

    .line 23
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/PromotionResource;

    return-object v0

    .line 23
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/PromotionResource;
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionType:I

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionCode:Ljava/lang/String;

    .line 44
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 45
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->cachedSize:I

    .line 46
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 108
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 109
    .local v2, "size":I
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionType:I

    if-eq v3, v4, :cond_0

    .line 110
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionType:I

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 113
    :cond_0
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionCode:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 114
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionCode:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 117
    :cond_1
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 118
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 119
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    aget-object v0, v3, v1

    .line 120
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v0, :cond_2

    .line 121
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 118
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 126
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v1    # "i":I
    :cond_3
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    if-ne p1, p0, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v1

    .line 54
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/PromotionResource;

    if-nez v3, :cond_2

    move v1, v2

    .line 55
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 57
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;

    .line 58
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/PromotionResource;
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionType:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionType:I

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 59
    goto :goto_0

    .line 61
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionCode:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 62
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionCode:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    .line 63
    goto :goto_0

    .line 65
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionCode:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 66
    goto :goto_0

    .line 68
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 70
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 77
    const/16 v0, 0x11

    .line 78
    .local v0, "result":I
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionType:I

    add-int/lit16 v0, v1, 0x20f

    .line 79
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionCode:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 81
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 83
    return v0

    .line 79
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionCode:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/PromotionResource;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/PromotionResource;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 134
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 135
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 139
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 140
    :sswitch_0
    return-object p0

    .line 145
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 146
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 148
    :pswitch_0
    iput v4, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionType:I

    goto :goto_0

    .line 154
    .end local v4    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionCode:Ljava/lang/String;

    goto :goto_0

    .line 158
    :sswitch_3
    const/16 v6, 0x1a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 160
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-nez v6, :cond_2

    move v1, v5

    .line 161
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 163
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v1, :cond_1

    .line 164
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 166
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 167
    new-instance v6, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-direct {v6}, Lcom/google/wireless/android/video/magma/proto/AssetResource;-><init>()V

    aput-object v6, v2, v1

    .line 168
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 169
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 166
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 160
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_2
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v1, v6

    goto :goto_1

    .line 172
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_3
    new-instance v6, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-direct {v6}, Lcom/google/wireless/android/video/magma/proto/AssetResource;-><init>()V

    aput-object v6, v2, v1

    .line 173
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 174
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    goto :goto_0

    .line 135
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    .line 146
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 89
    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionType:I

    if-eq v2, v3, :cond_0

    .line 90
    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionType:I

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 92
    :cond_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionCode:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 93
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionCode:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 95
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 96
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 97
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    aget-object v0, v2, v1

    .line 98
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v0, :cond_2

    .line 99
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 96
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 103
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v1    # "i":I
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 104
    return-void
.end method
