.class public final Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayMovies.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/play/playlog/proto/PlayMovies;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayMoviesLogEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;,
        Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;,
        Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;,
        Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;,
        Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;,
        Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;,
        Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;,
        Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;,
        Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;,
        Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;,
        Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;,
        Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;,
        Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;,
        Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;,
        Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;
    }
.end annotation


# instance fields
.field public assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

.field public click:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

.field public display:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

.field public displayType:I

.field public drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

.field public error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

.field public eventSource:I

.field public eventType:I

.field public externalApiAction:I

.field public externalPage:I

.field public impression:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

.field public licenseRefreshTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

.field public licenseReleaseTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

.field public mimeType:Ljava/lang/String;

.field public networkIsConnected:Z

.field public networkSubtype:I

.field public networkType:I

.field public newVersion:I

.field public oldVersion:I

.field public pageType:I

.field public pendingFlags:I

.field public pinQuality:I

.field public pinStorage:I

.field public pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

.field public pinningTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

.field public playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

.field public playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

.field public preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

.field public premiumStatus:I

.field public previousPage:I

.field public referer:Ljava/lang/String;

.field public searchCategory:Ljava/lang/String;

.field public session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

.field public updateLastPlaybackTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

.field public updateWishlistTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

.field public usedAutocomplete:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4660
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4661
    invoke-virtual {p0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    .line 4662
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 4665
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pageType:I

    .line 4666
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->previousPage:I

    .line 4667
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventType:I

    .line 4668
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    .line 4669
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    .line 4670
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalPage:I

    .line 4671
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->searchCategory:Ljava/lang/String;

    .line 4672
    iput-boolean v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->usedAutocomplete:Z

    .line 4673
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    .line 4674
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 4675
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    .line 4676
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinQuality:I

    .line 4677
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinStorage:I

    .line 4678
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkType:I

    .line 4679
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkSubtype:I

    .line 4680
    iput-boolean v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkIsConnected:Z

    .line 4681
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->oldVersion:I

    .line 4682
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->newVersion:I

    .line 4683
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->mimeType:Ljava/lang/String;

    .line 4684
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    .line 4685
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->premiumStatus:I

    .line 4686
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->referer:Ljava/lang/String;

    .line 4687
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->display:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

    .line 4688
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->displayType:I

    .line 4689
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    .line 4690
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseReleaseTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    .line 4691
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseRefreshTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    .line 4692
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateLastPlaybackTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    .line 4693
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateWishlistTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    .line 4694
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pendingFlags:I

    .line 4695
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    .line 4696
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    .line 4697
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalApiAction:I

    .line 4698
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->impression:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    .line 4699
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->click:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    .line 4700
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    .line 4701
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->cachedSize:I

    .line 4702
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 5110
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 5111
    .local v0, "size":I
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pageType:I

    if-eqz v1, :cond_0

    .line 5112
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pageType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5115
    :cond_0
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->previousPage:I

    if-eqz v1, :cond_1

    .line 5116
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->previousPage:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5119
    :cond_1
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventType:I

    if-eqz v1, :cond_2

    .line 5120
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5123
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    if-eqz v1, :cond_3

    .line 5124
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5127
    :cond_3
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    if-eqz v1, :cond_4

    .line 5128
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5131
    :cond_4
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalPage:I

    if-eqz v1, :cond_5

    .line 5132
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalPage:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5135
    :cond_5
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->searchCategory:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 5136
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->searchCategory:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5139
    :cond_6
    iget-boolean v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->usedAutocomplete:Z

    if-eqz v1, :cond_7

    .line 5140
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->usedAutocomplete:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 5143
    :cond_7
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    if-eqz v1, :cond_8

    .line 5144
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5147
    :cond_8
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    if-eqz v1, :cond_9

    .line 5148
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5151
    :cond_9
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    if-eqz v1, :cond_a

    .line 5152
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5155
    :cond_a
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinQuality:I

    if-eqz v1, :cond_b

    .line 5156
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinQuality:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5159
    :cond_b
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinStorage:I

    if-eqz v1, :cond_c

    .line 5160
    const/16 v1, 0xd

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinStorage:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5163
    :cond_c
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkType:I

    if-eqz v1, :cond_d

    .line 5164
    const/16 v1, 0xe

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5167
    :cond_d
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkSubtype:I

    if-eqz v1, :cond_e

    .line 5168
    const/16 v1, 0xf

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkSubtype:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5171
    :cond_e
    iget-boolean v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkIsConnected:Z

    if-eqz v1, :cond_f

    .line 5172
    const/16 v1, 0x10

    iget-boolean v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkIsConnected:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 5175
    :cond_f
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->oldVersion:I

    if-eqz v1, :cond_10

    .line 5176
    const/16 v1, 0x11

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->oldVersion:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5179
    :cond_10
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->newVersion:I

    if-eqz v1, :cond_11

    .line 5180
    const/16 v1, 0x12

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->newVersion:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5183
    :cond_11
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->mimeType:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 5184
    const/16 v1, 0x13

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->mimeType:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5187
    :cond_12
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    if-eqz v1, :cond_13

    .line 5188
    const/16 v1, 0x14

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5191
    :cond_13
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->premiumStatus:I

    if-eqz v1, :cond_14

    .line 5192
    const/16 v1, 0x15

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->premiumStatus:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5195
    :cond_14
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->referer:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 5196
    const/16 v1, 0x16

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->referer:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5199
    :cond_15
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->display:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

    if-eqz v1, :cond_16

    .line 5200
    const/16 v1, 0x17

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->display:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5203
    :cond_16
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->displayType:I

    if-eqz v1, :cond_17

    .line 5204
    const/16 v1, 0x18

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->displayType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5207
    :cond_17
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-eqz v1, :cond_18

    .line 5208
    const/16 v1, 0x19

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5211
    :cond_18
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseReleaseTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-eqz v1, :cond_19

    .line 5212
    const/16 v1, 0x1a

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseReleaseTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5215
    :cond_19
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseRefreshTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-eqz v1, :cond_1a

    .line 5216
    const/16 v1, 0x1b

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseRefreshTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5219
    :cond_1a
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateLastPlaybackTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-eqz v1, :cond_1b

    .line 5220
    const/16 v1, 0x1c

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateLastPlaybackTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5223
    :cond_1b
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateWishlistTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-eqz v1, :cond_1c

    .line 5224
    const/16 v1, 0x1d

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateWishlistTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5227
    :cond_1c
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pendingFlags:I

    if-eqz v1, :cond_1d

    .line 5228
    const/16 v1, 0x1e

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pendingFlags:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5231
    :cond_1d
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    if-eqz v1, :cond_1e

    .line 5232
    const/16 v1, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5235
    :cond_1e
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    if-eqz v1, :cond_1f

    .line 5236
    const/16 v1, 0x20

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5239
    :cond_1f
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalApiAction:I

    if-eqz v1, :cond_20

    .line 5240
    const/16 v1, 0x21

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalApiAction:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5243
    :cond_20
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->impression:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    if-eqz v1, :cond_21

    .line 5244
    const/16 v1, 0x22

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->impression:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5247
    :cond_21
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->click:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    if-eqz v1, :cond_22

    .line 5248
    const/16 v1, 0x23

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->click:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5251
    :cond_22
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    if-eqz v1, :cond_23

    .line 5252
    const/16 v1, 0x24

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5255
    :cond_23
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 4707
    if-ne p1, p0, :cond_1

    .line 4930
    :cond_0
    :goto_0
    return v1

    .line 4710
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    if-nez v3, :cond_2

    move v1, v2

    .line 4711
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 4713
    check-cast v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    .line 4714
    .local v0, "other":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pageType:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pageType:I

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 4715
    goto :goto_0

    .line 4717
    :cond_3
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->previousPage:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->previousPage:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 4718
    goto :goto_0

    .line 4720
    :cond_4
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventType:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventType:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 4721
    goto :goto_0

    .line 4723
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    if-nez v3, :cond_6

    .line 4724
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    if-eqz v3, :cond_7

    move v1, v2

    .line 4725
    goto :goto_0

    .line 4728
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 4729
    goto :goto_0

    .line 4732
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    if-nez v3, :cond_8

    .line 4733
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    if-eqz v3, :cond_9

    move v1, v2

    .line 4734
    goto :goto_0

    .line 4737
    :cond_8
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 4738
    goto :goto_0

    .line 4741
    :cond_9
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalPage:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalPage:I

    if-eq v3, v4, :cond_a

    move v1, v2

    .line 4742
    goto :goto_0

    .line 4744
    :cond_a
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->searchCategory:Ljava/lang/String;

    if-nez v3, :cond_b

    .line 4745
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->searchCategory:Ljava/lang/String;

    if-eqz v3, :cond_c

    move v1, v2

    .line 4746
    goto :goto_0

    .line 4748
    :cond_b
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->searchCategory:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->searchCategory:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    move v1, v2

    .line 4749
    goto :goto_0

    .line 4751
    :cond_c
    iget-boolean v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->usedAutocomplete:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->usedAutocomplete:Z

    if-eq v3, v4, :cond_d

    move v1, v2

    .line 4752
    goto :goto_0

    .line 4754
    :cond_d
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    if-nez v3, :cond_e

    .line 4755
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    if-eqz v3, :cond_f

    move v1, v2

    .line 4756
    goto :goto_0

    .line 4759
    :cond_e
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    move v1, v2

    .line 4760
    goto/16 :goto_0

    .line 4763
    :cond_f
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    if-nez v3, :cond_10

    .line 4764
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    if-eqz v3, :cond_11

    move v1, v2

    .line 4765
    goto/16 :goto_0

    .line 4768
    :cond_10
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    move v1, v2

    .line 4769
    goto/16 :goto_0

    .line 4772
    :cond_11
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    if-nez v3, :cond_12

    .line 4773
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    if-eqz v3, :cond_13

    move v1, v2

    .line 4774
    goto/16 :goto_0

    .line 4777
    :cond_12
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    move v1, v2

    .line 4778
    goto/16 :goto_0

    .line 4781
    :cond_13
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinQuality:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinQuality:I

    if-eq v3, v4, :cond_14

    move v1, v2

    .line 4782
    goto/16 :goto_0

    .line 4784
    :cond_14
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinStorage:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinStorage:I

    if-eq v3, v4, :cond_15

    move v1, v2

    .line 4785
    goto/16 :goto_0

    .line 4787
    :cond_15
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkType:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkType:I

    if-eq v3, v4, :cond_16

    move v1, v2

    .line 4788
    goto/16 :goto_0

    .line 4790
    :cond_16
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkSubtype:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkSubtype:I

    if-eq v3, v4, :cond_17

    move v1, v2

    .line 4791
    goto/16 :goto_0

    .line 4793
    :cond_17
    iget-boolean v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkIsConnected:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkIsConnected:Z

    if-eq v3, v4, :cond_18

    move v1, v2

    .line 4794
    goto/16 :goto_0

    .line 4796
    :cond_18
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->oldVersion:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->oldVersion:I

    if-eq v3, v4, :cond_19

    move v1, v2

    .line 4797
    goto/16 :goto_0

    .line 4799
    :cond_19
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->newVersion:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->newVersion:I

    if-eq v3, v4, :cond_1a

    move v1, v2

    .line 4800
    goto/16 :goto_0

    .line 4802
    :cond_1a
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->mimeType:Ljava/lang/String;

    if-nez v3, :cond_1b

    .line 4803
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->mimeType:Ljava/lang/String;

    if-eqz v3, :cond_1c

    move v1, v2

    .line 4804
    goto/16 :goto_0

    .line 4806
    :cond_1b
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->mimeType:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->mimeType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1c

    move v1, v2

    .line 4807
    goto/16 :goto_0

    .line 4809
    :cond_1c
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    if-eq v3, v4, :cond_1d

    move v1, v2

    .line 4810
    goto/16 :goto_0

    .line 4812
    :cond_1d
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->premiumStatus:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->premiumStatus:I

    if-eq v3, v4, :cond_1e

    move v1, v2

    .line 4813
    goto/16 :goto_0

    .line 4815
    :cond_1e
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->referer:Ljava/lang/String;

    if-nez v3, :cond_1f

    .line 4816
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->referer:Ljava/lang/String;

    if-eqz v3, :cond_20

    move v1, v2

    .line 4817
    goto/16 :goto_0

    .line 4819
    :cond_1f
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->referer:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->referer:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_20

    move v1, v2

    .line 4820
    goto/16 :goto_0

    .line 4822
    :cond_20
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->display:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

    if-nez v3, :cond_21

    .line 4823
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->display:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

    if-eqz v3, :cond_22

    move v1, v2

    .line 4824
    goto/16 :goto_0

    .line 4827
    :cond_21
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->display:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->display:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_22

    move v1, v2

    .line 4828
    goto/16 :goto_0

    .line 4831
    :cond_22
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->displayType:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->displayType:I

    if-eq v3, v4, :cond_23

    move v1, v2

    .line 4832
    goto/16 :goto_0

    .line 4834
    :cond_23
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-nez v3, :cond_24

    .line 4835
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-eqz v3, :cond_25

    move v1, v2

    .line 4836
    goto/16 :goto_0

    .line 4839
    :cond_24
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_25

    move v1, v2

    .line 4840
    goto/16 :goto_0

    .line 4843
    :cond_25
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseReleaseTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-nez v3, :cond_26

    .line 4844
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseReleaseTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-eqz v3, :cond_27

    move v1, v2

    .line 4845
    goto/16 :goto_0

    .line 4848
    :cond_26
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseReleaseTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseReleaseTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_27

    move v1, v2

    .line 4849
    goto/16 :goto_0

    .line 4852
    :cond_27
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseRefreshTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-nez v3, :cond_28

    .line 4853
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseRefreshTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-eqz v3, :cond_29

    move v1, v2

    .line 4854
    goto/16 :goto_0

    .line 4857
    :cond_28
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseRefreshTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseRefreshTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_29

    move v1, v2

    .line 4858
    goto/16 :goto_0

    .line 4861
    :cond_29
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateLastPlaybackTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-nez v3, :cond_2a

    .line 4862
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateLastPlaybackTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-eqz v3, :cond_2b

    move v1, v2

    .line 4863
    goto/16 :goto_0

    .line 4866
    :cond_2a
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateLastPlaybackTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateLastPlaybackTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2b

    move v1, v2

    .line 4867
    goto/16 :goto_0

    .line 4870
    :cond_2b
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateWishlistTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-nez v3, :cond_2c

    .line 4871
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateWishlistTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-eqz v3, :cond_2d

    move v1, v2

    .line 4872
    goto/16 :goto_0

    .line 4875
    :cond_2c
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateWishlistTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateWishlistTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2d

    move v1, v2

    .line 4876
    goto/16 :goto_0

    .line 4879
    :cond_2d
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pendingFlags:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pendingFlags:I

    if-eq v3, v4, :cond_2e

    move v1, v2

    .line 4880
    goto/16 :goto_0

    .line 4882
    :cond_2e
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    if-nez v3, :cond_2f

    .line 4883
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    if-eqz v3, :cond_30

    move v1, v2

    .line 4884
    goto/16 :goto_0

    .line 4887
    :cond_2f
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_30

    move v1, v2

    .line 4888
    goto/16 :goto_0

    .line 4891
    :cond_30
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    if-nez v3, :cond_31

    .line 4892
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    if-eqz v3, :cond_32

    move v1, v2

    .line 4893
    goto/16 :goto_0

    .line 4896
    :cond_31
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_32

    move v1, v2

    .line 4897
    goto/16 :goto_0

    .line 4900
    :cond_32
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalApiAction:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalApiAction:I

    if-eq v3, v4, :cond_33

    move v1, v2

    .line 4901
    goto/16 :goto_0

    .line 4903
    :cond_33
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->impression:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    if-nez v3, :cond_34

    .line 4904
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->impression:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    if-eqz v3, :cond_35

    move v1, v2

    .line 4905
    goto/16 :goto_0

    .line 4908
    :cond_34
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->impression:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->impression:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_35

    move v1, v2

    .line 4909
    goto/16 :goto_0

    .line 4912
    :cond_35
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->click:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    if-nez v3, :cond_36

    .line 4913
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->click:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    if-eqz v3, :cond_37

    move v1, v2

    .line 4914
    goto/16 :goto_0

    .line 4917
    :cond_36
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->click:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->click:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_37

    move v1, v2

    .line 4918
    goto/16 :goto_0

    .line 4921
    :cond_37
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    if-nez v3, :cond_38

    .line 4922
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    if-eqz v3, :cond_0

    move v1, v2

    .line 4923
    goto/16 :goto_0

    .line 4926
    :cond_38
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 4927
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    const/4 v2, 0x0

    .line 4935
    const/16 v0, 0x11

    .line 4936
    .local v0, "result":I
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pageType:I

    add-int/lit16 v0, v1, 0x20f

    .line 4937
    mul-int/lit8 v1, v0, 0x1f

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->previousPage:I

    add-int v0, v1, v5

    .line 4938
    mul-int/lit8 v1, v0, 0x1f

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventType:I

    add-int v0, v1, v5

    .line 4939
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v5, v1

    .line 4941
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v5, v1

    .line 4943
    mul-int/lit8 v1, v0, 0x1f

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalPage:I

    add-int v0, v1, v5

    .line 4944
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->searchCategory:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v5, v1

    .line 4946
    mul-int/lit8 v5, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->usedAutocomplete:Z

    if-eqz v1, :cond_3

    move v1, v3

    :goto_3
    add-int v0, v5, v1

    .line 4947
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    add-int v0, v5, v1

    .line 4949
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    add-int v0, v5, v1

    .line 4951
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    if-nez v1, :cond_6

    move v1, v2

    :goto_6
    add-int v0, v5, v1

    .line 4953
    mul-int/lit8 v1, v0, 0x1f

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinQuality:I

    add-int v0, v1, v5

    .line 4954
    mul-int/lit8 v1, v0, 0x1f

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinStorage:I

    add-int v0, v1, v5

    .line 4955
    mul-int/lit8 v1, v0, 0x1f

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkType:I

    add-int v0, v1, v5

    .line 4956
    mul-int/lit8 v1, v0, 0x1f

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkSubtype:I

    add-int v0, v1, v5

    .line 4957
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkIsConnected:Z

    if-eqz v5, :cond_7

    :goto_7
    add-int v0, v1, v3

    .line 4958
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->oldVersion:I

    add-int v0, v1, v3

    .line 4959
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->newVersion:I

    add-int v0, v1, v3

    .line 4960
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->mimeType:Ljava/lang/String;

    if-nez v1, :cond_8

    move v1, v2

    :goto_8
    add-int v0, v3, v1

    .line 4962
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    add-int v0, v1, v3

    .line 4963
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->premiumStatus:I

    add-int v0, v1, v3

    .line 4964
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->referer:Ljava/lang/String;

    if-nez v1, :cond_9

    move v1, v2

    :goto_9
    add-int v0, v3, v1

    .line 4966
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->display:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

    if-nez v1, :cond_a

    move v1, v2

    :goto_a
    add-int v0, v3, v1

    .line 4968
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->displayType:I

    add-int v0, v1, v3

    .line 4969
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-nez v1, :cond_b

    move v1, v2

    :goto_b
    add-int v0, v3, v1

    .line 4971
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseReleaseTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-nez v1, :cond_c

    move v1, v2

    :goto_c
    add-int v0, v3, v1

    .line 4973
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseRefreshTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-nez v1, :cond_d

    move v1, v2

    :goto_d
    add-int v0, v3, v1

    .line 4975
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateLastPlaybackTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-nez v1, :cond_e

    move v1, v2

    :goto_e
    add-int v0, v3, v1

    .line 4977
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateWishlistTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-nez v1, :cond_f

    move v1, v2

    :goto_f
    add-int v0, v3, v1

    .line 4979
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pendingFlags:I

    add-int v0, v1, v3

    .line 4980
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    if-nez v1, :cond_10

    move v1, v2

    :goto_10
    add-int v0, v3, v1

    .line 4982
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    if-nez v1, :cond_11

    move v1, v2

    :goto_11
    add-int v0, v3, v1

    .line 4984
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalApiAction:I

    add-int v0, v1, v3

    .line 4985
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->impression:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    if-nez v1, :cond_12

    move v1, v2

    :goto_12
    add-int v0, v3, v1

    .line 4987
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->click:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    if-nez v1, :cond_13

    move v1, v2

    :goto_13
    add-int v0, v3, v1

    .line 4989
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    if-nez v3, :cond_14

    :goto_14
    add-int v0, v1, v2

    .line 4991
    return v0

    .line 4939
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;->hashCode()I

    move-result v1

    goto/16 :goto_0

    .line 4941
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;->hashCode()I

    move-result v1

    goto/16 :goto_1

    .line 4944
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->searchCategory:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_2

    :cond_3
    move v1, v4

    .line 4946
    goto/16 :goto_3

    .line 4947
    :cond_4
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->hashCode()I

    move-result v1

    goto/16 :goto_4

    .line 4949
    :cond_5
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->hashCode()I

    move-result v1

    goto/16 :goto_5

    .line 4951
    :cond_6
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->hashCode()I

    move-result v1

    goto/16 :goto_6

    :cond_7
    move v3, v4

    .line 4957
    goto/16 :goto_7

    .line 4960
    :cond_8
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->mimeType:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_8

    .line 4964
    :cond_9
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->referer:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_9

    .line 4966
    :cond_a
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->display:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;->hashCode()I

    move-result v1

    goto/16 :goto_a

    .line 4969
    :cond_b
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->hashCode()I

    move-result v1

    goto/16 :goto_b

    .line 4971
    :cond_c
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseReleaseTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->hashCode()I

    move-result v1

    goto/16 :goto_c

    .line 4973
    :cond_d
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseRefreshTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->hashCode()I

    move-result v1

    goto/16 :goto_d

    .line 4975
    :cond_e
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateLastPlaybackTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->hashCode()I

    move-result v1

    goto/16 :goto_e

    .line 4977
    :cond_f
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateWishlistTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->hashCode()I

    move-result v1

    goto/16 :goto_f

    .line 4980
    :cond_10
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;->hashCode()I

    move-result v1

    goto/16 :goto_10

    .line 4982
    :cond_11
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;->hashCode()I

    move-result v1

    goto/16 :goto_11

    .line 4985
    :cond_12
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->impression:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->hashCode()I

    move-result v1

    goto/16 :goto_12

    .line 4987
    :cond_13
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->click:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;->hashCode()I

    move-result v1

    goto/16 :goto_13

    .line 4989
    :cond_14
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    invoke-virtual {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->hashCode()I

    move-result v2

    goto/16 :goto_14
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5263
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 5264
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 5268
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5269
    :sswitch_0
    return-object p0

    .line 5274
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 5275
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 5292
    :pswitch_0
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pageType:I

    goto :goto_0

    .line 5298
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 5299
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 5316
    :pswitch_1
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->previousPage:I

    goto :goto_0

    .line 5322
    .end local v1    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 5323
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_2

    goto :goto_0

    .line 5363
    :pswitch_2
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventType:I

    goto :goto_0

    .line 5369
    .end local v1    # "value":I
    :sswitch_4
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    if-nez v2, :cond_1

    .line 5370
    new-instance v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    invoke-direct {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    .line 5372
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5376
    :sswitch_5
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    if-nez v2, :cond_2

    .line 5377
    new-instance v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    invoke-direct {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    .line 5379
    :cond_2
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5383
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 5384
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_3

    goto :goto_0

    .line 5393
    :pswitch_3
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalPage:I

    goto :goto_0

    .line 5399
    .end local v1    # "value":I
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->searchCategory:Ljava/lang/String;

    goto :goto_0

    .line 5403
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->usedAutocomplete:Z

    goto :goto_0

    .line 5407
    :sswitch_9
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    if-nez v2, :cond_3

    .line 5408
    new-instance v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    invoke-direct {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    .line 5410
    :cond_3
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5414
    :sswitch_a
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    if-nez v2, :cond_4

    .line 5415
    new-instance v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    invoke-direct {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 5417
    :cond_4
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5421
    :sswitch_b
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    if-nez v2, :cond_5

    .line 5422
    new-instance v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    invoke-direct {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    .line 5424
    :cond_5
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5428
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinQuality:I

    goto/16 :goto_0

    .line 5432
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinStorage:I

    goto/16 :goto_0

    .line 5436
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 5437
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_4

    goto/16 :goto_0

    .line 5449
    :pswitch_4
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkType:I

    goto/16 :goto_0

    .line 5455
    .end local v1    # "value":I
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkSubtype:I

    goto/16 :goto_0

    .line 5459
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkIsConnected:Z

    goto/16 :goto_0

    .line 5463
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->oldVersion:I

    goto/16 :goto_0

    .line 5467
    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->newVersion:I

    goto/16 :goto_0

    .line 5471
    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->mimeType:Ljava/lang/String;

    goto/16 :goto_0

    .line 5475
    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 5476
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_5

    goto/16 :goto_0

    .line 5500
    :pswitch_5
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    goto/16 :goto_0

    .line 5506
    .end local v1    # "value":I
    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 5507
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_6

    goto/16 :goto_0

    .line 5514
    :pswitch_6
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->premiumStatus:I

    goto/16 :goto_0

    .line 5520
    .end local v1    # "value":I
    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->referer:Ljava/lang/String;

    goto/16 :goto_0

    .line 5524
    :sswitch_17
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->display:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

    if-nez v2, :cond_6

    .line 5525
    new-instance v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

    invoke-direct {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->display:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

    .line 5527
    :cond_6
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->display:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5531
    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 5532
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_7

    goto/16 :goto_0

    .line 5538
    :pswitch_7
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->displayType:I

    goto/16 :goto_0

    .line 5544
    .end local v1    # "value":I
    :sswitch_19
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-nez v2, :cond_7

    .line 5545
    new-instance v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-direct {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    .line 5547
    :cond_7
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5551
    :sswitch_1a
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseReleaseTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-nez v2, :cond_8

    .line 5552
    new-instance v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-direct {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseReleaseTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    .line 5554
    :cond_8
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseReleaseTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5558
    :sswitch_1b
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseRefreshTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-nez v2, :cond_9

    .line 5559
    new-instance v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-direct {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseRefreshTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    .line 5561
    :cond_9
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseRefreshTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5565
    :sswitch_1c
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateLastPlaybackTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-nez v2, :cond_a

    .line 5566
    new-instance v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-direct {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateLastPlaybackTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    .line 5568
    :cond_a
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateLastPlaybackTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5572
    :sswitch_1d
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateWishlistTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-nez v2, :cond_b

    .line 5573
    new-instance v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-direct {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateWishlistTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    .line 5575
    :cond_b
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateWishlistTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5579
    :sswitch_1e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pendingFlags:I

    goto/16 :goto_0

    .line 5583
    :sswitch_1f
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    if-nez v2, :cond_c

    .line 5584
    new-instance v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    invoke-direct {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    .line 5586
    :cond_c
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5590
    :sswitch_20
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    if-nez v2, :cond_d

    .line 5591
    new-instance v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    invoke-direct {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    .line 5593
    :cond_d
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5597
    :sswitch_21
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 5598
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_8

    goto/16 :goto_0

    .line 5605
    :pswitch_8
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalApiAction:I

    goto/16 :goto_0

    .line 5611
    .end local v1    # "value":I
    :sswitch_22
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->impression:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    if-nez v2, :cond_e

    .line 5612
    new-instance v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    invoke-direct {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->impression:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    .line 5614
    :cond_e
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->impression:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5618
    :sswitch_23
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->click:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    if-nez v2, :cond_f

    .line 5619
    new-instance v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    invoke-direct {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->click:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    .line 5621
    :cond_f
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->click:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5625
    :sswitch_24
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    if-nez v2, :cond_10

    .line 5626
    new-instance v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    invoke-direct {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    .line 5628
    :cond_10
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5264
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa0 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc0 -> :sswitch_18
        0xca -> :sswitch_19
        0xd2 -> :sswitch_1a
        0xda -> :sswitch_1b
        0xe2 -> :sswitch_1c
        0xea -> :sswitch_1d
        0xf0 -> :sswitch_1e
        0xfa -> :sswitch_1f
        0x102 -> :sswitch_20
        0x108 -> :sswitch_21
        0x112 -> :sswitch_22
        0x11a -> :sswitch_23
        0x122 -> :sswitch_24
    .end sparse-switch

    .line 5275
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 5299
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 5323
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 5384
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    .line 5437
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch

    .line 5476
    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch

    .line 5507
    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
    .end packed-switch

    .line 5532
    :pswitch_data_7
    .packed-switch 0x0
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
    .end packed-switch

    .line 5598
    :pswitch_data_8
    .packed-switch 0x0
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4997
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pageType:I

    if-eqz v0, :cond_0

    .line 4998
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pageType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5000
    :cond_0
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->previousPage:I

    if-eqz v0, :cond_1

    .line 5001
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->previousPage:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5003
    :cond_1
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventType:I

    if-eqz v0, :cond_2

    .line 5004
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5006
    :cond_2
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    if-eqz v0, :cond_3

    .line 5007
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5009
    :cond_3
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    if-eqz v0, :cond_4

    .line 5010
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5012
    :cond_4
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalPage:I

    if-eqz v0, :cond_5

    .line 5013
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalPage:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5015
    :cond_5
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->searchCategory:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 5016
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->searchCategory:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5018
    :cond_6
    iget-boolean v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->usedAutocomplete:Z

    if-eqz v0, :cond_7

    .line 5019
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->usedAutocomplete:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 5021
    :cond_7
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    if-eqz v0, :cond_8

    .line 5022
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5024
    :cond_8
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    if-eqz v0, :cond_9

    .line 5025
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5027
    :cond_9
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    if-eqz v0, :cond_a

    .line 5028
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5030
    :cond_a
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinQuality:I

    if-eqz v0, :cond_b

    .line 5031
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinQuality:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5033
    :cond_b
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinStorage:I

    if-eqz v0, :cond_c

    .line 5034
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinStorage:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5036
    :cond_c
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkType:I

    if-eqz v0, :cond_d

    .line 5037
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5039
    :cond_d
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkSubtype:I

    if-eqz v0, :cond_e

    .line 5040
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkSubtype:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5042
    :cond_e
    iget-boolean v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkIsConnected:Z

    if-eqz v0, :cond_f

    .line 5043
    const/16 v0, 0x10

    iget-boolean v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkIsConnected:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 5045
    :cond_f
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->oldVersion:I

    if-eqz v0, :cond_10

    .line 5046
    const/16 v0, 0x11

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->oldVersion:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5048
    :cond_10
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->newVersion:I

    if-eqz v0, :cond_11

    .line 5049
    const/16 v0, 0x12

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->newVersion:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5051
    :cond_11
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->mimeType:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 5052
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->mimeType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5054
    :cond_12
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    if-eqz v0, :cond_13

    .line 5055
    const/16 v0, 0x14

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5057
    :cond_13
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->premiumStatus:I

    if-eqz v0, :cond_14

    .line 5058
    const/16 v0, 0x15

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->premiumStatus:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5060
    :cond_14
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->referer:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 5061
    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->referer:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 5063
    :cond_15
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->display:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

    if-eqz v0, :cond_16

    .line 5064
    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->display:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5066
    :cond_16
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->displayType:I

    if-eqz v0, :cond_17

    .line 5067
    const/16 v0, 0x18

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->displayType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5069
    :cond_17
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-eqz v0, :cond_18

    .line 5070
    const/16 v0, 0x19

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5072
    :cond_18
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseReleaseTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-eqz v0, :cond_19

    .line 5073
    const/16 v0, 0x1a

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseReleaseTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5075
    :cond_19
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseRefreshTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-eqz v0, :cond_1a

    .line 5076
    const/16 v0, 0x1b

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseRefreshTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5078
    :cond_1a
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateLastPlaybackTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-eqz v0, :cond_1b

    .line 5079
    const/16 v0, 0x1c

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateLastPlaybackTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5081
    :cond_1b
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateWishlistTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-eqz v0, :cond_1c

    .line 5082
    const/16 v0, 0x1d

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateWishlistTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5084
    :cond_1c
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pendingFlags:I

    if-eqz v0, :cond_1d

    .line 5085
    const/16 v0, 0x1e

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pendingFlags:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5087
    :cond_1d
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    if-eqz v0, :cond_1e

    .line 5088
    const/16 v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5090
    :cond_1e
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    if-eqz v0, :cond_1f

    .line 5091
    const/16 v0, 0x20

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5093
    :cond_1f
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalApiAction:I

    if-eqz v0, :cond_20

    .line 5094
    const/16 v0, 0x21

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalApiAction:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5096
    :cond_20
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->impression:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    if-eqz v0, :cond_21

    .line 5097
    const/16 v0, 0x22

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->impression:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5099
    :cond_21
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->click:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    if-eqz v0, :cond_22

    .line 5100
    const/16 v0, 0x23

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->click:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5102
    :cond_22
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    if-eqz v0, :cond_23

    .line 5103
    const/16 v0, 0x24

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5105
    :cond_23
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 5106
    return-void
.end method
