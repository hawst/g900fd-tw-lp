.class public final Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ContentRatingScheme.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;
    }
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;


# instance fields
.field public contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

.field public country:[Ljava/lang/String;

.field public schemeId:Ljava/lang/String;

.field public types:[I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 195
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->clear()Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    .line 196
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
    .locals 2

    .prologue
    .line 171
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    if-nez v0, :cond_1

    .line 172
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 174
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    if-nez v0, :cond_0

    .line 175
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    .line 177
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    return-object v0

    .line 177
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
    .locals 1

    .prologue
    .line 199
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->schemeId:Ljava/lang/String;

    .line 200
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->country:[Ljava/lang/String;

    .line 201
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    .line 202
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    .line 203
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->cachedSize:I

    .line 204
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 284
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 285
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->schemeId:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 286
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->schemeId:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 289
    :cond_0
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->country:[Ljava/lang/String;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->country:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_3

    .line 290
    const/4 v0, 0x0

    .line 291
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 292
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->country:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_2

    .line 293
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->country:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 294
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 295
    add-int/lit8 v0, v0, 0x1

    .line 296
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 292
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 300
    .end local v2    # "element":Ljava/lang/String;
    :cond_2
    add-int/2addr v4, v1

    .line 301
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 303
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_3
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    array-length v5, v5

    if-lez v5, :cond_5

    .line 304
    const/4 v1, 0x0

    .line 305
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    array-length v5, v5

    if-ge v3, v5, :cond_4

    .line 306
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    aget v2, v5, v3

    .line 307
    .local v2, "element":I
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v1, v5

    .line 305
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 310
    .end local v2    # "element":I
    :cond_4
    add-int/2addr v4, v1

    .line 311
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    array-length v5, v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v4, v5

    .line 313
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_5
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    array-length v5, v5

    if-lez v5, :cond_7

    .line 314
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    array-length v5, v5

    if-ge v3, v5, :cond_7

    .line 315
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    aget-object v2, v5, v3

    .line 316
    .local v2, "element":Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;
    if-eqz v2, :cond_6

    .line 317
    const/4 v5, 0x4

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 314
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 322
    .end local v2    # "element":Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;
    .end local v3    # "i":I
    :cond_7
    return v4
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 209
    if-ne p1, p0, :cond_1

    .line 235
    :cond_0
    :goto_0
    return v1

    .line 212
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    if-nez v3, :cond_2

    move v1, v2

    .line 213
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 215
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    .line 216
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->schemeId:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 217
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->schemeId:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 218
    goto :goto_0

    .line 220
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->schemeId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->schemeId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 221
    goto :goto_0

    .line 223
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->country:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->country:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 225
    goto :goto_0

    .line 227
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 229
    goto :goto_0

    .line 231
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 233
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 240
    const/16 v0, 0x11

    .line 241
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->schemeId:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 243
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->country:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 245
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([I)I

    move-result v2

    add-int v0, v1, v2

    .line 247
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 249
    return v0

    .line 241
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->schemeId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
    .locals 17
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 330
    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v9

    .line 331
    .local v9, "tag":I
    sparse-switch v9, :sswitch_data_0

    .line 335
    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v14

    if-nez v14, :cond_0

    .line 336
    :sswitch_0
    return-object p0

    .line 341
    :sswitch_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->schemeId:Ljava/lang/String;

    goto :goto_0

    .line 345
    :sswitch_2
    const/16 v14, 0x12

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v1

    .line 347
    .local v1, "arrayLength":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->country:[Ljava/lang/String;

    if-nez v14, :cond_2

    const/4 v3, 0x0

    .line 348
    .local v3, "i":I
    :goto_1
    add-int v14, v3, v1

    new-array v7, v14, [Ljava/lang/String;

    .line 349
    .local v7, "newArray":[Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 350
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->country:[Ljava/lang/String;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 352
    :cond_1
    :goto_2
    array-length v14, v7

    add-int/lit8 v14, v14, -0x1

    if-ge v3, v14, :cond_3

    .line 353
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v7, v3

    .line 354
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 352
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 347
    .end local v3    # "i":I
    .end local v7    # "newArray":[Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->country:[Ljava/lang/String;

    array-length v3, v14

    goto :goto_1

    .line 357
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v7, v3

    .line 358
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->country:[Ljava/lang/String;

    goto :goto_0

    .line 362
    .end local v1    # "arrayLength":I
    .end local v3    # "i":I
    .end local v7    # "newArray":[Ljava/lang/String;
    :sswitch_3
    const/16 v14, 0x18

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v5

    .line 364
    .local v5, "length":I
    new-array v12, v5, [I

    .line 365
    .local v12, "validValues":[I
    const/4 v10, 0x0

    .line 366
    .local v10, "validCount":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    move v11, v10

    .end local v10    # "validCount":I
    .local v11, "validCount":I
    :goto_3
    if-ge v3, v5, :cond_5

    .line 367
    if-eqz v3, :cond_4

    .line 368
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 370
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 371
    .local v13, "value":I
    packed-switch v13, :pswitch_data_0

    :pswitch_0
    move v10, v11

    .line 366
    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    :goto_4
    add-int/lit8 v3, v3, 0x1

    move v11, v10

    .end local v10    # "validCount":I
    .restart local v11    # "validCount":I
    goto :goto_3

    .line 381
    :pswitch_1
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    aput v13, v12, v11

    goto :goto_4

    .line 385
    .end local v10    # "validCount":I
    .end local v13    # "value":I
    .restart local v11    # "validCount":I
    :cond_5
    if-eqz v11, :cond_0

    .line 386
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    if-nez v14, :cond_6

    const/4 v3, 0x0

    .line 387
    :goto_5
    if-nez v3, :cond_7

    array-length v14, v12

    if-ne v11, v14, :cond_7

    .line 388
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    goto/16 :goto_0

    .line 386
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    array-length v3, v14

    goto :goto_5

    .line 390
    :cond_7
    add-int v14, v3, v11

    new-array v7, v14, [I

    .line 391
    .local v7, "newArray":[I
    if-eqz v3, :cond_8

    .line 392
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 394
    :cond_8
    const/4 v14, 0x0

    invoke-static {v12, v14, v7, v3, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 395
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    goto/16 :goto_0

    .line 401
    .end local v3    # "i":I
    .end local v5    # "length":I
    .end local v7    # "newArray":[I
    .end local v11    # "validCount":I
    .end local v12    # "validValues":[I
    :sswitch_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 402
    .local v2, "bytes":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v6

    .line 404
    .local v6, "limit":I
    const/4 v1, 0x0

    .line 405
    .restart local v1    # "arrayLength":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v8

    .line 406
    .local v8, "startPos":I
    :goto_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_9

    .line 407
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    packed-switch v14, :pswitch_data_1

    :pswitch_2
    goto :goto_6

    .line 417
    :pswitch_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 421
    :cond_9
    if-eqz v1, :cond_d

    .line 422
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 423
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    if-nez v14, :cond_b

    const/4 v3, 0x0

    .line 424
    .restart local v3    # "i":I
    :goto_7
    add-int v14, v3, v1

    new-array v7, v14, [I

    .line 425
    .restart local v7    # "newArray":[I
    if-eqz v3, :cond_a

    .line 426
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 428
    :cond_a
    :goto_8
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_c

    .line 429
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 430
    .restart local v13    # "value":I
    packed-switch v13, :pswitch_data_2

    :pswitch_4
    goto :goto_8

    .line 440
    :pswitch_5
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    aput v13, v7, v3

    move v3, v4

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_8

    .line 423
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    .end local v13    # "value":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    array-length v3, v14

    goto :goto_7

    .line 444
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[I
    :cond_c
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    .line 446
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    :cond_d
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 450
    .end local v1    # "arrayLength":I
    .end local v2    # "bytes":I
    .end local v6    # "limit":I
    .end local v8    # "startPos":I
    :sswitch_5
    const/16 v14, 0x22

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v1

    .line 452
    .restart local v1    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    if-nez v14, :cond_f

    const/4 v3, 0x0

    .line 453
    .restart local v3    # "i":I
    :goto_9
    add-int v14, v3, v1

    new-array v7, v14, [Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    .line 455
    .local v7, "newArray":[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;
    if-eqz v3, :cond_e

    .line 456
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 458
    :cond_e
    :goto_a
    array-length v14, v7

    add-int/lit8 v14, v14, -0x1

    if-ge v3, v14, :cond_10

    .line 459
    new-instance v14, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    invoke-direct {v14}, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;-><init>()V

    aput-object v14, v7, v3

    .line 460
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 461
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 458
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    .line 452
    .end local v3    # "i":I
    .end local v7    # "newArray":[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;
    :cond_f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    array-length v3, v14

    goto :goto_9

    .line 464
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;
    :cond_10
    new-instance v14, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    invoke-direct {v14}, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;-><init>()V

    aput-object v14, v7, v3

    .line 465
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 466
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    goto/16 :goto_0

    .line 331
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
    .end sparse-switch

    .line 371
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 407
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    .line 430
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 255
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->schemeId:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 256
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->schemeId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 258
    :cond_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->country:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->country:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 259
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->country:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 260
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->country:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 261
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 262
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 259
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 266
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    array-length v2, v2

    if-lez v2, :cond_3

    .line 267
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 268
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->types:[I

    aget v3, v3, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 267
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 271
    .end local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 272
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 273
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;

    aget-object v0, v2, v1

    .line 274
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;
    if-eqz v0, :cond_4

    .line 275
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 272
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 279
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme$ContentRatingEntry;
    .end local v1    # "i":I
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 280
    return-void
.end method
