.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContentRating"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;


# instance fields
.field public contentRatingId:Ljava/lang/String;

.field public contentRatingName:Ljava/lang/String;

.field public countryCode:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 413
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 414
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->clear()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    .line 415
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;
    .locals 2

    .prologue
    .line 393
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    if-nez v0, :cond_1

    .line 394
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 396
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    if-nez v0, :cond_0

    .line 397
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    .line 399
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 401
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    return-object v0

    .line 399
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;
    .locals 1

    .prologue
    .line 418
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    .line 419
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingName:Ljava/lang/String;

    .line 420
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingId:Ljava/lang/String;

    .line 421
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->cachedSize:I

    .line 422
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 489
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 490
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_2

    .line 491
    const/4 v0, 0x0

    .line 492
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 493
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 494
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 495
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 496
    add-int/lit8 v0, v0, 0x1

    .line 497
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 493
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 501
    .end local v2    # "element":Ljava/lang/String;
    :cond_1
    add-int/2addr v4, v1

    .line 502
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 504
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_2
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 505
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 508
    :cond_3
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingId:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 509
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingId:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 512
    :cond_4
    return v4
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 427
    if-ne p1, p0, :cond_1

    .line 452
    :cond_0
    :goto_0
    return v1

    .line 430
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    if-nez v3, :cond_2

    move v1, v2

    .line 431
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 433
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    .line 434
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    move v1, v2

    .line 436
    goto :goto_0

    .line 438
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingName:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 439
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingName:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    .line 440
    goto :goto_0

    .line 442
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingName:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 443
    goto :goto_0

    .line 445
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingId:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 446
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingId:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 447
    goto :goto_0

    .line 449
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 450
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 457
    const/16 v0, 0x11

    .line 458
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 460
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingName:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 462
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingId:Ljava/lang/String;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 464
    return v0

    .line 460
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 462
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 387
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 520
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 521
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 525
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 526
    :sswitch_0
    return-object p0

    .line 531
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 533
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    if-nez v5, :cond_2

    move v1, v4

    .line 534
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 535
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 536
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 538
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 539
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 540
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 538
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 533
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_1

    .line 543
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 544
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    goto :goto_0

    .line 548
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingName:Ljava/lang/String;

    goto :goto_0

    .line 552
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingId:Ljava/lang/String;

    goto :goto_0

    .line 521
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 470
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 471
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 472
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 473
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 474
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 471
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 478
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 479
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 481
    :cond_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingId:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 482
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 484
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 485
    return-void
.end method
