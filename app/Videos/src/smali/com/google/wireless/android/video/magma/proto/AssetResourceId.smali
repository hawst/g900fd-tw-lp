.class public final Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AssetResourceId.java"


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;


# instance fields
.field public id:Ljava/lang/String;

.field public mid:Ljava/lang/String;

.field public titleEidrId:Ljava/lang/String;

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 49
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->clear()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 50
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 2

    .prologue
    .line 25
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v0, :cond_1

    .line 26
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 28
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v0, :cond_0

    .line 29
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 31
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    return-object v0

    .line 31
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 1

    .prologue
    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid:Ljava/lang/String;

    .line 55
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->titleEidrId:Ljava/lang/String;

    .line 56
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    .line 57
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->cachedSize:I

    .line 58
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 130
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 131
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 132
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    :cond_0
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    if-eq v1, v3, :cond_1

    .line 136
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 140
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 143
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->titleEidrId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 144
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->titleEidrId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 147
    :cond_3
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    if-ne p1, p0, :cond_1

    .line 94
    :cond_0
    :goto_0
    return v1

    .line 66
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v3, :cond_2

    move v1, v2

    .line 67
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 69
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 70
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 71
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 72
    goto :goto_0

    .line 74
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 75
    goto :goto_0

    .line 77
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 78
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid:Ljava/lang/String;

    if-eqz v3, :cond_6

    move v1, v2

    .line 79
    goto :goto_0

    .line 81
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 82
    goto :goto_0

    .line 84
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->titleEidrId:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 85
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->titleEidrId:Ljava/lang/String;

    if-eqz v3, :cond_8

    move v1, v2

    .line 86
    goto :goto_0

    .line 88
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->titleEidrId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->titleEidrId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 89
    goto :goto_0

    .line 91
    :cond_8
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 92
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 99
    const/16 v0, 0x11

    .line 100
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 102
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 104
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->titleEidrId:Ljava/lang/String;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 106
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    add-int v0, v1, v2

    .line 107
    return v0

    .line 100
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 102
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 104
    :cond_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->titleEidrId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 155
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 156
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 160
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 161
    :sswitch_0
    return-object p0

    .line 166
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    goto :goto_0

    .line 170
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 171
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 181
    :pswitch_1
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    goto :goto_0

    .line 187
    .end local v1    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid:Ljava/lang/String;

    goto :goto_0

    .line 191
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->titleEidrId:Ljava/lang/String;

    goto :goto_0

    .line 156
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    .line 171
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 113
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 116
    :cond_0
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    if-eq v0, v2, :cond_1

    .line 117
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 119
    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 120
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->mid:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 122
    :cond_2
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->titleEidrId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 123
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->titleEidrId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 125
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 126
    return-void
.end method
