.class public final Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "UserConfigGetResponse.java"


# instance fields
.field public resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 27
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->clear()Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;

    .line 28
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->cachedSize:I

    .line 33
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 76
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 77
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    if-eqz v1, :cond_0

    .line 78
    const/16 v1, 0x3ef

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_0
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 38
    if-ne p1, p0, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v1

    .line 41
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;

    if-nez v3, :cond_2

    move v1, v2

    .line 42
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 44
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;

    .line 45
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    if-nez v3, :cond_3

    .line 46
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    if-eqz v3, :cond_0

    move v1, v2

    .line 47
    goto :goto_0

    .line 50
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 51
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 59
    const/16 v0, 0x11

    .line 60
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 62
    return v0

    .line 60
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 90
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 94
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 95
    :sswitch_0
    return-object p0

    .line 100
    :sswitch_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    if-nez v1, :cond_1

    .line 101
    new-instance v1, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    invoke-direct {v1}, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    .line 103
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 90
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1f7a -> :sswitch_1
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    if-eqz v0, :cond_0

    .line 69
    const/16 v0, 0x3ef

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserConfigGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 71
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 72
    return-void
.end method
