.class public final Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayMovies.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BackgroundTaskCounts"
.end annotation


# instance fields
.field public activeCount:I

.field public backedOffCount:I

.field public canceledCount:I

.field public failedCount:I

.field public runningCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2895
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2896
    invoke-virtual {p0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    .line 2897
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2900
    iput v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->runningCount:I

    .line 2901
    iput v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->activeCount:I

    .line 2902
    iput v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->canceledCount:I

    .line 2903
    iput v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->failedCount:I

    .line 2904
    iput v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->backedOffCount:I

    .line 2905
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->cachedSize:I

    .line 2906
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2970
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2971
    .local v0, "size":I
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->runningCount:I

    if-eqz v1, :cond_0

    .line 2972
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->runningCount:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2975
    :cond_0
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->activeCount:I

    if-eqz v1, :cond_1

    .line 2976
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->activeCount:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2979
    :cond_1
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->canceledCount:I

    if-eqz v1, :cond_2

    .line 2980
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->canceledCount:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2983
    :cond_2
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->failedCount:I

    if-eqz v1, :cond_3

    .line 2984
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->failedCount:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2987
    :cond_3
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->backedOffCount:I

    if-eqz v1, :cond_4

    .line 2988
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->backedOffCount:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2991
    :cond_4
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2911
    if-ne p1, p0, :cond_1

    .line 2933
    :cond_0
    :goto_0
    return v1

    .line 2914
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    if-nez v3, :cond_2

    move v1, v2

    .line 2915
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 2917
    check-cast v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    .line 2918
    .local v0, "other":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->runningCount:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->runningCount:I

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 2919
    goto :goto_0

    .line 2921
    :cond_3
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->activeCount:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->activeCount:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 2922
    goto :goto_0

    .line 2924
    :cond_4
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->canceledCount:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->canceledCount:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 2925
    goto :goto_0

    .line 2927
    :cond_5
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->failedCount:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->failedCount:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 2928
    goto :goto_0

    .line 2930
    :cond_6
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->backedOffCount:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->backedOffCount:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 2931
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 2938
    const/16 v0, 0x11

    .line 2939
    .local v0, "result":I
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->runningCount:I

    add-int/lit16 v0, v1, 0x20f

    .line 2940
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->activeCount:I

    add-int v0, v1, v2

    .line 2941
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->canceledCount:I

    add-int v0, v1, v2

    .line 2942
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->failedCount:I

    add-int v0, v1, v2

    .line 2943
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->backedOffCount:I

    add-int v0, v1, v2

    .line 2944
    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2863
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2999
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3000
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3004
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3005
    :sswitch_0
    return-object p0

    .line 3010
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->runningCount:I

    goto :goto_0

    .line 3014
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->activeCount:I

    goto :goto_0

    .line 3018
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->canceledCount:I

    goto :goto_0

    .line 3022
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->failedCount:I

    goto :goto_0

    .line 3026
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->backedOffCount:I

    goto :goto_0

    .line 3000
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2950
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->runningCount:I

    if-eqz v0, :cond_0

    .line 2951
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->runningCount:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2953
    :cond_0
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->activeCount:I

    if-eqz v0, :cond_1

    .line 2954
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->activeCount:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2956
    :cond_1
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->canceledCount:I

    if-eqz v0, :cond_2

    .line 2957
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->canceledCount:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2959
    :cond_2
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->failedCount:I

    if-eqz v0, :cond_3

    .line 2960
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->failedCount:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2962
    :cond_3
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->backedOffCount:I

    if-eqz v0, :cond_4

    .line 2963
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->backedOffCount:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2965
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2966
    return-void
.end method
