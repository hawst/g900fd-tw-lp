.class public final Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayMovies.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Preference"
.end annotation


# instance fields
.field public boolValue:Z

.field public intValue:I

.field public name:Ljava/lang/String;

.field public stringValue:Ljava/lang/String;

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 644
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 645
    invoke-virtual {p0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    .line 646
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 649
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->name:Ljava/lang/String;

    .line 650
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->type:I

    .line 651
    iput-boolean v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->boolValue:Z

    .line 652
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->intValue:I

    .line 653
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->stringValue:Ljava/lang/String;

    .line 654
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->cachedSize:I

    .line 655
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 729
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 730
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 731
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 734
    :cond_0
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->type:I

    if-eqz v1, :cond_1

    .line 735
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->type:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 738
    :cond_1
    iget-boolean v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->boolValue:Z

    if-eqz v1, :cond_2

    .line 739
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->boolValue:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 742
    :cond_2
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->intValue:I

    if-eqz v1, :cond_3

    .line 743
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->intValue:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 746
    :cond_3
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->stringValue:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 747
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->stringValue:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 750
    :cond_4
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 660
    if-ne p1, p0, :cond_1

    .line 690
    :cond_0
    :goto_0
    return v1

    .line 663
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    if-nez v3, :cond_2

    move v1, v2

    .line 664
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 666
    check-cast v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    .line 667
    .local v0, "other":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->name:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 668
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->name:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 669
    goto :goto_0

    .line 671
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 672
    goto :goto_0

    .line 674
    :cond_4
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->type:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->type:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 675
    goto :goto_0

    .line 677
    :cond_5
    iget-boolean v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->boolValue:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->boolValue:Z

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 678
    goto :goto_0

    .line 680
    :cond_6
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->intValue:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->intValue:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 681
    goto :goto_0

    .line 683
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->stringValue:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 684
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->stringValue:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 685
    goto :goto_0

    .line 687
    :cond_8
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->stringValue:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->stringValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 688
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 695
    const/16 v0, 0x11

    .line 696
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->name:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 698
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->type:I

    add-int v0, v1, v3

    .line 699
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->boolValue:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x4cf

    :goto_1
    add-int v0, v3, v1

    .line 700
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->intValue:I

    add-int v0, v1, v3

    .line 701
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->stringValue:Ljava/lang/String;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 703
    return v0

    .line 696
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 699
    :cond_1
    const/16 v1, 0x4d5

    goto :goto_1

    .line 701
    :cond_2
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->stringValue:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 604
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 758
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 759
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 763
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 764
    :sswitch_0
    return-object p0

    .line 769
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->name:Ljava/lang/String;

    goto :goto_0

    .line 773
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 774
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 779
    :pswitch_0
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->type:I

    goto :goto_0

    .line 785
    .end local v1    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->boolValue:Z

    goto :goto_0

    .line 789
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->intValue:I

    goto :goto_0

    .line 793
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->stringValue:Ljava/lang/String;

    goto :goto_0

    .line 759
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    .line 774
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 709
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 710
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 712
    :cond_0
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->type:I

    if-eqz v0, :cond_1

    .line 713
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->type:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 715
    :cond_1
    iget-boolean v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->boolValue:Z

    if-eqz v0, :cond_2

    .line 716
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->boolValue:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 718
    :cond_2
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->intValue:I

    if-eqz v0, :cond_3

    .line 719
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->intValue:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 721
    :cond_3
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->stringValue:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 722
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->stringValue:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 724
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 725
    return-void
.end method
