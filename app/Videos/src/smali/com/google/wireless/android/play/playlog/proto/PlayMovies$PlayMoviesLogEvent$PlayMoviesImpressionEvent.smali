.class public final Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayMovies.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayMoviesImpressionEvent"
.end annotation


# instance fields
.field public id:J

.field public referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

.field public tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3765
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3766
    invoke-virtual {p0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    .line 3767
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;
    .locals 2

    .prologue
    .line 3770
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .line 3771
    invoke-static {}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->emptyArray()[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .line 3772
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->id:J

    .line 3773
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->cachedSize:I

    .line 3774
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    .line 3839
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 3840
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-eqz v3, :cond_0

    .line 3841
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3844
    :cond_0
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 3845
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 3846
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    aget-object v0, v3, v1

    .line 3847
    .local v0, "element":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    if-eqz v0, :cond_1

    .line 3848
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3845
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3853
    .end local v0    # "element":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    .end local v1    # "i":I
    :cond_2
    iget-wide v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->id:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    .line 3854
    const/4 v3, 0x3

    iget-wide v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->id:J

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 3857
    :cond_3
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3779
    if-ne p1, p0, :cond_1

    .line 3802
    :cond_0
    :goto_0
    return v1

    .line 3782
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    if-nez v3, :cond_2

    move v1, v2

    .line 3783
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 3785
    check-cast v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    .line 3786
    .local v0, "other":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-nez v3, :cond_3

    .line 3787
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-eqz v3, :cond_4

    move v1, v2

    .line 3788
    goto :goto_0

    .line 3791
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 3792
    goto :goto_0

    .line 3795
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 3797
    goto :goto_0

    .line 3799
    :cond_5
    iget-wide v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->id:J

    iget-wide v6, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->id:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    .line 3800
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 3807
    const/16 v0, 0x11

    .line 3808
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 3810
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 3812
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->id:J

    iget-wide v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->id:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 3814
    return v0

    .line 3808
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3739
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 3865
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3866
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3870
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3871
    :sswitch_0
    return-object p0

    .line 3876
    :sswitch_1
    iget-object v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-nez v5, :cond_1

    .line 3877
    new-instance v5, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-direct {v5}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;-><init>()V

    iput-object v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .line 3879
    :cond_1
    iget-object v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3883
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3885
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-nez v5, :cond_3

    move v1, v4

    .line 3886
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .line 3888
    .local v2, "newArray":[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    if-eqz v1, :cond_2

    .line 3889
    iget-object v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3891
    :cond_2
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 3892
    new-instance v5, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-direct {v5}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;-><init>()V

    aput-object v5, v2, v1

    .line 3893
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3894
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3891
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3885
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    :cond_3
    iget-object v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    array-length v1, v5

    goto :goto_1

    .line 3897
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    :cond_4
    new-instance v5, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-direct {v5}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;-><init>()V

    aput-object v5, v2, v1

    .line 3898
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3899
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    goto :goto_0

    .line 3903
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->id:J

    goto :goto_0

    .line 3866
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3820
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-eqz v2, :cond_0

    .line 3821
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3823
    :cond_0
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 3824
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 3825
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    aget-object v0, v2, v1

    .line 3826
    .local v0, "element":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    if-eqz v0, :cond_1

    .line 3827
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3824
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3831
    .end local v0    # "element":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    .end local v1    # "i":I
    :cond_2
    iget-wide v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->id:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 3832
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->id:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 3834
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3835
    return-void
.end method
