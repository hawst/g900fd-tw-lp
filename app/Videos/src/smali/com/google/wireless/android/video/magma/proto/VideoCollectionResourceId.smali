.class public final Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VideoCollectionResourceId.java"


# instance fields
.field public id:Ljava/lang/String;

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 38
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->clear()Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    .line 39
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;
    .locals 1

    .prologue
    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->id:Ljava/lang/String;

    .line 43
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->type:I

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->cachedSize:I

    .line 45
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 93
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 94
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->id:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 95
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->id:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_0
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->type:I

    if-eq v1, v3, :cond_1

    .line 99
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->type:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 50
    if-ne p1, p0, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v1

    .line 53
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    if-nez v3, :cond_2

    move v1, v2

    .line 54
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 56
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    .line 57
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->id:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 58
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->id:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 59
    goto :goto_0

    .line 61
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 62
    goto :goto_0

    .line 64
    :cond_4
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->type:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->type:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 65
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 72
    const/16 v0, 0x11

    .line 73
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->id:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 75
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->type:I

    add-int v0, v1, v2

    .line 76
    return v0

    .line 73
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->id:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 111
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 115
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 116
    :sswitch_0
    return-object p0

    .line 121
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->id:Ljava/lang/String;

    goto :goto_0

    .line 125
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 126
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 131
    :pswitch_0
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->type:I

    goto :goto_0

    .line 111
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    .line 126
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 82
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->id:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->id:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 85
    :cond_0
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->type:I

    if-eq v0, v2, :cond_1

    .line 86
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->type:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 88
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 89
    return-void
.end method
