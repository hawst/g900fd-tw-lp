.class public final Lcom/google/wireless/android/video/magma/proto/AssetListResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AssetListResponse.java"


# instance fields
.field public resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

.field public resourceErrors:[Lcom/google/wireless/android/video/magma/proto/VideoError;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 30
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->clear()Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .line 31
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/AssetListResponse;
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoError;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/VideoError;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resourceErrors:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    .line 35
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->cachedSize:I

    .line 37
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    .line 94
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 95
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resourceErrors:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resourceErrors:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 96
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resourceErrors:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 97
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resourceErrors:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    aget-object v0, v3, v1

    .line 98
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/VideoError;
    if-eqz v0, :cond_0

    .line 99
    const/16 v3, 0x13

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 96
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 104
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/VideoError;
    .end local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 105
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 106
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    aget-object v0, v3, v1

    .line 107
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v0, :cond_2

    .line 108
    const/16 v3, 0x3ef

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 105
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 113
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v1    # "i":I
    :cond_3
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    if-ne p1, p0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v1

    .line 45
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    if-nez v3, :cond_2

    move v1, v2

    .line 46
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 48
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .line 49
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/AssetListResponse;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resourceErrors:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resourceErrors:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    move v1, v2

    .line 51
    goto :goto_0

    .line 53
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 55
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 62
    const/16 v0, 0x11

    .line 63
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resourceErrors:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    invoke-static {v1}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 65
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 67
    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetListResponse;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 121
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 122
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 126
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 127
    :sswitch_0
    return-object p0

    .line 132
    :sswitch_1
    const/16 v5, 0x9a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 134
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resourceErrors:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    if-nez v5, :cond_2

    move v1, v4

    .line 135
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/VideoError;

    .line 137
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/VideoError;
    if-eqz v1, :cond_1

    .line 138
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resourceErrors:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 140
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 141
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/VideoError;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/VideoError;-><init>()V

    aput-object v5, v2, v1

    .line 142
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 143
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 140
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 134
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/VideoError;
    :cond_2
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resourceErrors:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    array-length v1, v5

    goto :goto_1

    .line 146
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/VideoError;
    :cond_3
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/VideoError;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/VideoError;-><init>()V

    aput-object v5, v2, v1

    .line 147
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 148
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resourceErrors:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    goto :goto_0

    .line 152
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/VideoError;
    :sswitch_2
    const/16 v5, 0x1f7a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 154
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-nez v5, :cond_5

    move v1, v4

    .line 155
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 157
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v1, :cond_4

    .line 158
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 160
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 161
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource;-><init>()V

    aput-object v5, v2, v1

    .line 162
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 163
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 160
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 154
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_5
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v1, v5

    goto :goto_3

    .line 166
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_6
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource;-><init>()V

    aput-object v5, v2, v1

    .line 167
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 168
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    goto/16 :goto_0

    .line 122
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9a -> :sswitch_1
        0x1f7a -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resourceErrors:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resourceErrors:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 74
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resourceErrors:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 75
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resourceErrors:[Lcom/google/wireless/android/video/magma/proto/VideoError;

    aget-object v0, v2, v1

    .line 76
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/VideoError;
    if-eqz v0, :cond_0

    .line 77
    const/16 v2, 0x13

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 74
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 81
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/VideoError;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 82
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 83
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    aget-object v0, v2, v1

    .line 84
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v0, :cond_2

    .line 85
    const/16 v2, 0x3ef

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 82
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 89
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v1    # "i":I
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 90
    return-void
.end method
