.class public final Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayMovies.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlaybackStats"
.end annotation


# instance fields
.field public averageVideoBandwidth:I

.field public averageVideoResolution:I

.field public earlyRebufferingCount:I

.field public earlyRebufferingTimeMillis:I

.field public firstItag:I

.field public itagsUsed:[I

.field public joiningTimeMillis:I

.field public networkTypesUsed:[I

.field public playingTimeMillis:I

.field public secondItag:I

.field public secondItagSelectionTimeMillis:I

.field public totalDroppedFrameCount:I

.field public totalErrorCount:I

.field public totalFailureCount:I

.field public totalRebufferingCount:I

.field public totalRebufferingTimeMillis:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1672
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1673
    invoke-virtual {p0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;

    .line 1674
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1677
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->joiningTimeMillis:I

    .line 1678
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->playingTimeMillis:I

    .line 1679
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingCount:I

    .line 1680
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingTimeMillis:I

    .line 1681
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalErrorCount:I

    .line 1682
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalFailureCount:I

    .line 1683
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalDroppedFrameCount:I

    .line 1684
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    .line 1685
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingCount:I

    .line 1686
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingTimeMillis:I

    .line 1687
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->firstItag:I

    .line 1688
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItag:I

    .line 1689
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItagSelectionTimeMillis:I

    .line 1690
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoResolution:I

    .line 1691
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoBandwidth:I

    .line 1692
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    .line 1693
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->cachedSize:I

    .line 1694
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 1843
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v3

    .line 1844
    .local v3, "size":I
    iget v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->joiningTimeMillis:I

    if-eqz v4, :cond_0

    .line 1845
    const/4 v4, 0x1

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->joiningTimeMillis:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 1848
    :cond_0
    iget v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->playingTimeMillis:I

    if-eqz v4, :cond_1

    .line 1849
    const/4 v4, 0x2

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->playingTimeMillis:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 1852
    :cond_1
    iget v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingCount:I

    if-eqz v4, :cond_2

    .line 1853
    const/4 v4, 0x3

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingCount:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 1856
    :cond_2
    iget v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingTimeMillis:I

    if-eqz v4, :cond_3

    .line 1857
    const/4 v4, 0x4

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingTimeMillis:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 1860
    :cond_3
    iget v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalErrorCount:I

    if-eqz v4, :cond_4

    .line 1861
    const/4 v4, 0x5

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalErrorCount:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 1864
    :cond_4
    iget v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalFailureCount:I

    if-eqz v4, :cond_5

    .line 1865
    const/4 v4, 0x6

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalFailureCount:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 1868
    :cond_5
    iget v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalDroppedFrameCount:I

    if-eqz v4, :cond_6

    .line 1869
    const/4 v4, 0x7

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalDroppedFrameCount:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 1872
    :cond_6
    iget-object v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    array-length v4, v4

    if-lez v4, :cond_8

    .line 1873
    const/4 v0, 0x0

    .line 1874
    .local v0, "dataSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    array-length v4, v4

    if-ge v2, v4, :cond_7

    .line 1875
    iget-object v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    aget v1, v4, v2

    .line 1876
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 1874
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1879
    .end local v1    # "element":I
    :cond_7
    add-int/2addr v3, v0

    .line 1880
    iget-object v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    .line 1882
    .end local v0    # "dataSize":I
    .end local v2    # "i":I
    :cond_8
    iget v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingCount:I

    if-eqz v4, :cond_9

    .line 1883
    const/16 v4, 0xa

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingCount:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 1886
    :cond_9
    iget v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingTimeMillis:I

    if-eqz v4, :cond_a

    .line 1887
    const/16 v4, 0xb

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingTimeMillis:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 1890
    :cond_a
    iget v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->firstItag:I

    if-eqz v4, :cond_b

    .line 1891
    const/16 v4, 0xc

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->firstItag:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 1894
    :cond_b
    iget v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItag:I

    if-eqz v4, :cond_c

    .line 1895
    const/16 v4, 0xd

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItag:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 1898
    :cond_c
    iget v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItagSelectionTimeMillis:I

    if-eqz v4, :cond_d

    .line 1899
    const/16 v4, 0xe

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItagSelectionTimeMillis:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 1902
    :cond_d
    iget v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoResolution:I

    if-eqz v4, :cond_e

    .line 1903
    const/16 v4, 0xf

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoResolution:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 1906
    :cond_e
    iget v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoBandwidth:I

    if-eqz v4, :cond_f

    .line 1907
    const/16 v4, 0x10

    iget v5, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoBandwidth:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 1910
    :cond_f
    iget-object v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    if-eqz v4, :cond_11

    iget-object v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    array-length v4, v4

    if-lez v4, :cond_11

    .line 1911
    const/4 v0, 0x0

    .line 1912
    .restart local v0    # "dataSize":I
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-object v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    array-length v4, v4

    if-ge v2, v4, :cond_10

    .line 1913
    iget-object v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    aget v1, v4, v2

    .line 1914
    .restart local v1    # "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 1912
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1917
    .end local v1    # "element":I
    :cond_10
    add-int/2addr v3, v0

    .line 1918
    iget-object v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 1920
    .end local v0    # "dataSize":I
    .end local v2    # "i":I
    :cond_11
    return v3
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1699
    if-ne p1, p0, :cond_1

    .line 1756
    :cond_0
    :goto_0
    return v1

    .line 1702
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;

    if-nez v3, :cond_2

    move v1, v2

    .line 1703
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1705
    check-cast v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;

    .line 1706
    .local v0, "other":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->joiningTimeMillis:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->joiningTimeMillis:I

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 1707
    goto :goto_0

    .line 1709
    :cond_3
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->playingTimeMillis:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->playingTimeMillis:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 1710
    goto :goto_0

    .line 1712
    :cond_4
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingCount:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingCount:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 1713
    goto :goto_0

    .line 1715
    :cond_5
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingTimeMillis:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingTimeMillis:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 1716
    goto :goto_0

    .line 1718
    :cond_6
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalErrorCount:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalErrorCount:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 1719
    goto :goto_0

    .line 1721
    :cond_7
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalFailureCount:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalFailureCount:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 1722
    goto :goto_0

    .line 1724
    :cond_8
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalDroppedFrameCount:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalDroppedFrameCount:I

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 1725
    goto :goto_0

    .line 1727
    :cond_9
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 1729
    goto :goto_0

    .line 1731
    :cond_a
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingCount:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingCount:I

    if-eq v3, v4, :cond_b

    move v1, v2

    .line 1732
    goto :goto_0

    .line 1734
    :cond_b
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingTimeMillis:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingTimeMillis:I

    if-eq v3, v4, :cond_c

    move v1, v2

    .line 1735
    goto :goto_0

    .line 1737
    :cond_c
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->firstItag:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->firstItag:I

    if-eq v3, v4, :cond_d

    move v1, v2

    .line 1738
    goto :goto_0

    .line 1740
    :cond_d
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItag:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItag:I

    if-eq v3, v4, :cond_e

    move v1, v2

    .line 1741
    goto :goto_0

    .line 1743
    :cond_e
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItagSelectionTimeMillis:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItagSelectionTimeMillis:I

    if-eq v3, v4, :cond_f

    move v1, v2

    .line 1744
    goto :goto_0

    .line 1746
    :cond_f
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoResolution:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoResolution:I

    if-eq v3, v4, :cond_10

    move v1, v2

    .line 1747
    goto :goto_0

    .line 1749
    :cond_10
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoBandwidth:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoBandwidth:I

    if-eq v3, v4, :cond_11

    move v1, v2

    .line 1750
    goto/16 :goto_0

    .line 1752
    :cond_11
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 1754
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 1761
    const/16 v0, 0x11

    .line 1762
    .local v0, "result":I
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->joiningTimeMillis:I

    add-int/lit16 v0, v1, 0x20f

    .line 1763
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->playingTimeMillis:I

    add-int v0, v1, v2

    .line 1764
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingCount:I

    add-int v0, v1, v2

    .line 1765
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingTimeMillis:I

    add-int v0, v1, v2

    .line 1766
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalErrorCount:I

    add-int v0, v1, v2

    .line 1767
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalFailureCount:I

    add-int v0, v1, v2

    .line 1768
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalDroppedFrameCount:I

    add-int v0, v1, v2

    .line 1769
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([I)I

    move-result v2

    add-int v0, v1, v2

    .line 1771
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingCount:I

    add-int v0, v1, v2

    .line 1772
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingTimeMillis:I

    add-int v0, v1, v2

    .line 1773
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->firstItag:I

    add-int v0, v1, v2

    .line 1774
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItag:I

    add-int v0, v1, v2

    .line 1775
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItagSelectionTimeMillis:I

    add-int v0, v1, v2

    .line 1776
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoResolution:I

    add-int v0, v1, v2

    .line 1777
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoBandwidth:I

    add-int v0, v1, v2

    .line 1778
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([I)I

    move-result v2

    add-int v0, v1, v2

    .line 1780
    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1607
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;
    .locals 17
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1928
    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v9

    .line 1929
    .local v9, "tag":I
    sparse-switch v9, :sswitch_data_0

    .line 1933
    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v14

    if-nez v14, :cond_0

    .line 1934
    :sswitch_0
    return-object p0

    .line 1939
    :sswitch_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->joiningTimeMillis:I

    goto :goto_0

    .line 1943
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->playingTimeMillis:I

    goto :goto_0

    .line 1947
    :sswitch_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingCount:I

    goto :goto_0

    .line 1951
    :sswitch_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingTimeMillis:I

    goto :goto_0

    .line 1955
    :sswitch_5
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalErrorCount:I

    goto :goto_0

    .line 1959
    :sswitch_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalFailureCount:I

    goto :goto_0

    .line 1963
    :sswitch_7
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalDroppedFrameCount:I

    goto :goto_0

    .line 1967
    :sswitch_8
    const/16 v14, 0x40

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v1

    .line 1969
    .local v1, "arrayLength":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    if-nez v14, :cond_2

    const/4 v3, 0x0

    .line 1970
    .local v3, "i":I
    :goto_1
    add-int v14, v3, v1

    new-array v7, v14, [I

    .line 1971
    .local v7, "newArray":[I
    if-eqz v3, :cond_1

    .line 1972
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1974
    :cond_1
    :goto_2
    array-length v14, v7

    add-int/lit8 v14, v14, -0x1

    if-ge v3, v14, :cond_3

    .line 1975
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    aput v14, v7, v3

    .line 1976
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1974
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1969
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    array-length v3, v14

    goto :goto_1

    .line 1979
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[I
    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    aput v14, v7, v3

    .line 1980
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    goto/16 :goto_0

    .line 1984
    .end local v1    # "arrayLength":I
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    :sswitch_9
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v5

    .line 1985
    .local v5, "length":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v6

    .line 1987
    .local v6, "limit":I
    const/4 v1, 0x0

    .line 1988
    .restart local v1    # "arrayLength":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v8

    .line 1989
    .local v8, "startPos":I
    :goto_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_4

    .line 1990
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    .line 1991
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1993
    :cond_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 1994
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    if-nez v14, :cond_6

    const/4 v3, 0x0

    .line 1995
    .restart local v3    # "i":I
    :goto_4
    add-int v14, v3, v1

    new-array v7, v14, [I

    .line 1996
    .restart local v7    # "newArray":[I
    if-eqz v3, :cond_5

    .line 1997
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1999
    :cond_5
    :goto_5
    array-length v14, v7

    if-ge v3, v14, :cond_7

    .line 2000
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    aput v14, v7, v3

    .line 1999
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 1994
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    array-length v3, v14

    goto :goto_4

    .line 2002
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[I
    :cond_7
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    .line 2003
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 2007
    .end local v1    # "arrayLength":I
    .end local v3    # "i":I
    .end local v5    # "length":I
    .end local v6    # "limit":I
    .end local v7    # "newArray":[I
    .end local v8    # "startPos":I
    :sswitch_a
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingCount:I

    goto/16 :goto_0

    .line 2011
    :sswitch_b
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingTimeMillis:I

    goto/16 :goto_0

    .line 2015
    :sswitch_c
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->firstItag:I

    goto/16 :goto_0

    .line 2019
    :sswitch_d
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItag:I

    goto/16 :goto_0

    .line 2023
    :sswitch_e
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItagSelectionTimeMillis:I

    goto/16 :goto_0

    .line 2027
    :sswitch_f
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoResolution:I

    goto/16 :goto_0

    .line 2031
    :sswitch_10
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoBandwidth:I

    goto/16 :goto_0

    .line 2035
    :sswitch_11
    const/16 v14, 0x88

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v5

    .line 2037
    .restart local v5    # "length":I
    new-array v12, v5, [I

    .line 2038
    .local v12, "validValues":[I
    const/4 v10, 0x0

    .line 2039
    .local v10, "validCount":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    move v11, v10

    .end local v10    # "validCount":I
    .local v11, "validCount":I
    :goto_6
    if-ge v3, v5, :cond_9

    .line 2040
    if-eqz v3, :cond_8

    .line 2041
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2043
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 2044
    .local v13, "value":I
    packed-switch v13, :pswitch_data_0

    move v10, v11

    .line 2039
    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    :goto_7
    add-int/lit8 v3, v3, 0x1

    move v11, v10

    .end local v10    # "validCount":I
    .restart local v11    # "validCount":I
    goto :goto_6

    .line 2056
    :pswitch_0
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    aput v13, v12, v11

    goto :goto_7

    .line 2060
    .end local v10    # "validCount":I
    .end local v13    # "value":I
    .restart local v11    # "validCount":I
    :cond_9
    if-eqz v11, :cond_0

    .line 2061
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    if-nez v14, :cond_a

    const/4 v3, 0x0

    .line 2062
    :goto_8
    if-nez v3, :cond_b

    array-length v14, v12

    if-ne v11, v14, :cond_b

    .line 2063
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    goto/16 :goto_0

    .line 2061
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    array-length v3, v14

    goto :goto_8

    .line 2065
    :cond_b
    add-int v14, v3, v11

    new-array v7, v14, [I

    .line 2066
    .restart local v7    # "newArray":[I
    if-eqz v3, :cond_c

    .line 2067
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2069
    :cond_c
    const/4 v14, 0x0

    invoke-static {v12, v14, v7, v3, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2070
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    goto/16 :goto_0

    .line 2076
    .end local v3    # "i":I
    .end local v5    # "length":I
    .end local v7    # "newArray":[I
    .end local v11    # "validCount":I
    .end local v12    # "validValues":[I
    :sswitch_12
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 2077
    .local v2, "bytes":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v6

    .line 2079
    .restart local v6    # "limit":I
    const/4 v1, 0x0

    .line 2080
    .restart local v1    # "arrayLength":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v8

    .line 2081
    .restart local v8    # "startPos":I
    :goto_9
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_d

    .line 2082
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    packed-switch v14, :pswitch_data_1

    goto :goto_9

    .line 2094
    :pswitch_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 2098
    :cond_d
    if-eqz v1, :cond_11

    .line 2099
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 2100
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    if-nez v14, :cond_f

    const/4 v3, 0x0

    .line 2101
    .restart local v3    # "i":I
    :goto_a
    add-int v14, v3, v1

    new-array v7, v14, [I

    .line 2102
    .restart local v7    # "newArray":[I
    if-eqz v3, :cond_e

    .line 2103
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2105
    :cond_e
    :goto_b
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_10

    .line 2106
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 2107
    .restart local v13    # "value":I
    packed-switch v13, :pswitch_data_2

    goto :goto_b

    .line 2119
    :pswitch_2
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    aput v13, v7, v3

    move v3, v4

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_b

    .line 2100
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    .end local v13    # "value":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    array-length v3, v14

    goto :goto_a

    .line 2123
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[I
    :cond_10
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    .line 2125
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    :cond_11
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 1929
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x42 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x8a -> :sswitch_12
    .end sparse-switch

    .line 2044
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 2082
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 2107
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1786
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->joiningTimeMillis:I

    if-eqz v1, :cond_0

    .line 1787
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->joiningTimeMillis:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1789
    :cond_0
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->playingTimeMillis:I

    if-eqz v1, :cond_1

    .line 1790
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->playingTimeMillis:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1792
    :cond_1
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingCount:I

    if-eqz v1, :cond_2

    .line 1793
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingCount:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1795
    :cond_2
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingTimeMillis:I

    if-eqz v1, :cond_3

    .line 1796
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingTimeMillis:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1798
    :cond_3
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalErrorCount:I

    if-eqz v1, :cond_4

    .line 1799
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalErrorCount:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1801
    :cond_4
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalFailureCount:I

    if-eqz v1, :cond_5

    .line 1802
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalFailureCount:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1804
    :cond_5
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalDroppedFrameCount:I

    if-eqz v1, :cond_6

    .line 1805
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalDroppedFrameCount:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1807
    :cond_6
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    array-length v1, v1

    if-lez v1, :cond_7

    .line 1808
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    array-length v1, v1

    if-ge v0, v1, :cond_7

    .line 1809
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1808
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1812
    .end local v0    # "i":I
    :cond_7
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingCount:I

    if-eqz v1, :cond_8

    .line 1813
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingCount:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1815
    :cond_8
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingTimeMillis:I

    if-eqz v1, :cond_9

    .line 1816
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingTimeMillis:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1818
    :cond_9
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->firstItag:I

    if-eqz v1, :cond_a

    .line 1819
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->firstItag:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1821
    :cond_a
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItag:I

    if-eqz v1, :cond_b

    .line 1822
    const/16 v1, 0xd

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItag:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1824
    :cond_b
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItagSelectionTimeMillis:I

    if-eqz v1, :cond_c

    .line 1825
    const/16 v1, 0xe

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItagSelectionTimeMillis:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1827
    :cond_c
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoResolution:I

    if-eqz v1, :cond_d

    .line 1828
    const/16 v1, 0xf

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoResolution:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1830
    :cond_d
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoBandwidth:I

    if-eqz v1, :cond_e

    .line 1831
    const/16 v1, 0x10

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoBandwidth:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1833
    :cond_e
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    array-length v1, v1

    if-lez v1, :cond_f

    .line 1834
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    array-length v1, v1

    if-ge v0, v1, :cond_f

    .line 1835
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1834
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1838
    .end local v0    # "i":I
    :cond_f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1839
    return-void
.end method
