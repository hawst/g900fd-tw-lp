.class public final Lcom/google/wireless/android/video/magma/proto/Storyboard;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Storyboard.java"


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/Storyboard;


# instance fields
.field public frameHeight:I

.field public frameWidth:I

.field public maxFramesPerColumn:I

.field public maxFramesPerRow:I

.field public numberOfFrames:I

.field public samplingIntervalMs:I

.field public totalImageBytes:J

.field public urls:[Ljava/lang/String;

.field public videoLengthMs:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 51
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/Storyboard;->clear()Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .line 52
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/Storyboard;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 55
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameWidth:I

    .line 56
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    .line 57
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->numberOfFrames:I

    .line 58
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->videoLengthMs:I

    .line 59
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->samplingIntervalMs:I

    .line 60
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerRow:I

    .line 61
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerColumn:I

    .line 62
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    .line 63
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->totalImageBytes:J

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->cachedSize:I

    .line 65
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 10

    .prologue
    .line 165
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 166
    .local v4, "size":I
    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameWidth:I

    if-eqz v5, :cond_0

    .line 167
    const/4 v5, 0x1

    iget v6, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameWidth:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 170
    :cond_0
    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    if-eqz v5, :cond_1

    .line 171
    const/4 v5, 0x2

    iget v6, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 174
    :cond_1
    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->numberOfFrames:I

    if-eqz v5, :cond_2

    .line 175
    const/4 v5, 0x3

    iget v6, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->numberOfFrames:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 178
    :cond_2
    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->videoLengthMs:I

    if-eqz v5, :cond_3

    .line 179
    const/4 v5, 0x4

    iget v6, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->videoLengthMs:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 182
    :cond_3
    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->samplingIntervalMs:I

    if-eqz v5, :cond_4

    .line 183
    const/4 v5, 0x5

    iget v6, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->samplingIntervalMs:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 186
    :cond_4
    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerRow:I

    if-eqz v5, :cond_5

    .line 187
    const/4 v5, 0x6

    iget v6, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerRow:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 190
    :cond_5
    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerColumn:I

    if-eqz v5, :cond_6

    .line 191
    const/4 v5, 0x7

    iget v6, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerColumn:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 194
    :cond_6
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_9

    .line 195
    const/4 v0, 0x0

    .line 196
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 197
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_8

    .line 198
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 199
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_7

    .line 200
    add-int/lit8 v0, v0, 0x1

    .line 201
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 197
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 205
    .end local v2    # "element":Ljava/lang/String;
    :cond_8
    add-int/2addr v4, v1

    .line 206
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 208
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_9
    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->totalImageBytes:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_a

    .line 209
    const/16 v5, 0x9

    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->totalImageBytes:J

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 212
    :cond_a
    return v4
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    if-ne p1, p0, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v1

    .line 73
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/Storyboard;

    if-nez v3, :cond_2

    move v1, v2

    .line 74
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 76
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .line 77
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/Storyboard;
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameWidth:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameWidth:I

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 78
    goto :goto_0

    .line 80
    :cond_3
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 81
    goto :goto_0

    .line 83
    :cond_4
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->numberOfFrames:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->numberOfFrames:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 84
    goto :goto_0

    .line 86
    :cond_5
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->videoLengthMs:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->videoLengthMs:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 87
    goto :goto_0

    .line 89
    :cond_6
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->samplingIntervalMs:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->samplingIntervalMs:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 90
    goto :goto_0

    .line 92
    :cond_7
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerRow:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerRow:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 93
    goto :goto_0

    .line 95
    :cond_8
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerColumn:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerColumn:I

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 96
    goto :goto_0

    .line 98
    :cond_9
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 100
    goto :goto_0

    .line 102
    :cond_a
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->totalImageBytes:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->totalImageBytes:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    .line 103
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 110
    const/16 v0, 0x11

    .line 111
    .local v0, "result":I
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameWidth:I

    add-int/lit16 v0, v1, 0x20f

    .line 112
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    add-int v0, v1, v2

    .line 113
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->numberOfFrames:I

    add-int v0, v1, v2

    .line 114
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->videoLengthMs:I

    add-int v0, v1, v2

    .line 115
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->samplingIntervalMs:I

    add-int v0, v1, v2

    .line 116
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerRow:I

    add-int v0, v1, v2

    .line 117
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerColumn:I

    add-int v0, v1, v2

    .line 118
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 120
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->totalImageBytes:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->totalImageBytes:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 122
    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/Storyboard;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/Storyboard;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 220
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 221
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 225
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 226
    :sswitch_0
    return-object p0

    .line 231
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameWidth:I

    goto :goto_0

    .line 235
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    goto :goto_0

    .line 239
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->numberOfFrames:I

    goto :goto_0

    .line 243
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->videoLengthMs:I

    goto :goto_0

    .line 247
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->samplingIntervalMs:I

    goto :goto_0

    .line 251
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerRow:I

    goto :goto_0

    .line 255
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerColumn:I

    goto :goto_0

    .line 259
    :sswitch_8
    const/16 v5, 0x42

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 261
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    if-nez v5, :cond_2

    move v1, v4

    .line 262
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 263
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 264
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 266
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 267
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 268
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 266
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 261
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_1

    .line 271
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 272
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    goto :goto_0

    .line 276
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->totalImageBytes:J

    goto :goto_0

    .line 221
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameWidth:I

    if-eqz v2, :cond_0

    .line 129
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameWidth:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 131
    :cond_0
    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    if-eqz v2, :cond_1

    .line 132
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 134
    :cond_1
    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->numberOfFrames:I

    if-eqz v2, :cond_2

    .line 135
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->numberOfFrames:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 137
    :cond_2
    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->videoLengthMs:I

    if-eqz v2, :cond_3

    .line 138
    const/4 v2, 0x4

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->videoLengthMs:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 140
    :cond_3
    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->samplingIntervalMs:I

    if-eqz v2, :cond_4

    .line 141
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->samplingIntervalMs:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 143
    :cond_4
    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerRow:I

    if-eqz v2, :cond_5

    .line 144
    const/4 v2, 0x6

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerRow:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 146
    :cond_5
    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerColumn:I

    if-eqz v2, :cond_6

    .line 147
    const/4 v2, 0x7

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerColumn:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 149
    :cond_6
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 150
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 151
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 152
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_7

    .line 153
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 150
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 157
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_8
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->totalImageBytes:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    .line 158
    const/16 v2, 0x9

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->totalImageBytes:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 160
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 161
    return-void
.end method
