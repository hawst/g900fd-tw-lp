.class public final Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;
.super Lcom/google/protobuf/nano/MessageNano;
.source "GdataRosyProtoCoreProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TokenPagination"
.end annotation


# instance fields
.field public nextPageToken:Ljava/lang/String;

.field public previousPageToken:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 176
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->clear()Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    .line 177
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;
    .locals 1

    .prologue
    .line 180
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->nextPageToken:Ljava/lang/String;

    .line 181
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->previousPageToken:Ljava/lang/String;

    .line 182
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->cachedSize:I

    .line 183
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 236
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 237
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->nextPageToken:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 238
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->nextPageToken:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 241
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->previousPageToken:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 242
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->previousPageToken:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 245
    :cond_1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 188
    if-ne p1, p0, :cond_1

    .line 209
    :cond_0
    :goto_0
    return v1

    .line 191
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    if-nez v3, :cond_2

    move v1, v2

    .line 192
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 194
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    .line 195
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->nextPageToken:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 196
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->nextPageToken:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 197
    goto :goto_0

    .line 199
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->nextPageToken:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->nextPageToken:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 200
    goto :goto_0

    .line 202
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->previousPageToken:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 203
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->previousPageToken:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 204
    goto :goto_0

    .line 206
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->previousPageToken:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->previousPageToken:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 207
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 214
    const/16 v0, 0x11

    .line 215
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->nextPageToken:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 217
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->previousPageToken:Ljava/lang/String;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 219
    return v0

    .line 215
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->nextPageToken:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 217
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->previousPageToken:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 253
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 254
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 258
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 259
    :sswitch_0
    return-object p0

    .line 264
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->nextPageToken:Ljava/lang/String;

    goto :goto_0

    .line 268
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->previousPageToken:Ljava/lang/String;

    goto :goto_0

    .line 254
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->nextPageToken:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->nextPageToken:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->previousPageToken:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 229
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->previousPageToken:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 231
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 232
    return-void
.end method
