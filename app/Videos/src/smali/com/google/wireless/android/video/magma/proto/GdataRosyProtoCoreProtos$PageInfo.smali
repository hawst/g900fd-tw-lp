.class public final Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "GdataRosyProtoCoreProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PageInfo"
.end annotation


# instance fields
.field public resultPerPage:I

.field public startIndex:I

.field public totalResults:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 35
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->clear()Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    .line 36
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 39
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->totalResults:I

    .line 40
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->resultPerPage:I

    .line 41
    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->startIndex:I

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->cachedSize:I

    .line 43
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 93
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 94
    .local v0, "size":I
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->totalResults:I

    if-eqz v1, :cond_0

    .line 95
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->totalResults:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_0
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->resultPerPage:I

    if-eqz v1, :cond_1

    .line 99
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->resultPerPage:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_1
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->startIndex:I

    if-eqz v1, :cond_2

    .line 103
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->startIndex:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    :cond_2
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    if-ne p1, p0, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v1

    .line 51
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    if-nez v3, :cond_2

    move v1, v2

    .line 52
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 54
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    .line 55
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->totalResults:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->totalResults:I

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 56
    goto :goto_0

    .line 58
    :cond_3
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->resultPerPage:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->resultPerPage:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 59
    goto :goto_0

    .line 61
    :cond_4
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->startIndex:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->startIndex:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 62
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 69
    const/16 v0, 0x11

    .line 70
    .local v0, "result":I
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->totalResults:I

    add-int/lit16 v0, v1, 0x20f

    .line 71
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->resultPerPage:I

    add-int v0, v1, v2

    .line 72
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->startIndex:I

    add-int v0, v1, v2

    .line 73
    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 115
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 119
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 120
    :sswitch_0
    return-object p0

    .line 125
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->totalResults:I

    goto :goto_0

    .line 129
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->resultPerPage:I

    goto :goto_0

    .line 133
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->startIndex:I

    goto :goto_0

    .line 115
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->totalResults:I

    if-eqz v0, :cond_0

    .line 80
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->totalResults:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 82
    :cond_0
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->resultPerPage:I

    if-eqz v0, :cond_1

    .line 83
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->resultPerPage:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 85
    :cond_1
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->startIndex:I

    if-eqz v0, :cond_2

    .line 86
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->startIndex:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 88
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 89
    return-void
.end method
