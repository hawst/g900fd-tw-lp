.class public final Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VideoCollectionResource.java"


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;


# instance fields
.field public asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

.field public child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

.field public collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

.field public estimatedChildCount:I

.field public image:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 42
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->clear()Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    .line 43
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46
    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    .line 47
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->estimatedChildCount:I

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->title:Ljava/lang/String;

    .line 50
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->image:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    .line 51
    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 52
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->cachedSize:I

    .line 53
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 157
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 158
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    if-eqz v3, :cond_0

    .line 159
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 162
    :cond_0
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 163
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 164
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    aget-object v0, v3, v1

    .line 165
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    if-eqz v0, :cond_1

    .line 166
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 163
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 171
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    .end local v1    # "i":I
    :cond_2
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->estimatedChildCount:I

    if-eqz v3, :cond_3

    .line 172
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->estimatedChildCount:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 175
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->title:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 176
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->title:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 179
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->image:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->image:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v3, v3

    if-lez v3, :cond_6

    .line 180
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->image:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v3, v3

    if-ge v1, v3, :cond_6

    .line 181
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->image:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    aget-object v0, v3, v1

    .line 182
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    if-eqz v0, :cond_5

    .line 183
    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 180
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 188
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .end local v1    # "i":I
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-eqz v3, :cond_7

    .line 189
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 192
    :cond_7
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    if-ne p1, p0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v1

    .line 61
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    if-nez v3, :cond_2

    move v1, v2

    .line 62
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 64
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    .line 65
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    if-nez v3, :cond_3

    .line 66
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    if-eqz v3, :cond_4

    move v1, v2

    .line 67
    goto :goto_0

    .line 70
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 71
    goto :goto_0

    .line 74
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 76
    goto :goto_0

    .line 78
    :cond_5
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->estimatedChildCount:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->estimatedChildCount:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 79
    goto :goto_0

    .line 81
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->title:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 82
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->title:Ljava/lang/String;

    if-eqz v3, :cond_8

    move v1, v2

    .line 83
    goto :goto_0

    .line 85
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 86
    goto :goto_0

    .line 88
    :cond_8
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->image:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->image:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 90
    goto :goto_0

    .line 92
    :cond_9
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-nez v3, :cond_a

    .line 93
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-eqz v3, :cond_0

    move v1, v2

    .line 94
    goto :goto_0

    .line 97
    :cond_a
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 98
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 106
    const/16 v0, 0x11

    .line 107
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 109
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 111
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->estimatedChildCount:I

    add-int v0, v1, v3

    .line 112
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->title:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 114
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->image:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 116
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 118
    return v0

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;->hashCode()I

    move-result v1

    goto :goto_0

    .line 112
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->title:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 116
    :cond_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 200
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 201
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 205
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 206
    :sswitch_0
    return-object p0

    .line 211
    :sswitch_1
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    if-nez v5, :cond_1

    .line 212
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;-><init>()V

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    .line 214
    :cond_1
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 218
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 220
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    if-nez v5, :cond_3

    move v1, v4

    .line 221
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    .line 223
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    if-eqz v1, :cond_2

    .line 224
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 226
    :cond_2
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 227
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;-><init>()V

    aput-object v5, v2, v1

    .line 228
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 229
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 226
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 220
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    :cond_3
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    array-length v1, v5

    goto :goto_1

    .line 232
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    :cond_4
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;-><init>()V

    aput-object v5, v2, v1

    .line 233
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 234
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    goto :goto_0

    .line 238
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->estimatedChildCount:I

    goto :goto_0

    .line 242
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->title:Ljava/lang/String;

    goto :goto_0

    .line 246
    :sswitch_5
    const/16 v5, 0x2a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 248
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->image:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-nez v5, :cond_6

    move v1, v4

    .line 249
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    .line 251
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    if-eqz v1, :cond_5

    .line 252
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->image:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 254
    :cond_5
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_7

    .line 255
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;-><init>()V

    aput-object v5, v2, v1

    .line 256
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 257
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 254
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 248
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    :cond_6
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->image:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v1, v5

    goto :goto_3

    .line 260
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    :cond_7
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;-><init>()V

    aput-object v5, v2, v1

    .line 261
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 262
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->image:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    goto/16 :goto_0

    .line 266
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    :sswitch_6
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-nez v5, :cond_8

    .line 267
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource;-><init>()V

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 269
    :cond_8
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 201
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    if-eqz v2, :cond_0

    .line 125
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->collectionId:Lcom/google/wireless/android/video/magma/proto/VideoCollectionResourceId;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 127
    :cond_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 128
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 129
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->child:[Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;

    aget-object v0, v2, v1

    .line 130
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    if-eqz v0, :cond_1

    .line 131
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 128
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 135
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;
    .end local v1    # "i":I
    :cond_2
    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->estimatedChildCount:I

    if-eqz v2, :cond_3

    .line 136
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->estimatedChildCount:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 138
    :cond_3
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 139
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 141
    :cond_4
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->image:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->image:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 142
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->image:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 143
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->image:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    aget-object v0, v2, v1

    .line 144
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    if-eqz v0, :cond_5

    .line 145
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 142
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 149
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .end local v1    # "i":I
    :cond_6
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-eqz v2, :cond_7

    .line 150
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCollectionResource;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 152
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 153
    return-void
.end method
