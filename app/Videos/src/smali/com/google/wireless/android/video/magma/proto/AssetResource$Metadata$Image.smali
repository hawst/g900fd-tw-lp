.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Image"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;


# instance fields
.field public height:I

.field public resizable:Z

.field public type:I

.field public url:Ljava/lang/String;

.field public width:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 79
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->clear()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    .line 80
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .locals 2

    .prologue
    .line 52
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-nez v0, :cond_1

    .line 53
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 55
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-nez v0, :cond_0

    .line 56
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    .line 58
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    return-object v0

    .line 58
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 83
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url:Ljava/lang/String;

    .line 84
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->width:I

    .line 85
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->height:I

    .line 86
    iput-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->resizable:Z

    .line 87
    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->type:I

    .line 88
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->cachedSize:I

    .line 89
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 158
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 159
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 160
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 163
    :cond_0
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->width:I

    if-eqz v1, :cond_1

    .line 164
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->width:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    :cond_1
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->height:I

    if-eqz v1, :cond_2

    .line 168
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->height:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 171
    :cond_2
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->resizable:Z

    if-eq v1, v3, :cond_3

    .line 172
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->resizable:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 175
    :cond_3
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->type:I

    if-eq v1, v3, :cond_4

    .line 176
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->type:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    :cond_4
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 94
    if-ne p1, p0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return v1

    .line 97
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-nez v3, :cond_2

    move v1, v2

    .line 98
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 100
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    .line 101
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 102
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 103
    goto :goto_0

    .line 105
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 106
    goto :goto_0

    .line 108
    :cond_4
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->width:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->width:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 109
    goto :goto_0

    .line 111
    :cond_5
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->height:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->height:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 112
    goto :goto_0

    .line 114
    :cond_6
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->resizable:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->resizable:Z

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 115
    goto :goto_0

    .line 117
    :cond_7
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->type:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->type:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 118
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 125
    const/16 v0, 0x11

    .line 126
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 128
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->width:I

    add-int v0, v1, v2

    .line 129
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->height:I

    add-int v0, v1, v2

    .line 130
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->resizable:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x4cf

    :goto_1
    add-int v0, v2, v1

    .line 131
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->type:I

    add-int v0, v1, v2

    .line 132
    return v0

    .line 126
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 130
    :cond_1
    const/16 v1, 0x4d5

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 187
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 188
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 192
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 193
    :sswitch_0
    return-object p0

    .line 198
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url:Ljava/lang/String;

    goto :goto_0

    .line 202
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->width:I

    goto :goto_0

    .line 206
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->height:I

    goto :goto_0

    .line 210
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->resizable:Z

    goto :goto_0

    .line 214
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 215
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 220
    :pswitch_0
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->type:I

    goto :goto_0

    .line 188
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    .line 215
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 138
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->url:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 141
    :cond_0
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->width:I

    if-eqz v0, :cond_1

    .line 142
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->width:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 144
    :cond_1
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->height:I

    if-eqz v0, :cond_2

    .line 145
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->height:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 147
    :cond_2
    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->resizable:Z

    if-eq v0, v2, :cond_3

    .line 148
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->resizable:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 150
    :cond_3
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->type:I

    if-eq v0, v2, :cond_4

    .line 151
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->type:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 153
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 154
    return-void
.end method
