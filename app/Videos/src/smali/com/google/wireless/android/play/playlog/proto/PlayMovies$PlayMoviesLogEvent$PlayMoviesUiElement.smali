.class public final Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayMovies.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayMoviesUiElement"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;


# instance fields
.field public child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

.field public clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3549
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3550
    invoke-virtual {p0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .line 3551
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    .locals 2

    .prologue
    .line 3529
    sget-object v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->_emptyArray:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-nez v0, :cond_1

    .line 3530
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 3532
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->_emptyArray:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-nez v0, :cond_0

    .line 3533
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    sput-object v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->_emptyArray:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .line 3535
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3537
    :cond_1
    sget-object v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->_emptyArray:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    return-object v0

    .line 3535
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    .locals 1

    .prologue
    .line 3554
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    .line 3555
    invoke-static {}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->emptyArray()[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .line 3556
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    .line 3557
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->cachedSize:I

    .line 3558
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 3622
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 3623
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    if-eqz v3, :cond_0

    .line 3624
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3627
    :cond_0
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 3628
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 3629
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    aget-object v0, v3, v1

    .line 3630
    .local v0, "element":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    if-eqz v0, :cond_1

    .line 3631
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3628
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3636
    .end local v0    # "element":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    .end local v1    # "i":I
    :cond_2
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    if-eq v3, v4, :cond_3

    .line 3637
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 3640
    :cond_3
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3563
    if-ne p1, p0, :cond_1

    .line 3586
    :cond_0
    :goto_0
    return v1

    .line 3566
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-nez v3, :cond_2

    move v1, v2

    .line 3567
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 3569
    check-cast v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .line 3570
    .local v0, "other":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    if-nez v3, :cond_3

    .line 3571
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    if-eqz v3, :cond_4

    move v1, v2

    .line 3572
    goto :goto_0

    .line 3575
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 3576
    goto :goto_0

    .line 3579
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 3581
    goto :goto_0

    .line 3583
    :cond_5
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 3584
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 3591
    const/16 v0, 0x11

    .line 3592
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 3594
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 3596
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    add-int v0, v1, v2

    .line 3597
    return v0

    .line 3592
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    invoke-virtual {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3523
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 3648
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3649
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3653
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 3654
    :sswitch_0
    return-object p0

    .line 3659
    :sswitch_1
    iget-object v6, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    if-nez v6, :cond_1

    .line 3660
    new-instance v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    invoke-direct {v6}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;-><init>()V

    iput-object v6, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    .line 3662
    :cond_1
    iget-object v6, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3666
    :sswitch_2
    const/16 v6, 0x12

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3668
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-nez v6, :cond_3

    move v1, v5

    .line 3669
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .line 3671
    .local v2, "newArray":[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    if-eqz v1, :cond_2

    .line 3672
    iget-object v6, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3674
    :cond_2
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_4

    .line 3675
    new-instance v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-direct {v6}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;-><init>()V

    aput-object v6, v2, v1

    .line 3676
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3677
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3674
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3668
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    :cond_3
    iget-object v6, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    array-length v1, v6

    goto :goto_1

    .line 3680
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    :cond_4
    new-instance v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-direct {v6}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;-><init>()V

    aput-object v6, v2, v1

    .line 3681
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3682
    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    goto :goto_0

    .line 3686
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 3687
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 3718
    :pswitch_0
    iput v4, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    goto :goto_0

    .line 3649
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    .line 3687
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 3603
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    if-eqz v2, :cond_0

    .line 3604
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3606
    :cond_0
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 3607
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 3608
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    aget-object v0, v2, v1

    .line 3609
    .local v0, "element":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    if-eqz v0, :cond_1

    .line 3610
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3607
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3614
    .end local v0    # "element":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    .end local v1    # "i":I
    :cond_2
    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    if-eq v2, v3, :cond_3

    .line 3615
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3617
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3618
    return-void
.end method
