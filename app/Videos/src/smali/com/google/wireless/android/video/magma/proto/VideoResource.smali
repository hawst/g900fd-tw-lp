.class public final Lcom/google/wireless/android/video/magma/proto/VideoResource;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VideoResource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;,
        Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    }
.end annotation


# instance fields
.field public completed:Z

.field public format:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

.field public pinnedOnDevice:[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

.field public playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

.field public resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

.field public viewerLastStarted:J

.field public viewerStarted:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 350
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 351
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->clear()Lcom/google/wireless/android/video/magma/proto/VideoResource;

    .line 352
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 355
    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 356
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    .line 357
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice:[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    .line 358
    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    .line 359
    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerStarted:J

    .line 360
    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerLastStarted:J

    .line 361
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->completed:Z

    .line 362
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->cachedSize:I

    .line 363
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 471
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 472
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v3, :cond_0

    .line 473
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 476
    :cond_0
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 477
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 478
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    aget-object v0, v3, v1

    .line 479
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    if-eqz v0, :cond_1

    .line 480
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 477
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 485
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    .end local v1    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice:[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice:[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    array-length v3, v3

    if-lez v3, :cond_4

    .line 486
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice:[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    array-length v3, v3

    if-ge v1, v3, :cond_4

    .line 487
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice:[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    aget-object v0, v3, v1

    .line 488
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    if-eqz v0, :cond_3

    .line 489
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 486
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 494
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    .end local v1    # "i":I
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    if-eqz v3, :cond_5

    .line 495
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 498
    :cond_5
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerStarted:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_6

    .line 499
    const/4 v3, 0x5

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerStarted:J

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 502
    :cond_6
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerLastStarted:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_7

    .line 503
    const/4 v3, 0x6

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerLastStarted:J

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 506
    :cond_7
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->completed:Z

    if-eqz v3, :cond_8

    .line 507
    const/16 v3, 0x44c

    iget-boolean v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->completed:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 510
    :cond_8
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 368
    if-ne p1, p0, :cond_1

    .line 410
    :cond_0
    :goto_0
    return v1

    .line 371
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/VideoResource;

    if-nez v3, :cond_2

    move v1, v2

    .line 372
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 374
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;

    .line 375
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/VideoResource;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v3, :cond_3

    .line 376
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v3, :cond_4

    move v1, v2

    .line 377
    goto :goto_0

    .line 380
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 381
    goto :goto_0

    .line 384
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 386
    goto :goto_0

    .line 388
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice:[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice:[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 390
    goto :goto_0

    .line 392
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    if-nez v3, :cond_7

    .line 393
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    if-eqz v3, :cond_8

    move v1, v2

    .line 394
    goto :goto_0

    .line 397
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 398
    goto :goto_0

    .line 401
    :cond_8
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerStarted:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerStarted:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_9

    move v1, v2

    .line 402
    goto :goto_0

    .line 404
    :cond_9
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerLastStarted:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerLastStarted:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_a

    move v1, v2

    .line 405
    goto :goto_0

    .line 407
    :cond_a
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->completed:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->completed:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 408
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v2, 0x0

    .line 415
    const/16 v0, 0x11

    .line 416
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 418
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 420
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice:[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 422
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 424
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerStarted:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerStarted:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 426
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerLastStarted:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerLastStarted:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 428
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->completed:Z

    if-eqz v1, :cond_2

    const/16 v1, 0x4cf

    :goto_2
    add-int v0, v2, v1

    .line 429
    return v0

    .line 416
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hashCode()I

    move-result v1

    goto :goto_0

    .line 422
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->hashCode()I

    move-result v2

    goto :goto_1

    .line 428
    :cond_2
    const/16 v1, 0x4d5

    goto :goto_2
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/VideoResource;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/VideoResource;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 518
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 519
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 523
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 524
    :sswitch_0
    return-object p0

    .line 529
    :sswitch_1
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v5, :cond_1

    .line 530
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>()V

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 532
    :cond_1
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 536
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 538
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    if-nez v5, :cond_3

    move v1, v4

    .line 539
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    .line 541
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    if-eqz v1, :cond_2

    .line 542
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 544
    :cond_2
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 545
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;-><init>()V

    aput-object v5, v2, v1

    .line 546
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 547
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 544
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 538
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    :cond_3
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    array-length v1, v5

    goto :goto_1

    .line 550
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    :cond_4
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;-><init>()V

    aput-object v5, v2, v1

    .line 551
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 552
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    goto :goto_0

    .line 556
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 558
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice:[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-nez v5, :cond_6

    move v1, v4

    .line 559
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    .line 561
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    if-eqz v1, :cond_5

    .line 562
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice:[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 564
    :cond_5
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_7

    .line 565
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;-><init>()V

    aput-object v5, v2, v1

    .line 566
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 567
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 564
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 558
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    :cond_6
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice:[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    array-length v1, v5

    goto :goto_3

    .line 570
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    :cond_7
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;-><init>()V

    aput-object v5, v2, v1

    .line 571
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 572
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice:[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    goto/16 :goto_0

    .line 576
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    :sswitch_4
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    if-nez v5, :cond_8

    .line 577
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;-><init>()V

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    .line 579
    :cond_8
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 583
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerStarted:J

    goto/16 :goto_0

    .line 587
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerLastStarted:J

    goto/16 :goto_0

    .line 591
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->completed:Z

    goto/16 :goto_0

    .line 519
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x2260 -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 8
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 435
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v2, :cond_0

    .line 436
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 438
    :cond_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 439
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 440
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->format:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    aget-object v0, v2, v1

    .line 441
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    if-eqz v0, :cond_1

    .line 442
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 439
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 446
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    .end local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice:[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice:[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 447
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice:[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 448
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->pinnedOnDevice:[Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;

    aget-object v0, v2, v1

    .line 449
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    if-eqz v0, :cond_3

    .line 450
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 447
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 454
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/DeviceResourceId;
    .end local v1    # "i":I
    :cond_4
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    if-eqz v2, :cond_5

    .line 455
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 457
    :cond_5
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerStarted:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_6

    .line 458
    const/4 v2, 0x5

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerStarted:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 460
    :cond_6
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerLastStarted:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_7

    .line 461
    const/4 v2, 0x6

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->viewerLastStarted:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 463
    :cond_7
    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->completed:Z

    if-eqz v2, :cond_8

    .line 464
    const/16 v2, 0x44c

    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource;->completed:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 466
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 467
    return-void
.end method
