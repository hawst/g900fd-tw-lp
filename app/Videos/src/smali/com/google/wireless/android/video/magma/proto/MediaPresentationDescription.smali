.class public final Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;
.super Lcom/google/protobuf/nano/MessageNano;
.source "MediaPresentationDescription.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;,
        Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;,
        Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    }
.end annotation


# instance fields
.field public captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

.field public representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

.field public resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

.field public storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

.field public xml:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 796
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 797
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->clear()Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;

    .line 798
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;
    .locals 1

    .prologue
    .line 801
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->xml:Ljava/lang/String;

    .line 802
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 803
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    .line 804
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    .line 805
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/Storyboard;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .line 806
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->cachedSize:I

    .line 807
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 904
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 905
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->xml:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 906
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->xml:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 909
    :cond_0
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v3, :cond_1

    .line 910
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 913
    :cond_1
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 914
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 915
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    aget-object v0, v3, v1

    .line 916
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    if-eqz v0, :cond_2

    .line 917
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 914
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 922
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    .end local v1    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    array-length v3, v3

    if-lez v3, :cond_5

    .line 923
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    array-length v3, v3

    if-ge v1, v3, :cond_5

    .line 924
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    aget-object v0, v3, v1

    .line 925
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    if-eqz v0, :cond_4

    .line 926
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 923
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 931
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    .end local v1    # "i":I
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    array-length v3, v3

    if-lez v3, :cond_7

    .line 932
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 933
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    aget-object v0, v3, v1

    .line 934
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/Storyboard;
    if-eqz v0, :cond_6

    .line 935
    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 932
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 940
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .end local v1    # "i":I
    :cond_7
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 812
    if-ne p1, p0, :cond_1

    .line 847
    :cond_0
    :goto_0
    return v1

    .line 815
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;

    if-nez v3, :cond_2

    move v1, v2

    .line 816
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 818
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;

    .line 819
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->xml:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 820
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->xml:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 821
    goto :goto_0

    .line 823
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->xml:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->xml:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 824
    goto :goto_0

    .line 826
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v3, :cond_5

    .line 827
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v3, :cond_6

    move v1, v2

    .line 828
    goto :goto_0

    .line 831
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 832
    goto :goto_0

    .line 835
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 837
    goto :goto_0

    .line 839
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 841
    goto :goto_0

    .line 843
    :cond_8
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 845
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 852
    const/16 v0, 0x11

    .line 853
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->xml:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 855
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 857
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 859
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 861
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 863
    return v0

    .line 853
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->xml:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 855
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 948
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 949
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 953
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 954
    :sswitch_0
    return-object p0

    .line 959
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->xml:Ljava/lang/String;

    goto :goto_0

    .line 963
    :sswitch_2
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v5, :cond_1

    .line 964
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>()V

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 966
    :cond_1
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 970
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 972
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    if-nez v5, :cond_3

    move v1, v4

    .line 973
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    .line 975
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    if-eqz v1, :cond_2

    .line 976
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 978
    :cond_2
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 979
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;-><init>()V

    aput-object v5, v2, v1

    .line 980
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 981
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 978
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 972
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    :cond_3
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    array-length v1, v5

    goto :goto_1

    .line 984
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    :cond_4
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;-><init>()V

    aput-object v5, v2, v1

    .line 985
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 986
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    goto :goto_0

    .line 990
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    :sswitch_4
    const/16 v5, 0x22

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 992
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    if-nez v5, :cond_6

    move v1, v4

    .line 993
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    .line 995
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    if-eqz v1, :cond_5

    .line 996
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 998
    :cond_5
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_7

    .line 999
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;-><init>()V

    aput-object v5, v2, v1

    .line 1000
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1001
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 998
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 992
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    :cond_6
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    array-length v1, v5

    goto :goto_3

    .line 1004
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    :cond_7
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;-><init>()V

    aput-object v5, v2, v1

    .line 1005
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1006
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    goto/16 :goto_0

    .line 1010
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    :sswitch_5
    const/16 v5, 0x2a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1012
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    if-nez v5, :cond_9

    move v1, v4

    .line 1013
    .restart local v1    # "i":I
    :goto_5
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .line 1015
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/Storyboard;
    if-eqz v1, :cond_8

    .line 1016
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1018
    :cond_8
    :goto_6
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_a

    .line 1019
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/Storyboard;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/Storyboard;-><init>()V

    aput-object v5, v2, v1

    .line 1020
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1021
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1018
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1012
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/Storyboard;
    :cond_9
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    array-length v1, v5

    goto :goto_5

    .line 1024
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/Storyboard;
    :cond_a
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/Storyboard;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/Storyboard;-><init>()V

    aput-object v5, v2, v1

    .line 1025
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1026
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    goto/16 :goto_0

    .line 949
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 869
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->xml:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 870
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->xml:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 872
    :cond_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v2, :cond_1

    .line 873
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 875
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 876
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 877
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    aget-object v0, v2, v1

    .line 878
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    if-eqz v0, :cond_2

    .line 879
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 876
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 883
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    .end local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 884
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 885
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->captions:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    aget-object v0, v2, v1

    .line 886
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    if-eqz v0, :cond_4

    .line 887
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 884
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 891
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    .end local v1    # "i":I
    :cond_5
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 892
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 893
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    aget-object v0, v2, v1

    .line 894
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/Storyboard;
    if-eqz v0, :cond_6

    .line 895
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 892
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 899
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .end local v1    # "i":I
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 900
    return-void
.end method
