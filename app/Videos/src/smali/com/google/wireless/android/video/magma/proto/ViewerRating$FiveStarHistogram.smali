.class public final Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ViewerRating.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/ViewerRating;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FiveStarHistogram"
.end annotation


# instance fields
.field public fiveStarRatings:J

.field public fourStarRatings:J

.field public oneStarRatings:J

.field public threeStarRatings:J

.field public twoStarRatings:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 48
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->clear()Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    .line 49
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 52
    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->oneStarRatings:J

    .line 53
    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->twoStarRatings:J

    .line 54
    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->threeStarRatings:J

    .line 55
    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fourStarRatings:J

    .line 56
    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fiveStarRatings:J

    .line 57
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->cachedSize:I

    .line 58
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 127
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 128
    .local v0, "size":I
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->oneStarRatings:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 129
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->oneStarRatings:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    :cond_0
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->twoStarRatings:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 133
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->twoStarRatings:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 136
    :cond_1
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->threeStarRatings:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 137
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->threeStarRatings:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 140
    :cond_2
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fourStarRatings:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 141
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fourStarRatings:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 144
    :cond_3
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fiveStarRatings:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 145
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fiveStarRatings:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 148
    :cond_4
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    if-ne p1, p0, :cond_1

    .line 85
    :cond_0
    :goto_0
    return v1

    .line 66
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    if-nez v3, :cond_2

    move v1, v2

    .line 67
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 69
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    .line 70
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->oneStarRatings:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->oneStarRatings:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    move v1, v2

    .line 71
    goto :goto_0

    .line 73
    :cond_3
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->twoStarRatings:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->twoStarRatings:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    move v1, v2

    .line 74
    goto :goto_0

    .line 76
    :cond_4
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->threeStarRatings:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->threeStarRatings:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_5

    move v1, v2

    .line 77
    goto :goto_0

    .line 79
    :cond_5
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fourStarRatings:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fourStarRatings:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_6

    move v1, v2

    .line 80
    goto :goto_0

    .line 82
    :cond_6
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fiveStarRatings:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fiveStarRatings:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    .line 83
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 90
    const/16 v0, 0x11

    .line 91
    .local v0, "result":I
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->oneStarRatings:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->oneStarRatings:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/lit16 v0, v1, 0x20f

    .line 93
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->twoStarRatings:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->twoStarRatings:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 95
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->threeStarRatings:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->threeStarRatings:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 97
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fourStarRatings:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fourStarRatings:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 99
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fiveStarRatings:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fiveStarRatings:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 101
    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 15
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 156
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 157
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 161
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 162
    :sswitch_0
    return-object p0

    .line 167
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->oneStarRatings:J

    goto :goto_0

    .line 171
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->twoStarRatings:J

    goto :goto_0

    .line 175
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->threeStarRatings:J

    goto :goto_0

    .line 179
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fourStarRatings:J

    goto :goto_0

    .line 183
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fiveStarRatings:J

    goto :goto_0

    .line 157
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 107
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->oneStarRatings:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 108
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->oneStarRatings:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 110
    :cond_0
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->twoStarRatings:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 111
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->twoStarRatings:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 113
    :cond_1
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->threeStarRatings:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 114
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->threeStarRatings:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 116
    :cond_2
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fourStarRatings:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 117
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fourStarRatings:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 119
    :cond_3
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fiveStarRatings:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_4

    .line 120
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->fiveStarRatings:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 122
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 123
    return-void
.end method
