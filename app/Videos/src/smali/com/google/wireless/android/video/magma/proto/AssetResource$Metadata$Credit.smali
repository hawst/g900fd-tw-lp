.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Credit"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;


# instance fields
.field public name:Ljava/lang/String;

.field public role:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 272
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 273
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->clear()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    .line 274
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    .locals 2

    .prologue
    .line 255
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    if-nez v0, :cond_1

    .line 256
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 258
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    if-nez v0, :cond_0

    .line 259
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    .line 261
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    return-object v0

    .line 261
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    .locals 1

    .prologue
    .line 277
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name:Ljava/lang/String;

    .line 278
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->role:I

    .line 279
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->cachedSize:I

    .line 280
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 328
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 329
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 330
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name:Ljava/lang/String;

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 333
    :cond_0
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->role:I

    if-eq v1, v3, :cond_1

    .line 334
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->role:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 337
    :cond_1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 285
    if-ne p1, p0, :cond_1

    .line 302
    :cond_0
    :goto_0
    return v1

    .line 288
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    if-nez v3, :cond_2

    move v1, v2

    .line 289
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 291
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    .line 292
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 293
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 294
    goto :goto_0

    .line 296
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 297
    goto :goto_0

    .line 299
    :cond_4
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->role:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->role:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 300
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 307
    const/16 v0, 0x11

    .line 308
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 310
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->role:I

    add-int v0, v1, v2

    .line 311
    return v0

    .line 308
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 241
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 345
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 346
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 350
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 351
    :sswitch_0
    return-object p0

    .line 356
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name:Ljava/lang/String;

    goto :goto_0

    .line 360
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 361
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 366
    :pswitch_0
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->role:I

    goto :goto_0

    .line 346
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    .line 361
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 317
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 320
    :cond_0
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->role:I

    if-eq v0, v2, :cond_1

    .line 321
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->role:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 323
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 324
    return-void
.end method
