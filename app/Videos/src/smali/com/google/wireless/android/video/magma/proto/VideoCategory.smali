.class public final Lcom/google/wireless/android/video/magma/proto/VideoCategory;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VideoCategory.java"


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoCategory;


# instance fields
.field public description:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

.field public name:Ljava/lang/String;

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 45
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->clear()Lcom/google/wireless/android/video/magma/proto/VideoCategory;

    .line 46
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/VideoCategory;
    .locals 2

    .prologue
    .line 18
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoCategory;

    if-nez v0, :cond_1

    .line 19
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 21
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoCategory;

    if-nez v0, :cond_0

    .line 22
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/VideoCategory;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoCategory;

    .line 24
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 26
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoCategory;

    return-object v0

    .line 24
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/VideoCategory;
    .locals 1

    .prologue
    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->id:Ljava/lang/String;

    .line 50
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->type:I

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->name:Ljava/lang/String;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->description:Ljava/lang/String;

    .line 53
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->cachedSize:I

    .line 55
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 141
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 142
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->id:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 143
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->id:Ljava/lang/String;

    invoke-static {v5, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 146
    :cond_0
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->type:I

    if-eq v3, v5, :cond_1

    .line 147
    const/4 v3, 0x2

    iget v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->type:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 150
    :cond_1
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->name:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 151
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->name:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 154
    :cond_2
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->description:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 155
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->description:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 158
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v3, v3

    if-lez v3, :cond_5

    .line 159
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v3, v3

    if-ge v1, v3, :cond_5

    .line 160
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    aget-object v0, v3, v1

    .line 161
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    if-eqz v0, :cond_4

    .line 162
    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 159
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 167
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .end local v1    # "i":I
    :cond_5
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60
    if-ne p1, p0, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v1

    .line 63
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/VideoCategory;

    if-nez v3, :cond_2

    move v1, v2

    .line 64
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 66
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;

    .line 67
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/VideoCategory;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->id:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 68
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->id:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 69
    goto :goto_0

    .line 71
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 72
    goto :goto_0

    .line 74
    :cond_4
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->type:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->type:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 75
    goto :goto_0

    .line 77
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->name:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 78
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->name:Ljava/lang/String;

    if-eqz v3, :cond_7

    move v1, v2

    .line 79
    goto :goto_0

    .line 81
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 82
    goto :goto_0

    .line 84
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->description:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 85
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->description:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v1, v2

    .line 86
    goto :goto_0

    .line 88
    :cond_8
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->description:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->description:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 89
    goto :goto_0

    .line 91
    :cond_9
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 93
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 100
    const/16 v0, 0x11

    .line 101
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->id:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 103
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->type:I

    add-int v0, v1, v3

    .line 104
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->name:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 106
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->description:Ljava/lang/String;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 108
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 110
    return v0

    .line 101
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->id:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 104
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 106
    :cond_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->description:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/VideoCategory;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/VideoCategory;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 175
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 176
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 180
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 181
    :sswitch_0
    return-object p0

    .line 186
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->id:Ljava/lang/String;

    goto :goto_0

    .line 190
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 191
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 194
    :pswitch_0
    iput v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->type:I

    goto :goto_0

    .line 200
    .end local v4    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->name:Ljava/lang/String;

    goto :goto_0

    .line 204
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->description:Ljava/lang/String;

    goto :goto_0

    .line 208
    :sswitch_5
    const/16 v6, 0x2a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 210
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-nez v6, :cond_2

    move v1, v5

    .line 211
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    .line 213
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    if-eqz v1, :cond_1

    .line 214
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 216
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 217
    new-instance v6, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-direct {v6}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;-><init>()V

    aput-object v6, v2, v1

    .line 218
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 219
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 216
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 210
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    :cond_2
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v1, v6

    goto :goto_1

    .line 222
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    :cond_3
    new-instance v6, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-direct {v6}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;-><init>()V

    aput-object v6, v2, v1

    .line 223
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 224
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    goto :goto_0

    .line 176
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    .line 191
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 116
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->id:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 117
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->id:Ljava/lang/String;

    invoke-virtual {p1, v4, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 119
    :cond_0
    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->type:I

    if-eq v2, v4, :cond_1

    .line 120
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->type:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 122
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->name:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 123
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->name:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 125
    :cond_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->description:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 126
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->description:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 128
    :cond_3
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 129
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 130
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoCategory;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    aget-object v0, v2, v1

    .line 131
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    if-eqz v0, :cond_4

    .line 132
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 129
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 136
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .end local v1    # "i":I
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 137
    return-void
.end method
