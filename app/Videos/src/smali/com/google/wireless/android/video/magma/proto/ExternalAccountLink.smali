.class public final Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ExternalAccountLink.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;
    }
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;


# instance fields
.field public consentDialogFrequencySec:J

.field public linkReconsentTimestampSec:J

.field public linked:Z

.field public partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 154
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->clear()Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    .line 155
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    .locals 2

    .prologue
    .line 130
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    if-nez v0, :cond_1

    .line 131
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 133
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    if-nez v0, :cond_0

    .line 134
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    .line 136
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    return-object v0

    .line 136
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linked:Z

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    .line 160
    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linkReconsentTimestampSec:J

    .line 161
    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->consentDialogFrequencySec:J

    .line 162
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->cachedSize:I

    .line 163
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 229
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 230
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linked:Z

    if-eqz v1, :cond_0

    .line 231
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linked:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 234
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    if-eqz v1, :cond_1

    .line 235
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 238
    :cond_1
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linkReconsentTimestampSec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 239
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linkReconsentTimestampSec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 242
    :cond_2
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->consentDialogFrequencySec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 243
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->consentDialogFrequencySec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 246
    :cond_3
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 168
    if-ne p1, p0, :cond_1

    .line 193
    :cond_0
    :goto_0
    return v1

    .line 171
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    if-nez v3, :cond_2

    move v1, v2

    .line 172
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 174
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    .line 175
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linked:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linked:Z

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 176
    goto :goto_0

    .line 178
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    if-nez v3, :cond_4

    .line 179
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    if-eqz v3, :cond_5

    move v1, v2

    .line 180
    goto :goto_0

    .line 183
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 184
    goto :goto_0

    .line 187
    :cond_5
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linkReconsentTimestampSec:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linkReconsentTimestampSec:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_6

    move v1, v2

    .line 188
    goto :goto_0

    .line 190
    :cond_6
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->consentDialogFrequencySec:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->consentDialogFrequencySec:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    .line 191
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 198
    const/16 v0, 0x11

    .line 199
    .local v0, "result":I
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linked:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x4cf

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 200
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    add-int v0, v2, v1

    .line 202
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linkReconsentTimestampSec:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linkReconsentTimestampSec:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 204
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->consentDialogFrequencySec:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->consentDialogFrequencySec:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 206
    return v0

    .line 199
    :cond_0
    const/16 v1, 0x4d5

    goto :goto_0

    .line 200
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 254
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 255
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 259
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 260
    :sswitch_0
    return-object p0

    .line 265
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linked:Z

    goto :goto_0

    .line 269
    :sswitch_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    if-nez v1, :cond_1

    .line 270
    new-instance v1, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    invoke-direct {v1}, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    .line 272
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 276
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linkReconsentTimestampSec:J

    goto :goto_0

    .line 280
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->consentDialogFrequencySec:J

    goto :goto_0

    .line 255
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 212
    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linked:Z

    if-eqz v0, :cond_0

    .line 213
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linked:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    if-eqz v0, :cond_1

    .line 216
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->partner:Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 218
    :cond_1
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linkReconsentTimestampSec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 219
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->linkReconsentTimestampSec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 221
    :cond_2
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->consentDialogFrequencySec:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 222
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->consentDialogFrequencySec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 224
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 225
    return-void
.end method
