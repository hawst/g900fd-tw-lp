.class public final Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VideoResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/VideoResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Format"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;


# instance fields
.field public url:Ljava/lang/String;

.field public value:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 33
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->clear()Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    .line 34
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    .locals 2

    .prologue
    .line 15
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    if-nez v0, :cond_1

    .line 16
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 18
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    if-nez v0, :cond_0

    .line 19
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    .line 21
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    return-object v0

    .line 21
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->value:I

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->url:Ljava/lang/String;

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->cachedSize:I

    .line 40
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 88
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 89
    .local v0, "size":I
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->value:I

    if-eqz v1, :cond_0

    .line 90
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->value:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->url:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 94
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->url:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45
    if-ne p1, p0, :cond_1

    .line 62
    :cond_0
    :goto_0
    return v1

    .line 48
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    if-nez v3, :cond_2

    move v1, v2

    .line 49
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 51
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    .line 52
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->value:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->value:I

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 53
    goto :goto_0

    .line 55
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->url:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 56
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->url:Ljava/lang/String;

    if-eqz v3, :cond_0

    move v1, v2

    .line 57
    goto :goto_0

    .line 59
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->url:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->url:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 60
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 67
    const/16 v0, 0x11

    .line 68
    .local v0, "result":I
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->value:I

    add-int/lit16 v0, v1, 0x20f

    .line 69
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->url:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 71
    return v0

    .line 69
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->url:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 105
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 106
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 110
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 111
    :sswitch_0
    return-object p0

    .line 116
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->value:I

    goto :goto_0

    .line 120
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->url:Ljava/lang/String;

    goto :goto_0

    .line 106
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 77
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->value:I

    if-eqz v0, :cond_0

    .line 78
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->value:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->url:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 81
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Format;->url:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 83
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 84
    return-void
.end method
