.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Metadata"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;,
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;,
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    }
.end annotation


# instance fields
.field public audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

.field public bonusContent:Z

.field public broadcaster:[Ljava/lang/String;

.field public captionDefaultLanguage:Ljava/lang/String;

.field public captionMode:I

.field public categoryId:[Ljava/lang/String;

.field public contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

.field public credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

.field public description:Ljava/lang/String;

.field public durationSec:I

.field public hasCaption:Z

.field public images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

.field public poster:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

.field public releaseDateTimestampSec:J

.field public sequenceNumber:Ljava/lang/String;

.field public startOfCreditSec:I

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 636
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 637
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->clear()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .line 638
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 641
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    .line 642
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description:Ljava/lang/String;

    .line 643
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec:J

    .line 644
    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    .line 645
    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->startOfCreditSec:I

    .line 646
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    .line 647
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    .line 648
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    .line 649
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    .line 650
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    .line 651
    iput-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasCaption:Z

    .line 652
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionMode:I

    .line 653
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionDefaultLanguage:Ljava/lang/String;

    .line 654
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    .line 655
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->categoryId:[Ljava/lang/String;

    .line 656
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 657
    iput-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->bonusContent:Z

    .line 658
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->cachedSize:I

    .line 659
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 877
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 878
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 879
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    invoke-static {v10, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 882
    :cond_0
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 883
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 886
    :cond_1
    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    .line 887
    const/4 v5, 0x3

    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec:J

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 890
    :cond_2
    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    if-eqz v5, :cond_3

    .line 891
    const/4 v5, 0x4

    iget v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 894
    :cond_3
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v5, v5

    if-lez v5, :cond_5

    .line 895
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v5, v5

    if-ge v3, v5, :cond_5

    .line 896
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    aget-object v2, v5, v3

    .line 897
    .local v2, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    if-eqz v2, :cond_4

    .line 898
    const/4 v5, 0x5

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 895
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 903
    .end local v2    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .end local v3    # "i":I
    :cond_5
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v5, v5

    if-lez v5, :cond_7

    .line 904
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v5, v5

    if-ge v3, v5, :cond_7

    .line 905
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    aget-object v2, v5, v3

    .line 906
    .restart local v2    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    if-eqz v2, :cond_6

    .line 907
    const/4 v5, 0x6

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 904
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 912
    .end local v2    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .end local v3    # "i":I
    :cond_7
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 913
    const/4 v5, 0x7

    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 916
    :cond_8
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    array-length v5, v5

    if-lez v5, :cond_a

    .line 917
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    array-length v5, v5

    if-ge v3, v5, :cond_a

    .line 918
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    aget-object v2, v5, v3

    .line 919
    .local v2, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    if-eqz v2, :cond_9

    .line 920
    const/16 v5, 0x8

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 917
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 925
    .end local v2    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    .end local v3    # "i":I
    :cond_a
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    if-eqz v5, :cond_c

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    array-length v5, v5

    if-lez v5, :cond_c

    .line 926
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    array-length v5, v5

    if-ge v3, v5, :cond_c

    .line 927
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    aget-object v2, v5, v3

    .line 928
    .local v2, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;
    if-eqz v2, :cond_b

    .line 929
    const/16 v5, 0x9

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 926
    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 934
    .end local v2    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;
    .end local v3    # "i":I
    :cond_c
    iget-boolean v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasCaption:Z

    if-eqz v5, :cond_d

    .line 935
    const/16 v5, 0xa

    iget-boolean v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasCaption:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 938
    :cond_d
    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionMode:I

    if-eq v5, v10, :cond_e

    .line 939
    const/16 v5, 0xb

    iget v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionMode:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 942
    :cond_e
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionDefaultLanguage:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_f

    .line 943
    const/16 v5, 0xc

    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionDefaultLanguage:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 946
    :cond_f
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    if-eqz v5, :cond_12

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_12

    .line 947
    const/4 v0, 0x0

    .line 948
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 949
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_4
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_11

    .line 950
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 951
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_10

    .line 952
    add-int/lit8 v0, v0, 0x1

    .line 953
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 949
    :cond_10
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 957
    .end local v2    # "element":Ljava/lang/String;
    :cond_11
    add-int/2addr v4, v1

    .line 958
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 960
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_12
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->categoryId:[Ljava/lang/String;

    if-eqz v5, :cond_15

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->categoryId:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_15

    .line 961
    const/4 v0, 0x0

    .line 962
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 963
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_5
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->categoryId:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_14

    .line 964
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->categoryId:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 965
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_13

    .line 966
    add-int/lit8 v0, v0, 0x1

    .line 967
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 963
    :cond_13
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 971
    .end local v2    # "element":Ljava/lang/String;
    :cond_14
    add-int/2addr v4, v1

    .line 972
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 974
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_15
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-eqz v5, :cond_17

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    array-length v5, v5

    if-lez v5, :cond_17

    .line 975
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_6
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    array-length v5, v5

    if-ge v3, v5, :cond_17

    .line 976
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    aget-object v2, v5, v3

    .line 977
    .local v2, "element":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    if-eqz v2, :cond_16

    .line 978
    const/16 v5, 0xf

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 975
    :cond_16
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 983
    .end local v2    # "element":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .end local v3    # "i":I
    :cond_17
    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->startOfCreditSec:I

    if-eqz v5, :cond_18

    .line 984
    const/16 v5, 0x10

    iget v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->startOfCreditSec:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 987
    :cond_18
    iget-boolean v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->bonusContent:Z

    if-eqz v5, :cond_19

    .line 988
    const/16 v5, 0x11

    iget-boolean v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->bonusContent:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 991
    :cond_19
    return v4
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 664
    if-ne p1, p0, :cond_1

    .line 745
    :cond_0
    :goto_0
    return v1

    .line 667
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-nez v3, :cond_2

    move v1, v2

    .line 668
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 670
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .line 671
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 672
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 673
    goto :goto_0

    .line 675
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 676
    goto :goto_0

    .line 678
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 679
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description:Ljava/lang/String;

    if-eqz v3, :cond_6

    move v1, v2

    .line 680
    goto :goto_0

    .line 682
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 683
    goto :goto_0

    .line 685
    :cond_6
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_7

    move v1, v2

    .line 686
    goto :goto_0

    .line 688
    :cond_7
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 689
    goto :goto_0

    .line 691
    :cond_8
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->startOfCreditSec:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->startOfCreditSec:I

    if-eq v3, v4, :cond_9

    move v1, v2

    .line 692
    goto :goto_0

    .line 694
    :cond_9
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 696
    goto :goto_0

    .line 698
    :cond_a
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    move v1, v2

    .line 700
    goto :goto_0

    .line 702
    :cond_b
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    if-nez v3, :cond_c

    .line 703
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    if-eqz v3, :cond_d

    move v1, v2

    .line 704
    goto :goto_0

    .line 706
    :cond_c
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    move v1, v2

    .line 707
    goto :goto_0

    .line 709
    :cond_d
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    move v1, v2

    .line 711
    goto/16 :goto_0

    .line 713
    :cond_e
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    move v1, v2

    .line 715
    goto/16 :goto_0

    .line 717
    :cond_f
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasCaption:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasCaption:Z

    if-eq v3, v4, :cond_10

    move v1, v2

    .line 718
    goto/16 :goto_0

    .line 720
    :cond_10
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionMode:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionMode:I

    if-eq v3, v4, :cond_11

    move v1, v2

    .line 721
    goto/16 :goto_0

    .line 723
    :cond_11
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionDefaultLanguage:Ljava/lang/String;

    if-nez v3, :cond_12

    .line 724
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionDefaultLanguage:Ljava/lang/String;

    if-eqz v3, :cond_13

    move v1, v2

    .line 725
    goto/16 :goto_0

    .line 727
    :cond_12
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionDefaultLanguage:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionDefaultLanguage:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    move v1, v2

    .line 728
    goto/16 :goto_0

    .line 730
    :cond_13
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    move v1, v2

    .line 732
    goto/16 :goto_0

    .line 734
    :cond_14
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->categoryId:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->categoryId:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_15

    move v1, v2

    .line 736
    goto/16 :goto_0

    .line 738
    :cond_15
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16

    move v1, v2

    .line 740
    goto/16 :goto_0

    .line 742
    :cond_16
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->bonusContent:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->bonusContent:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 743
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 10

    .prologue
    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    const/4 v2, 0x0

    .line 750
    const/16 v0, 0x11

    .line 751
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 753
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v5, v1

    .line 755
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec:J

    iget-wide v8, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec:J

    const/16 v5, 0x20

    ushr-long/2addr v8, v5

    xor-long/2addr v6, v8

    long-to-int v5, v6

    add-int v0, v1, v5

    .line 757
    mul-int/lit8 v1, v0, 0x1f

    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    add-int v0, v1, v5

    .line 758
    mul-int/lit8 v1, v0, 0x1f

    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->startOfCreditSec:I

    add-int v0, v1, v5

    .line 759
    mul-int/lit8 v1, v0, 0x1f

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-static {v5}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v5

    add-int v0, v1, v5

    .line 761
    mul-int/lit8 v1, v0, 0x1f

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-static {v5}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v5

    add-int v0, v1, v5

    .line 763
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v5, v1

    .line 765
    mul-int/lit8 v1, v0, 0x1f

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    invoke-static {v5}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v5

    add-int v0, v1, v5

    .line 767
    mul-int/lit8 v1, v0, 0x1f

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    invoke-static {v5}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v5

    add-int v0, v1, v5

    .line 769
    mul-int/lit8 v5, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasCaption:Z

    if-eqz v1, :cond_3

    move v1, v3

    :goto_3
    add-int v0, v5, v1

    .line 770
    mul-int/lit8 v1, v0, 0x1f

    iget v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionMode:I

    add-int v0, v1, v5

    .line 771
    mul-int/lit8 v1, v0, 0x1f

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionDefaultLanguage:Ljava/lang/String;

    if-nez v5, :cond_4

    :goto_4
    add-int v0, v1, v2

    .line 773
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 775
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->categoryId:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 777
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 779
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->bonusContent:Z

    if-eqz v2, :cond_5

    :goto_5
    add-int v0, v1, v3

    .line 780
    return v0

    .line 751
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_0

    .line 753
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto/16 :goto_1

    .line 763
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    move v1, v4

    .line 769
    goto :goto_3

    .line 771
    :cond_4
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionDefaultLanguage:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    move v3, v4

    .line 779
    goto :goto_5
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 999
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1000
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1004
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1005
    :sswitch_0
    return-object p0

    .line 1010
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    goto :goto_0

    .line 1014
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description:Ljava/lang/String;

    goto :goto_0

    .line 1018
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec:J

    goto :goto_0

    .line 1022
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    goto :goto_0

    .line 1026
    :sswitch_5
    const/16 v6, 0x2a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1028
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-nez v6, :cond_2

    move v1, v5

    .line 1029
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    .line 1031
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    if-eqz v1, :cond_1

    .line 1032
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1034
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 1035
    new-instance v6, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-direct {v6}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;-><init>()V

    aput-object v6, v2, v1

    .line 1036
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1037
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1034
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1028
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    :cond_2
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v1, v6

    goto :goto_1

    .line 1040
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    :cond_3
    new-instance v6, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-direct {v6}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;-><init>()V

    aput-object v6, v2, v1

    .line 1041
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1042
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    goto :goto_0

    .line 1046
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    :sswitch_6
    const/16 v6, 0x32

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1048
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-nez v6, :cond_5

    move v1, v5

    .line 1049
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    .line 1051
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    if-eqz v1, :cond_4

    .line 1052
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1054
    :cond_4
    :goto_4
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_6

    .line 1055
    new-instance v6, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-direct {v6}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;-><init>()V

    aput-object v6, v2, v1

    .line 1056
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1057
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1054
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1048
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    :cond_5
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v1, v6

    goto :goto_3

    .line 1060
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    :cond_6
    new-instance v6, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    invoke-direct {v6}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;-><init>()V

    aput-object v6, v2, v1

    .line 1061
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1062
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    goto/16 :goto_0

    .line 1066
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    goto/16 :goto_0

    .line 1070
    :sswitch_8
    const/16 v6, 0x42

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1072
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    if-nez v6, :cond_8

    move v1, v5

    .line 1073
    .restart local v1    # "i":I
    :goto_5
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    .line 1075
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    if-eqz v1, :cond_7

    .line 1076
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1078
    :cond_7
    :goto_6
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_9

    .line 1079
    new-instance v6, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    invoke-direct {v6}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;-><init>()V

    aput-object v6, v2, v1

    .line 1080
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1081
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1078
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1072
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    :cond_8
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    array-length v1, v6

    goto :goto_5

    .line 1084
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    :cond_9
    new-instance v6, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    invoke-direct {v6}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;-><init>()V

    aput-object v6, v2, v1

    .line 1085
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1086
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    goto/16 :goto_0

    .line 1090
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    :sswitch_9
    const/16 v6, 0x4a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1092
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    if-nez v6, :cond_b

    move v1, v5

    .line 1093
    .restart local v1    # "i":I
    :goto_7
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    .line 1095
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;
    if-eqz v1, :cond_a

    .line 1096
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1098
    :cond_a
    :goto_8
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_c

    .line 1099
    new-instance v6, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    invoke-direct {v6}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;-><init>()V

    aput-object v6, v2, v1

    .line 1100
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1101
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1098
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 1092
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;
    :cond_b
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    array-length v1, v6

    goto :goto_7

    .line 1104
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;
    :cond_c
    new-instance v6, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    invoke-direct {v6}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;-><init>()V

    aput-object v6, v2, v1

    .line 1105
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1106
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    goto/16 :goto_0

    .line 1110
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasCaption:Z

    goto/16 :goto_0

    .line 1114
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 1115
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto/16 :goto_0

    .line 1119
    :pswitch_0
    iput v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionMode:I

    goto/16 :goto_0

    .line 1125
    .end local v4    # "value":I
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionDefaultLanguage:Ljava/lang/String;

    goto/16 :goto_0

    .line 1129
    :sswitch_d
    const/16 v6, 0x6a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1131
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    if-nez v6, :cond_e

    move v1, v5

    .line 1132
    .restart local v1    # "i":I
    :goto_9
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 1133
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_d

    .line 1134
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1136
    :cond_d
    :goto_a
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_f

    .line 1137
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 1138
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1136
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 1131
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_e
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_9

    .line 1141
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 1142
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    goto/16 :goto_0

    .line 1146
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_e
    const/16 v6, 0x72

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1148
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->categoryId:[Ljava/lang/String;

    if-nez v6, :cond_11

    move v1, v5

    .line 1149
    .restart local v1    # "i":I
    :goto_b
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 1150
    .restart local v2    # "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_10

    .line 1151
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->categoryId:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1153
    :cond_10
    :goto_c
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_12

    .line 1154
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 1155
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1153
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 1148
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_11
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->categoryId:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_b

    .line 1158
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 1159
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->categoryId:[Ljava/lang/String;

    goto/16 :goto_0

    .line 1163
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_f
    const/16 v6, 0x7a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1165
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-nez v6, :cond_14

    move v1, v5

    .line 1166
    .restart local v1    # "i":I
    :goto_d
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 1168
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    if-eqz v1, :cond_13

    .line 1169
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1171
    :cond_13
    :goto_e
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_15

    .line 1172
    new-instance v6, Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-direct {v6}, Lcom/google/wireless/android/video/magma/proto/AudioInfo;-><init>()V

    aput-object v6, v2, v1

    .line 1173
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1174
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1171
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 1165
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    :cond_14
    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    array-length v1, v6

    goto :goto_d

    .line 1177
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    :cond_15
    new-instance v6, Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-direct {v6}, Lcom/google/wireless/android/video/magma/proto/AudioInfo;-><init>()V

    aput-object v6, v2, v1

    .line 1178
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1179
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    goto/16 :goto_0

    .line 1183
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->startOfCreditSec:I

    goto/16 :goto_0

    .line 1187
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->bonusContent:Z

    goto/16 :goto_0

    .line 1000
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
    .end sparse-switch

    .line 1115
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 7
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 786
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 787
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    invoke-virtual {p1, v6, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 789
    :cond_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 790
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 792
    :cond_1
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 793
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 795
    :cond_2
    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    if-eqz v2, :cond_3

    .line 796
    const/4 v2, 0x4

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 798
    :cond_3
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 799
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 800
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->poster:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    aget-object v0, v2, v1

    .line 801
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    if-eqz v0, :cond_4

    .line 802
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 799
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 806
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .end local v1    # "i":I
    :cond_5
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 807
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 808
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    aget-object v0, v2, v1

    .line 809
    .restart local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    if-eqz v0, :cond_6

    .line 810
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 807
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 814
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;
    .end local v1    # "i":I
    :cond_7
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 815
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 817
    :cond_8
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 818
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    array-length v2, v2

    if-ge v1, v2, :cond_a

    .line 819
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    aget-object v0, v2, v1

    .line 820
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    if-eqz v0, :cond_9

    .line 821
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 818
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 825
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    .end local v1    # "i":I
    :cond_a
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    array-length v2, v2

    if-lez v2, :cond_c

    .line 826
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    array-length v2, v2

    if-ge v1, v2, :cond_c

    .line 827
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    aget-object v0, v2, v1

    .line 828
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;
    if-eqz v0, :cond_b

    .line 829
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 826
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 833
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;
    .end local v1    # "i":I
    :cond_c
    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasCaption:Z

    if-eqz v2, :cond_d

    .line 834
    const/16 v2, 0xa

    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasCaption:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 836
    :cond_d
    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionMode:I

    if-eq v2, v6, :cond_e

    .line 837
    const/16 v2, 0xb

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionMode:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 839
    :cond_e
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionDefaultLanguage:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 840
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionDefaultLanguage:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 842
    :cond_f
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_11

    .line 843
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_11

    .line 844
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 845
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_10

    .line 846
    const/16 v2, 0xd

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 843
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 850
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_11
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->categoryId:[Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->categoryId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_13

    .line 851
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_5
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->categoryId:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_13

    .line 852
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->categoryId:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 853
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_12

    .line 854
    const/16 v2, 0xe

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 851
    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 858
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_13
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    array-length v2, v2

    if-lez v2, :cond_15

    .line 859
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_6
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    array-length v2, v2

    if-ge v1, v2, :cond_15

    .line 860
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    aget-object v0, v2, v1

    .line 861
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    if-eqz v0, :cond_14

    .line 862
    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 859
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 866
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .end local v1    # "i":I
    :cond_15
    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->startOfCreditSec:I

    if-eqz v2, :cond_16

    .line 867
    const/16 v2, 0x10

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->startOfCreditSec:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 869
    :cond_16
    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->bonusContent:Z

    if-eqz v2, :cond_17

    .line 870
    const/16 v2, 0x11

    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->bonusContent:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 872
    :cond_17
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 873
    return-void
.end method
