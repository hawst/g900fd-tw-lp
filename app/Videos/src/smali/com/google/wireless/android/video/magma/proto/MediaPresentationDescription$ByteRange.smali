.class public final Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;
.super Lcom/google/protobuf/nano/MessageNano;
.source "MediaPresentationDescription.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ByteRange"
.end annotation


# instance fields
.field public first:J

.field public last:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 422
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 423
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->clear()Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    .line 424
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 427
    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->first:J

    .line 428
    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->last:J

    .line 429
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->cachedSize:I

    .line 430
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 475
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 476
    .local v0, "size":I
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->first:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 477
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->first:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 480
    :cond_0
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->last:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 481
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->last:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 484
    :cond_1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 435
    if-ne p1, p0, :cond_1

    .line 448
    :cond_0
    :goto_0
    return v1

    .line 438
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    if-nez v3, :cond_2

    move v1, v2

    .line 439
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 441
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    .line 442
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->first:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->first:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    move v1, v2

    .line 443
    goto :goto_0

    .line 445
    :cond_3
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->last:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->last:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    move v1, v2

    .line 446
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 453
    const/16 v0, 0x11

    .line 454
    .local v0, "result":I
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->first:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->first:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/lit16 v0, v1, 0x20f

    .line 456
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->last:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->last:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 458
    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 399
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 492
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 493
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 497
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 498
    :sswitch_0
    return-object p0

    .line 503
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->first:J

    goto :goto_0

    .line 507
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->last:J

    goto :goto_0

    .line 493
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 464
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->first:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 465
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->first:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 467
    :cond_0
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->last:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 468
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$ByteRange;->last:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 470
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 471
    return-void
.end method
