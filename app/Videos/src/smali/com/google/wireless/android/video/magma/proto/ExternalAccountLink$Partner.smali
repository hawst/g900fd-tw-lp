.class public final Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ExternalAccountLink.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Partner"
.end annotation


# instance fields
.field public id:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 36
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;->clear()Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    .line 37
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;->id:I

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;->cachedSize:I

    .line 42
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 78
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 79
    .local v0, "size":I
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;->id:I

    if-eqz v1, :cond_0

    .line 80
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;->id:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 83
    :cond_0
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 47
    if-ne p1, p0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return v1

    .line 50
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    if-nez v3, :cond_2

    move v1, v2

    .line 51
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 53
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    .line 54
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;->id:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;->id:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 55
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 62
    const/16 v0, 0x11

    .line 63
    .local v0, "result":I
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;->id:I

    add-int/lit16 v0, v1, 0x20f

    .line 64
    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 91
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 92
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 96
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 97
    :sswitch_0
    return-object p0

    .line 102
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 103
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 106
    :pswitch_0
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;->id:I

    goto :goto_0

    .line 92
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch

    .line 103
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;->id:I

    if-eqz v0, :cond_0

    .line 71
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink$Partner;->id:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 73
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 74
    return-void
.end method
