.class public final Lcom/google/wireless/android/video/magma/proto/VideoFormat;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VideoFormat.java"


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;


# instance fields
.field public audioChannels:I

.field public dash:Z

.field public drmType:I

.field public height:I

.field public itag:I

.field public multi:Z

.field public width:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 53
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->clear()Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    .line 54
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    .locals 2

    .prologue
    .line 20
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    if-nez v0, :cond_1

    .line 21
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 23
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    if-nez v0, :cond_0

    .line 24
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    .line 26
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    return-object v0

    .line 26
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->itag:I

    .line 58
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->audioChannels:I

    .line 59
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->drmType:I

    .line 60
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->width:I

    .line 61
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->height:I

    .line 62
    iput-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->dash:Z

    .line 63
    iput-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->multi:Z

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->cachedSize:I

    .line 65
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 143
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 144
    .local v0, "size":I
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->itag:I

    if-eqz v1, :cond_0

    .line 145
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->itag:I

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 148
    :cond_0
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->audioChannels:I

    if-eqz v1, :cond_1

    .line 149
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->audioChannels:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 152
    :cond_1
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->drmType:I

    if-eq v1, v3, :cond_2

    .line 153
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->drmType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 156
    :cond_2
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->width:I

    if-eqz v1, :cond_3

    .line 157
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->width:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 160
    :cond_3
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->height:I

    if-eqz v1, :cond_4

    .line 161
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->height:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 164
    :cond_4
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->dash:Z

    if-eqz v1, :cond_5

    .line 165
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->dash:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    :cond_5
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->multi:Z

    if-eqz v1, :cond_6

    .line 169
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->multi:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    :cond_6
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    if-ne p1, p0, :cond_1

    .line 98
    :cond_0
    :goto_0
    return v1

    .line 73
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    if-nez v3, :cond_2

    move v1, v2

    .line 74
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 76
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    .line 77
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->itag:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->itag:I

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 78
    goto :goto_0

    .line 80
    :cond_3
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->audioChannels:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->audioChannels:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 81
    goto :goto_0

    .line 83
    :cond_4
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->drmType:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->drmType:I

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 84
    goto :goto_0

    .line 86
    :cond_5
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->width:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->width:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 87
    goto :goto_0

    .line 89
    :cond_6
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->height:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->height:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 90
    goto :goto_0

    .line 92
    :cond_7
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->dash:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->dash:Z

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 93
    goto :goto_0

    .line 95
    :cond_8
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->multi:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->multi:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 96
    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    .line 103
    const/16 v0, 0x11

    .line 104
    .local v0, "result":I
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->itag:I

    add-int/lit16 v0, v1, 0x20f

    .line 105
    mul-int/lit8 v1, v0, 0x1f

    iget v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->audioChannels:I

    add-int v0, v1, v4

    .line 106
    mul-int/lit8 v1, v0, 0x1f

    iget v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->drmType:I

    add-int v0, v1, v4

    .line 107
    mul-int/lit8 v1, v0, 0x1f

    iget v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->width:I

    add-int v0, v1, v4

    .line 108
    mul-int/lit8 v1, v0, 0x1f

    iget v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->height:I

    add-int v0, v1, v4

    .line 109
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->dash:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v4, v1

    .line 110
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->multi:Z

    if-eqz v4, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 111
    return v0

    :cond_0
    move v1, v3

    .line 109
    goto :goto_0

    :cond_1
    move v2, v3

    .line 110
    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 180
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 181
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 185
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 186
    :sswitch_0
    return-object p0

    .line 191
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->itag:I

    goto :goto_0

    .line 195
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->audioChannels:I

    goto :goto_0

    .line 199
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 200
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 205
    :pswitch_1
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->drmType:I

    goto :goto_0

    .line 211
    .end local v1    # "value":I
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->width:I

    goto :goto_0

    .line 215
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->height:I

    goto :goto_0

    .line 219
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->dash:Z

    goto :goto_0

    .line 223
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->multi:Z

    goto :goto_0

    .line 181
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch

    .line 200
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 117
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->itag:I

    if-eqz v0, :cond_0

    .line 118
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->itag:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 120
    :cond_0
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->audioChannels:I

    if-eqz v0, :cond_1

    .line 121
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->audioChannels:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 123
    :cond_1
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->drmType:I

    if-eq v0, v2, :cond_2

    .line 124
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->drmType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 126
    :cond_2
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->width:I

    if-eqz v0, :cond_3

    .line 127
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->width:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 129
    :cond_3
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->height:I

    if-eqz v0, :cond_4

    .line 130
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->height:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 132
    :cond_4
    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->dash:Z

    if-eqz v0, :cond_5

    .line 133
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->dash:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 135
    :cond_5
    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->multi:Z

    if-eqz v0, :cond_6

    .line 136
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->multi:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 138
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 139
    return-void
.end method
