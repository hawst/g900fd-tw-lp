.class public final Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
.super Lcom/google/protobuf/nano/MessageNano;
.source "MediaPresentationDescription.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Caption"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;


# instance fields
.field public draft:Z

.field public forced:Z

.field public format:J

.field public formatName:Ljava/lang/String;

.field public lang:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 564
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 565
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->clear()Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    .line 566
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    .locals 2

    .prologue
    .line 532
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    if-nez v0, :cond_1

    .line 533
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 535
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    if-nez v0, :cond_0

    .line 536
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    .line 538
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 540
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    return-object v0

    .line 538
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static parseFrom([B)Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 757
    new-instance v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    return-object v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 569
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->lang:Ljava/lang/String;

    .line 570
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->format:J

    .line 571
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->formatName:Ljava/lang/String;

    .line 572
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->url:Ljava/lang/String;

    .line 573
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->name:Ljava/lang/String;

    .line 574
    iput-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->draft:Z

    .line 575
    iput-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->forced:Z

    .line 576
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->cachedSize:I

    .line 577
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 676
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 677
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->lang:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 678
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->lang:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 681
    :cond_0
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->format:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 682
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->format:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 685
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->url:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 686
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->url:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 689
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 690
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 693
    :cond_3
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->formatName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 694
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->formatName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 697
    :cond_4
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->draft:Z

    if-eqz v1, :cond_5

    .line 698
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->draft:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 701
    :cond_5
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->forced:Z

    if-eqz v1, :cond_6

    .line 702
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->forced:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 705
    :cond_6
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 582
    if-ne p1, p0, :cond_1

    .line 626
    :cond_0
    :goto_0
    return v1

    .line 585
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    if-nez v3, :cond_2

    move v1, v2

    .line 586
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 588
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    .line 589
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->lang:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 590
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->lang:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 591
    goto :goto_0

    .line 593
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->lang:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->lang:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 594
    goto :goto_0

    .line 596
    :cond_4
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->format:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->format:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_5

    move v1, v2

    .line 597
    goto :goto_0

    .line 599
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->formatName:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 600
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->formatName:Ljava/lang/String;

    if-eqz v3, :cond_7

    move v1, v2

    .line 601
    goto :goto_0

    .line 603
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->formatName:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->formatName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 604
    goto :goto_0

    .line 606
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->url:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 607
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->url:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v1, v2

    .line 608
    goto :goto_0

    .line 610
    :cond_8
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->url:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->url:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 611
    goto :goto_0

    .line 613
    :cond_9
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->name:Ljava/lang/String;

    if-nez v3, :cond_a

    .line 614
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->name:Ljava/lang/String;

    if-eqz v3, :cond_b

    move v1, v2

    .line 615
    goto :goto_0

    .line 617
    :cond_a
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    move v1, v2

    .line 618
    goto :goto_0

    .line 620
    :cond_b
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->draft:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->draft:Z

    if-eq v3, v4, :cond_c

    move v1, v2

    .line 621
    goto :goto_0

    .line 623
    :cond_c
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->forced:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->forced:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 624
    goto :goto_0
.end method

.method public hashCode()I
    .locals 10

    .prologue
    const/16 v4, 0x4d5

    const/16 v3, 0x4cf

    const/4 v2, 0x0

    .line 631
    const/16 v0, 0x11

    .line 632
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->lang:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 634
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->format:J

    iget-wide v8, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->format:J

    const/16 v5, 0x20

    ushr-long/2addr v8, v5

    xor-long/2addr v6, v8

    long-to-int v5, v6

    add-int v0, v1, v5

    .line 636
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->formatName:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v5, v1

    .line 638
    mul-int/lit8 v5, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->url:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v5, v1

    .line 640
    mul-int/lit8 v1, v0, 0x1f

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->name:Ljava/lang/String;

    if-nez v5, :cond_3

    :goto_3
    add-int v0, v1, v2

    .line 642
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->draft:Z

    if-eqz v1, :cond_4

    move v1, v3

    :goto_4
    add-int v0, v2, v1

    .line 643
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->forced:Z

    if-eqz v2, :cond_5

    :goto_5
    add-int v0, v1, v3

    .line 644
    return v0

    .line 632
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->lang:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 636
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->formatName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 638
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->url:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 640
    :cond_3
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    move v1, v4

    .line 642
    goto :goto_4

    :cond_5
    move v3, v4

    .line 643
    goto :goto_5
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 526
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 713
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 714
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 718
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 719
    :sswitch_0
    return-object p0

    .line 724
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->lang:Ljava/lang/String;

    goto :goto_0

    .line 728
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->format:J

    goto :goto_0

    .line 732
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->url:Ljava/lang/String;

    goto :goto_0

    .line 736
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->name:Ljava/lang/String;

    goto :goto_0

    .line 740
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->formatName:Ljava/lang/String;

    goto :goto_0

    .line 744
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->draft:Z

    goto :goto_0

    .line 748
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->forced:Z

    goto :goto_0

    .line 714
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 650
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->lang:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 651
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->lang:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 653
    :cond_0
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->format:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 654
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->format:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 656
    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->url:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 657
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->url:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 659
    :cond_2
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 660
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 662
    :cond_3
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->formatName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 663
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->formatName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 665
    :cond_4
    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->draft:Z

    if-eqz v0, :cond_5

    .line 666
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->draft:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 668
    :cond_5
    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->forced:Z

    if-eqz v0, :cond_6

    .line 669
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->forced:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 671
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 672
    return-void
.end method
