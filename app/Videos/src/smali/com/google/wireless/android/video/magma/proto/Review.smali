.class public final Lcom/google/wireless/android/video/magma/proto/Review;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Review.java"


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/Review;


# instance fields
.field public author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

.field public comment:Ljava/lang/String;

.field public commentId:Ljava/lang/String;

.field public commentRating:Lcom/google/wireless/android/video/magma/proto/ViewerRating;

.field public externalUrl:Ljava/lang/String;

.field public resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

.field public starRating:I

.field public timestampMsec:J

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 51
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/Review;->clear()Lcom/google/wireless/android/video/magma/proto/Review;

    .line 52
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/Review;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/Review;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/Review;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/Review;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/Review;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/Review;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/Review;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/Review;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/Review;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/Review;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/Review;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 55
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/Review;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 56
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/Review;->title:Ljava/lang/String;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/Review;->comment:Ljava/lang/String;

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentId:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/Review;->externalUrl:Ljava/lang/String;

    .line 61
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/Review;->timestampMsec:J

    .line 62
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/Review;->starRating:I

    .line 63
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentRating:Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/Review;->cachedSize:I

    .line 65
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 199
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 200
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v1, :cond_0

    .line 201
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/Review;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    if-eqz v1, :cond_1

    .line 205
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 208
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 209
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/Review;->title:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->comment:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 213
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/Review;->comment:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_3
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 217
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    :cond_4
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->externalUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 221
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/Review;->externalUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 224
    :cond_5
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/Review;->timestampMsec:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_6

    .line 225
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/Review;->timestampMsec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 228
    :cond_6
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->starRating:I

    if-eqz v1, :cond_7

    .line 229
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/Review;->starRating:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 232
    :cond_7
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentRating:Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    if-eqz v1, :cond_8

    .line 233
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentRating:Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 236
    :cond_8
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    if-ne p1, p0, :cond_1

    .line 138
    :cond_0
    :goto_0
    return v1

    .line 73
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/Review;

    if-nez v3, :cond_2

    move v1, v2

    .line 74
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 76
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/Review;

    .line 77
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/Review;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/Review;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v3, :cond_3

    .line 78
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/Review;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v3, :cond_4

    move v1, v2

    .line 79
    goto :goto_0

    .line 82
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/Review;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/Review;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 83
    goto :goto_0

    .line 86
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    if-nez v3, :cond_5

    .line 87
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    if-eqz v3, :cond_6

    move v1, v2

    .line 88
    goto :goto_0

    .line 91
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/UserProfile;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 92
    goto :goto_0

    .line 95
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/Review;->title:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 96
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/Review;->title:Ljava/lang/String;

    if-eqz v3, :cond_8

    move v1, v2

    .line 97
    goto :goto_0

    .line 99
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/Review;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/Review;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 100
    goto :goto_0

    .line 102
    :cond_8
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/Review;->comment:Ljava/lang/String;

    if-nez v3, :cond_9

    .line 103
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/Review;->comment:Ljava/lang/String;

    if-eqz v3, :cond_a

    move v1, v2

    .line 104
    goto :goto_0

    .line 106
    :cond_9
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/Review;->comment:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/Review;->comment:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 107
    goto :goto_0

    .line 109
    :cond_a
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentId:Ljava/lang/String;

    if-nez v3, :cond_b

    .line 110
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/Review;->commentId:Ljava/lang/String;

    if-eqz v3, :cond_c

    move v1, v2

    .line 111
    goto :goto_0

    .line 113
    :cond_b
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/Review;->commentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    move v1, v2

    .line 114
    goto :goto_0

    .line 116
    :cond_c
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/Review;->externalUrl:Ljava/lang/String;

    if-nez v3, :cond_d

    .line 117
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/Review;->externalUrl:Ljava/lang/String;

    if-eqz v3, :cond_e

    move v1, v2

    .line 118
    goto/16 :goto_0

    .line 120
    :cond_d
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/Review;->externalUrl:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/Review;->externalUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    move v1, v2

    .line 121
    goto/16 :goto_0

    .line 123
    :cond_e
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/Review;->timestampMsec:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/Review;->timestampMsec:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_f

    move v1, v2

    .line 124
    goto/16 :goto_0

    .line 126
    :cond_f
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/Review;->starRating:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/Review;->starRating:I

    if-eq v3, v4, :cond_10

    move v1, v2

    .line 127
    goto/16 :goto_0

    .line 129
    :cond_10
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentRating:Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    if-nez v3, :cond_11

    .line 130
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/Review;->commentRating:Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    if-eqz v3, :cond_0

    move v1, v2

    .line 131
    goto/16 :goto_0

    .line 134
    :cond_11
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentRating:Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/Review;->commentRating:Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 135
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 143
    const/16 v0, 0x11

    .line 144
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 146
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 148
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->title:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 150
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->comment:Ljava/lang/String;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v3, v1

    .line 152
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentId:Ljava/lang/String;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    add-int v0, v3, v1

    .line 154
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->externalUrl:Ljava/lang/String;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    add-int v0, v3, v1

    .line 156
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/Review;->timestampMsec:J

    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/Review;->timestampMsec:J

    const/16 v3, 0x20

    ushr-long/2addr v6, v3

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 158
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/Review;->starRating:I

    add-int v0, v1, v3

    .line 159
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentRating:Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    if-nez v3, :cond_6

    :goto_6
    add-int v0, v1, v2

    .line 161
    return v0

    .line 144
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hashCode()I

    move-result v1

    goto :goto_0

    .line 146
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/UserProfile;->hashCode()I

    move-result v1

    goto :goto_1

    .line 148
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->title:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 150
    :cond_3
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->comment:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    .line 152
    :cond_4
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    .line 154
    :cond_5
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->externalUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    .line 159
    :cond_6
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentRating:Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->hashCode()I

    move-result v2

    goto :goto_6
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/Review;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/Review;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/Review;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 244
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 245
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 249
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 250
    :sswitch_0
    return-object p0

    .line 255
    :sswitch_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v1, :cond_1

    .line 256
    new-instance v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-direct {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 258
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 262
    :sswitch_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    if-nez v1, :cond_2

    .line 263
    new-instance v1, Lcom/google/wireless/android/video/magma/proto/UserProfile;

    invoke-direct {v1}, Lcom/google/wireless/android/video/magma/proto/UserProfile;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    .line 265
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 269
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->title:Ljava/lang/String;

    goto :goto_0

    .line 273
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->comment:Ljava/lang/String;

    goto :goto_0

    .line 277
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentId:Ljava/lang/String;

    goto :goto_0

    .line 281
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->externalUrl:Ljava/lang/String;

    goto :goto_0

    .line 285
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/Review;->timestampMsec:J

    goto :goto_0

    .line 289
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->starRating:I

    goto :goto_0

    .line 293
    :sswitch_9
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentRating:Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    if-nez v1, :cond_3

    .line 294
    new-instance v1, Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    invoke-direct {v1}, Lcom/google/wireless/android/video/magma/proto/ViewerRating;-><init>()V

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentRating:Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    .line 296
    :cond_3
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentRating:Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 245
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/Review;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v0, :cond_0

    .line 168
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    if-eqz v0, :cond_1

    .line 171
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/Review;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 174
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 176
    :cond_2
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/Review;->comment:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 177
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->comment:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 179
    :cond_3
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 180
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 182
    :cond_4
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/Review;->externalUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 183
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->externalUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 185
    :cond_5
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/Review;->timestampMsec:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_6

    .line 186
    const/4 v0, 0x7

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/Review;->timestampMsec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 188
    :cond_6
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/Review;->starRating:I

    if-eqz v0, :cond_7

    .line 189
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->starRating:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 191
    :cond_7
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentRating:Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    if-eqz v0, :cond_8

    .line 192
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/Review;->commentRating:Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 194
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 195
    return-void
.end method
