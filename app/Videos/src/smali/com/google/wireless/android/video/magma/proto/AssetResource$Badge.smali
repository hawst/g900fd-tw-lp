.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Badge"
.end annotation


# instance fields
.field public audio51:Z

.field public eastwood:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1913
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1914
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->clear()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    .line 1915
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1918
    iput-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->audio51:Z

    .line 1919
    iput-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->eastwood:Z

    .line 1920
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->cachedSize:I

    .line 1921
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1964
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1965
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->audio51:Z

    if-eqz v1, :cond_0

    .line 1966
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->audio51:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1969
    :cond_0
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->eastwood:Z

    if-eqz v1, :cond_1

    .line 1970
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->eastwood:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1973
    :cond_1
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1926
    if-ne p1, p0, :cond_1

    .line 1939
    :cond_0
    :goto_0
    return v1

    .line 1929
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    if-nez v3, :cond_2

    move v1, v2

    .line 1930
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1932
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    .line 1933
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->audio51:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->audio51:Z

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 1934
    goto :goto_0

    .line 1936
    :cond_3
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->eastwood:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->eastwood:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 1937
    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    .line 1944
    const/16 v0, 0x11

    .line 1945
    .local v0, "result":I
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->audio51:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 1946
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->eastwood:Z

    if-eqz v4, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 1947
    return v0

    :cond_0
    move v1, v3

    .line 1945
    goto :goto_0

    :cond_1
    move v2, v3

    .line 1946
    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1890
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1981
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1982
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1986
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1987
    :sswitch_0
    return-object p0

    .line 1992
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->audio51:Z

    goto :goto_0

    .line 1996
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->eastwood:Z

    goto :goto_0

    .line 1982
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x18 -> :sswitch_2
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1953
    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->audio51:Z

    if-eqz v0, :cond_0

    .line 1954
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->audio51:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1956
    :cond_0
    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->eastwood:Z

    if-eqz v0, :cond_1

    .line 1957
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->eastwood:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1959
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1960
    return-void
.end method
