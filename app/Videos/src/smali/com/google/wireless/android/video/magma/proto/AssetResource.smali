.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;,
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;,
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;,
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;,
        Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    }
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource;


# instance fields
.field public badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

.field public child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

.field public metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

.field public offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

.field public parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

.field public purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

.field public recommendationInfo:Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;

.field public resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

.field public trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

.field public video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

.field public viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2173
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2174
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->clear()Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 2175
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .locals 2

    .prologue
    .line 2129
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-nez v0, :cond_1

    .line 2130
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 2132
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-nez v0, :cond_0

    .line 2133
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/AssetResource;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 2135
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2137
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    return-object v0

    .line 2135
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2178
    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 2179
    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 2180
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 2181
    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .line 2182
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    .line 2183
    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    .line 2184
    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    .line 2185
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 2186
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    .line 2187
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    .line 2188
    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->recommendationInfo:Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;

    .line 2189
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->cachedSize:I

    .line 2190
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 2373
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 2374
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v3, :cond_0

    .line 2375
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2378
    :cond_0
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v3, :cond_1

    .line 2379
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2382
    :cond_1
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 2383
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 2384
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    aget-object v0, v3, v1

    .line 2385
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    if-eqz v0, :cond_2

    .line 2386
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2383
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2391
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .end local v1    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-eqz v3, :cond_4

    .line 2392
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2395
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    array-length v3, v3

    if-lez v3, :cond_6

    .line 2396
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    array-length v3, v3

    if-ge v1, v3, :cond_6

    .line 2397
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    aget-object v0, v3, v1

    .line 2398
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    if-eqz v0, :cond_5

    .line 2399
    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2396
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2404
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .end local v1    # "i":I
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    if-eqz v3, :cond_7

    .line 2405
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2408
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v3, v3

    if-lez v3, :cond_9

    .line 2409
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v3, v3

    if-ge v1, v3, :cond_9

    .line 2410
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    aget-object v0, v3, v1

    .line 2411
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    if-eqz v0, :cond_8

    .line 2412
    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2409
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2417
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .end local v1    # "i":I
    :cond_9
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    if-eqz v3, :cond_a

    .line 2418
    const/16 v3, 0x8

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2421
    :cond_a
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    array-length v3, v3

    if-lez v3, :cond_c

    .line 2422
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    array-length v3, v3

    if-ge v1, v3, :cond_c

    .line 2423
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    aget-object v0, v3, v1

    .line 2424
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    if-eqz v0, :cond_b

    .line 2425
    const/16 v3, 0x9

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2422
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2430
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    .end local v1    # "i":I
    :cond_c
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    array-length v3, v3

    if-lez v3, :cond_e

    .line 2431
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    array-length v3, v3

    if-ge v1, v3, :cond_e

    .line 2432
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    aget-object v0, v3, v1

    .line 2433
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    if-eqz v0, :cond_d

    .line 2434
    const/16 v3, 0xa

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2431
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2439
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    .end local v1    # "i":I
    :cond_e
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->recommendationInfo:Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;

    if-eqz v3, :cond_f

    .line 2440
    const/16 v3, 0xb

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->recommendationInfo:Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2443
    :cond_f
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2195
    if-ne p1, p0, :cond_1

    .line 2276
    :cond_0
    :goto_0
    return v1

    .line 2198
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-nez v3, :cond_2

    move v1, v2

    .line 2199
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 2201
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 2202
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v3, :cond_3

    .line 2203
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v3, :cond_4

    move v1, v2

    .line 2204
    goto :goto_0

    .line 2207
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 2208
    goto :goto_0

    .line 2211
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v3, :cond_5

    .line 2212
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v3, :cond_6

    move v1, v2

    .line 2213
    goto :goto_0

    .line 2216
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 2217
    goto :goto_0

    .line 2220
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 2222
    goto :goto_0

    .line 2224
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-nez v3, :cond_8

    .line 2225
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-eqz v3, :cond_9

    move v1, v2

    .line 2226
    goto :goto_0

    .line 2229
    :cond_8
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 2230
    goto :goto_0

    .line 2233
    :cond_9
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 2235
    goto :goto_0

    .line 2237
    :cond_a
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    if-nez v3, :cond_b

    .line 2238
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    if-eqz v3, :cond_c

    move v1, v2

    .line 2239
    goto :goto_0

    .line 2242
    :cond_b
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    move v1, v2

    .line 2243
    goto :goto_0

    .line 2246
    :cond_c
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    if-nez v3, :cond_d

    .line 2247
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    if-eqz v3, :cond_e

    move v1, v2

    .line 2248
    goto/16 :goto_0

    .line 2251
    :cond_d
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    move v1, v2

    .line 2252
    goto/16 :goto_0

    .line 2255
    :cond_e
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    move v1, v2

    .line 2257
    goto/16 :goto_0

    .line 2259
    :cond_f
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    move v1, v2

    .line 2261
    goto/16 :goto_0

    .line 2263
    :cond_10
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    move v1, v2

    .line 2265
    goto/16 :goto_0

    .line 2267
    :cond_11
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->recommendationInfo:Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;

    if-nez v3, :cond_12

    .line 2268
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->recommendationInfo:Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;

    if-eqz v3, :cond_0

    move v1, v2

    .line 2269
    goto/16 :goto_0

    .line 2272
    :cond_12
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->recommendationInfo:Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->recommendationInfo:Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 2273
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2281
    const/16 v0, 0x11

    .line 2282
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 2284
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 2286
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 2288
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 2290
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 2292
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v3, v1

    .line 2294
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    add-int v0, v3, v1

    .line 2296
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 2298
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 2300
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 2302
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->recommendationInfo:Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;

    if-nez v3, :cond_5

    :goto_5
    add-int v0, v1, v2

    .line 2304
    return v0

    .line 2282
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hashCode()I

    move-result v1

    goto :goto_0

    .line 2284
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->hashCode()I

    move-result v1

    goto :goto_1

    .line 2288
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hashCode()I

    move-result v1

    goto :goto_2

    .line 2292
    :cond_3
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->hashCode()I

    move-result v1

    goto :goto_3

    .line 2294
    :cond_4
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/VideoResource;->hashCode()I

    move-result v1

    goto :goto_4

    .line 2302
    :cond_5
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->recommendationInfo:Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;->hashCode()I

    move-result v2

    goto :goto_5
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2451
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 2452
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 2456
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2457
    :sswitch_0
    return-object p0

    .line 2462
    :sswitch_1
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v5, :cond_1

    .line 2463
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>()V

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 2465
    :cond_1
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2469
    :sswitch_2
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v5, :cond_2

    .line 2470
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>()V

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 2472
    :cond_2
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2476
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2478
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v5, :cond_4

    move v1, v4

    .line 2479
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 2481
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    if-eqz v1, :cond_3

    .line 2482
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2484
    :cond_3
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_5

    .line 2485
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>()V

    aput-object v5, v2, v1

    .line 2486
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2487
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2484
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2478
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :cond_4
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v1, v5

    goto :goto_1

    .line 2490
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :cond_5
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>()V

    aput-object v5, v2, v1

    .line 2491
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2492
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    goto :goto_0

    .line 2496
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :sswitch_4
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-nez v5, :cond_6

    .line 2497
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;-><init>()V

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .line 2499
    :cond_6
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2503
    :sswitch_5
    const/16 v5, 0x2a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2505
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    if-nez v5, :cond_8

    move v1, v4

    .line 2506
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    .line 2508
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    if-eqz v1, :cond_7

    .line 2509
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2511
    :cond_7
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_9

    .line 2512
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;-><init>()V

    aput-object v5, v2, v1

    .line 2513
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2514
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2511
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2505
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    :cond_8
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    array-length v1, v5

    goto :goto_3

    .line 2517
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    :cond_9
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;-><init>()V

    aput-object v5, v2, v1

    .line 2518
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2519
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    goto/16 :goto_0

    .line 2523
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    :sswitch_6
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    if-nez v5, :cond_a

    .line 2524
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/VideoResource;-><init>()V

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    .line 2526
    :cond_a
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2530
    :sswitch_7
    const/16 v5, 0x3a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2532
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v5, :cond_c

    move v1, v4

    .line 2533
    .restart local v1    # "i":I
    :goto_5
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 2535
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    if-eqz v1, :cond_b

    .line 2536
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2538
    :cond_b
    :goto_6
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_d

    .line 2539
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>()V

    aput-object v5, v2, v1

    .line 2540
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2541
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2538
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 2532
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :cond_c
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v1, v5

    goto :goto_5

    .line 2544
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :cond_d
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>()V

    aput-object v5, v2, v1

    .line 2545
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2546
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    goto/16 :goto_0

    .line 2550
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :sswitch_8
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    if-nez v5, :cond_e

    .line 2551
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;-><init>()V

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    .line 2553
    :cond_e
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2557
    :sswitch_9
    const/16 v5, 0x4a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2559
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    if-nez v5, :cond_10

    move v1, v4

    .line 2560
    .restart local v1    # "i":I
    :goto_7
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    .line 2562
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    if-eqz v1, :cond_f

    .line 2563
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2565
    :cond_f
    :goto_8
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_11

    .line 2566
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;-><init>()V

    aput-object v5, v2, v1

    .line 2567
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2568
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2565
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 2559
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    :cond_10
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    array-length v1, v5

    goto :goto_7

    .line 2571
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    :cond_11
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;-><init>()V

    aput-object v5, v2, v1

    .line 2572
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2573
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    goto/16 :goto_0

    .line 2577
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    :sswitch_a
    const/16 v5, 0x52

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2579
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    if-nez v5, :cond_13

    move v1, v4

    .line 2580
    .restart local v1    # "i":I
    :goto_9
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    .line 2582
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    if-eqz v1, :cond_12

    .line 2583
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2585
    :cond_12
    :goto_a
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_14

    .line 2586
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/ViewerRating;-><init>()V

    aput-object v5, v2, v1

    .line 2587
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2588
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2585
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 2579
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    :cond_13
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    array-length v1, v5

    goto :goto_9

    .line 2591
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    :cond_14
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/ViewerRating;-><init>()V

    aput-object v5, v2, v1

    .line 2592
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2593
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    goto/16 :goto_0

    .line 2597
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    :sswitch_b
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->recommendationInfo:Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;

    if-nez v5, :cond_15

    .line 2598
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;-><init>()V

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->recommendationInfo:Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;

    .line 2600
    :cond_15
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->recommendationInfo:Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 2452
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2310
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v2, :cond_0

    .line 2311
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2313
    :cond_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v2, :cond_1

    .line 2314
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2316
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 2317
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 2318
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->child:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    aget-object v0, v2, v1

    .line 2319
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    if-eqz v0, :cond_2

    .line 2320
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2317
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2324
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .end local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-eqz v2, :cond_4

    .line 2325
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2327
    :cond_4
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 2328
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 2329
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    aget-object v0, v2, v1

    .line 2330
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    if-eqz v0, :cond_5

    .line 2331
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2328
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2335
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .end local v1    # "i":I
    :cond_6
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    if-eqz v2, :cond_7

    .line 2336
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2338
    :cond_7
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 2339
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 2340
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    aget-object v0, v2, v1

    .line 2341
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    if-eqz v0, :cond_8

    .line 2342
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2339
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2346
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .end local v1    # "i":I
    :cond_9
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    if-eqz v2, :cond_a

    .line 2347
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2349
    :cond_a
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    array-length v2, v2

    if-lez v2, :cond_c

    .line 2350
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    array-length v2, v2

    if-ge v1, v2, :cond_c

    .line 2351
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    aget-object v0, v2, v1

    .line 2352
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    if-eqz v0, :cond_b

    .line 2353
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2350
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2357
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    .end local v1    # "i":I
    :cond_c
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    array-length v2, v2

    if-lez v2, :cond_e

    .line 2358
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    array-length v2, v2

    if-ge v1, v2, :cond_e

    .line 2359
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    aget-object v0, v2, v1

    .line 2360
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    if-eqz v0, :cond_d

    .line 2361
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2358
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2365
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    .end local v1    # "i":I
    :cond_e
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->recommendationInfo:Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;

    if-eqz v2, :cond_f

    .line 2366
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->recommendationInfo:Lcom/google/wireless/android/video/magma/proto/AssetResource$RecommendationInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2368
    :cond_f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2369
    return-void
.end method
