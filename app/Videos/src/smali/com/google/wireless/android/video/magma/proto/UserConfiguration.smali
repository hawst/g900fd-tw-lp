.class public final Lcom/google/wireless/android/video/magma/proto/UserConfiguration;
.super Lcom/google/protobuf/nano/MessageNano;
.source "UserConfiguration.java"


# instance fields
.field public accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

.field public contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

.field public countryCode:Ljava/lang/String;

.field public expirationTimeSec:J

.field public settings:[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

.field public videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 42
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->clear()Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    .line 43
    return-void
.end method

.method public static parseFrom([B)Lcom/google/wireless/android/video/magma/proto/UserConfiguration;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 315
    new-instance v0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    return-object v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/UserConfiguration;
    .locals 2

    .prologue
    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->countryCode:Ljava/lang/String;

    .line 47
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    .line 48
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/VideoFormat;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    .line 49
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->settings:[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    .line 50
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->expirationTimeSec:J

    .line 51
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    .line 52
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->cachedSize:I

    .line 53
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    .line 158
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 159
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->countryCode:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 160
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->countryCode:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 163
    :cond_0
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 164
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 165
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    aget-object v0, v3, v1

    .line 166
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
    if-eqz v0, :cond_1

    .line 167
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 164
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 172
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
    .end local v1    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    array-length v3, v3

    if-lez v3, :cond_4

    .line 173
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    array-length v3, v3

    if-ge v1, v3, :cond_4

    .line 174
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    aget-object v0, v3, v1

    .line 175
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    if-eqz v0, :cond_3

    .line 176
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 173
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 181
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    .end local v1    # "i":I
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->settings:[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->settings:[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    array-length v3, v3

    if-lez v3, :cond_6

    .line 182
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->settings:[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    array-length v3, v3

    if-ge v1, v3, :cond_6

    .line 183
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->settings:[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    aget-object v0, v3, v1

    .line 184
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;
    if-eqz v0, :cond_5

    .line 185
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 182
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 190
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;
    .end local v1    # "i":I
    :cond_6
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->expirationTimeSec:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_7

    .line 191
    const/4 v3, 0x5

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->expirationTimeSec:J

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 194
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    array-length v3, v3

    if-lez v3, :cond_9

    .line 195
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    array-length v3, v3

    if-ge v1, v3, :cond_9

    .line 196
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    aget-object v0, v3, v1

    .line 197
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    if-eqz v0, :cond_8

    .line 198
    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 195
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 203
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    .end local v1    # "i":I
    :cond_9
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    if-ne p1, p0, :cond_1

    .line 91
    :cond_0
    :goto_0
    return v1

    .line 61
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    if-nez v3, :cond_2

    move v1, v2

    .line 62
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 64
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    .line 65
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/UserConfiguration;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->countryCode:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 66
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->countryCode:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 67
    goto :goto_0

    .line 69
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->countryCode:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->countryCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 70
    goto :goto_0

    .line 72
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 74
    goto :goto_0

    .line 76
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 78
    goto :goto_0

    .line 80
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->settings:[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->settings:[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    move v1, v2

    .line 82
    goto :goto_0

    .line 84
    :cond_7
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->expirationTimeSec:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->expirationTimeSec:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_8

    move v1, v2

    .line 85
    goto :goto_0

    .line 87
    :cond_8
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 89
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 96
    const/16 v0, 0x11

    .line 97
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->countryCode:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 99
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 101
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 103
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->settings:[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 105
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->expirationTimeSec:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->expirationTimeSec:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 107
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 109
    return v0

    .line 97
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->countryCode:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/UserConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/UserConfiguration;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 211
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 212
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 216
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 217
    :sswitch_0
    return-object p0

    .line 222
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->countryCode:Ljava/lang/String;

    goto :goto_0

    .line 226
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 228
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    if-nez v5, :cond_2

    move v1, v4

    .line 229
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    .line 231
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
    if-eqz v1, :cond_1

    .line 232
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 234
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 235
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;-><init>()V

    aput-object v5, v2, v1

    .line 236
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 237
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 234
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 228
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
    :cond_2
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    array-length v1, v5

    goto :goto_1

    .line 240
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
    :cond_3
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;-><init>()V

    aput-object v5, v2, v1

    .line 241
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 242
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    goto :goto_0

    .line 246
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 248
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    if-nez v5, :cond_5

    move v1, v4

    .line 249
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    .line 251
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    if-eqz v1, :cond_4

    .line 252
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 254
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 255
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/VideoFormat;-><init>()V

    aput-object v5, v2, v1

    .line 256
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 257
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 254
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 248
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    :cond_5
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    array-length v1, v5

    goto :goto_3

    .line 260
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    :cond_6
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/VideoFormat;-><init>()V

    aput-object v5, v2, v1

    .line 261
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 262
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    goto/16 :goto_0

    .line 266
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    :sswitch_4
    const/16 v5, 0x22

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 268
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->settings:[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    if-nez v5, :cond_8

    move v1, v4

    .line 269
    .restart local v1    # "i":I
    :goto_5
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    .line 271
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;
    if-eqz v1, :cond_7

    .line 272
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->settings:[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 274
    :cond_7
    :goto_6
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_9

    .line 275
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;-><init>()V

    aput-object v5, v2, v1

    .line 276
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 277
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 274
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 268
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;
    :cond_8
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->settings:[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    array-length v1, v5

    goto :goto_5

    .line 280
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;
    :cond_9
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;-><init>()V

    aput-object v5, v2, v1

    .line 281
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 282
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->settings:[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    goto/16 :goto_0

    .line 286
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->expirationTimeSec:J

    goto/16 :goto_0

    .line 290
    :sswitch_6
    const/16 v5, 0x32

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 292
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    if-nez v5, :cond_b

    move v1, v4

    .line 293
    .restart local v1    # "i":I
    :goto_7
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    .line 295
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    if-eqz v1, :cond_a

    .line 296
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 298
    :cond_a
    :goto_8
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_c

    .line 299
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;-><init>()V

    aput-object v5, v2, v1

    .line 300
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 301
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 298
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 292
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    :cond_b
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    array-length v1, v5

    goto :goto_7

    .line 304
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    :cond_c
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;-><init>()V

    aput-object v5, v2, v1

    .line 305
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 306
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    goto/16 :goto_0

    .line 212
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->countryCode:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 116
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->countryCode:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 118
    :cond_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 119
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 120
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->contentRatingScheme:[Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;

    aget-object v0, v2, v1

    .line 121
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
    if-eqz v0, :cond_1

    .line 122
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 119
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 126
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/ContentRatingScheme;
    .end local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 127
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 128
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->videoFormat:[Lcom/google/wireless/android/video/magma/proto/VideoFormat;

    aget-object v0, v2, v1

    .line 129
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    if-eqz v0, :cond_3

    .line 130
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 127
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 134
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/VideoFormat;
    .end local v1    # "i":I
    :cond_4
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->settings:[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->settings:[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 135
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->settings:[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 136
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->settings:[Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;

    aget-object v0, v2, v1

    .line 137
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;
    if-eqz v0, :cond_5

    .line 138
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 135
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 142
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/ConfigKeyValuePair;
    .end local v1    # "i":I
    :cond_6
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->expirationTimeSec:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    .line 143
    const/4 v2, 0x5

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->expirationTimeSec:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 145
    :cond_7
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 146
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 147
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserConfiguration;->accountLink:[Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;

    aget-object v0, v2, v1

    .line 148
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    if-eqz v0, :cond_8

    .line 149
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 146
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 153
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/ExternalAccountLink;
    .end local v1    # "i":I
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 154
    return-void
.end method
