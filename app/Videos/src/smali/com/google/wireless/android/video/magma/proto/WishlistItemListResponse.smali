.class public final Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "WishlistItemListResponse.java"


# instance fields
.field public resource:[Ljava/lang/String;

.field public resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

.field public serverTimestampSec:J

.field public snapshotToken:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 36
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->clear()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    .line 37
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;
    .locals 2

    .prologue
    .line 40
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->snapshotToken:Ljava/lang/String;

    .line 42
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->serverTimestampSec:J

    .line 43
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource:[Ljava/lang/String;

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->cachedSize:I

    .line 45
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 10

    .prologue
    .line 122
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 123
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v5, v5

    if-lez v5, :cond_1

    .line 124
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 125
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    aget-object v2, v5, v3

    .line 126
    .local v2, "element":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    if-eqz v2, :cond_0

    .line 127
    const/4 v5, 0x1

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 124
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 132
    .end local v2    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .end local v3    # "i":I
    :cond_1
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->snapshotToken:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 133
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->snapshotToken:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 136
    :cond_2
    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->serverTimestampSec:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_3

    .line 137
    const/4 v5, 0x3

    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->serverTimestampSec:J

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 140
    :cond_3
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource:[Ljava/lang/String;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_6

    .line 141
    const/4 v0, 0x0

    .line 142
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 143
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_5

    .line 144
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 145
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_4

    .line 146
    add-int/lit8 v0, v0, 0x1

    .line 147
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 143
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 151
    .end local v2    # "element":Ljava/lang/String;
    :cond_5
    add-int/2addr v4, v1

    .line 152
    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    .line 154
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_6
    return v4
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 50
    if-ne p1, p0, :cond_1

    .line 75
    :cond_0
    :goto_0
    return v1

    .line 53
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    if-nez v3, :cond_2

    move v1, v2

    .line 54
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 56
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    .line 57
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    move v1, v2

    .line 59
    goto :goto_0

    .line 61
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->snapshotToken:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 62
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->snapshotToken:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    .line 63
    goto :goto_0

    .line 65
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->snapshotToken:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->snapshotToken:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 66
    goto :goto_0

    .line 68
    :cond_5
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->serverTimestampSec:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->serverTimestampSec:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_6

    move v1, v2

    .line 69
    goto :goto_0

    .line 71
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource:[Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 73
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 80
    const/16 v0, 0x11

    .line 81
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v1}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 83
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->snapshotToken:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    add-int v0, v2, v1

    .line 85
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->serverTimestampSec:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->serverTimestampSec:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 87
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 89
    return v0

    .line 83
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->snapshotToken:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 162
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 163
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 167
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 168
    :sswitch_0
    return-object p0

    .line 173
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 175
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v5, :cond_2

    move v1, v4

    .line 176
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 178
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    if-eqz v1, :cond_1

    .line 179
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 181
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 182
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>()V

    aput-object v5, v2, v1

    .line 183
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 184
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 181
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 175
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :cond_2
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v1, v5

    goto :goto_1

    .line 187
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :cond_3
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;-><init>()V

    aput-object v5, v2, v1

    .line 188
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 189
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    goto :goto_0

    .line 193
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->snapshotToken:Ljava/lang/String;

    goto :goto_0

    .line 197
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->serverTimestampSec:J

    goto :goto_0

    .line 201
    :sswitch_4
    const/16 v5, 0x1f7a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 203
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource:[Ljava/lang/String;

    if-nez v5, :cond_5

    move v1, v4

    .line 204
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 205
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 206
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 208
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 209
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 210
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 208
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 203
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_3

    .line 213
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 214
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource:[Ljava/lang/String;

    goto/16 :goto_0

    .line 163
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1f7a -> :sswitch_4
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 96
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 97
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    aget-object v0, v2, v1

    .line 98
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    if-eqz v0, :cond_0

    .line 99
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 96
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 103
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->snapshotToken:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 104
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->snapshotToken:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 106
    :cond_2
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->serverTimestampSec:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 107
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->serverTimestampSec:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 109
    :cond_3
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 110
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 111
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resource:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 112
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 113
    const/16 v2, 0x3ef

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 110
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 117
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 118
    return-void
.end method
