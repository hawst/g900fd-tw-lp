.class public final Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayMovies.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AssetInfo"
.end annotation


# instance fields
.field public assetId:Ljava/lang/String;

.field public seasonId:Ljava/lang/String;

.field public showId:Ljava/lang/String;

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 853
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 854
    invoke-virtual {p0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 855
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    .locals 1

    .prologue
    .line 858
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    .line 859
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    .line 860
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->seasonId:Ljava/lang/String;

    .line 861
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    .line 862
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->cachedSize:I

    .line 863
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 935
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 936
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 937
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 940
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 941
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 944
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->seasonId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 945
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->seasonId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 948
    :cond_2
    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    if-eqz v1, :cond_3

    .line 949
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 952
    :cond_3
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 868
    if-ne p1, p0, :cond_1

    .line 899
    :cond_0
    :goto_0
    return v1

    .line 871
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    if-nez v3, :cond_2

    move v1, v2

    .line 872
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 874
    check-cast v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 875
    .local v0, "other":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 876
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 877
    goto :goto_0

    .line 879
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 880
    goto :goto_0

    .line 882
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 883
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    if-eqz v3, :cond_6

    move v1, v2

    .line 884
    goto :goto_0

    .line 886
    :cond_5
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 887
    goto :goto_0

    .line 889
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->seasonId:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 890
    iget-object v3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->seasonId:Ljava/lang/String;

    if-eqz v3, :cond_8

    move v1, v2

    .line 891
    goto :goto_0

    .line 893
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->seasonId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->seasonId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 894
    goto :goto_0

    .line 896
    :cond_8
    iget v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    iget v4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 897
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 904
    const/16 v0, 0x11

    .line 905
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 907
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 909
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->seasonId:Ljava/lang/String;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 911
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    add-int v0, v1, v2

    .line 912
    return v0

    .line 905
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 907
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 909
    :cond_2
    iget-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->seasonId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 812
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 960
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 961
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 965
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 966
    :sswitch_0
    return-object p0

    .line 971
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    goto :goto_0

    .line 975
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    goto :goto_0

    .line 979
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->seasonId:Ljava/lang/String;

    goto :goto_0

    .line 983
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 984
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 993
    :pswitch_0
    iput v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    goto :goto_0

    .line 961
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    .line 984
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 918
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 919
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 921
    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 922
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 924
    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->seasonId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 925
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->seasonId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 927
    :cond_2
    iget v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    if-eqz v0, :cond_3

    .line 928
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 930
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 931
    return-void
.end method
