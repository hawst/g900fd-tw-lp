.class public final Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ViewerRating.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/ViewerRating;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ThumbsUpAndDown"
.end annotation


# instance fields
.field public recommended:Z

.field public thumbsDownCount:J

.field public thumbsUpCount:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 228
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 229
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->clear()Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    .line 230
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 233
    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsUpCount:J

    .line 234
    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsDownCount:J

    .line 235
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->recommended:Z

    .line 236
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->cachedSize:I

    .line 237
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 289
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 290
    .local v0, "size":I
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsUpCount:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 291
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsUpCount:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 294
    :cond_0
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsDownCount:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 295
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsDownCount:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 298
    :cond_1
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->recommended:Z

    if-eqz v1, :cond_2

    .line 299
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->recommended:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 302
    :cond_2
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 242
    if-ne p1, p0, :cond_1

    .line 258
    :cond_0
    :goto_0
    return v1

    .line 245
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    if-nez v3, :cond_2

    move v1, v2

    .line 246
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 248
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    .line 249
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsUpCount:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsUpCount:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    move v1, v2

    .line 250
    goto :goto_0

    .line 252
    :cond_3
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsDownCount:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsDownCount:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    move v1, v2

    .line 253
    goto :goto_0

    .line 255
    :cond_4
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->recommended:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->recommended:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 256
    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 263
    const/16 v0, 0x11

    .line 264
    .local v0, "result":I
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsUpCount:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsUpCount:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/lit16 v0, v1, 0x20f

    .line 266
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsDownCount:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsDownCount:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 268
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->recommended:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x4cf

    :goto_0
    add-int v0, v2, v1

    .line 269
    return v0

    .line 268
    :cond_0
    const/16 v1, 0x4d5

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 202
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 310
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 311
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 315
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 316
    :sswitch_0
    return-object p0

    .line 321
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsUpCount:J

    goto :goto_0

    .line 325
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsDownCount:J

    goto :goto_0

    .line 329
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->recommended:Z

    goto :goto_0

    .line 311
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 275
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsUpCount:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 276
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsUpCount:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 278
    :cond_0
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsDownCount:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 279
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->thumbsDownCount:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 281
    :cond_1
    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->recommended:Z

    if-eqz v0, :cond_2

    .line 282
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->recommended:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 284
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 285
    return-void
.end method
