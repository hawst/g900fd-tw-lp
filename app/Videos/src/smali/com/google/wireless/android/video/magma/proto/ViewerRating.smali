.class public final Lcom/google/wireless/android/video/magma/proto/ViewerRating;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ViewerRating.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;,
        Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;
    }
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;


# instance fields
.field public commentCount:J

.field public fiveStarHistogram:Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

.field public ratingScore:F

.field public ratingsCount:J

.field public thumbsUpAndDown:Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

.field public type:I

.field public userType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 383
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 384
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->clear()Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    .line 385
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    .locals 2

    .prologue
    .line 351
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    if-nez v0, :cond_1

    .line 352
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 354
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    if-nez v0, :cond_0

    .line 355
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    .line 357
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 359
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    return-object v0

    .line 357
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 388
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->type:I

    .line 389
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingScore:F

    .line 390
    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingsCount:J

    .line 391
    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->commentCount:J

    .line 392
    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->fiveStarHistogram:Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    .line 393
    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->thumbsUpAndDown:Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    .line 394
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->userType:I

    .line 395
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->cachedSize:I

    .line 396
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 495
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 496
    .local v0, "size":I
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->type:I

    if-eq v1, v3, :cond_0

    .line 497
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->type:I

    invoke-static {v4, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 500
    :cond_0
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingScore:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 502
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingScore:F

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    .line 505
    :cond_1
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingsCount:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_2

    .line 506
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingsCount:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 509
    :cond_2
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->commentCount:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_3

    .line 510
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->commentCount:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 513
    :cond_3
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->fiveStarHistogram:Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    if-eqz v1, :cond_4

    .line 514
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->fiveStarHistogram:Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 517
    :cond_4
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->thumbsUpAndDown:Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    if-eqz v1, :cond_5

    .line 518
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->thumbsUpAndDown:Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 521
    :cond_5
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->userType:I

    if-eq v1, v4, :cond_6

    .line 522
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->userType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 525
    :cond_6
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 401
    if-ne p1, p0, :cond_1

    .line 444
    :cond_0
    :goto_0
    return v2

    .line 404
    :cond_1
    instance-of v4, p1, Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    if-nez v4, :cond_2

    move v2, v3

    .line 405
    goto :goto_0

    :cond_2
    move-object v1, p1

    .line 407
    check-cast v1, Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    .line 408
    .local v1, "other":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    iget v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->type:I

    iget v5, v1, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->type:I

    if-eq v4, v5, :cond_3

    move v2, v3

    .line 409
    goto :goto_0

    .line 412
    :cond_3
    iget v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingScore:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    .line 413
    .local v0, "bits":I
    iget v4, v1, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingScore:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    if-eq v0, v4, :cond_4

    move v2, v3

    .line 414
    goto :goto_0

    .line 417
    :cond_4
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingsCount:J

    iget-wide v6, v1, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingsCount:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_5

    move v2, v3

    .line 418
    goto :goto_0

    .line 420
    :cond_5
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->commentCount:J

    iget-wide v6, v1, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->commentCount:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_6

    move v2, v3

    .line 421
    goto :goto_0

    .line 423
    :cond_6
    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->fiveStarHistogram:Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    if-nez v4, :cond_7

    .line 424
    iget-object v4, v1, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->fiveStarHistogram:Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    if-eqz v4, :cond_8

    move v2, v3

    .line 425
    goto :goto_0

    .line 428
    :cond_7
    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->fiveStarHistogram:Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    iget-object v5, v1, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->fiveStarHistogram:Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    invoke-virtual {v4, v5}, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    move v2, v3

    .line 429
    goto :goto_0

    .line 432
    :cond_8
    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->thumbsUpAndDown:Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    if-nez v4, :cond_9

    .line 433
    iget-object v4, v1, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->thumbsUpAndDown:Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    if-eqz v4, :cond_a

    move v2, v3

    .line 434
    goto :goto_0

    .line 437
    :cond_9
    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->thumbsUpAndDown:Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    iget-object v5, v1, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->thumbsUpAndDown:Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    invoke-virtual {v4, v5}, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    move v2, v3

    .line 438
    goto :goto_0

    .line 441
    :cond_a
    iget v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->userType:I

    iget v5, v1, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->userType:I

    if-eq v4, v5, :cond_0

    move v2, v3

    .line 442
    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v2, 0x0

    .line 449
    const/16 v0, 0x11

    .line 450
    .local v0, "result":I
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->type:I

    add-int/lit16 v0, v1, 0x20f

    .line 451
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingScore:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v0, v1, v3

    .line 453
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingsCount:J

    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingsCount:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 455
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->commentCount:J

    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->commentCount:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 457
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->fiveStarHistogram:Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 459
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->thumbsUpAndDown:Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    .line 461
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->userType:I

    add-int v0, v1, v2

    .line 462
    return v0

    .line 457
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->fiveStarHistogram:Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;->hashCode()I

    move-result v1

    goto :goto_0

    .line 459
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->thumbsUpAndDown:Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 533
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 534
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 538
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 539
    :sswitch_0
    return-object p0

    .line 544
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 545
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 548
    :pswitch_0
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->type:I

    goto :goto_0

    .line 554
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingScore:F

    goto :goto_0

    .line 558
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingsCount:J

    goto :goto_0

    .line 562
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->commentCount:J

    goto :goto_0

    .line 566
    :sswitch_5
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->fiveStarHistogram:Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    if-nez v2, :cond_1

    .line 567
    new-instance v2, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    invoke-direct {v2}, Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->fiveStarHistogram:Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    .line 569
    :cond_1
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->fiveStarHistogram:Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 573
    :sswitch_6
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->thumbsUpAndDown:Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    if-nez v2, :cond_2

    .line 574
    new-instance v2, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    invoke-direct {v2}, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;-><init>()V

    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->thumbsUpAndDown:Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    .line 576
    :cond_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->thumbsUpAndDown:Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 580
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 581
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 584
    :pswitch_1
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->userType:I

    goto :goto_0

    .line 534
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch

    .line 545
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 581
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 8
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    const/4 v2, 0x2

    const/4 v4, 0x1

    .line 468
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->type:I

    if-eq v0, v2, :cond_0

    .line 469
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->type:I

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 471
    :cond_0
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingScore:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 473
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingScore:F

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 475
    :cond_1
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingsCount:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_2

    .line 476
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingsCount:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 478
    :cond_2
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->commentCount:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_3

    .line 479
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->commentCount:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 481
    :cond_3
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->fiveStarHistogram:Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    if-eqz v0, :cond_4

    .line 482
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->fiveStarHistogram:Lcom/google/wireless/android/video/magma/proto/ViewerRating$FiveStarHistogram;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 484
    :cond_4
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->thumbsUpAndDown:Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    if-eqz v0, :cond_5

    .line 485
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->thumbsUpAndDown:Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 487
    :cond_5
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->userType:I

    if-eq v0, v4, :cond_6

    .line 488
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->userType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 490
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 491
    return-void
.end method
