.class public final Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AssetResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/android/video/magma/proto/AssetResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Offer"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;


# instance fields
.field public currencyCode:Ljava/lang/String;

.field public formatType:I

.field public formattedAmount:Ljava/lang/String;

.field public formattedFullAmount:Ljava/lang/String;

.field public offerType:I

.field public preorder:Z

.field public priceMicros:J

.field public rentalExpirationTimestampSec:J

.field public rentalLongTimerSec:I

.field public rentalPolicy:I

.field public rentalShortTimerSec:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1608
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1609
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->clear()Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    .line 1610
    return-void
.end method

.method public static emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    .locals 2

    .prologue
    .line 1564
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    if-nez v0, :cond_1

    .line 1565
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1567
    :try_start_0
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    if-nez v0, :cond_0

    .line 1568
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    sput-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    .line 1570
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1572
    :cond_1
    sget-object v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->_emptyArray:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    return-object v0

    .line 1570
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1613
    iput-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->priceMicros:J

    .line 1614
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->currencyCode:Ljava/lang/String;

    .line 1615
    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->offerType:I

    .line 1616
    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formatType:I

    .line 1617
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    .line 1618
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    .line 1619
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalLongTimerSec:I

    .line 1620
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalShortTimerSec:I

    .line 1621
    iput-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalExpirationTimestampSec:J

    .line 1622
    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalPolicy:I

    .line 1623
    iput-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->preorder:Z

    .line 1624
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->cachedSize:I

    .line 1625
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x1

    .line 1748
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1749
    .local v0, "size":I
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->priceMicros:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_0

    .line 1750
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->priceMicros:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1753
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->currencyCode:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1754
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->currencyCode:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1757
    :cond_1
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->offerType:I

    if-eq v1, v4, :cond_2

    .line 1758
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->offerType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1761
    :cond_2
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formatType:I

    if-eq v1, v4, :cond_3

    .line 1762
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formatType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1765
    :cond_3
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1766
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1769
    :cond_4
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1770
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1773
    :cond_5
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalLongTimerSec:I

    if-eqz v1, :cond_6

    .line 1774
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalLongTimerSec:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1777
    :cond_6
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalShortTimerSec:I

    if-eqz v1, :cond_7

    .line 1778
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalShortTimerSec:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1781
    :cond_7
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalExpirationTimestampSec:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_8

    .line 1782
    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalExpirationTimestampSec:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1785
    :cond_8
    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalPolicy:I

    if-eq v1, v4, :cond_9

    .line 1786
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalPolicy:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1789
    :cond_9
    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->preorder:Z

    if-eqz v1, :cond_a

    .line 1790
    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->preorder:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1793
    :cond_a
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1630
    if-ne p1, p0, :cond_1

    .line 1682
    :cond_0
    :goto_0
    return v1

    .line 1633
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    if-nez v3, :cond_2

    move v1, v2

    .line 1634
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1636
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    .line 1637
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->priceMicros:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->priceMicros:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    move v1, v2

    .line 1638
    goto :goto_0

    .line 1640
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->currencyCode:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 1641
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->currencyCode:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v1, v2

    .line 1642
    goto :goto_0

    .line 1644
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->currencyCode:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->currencyCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    move v1, v2

    .line 1645
    goto :goto_0

    .line 1647
    :cond_5
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->offerType:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->offerType:I

    if-eq v3, v4, :cond_6

    move v1, v2

    .line 1648
    goto :goto_0

    .line 1650
    :cond_6
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formatType:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formatType:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 1651
    goto :goto_0

    .line 1653
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 1654
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v1, v2

    .line 1655
    goto :goto_0

    .line 1657
    :cond_8
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 1658
    goto :goto_0

    .line 1660
    :cond_9
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    if-nez v3, :cond_a

    .line 1661
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    if-eqz v3, :cond_b

    move v1, v2

    .line 1662
    goto :goto_0

    .line 1664
    :cond_a
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    move v1, v2

    .line 1665
    goto :goto_0

    .line 1667
    :cond_b
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalLongTimerSec:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalLongTimerSec:I

    if-eq v3, v4, :cond_c

    move v1, v2

    .line 1668
    goto :goto_0

    .line 1670
    :cond_c
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalShortTimerSec:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalShortTimerSec:I

    if-eq v3, v4, :cond_d

    move v1, v2

    .line 1671
    goto :goto_0

    .line 1673
    :cond_d
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalExpirationTimestampSec:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalExpirationTimestampSec:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_e

    move v1, v2

    .line 1674
    goto :goto_0

    .line 1676
    :cond_e
    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalPolicy:I

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalPolicy:I

    if-eq v3, v4, :cond_f

    move v1, v2

    .line 1677
    goto/16 :goto_0

    .line 1679
    :cond_f
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->preorder:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->preorder:Z

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 1680
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v2, 0x0

    .line 1687
    const/16 v0, 0x11

    .line 1688
    .local v0, "result":I
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->priceMicros:J

    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->priceMicros:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v1, v4

    add-int/lit16 v0, v1, 0x20f

    .line 1690
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->currencyCode:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int v0, v3, v1

    .line 1692
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->offerType:I

    add-int v0, v1, v3

    .line 1693
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formatType:I

    add-int v0, v1, v3

    .line 1694
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 1696
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 1698
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalLongTimerSec:I

    add-int v0, v1, v2

    .line 1699
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalShortTimerSec:I

    add-int v0, v1, v2

    .line 1700
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalExpirationTimestampSec:J

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalExpirationTimestampSec:J

    ushr-long/2addr v4, v8

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 1702
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalPolicy:I

    add-int v0, v1, v2

    .line 1703
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->preorder:Z

    if-eqz v1, :cond_3

    const/16 v1, 0x4cf

    :goto_3
    add-int v0, v2, v1

    .line 1704
    return v0

    .line 1690
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->currencyCode:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 1694
    :cond_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 1696
    :cond_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 1703
    :cond_3
    const/16 v1, 0x4d5

    goto :goto_3
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1558
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1801
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1802
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1806
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1807
    :sswitch_0
    return-object p0

    .line 1812
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->priceMicros:J

    goto :goto_0

    .line 1816
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->currencyCode:Ljava/lang/String;

    goto :goto_0

    .line 1820
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1821
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1824
    :pswitch_0
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->offerType:I

    goto :goto_0

    .line 1830
    .end local v1    # "value":I
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1831
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 1834
    :pswitch_1
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formatType:I

    goto :goto_0

    .line 1840
    .end local v1    # "value":I
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    goto :goto_0

    .line 1844
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    goto :goto_0

    .line 1848
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalLongTimerSec:I

    goto :goto_0

    .line 1852
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalShortTimerSec:I

    goto :goto_0

    .line 1856
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalExpirationTimestampSec:J

    goto :goto_0

    .line 1860
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1861
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_2

    goto :goto_0

    .line 1865
    :pswitch_2
    iput v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalPolicy:I

    goto :goto_0

    .line 1871
    .end local v1    # "value":I
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->preorder:Z

    goto :goto_0

    .line 1802
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch

    .line 1821
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 1831
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 1861
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x1

    .line 1710
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->priceMicros:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1711
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->priceMicros:J

    invoke-virtual {p1, v4, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1713
    :cond_0
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->currencyCode:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1714
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->currencyCode:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1716
    :cond_1
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->offerType:I

    if-eq v0, v4, :cond_2

    .line 1717
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->offerType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1719
    :cond_2
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formatType:I

    if-eq v0, v4, :cond_3

    .line 1720
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formatType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1722
    :cond_3
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1723
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1725
    :cond_4
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1726
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1728
    :cond_5
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalLongTimerSec:I

    if-eqz v0, :cond_6

    .line 1729
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalLongTimerSec:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1731
    :cond_6
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalShortTimerSec:I

    if-eqz v0, :cond_7

    .line 1732
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalShortTimerSec:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1734
    :cond_7
    iget-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalExpirationTimestampSec:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_8

    .line 1735
    const/16 v0, 0x9

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalExpirationTimestampSec:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1737
    :cond_8
    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalPolicy:I

    if-eq v0, v4, :cond_9

    .line 1738
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalPolicy:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1740
    :cond_9
    iget-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->preorder:Z

    if-eqz v0, :cond_a

    .line 1741
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->preorder:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1743
    :cond_a
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1744
    return-void
.end method
