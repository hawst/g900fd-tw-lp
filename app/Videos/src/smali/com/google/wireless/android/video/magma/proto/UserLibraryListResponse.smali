.class public final Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "UserLibraryListResponse.java"


# instance fields
.field public pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

.field public resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

.field public serverTimestampSec:J

.field public snapshotToken:Ljava/lang/String;

.field public snapshotTokenOutdated:Z

.field public tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 42
    invoke-virtual {p0}, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->clear()Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;

    .line 43
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotToken:Ljava/lang/String;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotTokenOutdated:Z

    .line 48
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->serverTimestampSec:J

    .line 49
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    .line 50
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    .line 51
    invoke-static {}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->emptyArray()[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 52
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->cachedSize:I

    .line 53
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    .line 151
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 152
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotToken:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 153
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotToken:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 156
    :cond_0
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotTokenOutdated:Z

    if-eqz v3, :cond_1

    .line 157
    const/4 v3, 0x2

    iget-boolean v4, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotTokenOutdated:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 160
    :cond_1
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->serverTimestampSec:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_2

    .line 161
    const/4 v3, 0x3

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->serverTimestampSec:J

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 164
    :cond_2
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    if-eqz v3, :cond_3

    .line 165
    const/16 v3, 0x3ec

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 168
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    if-eqz v3, :cond_4

    .line 169
    const/16 v3, 0x3ee

    iget-object v4, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 172
    :cond_4
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v3, v3

    if-lez v3, :cond_6

    .line 173
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v3, v3

    if-ge v1, v3, :cond_6

    .line 174
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    aget-object v0, v3, v1

    .line 175
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v0, :cond_5

    .line 176
    const/16 v3, 0x3ef

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 173
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 181
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v1    # "i":I
    :cond_6
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    if-ne p1, p0, :cond_1

    .line 100
    :cond_0
    :goto_0
    return v1

    .line 61
    :cond_1
    instance-of v3, p1, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;

    if-nez v3, :cond_2

    move v1, v2

    .line 62
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 64
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;

    .line 65
    .local v0, "other":Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotToken:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 66
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotToken:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 67
    goto :goto_0

    .line 69
    :cond_3
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotToken:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotToken:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 70
    goto :goto_0

    .line 72
    :cond_4
    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotTokenOutdated:Z

    iget-boolean v4, v0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotTokenOutdated:Z

    if-eq v3, v4, :cond_5

    move v1, v2

    .line 73
    goto :goto_0

    .line 75
    :cond_5
    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->serverTimestampSec:J

    iget-wide v6, v0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->serverTimestampSec:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_6

    move v1, v2

    .line 76
    goto :goto_0

    .line 78
    :cond_6
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    if-nez v3, :cond_7

    .line 79
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    if-eqz v3, :cond_8

    move v1, v2

    .line 80
    goto :goto_0

    .line 83
    :cond_7
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 84
    goto :goto_0

    .line 87
    :cond_8
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    if-nez v3, :cond_9

    .line 88
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    if-eqz v3, :cond_a

    move v1, v2

    .line 89
    goto :goto_0

    .line 92
    :cond_9
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 93
    goto :goto_0

    .line 96
    :cond_a
    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 98
    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 105
    const/16 v0, 0x11

    .line 106
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotToken:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 108
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotTokenOutdated:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x4cf

    :goto_1
    add-int v0, v3, v1

    .line 109
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->serverTimestampSec:J

    iget-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->serverTimestampSec:J

    const/16 v3, 0x20

    ushr-long/2addr v6, v3

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int v0, v1, v3

    .line 111
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 113
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    if-nez v3, :cond_3

    :goto_3
    add-int v0, v1, v2

    .line 115
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 117
    return v0

    .line 106
    :cond_0
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotToken:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 108
    :cond_1
    const/16 v1, 0x4d5

    goto :goto_1

    .line 111
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    invoke-virtual {v1}, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;->hashCode()I

    move-result v1

    goto :goto_2

    .line 113
    :cond_3
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    invoke-virtual {v2}, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;->hashCode()I

    move-result v2

    goto :goto_3
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 189
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 190
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 194
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 195
    :sswitch_0
    return-object p0

    .line 200
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotToken:Ljava/lang/String;

    goto :goto_0

    .line 204
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotTokenOutdated:Z

    goto :goto_0

    .line 208
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->serverTimestampSec:J

    goto :goto_0

    .line 212
    :sswitch_4
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    if-nez v5, :cond_1

    .line 213
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;-><init>()V

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    .line 215
    :cond_1
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 219
    :sswitch_5
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    if-nez v5, :cond_2

    .line 220
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;-><init>()V

    iput-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    .line 222
    :cond_2
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 226
    :sswitch_6
    const/16 v5, 0x1f7a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 228
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-nez v5, :cond_4

    move v1, v4

    .line 229
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 231
    .local v2, "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v1, :cond_3

    .line 232
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 234
    :cond_3
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_5

    .line 235
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource;-><init>()V

    aput-object v5, v2, v1

    .line 236
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 237
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 234
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 228
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_4
    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v1, v5

    goto :goto_1

    .line 240
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_5
    new-instance v5, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-direct {v5}, Lcom/google/wireless/android/video/magma/proto/AssetResource;-><init>()V

    aput-object v5, v2, v1

    .line 241
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 242
    iput-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    goto/16 :goto_0

    .line 190
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1f62 -> :sswitch_4
        0x1f72 -> :sswitch_5
        0x1f7a -> :sswitch_6
    .end sparse-switch
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotToken:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 124
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotToken:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 126
    :cond_0
    iget-boolean v2, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotTokenOutdated:Z

    if-eqz v2, :cond_1

    .line 127
    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->snapshotTokenOutdated:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 129
    :cond_1
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->serverTimestampSec:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 130
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->serverTimestampSec:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 132
    :cond_2
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    if-eqz v2, :cond_3

    .line 133
    const/16 v2, 0x3ec

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->pageInfo:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$PageInfo;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 135
    :cond_3
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    if-eqz v2, :cond_4

    .line 136
    const/16 v2, 0x3ee

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->tokenPagination:Lcom/google/wireless/android/video/magma/proto/GdataRosyProtoCoreProtos$TokenPagination;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 138
    :cond_4
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 139
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 140
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    aget-object v0, v2, v1

    .line 141
    .local v0, "element":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v0, :cond_5

    .line 142
    const/16 v2, 0x3ef

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 139
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 146
    .end local v0    # "element":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v1    # "i":I
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 147
    return-void
.end method
