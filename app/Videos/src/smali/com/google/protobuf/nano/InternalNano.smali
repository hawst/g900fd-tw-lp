.class public final Lcom/google/protobuf/nano/InternalNano;
.super Ljava/lang/Object;
.source "InternalNano.java"


# static fields
.field public static final LAZY_INIT_LOCK:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    return-void
.end method

.method public static equals([I[I)Z
    .locals 1
    .param p0, "field1"    # [I
    .param p1, "field2"    # [I

    .prologue
    .line 135
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_3

    .line 136
    :cond_0
    if-eqz p1, :cond_1

    array-length v0, p1

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 138
    :goto_0
    return v0

    .line 136
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 138
    :cond_3
    invoke-static {p0, p1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    goto :goto_0
.end method

.method public static equals([Ljava/lang/Object;[Ljava/lang/Object;)Z
    .locals 10
    .param p0, "field1"    # [Ljava/lang/Object;
    .param p1, "field2"    # [Ljava/lang/Object;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 232
    const/4 v2, 0x0

    .line 233
    .local v2, "index1":I
    if-nez p0, :cond_0

    move v4, v6

    .line 234
    .local v4, "length1":I
    :goto_0
    const/4 v3, 0x0

    .line 235
    .local v3, "index2":I
    if-nez p1, :cond_1

    move v5, v6

    .line 237
    .local v5, "length2":I
    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v8, p0, v2

    if-nez v8, :cond_2

    .line 238
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 233
    .end local v3    # "index2":I
    .end local v4    # "length1":I
    .end local v5    # "length2":I
    :cond_0
    array-length v4, p0

    goto :goto_0

    .line 235
    .restart local v3    # "index2":I
    .restart local v4    # "length1":I
    :cond_1
    array-length v5, p1

    goto :goto_1

    .line 240
    .restart local v5    # "length2":I
    :cond_2
    :goto_2
    if-ge v3, v5, :cond_3

    aget-object v8, p1, v3

    if-nez v8, :cond_3

    .line 241
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 243
    :cond_3
    if-lt v2, v4, :cond_5

    move v0, v7

    .line 244
    .local v0, "atEndOf1":Z
    :goto_3
    if-lt v3, v5, :cond_6

    move v1, v7

    .line 245
    .local v1, "atEndOf2":Z
    :goto_4
    if-eqz v0, :cond_7

    if-eqz v1, :cond_7

    move v6, v7

    .line 253
    :cond_4
    return v6

    .end local v0    # "atEndOf1":Z
    .end local v1    # "atEndOf2":Z
    :cond_5
    move v0, v6

    .line 243
    goto :goto_3

    .restart local v0    # "atEndOf1":Z
    :cond_6
    move v1, v6

    .line 244
    goto :goto_4

    .line 248
    .restart local v1    # "atEndOf2":Z
    :cond_7
    if-ne v0, v1, :cond_4

    .line 251
    aget-object v8, p0, v2

    aget-object v9, p1, v3

    invoke-virtual {v8, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 255
    add-int/lit8 v2, v2, 0x1

    .line 256
    add-int/lit8 v3, v3, 0x1

    .line 257
    goto :goto_1
.end method

.method public static hashCode([I)I
    .locals 1
    .param p0, "field"    # [I

    .prologue
    .line 265
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Ljava/util/Arrays;->hashCode([I)I

    move-result v0

    goto :goto_0
.end method

.method public static hashCode([Ljava/lang/Object;)I
    .locals 6
    .param p0, "field"    # [Ljava/lang/Object;

    .prologue
    .line 323
    const/4 v2, 0x0

    .line 324
    .local v2, "result":I
    const/4 v1, 0x0

    .local v1, "i":I
    if-nez p0, :cond_1

    const/4 v3, 0x0

    .local v3, "size":I
    :goto_0
    if-ge v1, v3, :cond_2

    .line 325
    aget-object v0, p0, v1

    .line 326
    .local v0, "element":Ljava/lang/Object;
    if-eqz v0, :cond_0

    .line 327
    mul-int/lit8 v4, v2, 0x1f

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v5

    add-int v2, v4, v5

    .line 324
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v0    # "element":Ljava/lang/Object;
    .end local v3    # "size":I
    :cond_1
    array-length v3, p0

    goto :goto_0

    .line 330
    .restart local v3    # "size":I
    :cond_2
    return v2
.end method
