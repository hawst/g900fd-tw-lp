.class final Lcom/widevine/drm/internal/j;
.super Ljava/lang/Thread;


# instance fields
.field private synthetic a:Lcom/widevine/drm/internal/i;


# direct methods
.method synthetic constructor <init>(Lcom/widevine/drm/internal/i;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/widevine/drm/internal/j;-><init>(Lcom/widevine/drm/internal/i;B)V

    return-void
.end method

.method private constructor <init>(Lcom/widevine/drm/internal/i;B)V
    .locals 0

    iput-object p1, p0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 27

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-static {v4}, Lcom/widevine/drm/internal/i;->a(Lcom/widevine/drm/internal/i;)Z

    const/4 v5, 0x0

    const/4 v4, 0x0

    :goto_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    iget-object v7, v7, Lcom/widevine/drm/internal/i;->c:Lcom/widevine/drm/internal/HTTPDecrypter;

    invoke-virtual {v7}, Lcom/widevine/drm/internal/HTTPDecrypter;->b()Z
    :try_end_0
    .catch Lcom/widevine/drm/internal/ad; {:try_start_0 .. :try_end_0} :catch_3

    move-result v7

    if-eqz v7, :cond_0

    const-wide/16 v8, 0x5

    :try_start_1
    invoke-static {v8, v9}, Lcom/widevine/drm/internal/j;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/widevine/drm/internal/ad; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    :catch_0
    move-exception v7

    goto :goto_0

    :cond_0
    :try_start_2
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    iget-object v7, v7, Lcom/widevine/drm/internal/i;->c:Lcom/widevine/drm/internal/HTTPDecrypter;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    iget-object v8, v8, Lcom/widevine/drm/internal/i;->g:Ljava/lang/String;

    sget-object v9, Lcom/widevine/drm/internal/x;->d:Lcom/widevine/drm/internal/x;

    invoke-virtual {v9}, Lcom/widevine/drm/internal/x;->ordinal()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-virtual {v10}, Lcom/widevine/drm/internal/i;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Lcom/widevine/drm/internal/HTTPDecrypter;->a(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_2
    .catch Lcom/widevine/drm/internal/ad; {:try_start_2 .. :try_end_2} :catch_3

    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-static {v7}, Lcom/widevine/drm/internal/i;->b(Lcom/widevine/drm/internal/i;)Z

    move-result v7

    if-eqz v7, :cond_1c

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    const/4 v8, 0x0

    iput-boolean v8, v7, Lcom/widevine/drm/internal/i;->d:Z

    const/4 v9, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    iget-object v8, v8, Lcom/widevine/drm/internal/i;->c:Lcom/widevine/drm/internal/HTTPDecrypter;

    sget-object v10, Lcom/widevine/drm/internal/e;->a:Lcom/widevine/drm/internal/e;

    invoke-virtual {v8, v10}, Lcom/widevine/drm/internal/HTTPDecrypter;->a(Lcom/widevine/drm/internal/e;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    const-wide/16 v10, 0x0

    invoke-static {v8, v10, v11}, Lcom/widevine/drm/internal/i;->a(Lcom/widevine/drm/internal/i;J)J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-static {v8}, Lcom/widevine/drm/internal/i;->c(Lcom/widevine/drm/internal/i;)J

    const/16 v17, 0x0

    const/4 v15, 0x0

    :try_start_3
    new-instance v8, Ljava/net/Socket;

    invoke-direct {v8}, Ljava/net/Socket;-><init>()V
    :try_end_3
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_20
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_19
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_12

    :try_start_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-static {v7}, Lcom/widevine/drm/internal/i;->d(Lcom/widevine/drm/internal/i;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->getPort()I

    move-result v7

    const/4 v10, -0x1

    if-ne v7, v10, :cond_1f

    const/16 v7, 0x50

    move v11, v7

    :goto_2
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-static {v7}, Lcom/widevine/drm/internal/i;->d(Lcom/widevine/drm/internal/i;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/net/InetAddress;->getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v12

    array-length v13, v12

    const/4 v7, 0x0

    move/from16 v26, v7

    move-object v7, v10

    move/from16 v10, v26

    :goto_3
    if-ge v10, v13, :cond_2

    aget-object v14, v12, v10
    :try_end_4
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_1a
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_13

    const/4 v7, 0x0

    :try_start_5
    new-instance v16, Ljava/net/InetSocketAddress;

    move-object/from16 v0, v16

    invoke-direct {v0, v14, v11}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    const/16 v14, 0x1388

    move-object/from16 v0, v16

    invoke-virtual {v8, v0, v14}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_5 .. :try_end_5} :catch_1a

    :cond_2
    if-eqz v7, :cond_4

    :try_start_6
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    const/4 v11, 0x1

    sget-object v12, Lcom/widevine/drmapi/android/WVStatus;->CantConnectToMediaServer:Lcom/widevine/drmapi/android/WVStatus;

    const-string v13, "Error connecting to media server"

    invoke-virtual {v10, v11, v12, v13}, Lcom/widevine/drm/internal/i;->a(ZLcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    throw v7
    :try_end_6
    .catch Ljava/net/UnknownHostException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_6 .. :try_end_6} :catch_1a
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_13

    :catch_1
    move-exception v7

    move-object v7, v8

    move-object v8, v5

    move v5, v4

    move-object v4, v9

    move v9, v6

    :goto_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    const/4 v10, 0x1

    sget-object v11, Lcom/widevine/drmapi/android/WVStatus;->CantConnectToMediaServer:Lcom/widevine/drmapi/android/WVStatus;

    const-string v12, "Unable to resolve media server hostname"

    invoke-virtual {v6, v10, v11, v12}, Lcom/widevine/drm/internal/i;->a(ZLcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    move-object v10, v4

    move v6, v9

    move v4, v5

    move-object v5, v8

    :goto_5
    if-eqz v10, :cond_3

    :try_start_7
    invoke-virtual {v10}, Ljava/net/Socket;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_d

    :cond_3
    :goto_6
    if-eqz v7, :cond_1

    :try_start_8
    invoke-virtual {v7}, Ljava/net/Socket;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto/16 :goto_1

    :catch_2
    move-exception v7

    goto/16 :goto_1

    :catch_3
    move-exception v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    const/4 v9, 0x1

    iget-object v10, v7, Lcom/widevine/drm/internal/ad;->a:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v7}, Lcom/widevine/drm/internal/ad;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v9, v10, v7}, Lcom/widevine/drm/internal/i;->a(ZLcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_4
    move-exception v7

    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    :cond_4
    :try_start_9
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    iget-object v7, v7, Lcom/widevine/drm/internal/i;->a:Ljava/net/ServerSocket;

    invoke-virtual {v7}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;
    :try_end_9
    .catch Ljava/net/UnknownHostException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_9 .. :try_end_9} :catch_1a
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_13

    move-result-object v10

    :try_start_a
    invoke-virtual {v10}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v21

    invoke-virtual {v10}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v22

    invoke-virtual {v8}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v11

    invoke-virtual {v8}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    iget-object v7, v7, Lcom/widevine/drm/internal/i;->a:Ljava/net/ServerSocket;

    const/4 v12, 0x0

    invoke-virtual {v7, v12}, Ljava/net/ServerSocket;->setSoTimeout(I)V

    const/high16 v7, 0x10000

    new-array v0, v7, [B

    move-object/from16 v20, v0

    const/4 v7, 0x2

    new-array v0, v7, [J

    move-object/from16 v23, v0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-virtual {v12}, Lcom/widevine/drm/internal/i;->c()Z

    move-result v18

    if-nez v18, :cond_5

    const/4 v7, 0x1

    :cond_5
    if-eqz v5, :cond_7

    :goto_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    iget-object v12, v12, Lcom/widevine/drm/internal/i;->c:Lcom/widevine/drm/internal/HTTPDecrypter;

    invoke-virtual {v12}, Lcom/widevine/drm/internal/HTTPDecrypter;->b()Z
    :try_end_a
    .catch Ljava/net/UnknownHostException; {:try_start_a .. :try_end_a} :catch_21
    .catch Ljava/net/SocketTimeoutException; {:try_start_a .. :try_end_a} :catch_1b
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_14

    move-result v12

    if-eqz v12, :cond_6

    const-wide/16 v12, 0x5

    :try_start_b
    invoke-static {v12, v13}, Lcom/widevine/drm/internal/j;->sleep(J)V
    :try_end_b
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_5
    .catch Ljava/net/UnknownHostException; {:try_start_b .. :try_end_b} :catch_21
    .catch Ljava/net/SocketTimeoutException; {:try_start_b .. :try_end_b} :catch_1b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_14

    goto :goto_7

    :catch_5
    move-exception v12

    goto :goto_7

    :cond_6
    :try_start_c
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    array-length v13, v5

    invoke-virtual {v12, v5, v13}, Lcom/widevine/drm/internal/i;->b([BI)I
    :try_end_c
    .catch Ljava/net/UnknownHostException; {:try_start_c .. :try_end_c} :catch_21
    .catch Ljava/net/SocketTimeoutException; {:try_start_c .. :try_end_c} :catch_1b
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_14

    :cond_7
    const/4 v13, 0x0

    const/16 v16, 0x0

    move v12, v15

    move/from16 v14, v17

    move/from16 v17, v7

    move/from16 v15, v18

    move-object v7, v8

    move-object v8, v5

    move-object v5, v9

    move v9, v6

    move-object v6, v11

    move v11, v4

    move-object/from16 v4, v16

    :goto_8
    :try_start_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/widevine/drm/internal/i;->b(Lcom/widevine/drm/internal/i;)Z

    move-result v16

    if-eqz v16, :cond_23

    if-nez v15, :cond_24

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-virtual {v15}, Lcom/widevine/drm/internal/i;->c()Z

    move-result v15

    if-nez v15, :cond_24

    add-int/lit8 v16, v17, 0x1

    const/16 v18, 0x5

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    sget-object v19, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    const-string v24, "serror (32)"

    move-object/from16 v0, v17

    move/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Lcom/widevine/drm/internal/i;->a(ZLcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    :cond_8
    move/from16 v18, v15

    move/from16 v19, v16

    :goto_9
    invoke-virtual/range {v21 .. v21}, Ljava/io/InputStream;->available()I

    move-result v15

    if-gtz v15, :cond_9

    if-eqz v13, :cond_22

    :cond_9
    invoke-virtual/range {v21 .. v21}, Ljava/io/InputStream;->available()I

    move-result v14

    if-lez v14, :cond_a

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v14

    move v15, v14

    :goto_a
    const/4 v14, -0x1

    if-eq v15, v14, :cond_23

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-static {v14, v0, v15, v1}, Lcom/widevine/drm/internal/i;->a(Lcom/widevine/drm/internal/i;[BI[J)Ljava/lang/String;

    move-result-object v16

    const/4 v14, 0x1

    if-eqz v16, :cond_22

    new-array v4, v15, [B

    const/4 v13, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-static {v0, v13, v4, v1, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->getBytes()[B

    move-result-object v13

    const/4 v15, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v5, v13, v15, v0}, Ljava/io/OutputStream;->write([BII)V

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "HTTP remote server request:\n"

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    const/4 v13, 0x0

    move-object/from16 v16, v4

    move/from16 v17, v14

    move v4, v13

    :goto_b
    if-nez v17, :cond_b

    add-int/lit8 v12, v12, 0x1

    const/16 v13, 0x32

    if-le v12, v13, :cond_b

    new-instance v4, Ljava/io/IOException;

    const-string v5, "mediaserver HTTP request timeout"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    :catch_6
    move-exception v4

    move-object v4, v10

    move v5, v11

    goto/16 :goto_4

    :cond_a
    array-length v14, v4

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v16

    invoke-static {v4, v15, v0, v1, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v15, v14

    goto :goto_a

    :cond_b
    move v15, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    iget-boolean v12, v12, Lcom/widevine/drm/internal/i;->d:Z

    if-nez v12, :cond_17

    invoke-virtual {v6}, Ljava/io/InputStream;->available()I

    move-result v12

    if-lez v12, :cond_13

    const/4 v11, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/io/InputStream;->read([B)I

    move-result v13

    if-lez v13, :cond_12

    move-object/from16 v0, v20

    invoke-static {v0, v13}, Lcom/widevine/drm/internal/i;->a([BI)I

    move-result v12

    if-lez v12, :cond_21

    new-instance v14, Ljava/lang/String;

    const/16 v24, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v24

    invoke-direct {v14, v0, v1, v12}, Ljava/lang/String;-><init>([BII)V

    invoke-static {v14}, Lcom/widevine/drm/internal/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    if-eqz v24, :cond_21

    const/4 v13, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x1

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_d
    .catch Ljava/net/UnknownHostException; {:try_start_d .. :try_end_d} :catch_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_d .. :try_end_d} :catch_a
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_b

    :try_start_e
    invoke-virtual {v7}, Ljava/net/Socket;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_c
    .catch Ljava/net/UnknownHostException; {:try_start_e .. :try_end_e} :catch_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_e .. :try_end_e} :catch_a

    :goto_c
    :try_start_f
    new-instance v4, Ljava/net/Socket;

    invoke-direct {v4}, Ljava/net/Socket;-><init>()V
    :try_end_f
    .catch Ljava/net/UnknownHostException; {:try_start_f .. :try_end_f} :catch_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_f .. :try_end_f} :catch_a
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_b

    :try_start_10
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-static/range {v24 .. v24}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-static {v7, v0}, Lcom/widevine/drm/internal/i;->a(Lcom/widevine/drm/internal/i;Landroid/net/Uri;)Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-static {v7}, Lcom/widevine/drm/internal/i;->d(Lcom/widevine/drm/internal/i;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-static {v7}, Lcom/widevine/drm/internal/i;->d(Lcom/widevine/drm/internal/i;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->getPort()I

    move-result v7

    if-nez v25, :cond_c

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    const/4 v12, 0x1

    sget-object v13, Lcom/widevine/drmapi/android/WVStatus;->CantConnectToMediaServer:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Invalid redirection: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7, v12, v13, v14}, Lcom/widevine/drm/internal/i;->a(ZLcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V
    :try_end_10
    .catch Ljava/net/UnknownHostException; {:try_start_10 .. :try_end_10} :catch_1e
    .catch Ljava/net/SocketTimeoutException; {:try_start_10 .. :try_end_10} :catch_17
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_10

    move-object v7, v4

    move v4, v11

    :goto_d
    :try_start_11
    invoke-virtual/range {v21 .. v21}, Ljava/io/InputStream;->close()V

    invoke-virtual/range {v22 .. v22}, Ljava/io/OutputStream;->close()V

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_11
    .catch Ljava/net/UnknownHostException; {:try_start_11 .. :try_end_11} :catch_1f
    .catch Ljava/net/SocketTimeoutException; {:try_start_11 .. :try_end_11} :catch_18
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_11

    move-object v5, v8

    move v6, v9

    goto/16 :goto_5

    :cond_c
    const/4 v5, -0x1

    if-ne v7, v5, :cond_20

    const/16 v5, 0x50

    :goto_e
    :try_start_12
    new-instance v6, Ljava/net/InetSocketAddress;

    move-object/from16 v0, v25

    invoke-direct {v6, v0, v5}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    const/16 v5, 0x1388

    invoke-virtual {v4, v6, v5}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    invoke-virtual {v4}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    invoke-virtual {v4}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;
    :try_end_12
    .catch Ljava/net/UnknownHostException; {:try_start_12 .. :try_end_12} :catch_1e
    .catch Ljava/net/SocketTimeoutException; {:try_start_12 .. :try_end_12} :catch_17
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_10

    move-result-object v5

    move-object v7, v4

    move v4, v14

    move v14, v12

    move v12, v13

    :goto_f
    const/4 v13, 0x0

    if-lez v14, :cond_d

    :try_start_13
    new-instance v13, Ljava/lang/String;

    const/16 v24, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v24

    invoke-direct {v13, v0, v1, v14}, Ljava/lang/String;-><init>([BII)V

    sub-int/2addr v12, v14

    const/16 v24, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v20

    move/from16 v2, v24

    invoke-static {v0, v14, v1, v2, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_d
    if-lez v12, :cond_10

    if-nez v8, :cond_e

    move-object/from16 v8, v20

    :cond_e
    :goto_10
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    iget-object v14, v14, Lcom/widevine/drm/internal/i;->c:Lcom/widevine/drm/internal/HTTPDecrypter;

    invoke-virtual {v14}, Lcom/widevine/drm/internal/HTTPDecrypter;->b()Z
    :try_end_13
    .catch Ljava/net/UnknownHostException; {:try_start_13 .. :try_end_13} :catch_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_13 .. :try_end_13} :catch_a
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_b

    move-result v14

    if-eqz v14, :cond_f

    const-wide/16 v24, 0x5

    :try_start_14
    invoke-static/range {v24 .. v25}, Lcom/widevine/drm/internal/j;->sleep(J)V
    :try_end_14
    .catch Ljava/lang/InterruptedException; {:try_start_14 .. :try_end_14} :catch_7
    .catch Ljava/net/UnknownHostException; {:try_start_14 .. :try_end_14} :catch_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_14 .. :try_end_14} :catch_a
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_b

    goto :goto_10

    :catch_7
    move-exception v14

    goto :goto_10

    :cond_f
    :try_start_15
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    move-object/from16 v0, v20

    invoke-virtual {v14, v0, v12}, Lcom/widevine/drm/internal/i;->b([BI)I

    move-result v12

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "decrypt: "

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    :cond_10
    if-eqz v13, :cond_11

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    move-object/from16 v0, v23

    invoke-static {v14, v13, v0}, Lcom/widevine/drm/internal/i;->a(Lcom/widevine/drm/internal/i;Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_11

    invoke-virtual {v13}, Ljava/lang/String;->getBytes()[B

    move-result-object v14

    const/16 v24, 0x0

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v25

    move-object/from16 v0, v22

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v14, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "HTTP media player response:\n"

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-static {v14}, Lcom/widevine/drm/internal/i;->e(Lcom/widevine/drm/internal/i;)J

    move-result-wide v24

    move-wide/from16 v0, v24

    invoke-static {v13, v0, v1}, Lcom/widevine/drm/internal/i;->a(Lcom/widevine/drm/internal/i;J)J

    :cond_11
    if-lez v12, :cond_12

    const/4 v13, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v13, v12}, Ljava/io/OutputStream;->write([BII)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    int-to-long v0, v12

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-static {v13, v0, v1}, Lcom/widevine/drm/internal/i;->b(Lcom/widevine/drm/internal/i;J)J
    :try_end_15
    .catch Ljava/net/UnknownHostException; {:try_start_15 .. :try_end_15} :catch_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_15 .. :try_end_15} :catch_a
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_b

    :cond_12
    move v13, v4

    move-object/from16 v26, v5

    move v5, v11

    move-object v11, v6

    move-object/from16 v6, v26

    :goto_11
    :try_start_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    iget v4, v4, Lcom/widevine/drm/internal/i;->h:I

    const/16 v12, 0xa

    if-ne v4, v12, :cond_1a

    invoke-static {}, Lcom/widevine/drm/internal/HTTPDecrypter;->c()Lcom/widevine/drm/internal/e;

    move-result-object v4

    sget-object v12, Lcom/widevine/drm/internal/e;->c:Lcom/widevine/drm/internal/e;
    :try_end_16
    .catch Ljava/lang/InterruptedException; {:try_start_16 .. :try_end_16} :catch_9
    .catch Ljava/net/UnknownHostException; {:try_start_16 .. :try_end_16} :catch_1c
    .catch Ljava/net/SocketTimeoutException; {:try_start_16 .. :try_end_16} :catch_15
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_e

    if-eq v4, v12, :cond_1a

    add-int/lit8 v4, v9, 0x1

    const/16 v9, 0xa

    if-ge v4, v9, :cond_1b

    const-wide/16 v24, 0x32

    :try_start_17
    invoke-static/range {v24 .. v25}, Lcom/widevine/drm/internal/j;->sleep(J)V
    :try_end_17
    .catch Ljava/lang/InterruptedException; {:try_start_17 .. :try_end_17} :catch_22
    .catch Ljava/net/UnknownHostException; {:try_start_17 .. :try_end_17} :catch_1d
    .catch Ljava/net/SocketTimeoutException; {:try_start_17 .. :try_end_17} :catch_16
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_f

    move v12, v15

    move/from16 v14, v17

    move v9, v4

    move-object/from16 v4, v16

    move/from16 v15, v18

    move/from16 v17, v19

    move-object/from16 v26, v11

    move v11, v5

    move-object v5, v6

    move-object/from16 v6, v26

    goto/16 :goto_8

    :cond_13
    :try_start_18
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-virtual {v12}, Lcom/widevine/drm/internal/i;->a()J

    move-result-wide v12

    const-wide/16 v24, 0x0

    cmp-long v14, v12, v24

    if-nez v14, :cond_14

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-static {v12}, Lcom/widevine/drm/internal/i;->f(Lcom/widevine/drm/internal/i;)J

    move-result-wide v12

    :cond_14
    const-wide/16 v24, 0x0

    cmp-long v14, v12, v24

    if-lez v14, :cond_15

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-static {v14}, Lcom/widevine/drm/internal/i;->g(Lcom/widevine/drm/internal/i;)J

    move-result-wide v24

    cmp-long v12, v24, v12

    if-ltz v12, :cond_15

    invoke-virtual/range {v21 .. v21}, Ljava/io/InputStream;->close()V

    invoke-virtual/range {v22 .. v22}, Ljava/io/OutputStream;->close()V

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    :cond_15
    add-int/lit8 v11, v11, 0xa

    const v12, 0xea60

    if-le v11, v12, :cond_16

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    const/4 v13, 0x1

    sget-object v14, Lcom/widevine/drmapi/android/WVStatus;->LostConnection:Lcom/widevine/drmapi/android/WVStatus;

    const-string v24, "Media server connection timeout"

    move-object/from16 v0, v24

    invoke-virtual {v12, v13, v14, v0}, Lcom/widevine/drm/internal/i;->a(ZLcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    :cond_16
    move v13, v4

    move-object/from16 v26, v5

    move v5, v11

    move-object v11, v6

    move-object/from16 v6, v26

    goto :goto_11

    :cond_17
    invoke-static {}, Lcom/widevine/drm/internal/HTTPDecrypter;->c()Lcom/widevine/drm/internal/e;

    move-result-object v12

    sget-object v13, Lcom/widevine/drm/internal/e;->c:Lcom/widevine/drm/internal/e;

    if-ne v12, v13, :cond_19

    :goto_12
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    iget-object v12, v12, Lcom/widevine/drm/internal/i;->c:Lcom/widevine/drm/internal/HTTPDecrypter;

    invoke-virtual {v12}, Lcom/widevine/drm/internal/HTTPDecrypter;->b()Z
    :try_end_18
    .catch Ljava/net/UnknownHostException; {:try_start_18 .. :try_end_18} :catch_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_18 .. :try_end_18} :catch_a
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_b

    move-result v12

    if-eqz v12, :cond_18

    const-wide/16 v12, 0x5

    :try_start_19
    invoke-static {v12, v13}, Lcom/widevine/drm/internal/j;->sleep(J)V
    :try_end_19
    .catch Ljava/lang/InterruptedException; {:try_start_19 .. :try_end_19} :catch_8
    .catch Ljava/net/UnknownHostException; {:try_start_19 .. :try_end_19} :catch_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_19 .. :try_end_19} :catch_a
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_b

    goto :goto_12

    :catch_8
    move-exception v12

    goto :goto_12

    :cond_18
    :try_start_1a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    const/4 v13, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v12, v0, v13}, Lcom/widevine/drm/internal/i;->b([BI)I

    move-result v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "decrypt (key received): "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    if-lez v12, :cond_19

    const/4 v13, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v13, v12}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1a
    .catch Ljava/net/UnknownHostException; {:try_start_1a .. :try_end_1a} :catch_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_1a .. :try_end_1a} :catch_a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_b

    :cond_19
    move v13, v4

    move-object/from16 v26, v5

    move v5, v11

    move-object v11, v6

    move-object/from16 v6, v26

    goto/16 :goto_11

    :cond_1a
    move v4, v9

    :cond_1b
    const-wide/16 v24, 0xa

    :try_start_1b
    invoke-static/range {v24 .. v25}, Lcom/widevine/drm/internal/j;->sleep(J)V
    :try_end_1b
    .catch Ljava/lang/InterruptedException; {:try_start_1b .. :try_end_1b} :catch_22
    .catch Ljava/net/UnknownHostException; {:try_start_1b .. :try_end_1b} :catch_1d
    .catch Ljava/net/SocketTimeoutException; {:try_start_1b .. :try_end_1b} :catch_16
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_f

    move v12, v15

    move/from16 v14, v17

    move v9, v4

    move-object/from16 v4, v16

    move/from16 v15, v18

    move/from16 v17, v19

    move-object/from16 v26, v11

    move v11, v5

    move-object v5, v6

    move-object/from16 v6, v26

    goto/16 :goto_8

    :catch_9
    move-exception v4

    move v4, v9

    :goto_13
    move v12, v15

    move/from16 v14, v17

    move v9, v4

    move-object/from16 v4, v16

    move/from16 v15, v18

    move/from16 v17, v19

    move-object/from16 v26, v11

    move v11, v5

    move-object v5, v6

    move-object/from16 v6, v26

    goto/16 :goto_8

    :catch_a
    move-exception v4

    :goto_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    const/4 v5, 0x1

    sget-object v6, Lcom/widevine/drmapi/android/WVStatus;->LostConnection:Lcom/widevine/drmapi/android/WVStatus;

    const-string v12, "Mediaplayer connection timeout"

    invoke-virtual {v4, v5, v6, v12}, Lcom/widevine/drm/internal/i;->a(ZLcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    move v4, v11

    move-object v5, v8

    move v6, v9

    goto/16 :goto_5

    :catch_b
    move-exception v4

    :goto_15
    move v4, v11

    move-object v5, v8

    move v6, v9

    goto/16 :goto_5

    :cond_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    iget-boolean v4, v4, Lcom/widevine/drm/internal/i;->f:Z

    if-eqz v4, :cond_1d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    iget-object v4, v4, Lcom/widevine/drm/internal/i;->c:Lcom/widevine/drm/internal/HTTPDecrypter;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    iget-object v5, v5, Lcom/widevine/drm/internal/i;->g:Ljava/lang/String;

    sget-object v6, Lcom/widevine/drm/internal/x;->d:Lcom/widevine/drm/internal/x;

    invoke-virtual {v6}, Lcom/widevine/drm/internal/x;->ordinal()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/widevine/drm/internal/HTTPDecrypter;->a(Ljava/lang/String;I)I

    :cond_1d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    iget-boolean v4, v4, Lcom/widevine/drm/internal/i;->e:Z

    if-eqz v4, :cond_1e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/widevine/drm/internal/i;->e:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-static {v5}, Lcom/widevine/drm/internal/i;->h(Lcom/widevine/drm/internal/i;)Lcom/widevine/drmapi/android/WVStatus;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/widevine/drm/internal/j;->a:Lcom/widevine/drm/internal/i;

    invoke-static {v6}, Lcom/widevine/drm/internal/i;->i(Lcom/widevine/drm/internal/i;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/widevine/drm/internal/i;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    :cond_1e
    return-void

    :catch_c
    move-exception v4

    goto/16 :goto_c

    :catch_d
    move-exception v8

    goto/16 :goto_6

    :catch_e
    move-exception v4

    move v11, v5

    goto :goto_15

    :catch_f
    move-exception v6

    move v11, v5

    move v9, v4

    goto :goto_15

    :catch_10
    move-exception v5

    move-object v7, v4

    goto :goto_15

    :catch_11
    move-exception v5

    move v11, v4

    goto :goto_15

    :catch_12
    move-exception v8

    move-object v10, v9

    move v11, v4

    move-object v8, v5

    move v9, v6

    goto :goto_15

    :catch_13
    move-exception v7

    move-object v7, v8

    move-object v10, v9

    move v11, v4

    move-object v8, v5

    move v9, v6

    goto :goto_15

    :catch_14
    move-exception v7

    move-object v7, v8

    move v11, v4

    move v9, v6

    move-object v8, v5

    goto :goto_15

    :catch_15
    move-exception v4

    move v11, v5

    goto/16 :goto_14

    :catch_16
    move-exception v6

    move v11, v5

    move v9, v4

    goto/16 :goto_14

    :catch_17
    move-exception v5

    move-object v7, v4

    goto/16 :goto_14

    :catch_18
    move-exception v5

    move v11, v4

    goto/16 :goto_14

    :catch_19
    move-exception v8

    move-object v10, v9

    move v11, v4

    move-object v8, v5

    move v9, v6

    goto/16 :goto_14

    :catch_1a
    move-exception v7

    move-object v7, v8

    move-object v10, v9

    move v11, v4

    move-object v8, v5

    move v9, v6

    goto/16 :goto_14

    :catch_1b
    move-exception v7

    move-object v7, v8

    move v11, v4

    move v9, v6

    move-object v8, v5

    goto/16 :goto_14

    :catch_1c
    move-exception v4

    move-object v4, v10

    goto/16 :goto_4

    :catch_1d
    move-exception v6

    move v9, v4

    move-object v4, v10

    goto/16 :goto_4

    :catch_1e
    move-exception v5

    move-object v7, v4

    move v5, v11

    move-object v4, v10

    goto/16 :goto_4

    :catch_1f
    move-exception v5

    move v5, v4

    move-object v4, v10

    goto/16 :goto_4

    :catch_20
    move-exception v8

    move-object v8, v5

    move v5, v4

    move-object v4, v9

    move v9, v6

    goto/16 :goto_4

    :catch_21
    move-exception v7

    move-object v7, v8

    move v9, v6

    move-object v8, v5

    move v5, v4

    move-object v4, v10

    goto/16 :goto_4

    :catch_22
    move-exception v9

    goto/16 :goto_13

    :cond_1f
    move v11, v7

    goto/16 :goto_2

    :cond_20
    move v5, v7

    goto/16 :goto_e

    :cond_21
    move v14, v12

    move v12, v13

    goto/16 :goto_f

    :cond_22
    move-object/from16 v16, v4

    move/from16 v17, v14

    move v4, v13

    goto/16 :goto_b

    :cond_23
    move v4, v11

    goto/16 :goto_d

    :cond_24
    move/from16 v18, v15

    move/from16 v19, v17

    goto/16 :goto_9
.end method
