.class public final Lcom/widevine/drm/internal/m;
.super Lcom/widevine/drm/internal/aa;


# instance fields
.field private b:Landroid/content/pm/ApplicationInfo;

.field private c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/widevine/drm/internal/ab;Landroid/content/pm/ApplicationInfo;Ljava/util/HashMap;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/widevine/drm/internal/ab;",
            "Landroid/content/pm/ApplicationInfo;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/widevine/drm/internal/aa;-><init>(Lcom/widevine/drm/internal/ab;)V

    iput-object v0, p0, Lcom/widevine/drm/internal/m;->b:Landroid/content/pm/ApplicationInfo;

    iput-object v0, p0, Lcom/widevine/drm/internal/m;->c:Ljava/util/HashMap;

    iput-object p2, p0, Lcom/widevine/drm/internal/m;->b:Landroid/content/pm/ApplicationInfo;

    iput-object p3, p0, Lcom/widevine/drm/internal/m;->c:Ljava/util/HashMap;

    return-void
.end method

.method private a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V
    .locals 3

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->Initialized:Lcom/widevine/drmapi/android/WVEvent;

    const-string v2, "WVStatusKey"

    invoke-virtual {v1, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    if-eq p1, v2, :cond_0

    const-string v0, "WVErrorKey"

    invoke-virtual {v1, v0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/widevine/drmapi/android/WVEvent;->InitializeFailed:Lcom/widevine/drmapi/android/WVEvent;

    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/widevine/drm/internal/m;->a(Lcom/widevine/drmapi/android/WVEvent;Ljava/util/HashMap;)V

    return-void
.end method

.method private a(Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/widevine/drm/internal/ad;
        }
    .end annotation

    const-string v1, "WVPortalKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "WVPortalKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    if-nez v5, :cond_1

    new-instance v1, Lcom/widevine/drm/internal/ad;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->MandatorySettingAbsent:Lcom/widevine/drmapi/android/WVStatus;

    const-string v3, "WVPortalKey null"

    invoke-direct {v1, v2, v3}, Lcom/widevine/drm/internal/ad;-><init>(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v1, Lcom/widevine/drm/internal/ad;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->MandatorySettingAbsent:Lcom/widevine/drmapi/android/WVStatus;

    const-string v3, "WVPortalKey absent"

    invoke-direct {v1, v2, v3}, Lcom/widevine/drm/internal/ad;-><init>(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    throw v1

    :cond_1
    const-string v1, "WVDRMServer"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "WVDRMServer"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    if-nez v3, :cond_3

    new-instance v1, Lcom/widevine/drm/internal/ad;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->MandatorySettingAbsent:Lcom/widevine/drmapi/android/WVStatus;

    const-string v3, "WVDRMServer null"

    invoke-direct {v1, v2, v3}, Lcom/widevine/drm/internal/ad;-><init>(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    throw v1

    :cond_2
    new-instance v1, Lcom/widevine/drm/internal/ad;

    sget-object v2, Lcom/widevine/drmapi/android/WVStatus;->MandatorySettingAbsent:Lcom/widevine/drmapi/android/WVStatus;

    const-string v3, "WVDRMServer absent"

    invoke-direct {v1, v2, v3}, Lcom/widevine/drm/internal/ad;-><init>(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    throw v1

    :cond_3
    const-string v4, ""

    const-string v1, "WVAssetDBPathKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "WVAssetDBPathKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v4, v1

    :cond_4
    const-string v6, ""

    const-string v1, "WVCAUserDataKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "WVCAUserDataKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v6, v1

    :cond_5
    const-string v7, ""

    const-string v1, "WVDeviceIDKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "WVDeviceIDKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v7, v1

    :cond_6
    const-string v8, ""

    const-string v1, "WVStreamIDKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "WVStreamIDKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v8, v1

    :cond_7
    const-string v1, "WVLicenseTypeKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    const-string v1, "WVLicenseTypeKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v9

    :goto_0
    const-string v10, ""

    const-string v1, "WVAndroidIDKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "WVAndroidIDKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v10, v1

    :cond_8
    const-string v11, ""

    const-string v1, "WVIMEIKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "WVIMEIKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v11, v1

    :cond_9
    const-string v12, ""

    const-string v1, "WVWifiMacKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v1, "WVWifiMacKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v12, v1

    :cond_a
    const-string v13, ""

    const-string v1, "WVHWDeviceKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "WVHWDeviceKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v13, v1

    :cond_b
    const-string v14, ""

    const-string v1, "WVHWModelKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    const-string v1, "WVHWModelKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v14, v1

    :cond_c
    const-string v15, ""

    const-string v1, "WVUIDKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    const-string v1, "WVUIDKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v15, v1

    :cond_d
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/widevine/drm/internal/m;->b:Landroid/content/pm/ApplicationInfo;

    if-eqz v1, :cond_e

    const/4 v2, 0x0

    :cond_e
    invoke-static {}, Lcom/widevine/drm/internal/HTTPDecrypter;->a()Lcom/widevine/drm/internal/HTTPDecrypter;

    move-result-object v1

    move-object/from16 v16, p2

    invoke-virtual/range {v1 .. v16}, Lcom/widevine/drm/internal/HTTPDecrypter;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_f
    const/4 v9, 0x0

    goto/16 :goto_0
.end method

.method private static b(I)C
    .locals 2

    rem-int/lit8 v0, p0, 0x3e

    const/16 v1, 0x1a

    if-ge v0, v1, :cond_0

    add-int/lit8 v0, v0, 0x41

    :goto_0
    int-to-char v0, v0

    return v0

    :cond_0
    const/16 v1, 0x34

    if-ge v0, v1, :cond_1

    add-int/lit8 v0, v0, 0x47

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, -0x4

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/HashMap;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/widevine/drm/internal/ad;
        }
    .end annotation

    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v0, 0x0

    :goto_0
    const/16 v3, 0xe

    if-ge v0, v3, :cond_0

    const/16 v3, 0x3e

    invoke-virtual {v1, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/widevine/drm/internal/m;->b(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/widevine/drm/internal/m;->a(Ljava/util/HashMap;Ljava/lang/String;)V

    return-void
.end method

.method public final run()V
    .locals 10

    const/high16 v9, 0xf000000

    const/high16 v8, -0x10000000

    new-instance v1, Lcom/widevine/drm/internal/y;

    invoke-direct {v1}, Lcom/widevine/drm/internal/y;-><init>()V

    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    iget-object v0, p0, Lcom/widevine/drm/internal/m;->c:Ljava/util/HashMap;

    const-string v3, "WVAndroidIDKey"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/widevine/drm/internal/m;->c:Ljava/util/HashMap;

    const-string v3, "WVAndroidIDKey"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    new-instance v3, Ljava/util/zip/CRC32;

    invoke-direct {v3}, Ljava/util/zip/CRC32;-><init>()V

    const/4 v4, 0x0

    array-length v5, v0

    invoke-virtual {v3, v0, v4, v5}, Ljava/util/zip/CRC32;->update([BII)V

    invoke-virtual {v3}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    and-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/util/Random;->setSeed(J)V

    :cond_0
    invoke-static {}, Lcom/widevine/drm/internal/HTTPDecrypter;->a()Lcom/widevine/drm/internal/HTTPDecrypter;

    move-result-object v0

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/widevine/drm/internal/HTTPDecrypter;->gin(I)I

    move-result v0

    :try_start_0
    invoke-virtual {v1, v0}, Lcom/widevine/drm/internal/y;->a(I)I

    move-result v3

    if-nez v3, :cond_1

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    const-string v1, "serror (11) "

    invoke-direct {p0, v0, v1}, Lcom/widevine/drm/internal/m;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "serror (12) "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/widevine/drm/internal/m;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/widevine/drm/internal/m;->b:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v1, v0, v4}, Lcom/widevine/drm/internal/y;->a(ILandroid/content/pm/ApplicationInfo;)I

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    const-string v1, "serror (13) "

    invoke-direct {p0, v0, v1}, Lcom/widevine/drm/internal/m;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lcom/widevine/drmapi/android/WVStatus;->TamperDetected:Lcom/widevine/drmapi/android/WVStatus;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "serror (14) "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/widevine/drm/internal/m;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_2
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    and-int/lit8 v4, v0, 0xf

    invoke-static {v4}, Lcom/widevine/drm/internal/m;->b(I)C

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/16 v4, 0x3e

    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/widevine/drm/internal/m;->b(I)C

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int v4, v3, v9

    ushr-int/lit8 v4, v4, 0x18

    invoke-static {v4}, Lcom/widevine/drm/internal/m;->b(I)C

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int/lit16 v4, v3, 0xf0

    ushr-int/lit8 v4, v4, 0x4

    invoke-static {v4}, Lcom/widevine/drm/internal/m;->b(I)C

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/16 v4, 0x3e

    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/widevine/drm/internal/m;->b(I)C

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int v4, v0, v9

    ushr-int/lit8 v4, v4, 0x18

    invoke-static {v4}, Lcom/widevine/drm/internal/m;->b(I)C

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int/lit16 v4, v3, 0x3f00

    ushr-int/lit8 v4, v4, 0x8

    invoke-static {v4}, Lcom/widevine/drm/internal/m;->b(I)C

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int/lit16 v4, v0, 0xf0

    ushr-int/lit8 v4, v4, 0x4

    invoke-static {v4}, Lcom/widevine/drm/internal/m;->b(I)C

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int v4, v3, v8

    ushr-int/lit8 v4, v4, 0x1c

    invoke-static {v4}, Lcom/widevine/drm/internal/m;->b(I)C

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/16 v4, 0x3e

    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/widevine/drm/internal/m;->b(I)C

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int/lit16 v4, v0, 0x3f00

    ushr-int/lit8 v4, v4, 0x8

    invoke-static {v4}, Lcom/widevine/drm/internal/m;->b(I)C

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int/lit8 v3, v3, 0xf

    invoke-static {v3}, Lcom/widevine/drm/internal/m;->b(I)C

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    and-int/2addr v0, v8

    ushr-int/lit8 v0, v0, 0x1c

    invoke-static {v0}, Lcom/widevine/drm/internal/m;->b(I)C

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/16 v0, 0x3e

    invoke-virtual {v2, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/widevine/drm/internal/m;->b(I)C

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    iget-object v0, p0, Lcom/widevine/drm/internal/m;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/widevine/drm/internal/m;->a(Ljava/util/HashMap;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/widevine/drm/internal/ad; {:try_start_2 .. :try_end_2} :catch_2

    iget-object v0, p0, Lcom/widevine/drm/internal/m;->a:Lcom/widevine/drm/internal/ab;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/ab;->a()V

    sget-object v0, Lcom/widevine/drmapi/android/WVStatus;->OK:Lcom/widevine/drmapi/android/WVStatus;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/widevine/drm/internal/m;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_2
    move-exception v0

    iget-object v1, v0, Lcom/widevine/drm/internal/ad;->a:Lcom/widevine/drmapi/android/WVStatus;

    invoke-virtual {v0}, Lcom/widevine/drm/internal/ad;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/widevine/drm/internal/m;->a(Lcom/widevine/drmapi/android/WVStatus;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
