.class final Lsb;
.super Lru;


# instance fields
.field final synthetic a:Lrz;

.field private final b:Lach;

.field private final c:Lacg;

.field private final d:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lrz;Lacg;Landroid/net/Uri;)V
    .locals 1

    iput-object p1, p0, Lsb;->a:Lrz;

    invoke-direct {p0}, Lru;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lsb;->b:Lach;

    iput-object p2, p0, Lsb;->c:Lacg;

    iput-object p3, p0, Lsb;->d:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;ILandroid/content/Intent;)V
    .locals 7

    iget-object v0, p0, Lsb;->d:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsb;->a:Lrz;

    invoke-virtual {v0}, Lrz;->i()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lsb;->d:Landroid/net/Uri;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    :cond_0
    const/4 v0, 0x0

    if-eqz p2, :cond_1

    const-string v0, "pendingIntent"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    :cond_1
    new-instance v3, Lbz;

    invoke-direct {v3, p1, v0}, Lbz;-><init>(ILandroid/app/PendingIntent;)V

    iget-object v0, p0, Lsb;->b:Lach;

    if-eqz v0, :cond_2

    iget-object v6, p0, Lsb;->a:Lrz;

    new-instance v0, Lsa;

    iget-object v1, p0, Lsb;->a:Lrz;

    iget-object v2, p0, Lsb;->b:Lach;

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lsa;-><init>(Lrz;Lach;Lbz;ILandroid/content/Intent;)V

    invoke-virtual {v6, v0}, Lrz;->a(Lnb;)V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lsb;->a:Lrz;

    new-instance v1, Lsc;

    iget-object v2, p0, Lsb;->a:Lrz;

    iget-object v4, p0, Lsb;->c:Lacg;

    invoke-direct {v1, v2, v4, v3, p4}, Lsc;-><init>(Lrz;Lacg;Lbz;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Lrz;->a(Lnb;)V

    goto :goto_0
.end method
