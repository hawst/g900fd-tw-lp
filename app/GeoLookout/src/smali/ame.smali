.class public Lame;
.super Ljava/lang/Object;
.source "HistorysDbTable.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# static fields
.field public static final A:Ljava/lang/String; = "JPN_DETAILSZONECOUNTRY"

.field public static final B:Ljava/lang/String; = "JPN_DETAILSZONECITY"

.field public static final C:I = 0x0

.field public static final D:I = 0x1

.field public static final E:I = 0x2

.field public static final F:I = 0x3

.field public static final G:I = 0x4

.field public static final H:I = 0x5

.field public static final I:I = 0x6

.field public static final J:I = 0x7

.field public static final K:I = 0x8

.field public static final L:I = 0x9

.field public static final M:I = 0xa

.field public static final N:I = 0xb

.field public static final O:I = 0xc

.field public static final P:I = 0xd

.field public static final Q:I = 0xe

.field public static final R:I = 0xf

.field public static final S:I = 0x10

.field public static final T:I = 0x11

.field public static final U:I = 0x12

.field public static final V:I = 0x13

.field public static final W:I = 0x14

.field public static final X:I = 0x15

.field public static final Y:I = 0x16

.field public static final Z:I = 0x17

.field public static final a:Ljava/lang/String; = "STIME"

.field public static final aa:I = 0x18

.field public static final ab:I = 0x19

.field public static final ac:I = 0x1a

.field public static final ad:I = 0x1b

.field public static final ae:I = 0x1c

.field protected static af:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static ag:Ljava/lang/String; = null

.field protected static ah:Ljava/lang/String; = null

.field protected static ai:Ljava/lang/String; = null

.field protected static aj:Ljava/lang/String; = null

.field protected static ak:[Ljava/lang/String; = null

.field public static al:Ljava/lang/String; = null

.field public static final b:Ljava/lang/String; = "ETIME"

.field public static final c:Ljava/lang/String; = "RTIME"

.field public static final d:Ljava/lang/String; = "DID"

.field public static final e:Ljava/lang/String; = "DTYPE"

.field public static final f:Ljava/lang/String; = "DTYPE_ID"

.field public static final g:Ljava/lang/String; = "LINK"

.field public static final h:Ljava/lang/String; = "HEADLINE"

.field public static final i:Ljava/lang/String; = "LOCATIONID"

.field public static final j:Ljava/lang/String; = "POLYGONJSON"

.field public static final k:Ljava/lang/String; = "CITY"

.field public static final l:Ljava/lang/String; = "COUNTRY"

.field public static final m:Ljava/lang/String; = "LATITUDE"

.field public static final n:Ljava/lang/String; = "LONGITUDE"

.field public static final o:Ljava/lang/String; = "DETAILSINFO"

.field public static final p:Ljava/lang/String; = "SIGNIFICANCE"

.field public static final q:Ljava/lang/String; = "LOCATIONTYPE"

.field public static final r:Ljava/lang/String; = "DISTANCE"

.field public static final s:Ljava/lang/String; = "POLLING"

.field public static final t:Ljava/lang/String; = "ACTION_STATUS"

.field public static final u:Ljava/lang/String; = "DETAILSPEED"

.field public static final v:Ljava/lang/String; = "created"

.field public static final w:Ljava/lang/String; = "modified"

.field public static final x:Ljava/lang/String; = "MULTI_POINT_UID"

.field public static final y:Ljava/lang/String; = "JPN_DETAILSDEPTH"

.field public static final z:Ljava/lang/String; = "JPN_DETAILSLEVEL"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 88
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lame;->af:Ljava/util/HashMap;

    .line 89
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "STIME"

    const-string v2, "STIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "ETIME"

    const-string v2, "ETIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "RTIME"

    const-string v2, "RTIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "DID"

    const-string v2, "DID"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "DTYPE"

    const-string v2, "DTYPE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "DTYPE_ID"

    const-string v2, "DTYPE_ID"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "LINK"

    const-string v2, "LINK"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "HEADLINE"

    const-string v2, "HEADLINE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "LOCATIONID"

    const-string v2, "LOCATIONID"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "POLYGONJSON"

    const-string v2, "POLYGONJSON"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "CITY"

    const-string v2, "CITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "COUNTRY"

    const-string v2, "COUNTRY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "LATITUDE"

    const-string v2, "LATITUDE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "LONGITUDE"

    const-string v2, "LONGITUDE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "DETAILSINFO"

    const-string v2, "DETAILSINFO"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "SIGNIFICANCE"

    const-string v2, "SIGNIFICANCE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "LOCATIONTYPE"

    const-string v2, "LOCATIONTYPE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "DISTANCE"

    const-string v2, "DISTANCE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "POLLING"

    const-string v2, "POLLING"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "ACTION_STATUS"

    const-string v2, "ACTION_STATUS"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "DETAILSPEED"

    const-string v2, "DETAILSPEED"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "created"

    const-string v2, "created"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "modified"

    const-string v2, "modified"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "MULTI_POINT_UID"

    const-string v2, "MULTI_POINT_UID"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "JPN_DETAILSDEPTH"

    const-string v2, "JPN_DETAILSDEPTH"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "JPN_DETAILSLEVEL"

    const-string v2, "JPN_DETAILSLEVEL"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "JPN_DETAILSZONECOUNTRY"

    const-string v2, "JPN_DETAILSZONECOUNTRY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    const-string v1, "JPN_DETAILSZONECITY"

    const-string v2, "JPN_DETAILSZONECITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    const-string v0, "CREATE TABLE HISTORY_DISASTER_TBL (_id INTEGER PRIMARY KEY,STIME LONG,ETIME LONG,RTIME LONG,DID TEXT UNIQUE ON CONFLICT REPLACE,DTYPE TEXT,DTYPE_ID INTEGER,LINK TEXT,HEADLINE TEXT,LOCATIONID TEXT,POLYGONJSON TEXT,CITY TEXT,COUNTRY TEXT,LATITUDE DOUBLE,LONGITUDE DOUBLE,DETAILSINFO DOUBLE,SIGNIFICANCE INTEGER,LOCATIONTYPE INTEGER,DISTANCE INTEGER,POLLING SHORT,DETAILSPEED TEXT,ACTION_STATUS SHORT,created INTEGER,modified INTEGER,JPN_DETAILSDEPTH TEXT,JPN_DETAILSLEVEL TEXT,JPN_DETAILSZONECOUNTRY TEXT,JPN_DETAILSZONECITY TEXT,MULTI_POINT_UID INTEGER"

    sput-object v0, Lame;->ag:Ljava/lang/String;

    .line 153
    const-string v0, "CREATE TABLE HISTORY_DISASTER_TBL (_id INTEGER PRIMARY KEY,STIME LONG,ETIME LONG,RTIME LONG,DID TEXT UNIQUE ON CONFLICT REPLACE,DTYPE TEXT,DTYPE_ID INTEGER,LINK TEXT,HEADLINE TEXT,LOCATIONID TEXT,POLYGONJSON TEXT,CITY TEXT,COUNTRY TEXT,LATITUDE DOUBLE,LONGITUDE DOUBLE,DETAILSINFO DOUBLE,SIGNIFICANCE INTEGER,LOCATIONTYPE INTEGER,DISTANCE INTEGER,POLLING SHORT,DETAILSPEED TEXT,ACTION_STATUS SHORT,created INTEGER,modified INTEGER,JPN_DETAILSDEPTH TEXT,JPN_DETAILSLEVEL TEXT,JPN_DETAILSZONECOUNTRY TEXT,JPN_DETAILSZONECITY TEXT,MULTI_POINT_UID INTEGER"

    sput-object v0, Lame;->ah:Ljava/lang/String;

    .line 186
    const-string v0, "_id,STIME,ETIME,RTIME,DID,DTYPE,DTYPE_ID,LINK,HEADLINE,LOCATIONID,POLYGONJSON,CITY,COUNTRY,LATITUDE,LONGITUDE,DETAILSINFO,SIGNIFICANCE,LOCATIONTYPE,DISTANCE,POLLING,DETAILSPEED,ACTION_STATUS,created,modified"

    sput-object v0, Lame;->ai:Ljava/lang/String;

    .line 211
    const-string v0, "JPN_DETAILSDEPTH,JPN_DETAILSLEVEL,JPN_DETAILSZONECOUNTRY,JPN_DETAILSZONECITY"

    sput-object v0, Lame;->aj:Ljava/lang/String;

    .line 216
    const-string v0, ");"

    sput-object v0, Lame;->al:Ljava/lang/String;

    .line 218
    const/16 v0, 0x1d

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "STIME"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ETIME"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "RTIME"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "DID"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "DTYPE"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "DTYPE_ID"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "LINK"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "HEADLINE"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "LOCATIONID"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "POLYGONJSON"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "CITY"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "COUNTRY"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "LATITUDE"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "LONGITUDE"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "DETAILSINFO"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "SIGNIFICANCE"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "LOCATIONTYPE"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "DISTANCE"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "POLLING"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "ACTION_STATUS"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "DETAILSPEED"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "created"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "modified"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "JPN_DETAILSDEPTH"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "JPN_DETAILSLEVEL"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "JPN_DETAILSZONECOUNTRY"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "JPN_DETAILSZONECITY"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "MULTI_POINT_UID"

    aput-object v2, v0, v1

    sput-object v0, Lame;->ak:[Ljava/lang/String;

    .line 251
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 326
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 327
    const-string v1, "STIME"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getStartTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 328
    const-string v1, "ETIME"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getEndTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 329
    const-string v1, "RTIME"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getReceivedTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 330
    const-string v1, "DID"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    const-string v1, "DTYPE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    const-string v1, "DTYPE_ID"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 333
    const-string v1, "LINK"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLink()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    const-string v1, "HEADLINE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getHeadline()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    const-string v1, "LOCATIONID"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    const-string v1, "POLYGONJSON"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getPolygonJSON()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    const-string v1, "CITY"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getCity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    const-string v1, "COUNTRY"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const-string v1, "LATITUDE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v2

    iget-wide v2, v2, Lamf;->a:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 340
    const-string v1, "LONGITUDE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v2

    iget-wide v2, v2, Lamf;->b:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 341
    const-string v1, "DETAILSINFO"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailsMagnitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 342
    const-string v1, "SIGNIFICANCE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 343
    const-string v1, "LOCATIONTYPE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 344
    const-string v1, "DISTANCE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDistance()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 345
    const-string v1, "POLLING"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getPolling()S

    move-result v2

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    .line 346
    const-string v1, "DETAILSPEED"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailSpeed()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    const-string v1, "ACTION_STATUS"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getActionStatus()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 349
    const-string v1, "JPN_DETAILSDEPTH"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsDepth()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    const-string v1, "JPN_DETAILSLEVEL"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsLevel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    const-string v1, "JPN_DETAILSZONECOUNTRY"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsZoneCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    const-string v1, "JPN_DETAILSZONECITY"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsZoneCity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    const-string v1, "MULTI_POINT_UID"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 355
    return-object v0
.end method

.method protected a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 8

    .prologue
    const/16 v4, 0xa

    .line 289
    new-instance v0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    invoke-direct {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;-><init>()V

    .line 290
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setId(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setStartTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setEndTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setReceivedTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDid(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x5

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setType(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x6

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setTypeId(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x7

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLink(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x8

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setHeadline(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x9

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocationID(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setPolygonJSON(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/GeoLookout/db/DisasterParser;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setPolygonList(Ljava/util/ArrayList;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0xb

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setCity(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0xc

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setCountry(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    new-instance v2, Lamf;

    const/16 v3, 0xd

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    const/16 v3, 0xe

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lamf;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocation(Lamf;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0xf

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailsMagnitude(D)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x10

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setSignificance(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x11

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocationType(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x12

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDistance(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x13

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getShort(I)S

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setPolling(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x15

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailSpeed(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x14

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getShort(I)S

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setActionStatus(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x1c

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setMultiPointUid(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x18

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsDepth(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x19

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsLevel(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x1a

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsZoneCountry(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x1b

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsZoneCity(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 317
    return-object v0
.end method

.method public a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 254
    sget-object v0, Lame;->ak:[Ljava/lang/String;

    return-object v0
.end method

.method public b(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 1

    .prologue
    .line 321
    invoke-virtual {p0, p1}, Lame;->a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v0

    .line 322
    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->build()Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 258
    sget-object v0, Lame;->af:Ljava/util/HashMap;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 262
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lame;->ag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lame;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 263
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCreateDbTableString : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 264
    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 268
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lame;->ah:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lame;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 269
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCreateDbTableString : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 270
    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lame;->ai:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lame;->aj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 275
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getUpgradeDbTableStringForV17 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 276
    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 280
    sget-object v0, Lame;->ai:Ljava/lang/String;

    .line 281
    sget-boolean v1, Laky;->b:Z

    if-eqz v1, :cond_0

    .line 282
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lame;->aj:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 284
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getUpgradeDbTableStringForV19 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 285
    return-object v0
.end method
