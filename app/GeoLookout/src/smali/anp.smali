.class Lanp;
.super Lcom/loopj/android/http/AsyncHttpResponseHandler;
.source "DisasterServerInterface.java"


# instance fields
.field final synthetic a:Lanh;


# direct methods
.method constructor <init>(Lanh;)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lanp;->a:Lanh;

    invoke-direct {p0}, Lcom/loopj/android/http/AsyncHttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 236
    invoke-super {p0, p1, p2, p3, p4}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onFailure(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V

    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unregisterDisasterServer: onFailure(statusCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Lanp;->a:Lanh;

    invoke-static {v0}, Lanh;->c(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 239
    iget-object v0, p0, Lanp;->a:Lanh;

    invoke-static {v0}, Lanh;->c(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 240
    iget-object v0, p0, Lanp;->a:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lanp;->a:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lage;->f(Landroid/content/Context;Z)V

    .line 241
    return-void
.end method

.method public onFinish()V
    .locals 1

    .prologue
    .line 245
    const-string v0, "unregisterDisasterServer: onFinish()  "

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 247
    return-void
.end method

.method public onRetry()V
    .locals 1

    .prologue
    .line 251
    const-string v0, "unregisterDisasterServer: onRetry() "

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 252
    return-void
.end method

.method public onSuccess(I[Lorg/apache/http/Header;[B)V
    .locals 2

    .prologue
    .line 223
    invoke-super {p0, p1, p2, p3}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onSuccess(I[Lorg/apache/http/Header;[B)V

    .line 224
    const-string v0, "unregisterDisasterServer: onSuccess "

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lanp;->a:Lanh;

    invoke-static {v0}, Lanh;->c(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 226
    iget-object v0, p0, Lanp;->a:Lanh;

    invoke-static {v0}, Lanh;->c(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 228
    iget-object v0, p0, Lanp;->a:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lall;->b(Landroid/content/Context;)V

    .line 229
    iget-object v0, p0, Lanp;->a:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lanp;->a:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lage;->r(Landroid/content/Context;)V

    .line 230
    iget-object v0, p0, Lanp;->a:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Laog;->a(Landroid/content/Context;Z)V

    .line 231
    invoke-static {}, Laop;->a()Laop;

    move-result-object v0

    iget-object v1, p0, Lanp;->a:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Laop;->b(Landroid/content/Context;)V

    .line 232
    return-void
.end method
