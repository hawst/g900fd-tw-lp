.class public Laqc;
.super Ljava/lang/Object;
.source "SafetyCareConnectAgreeDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Laqc;->a:Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->a:Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;->a(Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    iget-object v0, p0, Laqc;->a:Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;->a(Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lall;->x(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 63
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 64
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    iget-object v1, p0, Laqc;->a:Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;

    const/16 v2, 0x6f

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;->startActivityForResult(Landroid/content/Intent;I)V

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    iget-object v0, p0, Laqc;->a:Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;->a(Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Laqc;->a:Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;->a(Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lage;->e(Landroid/content/Context;)V

    .line 68
    sget-boolean v0, Laky;->k:Z

    if-eqz v0, :cond_2

    .line 69
    iget-object v0, p0, Laqc;->a:Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;->a(Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 70
    invoke-static {}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a()Lcom/sec/android/GeoLookout/widget/LifeModeWidget;

    move-result-object v1

    iget-object v2, p0, Laqc;->a:Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;

    invoke-static {v2}, Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;->a(Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 71
    invoke-static {}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a()Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;

    move-result-object v1

    iget-object v2, p0, Laqc;->a:Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;

    invoke-static {v2}, Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;->a(Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 73
    :cond_2
    iget-object v0, p0, Laqc;->a:Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;->a(Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 74
    invoke-static {}, Laqs;->a()Laqs;

    move-result-object v1

    iget-object v2, p0, Laqc;->a:Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;

    invoke-static {v2}, Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;->a(Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Laqs;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 75
    iget-object v0, p0, Laqc;->a:Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;->finish()V

    goto :goto_0
.end method
