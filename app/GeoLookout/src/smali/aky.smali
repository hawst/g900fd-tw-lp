.class public Laky;
.super Ljava/lang/Object;
.source "DisasterFeatures.java"


# static fields
.field public static final a:Z

.field public static final b:Z

.field public static final c:Z

.field public static final d:Z

.field public static final e:Z

.field public static final f:Z

.field public static final g:Z

.field public static final h:Z

.field public static final i:Z

.field public static final j:Z

.field public static final k:Z

.field public static final l:Z = true

.field public static final m:Z = true

.field public static final n:Z = true

.field public static final o:Z = true

.field public static final p:Z = true

.field public static final q:Z = true

.field public static final r:Z

.field public static final s:Z = true

.field public static final t:Z

.field private static u:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 8
    const-string v0, "KOREA"

    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Laky;->a:Z

    .line 9
    const-string v0, "JP"

    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Laky;->b:Z

    .line 10
    const-string v0, "CHINA"

    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Laky;->c:Z

    .line 11
    const-string v0, "USA"

    const-string v3, "ro.csc.country_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Laky;->d:Z

    .line 13
    const-string v0, "VZW"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Laky;->e:Z

    .line 15
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Laky;->u:Ljava/lang/String;

    .line 17
    sget-object v0, Laky;->u:Ljava/lang/String;

    const-string v3, "tb"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Laky;->u:Ljava/lang/String;

    const-string v3, "tr"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Laky;->u:Ljava/lang/String;

    const-string v3, "SC-01G"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Laky;->u:Ljava/lang/String;

    const-string v3, "SCL24"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Laky;->f:Z

    .line 18
    sget-object v0, Laky;->u:Ljava/lang/String;

    const-string v3, "lentislte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Laky;->g:Z

    .line 19
    sget-object v0, Laky;->u:Ljava/lang/String;

    const-string v3, "klte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Laky;->h:Z

    .line 20
    const-string v0, "SM-G850X"

    const-string v3, "ro.product.model"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Laky;->i:Z

    .line 22
    sget-boolean v0, Laky;->a:Z

    sput-boolean v0, Laky;->j:Z

    .line 23
    sget-boolean v0, Laky;->j:Z

    if-nez v0, :cond_1

    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_3

    :cond_1
    move v0, v2

    :goto_1
    sput-boolean v0, Laky;->k:Z

    .line 31
    sget-boolean v0, Laky;->f:Z

    if-eqz v0, :cond_4

    sget-boolean v0, Laky;->e:Z

    if-nez v0, :cond_4

    move v0, v2

    :goto_2
    sput-boolean v0, Laky;->r:Z

    .line 33
    sget-boolean v0, Laky;->c:Z

    if-nez v0, :cond_5

    :goto_3
    sput-boolean v2, Laky;->t:Z

    return-void

    :cond_2
    move v0, v1

    .line 17
    goto :goto_0

    :cond_3
    move v0, v1

    .line 23
    goto :goto_1

    :cond_4
    move v0, v1

    .line 31
    goto :goto_2

    :cond_5
    move v2, v1

    .line 33
    goto :goto_3
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
