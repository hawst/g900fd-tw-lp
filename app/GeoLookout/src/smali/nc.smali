.class public abstract Lnc;
.super Lnb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T",
        "Listener:Ljava/lang/Object;",
        ">",
        "Lmz",
        "<TT;>.nb<TT",
        "Listener;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/common/data/d;

.field final synthetic c:Lmz;


# direct methods
.method public constructor <init>(Lmz;Ljava/lang/Object;Lcom/google/android/gms/common/data/d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT",
            "Listener;",
            "Lcom/google/android/gms/common/data/d;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lnc;->c:Lmz;

    invoke-direct {p0, p1, p2}, Lnb;-><init>(Lmz;Ljava/lang/Object;)V

    iput-object p3, p0, Lnc;->a:Lcom/google/android/gms/common/data/d;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 1

    iget-object v0, p0, Lnc;->a:Lcom/google/android/gms/common/data/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnc;->a:Lcom/google/android/gms/common/data/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/d;->i()V

    :cond_0
    return-void
.end method

.method protected final a(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT",
            "Listener;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lnc;->a:Lcom/google/android/gms/common/data/d;

    invoke-virtual {p0, p1, v0}, Lnc;->a(Ljava/lang/Object;Lcom/google/android/gms/common/data/d;)V

    return-void
.end method

.method protected abstract a(Ljava/lang/Object;Lcom/google/android/gms/common/data/d;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT",
            "Listener;",
            "Lcom/google/android/gms/common/data/d;",
            ")V"
        }
    .end annotation
.end method

.method public bridge synthetic b()V
    .locals 0

    invoke-super {p0}, Lnb;->b()V

    return-void
.end method

.method public bridge synthetic c()V
    .locals 0

    invoke-super {p0}, Lnb;->c()V

    return-void
.end method

.method public bridge synthetic d()V
    .locals 0

    invoke-super {p0}, Lnb;->d()V

    return-void
.end method
