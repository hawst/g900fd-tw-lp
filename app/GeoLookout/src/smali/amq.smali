.class public Lamq;
.super Ljava/lang/Object;
.source "LifemodeValue.java"


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field private static final u:[I

.field private static final v:[I

.field private static final w:[I

.field private static final x:[I

.field private static final y:[I


# instance fields
.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:Ljava/lang/String;

.field private m:I

.field private n:I

.field private o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private p:I

.field private q:I

.field private r:J

.field private s:I

.field private t:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x6

    const/4 v1, 0x4

    .line 47
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lamq;->u:[I

    .line 53
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lamq;->v:[I

    .line 59
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lamq;->w:[I

    .line 67
    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lamq;->x:[I

    .line 74
    new-array v0, v1, [I

    fill-array-data v0, :array_4

    sput-object v0, Lamq;->y:[I

    return-void

    .line 47
    nop

    :array_0
    .array-data 4
        0x7f0a00e1
        0x7f0a00e2
        0x7f0a00e3
        0x7f0a00e4
        0x7f0a00e5
        0x7f0a00e6
    .end array-data

    .line 53
    :array_1
    .array-data 4
        0x7f0a00e1
        0x7f0a00e8
        0x7f0a00e9
        0x7f0a00ea
        0x7f0a00eb
        0x7f0a00ec
    .end array-data

    .line 59
    :array_2
    .array-data 4
        0x7f0a00e1
        0x7f0a02ac
        0x7f0a02ad
        0x7f0a02ae
        0x7f0a02af
    .end array-data

    .line 67
    :array_3
    .array-data 4
        0x7f0a00e1
        0x7f0a02b0
        0x7f0a02b1
        0x7f0a02b2
    .end array-data

    .line 74
    :array_4
    .array-data 4
        0x7f0a00e1
        0x7f0a02b3
        0x7f0a02b4
        0x7f0a02b5
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x1

    iput v0, p0, Lamq;->n:I

    .line 246
    return-void
.end method

.method static synthetic a(Lamq;I)I
    .locals 0

    .prologue
    .line 11
    iput p1, p0, Lamq;->q:I

    return p1
.end method

.method static synthetic a(Lamq;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 11
    iput-object p1, p0, Lamq;->l:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lamq;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lamq;->o:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic a(Lamq;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 11
    iput-object p1, p0, Lamq;->o:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic b(Lamq;I)I
    .locals 0

    .prologue
    .line 11
    iput p1, p0, Lamq;->p:I

    return p1
.end method

.method static synthetic c(Lamq;I)I
    .locals 0

    .prologue
    .line 11
    iput p1, p0, Lamq;->d:I

    return p1
.end method

.method static synthetic d(Lamq;I)I
    .locals 0

    .prologue
    .line 11
    iput p1, p0, Lamq;->h:I

    return p1
.end method

.method static synthetic e(Lamq;I)I
    .locals 0

    .prologue
    .line 11
    iput p1, p0, Lamq;->i:I

    return p1
.end method

.method static synthetic f(Lamq;I)I
    .locals 0

    .prologue
    .line 11
    iput p1, p0, Lamq;->m:I

    return p1
.end method

.method static synthetic g(Lamq;I)I
    .locals 0

    .prologue
    .line 11
    iput p1, p0, Lamq;->k:I

    return p1
.end method

.method static synthetic h(Lamq;I)I
    .locals 0

    .prologue
    .line 11
    iput p1, p0, Lamq;->j:I

    return p1
.end method

.method static synthetic i(Lamq;I)I
    .locals 0

    .prologue
    .line 11
    iput p1, p0, Lamq;->g:I

    return p1
.end method

.method static synthetic j(Lamq;I)I
    .locals 0

    .prologue
    .line 11
    iput p1, p0, Lamq;->s:I

    return p1
.end method

.method static synthetic k(Lamq;I)I
    .locals 0

    .prologue
    .line 11
    iput p1, p0, Lamq;->t:I

    return p1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lamq;->e:I

    return v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lamq;->m:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 86
    iput p1, p0, Lamq;->e:I

    .line 87
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 102
    iput-wide p1, p0, Lamq;->r:J

    .line 103
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lamq;->f:I

    return v0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 191
    iget-object v0, p0, Lamq;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lamq;->e:I

    if-lt v0, v1, :cond_0

    .line 192
    iget-object v0, p0, Lamq;->o:Ljava/util/ArrayList;

    iget v1, p0, Lamq;->e:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 194
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 195
    new-instance v1, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Random;-><init>(J)V

    .line 196
    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    array-length v2, v0

    rem-int/2addr v1, v2

    aget-object v0, v0, v1

    .line 198
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 90
    iput p1, p0, Lamq;->f:I

    .line 91
    return-void
.end method

.method public c()J
    .locals 2

    .prologue
    .line 106
    iget-wide v0, p0, Lamq;->r:J

    return-wide v0
.end method

.method public c(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 204
    const-string v0, ""

    .line 206
    iget v0, p0, Lamq;->d:I

    const/16 v1, 0x1f6

    if-eq v0, v1, :cond_0

    iget v0, p0, Lamq;->d:I

    const/16 v1, 0x1f7

    if-ne v0, v1, :cond_1

    .line 208
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lamq;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a02c0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 218
    :goto_0
    return-object v0

    .line 210
    :cond_1
    iget v0, p0, Lamq;->d:I

    const/16 v1, 0x1f5

    if-ne v0, v1, :cond_2

    .line 211
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lamq;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a02c2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 214
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lamq;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a02c3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lamq;->h:I

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lamq;->h:I

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lamq;->i:I

    return v0
.end method

.method public g()I
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 123
    iget v0, p0, Lamq;->e:I

    if-le v0, v1, :cond_0

    .line 124
    iget v0, p0, Lamq;->j:I

    .line 128
    :goto_0
    return v0

    .line 125
    :cond_0
    iget v0, p0, Lamq;->e:I

    if-ne v0, v1, :cond_1

    .line 126
    iget v0, p0, Lamq;->k:I

    goto :goto_0

    .line 128
    :cond_1
    iget v0, p0, Lamq;->g:I

    goto :goto_0
.end method

.method public h()I
    .locals 2

    .prologue
    .line 133
    iget v0, p0, Lamq;->e:I

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    .line 134
    iget v0, p0, Lamq;->q:I

    .line 136
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lamq;->p:I

    goto :goto_0
.end method

.method public i()I
    .locals 2

    .prologue
    .line 142
    iget v0, p0, Lamq;->d:I

    const/16 v1, 0x260

    if-ne v0, v1, :cond_0

    .line 143
    sget-object v0, Lamq;->v:[I

    iget v1, p0, Lamq;->e:I

    aget v0, v0, v1

    .line 145
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lamq;->u:[I

    iget v1, p0, Lamq;->e:I

    aget v0, v0, v1

    goto :goto_0
.end method

.method public j()I
    .locals 2

    .prologue
    .line 149
    iget v0, p0, Lamq;->d:I

    const/16 v1, 0x1f5

    if-eq v0, v1, :cond_0

    iget v0, p0, Lamq;->d:I

    const/16 v1, 0x1f7

    if-eq v0, v1, :cond_0

    iget v0, p0, Lamq;->d:I

    const/16 v1, 0x1f6

    if-ne v0, v1, :cond_1

    .line 153
    :cond_0
    sget-object v0, Lamq;->w:[I

    iget v1, p0, Lamq;->e:I

    aget v0, v0, v1

    .line 157
    :goto_0
    return v0

    .line 154
    :cond_1
    iget v0, p0, Lamq;->d:I

    const/16 v1, 0x1f8

    if-ne v0, v1, :cond_2

    .line 155
    sget-object v0, Lamq;->x:[I

    iget v1, p0, Lamq;->e:I

    aget v0, v0, v1

    goto :goto_0

    .line 157
    :cond_2
    sget-object v0, Lamq;->y:[I

    iget v1, p0, Lamq;->e:I

    aget v0, v0, v1

    goto :goto_0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lamq;->t:I

    return v0
.end method

.method public l()I
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 165
    iget v0, p0, Lamq;->e:I

    if-le v0, v1, :cond_0

    .line 166
    const v0, 0x7f02010b

    .line 170
    :goto_0
    return v0

    .line 167
    :cond_0
    iget v0, p0, Lamq;->e:I

    if-ne v0, v1, :cond_1

    .line 168
    const v0, 0x7f020103

    goto :goto_0

    .line 170
    :cond_1
    const v0, 0x7f020105

    goto :goto_0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lamq;->m:I

    return v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lamq;->l:Ljava/lang/String;

    return-object v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lamq;->d:I

    return v0
.end method

.method public p()Lamq;
    .locals 2

    .prologue
    .line 222
    new-instance v0, Lamq;

    invoke-direct {v0}, Lamq;-><init>()V

    .line 224
    iget v1, p0, Lamq;->d:I

    iput v1, v0, Lamq;->d:I

    .line 225
    iget v1, p0, Lamq;->e:I

    iput v1, v0, Lamq;->e:I

    .line 226
    iget v1, p0, Lamq;->n:I

    iput v1, v0, Lamq;->n:I

    .line 227
    iget v1, p0, Lamq;->m:I

    iput v1, v0, Lamq;->m:I

    .line 228
    iget-object v1, p0, Lamq;->l:Ljava/lang/String;

    iput-object v1, v0, Lamq;->l:Ljava/lang/String;

    .line 229
    iget-object v1, p0, Lamq;->o:Ljava/util/ArrayList;

    iput-object v1, v0, Lamq;->o:Ljava/util/ArrayList;

    .line 230
    iget v1, p0, Lamq;->f:I

    iput v1, v0, Lamq;->f:I

    .line 231
    iget v1, p0, Lamq;->g:I

    iput v1, v0, Lamq;->g:I

    .line 232
    iget v1, p0, Lamq;->h:I

    iput v1, v0, Lamq;->h:I

    .line 233
    iget v1, p0, Lamq;->q:I

    iput v1, v0, Lamq;->q:I

    .line 234
    iget v1, p0, Lamq;->p:I

    iput v1, v0, Lamq;->p:I

    .line 235
    iget v1, p0, Lamq;->s:I

    iput v1, v0, Lamq;->s:I

    .line 236
    iget v1, p0, Lamq;->k:I

    iput v1, v0, Lamq;->k:I

    .line 237
    iget v1, p0, Lamq;->j:I

    iput v1, v0, Lamq;->j:I

    .line 238
    iget v1, p0, Lamq;->i:I

    iput v1, v0, Lamq;->i:I

    .line 239
    iget v1, p0, Lamq;->t:I

    iput v1, v0, Lamq;->t:I

    .line 241
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 250
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lamq;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Value : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lamq;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Level : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lamq;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
