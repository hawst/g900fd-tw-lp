.class public final Leb;
.super Ljava/lang/Object;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private final d:Lcb;

.field private final e:Lcc;

.field private f:[Ljava/lang/String;

.field private g:I

.field private h:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcb;Lcc;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "<<default account>>"

    iput-object v0, p0, Leb;->c:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "https://www.googleapis.com/auth/games"

    aput-object v2, v0, v1

    iput-object v0, p0, Leb;->f:[Ljava/lang/String;

    const/16 v0, 0x31

    iput v0, p0, Leb;->g:I

    iput-object p1, p0, Leb;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leb;->b:Ljava/lang/String;

    iput-object p2, p0, Leb;->d:Lcb;

    iput-object p3, p0, Leb;->e:Lcc;

    return-void
.end method


# virtual methods
.method public a()Ldz;
    .locals 10

    new-instance v0, Ldz;

    iget-object v1, p0, Leb;->a:Landroid/content/Context;

    iget-object v2, p0, Leb;->b:Ljava/lang/String;

    iget-object v3, p0, Leb;->c:Ljava/lang/String;

    iget-object v4, p0, Leb;->d:Lcb;

    iget-object v5, p0, Leb;->e:Lcc;

    iget-object v6, p0, Leb;->f:[Ljava/lang/String;

    iget v7, p0, Leb;->g:I

    iget-object v8, p0, Leb;->h:Landroid/view/View;

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Ldz;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcb;Lcc;[Ljava/lang/String;ILandroid/view/View;Lea;)V

    return-object v0
.end method

.method public a(I)Leb;
    .locals 0

    iput p1, p0, Leb;->g:I

    return-object p0
.end method

.method public a(Landroid/view/View;)Leb;
    .locals 1

    invoke-static {p1}, Lnx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Leb;->h:Landroid/view/View;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Leb;
    .locals 1

    invoke-static {p1}, Lnx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Leb;->c:Ljava/lang/String;

    return-object p0
.end method

.method public varargs a([Ljava/lang/String;)Leb;
    .locals 0

    iput-object p1, p0, Leb;->f:[Ljava/lang/String;

    return-object p0
.end method
