.class public Lamz;
.super Ljava/lang/Object;
.source "LifemodeInfo.java"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:Lamf;

.field private f:I

.field private g:J

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput v1, p0, Lamz;->a:I

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lamz;->b:Ljava/lang/String;

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lamz;->c:Ljava/lang/String;

    .line 46
    iput v1, p0, Lamz;->d:I

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lamz;->e:Lamf;

    .line 48
    iput v1, p0, Lamz;->f:I

    .line 49
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lamz;->g:J

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lamz;->h:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public constructor <init>(Lamx;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput v1, p0, Lamz;->a:I

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lamz;->b:Ljava/lang/String;

    .line 45
    const-string v0, ""

    iput-object v0, p0, Lamz;->c:Ljava/lang/String;

    .line 46
    iput v1, p0, Lamz;->d:I

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lamz;->e:Lamf;

    .line 48
    iput v1, p0, Lamz;->f:I

    .line 49
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lamz;->g:J

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lamz;->h:Ljava/lang/String;

    .line 61
    invoke-static {p1}, Lamx;->a(Lamx;)I

    move-result v0

    iput v0, p0, Lamz;->a:I

    .line 62
    invoke-static {p1}, Lamx;->b(Lamx;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lamz;->b:Ljava/lang/String;

    .line 63
    invoke-static {p1}, Lamx;->c(Lamx;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lamz;->c:Ljava/lang/String;

    .line 64
    invoke-static {p1}, Lamx;->d(Lamx;)I

    move-result v0

    iput v0, p0, Lamz;->d:I

    .line 65
    invoke-static {p1}, Lamx;->e(Lamx;)Lamf;

    move-result-object v0

    iput-object v0, p0, Lamz;->e:Lamf;

    .line 66
    invoke-static {p1}, Lamx;->f(Lamx;)I

    move-result v0

    iput v0, p0, Lamz;->f:I

    .line 67
    invoke-static {p1}, Lamx;->g(Lamx;)J

    move-result-wide v0

    iput-wide v0, p0, Lamz;->g:J

    .line 68
    invoke-static {p1}, Lamx;->h(Lamx;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lamz;->h:Ljava/lang/String;

    .line 69
    return-void
.end method

.method static synthetic a(Lamz;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lamz;->a:I

    return v0
.end method

.method static synthetic b(Lamz;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lamz;->b:Ljava/lang/String;

    return-object v0
.end method

.method private b()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method static synthetic c(Lamz;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lamz;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lamz;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lamz;->d:I

    return v0
.end method

.method static synthetic e(Lamz;)Lamf;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lamz;->e:Lamf;

    return-object v0
.end method

.method static synthetic f(Lamz;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lamz;->f:I

    return v0
.end method

.method static synthetic g(Lamz;)J
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lamz;->g:J

    return-wide v0
.end method

.method static synthetic h(Lamz;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lamz;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()Lamx;
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Lamz;->b()V

    .line 73
    new-instance v0, Lamx;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lamx;-><init>(Lamz;Lamy;)V

    return-object v0
.end method

.method public a(I)Lamz;
    .locals 0

    .prologue
    .line 80
    iput p1, p0, Lamz;->a:I

    .line 81
    return-object p0
.end method

.method public a(J)Lamz;
    .locals 1

    .prologue
    .line 113
    iput-wide p1, p0, Lamz;->g:J

    .line 114
    return-object p0
.end method

.method public a(Lamf;)Lamz;
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lamz;->e:Lamf;

    .line 104
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lamz;
    .locals 2

    .prologue
    .line 85
    if-eqz p1, :cond_0

    const-string v0, "@"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    const/4 v0, 0x0

    const-string v1, "@"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 88
    :cond_0
    iput-object p1, p0, Lamz;->b:Ljava/lang/String;

    .line 89
    return-object p0
.end method

.method public b(I)Lamz;
    .locals 0

    .prologue
    .line 98
    iput p1, p0, Lamz;->d:I

    .line 99
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lamz;
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lamz;->c:Ljava/lang/String;

    .line 94
    return-object p0
.end method

.method public c(I)Lamz;
    .locals 0

    .prologue
    .line 108
    iput p1, p0, Lamz;->f:I

    .line 109
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lamz;
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lamz;->h:Ljava/lang/String;

    .line 119
    return-object p0
.end method
