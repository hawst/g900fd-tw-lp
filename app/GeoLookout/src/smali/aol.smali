.class Laol;
.super Landroid/os/AsyncTask;
.source "SPP.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/content/Intent;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Laoi;


# direct methods
.method constructor <init>(Laoi;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Laol;->b:Laoi;

    iput-object p2, p0, Laol;->a:Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Landroid/content/Intent;)Ljava/lang/Void;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 187
    .line 188
    iget-object v2, p0, Laol;->b:Laoi;

    iput-boolean v1, v2, Laoi;->d:Z

    .line 189
    aget-object v2, p1, v1

    const-string v3, "com.sec.spp.Status"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 190
    aget-object v3, p1, v1

    const-string v4, "RegistrationID"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 191
    aget-object v4, p1, v1

    const-string v5, "Error"

    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 193
    if-nez v2, :cond_0

    iget-object v5, p0, Laol;->b:Laoi;

    iget-boolean v5, v5, Laoi;->c:Z

    if-eqz v5, :cond_0

    .line 194
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 195
    const-string v1, "SPP Push registration Success"

    invoke-static {v1}, Lalj;->c(Ljava/lang/String;)V

    .line 197
    iget-object v1, p0, Laol;->a:Landroid/content/Context;

    invoke-static {v1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v1

    iget-object v2, p0, Laol;->a:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lage;->m(Landroid/content/Context;)V

    .line 199
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Laol;->a:Landroid/content/Context;

    const-class v5, Lcom/sec/android/GeoLookout/DisasterService;

    invoke-direct {v1, v2, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 200
    const-string v2, "com.sec.android.GeoLookout.ACTION_REGISTER_DISASTER_SERVER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 201
    const-string v2, "regId"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 202
    iget-object v2, p0, Laol;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 205
    iget-object v1, p0, Laol;->a:Landroid/content/Context;

    invoke-static {v1, v3}, Laow;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 213
    :goto_0
    iget-object v1, p0, Laol;->b:Laoi;

    invoke-static {v1}, Laoi;->a(Laoi;)V

    .line 214
    iget-object v1, p0, Laol;->b:Laoi;

    new-instance v2, Laot;

    invoke-direct {v2, v0}, Laot;-><init>(Z)V

    iput-object v2, v1, Laoi;->b:Laot;

    .line 215
    iget-object v0, p0, Laol;->b:Laoi;

    iget-object v0, v0, Laoi;->b:Laot;

    invoke-virtual {v0}, Laot;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216
    iget-object v0, p0, Laol;->b:Laoi;

    iget-object v0, v0, Laoi;->b:Laot;

    invoke-virtual {v0, v3}, Laot;->a(Ljava/lang/String;)V

    .line 221
    :goto_1
    const/4 v0, 0x0

    return-object v0

    .line 208
    :cond_0
    const/4 v5, 0x2

    if-ne v2, v5, :cond_2

    iget-object v2, p0, Laol;->b:Laoi;

    iget-boolean v2, v2, Laoi;->c:Z

    if-nez v2, :cond_2

    .line 209
    const-string v1, "SPP Push unregistration Success"

    invoke-static {v1}, Lalj;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 218
    :cond_1
    iget-object v0, p0, Laol;->b:Laoi;

    iget-object v0, v0, Laoi;->b:Laot;

    invoke-virtual {v0, v4}, Laot;->a(I)V

    .line 219
    iget-object v0, p0, Laol;->b:Laoi;

    iget v1, v0, Laoi;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Laoi;->e:I

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Laol;->b:Laoi;

    iget-object v0, v0, Laoi;->a:Laou;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Laol;->b:Laoi;

    iget-object v0, v0, Laoi;->a:Laou;

    iget-object v1, p0, Laol;->b:Laoi;

    iget-object v1, v1, Laoi;->b:Laot;

    invoke-interface {v0, v1}, Laou;->a(Laot;)V

    .line 228
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 183
    check-cast p1, [Landroid/content/Intent;

    invoke-virtual {p0, p1}, Laol;->a([Landroid/content/Intent;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 183
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Laol;->a(Ljava/lang/Void;)V

    return-void
.end method
