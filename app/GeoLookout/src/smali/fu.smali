.class public final Lfu;
.super Ljava/lang/Object;


# instance fields
.field final a:Lfx;

.field b:Lfw;

.field c:Lfp;

.field d:Ljava/lang/String;

.field e:I

.field f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field g:Landroid/os/Bundle;

.field h:Z


# direct methods
.method private constructor <init>(Lfx;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lfu;->d:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lfu;->e:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfu;->f:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfu;->h:Z

    const-string v0, "Must provide a RoomUpdateListener"

    invoke-static {p1, v0}, Lnx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfx;

    iput-object v0, p0, Lfu;->a:Lfx;

    return-void
.end method

.method synthetic constructor <init>(Lfx;Lft;)V
    .locals 0

    invoke-direct {p0, p1}, Lfu;-><init>(Lfx;)V

    return-void
.end method


# virtual methods
.method public a()Lfs;
    .locals 2

    new-instance v0, Lfs;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lfs;-><init>(Lfu;Lft;)V

    return-object v0
.end method

.method public a(I)Lfu;
    .locals 0

    iput p1, p0, Lfu;->e:I

    return-object p0
.end method

.method public a(Landroid/os/Bundle;)Lfu;
    .locals 0

    iput-object p1, p0, Lfu;->g:Landroid/os/Bundle;

    return-object p0
.end method

.method public a(Lfp;)Lfu;
    .locals 0

    iput-object p1, p0, Lfu;->c:Lfp;

    return-object p0
.end method

.method public a(Lfw;)Lfu;
    .locals 0

    iput-object p1, p0, Lfu;->b:Lfw;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lfu;
    .locals 0

    invoke-static {p1}, Lnx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lfu;->d:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/util/ArrayList;)Lfu;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lfu;"
        }
    .end annotation

    invoke-static {p1}, Lnx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lfu;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public a(Z)Lfu;
    .locals 0

    iput-boolean p1, p0, Lfu;->h:Z

    return-object p0
.end method

.method public varargs a([Ljava/lang/String;)Lfu;
    .locals 2

    invoke-static {p1}, Lnx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lfu;->f:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method
