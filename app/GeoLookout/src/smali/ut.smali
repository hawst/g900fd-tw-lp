.class public final Lut;
.super Lgh;

# interfaces
.implements Lgq;
.implements Lhi;
.implements Ljk;
.implements Ljn;
.implements Ljx;
.implements Lus;


# instance fields
.field private final a:Lhq;

.field private final b:Luu;

.field private final c:Luv;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/x;Ljava/lang/String;Lhq;Lcom/google/android/gms/internal/co;)V
    .locals 2

    invoke-direct {p0}, Lgh;-><init>()V

    new-instance v0, Luu;

    invoke-direct {v0, p1, p2, p3, p5}, Luu;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/x;Ljava/lang/String;Lcom/google/android/gms/internal/co;)V

    iput-object v0, p0, Lut;->b:Luu;

    iput-object p4, p0, Lut;->a:Lhq;

    new-instance v0, Luv;

    invoke-direct {v0, p0}, Luv;-><init>(Lut;)V

    iput-object v0, p0, Lut;->c:Luv;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Use AdRequest.Builder.addTestDevice(\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lll;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\") to get test ads on this device."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Llm;->c(Ljava/lang/String;)V

    invoke-static {p1}, Llf;->b(Landroid/content/Context;)V

    return-void
.end method

.method private a(I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failed to load ad: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Llm;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->g:Lgd;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->g:Lgd;

    invoke-interface {v0, p1}, Lgd;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdFailedToLoad()."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    const/4 v1, -0x2

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lut;->b:Luu;

    iget-object v1, v1, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1, p1, v0}, Landroid/widget/ViewSwitcher;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a(Z)V
    .locals 6

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    if-nez v0, :cond_1

    const-string v0, "Ad state was null when trying to ping impression URLs."

    invoke-static {v0}, Llm;->e(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "Pinging Impression URLs."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->e:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->d:Landroid/content/Context;

    iget-object v1, p0, Lut;->b:Luu;

    iget-object v1, v1, Luu;->f:Lcom/google/android/gms/internal/co;

    iget-object v1, v1, Lcom/google/android/gms/internal/co;->c:Ljava/lang/String;

    iget-object v2, p0, Lut;->b:Luu;

    iget-object v2, v2, Luu;->i:Lky;

    iget-object v2, v2, Lky;->e:Ljava/util/List;

    invoke-static {v0, v1, v2}, Llf;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    :cond_2
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->n:Lhh;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->n:Lhh;

    iget-object v0, v0, Lhh;->d:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->d:Landroid/content/Context;

    iget-object v1, p0, Lut;->b:Luu;

    iget-object v1, v1, Luu;->f:Lcom/google/android/gms/internal/co;

    iget-object v1, v1, Lcom/google/android/gms/internal/co;->c:Ljava/lang/String;

    iget-object v2, p0, Lut;->b:Luu;

    iget-object v2, v2, Luu;->i:Lky;

    iget-object v3, p0, Lut;->b:Luu;

    iget-object v3, v3, Luu;->c:Ljava/lang/String;

    iget-object v4, p0, Lut;->b:Luu;

    iget-object v4, v4, Luu;->i:Lky;

    iget-object v4, v4, Lky;->n:Lhh;

    iget-object v5, v4, Lhh;->d:Ljava/util/List;

    move v4, p1

    invoke-static/range {v0 .. v5}, Lho;->a(Landroid/content/Context;Ljava/lang/String;Lky;Ljava/lang/String;ZLjava/util/List;)V

    :cond_3
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->k:Lhg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->k:Lhg;

    iget-object v0, v0, Lhg;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->d:Landroid/content/Context;

    iget-object v1, p0, Lut;->b:Luu;

    iget-object v1, v1, Luu;->f:Lcom/google/android/gms/internal/co;

    iget-object v1, v1, Lcom/google/android/gms/internal/co;->c:Ljava/lang/String;

    iget-object v2, p0, Lut;->b:Luu;

    iget-object v2, v2, Luu;->i:Lky;

    iget-object v3, p0, Lut;->b:Luu;

    iget-object v3, v3, Luu;->c:Ljava/lang/String;

    iget-object v4, p0, Lut;->b:Luu;

    iget-object v4, v4, Luu;->i:Lky;

    iget-object v4, v4, Lky;->k:Lhg;

    iget-object v5, v4, Lhg;->e:Ljava/util/List;

    move v4, p1

    invoke-static/range {v0 .. v5}, Lho;->a(Landroid/content/Context;Ljava/lang/String;Lky;Ljava/lang/String;ZLjava/util/List;)V

    goto/16 :goto_0
.end method

.method private b(Lky;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p1, Lky;->j:Z

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p1, Lky;->l:Lht;

    invoke-interface {v0}, Lht;->a()Ldp;

    move-result-object v0

    invoke-static {v0}, Lds;->a(Ldp;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v3, p0, Lut;->b:Luu;

    iget-object v3, v3, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v3}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v4, p0, Lut;->b:Luu;

    iget-object v4, v4, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v4, v3}, Landroid/widget/ViewSwitcher;->removeView(Landroid/view/View;)V

    :cond_0
    :try_start_1
    invoke-direct {p0, v0}, Lut;->a(Landroid/view/View;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getChildCount()I

    move-result v0

    if-le v0, v2, :cond_2

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->showNext()V

    :cond_2
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v0

    instance-of v3, v0, Lcom/google/android/gms/internal/cq;

    if-eqz v3, :cond_5

    check-cast v0, Lcom/google/android/gms/internal/cq;

    iget-object v3, p0, Lut;->b:Luu;

    iget-object v3, v3, Luu;->d:Landroid/content/Context;

    iget-object v4, p0, Lut;->b:Luu;

    iget-object v4, v4, Luu;->b:Lcom/google/android/gms/internal/x;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/internal/cq;->a(Landroid/content/Context;Lcom/google/android/gms/internal/x;)V

    :cond_3
    :goto_0
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->l:Lht;

    if-eqz v0, :cond_4

    :try_start_2
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->l:Lht;

    invoke-interface {v0}, Lht;->c()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_4
    :goto_1
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setVisibility(I)V

    move v0, v2

    :goto_2
    return v0

    :catch_0
    move-exception v0

    const-string v2, "Could not get View from mediation adapter."

    invoke-static {v2, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    goto :goto_2

    :catch_1
    move-exception v0

    const-string v2, "Could not add mediation view to view hierarchy."

    invoke-static {v2, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    goto :goto_2

    :cond_5
    if-eqz v0, :cond_3

    iget-object v3, p0, Lut;->b:Luu;

    iget-object v3, v3, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v3, v0}, Landroid/widget/ViewSwitcher;->removeView(Landroid/view/View;)V

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v0, "Could not destroy previous mediation adapter."

    invoke-static {v0}, Llm;->e(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private c(Lcom/google/android/gms/internal/v;)Lkj;
    .locals 10

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lut;->b:Luu;

    iget-object v3, v3, Luu;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    :try_start_0
    iget-object v3, p0, Lut;->b:Luu;

    iget-object v3, v3, Luu;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    iget-object v4, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    :goto_0
    iget-object v3, p0, Lut;->b:Luu;

    iget-object v3, v3, Luu;->b:Lcom/google/android/gms/internal/x;

    iget-boolean v3, v3, Lcom/google/android/gms/internal/x;->f:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lut;->b:Luu;

    iget-object v3, v3, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v3}, Landroid/widget/ViewSwitcher;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [I

    iget-object v3, p0, Lut;->b:Luu;

    iget-object v3, v3, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v3, v0}, Landroid/widget/ViewSwitcher;->getLocationOnScreen([I)V

    aget v3, v0, v2

    aget v4, v0, v1

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget-object v7, p0, Lut;->b:Luu;

    iget-object v7, v7, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v7}, Landroid/widget/ViewSwitcher;->getWidth()I

    move-result v7

    iget-object v8, p0, Lut;->b:Luu;

    iget-object v8, v8, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v8}, Landroid/widget/ViewSwitcher;->getHeight()I

    move-result v8

    iget-object v9, p0, Lut;->b:Luu;

    iget-object v9, v9, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v9}, Landroid/widget/ViewSwitcher;->isShown()Z

    move-result v9

    if-eqz v9, :cond_0

    add-int v9, v3, v7

    if-lez v9, :cond_0

    add-int v9, v4, v8

    if-lez v9, :cond_0

    iget v9, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-gt v3, v9, :cond_0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-gt v4, v0, :cond_0

    move v0, v1

    :goto_1
    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string v2, "x"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "y"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "width"

    invoke-virtual {v1, v2, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "height"

    invoke-virtual {v1, v2, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "visible"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_2
    new-instance v0, Lkj;

    iget-object v2, p0, Lut;->b:Luu;

    iget-object v3, v2, Luu;->b:Lcom/google/android/gms/internal/x;

    iget-object v2, p0, Lut;->b:Luu;

    iget-object v4, v2, Luu;->c:Ljava/lang/String;

    invoke-static {}, Lkz;->a()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lkz;->a:Ljava/lang/String;

    iget-object v2, p0, Lut;->b:Luu;

    iget-object v9, v2, Luu;->f:Lcom/google/android/gms/internal/co;

    move-object v2, p1

    invoke-direct/range {v0 .. v9}, Lkj;-><init>(Landroid/os/Bundle;Lcom/google/android/gms/internal/v;Lcom/google/android/gms/internal/x;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Landroid/content/pm/PackageInfo;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/co;)V

    return-object v0

    :catch_0
    move-exception v3

    move-object v6, v0

    goto/16 :goto_0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_2
.end method

.method private q()V
    .locals 2

    const-string v0, "Ad closing."

    invoke-static {v0}, Llm;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->g:Lgd;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->g:Lgd;

    invoke-interface {v0}, Lgd;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdClosed()."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private r()V
    .locals 2

    const-string v0, "Ad leaving application."

    invoke-static {v0}, Llm;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->g:Lgd;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->g:Lgd;

    invoke-interface {v0}, Lgd;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdLeftApplication()."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private s()V
    .locals 2

    const-string v0, "Ad opening."

    invoke-static {v0}, Llm;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->g:Lgd;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->g:Lgd;

    invoke-interface {v0}, Lgd;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdOpened()."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private t()V
    .locals 2

    const-string v0, "Ad finished loading."

    invoke-static {v0}, Llm;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->g:Lgd;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->g:Lgd;

    invoke-interface {v0}, Lgd;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call AdListener.onAdLoaded()."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private u()Z
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lut;->b:Luu;

    iget-object v2, v2, Luu;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lut;->b:Luu;

    iget-object v3, v3, Luu;->d:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.permission.INTERNET"

    invoke-static {v2, v3, v4}, Llf;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->b:Lcom/google/android/gms/internal/x;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/x;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->a:Landroid/widget/ViewSwitcher;

    iget-object v2, p0, Lut;->b:Luu;

    iget-object v2, v2, Luu;->b:Lcom/google/android/gms/internal/x;

    const-string v3, "Missing internet permission in AndroidManifest.xml."

    invoke-static {v0, v2, v3}, Lll;->a(Landroid/view/ViewGroup;Lcom/google/android/gms/internal/x;Ljava/lang/String;)V

    :cond_0
    move v0, v1

    :cond_1
    iget-object v2, p0, Lut;->b:Luu;

    iget-object v2, v2, Luu;->d:Landroid/content/Context;

    invoke-static {v2}, Llf;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->b:Lcom/google/android/gms/internal/x;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/x;->f:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->a:Landroid/widget/ViewSwitcher;

    iget-object v2, p0, Lut;->b:Luu;

    iget-object v2, v2, Luu;->b:Lcom/google/android/gms/internal/x;

    const-string v3, "Missing AdActivity with android:configChanges in AndroidManifest.xml."

    invoke-static {v0, v2, v3}, Lll;->a(Landroid/view/ViewGroup;Lcom/google/android/gms/internal/x;Ljava/lang/String;)V

    :cond_2
    move v0, v1

    :cond_3
    if-nez v0, :cond_4

    iget-object v2, p0, Lut;->b:Luu;

    iget-object v2, v2, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v2, v1}, Landroid/widget/ViewSwitcher;->setVisibility(I)V

    :cond_4
    return v0
.end method

.method private v()V
    .locals 6

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    if-nez v0, :cond_1

    const-string v0, "Ad state was null when trying to ping click URLs."

    invoke-static {v0}, Llm;->e(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "Pinging click URLs."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->c:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->d:Landroid/content/Context;

    iget-object v1, p0, Lut;->b:Luu;

    iget-object v1, v1, Luu;->f:Lcom/google/android/gms/internal/co;

    iget-object v1, v1, Lcom/google/android/gms/internal/co;->c:Ljava/lang/String;

    iget-object v2, p0, Lut;->b:Luu;

    iget-object v2, v2, Luu;->i:Lky;

    iget-object v2, v2, Lky;->c:Ljava/util/List;

    invoke-static {v0, v1, v2}, Llf;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    :cond_2
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->n:Lhh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->n:Lhh;

    iget-object v0, v0, Lhh;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->d:Landroid/content/Context;

    iget-object v1, p0, Lut;->b:Luu;

    iget-object v1, v1, Luu;->f:Lcom/google/android/gms/internal/co;

    iget-object v1, v1, Lcom/google/android/gms/internal/co;->c:Ljava/lang/String;

    iget-object v2, p0, Lut;->b:Luu;

    iget-object v2, v2, Luu;->i:Lky;

    iget-object v3, p0, Lut;->b:Luu;

    iget-object v3, v3, Luu;->c:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lut;->b:Luu;

    iget-object v5, v5, Luu;->i:Lky;

    iget-object v5, v5, Lky;->n:Lhh;

    iget-object v5, v5, Lhh;->c:Ljava/util/List;

    invoke-static/range {v0 .. v5}, Lho;->a(Landroid/content/Context;Ljava/lang/String;Lky;Ljava/lang/String;ZLjava/util/List;)V

    goto :goto_0
.end method

.method private w()V
    .locals 2

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->b:Lcom/google/android/gms/internal/cq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cq;->destroy()V

    iget-object v0, p0, Lut;->b:Luu;

    const/4 v1, 0x0

    iput-object v1, v0, Luu;->i:Lky;

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ldp;
    .locals 1

    const-string v0, "getAdFrame must be called on the main UI thread."

    invoke-static {v0}, Lnx;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-static {v0}, Lds;->a(Ljava/lang/Object;)Ldp;

    move-result-object v0

    return-object v0
.end method

.method public a(Lgd;)V
    .locals 1

    const-string v0, "setAdListener must be called on the main UI thread."

    invoke-static {v0}, Lnx;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iput-object p1, v0, Luu;->g:Lgd;

    return-void
.end method

.method public a(Lgm;)V
    .locals 1

    const-string v0, "setAppEventListener must be called on the main UI thread."

    invoke-static {v0}, Lnx;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iput-object p1, v0, Luu;->j:Lgm;

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->j:Lgm;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->j:Lgm;

    invoke-interface {v0, p1, p2}, Lgm;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not call the AppEventListener."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(Lky;)V
    .locals 7

    const-wide/16 v2, 0x0

    const/4 v6, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lut;->b:Luu;

    iput-object v6, v0, Luu;->h:Lla;

    iget v0, p1, Lky;->d:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Lky;->a:Lcom/google/android/gms/internal/v;

    iget-object v0, v0, Lcom/google/android/gms/internal/v;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lky;->a:Lcom/google/android/gms/internal/v;

    iget-object v0, v0, Lcom/google/android/gms/internal/v;->d:Landroid/os/Bundle;

    const-string v1, "_noRefresh"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :goto_1
    iget-object v1, p0, Lut;->b:Luu;

    iget-object v1, v1, Luu;->b:Lcom/google/android/gms/internal/x;

    iget-boolean v1, v1, Lcom/google/android/gms/internal/x;->f:Z

    if-eqz v1, :cond_4

    iget-object v0, p1, Lky;->b:Lcom/google/android/gms/internal/cq;

    invoke-static {v0}, Llf;->a(Landroid/webkit/WebView;)V

    :cond_1
    :goto_2
    iget v0, p1, Lky;->d:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    iget-object v0, p1, Lky;->n:Lhh;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lky;->n:Lhh;

    iget-object v0, v0, Lhh;->e:Ljava/util/List;

    if-eqz v0, :cond_2

    const-string v0, "Pinging no fill URLs."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->d:Landroid/content/Context;

    iget-object v1, p0, Lut;->b:Luu;

    iget-object v1, v1, Luu;->f:Lcom/google/android/gms/internal/co;

    iget-object v1, v1, Lcom/google/android/gms/internal/co;->c:Ljava/lang/String;

    iget-object v2, p0, Lut;->b:Luu;

    iget-object v3, v2, Luu;->c:Ljava/lang/String;

    iget-object v2, p1, Lky;->n:Lhh;

    iget-object v5, v2, Lhh;->e:Ljava/util/List;

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lho;->a(Landroid/content/Context;Ljava/lang/String;Lky;Ljava/lang/String;ZLjava/util/List;)V

    :cond_2
    iget v0, p1, Lky;->d:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_7

    iget v0, p1, Lky;->d:I

    invoke-direct {p0, v0}, Lut;->a(I)V

    goto :goto_0

    :cond_3
    move v0, v4

    goto :goto_1

    :cond_4
    if-nez v0, :cond_1

    iget-wide v0, p1, Lky;->h:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_5

    iget-object v0, p0, Lut;->c:Luv;

    iget-object v1, p1, Lky;->a:Lcom/google/android/gms/internal/v;

    iget-wide v2, p1, Lky;->h:J

    invoke-virtual {v0, v1, v2, v3}, Luv;->a(Lcom/google/android/gms/internal/v;J)V

    goto :goto_2

    :cond_5
    iget-object v0, p1, Lky;->n:Lhh;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lky;->n:Lhh;

    iget-wide v0, v0, Lhh;->g:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_6

    iget-object v0, p0, Lut;->c:Luv;

    iget-object v1, p1, Lky;->a:Lcom/google/android/gms/internal/v;

    iget-object v2, p1, Lky;->n:Lhh;

    iget-wide v2, v2, Lhh;->g:J

    invoke-virtual {v0, v1, v2, v3}, Luv;->a(Lcom/google/android/gms/internal/v;J)V

    goto :goto_2

    :cond_6
    iget-boolean v0, p1, Lky;->j:Z

    if-nez v0, :cond_1

    iget v0, p1, Lky;->d:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lut;->c:Luv;

    iget-object v1, p1, Lky;->a:Lcom/google/android/gms/internal/v;

    invoke-virtual {v0, v1}, Luv;->a(Lcom/google/android/gms/internal/v;)V

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->b:Lcom/google/android/gms/internal/x;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/x;->f:Z

    if-nez v0, :cond_8

    invoke-direct {p0, p1}, Lut;->b(Lky;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-direct {p0, v4}, Lut;->a(I)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->o:Lhj;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->o:Lhj;

    invoke-virtual {v0, v6}, Lhj;->a(Lhi;)V

    :cond_9
    iget-object v0, p1, Lky;->o:Lhj;

    if-eqz v0, :cond_a

    iget-object v0, p1, Lky;->o:Lhj;

    invoke-virtual {v0, p0}, Lhj;->a(Lhi;)V

    :cond_a
    iget-object v0, p0, Lut;->b:Luu;

    iput-object p1, v0, Luu;->i:Lky;

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->b:Lcom/google/android/gms/internal/x;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/x;->f:Z

    if-nez v0, :cond_b

    invoke-direct {p0, v4}, Lut;->a(Z)V

    :cond_b
    invoke-direct {p0}, Lut;->t()V

    goto/16 :goto_0
.end method

.method public a(Lcom/google/android/gms/internal/v;)Z
    .locals 11

    const/4 v9, 0x1

    const/4 v2, 0x0

    const-string v0, "loadAd must be called on the main UI thread."

    invoke-static {v0}, Lnx;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->h:Lla;

    if-eqz v0, :cond_1

    const-string v0, "An ad request is already in progress. Aborting."

    invoke-static {v0}, Llm;->e(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->b:Lcom/google/android/gms/internal/x;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/x;->f:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    if-eqz v0, :cond_2

    const-string v0, "An interstitial is already loading. Aborting."

    invoke-static {v0}, Llm;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lut;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Starting ad request."

    invoke-static {v0}, Llm;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->c:Luv;

    invoke-virtual {v0}, Luv;->a()V

    invoke-direct {p0, p1}, Lut;->c(Lcom/google/android/gms/internal/v;)Lkj;

    move-result-object v10

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->b:Lcom/google/android/gms/internal/x;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/x;->f:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->d:Landroid/content/Context;

    iget-object v1, p0, Lut;->b:Luu;

    iget-object v1, v1, Luu;->b:Lcom/google/android/gms/internal/x;

    iget-object v3, p0, Lut;->b:Luu;

    iget-object v4, v3, Luu;->e:Lue;

    iget-object v3, p0, Lut;->b:Luu;

    iget-object v5, v3, Luu;->f:Lcom/google/android/gms/internal/co;

    move v3, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/internal/cq;->a(Landroid/content/Context;Lcom/google/android/gms/internal/x;ZZLue;Lcom/google/android/gms/internal/co;)Lcom/google/android/gms/internal/cq;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/internal/cq;->e()Llo;

    move-result-object v0

    const/4 v2, 0x0

    move-object v1, p0

    move-object v3, p0

    move-object v4, p0

    move v5, v9

    invoke-virtual/range {v0 .. v5}, Llo;->a(Lus;Ljk;Lgq;Ljn;Z)V

    move-object v3, v6

    :goto_1
    iget-object v6, p0, Lut;->b:Luu;

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->d:Landroid/content/Context;

    iget-object v1, p0, Lut;->b:Luu;

    iget-object v2, v1, Luu;->e:Lue;

    iget-object v4, p0, Lut;->a:Lhq;

    move-object v1, v10

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Ljw;->a(Landroid/content/Context;Lkj;Lue;Lcom/google/android/gms/internal/cq;Lhq;Ljx;)Lla;

    move-result-object v0

    iput-object v0, v6, Luu;->h:Lla;

    move v2, v9

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/gms/internal/cq;

    if-eqz v1, :cond_4

    check-cast v0, Lcom/google/android/gms/internal/cq;

    iget-object v1, p0, Lut;->b:Luu;

    iget-object v1, v1, Luu;->d:Landroid/content/Context;

    iget-object v3, p0, Lut;->b:Luu;

    iget-object v3, v3, Luu;->b:Lcom/google/android/gms/internal/x;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/internal/cq;->a(Landroid/content/Context;Lcom/google/android/gms/internal/x;)V

    :goto_2
    invoke-virtual {v0}, Lcom/google/android/gms/internal/cq;->e()Llo;

    move-result-object v3

    move-object v4, p0

    move-object v5, p0

    move-object v6, p0

    move-object v7, p0

    move v8, v2

    invoke-virtual/range {v3 .. v8}, Llo;->a(Lus;Ljk;Lgq;Ljn;Z)V

    move-object v3, v0

    goto :goto_1

    :cond_4
    if-eqz v0, :cond_5

    iget-object v1, p0, Lut;->b:Luu;

    iget-object v1, v1, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1, v0}, Landroid/widget/ViewSwitcher;->removeView(Landroid/view/View;)V

    :cond_5
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->d:Landroid/content/Context;

    iget-object v1, p0, Lut;->b:Luu;

    iget-object v1, v1, Luu;->b:Lcom/google/android/gms/internal/x;

    iget-object v3, p0, Lut;->b:Luu;

    iget-object v4, v3, Luu;->e:Lue;

    iget-object v3, p0, Lut;->b:Luu;

    iget-object v5, v3, Luu;->f:Lcom/google/android/gms/internal/co;

    move v3, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/internal/cq;->a(Landroid/content/Context;Lcom/google/android/gms/internal/x;ZZLue;Lcom/google/android/gms/internal/co;)Lcom/google/android/gms/internal/cq;

    move-result-object v0

    invoke-direct {p0, v0}, Lut;->a(Landroid/view/View;)V

    goto :goto_2
.end method

.method public b()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "destroy must be called on the main UI thread."

    invoke-static {v0}, Lnx;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iput-object v1, v0, Luu;->g:Lgd;

    iget-object v0, p0, Lut;->b:Luu;

    iput-object v1, v0, Luu;->j:Lgm;

    iget-object v0, p0, Lut;->c:Luv;

    invoke-virtual {v0}, Luv;->a()V

    invoke-virtual {p0}, Lut;->g()V

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->a:Landroid/widget/ViewSwitcher;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->removeAllViews()V

    :cond_0
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->b:Lcom/google/android/gms/internal/cq;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->b:Lcom/google/android/gms/internal/cq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cq;->destroy()V

    :cond_1
    return-void
.end method

.method public b(Lcom/google/android/gms/internal/v;)V
    .locals 2

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->a:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Llf;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lut;->a(Lcom/google/android/gms/internal/v;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "Ad is not visible. Not refreshing ad."

    invoke-static {v0}, Llm;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->c:Luv;

    invoke-virtual {v0, p1}, Luv;->a(Lcom/google/android/gms/internal/v;)V

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    const-string v0, "isReady must be called on the main UI thread."

    invoke-static {v0}, Lnx;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->h:Lla;

    if-nez v0, :cond_0

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 1

    const-string v0, "pause must be called on the main UI thread."

    invoke-static {v0}, Lnx;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->b:Lcom/google/android/gms/internal/cq;

    invoke-static {v0}, Llf;->a(Landroid/webkit/WebView;)V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    const-string v0, "resume must be called on the main UI thread."

    invoke-static {v0}, Lnx;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->b:Lcom/google/android/gms/internal/cq;

    invoke-static {v0}, Llf;->b(Landroid/webkit/WebView;)V

    :cond_0
    return-void
.end method

.method public f()V
    .locals 7

    const-string v0, "showInterstitial must be called on the main UI thread."

    invoke-static {v0}, Lnx;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->b:Lcom/google/android/gms/internal/x;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/x;->f:Z

    if-nez v0, :cond_0

    const-string v0, "Cannot call showInterstitial on a banner ad."

    invoke-static {v0}, Llm;->e(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    if-nez v0, :cond_1

    const-string v0, "The interstitial has not loaded."

    invoke-static {v0}, Llm;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->b:Lcom/google/android/gms/internal/cq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cq;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "The interstitial is already showing."

    invoke-static {v0}, Llm;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->b:Lcom/google/android/gms/internal/cq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/cq;->a(Z)V

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-boolean v0, v0, Lky;->j:Z

    if-eqz v0, :cond_3

    :try_start_0
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->l:Lht;

    invoke-interface {v0}, Lht;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Could not show interstitial."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lut;->w()V

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/google/android/gms/internal/bh;

    iget-object v1, p0, Lut;->b:Luu;

    iget-object v1, v1, Luu;->i:Lky;

    iget-object v4, v1, Lky;->b:Lcom/google/android/gms/internal/cq;

    iget-object v1, p0, Lut;->b:Luu;

    iget-object v1, v1, Luu;->i:Lky;

    iget v5, v1, Lky;->g:I

    iget-object v1, p0, Lut;->b:Luu;

    iget-object v6, v1, Luu;->f:Lcom/google/android/gms/internal/co;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/bh;-><init>(Lus;Ljk;Ljn;Lcom/google/android/gms/internal/cq;ILcom/google/android/gms/internal/co;)V

    iget-object v1, p0, Lut;->b:Luu;

    iget-object v1, v1, Luu;->d:Landroid/content/Context;

    invoke-static {v1, v0}, Ljf;->a(Landroid/content/Context;Lcom/google/android/gms/internal/bh;)V

    goto :goto_0
.end method

.method public g()V
    .locals 2

    const-string v0, "stopLoading must be called on the main UI thread."

    invoke-static {v0}, Lnx;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    iget-object v0, v0, Lky;->b:Lcom/google/android/gms/internal/cq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cq;->stopLoading()V

    iget-object v0, p0, Lut;->b:Luu;

    const/4 v1, 0x0

    iput-object v1, v0, Luu;->i:Lky;

    :cond_0
    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->h:Lla;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->h:Lla;

    invoke-virtual {v0}, Lla;->f()V

    :cond_1
    return-void
.end method

.method public h()V
    .locals 0

    invoke-virtual {p0}, Lut;->p()V

    return-void
.end method

.method public i()V
    .locals 0

    invoke-virtual {p0}, Lut;->m()V

    return-void
.end method

.method public j()V
    .locals 0

    invoke-virtual {p0}, Lut;->o()V

    return-void
.end method

.method public k()V
    .locals 0

    invoke-virtual {p0}, Lut;->n()V

    return-void
.end method

.method public l()V
    .locals 2

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->i:Lky;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Mediation adapter "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lut;->b:Luu;

    iget-object v1, v1, Luu;->i:Lky;

    iget-object v1, v1, Lky;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " refreshed, but mediation adapters should never refresh."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Llm;->e(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lut;->a(Z)V

    invoke-direct {p0}, Lut;->t()V

    return-void
.end method

.method public m()V
    .locals 1

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->b:Lcom/google/android/gms/internal/x;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/x;->f:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lut;->w()V

    :cond_0
    invoke-direct {p0}, Lut;->q()V

    return-void
.end method

.method public n()V
    .locals 1

    iget-object v0, p0, Lut;->b:Luu;

    iget-object v0, v0, Luu;->b:Lcom/google/android/gms/internal/x;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/x;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lut;->a(Z)V

    :cond_0
    invoke-direct {p0}, Lut;->s()V

    return-void
.end method

.method public o()V
    .locals 0

    invoke-direct {p0}, Lut;->r()V

    return-void
.end method

.method public p()V
    .locals 0

    invoke-direct {p0}, Lut;->v()V

    return-void
.end method
