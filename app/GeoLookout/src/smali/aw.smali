.class public final Law;
.super Ljava/lang/Object;


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field public static final d:I = 0x3

.field public static final e:I = 0x0

.field public static final f:I = 0x1

.field public static final g:I = 0x2

.field public static final h:Ljava/lang/String;


# instance fields
.field private final i:Ljava/util/Date;

.field private final j:I

.field private final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lbe;",
            ">;",
            "Lbe;",
            ">;"
        }
    .end annotation
.end field

.field private final m:I

.field private final n:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "emulator"

    invoke-static {v0}, Lll;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Law;->h:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lay;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lay;->a(Lay;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Law;->i:Ljava/util/Date;

    invoke-static {p1}, Lay;->b(Lay;)I

    move-result v0

    iput v0, p0, Law;->j:I

    invoke-static {p1}, Lay;->c(Lay;)Ljava/util/HashSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Law;->k:Ljava/util/Set;

    invoke-static {p1}, Lay;->d(Lay;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Law;->l:Ljava/util/Map;

    invoke-static {p1}, Lay;->e(Lay;)I

    move-result v0

    iput v0, p0, Law;->m:I

    invoke-static {p1}, Lay;->f(Lay;)Ljava/util/HashSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Law;->n:Ljava/util/Set;

    return-void
.end method

.method synthetic constructor <init>(Lay;Lax;)V
    .locals 0

    invoke-direct {p0, p1}, Law;-><init>(Lay;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;)Lbe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lbe;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    iget-object v0, p0, Law;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbe;

    return-object v0
.end method

.method public a()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Law;->i:Ljava/util/Date;

    return-object v0
.end method

.method public a(Landroid/content/Context;)Z
    .locals 2

    iget-object v0, p0, Law;->n:Ljava/util/Set;

    invoke-static {p1}, Lll;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Law;->j:I

    return v0
.end method

.method public c()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Law;->k:Ljava/util/Set;

    return-object v0
.end method

.method public d()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lbe;",
            ">;",
            "Lbe;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Law;->l:Ljava/util/Map;

    return-object v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Law;->m:I

    return v0
.end method
