.class public final Lky;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lcom/google/android/gms/internal/v;

.field public final b:Lcom/google/android/gms/internal/cq;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:I

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:I

.field public final h:J

.field public final i:Ljava/lang/String;

.field public final j:Z

.field public final k:Lhg;

.field public final l:Lht;

.field public final m:Ljava/lang/String;

.field public final n:Lhh;

.field public final o:Lhj;

.field public final p:J

.field public final q:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/v;Lcom/google/android/gms/internal/cq;Ljava/util/List;ILjava/util/List;Ljava/util/List;IJLjava/lang/String;ZLhg;Lht;Ljava/lang/String;Lhh;Lhj;JJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/v;",
            "Lcom/google/android/gms/internal/cq;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IJ",
            "Ljava/lang/String;",
            "Z",
            "Lhg;",
            "Lht;",
            "Ljava/lang/String;",
            "Lhh;",
            "Lhj;",
            "JJ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lky;->a:Lcom/google/android/gms/internal/v;

    iput-object p2, p0, Lky;->b:Lcom/google/android/gms/internal/cq;

    if-eqz p3, :cond_0

    invoke-static {p3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_0
    iput-object v2, p0, Lky;->c:Ljava/util/List;

    iput p4, p0, Lky;->d:I

    if-eqz p5, :cond_1

    invoke-static {p5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_1
    iput-object v2, p0, Lky;->e:Ljava/util/List;

    if-eqz p6, :cond_2

    invoke-static {p6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_2
    iput-object v2, p0, Lky;->f:Ljava/util/List;

    iput p7, p0, Lky;->g:I

    iput-wide p8, p0, Lky;->h:J

    iput-object p10, p0, Lky;->i:Ljava/lang/String;

    iput-boolean p11, p0, Lky;->j:Z

    iput-object p12, p0, Lky;->k:Lhg;

    move-object/from16 v0, p13

    iput-object v0, p0, Lky;->l:Lht;

    move-object/from16 v0, p14

    iput-object v0, p0, Lky;->m:Ljava/lang/String;

    move-object/from16 v0, p15

    iput-object v0, p0, Lky;->n:Lhh;

    move-object/from16 v0, p16

    iput-object v0, p0, Lky;->o:Lhj;

    move-wide/from16 v0, p17

    iput-wide v0, p0, Lky;->p:J

    move-wide/from16 v0, p19

    iput-wide v0, p0, Lky;->q:J

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2
.end method
