.class public final Lafj;
.super Ljava/lang/Object;
.source "SlookCocktailDecorManager.java"


# static fields
.field private static final a:Ljava/lang/String; = "SlookCocktailDecorManager"

.field private static e:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/content/Context;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lafj;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Laew;

.field private c:Landroid/content/Context;

.field private d:Lcom/samsung/android/cocktailbar/CocktailBarDecorManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lafj;->e:Ljava/util/WeakHashMap;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Laew;

    invoke-direct {v0}, Laew;-><init>()V

    iput-object v0, p0, Lafj;->b:Laew;

    .line 63
    iget-object v0, p0, Lafj;->b:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iput-object p1, p0, Lafj;->c:Landroid/content/Context;

    .line 65
    iget-object v0, p0, Lafj;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/cocktailbar/CocktailBarDecorManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/cocktailbar/CocktailBarDecorManager;

    move-result-object v0

    iput-object v0, p0, Lafj;->d:Lcom/samsung/android/cocktailbar/CocktailBarDecorManager;

    .line 67
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;)Lafj;
    .locals 4

    .prologue
    .line 48
    sget-object v2, Lafj;->e:Ljava/util/WeakHashMap;

    monitor-enter v2

    .line 49
    :try_start_0
    sget-object v0, Lafj;->e:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 50
    const/4 v1, 0x0

    .line 51
    if-eqz v0, :cond_1

    .line 52
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafj;

    .line 54
    :goto_0
    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lafj;

    invoke-direct {v0, p0}, Lafj;-><init>(Landroid/content/Context;)V

    .line 56
    sget-object v1, Lafj;->e:Ljava/util/WeakHashMap;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, p0, v3}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    :cond_0
    monitor-exit v2

    return-object v0

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lafj;->b:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lafj;->d:Lcom/samsung/android/cocktailbar/CocktailBarDecorManager;

    invoke-virtual {v0}, Lcom/samsung/android/cocktailbar/CocktailBarDecorManager;->getDesiredWidth()I

    move-result v0

    .line 91
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lafj;->b:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lafj;->d:Lcom/samsung/android/cocktailbar/CocktailBarDecorManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/cocktailbar/CocktailBarDecorManager;->setResource(I)V

    .line 79
    :cond_0
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lafj;->b:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lafj;->d:Lcom/samsung/android/cocktailbar/CocktailBarDecorManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/cocktailbar/CocktailBarDecorManager;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 73
    :cond_0
    return-void
.end method

.method public a(Ljava/io/InputStream;)V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lafj;->b:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lafj;->d:Lcom/samsung/android/cocktailbar/CocktailBarDecorManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/cocktailbar/CocktailBarDecorManager;->setStream(Ljava/io/InputStream;)V

    .line 85
    :cond_0
    return-void
.end method

.method public b()I
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lafj;->b:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lafj;->d:Lcom/samsung/android/cocktailbar/CocktailBarDecorManager;

    invoke-virtual {v0}, Lcom/samsung/android/cocktailbar/CocktailBarDecorManager;->getDesiredHeight()I

    move-result v0

    .line 98
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method
