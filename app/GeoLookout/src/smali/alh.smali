.class Lalh;
.super Landroid/content/BroadcastReceiver;
.source "EmergencyDialog.java"


# instance fields
.field final synthetic a:Lalb;


# direct methods
.method constructor <init>(Lalb;)V
    .locals 0

    .prologue
    .line 284
    iput-object p1, p0, Lalh;->a:Lalb;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 286
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 287
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceive action="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->a(Ljava/lang/String;)V

    .line 288
    const/4 v1, -0x1

    .line 290
    const-string v2, "com.sec.android.GeoLookout.ACTION_UPDATE_CURRENT_LOCATION"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    const-string v0, "location"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    .line 292
    if-eqz v0, :cond_1

    .line 293
    iget-object v1, p0, Lalh;->a:Lalb;

    invoke-static {v1}, Lalb;->c(Lalb;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v1

    new-instance v2, Lamf;

    invoke-direct {v2, v0}, Lamf;-><init>(Landroid/location/Location;)V

    const/4 v0, 0x1

    invoke-static {p1, v1, v2, v0}, Lall;->a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;Lamf;Z)I

    move-result v0

    .line 296
    :goto_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 298
    iget-object v2, p0, Lalh;->a:Lalb;

    invoke-static {v2}, Lalb;->c(Lalb;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationType()I

    move-result v2

    if-nez v2, :cond_0

    .line 299
    invoke-static {p1, v0}, Lall;->e(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 305
    iget-object v0, p0, Lalh;->a:Lalb;

    invoke-static {v0}, Lalb;->d(Lalb;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 308
    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method
