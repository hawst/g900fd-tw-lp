.class public Lang;
.super Ljava/lang/Object;
.source "NetError.java"


# static fields
.field public static final a:I = 0x190

.field public static final b:I = 0x19a

.field public static final c:I = 0x1a4

.field public static final d:I = 0x1ae

.field public static final e:I = 0x1b8

.field public static final f:I = 0x1bf

.field public static final g:I = 0x1f4

.field public static final h:I = 0x1fe

.field public static final i:I = 0x208

.field public static final j:I = 0x226

.field public static final k:I = 0x258

.field public static final l:I = 0x3e8

.field public static final m:I = 0x3e9


# instance fields
.field private n:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/16 v0, 0x258

    iput v0, p0, Lang;->n:I

    .line 59
    iput p1, p0, Lang;->n:I

    .line 60
    return-void
.end method

.method public static a(ZZZ)Lang;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isConnected = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",isWiFiConnected = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",isRoaming = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 47
    if-nez p0, :cond_1

    .line 48
    if-eqz p2, :cond_0

    .line 51
    :cond_0
    new-instance v0, Lang;

    const/16 v1, 0x190

    invoke-direct {v0, v1}, Lang;-><init>(I)V

    .line 55
    :goto_0
    return-object v0

    .line 52
    :cond_1
    if-nez p1, :cond_2

    .line 55
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lang;->n:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 66
    iput p1, p0, Lang;->n:I

    .line 67
    return-void
.end method
