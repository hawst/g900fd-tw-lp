.class public final Lat;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static auth_client_needs_enabling_title:I

.field public static auth_client_needs_installation_title:I

.field public static auth_client_needs_update_title:I

.field public static auth_client_play_services_err_notification_msg:I

.field public static auth_client_requested_by_msg:I

.field public static auth_client_using_bad_version_title:I

.field public static common_google_play_services_enable_button:I

.field public static common_google_play_services_enable_text:I

.field public static common_google_play_services_enable_title:I

.field public static common_google_play_services_install_button:I

.field public static common_google_play_services_install_text_phone:I

.field public static common_google_play_services_install_text_tablet:I

.field public static common_google_play_services_install_title:I

.field public static common_google_play_services_invalid_account_text:I

.field public static common_google_play_services_invalid_account_title:I

.field public static common_google_play_services_network_error_text:I

.field public static common_google_play_services_network_error_title:I

.field public static common_google_play_services_unknown_issue:I

.field public static common_google_play_services_unsupported_date_text:I

.field public static common_google_play_services_unsupported_text:I

.field public static common_google_play_services_unsupported_title:I

.field public static common_google_play_services_update_button:I

.field public static common_google_play_services_update_text:I

.field public static common_google_play_services_update_title:I

.field public static common_signin_button_text:I

.field public static common_signin_button_text_long:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 213
    const v0, 0x7f050015

    sput v0, Lat;->auth_client_needs_enabling_title:I

    .line 217
    const v0, 0x7f050016

    sput v0, Lat;->auth_client_needs_installation_title:I

    .line 221
    const v0, 0x7f050017

    sput v0, Lat;->auth_client_needs_update_title:I

    .line 224
    const v0, 0x7f050018

    sput v0, Lat;->auth_client_play_services_err_notification_msg:I

    .line 227
    const v0, 0x7f050019

    sput v0, Lat;->auth_client_requested_by_msg:I

    .line 232
    const v0, 0x7f050014

    sput v0, Lat;->auth_client_using_bad_version_title:I

    .line 237
    const v0, 0x7f050006

    sput v0, Lat;->common_google_play_services_enable_button:I

    .line 241
    const v0, 0x7f050005

    sput v0, Lat;->common_google_play_services_enable_text:I

    .line 245
    const v0, 0x7f050004

    sput v0, Lat;->common_google_play_services_enable_title:I

    .line 248
    const v0, 0x7f050003

    sput v0, Lat;->common_google_play_services_install_button:I

    .line 252
    const v0, 0x7f050001

    sput v0, Lat;->common_google_play_services_install_text_phone:I

    .line 256
    const v0, 0x7f050002

    sput v0, Lat;->common_google_play_services_install_text_tablet:I

    .line 260
    const/high16 v0, 0x7f050000

    sput v0, Lat;->common_google_play_services_install_title:I

    .line 263
    const v0, 0x7f05000c

    sput v0, Lat;->common_google_play_services_invalid_account_text:I

    .line 266
    const v0, 0x7f05000b

    sput v0, Lat;->common_google_play_services_invalid_account_title:I

    .line 269
    const v0, 0x7f05000a

    sput v0, Lat;->common_google_play_services_network_error_text:I

    .line 272
    const v0, 0x7f050009

    sput v0, Lat;->common_google_play_services_network_error_title:I

    .line 276
    const v0, 0x7f05000d

    sput v0, Lat;->common_google_play_services_unknown_issue:I

    .line 280
    const v0, 0x7f050010

    sput v0, Lat;->common_google_play_services_unsupported_date_text:I

    .line 283
    const v0, 0x7f05000f

    sput v0, Lat;->common_google_play_services_unsupported_text:I

    .line 286
    const v0, 0x7f05000e

    sput v0, Lat;->common_google_play_services_unsupported_title:I

    .line 289
    const v0, 0x7f050011

    sput v0, Lat;->common_google_play_services_update_button:I

    .line 293
    const v0, 0x7f050008

    sput v0, Lat;->common_google_play_services_update_text:I

    .line 297
    const v0, 0x7f050007

    sput v0, Lat;->common_google_play_services_update_title:I

    .line 300
    const v0, 0x7f050012

    sput v0, Lat;->common_signin_button_text:I

    .line 303
    const v0, 0x7f050013

    sput v0, Lat;->common_signin_button_text_long:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
