.class public Lajg;
.super Landroid/content/BroadcastReceiver;
.source "DisasterLifeMainActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lajg;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 202
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 203
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mRefreshResultReceiver : onReceive action="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->a(Ljava/lang/String;)V

    .line 205
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA_RESULT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 206
    iget-object v0, p0, Lajg;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V

    .line 208
    iget-object v0, p0, Lajg;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->b(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lamu;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lajg;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->c(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V

    .line 210
    iget-object v0, p0, Lajg;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)Lapx;

    move-result-object v0

    invoke-virtual {v0}, Lapx;->notifyDataSetChanged()V

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA_CANCEL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lajg;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V

    goto :goto_0
.end method
