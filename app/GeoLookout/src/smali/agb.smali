.class public Lagb;
.super Ljava/lang/Object;
.source "DisasterAlertService.java"

# interfaces
.implements Landroid/hardware/scontext/SContextListener;


# instance fields
.field final synthetic a:Landroid/hardware/scontext/SContextManager;

.field final synthetic b:Lcom/sec/android/GeoLookout/DisasterAlertService;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/DisasterAlertService;Landroid/hardware/scontext/SContextManager;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lagb;->b:Lcom/sec/android/GeoLookout/DisasterAlertService;

    iput-object p2, p0, Lagb;->a:Landroid/hardware/scontext/SContextManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSContextChanged(Landroid/hardware/scontext/SContextEvent;)V
    .locals 4

    .prologue
    .line 188
    iget-object v0, p1, Landroid/hardware/scontext/SContextEvent;->scontext:Landroid/hardware/scontext/SContext;

    .line 190
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mActivityTrackerListener: onSContextChanged(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 192
    invoke-virtual {v0}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v0

    const/16 v1, 0x19

    if-ne v0, v1, :cond_0

    .line 193
    invoke-virtual {p1}, Landroid/hardware/scontext/SContextEvent;->getActivityTrackerContext()Landroid/hardware/scontext/SContextActivityTracker;

    move-result-object v0

    .line 194
    iget-object v1, p0, Lagb;->b:Lcom/sec/android/GeoLookout/DisasterAlertService;

    const-wide/32 v2, 0x36ee80

    invoke-static {v1, v2, v3}, Lcom/sec/android/GeoLookout/DisasterAlertService;->b(Lcom/sec/android/GeoLookout/DisasterAlertService;J)J

    .line 196
    invoke-virtual {v0}, Landroid/hardware/scontext/SContextActivityTracker;->getStatus()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 204
    :goto_0
    iget-object v0, p0, Lagb;->b:Lcom/sec/android/GeoLookout/DisasterAlertService;

    iget-object v1, p0, Lagb;->b:Lcom/sec/android/GeoLookout/DisasterAlertService;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/DisasterAlertService;->c(Lcom/sec/android/GeoLookout/DisasterAlertService;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/sec/android/GeoLookout/DisasterAlertService;->a(Lcom/sec/android/GeoLookout/DisasterAlertService;J)V

    .line 205
    iget-object v0, p0, Lagb;->a:Landroid/hardware/scontext/SContextManager;

    invoke-virtual {v0, p0}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;)V

    .line 207
    iget-object v0, p0, Lagb;->b:Lcom/sec/android/GeoLookout/DisasterAlertService;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/DisasterAlertService;->b(Lcom/sec/android/GeoLookout/DisasterAlertService;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lagb;->b:Lcom/sec/android/GeoLookout/DisasterAlertService;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/DisasterAlertService;->b(Lcom/sec/android/GeoLookout/DisasterAlertService;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 209
    iget-object v0, p0, Lagb;->b:Lcom/sec/android/GeoLookout/DisasterAlertService;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/DisasterAlertService;->b(Lcom/sec/android/GeoLookout/DisasterAlertService;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 210
    iget-object v0, p0, Lagb;->b:Lcom/sec/android/GeoLookout/DisasterAlertService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/DisasterAlertService;->a(Lcom/sec/android/GeoLookout/DisasterAlertService;Ljava/util/Timer;)Ljava/util/Timer;

    .line 213
    :cond_0
    return-void

    .line 199
    :pswitch_0
    const-string v0, "mActivityTrackerListener: onSContextChanged(): driving"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 200
    iget-object v0, p0, Lagb;->b:Lcom/sec/android/GeoLookout/DisasterAlertService;

    const-wide/32 v2, 0x124f80

    invoke-static {v0, v2, v3}, Lcom/sec/android/GeoLookout/DisasterAlertService;->b(Lcom/sec/android/GeoLookout/DisasterAlertService;J)J

    goto :goto_0

    .line 196
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
