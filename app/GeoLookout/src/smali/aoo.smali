.class public Laoo;
.super Ljava/lang/Object;
.source "SPPError.java"


# static fields
.field public static final A:I = 0xfaa

.field public static final B:I = 0xfab

.field public static final C:I = 0xfac

.field public static final D:[I

.field public static final E:[Ljava/lang/String;

.field public static final a:I = 0x3e8

.field public static final b:I = -0x1

.field public static final c:I = -0x2

.field public static final d:I = -0x64

.field public static final e:I = -0x66

.field public static final f:I = -0x67

.field public static final g:I = -0x68

.field public static final h:I = -0x69

.field public static final i:I = 0x7d0

.field public static final j:I = 0x7d1

.field public static final k:I = 0x7d2

.field public static final l:I = 0x7d3

.field public static final m:I = 0xbb8

.field public static final n:I = 0xbb9

.field public static final o:I = 0xbba

.field public static final p:I = 0xbbb

.field public static final q:I = 0xfa0

.field public static final r:I = 0xfa1

.field public static final s:I = 0xfa2

.field public static final t:I = 0xfa3

.field public static final u:I = 0xfa4

.field public static final v:I = 0xfa5

.field public static final w:I = 0xfa6

.field public static final x:I = 0xfa7

.field public static final y:I = 0xfa8

.field public static final z:I = 0xfa9


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v1, 0x1e

    .line 41
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Laoo;->D:[I

    .line 50
    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "TIMEOUT"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "NETWORK_NOT_AVAILABLE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "PROVISIONING_DATA_EXISTS"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "INITIALIZATION_ALREADY_COMPLETED"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "PROVISIONING_FAIL"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "PROVISIONING_FAIL"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "INITIALIZATION_FAIL"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "APPLICATION_ALREADY_DEREGISTRATION"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "UNKNOWN_MESSAGE_TYPE"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "UNEXPECTED_MESSAGE"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "INTERNAL_SERVER_ERROR"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "INTERRUPTED"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "BAD_REQUEST_FOR_PROVISION"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "FAIL_TO_AUTHENTICATE"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "INVALID_DEVICE_TOKEN_TO_REPROVISION"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "PROVISION_EXCEPTION"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "CONNECTION_MAX_EXCEEDED"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "INVALID_STATE"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "INVALID_DEVICE_TOKEN"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "INVALID_APP_ID"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "INVALID_REG_ID"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "RESET_BY_NEW_INITIALIZATION"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "REPROVISIONING_REQUIRED"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "REGISTRATION_FAILED"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "DEREGISTRATION_FAILED"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "WRONG_DEVICE_TOKEN"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "WRONG_DEVICE_TOKEN"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "WRONG_APP_ID"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "WRONG_REG_ID"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "UNSUPPORTED_PING_SPECIFICATION"

    aput-object v2, v0, v1

    sput-object v0, Laoo;->E:[Ljava/lang/String;

    return-void

    .line 41
    :array_0
    .array-data 4
        -0x1
        -0x2
        -0x64
        -0x66
        -0x67
        -0x67
        -0x68
        -0x69
        0x7d0
        0x7d1
        0x7d2
        0x7d3
        0xbb8
        0xbb9
        0xbba
        0xbbb
        0xfa0
        0xfa1
        0xfa2
        0xfa3
        0xfa4
        0xfa5
        0xfa6
        0xfa7
        0xfa8
        0xfa9
        0xfa9
        0xfaa
        0xfab
        0xfac
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(I)V
    .locals 3

    .prologue
    .line 60
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Laoo;->D:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 61
    sget-object v1, Laoo;->D:[I

    aget v1, v1, v0

    if-ne v1, p0, :cond_0

    .line 62
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SPP Register error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Laoo;->E:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->c(Ljava/lang/String;)V

    .line 60
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63
    :cond_1
    return-void
.end method
