.class public Lake;
.super Ljava/lang/Object;
.source "DisasterReportActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;

.field private b:Lcom/sec/android/GeoLookout/db/DisasterInfo;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 801
    iput-object p1, p0, Lake;->a:Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 802
    iput-object p2, p0, Lake;->b:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    .line 803
    iget-object v0, p0, Lake;->b:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-static {v0}, Lall;->b(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lake;->c:Ljava/lang/String;

    .line 805
    invoke-static {p1}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->d(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lake;->b:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getStartTime()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lall;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 807
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->d(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0044

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lake;->e:Ljava/lang/String;

    .line 810
    invoke-static {p1}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->d(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lake;->b:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getEndTime()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lall;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 811
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->d(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0040

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lake;->d:Ljava/lang/String;

    .line 813
    return-void
.end method

.method public static synthetic a(Lake;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 1

    .prologue
    .line 800
    iget-object v0, p0, Lake;->b:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    return-object v0
.end method

.method static synthetic b(Lake;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 800
    iget-object v0, p0, Lake;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lake;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 800
    iget-object v0, p0, Lake;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lake;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 800
    iget-object v0, p0, Lake;->d:Ljava/lang/String;

    return-object v0
.end method
