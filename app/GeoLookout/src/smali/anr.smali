.class Lanr;
.super Lcom/loopj/android/http/AsyncHttpResponseHandler;
.source "DisasterServerInterface.java"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lanh;


# direct methods
.method constructor <init>(Lanh;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lanr;->c:Lanh;

    iput-object p2, p0, Lanr;->a:Ljava/lang/String;

    iput-object p3, p0, Lanr;->b:Ljava/lang/String;

    invoke-direct {p0}, Lcom/loopj/android/http/AsyncHttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 343
    invoke-super {p0, p1, p2, p3, p4}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onFailure(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V

    .line 344
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "registerNearbyTrackingInfoToDisasterServer: onFailure(statusCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 345
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lanr;->c:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/GeoLookout/DisasterService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 346
    const-string v1, "com.sec.android.GeoLookout.ACTION_HANDLE_PUSH_MESSAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 347
    const-string v1, "msg"

    iget-object v2, p0, Lanr;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 348
    const-string v1, "istracked"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 349
    iget-object v1, p0, Lanr;->c:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 350
    return-void
.end method

.method public onFinish()V
    .locals 1

    .prologue
    .line 354
    const-string v0, "registerNearbyTrackingInfoToDisasterServer: onFinish()  "

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 355
    return-void
.end method

.method public onRetry()V
    .locals 1

    .prologue
    .line 359
    invoke-super {p0}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onRetry()V

    .line 360
    const-string v0, "registerNearbyTrackingInfoToDisasterServer: onRetry"

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 361
    return-void
.end method

.method public onSuccess(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 327
    invoke-super {p0, p1}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onSuccess(Ljava/lang/String;)V

    .line 328
    const-string v0, "registerNearbyTrackingInfoToDisasterServer: onSuccess "

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 329
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onSuccess :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 331
    iget-object v0, p0, Lanr;->c:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lanr;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/db/DisasterParser;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v2

    .line 333
    if-eqz v2, :cond_0

    .line 334
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    iget-object v1, p0, Lanr;->c:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lanr;->a:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v5

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lalu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 338
    :cond_0
    iget-object v0, p0, Lanr;->c:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lanr;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lall;->g(Landroid/content/Context;Ljava/lang/String;)V

    .line 339
    return-void
.end method
