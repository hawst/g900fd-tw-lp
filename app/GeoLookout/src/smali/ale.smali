.class Lale;
.super Ljava/lang/Object;
.source "EmergencyDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/db/DisasterInfo;

.field final synthetic b:Lalb;


# direct methods
.method constructor <init>(Lalb;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lale;->b:Lalb;

    iput-object p2, p0, Lale;->a:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 98
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lale;->b:Lalb;

    invoke-static {v1}, Lalb;->a(Lalb;)Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 99
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 100
    const-string v1, "DID"

    iget-object v2, p0, Lale;->a:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    iget-object v1, p0, Lale;->b:Lalb;

    invoke-static {v1}, Lalb;->a(Lalb;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 102
    return-void
.end method
