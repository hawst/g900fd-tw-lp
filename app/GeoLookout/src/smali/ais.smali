.class public Lais;
.super Landroid/os/Handler;
.source "DisasterDialogRegisterActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 1

    .prologue
    .line 164
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 165
    iput p1, v0, Landroid/os/Message;->what:I

    .line 166
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 167
    invoke-virtual {p0, v0}, Lais;->sendMessage(Landroid/os/Message;)Z

    .line 168
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const v4, 0x7f0a0017

    const v2, 0x7f0a0013

    const v3, 0x7f0a000f

    const v1, 0x7f0a000e

    const v0, 0x7f0a000b

    .line 55
    iget-object v5, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    invoke-static {v5}, Lall;->a(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 56
    iget-object v0, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->finish()V

    .line 161
    :goto_0
    return-void

    .line 61
    :cond_0
    iget v5, p1, Landroid/os/Message;->what:I

    sparse-switch v5, :sswitch_data_0

    goto :goto_0

    .line 63
    :sswitch_0
    const-string v0, "UI State: registering device progress dialog"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    const v1, 0x7f0a0016

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;I)V

    goto :goto_0

    .line 67
    :sswitch_1
    const-string v0, "UI State: deregistering device progress dialog"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    const v1, 0x7f0a0008

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;I)V

    goto :goto_0

    .line 71
    :sswitch_2
    const-string v5, "UI State: registering fail alert dialog"

    invoke-static {v5}, Lalj;->c(Ljava/lang/String;)V

    .line 72
    iget-object v5, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    invoke-static {v5}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;)V

    .line 73
    const v5, 0x7f0a0014

    .line 74
    iget v6, p1, Landroid/os/Message;->arg1:I

    sparse-switch v6, :sswitch_data_1

    move v0, v5

    .line 120
    :goto_1
    iget-object v1, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    iget-object v2, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->a(Landroid/content/Context;I)V

    .line 121
    iget-object v0, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->finish()V

    goto :goto_0

    :sswitch_3
    move v0, v2

    .line 78
    goto :goto_1

    :sswitch_4
    move v0, v3

    .line 81
    goto :goto_1

    :sswitch_5
    move v0, v4

    .line 84
    goto :goto_1

    .line 86
    :sswitch_6
    const v0, 0x7f0a0019

    .line 87
    goto :goto_1

    .line 90
    :sswitch_7
    sget-boolean v2, Laky;->d:Z

    if-eqz v2, :cond_1

    sget-boolean v2, Laky;->e:Z

    if-eqz v2, :cond_1

    .line 95
    iget-object v2, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    invoke-static {v2, v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;II)V

    goto :goto_0

    .line 99
    :cond_1
    const v0, 0x7f0a048c

    .line 101
    goto :goto_1

    .line 103
    :sswitch_8
    const v0, 0x7f0a0037

    .line 104
    iget-object v1, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    const/4 v2, -0x1

    invoke-static {v1, v2, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;II)V

    goto :goto_0

    .line 109
    :sswitch_9
    sget-boolean v2, Laky;->d:Z

    if-eqz v2, :cond_2

    sget-boolean v2, Laky;->e:Z

    if-eqz v2, :cond_2

    .line 117
    :goto_2
    iget-object v2, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    invoke-static {v2, v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;II)V

    goto :goto_0

    .line 113
    :cond_2
    const v1, 0x7f0a000c

    .line 114
    const v0, 0x7f0a000a

    goto :goto_2

    .line 124
    :sswitch_a
    const-string v0, "UI State: deregistering fail alert dialog"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;)V

    .line 126
    const v0, 0x7f0a0005

    .line 127
    iget v1, p1, Landroid/os/Message;->arg1:I

    sparse-switch v1, :sswitch_data_2

    move v2, v0

    .line 142
    :goto_3
    :sswitch_b
    iget-object v0, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    iget-object v1, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->a(Landroid/content/Context;I)V

    .line 143
    iget-object v0, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->finish()V

    goto/16 :goto_0

    :sswitch_c
    move v2, v3

    .line 134
    goto :goto_3

    :sswitch_d
    move v2, v4

    .line 137
    goto :goto_3

    .line 139
    :sswitch_e
    const v2, 0x7f0a0019

    goto :goto_3

    .line 146
    :sswitch_f
    const-string v0, "UI State: registering success toast"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.sec.android.GeoLookout.SETTING_CHANGE_FINISH"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 149
    iget-object v0, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    iget-object v1, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    const v2, 0x7f0a0015

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->a(Landroid/content/Context;I)V

    .line 150
    iget-object v0, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->finish()V

    goto/16 :goto_0

    .line 153
    :sswitch_10
    const-string v0, "UI State: deregistering success toast"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.sec.android.GeoLookout.SETTING_CHANGE_FINISH"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 156
    iget-object v0, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    iget-object v1, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    const v2, 0x7f0a0007

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->a(Landroid/content/Context;I)V

    .line 157
    iget-object v0, p0, Lais;->a:Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->finish()V

    goto/16 :goto_0

    .line 61
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0xa -> :sswitch_2
        0x14 -> :sswitch_f
        0x1e -> :sswitch_a
        0x28 -> :sswitch_10
    .end sparse-switch

    .line 74
    :sswitch_data_1
    .sparse-switch
        0x190 -> :sswitch_4
        0x19a -> :sswitch_6
        0x1a4 -> :sswitch_5
        0x1ae -> :sswitch_3
        0x1bf -> :sswitch_8
        0x1f4 -> :sswitch_3
        0x3e8 -> :sswitch_7
        0x3e9 -> :sswitch_9
    .end sparse-switch

    .line 127
    :sswitch_data_2
    .sparse-switch
        0x190 -> :sswitch_c
        0x19a -> :sswitch_e
        0x1a4 -> :sswitch_d
        0x1ae -> :sswitch_b
        0x1f4 -> :sswitch_b
    .end sparse-switch
.end method
