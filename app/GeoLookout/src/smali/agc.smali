.class public Lagc;
.super Ljava/util/TimerTask;
.source "DisasterAlertService.java"


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/DisasterAlertService;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/DisasterAlertService;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lagc;->a:Lcom/sec/android/GeoLookout/DisasterAlertService;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 151
    const-string v0, "CancelTimerTask timeout"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lagc;->a:Lcom/sec/android/GeoLookout/DisasterAlertService;

    const-string v1, "scontext"

    invoke-virtual {v0, v1}, Lcom/sec/android/GeoLookout/DisasterAlertService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/scontext/SContextManager;

    .line 154
    iget-object v1, p0, Lagc;->a:Lcom/sec/android/GeoLookout/DisasterAlertService;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/DisasterAlertService;->a(Lcom/sec/android/GeoLookout/DisasterAlertService;)Landroid/hardware/scontext/SContextListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;)V

    .line 157
    iget-object v0, p0, Lagc;->a:Lcom/sec/android/GeoLookout/DisasterAlertService;

    const-wide/32 v2, 0x36ee80

    invoke-static {v0, v2, v3}, Lcom/sec/android/GeoLookout/DisasterAlertService;->a(Lcom/sec/android/GeoLookout/DisasterAlertService;J)V

    .line 158
    iget-object v0, p0, Lagc;->a:Lcom/sec/android/GeoLookout/DisasterAlertService;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/DisasterAlertService;->b(Lcom/sec/android/GeoLookout/DisasterAlertService;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lagc;->a:Lcom/sec/android/GeoLookout/DisasterAlertService;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/DisasterAlertService;->b(Lcom/sec/android/GeoLookout/DisasterAlertService;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 160
    iget-object v0, p0, Lagc;->a:Lcom/sec/android/GeoLookout/DisasterAlertService;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/DisasterAlertService;->b(Lcom/sec/android/GeoLookout/DisasterAlertService;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 162
    :cond_0
    return-void
.end method
