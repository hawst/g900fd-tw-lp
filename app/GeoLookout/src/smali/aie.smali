.class public Laie;
.super Ljava/lang/Object;
.source "DisasterDashboardActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/GeoLookout/db/DisasterInfo;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 3130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lagz;)V
    .locals 0

    .prologue
    .line 3130
    invoke-direct {p0}, Laie;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/sec/android/GeoLookout/db/DisasterInfo;Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 4

    .prologue
    .line 3133
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3134
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 3142
    :goto_0
    return v0

    .line 3136
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDataType()S

    move-result v0

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDataType()S

    move-result v1

    if-eq v0, v1, :cond_1

    .line 3137
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDataType()S

    move-result v0

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDataType()S

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0

    .line 3139
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDataType()S

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 3140
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailForecastHour()I

    move-result v0

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailForecastHour()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0

    .line 3142
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getStartTime()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getStartTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 3130
    check-cast p1, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    check-cast p2, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {p0, p1, p2}, Laie;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v0

    return v0
.end method
