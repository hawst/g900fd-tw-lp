.class Lakj;
.super Landroid/database/ContentObserver;
.source "DisasterSettingsFragment.java"


# instance fields
.field final synthetic a:Lakf;


# direct methods
.method constructor <init>(Lakf;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lakj;->a:Lakf;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 133
    iget-object v0, p0, Lakj;->a:Lakf;

    invoke-virtual {v0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "safetycare_geolookout_registering"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 135
    :goto_0
    iget-object v3, p0, Lakj;->a:Lakf;

    invoke-static {v3}, Lakf;->a(Lakf;)Lakr;

    move-result-object v3

    invoke-virtual {v3, v0}, Lakr;->setEnabled(Z)V

    .line 137
    if-eqz v0, :cond_2

    .line 138
    iget-object v0, p0, Lakj;->a:Lakf;

    invoke-virtual {v0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "safetycare_earthquake"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    move v2, v1

    .line 140
    :cond_0
    iget-object v0, p0, Lakj;->a:Lakf;

    invoke-static {v0}, Lakf;->a(Lakf;)Lakr;

    move-result-object v0

    invoke-virtual {v0, v2}, Lakr;->setChecked(Z)V

    .line 141
    iget-object v0, p0, Lakj;->a:Lakf;

    invoke-static {v0}, Lakf;->b(Lakf;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 145
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 133
    goto :goto_0

    .line 143
    :cond_2
    iget-object v0, p0, Lakj;->a:Lakf;

    invoke-static {v0}, Lakf;->b(Lakf;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_1
.end method
