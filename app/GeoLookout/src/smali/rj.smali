.class public Lrj;
.super Lmz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmz",
        "<",
        "Lrd;",
        ">;"
    }
.end annotation


# instance fields
.field private final f:Lrs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrs",
            "<",
            "Lrd;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lrg;

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcb;Lcc;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lmz;-><init>(Landroid/content/Context;Lcb;Lcc;[Ljava/lang/String;)V

    new-instance v0, Lrn;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lrn;-><init>(Lrj;Lrk;)V

    iput-object v0, p0, Lrj;->f:Lrs;

    new-instance v0, Lrg;

    iget-object v1, p0, Lrj;->f:Lrs;

    invoke-direct {v0, p1, v1}, Lrg;-><init>(Landroid/content/Context;Lrs;)V

    iput-object v0, p0, Lrj;->g:Lrg;

    iput-object p4, p0, Lrj;->h:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lrj;)V
    .locals 0

    invoke-virtual {p0}, Lrj;->n()V

    return-void
.end method

.method static synthetic b(Lrj;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0}, Lrj;->o()Landroid/os/IInterface;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/os/IBinder;)Lrd;
    .locals 1

    invoke-static {p1}, Lre;->a(Landroid/os/IBinder;)Lrd;

    move-result-object v0

    return-object v0
.end method

.method public a(JLandroid/app/PendingIntent;)V
    .locals 5

    const/4 v0, 0x1

    invoke-virtual {p0}, Lrj;->n()V

    invoke-static {p3}, Lnx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-ltz v1, :cond_0

    :goto_0
    const-string v1, "detectionIntervalMillis must be >= 0"

    invoke-static {v0, v1}, Lnx;->b(ZLjava/lang/Object;)V

    :try_start_0
    invoke-virtual {p0}, Lrj;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lrd;

    const/4 v1, 0x1

    invoke-interface {v0, p1, p2, v1, p3}, Lrd;->a(JZLandroid/app/PendingIntent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Landroid/app/PendingIntent;)V
    .locals 2

    invoke-virtual {p0}, Lrj;->n()V

    invoke-static {p1}, Lnx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    invoke-virtual {p0}, Lrj;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lrd;

    invoke-interface {v0, p1}, Lrd;->a(Landroid/app/PendingIntent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Landroid/app/PendingIntent;Lvj;)V
    .locals 3

    invoke-virtual {p0}, Lrj;->n()V

    const-string v0, "PendingIntent must be specified."

    invoke-static {p1, v0}, Lnx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "OnRemoveGeofencesResultListener not provided."

    invoke-static {p2, v0}, Lnx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p2, :cond_0

    const/4 v0, 0x0

    move-object v1, v0

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lrj;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lrd;

    invoke-virtual {p0}, Lrj;->i()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, v1, v2}, Lrd;->a(Landroid/app/PendingIntent;Lra;Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Lrm;

    invoke-direct {v0, p2, p0}, Lrm;-><init>(Lvj;Lrj;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Landroid/location/Location;)V
    .locals 1

    iget-object v0, p0, Lrj;->g:Lrg;

    invoke-virtual {v0, p1}, Lrg;->a(Landroid/location/Location;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V
    .locals 1

    iget-object v0, p0, Lrj;->g:Lrg;

    invoke-virtual {v0, p1, p2}, Lrg;->a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/location/LocationRequest;Lvk;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lrj;->a(Lcom/google/android/gms/location/LocationRequest;Lvk;Landroid/os/Looper;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/location/LocationRequest;Lvk;Landroid/os/Looper;)V
    .locals 2

    iget-object v1, p0, Lrj;->g:Lrg;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lrj;->g:Lrg;

    invoke-virtual {v0, p1, p2, p3}, Lrg;->a(Lcom/google/android/gms/location/LocationRequest;Lvk;Landroid/os/Looper;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/util/List;Landroid/app/PendingIntent;Lvi;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/internal/fa;",
            ">;",
            "Landroid/app/PendingIntent;",
            "Lvi;",
            ")V"
        }
    .end annotation

    invoke-virtual {p0}, Lrj;->n()V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "At least one geofence must be specified."

    invoke-static {v0, v1}, Lnx;->b(ZLjava/lang/Object;)V

    const-string v0, "PendingIntent must be specified."

    invoke-static {p2, v0}, Lnx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "OnAddGeofencesResultListener not provided."

    invoke-static {p3, v0}, Lnx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p3, :cond_1

    const/4 v0, 0x0

    move-object v1, v0

    :goto_1
    :try_start_0
    invoke-virtual {p0}, Lrj;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lrd;

    invoke-virtual {p0}, Lrj;->i()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, p2, v1, v2}, Lrd;->a(Ljava/util/List;Landroid/app/PendingIntent;Lra;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Lrm;

    invoke-direct {v0, p3, p0}, Lrm;-><init>(Lvi;Lrj;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Ljava/util/List;Lvj;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lvj;",
            ")V"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-virtual {p0}, Lrj;->n()V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "geofenceRequestIds can\'t be null nor empty."

    invoke-static {v0, v2}, Lnx;->b(ZLjava/lang/Object;)V

    const-string v0, "OnRemoveGeofencesResultListener not provided."

    invoke-static {p2, v0}, Lnx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-array v0, v1, [Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    if-nez p2, :cond_1

    const/4 v1, 0x0

    move-object v2, v1

    :goto_1
    :try_start_0
    invoke-virtual {p0}, Lrj;->o()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lrd;

    invoke-virtual {p0}, Lrj;->i()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Lrd;->a([Ljava/lang/String;Lra;Ljava/lang/String;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    new-instance v1, Lrm;

    invoke-direct {v1, p2, p0}, Lrm;-><init>(Lvj;Lrj;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v1

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected a(Lno;Lnd;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "client_name"

    iget-object v2, p0, Lrj;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x3d8024

    invoke-virtual {p0}, Lrj;->i()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lno;->e(Lnl;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Lvk;)V
    .locals 1

    iget-object v0, p0, Lrj;->g:Lrg;

    invoke-virtual {v0, p1}, Lrg;->a(Lvk;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lrj;->g:Lrg;

    invoke-virtual {v0, p1}, Lrg;->a(Z)V

    return-void
.end method

.method protected synthetic b(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lrj;->a(Landroid/os/IBinder;)Lrd;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/app/PendingIntent;)V
    .locals 1

    iget-object v0, p0, Lrj;->g:Lrg;

    invoke-virtual {v0, p1}, Lrg;->a(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public d()V
    .locals 2

    iget-object v1, p0, Lrj;->g:Lrg;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lrj;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lrj;->g:Lrg;

    invoke-virtual {v0}, Lrg;->b()V

    iget-object v0, p0, Lrj;->g:Lrg;

    invoke-virtual {v0}, Lrg;->c()V

    :cond_0
    invoke-super {p0}, Lmz;->d()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.location.internal.GoogleLocationManagerService.START"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.location.internal.IGoogleLocationManagerService"

    return-object v0
.end method

.method public g()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lrj;->g:Lrg;

    invoke-virtual {v0}, Lrg;->a()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method
