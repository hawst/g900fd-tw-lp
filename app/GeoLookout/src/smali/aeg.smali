.class public Laeg;
.super Ljava/lang/Object;

# interfaces
.implements Lca;


# instance fields
.field private final a:Lty;


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILjava/lang/String;ILcb;Lcc;)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lty;

    move-object v1, p1

    move-object v2, p5

    move-object v3, p6

    move v4, p2

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lty;-><init>(Landroid/app/Activity;Lcb;Lcc;ILjava/lang/String;I)V

    iput-object v0, p0, Laeg;->a:Lty;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;ILjava/lang/String;Lcb;Lcc;)V
    .locals 7

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Laeg;-><init>(Landroid/app/Activity;ILjava/lang/String;ILcb;Lcc;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Laeg;->a:Lty;

    invoke-virtual {v0}, Lty;->a()V

    return-void
.end method

.method public a(I)V
    .locals 1

    iget-object v0, p0, Laeg;->a:Lty;

    invoke-virtual {v0, p1}, Lty;->a(I)V

    return-void
.end method

.method public a(Lcb;)V
    .locals 1

    iget-object v0, p0, Laeg;->a:Lty;

    invoke-virtual {v0, p1}, Lty;->a(Lcb;)V

    return-void
.end method

.method public a(Lcc;)V
    .locals 1

    iget-object v0, p0, Laeg;->a:Lty;

    invoke-virtual {v0, p1}, Lty;->a(Lcc;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/wallet/FullWalletRequest;I)V
    .locals 1

    iget-object v0, p0, Laeg;->a:Lty;

    invoke-virtual {v0, p1, p2}, Lty;->a(Lcom/google/android/gms/wallet/FullWalletRequest;I)V

    return-void
.end method

.method public a(Lcom/google/android/gms/wallet/MaskedWalletRequest;I)V
    .locals 1

    iget-object v0, p0, Laeg;->a:Lty;

    invoke-virtual {v0, p1, p2}, Lty;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;I)V

    return-void
.end method

.method public a(Lcom/google/android/gms/wallet/NotifyTransactionStatusRequest;)V
    .locals 1

    iget-object v0, p0, Laeg;->a:Lty;

    invoke-virtual {v0, p1}, Lty;->a(Lcom/google/android/gms/wallet/NotifyTransactionStatusRequest;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Laeg;->a:Lty;

    invoke-virtual {v0, p1, p2, p3}, Lty;->a(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Laeg;->a:Lty;

    invoke-virtual {v0}, Lty;->b()Z

    move-result v0

    return v0
.end method

.method public b(Lcb;)Z
    .locals 1

    iget-object v0, p0, Laeg;->a:Lty;

    invoke-virtual {v0, p1}, Lty;->b(Lcb;)Z

    move-result v0

    return v0
.end method

.method public b(Lcc;)Z
    .locals 1

    iget-object v0, p0, Laeg;->a:Lty;

    invoke-virtual {v0, p1}, Lty;->b(Lcc;)Z

    move-result v0

    return v0
.end method

.method public c(Lcb;)V
    .locals 1

    iget-object v0, p0, Laeg;->a:Lty;

    invoke-virtual {v0, p1}, Lty;->c(Lcb;)V

    return-void
.end method

.method public c(Lcc;)V
    .locals 1

    iget-object v0, p0, Laeg;->a:Lty;

    invoke-virtual {v0, p1}, Lty;->c(Lcc;)V

    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Laeg;->a:Lty;

    invoke-virtual {v0}, Lty;->c()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Laeg;->a:Lty;

    invoke-virtual {v0}, Lty;->d()V

    return-void
.end method
