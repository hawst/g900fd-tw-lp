.class public Lapn;
.super Lapd;
.source "ShowDisasterAlertNotificationTask.java"


# instance fields
.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lalo;Lalr;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lapd;-><init>(Landroid/content/Context;Lalo;Lalr;)V

    .line 20
    iput-object p1, p0, Lapn;->b:Landroid/content/Context;

    .line 21
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 25
    iget-object v0, p0, Lapn;->b:Landroid/content/Context;

    invoke-static {v0}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapn;->b:Landroid/content/Context;

    invoke-static {v0}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lapn;->b:Landroid/content/Context;

    invoke-static {v0}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v1, :cond_1

    .line 28
    :cond_0
    const-string v0, "GeoNews is turning OFF"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 53
    :goto_0
    return-void

    .line 32
    :cond_1
    invoke-virtual {p0}, Lapn;->b()Lalr;

    move-result-object v0

    invoke-virtual {v0}, Lalr;->k()Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 34
    if-eqz v0, :cond_3

    .line 35
    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v1

    .line 37
    if-eqz v1, :cond_2

    iget-object v2, p0, Lapn;->b:Landroid/content/Context;

    invoke-static {v2, v1}, Lalz;->b(Landroid/content/Context;I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No notification because buddy "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is not found"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 43
    :cond_2
    const-wide/16 v2, 0x5dc

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :goto_1
    iget-object v1, p0, Lapn;->b:Landroid/content/Context;

    invoke-virtual {p0, v1, v0}, Lapn;->b(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    goto :goto_0

    .line 45
    :catch_0
    move-exception v1

    .line 46
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 51
    :cond_3
    const-string v0, "DisasterInfo is NULL"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
