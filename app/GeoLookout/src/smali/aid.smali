.class public Laid;
.super Ljava/lang/Object;
.source "DisasterDashboardActivity.java"

# interfaces
.implements Lwh;


# instance fields
.field a:Landroid/view/View;

.field b:Landroid/widget/TextView;

.field c:Landroid/widget/TextView;

.field d:Landroid/widget/TextView;

.field e:Landroid/widget/ImageView;

.field final synthetic f:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1239
    iput-object p1, p0, Laid;->f:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1241
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1242
    const/high16 v1, 0x7f030000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laid;->a:Landroid/view/View;

    .line 1243
    return-void
.end method

.method public synthetic constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Landroid/content/Context;Lagz;)V
    .locals 0

    .prologue
    .line 1232
    invoke-direct {p0, p1, p2}, Laid;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public a(Laah;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1281
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Laah;)Landroid/view/View;
    .locals 12

    .prologue
    const v6, 0x7f0a00a5

    const-wide v10, 0x3ff9bfdf7e8038a0L    # 1.609344

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1247
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Laah;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1248
    :cond_0
    const/4 v0, 0x0

    .line 1276
    :goto_0
    return-object v0

    .line 1250
    :cond_1
    invoke-virtual {p1}, Laah;->e()Ljava/lang/String;

    move-result-object v0

    .line 1251
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1252
    aget-object v0, v1, v9

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 1254
    iget-object v0, p0, Laid;->a:Landroid/view/View;

    const/high16 v3, 0x7f0e0000

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laid;->b:Landroid/widget/TextView;

    .line 1255
    iget-object v0, p0, Laid;->a:Landroid/view/View;

    const v3, 0x7f0e0001

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laid;->d:Landroid/widget/TextView;

    .line 1256
    iget-object v0, p0, Laid;->a:Landroid/view/View;

    const v3, 0x7f0e0002

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Laid;->c:Landroid/widget/TextView;

    .line 1257
    iget-object v0, p0, Laid;->a:Landroid/view/View;

    const v3, 0x7f0e0003

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Laid;->e:Landroid/widget/ImageView;

    .line 1259
    aget-object v0, v1, v7

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1260
    aget-object v1, v1, v8

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1262
    iget-object v3, p0, Laid;->f:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v3}, Lall;->w(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1263
    iget-object v3, p0, Laid;->b:Landroid/widget/TextView;

    iget-object v4, p0, Laid;->f:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-virtual {v4}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "mph"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1264
    iget-object v0, p0, Laid;->d:Landroid/widget/TextView;

    iget-object v3, p0, Laid;->f:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-virtual {v3}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00a7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v7

    const-string v1, "mph"

    aput-object v1, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1273
    :goto_1
    iget-object v0, p0, Laid;->c:Landroid/widget/TextView;

    iget-object v1, p0, Laid;->f:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a00a6

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/Object;

    float-to-int v4, v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1275
    iget-object v0, p0, Laid;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setRotation(F)V

    .line 1276
    iget-object v0, p0, Laid;->a:Landroid/view/View;

    goto/16 :goto_0

    .line 1266
    :cond_2
    int-to-double v4, v0

    mul-double/2addr v4, v10

    double-to-int v0, v4

    .line 1267
    iget-object v3, p0, Laid;->b:Landroid/widget/TextView;

    iget-object v4, p0, Laid;->f:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-virtual {v4}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/Object;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "km/h"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1269
    int-to-double v0, v1

    mul-double/2addr v0, v10

    double-to-int v0, v0

    .line 1270
    iget-object v1, p0, Laid;->d:Landroid/widget/TextView;

    iget-object v3, p0, Laid;->f:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-virtual {v3}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00a7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v7

    const-string v0, "km/h"

    aput-object v0, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
