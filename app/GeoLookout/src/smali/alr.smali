.class public Lalr;
.super Ljava/lang/Object;
.source "DisasterOptions.java"


# instance fields
.field private final A:I

.field private B:Ljava/lang/String;

.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:I

.field private final e:D

.field private final f:D

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;

.field private final m:Lcom/sec/android/GeoLookout/db/DisasterInfo;

.field private final n:Lanx;

.field private final o:Lanz;

.field private final p:Lanw;

.field private final q:Lany;

.field private final r:I

.field private final s:I

.field private final t:Landroid/location/Location;

.field private final u:Z

.field private final v:Ljava/lang/String;

.field private final w:Ljava/lang/String;

.field private final x:Z

.field private final y:Z

.field private final z:I


# direct methods
.method private constructor <init>(Lalt;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {p1}, Lalt;->a(Lalt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lalr;->a:Ljava/lang/String;

    .line 52
    invoke-static {p1}, Lalt;->b(Lalt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lalr;->b:Ljava/lang/String;

    .line 53
    invoke-static {p1}, Lalt;->c(Lalt;)I

    move-result v0

    iput v0, p0, Lalr;->c:I

    .line 54
    invoke-static {p1}, Lalt;->d(Lalt;)I

    move-result v0

    iput v0, p0, Lalr;->d:I

    .line 55
    invoke-static {p1}, Lalt;->e(Lalt;)D

    move-result-wide v0

    iput-wide v0, p0, Lalr;->e:D

    .line 56
    invoke-static {p1}, Lalt;->f(Lalt;)D

    move-result-wide v0

    iput-wide v0, p0, Lalr;->f:D

    .line 58
    invoke-static {p1}, Lalt;->g(Lalt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lalr;->g:Ljava/lang/String;

    .line 59
    invoke-static {p1}, Lalt;->h(Lalt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lalr;->h:Ljava/lang/String;

    .line 60
    invoke-static {p1}, Lalt;->i(Lalt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lalr;->i:Ljava/lang/String;

    .line 61
    invoke-static {p1}, Lalt;->j(Lalt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lalr;->j:Ljava/lang/String;

    .line 63
    invoke-static {p1}, Lalt;->k(Lalt;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    iput-object v0, p0, Lalr;->m:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    .line 64
    invoke-static {p1}, Lalt;->l(Lalt;)Lanx;

    move-result-object v0

    iput-object v0, p0, Lalr;->n:Lanx;

    .line 65
    invoke-static {p1}, Lalt;->m(Lalt;)Lanz;

    move-result-object v0

    iput-object v0, p0, Lalr;->o:Lanz;

    .line 66
    invoke-static {p1}, Lalt;->n(Lalt;)Lanw;

    move-result-object v0

    iput-object v0, p0, Lalr;->p:Lanw;

    .line 67
    invoke-static {p1}, Lalt;->o(Lalt;)Lany;

    move-result-object v0

    iput-object v0, p0, Lalr;->q:Lany;

    .line 69
    invoke-static {p1}, Lalt;->p(Lalt;)I

    move-result v0

    iput v0, p0, Lalr;->r:I

    .line 70
    invoke-static {p1}, Lalt;->q(Lalt;)I

    move-result v0

    iput v0, p0, Lalr;->s:I

    .line 71
    invoke-static {p1}, Lalt;->r(Lalt;)Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lalr;->t:Landroid/location/Location;

    .line 72
    invoke-static {p1}, Lalt;->s(Lalt;)Z

    move-result v0

    iput-boolean v0, p0, Lalr;->u:Z

    .line 73
    invoke-static {p1}, Lalt;->t(Lalt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lalr;->v:Ljava/lang/String;

    .line 74
    invoke-static {p1}, Lalt;->u(Lalt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lalr;->w:Ljava/lang/String;

    .line 75
    invoke-static {p1}, Lalt;->v(Lalt;)Z

    move-result v0

    iput-boolean v0, p0, Lalr;->x:Z

    .line 76
    invoke-static {p1}, Lalt;->w(Lalt;)Z

    move-result v0

    iput-boolean v0, p0, Lalr;->y:Z

    .line 77
    invoke-static {p1}, Lalt;->x(Lalt;)I

    move-result v0

    iput v0, p0, Lalr;->z:I

    .line 78
    invoke-static {p1}, Lalt;->y(Lalt;)I

    move-result v0

    iput v0, p0, Lalr;->A:I

    .line 79
    invoke-static {p1}, Lalt;->z(Lalt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lalr;->B:Ljava/lang/String;

    .line 80
    invoke-static {p1}, Lalt;->A(Lalt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lalr;->k:Ljava/lang/String;

    .line 81
    invoke-static {p1}, Lalt;->B(Lalt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lalr;->l:Ljava/lang/String;

    .line 82
    return-void
.end method

.method synthetic constructor <init>(Lalt;Lals;)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lalr;-><init>(Lalt;)V

    return-void
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lalr;->k:Ljava/lang/String;

    return-object v0
.end method

.method public B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lalr;->l:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lalr;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lalr;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 281
    iget v0, p0, Lalr;->c:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 285
    iget v0, p0, Lalr;->d:I

    return v0
.end method

.method public e()D
    .locals 2

    .prologue
    .line 289
    iget-wide v0, p0, Lalr;->e:D

    return-wide v0
.end method

.method public f()D
    .locals 2

    .prologue
    .line 293
    iget-wide v0, p0, Lalr;->f:D

    return-wide v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lalr;->g:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lalr;->h:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lalr;->i:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lalr;->j:Ljava/lang/String;

    return-object v0
.end method

.method public k()Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lalr;->m:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    return-object v0
.end method

.method public l()Lanx;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lalr;->n:Lanx;

    return-object v0
.end method

.method public m()Lanz;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lalr;->o:Lanz;

    return-object v0
.end method

.method public n()Lanw;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lalr;->p:Lanw;

    return-object v0
.end method

.method public o()Lany;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lalr;->q:Lany;

    return-object v0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 333
    iget v0, p0, Lalr;->r:I

    return v0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 337
    iget v0, p0, Lalr;->s:I

    return v0
.end method

.method public r()Landroid/location/Location;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lalr;->t:Landroid/location/Location;

    return-object v0
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 345
    iget-boolean v0, p0, Lalr;->u:Z

    return v0
.end method

.method public t()Ljava/lang/String;
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lalr;->v:Ljava/lang/String;

    return-object v0
.end method

.method public u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lalr;->w:Ljava/lang/String;

    return-object v0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 357
    iget-boolean v0, p0, Lalr;->x:Z

    return v0
.end method

.method public w()Z
    .locals 1

    .prologue
    .line 361
    iget-boolean v0, p0, Lalr;->y:Z

    return v0
.end method

.method public x()I
    .locals 1

    .prologue
    .line 365
    iget v0, p0, Lalr;->z:I

    return v0
.end method

.method public y()I
    .locals 1

    .prologue
    .line 369
    iget v0, p0, Lalr;->A:I

    return v0
.end method

.method public z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lalr;->B:Ljava/lang/String;

    return-object v0
.end method
