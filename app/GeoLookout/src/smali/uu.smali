.class final Luu;
.super Ljava/lang/Object;


# instance fields
.field public final a:Landroid/widget/ViewSwitcher;

.field public final b:Lcom/google/android/gms/internal/x;

.field public final c:Ljava/lang/String;

.field public final d:Landroid/content/Context;

.field public final e:Lue;

.field public final f:Lcom/google/android/gms/internal/co;

.field public g:Lgd;

.field public h:Lla;

.field public i:Lky;

.field public j:Lgm;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/x;Ljava/lang/String;Lcom/google/android/gms/internal/co;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-boolean v0, p2, Lcom/google/android/gms/internal/x;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Luu;->a:Landroid/widget/ViewSwitcher;

    :goto_0
    iput-object p2, p0, Luu;->b:Lcom/google/android/gms/internal/x;

    iput-object p3, p0, Luu;->c:Ljava/lang/String;

    iput-object p1, p0, Luu;->d:Landroid/content/Context;

    new-instance v0, Lue;

    iget-object v1, p4, Lcom/google/android/gms/internal/co;->c:Ljava/lang/String;

    invoke-static {v1, p1}, Ltj;->a(Ljava/lang/String;Landroid/content/Context;)Ltj;

    move-result-object v1

    invoke-direct {v0, v1}, Lue;-><init>(Lmw;)V

    iput-object v0, p0, Luu;->e:Lue;

    iput-object p4, p0, Luu;->f:Lcom/google/android/gms/internal/co;

    return-void

    :cond_0
    new-instance v0, Landroid/widget/ViewSwitcher;

    invoke-direct {v0, p1}, Landroid/widget/ViewSwitcher;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Luu;->a:Landroid/widget/ViewSwitcher;

    iget-object v0, p0, Luu;->a:Landroid/widget/ViewSwitcher;

    iget v1, p2, Lcom/google/android/gms/internal/x;->i:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setMinimumWidth(I)V

    iget-object v0, p0, Luu;->a:Landroid/widget/ViewSwitcher;

    iget v1, p2, Lcom/google/android/gms/internal/x;->e:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setMinimumHeight(I)V

    iget-object v0, p0, Luu;->a:Landroid/widget/ViewSwitcher;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setVisibility(I)V

    goto :goto_0
.end method
