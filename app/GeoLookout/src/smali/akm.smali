.class Lakm;
.super Ljava/lang/Object;
.source "DisasterSettingsFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lakf;


# direct methods
.method constructor <init>(Lakf;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lakm;->a:Lakf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 302
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 304
    if-nez v2, :cond_1

    .line 305
    iget-object v1, p0, Lakm;->a:Lakf;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-static {v1, v2}, Lakf;->a(Lakf;F)F

    .line 306
    iget-object v1, p0, Lakm;->a:Lakf;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-static {v1, v2}, Lakf;->b(Lakf;F)F

    .line 339
    :cond_0
    :goto_0
    return v0

    .line 309
    :cond_1
    if-eq v2, v0, :cond_2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    .line 311
    :cond_2
    iget-object v2, p0, Lakm;->a:Lakf;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-static {v2, v3}, Lakf;->c(Lakf;F)F

    .line 312
    iget-object v2, p0, Lakm;->a:Lakf;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-static {v2, v3}, Lakf;->d(Lakf;F)F

    .line 314
    iget-object v2, p0, Lakm;->a:Lakf;

    invoke-static {v2}, Lakf;->c(Lakf;)F

    move-result v2

    iget-object v3, p0, Lakm;->a:Lakf;

    invoke-static {v3}, Lakf;->d(Lakf;)F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 315
    iget-object v3, p0, Lakm;->a:Lakf;

    invoke-static {v3}, Lakf;->e(Lakf;)F

    move-result v3

    iget-object v4, p0, Lakm;->a:Lakf;

    invoke-static {v4}, Lakf;->f(Lakf;)F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 317
    cmpl-float v3, v3, v2

    if-gtz v3, :cond_0

    const/high16 v3, 0x41200000    # 10.0f

    cmpl-float v2, v3, v2

    if-gtz v2, :cond_0

    .line 321
    iget-object v2, p0, Lakm;->a:Lakf;

    invoke-static {v2}, Lakf;->d(Lakf;)F

    move-result v2

    iget-object v3, p0, Lakm;->a:Lakf;

    invoke-static {v3}, Lakf;->c(Lakf;)F

    move-result v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    .line 323
    iget-object v1, p0, Lakm;->a:Lakf;

    invoke-static {v1}, Lakf;->g(Lakf;)I

    move-result v1

    if-nez v1, :cond_0

    .line 324
    iget-object v1, p0, Lakm;->a:Lakf;

    invoke-static {v1, v0}, Lakf;->a(Lakf;I)I

    .line 325
    iget-object v1, p0, Lakm;->a:Lakf;

    invoke-static {v1}, Lakf;->h(Lakf;)V

    goto :goto_0

    .line 330
    :cond_3
    iget-object v2, p0, Lakm;->a:Lakf;

    invoke-static {v2}, Lakf;->g(Lakf;)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 331
    iget-object v2, p0, Lakm;->a:Lakf;

    invoke-static {v2, v1}, Lakf;->a(Lakf;I)I

    .line 332
    iget-object v1, p0, Lakm;->a:Lakf;

    invoke-static {v1}, Lakf;->h(Lakf;)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 339
    goto :goto_0
.end method
