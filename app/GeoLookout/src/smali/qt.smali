.class public Lqt;
.super Ljava/lang/Object;


# instance fields
.field protected a:Lot;

.field protected b:Lqv;


# direct methods
.method private constructor <init>(Lot;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lqt;->a:Lot;

    invoke-virtual {p0, p2}, Lqt;->a(I)V

    return-void
.end method

.method synthetic constructor <init>(Lot;ILqu;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lqt;-><init>(Lot;I)V

    return-void
.end method

.method public static a(Lot;I)Lqt;
    .locals 1

    invoke-static {}, Lor;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lqw;

    invoke-direct {v0, p0, p1}, Lqw;-><init>(Lot;I)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lqt;

    invoke-direct {v0, p0, p1}, Lqt;-><init>(Lot;I)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v0, p0, Lqt;->a:Lot;

    iget-object v1, p0, Lqt;->b:Lqv;

    iget-object v1, v1, Lqv;->a:Landroid/os/IBinder;

    iget-object v2, p0, Lqt;->b:Lqv;

    invoke-virtual {v2}, Lqv;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lot;->a(Landroid/os/IBinder;Landroid/os/Bundle;)V

    return-void
.end method

.method protected a(I)V
    .locals 3

    new-instance v0, Lqv;

    new-instance v1, Landroid/os/Binder;

    invoke-direct {v1}, Landroid/os/Binder;-><init>()V

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lqv;-><init>(ILandroid/os/IBinder;Lqu;)V

    iput-object v0, p0, Lqt;->b:Lqv;

    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    return-void
.end method

.method public b()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lqt;->b:Lqv;

    invoke-virtual {v0}, Lqv;->a()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, Lqt;->b:Lqv;

    iput p1, v0, Lqv;->b:I

    return-void
.end method

.method public c()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lqt;->b:Lqv;

    iget-object v0, v0, Lqv;->a:Landroid/os/IBinder;

    return-object v0
.end method
