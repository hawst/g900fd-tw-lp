.class public final Lay;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lbe;",
            ">;",
            "Lbe;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Date;

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lay;->a:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lay;->b:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lay;->c:Ljava/util/HashSet;

    iput v1, p0, Lay;->e:I

    iput v1, p0, Lay;->f:I

    return-void
.end method

.method static synthetic a(Lay;)Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lay;->d:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic b(Lay;)I
    .locals 1

    iget v0, p0, Lay;->e:I

    return v0
.end method

.method static synthetic c(Lay;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lay;->a:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic d(Lay;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lay;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic e(Lay;)I
    .locals 1

    iget v0, p0, Lay;->f:I

    return v0
.end method

.method static synthetic f(Lay;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lay;->c:Ljava/util/HashSet;

    return-object v0
.end method


# virtual methods
.method public a()Law;
    .locals 2

    new-instance v0, Law;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Law;-><init>(Lay;Lax;)V

    return-object v0
.end method

.method public a(I)Lay;
    .locals 0

    iput p1, p0, Lay;->e:I

    return-object p0
.end method

.method public a(Lbe;)Lay;
    .locals 2

    iget-object v0, p0, Lay;->b:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lay;
    .locals 1

    iget-object v0, p0, Lay;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Ljava/util/Date;)Lay;
    .locals 0

    iput-object p1, p0, Lay;->d:Ljava/util/Date;

    return-object p0
.end method

.method public a(Z)Lay;
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lay;->f:I

    return-object p0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Lay;
    .locals 1

    iget-object v0, p0, Lay;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-object p0
.end method
