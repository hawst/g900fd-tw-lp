.class public Lvh;
.super Ljava/lang/Object;

# interfaces
.implements Lca;


# static fields
.field public static final a:Ljava/lang/String; = "com.google.android.location.LOCATION"

.field public static final b:Ljava/lang/String; = "mockLocation"


# instance fields
.field private final c:Lrj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcb;Lcc;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lrj;

    const-string v1, "location"

    invoke-direct {v0, p1, p2, p3, v1}, Lrj;-><init>(Landroid/content/Context;Lcb;Lcc;Ljava/lang/String;)V

    iput-object v0, p0, Lvh;->c:Lrj;

    return-void
.end method

.method public static a(Landroid/content/Intent;)Z
    .locals 1

    const-string v0, "gms_error_code"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Intent;)I
    .locals 2

    const-string v0, "gms_error_code"

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static c(Landroid/content/Intent;)I
    .locals 3

    const/4 v0, -0x1

    const-string v1, "com.google.android.location.intent.extra.transition"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static d(Landroid/content/Intent;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Lvf;",
            ">;"
        }
    .end annotation

    const-string v0, "com.google.android.location.intent.extra.geofence_list"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0}, Lcom/google/android/gms/internal/fa;->a([B)Lcom/google/android/gms/internal/fa;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0}, Lrj;->a()V

    return-void
.end method

.method public a(Landroid/app/PendingIntent;)V
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0, p1}, Lrj;->b(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public a(Landroid/app/PendingIntent;Lvj;)V
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0, p1, p2}, Lrj;->a(Landroid/app/PendingIntent;Lvj;)V

    return-void
.end method

.method public a(Landroid/location/Location;)V
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0, p1}, Lrj;->a(Landroid/location/Location;)V

    return-void
.end method

.method public a(Lcb;)V
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0, p1}, Lrj;->a(Lcb;)V

    return-void
.end method

.method public a(Lcc;)V
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0, p1}, Lrj;->a(Lcc;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0, p1, p2}, Lrj;->a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/location/LocationRequest;Lvk;)V
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0, p1, p2}, Lrj;->a(Lcom/google/android/gms/location/LocationRequest;Lvk;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/location/LocationRequest;Lvk;Landroid/os/Looper;)V
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0, p1, p2, p3}, Lrj;->a(Lcom/google/android/gms/location/LocationRequest;Lvk;Landroid/os/Looper;)V

    return-void
.end method

.method public a(Ljava/util/List;Landroid/app/PendingIntent;Lvi;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lvf;",
            ">;",
            "Landroid/app/PendingIntent;",
            "Lvi;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvf;

    instance-of v3, v0, Lcom/google/android/gms/internal/fa;

    const-string v4, "Geofence must be created using Geofence.Builder."

    invoke-static {v3, v4}, Lnx;->b(ZLjava/lang/Object;)V

    check-cast v0, Lcom/google/android/gms/internal/fa;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object v0, v1

    :cond_1
    iget-object v1, p0, Lvh;->c:Lrj;

    invoke-virtual {v1, v0, p2, p3}, Lrj;->a(Ljava/util/List;Landroid/app/PendingIntent;Lvi;)V

    return-void
.end method

.method public a(Ljava/util/List;Lvj;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lvj;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0, p1, p2}, Lrj;->a(Ljava/util/List;Lvj;)V

    return-void
.end method

.method public a(Lvk;)V
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0, p1}, Lrj;->a(Lvk;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0, p1}, Lrj;->a(Z)V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0}, Lrj;->b()Z

    move-result v0

    return v0
.end method

.method public b(Lcb;)Z
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0, p1}, Lrj;->b(Lcb;)Z

    move-result v0

    return v0
.end method

.method public b(Lcc;)Z
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0, p1}, Lrj;->b(Lcc;)Z

    move-result v0

    return v0
.end method

.method public c(Lcb;)V
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0, p1}, Lrj;->c(Lcb;)V

    return-void
.end method

.method public c(Lcc;)V
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0, p1}, Lrj;->c(Lcc;)V

    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0}, Lrj;->c()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0}, Lrj;->d()V

    return-void
.end method

.method public e()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lvh;->c:Lrj;

    invoke-virtual {v0}, Lrj;->g()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method
