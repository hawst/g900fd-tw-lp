.class public final Lkt;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lgz;

.field public final b:Lgz;

.field private final c:Ljava/lang/Object;

.field private d:Lcom/google/android/gms/internal/cq;

.field private e:I

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lkt;->c:Ljava/lang/Object;

    const/4 v0, -0x2

    iput v0, p0, Lkt;->e:I

    new-instance v0, Lku;

    invoke-direct {v0, p0}, Lku;-><init>(Lkt;)V

    iput-object v0, p0, Lkt;->a:Lgz;

    new-instance v0, Lkv;

    invoke-direct {v0, p0}, Lkv;-><init>(Lkt;)V

    iput-object v0, p0, Lkt;->b:Lgz;

    return-void
.end method

.method static synthetic a(Lkt;I)I
    .locals 0

    iput p1, p0, Lkt;->e:I

    return p1
.end method

.method static synthetic a(Lkt;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lkt;->c:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Lkt;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lkt;->f:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public a()I
    .locals 2

    iget-object v1, p0, Lkt;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lkt;->e:I

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/gms/internal/cq;)V
    .locals 2

    iget-object v1, p0, Lkt;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p1, p0, Lkt;->d:Lcom/google/android/gms/internal/cq;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lkt;->c:Ljava/lang/Object;

    monitor-enter v1

    :goto_0
    :try_start_0
    iget-object v0, p0, Lkt;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    iget v0, p0, Lkt;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, -0x2

    if-ne v0, v2, :cond_0

    :try_start_1
    iget-object v0, p0, Lkt;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "Ad request service was interrupted."

    invoke-static {v0}, Llm;->e(Ljava/lang/String;)V

    const/4 v0, 0x0

    monitor-exit v1

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, Lkt;->f:Ljava/lang/String;

    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
