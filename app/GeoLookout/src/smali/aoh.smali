.class public Laoh;
.super Ljava/lang/Object;
.source "DserverURL.java"


# static fields
.field private static volatile a:Laoh;

.field private static b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19
    sput-object v0, Laoh;->a:Laoh;

    .line 36
    sput-object v0, Laoh;->b:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    return-void
.end method

.method public static a()Laoh;
    .locals 2

    .prologue
    .line 25
    sget-object v0, Laoh;->a:Laoh;

    if-nez v0, :cond_1

    .line 26
    const-class v1, Laoh;

    monitor-enter v1

    .line 27
    :try_start_0
    sget-object v0, Laoh;->a:Laoh;

    if-nez v0, :cond_0

    .line 28
    new-instance v0, Laoh;

    invoke-direct {v0}, Laoh;-><init>()V

    sput-object v0, Laoh;->a:Laoh;

    .line 30
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 33
    :cond_1
    sget-object v0, Laoh;->a:Laoh;

    return-object v0

    .line 30
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 39
    sget-boolean v0, Lalj;->b:Z

    if-eqz v0, :cond_0

    .line 40
    const-string v0, "http://sew-sd-elb-326025053.eu-west-1.elb.amazonaws.com"

    sput-object v0, Laoh;->b:Ljava/lang/String;

    .line 49
    :goto_0
    sget-object v0, Laoh;->b:Ljava/lang/String;

    return-object v0

    .line 42
    :cond_0
    sget-boolean v0, Laky;->c:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 43
    invoke-virtual {p0, p1}, Laoh;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Laoh;->b:Ljava/lang/String;

    goto :goto_0

    .line 45
    :cond_1
    invoke-static {p1}, Lall;->A(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "https://ew.disaster-device.ssp.samsung.com"

    :goto_1
    sput-object v0, Laoh;->b:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v0, "http://sew-sd-elb-326025053.eu-west-1.elb.amazonaws.com"

    goto :goto_1
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 122
    sput-object p2, Laoh;->b:Ljava/lang/String;

    .line 124
    invoke-static {p1}, Lall;->A(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 125
    const-string v0, "https://apchina.disaster-device.ssp.samsung.com"

    sget-object v1, Laoh;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https://ew.disaster-device.ssp.samsung.com"

    sget-object v1, Laoh;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 126
    invoke-virtual {p0, p1}, Laoh;->a(Landroid/content/Context;)Ljava/lang/String;

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    const-string v0, "http://safety-stg1-sd-elb-1203804584.ap-northeast-1.elb.amazonaws.com"

    sget-object v1, Laoh;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "http://sew-sd-elb-326025053.eu-west-1.elb.amazonaws.com"

    sget-object v1, Laoh;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    invoke-virtual {p0, p1}, Laoh;->a(Landroid/content/Context;)Ljava/lang/String;

    goto :goto_0
.end method

.method public b(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    sget-object v0, Laoh;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 54
    sget-boolean v0, Laky;->c:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 55
    invoke-virtual {p0, p1}, Laoh;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Laoh;->b:Ljava/lang/String;

    .line 61
    :cond_0
    :goto_0
    sget-object v0, Laoh;->b:Ljava/lang/String;

    return-object v0

    .line 57
    :cond_1
    invoke-static {p1}, Lall;->A(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "https://ew.disaster-device.ssp.samsung.com"

    :goto_1
    sput-object v0, Laoh;->b:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v0, "http://sew-sd-elb-326025053.eu-west-1.elb.amazonaws.com"

    goto :goto_1
.end method

.method public b()Z
    .locals 3

    .prologue
    .line 73
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    const-string v2, "refresh.xml"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 75
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    const-string v0, "refresh test mode"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 77
    const/4 v0, 0x1

    .line 80
    :goto_0
    return v0

    .line 79
    :cond_0
    const-string v0, "refresh user mode"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 80
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 65
    invoke-virtual {p0}, Laoh;->b()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 66
    const-string v0, "http://sew-sd-elb-326025053.eu-west-1.elb.amazonaws.com"

    .line 68
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Laoh;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public d(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    .line 88
    invoke-static {p1}, Lall;->A(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 89
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 90
    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    const-string v0, "https://apchina.disaster-device.ssp.samsung.com"

    .line 118
    :goto_0
    return-object v0

    .line 93
    :cond_0
    const-string v0, "https://ew.disaster-device.ssp.samsung.com"

    goto :goto_0

    .line 96
    :cond_1
    invoke-static {p1}, Lall;->n(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 97
    const-string v0, "https://apchina.disaster-device.ssp.samsung.com"

    goto :goto_0

    .line 99
    :cond_2
    const-string v0, "https://ew.disaster-device.ssp.samsung.com"

    goto :goto_0

    .line 103
    :cond_3
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 104
    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 105
    const-string v0, "http://safety-stg1-sd-elb-1203804584.ap-northeast-1.elb.amazonaws.com"

    goto :goto_0

    .line 107
    :cond_4
    const-string v0, "http://sew-sd-elb-326025053.eu-west-1.elb.amazonaws.com"

    goto :goto_0

    .line 110
    :cond_5
    invoke-static {p1}, Lall;->n(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 111
    const-string v0, "http://safety-stg1-sd-elb-1203804584.ap-northeast-1.elb.amazonaws.com"

    goto :goto_0

    .line 113
    :cond_6
    const-string v0, "http://sew-sd-elb-326025053.eu-west-1.elb.amazonaws.com"

    goto :goto_0
.end method
