.class public Lada;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/google/android/gms/internal/fq;

.field private c:Ljava/lang/String;

.field private d:Lcom/google/android/gms/internal/fq;

.field private e:Ljava/lang/String;

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lada;->f:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public a()Lacz;
    .locals 7

    new-instance v0, Lcom/google/android/gms/internal/fs;

    iget-object v1, p0, Lada;->f:Ljava/util/Set;

    iget-object v2, p0, Lada;->a:Ljava/lang/String;

    iget-object v3, p0, Lada;->b:Lcom/google/android/gms/internal/fq;

    iget-object v4, p0, Lada;->c:Ljava/lang/String;

    iget-object v5, p0, Lada;->d:Lcom/google/android/gms/internal/fq;

    iget-object v6, p0, Lada;->e:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/fs;-><init>(Ljava/util/Set;Ljava/lang/String;Lcom/google/android/gms/internal/fq;Ljava/lang/String;Lcom/google/android/gms/internal/fq;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Lacx;)Lada;
    .locals 2

    check-cast p1, Lcom/google/android/gms/internal/fq;

    iput-object p1, p0, Lada;->b:Lcom/google/android/gms/internal/fq;

    iget-object v0, p0, Lada;->f:Ljava/util/Set;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lada;
    .locals 2

    iput-object p1, p0, Lada;->a:Ljava/lang/String;

    iget-object v0, p0, Lada;->f:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public b(Lacx;)Lada;
    .locals 2

    check-cast p1, Lcom/google/android/gms/internal/fq;

    iput-object p1, p0, Lada;->d:Lcom/google/android/gms/internal/fq;

    iget-object v0, p0, Lada;->f:Ljava/util/Set;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lada;
    .locals 2

    iput-object p1, p0, Lada;->c:Ljava/lang/String;

    iget-object v0, p0, Lada;->f:Ljava/util/Set;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public c(Ljava/lang/String;)Lada;
    .locals 2

    iput-object p1, p0, Lada;->e:Ljava/lang/String;

    iget-object v0, p0, Lada;->f:Ljava/util/Set;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method
