.class public Laju;
.super Landroid/widget/BaseAdapter;
.source "DisasterMultiPointActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)V
    .locals 0

    .prologue
    .line 455
    iput-object p1, p0, Laju;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Lajo;)V
    .locals 0

    .prologue
    .line 455
    invoke-direct {p0, p1}, Laju;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Laju;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->f(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, Laju;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->f(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 463
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Laju;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->f(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 473
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 479
    if-nez p2, :cond_0

    .line 480
    iget-object v0, p0, Laju;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 481
    const v1, 0x7f03002c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 484
    :cond_0
    iget-object v0, p0, Laju;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->f(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 485
    invoke-virtual {v0}, Landroid/location/Address;->getMaxAddressLineIndex()I

    move-result v2

    .line 486
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 487
    const/4 v1, 0x0

    :goto_0
    if-gt v1, v2, :cond_2

    .line 488
    invoke-virtual {v0, v1}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v4

    .line 490
    if-eqz v1, :cond_1

    .line 494
    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 496
    :cond_1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 487
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 499
    :cond_2
    const v1, 0x7f0e00eb

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 500
    const v2, 0x7f0e00ec

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 501
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lalj;->b(Ljava/lang/String;)V

    .line 503
    invoke-virtual {v0}, Landroid/location/Address;->getFeatureName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 504
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 505
    return-object p2
.end method

.method public notifyDataSetChanged()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 510
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 512
    iget-object v0, p0, Laju;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->f(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laju;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->f(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 513
    :cond_0
    iget-object v0, p0, Laju;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->g(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 524
    :cond_1
    :goto_0
    return-void

    .line 515
    :cond_2
    iget-object v0, p0, Laju;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->f(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 516
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MULTIPOINT Adapter changed : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Laju;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->f(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 517
    iget-object v0, p0, Laju;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->h(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 518
    iget-object v0, p0, Laju;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->g(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 519
    iget-object v0, p0, Laju;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Z)Z

    goto :goto_0

    .line 521
    :cond_3
    iget-object v0, p0, Laju;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->g(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0
.end method
