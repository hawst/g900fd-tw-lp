.class public Laic;
.super Landroid/os/Handler;
.source "DisasterDashboardActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V
    .locals 0

    .prologue
    .line 1286
    iput-object p1, p0, Laic;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Lagz;)V
    .locals 0

    .prologue
    .line 1286
    invoke-direct {p0, p1}, Laic;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11

    .prologue
    const/high16 v10, 0x3f000000    # 0.5f

    .line 1289
    iget-object v0, p0, Laic;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->e(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Lvs;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1290
    iget-object v0, p0, Laic;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iget-object v1, p0, Laic;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->f(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Lvs;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Lvs;)Lvs;

    .line 1292
    :cond_0
    iget-object v0, p0, Laic;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->e(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Lvs;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1323
    :cond_1
    :goto_0
    return-void

    .line 1296
    :cond_2
    iget-object v0, p0, Laic;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->g(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Laks;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1300
    iget-object v0, p0, Laic;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->g(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Laks;

    move-result-object v0

    iget-boolean v0, v0, Laks;->b:Z

    if-nez v0, :cond_1

    .line 1301
    iget-object v0, p0, Laic;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 1304
    iget-object v1, p0, Laic;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->g(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Laks;

    move-result-object v1

    iget-object v2, p0, Laic;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->i(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/content/Context;

    move-result-object v2

    iget v3, v0, Laif;->n:I

    invoke-virtual {v1, v2, v3}, Laks;->b(Landroid/content/Context;I)V

    .line 1306
    iget-object v1, p0, Laic;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->g(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Laks;

    move-result-object v1

    iget v1, v1, Laks;->a:I

    const/4 v2, 0x3

    if-lt v1, v2, :cond_3

    .line 1307
    iget-object v1, p0, Laic;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v1, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)V

    .line 1308
    iget-object v1, v0, Laif;->l:Lamf;

    if-eqz v1, :cond_3

    .line 1309
    iget-object v1, p0, Laic;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->g(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Laks;

    move-result-object v1

    iget-object v2, p0, Laic;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->e(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Lvs;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v3}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v5, v0, Laif;->l:Lamf;

    iget-wide v6, v5, Lamf;->a:D

    iget-object v0, v0, Laif;->l:Lamf;

    iget-wide v8, v0, Lamf;->b:D

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v3, v4}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v0

    invoke-virtual {v0, v10, v10}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v0

    iget-object v3, p0, Laic;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v3}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->g(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Laks;

    move-result-object v3

    iget-object v4, p0, Laic;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v4}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->i(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f020060

    invoke-virtual {v3, v4, v5}, Laks;->a(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v3}, Lzx;->a(Landroid/graphics/Bitmap;)Lzw;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lzw;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v0

    invoke-virtual {v2, v0}, Lvs;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Laah;

    move-result-object v0

    iput-object v0, v1, Laks;->c:Laah;

    .line 1321
    :cond_3
    iget-object v0, p0, Laic;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->j(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Laic;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->what:I

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Laic;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0
.end method
