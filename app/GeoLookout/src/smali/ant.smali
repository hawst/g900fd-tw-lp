.class Lant;
.super Lcom/loopj/android/http/AsyncHttpResponseHandler;
.source "DisasterServerInterface.java"


# instance fields
.field final synthetic a:I

.field final synthetic b:Lanh;


# direct methods
.method constructor <init>(Lanh;I)V
    .locals 0

    .prologue
    .line 444
    iput-object p1, p0, Lant;->b:Lanh;

    iput p2, p0, Lant;->a:I

    invoke-direct {p0}, Lcom/loopj/android/http/AsyncHttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 457
    invoke-super {p0, p1, p2, p3, p4}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onFailure(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V

    .line 458
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "refreshDisasterData: onFailure(statusCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 459
    iget-object v0, p0, Lant;->b:Lanh;

    invoke-static {v0}, Lanh;->d(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 460
    iget-object v0, p0, Lant;->b:Lanh;

    invoke-static {v0}, Lanh;->d(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 461
    iget-object v0, p0, Lant;->b:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Lant;->b:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lant;->b:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lage;->a(Landroid/content/Context;I)V

    .line 464
    :cond_0
    return-void
.end method

.method public onFinish()V
    .locals 1

    .prologue
    .line 468
    const-string v0, "refreshDisasterData: onFinish()  "

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 469
    return-void
.end method

.method public onRetry()V
    .locals 1

    .prologue
    .line 473
    invoke-super {p0}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onRetry()V

    .line 474
    const-string v0, "refreshDisasterData: onRetry"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 475
    return-void
.end method

.method public onSuccess(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 448
    invoke-super {p0, p1}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onSuccess(Ljava/lang/String;)V

    .line 449
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "refreshDisasterData: responseHandler : onSuccess "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 450
    iget-object v0, p0, Lant;->b:Lanh;

    invoke-static {v0}, Lanh;->d(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 451
    iget-object v0, p0, Lant;->b:Lanh;

    invoke-static {v0}, Lanh;->d(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 452
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    iget-object v1, p0, Lant;->b:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lant;->a:I

    invoke-virtual {v0, v1, p1, v2}, Lalu;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 453
    return-void
.end method
