.class public Lalb;
.super Ljava/lang/Object;
.source "EmergencyDialog.java"


# instance fields
.field a:Landroid/content/BroadcastReceiver;

.field b:Landroid/content/BroadcastReceiver;

.field private c:Landroid/content/Context;

.field private d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

.field private e:Landroid/app/AlertDialog;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284
    new-instance v0, Lalh;

    invoke-direct {v0, p0}, Lalh;-><init>(Lalb;)V

    iput-object v0, p0, Lalb;->a:Landroid/content/BroadcastReceiver;

    .line 311
    new-instance v0, Lali;

    invoke-direct {v0, p0}, Lali;-><init>(Lalb;)V

    iput-object v0, p0, Lalb;->b:Landroid/content/BroadcastReceiver;

    .line 41
    iput-object p1, p0, Lalb;->c:Landroid/content/Context;

    .line 42
    return-void
.end method

.method static synthetic a(Lalb;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lalb;->c:Landroid/content/Context;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 10

    .prologue
    const v5, 0x7f0a0562

    const v9, 0x7f0800bc

    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 184
    const v0, 0x7f0e0074

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lalb;->f:Landroid/widget/TextView;

    .line 185
    const v0, 0x7f0e0075

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lalb;->g:Landroid/widget/TextView;

    .line 186
    const v0, 0x7f0e0076

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lalb;->h:Landroid/widget/TextView;

    .line 187
    const v0, 0x7f0e0077

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lalb;->i:Landroid/widget/TextView;

    .line 189
    iget-object v0, p0, Lalb;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-static {v1}, Lall;->b(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v0, p0, Lalb;->c:Landroid/content/Context;

    iget-object v1, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getStartTime()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lall;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 192
    iget-object v1, p0, Lalb;->i:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0044

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    iget-object v0, p0, Lalb;->c:Landroid/content/Context;

    iget-object v1, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getEndTime()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lall;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 195
    iget-object v1, p0, Lalb;->h:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0040

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v0, p0, Lalb;->c:Landroid/content/Context;

    invoke-static {v0}, Lanb;->c(Landroid/content/Context;)Lamf;

    move-result-object v0

    .line 198
    iget-object v1, p0, Lalb;->c:Landroid/content/Context;

    iget-object v2, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-static {v1, v2, v0, v7}, Lall;->a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;Lamf;Z)I

    move-result v0

    .line 203
    iget-object v1, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v1

    .line 204
    sparse-switch v1, :sswitch_data_0

    .line 270
    iget-object v0, p0, Lalb;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 273
    :goto_0
    const-string v0, "03"

    iget-object v1, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lalb;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 275
    iget-object v0, p0, Lalb;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 278
    :cond_0
    iget-object v0, p0, Lalb;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 279
    iget-object v0, p0, Lalb;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 280
    iget-object v0, p0, Lalb;->h:Landroid/widget/TextView;

    iget-object v1, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 281
    iget-object v0, p0, Lalb;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 282
    return-void

    .line 206
    :sswitch_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 208
    const-string v2, "03"

    iget-object v3, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 210
    iget-object v0, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a00b3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 211
    const-string v0, ":"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 212
    iget-object v0, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsZoneCity()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 213
    const-string v0, ", "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 214
    iget-object v0, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a00bb

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsLevel()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 216
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 217
    iget-object v2, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00bd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 218
    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 219
    iget-object v2, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getCity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 220
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 221
    iget-object v2, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 222
    const-string v2, " %.1f"

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v4}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailsMagnitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 223
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 224
    iget-object v2, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00ba

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v4}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsDepth()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 225
    iget-object v2, p0, Lalb;->g:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    :goto_1
    iget-object v0, p0, Lalb;->f:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    iget-object v0, p0, Lalb;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 241
    iget-object v0, p0, Lalb;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 228
    :cond_1
    iget-object v2, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 229
    const-string v2, " %.1f"

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v4}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailsMagnitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 230
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 232
    iget-object v2, p0, Lalb;->c:Landroid/content/Context;

    invoke-static {v2, v0}, Lall;->e(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 249
    :sswitch_1
    iget-object v1, p0, Lalb;->f:Landroid/widget/TextView;

    iget-object v2, p0, Lalb;->c:Landroid/content/Context;

    invoke-static {v2, v0}, Lall;->e(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 254
    iget-object v0, p0, Lalb;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 255
    iget-object v0, p0, Lalb;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 259
    :sswitch_2
    iget-object v0, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailSpeed()Ljava/lang/String;

    move-result-object v0

    .line 260
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 261
    const-string v0, "0"

    .line 263
    :cond_2
    iget-object v1, p0, Lalb;->c:Landroid/content/Context;

    invoke-static {v1, v0}, Lall;->i(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 264
    iget-object v1, p0, Lalb;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    iget-object v0, p0, Lalb;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 266
    iget-object v0, p0, Lalb;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 204
    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_1
        0x69 -> :sswitch_0
        0x6e -> :sswitch_2
        0x70 -> :sswitch_2
    .end sparse-switch
.end method

.method static synthetic b(Lalb;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lalb;->e:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic c(Lalb;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    return-object v0
.end method

.method static synthetic d(Lalb;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lalb;->f:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Lalb;
    .locals 10

    .prologue
    const v9, 0x7f0b0008

    const v8, 0x7f0a004c

    const v7, 0x7f0a004b

    const/4 v1, 0x0

    .line 47
    if-nez p1, :cond_0

    move-object p0, v1

    .line 162
    :goto_0
    return-object p0

    .line 50
    :cond_0
    iput-object p1, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    .line 52
    iget-object v0, p0, Lalb;->e:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 53
    iget-object v0, p0, Lalb;->e:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 56
    :cond_1
    iget-object v0, p0, Lalb;->c:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 57
    const v2, 0x7f030016

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 58
    const v3, 0x7f030017

    invoke-virtual {v0, v3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 59
    iget-object v0, p0, Lalb;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v4, p0, Lalb;->a:Landroid/content/BroadcastReceiver;

    new-instance v5, Landroid/content/IntentFilter;

    const-string v6, "com.sec.android.GeoLookout.ACTION_UPDATE_CURRENT_LOCATION"

    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4, v5}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 60
    new-instance v0, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v0, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 62
    iget-object v4, p0, Lalb;->c:Landroid/content/Context;

    iget-object v5, p0, Lalb;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 63
    invoke-direct {p0, v2}, Lalb;->a(Landroid/view/View;)V

    .line 65
    const v0, 0x7f0e0078

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 66
    iget-object v4, p0, Lalb;->c:Landroid/content/Context;

    iget-object v5, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v5}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v5

    iget-object v6, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v6

    invoke-static {v4, v5, v6}, Lall;->b(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    const v0, 0x7f0e0079

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v4, Lalc;

    invoke-direct {v4, p0}, Lalc;-><init>(Lalb;)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v4, p0, Lalb;->c:Landroid/content/Context;

    const v5, 0x1030134

    invoke-direct {v0, v4, v5}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lalb;->c:Landroid/content/Context;

    .line 81
    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lalb;->c:Landroid/content/Context;

    invoke-direct {v4, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 82
    const v0, 0x7f0e0074

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 83
    iget-object v5, p0, Lalb;->c:Landroid/content/Context;

    const v6, 0x7f0b000d

    invoke-virtual {v0, v5, v6}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 84
    const v0, 0x7f0e0075

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 85
    iget-object v5, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v0, v5, v9}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 86
    const v0, 0x7f0e0076

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 87
    iget-object v5, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v0, v5, v9}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 88
    const v0, 0x7f0e0077

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 89
    iget-object v5, p0, Lalb;->c:Landroid/content/Context;

    const v6, 0x7f0b000c

    invoke-virtual {v0, v5, v6}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 92
    const-string v0, "03"

    iget-object v5, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v5}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 93
    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, p0, Lalb;->c:Landroid/content/Context;

    iget-object v5, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v5}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v5

    invoke-static {v2, v5}, Lall;->h(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v5

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v6

    invoke-static {v2, v5, v6}, Lall;->b(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v2, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Lale;

    invoke-direct {v3, p0, p1}, Lale;-><init>(Lalb;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v2, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Lald;

    invoke-direct {v3, p0}, Lald;-><init>(Lalb;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 130
    :goto_1
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lalb;->e:Landroid/app/AlertDialog;

    .line 132
    iget-object v0, p0, Lalb;->e:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 137
    iget-object v0, p0, Lalb;->c:Landroid/content/Context;

    invoke-static {v0}, Lall;->F(Landroid/content/Context;)Z

    move-result v0

    .line 139
    if-eqz v0, :cond_3

    .line 140
    const/high16 v0, 0x400000

    invoke-virtual {v2, v0}, Landroid/view/Window;->addFlags(I)V

    .line 145
    :goto_2
    iget-object v0, p0, Lalb;->c:Landroid/content/Context;

    const-string v3, "keyguard"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 146
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 147
    const/16 v0, 0x7d9

    invoke-virtual {v2, v0}, Landroid/view/Window;->setType(I)V

    .line 151
    :goto_3
    invoke-virtual {v2, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 156
    iget-object v0, p0, Lalb;->e:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 111
    :cond_2
    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, p0, Lalb;->c:Landroid/content/Context;

    iget-object v3, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v3

    invoke-static {v2, v3}, Lall;->h(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v2, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Lalg;

    invoke-direct {v3, p0, p1}, Lalg;-><init>(Lalb;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, p0, Lalb;->c:Landroid/content/Context;

    invoke-virtual {v2, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Lalf;

    invoke-direct {v3, p0}, Lalf;-><init>(Lalb;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 142
    :cond_3
    const/high16 v0, 0x680000

    invoke-virtual {v2, v0}, Landroid/view/Window;->addFlags(I)V

    goto :goto_2

    .line 149
    :cond_4
    const/16 v0, 0x7d8

    invoke-virtual {v2, v0}, Landroid/view/Window;->setType(I)V

    goto :goto_3
.end method

.method public a()V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lalb;->e:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lalb;->e:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 168
    iget-object v0, p0, Lalb;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lalb;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 170
    :try_start_0
    iget-object v0, p0, Lalb;->c:Landroid/content/Context;

    iget-object v1, p0, Lalb;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 171
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lalb;->d:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    invoke-virtual {p0}, Lalb;->a()V

    .line 180
    :cond_0
    return-void
.end method
