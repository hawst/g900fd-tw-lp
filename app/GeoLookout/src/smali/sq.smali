.class public Lsq;
.super Lmz;

# interfaces
.implements Lca;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmz",
        "<",
        "Lsn;",
        ">;",
        "Lca;"
    }
.end annotation


# instance fields
.field private f:Ladc;

.field private g:Lcom/google/android/gms/internal/fn;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/fn;Lcb;Lcc;)V
    .locals 1

    invoke-virtual {p2}, Lcom/google/android/gms/internal/fn;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p3, p4, v0}, Lmz;-><init>(Landroid/content/Context;Lcb;Lcc;[Ljava/lang/String;)V

    iput-object p2, p0, Lsq;->g:Lcom/google/android/gms/internal/fn;

    return-void
.end method


# virtual methods
.method protected a(Landroid/os/IBinder;)Lsn;
    .locals 1

    invoke-static {p1}, Lso;->a(Landroid/os/IBinder;)Lsn;

    move-result-object v0

    return-object v0
.end method

.method protected a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "loaded_person"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "loaded_person"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/fv;->a([B)Lcom/google/android/gms/internal/fv;

    move-result-object v0

    iput-object v0, p0, Lsq;->f:Ladc;

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lmz;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Lack;)V
    .locals 3

    invoke-virtual {p0}, Lsq;->n()V

    invoke-virtual {p0}, Lsq;->p()V

    new-instance v1, Lsv;

    invoke-direct {v1, p0, p1}, Lsv;-><init>(Lsq;Lack;)V

    :try_start_0
    invoke-virtual {p0}, Lsq;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lsn;

    invoke-interface {v0, v1}, Lsn;->b(Lse;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lsv;->a(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(Lacl;)V
    .locals 7

    const/4 v3, 0x0

    const/16 v2, 0x14

    const-string v6, "me"

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lsq;->a(Lacl;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lacl;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const/4 v7, 0x0

    invoke-virtual {p0}, Lsq;->n()V

    if-eqz p1, :cond_0

    new-instance v1, Lsr;

    invoke-direct {v1, p0, p1}, Lsr;-><init>(Lsq;Lacl;)V

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lsq;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lsn;

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lsn;->a(Lse;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    move-object v1, v7

    goto :goto_0

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/google/android/gms/common/data/d;->b(I)Lcom/google/android/gms/common/data/d;

    move-result-object v0

    invoke-virtual {v1, v0, v7, v7}, Lsr;->a(Lcom/google/android/gms/common/data/d;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Lacm;ILjava/lang/String;)V
    .locals 6

    invoke-virtual {p0}, Lsq;->n()V

    new-instance v1, Lst;

    invoke-direct {v1, p0, p1}, Lst;-><init>(Lsq;Lacm;)V

    :try_start_0
    invoke-virtual {p0}, Lsq;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lsn;

    const/4 v2, 0x1

    const/4 v4, -0x1

    move v3, p2

    move-object v5, p3

    invoke-interface/range {v0 .. v5}, Lsn;->a(Lse;IIILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/google/android/gms/common/data/d;->b(I)Lcom/google/android/gms/common/data/d;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lst;->a(Lcom/google/android/gms/common/data/d;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lacm;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lsq;->a(Lacm;ILjava/lang/String;)V

    return-void
.end method

.method public a(Lacm;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lacm;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lsq;->n()V

    new-instance v1, Lst;

    invoke-direct {v1, p0, p1}, Lst;-><init>(Lsq;Lacm;)V

    :try_start_0
    invoke-virtual {p0}, Lsq;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lsn;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, v1, v2}, Lsn;->a(Lse;Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/google/android/gms/common/data/d;->b(I)Lcom/google/android/gms/common/data/d;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lst;->a(Lcom/google/android/gms/common/data/d;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lacm;[Ljava/lang/String;)V
    .locals 1

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lsq;->a(Lacm;Ljava/util/Collection;)V

    return-void
.end method

.method public a(Lacz;)V
    .locals 2

    invoke-virtual {p0}, Lsq;->n()V

    :try_start_0
    check-cast p1, Lcom/google/android/gms/internal/fs;

    invoke-static {p1}, Lcom/google/android/gms/internal/ec;->a(Lcom/google/android/gms/internal/dw;)Lcom/google/android/gms/internal/ec;

    move-result-object v1

    invoke-virtual {p0}, Lsq;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lsn;

    invoke-interface {v0, v1}, Lsn;->a(Lcom/google/android/gms/internal/ec;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lsq;->n()V

    :try_start_0
    invoke-virtual {p0}, Lsq;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lsn;

    invoke-interface {v0, p1}, Lsn;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected a(Lno;Lnd;)V
    .locals 8

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p0, Lsq;->g:Lcom/google/android/gms/internal/fn;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fn;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "client_id"

    invoke-virtual {v7, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "request_visible_actions"

    iget-object v1, p0, Lsq;->g:Lcom/google/android/gms/internal/fn;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/fn;->d()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    const v2, 0x3d8024

    iget-object v0, p0, Lsq;->g:Lcom/google/android/gms/internal/fn;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fn;->g()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lsq;->g:Lcom/google/android/gms/internal/fn;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fn;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lsq;->j()[Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lsq;->g:Lcom/google/android/gms/internal/fn;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fn;->b()Ljava/lang/String;

    move-result-object v6

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v7}, Lno;->a(Lnl;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method protected synthetic b(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lsq;->a(Landroid/os/IBinder;)Lsn;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0}, Lsq;->j()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.plus.service.START"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.plus.internal.IPlusService"

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lsq;->n()V

    :try_start_0
    invoke-virtual {p0}, Lsq;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lsn;

    invoke-interface {v0}, Lsn;->a()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public h()Ladc;
    .locals 1

    invoke-virtual {p0}, Lsq;->n()V

    iget-object v0, p0, Lsq;->f:Ladc;

    return-object v0
.end method

.method public p()V
    .locals 2

    invoke-virtual {p0}, Lsq;->n()V

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lsq;->f:Ladc;

    invoke-virtual {p0}, Lsq;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lsn;

    invoke-interface {v0}, Lsn;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
