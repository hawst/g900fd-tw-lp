.class public Lahu;
.super Landroid/content/BroadcastReceiver;
.source "DisasterDashboardActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V
    .locals 0

    .prologue
    .line 2249
    iput-object p1, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, -0x2

    const/4 v8, 0x0

    .line 2252
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 2255
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 2256
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceive action="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 2260
    iget-object v0, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->k(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/widget/TabHost;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v7

    .line 2262
    iget-object v0, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Laif;

    .line 2264
    const-string v0, "com.sec.android.GeoLookout.ACTION_UPDATE_CURRENT_LOCATION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2265
    const-string v0, "location"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/location/Location;

    .line 2266
    const-string v0, "uid"

    invoke-virtual {p2, v0, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 2267
    const-string v0, "dv"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2268
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceive location="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 2270
    iget-object v0, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    new-instance v1, Lahv;

    invoke-direct {v1, p0}, Lahv;-><init>(Lahu;)V

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Ljava/util/TimerTask;)Ljava/util/TimerTask;

    .line 2285
    iget-object v0, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Ljava/util/Timer;)Ljava/util/Timer;

    .line 2287
    if-eqz v3, :cond_7

    .line 2288
    iget-object v0, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2289
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    iget-object v1, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iget-object v2, v1, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->c:Lanx;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lalu;->a(Landroid/content/Context;Lanx;Landroid/location/Location;ILjava/lang/String;)V

    .line 2290
    iget-object v0, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/Timer;

    move-result-object v0

    iget-object v1, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->p(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/TimerTask;

    move-result-object v1

    const-wide/16 v4, 0x7530

    invoke-virtual {v0, v1, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 2292
    :cond_0
    iget-object v0, v6, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    if-nez v0, :cond_2

    .line 2293
    new-instance v0, Lamf;

    invoke-direct {v0, v3}, Lamf;-><init>(Landroid/location/Location;)V

    iput-object v0, v6, Laif;->u:Lamf;

    .line 2294
    if-nez v7, :cond_1

    .line 2295
    iget-object v0, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iget-object v1, v6, Laif;->u:Lamf;

    invoke-static {v0, v1, v8}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Lamf;I)V

    .line 2369
    :cond_1
    :goto_0
    return-void

    .line 2300
    :cond_2
    iget-object v0, v6, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationType()I

    move-result v0

    .line 2302
    if-nez v0, :cond_4

    .line 2303
    new-instance v0, Lamf;

    invoke-direct {v0, v3}, Lamf;-><init>(Landroid/location/Location;)V

    iput-object v0, v6, Laif;->u:Lamf;

    .line 2304
    if-nez v7, :cond_3

    .line 2305
    iget-object v0, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iget-object v1, v6, Laif;->u:Lamf;

    invoke-static {v0, v1, v8}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Lamf;I)V

    .line 2308
    :cond_3
    iget-object v0, v6, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-static {v0}, Lall;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2309
    iget-object v0, v6, Laif;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 2313
    iget-object v0, v6, Laif;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v1, v8}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2328
    :cond_4
    :goto_1
    iget-object v0, v6, Laif;->l:Lamf;

    iget-object v1, v6, Laif;->u:Lamf;

    invoke-static {v0, v1}, Lall;->a(Lamf;Lamf;)I

    move-result v0

    .line 2329
    iget-object v1, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iget v1, v1, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a:I

    if-le v0, v1, :cond_5

    .line 2330
    iget-object v1, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iput v0, v1, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a:I

    .line 2332
    :cond_5
    iget-object v0, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iget-object v1, v6, Laif;->l:Lamf;

    iget-object v2, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iget v2, v2, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Lamf;I)V

    goto :goto_0

    .line 2317
    :cond_6
    iget-object v0, v6, Laif;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 2318
    iget-object v0, v6, Laif;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2319
    iput v9, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2320
    iget-object v1, v6, Laif;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2321
    iget-object v0, v6, Laif;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 2322
    iget-object v0, v6, Laif;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v1, v8}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2335
    :cond_7
    iget-object v0, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/Timer;

    move-result-object v0

    iget-object v1, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->p(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/TimerTask;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 2337
    iget-object v0, v6, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    if-eqz v0, :cond_1

    .line 2341
    iget-object v0, v6, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationType()I

    move-result v0

    .line 2343
    if-nez v0, :cond_8

    .line 2344
    iget-object v0, v6, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-static {v0}, Lall;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2345
    iget-object v0, v6, Laif;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_8

    .line 2349
    iget-object v0, v6, Laif;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v1, v8}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2362
    :cond_8
    :goto_2
    iget-object v0, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    new-instance v1, Lahx;

    invoke-direct {v1, p0}, Lahx;-><init>(Lahu;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 2352
    :cond_9
    iget-object v0, v6, Laif;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_8

    .line 2353
    iget-object v0, v6, Laif;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2354
    iput v9, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2355
    iget-object v1, v6, Laif;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2356
    iget-object v0, v6, Laif;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 2357
    iget-object v0, v6, Laif;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lahu;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v1, v8}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method
