.class public interface abstract Lqq;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a(Lqn;[BLjava/lang/String;Ljava/lang/String;)I
.end method

.method public abstract a([BLjava/lang/String;[Ljava/lang/String;)I
.end method

.method public abstract a(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public abstract a(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract a(I)V
.end method

.method public abstract a(J)V
.end method

.method public abstract a(Landroid/os/IBinder;Landroid/os/Bundle;)V
.end method

.method public abstract a(Ljava/lang/String;I)V
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract a(Lqn;)V
.end method

.method public abstract a(Lqn;IIZZ)V
.end method

.method public abstract a(Lqn;IZZ)V
.end method

.method public abstract a(Lqn;J)V
.end method

.method public abstract a(Lqn;Landroid/os/Bundle;II)V
.end method

.method public abstract a(Lqn;Landroid/os/IBinder;I[Ljava/lang/String;Landroid/os/Bundle;ZJ)V
.end method

.method public abstract a(Lqn;Landroid/os/IBinder;Ljava/lang/String;ZJ)V
.end method

.method public abstract a(Lqn;Ljava/lang/String;)V
.end method

.method public abstract a(Lqn;Ljava/lang/String;IIIZ)V
.end method

.method public abstract a(Lqn;Ljava/lang/String;ILandroid/os/IBinder;Landroid/os/Bundle;)V
.end method

.method public abstract a(Lqn;Ljava/lang/String;IZZ)V
.end method

.method public abstract a(Lqn;Ljava/lang/String;IZZZZ)V
.end method

.method public abstract a(Lqn;Ljava/lang/String;J)V
.end method

.method public abstract a(Lqn;Ljava/lang/String;JLjava/lang/String;)V
.end method

.method public abstract a(Lqn;Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V
.end method

.method public abstract a(Lqn;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lqn;Ljava/lang/String;Ljava/lang/String;IIIZ)V
.end method

.method public abstract a(Lqn;Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract a(Lqn;Ljava/lang/String;Z)V
.end method

.method public abstract a(Lqn;Ljava/lang/String;Z[J)V
.end method

.method public abstract a(Lqn;Z)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()Landroid/os/Bundle;
.end method

.method public abstract b(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract b(J)V
.end method

.method public abstract b(Ljava/lang/String;I)V
.end method

.method public abstract b(Lqn;)V
.end method

.method public abstract b(Lqn;IZZ)V
.end method

.method public abstract b(Lqn;Ljava/lang/String;)V
.end method

.method public abstract b(Lqn;Ljava/lang/String;IIIZ)V
.end method

.method public abstract b(Lqn;Ljava/lang/String;ILandroid/os/IBinder;Landroid/os/Bundle;)V
.end method

.method public abstract b(Lqn;Ljava/lang/String;IZZ)V
.end method

.method public abstract b(Lqn;Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V
.end method

.method public abstract b(Lqn;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract b(Lqn;Ljava/lang/String;Ljava/lang/String;IIIZ)V
.end method

.method public abstract b(Lqn;Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract b(Lqn;Ljava/lang/String;Z)V
.end method

.method public abstract b(Lqn;Z)V
.end method

.method public abstract c()V
.end method

.method public abstract c(Ljava/lang/String;)V
.end method

.method public abstract c(Ljava/lang/String;I)V
.end method

.method public abstract c(Lqn;)V
.end method

.method public abstract c(Lqn;IZZ)V
.end method

.method public abstract c(Lqn;Ljava/lang/String;)V
.end method

.method public abstract c(Lqn;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract c(Lqn;Ljava/lang/String;Z)V
.end method

.method public abstract c(Lqn;Z)V
.end method

.method public abstract d(Ljava/lang/String;)I
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public abstract d(Lqn;)V
.end method

.method public abstract d(Lqn;IZZ)V
.end method

.method public abstract d(Lqn;Ljava/lang/String;)V
.end method

.method public abstract d(Lqn;Ljava/lang/String;Z)V
.end method

.method public abstract e(Ljava/lang/String;)Landroid/net/Uri;
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public abstract e(Lqn;)V
.end method

.method public abstract e(Lqn;IZZ)V
.end method

.method public abstract e(Lqn;Ljava/lang/String;)V
.end method

.method public abstract f()Lcom/google/android/gms/common/data/d;
.end method

.method public abstract f(Lqn;)V
.end method

.method public abstract f(Lqn;Ljava/lang/String;)V
.end method

.method public abstract g(Lqn;)V
.end method

.method public abstract g(Lqn;Ljava/lang/String;)V
.end method

.method public abstract g()Z
.end method

.method public abstract h()Lcom/google/android/gms/common/data/d;
.end method

.method public abstract h(Lqn;)V
.end method

.method public abstract h(Lqn;Ljava/lang/String;)V
.end method

.method public abstract i(Lqn;)V
.end method

.method public abstract i(Lqn;Ljava/lang/String;)V
.end method

.method public abstract j(Lqn;Ljava/lang/String;)I
.end method

.method public abstract k(Lqn;Ljava/lang/String;)V
.end method

.method public abstract l(Lqn;Ljava/lang/String;)V
.end method

.method public abstract m(Lqn;Ljava/lang/String;)V
.end method
