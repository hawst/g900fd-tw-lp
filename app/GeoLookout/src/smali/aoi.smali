.class public Laoi;
.super Ljava/lang/Object;
.source "SPP.java"


# static fields
.field private static f:Laoi;


# instance fields
.field protected a:Laou;

.field protected b:Laot;

.field protected c:Z

.field protected d:Z

.field protected e:I

.field private g:Ljava/util/TimerTask;

.field private h:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput-object v0, Laoi;->f:Laoi;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, Laoi;->a:Laou;

    .line 28
    iput-object v0, p0, Laoi;->b:Laot;

    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Laoi;->c:Z

    .line 31
    iput-boolean v1, p0, Laoi;->d:Z

    .line 33
    iput v1, p0, Laoi;->e:I

    return-void
.end method

.method public static a()Laoi;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Laoi;->f:Laoi;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Laoi;

    invoke-direct {v0}, Laoi;-><init>()V

    sput-object v0, Laoi;->f:Laoi;

    .line 41
    :cond_0
    sget-object v0, Laoi;->f:Laoi;

    return-object v0
.end method

.method static synthetic a(Laoi;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Laoi;->e()V

    return-void
.end method

.method private e()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Laoi;->h:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Laoi;->h:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 103
    iget-object v0, p0, Laoi;->h:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 106
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Laoi;->h:Ljava/util/Timer;

    .line 107
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 62
    const-string v0, "registerTimeOutToSPP start"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 64
    new-instance v0, Laoj;

    invoke-direct {v0, p0, p1}, Laoj;-><init>(Laoi;Landroid/content/Context;)V

    iput-object v0, p0, Laoi;->g:Ljava/util/TimerTask;

    .line 74
    iget-object v0, p0, Laoi;->h:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 75
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Laoi;->h:Ljava/util/Timer;

    .line 76
    iget-object v0, p0, Laoi;->h:Ljava/util/Timer;

    iget-object v1, p0, Laoi;->g:Ljava/util/TimerTask;

    const-wide/32 v2, 0x1adb0

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 78
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 183
    new-instance v0, Laol;

    invoke-direct {v0, p0, p1}, Laol;-><init>(Laoi;Landroid/content/Context;)V

    .line 231
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/content/Intent;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 232
    return-void
.end method

.method public a(Laou;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Laoi;->a:Laou;

    if-eqz v0, :cond_0

    .line 56
    const-string v0, "mResultReceiver is not null"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 58
    :cond_0
    iput-object p1, p0, Laoi;->a:Laou;

    .line 59
    return-void
.end method

.method public b()Laou;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Laoi;->a:Laou;

    return-object v0
.end method

.method public b(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 81
    const-string v0, "unRegisterTimeOutToSPP start"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 83
    new-instance v0, Laok;

    invoke-direct {v0, p0, p1}, Laok;-><init>(Laoi;Landroid/content/Context;)V

    iput-object v0, p0, Laoi;->g:Ljava/util/TimerTask;

    .line 94
    iget-object v0, p0, Laoi;->h:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 95
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Laoi;->h:Ljava/util/Timer;

    .line 96
    iget-object v0, p0, Laoi;->h:Ljava/util/Timer;

    iget-object v1, p0, Laoi;->g:Ljava/util/TimerTask;

    const-wide/32 v2, 0x1adb0

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 98
    :cond_0
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Laoi;->e:I

    return v0
.end method

.method public c(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 113
    iget-boolean v0, p0, Laoi;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Laoi;->c:Z

    if-eqz v0, :cond_0

    .line 130
    :goto_0
    return-void

    .line 118
    :cond_0
    :try_start_0
    const-string v0, "Register SPP server"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 119
    invoke-static {p1}, Laof;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 120
    const/4 v1, 0x1

    iput-boolean v1, p0, Laoi;->c:Z

    .line 121
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.spp.action.SPP_REQUEST"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 122
    const-string v2, "reqType"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 123
    const-string v2, "appId"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 124
    const-string v0, "userdata"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 125
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 126
    const/4 v0, 0x1

    iput-boolean v0, p0, Laoi;->d:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 127
    :catch_0
    move-exception v0

    .line 128
    invoke-static {v0}, Lalj;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public d(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 136
    iget-boolean v0, p0, Laoi;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Laoi;->c:Z

    if-nez v0, :cond_0

    .line 153
    :goto_0
    return-void

    .line 141
    :cond_0
    :try_start_0
    const-string v0, "Unregister SPP server"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 142
    invoke-static {p1}, Laof;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 143
    const/4 v1, 0x0

    iput-boolean v1, p0, Laoi;->c:Z

    .line 144
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.spp.action.SPP_REQUEST"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 145
    const-string v2, "reqType"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 146
    const-string v2, "appId"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 147
    const-string v0, "userdata"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Laoi;->d:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 150
    :catch_0
    move-exception v0

    .line 151
    invoke-static {v0}, Lalj;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public d()Z
    .locals 3

    .prologue
    .line 245
    iget v0, p0, Laoi;->e:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 247
    const/16 v0, 0x1388

    .line 248
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Wait for a while... :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->c(Ljava/lang/String;)V

    .line 249
    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    :goto_0
    const/4 v0, 0x1

    .line 255
    :goto_1
    return v0

    .line 250
    :catch_0
    move-exception v0

    .line 251
    invoke-static {v0}, Lalj;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 255
    :cond_0
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public e(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 159
    iget-boolean v0, p0, Laoi;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Laoi;->c:Z

    if-nez v0, :cond_0

    .line 176
    :goto_0
    return-void

    .line 164
    :cond_0
    :try_start_0
    const-string v0, "retry - Unregister SPP server"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 165
    invoke-static {p1}, Laof;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 166
    const/4 v1, 0x0

    iput-boolean v1, p0, Laoi;->c:Z

    .line 167
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.spp.action.SPP_REQUEST"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 168
    const-string v2, "reqType"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 169
    const-string v2, "appId"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 170
    const-string v0, "userdata"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 171
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 172
    const/4 v0, 0x1

    iput-boolean v0, p0, Laoi;->d:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 173
    :catch_0
    move-exception v0

    .line 174
    invoke-static {v0}, Lalj;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public f(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 260
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 261
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.spp.action.SPP_REQUEST"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 263
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 265
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isSPPAvailable : receiver application count = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 267
    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
