.class public final Lkd;
.super Ljava/lang/Object;


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/gms/internal/bu;Lke;)Lla;
    .locals 1

    iget-object v0, p1, Lcom/google/android/gms/internal/bu;->m:Lcom/google/android/gms/internal/co;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/co;->f:Z

    if-eqz v0, :cond_0

    invoke-static {p0, p1, p2}, Lkd;->b(Landroid/content/Context;Lcom/google/android/gms/internal/bu;Lke;)Lla;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1, p2}, Lkd;->c(Landroid/content/Context;Lcom/google/android/gms/internal/bu;Lke;)Lla;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;Lcom/google/android/gms/internal/bu;Lke;)Lla;
    .locals 1

    const-string v0, "Fetching ad response from local ad request service."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    new-instance v0, Lkg;

    invoke-direct {v0, p0, p1, p2}, Lkg;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/bu;Lke;)V

    invoke-virtual {v0}, Lkg;->e()V

    return-object v0
.end method

.method private static c(Landroid/content/Context;Lcom/google/android/gms/internal/bu;Lke;)Lla;
    .locals 1

    const-string v0, "Fetching ad response from remote ad request service."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcf;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Failed to connect to remote ad request service."

    invoke-static {v0}, Llm;->e(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lkh;

    invoke-direct {v0, p0, p1, p2}, Lkh;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/bu;Lke;)V

    goto :goto_0
.end method
