.class final Lae;
.super Ljava/lang/Object;

# interfaces
.implements Lah;


# instance fields
.field private final a:Lad;

.field private final b:Lr;


# direct methods
.method public constructor <init>(Lad;Lr;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lae;->a:Lad;

    iput-object p2, p0, Lae;->b:Lr;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    const-string v0, "Custom event adapter called onFailedToReceiveAd."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lae;->b:Lr;

    iget-object v1, p0, Lae;->a:Lad;

    sget-object v2, Lk;->b:Lk;

    invoke-interface {v0, v1, v2}, Lr;->a(Lq;Lk;)V

    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    const-string v0, "Custom event adapter called onReceivedAd."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lae;->a:Lad;

    invoke-static {v0, p1}, Lad;->a(Lad;Landroid/view/View;)V

    iget-object v0, p0, Lae;->b:Lr;

    iget-object v1, p0, Lae;->a:Lad;

    invoke-interface {v0, v1}, Lr;->a(Lq;)V

    return-void
.end method

.method public b()V
    .locals 2

    const-string v0, "Custom event adapter called onFailedToReceiveAd."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lae;->b:Lr;

    iget-object v1, p0, Lae;->a:Lad;

    invoke-interface {v0, v1}, Lr;->e(Lq;)V

    return-void
.end method

.method public c()V
    .locals 2

    const-string v0, "Custom event adapter called onFailedToReceiveAd."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lae;->b:Lr;

    iget-object v1, p0, Lae;->a:Lad;

    invoke-interface {v0, v1}, Lr;->b(Lq;)V

    return-void
.end method

.method public d()V
    .locals 2

    const-string v0, "Custom event adapter called onFailedToReceiveAd."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lae;->b:Lr;

    iget-object v1, p0, Lae;->a:Lad;

    invoke-interface {v0, v1}, Lr;->c(Lq;)V

    return-void
.end method

.method public e()V
    .locals 2

    const-string v0, "Custom event adapter called onFailedToReceiveAd."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lae;->b:Lr;

    iget-object v1, p0, Lae;->a:Lad;

    invoke-interface {v0, v1}, Lr;->d(Lq;)V

    return-void
.end method
