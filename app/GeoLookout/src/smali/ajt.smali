.class public Lajt;
.super Landroid/os/AsyncTask;
.source "DisasterMultiPointActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Landroid/location/Address;",
        ">;>;"
    }
.end annotation


# instance fields
.field a:Landroid/content/Context;

.field final synthetic b:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Lajt;->b:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    .line 351
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 352
    iput-object p2, p0, Lajt;->a:Landroid/content/Context;

    .line 353
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 357
    new-instance v1, Landroid/location/Geocoder;

    iget-object v2, p0, Lajt;->a:Landroid/content/Context;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    .line 359
    const/4 v2, 0x0

    aget-object v2, p1, v2

    .line 361
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MULTIPOINT search "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lalj;->b(Ljava/lang/String;)V

    .line 367
    const/16 v3, 0x14

    :try_start_0
    invoke-virtual {v1, v2, v3}, Landroid/location/Geocoder;->getFromLocationName(Ljava/lang/String;I)Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 377
    :goto_0
    return-object v0

    .line 368
    :catch_0
    move-exception v1

    .line 369
    const-string v2, "LocationSampleActivity IO Exception in getFromLocation()"

    invoke-static {v2}, Lalj;->e(Ljava/lang/String;)V

    .line 370
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 372
    :catch_1
    move-exception v1

    .line 373
    const-string v2, "LocationSampleActivity"

    invoke-static {v2}, Lalj;->e(Ljava/lang/String;)V

    .line 374
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method protected a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 383
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MULTIPOINT addresses "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 384
    iget-object v0, p0, Lajt;->b:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Ljava/util/List;)Ljava/util/List;

    .line 385
    iget-object v0, p0, Lajt;->b:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->b(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Laju;

    move-result-object v0

    invoke-virtual {v0}, Laju;->notifyDataSetChanged()V

    .line 386
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 347
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lajt;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 347
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lajt;->a(Ljava/util/List;)V

    return-void
.end method
