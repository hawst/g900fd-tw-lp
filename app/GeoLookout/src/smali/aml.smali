.class Laml;
.super Ljava/lang/Object;
.source "LifemodeCity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lamj;

.field private final b:Ljava/text/Collator;


# direct methods
.method constructor <init>(Lamj;)V
    .locals 1

    .prologue
    .line 56
    iput-object p1, p0, Laml;->a:Lamj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Laml;->b:Ljava/text/Collator;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Integer;Ljava/lang/Integer;)I
    .locals 4

    .prologue
    .line 62
    sget-object v0, Lami;->b:Landroid/util/SparseArray;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamq;

    .line 63
    sget-object v1, Lami;->b:Landroid/util/SparseArray;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lamq;

    .line 64
    iget-object v2, p0, Laml;->b:Ljava/text/Collator;

    iget-object v3, p0, Laml;->a:Lamj;

    invoke-static {v3}, Lamj;->b(Lamj;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v3}, Lamq;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Laml;->a:Lamj;

    invoke-static {v3}, Lamj;->b(Lamj;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1, v3}, Lamq;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 56
    check-cast p1, Ljava/lang/Integer;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2}, Laml;->a(Ljava/lang/Integer;Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method
