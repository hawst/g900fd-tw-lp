.class public final Lago;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final ScrollView:I = 0x7f0e007a

.field public static final assgin_contact_button:I = 0x7f0e0043

.field public static final call_family_icon_img_imageview:I = 0x7f0e00cc

.field public static final call_family_icon_layout:I = 0x7f0e00cb

.field public static final checkbox:I = 0x7f0e007c

.field public static final contact_image:I = 0x7f0e0046

.field public static final contact_layout:I = 0x7f0e0044

.field public static final contact_minus:I = 0x7f0e0045

.field public static final contact_text:I = 0x7f0e0047

.field public static final content_frame:I = 0x7f0e0071

.field public static final custom_infowindow_storm_direction:I = 0x7f0e0002

.field public static final custom_infowindow_storm_maximum_wind:I = 0x7f0e0000

.field public static final custom_infowindow_storm_speed:I = 0x7f0e0001

.field public static final dashboard_description:I = 0x7f0e0014

.field public static final dashboard_description_disable_scrollview:I = 0x7f0e001a

.field public static final description:I = 0x7f0e0015

.field public static final description_details:I = 0x7f0e0016

.field public static final description_disable_scrollview_details:I = 0x7f0e001b

.field public static final description_disable_scrollview_effective:I = 0x7f0e001d

.field public static final description_disable_scrollview_location:I = 0x7f0e001c

.field public static final description_disable_scrollview_time:I = 0x7f0e001e

.field public static final description_effective:I = 0x7f0e0018

.field public static final description_location:I = 0x7f0e0017

.field public static final description_time:I = 0x7f0e0019

.field public static final direction_arrow:I = 0x7f0e0003

.field public static final disaster_dashboard_layout:I = 0x7f0e0004

.field public static final disaster_filter_message_view:I = 0x7f0e0039

.field public static final disaster_history_layout:I = 0x7f0e000f

.field public static final disaster_map_icon_img_imageview:I = 0x7f0e00c8

.field public static final disaster_map_icon_layout:I = 0x7f0e00c7

.field public static final disaster_map_layout:I = 0x7f0e0006

.field public static final disaster_refresh:I = 0x7f0e0062

.field public static final disaster_refresh_animation:I = 0x7f0e0063

.field public static final disaster_refresh_area:I = 0x7f0e005f

.field public static final disaster_safe_layout:I = 0x7f0e000e

.field public static final disaster_share_layout:I = 0x7f0e0005

.field public static final disaster_source:I = 0x7f0e003d

.field public static final disaster_tab_share:I = 0x7f0e0009

.field public static final disclaimer_linearlayout:I = 0x7f0e00ef

.field public static final do_not_show_message:I = 0x7f0e0084

.field public static final do_not_show_message_text:I = 0x7f0e0085

.field public static final drawer_layout:I = 0x7f0e0070

.field public static final emergency_dialog:I = 0x7f0e0073

.field public static final emergency_icon:I = 0x7f0e002a

.field public static final geonews_menu:I = 0x7f0e0101

.field public static final history_description_layout:I = 0x7f0e0012

.field public static final history_help:I = 0x7f0e0119

.field public static final history_icon:I = 0x7f0e0027

.field public static final history_list:I = 0x7f0e007d

.field public static final history_listview:I = 0x7f0e0011

.field public static final history_map:I = 0x7f0e0010

.field public static final history_share_icon:I = 0x7f0e0118

.field public static final history_title:I = 0x7f0e0028

.field public static final i_am_ok_text:I = 0x7f0e0033

.field public static final i_need_help_text:I = 0x7f0e0031

.field public static final imageView1:I = 0x7f0e0068

.field public static final imageView2:I = 0x7f0e00f5

.field public static final image_view:I = 0x7f0e00f9

.field public static final image_view2:I = 0x7f0e00fa

.field public static final jpn_description_current_contents:I = 0x7f0e0035

.field public static final jpn_description_current_title:I = 0x7f0e0034

.field public static final jpn_description_source_contents:I = 0x7f0e0037

.field public static final jpn_description_source_title:I = 0x7f0e0036

.field public static final jpn_description_time:I = 0x7f0e0038

.field public static final layout_emergency_call:I = 0x7f0e0029

.field public static final layout_history:I = 0x7f0e0026

.field public static final layout_move:I = 0x7f0e00f8

.field public static final layout_msg_button:I = 0x7f0e002f

.field public static final layout_share_via:I = 0x7f0e002c

.field public static final layout_summary:I = 0x7f0e0020

.field public static final layout_tips:I = 0x7f0e0021

.field public static final left_drawer:I = 0x7f0e0072

.field public static final lifemode_actionbar_changemode_menu_layout:I = 0x7f0e0083

.field public static final lifemode_actionbar_main_layout:I = 0x7f0e0082

.field public static final lifemode_chilblain_index_info:I = 0x7f0e0098

.field public static final lifemode_cocktailbar_app_icon:I = 0x7f0e008a

.field public static final lifemode_cocktailbar_info_linearLayout:I = 0x7f0e0090

.field public static final lifemode_cocktailbar_info_location_textview:I = 0x7f0e0091

.field public static final lifemode_cocktailbar_roaming_textview:I = 0x7f0e008c

.field public static final lifemode_cocktailbar_roaming_textview_layout:I = 0x7f0e008b

.field public static final lifemode_cocktailbar_status_items_linearLayout_fadeout_imageview:I = 0x7f0e008f

.field public static final lifemode_cocktailbar_status_items_list_layout:I = 0x7f0e008e

.field public static final lifemode_cocktailbar_status_items_list_linearLayout:I = 0x7f0e008d

.field public static final lifemode_cocktailbar_status_items_status_img_imageview:I = 0x7f0e0093

.field public static final lifemode_cocktailbar_status_items_status_name_textview:I = 0x7f0e0094

.field public static final lifemode_cocktailbar_status_items_warningnone_bg_color_img_imageview:I = 0x7f0e0092

.field public static final lifemode_flu_index_info:I = 0x7f0e0096

.field public static final lifemode_food_poison_index_info:I = 0x7f0e009d

.field public static final lifemode_freeze_index_info:I = 0x7f0e0099

.field public static final lifemode_heat_index_info:I = 0x7f0e009e

.field public static final lifemode_index_information:I = 0x7f0e0095

.field public static final lifemode_item_listview_layout:I = 0x7f0e00c4

.field public static final lifemode_librarylistview:I = 0x7f0e00c5

.field public static final lifemode_listview_item_row_childview_include_lv4:I = 0x7f0e00a6

.field public static final lifemode_listview_item_row_childview_include_lv5:I = 0x7f0e00a5

.field public static final lifemode_listview_item_row_childview_layout:I = 0x7f0e00a1

.field public static final lifemode_listview_item_row_childview_layout_map_bg_img:I = 0x7f0e00a2

.field public static final lifemode_listview_item_row_childview_layout_status_detailinfo_bottom_text:I = 0x7f0e00a4

.field public static final lifemode_listview_item_row_childview_layout_status_detailinfo_text_layout:I = 0x7f0e00a3

.field public static final lifemode_listview_item_row_childview_layout_temp_bar_level4:I = 0x7f0e00a7

.field public static final lifemode_listview_item_row_childview_layout_temp_bar_level5:I = 0x7f0e00b3

.field public static final lifemode_listview_item_row_layout:I = 0x7f0e00b7

.field public static final lifemode_listview_item_row_layout_status_checkbox_img:I = 0x7f0e00bd

.field public static final lifemode_listview_item_row_layout_status_detail_text:I = 0x7f0e00ba

.field public static final lifemode_listview_item_row_layout_status_img:I = 0x7f0e00b8

.field public static final lifemode_listview_item_row_layout_status_text:I = 0x7f0e00b9

.field public static final lifemode_listview_item_row_layout_status_warningnone_bg_img:I = 0x7f0e00bb

.field public static final lifemode_listview_item_row_layout_status_warningnone_text:I = 0x7f0e00bc

.field public static final lifemode_mainscreen_layout:I = 0x7f0e00be

.field public static final lifemode_mainscreen_status_line_framelayout:I = 0x7f0e00bf

.field public static final lifemode_mainscreen_status_line_location_btn_img:I = 0x7f0e00c0

.field public static final lifemode_mainscreen_status_line_location_textview:I = 0x7f0e00c1

.field public static final lifemode_mainscreen_status_line_refresh_btn_img:I = 0x7f0e00c2

.field public static final lifemode_mainscreen_status_line_time_textview:I = 0x7f0e00c3

.field public static final lifemode_pm10_index_info:I = 0x7f0e009a

.field public static final lifemode_pollen_index_info:I = 0x7f0e0097

.field public static final lifemode_safety_tools_icon_list_layout:I = 0x7f0e00c6

.field public static final lifemode_spoil_index_info:I = 0x7f0e009b

.field public static final lifemode_status_item_map_icon_img_left:I = 0x7f0e0087

.field public static final lifemode_status_item_map_icon_img_right:I = 0x7f0e0089

.field public static final lifemode_status_item_map_icon_layout:I = 0x7f0e0086

.field public static final lifemode_status_item_map_icon_textview:I = 0x7f0e0088

.field public static final lifemode_sweat_index_info:I = 0x7f0e009c

.field public static final lifemode_uv_index_info:I = 0x7f0e009f

.field public static final lifemode_wind_chill_index_info:I = 0x7f0e00a0

.field public static final linearLayout1:I = 0x7f0e005e

.field public static final lisenceweb:I = 0x7f0e00ed

.field public static final list_effective:I = 0x7f0e0080

.field public static final list_icon:I = 0x7f0e001f

.field public static final list_icon_tip:I = 0x7f0e0024

.field public static final list_location:I = 0x7f0e007f

.field public static final list_state:I = 0x7f0e007e

.field public static final list_time:I = 0x7f0e0081

.field public static final list_title:I = 0x7f0e0013

.field public static final lmwIvArrdown:I = 0x7f0e00e2

.field public static final lmwIvArrdownLayout:I = 0x7f0e00e1

.field public static final lmwIvRefresh:I = 0x7f0e00e5

.field public static final lmwIvTypeIcon1:I = 0x7f0e00d3

.field public static final lmwIvTypeIcon2:I = 0x7f0e00d7

.field public static final lmwIvTypeIcon3:I = 0x7f0e00db

.field public static final lmwIvTypeIcon4:I = 0x7f0e00df

.field public static final lmwLayout1:I = 0x7f0e00d1

.field public static final lmwLayout2:I = 0x7f0e00d5

.field public static final lmwLayout3:I = 0x7f0e00d9

.field public static final lmwLayout4:I = 0x7f0e00dd

.field public static final lmwLayoutBody:I = 0x7f0e00d0

.field public static final lmwLayoutOff:I = 0x7f0e00e7

.field public static final lmwTvBodyOff:I = 0x7f0e00e9

.field public static final lmwTvTitle:I = 0x7f0e00e8

.field public static final lmwTvTypeState1:I = 0x7f0e00d4

.field public static final lmwTvTypeState2:I = 0x7f0e00d8

.field public static final lmwTvTypeState3:I = 0x7f0e00dc

.field public static final lmwTvTypeState4:I = 0x7f0e00e0

.field public static final lmwTvTypeTitle1:I = 0x7f0e00d2

.field public static final lmwTvTypeTitle2:I = 0x7f0e00d6

.field public static final lmwTvTypeTitle3:I = 0x7f0e00da

.field public static final lmwTvTypeTitle4:I = 0x7f0e00de

.field public static final lmw_loadgin_to_get_started:I = 0x7f0e00e6

.field public static final location_qeury:I = 0x7f0e00ea

.field public static final map:I = 0x7f0e003a

.field public static final map_cp_link:I = 0x7f0e003b

.field public static final menu_add_location:I = 0x7f0e010e

.field public static final menu_alarm_type:I = 0x7f0e0108

.field public static final menu_delete_location:I = 0x7f0e0103

.field public static final menu_disable_geo_lookout:I = 0x7f0e0107

.field public static final menu_edit_location:I = 0x7f0e0102

.field public static final menu_emergency_mode:I = 0x7f0e010c

.field public static final menu_help:I = 0x7f0e010a

.field public static final menu_history:I = 0x7f0e0104

.field public static final menu_lifemode_about_index:I = 0x7f0e011b

.field public static final menu_lifemode_help:I = 0x7f0e011c

.field public static final menu_lifemode_refresh_time:I = 0x7f0e011a

.field public static final menu_manage_primary_contacts:I = 0x7f0e0106

.field public static final menu_my_version:I = 0x7f0e0117

.field public static final menu_next:I = 0x7f0e011e

.field public static final menu_open_source_license:I = 0x7f0e010b

.field public static final menu_prod_server_mode:I = 0x7f0e0111

.field public static final menu_save:I = 0x7f0e011d

.field public static final menu_service_coverage:I = 0x7f0e0109

.field public static final menu_set_alert:I = 0x7f0e010d

.field public static final menu_share:I = 0x7f0e0105

.field public static final menu_stg_server_mode:I = 0x7f0e0110

.field public static final menu_test_lat:I = 0x7f0e0113

.field public static final menu_test_lng:I = 0x7f0e0114

.field public static final menu_test_mode:I = 0x7f0e010f

.field public static final menu_test_remove_disaster:I = 0x7f0e0115

.field public static final menu_test_remove_disclaimer:I = 0x7f0e0116

.field public static final menu_test_zone:I = 0x7f0e0112

.field public static final message:I = 0x7f0e007b

.field public static final message_view:I = 0x7f0e00fe

.field public static final mulit_point_map_layout:I = 0x7f0e003f

.field public static final no_disaster_alert_type:I = 0x7f0e004e

.field public static final no_disaster_description:I = 0x7f0e004d

.field public static final no_disaster_img:I = 0x7f0e004c

.field public static final no_disaster_layout:I = 0x7f0e000b

.field public static final no_disaster_title:I = 0x7f0e004b

.field public static final no_disaster_title_wrapper:I = 0x7f0e004a

.field public static final no_item_bg:I = 0x7f0e00cd

.field public static final no_item_image:I = 0x7f0e00ce

.field public static final no_item_text:I = 0x7f0e00cf

.field public static final nodisaster_dashboard_layout:I = 0x7f0e000a

.field public static final nodisaster_map_layout:I = 0x7f0e000d

.field public static final nodisaster_share_layout:I = 0x7f0e000c

.field public static final popup_description_effective:I = 0x7f0e0076

.field public static final popup_description_location:I = 0x7f0e0075

.field public static final popup_description_state:I = 0x7f0e0074

.field public static final popup_description_time:I = 0x7f0e0077

.field public static final popup_emer_img:I = 0x7f0e0079

.field public static final popup_title_tv:I = 0x7f0e0078

.field public static final primary_contact:I = 0x7f0e002b

.field public static final query_list_full_address:I = 0x7f0e00ec

.field public static final query_list_name:I = 0x7f0e00eb

.field public static final refresh_progress_layout:I = 0x7f0e00e4

.field public static final relativeLayout:I = 0x7f0e00a9

.field public static final relativeLayout2:I = 0x7f0e00ab

.field public static final relativeLayout3:I = 0x7f0e00ad

.field public static final relativeLayout4:I = 0x7f0e00b4

.field public static final report_listview:I = 0x7f0e0048

.field public static final report_map:I = 0x7f0e0049

.field public static final safety_manual_icon_img_imageview:I = 0x7f0e00ca

.field public static final safety_manual_icon_layout:I = 0x7f0e00c9

.field public static final safety_map_ic_location_01:I = 0x7f0e00a8

.field public static final safety_map_ic_location_02:I = 0x7f0e00aa

.field public static final safety_map_ic_location_03:I = 0x7f0e00ac

.field public static final safety_map_ic_location_04:I = 0x7f0e00ae

.field public static final safety_map_ic_location_05:I = 0x7f0e00b5

.field public static final safetycare_cancel_btn:I = 0x7f0e006e

.field public static final safetycare_check1:I = 0x7f0e0067

.field public static final safetycare_check1_op1:I = 0x7f0e00f1

.field public static final safetycare_check1_op2:I = 0x7f0e00f2

.field public static final safetycare_check1_op3:I = 0x7f0e00f3

.field public static final safetycare_check2:I = 0x7f0e006a

.field public static final safetycare_check3:I = 0x7f0e006c

.field public static final safetycare_check4:I = 0x7f0e006d

.field public static final safetycare_geonews_desc1:I = 0x7f0e0066

.field public static final safetycare_geonews_desc2:I = 0x7f0e0069

.field public static final safetycare_geonews_desc3:I = 0x7f0e006b

.field public static final safetycare_geonews_scroll1:I = 0x7f0e00f0

.field public static final safetycare_geonews_scroll2:I = 0x7f0e00f4

.field public static final safetycare_geonews_scroll3:I = 0x7f0e00f6

.field public static final safetycare_ok_btn:I = 0x7f0e006f

.field public static final safetypcare_discalimer_button_layout:I = 0x7f0e00f7

.field public static final save_location:I = 0x7f0e0041

.field public static final save_name:I = 0x7f0e0042

.field public static final scrollView:I = 0x7f0e0065

.field public static final search_result:I = 0x7f0e0040

.field public static final search_view:I = 0x7f0e003e

.field public static final send_help_message:I = 0x7f0e0030

.field public static final send_ok_message:I = 0x7f0e0032

.field public static final seperator_history:I = 0x7f0e0025

.field public static final share_title:I = 0x7f0e002e

.field public static final share_via_icon:I = 0x7f0e002d

.field public static final skip:I = 0x7f0e00ee

.field public static final sw_navi:I = 0x7f0e00fb

.field public static final sw_navi_01:I = 0x7f0e00fc

.field public static final sw_navi_02:I = 0x7f0e00fd

.field public static final tab_indicator_layout:I = 0x7f0e00ff

.field public static final tab_indicator_text:I = 0x7f0e0100

.field public static final tabhost:I = 0x7f0e0007

.field public static final tablayout:I = 0x7f0e0008

.field public static final temperature_lv_text1:I = 0x7f0e00af

.field public static final temperature_lv_text2:I = 0x7f0e00b0

.field public static final temperature_lv_text3:I = 0x7f0e00b1

.field public static final temperature_lv_text4:I = 0x7f0e00b2

.field public static final temperature_lv_text5:I = 0x7f0e00b6

.field public static final tips_icon:I = 0x7f0e0022

.field public static final tips_title:I = 0x7f0e0023

.field public static final updateTime:I = 0x7f0e00e3

.field public static final update_first_location:I = 0x7f0e003c

.field public static final widget_all_view:I = 0x7f0e004f

.field public static final widget_body:I = 0x7f0e0053

.field public static final widget_body_trans:I = 0x7f0e0064

.field public static final widget_buddy_name:I = 0x7f0e0061

.field public static final widget_description:I = 0x7f0e005a

.field public static final widget_description_secondline:I = 0x7f0e005c

.field public static final widget_description_thirdline:I = 0x7f0e005d

.field public static final widget_description_time:I = 0x7f0e0060

.field public static final widget_description_title:I = 0x7f0e005b

.field public static final widget_loadgin_to_get_started:I = 0x7f0e0052

.field public static final widget_next_btn_img:I = 0x7f0e0057

.field public static final widget_off_content:I = 0x7f0e0051

.field public static final widget_off_title:I = 0x7f0e0050

.field public static final widget_on_img:I = 0x7f0e0054

.field public static final widget_on_progress:I = 0x7f0e0055

.field public static final widget_safe:I = 0x7f0e0056

.field public static final widget_safe_body:I = 0x7f0e0059

.field public static final widget_safe_title:I = 0x7f0e0058


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1003
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
