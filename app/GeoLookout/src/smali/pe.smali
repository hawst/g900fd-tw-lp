.class final Lpe;
.super Los;


# instance fields
.field final synthetic a:Lot;

.field private final b:Lfx;

.field private final c:Lfw;

.field private final d:Lfp;


# direct methods
.method public constructor <init>(Lot;Lfx;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lpe;->a:Lot;

    invoke-direct {p0}, Los;-><init>()V

    const-string v0, "Callbacks must not be null"

    invoke-static {p2, v0}, Lnx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfx;

    iput-object v0, p0, Lpe;->b:Lfx;

    iput-object v1, p0, Lpe;->c:Lfw;

    iput-object v1, p0, Lpe;->d:Lfp;

    return-void
.end method

.method public constructor <init>(Lot;Lfx;Lfw;Lfp;)V
    .locals 1

    iput-object p1, p0, Lpe;->a:Lot;

    invoke-direct {p0}, Los;-><init>()V

    const-string v0, "Callbacks must not be null"

    invoke-static {p2, v0}, Lnx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfx;

    iput-object v0, p0, Lpe;->b:Lfx;

    iput-object p3, p0, Lpe;->c:Lfw;

    iput-object p4, p0, Lpe;->d:Lfp;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/common/data/d;[Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lpe;->a:Lot;

    new-instance v1, Low;

    iget-object v2, p0, Lpe;->a:Lot;

    iget-object v3, p0, Lpe;->c:Lfw;

    invoke-direct {v1, v2, v3, p1, p2}, Low;-><init>(Lot;Lfw;Lcom/google/android/gms/common/data/d;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lot;->a(Lnb;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessage;)V
    .locals 4

    const-string v0, "GamesClient"

    const-string v1, "RoomBinderCallbacks: onRealTimeMessageReceived"

    invoke-static {v0, v1}, Lqm;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lpe;->a:Lot;

    new-instance v1, Lqg;

    iget-object v2, p0, Lpe;->a:Lot;

    iget-object v3, p0, Lpe;->d:Lfp;

    invoke-direct {v1, v2, v3, p1}, Lqg;-><init>(Lot;Lfp;Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessage;)V

    invoke-virtual {v0, v1}, Lot;->a(Lnb;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lpe;->a:Lot;

    new-instance v1, Lqh;

    iget-object v2, p0, Lpe;->a:Lot;

    iget-object v3, p0, Lpe;->c:Lfw;

    invoke-direct {v1, v2, v3, p1}, Lqh;-><init>(Lot;Lfw;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lot;->a(Lnb;)V

    return-void
.end method

.method public b(Lcom/google/android/gms/common/data/d;[Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lpe;->a:Lot;

    new-instance v1, Lox;

    iget-object v2, p0, Lpe;->a:Lot;

    iget-object v3, p0, Lpe;->c:Lfw;

    invoke-direct {v1, v2, v3, p1, p2}, Lox;-><init>(Lot;Lfw;Lcom/google/android/gms/common/data/d;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lot;->a(Lnb;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lpe;->a:Lot;

    new-instance v1, Lqi;

    iget-object v2, p0, Lpe;->a:Lot;

    iget-object v3, p0, Lpe;->c:Lfw;

    invoke-direct {v1, v2, v3, p1}, Lqi;-><init>(Lot;Lfw;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lot;->a(Lnb;)V

    return-void
.end method

.method public c(ILjava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lpe;->a:Lot;

    new-instance v1, Lqf;

    iget-object v2, p0, Lpe;->a:Lot;

    iget-object v3, p0, Lpe;->b:Lfx;

    invoke-direct {v1, v2, v3, p1, p2}, Lqf;-><init>(Lot;Lfx;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Lot;->a(Lnb;)V

    return-void
.end method

.method public c(Lcom/google/android/gms/common/data/d;[Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lpe;->a:Lot;

    new-instance v1, Loy;

    iget-object v2, p0, Lpe;->a:Lot;

    iget-object v3, p0, Lpe;->c:Lfw;

    invoke-direct {v1, v2, v3, p1, p2}, Loy;-><init>(Lot;Lfw;Lcom/google/android/gms/common/data/d;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lot;->a(Lnb;)V

    return-void
.end method

.method public d(Lcom/google/android/gms/common/data/d;[Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lpe;->a:Lot;

    new-instance v1, Lqk;

    iget-object v2, p0, Lpe;->a:Lot;

    iget-object v3, p0, Lpe;->c:Lfw;

    invoke-direct {v1, v2, v3, p1, p2}, Lqk;-><init>(Lot;Lfw;Lcom/google/android/gms/common/data/d;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lot;->a(Lnb;)V

    return-void
.end method

.method public e(Lcom/google/android/gms/common/data/d;[Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lpe;->a:Lot;

    new-instance v1, Lqj;

    iget-object v2, p0, Lpe;->a:Lot;

    iget-object v3, p0, Lpe;->c:Lfw;

    invoke-direct {v1, v2, v3, p1, p2}, Lqj;-><init>(Lot;Lfw;Lcom/google/android/gms/common/data/d;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lot;->a(Lnb;)V

    return-void
.end method

.method public f(Lcom/google/android/gms/common/data/d;[Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lpe;->a:Lot;

    new-instance v1, Lov;

    iget-object v2, p0, Lpe;->a:Lot;

    iget-object v3, p0, Lpe;->c:Lfw;

    invoke-direct {v1, v2, v3, p1, p2}, Lov;-><init>(Lot;Lfw;Lcom/google/android/gms/common/data/d;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lot;->a(Lnb;)V

    return-void
.end method

.method public m(Lcom/google/android/gms/common/data/d;)V
    .locals 4

    iget-object v0, p0, Lpe;->a:Lot;

    new-instance v1, Lph;

    iget-object v2, p0, Lpe;->a:Lot;

    iget-object v3, p0, Lpe;->b:Lfx;

    invoke-direct {v1, v2, v3, p1}, Lph;-><init>(Lot;Lfx;Lcom/google/android/gms/common/data/d;)V

    invoke-virtual {v0, v1}, Lot;->a(Lnb;)V

    return-void
.end method

.method public n(Lcom/google/android/gms/common/data/d;)V
    .locals 4

    iget-object v0, p0, Lpe;->a:Lot;

    new-instance v1, Lqa;

    iget-object v2, p0, Lpe;->a:Lot;

    iget-object v3, p0, Lpe;->b:Lfx;

    invoke-direct {v1, v2, v3, p1}, Lqa;-><init>(Lot;Lfx;Lcom/google/android/gms/common/data/d;)V

    invoke-virtual {v0, v1}, Lot;->a(Lnb;)V

    return-void
.end method

.method public o(Lcom/google/android/gms/common/data/d;)V
    .locals 4

    iget-object v0, p0, Lpe;->a:Lot;

    new-instance v1, Lpg;

    iget-object v2, p0, Lpe;->a:Lot;

    iget-object v3, p0, Lpe;->c:Lfw;

    invoke-direct {v1, v2, v3, p1}, Lpg;-><init>(Lot;Lfw;Lcom/google/android/gms/common/data/d;)V

    invoke-virtual {v0, v1}, Lot;->a(Lnb;)V

    return-void
.end method

.method public p(Lcom/google/android/gms/common/data/d;)V
    .locals 4

    iget-object v0, p0, Lpe;->a:Lot;

    new-instance v1, Lpd;

    iget-object v2, p0, Lpe;->a:Lot;

    iget-object v3, p0, Lpe;->c:Lfw;

    invoke-direct {v1, v2, v3, p1}, Lpd;-><init>(Lot;Lfw;Lcom/google/android/gms/common/data/d;)V

    invoke-virtual {v0, v1}, Lot;->a(Lnb;)V

    return-void
.end method

.method public q(Lcom/google/android/gms/common/data/d;)V
    .locals 4

    iget-object v0, p0, Lpe;->a:Lot;

    new-instance v1, Lpf;

    iget-object v2, p0, Lpe;->a:Lot;

    iget-object v3, p0, Lpe;->b:Lfx;

    invoke-direct {v1, v2, v3, p1}, Lpf;-><init>(Lot;Lfx;Lcom/google/android/gms/common/data/d;)V

    invoke-virtual {v0, v1}, Lot;->a(Lnb;)V

    return-void
.end method

.method public r(Lcom/google/android/gms/common/data/d;)V
    .locals 4

    iget-object v0, p0, Lpe;->a:Lot;

    new-instance v1, Lps;

    iget-object v2, p0, Lpe;->a:Lot;

    iget-object v3, p0, Lpe;->c:Lfw;

    invoke-direct {v1, v2, v3, p1}, Lps;-><init>(Lot;Lfw;Lcom/google/android/gms/common/data/d;)V

    invoke-virtual {v0, v1}, Lot;->a(Lnb;)V

    return-void
.end method

.method public s(Lcom/google/android/gms/common/data/d;)V
    .locals 4

    iget-object v0, p0, Lpe;->a:Lot;

    new-instance v1, Lpt;

    iget-object v2, p0, Lpe;->a:Lot;

    iget-object v3, p0, Lpe;->c:Lfw;

    invoke-direct {v1, v2, v3, p1}, Lpt;-><init>(Lot;Lfw;Lcom/google/android/gms/common/data/d;)V

    invoke-virtual {v0, v1}, Lot;->a(Lnb;)V

    return-void
.end method
