.class Lans;
.super Lcom/loopj/android/http/AsyncHttpResponseHandler;
.source "DisasterServerInterface.java"


# instance fields
.field final synthetic a:Lanx;

.field final synthetic b:Landroid/location/Location;

.field final synthetic c:I

.field final synthetic d:Lanh;


# direct methods
.method constructor <init>(Lanh;Lanx;Landroid/location/Location;I)V
    .locals 0

    .prologue
    .line 394
    iput-object p1, p0, Lans;->d:Lanh;

    iput-object p2, p0, Lans;->a:Lanx;

    iput-object p3, p0, Lans;->b:Landroid/location/Location;

    iput p4, p0, Lans;->c:I

    invoke-direct {p0}, Lcom/loopj/android/http/AsyncHttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 405
    invoke-super {p0, p1, p2, p3, p4}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onFailure(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V

    .line 406
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "requestDisasterHistory: onFailure(statusCode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 407
    iget-object v0, p0, Lans;->a:Lanx;

    invoke-interface {v0}, Lanx;->b()V

    .line 408
    return-void
.end method

.method public onFinish()V
    .locals 1

    .prologue
    .line 412
    const-string v0, "requestDisasterHistory: onFinish()  "

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 413
    return-void
.end method

.method public onRetry()V
    .locals 1

    .prologue
    .line 417
    invoke-super {p0}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onRetry()V

    .line 418
    const-string v0, "requestDisasterHistory: onRetry"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 419
    return-void
.end method

.method public onSuccess(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 398
    invoke-super {p0, p1}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onSuccess(Ljava/lang/String;)V

    .line 399
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "requestDisasterHistory: responseHandler : onSuccess "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 400
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    iget-object v1, p0, Lans;->d:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lans;->a:Lanx;

    iget-object v4, p0, Lans;->b:Landroid/location/Location;

    iget v5, p0, Lans;->c:I

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lalu;->a(Landroid/content/Context;Ljava/lang/String;Lanx;Landroid/location/Location;I)V

    .line 401
    return-void
.end method
