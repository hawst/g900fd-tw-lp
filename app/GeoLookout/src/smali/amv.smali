.class public final enum Lamv;
.super Ljava/lang/Enum;
.source "LifemodeDBStructure.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lamv;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lamv;

.field public static final enum b:Lamv;

.field public static final enum c:Lamv;

.field public static final enum d:Lamv;

.field public static final enum e:Lamv;

.field public static final enum f:Lamv;

.field public static final enum g:Lamv;

.field public static final enum h:Lamv;

.field private static final synthetic i:[Lamv;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 62
    new-instance v0, Lamv;

    const-string v1, "LOCATION_ID"

    invoke-direct {v0, v1, v3}, Lamv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamv;->a:Lamv;

    new-instance v0, Lamv;

    const-string v1, "VALUE"

    invoke-direct {v0, v1, v4}, Lamv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamv;->b:Lamv;

    new-instance v0, Lamv;

    const-string v1, "LEVEL"

    invoke-direct {v0, v1, v5}, Lamv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamv;->c:Lamv;

    new-instance v0, Lamv;

    const-string v1, "LOCATION_LAT"

    invoke-direct {v0, v1, v6}, Lamv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamv;->d:Lamv;

    new-instance v0, Lamv;

    const-string v1, "LOCATION_LNG"

    invoke-direct {v0, v1, v7}, Lamv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamv;->e:Lamv;

    new-instance v0, Lamv;

    const-string v1, "TYPE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lamv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamv;->f:Lamv;

    new-instance v0, Lamv;

    const-string v1, "STARTTIME"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lamv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamv;->g:Lamv;

    new-instance v0, Lamv;

    const-string v1, "CITY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lamv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamv;->h:Lamv;

    .line 61
    const/16 v0, 0x8

    new-array v0, v0, [Lamv;

    sget-object v1, Lamv;->a:Lamv;

    aput-object v1, v0, v3

    sget-object v1, Lamv;->b:Lamv;

    aput-object v1, v0, v4

    sget-object v1, Lamv;->c:Lamv;

    aput-object v1, v0, v5

    sget-object v1, Lamv;->d:Lamv;

    aput-object v1, v0, v6

    sget-object v1, Lamv;->e:Lamv;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lamv;->f:Lamv;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lamv;->g:Lamv;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lamv;->h:Lamv;

    aput-object v2, v0, v1

    sput-object v0, Lamv;->i:[Lamv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lamv;
    .locals 1

    .prologue
    .line 61
    const-class v0, Lamv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lamv;

    return-object v0
.end method

.method public static values()[Lamv;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lamv;->i:[Lamv;

    invoke-virtual {v0}, [Lamv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lamv;

    return-object v0
.end method
