.class Lani;
.super Lcom/loopj/android/http/AsyncHttpResponseHandler;
.source "DisasterServerInterface.java"


# instance fields
.field final synthetic a:Lanh;


# direct methods
.method constructor <init>(Lanh;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lani;->a:Lanh;

    invoke-direct {p0}, Lcom/loopj/android/http/AsyncHttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I[Lorg/apache/http/Header;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 138
    invoke-super {p0, p1, p2, p3, p4}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onFailure(I[Lorg/apache/http/Header;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "registerDisasterServer: onFailure("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "error:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Throwable;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "registerDisasterServer: content: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lani;->a:Lanh;

    invoke-static {v0}, Lanh;->a(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 142
    iget-object v0, p0, Lani;->a:Lanh;

    invoke-static {v0}, Lanh;->a(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 143
    iget-object v0, p0, Lani;->a:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Laog;->a(Landroid/content/Context;Z)V

    .line 144
    iget-object v0, p0, Lani;->a:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lani;->a:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lage;->c(Landroid/content/Context;Z)V

    .line 145
    sget-boolean v0, Laky;->c:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x1bf

    if-ne p1, v0, :cond_0

    .line 146
    const/4 v0, 0x0

    .line 148
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 149
    const-string v2, "addr"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 150
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "newServerURL = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->f(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :goto_0
    invoke-static {}, Laoh;->a()Laoh;

    move-result-object v1

    iget-object v2, p0, Lani;->a:Lanh;

    invoke-static {v2}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Laoh;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 156
    :cond_0
    return-void

    .line 151
    :catch_0
    move-exception v1

    .line 152
    const-string v1, "JSONException"

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onFinish()V
    .locals 1

    .prologue
    .line 160
    const-string v0, "registerDisasterServer: onFinish()  "

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 162
    return-void
.end method

.method public onRetry()V
    .locals 1

    .prologue
    .line 166
    invoke-super {p0}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onRetry()V

    .line 167
    const-string v0, "registerDisasterServer: onRetry"

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 168
    return-void
.end method

.method public onSuccess(I[Lorg/apache/http/Header;[B)V
    .locals 2

    .prologue
    .line 126
    invoke-super {p0, p1, p2, p3}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onSuccess(I[Lorg/apache/http/Header;[B)V

    .line 127
    const-string v0, "registerDisasterServer: onSuccess "

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lani;->a:Lanh;

    invoke-static {v0}, Lanh;->a(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 129
    iget-object v0, p0, Lani;->a:Lanh;

    invoke-static {v0}, Lanh;->a(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 130
    iget-object v0, p0, Lani;->a:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lani;->a:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lage;->n(Landroid/content/Context;)V

    .line 131
    iget-object v0, p0, Lani;->a:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/GeoLookout/DisasterAlertService;->b(Landroid/content/Context;)V

    .line 132
    iget-object v0, p0, Lani;->a:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Laog;->a(Landroid/content/Context;Z)V

    .line 133
    return-void
.end method
