.class public final Lalx;
.super Ljava/lang/Object;
.source "Disaster.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# static fields
.field public static final a:Ljava/lang/String; = "CURRENT_DISASTER_TBL"

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field public static final d:Landroid/net/Uri;

.field public static final e:Landroid/net/Uri;

.field public static final f:Landroid/net/Uri;

.field public static final g:Landroid/net/Uri;

.field public static final h:Landroid/net/Uri;

.field public static final i:Ljava/lang/String; = "vnd.android.cursor.dir/current"

.field public static final j:Ljava/lang/String; = "vnd.android.cursor.item/current"

.field public static final k:Ljava/lang/String; = "created DESC"

.field public static final l:Ljava/lang/String; = "RTIME DESC"

.field public static final m:Ljava/lang/String; = "RTIME DESC"

.field public static n:Lalv; = null

.field private static final o:Ljava/lang/String; = "content://"

.field private static final p:Ljava/lang/String; = "/current"

.field private static final q:Ljava/lang/String; = "/current/"

.field private static final r:Ljava/lang/String; = "/current/did/"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 229
    const-string v0, "content://com.sec.android.GeoLookout.db/current"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lalx;->d:Landroid/net/Uri;

    .line 231
    const-string v0, "content://com.sec.android.GeoLookout.db/current/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lalx;->e:Landroid/net/Uri;

    .line 233
    const-string v0, "content://com.sec.android.GeoLookout.db/current/did/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lalx;->f:Landroid/net/Uri;

    .line 235
    const-string v0, "content://com.sec.android.GeoLookout.db/current//#"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lalx;->g:Landroid/net/Uri;

    .line 237
    const-string v0, "content://com.sec.android.GeoLookout.db/current/did//*"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lalx;->h:Landroid/net/Uri;

    .line 252
    new-instance v0, Lalv;

    invoke-direct {v0}, Lalv;-><init>()V

    sput-object v0, Lalx;->n:Lalv;

    .line 253
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 213
    return-void
.end method

.method private static a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Landroid/content/ContentValues;
    .locals 1

    .prologue
    .line 713
    const-string v0, "createContentValue"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 714
    sget-object v0, Lalx;->n:Lalv;

    invoke-virtual {v0, p0}, Lalv;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method public static a(I)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 807
    sget-object v0, Lalx;->e:Landroid/net/Uri;

    invoke-static {v0, p0}, Lalx;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/net/Uri;I)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 811
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 812
    invoke-static {p0, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 803
    invoke-static {p0, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 799
    sget-object v0, Lalx;->f:Landroid/net/Uri;

    invoke-static {v0, p0}, Lalx;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 1

    .prologue
    .line 433
    const-string v0, "getLastestDisasterInfoBySignificance"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 435
    const/4 v0, -0x1

    invoke-static {p0, v0}, Lalx;->b(Landroid/content/Context;I)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;I)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 291
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getDisasterInfo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lalx;->a(I)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 292
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 295
    :try_start_0
    invoke-static {p1}, Lalx;->a(I)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lalx;->n:Lalv;

    invoke-virtual {v2}, Lalv;->a()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 296
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    invoke-static {v1}, Lalx;->a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 303
    if-eqz v1, :cond_0

    .line 304
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 307
    :cond_0
    :goto_0
    return-object v0

    .line 303
    :cond_1
    if-eqz v1, :cond_2

    .line 304
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_1
    move-object v0, v6

    .line 307
    goto :goto_0

    .line 300
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 301
    :goto_2
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 303
    if-eqz v1, :cond_2

    .line 304
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 303
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v6, :cond_3

    .line 304
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 303
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_3

    .line 300
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;II)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 469
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getLastestDisasterInfo : significance "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 471
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 472
    const-string v3, "SIGNIFICANCE=?"

    .line 475
    if-ltz p2, :cond_1

    .line 476
    const-string v1, " AND MULTI_POINT_ID=?"

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 477
    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    .line 489
    :goto_0
    :try_start_0
    sget-object v1, Lalx;->d:Landroid/net/Uri;

    sget-object v2, Lalx;->n:Lalv;

    invoke-virtual {v2}, Lalv;->a()[Ljava/lang/String;

    move-result-object v2

    const-string v5, "RTIME DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 492
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 493
    invoke-static {v1}, Lalx;->a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 499
    if-eqz v1, :cond_0

    .line 500
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 503
    :cond_0
    :goto_1
    return-object v0

    .line 482
    :cond_1
    new-array v4, v5, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    goto :goto_0

    .line 499
    :cond_2
    if-eqz v1, :cond_3

    .line 500
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_2
    move-object v0, v6

    .line 503
    goto :goto_1

    .line 496
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 497
    :goto_3
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 499
    if-eqz v1, :cond_3

    .line 500
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 499
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v6, :cond_4

    .line 500
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 499
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_4

    .line 496
    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;III)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 508
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getLastestDisasterInfo : significance "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dtype "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 510
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 511
    const-string v3, "SIGNIFICANCE=? AND DTYPE_ID=?"

    .line 514
    if-ltz p3, :cond_1

    .line 515
    const-string v1, " AND MULTI_POINT_ID=?"

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 516
    const/4 v1, 0x3

    new-array v4, v1, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v7

    .line 528
    :goto_0
    :try_start_0
    sget-object v1, Lalx;->d:Landroid/net/Uri;

    sget-object v2, Lalx;->n:Lalv;

    invoke-virtual {v2}, Lalv;->a()[Ljava/lang/String;

    move-result-object v2

    const-string v5, "RTIME DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 531
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 532
    invoke-static {v1}, Lalx;->a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 538
    if-eqz v1, :cond_0

    .line 539
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 542
    :cond_0
    :goto_1
    return-object v0

    .line 521
    :cond_1
    new-array v4, v7, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    goto :goto_0

    .line 538
    :cond_2
    if-eqz v1, :cond_3

    .line 539
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_2
    move-object v0, v6

    .line 542
    goto :goto_1

    .line 535
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 536
    :goto_3
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 538
    if-eqz v1, :cond_3

    .line 539
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 538
    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v6, :cond_4

    .line 539
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 538
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_4

    .line 535
    :catch_1
    move-exception v0

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 391
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 394
    :try_start_0
    sget-object v1, Lalx;->n:Lalv;

    invoke-virtual {v1}, Lalv;->a()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 395
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 396
    invoke-static {v1}, Lalx;->a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 402
    if-eqz v1, :cond_0

    .line 403
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 406
    :cond_0
    :goto_0
    return-object v0

    .line 402
    :cond_1
    if-eqz v1, :cond_2

    .line 403
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_1
    move-object v0, v6

    .line 406
    goto :goto_0

    .line 399
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 400
    :goto_2
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 402
    if-eqz v1, :cond_2

    .line 403
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 402
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_3

    .line 403
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 402
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 399
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 2

    .prologue
    .line 257
    const-wide/16 v0, 0x0

    invoke-static {p0, p1, v0, v1}, Lalx;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;J)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getDisasterInfo : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lalx;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 262
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 266
    :try_start_0
    invoke-static {p1}, Lalx;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lalx;->n:Lalv;

    invoke-virtual {v2}, Lalv;->a()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "MULTI_POINT_ID=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 274
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276
    invoke-static {v1}, Lalx;->a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 277
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDisasterInfo moveToFirst "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 283
    if-eqz v1, :cond_0

    .line 284
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 287
    :cond_0
    :goto_0
    return-object v0

    .line 283
    :cond_1
    if-eqz v1, :cond_2

    .line 284
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_1
    move-object v0, v6

    .line 287
    goto :goto_0

    .line 280
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 281
    :goto_2
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 283
    if-eqz v1, :cond_2

    .line 284
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 283
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_3

    .line 284
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 283
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 280
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private static a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 1

    .prologue
    .line 410
    const-string v0, "getDisasterInfoFromCursor"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 411
    sget-object v0, Lalx;->n:Lalv;

    invoke-virtual {v0, p0}, Lalv;->b(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;J)V
    .locals 13

    .prologue
    const/4 v7, 0x0

    .line 633
    const-string v0, "deleteOldFromStartTime"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 634
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 635
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 636
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Landroid/app/NotificationManager;

    .line 640
    :try_start_0
    sget-object v1, Lalx;->d:Landroid/net/Uri;

    sget-object v2, Lalx;->n:Lalv;

    invoke-virtual {v2}, Lalv;->a()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "ETIME<?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    sub-long v10, v8, p1

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 644
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 646
    :cond_0
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 647
    if-eqz v6, :cond_1

    .line 648
    invoke-virtual {v6, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 651
    :cond_1
    const/4 v2, 0x4

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 652
    invoke-static {p0, v2}, Lama;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 654
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 657
    :cond_2
    sget-object v2, Lalx;->d:Landroid/net/Uri;

    const-string v3, "ETIME<?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    sub-long v6, v8, p1

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 663
    if-eqz v1, :cond_3

    .line 664
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 667
    :cond_3
    :goto_0
    return-void

    .line 660
    :catch_0
    move-exception v0

    move-object v1, v7

    .line 661
    :goto_1
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 663
    if-eqz v1, :cond_3

    .line 664
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 663
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_2
    if-eqz v1, :cond_4

    .line 664
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 663
    :catchall_1
    move-exception v0

    goto :goto_2

    .line 660
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 6

    .prologue
    .line 547
    .line 548
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v0

    .line 551
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v1

    .line 554
    const-string v2, "DID=? AND MULTI_POINT_ID=?"

    .line 556
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 560
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteDisasterInfo DID : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " uid: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 561
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 563
    if-eqz v0, :cond_0

    .line 564
    sget-object v1, Lalx;->d:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 568
    :goto_0
    return-void

    .line 566
    :cond_0
    const-string v0, "resolver is null !!"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/GeoLookout/db/DisasterInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 700
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "insertDisasterInfo() length:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 701
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 702
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v4, v0, [Landroid/content/ContentValues;

    .line 703
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 704
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 705
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-static {v0}, Lalx;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Landroid/content/ContentValues;

    move-result-object v0

    aput-object v0, v4, v1

    .line 704
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 707
    :cond_0
    sget-object v0, Lalx;->d:Landroid/net/Uri;

    invoke-virtual {v5, v0, v4}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v0

    .line 709
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insertDisasterInfo() end! elapsedTime:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " count : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 710
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;I)Z
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 362
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getDisasterInfoForMulitPoint : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " uid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 364
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 365
    const-string v3, "MESSAGE_ID=? AND MULTI_POINT_ID=?"

    .line 370
    :try_start_0
    sget-object v1, Lalx;->d:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "MESSAGE_ID"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 376
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    .line 382
    if-eqz v1, :cond_0

    .line 383
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    move v0, v6

    .line 387
    :goto_0
    return v0

    .line 382
    :cond_1
    if-eqz v1, :cond_2

    .line 383
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_1
    move v0, v7

    .line 387
    goto :goto_0

    .line 379
    :catch_0
    move-exception v0

    move-object v1, v8

    .line 380
    :goto_2
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 382
    if-eqz v1, :cond_2

    .line 383
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 382
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v8, :cond_3

    .line 383
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 382
    :catchall_1
    move-exception v0

    move-object v8, v1

    goto :goto_3

    .line 379
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)I
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 311
    .line 312
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 315
    :try_start_0
    invoke-static {p1}, Lalx;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "MULTI_POINT_ID"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 318
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 320
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    .line 321
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MULTIPOINT getMultiPointUidByDID : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " multipoint : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move v0, v2

    .line 326
    :goto_0
    if-eqz v1, :cond_0

    .line 327
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 330
    :cond_0
    :goto_1
    return v0

    .line 323
    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v2, v6

    move v0, v7

    .line 324
    :goto_2
    :try_start_3
    invoke-static {v1}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 326
    if-eqz v2, :cond_0

    .line 327
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 326
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_1

    .line 327
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 326
    :catchall_1
    move-exception v0

    goto :goto_3

    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_3

    .line 323
    :catch_1
    move-exception v0

    move-object v2, v1

    move-object v1, v0

    move v0, v7

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v8, v0

    move v0, v2

    move-object v2, v1

    move-object v1, v8

    goto :goto_2

    :cond_2
    move v0, v7

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;I)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 416
    const-string v0, "getLastestDisasterInfoBySignificance"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 418
    const/16 v0, 0x70

    invoke-static {p0, v1, v0, p1}, Lalx;->a(Landroid/content/Context;III)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 421
    if-nez v0, :cond_0

    .line 422
    invoke-static {p0, v1, p1}, Lalx;->a(Landroid/content/Context;II)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 425
    :cond_0
    if-nez v0, :cond_1

    .line 426
    invoke-static {p0, p1}, Lalx;->c(Landroid/content/Context;I)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 429
    :cond_1
    return-object v0
.end method

.method public static b(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/GeoLookout/db/DisasterInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 760
    const-string v0, "getNotificationUncheckedInfo"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 761
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 762
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 766
    :try_start_0
    sget-object v1, Lalx;->d:Landroid/net/Uri;

    sget-object v2, Lalx;->n:Lalv;

    invoke-virtual {v2}, Lalv;->a()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "NOTISTATUS=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 769
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 771
    :cond_0
    invoke-static {v1}, Lalx;->a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 772
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 773
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    .line 779
    if-eqz v1, :cond_1

    .line 780
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object v0, v6

    .line 784
    :goto_0
    return-object v0

    .line 779
    :cond_2
    if-eqz v1, :cond_3

    .line 780
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    move-object v0, v7

    .line 784
    goto :goto_0

    .line 776
    :catch_0
    move-exception v0

    move-object v1, v7

    .line 777
    :goto_2
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 779
    if-eqz v1, :cond_3

    .line 780
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 779
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v7, :cond_4

    .line 780
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 779
    :catchall_1
    move-exception v0

    move-object v7, v1

    goto :goto_3

    .line 776
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static b(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 7

    .prologue
    .line 604
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v0

    .line 605
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateDisasterInfo : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 607
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 608
    invoke-static {p1}, Lalx;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Landroid/content/ContentValues;

    move-result-object v2

    .line 613
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v3

    .line 616
    const-string v4, "DID=? AND MULTI_POINT_ID=?"

    .line 618
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v0

    .line 624
    :try_start_0
    sget-object v0, Lalx;->d:Landroid/net/Uri;

    invoke-virtual {v1, v0, v2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 629
    :goto_0
    return-void

    .line 625
    :catch_0
    move-exception v0

    .line 626
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 718
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updateNotificationStatus : notiStatus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 719
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 720
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 721
    const-string v2, "NOTISTATUS"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 723
    :try_start_0
    sget-object v2, Lalx;->d:Landroid/net/Uri;

    const-string v3, "DID=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 729
    :goto_0
    return-void

    .line 726
    :catch_0
    move-exception v0

    .line 727
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 746
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updateEndTime did:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " endTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 747
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 748
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 749
    const-string v2, "ETIME"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 751
    :try_start_0
    sget-object v2, Lalx;->d:Landroid/net/Uri;

    const-string v3, "DID=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 757
    :goto_0
    return-void

    .line 754
    :catch_0
    move-exception v0

    .line 755
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 692
    const-string v0, "insertDisasterInfo"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 694
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 695
    invoke-static {p1}, Lalx;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Landroid/content/ContentValues;

    move-result-object v1

    .line 696
    sget-object v2, Lalx;->d:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/content/Context;I)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 439
    const-string v0, "getLastestDisasterInfo"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 440
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 444
    if-ltz p1, :cond_3

    .line 445
    const-string v3, "MULTI_POINT_ID=?"

    .line 446
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    .line 452
    :goto_0
    :try_start_0
    sget-object v1, Lalx;->d:Landroid/net/Uri;

    sget-object v2, Lalx;->n:Lalv;

    invoke-virtual {v2}, Lalv;->a()[Ljava/lang/String;

    move-result-object v2

    const-string v5, "RTIME DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 454
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 455
    invoke-static {v1}, Lalx;->a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 461
    if-eqz v1, :cond_0

    .line 462
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 465
    :cond_0
    :goto_1
    return-object v6

    .line 461
    :cond_1
    if-eqz v1, :cond_0

    .line 462
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 458
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 459
    :goto_2
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 461
    if-eqz v1, :cond_0

    .line 462
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 461
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v6, :cond_2

    .line 462
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 461
    :catchall_1
    move-exception v0

    move-object v6, v1

    goto :goto_3

    .line 458
    :catch_1
    move-exception v0

    goto :goto_2

    :cond_3
    move-object v4, v6

    move-object v3, v6

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 788
    const-string v0, "deleteAll"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 789
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 791
    if-eqz v0, :cond_0

    .line 792
    sget-object v1, Lalx;->d:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 796
    :goto_0
    return-void

    .line 794
    :cond_0
    const-string v0, "resolver is null !!"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 732
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updateDistance did:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " distance:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 733
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 734
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 735
    const-string v2, "DISTANCE"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 737
    :try_start_0
    sget-object v2, Lalx;->d:Landroid/net/Uri;

    const-string v3, "DID=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 743
    :goto_0
    return-void

    .line 740
    :catch_0
    move-exception v0

    .line 741
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 334
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getDisasterInfoForMsgId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 336
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 337
    const-string v3, "MESSAGE_ID=?"

    .line 341
    :try_start_0
    sget-object v1, Lalx;->d:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "MESSAGE_ID"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 347
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_1

    .line 353
    if-eqz v1, :cond_0

    .line 354
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    move v0, v6

    .line 358
    :goto_0
    return v0

    .line 353
    :cond_1
    if-eqz v1, :cond_2

    .line 354
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_1
    move v0, v7

    .line 358
    goto :goto_0

    .line 350
    :catch_0
    move-exception v0

    move-object v1, v8

    .line 351
    :goto_2
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 353
    if-eqz v1, :cond_2

    .line 354
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 353
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v8, :cond_3

    .line 354
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 353
    :catchall_1
    move-exception v0

    move-object v8, v1

    goto :goto_3

    .line 350
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static d(Landroid/content/Context;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 586
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 588
    if-eqz v0, :cond_0

    .line 589
    sget-object v1, Lalx;->d:Landroid/net/Uri;

    const-string v2, "MULTI_POINT_ID=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 593
    invoke-static {p0, p1}, Laly;->b(Landroid/content/Context;I)V

    .line 595
    invoke-static {p0, v5, p1}, Lall;->a(Landroid/content/Context;II)V

    .line 601
    :goto_0
    return-void

    .line 598
    :cond_0
    const-string v0, "resolver is null !!"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 571
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 572
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "deleteDisasterInfo DID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 573
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 575
    if-eqz v0, :cond_0

    .line 576
    invoke-static {p1}, Lalx;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 583
    :goto_0
    return-void

    .line 578
    :cond_0
    const-string v0, "resolver is null !!"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 581
    :cond_1
    const-string v0, "deleteDisasterInfo DID null !!"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 671
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 674
    :try_start_0
    invoke-static {p1}, Lalx;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lalx;->n:Lalv;

    invoke-virtual {v2}, Lalv;->a()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 675
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 676
    const-string v0, "isSameDID true"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 677
    const/4 v0, 0x1

    .line 682
    if-eqz v1, :cond_0

    .line 683
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 688
    :cond_0
    :goto_0
    return v0

    .line 682
    :cond_1
    if-eqz v1, :cond_2

    .line 683
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 687
    :cond_2
    :goto_1
    const-string v0, "isSameDID false"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 688
    const/4 v0, 0x0

    goto :goto_0

    .line 679
    :catch_0
    move-exception v0

    move-object v1, v6

    .line 680
    :goto_2
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 682
    if-eqz v1, :cond_2

    .line 683
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 682
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_3

    .line 683
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 682
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 679
    :catch_1
    move-exception v0

    goto :goto_2
.end method
