.class public Lajv;
.super Ljava/lang/Object;
.source "DisasterMultiPointSaveActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lajv;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    const/16 v3, 0x41

    const/4 v4, 0x0

    .line 150
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lt v0, v3, :cond_0

    .line 151
    iget-object v0, p0, Lajv;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    iget-object v1, p0, Lajv;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    const v2, 0x7f0a055d

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 157
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 146
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lajv;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->invalidateOptionsMenu()V

    .line 142
    return-void
.end method
