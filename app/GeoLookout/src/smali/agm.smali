.class public final Lagm;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final action_bar_switch_padding:I = 0x7f090000

.field public static final activity_horizontal_margin:I = 0x7f090001

.field public static final activity_vertical_margin:I = 0x7f090002

.field public static final dashboard_contact_body_height:I = 0x7f090003

.field public static final dashboard_contact_image_below_margin:I = 0x7f090004

.field public static final dashboard_contact_image_height:I = 0x7f090005

.field public static final dashboard_contact_image_image_margin:I = 0x7f090006

.field public static final dashboard_contact_image_inside_padding:I = 0x7f090007

.field public static final dashboard_contact_image_left_margin:I = 0x7f090008

.field public static final dashboard_contact_image_top_margin:I = 0x7f090009

.field public static final dashboard_contact_imageframe_height:I = 0x7f09000a

.field public static final dashboard_contact_name_height:I = 0x7f09000b

.field public static final dashboard_contact_name_size:I = 0x7f09000c

.field public static final dashboard_contactbar_height:I = 0x7f09000d

.field public static final dashboard_contactbar_text:I = 0x7f09000e

.field public static final dashboard_emergencymode_height:I = 0x7f09000f

.field public static final dashboard_help_button_width:I = 0x7f090010

.field public static final dashboard_margin_bar:I = 0x7f090011

.field public static final dashboard_margin_left:I = 0x7f090012

.field public static final dashboard_msg_btn_below_margin:I = 0x7f090013

.field public static final dashboard_msg_btn_center_margin:I = 0x7f090014

.field public static final dashboard_msg_btn_side_margin:I = 0x7f090015

.field public static final dashboard_msg_button_height:I = 0x7f090016

.field public static final dashboard_msgbody_height:I = 0x7f090017

.field public static final dashboard_no_disaster_alert_type_margin_left:I = 0x7f090018

.field public static final dashboard_no_disaster_alert_type_margin_right:I = 0x7f090019

.field public static final dashboard_no_disaster_description_margin_bottom:I = 0x7f09001a

.field public static final dashboard_no_disaster_description_text:I = 0x7f09001b

.field public static final dashboard_no_disaster_icon_margin_right:I = 0x7f09001c

.field public static final dashboard_no_disaster_icon_width:I = 0x7f09001d

.field public static final dashboard_no_disaster_layout_margin_bottom:I = 0x7f09001e

.field public static final dashboard_no_disaster_share_layout_margin_bottom:I = 0x7f09001f

.field public static final dashboard_no_disaster_title_height:I = 0x7f090020

.field public static final dashboard_no_disaster_title_margin_left:I = 0x7f090021

.field public static final dashboard_no_disaster_title_text:I = 0x7f090022

.field public static final dashboard_no_disaster_title_wrapper_margin_bottom:I = 0x7f090023

.field public static final dashboard_no_disaster_title_wrapper_margin_left:I = 0x7f090024

.field public static final dashboard_no_disaster_title_wrapper_margin_right:I = 0x7f090025

.field public static final dashboard_ok_button_width:I = 0x7f090026

.field public static final dialog_body_textsize:I = 0x7f090027

.field public static final dialog_bodyin_margin:I = 0x7f090028

.field public static final dialog_button_call_height:I = 0x7f090029

.field public static final dialog_button_call_textsize:I = 0x7f09002a

.field public static final dialog_button_textsize:I = 0x7f09002b

.field public static final dialog_contact_call_height:I = 0x7f09002c

.field public static final dialog_contact_company_bottom_margin:I = 0x7f09002d

.field public static final dialog_contact_company_height:I = 0x7f09002e

.field public static final dialog_contact_company_size:I = 0x7f09002f

.field public static final dialog_contact_height:I = 0x7f090030

.field public static final dialog_contact_image_height:I = 0x7f090031

.field public static final dialog_contact_image_top_margin:I = 0x7f090032

.field public static final dialog_contact_imageshadow_height:I = 0x7f090033

.field public static final dialog_contact_msgbody_height:I = 0x7f090034

.field public static final dialog_contact_name_bottom_margin:I = 0x7f090035

.field public static final dialog_contact_name_height:I = 0x7f090036

.field public static final dialog_contact_name_size:I = 0x7f090037

.field public static final dialog_contact_width:I = 0x7f090038

.field public static final dialog_env_image_height:I = 0x7f090039

.field public static final dialog_horizontal_margin:I = 0x7f09003a

.field public static final dialog_title_height:I = 0x7f09003b

.field public static final dialog_title_textsize:I = 0x7f09003c

.field public static final dialog_vertical_margin:I = 0x7f09003d

.field public static final history_list_item_height:I = 0x7f09003e

.field public static final lifemode_actionbar_changemode_menu_botton_height:I = 0x7f09003f

.field public static final lifemode_actionbar_changemode_menu_botton_padding_bottom:I = 0x7f090040

.field public static final lifemode_actionbar_changemode_menu_botton_padding_left:I = 0x7f090041

.field public static final lifemode_actionbar_changemode_menu_botton_padding_right:I = 0x7f090042

.field public static final lifemode_actionbar_changemode_menu_botton_padding_top:I = 0x7f090043

.field public static final lifemode_actionbar_changemode_menu_botton_width:I = 0x7f090044

.field public static final lifemode_actionbar_layout_height:I = 0x7f090045

.field public static final lifemode_cocktailbar_app_icon_layout_height:I = 0x7f090046

.field public static final lifemode_cocktailbar_app_icon_width_height:I = 0x7f090047

.field public static final lifemode_cocktailbar_header_layout_height:I = 0x7f090048

.field public static final lifemode_cocktailbar_icon_width:I = 0x7f090049

.field public static final lifemode_cocktailbar_info_layout_height:I = 0x7f09004a

.field public static final lifemode_cocktailbar_info_layout_padding_bottom:I = 0x7f09004b

.field public static final lifemode_cocktailbar_info_location_layout_padding_left:I = 0x7f09004c

.field public static final lifemode_cocktailbar_info_location_layout_width:I = 0x7f09004d

.field public static final lifemode_cocktailbar_info_location_textview_height:I = 0x7f09004e

.field public static final lifemode_cocktailbar_info_location_textview_textsize:I = 0x7f09004f

.field public static final lifemode_cocktailbar_info_location_textview_width:I = 0x7f090050

.field public static final lifemode_cocktailbar_info_textviews_height:I = 0x7f090051

.field public static final lifemode_cocktailbar_info_time_layout_padding_left:I = 0x7f090052

.field public static final lifemode_cocktailbar_info_time_layout_width:I = 0x7f090053

.field public static final lifemode_cocktailbar_info_time_textview_height:I = 0x7f090054

.field public static final lifemode_cocktailbar_info_time_textview_textsize:I = 0x7f090055

.field public static final lifemode_cocktailbar_info_time_textview_width:I = 0x7f090056

.field public static final lifemode_cocktailbar_refresh_btn_layout_height:I = 0x7f090057

.field public static final lifemode_cocktailbar_refresh_btn_layout_margin_top:I = 0x7f090058

.field public static final lifemode_cocktailbar_refresh_imageview_margin_top:I = 0x7f090059

.field public static final lifemode_cocktailbar_refresh_imageview_width_height:I = 0x7f09005a

.field public static final lifemode_cocktailbar_roaming_textview_layout_height:I = 0x7f09005b

.field public static final lifemode_cocktailbar_roaming_textview_width:I = 0x7f09005c

.field public static final lifemode_cocktailbar_status_items_icon_img_layout_height:I = 0x7f09005d

.field public static final lifemode_cocktailbar_status_items_icon_img_layout_width:I = 0x7f09005e

.field public static final lifemode_cocktailbar_status_items_layout_fadeout_imageview_height:I = 0x7f09005f

.field public static final lifemode_cocktailbar_status_items_layout_height:I = 0x7f090060

.field public static final lifemode_cocktailbar_status_items_layout_padding_bottom:I = 0x7f090061

.field public static final lifemode_cocktailbar_status_items_layout_padding_top:I = 0x7f090062

.field public static final lifemode_cocktailbar_status_items_name_textview_height:I = 0x7f090063

.field public static final lifemode_cocktailbar_status_items_padding_top:I = 0x7f090064

.field public static final lifemode_cocktailbar_status_items_scrollview_layout_height:I = 0x7f090065

.field public static final lifemode_cocktailbar_status_items_scrollview_layout_padding_bottom:I = 0x7f090066

.field public static final lifemode_cocktailbar_status_items_scrollview_layout_padding_top:I = 0x7f090067

.field public static final lifemode_cocktailbar_status_items_textviews_layout_height:I = 0x7f090068

.field public static final lifemode_cocktailbar_status_items_warningnone_img_layout_height:I = 0x7f090069

.field public static final lifemode_drawer_item_min_height:I = 0x7f09006a

.field public static final lifemode_drawer_width:I = 0x7f09006b

.field public static final lifemode_listview_item_row_layout_checkbox_widthheight:I = 0x7f09006c

.field public static final lifemode_listview_item_row_layout_childview_item_bottomlayout_margin_between_details_index_info:I = 0x7f09006d

.field public static final lifemode_listview_item_row_layout_childview_item_bottomlayout_margin_between_head_bottom:I = 0x7f09006e

.field public static final lifemode_listview_item_row_layout_childview_item_bottomlayout_padding_bottom:I = 0x7f09006f

.field public static final lifemode_listview_item_row_layout_childview_item_bottomlayout_padding_leftright:I = 0x7f090070

.field public static final lifemode_listview_item_row_layout_childview_item_bottomlayout_padding_top:I = 0x7f090071

.field public static final lifemode_listview_item_row_layout_childview_item_entire_height:I = 0x7f090072

.field public static final lifemode_listview_item_row_layout_childview_item_head_text_size:I = 0x7f090073

.field public static final lifemode_listview_item_row_layout_childview_item_map_height:I = 0x7f090074

.field public static final lifemode_listview_item_row_layout_childview_item_text_size:I = 0x7f090075

.field public static final lifemode_listview_item_row_layout_childview_item_textinfo_height:I = 0x7f090076

.field public static final lifemode_listview_item_row_layout_divider_height:I = 0x7f090077

.field public static final lifemode_listview_item_row_layout_height:I = 0x7f090078

.field public static final lifemode_listview_item_row_layout_padding_left:I = 0x7f090079

.field public static final lifemode_listview_item_row_layout_padding_right:I = 0x7f09007a

.field public static final lifemode_listview_item_row_layout_padding_topbottom:I = 0x7f09007b

.field public static final lifemode_listview_item_row_layout_status_detail_text_size:I = 0x7f09007c

.field public static final lifemode_listview_item_row_layout_status_img_margin_right:I = 0x7f09007d

.field public static final lifemode_listview_item_row_layout_status_img_widthheight:I = 0x7f09007e

.field public static final lifemode_listview_item_row_layout_status_text_height:I = 0x7f09007f

.field public static final lifemode_listview_item_row_layout_status_text_margin_right:I = 0x7f090080

.field public static final lifemode_listview_item_row_layout_status_text_margin_top:I = 0x7f090081

.field public static final lifemode_listview_item_row_layout_status_text_size:I = 0x7f090082

.field public static final lifemode_listview_item_row_layout_status_text_width:I = 0x7f090083

.field public static final lifemode_listview_item_row_layout_status_warningnone_bg_img_height:I = 0x7f090084

.field public static final lifemode_listview_item_row_layout_status_warningnone_bg_img_width:I = 0x7f090085

.field public static final lifemode_listview_item_row_layout_status_warningnone_margin_right:I = 0x7f090086

.field public static final lifemode_listview_item_row_layout_status_warningnone_text_size:I = 0x7f090087

.field public static final lifemode_listview_item_row_layout_status_warningnone_width:I = 0x7f090088

.field public static final lifemode_listview_layout_height:I = 0x7f090089

.field public static final lifemode_mainscreen_layout_height:I = 0x7f09008a

.field public static final lifemode_mainscreen_status_line_layout_height:I = 0x7f09008b

.field public static final lifemode_mainscreen_status_line_location_textview_margin_left:I = 0x7f09008c

.field public static final lifemode_mainscreen_status_line_location_textview_padding_top:I = 0x7f09008d

.field public static final lifemode_mainscreen_status_line_location_textview_textsize:I = 0x7f09008e

.field public static final lifemode_mainscreen_status_line_location_textview_width:I = 0x7f09008f

.field public static final lifemode_mainscreen_status_line_refresh_btn_image_margin_right:I = 0x7f090090

.field public static final lifemode_mainscreen_status_line_refresh_btn_image_margin_top:I = 0x7f090091

.field public static final lifemode_mainscreen_status_line_refresh_btn_image_width_height:I = 0x7f090092

.field public static final lifemode_mainscreen_status_line_time_textview_margin_left:I = 0x7f090093

.field public static final lifemode_mainscreen_status_line_time_textview_padding_top:I = 0x7f090094

.field public static final lifemode_mainscreen_status_line_time_textview_textsize:I = 0x7f090095

.field public static final lifemode_mainscreen_status_line_time_textview_width:I = 0x7f090096

.field public static final lifemode_safety_roaming_layout_imageview_margin_top:I = 0x7f090097

.field public static final lifemode_safety_roaming_layout_imageview_width_height:I = 0x7f090098

.field public static final lifemode_safety_roaming_layout_textview_height:I = 0x7f090099

.field public static final lifemode_safety_roaming_layout_textview_text_size:I = 0x7f09009a

.field public static final lifemode_safety_tools_icon_framelayout_height:I = 0x7f09009b

.field public static final lifemode_safety_tools_icon_framelayout_margin_leftright:I = 0x7f09009c

.field public static final lifemode_safety_tools_icon_framelayout_width:I = 0x7f09009d

.field public static final lifemode_safety_tools_icon_img_height:I = 0x7f09009e

.field public static final lifemode_safety_tools_icon_img_margin_left:I = 0x7f09009f

.field public static final lifemode_safety_tools_icon_img_width:I = 0x7f0900a0

.field public static final lifemode_safety_tools_icon_list_layout_height:I = 0x7f0900a1

.field public static final lifemode_safety_tools_icon_list_layout_padding_bottom:I = 0x7f0900a2

.field public static final lifemode_safety_tools_icon_list_layout_padding_left:I = 0x7f0900a3

.field public static final lifemode_safety_tools_icon_list_layout_padding_right:I = 0x7f0900a4

.field public static final lifemode_safety_tools_icon_list_layout_padding_top:I = 0x7f0900a5

.field public static final lifemode_safety_tools_icon_margin_between_icon_text:I = 0x7f0900a6

.field public static final lifemode_safety_tools_icon_text_height:I = 0x7f0900a7

.field public static final lifemode_safety_tools_icon_text_size:I = 0x7f0900a8

.field public static final lifemode_safety_tools_icon_text_width:I = 0x7f0900a9

.field public static final lifemode_status_item_map_icon_img_margin_leftright:I = 0x7f0900aa

.field public static final lifemode_status_item_map_icon_img_padding_left_for_akita:I = 0x7f0900ab

.field public static final lifemode_status_item_map_icon_img_padding_left_for_baengnyeong:I = 0x7f0900ac

.field public static final lifemode_status_item_map_icon_img_padding_left_for_busan:I = 0x7f0900ad

.field public static final lifemode_status_item_map_icon_img_padding_left_for_cheongju:I = 0x7f0900ae

.field public static final lifemode_status_item_map_icon_img_padding_left_for_daegu:I = 0x7f0900af

.field public static final lifemode_status_item_map_icon_img_padding_left_for_daejeon:I = 0x7f0900b0

.field public static final lifemode_status_item_map_icon_img_padding_left_for_dakamatsu:I = 0x7f0900b1

.field public static final lifemode_status_item_map_icon_img_padding_left_for_gagoshima:I = 0x7f0900b2

.field public static final lifemode_status_item_map_icon_img_padding_left_for_gangneung:I = 0x7f0900b3

.field public static final lifemode_status_item_map_icon_img_padding_left_for_gwangju:I = 0x7f0900b4

.field public static final lifemode_status_item_map_icon_img_padding_left_for_hiroshima:I = 0x7f0900b5

.field public static final lifemode_status_item_map_icon_img_padding_left_for_hukuoka:I = 0x7f0900b6

.field public static final lifemode_status_item_map_icon_img_padding_left_for_jejudo:I = 0x7f0900b7

.field public static final lifemode_status_item_map_icon_img_padding_left_for_jeonju:I = 0x7f0900b8

.field public static final lifemode_status_item_map_icon_img_padding_left_for_kanazawa:I = 0x7f0900b9

.field public static final lifemode_status_item_map_icon_img_padding_left_for_kusiro:I = 0x7f0900ba

.field public static final lifemode_status_item_map_icon_img_padding_left_for_nagoya:I = 0x7f0900bb

.field public static final lifemode_status_item_map_icon_img_padding_left_for_naha:I = 0x7f0900bc

.field public static final lifemode_status_item_map_icon_img_padding_left_for_osaka:I = 0x7f0900bd

.field public static final lifemode_status_item_map_icon_img_padding_left_for_sapporo:I = 0x7f0900be

.field public static final lifemode_status_item_map_icon_img_padding_left_for_sendai:I = 0x7f0900bf

.field public static final lifemode_status_item_map_icon_img_padding_left_for_seoul:I = 0x7f0900c0

.field public static final lifemode_status_item_map_icon_img_padding_left_for_tokyo:I = 0x7f0900c1

.field public static final lifemode_status_item_map_icon_img_padding_left_for_ullenung:I = 0x7f0900c2

.field public static final lifemode_status_item_map_icon_img_padding_right_for_akita:I = 0x7f0900c3

.field public static final lifemode_status_item_map_icon_img_padding_right_for_baengnyeong:I = 0x7f0900c4

.field public static final lifemode_status_item_map_icon_img_padding_right_for_busan:I = 0x7f0900c5

.field public static final lifemode_status_item_map_icon_img_padding_right_for_cheongju:I = 0x7f0900c6

.field public static final lifemode_status_item_map_icon_img_padding_right_for_daegu:I = 0x7f0900c7

.field public static final lifemode_status_item_map_icon_img_padding_right_for_daejeon:I = 0x7f0900c8

.field public static final lifemode_status_item_map_icon_img_padding_right_for_dakamatsu:I = 0x7f0900c9

.field public static final lifemode_status_item_map_icon_img_padding_right_for_gagoshima:I = 0x7f0900ca

.field public static final lifemode_status_item_map_icon_img_padding_right_for_gangneung:I = 0x7f0900cb

.field public static final lifemode_status_item_map_icon_img_padding_right_for_gwangju:I = 0x7f0900cc

.field public static final lifemode_status_item_map_icon_img_padding_right_for_hiroshima:I = 0x7f0900cd

.field public static final lifemode_status_item_map_icon_img_padding_right_for_hukuoka:I = 0x7f0900ce

.field public static final lifemode_status_item_map_icon_img_padding_right_for_jejudo:I = 0x7f0900cf

.field public static final lifemode_status_item_map_icon_img_padding_right_for_jeonju:I = 0x7f0900d0

.field public static final lifemode_status_item_map_icon_img_padding_right_for_kanazawa:I = 0x7f0900d1

.field public static final lifemode_status_item_map_icon_img_padding_right_for_kusiro:I = 0x7f0900d2

.field public static final lifemode_status_item_map_icon_img_padding_right_for_nagoya:I = 0x7f0900d3

.field public static final lifemode_status_item_map_icon_img_padding_right_for_naha:I = 0x7f0900d4

.field public static final lifemode_status_item_map_icon_img_padding_right_for_osaka:I = 0x7f0900d5

.field public static final lifemode_status_item_map_icon_img_padding_right_for_sapporo:I = 0x7f0900d6

.field public static final lifemode_status_item_map_icon_img_padding_right_for_sendai:I = 0x7f0900d7

.field public static final lifemode_status_item_map_icon_img_padding_right_for_seoul:I = 0x7f0900d8

.field public static final lifemode_status_item_map_icon_img_padding_right_for_tokyo:I = 0x7f0900d9

.field public static final lifemode_status_item_map_icon_img_padding_right_for_ullenung:I = 0x7f0900da

.field public static final lifemode_status_item_map_icon_img_padding_top_for_akita:I = 0x7f0900db

.field public static final lifemode_status_item_map_icon_img_padding_top_for_baengnyeong:I = 0x7f0900dc

.field public static final lifemode_status_item_map_icon_img_padding_top_for_busan:I = 0x7f0900dd

.field public static final lifemode_status_item_map_icon_img_padding_top_for_cheongju:I = 0x7f0900de

.field public static final lifemode_status_item_map_icon_img_padding_top_for_daegu:I = 0x7f0900df

.field public static final lifemode_status_item_map_icon_img_padding_top_for_daejeon:I = 0x7f0900e0

.field public static final lifemode_status_item_map_icon_img_padding_top_for_dakamatsu:I = 0x7f0900e1

.field public static final lifemode_status_item_map_icon_img_padding_top_for_gagoshima:I = 0x7f0900e2

.field public static final lifemode_status_item_map_icon_img_padding_top_for_gangneung:I = 0x7f0900e3

.field public static final lifemode_status_item_map_icon_img_padding_top_for_gwangju:I = 0x7f0900e4

.field public static final lifemode_status_item_map_icon_img_padding_top_for_hiroshima:I = 0x7f0900e5

.field public static final lifemode_status_item_map_icon_img_padding_top_for_hukuoka:I = 0x7f0900e6

.field public static final lifemode_status_item_map_icon_img_padding_top_for_jejudo:I = 0x7f0900e7

.field public static final lifemode_status_item_map_icon_img_padding_top_for_jeonju:I = 0x7f0900e8

.field public static final lifemode_status_item_map_icon_img_padding_top_for_kanazawa:I = 0x7f0900e9

.field public static final lifemode_status_item_map_icon_img_padding_top_for_kusiro:I = 0x7f0900ea

.field public static final lifemode_status_item_map_icon_img_padding_top_for_nagoya:I = 0x7f0900eb

.field public static final lifemode_status_item_map_icon_img_padding_top_for_naha:I = 0x7f0900ec

.field public static final lifemode_status_item_map_icon_img_padding_top_for_osaka:I = 0x7f0900ed

.field public static final lifemode_status_item_map_icon_img_padding_top_for_sapporo:I = 0x7f0900ee

.field public static final lifemode_status_item_map_icon_img_padding_top_for_sendai:I = 0x7f0900ef

.field public static final lifemode_status_item_map_icon_img_padding_top_for_seoul:I = 0x7f0900f0

.field public static final lifemode_status_item_map_icon_img_padding_top_for_tokyo:I = 0x7f0900f1

.field public static final lifemode_status_item_map_icon_img_padding_top_for_ullenung:I = 0x7f0900f2

.field public static final lifemode_status_item_map_icon_img_width_height:I = 0x7f0900f3

.field public static final lifemode_status_item_map_icon_text_margin_top:I = 0x7f0900f4

.field public static final lifemode_status_item_map_icon_text_size:I = 0x7f0900f5

.field public static final lifemode_widget_bottom_all_teims_layout_height:I = 0x7f0900f6

.field public static final lifemode_widget_bottom_datetext_height:I = 0x7f0900f7

.field public static final lifemode_widget_bottom_datetext_margin_bottom:I = 0x7f0900f8

.field public static final lifemode_widget_bottom_datetext_margin_right:I = 0x7f0900f9

.field public static final lifemode_widget_bottom_datetext_text_size:I = 0x7f0900fa

.field public static final lifemode_widget_bottom_refresh_image_margin_right_bottom:I = 0x7f0900fb

.field public static final lifemode_widget_bottom_refresh_image_width_height:I = 0x7f0900fc

.field public static final lifemode_widget_each_item_bottom_value_item_bg_image_height:I = 0x7f0900fd

.field public static final lifemode_widget_each_item_bottom_value_item_bg_image_width:I = 0x7f0900fe

.field public static final lifemode_widget_each_item_bottom_value_item_text_size:I = 0x7f0900ff

.field public static final lifemode_widget_each_item_layout_height:I = 0x7f090100

.field public static final lifemode_widget_each_item_layout_padding_top:I = 0x7f090101

.field public static final lifemode_widget_each_item_layout_width:I = 0x7f090102

.field public static final lifemode_widget_each_item_middle_status_item_bottom_value_item_layout_height:I = 0x7f090103

.field public static final lifemode_widget_each_item_middle_status_item_icon_margin_top:I = 0x7f090104

.field public static final lifemode_widget_each_item_middle_status_item_icon_width_height:I = 0x7f090105

.field public static final lifemode_widget_each_item_top_status_item_text_layout_height:I = 0x7f090106

.field public static final lifemode_widget_each_item_top_status_item_text_size:I = 0x7f090107

.field public static final lifemode_widget_main_items_layout_height:I = 0x7f090108

.field public static final lifemode_widget_main_items_layout_width:I = 0x7f090109

.field public static final safetycare_disaster_dashboard_description_for_open:I = 0x7f09010a

.field public static final safetycare_discalimer_height_for_open_horizontal:I = 0x7f09010b

.field public static final safetycare_discalimer_height_for_open_vertical:I = 0x7f09010c

.field public static final safetycare_discalimer_height_for_usa_horizontal:I = 0x7f09010d

.field public static final safetycare_discalimer_height_for_usa_vertical:I = 0x7f09010e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
