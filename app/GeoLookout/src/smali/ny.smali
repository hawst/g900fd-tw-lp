.class public final Lny;
.super Ldu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldu",
        "<",
        "Lnr;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lny;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lny;

    invoke-direct {v0}, Lny;-><init>()V

    sput-object v0, Lny;->a:Lny;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const-string v0, "com.google.android.gms.common.ui.SignInButtonCreatorImpl"

    invoke-direct {p0, v0}, Ldu;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;II)Landroid/view/View;
    .locals 1

    sget-object v0, Lny;->a:Lny;

    invoke-direct {v0, p0, p1, p2}, Lny;->b(Landroid/content/Context;II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/content/Context;II)Landroid/view/View;
    .locals 4

    :try_start_0
    invoke-static {p1}, Lds;->a(Ljava/lang/Object;)Ldp;

    move-result-object v1

    invoke-virtual {p0, p1}, Lny;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnr;

    invoke-interface {v0, v1, p2, p3}, Lnr;->a(Ldp;II)Ldp;

    move-result-object v0

    invoke-static {v0}, Lds;->a(Ldp;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ldv;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not get button with size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and color "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ldv;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public synthetic a(Landroid/os/IBinder;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lny;->b(Landroid/os/IBinder;)Lnr;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/os/IBinder;)Lnr;
    .locals 1

    invoke-static {p1}, Lns;->a(Landroid/os/IBinder;)Lnr;

    move-result-object v0

    return-object v0
.end method
