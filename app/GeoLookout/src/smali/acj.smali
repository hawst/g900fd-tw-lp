.class public Lacj;
.super Ljava/lang/Object;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcb;

.field private final c:Lcc;

.field private final d:Lsz;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcb;Lcc;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lacj;->a:Landroid/content/Context;

    iput-object p2, p0, Lacj;->b:Lcb;

    iput-object p3, p0, Lacj;->c:Lcc;

    new-instance v0, Lsz;

    iget-object v1, p0, Lacj;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lsz;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lacj;->d:Lsz;

    return-void
.end method


# virtual methods
.method public a()Lacj;
    .locals 1

    iget-object v0, p0, Lacj;->d:Lsz;

    invoke-virtual {v0}, Lsz;->a()Lsz;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lacj;
    .locals 1

    iget-object v0, p0, Lacj;->d:Lsz;

    invoke-virtual {v0, p1}, Lsz;->a(Ljava/lang/String;)Lsz;

    return-object p0
.end method

.method public varargs a([Ljava/lang/String;)Lacj;
    .locals 1

    iget-object v0, p0, Lacj;->d:Lsz;

    invoke-virtual {v0, p1}, Lsz;->a([Ljava/lang/String;)Lsz;

    return-object p0
.end method

.method public b()Laci;
    .locals 6

    new-instance v0, Laci;

    new-instance v1, Lsq;

    iget-object v2, p0, Lacj;->a:Landroid/content/Context;

    iget-object v3, p0, Lacj;->d:Lsz;

    invoke-virtual {v3}, Lsz;->b()Lcom/google/android/gms/internal/fn;

    move-result-object v3

    iget-object v4, p0, Lacj;->b:Lcb;

    iget-object v5, p0, Lacj;->c:Lcc;

    invoke-direct {v1, v2, v3, v4, v5}, Lsq;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/fn;Lcb;Lcc;)V

    invoke-direct {v0, v1}, Laci;-><init>(Lsq;)V

    return-object v0
.end method

.method public varargs b([Ljava/lang/String;)Lacj;
    .locals 1

    iget-object v0, p0, Lacj;->d:Lsz;

    invoke-virtual {v0, p1}, Lsz;->b([Ljava/lang/String;)Lsz;

    return-object p0
.end method
