.class public Laox;
.super Landroid/preference/PreferenceFragment;
.source "DisasterSettingsFilterListActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field a:[I

.field b:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 72
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Laox;->a:[I

    .line 81
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Laox;->b:[I

    return-void

    .line 72
    nop

    :array_0
    .array-data 4
        0x69
        0x72
        0xca
        0x6c
        0xcb
        0x191
        0x6f
        0xcc
        0x197
    .end array-data

    .line 81
    :array_1
    .array-data 4
        0xce
        0x72
        0xca
        0x6c
        0xcb
        0xcc
        0x197
    .end array-data
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 117
    move v1, v2

    :goto_0
    iget-object v0, p0, Laox;->a:[I

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 118
    sget-object v0, Lakx;->eR:Landroid/util/SparseArray;

    iget-object v3, p0, Laox;->a:[I

    aget v3, v3, v1

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Laox;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 119
    if-eqz v0, :cond_0

    .line 120
    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 117
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 123
    :cond_1
    :goto_1
    iget-object v0, p0, Laox;->b:[I

    array-length v0, v0

    if-ge v2, v0, :cond_3

    .line 124
    sget-object v0, Lakx;->eS:Landroid/util/SparseArray;

    iget-object v1, p0, Laox;->b:[I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Laox;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 125
    if-eqz v0, :cond_2

    .line 126
    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 123
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 130
    :cond_3
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 102
    invoke-virtual {p0}, Laox;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 103
    const v1, 0x7f030008

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 104
    const v0, 0x7f0e0039

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/settings/DisasterSettingsFilterListActivity;->a(Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 106
    invoke-virtual {p0}, Laox;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/GeoLookout/settings/DisasterSettingsFilterListActivity;->a(Landroid/widget/ListView;)Landroid/widget/ListView;

    .line 107
    invoke-static {}, Lcom/sec/android/GeoLookout/settings/DisasterSettingsFilterListActivity;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 109
    invoke-static {}, Lcom/sec/android/GeoLookout/settings/DisasterSettingsFilterListActivity;->b()Landroid/widget/TextView;

    move-result-object v0

    const v2, 0x7f0a004d

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 111
    invoke-virtual {p0}, Laox;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 113
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 114
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 92
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 94
    const/high16 v0, 0x7f050000

    invoke-virtual {p0, v0}, Laox;->addPreferencesFromResource(I)V

    .line 95
    invoke-direct {p0}, Laox;->a()V

    .line 97
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 137
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 138
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "key = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", newValue = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 139
    invoke-static {}, Lcom/sec/android/GeoLookout/settings/DisasterSettingsFilterListActivity;->c()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v1, v2, v0}, Lall;->b(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 140
    const/4 v0, 0x1

    return v0
.end method
