.class public Lapf;
.super Lapd;
.source "PushMessageTask.java"


# instance fields
.field private b:Landroid/content/Context;

.field private c:Lamf;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lalo;Lalr;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lapd;-><init>(Landroid/content/Context;Lalo;Lalr;)V

    .line 38
    iput-object p1, p0, Lapf;->b:Landroid/content/Context;

    .line 39
    invoke-virtual {p0}, Lapf;->c()V

    .line 40
    return-void
.end method

.method private a(Lcom/sec/android/GeoLookout/db/DisasterInfo;I)I
    .locals 2

    .prologue
    .line 388
    .line 390
    const-string v0, "getSignificanceOfSpotMessage"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 393
    if-gez p2, :cond_0

    .line 394
    const/4 v0, 0x1

    .line 401
    :goto_0
    return v0

    .line 395
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v0

    const/16 v1, 0x69

    if-ne v0, v1, :cond_1

    .line 396
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailsMagnitude()D

    move-result-wide v0

    invoke-static {v0, v1, p2}, Lall;->a(DI)I

    move-result v0

    goto :goto_0

    .line 398
    :cond_1
    invoke-static {p2}, Lall;->a(I)I

    move-result v0

    goto :goto_0
.end method

.method private e(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 88
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lall;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    const-string v0, "Push message has different dv from current dv"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 127
    :goto_0
    return-void

    .line 93
    :cond_0
    const-string v0, "03"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {p0, v0, p1}, Lapf;->c(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 95
    const-string v0, "disaster is filtered in settings"

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 100
    :cond_1
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    const-wide/16 v2, 0x0

    invoke-static {v0, v2, v3}, Lalx;->a(Landroid/content/Context;J)V

    .line 102
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getPolling()S

    move-result v0

    if-ne v0, v4, :cond_2

    invoke-virtual {p0}, Lapf;->b()Lalr;

    move-result-object v0

    invoke-virtual {v0}, Lalr;->s()Z

    move-result v0

    if-nez v0, :cond_2

    .line 103
    const-string v0, "send request to disaster server for getting HT/HP"

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 104
    invoke-virtual {p0}, Lapf;->b()Lalr;

    move-result-object v0

    invoke-virtual {v0}, Lalr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lapf;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 108
    :cond_2
    const-string v0, "handleNewPushMessage : create validate Disasterinfo"

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0, p1, v4}, Lapf;->b(Lcom/sec/android/GeoLookout/db/DisasterInfo;Z)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 111
    if-nez v0, :cond_3

    .line 112
    const-string v0, "handleNewPushMessage : createValidDisasterInfo return null"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 116
    :cond_3
    const-string v1, "handleNewPushMessage : save disaster info to DB"

    invoke-static {v1}, Lalj;->a(Ljava/lang/String;)V

    .line 117
    invoke-virtual {p0, v0}, Lapf;->c(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 119
    if-nez v0, :cond_4

    .line 120
    const-string v0, "handleNewPushMessage : saveValidDisasterInfo return null"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 124
    :cond_4
    const-string v1, "handleNewPushMessage : notify new push message"

    invoke-static {v1}, Lalj;->a(Ljava/lang/String;)V

    .line 125
    invoke-virtual {p0, v0}, Lapf;->d(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    goto :goto_0
.end method

.method private f(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 14

    .prologue
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    .line 360
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getChnDetailsEqType()Ljava/lang/String;

    move-result-object v0

    .line 361
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v1

    .line 363
    const/16 v2, 0x69

    if-ne v1, v2, :cond_0

    .line 364
    if-eqz v0, :cond_1

    const-string v1, "A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 365
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lall;->h(Landroid/content/Context;Ljava/lang/String;)V

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 367
    :cond_1
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v0}, Lall;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 368
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 369
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "lastdid = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 370
    iget-object v1, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lalx;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 371
    if-eqz v0, :cond_0

    .line 372
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v1

    iget-wide v2, v1, Lamf;->b:D

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v1

    iget-wide v4, v1, Lamf;->b:D

    sub-double/2addr v2, v4

    .line 373
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v1

    iget-wide v4, v1, Lamf;->a:D

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v1

    iget-wide v6, v1, Lamf;->a:D

    sub-double/2addr v4, v6

    .line 374
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailsMagnitude()D

    move-result-wide v6

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailsMagnitude()D

    move-result-wide v8

    sub-double/2addr v6, v8

    .line 375
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getStartTime()J

    move-result-wide v8

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getStartTime()J

    move-result-wide v10

    sub-long/2addr v8, v10

    long-to-double v8, v8

    .line 377
    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    cmpg-double v1, v2, v12

    if-gtz v1, :cond_0

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    cmpg-double v1, v2, v12

    if-gtz v1, :cond_0

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    cmpg-double v1, v2, v12

    if-gtz v1, :cond_0

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x40ed4c0000000000L    # 60000.0

    cmpg-double v1, v2, v4

    if-gtz v1, :cond_0

    .line 378
    invoke-virtual {p0, v0}, Lapf;->b(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 379
    const-string v0, "checkValidEarthquake Delete A type Earthquake of Duplcate case"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method protected a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 4

    .prologue
    .line 131
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getEndTime()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lalx;->b(Landroid/content/Context;Ljava/lang/String;J)V

    .line 134
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lapf;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;Z)V

    .line 135
    return-void
.end method

.method protected a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 334
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v0}, Lall;->l(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 337
    if-eqz p2, :cond_2

    .line 338
    iget-object v1, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v1, p2}, Lalz;->b(Landroid/content/Context;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 339
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "requestTrackingInformation: Phone doesn\'t have uid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 356
    :goto_0
    return-void

    .line 342
    :cond_0
    iget-object v1, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v1, p2}, Lalz;->c(Landroid/content/Context;I)Lamf;

    move-result-object v1

    .line 343
    if-nez v1, :cond_1

    .line 344
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "requestTrackingInformation: location of buddy is null "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 347
    :cond_1
    iput-object v1, p0, Lapf;->c:Lamf;

    .line 351
    :cond_2
    iget-object v1, p0, Lapf;->c:Lamf;

    if-eqz v1, :cond_3

    .line 352
    invoke-virtual {p0}, Lapf;->a()Lalo;

    move-result-object v1

    invoke-virtual {v1}, Lalo;->a()Lanh;

    move-result-object v1

    iget-object v2, p0, Lapf;->c:Lamf;

    invoke-virtual {v1, p1, v0, v2}, Lanh;->a(Ljava/lang/String;Ljava/lang/String;Lamf;)V

    goto :goto_0

    .line 354
    :cond_3
    const-string v0, "requestTrackingInformation: location is null"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected b(Lcom/sec/android/GeoLookout/db/DisasterInfo;Z)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 170
    const-string v1, "checkDisasterInfoIsValid()"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 172
    if-nez p1, :cond_0

    .line 243
    :goto_0
    return-object v0

    .line 176
    :cond_0
    new-instance v2, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    invoke-direct {v2, p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;-><init>(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 179
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v1

    .line 180
    if-eqz v1, :cond_3

    .line 182
    iget-object v3, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v3, v1}, Lalz;->b(Landroid/content/Context;I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 183
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MULTIPOINT Phone doesn\'t have uid!! It will return null!! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 186
    :cond_1
    iget-object v1, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v3

    invoke-static {v1, v3}, Lalz;->c(Landroid/content/Context;I)Lamf;

    move-result-object v1

    .line 188
    if-eqz v1, :cond_2

    .line 189
    iput-object v1, p0, Lapf;->c:Lamf;

    .line 191
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Distance Multipoint : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lapf;->c:Lamf;

    iget-wide v4, v3, Lamf;->a:D

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " / "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lapf;->c:Lamf;

    iget-wide v4, v3, Lamf;->b:D

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 195
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationType()I

    move-result v3

    .line 196
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v1

    iget-object v4, p0, Lapf;->c:Lamf;

    invoke-static {v1, v4}, Lall;->a(Lamf;Lamf;)I

    move-result v4

    .line 197
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v1

    .line 199
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "checkDisasterInfoIsValid : distance ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lalj;->f(Ljava/lang/String;)V

    .line 200
    invoke-virtual {v2, v4}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDistance(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 202
    if-nez v3, :cond_8

    .line 203
    const-string v3, "02"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 204
    invoke-direct {p0, p1}, Lapf;->f(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 206
    :cond_4
    const-string v3, "03"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 207
    invoke-direct {p0, p1, v4}, Lapf;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;I)I

    move-result v1

    .line 209
    :cond_5
    const/4 v3, -0x1

    if-ne v1, v3, :cond_6

    .line 210
    const-string v1, "DisasterConstants.INVALID_SIGNIFICANCE"

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 214
    :cond_6
    invoke-virtual {v2, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setSignificance(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 223
    :cond_7
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getStartTime()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getEndTime()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lall;->a(JJ)Z

    move-result v1

    if-nez v1, :cond_9

    .line 224
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Disaster is eneded!!! oldSignificance:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " newSignificance:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 217
    :cond_8
    invoke-virtual {p0, p1, p2}, Lapf;->c(Lcom/sec/android/GeoLookout/db/DisasterInfo;Z)Z

    move-result v1

    if-nez v1, :cond_7

    .line 218
    const-string v1, "different location with current zone id"

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 230
    :cond_9
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailSpeed()Ljava/lang/String;

    move-result-object v0

    .line 231
    const-string v1, ","

    const-string v3, "."

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 232
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 234
    const-string v3, "01"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 235
    const-wide v4, 0x3fdc9c4da9003eeaL    # 0.44704

    mul-double/2addr v0, v4

    .line 236
    const-string v3, "%.1f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 237
    invoke-virtual {v2, v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailSpeed(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    :cond_a
    :goto_1
    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->build()Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    invoke-static {v0}, Lall;->c(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 240
    :catch_0
    move-exception v0

    goto :goto_1

    .line 239
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method b(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 5

    .prologue
    .line 138
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Lalx;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 140
    if-nez v0, :cond_0

    .line 141
    const-string v0, "handleCanceledPushMessage : original message is null"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 165
    :goto_0
    return-void

    .line 145
    :cond_0
    new-instance v1, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    invoke-direct {v1, v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;-><init>(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 146
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setActionStatus(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->build()Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 149
    invoke-static {}, Lalk;->a()Lalk;

    move-result-object v1

    iget-object v2, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lalk;->b(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 152
    invoke-static {}, Lalk;->a()Lalk;

    move-result-object v1

    iget-object v2, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getID()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lalk;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 155
    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lapf;->a(Ljava/lang/String;)V

    .line 158
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lapf;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;Z)V

    .line 161
    iget-object v1, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v1, v0}, Lalx;->a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 164
    iget-object v1, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lapf;->a(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method protected c(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 248
    if-nez p1, :cond_0

    .line 309
    :goto_0
    return-object v0

    .line 252
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveValidDisasterInfo : disasterInfo.getActionStatus()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getActionStatus()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->a(Ljava/lang/String;)V

    .line 255
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v1

    .line 256
    if-lez v1, :cond_1

    .line 257
    iget-object v2, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v2, v1}, Lalz;->b(Landroid/content/Context;I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 258
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MULTIPOINT Phone doesn\'t have uid!! It will return null!! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 262
    :cond_1
    iget-object v1, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMsgId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v3

    invoke-static {v1, v2, v3}, Lalx;->a(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 264
    const-string v1, "saveValidDisasterInfo : Exist same id for MPA"

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 274
    :cond_2
    iget-object v1, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v3

    int-to-long v4, v3

    invoke-static {v1, v2, v4, v5}, Lalx;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v1

    .line 275
    new-instance v2, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    invoke-direct {v2, p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;-><init>(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 277
    if-eqz v1, :cond_6

    .line 278
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "original.getID() "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getID()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lalj;->b(Ljava/lang/String;)V

    .line 280
    const-string v3, "04"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "03"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 281
    :cond_3
    const-string v3, "same DID : Delete old DB for Korea & Japan"

    invoke-static {v3}, Lalj;->b(Ljava/lang/String;)V

    .line 282
    invoke-static {}, Lalk;->a()Lalk;

    move-result-object v3

    iget-object v4, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getID()I

    move-result v5

    invoke-virtual {v3, v4, v5, v0}, Lalk;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 283
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v0, v1}, Lalx;->a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 285
    invoke-virtual {v2, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setActionStatus(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 286
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->build()Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v1

    invoke-static {v0, v1}, Lalx;->c(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)Landroid/net/Uri;

    .line 303
    :goto_1
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Lalx;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v1

    .line 307
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveValidDisasterInfo : getId()"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v1, :cond_7

    const-string v0, "null"

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    move-object v0, v1

    .line 309
    goto/16 :goto_0

    .line 288
    :cond_4
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getReceivedTime()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getReceivedTime()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-gez v0, :cond_5

    .line 289
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setActionStatus(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 290
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->build()Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v1

    invoke-static {v0, v1}, Lalx;->b(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 292
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Refresh] [DB] updateDisasterInfo  : did "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 294
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Refresh] original.getReceivedTime() is newer :::"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getReceivedTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " < "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getReceivedTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 298
    :cond_6
    invoke-virtual {v2, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setActionStatus(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 299
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->build()Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v1

    invoke-static {v0, v1}, Lalx;->c(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)Landroid/net/Uri;

    goto/16 :goto_1

    .line 307
    :cond_7
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getID()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_2
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lapf;->c:Lamf;

    .line 77
    invoke-virtual {p0}, Lapf;->b()Lalr;

    move-result-object v0

    invoke-virtual {v0}, Lalr;->r()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 78
    new-instance v0, Lamf;

    invoke-virtual {p0}, Lapf;->b()Lalr;

    move-result-object v1

    invoke-virtual {v1}, Lalr;->r()Landroid/location/Location;

    move-result-object v1

    invoke-direct {v0, v1}, Lamf;-><init>(Landroid/location/Location;)V

    iput-object v0, p0, Lapf;->c:Lamf;

    .line 83
    :goto_0
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v0}, Lall;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapf;->d:Ljava/lang/String;

    .line 84
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v0}, Lall;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lapf;->e:Ljava/lang/String;

    .line 85
    return-void

    .line 80
    :cond_0
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v0}, Lanb;->c(Landroid/content/Context;)Lamf;

    move-result-object v0

    iput-object v0, p0, Lapf;->c:Lamf;

    goto :goto_0
.end method

.method public c(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 491
    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v1

    .line 494
    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 495
    sget-object v0, Lakx;->eR:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 496
    invoke-static {p1, v0, v2}, Lall;->a(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 501
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkIsDisasterFiltered, typeId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isFiltered = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 503
    return v0

    .line 498
    :cond_0
    sget-object v0, Lakx;->eS:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 499
    invoke-static {p1, v0, v2}, Lall;->a(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public c(Lcom/sec/android/GeoLookout/db/DisasterInfo;Z)Z
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v3, 0x1

    .line 406
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationType()I

    move-result v5

    .line 408
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v6

    .line 412
    if-lez v6, :cond_0

    .line 413
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v0, v6}, Lalz;->h(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 414
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v0, v6}, Lalz;->i(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 420
    :goto_0
    const-string v2, ""

    .line 422
    packed-switch v5, :pswitch_data_0

    .line 439
    const-string v2, "Z"

    .line 443
    :goto_1
    const-string v4, ":"

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 445
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "checkZoneForZoneTypeMessage zone : "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 447
    if-ne v5, v8, :cond_4

    .line 448
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lapf;->e:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 449
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "checkDisasterInfoIsValid : same with current county: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    move v0, v3

    .line 484
    :goto_2
    return v0

    .line 416
    :cond_0
    iget-object v1, p0, Lapf;->d:Ljava/lang/String;

    .line 417
    iget-object v0, p0, Lapf;->e:Ljava/lang/String;

    goto :goto_0

    .line 424
    :pswitch_0
    const-string v2, "Z"

    goto :goto_1

    .line 427
    :pswitch_1
    const-string v2, "C"

    goto :goto_1

    .line 430
    :pswitch_2
    const-string v2, "K"

    goto :goto_1

    .line 433
    :pswitch_3
    const-string v2, "S"

    goto :goto_1

    .line 436
    :pswitch_4
    const-string v2, "J"

    goto :goto_1

    .line 451
    :cond_1
    if-nez v6, :cond_9

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_9

    .line 452
    :cond_2
    const-string v0, "checkDisasterInfoIsValid : there is no current county"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 453
    if-eqz p2, :cond_3

    .line 454
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v0, v4}, Lall;->d(Landroid/content/Context;Ljava/lang/String;)V

    :cond_3
    move v0, v3

    .line 456
    goto :goto_2

    .line 459
    :cond_4
    const-string v0, "02"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz v4, :cond_5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v2, 0x5

    if-lt v0, v2, :cond_5

    if-eqz v1, :cond_5

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 460
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "checkDisasterInfoIsValid : same with current area: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    move v0, v3

    .line 461
    goto :goto_2

    .line 462
    :cond_5
    if-eqz v4, :cond_6

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_6

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 463
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "checkDisasterInfoIsValid : same with current zone: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    move v0, v3

    .line 464
    goto/16 :goto_2

    .line 465
    :cond_6
    if-nez v6, :cond_9

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_9

    .line 466
    :cond_7
    const-string v0, "checkDisasterInfoIsValid : there is no current zone"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 467
    if-eqz p2, :cond_8

    .line 468
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v0, v4}, Lall;->b(Landroid/content/Context;Ljava/lang/String;)V

    :cond_8
    move v0, v3

    .line 470
    goto/16 :goto_2

    .line 474
    :cond_9
    const-string v0, "checkDisasterInfoIsValid : result is false"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 476
    if-eqz p2, :cond_a

    .line 477
    if-ne v5, v8, :cond_b

    .line 478
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    iget-object v1, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lapf;->b()Lalr;

    move-result-object v2

    invoke-virtual {v2}, Lalr;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    iget-object v5, p0, Lapf;->d:Ljava/lang/String;

    iget-object v6, p0, Lapf;->e:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lalu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    :cond_a
    :goto_3
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 480
    :cond_b
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v1

    iget-object v2, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lapf;->b()Lalr;

    move-result-object v0

    invoke-virtual {v0}, Lalr;->b()Ljava/lang/String;

    move-result-object v3

    const-string v5, ""

    iget-object v6, p0, Lapf;->d:Ljava/lang/String;

    iget-object v7, p0, Lapf;->e:Ljava/lang/String;

    invoke-virtual/range {v1 .. v7}, Lalu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 422
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected d(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 2

    .prologue
    .line 313
    if-nez p1, :cond_0

    .line 314
    const-string v0, "disaster info is null"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 330
    :goto_0
    return-void

    .line 318
    :cond_0
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {p0, v0, p1}, Lapf;->b(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 319
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lapf;->a(Landroid/content/Context;)V

    .line 321
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lapf;->a(Landroid/content/Context;I)V

    .line 323
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {p0, v0, p1}, Lapf;->a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 325
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getActionStatus()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 326
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lapf;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;Z)V

    .line 329
    :cond_1
    invoke-static {}, Lalk;->a()Lalk;

    move-result-object v0

    iget-object v1, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lalk;->b(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    goto :goto_0
.end method

.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 44
    const-string v0, "PushMessageTask : run()"

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v0}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v0}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 47
    :cond_0
    const-string v0, "GeoNews is OFF or DIM : ignore the push message"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v0, v6}, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->a(Landroid/content/Context;Z)V

    .line 72
    :goto_0
    return-void

    .line 52
    :cond_1
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-virtual {p0}, Lapf;->b()Lalr;

    move-result-object v1

    invoke-virtual {v1}, Lalr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/db/DisasterParser;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 54
    if-nez v0, :cond_2

    .line 55
    const-string v0, "parsing : disasterInfo null"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v0, v6}, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->a(Landroid/content/Context;Z)V

    goto :goto_0

    .line 60
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getActionStatus()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_3

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getEndTime()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gez v1, :cond_4

    .line 61
    :cond_3
    invoke-virtual {p0, v0}, Lapf;->b(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 71
    :goto_1
    iget-object v0, p0, Lapf;->b:Landroid/content/Context;

    invoke-static {v0, v6}, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->a(Landroid/content/Context;Z)V

    goto :goto_0

    .line 62
    :cond_4
    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getActionStatus()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_7

    .line 63
    const-string v1, "04"

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "03"

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 64
    :cond_5
    invoke-virtual {p0, v0}, Lapf;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    goto :goto_1

    .line 66
    :cond_6
    invoke-direct {p0, v0}, Lapf;->e(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    goto :goto_1

    .line 69
    :cond_7
    invoke-direct {p0, v0}, Lapf;->e(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    goto :goto_1
.end method
