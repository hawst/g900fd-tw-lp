.class public final Lbr;
.super Lco;

# interfaces
.implements Lbh;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/data/d;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lco;-><init>(Lcom/google/android/gms/common/data/d;I)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const-string v0, "key"

    invoke-virtual {p0, v0}, Lbr;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const-string v0, "local_version"

    invoke-virtual {p0, v0}, Lbr;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()[B
    .locals 1

    const-string v0, "local_data"

    invoke-virtual {p0, v0}, Lbr;->e(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    const-string v0, "conflict_version"

    invoke-virtual {p0, v0}, Lbr;->g(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    const-string v0, "conflict_version"

    invoke-virtual {p0, v0}, Lbr;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    invoke-static {p0, p1}, Lbq;->a(Lbh;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public f()[B
    .locals 1

    const-string v0, "conflict_data"

    invoke-virtual {p0, v0}, Lbr;->e(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public g()Lbh;
    .locals 1

    new-instance v0, Lbq;

    invoke-direct {v0, p0}, Lbq;-><init>(Lbh;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    invoke-static {p0}, Lbq;->a(Lbh;)I

    move-result v0

    return v0
.end method

.method public synthetic i()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lbr;->g()Lbh;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lbq;->b(Lbh;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
