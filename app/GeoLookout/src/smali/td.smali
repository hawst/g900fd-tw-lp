.class public final Ltd;
.super Lco;

# interfaces
.implements Lacz;


# instance fields
.field private c:Lcom/google/android/gms/internal/fs;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/d;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lco;-><init>(Lcom/google/android/gms/common/data/d;I)V

    return-void
.end method

.method private b()Lcom/google/android/gms/internal/fs;
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ltd;->c:Lcom/google/android/gms/internal/fs;

    if-nez v0, :cond_0

    const-string v0, "momentImpl"

    invoke-virtual {p0, v0}, Ltd;->e(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const/4 v2, 0x0

    array-length v3, v0

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Parcel;->unmarshall([BII)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    sget-object v0, Lcom/google/android/gms/internal/fs;->a:Ltc;

    invoke-virtual {v0, v1}, Ltc;->a(Landroid/os/Parcel;)Lcom/google/android/gms/internal/fs;

    move-result-object v0

    iput-object v0, p0, Ltd;->c:Lcom/google/android/gms/internal/fs;

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Ltd;->c:Lcom/google/android/gms/internal/fs;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()Lcom/google/android/gms/internal/fs;
    .locals 1

    invoke-direct {p0}, Ltd;->b()Lcom/google/android/gms/internal/fs;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Ltd;->b()Lcom/google/android/gms/internal/fs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fs;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()Z
    .locals 1

    invoke-direct {p0}, Ltd;->b()Lcom/google/android/gms/internal/fs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fs;->g()Z

    move-result v0

    return v0
.end method

.method public synthetic i()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Ltd;->a()Lcom/google/android/gms/internal/fs;

    move-result-object v0

    return-object v0
.end method

.method public j()Lacx;
    .locals 1

    invoke-direct {p0}, Ltd;->b()Lcom/google/android/gms/internal/fs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fs;->j()Lacx;

    move-result-object v0

    return-object v0
.end method

.method public l()Z
    .locals 1

    invoke-direct {p0}, Ltd;->b()Lcom/google/android/gms/internal/fs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fs;->g()Z

    move-result v0

    return v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Ltd;->b()Lcom/google/android/gms/internal/fs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fs;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public n()Z
    .locals 1

    invoke-direct {p0}, Ltd;->b()Lcom/google/android/gms/internal/fs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fs;->n()Z

    move-result v0

    return v0
.end method

.method public o()Lacx;
    .locals 1

    invoke-direct {p0}, Ltd;->b()Lcom/google/android/gms/internal/fs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fs;->o()Lacx;

    move-result-object v0

    return-object v0
.end method

.method public q()Z
    .locals 1

    invoke-direct {p0}, Ltd;->b()Lcom/google/android/gms/internal/fs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fs;->q()Z

    move-result v0

    return v0
.end method

.method public r()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Ltd;->b()Lcom/google/android/gms/internal/fs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fs;->r()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public s()Z
    .locals 1

    invoke-direct {p0}, Ltd;->b()Lcom/google/android/gms/internal/fs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fs;->s()Z

    move-result v0

    return v0
.end method
