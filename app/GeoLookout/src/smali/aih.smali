.class public Laih;
.super Ljava/lang/Object;
.source "DisasterDashboardActivity.java"

# interfaces
.implements Lwr;


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;


# direct methods
.method private constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V
    .locals 0

    .prologue
    .line 1832
    iput-object p1, p0, Laih;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Lagz;)V
    .locals 0

    .prologue
    .line 1832
    invoke-direct {p0, p1}, Laih;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v9, -0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 1842
    iget-object v0, p0, Laih;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lalz;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Laih;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Laih;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->k(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/widget/TabHost;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    move-object v1, v0

    .line 1845
    :goto_0
    iget v0, v1, Laif;->n:I

    if-nez v0, :cond_3

    .line 1846
    iget-object v0, v1, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e000a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 1847
    iget-object v1, v1, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e000b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 1849
    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setDrawingCacheEnabled(Z)V

    .line 1850
    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->setDrawingCacheEnabled(Z)V

    .line 1853
    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getMeasuredHeight()I

    move-result v3

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1854
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1855
    invoke-virtual {v2, v9}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 1856
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1859
    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getMeasuredHeight()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1860
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1861
    invoke-virtual {v5, v9}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 1864
    invoke-virtual {v5, p1, v7, v7, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1865
    invoke-virtual {v5, v3, v7, v7, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1867
    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout;->setDrawingCacheEnabled(Z)V

    .line 1868
    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout;->setDrawingCacheEnabled(Z)V

    move-object v0, v2

    move-object v1, v3

    .line 1902
    :goto_1
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/GeoNews"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1903
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1904
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 1907
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ".jpg"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1908
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1913
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1918
    :goto_2
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x32

    invoke-virtual {v0, v4, v5, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1921
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1926
    :goto_3
    if-eqz v2, :cond_1

    .line 1928
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1934
    :cond_1
    :goto_4
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1936
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1939
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1940
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1941
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Map Image fileUri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 1944
    :try_start_3
    iget-object v1, p0, Laih;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v1, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1945
    iget-object v1, p0, Laih;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04a1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 1946
    iget-object v1, p0, Laih;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    .line 1951
    :goto_5
    return-void

    .line 1842
    :cond_2
    iget-object v0, p0, Laih;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    move-object v1, v0

    goto/16 :goto_0

    .line 1872
    :cond_3
    iget-object v0, v1, Laif;->w:Landroid/view/View;

    const v1, 0x7f0e0014

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1876
    iget-object v1, p0, Laih;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    const v2, 0x7f0e0009

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1877
    iget-object v1, p0, Laih;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    const v2, 0x7f0e0006

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 1882
    invoke-virtual {v3, v6}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 1884
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v2

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1885
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1886
    invoke-virtual {v1, v9}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 1887
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1890
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v6

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v0, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1891
    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1895
    invoke-virtual {v6, p1, v7, v7, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1896
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v6, v2, v7, v0, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1897
    iget-object v0, p0, Laih;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v5, p0, Laih;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v5}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v5

    int-to-float v5, v5

    iget-object v7, p0, Laih;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v7}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v6, v0, v5, v7, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1899
    invoke-virtual {v3, v8}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    move-object v0, v1

    move-object v1, v2

    goto/16 :goto_1

    .line 1914
    :catch_0
    move-exception v2

    .line 1915
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move-object v2, v4

    goto/16 :goto_2

    .line 1922
    :catch_1
    move-exception v4

    .line 1923
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 1929
    :catch_2
    move-exception v2

    .line 1930
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    .line 1947
    :catch_3
    move-exception v0

    .line 1948
    const-string v1, "ActivityNotFoundException "

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    .line 1949
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_5
.end method
