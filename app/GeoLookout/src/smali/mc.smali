.class public final Lmc;
.super Lmz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmz",
        "<",
        "Lmo;",
        ">;"
    }
.end annotation


# instance fields
.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcb;Lcc;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p5}, Lmz;-><init>(Landroid/content/Context;Lcb;Lcc;[Ljava/lang/String;)V

    invoke-static {p4}, Lnx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lmc;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected a(Landroid/os/IBinder;)Lmo;
    .locals 1

    invoke-static {p1}, Lmp;->a(Landroid/os/IBinder;)Lmo;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbm;)V
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    move-object v1, v0

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lmc;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lmo;

    invoke-interface {v0, v1}, Lmo;->b(Lml;)V

    :goto_1
    return-void

    :cond_0
    new-instance v0, Lmj;

    invoke-direct {v0, p0, p1}, Lmj;-><init>(Lmc;Lbm;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "AppStateClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public a(Lbn;I)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lmc;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lmo;

    new-instance v1, Lmd;

    invoke-direct {v1, p0, p1}, Lmd;-><init>(Lmc;Lbn;)V

    invoke-interface {v0, v1, p2}, Lmo;->b(Lml;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AppStateClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public a(Lbo;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lmc;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lmo;

    new-instance v1, Lmf;

    invoke-direct {v1, p0, p1}, Lmf;-><init>(Lmc;Lbo;)V

    invoke-interface {v0, v1}, Lmo;->a(Lml;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AppStateClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public a(Lbp;I)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lmc;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lmo;

    new-instance v1, Lmh;

    invoke-direct {v1, p0, p1}, Lmh;-><init>(Lmc;Lbp;)V

    invoke-interface {v0, v1, p2}, Lmo;->a(Lml;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AppStateClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public a(Lbp;ILjava/lang/String;[B)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lmc;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lmo;

    new-instance v1, Lmh;

    invoke-direct {v1, p0, p1}, Lmh;-><init>(Lmc;Lbp;)V

    invoke-interface {v0, v1, p2, p3, p4}, Lmo;->a(Lml;ILjava/lang/String;[B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "AppStateClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public a(Lbp;I[B)V
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    move-object v1, v0

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lmc;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lmo;

    invoke-interface {v0, v1, p2, p3}, Lmo;->a(Lml;I[B)V

    :goto_1
    return-void

    :cond_0
    new-instance v0, Lmh;

    invoke-direct {v0, p0, p1}, Lmh;-><init>(Lmc;Lbp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "AppStateClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected a(Lno;Lnd;)V
    .locals 6

    const v2, 0x3d8024

    invoke-virtual {p0}, Lmc;->i()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lmc;->f:Ljava/lang/String;

    invoke-virtual {p0}, Lmc;->j()[Ljava/lang/String;

    move-result-object v5

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Lno;->a(Lnl;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method protected varargs a([Ljava/lang/String;)V
    .locals 6

    const/4 v3, 0x1

    const/4 v1, 0x0

    move v0, v1

    move v2, v1

    :goto_0
    array-length v4, p1

    if-ge v0, v4, :cond_1

    aget-object v4, p1, v0

    const-string v5, "https://www.googleapis.com/auth/appstate"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "AppStateClient requires %s to function."

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "https://www.googleapis.com/auth/appstate"

    aput-object v4, v3, v1

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lnx;->a(ZLjava/lang/Object;)V

    return-void
.end method

.method protected synthetic b(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lmc;->a(Landroid/os/IBinder;)Lmo;

    move-result-object v0

    return-object v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.appstate.service.START"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.appstate.internal.IAppStateService"

    return-object v0
.end method

.method public g()I
    .locals 3

    const/4 v1, 0x2

    :try_start_0
    invoke-virtual {p0}, Lmc;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lmo;

    invoke-interface {v0}, Lmo;->a()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v0, "AppStateClient"

    const-string v2, "service died"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0
.end method

.method public h()I
    .locals 3

    const/4 v1, 0x2

    :try_start_0
    invoke-virtual {p0}, Lmc;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lmo;

    invoke-interface {v0}, Lmo;->b()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v0, "AppStateClient"

    const-string v2, "service died"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0
.end method
