.class public Land;
.super Ljava/lang/Object;
.source "DisasterTestMode.java"


# static fields
.field private static volatile a:Land;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput-object v0, Land;->a:Land;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method public static a()Land;
    .locals 2

    .prologue
    .line 22
    sget-object v0, Land;->a:Land;

    if-nez v0, :cond_1

    .line 23
    const-class v1, Land;

    monitor-enter v1

    .line 24
    :try_start_0
    sget-object v0, Land;->a:Land;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Land;

    invoke-direct {v0}, Land;-><init>()V

    sput-object v0, Land;->a:Land;

    .line 27
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    :cond_1
    sget-object v0, Land;->a:Land;

    return-object v0

    .line 27
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(Landroid/content/Context;Z)V
    .locals 3

    .prologue
    .line 89
    if-nez p1, :cond_0

    .line 90
    const-string v0, "setTestMode context null!!"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 101
    :goto_0
    return-void

    .line 94
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "disasteralert_test_mode_state"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 95
    if-eqz p2, :cond_1

    .line 96
    or-int/lit8 v0, v0, 0x1

    .line 100
    :goto_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "disasteralert_test_mode_state"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 98
    :cond_1
    and-int/lit8 v0, v0, -0x2

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 36
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/GeoNews/TestAppId"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 37
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Land;->a(Landroid/content/Context;Z)V

    .line 39
    const-string v0, "checkTestMode : TEST Mode OFF"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 45
    :goto_0
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Land;->a(Landroid/content/Context;Z)V

    .line 43
    const-string v0, "checkTestMode : TEST Mode ON"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 51
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "userdebug"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 58
    if-nez p1, :cond_0

    .line 59
    const-string v1, "getTestMode context null!! default : TEST Mode OFF"

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    .line 69
    :goto_0
    return v0

    .line 63
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "disasteralert_test_mode_state"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 64
    and-int/lit8 v2, v2, 0x1

    if-ne v2, v1, :cond_1

    .line 65
    const-string v0, "getTestMode : TEST Mode ON"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    move v0, v1

    .line 66
    goto :goto_0

    .line 68
    :cond_1
    const-string v1, "getTestMode : TEST Mode OFF"

    invoke-static {v1}, Lalj;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public d(Landroid/content/Context;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 76
    if-nez p1, :cond_0

    .line 77
    const-string v1, "getTestModeValue context null!! default : TEST Mode OFF"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 82
    :goto_0
    return v0

    .line 80
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "disasteralert_test_mode_state"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getTestModeValue : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v0, 0x1

    if-ne v1, v0, :cond_1

    const-string v0, "TEST Mode ON"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    move v0, v1

    .line 82
    goto :goto_0

    .line 81
    :cond_1
    const-string v0, "TEST Mode OFF"

    goto :goto_1
.end method
