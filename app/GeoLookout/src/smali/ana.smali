.class public Lana;
.super Ljava/lang/Object;
.source "LifemodeParser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 391
    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->setPrettyPrinting()Lcom/google/gson/GsonBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    .line 392
    new-instance v1, Lcom/google/gson/JsonParser;

    invoke-direct {v1}, Lcom/google/gson/JsonParser;-><init>()V

    .line 393
    invoke-virtual {v1, p0}, Lcom/google/gson/JsonParser;->parse(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v1

    .line 394
    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->toJson(Lcom/google/gson/JsonElement;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lamx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 20
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    .line 22
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 23
    const/4 v2, 0x0

    .line 25
    invoke-static {p1}, Lana;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Laow;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 26
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LIFEMODE_INFO - ServerData fromLifemodeJsonToLifemodeInfos : ============================================================\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lana;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n============================================================\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 29
    const-class v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;

    .line 30
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LIFEMODE_INFO - ParsedData fromLifemodeJsonToLifemodeInfos : ============================================================\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n============================================================\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 34
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_pm10:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 35
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_pm10:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 37
    :cond_0
    if-eqz v2, :cond_2

    .line 38
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 39
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;

    .line 40
    iget-object v4, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 43
    new-instance v4, Lamz;

    invoke-direct {v4}, Lamz;-><init>()V

    .line 44
    iget-object v5, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget-object v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->level:I

    invoke-virtual {v5, v6}, Lamz;->b(I)Lamz;

    move-result-object v5

    new-instance v6, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lon:D

    invoke-direct {v6, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v5, v6}, Lamz;->a(Lamf;)Lamz;

    move-result-object v5

    iget-wide v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->startTime:J

    invoke-virtual {v5, v6, v7}, Lamz;->a(J)Lamz;

    move-result-object v5

    iget-object v1, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lamz;->c(Ljava/lang/String;)Lamz;

    move-result-object v1

    const/16 v5, 0x260

    invoke-virtual {v1, v5}, Lamz;->c(I)Lamz;

    .line 51
    invoke-virtual {v4}, Lamz;->a()Lamx;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 55
    :cond_2
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_cold:Ljava/util/List;

    if-eqz v1, :cond_3

    .line 56
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_cold:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 58
    :cond_3
    if-eqz v2, :cond_5

    .line 59
    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 60
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;

    .line 61
    iget-object v4, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    if-eqz v4, :cond_4

    .line 64
    new-instance v4, Lamz;

    invoke-direct {v4}, Lamz;-><init>()V

    .line 65
    iget-object v5, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget-object v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->level:I

    invoke-virtual {v5, v6}, Lamz;->b(I)Lamz;

    move-result-object v5

    new-instance v6, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lon:D

    invoke-direct {v6, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v5, v6}, Lamz;->a(Lamf;)Lamz;

    move-result-object v5

    iget-wide v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->startTime:J

    invoke-virtual {v5, v6, v7}, Lamz;->a(J)Lamz;

    move-result-object v5

    iget-object v1, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lamz;->c(Ljava/lang/String;)Lamz;

    move-result-object v1

    const/16 v5, 0x261

    invoke-virtual {v1, v5}, Lamz;->c(I)Lamz;

    .line 72
    invoke-virtual {v4}, Lamz;->a()Lamx;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 76
    :cond_5
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_freez:Ljava/util/List;

    if-eqz v1, :cond_6

    .line 77
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_freez:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 79
    :cond_6
    if-eqz v2, :cond_8

    .line 80
    :cond_7
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 81
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;

    .line 82
    iget-object v4, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    if-eqz v4, :cond_7

    .line 85
    new-instance v4, Lamz;

    invoke-direct {v4}, Lamz;-><init>()V

    .line 86
    iget-object v5, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget-object v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->level:I

    invoke-virtual {v5, v6}, Lamz;->b(I)Lamz;

    move-result-object v5

    new-instance v6, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lon:D

    invoke-direct {v6, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v5, v6}, Lamz;->a(Lamf;)Lamz;

    move-result-object v5

    iget-wide v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->startTime:J

    invoke-virtual {v5, v6, v7}, Lamz;->a(J)Lamz;

    move-result-object v5

    iget-object v1, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lamz;->c(Ljava/lang/String;)Lamz;

    move-result-object v1

    const/16 v5, 0x25e

    invoke-virtual {v1, v5}, Lamz;->c(I)Lamz;

    .line 93
    invoke-virtual {v4}, Lamz;->a()Lamx;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 97
    :cond_8
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_fros:Ljava/util/List;

    if-eqz v1, :cond_9

    .line 98
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_fros:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 100
    :cond_9
    if-eqz v2, :cond_b

    .line 101
    :cond_a
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 102
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;

    .line 103
    iget-object v4, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    if-eqz v4, :cond_a

    .line 106
    new-instance v4, Lamz;

    invoke-direct {v4}, Lamz;-><init>()V

    .line 107
    iget-object v5, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget-object v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->level:I

    invoke-virtual {v5, v6}, Lamz;->b(I)Lamz;

    move-result-object v5

    new-instance v6, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lon:D

    invoke-direct {v6, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v5, v6}, Lamz;->a(Lamf;)Lamz;

    move-result-object v5

    iget-wide v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->startTime:J

    invoke-virtual {v5, v6, v7}, Lamz;->a(J)Lamz;

    move-result-object v5

    iget-object v1, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lamz;->c(Ljava/lang/String;)Lamz;

    move-result-object v1

    const/16 v5, 0x25f

    invoke-virtual {v1, v5}, Lamz;->c(I)Lamz;

    .line 114
    invoke-virtual {v4}, Lamz;->a()Lamx;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 118
    :cond_b
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_f_poison:Ljava/util/List;

    if-eqz v1, :cond_c

    .line 119
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_f_poison:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 121
    :cond_c
    if-eqz v2, :cond_e

    .line 122
    :cond_d
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 123
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;

    .line 124
    iget-object v4, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    if-eqz v4, :cond_d

    .line 127
    new-instance v4, Lamz;

    invoke-direct {v4}, Lamz;-><init>()V

    .line 128
    iget-object v5, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget-object v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->level:I

    invoke-virtual {v5, v6}, Lamz;->b(I)Lamz;

    move-result-object v5

    new-instance v6, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lon:D

    invoke-direct {v6, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v5, v6}, Lamz;->a(Lamf;)Lamz;

    move-result-object v5

    iget-wide v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->startTime:J

    invoke-virtual {v5, v6, v7}, Lamz;->a(J)Lamz;

    move-result-object v5

    iget-object v1, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lamz;->c(Ljava/lang/String;)Lamz;

    move-result-object v1

    const/16 v5, 0x25a

    invoke-virtual {v1, v5}, Lamz;->c(I)Lamz;

    .line 135
    invoke-virtual {v4}, Lamz;->a()Lamx;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 139
    :cond_e
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_hum:Ljava/util/List;

    if-eqz v1, :cond_f

    .line 140
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_hum:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 142
    :cond_f
    if-eqz v2, :cond_12

    .line 143
    :cond_10
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 144
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;

    .line 145
    iget-object v4, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    if-eqz v4, :cond_10

    .line 148
    new-instance v4, Lamz;

    invoke-direct {v4}, Lamz;-><init>()V

    .line 149
    iget-object v5, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget-object v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->level:I

    invoke-virtual {v5, v6}, Lamz;->b(I)Lamz;

    move-result-object v5

    new-instance v6, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lon:D

    invoke-direct {v6, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v5, v6}, Lamz;->a(Lamf;)Lamz;

    move-result-object v5

    iget-wide v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->startTime:J

    invoke-virtual {v5, v6, v7}, Lamz;->a(J)Lamz;

    move-result-object v5

    iget-object v1, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lamz;->c(Ljava/lang/String;)Lamz;

    .line 156
    sget-boolean v1, Laky;->b:Z

    if-eqz v1, :cond_11

    .line 157
    const/16 v1, 0x1fb

    invoke-virtual {v4, v1}, Lamz;->c(I)Lamz;

    .line 162
    :goto_6
    invoke-virtual {v4}, Lamz;->a()Lamx;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 159
    :cond_11
    const/16 v1, 0x25c

    invoke-virtual {v4, v1}, Lamz;->c(I)Lamz;

    goto :goto_6

    .line 166
    :cond_12
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_pollen:Ljava/util/List;

    if-eqz v1, :cond_13

    .line 167
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_pollen:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 169
    :cond_13
    if-eqz v2, :cond_16

    .line 170
    :cond_14
    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_16

    .line 171
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;

    .line 172
    iget-object v4, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    if-eqz v4, :cond_14

    .line 175
    new-instance v4, Lamz;

    invoke-direct {v4}, Lamz;-><init>()V

    .line 176
    iget-object v5, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget-object v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->level:I

    invoke-virtual {v5, v6}, Lamz;->b(I)Lamz;

    move-result-object v5

    new-instance v6, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lon:D

    invoke-direct {v6, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v5, v6}, Lamz;->a(Lamf;)Lamz;

    move-result-object v5

    iget-wide v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->startTime:J

    invoke-virtual {v5, v6, v7}, Lamz;->a(J)Lamz;

    move-result-object v5

    iget-object v1, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lamz;->c(Ljava/lang/String;)Lamz;

    .line 182
    sget-boolean v1, Laky;->b:Z

    if-eqz v1, :cond_15

    .line 183
    const/16 v1, 0x1f5

    invoke-virtual {v4, v1}, Lamz;->c(I)Lamz;

    .line 187
    :goto_8
    invoke-virtual {v4}, Lamz;->a()Lamx;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 185
    :cond_15
    const/16 v1, 0x263

    invoke-virtual {v4, v1}, Lamz;->c(I)Lamz;

    goto :goto_8

    .line 191
    :cond_16
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_putre:Ljava/util/List;

    if-eqz v1, :cond_17

    .line 192
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_putre:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 194
    :cond_17
    if-eqz v2, :cond_19

    .line 195
    :cond_18
    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 196
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;

    .line 197
    iget-object v4, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    if-eqz v4, :cond_18

    .line 200
    new-instance v4, Lamz;

    invoke-direct {v4}, Lamz;-><init>()V

    .line 201
    iget-object v5, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget-object v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->level:I

    invoke-virtual {v5, v6}, Lamz;->b(I)Lamz;

    move-result-object v5

    new-instance v6, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lon:D

    invoke-direct {v6, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v5, v6}, Lamz;->a(Lamf;)Lamz;

    move-result-object v5

    iget-wide v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->startTime:J

    invoke-virtual {v5, v6, v7}, Lamz;->a(J)Lamz;

    move-result-object v5

    iget-object v1, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lamz;->c(Ljava/lang/String;)Lamz;

    move-result-object v1

    const/16 v5, 0x25b

    invoke-virtual {v1, v5}, Lamz;->c(I)Lamz;

    .line 208
    invoke-virtual {v4}, Lamz;->a()Lamx;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 212
    :cond_19
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_s_temp:Ljava/util/List;

    if-eqz v1, :cond_1a

    .line 213
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_s_temp:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 215
    :cond_1a
    if-eqz v2, :cond_1c

    .line 216
    :cond_1b
    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 217
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;

    .line 218
    iget-object v4, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    if-eqz v4, :cond_1b

    .line 221
    new-instance v4, Lamz;

    invoke-direct {v4}, Lamz;-><init>()V

    .line 222
    iget-object v5, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget-object v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->level:I

    invoke-virtual {v5, v6}, Lamz;->b(I)Lamz;

    move-result-object v5

    new-instance v6, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lon:D

    invoke-direct {v6, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v5, v6}, Lamz;->a(Lamf;)Lamz;

    move-result-object v5

    iget-wide v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->startTime:J

    invoke-virtual {v5, v6, v7}, Lamz;->a(J)Lamz;

    move-result-object v5

    iget-object v1, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lamz;->c(Ljava/lang/String;)Lamz;

    move-result-object v1

    const/16 v5, 0x25d

    invoke-virtual {v1, v5}, Lamz;->c(I)Lamz;

    .line 229
    invoke-virtual {v4}, Lamz;->a()Lamx;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 233
    :cond_1c
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_temp:Ljava/util/List;

    if-eqz v1, :cond_1d

    .line 234
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_temp:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 236
    :cond_1d
    if-eqz v2, :cond_1f

    .line 237
    :cond_1e
    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 238
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;

    .line 239
    iget-object v4, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    if-eqz v4, :cond_1e

    .line 242
    new-instance v4, Lamz;

    invoke-direct {v4}, Lamz;-><init>()V

    .line 243
    iget-object v5, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget-object v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->level:I

    invoke-virtual {v5, v6}, Lamz;->b(I)Lamz;

    move-result-object v5

    new-instance v6, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lon:D

    invoke-direct {v6, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v5, v6}, Lamz;->a(Lamf;)Lamz;

    move-result-object v5

    iget-wide v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->startTime:J

    invoke-virtual {v5, v6, v7}, Lamz;->a(J)Lamz;

    move-result-object v5

    iget-object v1, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lamz;->c(Ljava/lang/String;)Lamz;

    move-result-object v1

    const/16 v5, 0x262

    invoke-virtual {v1, v5}, Lamz;->c(I)Lamz;

    .line 250
    invoke-virtual {v4}, Lamz;->a()Lamx;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 254
    :cond_1f
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_uv:Ljava/util/List;

    if-eqz v1, :cond_20

    .line 255
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_uv:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 257
    :cond_20
    if-eqz v2, :cond_31

    .line 258
    :cond_21
    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_23

    .line 259
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;

    .line 260
    iget-object v4, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    if-eqz v4, :cond_21

    .line 263
    new-instance v4, Lamz;

    invoke-direct {v4}, Lamz;-><init>()V

    .line 264
    iget-object v5, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget-object v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->level:I

    invoke-virtual {v5, v6}, Lamz;->b(I)Lamz;

    move-result-object v5

    new-instance v6, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lon:D

    invoke-direct {v6, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v5, v6}, Lamz;->a(Lamf;)Lamz;

    move-result-object v5

    iget-wide v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->startTime:J

    invoke-virtual {v5, v6, v7}, Lamz;->a(J)Lamz;

    move-result-object v5

    iget-object v1, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lamz;->c(Ljava/lang/String;)Lamz;

    .line 271
    sget-boolean v1, Laky;->b:Z

    if-eqz v1, :cond_22

    .line 272
    const/16 v1, 0x1f8

    invoke-virtual {v4, v1}, Lamz;->c(I)Lamz;

    .line 276
    :goto_d
    invoke-virtual {v4}, Lamz;->a()Lamx;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 274
    :cond_22
    const/16 v1, 0x259

    invoke-virtual {v4, v1}, Lamz;->c(I)Lamz;

    goto :goto_d

    .line 279
    :cond_23
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_y_dust:Ljava/util/List;

    if-eqz v1, :cond_24

    .line 280
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_y_dust:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 282
    :cond_24
    if-eqz v2, :cond_26

    .line 283
    :cond_25
    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_26

    .line 284
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;

    .line 285
    iget-object v4, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    if-eqz v4, :cond_25

    .line 288
    new-instance v4, Lamz;

    invoke-direct {v4}, Lamz;-><init>()V

    .line 289
    iget-object v5, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget-object v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->level:I

    invoke-virtual {v5, v6}, Lamz;->b(I)Lamz;

    move-result-object v5

    new-instance v6, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lon:D

    invoke-direct {v6, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v5, v6}, Lamz;->a(Lamf;)Lamz;

    move-result-object v5

    iget-wide v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->startTime:J

    invoke-virtual {v5, v6, v7}, Lamz;->a(J)Lamz;

    move-result-object v5

    iget-object v1, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lamz;->c(Ljava/lang/String;)Lamz;

    move-result-object v1

    const/16 v5, 0x1f7

    invoke-virtual {v1, v5}, Lamz;->c(I)Lamz;

    .line 296
    invoke-virtual {v4}, Lamz;->a()Lamx;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_e

    .line 300
    :cond_26
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_w_acci:Ljava/util/List;

    if-eqz v1, :cond_27

    .line 301
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_w_acci:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 303
    :cond_27
    if-eqz v2, :cond_29

    .line 304
    :cond_28
    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_29

    .line 305
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;

    .line 306
    iget-object v4, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    if-eqz v4, :cond_28

    .line 309
    new-instance v4, Lamz;

    invoke-direct {v4}, Lamz;-><init>()V

    .line 310
    iget-object v5, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget-object v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->level:I

    invoke-virtual {v5, v6}, Lamz;->b(I)Lamz;

    move-result-object v5

    new-instance v6, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lon:D

    invoke-direct {v6, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v5, v6}, Lamz;->a(Lamf;)Lamz;

    move-result-object v5

    iget-wide v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->startTime:J

    invoke-virtual {v5, v6, v7}, Lamz;->a(J)Lamz;

    move-result-object v5

    iget-object v1, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lamz;->c(Ljava/lang/String;)Lamz;

    move-result-object v1

    const/16 v5, 0x1f9

    invoke-virtual {v1, v5}, Lamz;->c(I)Lamz;

    .line 317
    invoke-virtual {v4}, Lamz;->a()Lamx;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 321
    :cond_29
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_dryskin:Ljava/util/List;

    if-eqz v1, :cond_2a

    .line 322
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_dryskin:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 324
    :cond_2a
    if-eqz v2, :cond_2c

    .line 325
    :cond_2b
    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 326
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;

    .line 327
    iget-object v4, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    if-eqz v4, :cond_2b

    .line 330
    new-instance v4, Lamz;

    invoke-direct {v4}, Lamz;-><init>()V

    .line 331
    iget-object v5, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget-object v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->level:I

    invoke-virtual {v5, v6}, Lamz;->b(I)Lamz;

    move-result-object v5

    new-instance v6, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lon:D

    invoke-direct {v6, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v5, v6}, Lamz;->a(Lamf;)Lamz;

    move-result-object v5

    iget-wide v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->startTime:J

    invoke-virtual {v5, v6, v7}, Lamz;->a(J)Lamz;

    move-result-object v5

    iget-object v1, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lamz;->c(Ljava/lang/String;)Lamz;

    move-result-object v1

    const/16 v5, 0x1fa

    invoke-virtual {v1, v5}, Lamz;->c(I)Lamz;

    .line 338
    invoke-virtual {v4}, Lamz;->a()Lamx;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_10

    .line 342
    :cond_2c
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_umb:Ljava/util/List;

    if-eqz v1, :cond_2d

    .line 343
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_umb:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 345
    :cond_2d
    if-eqz v2, :cond_2f

    .line 346
    :cond_2e
    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 347
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;

    .line 348
    iget-object v4, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    if-eqz v4, :cond_2e

    .line 351
    new-instance v4, Lamz;

    invoke-direct {v4}, Lamz;-><init>()V

    .line 352
    iget-object v5, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget-object v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v5

    iget v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->level:I

    invoke-virtual {v5, v6}, Lamz;->b(I)Lamz;

    move-result-object v5

    new-instance v6, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lon:D

    invoke-direct {v6, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v5, v6}, Lamz;->a(Lamf;)Lamz;

    move-result-object v5

    iget-wide v6, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->startTime:J

    invoke-virtual {v5, v6, v7}, Lamz;->a(J)Lamz;

    move-result-object v5

    iget-object v1, v1, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lamz;->c(Ljava/lang/String;)Lamz;

    move-result-object v1

    const/16 v5, 0x1fc

    invoke-virtual {v1, v5}, Lamz;->c(I)Lamz;

    .line 359
    invoke-virtual {v4}, Lamz;->a()Lamx;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_11

    .line 363
    :cond_2f
    iget-object v1, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_f_dust:Ljava/util/List;

    if-eqz v1, :cond_32

    .line 364
    iget-object v0, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;->life_f_dust:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    move-object v1, v0

    .line 366
    :goto_12
    if-eqz v1, :cond_31

    .line 367
    :cond_30
    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 368
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;

    .line 369
    iget-object v2, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    if-eqz v2, :cond_30

    .line 372
    new-instance v2, Lamz;

    invoke-direct {v2}, Lamz;-><init>()V

    .line 373
    iget-object v4, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->loc:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v4

    iget-object v5, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->value:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v4

    iget v5, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->level:I

    invoke-virtual {v4, v5}, Lamz;->b(I)Lamz;

    move-result-object v4

    new-instance v5, Lamf;

    iget-wide v6, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lat:D

    iget-wide v8, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->lon:D

    invoke-direct {v5, v6, v7, v8, v9}, Lamf;-><init>(DD)V

    invoke-virtual {v4, v5}, Lamz;->a(Lamf;)Lamz;

    move-result-object v4

    iget-wide v6, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->startTime:J

    invoke-virtual {v4, v6, v7}, Lamz;->a(J)Lamz;

    move-result-object v4

    iget-object v0, v0, Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;->city:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lamz;->c(Ljava/lang/String;)Lamz;

    move-result-object v0

    const/16 v4, 0x1f6

    invoke-virtual {v0, v4}, Lamz;->c(I)Lamz;

    .line 380
    invoke-virtual {v2}, Lamz;->a()Lamx;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_13

    .line 385
    :cond_31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LIFEMODE_INFO - lifemodeInfo : ============================================================\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n============================================================\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 387
    return-object v3

    :cond_32
    move-object v1, v2

    goto :goto_12
.end method
