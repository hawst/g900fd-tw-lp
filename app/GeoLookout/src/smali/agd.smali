.class public Lagd;
.super Ljava/lang/Object;
.source "DisasterService.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Landroid/content/Intent;

.field final synthetic c:Lcom/sec/android/GeoLookout/DisasterService;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/DisasterService;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lagd;->c:Lcom/sec/android/GeoLookout/DisasterService;

    iput-object p2, p0, Lagd;->a:Ljava/lang/String;

    iput-object p3, p0, Lagd;->b:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 96
    const-string v0, "com.sec.android.GeoLookout.ACTION_HANDLE_PUSH_MESSAGE"

    iget-object v1, p0, Lagd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lagd;->b:Landroid/content/Intent;

    const-string v1, "msg"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 98
    iget-object v1, p0, Lagd;->b:Landroid/content/Intent;

    const-string v2, "istracked"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 99
    iget-object v2, p0, Lagd;->c:Lcom/sec/android/GeoLookout/DisasterService;

    invoke-static {v2, v0, v1}, Lcom/sec/android/GeoLookout/DisasterService;->a(Lcom/sec/android/GeoLookout/DisasterService;Ljava/lang/String;Z)V

    .line 145
    :goto_0
    return-void

    .line 100
    :cond_0
    const-string v0, "com.sec.android.GeoLookout.ACTION_REGISTER_DISASTER_SERVER"

    iget-object v1, p0, Lagd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lagd;->b:Landroid/content/Intent;

    const-string v1, "regId"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "regId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->a(Ljava/lang/String;)V

    .line 103
    iget-object v1, p0, Lagd;->c:Lcom/sec/android/GeoLookout/DisasterService;

    invoke-static {v1, v0}, Lcom/sec/android/GeoLookout/DisasterService;->a(Lcom/sec/android/GeoLookout/DisasterService;Ljava/lang/String;)V

    goto :goto_0

    .line 104
    :cond_1
    const-string v0, "com.sec.android.GeoLookout.ACTION_UNREGISTER_DISASTER_SERVER"

    iget-object v1, p0, Lagd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 105
    iget-object v0, p0, Lagd;->c:Lcom/sec/android/GeoLookout/DisasterService;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/DisasterService;->a(Lcom/sec/android/GeoLookout/DisasterService;)V

    goto :goto_0

    .line 106
    :cond_2
    const-string v0, "com.sec.android.GeoLookout.ACTION_CHANGE_LOCALE"

    iget-object v1, p0, Lagd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 107
    iget-object v0, p0, Lagd;->c:Lcom/sec/android/GeoLookout/DisasterService;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/DisasterService;->b(Lcom/sec/android/GeoLookout/DisasterService;)V

    goto :goto_0

    .line 108
    :cond_3
    const-string v0, "com.sec.android.GeoLookout.ACTION_UPDATE_LOCATION_TO_SPP_SERVER"

    iget-object v1, p0, Lagd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 109
    iget-object v0, p0, Lagd;->b:Landroid/content/Intent;

    const-string v1, "latitude"

    invoke-virtual {v0, v1, v6, v7}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v0

    .line 110
    iget-object v2, p0, Lagd;->b:Landroid/content/Intent;

    const-string v3, "longitude"

    invoke-virtual {v2, v3, v6, v7}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v2

    .line 111
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "latitude="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " longitude="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lalj;->a(Ljava/lang/String;)V

    .line 112
    iget-object v4, p0, Lagd;->c:Lcom/sec/android/GeoLookout/DisasterService;

    invoke-static {v4, v0, v1, v2, v3}, Lcom/sec/android/GeoLookout/DisasterService;->a(Lcom/sec/android/GeoLookout/DisasterService;DD)V

    goto/16 :goto_0

    .line 113
    :cond_4
    const-string v0, "com.sec.android.GeoLookout.ACTION_UPDATE_LOCATION_TO_DISASTER_SERVER"

    iget-object v1, p0, Lagd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 114
    iget-object v0, p0, Lagd;->b:Landroid/content/Intent;

    const-string v1, "currentZone"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lagd;->b:Landroid/content/Intent;

    const-string v2, "currentCounty"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 116
    iget-object v2, p0, Lagd;->b:Landroid/content/Intent;

    const-string v3, "newZone"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 117
    iget-object v3, p0, Lagd;->b:Landroid/content/Intent;

    const-string v4, "newCounty"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 118
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "currentZone="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " currentCounty="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " newZone="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " newCounty="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lalj;->a(Ljava/lang/String;)V

    .line 119
    iget-object v4, p0, Lagd;->c:Lcom/sec/android/GeoLookout/DisasterService;

    invoke-static {v4, v0, v1, v2, v3}, Lcom/sec/android/GeoLookout/DisasterService;->a(Lcom/sec/android/GeoLookout/DisasterService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 120
    :cond_5
    const-string v0, "com.sec.android.GeoLookout.ACTION_SHOW_NETWORK_DIALOG"

    iget-object v1, p0, Lagd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 121
    iget-object v0, p0, Lagd;->b:Landroid/content/Intent;

    const-string v1, "network_status"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 122
    iget-object v1, p0, Lagd;->b:Landroid/content/Intent;

    const-string v2, "network_error_code"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 123
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "networkStatus="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " networkErrorCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->a(Ljava/lang/String;)V

    .line 124
    iget-object v2, p0, Lagd;->c:Lcom/sec/android/GeoLookout/DisasterService;

    invoke-static {v2, v0, v1}, Lcom/sec/android/GeoLookout/DisasterService;->a(Lcom/sec/android/GeoLookout/DisasterService;II)V

    goto/16 :goto_0

    .line 125
    :cond_6
    const-string v0, "com.sec.android.GeoLookout.ACTION_REQUEST_START_CURRENT_LOCATION"

    iget-object v1, p0, Lagd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 126
    iget-object v0, p0, Lagd;->c:Lcom/sec/android/GeoLookout/DisasterService;

    iget-object v1, p0, Lagd;->c:Lcom/sec/android/GeoLookout/DisasterService;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/DisasterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/DisasterService;->a(Lcom/sec/android/GeoLookout/DisasterService;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 127
    :cond_7
    const-string v0, "com.sec.android.GeoLookout.ACTION_REQUEST_CURRENT_LOCATION"

    iget-object v1, p0, Lagd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 128
    iget-object v0, p0, Lagd;->b:Landroid/content/Intent;

    const-string v1, "reuqest_new_location"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 129
    iget-object v1, p0, Lagd;->b:Landroid/content/Intent;

    const-string v2, "force_high_accuracy"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 130
    iget-object v2, p0, Lagd;->b:Landroid/content/Intent;

    const-string v3, "uid"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 131
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "requestNewLocation="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " forceHighAccuracy:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lalj;->a(Ljava/lang/String;)V

    .line 132
    iget-object v3, p0, Lagd;->c:Lcom/sec/android/GeoLookout/DisasterService;

    invoke-static {v3, v0, v1, v2}, Lcom/sec/android/GeoLookout/DisasterService;->a(Lcom/sec/android/GeoLookout/DisasterService;ZZI)V

    goto/16 :goto_0

    .line 133
    :cond_8
    const-string v0, "com.sec.android.GeoLookout.ACTION_CHECK_CURRENT_LOCATION"

    iget-object v1, p0, Lagd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 134
    iget-object v0, p0, Lagd;->c:Lcom/sec/android/GeoLookout/DisasterService;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/DisasterService;->c(Lcom/sec/android/GeoLookout/DisasterService;)V

    goto/16 :goto_0

    .line 135
    :cond_9
    const-string v0, "com.sec.android.GeoLookout.ACTION_CLOSE_LOCATION"

    iget-object v1, p0, Lagd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 136
    iget-object v0, p0, Lagd;->c:Lcom/sec/android/GeoLookout/DisasterService;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/DisasterService;->b()V

    goto/16 :goto_0

    .line 137
    :cond_a
    const-string v0, "com.sec.android.GeoLookout.ACTION_NOTIFICATION_CANCELED"

    iget-object v1, p0, Lagd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 138
    iget-object v0, p0, Lagd;->b:Landroid/content/Intent;

    const-string v1, "DID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 139
    iget-object v1, p0, Lagd;->c:Lcom/sec/android/GeoLookout/DisasterService;

    invoke-static {v1, v0}, Lcom/sec/android/GeoLookout/DisasterService;->b(Lcom/sec/android/GeoLookout/DisasterService;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 140
    :cond_b
    const-string v0, "com.sec.android.geolookout.updatelocation"

    iget-object v1, p0, Lagd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 141
    iget-object v0, p0, Lagd;->c:Lcom/sec/android/GeoLookout/DisasterService;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/DisasterService;->d(Lcom/sec/android/GeoLookout/DisasterService;)V

    goto/16 :goto_0

    .line 143
    :cond_c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unknown action! action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lagd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
