.class public final Lagt;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final EmptyString:I = 0x7f0a0000

.field public static final STR_Btn_Cancel:I = 0x7f0a0001

.field public static final STR_Btn_OK:I = 0x7f0a0002

.field public static final STR_Btn_Settings:I = 0x7f0a0003

.field public static final STR_Connecting_Server:I = 0x7f0a0004

.field public static final STR_Deactivation_Failed:I = 0x7f0a0005

.field public static final STR_Deregister_Fail:I = 0x7f0a0006

.field public static final STR_Deregister_Success:I = 0x7f0a0007

.field public static final STR_Deregistering_Device:I = 0x7f0a0008

.field public static final STR_Failed_to_find_location:I = 0x7f0a0009

.field public static final STR_Failed_to_find_location_header:I = 0x7f0a000a

.field public static final STR_Failed_to_find_location_header_vzw:I = 0x7f0a000b

.field public static final STR_Location_disabled:I = 0x7f0a000c

.field public static final STR_Location_disabled_noti:I = 0x7f0a000d

.field public static final STR_Location_disabled_vzw:I = 0x7f0a000e

.field public static final STR_Network_Fail:I = 0x7f0a000f

.field public static final STR_Network_error:I = 0x7f0a0010

.field public static final STR_No_network_connection:I = 0x7f0a0011

.field public static final STR_No_sim:I = 0x7f0a0012

.field public static final STR_Process_Fail:I = 0x7f0a0013

.field public static final STR_Register_Fail:I = 0x7f0a0014

.field public static final STR_Register_Success:I = 0x7f0a0015

.field public static final STR_Registering_Device:I = 0x7f0a0016

.field public static final STR_Roaming_Network_Fail:I = 0x7f0a0017

.field public static final STR_Roaming_Network_Fails:I = 0x7f0a0018

.field public static final STR_Roaming_Wifi_Only:I = 0x7f0a0019

.field public static final STR_Roaming_Wifi_Onlys:I = 0x7f0a001a

.field public static final STR_System_error:I = 0x7f0a001b

.field public static final Title_Using_Data_Background:I = 0x7f0a001c

.field public static final Unknown_location:I = 0x7f0a001d

.field public static final Using_Data_Background:I = 0x7f0a001e

.field public static final actionbar_changemode_button_description:I = 0x7f0a001f

.field public static final actionbar_s_safety:I = 0x7f0a0020

.field public static final agree:I = 0x7f0a0021

.field public static final alarm_type:I = 0x7f0a0022

.field public static final alarm_type_setting_item_mute:I = 0x7f0a0023

.field public static final alarm_type_setting_item_sound:I = 0x7f0a0024

.field public static final alarm_type_setting_item_vibration:I = 0x7f0a0025

.field public static final alarm_type_setting_item_vibration_and_sound:I = 0x7f0a0026

.field public static final alarm_type_setting_popup_title:I = 0x7f0a0027

.field public static final alarm_type_using_device_sound_setting:I = 0x7f0a0028

.field public static final app_name:I = 0x7f0a0029

.field public static final app_name_only_geo_news:I = 0x7f0a002a

.field public static final are_you_ok:I = 0x7f0a002b

.field public static final assgin_contact:I = 0x7f0a002c

.field public static final authdlg_auto_reply:I = 0x7f0a002d

.field public static final auto_refresh_dialog_do_not_show_again:I = 0x7f0a002e

.field public static final auto_refresh_dialog_text:I = 0x7f0a002f

.field public static final baengnyeongdo:I = 0x7f0a0030

.field public static final base_url:I = 0x7f0a0031

.field public static final busan:I = 0x7f0a0032

.field public static final call_me:I = 0x7f0a0033

.field public static final cancel_popup_message:I = 0x7f0a0034

.field public static final cancel_popup_title:I = 0x7f0a0035

.field public static final cheongju:I = 0x7f0a0036

.field public static final chn_mismatch_cp_type:I = 0x7f0a0037

.field public static final daegu:I = 0x7f0a0038

.field public static final daejeon:I = 0x7f0a0039

.field public static final dashboard_emergency_contact:I = 0x7f0a003a

.field public static final dashboard_primary_contact:I = 0x7f0a003b

.field public static final days_ago:I = 0x7f0a003c

.field public static final delete:I = 0x7f0a003d

.field public static final delete_location:I = 0x7f0a003e

.field public static final description_degree:I = 0x7f0a003f

.field public static final description_effective:I = 0x7f0a0040

.field public static final description_meter_per_sec:I = 0x7f0a0041

.field public static final description_mile_per_hour:I = 0x7f0a0042

.field public static final description_pressure:I = 0x7f0a0043

.field public static final description_reported:I = 0x7f0a0044

.field public static final description_speed:I = 0x7f0a0045

.field public static final description_speed_mph:I = 0x7f0a0046

.field public static final description_windspeed:I = 0x7f0a0047

.field public static final dialog_block_button:I = 0x7f0a0048

.field public static final disagree:I = 0x7f0a0049

.field public static final disaster_alert:I = 0x7f0a004a

.field public static final disaster_dialog_cancel:I = 0x7f0a004b

.field public static final disaster_dialog_detail_view:I = 0x7f0a004c

.field public static final disaster_filter_info_discription:I = 0x7f0a004d

.field public static final disaster_filter_pref_region_japan:I = 0x7f0a004e

.field public static final disaster_filter_pref_title_warning:I = 0x7f0a004f

.field public static final disaster_filter_pref_title_watch:I = 0x7f0a0050

.field public static final disaster_history_title:I = 0x7f0a0051

.field public static final disaster_menu_settings:I = 0x7f0a0052

.field public static final disaster_name_Hail:I = 0x7f0a0053

.field public static final disaster_name_active_wildfire:I = 0x7f0a0054

.field public static final disaster_name_air_pollution:I = 0x7f0a0055

.field public static final disaster_name_avalanche:I = 0x7f0a0056

.field public static final disaster_name_avalanche_jp:I = 0x7f0a0057

.field public static final disaster_name_cma_name:I = 0x7f0a0058

.field public static final disaster_name_cold:I = 0x7f0a0059

.field public static final disaster_name_cold_wave:I = 0x7f0a005a

.field public static final disaster_name_cool_down:I = 0x7f0a005b

.field public static final disaster_name_dense_fog:I = 0x7f0a005c

.field public static final disaster_name_drought:I = 0x7f0a005d

.field public static final disaster_name_dry:I = 0x7f0a005e

.field public static final disaster_name_dry_jp:I = 0x7f0a005f

.field public static final disaster_name_dust_haze:I = 0x7f0a0060

.field public static final disaster_name_dust_storm:I = 0x7f0a0061

.field public static final disaster_name_dust_storms:I = 0x7f0a0062

.field public static final disaster_name_earthquake:I = 0x7f0a0063

.field public static final disaster_name_earthquake_jp:I = 0x7f0a0064

.field public static final disaster_name_extreme_cold:I = 0x7f0a0065

.field public static final disaster_name_extreme_cold_jp:I = 0x7f0a0066

.field public static final disaster_name_extreme_heat:I = 0x7f0a0067

.field public static final disaster_name_flood:I = 0x7f0a0068

.field public static final disaster_name_flood_jp:I = 0x7f0a0069

.field public static final disaster_name_forest_fire:I = 0x7f0a006a

.field public static final disaster_name_freezing_jp:I = 0x7f0a006b

.field public static final disaster_name_frost:I = 0x7f0a006c

.field public static final disaster_name_frost_jp:I = 0x7f0a006d

.field public static final disaster_name_gale:I = 0x7f0a006e

.field public static final disaster_name_haze:I = 0x7f0a006f

.field public static final disaster_name_heat:I = 0x7f0a0070

.field public static final disaster_name_heavy_rain:I = 0x7f0a0071

.field public static final disaster_name_heavy_rain_jp:I = 0x7f0a0072

.field public static final disaster_name_heavy_snow:I = 0x7f0a0073

.field public static final disaster_name_heavy_snow_jp:I = 0x7f0a0074

.field public static final disaster_name_high_seas:I = 0x7f0a0075

.field public static final disaster_name_high_seas_jp:I = 0x7f0a0076

.field public static final disaster_name_high_winds:I = 0x7f0a0077

.field public static final disaster_name_high_winds_jp:I = 0x7f0a0078

.field public static final disaster_name_icy_road:I = 0x7f0a0079

.field public static final disaster_name_lightning:I = 0x7f0a007a

.field public static final disaster_name_others:I = 0x7f0a007b

.field public static final disaster_name_rainstorms:I = 0x7f0a007c

.field public static final disaster_name_road_snow:I = 0x7f0a007d

.field public static final disaster_name_severe_thunderstorms:I = 0x7f0a007e

.field public static final disaster_name_snow_accretion_jp:I = 0x7f0a007f

.field public static final disaster_name_snow_and_ice:I = 0x7f0a0080

.field public static final disaster_name_snow_and_ice_jp:I = 0x7f0a0081

.field public static final disaster_name_snow_melting_jp:I = 0x7f0a0082

.field public static final disaster_name_snowstorms:I = 0x7f0a0083

.field public static final disaster_name_storm_surge_jp:I = 0x7f0a0084

.field public static final disaster_name_thick_fog_jp:I = 0x7f0a0085

.field public static final disaster_name_thunderstorm:I = 0x7f0a0086

.field public static final disaster_name_thunderstorm_and_gale:I = 0x7f0a0087

.field public static final disaster_name_thunderstorm_jp:I = 0x7f0a0088

.field public static final disaster_name_tornado:I = 0x7f0a0089

.field public static final disaster_name_tropical_storm:I = 0x7f0a008a

.field public static final disaster_name_tsunami:I = 0x7f0a008b

.field public static final disaster_name_tsunami_jp:I = 0x7f0a008c

.field public static final disaster_name_typhoon:I = 0x7f0a008d

.field public static final disaster_name_typhoon_jp:I = 0x7f0a008e

.field public static final disaster_name_typhoons:I = 0x7f0a008f

.field public static final disaster_name_volcanic_ash:I = 0x7f0a0090

.field public static final disaster_name_volcano:I = 0x7f0a0091

.field public static final disaster_name_volcano_jp:I = 0x7f0a0092

.field public static final disaster_name_wind_storm_jp:I = 0x7f0a0093

.field public static final disaster_name_winter_storms:I = 0x7f0a0094

.field public static final disaster_name_winter_storms_jp:I = 0x7f0a0095

.field public static final disaster_pref_set_alert_about_disaster:I = 0x7f0a0096

.field public static final disaster_report_ended:I = 0x7f0a0097

.field public static final disaster_report_ongoing:I = 0x7f0a0098

.field public static final disaster_report_title:I = 0x7f0a0099

.field public static final distance_away:I = 0x7f0a009a

.field public static final distance_away_mile:I = 0x7f0a009b

.field public static final distance_away_one_mile:I = 0x7f0a009c

.field public static final distance_measurement_failed:I = 0x7f0a009d

.field public static final do_not_show_again:I = 0x7f0a009e

.field public static final drawer_close:I = 0x7f0a009f

.field public static final drawer_open:I = 0x7f0a00a0

.field public static final edit_location:I = 0x7f0a00a1

.field public static final enable_geo_news_message:I = 0x7f0a00a2

.field public static final enable_geo_news_title:I = 0x7f0a00a3

.field public static final fail_to_connect_server:I = 0x7f0a00a4

.field public static final flybox_maximum_wind:I = 0x7f0a00a5

.field public static final flybox_storm_direction:I = 0x7f0a00a6

.field public static final flybox_storm_speed:I = 0x7f0a00a7

.field public static final gangneung:I = 0x7f0a00a8

.field public static final geo_lookout_title:I = 0x7f0a00a9

.field public static final gwangju:I = 0x7f0a00aa

.field public static final history:I = 0x7f0a00ab

.field public static final homepage_url_TWC:I = 0x7f0a00ac

.field public static final homepage_url_WEATHERNEWS:I = 0x7f0a00ad

.field public static final homepage_url_WEATHERNEWS_JP:I = 0x7f0a00ae

.field public static final i_am_ok:I = 0x7f0a00af

.field public static final i_need_help:I = 0x7f0a00b0

.field public static final jeju:I = 0x7f0a00b1

.field public static final jeonju:I = 0x7f0a00b2

.field public static final jpn_current_location:I = 0x7f0a00b3

.field public static final jpn_depth:I = 0x7f0a00b4

.field public static final jpn_dialog_attention:I = 0x7f0a00b5

.field public static final jpn_dialog_check_contact:I = 0x7f0a00b6

.field public static final jpn_dialog_no_sim_inserted:I = 0x7f0a00b7

.field public static final jpn_dialog_sms_successfully_sent:I = 0x7f0a00b8

.field public static final jpn_dialog_success:I = 0x7f0a00b9

.field public static final jpn_earthquake_deep:I = 0x7f0a00ba

.field public static final jpn_earthquake_intensity:I = 0x7f0a00bb

.field public static final jpn_epi:I = 0x7f0a00bc

.field public static final jpn_epicenter:I = 0x7f0a00bd

.field public static final jpn_intensity:I = 0x7f0a00be

.field public static final jpn_message_is_sent:I = 0x7f0a00bf

.field public static final jpn_no_geo_news:I = 0x7f0a00c0

.field public static final jpn_no_reciepient_when_send_msg:I = 0x7f0a00c1

.field public static final jpn_notification:I = 0x7f0a00c2

.field public static final jpn_radius:I = 0x7f0a00c3

.field public static final jpn_shelter:I = 0x7f0a00c4

.field public static final jpn_your_location:I = 0x7f0a00c5

.field public static final lifemode_alert_charging_popup_body:I = 0x7f0a00c6

.field public static final lifemode_alert_charging_popup_title:I = 0x7f0a00c7

.field public static final lifemode_chilblain_text:I = 0x7f0a00c8

.field public static final lifemode_city_akita:I = 0x7f0a00c9

.field public static final lifemode_city_fukuoka:I = 0x7f0a00ca

.field public static final lifemode_city_hiroshima:I = 0x7f0a00cb

.field public static final lifemode_city_kagoshima:I = 0x7f0a00cc

.field public static final lifemode_city_kanajawa:I = 0x7f0a00cd

.field public static final lifemode_city_kushiro:I = 0x7f0a00ce

.field public static final lifemode_city_nagoya:I = 0x7f0a00cf

.field public static final lifemode_city_okinawa:I = 0x7f0a00d0

.field public static final lifemode_city_osaka:I = 0x7f0a00d1

.field public static final lifemode_city_sapporo:I = 0x7f0a00d2

.field public static final lifemode_city_sendai:I = 0x7f0a00d3

.field public static final lifemode_city_takamatsu:I = 0x7f0a00d4

.field public static final lifemode_city_tokyo:I = 0x7f0a00d5

.field public static final lifemode_disaster_map:I = 0x7f0a00d6

.field public static final lifemode_etc:I = 0x7f0a00d7

.field public static final lifemode_flu_text:I = 0x7f0a00d8

.field public static final lifemode_food_poison_text:I = 0x7f0a00d9

.field public static final lifemode_freeze_text:I = 0x7f0a00da

.field public static final lifemode_geoindex:I = 0x7f0a00db

.field public static final lifemode_heat_text:I = 0x7f0a00dc

.field public static final lifemode_info_update_period:I = 0x7f0a00dd

.field public static final lifemode_info_update_period_next_year:I = 0x7f0a00de

.field public static final lifemode_info_update_time:I = 0x7f0a00df

.field public static final lifemode_info_update_time_every_hours:I = 0x7f0a00e0

.field public static final lifemode_kor_level_text_0:I = 0x7f0a00e1

.field public static final lifemode_kor_level_text_1:I = 0x7f0a00e2

.field public static final lifemode_kor_level_text_2:I = 0x7f0a00e3

.field public static final lifemode_kor_level_text_3:I = 0x7f0a00e4

.field public static final lifemode_kor_level_text_4:I = 0x7f0a00e5

.field public static final lifemode_kor_level_text_5:I = 0x7f0a00e6

.field public static final lifemode_kor_level_text_none:I = 0x7f0a00e7

.field public static final lifemode_kor_pm10_level_text_1:I = 0x7f0a00e8

.field public static final lifemode_kor_pm10_level_text_2:I = 0x7f0a00e9

.field public static final lifemode_kor_pm10_level_text_3:I = 0x7f0a00ea

.field public static final lifemode_kor_pm10_level_text_4:I = 0x7f0a00eb

.field public static final lifemode_kor_pm10_level_text_5:I = 0x7f0a00ec

.field public static final lifemode_kor_tip_a_pollu_1_1:I = 0x7f0a00ed

.field public static final lifemode_kor_tip_a_pollu_1_2:I = 0x7f0a00ee

.field public static final lifemode_kor_tip_a_pollu_1_3:I = 0x7f0a00ef

.field public static final lifemode_kor_tip_a_pollu_1_4:I = 0x7f0a00f0

.field public static final lifemode_kor_tip_a_pollu_1_5:I = 0x7f0a00f1

.field public static final lifemode_kor_tip_a_pollu_1_6:I = 0x7f0a00f2

.field public static final lifemode_kor_tip_a_pollu_1_7:I = 0x7f0a00f3

.field public static final lifemode_kor_tip_a_pollu_1_8:I = 0x7f0a00f4

.field public static final lifemode_kor_tip_a_pollu_1_9:I = 0x7f0a00f5

.field public static final lifemode_kor_tip_a_pollu_2_1:I = 0x7f0a00f6

.field public static final lifemode_kor_tip_a_pollu_2_10:I = 0x7f0a00f7

.field public static final lifemode_kor_tip_a_pollu_2_2:I = 0x7f0a00f8

.field public static final lifemode_kor_tip_a_pollu_2_3:I = 0x7f0a00f9

.field public static final lifemode_kor_tip_a_pollu_2_4:I = 0x7f0a00fa

.field public static final lifemode_kor_tip_a_pollu_2_5:I = 0x7f0a00fb

.field public static final lifemode_kor_tip_a_pollu_2_6:I = 0x7f0a00fc

.field public static final lifemode_kor_tip_a_pollu_2_7:I = 0x7f0a00fd

.field public static final lifemode_kor_tip_a_pollu_2_8:I = 0x7f0a00fe

.field public static final lifemode_kor_tip_a_pollu_2_9:I = 0x7f0a00ff

.field public static final lifemode_kor_tip_a_pollu_3_1:I = 0x7f0a0100

.field public static final lifemode_kor_tip_a_pollu_3_10:I = 0x7f0a0101

.field public static final lifemode_kor_tip_a_pollu_3_2:I = 0x7f0a0102

.field public static final lifemode_kor_tip_a_pollu_3_3:I = 0x7f0a0103

.field public static final lifemode_kor_tip_a_pollu_3_4:I = 0x7f0a0104

.field public static final lifemode_kor_tip_a_pollu_3_5:I = 0x7f0a0105

.field public static final lifemode_kor_tip_a_pollu_3_6:I = 0x7f0a0106

.field public static final lifemode_kor_tip_a_pollu_3_7:I = 0x7f0a0107

.field public static final lifemode_kor_tip_a_pollu_3_8:I = 0x7f0a0108

.field public static final lifemode_kor_tip_a_pollu_3_9:I = 0x7f0a0109

.field public static final lifemode_kor_tip_a_pollu_4_1:I = 0x7f0a010a

.field public static final lifemode_kor_tip_a_pollu_4_10:I = 0x7f0a010b

.field public static final lifemode_kor_tip_a_pollu_4_2:I = 0x7f0a010c

.field public static final lifemode_kor_tip_a_pollu_4_3:I = 0x7f0a010d

.field public static final lifemode_kor_tip_a_pollu_4_4:I = 0x7f0a010e

.field public static final lifemode_kor_tip_a_pollu_4_5:I = 0x7f0a010f

.field public static final lifemode_kor_tip_a_pollu_4_6:I = 0x7f0a0110

.field public static final lifemode_kor_tip_a_pollu_4_7:I = 0x7f0a0111

.field public static final lifemode_kor_tip_a_pollu_4_8:I = 0x7f0a0112

.field public static final lifemode_kor_tip_a_pollu_4_9:I = 0x7f0a0113

.field public static final lifemode_kor_tip_cold_1_1:I = 0x7f0a0114

.field public static final lifemode_kor_tip_cold_1_2:I = 0x7f0a0115

.field public static final lifemode_kor_tip_cold_1_3:I = 0x7f0a0116

.field public static final lifemode_kor_tip_cold_1_4:I = 0x7f0a0117

.field public static final lifemode_kor_tip_cold_1_5:I = 0x7f0a0118

.field public static final lifemode_kor_tip_cold_1_6:I = 0x7f0a0119

.field public static final lifemode_kor_tip_cold_1_7:I = 0x7f0a011a

.field public static final lifemode_kor_tip_cold_1_8:I = 0x7f0a011b

.field public static final lifemode_kor_tip_cold_1_9:I = 0x7f0a011c

.field public static final lifemode_kor_tip_cold_2_1:I = 0x7f0a011d

.field public static final lifemode_kor_tip_cold_2_2:I = 0x7f0a011e

.field public static final lifemode_kor_tip_cold_2_3:I = 0x7f0a011f

.field public static final lifemode_kor_tip_cold_2_4:I = 0x7f0a0120

.field public static final lifemode_kor_tip_cold_2_5:I = 0x7f0a0121

.field public static final lifemode_kor_tip_cold_2_6:I = 0x7f0a0122

.field public static final lifemode_kor_tip_cold_2_7:I = 0x7f0a0123

.field public static final lifemode_kor_tip_cold_2_8:I = 0x7f0a0124

.field public static final lifemode_kor_tip_cold_2_9:I = 0x7f0a0125

.field public static final lifemode_kor_tip_cold_3_1:I = 0x7f0a0126

.field public static final lifemode_kor_tip_cold_3_2:I = 0x7f0a0127

.field public static final lifemode_kor_tip_cold_3_3:I = 0x7f0a0128

.field public static final lifemode_kor_tip_cold_3_4:I = 0x7f0a0129

.field public static final lifemode_kor_tip_cold_3_5:I = 0x7f0a012a

.field public static final lifemode_kor_tip_cold_3_6:I = 0x7f0a012b

.field public static final lifemode_kor_tip_cold_3_7:I = 0x7f0a012c

.field public static final lifemode_kor_tip_cold_3_8:I = 0x7f0a012d

.field public static final lifemode_kor_tip_cold_3_9:I = 0x7f0a012e

.field public static final lifemode_kor_tip_cold_4_1:I = 0x7f0a012f

.field public static final lifemode_kor_tip_cold_4_10:I = 0x7f0a0130

.field public static final lifemode_kor_tip_cold_4_2:I = 0x7f0a0131

.field public static final lifemode_kor_tip_cold_4_3:I = 0x7f0a0132

.field public static final lifemode_kor_tip_cold_4_4:I = 0x7f0a0133

.field public static final lifemode_kor_tip_cold_4_5:I = 0x7f0a0134

.field public static final lifemode_kor_tip_cold_4_6:I = 0x7f0a0135

.field public static final lifemode_kor_tip_cold_4_7:I = 0x7f0a0136

.field public static final lifemode_kor_tip_cold_4_8:I = 0x7f0a0137

.field public static final lifemode_kor_tip_cold_4_9:I = 0x7f0a0138

.field public static final lifemode_kor_tip_f_poison_1_1:I = 0x7f0a0139

.field public static final lifemode_kor_tip_f_poison_1_10:I = 0x7f0a013a

.field public static final lifemode_kor_tip_f_poison_1_2:I = 0x7f0a013b

.field public static final lifemode_kor_tip_f_poison_1_3:I = 0x7f0a013c

.field public static final lifemode_kor_tip_f_poison_1_4:I = 0x7f0a013d

.field public static final lifemode_kor_tip_f_poison_1_5:I = 0x7f0a013e

.field public static final lifemode_kor_tip_f_poison_1_6:I = 0x7f0a013f

.field public static final lifemode_kor_tip_f_poison_1_7:I = 0x7f0a0140

.field public static final lifemode_kor_tip_f_poison_1_8:I = 0x7f0a0141

.field public static final lifemode_kor_tip_f_poison_1_9:I = 0x7f0a0142

.field public static final lifemode_kor_tip_f_poison_2_1:I = 0x7f0a0143

.field public static final lifemode_kor_tip_f_poison_2_10:I = 0x7f0a0144

.field public static final lifemode_kor_tip_f_poison_2_2:I = 0x7f0a0145

.field public static final lifemode_kor_tip_f_poison_2_3:I = 0x7f0a0146

.field public static final lifemode_kor_tip_f_poison_2_4:I = 0x7f0a0147

.field public static final lifemode_kor_tip_f_poison_2_5:I = 0x7f0a0148

.field public static final lifemode_kor_tip_f_poison_2_6:I = 0x7f0a0149

.field public static final lifemode_kor_tip_f_poison_2_7:I = 0x7f0a014a

.field public static final lifemode_kor_tip_f_poison_2_8:I = 0x7f0a014b

.field public static final lifemode_kor_tip_f_poison_2_9:I = 0x7f0a014c

.field public static final lifemode_kor_tip_f_poison_3_1:I = 0x7f0a014d

.field public static final lifemode_kor_tip_f_poison_3_10:I = 0x7f0a014e

.field public static final lifemode_kor_tip_f_poison_3_2:I = 0x7f0a014f

.field public static final lifemode_kor_tip_f_poison_3_3:I = 0x7f0a0150

.field public static final lifemode_kor_tip_f_poison_3_4:I = 0x7f0a0151

.field public static final lifemode_kor_tip_f_poison_3_5:I = 0x7f0a0152

.field public static final lifemode_kor_tip_f_poison_3_6:I = 0x7f0a0153

.field public static final lifemode_kor_tip_f_poison_3_7:I = 0x7f0a0154

.field public static final lifemode_kor_tip_f_poison_3_8:I = 0x7f0a0155

.field public static final lifemode_kor_tip_f_poison_3_9:I = 0x7f0a0156

.field public static final lifemode_kor_tip_f_poison_4_1:I = 0x7f0a0157

.field public static final lifemode_kor_tip_f_poison_4_10:I = 0x7f0a0158

.field public static final lifemode_kor_tip_f_poison_4_11:I = 0x7f0a0159

.field public static final lifemode_kor_tip_f_poison_4_2:I = 0x7f0a015a

.field public static final lifemode_kor_tip_f_poison_4_3:I = 0x7f0a015b

.field public static final lifemode_kor_tip_f_poison_4_4:I = 0x7f0a015c

.field public static final lifemode_kor_tip_f_poison_4_5:I = 0x7f0a015d

.field public static final lifemode_kor_tip_f_poison_4_6:I = 0x7f0a015e

.field public static final lifemode_kor_tip_f_poison_4_7:I = 0x7f0a015f

.field public static final lifemode_kor_tip_f_poison_4_8:I = 0x7f0a0160

.field public static final lifemode_kor_tip_f_poison_4_9:I = 0x7f0a0161

.field public static final lifemode_kor_tip_freez_1_1:I = 0x7f0a0162

.field public static final lifemode_kor_tip_freez_1_10:I = 0x7f0a0163

.field public static final lifemode_kor_tip_freez_1_2:I = 0x7f0a0164

.field public static final lifemode_kor_tip_freez_1_3:I = 0x7f0a0165

.field public static final lifemode_kor_tip_freez_1_4:I = 0x7f0a0166

.field public static final lifemode_kor_tip_freez_1_5:I = 0x7f0a0167

.field public static final lifemode_kor_tip_freez_1_6:I = 0x7f0a0168

.field public static final lifemode_kor_tip_freez_1_7:I = 0x7f0a0169

.field public static final lifemode_kor_tip_freez_1_8:I = 0x7f0a016a

.field public static final lifemode_kor_tip_freez_1_9:I = 0x7f0a016b

.field public static final lifemode_kor_tip_freez_2_1:I = 0x7f0a016c

.field public static final lifemode_kor_tip_freez_2_10:I = 0x7f0a016d

.field public static final lifemode_kor_tip_freez_2_2:I = 0x7f0a016e

.field public static final lifemode_kor_tip_freez_2_3:I = 0x7f0a016f

.field public static final lifemode_kor_tip_freez_2_4:I = 0x7f0a0170

.field public static final lifemode_kor_tip_freez_2_5:I = 0x7f0a0171

.field public static final lifemode_kor_tip_freez_2_6:I = 0x7f0a0172

.field public static final lifemode_kor_tip_freez_2_7:I = 0x7f0a0173

.field public static final lifemode_kor_tip_freez_2_8:I = 0x7f0a0174

.field public static final lifemode_kor_tip_freez_2_9:I = 0x7f0a0175

.field public static final lifemode_kor_tip_freez_3_1:I = 0x7f0a0176

.field public static final lifemode_kor_tip_freez_3_10:I = 0x7f0a0177

.field public static final lifemode_kor_tip_freez_3_2:I = 0x7f0a0178

.field public static final lifemode_kor_tip_freez_3_3:I = 0x7f0a0179

.field public static final lifemode_kor_tip_freez_3_4:I = 0x7f0a017a

.field public static final lifemode_kor_tip_freez_3_5:I = 0x7f0a017b

.field public static final lifemode_kor_tip_freez_3_6:I = 0x7f0a017c

.field public static final lifemode_kor_tip_freez_3_7:I = 0x7f0a017d

.field public static final lifemode_kor_tip_freez_3_8:I = 0x7f0a017e

.field public static final lifemode_kor_tip_freez_3_9:I = 0x7f0a017f

.field public static final lifemode_kor_tip_freez_4_1:I = 0x7f0a0180

.field public static final lifemode_kor_tip_freez_4_10:I = 0x7f0a0181

.field public static final lifemode_kor_tip_freez_4_2:I = 0x7f0a0182

.field public static final lifemode_kor_tip_freez_4_3:I = 0x7f0a0183

.field public static final lifemode_kor_tip_freez_4_4:I = 0x7f0a0184

.field public static final lifemode_kor_tip_freez_4_5:I = 0x7f0a0185

.field public static final lifemode_kor_tip_freez_4_6:I = 0x7f0a0186

.field public static final lifemode_kor_tip_freez_4_7:I = 0x7f0a0187

.field public static final lifemode_kor_tip_freez_4_8:I = 0x7f0a0188

.field public static final lifemode_kor_tip_freez_4_9:I = 0x7f0a0189

.field public static final lifemode_kor_tip_fros_1_1:I = 0x7f0a018a

.field public static final lifemode_kor_tip_fros_1_10:I = 0x7f0a018b

.field public static final lifemode_kor_tip_fros_1_2:I = 0x7f0a018c

.field public static final lifemode_kor_tip_fros_1_3:I = 0x7f0a018d

.field public static final lifemode_kor_tip_fros_1_4:I = 0x7f0a018e

.field public static final lifemode_kor_tip_fros_1_5:I = 0x7f0a018f

.field public static final lifemode_kor_tip_fros_1_6:I = 0x7f0a0190

.field public static final lifemode_kor_tip_fros_1_7:I = 0x7f0a0191

.field public static final lifemode_kor_tip_fros_1_8:I = 0x7f0a0192

.field public static final lifemode_kor_tip_fros_1_9:I = 0x7f0a0193

.field public static final lifemode_kor_tip_fros_2_1:I = 0x7f0a0194

.field public static final lifemode_kor_tip_fros_2_10:I = 0x7f0a0195

.field public static final lifemode_kor_tip_fros_2_2:I = 0x7f0a0196

.field public static final lifemode_kor_tip_fros_2_3:I = 0x7f0a0197

.field public static final lifemode_kor_tip_fros_2_4:I = 0x7f0a0198

.field public static final lifemode_kor_tip_fros_2_5:I = 0x7f0a0199

.field public static final lifemode_kor_tip_fros_2_6:I = 0x7f0a019a

.field public static final lifemode_kor_tip_fros_2_7:I = 0x7f0a019b

.field public static final lifemode_kor_tip_fros_2_8:I = 0x7f0a019c

.field public static final lifemode_kor_tip_fros_2_9:I = 0x7f0a019d

.field public static final lifemode_kor_tip_fros_3_1:I = 0x7f0a019e

.field public static final lifemode_kor_tip_fros_3_10:I = 0x7f0a019f

.field public static final lifemode_kor_tip_fros_3_2:I = 0x7f0a01a0

.field public static final lifemode_kor_tip_fros_3_3:I = 0x7f0a01a1

.field public static final lifemode_kor_tip_fros_3_4:I = 0x7f0a01a2

.field public static final lifemode_kor_tip_fros_3_5:I = 0x7f0a01a3

.field public static final lifemode_kor_tip_fros_3_6:I = 0x7f0a01a4

.field public static final lifemode_kor_tip_fros_3_7:I = 0x7f0a01a5

.field public static final lifemode_kor_tip_fros_3_8:I = 0x7f0a01a6

.field public static final lifemode_kor_tip_fros_3_9:I = 0x7f0a01a7

.field public static final lifemode_kor_tip_fros_4_1:I = 0x7f0a01a8

.field public static final lifemode_kor_tip_fros_4_10:I = 0x7f0a01a9

.field public static final lifemode_kor_tip_fros_4_2:I = 0x7f0a01aa

.field public static final lifemode_kor_tip_fros_4_3:I = 0x7f0a01ab

.field public static final lifemode_kor_tip_fros_4_4:I = 0x7f0a01ac

.field public static final lifemode_kor_tip_fros_4_5:I = 0x7f0a01ad

.field public static final lifemode_kor_tip_fros_4_6:I = 0x7f0a01ae

.field public static final lifemode_kor_tip_fros_4_7:I = 0x7f0a01af

.field public static final lifemode_kor_tip_fros_4_8:I = 0x7f0a01b0

.field public static final lifemode_kor_tip_fros_4_9:I = 0x7f0a01b1

.field public static final lifemode_kor_tip_humidity_1_1:I = 0x7f0a01b2

.field public static final lifemode_kor_tip_humidity_1_10:I = 0x7f0a01b3

.field public static final lifemode_kor_tip_humidity_1_2:I = 0x7f0a01b4

.field public static final lifemode_kor_tip_humidity_1_3:I = 0x7f0a01b5

.field public static final lifemode_kor_tip_humidity_1_4:I = 0x7f0a01b6

.field public static final lifemode_kor_tip_humidity_1_5:I = 0x7f0a01b7

.field public static final lifemode_kor_tip_humidity_1_6:I = 0x7f0a01b8

.field public static final lifemode_kor_tip_humidity_1_7:I = 0x7f0a01b9

.field public static final lifemode_kor_tip_humidity_1_8:I = 0x7f0a01ba

.field public static final lifemode_kor_tip_humidity_1_9:I = 0x7f0a01bb

.field public static final lifemode_kor_tip_humidity_2_1:I = 0x7f0a01bc

.field public static final lifemode_kor_tip_humidity_2_10:I = 0x7f0a01bd

.field public static final lifemode_kor_tip_humidity_2_2:I = 0x7f0a01be

.field public static final lifemode_kor_tip_humidity_2_3:I = 0x7f0a01bf

.field public static final lifemode_kor_tip_humidity_2_4:I = 0x7f0a01c0

.field public static final lifemode_kor_tip_humidity_2_5:I = 0x7f0a01c1

.field public static final lifemode_kor_tip_humidity_2_6:I = 0x7f0a01c2

.field public static final lifemode_kor_tip_humidity_2_7:I = 0x7f0a01c3

.field public static final lifemode_kor_tip_humidity_2_8:I = 0x7f0a01c4

.field public static final lifemode_kor_tip_humidity_2_9:I = 0x7f0a01c5

.field public static final lifemode_kor_tip_humidity_3_1:I = 0x7f0a01c6

.field public static final lifemode_kor_tip_humidity_3_10:I = 0x7f0a01c7

.field public static final lifemode_kor_tip_humidity_3_2:I = 0x7f0a01c8

.field public static final lifemode_kor_tip_humidity_3_3:I = 0x7f0a01c9

.field public static final lifemode_kor_tip_humidity_3_4:I = 0x7f0a01ca

.field public static final lifemode_kor_tip_humidity_3_5:I = 0x7f0a01cb

.field public static final lifemode_kor_tip_humidity_3_6:I = 0x7f0a01cc

.field public static final lifemode_kor_tip_humidity_3_7:I = 0x7f0a01cd

.field public static final lifemode_kor_tip_humidity_3_8:I = 0x7f0a01ce

.field public static final lifemode_kor_tip_humidity_3_9:I = 0x7f0a01cf

.field public static final lifemode_kor_tip_humidity_4_1:I = 0x7f0a01d0

.field public static final lifemode_kor_tip_humidity_4_10:I = 0x7f0a01d1

.field public static final lifemode_kor_tip_humidity_4_2:I = 0x7f0a01d2

.field public static final lifemode_kor_tip_humidity_4_3:I = 0x7f0a01d3

.field public static final lifemode_kor_tip_humidity_4_4:I = 0x7f0a01d4

.field public static final lifemode_kor_tip_humidity_4_5:I = 0x7f0a01d5

.field public static final lifemode_kor_tip_humidity_4_6:I = 0x7f0a01d6

.field public static final lifemode_kor_tip_humidity_4_7:I = 0x7f0a01d7

.field public static final lifemode_kor_tip_humidity_4_8:I = 0x7f0a01d8

.field public static final lifemode_kor_tip_humidity_4_9:I = 0x7f0a01d9

.field public static final lifemode_kor_tip_pollen_1_1:I = 0x7f0a01da

.field public static final lifemode_kor_tip_pollen_1_10:I = 0x7f0a01db

.field public static final lifemode_kor_tip_pollen_1_2:I = 0x7f0a01dc

.field public static final lifemode_kor_tip_pollen_1_3:I = 0x7f0a01dd

.field public static final lifemode_kor_tip_pollen_1_4:I = 0x7f0a01de

.field public static final lifemode_kor_tip_pollen_1_5:I = 0x7f0a01df

.field public static final lifemode_kor_tip_pollen_1_6:I = 0x7f0a01e0

.field public static final lifemode_kor_tip_pollen_1_7:I = 0x7f0a01e1

.field public static final lifemode_kor_tip_pollen_1_8:I = 0x7f0a01e2

.field public static final lifemode_kor_tip_pollen_1_9:I = 0x7f0a01e3

.field public static final lifemode_kor_tip_pollen_2_1:I = 0x7f0a01e4

.field public static final lifemode_kor_tip_pollen_2_10:I = 0x7f0a01e5

.field public static final lifemode_kor_tip_pollen_2_2:I = 0x7f0a01e6

.field public static final lifemode_kor_tip_pollen_2_3:I = 0x7f0a01e7

.field public static final lifemode_kor_tip_pollen_2_4:I = 0x7f0a01e8

.field public static final lifemode_kor_tip_pollen_2_5:I = 0x7f0a01e9

.field public static final lifemode_kor_tip_pollen_2_6:I = 0x7f0a01ea

.field public static final lifemode_kor_tip_pollen_2_7:I = 0x7f0a01eb

.field public static final lifemode_kor_tip_pollen_2_8:I = 0x7f0a01ec

.field public static final lifemode_kor_tip_pollen_2_9:I = 0x7f0a01ed

.field public static final lifemode_kor_tip_pollen_3_1:I = 0x7f0a01ee

.field public static final lifemode_kor_tip_pollen_3_10:I = 0x7f0a01ef

.field public static final lifemode_kor_tip_pollen_3_2:I = 0x7f0a01f0

.field public static final lifemode_kor_tip_pollen_3_3:I = 0x7f0a01f1

.field public static final lifemode_kor_tip_pollen_3_4:I = 0x7f0a01f2

.field public static final lifemode_kor_tip_pollen_3_5:I = 0x7f0a01f3

.field public static final lifemode_kor_tip_pollen_3_6:I = 0x7f0a01f4

.field public static final lifemode_kor_tip_pollen_3_7:I = 0x7f0a01f5

.field public static final lifemode_kor_tip_pollen_3_8:I = 0x7f0a01f6

.field public static final lifemode_kor_tip_pollen_3_9:I = 0x7f0a01f7

.field public static final lifemode_kor_tip_pollen_4_1:I = 0x7f0a01f8

.field public static final lifemode_kor_tip_pollen_4_10:I = 0x7f0a01f9

.field public static final lifemode_kor_tip_pollen_4_2:I = 0x7f0a01fa

.field public static final lifemode_kor_tip_pollen_4_3:I = 0x7f0a01fb

.field public static final lifemode_kor_tip_pollen_4_4:I = 0x7f0a01fc

.field public static final lifemode_kor_tip_pollen_4_5:I = 0x7f0a01fd

.field public static final lifemode_kor_tip_pollen_4_6:I = 0x7f0a01fe

.field public static final lifemode_kor_tip_pollen_4_7:I = 0x7f0a01ff

.field public static final lifemode_kor_tip_pollen_4_8:I = 0x7f0a0200

.field public static final lifemode_kor_tip_pollen_4_9:I = 0x7f0a0201

.field public static final lifemode_kor_tip_putre_1_1:I = 0x7f0a0202

.field public static final lifemode_kor_tip_putre_1_2:I = 0x7f0a0203

.field public static final lifemode_kor_tip_putre_1_3:I = 0x7f0a0204

.field public static final lifemode_kor_tip_putre_1_4:I = 0x7f0a0205

.field public static final lifemode_kor_tip_putre_2_1:I = 0x7f0a0206

.field public static final lifemode_kor_tip_putre_2_2:I = 0x7f0a0207

.field public static final lifemode_kor_tip_putre_2_3:I = 0x7f0a0208

.field public static final lifemode_kor_tip_putre_2_4:I = 0x7f0a0209

.field public static final lifemode_kor_tip_putre_3_1:I = 0x7f0a020a

.field public static final lifemode_kor_tip_putre_3_2:I = 0x7f0a020b

.field public static final lifemode_kor_tip_putre_3_3:I = 0x7f0a020c

.field public static final lifemode_kor_tip_putre_3_4:I = 0x7f0a020d

.field public static final lifemode_kor_tip_putre_4_1:I = 0x7f0a020e

.field public static final lifemode_kor_tip_putre_4_2:I = 0x7f0a020f

.field public static final lifemode_kor_tip_putre_4_3:I = 0x7f0a0210

.field public static final lifemode_kor_tip_putre_4_4:I = 0x7f0a0211

.field public static final lifemode_kor_tip_s_temp_1_1:I = 0x7f0a0212

.field public static final lifemode_kor_tip_s_temp_1_2:I = 0x7f0a0213

.field public static final lifemode_kor_tip_s_temp_1_3:I = 0x7f0a0214

.field public static final lifemode_kor_tip_s_temp_1_4:I = 0x7f0a0215

.field public static final lifemode_kor_tip_s_temp_1_5:I = 0x7f0a0216

.field public static final lifemode_kor_tip_s_temp_1_6:I = 0x7f0a0217

.field public static final lifemode_kor_tip_s_temp_1_7:I = 0x7f0a0218

.field public static final lifemode_kor_tip_s_temp_1_8:I = 0x7f0a0219

.field public static final lifemode_kor_tip_s_temp_1_9:I = 0x7f0a021a

.field public static final lifemode_kor_tip_s_temp_2_1:I = 0x7f0a021b

.field public static final lifemode_kor_tip_s_temp_2_10:I = 0x7f0a021c

.field public static final lifemode_kor_tip_s_temp_2_2:I = 0x7f0a021d

.field public static final lifemode_kor_tip_s_temp_2_3:I = 0x7f0a021e

.field public static final lifemode_kor_tip_s_temp_2_4:I = 0x7f0a021f

.field public static final lifemode_kor_tip_s_temp_2_5:I = 0x7f0a0220

.field public static final lifemode_kor_tip_s_temp_2_6:I = 0x7f0a0221

.field public static final lifemode_kor_tip_s_temp_2_7:I = 0x7f0a0222

.field public static final lifemode_kor_tip_s_temp_2_8:I = 0x7f0a0223

.field public static final lifemode_kor_tip_s_temp_2_9:I = 0x7f0a0224

.field public static final lifemode_kor_tip_s_temp_3_1:I = 0x7f0a0225

.field public static final lifemode_kor_tip_s_temp_3_10:I = 0x7f0a0226

.field public static final lifemode_kor_tip_s_temp_3_2:I = 0x7f0a0227

.field public static final lifemode_kor_tip_s_temp_3_3:I = 0x7f0a0228

.field public static final lifemode_kor_tip_s_temp_3_4:I = 0x7f0a0229

.field public static final lifemode_kor_tip_s_temp_3_5:I = 0x7f0a022a

.field public static final lifemode_kor_tip_s_temp_3_6:I = 0x7f0a022b

.field public static final lifemode_kor_tip_s_temp_3_7:I = 0x7f0a022c

.field public static final lifemode_kor_tip_s_temp_3_8:I = 0x7f0a022d

.field public static final lifemode_kor_tip_s_temp_3_9:I = 0x7f0a022e

.field public static final lifemode_kor_tip_s_temp_4_1:I = 0x7f0a022f

.field public static final lifemode_kor_tip_s_temp_4_10:I = 0x7f0a0230

.field public static final lifemode_kor_tip_s_temp_4_2:I = 0x7f0a0231

.field public static final lifemode_kor_tip_s_temp_4_3:I = 0x7f0a0232

.field public static final lifemode_kor_tip_s_temp_4_4:I = 0x7f0a0233

.field public static final lifemode_kor_tip_s_temp_4_5:I = 0x7f0a0234

.field public static final lifemode_kor_tip_s_temp_4_6:I = 0x7f0a0235

.field public static final lifemode_kor_tip_s_temp_4_7:I = 0x7f0a0236

.field public static final lifemode_kor_tip_s_temp_4_8:I = 0x7f0a0237

.field public static final lifemode_kor_tip_s_temp_4_9:I = 0x7f0a0238

.field public static final lifemode_kor_tip_temp_1_1:I = 0x7f0a0239

.field public static final lifemode_kor_tip_temp_1_10:I = 0x7f0a023a

.field public static final lifemode_kor_tip_temp_1_2:I = 0x7f0a023b

.field public static final lifemode_kor_tip_temp_1_3:I = 0x7f0a023c

.field public static final lifemode_kor_tip_temp_1_4:I = 0x7f0a023d

.field public static final lifemode_kor_tip_temp_1_5:I = 0x7f0a023e

.field public static final lifemode_kor_tip_temp_1_6:I = 0x7f0a023f

.field public static final lifemode_kor_tip_temp_1_7:I = 0x7f0a0240

.field public static final lifemode_kor_tip_temp_1_8:I = 0x7f0a0241

.field public static final lifemode_kor_tip_temp_1_9:I = 0x7f0a0242

.field public static final lifemode_kor_tip_temp_2_1:I = 0x7f0a0243

.field public static final lifemode_kor_tip_temp_2_10:I = 0x7f0a0244

.field public static final lifemode_kor_tip_temp_2_2:I = 0x7f0a0245

.field public static final lifemode_kor_tip_temp_2_3:I = 0x7f0a0246

.field public static final lifemode_kor_tip_temp_2_4:I = 0x7f0a0247

.field public static final lifemode_kor_tip_temp_2_5:I = 0x7f0a0248

.field public static final lifemode_kor_tip_temp_2_6:I = 0x7f0a0249

.field public static final lifemode_kor_tip_temp_2_7:I = 0x7f0a024a

.field public static final lifemode_kor_tip_temp_2_8:I = 0x7f0a024b

.field public static final lifemode_kor_tip_temp_2_9:I = 0x7f0a024c

.field public static final lifemode_kor_tip_temp_3_1:I = 0x7f0a024d

.field public static final lifemode_kor_tip_temp_3_10:I = 0x7f0a024e

.field public static final lifemode_kor_tip_temp_3_2:I = 0x7f0a024f

.field public static final lifemode_kor_tip_temp_3_3:I = 0x7f0a0250

.field public static final lifemode_kor_tip_temp_3_4:I = 0x7f0a0251

.field public static final lifemode_kor_tip_temp_3_5:I = 0x7f0a0252

.field public static final lifemode_kor_tip_temp_3_6:I = 0x7f0a0253

.field public static final lifemode_kor_tip_temp_3_7:I = 0x7f0a0254

.field public static final lifemode_kor_tip_temp_3_8:I = 0x7f0a0255

.field public static final lifemode_kor_tip_temp_3_9:I = 0x7f0a0256

.field public static final lifemode_kor_tip_temp_4_1:I = 0x7f0a0257

.field public static final lifemode_kor_tip_temp_4_10:I = 0x7f0a0258

.field public static final lifemode_kor_tip_temp_4_2:I = 0x7f0a0259

.field public static final lifemode_kor_tip_temp_4_3:I = 0x7f0a025a

.field public static final lifemode_kor_tip_temp_4_4:I = 0x7f0a025b

.field public static final lifemode_kor_tip_temp_4_5:I = 0x7f0a025c

.field public static final lifemode_kor_tip_temp_4_6:I = 0x7f0a025d

.field public static final lifemode_kor_tip_temp_4_7:I = 0x7f0a025e

.field public static final lifemode_kor_tip_temp_4_8:I = 0x7f0a025f

.field public static final lifemode_kor_tip_temp_4_9:I = 0x7f0a0260

.field public static final lifemode_kor_tip_uv_1_1:I = 0x7f0a0261

.field public static final lifemode_kor_tip_uv_1_10:I = 0x7f0a0262

.field public static final lifemode_kor_tip_uv_1_2:I = 0x7f0a0263

.field public static final lifemode_kor_tip_uv_1_3:I = 0x7f0a0264

.field public static final lifemode_kor_tip_uv_1_4:I = 0x7f0a0265

.field public static final lifemode_kor_tip_uv_1_5:I = 0x7f0a0266

.field public static final lifemode_kor_tip_uv_1_6:I = 0x7f0a0267

.field public static final lifemode_kor_tip_uv_1_7:I = 0x7f0a0268

.field public static final lifemode_kor_tip_uv_1_8:I = 0x7f0a0269

.field public static final lifemode_kor_tip_uv_1_9:I = 0x7f0a026a

.field public static final lifemode_kor_tip_uv_2_1:I = 0x7f0a026b

.field public static final lifemode_kor_tip_uv_2_10:I = 0x7f0a026c

.field public static final lifemode_kor_tip_uv_2_2:I = 0x7f0a026d

.field public static final lifemode_kor_tip_uv_2_3:I = 0x7f0a026e

.field public static final lifemode_kor_tip_uv_2_4:I = 0x7f0a026f

.field public static final lifemode_kor_tip_uv_2_5:I = 0x7f0a0270

.field public static final lifemode_kor_tip_uv_2_6:I = 0x7f0a0271

.field public static final lifemode_kor_tip_uv_2_7:I = 0x7f0a0272

.field public static final lifemode_kor_tip_uv_2_8:I = 0x7f0a0273

.field public static final lifemode_kor_tip_uv_2_9:I = 0x7f0a0274

.field public static final lifemode_kor_tip_uv_3_1:I = 0x7f0a0275

.field public static final lifemode_kor_tip_uv_3_10:I = 0x7f0a0276

.field public static final lifemode_kor_tip_uv_3_2:I = 0x7f0a0277

.field public static final lifemode_kor_tip_uv_3_3:I = 0x7f0a0278

.field public static final lifemode_kor_tip_uv_3_4:I = 0x7f0a0279

.field public static final lifemode_kor_tip_uv_3_5:I = 0x7f0a027a

.field public static final lifemode_kor_tip_uv_3_6:I = 0x7f0a027b

.field public static final lifemode_kor_tip_uv_3_7:I = 0x7f0a027c

.field public static final lifemode_kor_tip_uv_3_8:I = 0x7f0a027d

.field public static final lifemode_kor_tip_uv_3_9:I = 0x7f0a027e

.field public static final lifemode_kor_tip_uv_4_1:I = 0x7f0a027f

.field public static final lifemode_kor_tip_uv_4_10:I = 0x7f0a0280

.field public static final lifemode_kor_tip_uv_4_2:I = 0x7f0a0281

.field public static final lifemode_kor_tip_uv_4_3:I = 0x7f0a0282

.field public static final lifemode_kor_tip_uv_4_4:I = 0x7f0a0283

.field public static final lifemode_kor_tip_uv_4_5:I = 0x7f0a0284

.field public static final lifemode_kor_tip_uv_4_6:I = 0x7f0a0285

.field public static final lifemode_kor_tip_uv_4_7:I = 0x7f0a0286

.field public static final lifemode_kor_tip_uv_4_8:I = 0x7f0a0287

.field public static final lifemode_kor_tip_uv_4_9:I = 0x7f0a0288

.field public static final lifemode_kor_tip_uv_5_1:I = 0x7f0a0289

.field public static final lifemode_kor_tip_uv_5_10:I = 0x7f0a028a

.field public static final lifemode_kor_tip_uv_5_2:I = 0x7f0a028b

.field public static final lifemode_kor_tip_uv_5_3:I = 0x7f0a028c

.field public static final lifemode_kor_tip_uv_5_4:I = 0x7f0a028d

.field public static final lifemode_kor_tip_uv_5_5:I = 0x7f0a028e

.field public static final lifemode_kor_tip_uv_5_6:I = 0x7f0a028f

.field public static final lifemode_kor_tip_uv_5_7:I = 0x7f0a0290

.field public static final lifemode_kor_tip_uv_5_8:I = 0x7f0a0291

.field public static final lifemode_kor_tip_uv_5_9:I = 0x7f0a0292

.field public static final lifemode_list_detail_time_text:I = 0x7f0a0293

.field public static final lifemode_list_item_cold:I = 0x7f0a0294

.field public static final lifemode_list_item_drying_of_the_skin:I = 0x7f0a0295

.field public static final lifemode_list_item_extream_cold:I = 0x7f0a0296

.field public static final lifemode_list_item_food_poisoning:I = 0x7f0a0297

.field public static final lifemode_list_item_freez:I = 0x7f0a0298

.field public static final lifemode_list_item_fros:I = 0x7f0a0299

.field public static final lifemode_list_item_heat_strength:I = 0x7f0a029a

.field public static final lifemode_list_item_minute_dust:I = 0x7f0a029b

.field public static final lifemode_list_item_outdoor_index:I = 0x7f0a029c

.field public static final lifemode_list_item_pm10:I = 0x7f0a029d

.field public static final lifemode_list_item_pollen:I = 0x7f0a029e

.field public static final lifemode_list_item_putre:I = 0x7f0a029f

.field public static final lifemode_list_item_s_temp:I = 0x7f0a02a0

.field public static final lifemode_list_item_sweat_index:I = 0x7f0a02a1

.field public static final lifemode_list_item_temp:I = 0x7f0a02a2

.field public static final lifemode_list_item_ultraviolet_rays:I = 0x7f0a02a3

.field public static final lifemode_list_item_umbrella_index:I = 0x7f0a02a4

.field public static final lifemode_list_item_yellow_sand:I = 0x7f0a02a5

.field public static final lifemode_list_no_info:I = 0x7f0a02a6

.field public static final lifemode_list_noinfo:I = 0x7f0a02a7

.field public static final lifemode_list_normal:I = 0x7f0a02a8

.field public static final lifemode_list_type1_level1:I = 0x7f0a02a9

.field public static final lifemode_list_type1_level2:I = 0x7f0a02aa

.field public static final lifemode_list_type1_level3:I = 0x7f0a02ab

.field public static final lifemode_list_type2_level1:I = 0x7f0a02ac

.field public static final lifemode_list_type2_level2:I = 0x7f0a02ad

.field public static final lifemode_list_type2_level3:I = 0x7f0a02ae

.field public static final lifemode_list_type2_level4:I = 0x7f0a02af

.field public static final lifemode_list_type3_level1:I = 0x7f0a02b0

.field public static final lifemode_list_type3_level2:I = 0x7f0a02b1

.field public static final lifemode_list_type3_level3:I = 0x7f0a02b2

.field public static final lifemode_list_type4_level1:I = 0x7f0a02b3

.field public static final lifemode_list_type4_level2:I = 0x7f0a02b4

.field public static final lifemode_list_type4_level3:I = 0x7f0a02b5

.field public static final lifemode_list_type5_level1:I = 0x7f0a02b6

.field public static final lifemode_list_type5_level2:I = 0x7f0a02b7

.field public static final lifemode_list_type5_level3:I = 0x7f0a02b8

.field public static final lifemode_list_warning:I = 0x7f0a02b9

.field public static final lifemode_pm10_text:I = 0x7f0a02ba

.field public static final lifemode_pollen_text:I = 0x7f0a02bb

.field public static final lifemode_roaming:I = 0x7f0a02bc

.field public static final lifemode_roaming_korea:I = 0x7f0a02bd

.field public static final lifemode_safety_manual:I = 0x7f0a02be

.field public static final lifemode_spoil_text:I = 0x7f0a02bf

.field public static final lifemode_status_detail_uint_pm:I = 0x7f0a02c0

.field public static final lifemode_status_detail_unit_celsius:I = 0x7f0a02c1

.field public static final lifemode_status_detail_unit_level:I = 0x7f0a02c2

.field public static final lifemode_status_detail_unit_percent:I = 0x7f0a02c3

.field public static final lifemode_status_detail_unit_rank:I = 0x7f0a02c4

.field public static final lifemode_sweat_text:I = 0x7f0a02c5

.field public static final lifemode_tip_accident_1_1:I = 0x7f0a02c6

.field public static final lifemode_tip_accident_1_10:I = 0x7f0a02c7

.field public static final lifemode_tip_accident_1_11:I = 0x7f0a02c8

.field public static final lifemode_tip_accident_1_12:I = 0x7f0a02c9

.field public static final lifemode_tip_accident_1_13:I = 0x7f0a02ca

.field public static final lifemode_tip_accident_1_14:I = 0x7f0a02cb

.field public static final lifemode_tip_accident_1_15:I = 0x7f0a02cc

.field public static final lifemode_tip_accident_1_2:I = 0x7f0a02cd

.field public static final lifemode_tip_accident_1_3:I = 0x7f0a02ce

.field public static final lifemode_tip_accident_1_4:I = 0x7f0a02cf

.field public static final lifemode_tip_accident_1_5:I = 0x7f0a02d0

.field public static final lifemode_tip_accident_1_6:I = 0x7f0a02d1

.field public static final lifemode_tip_accident_1_7:I = 0x7f0a02d2

.field public static final lifemode_tip_accident_1_8:I = 0x7f0a02d3

.field public static final lifemode_tip_accident_1_9:I = 0x7f0a02d4

.field public static final lifemode_tip_accident_2_1:I = 0x7f0a02d5

.field public static final lifemode_tip_accident_2_10:I = 0x7f0a02d6

.field public static final lifemode_tip_accident_2_11:I = 0x7f0a02d7

.field public static final lifemode_tip_accident_2_12:I = 0x7f0a02d8

.field public static final lifemode_tip_accident_2_13:I = 0x7f0a02d9

.field public static final lifemode_tip_accident_2_14:I = 0x7f0a02da

.field public static final lifemode_tip_accident_2_15:I = 0x7f0a02db

.field public static final lifemode_tip_accident_2_2:I = 0x7f0a02dc

.field public static final lifemode_tip_accident_2_3:I = 0x7f0a02dd

.field public static final lifemode_tip_accident_2_4:I = 0x7f0a02de

.field public static final lifemode_tip_accident_2_5:I = 0x7f0a02df

.field public static final lifemode_tip_accident_2_6:I = 0x7f0a02e0

.field public static final lifemode_tip_accident_2_7:I = 0x7f0a02e1

.field public static final lifemode_tip_accident_2_8:I = 0x7f0a02e2

.field public static final lifemode_tip_accident_2_9:I = 0x7f0a02e3

.field public static final lifemode_tip_accident_3_1:I = 0x7f0a02e4

.field public static final lifemode_tip_accident_3_10:I = 0x7f0a02e5

.field public static final lifemode_tip_accident_3_11:I = 0x7f0a02e6

.field public static final lifemode_tip_accident_3_12:I = 0x7f0a02e7

.field public static final lifemode_tip_accident_3_13:I = 0x7f0a02e8

.field public static final lifemode_tip_accident_3_14:I = 0x7f0a02e9

.field public static final lifemode_tip_accident_3_15:I = 0x7f0a02ea

.field public static final lifemode_tip_accident_3_2:I = 0x7f0a02eb

.field public static final lifemode_tip_accident_3_3:I = 0x7f0a02ec

.field public static final lifemode_tip_accident_3_4:I = 0x7f0a02ed

.field public static final lifemode_tip_accident_3_5:I = 0x7f0a02ee

.field public static final lifemode_tip_accident_3_6:I = 0x7f0a02ef

.field public static final lifemode_tip_accident_3_7:I = 0x7f0a02f0

.field public static final lifemode_tip_accident_3_8:I = 0x7f0a02f1

.field public static final lifemode_tip_accident_3_9:I = 0x7f0a02f2

.field public static final lifemode_tip_extremecold_1_1:I = 0x7f0a02f3

.field public static final lifemode_tip_extremecold_1_10:I = 0x7f0a02f4

.field public static final lifemode_tip_extremecold_1_11:I = 0x7f0a02f5

.field public static final lifemode_tip_extremecold_1_12:I = 0x7f0a02f6

.field public static final lifemode_tip_extremecold_1_13:I = 0x7f0a02f7

.field public static final lifemode_tip_extremecold_1_14:I = 0x7f0a02f8

.field public static final lifemode_tip_extremecold_1_2:I = 0x7f0a02f9

.field public static final lifemode_tip_extremecold_1_3:I = 0x7f0a02fa

.field public static final lifemode_tip_extremecold_1_4:I = 0x7f0a02fb

.field public static final lifemode_tip_extremecold_1_5:I = 0x7f0a02fc

.field public static final lifemode_tip_extremecold_1_6:I = 0x7f0a02fd

.field public static final lifemode_tip_extremecold_1_7:I = 0x7f0a02fe

.field public static final lifemode_tip_extremecold_1_8:I = 0x7f0a02ff

.field public static final lifemode_tip_extremecold_1_9:I = 0x7f0a0300

.field public static final lifemode_tip_extremecold_2_1:I = 0x7f0a0301

.field public static final lifemode_tip_extremecold_2_10:I = 0x7f0a0302

.field public static final lifemode_tip_extremecold_2_11:I = 0x7f0a0303

.field public static final lifemode_tip_extremecold_2_12:I = 0x7f0a0304

.field public static final lifemode_tip_extremecold_2_13:I = 0x7f0a0305

.field public static final lifemode_tip_extremecold_2_14:I = 0x7f0a0306

.field public static final lifemode_tip_extremecold_2_15:I = 0x7f0a0307

.field public static final lifemode_tip_extremecold_2_2:I = 0x7f0a0308

.field public static final lifemode_tip_extremecold_2_3:I = 0x7f0a0309

.field public static final lifemode_tip_extremecold_2_4:I = 0x7f0a030a

.field public static final lifemode_tip_extremecold_2_5:I = 0x7f0a030b

.field public static final lifemode_tip_extremecold_2_6:I = 0x7f0a030c

.field public static final lifemode_tip_extremecold_2_7:I = 0x7f0a030d

.field public static final lifemode_tip_extremecold_2_8:I = 0x7f0a030e

.field public static final lifemode_tip_extremecold_2_9:I = 0x7f0a030f

.field public static final lifemode_tip_extremecold_3_1:I = 0x7f0a0310

.field public static final lifemode_tip_extremecold_3_10:I = 0x7f0a0311

.field public static final lifemode_tip_extremecold_3_11:I = 0x7f0a0312

.field public static final lifemode_tip_extremecold_3_12:I = 0x7f0a0313

.field public static final lifemode_tip_extremecold_3_2:I = 0x7f0a0314

.field public static final lifemode_tip_extremecold_3_3:I = 0x7f0a0315

.field public static final lifemode_tip_extremecold_3_4:I = 0x7f0a0316

.field public static final lifemode_tip_extremecold_3_5:I = 0x7f0a0317

.field public static final lifemode_tip_extremecold_3_6:I = 0x7f0a0318

.field public static final lifemode_tip_extremecold_3_7:I = 0x7f0a0319

.field public static final lifemode_tip_extremecold_3_8:I = 0x7f0a031a

.field public static final lifemode_tip_extremecold_3_9:I = 0x7f0a031b

.field public static final lifemode_tip_finedust_1_1:I = 0x7f0a031c

.field public static final lifemode_tip_finedust_1_10:I = 0x7f0a031d

.field public static final lifemode_tip_finedust_1_11:I = 0x7f0a031e

.field public static final lifemode_tip_finedust_1_12:I = 0x7f0a031f

.field public static final lifemode_tip_finedust_1_2:I = 0x7f0a0320

.field public static final lifemode_tip_finedust_1_3:I = 0x7f0a0321

.field public static final lifemode_tip_finedust_1_4:I = 0x7f0a0322

.field public static final lifemode_tip_finedust_1_5:I = 0x7f0a0323

.field public static final lifemode_tip_finedust_1_6:I = 0x7f0a0324

.field public static final lifemode_tip_finedust_1_7:I = 0x7f0a0325

.field public static final lifemode_tip_finedust_1_8:I = 0x7f0a0326

.field public static final lifemode_tip_finedust_1_9:I = 0x7f0a0327

.field public static final lifemode_tip_finedust_2_1:I = 0x7f0a0328

.field public static final lifemode_tip_finedust_2_10:I = 0x7f0a0329

.field public static final lifemode_tip_finedust_2_11:I = 0x7f0a032a

.field public static final lifemode_tip_finedust_2_12:I = 0x7f0a032b

.field public static final lifemode_tip_finedust_2_13:I = 0x7f0a032c

.field public static final lifemode_tip_finedust_2_14:I = 0x7f0a032d

.field public static final lifemode_tip_finedust_2_2:I = 0x7f0a032e

.field public static final lifemode_tip_finedust_2_3:I = 0x7f0a032f

.field public static final lifemode_tip_finedust_2_4:I = 0x7f0a0330

.field public static final lifemode_tip_finedust_2_5:I = 0x7f0a0331

.field public static final lifemode_tip_finedust_2_6:I = 0x7f0a0332

.field public static final lifemode_tip_finedust_2_7:I = 0x7f0a0333

.field public static final lifemode_tip_finedust_2_8:I = 0x7f0a0334

.field public static final lifemode_tip_finedust_2_9:I = 0x7f0a0335

.field public static final lifemode_tip_finedust_3_1:I = 0x7f0a0336

.field public static final lifemode_tip_finedust_3_10:I = 0x7f0a0337

.field public static final lifemode_tip_finedust_3_11:I = 0x7f0a0338

.field public static final lifemode_tip_finedust_3_12:I = 0x7f0a0339

.field public static final lifemode_tip_finedust_3_2:I = 0x7f0a033a

.field public static final lifemode_tip_finedust_3_3:I = 0x7f0a033b

.field public static final lifemode_tip_finedust_3_4:I = 0x7f0a033c

.field public static final lifemode_tip_finedust_3_5:I = 0x7f0a033d

.field public static final lifemode_tip_finedust_3_6:I = 0x7f0a033e

.field public static final lifemode_tip_finedust_3_7:I = 0x7f0a033f

.field public static final lifemode_tip_finedust_3_8:I = 0x7f0a0340

.field public static final lifemode_tip_finedust_3_9:I = 0x7f0a0341

.field public static final lifemode_tip_finedust_4_1:I = 0x7f0a0342

.field public static final lifemode_tip_finedust_4_10:I = 0x7f0a0343

.field public static final lifemode_tip_finedust_4_11:I = 0x7f0a0344

.field public static final lifemode_tip_finedust_4_12:I = 0x7f0a0345

.field public static final lifemode_tip_finedust_4_2:I = 0x7f0a0346

.field public static final lifemode_tip_finedust_4_3:I = 0x7f0a0347

.field public static final lifemode_tip_finedust_4_4:I = 0x7f0a0348

.field public static final lifemode_tip_finedust_4_5:I = 0x7f0a0349

.field public static final lifemode_tip_finedust_4_6:I = 0x7f0a034a

.field public static final lifemode_tip_finedust_4_7:I = 0x7f0a034b

.field public static final lifemode_tip_finedust_4_8:I = 0x7f0a034c

.field public static final lifemode_tip_finedust_4_9:I = 0x7f0a034d

.field public static final lifemode_tip_humidity_1_1:I = 0x7f0a034e

.field public static final lifemode_tip_humidity_1_10:I = 0x7f0a034f

.field public static final lifemode_tip_humidity_1_11:I = 0x7f0a0350

.field public static final lifemode_tip_humidity_1_12:I = 0x7f0a0351

.field public static final lifemode_tip_humidity_1_2:I = 0x7f0a0352

.field public static final lifemode_tip_humidity_1_3:I = 0x7f0a0353

.field public static final lifemode_tip_humidity_1_4:I = 0x7f0a0354

.field public static final lifemode_tip_humidity_1_5:I = 0x7f0a0355

.field public static final lifemode_tip_humidity_1_6:I = 0x7f0a0356

.field public static final lifemode_tip_humidity_1_7:I = 0x7f0a0357

.field public static final lifemode_tip_humidity_1_8:I = 0x7f0a0358

.field public static final lifemode_tip_humidity_1_9:I = 0x7f0a0359

.field public static final lifemode_tip_humidity_2_1:I = 0x7f0a035a

.field public static final lifemode_tip_humidity_2_10:I = 0x7f0a035b

.field public static final lifemode_tip_humidity_2_11:I = 0x7f0a035c

.field public static final lifemode_tip_humidity_2_12:I = 0x7f0a035d

.field public static final lifemode_tip_humidity_2_13:I = 0x7f0a035e

.field public static final lifemode_tip_humidity_2_14:I = 0x7f0a035f

.field public static final lifemode_tip_humidity_2_2:I = 0x7f0a0360

.field public static final lifemode_tip_humidity_2_3:I = 0x7f0a0361

.field public static final lifemode_tip_humidity_2_4:I = 0x7f0a0362

.field public static final lifemode_tip_humidity_2_5:I = 0x7f0a0363

.field public static final lifemode_tip_humidity_2_6:I = 0x7f0a0364

.field public static final lifemode_tip_humidity_2_7:I = 0x7f0a0365

.field public static final lifemode_tip_humidity_2_8:I = 0x7f0a0366

.field public static final lifemode_tip_humidity_2_9:I = 0x7f0a0367

.field public static final lifemode_tip_humidity_3_1:I = 0x7f0a0368

.field public static final lifemode_tip_humidity_3_10:I = 0x7f0a0369

.field public static final lifemode_tip_humidity_3_11:I = 0x7f0a036a

.field public static final lifemode_tip_humidity_3_12:I = 0x7f0a036b

.field public static final lifemode_tip_humidity_3_13:I = 0x7f0a036c

.field public static final lifemode_tip_humidity_3_14:I = 0x7f0a036d

.field public static final lifemode_tip_humidity_3_15:I = 0x7f0a036e

.field public static final lifemode_tip_humidity_3_2:I = 0x7f0a036f

.field public static final lifemode_tip_humidity_3_3:I = 0x7f0a0370

.field public static final lifemode_tip_humidity_3_4:I = 0x7f0a0371

.field public static final lifemode_tip_humidity_3_5:I = 0x7f0a0372

.field public static final lifemode_tip_humidity_3_6:I = 0x7f0a0373

.field public static final lifemode_tip_humidity_3_7:I = 0x7f0a0374

.field public static final lifemode_tip_humidity_3_8:I = 0x7f0a0375

.field public static final lifemode_tip_humidity_3_9:I = 0x7f0a0376

.field public static final lifemode_tip_leisure_1_1:I = 0x7f0a0377

.field public static final lifemode_tip_leisure_1_10:I = 0x7f0a0378

.field public static final lifemode_tip_leisure_1_11:I = 0x7f0a0379

.field public static final lifemode_tip_leisure_1_12:I = 0x7f0a037a

.field public static final lifemode_tip_leisure_1_13:I = 0x7f0a037b

.field public static final lifemode_tip_leisure_1_2:I = 0x7f0a037c

.field public static final lifemode_tip_leisure_1_3:I = 0x7f0a037d

.field public static final lifemode_tip_leisure_1_4:I = 0x7f0a037e

.field public static final lifemode_tip_leisure_1_5:I = 0x7f0a037f

.field public static final lifemode_tip_leisure_1_6:I = 0x7f0a0380

.field public static final lifemode_tip_leisure_1_7:I = 0x7f0a0381

.field public static final lifemode_tip_leisure_1_8:I = 0x7f0a0382

.field public static final lifemode_tip_leisure_1_9:I = 0x7f0a0383

.field public static final lifemode_tip_leisure_2_1:I = 0x7f0a0384

.field public static final lifemode_tip_leisure_2_10:I = 0x7f0a0385

.field public static final lifemode_tip_leisure_2_11:I = 0x7f0a0386

.field public static final lifemode_tip_leisure_2_12:I = 0x7f0a0387

.field public static final lifemode_tip_leisure_2_13:I = 0x7f0a0388

.field public static final lifemode_tip_leisure_2_14:I = 0x7f0a0389

.field public static final lifemode_tip_leisure_2_15:I = 0x7f0a038a

.field public static final lifemode_tip_leisure_2_2:I = 0x7f0a038b

.field public static final lifemode_tip_leisure_2_3:I = 0x7f0a038c

.field public static final lifemode_tip_leisure_2_4:I = 0x7f0a038d

.field public static final lifemode_tip_leisure_2_5:I = 0x7f0a038e

.field public static final lifemode_tip_leisure_2_6:I = 0x7f0a038f

.field public static final lifemode_tip_leisure_2_7:I = 0x7f0a0390

.field public static final lifemode_tip_leisure_2_8:I = 0x7f0a0391

.field public static final lifemode_tip_leisure_2_9:I = 0x7f0a0392

.field public static final lifemode_tip_leisure_3_1:I = 0x7f0a0393

.field public static final lifemode_tip_leisure_3_10:I = 0x7f0a0394

.field public static final lifemode_tip_leisure_3_11:I = 0x7f0a0395

.field public static final lifemode_tip_leisure_3_12:I = 0x7f0a0396

.field public static final lifemode_tip_leisure_3_13:I = 0x7f0a0397

.field public static final lifemode_tip_leisure_3_14:I = 0x7f0a0398

.field public static final lifemode_tip_leisure_3_2:I = 0x7f0a0399

.field public static final lifemode_tip_leisure_3_3:I = 0x7f0a039a

.field public static final lifemode_tip_leisure_3_4:I = 0x7f0a039b

.field public static final lifemode_tip_leisure_3_5:I = 0x7f0a039c

.field public static final lifemode_tip_leisure_3_6:I = 0x7f0a039d

.field public static final lifemode_tip_leisure_3_7:I = 0x7f0a039e

.field public static final lifemode_tip_leisure_3_8:I = 0x7f0a039f

.field public static final lifemode_tip_leisure_3_9:I = 0x7f0a03a0

.field public static final lifemode_tip_pollen_1_1:I = 0x7f0a03a1

.field public static final lifemode_tip_pollen_1_10:I = 0x7f0a03a2

.field public static final lifemode_tip_pollen_1_11:I = 0x7f0a03a3

.field public static final lifemode_tip_pollen_1_12:I = 0x7f0a03a4

.field public static final lifemode_tip_pollen_1_2:I = 0x7f0a03a5

.field public static final lifemode_tip_pollen_1_3:I = 0x7f0a03a6

.field public static final lifemode_tip_pollen_1_4:I = 0x7f0a03a7

.field public static final lifemode_tip_pollen_1_5:I = 0x7f0a03a8

.field public static final lifemode_tip_pollen_1_6:I = 0x7f0a03a9

.field public static final lifemode_tip_pollen_1_7:I = 0x7f0a03aa

.field public static final lifemode_tip_pollen_1_8:I = 0x7f0a03ab

.field public static final lifemode_tip_pollen_1_9:I = 0x7f0a03ac

.field public static final lifemode_tip_pollen_2_1:I = 0x7f0a03ad

.field public static final lifemode_tip_pollen_2_10:I = 0x7f0a03ae

.field public static final lifemode_tip_pollen_2_11:I = 0x7f0a03af

.field public static final lifemode_tip_pollen_2_12:I = 0x7f0a03b0

.field public static final lifemode_tip_pollen_2_13:I = 0x7f0a03b1

.field public static final lifemode_tip_pollen_2_14:I = 0x7f0a03b2

.field public static final lifemode_tip_pollen_2_15:I = 0x7f0a03b3

.field public static final lifemode_tip_pollen_2_2:I = 0x7f0a03b4

.field public static final lifemode_tip_pollen_2_3:I = 0x7f0a03b5

.field public static final lifemode_tip_pollen_2_4:I = 0x7f0a03b6

.field public static final lifemode_tip_pollen_2_5:I = 0x7f0a03b7

.field public static final lifemode_tip_pollen_2_6:I = 0x7f0a03b8

.field public static final lifemode_tip_pollen_2_7:I = 0x7f0a03b9

.field public static final lifemode_tip_pollen_2_8:I = 0x7f0a03ba

.field public static final lifemode_tip_pollen_2_9:I = 0x7f0a03bb

.field public static final lifemode_tip_pollen_3_1:I = 0x7f0a03bc

.field public static final lifemode_tip_pollen_3_10:I = 0x7f0a03bd

.field public static final lifemode_tip_pollen_3_11:I = 0x7f0a03be

.field public static final lifemode_tip_pollen_3_12:I = 0x7f0a03bf

.field public static final lifemode_tip_pollen_3_13:I = 0x7f0a03c0

.field public static final lifemode_tip_pollen_3_14:I = 0x7f0a03c1

.field public static final lifemode_tip_pollen_3_15:I = 0x7f0a03c2

.field public static final lifemode_tip_pollen_3_16:I = 0x7f0a03c3

.field public static final lifemode_tip_pollen_3_2:I = 0x7f0a03c4

.field public static final lifemode_tip_pollen_3_3:I = 0x7f0a03c5

.field public static final lifemode_tip_pollen_3_4:I = 0x7f0a03c6

.field public static final lifemode_tip_pollen_3_5:I = 0x7f0a03c7

.field public static final lifemode_tip_pollen_3_6:I = 0x7f0a03c8

.field public static final lifemode_tip_pollen_3_7:I = 0x7f0a03c9

.field public static final lifemode_tip_pollen_3_8:I = 0x7f0a03ca

.field public static final lifemode_tip_pollen_3_9:I = 0x7f0a03cb

.field public static final lifemode_tip_pollen_4_1:I = 0x7f0a03cc

.field public static final lifemode_tip_pollen_4_10:I = 0x7f0a03cd

.field public static final lifemode_tip_pollen_4_11:I = 0x7f0a03ce

.field public static final lifemode_tip_pollen_4_12:I = 0x7f0a03cf

.field public static final lifemode_tip_pollen_4_13:I = 0x7f0a03d0

.field public static final lifemode_tip_pollen_4_14:I = 0x7f0a03d1

.field public static final lifemode_tip_pollen_4_15:I = 0x7f0a03d2

.field public static final lifemode_tip_pollen_4_16:I = 0x7f0a03d3

.field public static final lifemode_tip_pollen_4_2:I = 0x7f0a03d4

.field public static final lifemode_tip_pollen_4_3:I = 0x7f0a03d5

.field public static final lifemode_tip_pollen_4_4:I = 0x7f0a03d6

.field public static final lifemode_tip_pollen_4_5:I = 0x7f0a03d7

.field public static final lifemode_tip_pollen_4_6:I = 0x7f0a03d8

.field public static final lifemode_tip_pollen_4_7:I = 0x7f0a03d9

.field public static final lifemode_tip_pollen_4_8:I = 0x7f0a03da

.field public static final lifemode_tip_pollen_4_9:I = 0x7f0a03db

.field public static final lifemode_tip_skinhumid_1_1:I = 0x7f0a03dc

.field public static final lifemode_tip_skinhumid_1_10:I = 0x7f0a03dd

.field public static final lifemode_tip_skinhumid_1_11:I = 0x7f0a03de

.field public static final lifemode_tip_skinhumid_1_12:I = 0x7f0a03df

.field public static final lifemode_tip_skinhumid_1_13:I = 0x7f0a03e0

.field public static final lifemode_tip_skinhumid_1_14:I = 0x7f0a03e1

.field public static final lifemode_tip_skinhumid_1_2:I = 0x7f0a03e2

.field public static final lifemode_tip_skinhumid_1_3:I = 0x7f0a03e3

.field public static final lifemode_tip_skinhumid_1_4:I = 0x7f0a03e4

.field public static final lifemode_tip_skinhumid_1_5:I = 0x7f0a03e5

.field public static final lifemode_tip_skinhumid_1_6:I = 0x7f0a03e6

.field public static final lifemode_tip_skinhumid_1_7:I = 0x7f0a03e7

.field public static final lifemode_tip_skinhumid_1_8:I = 0x7f0a03e8

.field public static final lifemode_tip_skinhumid_1_9:I = 0x7f0a03e9

.field public static final lifemode_tip_skinhumid_2_1:I = 0x7f0a03ea

.field public static final lifemode_tip_skinhumid_2_10:I = 0x7f0a03eb

.field public static final lifemode_tip_skinhumid_2_11:I = 0x7f0a03ec

.field public static final lifemode_tip_skinhumid_2_12:I = 0x7f0a03ed

.field public static final lifemode_tip_skinhumid_2_13:I = 0x7f0a03ee

.field public static final lifemode_tip_skinhumid_2_14:I = 0x7f0a03ef

.field public static final lifemode_tip_skinhumid_2_15:I = 0x7f0a03f0

.field public static final lifemode_tip_skinhumid_2_2:I = 0x7f0a03f1

.field public static final lifemode_tip_skinhumid_2_3:I = 0x7f0a03f2

.field public static final lifemode_tip_skinhumid_2_4:I = 0x7f0a03f3

.field public static final lifemode_tip_skinhumid_2_5:I = 0x7f0a03f4

.field public static final lifemode_tip_skinhumid_2_6:I = 0x7f0a03f5

.field public static final lifemode_tip_skinhumid_2_7:I = 0x7f0a03f6

.field public static final lifemode_tip_skinhumid_2_8:I = 0x7f0a03f7

.field public static final lifemode_tip_skinhumid_2_9:I = 0x7f0a03f8

.field public static final lifemode_tip_skinhumid_3_1:I = 0x7f0a03f9

.field public static final lifemode_tip_skinhumid_3_10:I = 0x7f0a03fa

.field public static final lifemode_tip_skinhumid_3_11:I = 0x7f0a03fb

.field public static final lifemode_tip_skinhumid_3_12:I = 0x7f0a03fc

.field public static final lifemode_tip_skinhumid_3_13:I = 0x7f0a03fd

.field public static final lifemode_tip_skinhumid_3_14:I = 0x7f0a03fe

.field public static final lifemode_tip_skinhumid_3_15:I = 0x7f0a03ff

.field public static final lifemode_tip_skinhumid_3_2:I = 0x7f0a0400

.field public static final lifemode_tip_skinhumid_3_3:I = 0x7f0a0401

.field public static final lifemode_tip_skinhumid_3_4:I = 0x7f0a0402

.field public static final lifemode_tip_skinhumid_3_5:I = 0x7f0a0403

.field public static final lifemode_tip_skinhumid_3_6:I = 0x7f0a0404

.field public static final lifemode_tip_skinhumid_3_7:I = 0x7f0a0405

.field public static final lifemode_tip_skinhumid_3_8:I = 0x7f0a0406

.field public static final lifemode_tip_skinhumid_3_9:I = 0x7f0a0407

.field public static final lifemode_tip_umbrella_1_1:I = 0x7f0a0408

.field public static final lifemode_tip_umbrella_1_10:I = 0x7f0a0409

.field public static final lifemode_tip_umbrella_1_11:I = 0x7f0a040a

.field public static final lifemode_tip_umbrella_1_2:I = 0x7f0a040b

.field public static final lifemode_tip_umbrella_1_3:I = 0x7f0a040c

.field public static final lifemode_tip_umbrella_1_4:I = 0x7f0a040d

.field public static final lifemode_tip_umbrella_1_5:I = 0x7f0a040e

.field public static final lifemode_tip_umbrella_1_6:I = 0x7f0a040f

.field public static final lifemode_tip_umbrella_1_7:I = 0x7f0a0410

.field public static final lifemode_tip_umbrella_1_8:I = 0x7f0a0411

.field public static final lifemode_tip_umbrella_1_9:I = 0x7f0a0412

.field public static final lifemode_tip_umbrella_2_1:I = 0x7f0a0413

.field public static final lifemode_tip_umbrella_2_10:I = 0x7f0a0414

.field public static final lifemode_tip_umbrella_2_11:I = 0x7f0a0415

.field public static final lifemode_tip_umbrella_2_12:I = 0x7f0a0416

.field public static final lifemode_tip_umbrella_2_13:I = 0x7f0a0417

.field public static final lifemode_tip_umbrella_2_14:I = 0x7f0a0418

.field public static final lifemode_tip_umbrella_2_15:I = 0x7f0a0419

.field public static final lifemode_tip_umbrella_2_2:I = 0x7f0a041a

.field public static final lifemode_tip_umbrella_2_3:I = 0x7f0a041b

.field public static final lifemode_tip_umbrella_2_4:I = 0x7f0a041c

.field public static final lifemode_tip_umbrella_2_5:I = 0x7f0a041d

.field public static final lifemode_tip_umbrella_2_6:I = 0x7f0a041e

.field public static final lifemode_tip_umbrella_2_7:I = 0x7f0a041f

.field public static final lifemode_tip_umbrella_2_8:I = 0x7f0a0420

.field public static final lifemode_tip_umbrella_2_9:I = 0x7f0a0421

.field public static final lifemode_tip_umbrella_3_1:I = 0x7f0a0422

.field public static final lifemode_tip_umbrella_3_10:I = 0x7f0a0423

.field public static final lifemode_tip_umbrella_3_11:I = 0x7f0a0424

.field public static final lifemode_tip_umbrella_3_12:I = 0x7f0a0425

.field public static final lifemode_tip_umbrella_3_13:I = 0x7f0a0426

.field public static final lifemode_tip_umbrella_3_14:I = 0x7f0a0427

.field public static final lifemode_tip_umbrella_3_15:I = 0x7f0a0428

.field public static final lifemode_tip_umbrella_3_2:I = 0x7f0a0429

.field public static final lifemode_tip_umbrella_3_3:I = 0x7f0a042a

.field public static final lifemode_tip_umbrella_3_4:I = 0x7f0a042b

.field public static final lifemode_tip_umbrella_3_5:I = 0x7f0a042c

.field public static final lifemode_tip_umbrella_3_6:I = 0x7f0a042d

.field public static final lifemode_tip_umbrella_3_7:I = 0x7f0a042e

.field public static final lifemode_tip_umbrella_3_8:I = 0x7f0a042f

.field public static final lifemode_tip_umbrella_3_9:I = 0x7f0a0430

.field public static final lifemode_tip_uv_1_1:I = 0x7f0a0431

.field public static final lifemode_tip_uv_1_10:I = 0x7f0a0432

.field public static final lifemode_tip_uv_1_11:I = 0x7f0a0433

.field public static final lifemode_tip_uv_1_12:I = 0x7f0a0434

.field public static final lifemode_tip_uv_1_2:I = 0x7f0a0435

.field public static final lifemode_tip_uv_1_3:I = 0x7f0a0436

.field public static final lifemode_tip_uv_1_4:I = 0x7f0a0437

.field public static final lifemode_tip_uv_1_5:I = 0x7f0a0438

.field public static final lifemode_tip_uv_1_6:I = 0x7f0a0439

.field public static final lifemode_tip_uv_1_7:I = 0x7f0a043a

.field public static final lifemode_tip_uv_1_8:I = 0x7f0a043b

.field public static final lifemode_tip_uv_1_9:I = 0x7f0a043c

.field public static final lifemode_tip_uv_2_1:I = 0x7f0a043d

.field public static final lifemode_tip_uv_2_10:I = 0x7f0a043e

.field public static final lifemode_tip_uv_2_11:I = 0x7f0a043f

.field public static final lifemode_tip_uv_2_12:I = 0x7f0a0440

.field public static final lifemode_tip_uv_2_13:I = 0x7f0a0441

.field public static final lifemode_tip_uv_2_14:I = 0x7f0a0442

.field public static final lifemode_tip_uv_2_15:I = 0x7f0a0443

.field public static final lifemode_tip_uv_2_2:I = 0x7f0a0444

.field public static final lifemode_tip_uv_2_3:I = 0x7f0a0445

.field public static final lifemode_tip_uv_2_4:I = 0x7f0a0446

.field public static final lifemode_tip_uv_2_5:I = 0x7f0a0447

.field public static final lifemode_tip_uv_2_6:I = 0x7f0a0448

.field public static final lifemode_tip_uv_2_7:I = 0x7f0a0449

.field public static final lifemode_tip_uv_2_8:I = 0x7f0a044a

.field public static final lifemode_tip_uv_2_9:I = 0x7f0a044b

.field public static final lifemode_tip_uv_3_1:I = 0x7f0a044c

.field public static final lifemode_tip_uv_3_10:I = 0x7f0a044d

.field public static final lifemode_tip_uv_3_11:I = 0x7f0a044e

.field public static final lifemode_tip_uv_3_12:I = 0x7f0a044f

.field public static final lifemode_tip_uv_3_13:I = 0x7f0a0450

.field public static final lifemode_tip_uv_3_14:I = 0x7f0a0451

.field public static final lifemode_tip_uv_3_15:I = 0x7f0a0452

.field public static final lifemode_tip_uv_3_2:I = 0x7f0a0453

.field public static final lifemode_tip_uv_3_3:I = 0x7f0a0454

.field public static final lifemode_tip_uv_3_4:I = 0x7f0a0455

.field public static final lifemode_tip_uv_3_5:I = 0x7f0a0456

.field public static final lifemode_tip_uv_3_6:I = 0x7f0a0457

.field public static final lifemode_tip_uv_3_7:I = 0x7f0a0458

.field public static final lifemode_tip_uv_3_8:I = 0x7f0a0459

.field public static final lifemode_tip_uv_3_9:I = 0x7f0a045a

.field public static final lifemode_tip_yellowdust_1_1:I = 0x7f0a045b

.field public static final lifemode_tip_yellowdust_1_10:I = 0x7f0a045c

.field public static final lifemode_tip_yellowdust_1_11:I = 0x7f0a045d

.field public static final lifemode_tip_yellowdust_1_12:I = 0x7f0a045e

.field public static final lifemode_tip_yellowdust_1_13:I = 0x7f0a045f

.field public static final lifemode_tip_yellowdust_1_2:I = 0x7f0a0460

.field public static final lifemode_tip_yellowdust_1_3:I = 0x7f0a0461

.field public static final lifemode_tip_yellowdust_1_4:I = 0x7f0a0462

.field public static final lifemode_tip_yellowdust_1_5:I = 0x7f0a0463

.field public static final lifemode_tip_yellowdust_1_6:I = 0x7f0a0464

.field public static final lifemode_tip_yellowdust_1_7:I = 0x7f0a0465

.field public static final lifemode_tip_yellowdust_1_8:I = 0x7f0a0466

.field public static final lifemode_tip_yellowdust_1_9:I = 0x7f0a0467

.field public static final lifemode_tip_yellowdust_2_1:I = 0x7f0a0468

.field public static final lifemode_tip_yellowdust_2_10:I = 0x7f0a0469

.field public static final lifemode_tip_yellowdust_2_2:I = 0x7f0a046a

.field public static final lifemode_tip_yellowdust_2_3:I = 0x7f0a046b

.field public static final lifemode_tip_yellowdust_2_4:I = 0x7f0a046c

.field public static final lifemode_tip_yellowdust_2_5:I = 0x7f0a046d

.field public static final lifemode_tip_yellowdust_2_6:I = 0x7f0a046e

.field public static final lifemode_tip_yellowdust_2_7:I = 0x7f0a046f

.field public static final lifemode_tip_yellowdust_2_8:I = 0x7f0a0470

.field public static final lifemode_tip_yellowdust_2_9:I = 0x7f0a0471

.field public static final lifemode_tip_yellowdust_3_1:I = 0x7f0a0472

.field public static final lifemode_tip_yellowdust_3_10:I = 0x7f0a0473

.field public static final lifemode_tip_yellowdust_3_11:I = 0x7f0a0474

.field public static final lifemode_tip_yellowdust_3_12:I = 0x7f0a0475

.field public static final lifemode_tip_yellowdust_3_13:I = 0x7f0a0476

.field public static final lifemode_tip_yellowdust_3_2:I = 0x7f0a0477

.field public static final lifemode_tip_yellowdust_3_3:I = 0x7f0a0478

.field public static final lifemode_tip_yellowdust_3_4:I = 0x7f0a0479

.field public static final lifemode_tip_yellowdust_3_5:I = 0x7f0a047a

.field public static final lifemode_tip_yellowdust_3_6:I = 0x7f0a047b

.field public static final lifemode_tip_yellowdust_3_7:I = 0x7f0a047c

.field public static final lifemode_tip_yellowdust_3_8:I = 0x7f0a047d

.field public static final lifemode_tip_yellowdust_3_9:I = 0x7f0a047e

.field public static final lifemode_tip_yellowdust_4_1:I = 0x7f0a047f

.field public static final lifemode_tip_yellowdust_4_10:I = 0x7f0a0480

.field public static final lifemode_tip_yellowdust_4_11:I = 0x7f0a0481

.field public static final lifemode_tip_yellowdust_4_2:I = 0x7f0a0482

.field public static final lifemode_tip_yellowdust_4_3:I = 0x7f0a0483

.field public static final lifemode_tip_yellowdust_4_4:I = 0x7f0a0484

.field public static final lifemode_tip_yellowdust_4_5:I = 0x7f0a0485

.field public static final lifemode_tip_yellowdust_4_6:I = 0x7f0a0486

.field public static final lifemode_tip_yellowdust_4_7:I = 0x7f0a0487

.field public static final lifemode_tip_yellowdust_4_8:I = 0x7f0a0488

.field public static final lifemode_tip_yellowdust_4_9:I = 0x7f0a0489

.field public static final lifemode_uv_info_text:I = 0x7f0a048a

.field public static final lifemode_wind_chill_text:I = 0x7f0a048b

.field public static final location_gps_not_found:I = 0x7f0a048c

.field public static final location_masterswitch_dialogtext:I = 0x7f0a048d

.field public static final location_masterswitch_dialogtext_tablet:I = 0x7f0a048e

.field public static final location_masterswitch_dialogtitle:I = 0x7f0a048f

.field public static final location_searching:I = 0x7f0a0490

.field public static final location_will_be_delete:I = 0x7f0a0491

.field public static final me:I = 0x7f0a0492

.field public static final menu_about_index:I = 0x7f0a0493

.field public static final menu_add_location:I = 0x7f0a0494

.field public static final menu_disable_geo_lookout:I = 0x7f0a0495

.field public static final menu_disable_popup_window_text:I = 0x7f0a0496

.field public static final menu_emergency_mode:I = 0x7f0a0497

.field public static final menu_enable_geo_lookout:I = 0x7f0a0498

.field public static final menu_help:I = 0x7f0a0499

.field public static final menu_manage_emergency_contacts:I = 0x7f0a049a

.field public static final menu_manage_primary_contacts:I = 0x7f0a049b

.field public static final menu_prod_server_mode:I = 0x7f0a049c

.field public static final menu_refresh:I = 0x7f0a049d

.field public static final menu_refresh_time:I = 0x7f0a049e

.field public static final menu_refresh_time_none:I = 0x7f0a049f

.field public static final menu_service_coverage:I = 0x7f0a04a0

.field public static final menu_share:I = 0x7f0a04a1

.field public static final menu_stg_server_mode:I = 0x7f0a04a2

.field public static final menu_test_mode:I = 0x7f0a04a3

.field public static final message_is_not_available:I = 0x7f0a04a4

.field public static final message_is_updated:I = 0x7f0a04a5

.field public static final mobile_link_TWC:I = 0x7f0a04a6

.field public static final mobile_link_WEATHERNEWS:I = 0x7f0a04a7

.field public static final mobile_link_WEATHERNEWS_JP:I = 0x7f0a04a8

.field public static final more_info:I = 0x7f0a04a9

.field public static final name:I = 0x7f0a04aa

.field public static final network_error_occurred:I = 0x7f0a04ab

.field public static final network_or_server_error_occurred:I = 0x7f0a04ac

.field public static final next:I = 0x7f0a04ad

.field public static final no_disaster_description:I = 0x7f0a04ae

.field public static final no_disaster_title:I = 0x7f0a04af

.field public static final no_tips_for_this_alert:I = 0x7f0a04b0

.field public static final not_supported_Languages_title:I = 0x7f0a04b1

.field public static final not_supported_languages_dialog:I = 0x7f0a04b2

.field public static final notification_canceled:I = 0x7f0a04b3

.field public static final open_source_licenses:I = 0x7f0a04b4

.field public static final safetycare_disaster_alert_noti_desc:I = 0x7f0a04b5

.field public static final safetycare_disaster_alert_noti_title:I = 0x7f0a04b6

.field public static final safetycare_disaster_alert_summary:I = 0x7f0a04b7

.field public static final safetycare_disaster_alert_title:I = 0x7f0a04b8

.field public static final safetycare_geoinfo_summary:I = 0x7f0a04b9

.field public static final safetycare_geolookout_title:I = 0x7f0a04ba

.field public static final safetycare_geonews_agree_check1:I = 0x7f0a04bb

.field public static final safetycare_geonews_agree_check2:I = 0x7f0a04bc

.field public static final safetycare_geonews_agree_check3:I = 0x7f0a04bd

.field public static final safetycare_geonews_agree_check_CHN_1_2:I = 0x7f0a04be

.field public static final safetycare_geonews_agree_check_CHN_1_3:I = 0x7f0a04bf

.field public static final safetycare_geonews_agree_check_CHN_1_4:I = 0x7f0a04c0

.field public static final safetycare_geonews_disclaimer:I = 0x7f0a04c1

.field public static final safetycare_geonews_disclaimer1_CHN:I = 0x7f0a04c2

.field public static final safetycare_geonews_disclaimer1_JPN:I = 0x7f0a04c3

.field public static final safetycare_geonews_disclaimer1_jpn_desc:I = 0x7f0a04c4

.field public static final safetycare_geonews_disclaimer1_jpn_title:I = 0x7f0a04c5

.field public static final safetycare_geonews_disclaimer2_CHN:I = 0x7f0a04c6

.field public static final safetycare_geonews_disclaimer2_JPN:I = 0x7f0a04c7

.field public static final safetycare_geonews_disclaimer2_jpn_desc:I = 0x7f0a04c8

.field public static final safetycare_geonews_disclaimer2_jpn_title:I = 0x7f0a04c9

.field public static final safetycare_geonews_disclaimer3_JPN:I = 0x7f0a04ca

.field public static final safetycare_geonews_disclaimer3_jpn_desc:I = 0x7f0a04cb

.field public static final safetycare_geonews_disclaimer3_jpn_title:I = 0x7f0a04cc

.field public static final safetycare_geonews_disclaimer_usa:I = 0x7f0a04cd

.field public static final safetycare_geonews_geolife_kor_desc1:I = 0x7f0a04ce

.field public static final safetycare_geonews_geolife_kor_desc2:I = 0x7f0a04cf

.field public static final safetycare_geonews_geolife_kor_desc3:I = 0x7f0a04d0

.field public static final safetycare_geonews_geolife_kor_desc4:I = 0x7f0a04d1

.field public static final safetycare_geonews_geolife_kor_desc5:I = 0x7f0a04d2

.field public static final safetycare_geonews_geolife_kor_desc6:I = 0x7f0a04d3

.field public static final safetycare_geonews_geolife_kor_desc7:I = 0x7f0a04d4

.field public static final safetycare_geonews_geolife_kor_desc8:I = 0x7f0a04d5

.field public static final safetycare_geonews_geolife_kor_desc9:I = 0x7f0a04d6

.field public static final safetycare_geonews_kor_desc1:I = 0x7f0a04d7

.field public static final safetycare_geonews_kor_desc2:I = 0x7f0a04d8

.field public static final safetycare_geonews_kor_desc3:I = 0x7f0a04d9

.field public static final safetycare_geonews_kor_desc4:I = 0x7f0a04da

.field public static final safetycare_geonews_kor_desc5:I = 0x7f0a04db

.field public static final safetycare_geonews_kor_desc6:I = 0x7f0a04dc

.field public static final safetycare_geonews_kor_desc7:I = 0x7f0a04dd

.field public static final safetycare_geonews_kor_desc8:I = 0x7f0a04de

.field public static final safetycare_geonews_kor_desc9:I = 0x7f0a04df

.field public static final safetycare_geonews_popup_disclaimer_text:I = 0x7f0a04e0

.field public static final safetycare_geonews_popup_disclaimer_title:I = 0x7f0a04e1

.field public static final safetycare_geonews_popup_summary:I = 0x7f0a04e2

.field public static final safetycare_no_sim:I = 0x7f0a04e3

.field public static final safetycare_terms_and_conditions_checkbox_text:I = 0x7f0a04e4

.field public static final safetycare_terms_and_conditions_checkbox_text_usa:I = 0x7f0a04e5

.field public static final save:I = 0x7f0a04e6

.field public static final save_location:I = 0x7f0a04e7

.field public static final search:I = 0x7f0a04e8

.field public static final seoul:I = 0x7f0a04e9

.field public static final shortcut_name_jp:I = 0x7f0a04ea

.field public static final status_warning:I = 0x7f0a04eb

.field public static final status_watch:I = 0x7f0a04ec

.field public static final stms_version:I = 0x7f0a04ed

.field public static final string_lifemode_db_refreshing:I = 0x7f0a04ee

.field public static final string_software_update_available:I = 0x7f0a04ef

.field public static final support_language_only_english:I = 0x7f0a04f0

.field public static final support_language_only_korean:I = 0x7f0a04f1

.field public static final talkback_TWC:I = 0x7f0a04f2

.field public static final talkback_WN:I = 0x7f0a04f3

.field public static final talkback_dashboard:I = 0x7f0a04f4

.field public static final talkback_default_location:I = 0x7f0a04f5

.field public static final talkback_emergency_mode:I = 0x7f0a04f6

.field public static final tap_to_assign_contact:I = 0x7f0a04f7

.field public static final this_region_is_okay:I = 0x7f0a04f8

.field public static final tips:I = 0x7f0a04f9

.field public static final tips_air_pollution:I = 0x7f0a04fa

.field public static final tips_ash_fall:I = 0x7f0a04fb

.field public static final tips_avalanche:I = 0x7f0a04fc

.field public static final tips_dense_fog:I = 0x7f0a04fd

.field public static final tips_drought:I = 0x7f0a04fe

.field public static final tips_dry:I = 0x7f0a04ff

.field public static final tips_dust_storm:I = 0x7f0a0500

.field public static final tips_earthquake:I = 0x7f0a0501

.field public static final tips_excessive_heat:I = 0x7f0a0502

.field public static final tips_extreme_cold:I = 0x7f0a0503

.field public static final tips_extreme_wind:I = 0x7f0a0504

.field public static final tips_flood:I = 0x7f0a0505

.field public static final tips_freezing:I = 0x7f0a0506

.field public static final tips_frost:I = 0x7f0a0507

.field public static final tips_gale:I = 0x7f0a0508

.field public static final tips_hail:I = 0x7f0a0509

.field public static final tips_haze:I = 0x7f0a050a

.field public static final tips_heavy_rain:I = 0x7f0a050b

.field public static final tips_heavy_snow:I = 0x7f0a050c

.field public static final tips_high_seas:I = 0x7f0a050d

.field public static final tips_high_wind:I = 0x7f0a050e

.field public static final tips_hurricane:I = 0x7f0a050f

.field public static final tips_multi:I = 0x7f0a0510

.field public static final tips_ozone:I = 0x7f0a0511

.field public static final tips_rainstorm:I = 0x7f0a0512

.field public static final tips_road_icing:I = 0x7f0a0513

.field public static final tips_sand_storm:I = 0x7f0a0514

.field public static final tips_snow_accretion:I = 0x7f0a0515

.field public static final tips_snow_melting:I = 0x7f0a0516

.field public static final tips_snowstorm:I = 0x7f0a0517

.field public static final tips_storm:I = 0x7f0a0518

.field public static final tips_storm_surge:I = 0x7f0a0519

.field public static final tips_thick_fog:I = 0x7f0a051a

.field public static final tips_thunderstorm:I = 0x7f0a051b

.field public static final tips_tornado:I = 0x7f0a051c

.field public static final tips_tropical_storm:I = 0x7f0a051d

.field public static final tips_tsunami:I = 0x7f0a051e

.field public static final tips_typhoon:I = 0x7f0a051f

.field public static final tips_url_air_pollution:I = 0x7f0a0520

.field public static final tips_url_ashfall:I = 0x7f0a0521

.field public static final tips_url_avalanche:I = 0x7f0a0522

.field public static final tips_url_dust_storm:I = 0x7f0a0523

.field public static final tips_url_earthquake:I = 0x7f0a0524

.field public static final tips_url_excessive_heat:I = 0x7f0a0525

.field public static final tips_url_extream_cold:I = 0x7f0a0526

.field public static final tips_url_extreme_wind:I = 0x7f0a0527

.field public static final tips_url_flood:I = 0x7f0a0528

.field public static final tips_url_for_japan_avalanche:I = 0x7f0a0529

.field public static final tips_url_for_japan_dry:I = 0x7f0a052a

.field public static final tips_url_for_japan_earthquake:I = 0x7f0a052b

.field public static final tips_url_for_japan_extreme_cold:I = 0x7f0a052c

.field public static final tips_url_for_japan_extreme_wind:I = 0x7f0a052d

.field public static final tips_url_for_japan_flood:I = 0x7f0a052e

.field public static final tips_url_for_japan_freezing:I = 0x7f0a052f

.field public static final tips_url_for_japan_frost:I = 0x7f0a0530

.field public static final tips_url_for_japan_heavy_rain:I = 0x7f0a0531

.field public static final tips_url_for_japan_heavy_snow:I = 0x7f0a0532

.field public static final tips_url_for_japan_high_seas:I = 0x7f0a0533

.field public static final tips_url_for_japan_snow_accretion:I = 0x7f0a0534

.field public static final tips_url_for_japan_snow_ice:I = 0x7f0a0535

.field public static final tips_url_for_japan_snow_melting:I = 0x7f0a0536

.field public static final tips_url_for_japan_storm_surge:I = 0x7f0a0537

.field public static final tips_url_for_japan_thick_fog:I = 0x7f0a0538

.field public static final tips_url_for_japan_thunderstorm:I = 0x7f0a0539

.field public static final tips_url_for_japan_tsunami:I = 0x7f0a053a

.field public static final tips_url_for_japan_typhoon:I = 0x7f0a053b

.field public static final tips_url_for_japan_wind_storm:I = 0x7f0a053c

.field public static final tips_url_for_japan_winter_storm:I = 0x7f0a053d

.field public static final tips_url_for_korea_dry:I = 0x7f0a053e

.field public static final tips_url_for_korea_dust_storm:I = 0x7f0a053f

.field public static final tips_url_for_korea_earthquake:I = 0x7f0a0540

.field public static final tips_url_for_korea_extreme_cold:I = 0x7f0a0541

.field public static final tips_url_for_korea_extreme_heat:I = 0x7f0a0542

.field public static final tips_url_for_korea_extreme_wind:I = 0x7f0a0543

.field public static final tips_url_for_korea_heavy_rain:I = 0x7f0a0544

.field public static final tips_url_for_korea_heavy_snow:I = 0x7f0a0545

.field public static final tips_url_for_korea_high_seas:I = 0x7f0a0546

.field public static final tips_url_for_korea_thunderstorm:I = 0x7f0a0547

.field public static final tips_url_for_korea_tsunami:I = 0x7f0a0548

.field public static final tips_url_for_korea_typhoon:I = 0x7f0a0549

.field public static final tips_url_gale:I = 0x7f0a054a

.field public static final tips_url_high_wind:I = 0x7f0a054b

.field public static final tips_url_hurricane:I = 0x7f0a054c

.field public static final tips_url_ozone:I = 0x7f0a054d

.field public static final tips_url_snow_ice:I = 0x7f0a054e

.field public static final tips_url_strom:I = 0x7f0a054f

.field public static final tips_url_thunderstorm:I = 0x7f0a0550

.field public static final tips_url_tornado:I = 0x7f0a0551

.field public static final tips_url_tropical_storm:I = 0x7f0a0552

.field public static final tips_url_tsunami:I = 0x7f0a0553

.field public static final tips_url_typhoon:I = 0x7f0a0554

.field public static final tips_url_volcano:I = 0x7f0a0555

.field public static final tips_url_wildfire:I = 0x7f0a0556

.field public static final tips_url_winter_storm:I = 0x7f0a0557

.field public static final tips_volcano:I = 0x7f0a0558

.field public static final tips_wildfire:I = 0x7f0a0559

.field public static final tips_wind_storm:I = 0x7f0a055a

.field public static final ulleungdo:I = 0x7f0a055b

.field public static final unable_to_find_your_location_toast:I = 0x7f0a055c

.field public static final up_to_characters_available:I = 0x7f0a055d

.field public static final update_time_items:I = 0x7f0a055e

.field public static final waiting_for_server:I = 0x7f0a055f

.field public static final widget_enabling:I = 0x7f0a0560

.field public static final widget_geo_lookout:I = 0x7f0a0561

.field public static final widget_magnitude:I = 0x7f0a0562

.field public static final widget_reported_on:I = 0x7f0a0563

.field public static final widget_tap_to_get_started:I = 0x7f0a0564

.field public static final widget_you_are_safe:I = 0x7f0a0565


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
