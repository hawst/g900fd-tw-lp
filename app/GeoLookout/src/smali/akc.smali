.class public Lakc;
.super Landroid/widget/BaseAdapter;
.source "DisasterReportActivity.java"


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lake;",
            ">;"
        }
    .end annotation
.end field

.field b:Lakb;

.field final synthetic c:Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;

.field private d:I


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lake;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 605
    iput-object p1, p0, Lakc;->c:Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 606
    iput-object p2, p0, Lakc;->a:Ljava/util/ArrayList;

    .line 607
    const/4 v0, 0x0

    iput v0, p0, Lakc;->d:I

    .line 608
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 597
    iget v0, p0, Lakc;->d:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 601
    iput p1, p0, Lakc;->d:I

    .line 602
    invoke-virtual {p0}, Lakc;->notifyDataSetChanged()V

    .line 603
    return-void
.end method

.method public b(I)Lake;
    .locals 1

    .prologue
    .line 617
    iget-object v0, p0, Lakc;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lake;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 612
    iget-object v0, p0, Lakc;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 589
    invoke-virtual {p0, p1}, Lakc;->b(I)Lake;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 622
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 627
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getView() called : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 628
    iget-object v0, p0, Lakc;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lake;

    .line 629
    invoke-static {v0}, Lake;->a(Lake;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v2

    .line 631
    if-nez p2, :cond_1

    .line 632
    iget-object v1, p0, Lakc;->c:Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->d(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f030019

    invoke-virtual {v1, v3, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 635
    new-instance v1, Lakb;

    invoke-direct {v1}, Lakb;-><init>()V

    iput-object v1, p0, Lakc;->b:Lakb;

    .line 636
    iget-object v3, p0, Lakc;->b:Lakb;

    const v1, 0x7f0e0013

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lakb;->a:Landroid/widget/TextView;

    .line 637
    iget-object v3, p0, Lakc;->b:Lakb;

    const v1, 0x7f0e007e

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lakb;->b:Landroid/widget/TextView;

    .line 638
    iget-object v3, p0, Lakc;->b:Lakb;

    const v1, 0x7f0e007f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lakb;->c:Landroid/widget/TextView;

    .line 639
    iget-object v3, p0, Lakc;->b:Lakb;

    const v1, 0x7f0e0080

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lakb;->e:Landroid/widget/TextView;

    .line 640
    iget-object v3, p0, Lakc;->b:Lakb;

    const v1, 0x7f0e0081

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Lakb;->d:Landroid/widget/TextView;

    .line 642
    iget-object v1, p0, Lakc;->b:Lakb;

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 647
    :goto_0
    const v1, 0x7f0e001f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 648
    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLink()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLink()Ljava/lang/String;

    move-result-object v3

    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 649
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 651
    new-instance v3, Lakd;

    invoke-direct {v3, p0, v2}, Lakd;-><init>(Lakc;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 661
    :goto_1
    const-string v1, "03"

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 662
    iget-object v1, p0, Lakc;->b:Lakb;

    iget-object v1, v1, Lakb;->a:Landroid/widget/TextView;

    iget-object v3, p0, Lakc;->c:Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;

    invoke-static {v3}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->d(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v4

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v5

    invoke-static {v3, v4, v5}, Lall;->b(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 666
    :goto_2
    iget v1, p0, Lakc;->d:I

    if-ne p1, v1, :cond_4

    .line 667
    iget-object v1, p0, Lakc;->b:Lakb;

    iget-object v1, v1, Lakb;->a:Landroid/widget/TextView;

    iget-object v3, p0, Lakc;->c:Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;

    invoke-static {v3}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->d(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0800b7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 673
    :goto_3
    iget-object v1, p0, Lakc;->b:Lakb;

    iget-object v3, v1, Lakb;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lakc;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lake;

    invoke-static {v1}, Lake;->b(Lake;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 675
    iget-object v1, p0, Lakc;->b:Lakb;

    iget-object v1, v1, Lakb;->d:Landroid/widget/TextView;

    invoke-static {v0}, Lake;->c(Lake;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 677
    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v1

    .line 678
    sparse-switch v1, :sswitch_data_0

    .line 731
    iget-object v1, p0, Lakc;->b:Lakb;

    iget-object v1, v1, Lakb;->e:Landroid/widget/TextView;

    invoke-static {v0}, Lake;->d(Lake;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 732
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 733
    const-string v0, "03"

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 734
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 737
    :cond_0
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 741
    :goto_4
    return-object p2

    .line 644
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lakb;

    iput-object v1, p0, Lakc;->b:Lakb;

    goto/16 :goto_0

    .line 658
    :cond_2
    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 664
    :cond_3
    iget-object v1, p0, Lakc;->b:Lakb;

    iget-object v1, v1, Lakb;->a:Landroid/widget/TextView;

    iget-object v3, p0, Lakc;->c:Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;

    invoke-static {v3}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->d(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v4

    invoke-static {v3, v4}, Lall;->h(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 670
    :cond_4
    iget-object v1, p0, Lakc;->b:Lakb;

    iget-object v1, v1, Lakb;->a:Landroid/widget/TextView;

    iget-object v3, p0, Lakc;->c:Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;

    invoke-static {v3}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->d(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f080000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3

    .line 681
    :sswitch_0
    const-string v0, "03"

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 682
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 684
    iget-object v1, p0, Lakc;->c:Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a00b3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 685
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 686
    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsZoneCity()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 687
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 688
    iget-object v1, p0, Lakc;->c:Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a00bb

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsLevel()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 692
    iget-object v1, p0, Lakc;->b:Lakb;

    iget-object v1, v1, Lakb;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 694
    :cond_5
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 695
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lakc;->c:Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;

    invoke-static {v1, v2}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 696
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 699
    :sswitch_1
    iget-object v1, p0, Lakc;->b:Lakb;

    iget-object v1, v1, Lakb;->e:Landroid/widget/TextView;

    invoke-static {v0}, Lake;->d(Lake;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 700
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 701
    const-string v0, "03"

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 702
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 705
    :cond_6
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lakc;->c:Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;

    invoke-static {v1, v2}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 706
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 710
    :sswitch_2
    iget-object v1, p0, Lakc;->b:Lakb;

    iget-object v1, v1, Lakb;->e:Landroid/widget/TextView;

    invoke-static {v0}, Lake;->d(Lake;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 711
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 712
    const-string v0, "03"

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 713
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 716
    :cond_7
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 720
    :sswitch_3
    iget-object v1, p0, Lakc;->b:Lakb;

    iget-object v1, v1, Lakb;->e:Landroid/widget/TextView;

    invoke-static {v0}, Lake;->d(Lake;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 721
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 722
    const-string v0, "03"

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 723
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 724
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 726
    :cond_8
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lakc;->c:Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;

    invoke-static {v1, v2}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 727
    iget-object v0, p0, Lakc;->b:Lakb;

    iget-object v0, v0, Lakb;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 678
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_1
        0x69 -> :sswitch_0
        0x6e -> :sswitch_3
        0x70 -> :sswitch_3
        0x71 -> :sswitch_2
        0xce -> :sswitch_2
    .end sparse-switch
.end method
