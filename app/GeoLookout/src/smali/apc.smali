.class public Lapc;
.super Lapb;
.source "NetworkDialogTask.java"


# instance fields
.field a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lalo;Lalr;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p2, p3}, Lapb;-><init>(Lalo;Lalr;)V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lapc;->a:Landroid/content/Context;

    .line 20
    iput-object p1, p0, Lapc;->a:Landroid/content/Context;

    .line 21
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 26
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lapc;->a:Landroid/content/Context;

    const-class v2, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 27
    const v1, 0x14000020

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 28
    const-string v1, "network_status"

    invoke-virtual {p0}, Lapc;->b()Lalr;

    move-result-object v2

    invoke-virtual {v2}, Lalr;->p()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 29
    const-string v1, "network_error_code"

    invoke-virtual {p0}, Lapc;->b()Lalr;

    move-result-object v2

    invoke-virtual {v2}, Lalr;->q()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 30
    iget-object v1, p0, Lapc;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    :goto_0
    return-void

    .line 31
    :catch_0
    move-exception v0

    .line 32
    const-string v1, "ActivityNotFoundExceptilon"

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    .line 33
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
