.class Luw;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lut;

.field final synthetic b:Luv;

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lut;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Luv;Lut;)V
    .locals 2

    iput-object p1, p0, Luw;->b:Luv;

    iput-object p2, p0, Luw;->a:Lut;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Luw;->a:Lut;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Luw;->c:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Luw;->b:Luv;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Luv;->a(Luv;Z)Z

    iget-object v0, p0, Luw;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lut;

    if-eqz v0, :cond_0

    iget-object v1, p0, Luw;->b:Luv;

    invoke-static {v1}, Luv;->a(Luv;)Lcom/google/android/gms/internal/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lut;->b(Lcom/google/android/gms/internal/v;)V

    :cond_0
    return-void
.end method
