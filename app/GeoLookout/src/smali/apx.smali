.class public Lapx;
.super Landroid/widget/BaseExpandableListAdapter;
.source "LifeModeListViewAdapter.java"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lapz;

.field private final c:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lapx;->b:Lapz;

    .line 37
    iput-object p1, p0, Lapx;->a:Landroid/content/Context;

    .line 38
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lapx;->c:Landroid/view/LayoutInflater;

    .line 39
    return-void
.end method


# virtual methods
.method public a(I)Lamq;
    .locals 2

    .prologue
    .line 43
    sget-object v0, Lami;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamj;

    invoke-virtual {v0, p1}, Lamj;->b(I)Lamq;

    move-result-object v0

    return-object v0
.end method

.method public a(II)Lamq;
    .locals 1

    .prologue
    .line 158
    sget-object v0, Lami;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamq;

    return-object v0
.end method

.method public synthetic getChild(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0, p1, p2}, Lapx;->a(II)Lamq;

    move-result-object v0

    return-object v0
.end method

.method public getChildId(II)J
    .locals 2

    .prologue
    .line 168
    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const v9, 0x7f0e00aa

    const v6, 0x7f0e00a8

    const/16 v8, 0x8

    const/4 v5, 0x4

    const/4 v7, 0x0

    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getChildView start : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 177
    if-nez p4, :cond_0

    .line 178
    iget-object v0, p0, Lapx;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f030020

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 181
    :cond_0
    sget-object v0, Lami;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamj;

    invoke-virtual {v0, p1}, Lamj;->b(I)Lamq;

    move-result-object v3

    .line 182
    const v0, 0x7f0e00a4

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 185
    iget-object v1, p0, Lapx;->a:Landroid/content/Context;

    invoke-virtual {v3, v1}, Lamq;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    const v0, 0x7f0e00a6

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 189
    const v1, 0x7f0e00a5

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 192
    invoke-virtual {v3}, Lamq;->o()I

    move-result v2

    const/16 v4, 0x25d

    if-eq v2, v4, :cond_1

    invoke-virtual {v3}, Lamq;->o()I

    move-result v2

    const/16 v4, 0x25a

    if-ne v2, v4, :cond_6

    .line 193
    :cond_1
    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 194
    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 196
    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 197
    invoke-virtual {v1, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 198
    const v0, 0x7f0e00ac

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 199
    const v0, 0x7f0e00ae

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 200
    const v0, 0x7f0e00b5

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 202
    invoke-virtual {v3}, Lamq;->o()I

    move-result v0

    const/16 v2, 0x260

    if-ne v0, v2, :cond_5

    .line 203
    const v0, 0x7f0e00af

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0a00e8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 204
    const v0, 0x7f0e00b0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0a00e9

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 205
    const v0, 0x7f0e00b1

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0a00ea

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 206
    const v0, 0x7f0e00b2

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0a00eb

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 207
    const v0, 0x7f0e00b6

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0a00ec

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 216
    :goto_0
    invoke-virtual {v3}, Lamq;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 278
    :goto_1
    sget-object v0, Lami;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamj;

    .line 279
    invoke-virtual {v0}, Lamj;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 283
    invoke-virtual {v0}, Lamj;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 285
    if-nez v1, :cond_4

    .line 286
    iget-object v1, p0, Lapx;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f03001c

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 287
    const v2, 0x7f0e00a2

    invoke-virtual {p4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 289
    sget-boolean v5, Laky;->b:Z

    if-eqz v5, :cond_3

    .line 290
    const v5, 0x7f020100

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 292
    :cond_3
    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 293
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v5, p0, Lapx;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v0}, Lamj;->j()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 296
    invoke-virtual {v0}, Lamj;->k()I

    move-result v2

    if-nez v2, :cond_8

    .line 297
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 298
    iget-object v5, p0, Lapx;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v0}, Lamj;->l()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 299
    const/16 v5, 0xb

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 300
    const v2, 0x7f0e0089

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lamj;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ImageIcon"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 301
    const v2, 0x7f0e0087

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 309
    :goto_3
    invoke-virtual {v0}, Lamj;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    :cond_4
    move-object v2, v1

    .line 312
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lamj;->i()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "ImageIcon"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 314
    invoke-virtual {v3}, Lamq;->o()I

    move-result v5

    invoke-virtual {v0, v5}, Lamj;->d(I)Lamq;

    move-result-object v5

    if-nez v5, :cond_9

    .line 315
    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 321
    :goto_4
    const v1, 0x7f0e0088

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 322
    invoke-virtual {v0}, Lamj;->g()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    .line 209
    :cond_5
    const v0, 0x7f0e00af

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0a00e2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 210
    const v0, 0x7f0e00b0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0a00e3

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 211
    const v0, 0x7f0e00b1

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0a00e4

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 212
    const v0, 0x7f0e00b2

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0a00e5

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 213
    const v0, 0x7f0e00b6

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f0a00e6

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 218
    :pswitch_0
    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 221
    :pswitch_1
    invoke-virtual {v1, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 224
    :pswitch_2
    const v0, 0x7f0e00ac

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 227
    :pswitch_3
    const v0, 0x7f0e00ae

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 230
    :pswitch_4
    const v0, 0x7f0e00b5

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 238
    :cond_6
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 239
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 241
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 242
    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 243
    const v1, 0x7f0e00ac

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 244
    const v1, 0x7f0e00ae

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 246
    invoke-virtual {v3}, Lamq;->o()I

    move-result v1

    const/16 v2, 0x260

    if-ne v1, v2, :cond_7

    .line 247
    const v1, 0x7f0e00af

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0a00e8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 248
    const v1, 0x7f0e00b0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0a00e9

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 249
    const v1, 0x7f0e00b1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0a00ea

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 250
    const v1, 0x7f0e00b2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0a00eb

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 258
    :goto_5
    invoke-virtual {v3}, Lamq;->a()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    goto/16 :goto_1

    .line 260
    :pswitch_5
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 252
    :cond_7
    const v1, 0x7f0e00af

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0a00e2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 253
    const v1, 0x7f0e00b0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0a00e3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 254
    const v1, 0x7f0e00b1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0a00e4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 255
    const v1, 0x7f0e00b2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0a00e5

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_5

    .line 263
    :pswitch_6
    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 266
    :pswitch_7
    const v1, 0x7f0e00ac

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 269
    :pswitch_8
    const v1, 0x7f0e00ae

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 303
    :cond_8
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v5, p0, Lapx;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v0}, Lamj;->k()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 305
    const v2, 0x7f0e0087

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lamj;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ImageIcon"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 306
    const v2, 0x7f0e0089

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 317
    :cond_9
    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 318
    invoke-virtual {v3}, Lamq;->o()I

    move-result v5

    invoke-virtual {v0, v5}, Lamj;->d(I)Lamq;

    move-result-object v5

    invoke-virtual {v5}, Lamq;->g()I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_4

    .line 325
    :cond_a
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_b

    .line 326
    const v0, 0x7f0e00a6

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 327
    const v0, 0x7f0e00a5

    invoke-virtual {p4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 330
    :cond_b
    return-object p4

    .line 216
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 258
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public getChildrenCount(I)I
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic getGroup(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lapx;->a(I)Lamq;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48
    sget-object v0, Lami;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamj;

    invoke-virtual {v0}, Lamj;->f()I

    move-result v0

    sget-object v1, Lami;->b:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 49
    sget-object v0, Lami;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 51
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lami;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamj;

    invoke-virtual {v0}, Lamj;->f()I

    move-result v0

    goto :goto_0
.end method

.method public getGroupId(I)J
    .locals 2

    .prologue
    .line 57
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const v7, 0x7f0a00e1

    const/16 v6, 0x8

    const v5, 0x7f080060

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 62
    const-string v0, "getGroupView start"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 65
    if-nez p3, :cond_1

    .line 66
    new-instance v0, Lapz;

    invoke-direct {v0}, Lapz;-><init>()V

    iput-object v0, p0, Lapx;->b:Lapz;

    .line 67
    iget-object v0, p0, Lapx;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f030023

    invoke-virtual {v0, v1, p4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    .line 68
    iget-object v1, p0, Lapx;->b:Lapz;

    const v0, 0x7f0e00b8

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lapz;->a:Landroid/widget/ImageView;

    .line 70
    iget-object v1, p0, Lapx;->b:Lapz;

    const v0, 0x7f0e00b9

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lapz;->b:Landroid/widget/TextView;

    .line 72
    iget-object v1, p0, Lapx;->b:Lapz;

    const v0, 0x7f0e00ba

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lapz;->c:Landroid/widget/TextView;

    .line 74
    iget-object v1, p0, Lapx;->b:Lapz;

    const v0, 0x7f0e00bb

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lapz;->d:Landroid/widget/ImageView;

    .line 76
    iget-object v1, p0, Lapx;->b:Lapz;

    const v0, 0x7f0e00bc

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lapz;->e:Landroid/widget/TextView;

    .line 78
    iget-object v1, p0, Lapx;->b:Lapz;

    const v0, 0x7f0e00bd

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lapz;->f:Landroid/widget/ImageView;

    .line 81
    iget-object v0, p0, Lapx;->b:Lapz;

    invoke-virtual {p3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 87
    :goto_0
    sget-object v0, Lami;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamj;

    invoke-virtual {v0}, Lamj;->f()I

    move-result v1

    .line 89
    if-le v1, p1, :cond_2

    .line 90
    sget-object v0, Lami;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamj;

    invoke-virtual {v0, p1}, Lamj;->b(I)Lamq;

    move-result-object v0

    .line 95
    :goto_1
    if-eqz v0, :cond_5

    .line 96
    iget-object v1, p0, Lapx;->b:Lapz;

    iget-object v1, v1, Lapz;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Lamq;->m()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 98
    invoke-virtual {v0}, Lamq;->a()I

    move-result v1

    if-lez v1, :cond_4

    .line 99
    iget-object v1, p0, Lapx;->b:Lapz;

    iget-object v1, v1, Lapz;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lamq;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 100
    iget-object v1, p0, Lapx;->b:Lapz;

    iget-object v1, v1, Lapz;->b:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 101
    sget-boolean v1, Laky;->b:Z

    if-eqz v1, :cond_3

    .line 102
    iget-object v1, p0, Lapx;->b:Lapz;

    iget-object v1, v1, Lapz;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lapx;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lamq;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v1, p0, Lapx;->b:Lapz;

    iget-object v1, v1, Lapz;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Lamq;->j()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 104
    iget-object v1, p0, Lapx;->b:Lapz;

    iget-object v1, v1, Lapz;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 105
    iget-object v1, p0, Lapx;->b:Lapz;

    iget-object v1, v1, Lapz;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 111
    :cond_0
    :goto_2
    iget-object v1, p0, Lapx;->b:Lapz;

    iget-object v1, v1, Lapz;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lamq;->l()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 112
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 113
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 143
    :goto_3
    if-eqz p2, :cond_6

    .line 144
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->f:Landroid/widget/ImageView;

    const v1, 0x7f02016f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 145
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->b:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 146
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->b:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 153
    :goto_4
    return-object p3

    .line 83
    :cond_1
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapz;

    iput-object v0, p0, Lapx;->b:Lapz;

    goto/16 :goto_0

    .line 92
    :cond_2
    sget-object v2, Lami;->b:Landroid/util/SparseArray;

    sget-object v0, Lami;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamj;

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lamj;->e(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamq;

    goto/16 :goto_1

    .line 108
    :cond_3
    iget-object v1, p0, Lapx;->b:Lapz;

    iget-object v1, v1, Lapz;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 109
    iget-object v1, p0, Lapx;->b:Lapz;

    iget-object v1, v1, Lapz;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Lamq;->i()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 116
    :cond_4
    iget-object v1, p0, Lapx;->b:Lapz;

    iget-object v1, v1, Lapz;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lamq;->f()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 117
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lapx;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 119
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lapx;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 121
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->c:Landroid/widget/TextView;

    const v1, 0x7f0a00e7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 122
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 127
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 130
    :cond_5
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v1, v0, Lapz;->a:Landroid/widget/ImageView;

    sget-object v0, Lami;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamq;

    invoke-virtual {v0}, Lamq;->e()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 132
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lapx;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 134
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lapx;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 136
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(I)V

    .line 137
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(I)V

    .line 138
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->d:Landroid/widget/ImageView;

    const v1, 0x7f0200d7

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 139
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 148
    :cond_6
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->f:Landroid/widget/ImageView;

    const v1, 0x7f020170

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 149
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->b:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 150
    iget-object v0, p0, Lapx;->b:Lapz;

    iget-object v0, v0, Lapz;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSelected(Z)V

    goto/16 :goto_4
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 335
    const/4 v0, 0x1

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 340
    sget-object v0, Lami;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamq;

    invoke-virtual {v0}, Lamq;->o()I

    move-result v2

    .line 341
    sget-object v0, Lami;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamj;

    invoke-virtual {v0, v2}, Lamj;->d(I)Lamq;

    move-result-object v0

    .line 343
    if-eqz v0, :cond_0

    .line 344
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "JAEMIN isChildSelectable "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 345
    const/4 v0, 0x1

    .line 348
    :goto_0
    return v0

    .line 347
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JAEMIN isChildSelectable "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " false"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    move v0, v1

    .line 348
    goto :goto_0
.end method

.method public onGroupCollapsed(I)V
    .locals 1

    .prologue
    .line 361
    const-string v0, "onGroupCollapsed"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 362
    invoke-virtual {p0}, Lapx;->notifyDataSetChanged()V

    .line 363
    invoke-super {p0, p1}, Landroid/widget/BaseExpandableListAdapter;->onGroupCollapsed(I)V

    .line 364
    return-void
.end method

.method public onGroupExpanded(I)V
    .locals 1

    .prologue
    .line 354
    const-string v0, "onGroupExpanded"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 355
    invoke-virtual {p0}, Lapx;->notifyDataSetChanged()V

    .line 356
    invoke-super {p0, p1}, Landroid/widget/BaseExpandableListAdapter;->onGroupExpanded(I)V

    .line 357
    return-void
.end method
