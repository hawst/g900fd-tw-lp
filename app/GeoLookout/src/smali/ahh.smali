.class public Lahh;
.super Ljava/lang/Object;
.source "DisasterDashboardActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V
    .locals 0

    .prologue
    .line 2575
    iput-object p1, p0, Lahh;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2579
    iget-object v0, p0, Lahh;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 2580
    iget-object v0, p0, Lahh;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->k(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/widget/TabHost;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v2

    .line 2581
    iget-object v0, p0, Lahh;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)V

    .line 2582
    iget-object v0, p0, Lahh;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->k(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/widget/TabHost;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TabHost;->clearAllTabs()V

    .line 2583
    const-string v0, "pref_geonews_current_page"

    const/4 v3, -0x1

    invoke-static {v1, v0, v3}, Lall;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v3

    iget-object v0, p0, Lahh;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    iget v0, v0, Laif;->m:I

    if-ne v3, v0, :cond_0

    .line 2585
    const-string v0, "pref_geonews_current_page"

    invoke-static {v1, v0, v4}, Lall;->b(Landroid/content/Context;Ljava/lang/String;I)V

    .line 2587
    :cond_0
    iget-object v0, p0, Lahh;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    iget v0, v0, Laif;->m:I

    invoke-static {v1, v0}, Lalz;->a(Landroid/content/Context;I)V

    .line 2588
    iget-object v0, p0, Lahh;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    iget v0, v0, Laif;->m:I

    invoke-static {v1, v0}, Lalx;->d(Landroid/content/Context;I)V

    .line 2590
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/GeoLookout/DisasterService;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2591
    const-string v1, "com.sec.android.geolookout.updatelocation"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2592
    iget-object v1, p0, Lahh;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 2593
    iget-object v0, p0, Lahh;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2594
    iget-object v0, p0, Lahh;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->r(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    .line 2595
    iget-object v0, p0, Lahh;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->invalidateOptionsMenu()V

    .line 2596
    iget-object v0, p0, Lahh;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 2597
    if-eqz v0, :cond_1

    iget v1, v0, Laif;->n:I

    if-eqz v1, :cond_1

    .line 2598
    iget-object v1, p0, Lahh;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iget-object v0, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationType()I

    move-result v0

    invoke-static {v1, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->f(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)I

    .line 2599
    iget-object v0, p0, Lahh;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iget-object v1, p0, Lahh;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)I

    move-result v1

    invoke-static {v0, v1, v4}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;II)V

    .line 2600
    iget-object v0, p0, Lahh;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0, v4}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->d(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)V

    .line 2602
    :cond_1
    return-void
.end method
