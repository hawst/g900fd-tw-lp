.class public final Lhe;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/android/gms/internal/bu;

.field private final b:Lhq;

.field private final c:Landroid/content/Context;

.field private final d:Ljava/lang/Object;

.field private final e:Lhh;

.field private f:Z

.field private g:Lhk;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/bu;Lhq;Lhh;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lhe;->d:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhe;->f:Z

    iput-object p1, p0, Lhe;->c:Landroid/content/Context;

    iput-object p2, p0, Lhe;->a:Lcom/google/android/gms/internal/bu;

    iput-object p3, p0, Lhe;->b:Lhq;

    iput-object p4, p0, Lhe;->e:Lhh;

    return-void
.end method


# virtual methods
.method public a(JJ)Lhm;
    .locals 11

    const-string v0, "Starting mediation."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lhe;->e:Lhh;

    iget-object v0, v0, Lhh;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lhg;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Trying mediation network: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v5, Lhg;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Llm;->c(Ljava/lang/String;)V

    iget-object v0, v5, Lhg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v10, p0, Lhe;->d:Ljava/lang/Object;

    monitor-enter v10

    :try_start_0
    iget-boolean v0, p0, Lhe;->f:Z

    if-eqz v0, :cond_2

    new-instance v0, Lhm;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lhm;-><init>(I)V

    monitor-exit v10

    :goto_1
    return-object v0

    :cond_2
    new-instance v0, Lhk;

    iget-object v1, p0, Lhe;->c:Landroid/content/Context;

    iget-object v3, p0, Lhe;->b:Lhq;

    iget-object v4, p0, Lhe;->e:Lhh;

    iget-object v6, p0, Lhe;->a:Lcom/google/android/gms/internal/bu;

    iget-object v6, v6, Lcom/google/android/gms/internal/bu;->d:Lcom/google/android/gms/internal/v;

    iget-object v7, p0, Lhe;->a:Lcom/google/android/gms/internal/bu;

    iget-object v7, v7, Lcom/google/android/gms/internal/bu;->e:Lcom/google/android/gms/internal/x;

    invoke-direct/range {v0 .. v7}, Lhk;-><init>(Landroid/content/Context;Ljava/lang/String;Lhq;Lhh;Lhg;Lcom/google/android/gms/internal/v;Lcom/google/android/gms/internal/x;)V

    iput-object v0, p0, Lhe;->g:Lhk;

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lhe;->g:Lhk;

    invoke-virtual {v0, p1, p2, p3, p4}, Lhk;->a(JJ)Lhm;

    move-result-object v0

    iget v1, v0, Lhm;->a:I

    if-nez v1, :cond_3

    const-string v1, "Adapter succeeded."

    invoke-static {v1}, Llm;->a(Ljava/lang/String;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    iget-object v1, v0, Lhm;->c:Lht;

    if-eqz v1, :cond_1

    sget-object v1, Lll;->a:Landroid/os/Handler;

    new-instance v2, Lhf;

    invoke-direct {v2, p0, v0}, Lhf;-><init>(Lhe;Lhm;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_4
    new-instance v0, Lhm;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lhm;-><init>(I)V

    goto :goto_1
.end method

.method public a()V
    .locals 2

    iget-object v1, p0, Lhe;->d:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lhe;->f:Z

    iget-object v0, p0, Lhe;->g:Lhk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhe;->g:Lhk;

    invoke-virtual {v0}, Lhk;->a()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
