.class public Lapp;
.super Lapb;
.source "UpdateHistoryFromServerTask.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lamf;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lalo;Lalr;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p2, p3}, Lapb;-><init>(Lalo;Lalr;)V

    .line 25
    iput-object p1, p0, Lapp;->a:Landroid/content/Context;

    .line 26
    return-void
.end method

.method private a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 62
    new-instance v1, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    invoke-direct {v1, p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;-><init>(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 64
    iget-object v2, p0, Lapp;->b:Lamf;

    new-instance v3, Lamf;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v4

    iget-wide v4, v4, Lamf;->a:D

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v6

    iget-wide v6, v6, Lamf;->b:D

    invoke-direct {v3, v4, v5, v6, v7}, Lamf;-><init>(DD)V

    invoke-static {v2, v3}, Lall;->a(Lamf;Lamf;)I

    move-result v2

    .line 65
    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDistance(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 67
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationType()I

    move-result v3

    if-nez v3, :cond_3

    .line 70
    if-gez v2, :cond_0

    .line 78
    :goto_0
    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 79
    const-string v0, "checkDisasterInfoIsValid : invalid significance "

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 80
    const/4 v0, 0x0

    .line 106
    :goto_1
    return-object v0

    .line 72
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v0

    const/16 v3, 0x69

    if-ne v0, v3, :cond_1

    .line 73
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailsMagnitude()D

    move-result-wide v4

    invoke-static {v4, v5, v2}, Lall;->a(DI)I

    move-result v0

    goto :goto_0

    .line 75
    :cond_1
    invoke-static {v2}, Lall;->a(I)I

    move-result v0

    goto :goto_0

    .line 83
    :cond_2
    invoke-virtual {v1, v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setSignificance(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 86
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 106
    :cond_4
    :goto_2
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->build()Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    goto :goto_1

    .line 91
    :sswitch_0
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailSpeed()Ljava/lang/String;

    move-result-object v0

    .line 92
    const-string v2, ","

    const-string v3, "."

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 93
    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 95
    const-string v0, "01"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 96
    const-wide v4, 0x3fdc9c4da9003eeaL    # 0.44704

    mul-double/2addr v2, v4

    .line 97
    const-string v0, "%.1f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 98
    invoke-virtual {v1, v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailSpeed(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    .line 100
    :catch_0
    move-exception v0

    goto :goto_2

    .line 101
    :catch_1
    move-exception v0

    goto :goto_2

    .line 86
    nop

    :sswitch_data_0
    .sparse-switch
        0x6e -> :sswitch_0
        0x70 -> :sswitch_0
        0xcd -> :sswitch_0
    .end sparse-switch
.end method

.method private a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/GeoLookout/db/DisasterInfo;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/GeoLookout/db/DisasterInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 50
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 51
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-direct {p0, v0}, Lapp;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 58
    :cond_1
    return-object v2
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lapp;->b:Lamf;

    .line 32
    invoke-virtual {p0}, Lapp;->b()Lalr;

    move-result-object v0

    invoke-virtual {v0}, Lalr;->r()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 33
    new-instance v0, Lamf;

    invoke-virtual {p0}, Lapp;->b()Lalr;

    move-result-object v1

    invoke-virtual {v1}, Lalr;->r()Landroid/location/Location;

    move-result-object v1

    invoke-direct {v0, v1}, Lamf;-><init>(Landroid/location/Location;)V

    iput-object v0, p0, Lapp;->b:Lamf;

    .line 36
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 37
    invoke-virtual {p0}, Lapp;->b()Lalr;

    move-result-object v0

    invoke-virtual {v0}, Lalr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lapp;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lapp;->b()Lalr;

    move-result-object v2

    invoke-virtual {v2}, Lalr;->y()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterParser;->a(Ljava/lang/String;Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v0

    .line 38
    invoke-direct {p0, v0}, Lapp;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 40
    iget-object v1, p0, Lapp;->a:Landroid/content/Context;

    const-wide/32 v2, 0x240c8400

    invoke-static {v1, v2, v3}, Laly;->b(Landroid/content/Context;J)V

    .line 41
    iget-object v1, p0, Lapp;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Laly;->a(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 43
    invoke-virtual {p0}, Lapp;->b()Lalr;

    move-result-object v0

    invoke-virtual {v0}, Lalr;->l()Lanx;

    move-result-object v0

    invoke-interface {v0}, Lanx;->a()V

    .line 44
    return-void
.end method
