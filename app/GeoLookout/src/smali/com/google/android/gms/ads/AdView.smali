.class public final Lcom/google/android/gms/ads/AdView;
.super Landroid/view/ViewGroup;


# instance fields
.field private final a:Lhp;

.field private b:Lav;

.field private c:Lgg;

.field private d:Laz;

.field private e:Ljava/lang/String;

.field private f:Lbb;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    new-instance v0, Lhp;

    invoke-direct {v0}, Lhp;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/AdView;->a:Lhp;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/ads/AdView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lhp;

    invoke-direct {v0}, Lhp;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/AdView;->a:Lhp;

    :try_start_0
    new-instance v0, Lcom/google/android/gms/internal/aa;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/internal/aa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {v0}, Lcom/google/android/gms/internal/aa;->a()Laz;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/ads/AdView;->d:Laz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/aa;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/AdView;->e:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p0}, Lcom/google/android/gms/ads/AdView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/x;

    iget-object v1, p0, Lcom/google/android/gms/ads/AdView;->d:Laz;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/internal/x;-><init>(Landroid/content/Context;Laz;)V

    const-string v1, "Ads by Google"

    invoke-static {p0, v0, v1}, Lll;->b(Landroid/view/ViewGroup;Lcom/google/android/gms/internal/x;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/internal/x;

    sget-object v2, Laz;->c:Laz;

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/internal/x;-><init>(Landroid/content/Context;Laz;)V

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lll;->a(Landroid/view/ViewGroup;Lcom/google/android/gms/internal/x;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->d:Laz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/ads/AdView;->b(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/ads/AdView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/internal/x;

    iget-object v2, p0, Lcom/google/android/gms/ads/AdView;->d:Laz;

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/internal/x;-><init>(Landroid/content/Context;Laz;)V

    iget-object v2, p0, Lcom/google/android/gms/ads/AdView;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/ads/AdView;->a:Lhp;

    invoke-static {v0, v1, v2, v3}, Luy;->a(Landroid/content/Context;Lcom/google/android/gms/internal/x;Ljava/lang/String;Lhp;)Lgg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/AdView;->c:Lgg;

    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->b:Lav;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->c:Lgg;

    new-instance v1, Lux;

    iget-object v2, p0, Lcom/google/android/gms/ads/AdView;->b:Lav;

    invoke-direct {v1, v2}, Lux;-><init>(Lav;)V

    invoke-interface {v0, v1}, Lgg;->a(Lgd;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->f:Lbb;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->c:Lgg;

    new-instance v1, Lvb;

    iget-object v2, p0, Lcom/google/android/gms/ads/AdView;->f:Lbb;

    invoke-direct {v1, v2}, Lvb;-><init>(Lbb;)V

    invoke-interface {v0, v1}, Lgg;->a(Lgm;)V

    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/ads/AdView;->d()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->c:Lgg;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The ad size and ad unit ID must be set on AdView before "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is called."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private d()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->c:Lgg;

    invoke-interface {v0}, Lgg;->a()Ldp;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Lds;->a(Ldp;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/AdView;->addView(Landroid/view/View;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Failed to get an ad frame."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->c:Lgg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->c:Lgg;

    invoke-interface {v0}, Lgg;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to destroy AdView."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(Law;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->c:Lgg;

    if-nez v0, :cond_0

    const-string v0, "loadAd"

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/AdView;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->c:Lgg;

    new-instance v1, Lcom/google/android/gms/internal/v;

    invoke-virtual {p0}, Lcom/google/android/gms/ads/AdView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/google/android/gms/internal/v;-><init>(Landroid/content/Context;Law;)V

    invoke-interface {v0, v1}, Lgg;->a(Lcom/google/android/gms/internal/v;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->a:Lhp;

    invoke-virtual {p1}, Law;->d()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhp;->a(Ljava/util/Map;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to load ad."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->c:Lgg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->c:Lgg;

    invoke-interface {v0}, Lgg;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to call pause."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public c()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->c:Lgg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->c:Lgg;

    invoke-interface {v0}, Lgg;->e()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to call resume."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getAdListener()Lav;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->b:Lav;

    return-object v0
.end method

.method public getAdSize()Laz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->d:Laz;

    return-object v0
.end method

.method public getAdUnitId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->e:Ljava/lang/String;

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/AdView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    sub-int v3, p4, p2

    sub-int/2addr v3, v1

    div-int/lit8 v3, v3, 0x2

    sub-int v4, p5, p3

    sub-int/2addr v4, v2

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v1, v3

    add-int/2addr v2, v4

    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/view/View;->layout(IIII)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/AdView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v3, 0x8

    if-eq v1, v3, :cond_0

    invoke-virtual {p0, v2, p1, p2}, Lcom/google/android/gms/ads/AdView;->measureChild(Landroid/view/View;II)V

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/ads/AdView;->getSuggestedMinimumWidth()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/gms/ads/AdView;->getSuggestedMinimumHeight()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, p1}, Lcom/google/android/gms/ads/AdView;->resolveSize(II)I

    move-result v1

    invoke-static {v0, p2}, Lcom/google/android/gms/ads/AdView;->resolveSize(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/ads/AdView;->setMeasuredDimension(II)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/ads/AdView;->d:Laz;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/ads/AdView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/ads/AdView;->d:Laz;

    invoke-virtual {v1, v0}, Laz;->b(Landroid/content/Context;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/ads/AdView;->d:Laz;

    invoke-virtual {v2, v0}, Laz;->a(Landroid/content/Context;)I

    move-result v0

    goto :goto_0

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method public setAdListener(Lav;)V
    .locals 2

    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/ads/AdView;->b:Lav;

    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->c:Lgg;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/ads/AdView;->c:Lgg;

    if-eqz p1, :cond_1

    new-instance v0, Lux;

    invoke-direct {v0, p1}, Lux;-><init>(Lav;)V

    :goto_0
    invoke-interface {v1, v0}, Lgg;->a(Lgd;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Failed to set the AdListener."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public setAdSize(Laz;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->d:Laz;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The ad size can only be set once on AdView."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/ads/AdView;->d:Laz;

    invoke-virtual {p0}, Lcom/google/android/gms/ads/AdView;->requestLayout()V

    return-void
.end method

.method public setAdUnitId(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/ads/AdView;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The ad unit ID can only be set once on AdView."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/ads/AdView;->e:Ljava/lang/String;

    return-void
.end method
