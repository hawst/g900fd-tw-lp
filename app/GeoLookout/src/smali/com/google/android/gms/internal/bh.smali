.class public final Lcom/google/android/gms/internal/bh;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final a:Ljj;


# instance fields
.field public final b:I

.field public final c:Lcom/google/android/gms/internal/be;

.field public final d:Lus;

.field public final e:Ljk;

.field public final f:Lcom/google/android/gms/internal/cq;

.field public final g:Lgq;

.field public final i:Ljava/lang/String;

.field public final j:Z

.field public final k:Ljava/lang/String;

.field public final l:Ljn;

.field public final m:I

.field public final n:I

.field public final o:Ljava/lang/String;

.field public final p:Lcom/google/android/gms/internal/co;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljj;

    invoke-direct {v0}, Ljj;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/bh;->a:Ljj;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/internal/be;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Ljava/lang/String;ZLjava/lang/String;Landroid/os/IBinder;IILjava/lang/String;Lcom/google/android/gms/internal/co;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/bh;->b:I

    iput-object p2, p0, Lcom/google/android/gms/internal/bh;->c:Lcom/google/android/gms/internal/be;

    invoke-static {p3}, Ldq;->a(Landroid/os/IBinder;)Ldp;

    move-result-object v0

    invoke-static {v0}, Lds;->a(Ldp;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lus;

    iput-object v0, p0, Lcom/google/android/gms/internal/bh;->d:Lus;

    invoke-static {p4}, Ldq;->a(Landroid/os/IBinder;)Ldp;

    move-result-object v0

    invoke-static {v0}, Lds;->a(Ldp;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljk;

    iput-object v0, p0, Lcom/google/android/gms/internal/bh;->e:Ljk;

    invoke-static {p5}, Ldq;->a(Landroid/os/IBinder;)Ldp;

    move-result-object v0

    invoke-static {v0}, Lds;->a(Ldp;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/cq;

    iput-object v0, p0, Lcom/google/android/gms/internal/bh;->f:Lcom/google/android/gms/internal/cq;

    invoke-static {p6}, Ldq;->a(Landroid/os/IBinder;)Ldp;

    move-result-object v0

    invoke-static {v0}, Lds;->a(Ldp;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgq;

    iput-object v0, p0, Lcom/google/android/gms/internal/bh;->g:Lgq;

    iput-object p7, p0, Lcom/google/android/gms/internal/bh;->i:Ljava/lang/String;

    iput-boolean p8, p0, Lcom/google/android/gms/internal/bh;->j:Z

    iput-object p9, p0, Lcom/google/android/gms/internal/bh;->k:Ljava/lang/String;

    invoke-static {p10}, Ldq;->a(Landroid/os/IBinder;)Ldp;

    move-result-object v0

    invoke-static {v0}, Lds;->a(Ldp;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljn;

    iput-object v0, p0, Lcom/google/android/gms/internal/bh;->l:Ljn;

    iput p11, p0, Lcom/google/android/gms/internal/bh;->m:I

    iput p12, p0, Lcom/google/android/gms/internal/bh;->n:I

    iput-object p13, p0, Lcom/google/android/gms/internal/bh;->o:Ljava/lang/String;

    iput-object p14, p0, Lcom/google/android/gms/internal/bh;->p:Lcom/google/android/gms/internal/co;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/be;Lus;Ljk;Ljn;Lcom/google/android/gms/internal/co;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/bh;->b:I

    iput-object p1, p0, Lcom/google/android/gms/internal/bh;->c:Lcom/google/android/gms/internal/be;

    iput-object p2, p0, Lcom/google/android/gms/internal/bh;->d:Lus;

    iput-object p3, p0, Lcom/google/android/gms/internal/bh;->e:Ljk;

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->f:Lcom/google/android/gms/internal/cq;

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->g:Lgq;

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->i:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/bh;->j:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->k:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/bh;->l:Ljn;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/bh;->m:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/internal/bh;->n:I

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->o:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/internal/bh;->p:Lcom/google/android/gms/internal/co;

    return-void
.end method

.method public constructor <init>(Lus;Ljk;Lgq;Ljn;Lcom/google/android/gms/internal/cq;ZILjava/lang/String;Lcom/google/android/gms/internal/co;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/bh;->b:I

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->c:Lcom/google/android/gms/internal/be;

    iput-object p1, p0, Lcom/google/android/gms/internal/bh;->d:Lus;

    iput-object p2, p0, Lcom/google/android/gms/internal/bh;->e:Ljk;

    iput-object p5, p0, Lcom/google/android/gms/internal/bh;->f:Lcom/google/android/gms/internal/cq;

    iput-object p3, p0, Lcom/google/android/gms/internal/bh;->g:Lgq;

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->i:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/google/android/gms/internal/bh;->j:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->k:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/bh;->l:Ljn;

    iput p7, p0, Lcom/google/android/gms/internal/bh;->m:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/internal/bh;->n:I

    iput-object p8, p0, Lcom/google/android/gms/internal/bh;->o:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/gms/internal/bh;->p:Lcom/google/android/gms/internal/co;

    return-void
.end method

.method public constructor <init>(Lus;Ljk;Lgq;Ljn;Lcom/google/android/gms/internal/cq;ZILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/co;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/bh;->b:I

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->c:Lcom/google/android/gms/internal/be;

    iput-object p1, p0, Lcom/google/android/gms/internal/bh;->d:Lus;

    iput-object p2, p0, Lcom/google/android/gms/internal/bh;->e:Ljk;

    iput-object p5, p0, Lcom/google/android/gms/internal/bh;->f:Lcom/google/android/gms/internal/cq;

    iput-object p3, p0, Lcom/google/android/gms/internal/bh;->g:Lgq;

    iput-object p9, p0, Lcom/google/android/gms/internal/bh;->i:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/google/android/gms/internal/bh;->j:Z

    iput-object p8, p0, Lcom/google/android/gms/internal/bh;->k:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/bh;->l:Ljn;

    iput p7, p0, Lcom/google/android/gms/internal/bh;->m:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/internal/bh;->n:I

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->o:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/gms/internal/bh;->p:Lcom/google/android/gms/internal/co;

    return-void
.end method

.method public constructor <init>(Lus;Ljk;Ljn;Lcom/google/android/gms/internal/cq;ILcom/google/android/gms/internal/co;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/google/android/gms/internal/bh;->b:I

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->c:Lcom/google/android/gms/internal/be;

    iput-object p1, p0, Lcom/google/android/gms/internal/bh;->d:Lus;

    iput-object p2, p0, Lcom/google/android/gms/internal/bh;->e:Ljk;

    iput-object p4, p0, Lcom/google/android/gms/internal/bh;->f:Lcom/google/android/gms/internal/cq;

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->g:Lgq;

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->i:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/bh;->j:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->k:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/internal/bh;->l:Ljn;

    iput p5, p0, Lcom/google/android/gms/internal/bh;->m:I

    iput v2, p0, Lcom/google/android/gms/internal/bh;->n:I

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->o:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/internal/bh;->p:Lcom/google/android/gms/internal/co;

    return-void
.end method

.method public constructor <init>(Lus;Ljk;Ljn;Lcom/google/android/gms/internal/cq;ZILcom/google/android/gms/internal/co;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/bh;->b:I

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->c:Lcom/google/android/gms/internal/be;

    iput-object p1, p0, Lcom/google/android/gms/internal/bh;->d:Lus;

    iput-object p2, p0, Lcom/google/android/gms/internal/bh;->e:Ljk;

    iput-object p4, p0, Lcom/google/android/gms/internal/bh;->f:Lcom/google/android/gms/internal/cq;

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->g:Lgq;

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->i:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/google/android/gms/internal/bh;->j:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->k:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/internal/bh;->l:Ljn;

    iput p6, p0, Lcom/google/android/gms/internal/bh;->m:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/internal/bh;->n:I

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->o:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/internal/bh;->p:Lcom/google/android/gms/internal/co;

    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/google/android/gms/internal/bh;
    .locals 2

    :try_start_0
    const-string v0, "com.google.android.gms.ads.inernal.overlay.AdOverlayInfo"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-class v1, Lcom/google/android/gms/internal/bh;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v1, "com.google.android.gms.ads.inernal.overlay.AdOverlayInfo"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/bh;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;Lcom/google/android/gms/internal/bh;)V
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    const-string v1, "com.google.android.gms.ads.inernal.overlay.AdOverlayInfo"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "com.google.android.gms.ads.inernal.overlay.AdOverlayInfo"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public a()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bh;->d:Lus;

    invoke-static {v0}, Lds;->a(Ljava/lang/Object;)Ldp;

    move-result-object v0

    invoke-interface {v0}, Ldp;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public b()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bh;->e:Ljk;

    invoke-static {v0}, Lds;->a(Ljava/lang/Object;)Ldp;

    move-result-object v0

    invoke-interface {v0}, Ldp;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public c()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bh;->f:Lcom/google/android/gms/internal/cq;

    invoke-static {v0}, Lds;->a(Ljava/lang/Object;)Ldp;

    move-result-object v0

    invoke-interface {v0}, Ldp;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public d()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bh;->g:Lgq;

    invoke-static {v0}, Lds;->a(Ljava/lang/Object;)Ldp;

    move-result-object v0

    invoke-interface {v0}, Ldp;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bh;->l:Ljn;

    invoke-static {v0}, Lds;->a(Ljava/lang/Object;)Ldp;

    move-result-object v0

    invoke-interface {v0}, Ldp;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Ljj;->a(Lcom/google/android/gms/internal/bh;Landroid/os/Parcel;I)V

    return-void
.end method
