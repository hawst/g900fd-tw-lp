.class public Lcom/google/android/gms/internal/dr;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final a:Lob;


# instance fields
.field private final b:I

.field private final c:Lcom/google/android/gms/internal/dt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lob;

    invoke-direct {v0}, Lob;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/dr;->a:Lob;

    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/internal/dt;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/dr;->b:I

    iput-object p2, p0, Lcom/google/android/gms/internal/dr;->c:Lcom/google/android/gms/internal/dt;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/internal/dt;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/dr;->b:I

    iput-object p1, p0, Lcom/google/android/gms/internal/dr;->c:Lcom/google/android/gms/internal/dt;

    return-void
.end method

.method public static a(Loe;)Lcom/google/android/gms/internal/dr;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Loe",
            "<**>;)",
            "Lcom/google/android/gms/internal/dr;"
        }
    .end annotation

    instance-of v0, p0, Lcom/google/android/gms/internal/dt;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/dr;

    check-cast p0, Lcom/google/android/gms/internal/dt;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/dr;-><init>(Lcom/google/android/gms/internal/dt;)V

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported safe parcelable field converter class."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/dr;->b:I

    return v0
.end method

.method public b()Lcom/google/android/gms/internal/dt;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/dr;->c:Lcom/google/android/gms/internal/dt;

    return-object v0
.end method

.method public c()Loe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Loe",
            "<**>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/dr;->c:Lcom/google/android/gms/internal/dt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/dr;->c:Lcom/google/android/gms/internal/dt;

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There was no converter wrapped in this ConverterWrapper."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/dr;->a:Lob;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/dr;->a:Lob;

    invoke-static {p0, p1, p2}, Lob;->a(Lcom/google/android/gms/internal/dr;Landroid/os/Parcel;I)V

    return-void
.end method
