.class public interface abstract Lcom/google/android/gms/games/multiplayer/Invitation;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcm;
.implements Lfl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;",
        "Lcm",
        "<",
        "Lcom/google/android/gms/games/multiplayer/Invitation;",
        ">;",
        "Lfl;"
    }
.end annotation


# virtual methods
.method public abstract a()Lcom/google/android/gms/games/Game;
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract c()Lcom/google/android/gms/games/multiplayer/Participant;
.end method

.method public abstract d()J
.end method

.method public abstract e()I
.end method

.method public abstract f()I
.end method
