.class public final Lcom/google/android/gms/internal/v;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final a:Luz;


# instance fields
.field public final b:I

.field public final c:J

.field public final d:Landroid/os/Bundle;

.field public final e:I

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Z

.field public final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Luz;

    invoke-direct {v0}, Luz;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/v;->a:Luz;

    return-void
.end method

.method public constructor <init>(IJLandroid/os/Bundle;ILjava/util/List;ZI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Landroid/os/Bundle;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;ZI)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/v;->b:I

    iput-wide p2, p0, Lcom/google/android/gms/internal/v;->c:J

    iput-object p4, p0, Lcom/google/android/gms/internal/v;->d:Landroid/os/Bundle;

    iput p5, p0, Lcom/google/android/gms/internal/v;->e:I

    iput-object p6, p0, Lcom/google/android/gms/internal/v;->f:Ljava/util/List;

    iput-boolean p7, p0, Lcom/google/android/gms/internal/v;->g:Z

    iput p8, p0, Lcom/google/android/gms/internal/v;->i:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Law;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/v;->b:I

    invoke-virtual {p2}, Law;->a()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lcom/google/android/gms/internal/v;->c:J

    invoke-virtual {p2}, Law;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/v;->e:I

    invoke-virtual {p2}, Law;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/android/gms/internal/v;->f:Ljava/util/List;

    invoke-virtual {p2, p1}, Law;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/v;->g:Z

    invoke-virtual {p2}, Law;->e()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/v;->i:I

    const-class v0, Lbf;

    invoke-virtual {p2, v0}, Law;->a(Ljava/lang/Class;)Lbe;

    move-result-object v0

    check-cast v0, Lbf;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbf;->a()Landroid/os/Bundle;

    move-result-object v2

    :cond_0
    iput-object v2, p0, Lcom/google/android/gms/internal/v;->d:Landroid/os/Bundle;

    return-void

    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0

    :cond_2
    move-object v0, v2

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Luz;->a(Lcom/google/android/gms/internal/v;Landroid/os/Parcel;I)V

    return-void
.end method
