.class public Lcom/sec/android/GeoLookout/view/LifeModeListView;
.super Landroid/widget/ExpandableListView;
.source "LifeModeListView.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnGroupClickListener;
.implements Landroid/widget/ExpandableListView$OnGroupExpandListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/ExpandableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/view/LifeModeListView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 23
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020108

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/view/LifeModeListView;->setChildDivider(Landroid/graphics/drawable/Drawable;)V

    .line 24
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020173

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/view/LifeModeListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 25
    invoke-virtual {p0, p0}, Lcom/sec/android/GeoLookout/view/LifeModeListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 26
    invoke-virtual {p0, p0}, Lcom/sec/android/GeoLookout/view/LifeModeListView;->setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V

    .line 28
    :cond_0
    return-void
.end method


# virtual methods
.method public onGroupClick(Landroid/widget/ExpandableListView;Landroid/view/View;IJ)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 41
    sget-object v0, Lami;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamj;

    invoke-virtual {v0, p3}, Lamj;->b(I)Lamq;

    move-result-object v0

    .line 43
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lamq;->a()I

    move-result v0

    if-ge v0, v2, :cond_1

    :cond_0
    move v0, v2

    .line 46
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onGroupExpand(I)V
    .locals 2

    .prologue
    .line 52
    const-string v0, "onGroupExpand"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 53
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/view/LifeModeListView;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 54
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/view/LifeModeListView;->isGroupExpanded(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    if-eq p1, v0, :cond_0

    .line 56
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/view/LifeModeListView;->collapseGroup(I)Z

    .line 53
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_1
    return-void
.end method

.method public setAdapter(Landroid/widget/ExpandableListAdapter;)V
    .locals 0

    .prologue
    .line 32
    if-nez p1, :cond_0

    .line 37
    :goto_0
    return-void

    .line 36
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    goto :goto_0
.end method
