.class public Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "DisasterMultiPointActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/SearchView$OnQueryTextListener;
.implements Lwm;
.implements Lwn;


# static fields
.field public static final a:I = 0x100

.field private static final u:F = 0.5f

.field private static final v:F = 1.0f


# instance fields
.field private b:Landroid/widget/SearchView;

.field private c:Landroid/widget/FrameLayout;

.field private d:Lajt;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/widget/ListView;

.field private g:Laju;

.field private h:Lvs;

.field private i:Z

.field private j:Lcom/google/android/gms/maps/model/LatLng;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:I

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Z

.field private r:Z

.field private s:Laah;

.field private t:Lamf;

.field private w:Landroid/os/Handler;

.field private x:Landroid/os/Handler;

.field private y:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 325
    new-instance v0, Lajp;

    invoke-direct {v0, p0}, Lajp;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->w:Landroid/os/Handler;

    .line 341
    new-instance v0, Lajq;

    invoke-direct {v0, p0}, Lajq;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->x:Landroid/os/Handler;

    .line 614
    new-instance v0, Lajr;

    invoke-direct {v0, p0}, Lajr;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->y:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Lajt;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->d:Lajt;

    return-object v0
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Lajt;)Lajt;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->d:Lajt;

    return-object p1
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->k:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->e:Ljava/util/List;

    return-object p1
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 249
    const v0, 0x7f0e003f

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->c:Landroid/widget/FrameLayout;

    .line 250
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 251
    const v1, 0x7f030009

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 252
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 254
    new-instance v0, Laju;

    invoke-direct {v0, p0, v2}, Laju;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Lajo;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->g:Laju;

    .line 255
    const v0, 0x7f0e0040

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->f:Landroid/widget/ListView;

    .line 256
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->g:Laju;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->f:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 259
    const v0, 0x7f0e003c

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    const v0, 0x7f0e003b

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 261
    return-void
.end method

.method private a(I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 162
    iput p1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->n:I

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->n:I

    invoke-static {v0, v1}, Lalz;->c(Landroid/content/Context;I)Lamf;

    move-result-object v0

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->n:I

    invoke-static {v1, v2}, Lalz;->d(Landroid/content/Context;I)[Ljava/lang/String;

    move-result-object v1

    .line 168
    const/4 v2, 0x0

    aget-object v2, v1, v2

    iput-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->o:Ljava/lang/String;

    .line 169
    aget-object v1, v1, v6

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->p:Ljava/lang/String;

    .line 171
    if-eqz v0, :cond_1

    .line 172
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v0, Lamf;->a:D

    iget-wide v4, v0, Lamf;->b:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->j:Lcom/google/android/gms/maps/model/LatLng;

    .line 173
    const v0, 0x4019999a    # 2.4f

    .line 174
    const-string v1, "04"

    invoke-static {}, Laof;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 176
    const/high16 v0, 0x40c00000    # 6.0f

    .line 178
    :cond_0
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->j:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1, v0}, Lvr;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lvq;

    move-result-object v0

    .line 179
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->h:Lvs;

    new-instance v2, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v2}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->j:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    const v3, 0x7f02001c

    invoke-static {v3}, Lzx;->a(I)Lzw;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lzw;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    const/high16 v3, 0x3f000000    # 0.5f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->n:I

    invoke-static {v3, v4}, Lalz;->e(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    invoke-virtual {v1, v2}, Lvs;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Laah;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->s:Laah;

    .line 183
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->h:Lvs;

    invoke-virtual {v1, v0}, Lvs;->a(Lvq;)V

    .line 184
    iput-boolean v6, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->i:Z

    .line 186
    :cond_1
    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Z)Z
    .locals 0

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->r:Z

    return p1
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Laju;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->g:Laju;

    return-object v0
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->o:Ljava/lang/String;

    return-object p1
.end method

.method private b()V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->h:Lvs;

    if-nez v0, :cond_0

    .line 266
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0e003a

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/MapFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapFragment;->c()Lvs;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->h:Lvs;

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->h:Lvs;

    invoke-virtual {v0, p0}, Lvs;->a(Lwn;)V

    .line 269
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->h:Lvs;

    invoke-virtual {v0, p0}, Lvs;->a(Lwm;)V

    .line 271
    return-void
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Z)Z
    .locals 0

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->i:Z

    return p1
.end method

.method public static synthetic c(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->k:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic c(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->p:Ljava/lang/String;

    return-object p1
.end method

.method private c()V
    .locals 2

    .prologue
    .line 274
    const v0, 0x7f0e003e

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->b:Landroid/widget/SearchView;

    .line 276
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->onActionViewExpanded()V

    .line 277
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->b:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->b:Landroid/widget/SearchView;

    new-instance v1, Lajo;

    invoke-direct {v1, p0}, Lajo;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnCloseListener(Landroid/widget/SearchView$OnCloseListener;)V

    .line 288
    return-void
.end method

.method public static synthetic c(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Z)Z
    .locals 0

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->q:Z

    return p1
.end method

.method public static synthetic d(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Landroid/widget/SearchView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->b:Landroid/widget/SearchView;

    return-object v0
.end method

.method public static synthetic e(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Laah;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->s:Laah;

    return-object v0
.end method

.method public static synthetic f(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Ljava/util/List;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->e:Ljava/util/List;

    return-object v0
.end method

.method public static synthetic g(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->f:Landroid/widget/ListView;

    return-object v0
.end method

.method public static synthetic h(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->r:Z

    return v0
.end method

.method public static synthetic i(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->q:Z

    return v0
.end method

.method public static synthetic j(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->y:Landroid/content/BroadcastReceiver;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 584
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->h:Lvs;

    invoke-virtual {v0}, Lvs;->f()V

    .line 585
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->h:Lvs;

    new-instance v1, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    const v2, 0x7f02001c

    invoke-static {v2}, Lzx;->a(I)Lzw;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lzw;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    const/high16 v2, 0x3f000000    # 0.5f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, Lvs;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Laah;

    .line 589
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 590
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->k:Ljava/lang/String;

    .line 593
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->y:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.sec.android.geolookout.querylocation"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 595
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v0

    .line 596
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 598
    :try_start_0
    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 599
    iget-wide v4, p1, Lcom/google/android/gms/maps/model/LatLng;->c:D

    invoke-virtual {v0, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 600
    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v4, v2, v3, v0, v1}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iput-object v4, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->j:Lcom/google/android/gms/maps/model/LatLng;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 606
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onMapClick : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->j:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->j:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v1, Lcom/google/android/gms/maps/model/LatLng;->c:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 607
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->j:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-object v4, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->j:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v4, Lcom/google/android/gms/maps/model/LatLng;->c:D

    invoke-virtual/range {v0 .. v6}, Lalu;->a(Landroid/content/Context;DDZ)V

    .line 609
    iput-boolean v6, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->i:Z

    .line 610
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->q:Z

    .line 611
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->invalidateOptionsMenu()V

    .line 612
    return-void

    .line 601
    :catch_0
    move-exception v0

    .line 602
    const-string v0, "Location parse error"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 603
    iput-object p1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->j:Lcom/google/android/gms/maps/model/LatLng;

    goto :goto_0
.end method

.method public a(Laah;)Z
    .locals 1

    .prologue
    .line 672
    const/4 v0, 0x1

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 317
    const/16 v0, 0x100

    if-ne p1, v0, :cond_1

    .line 318
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    const/16 v0, -0xf

    if-ne p2, v0, :cond_1

    .line 319
    :cond_0
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->setResult(ILandroid/content/Intent;)V

    .line 320
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->finish()V

    .line 323
    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 654
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 656
    packed-switch v0, :pswitch_data_0

    .line 668
    :cond_0
    :goto_0
    return-void

    .line 658
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->t:Lamf;

    if-eqz v0, :cond_0

    .line 659
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->t:Lamf;

    iget-wide v2, v1, Lamf;->a:D

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->t:Lamf;

    iget-wide v4, v1, Lamf;->b:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-static {v0}, Lvr;->a(Lcom/google/android/gms/maps/model/LatLng;)Lvq;

    move-result-object v0

    .line 662
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->h:Lvs;

    invoke-virtual {v1, v0}, Lvs;->a(Lvq;)V

    goto :goto_0

    .line 656
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e003c
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v4, -0x1

    const/4 v2, 0x0

    .line 112
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 116
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lxa;->a(Landroid/content/Context;)V
    :try_end_0
    .catch Lcd; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :goto_0
    const v0, 0x7f03000a

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->setContentView(I)V

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "multi_point_edit_id"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 122
    if-eq v3, v4, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->m:Z

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MULTIPOINT Edit ID : "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v4, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->n:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 125
    const-string v4, ""

    iput-object v4, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->o:Ljava/lang/String;

    .line 126
    const-string v4, ""

    iput-object v4, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->p:Ljava/lang/String;

    .line 127
    if-eqz v0, :cond_0

    .line 128
    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080018

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-direct {v4, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 129
    iget-boolean v4, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->m:Z

    if-eqz v4, :cond_3

    .line 130
    const v4, 0x7f0a00a1

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setTitle(I)V

    .line 134
    :goto_2
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 135
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 136
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 138
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 140
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->a()V

    .line 141
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->c()V

    .line 143
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->b()V

    .line 144
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lanb;->c(Landroid/content/Context;)Lamf;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->t:Lamf;

    .line 145
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->m:Z

    if-eqz v0, :cond_4

    .line 146
    invoke-direct {p0, v3}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->a(I)V

    .line 159
    :cond_1
    :goto_3
    return-void

    .line 117
    :catch_0
    move-exception v0

    .line 118
    invoke-virtual {v0}, Lcd;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 122
    goto/16 :goto_1

    .line 132
    :cond_3
    const v4, 0x7f0a0494

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setTitle(I)V

    goto :goto_2

    .line 147
    :cond_4
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->t:Lamf;

    if-eqz v0, :cond_1

    .line 148
    const v0, 0x4019999a    # 2.4f

    .line 149
    const-string v1, "04"

    invoke-static {}, Laof;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 151
    const/high16 v0, 0x40c00000    # 6.0f

    .line 153
    :cond_5
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->t:Lamf;

    iget-wide v2, v2, Lamf;->a:D

    iget-object v4, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->t:Lamf;

    iget-wide v4, v4, Lamf;->b:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-static {v1, v0}, Lvr;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lvq;

    move-result-object v0

    .line 156
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->h:Lvs;

    invoke-virtual {v1, v0}, Lvs;->a(Lvq;)V

    goto :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0d0004

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 205
    const v0, 0x7f0e011e

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 207
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->h:Lvs;

    invoke-virtual {v0}, Lvs;->f()V

    .line 193
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->y:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    :goto_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 198
    return-void

    .line 194
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 529
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 530
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->b:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 532
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->e:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 533
    invoke-virtual {v0}, Landroid/location/Address;->getLatitude()D

    move-result-wide v2

    .line 534
    invoke-virtual {v0}, Landroid/location/Address;->getLongitude()D

    move-result-wide v4

    .line 535
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->j:Lcom/google/android/gms/maps/model/LatLng;

    .line 536
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->j:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v1}, Lvr;->a(Lcom/google/android/gms/maps/model/LatLng;)Lvq;

    move-result-object v1

    .line 537
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->h:Lvs;

    invoke-virtual {v2}, Lvs;->f()V

    .line 538
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->h:Lvs;

    invoke-virtual {v2, v1}, Lvs;->a(Lvq;)V

    .line 540
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->h:Lvs;

    new-instance v2, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v2}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->j:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    const v3, 0x7f02001c

    invoke-static {v3}, Lzx;->a(I)Lzw;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lzw;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    const/high16 v3, 0x3f000000    # 0.5f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    invoke-virtual {v1, v2}, Lvs;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Laah;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->s:Laah;

    .line 544
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 545
    invoke-virtual {v0}, Landroid/location/Address;->getMaxAddressLineIndex()I

    move-result v3

    move v1, v6

    .line 546
    :goto_0
    if-gt v1, v3, :cond_1

    .line 547
    invoke-virtual {v0, v1}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v4

    .line 548
    if-eqz v1, :cond_0

    .line 552
    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 554
    :cond_0
    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 546
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 557
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->k:Ljava/lang/String;

    .line 558
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->s:Laah;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Laah;->a(Ljava/lang/String;)V

    .line 561
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->k:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 562
    iput-boolean v7, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->r:Z

    .line 563
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->g:Laju;

    invoke-virtual {v0}, Laju;->notifyDataSetChanged()V

    .line 571
    :goto_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->y:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.sec.android.geolookout.querylocation"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 573
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->j:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-object v4, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->j:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v4, Lcom/google/android/gms/maps/model/LatLng;->c:D

    invoke-virtual/range {v0 .. v6}, Lalu;->a(Landroid/content/Context;DDZ)V

    .line 575
    iput-boolean v6, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->i:Z

    .line 578
    iput-boolean v6, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->q:Z

    .line 579
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->invalidateOptionsMenu()V

    .line 580
    return-void

    .line 566
    :cond_2
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->b:Landroid/widget/SearchView;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v6}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 568
    iput-boolean v7, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->r:Z

    goto :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 222
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 245
    :goto_0
    return v5

    .line 224
    :sswitch_0
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 225
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->b:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 226
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->x:Landroid/os/Handler;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 230
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 231
    const/4 v1, 0x2

    new-array v1, v1, [D

    .line 232
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->j:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->b:D

    aput-wide v2, v1, v4

    .line 233
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->j:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->c:D

    aput-wide v2, v1, v5

    .line 234
    const-string v2, "latlong"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[D)Landroid/content/Intent;

    .line 235
    const-string v1, "locationName"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 236
    iget-boolean v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->m:Z

    if-eqz v1, :cond_0

    .line 237
    const-string v1, "multi_point_edit_id"

    iget v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->n:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 239
    :cond_0
    const-string v1, "query_zone"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 240
    const-string v1, "query_county"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 241
    const/16 v1, 0x100

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 222
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0e011e -> :sswitch_1
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const v1, 0x7f0e011e

    const/4 v2, 0x1

    .line 212
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->i:Z

    if-eqz v0, :cond_0

    .line 213
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 217
    :goto_0
    return v2

    .line 215
    :cond_0
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 293
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->w:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 294
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 295
    iput v4, v0, Landroid/os/Message;->what:I

    .line 297
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->r:Z

    .line 298
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->w:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 299
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->w:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 302
    :cond_0
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->w:Landroid/os/Handler;

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 303
    iput-object p1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->l:Ljava/lang/String;

    .line 306
    return v4
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x0

    return v0
.end method
