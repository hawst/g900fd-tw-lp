.class public Lcom/sec/android/GeoLookout/widget/SafetyCareWidget;
.super Landroid/appwidget/AppWidgetProvider;
.source "SafetyCareWidget.java"


# static fields
.field private static a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/GeoLookout/widget/SafetyCareWidget;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 177
    sget-boolean v0, Lcom/sec/android/GeoLookout/widget/SafetyCareWidget;->a:Z

    return v0
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Landroid/content/ComponentName;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/sec/android/GeoLookout/widget/SafetyCareWidget;

    invoke-direct {v0, p1, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public onDeleted(Landroid/content/Context;[I)V
    .locals 1

    .prologue
    .line 67
    const-string v0, "onDeleted()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 68
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 69
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 73
    const-string v0, "onDisabled()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 74
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 75
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/GeoLookout/widget/SafetyCareWidget;->a:Z

    .line 76
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 47
    const-string v0, "onEnabled()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 48
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 49
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/GeoLookout/widget/SafetyCareWidget;->a:Z

    .line 51
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    invoke-static {}, Laqs;->a()Laqs;

    move-result-object v0

    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Laqs;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 55
    :cond_0
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 80
    const-string v2, "onReceive()"

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 82
    if-nez p2, :cond_1

    .line 83
    const-string v0, "intent is null!!!"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 88
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "intent = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lalj;->b(Ljava/lang/String;)V

    .line 90
    invoke-virtual {p0, p1}, Lcom/sec/android/GeoLookout/widget/SafetyCareWidget;->a(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v3

    .line 91
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v4

    .line 92
    invoke-virtual {v4, v3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v5

    .line 94
    if-eqz v5, :cond_2

    array-length v6, v5

    if-gtz v6, :cond_3

    .line 95
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No instance for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 100
    :cond_3
    const-string v3, "com.sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "android.appwidget.action.APPWIDGET_UPDATE_OPTIONS"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 101
    :cond_4
    if-eqz v5, :cond_5

    array-length v0, v5

    if-eqz v0, :cond_5

    .line 102
    invoke-static {}, Laqs;->a()Laqs;

    move-result-object v0

    invoke-virtual {v0, p1, v4}, Laqs;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 171
    :cond_5
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 104
    :cond_6
    const-string v3, "android.appwidget.action.NEW_PUSH_RECIEVE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 105
    sput v1, Laqs;->a:I

    .line 109
    const-string v0, "uid"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ACTION_NEW_PUS_RECIEVE : mCurrentPage : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 111
    const-string v1, "pref_geonews_current_page"

    invoke-static {p1, v1, v0}, Lall;->b(Landroid/content/Context;Ljava/lang/String;I)V

    .line 113
    invoke-static {}, Laqs;->a()Laqs;

    move-result-object v1

    invoke-virtual {v1, p1, v4, v0}, Laqs;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V

    goto :goto_1

    .line 114
    :cond_7
    const-string v3, "com.sec.android.GeoLookout.SETTING_CHANGE_FINISH"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "com.sec.android.GeoLookout.SETTING_CHANGE_START"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "android.intent.action.TIME_SET"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 120
    :cond_8
    invoke-static {}, Laqs;->a()Laqs;

    move-result-object v0

    invoke-virtual {v0, p1, v4}, Laqs;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    goto :goto_1

    .line 121
    :cond_9
    const-string v3, "com.sec.android.ACTION_REMOVE_ANIMATION"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 122
    const-string v0, "error"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 123
    invoke-static {}, Laqs;->a()Laqs;

    move-result-object v1

    invoke-virtual {v1, p1, v5, v0}, Laqs;->a(Landroid/content/Context;[II)V

    goto/16 :goto_1

    .line 125
    :cond_a
    const-string v3, "com.sec.android.GeoLookout.widget.ACTION_DISASTER_REFRESH_DATA"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 126
    invoke-static {}, Laqs;->a()Laqs;

    move-result-object v0

    invoke-virtual {v0, p1, v4, v5}, Laqs;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto/16 :goto_1

    .line 127
    :cond_b
    const-string v3, "com.sec.android.GeoLookout.widget.ACTION_DISASTER_REFRESH_REMOVE_ANIMATION"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 128
    invoke-static {}, Laqs;->a()Laqs;

    move-result-object v0

    invoke-virtual {v0, p1, v4, v5}, Laqs;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto/16 :goto_1

    .line 130
    :cond_c
    const-string v3, "com.sec.android.safetywidget.ACTION_SHOW_TOAST_FOR_REGISTER"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 131
    const-string v2, "isRegister"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 132
    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v3, 0x103012b

    invoke-direct {v1, p1, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 133
    if-eqz v2, :cond_d

    .line 134
    const v2, 0x7f0a0016

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_2
    move-object p1, v1

    .line 139
    goto/16 :goto_1

    .line 136
    :cond_d
    const v2, 0x7f0a0008

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 139
    :cond_e
    const-string v3, "com.sec.android.GeoLookout.widget.ACTION_GEONEWS_SHOW_NEXT"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 140
    const-string v2, "ACTION_GEONEWS_SHOW_NEXT"

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 141
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v2

    invoke-virtual {v2}, Lage;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 144
    const-string v2, "pref_geonews_current_page"

    invoke-static {p1, v2, v1}, Lall;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v3

    .line 146
    const-string v2, "from_transparent"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 147
    invoke-virtual {p0, p1}, Lcom/sec/android/GeoLookout/widget/SafetyCareWidget;->a(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v5

    .line 149
    if-eqz v2, :cond_f

    const-class v6, Lcom/sec/android/GeoLookout/widget/SafetyCareTransparentWidget;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_10

    :cond_f
    if-nez v2, :cond_11

    const-class v6, Lcom/sec/android/GeoLookout/widget/SafetyCareWidget;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 151
    :cond_10
    :goto_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MULTIPOINT next from transparent "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " widget name : "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " update : "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 153
    if-eqz v0, :cond_5

    .line 154
    invoke-static {p1}, Lall;->y(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v5

    move v2, v1

    .line 155
    :goto_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_14

    .line 156
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v3, :cond_13

    .line 157
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v2, v0, :cond_12

    .line 158
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 165
    :goto_5
    const-string v1, "pref_geonews_current_page"

    invoke-static {p1, v1, v0}, Lall;->b(Landroid/content/Context;Ljava/lang/String;I)V

    .line 166
    invoke-static {}, Laqs;->a()Laqs;

    move-result-object v1

    invoke-virtual {v1, p1, v4, v0}, Laqs;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V

    goto/16 :goto_1

    :cond_11
    move v0, v1

    .line 149
    goto :goto_3

    .line 160
    :cond_12
    add-int/lit8 v0, v2, 0x1

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_5

    .line 155
    :cond_13
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_14
    move v0, v3

    goto :goto_5
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 2

    .prologue
    .line 59
    const-string v0, "onUpdate()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 60
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 61
    invoke-static {}, Laqs;->a()Laqs;

    move-result-object v0

    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Laqs;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 63
    return-void
.end method
