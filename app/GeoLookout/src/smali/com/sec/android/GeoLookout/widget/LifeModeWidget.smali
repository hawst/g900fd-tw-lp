.class public Lcom/sec/android/GeoLookout/widget/LifeModeWidget;
.super Landroid/appwidget/AppWidgetProvider;
.source "LifeModeWidget.java"


# static fields
.field private static volatile j:Lcom/sec/android/GeoLookout/widget/LifeModeWidget;

.field private static k:Z

.field private static l:I

.field private static n:[I

.field private static o:[I


# instance fields
.field a:Landroid/database/Cursor;

.field b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lamx;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/lang/String;

.field d:[[I

.field e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lamj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->j:Lcom/sec/android/GeoLookout/widget/LifeModeWidget;

    .line 58
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->k:Z

    .line 64
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->n:[I

    .line 73
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->o:[I

    return-void

    .line 64
    nop

    :array_0
    .array-data 4
        0x260
        0x261
        0x25e
        0x25f
        0x25a
        0x25c
        0x263
        0x25b
        0x25d
        0x262
        0x259
        0x260
    .end array-data

    .line 73
    :array_1
    .array-data 4
        0x1f5
        0x1f6
        0x1f7
        0x1f8
        0x1f9
        0x1fa
        0x1fb
        0x1fc
    .end array-data
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x4

    .line 111
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    .line 80
    new-array v0, v3, [[I

    new-array v1, v3, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v2

    new-array v1, v3, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v4

    new-array v1, v3, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v5

    new-array v1, v3, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    .line 99
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    .line 103
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    .line 105
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->h:Ljava/util/HashMap;

    .line 107
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->i:Ljava/util/HashMap;

    .line 113
    sput v2, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->l:I

    .line 114
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_0

    .line 115
    const/16 v0, 0x1f5

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 116
    const/16 v0, 0x1f6

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 117
    const/16 v0, 0x1f7

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 118
    const/16 v0, 0x1f8

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 119
    const/16 v0, 0x1f9

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 120
    const/16 v0, 0x1fa

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 121
    const/16 v0, 0x1fb

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 122
    const/16 v0, 0x1fc

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 131
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->h:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020015

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->h:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020016

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->h:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020018

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->h:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020017

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->h:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020017

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->h:Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020017

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x1f5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201ab

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x1f6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201a4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x1f7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201b5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x1f8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201b3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x1f9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x1fa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201ad

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x1fb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201b0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x1fc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201b1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x1f5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a029e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x1f6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a029b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x1f7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02a5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x1f8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x1f9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a029a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x1fa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a0295

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x1fb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02a1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x1fc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02a4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    :goto_0
    return-void

    .line 160
    :cond_0
    const/16 v0, 0x260

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 161
    const/16 v0, 0x261

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 162
    const/16 v0, 0x25e

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 163
    const/16 v0, 0x25f

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 164
    const/16 v0, 0x25a

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 165
    const/16 v0, 0x25c

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 166
    const/16 v0, 0x263

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 167
    const/16 v0, 0x25b

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 168
    const/16 v0, 0x25d

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 169
    const/16 v0, 0x262

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 170
    const/16 v0, 0x259

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(I)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->h:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201a1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->h:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201b6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->h:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201b7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->h:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201b8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->h:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201b8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->h:Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201b8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x260

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201a9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x261

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201a5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x25e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201a7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x25f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201a2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x25a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201a6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x25c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201af

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x263

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201aa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x25b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201ae

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x25d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201b4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x262

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201a8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x259

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201b2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x260

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a029d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x261

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a0294

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x25e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a0298

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x25f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a0299

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x25a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a0297

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x25c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02a1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x263

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a029e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x25b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a029f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x25d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x262

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02a2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x259

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 80
    nop

    :array_0
    .array-data 4
        0x7f0e00d4
        0x7f0e00d3
        0x7f0e00d2
        0x7f0e00d1
    .end array-data

    :array_1
    .array-data 4
        0x7f0e00d8
        0x7f0e00d7
        0x7f0e00d6
        0x7f0e00d5
    .end array-data

    :array_2
    .array-data 4
        0x7f0e00dc
        0x7f0e00db
        0x7f0e00da
        0x7f0e00d9
    .end array-data

    :array_3
    .array-data 4
        0x7f0e00e0
        0x7f0e00df
        0x7f0e00de
        0x7f0e00dd
    .end array-data
.end method

.method private a(II)I
    .locals 1

    .prologue
    .line 774
    mul-int/lit8 v0, p1, 0xa

    add-int/2addr v0, p2

    return v0
.end method

.method public static a()Lcom/sec/android/GeoLookout/widget/LifeModeWidget;
    .locals 2

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->j:Lcom/sec/android/GeoLookout/widget/LifeModeWidget;

    if-nez v0, :cond_1

    .line 46
    const-class v1, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;

    monitor-enter v1

    .line 47
    :try_start_0
    sget-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->j:Lcom/sec/android/GeoLookout/widget/LifeModeWidget;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;

    invoke-direct {v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;-><init>()V

    sput-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->j:Lcom/sec/android/GeoLookout/widget/LifeModeWidget;

    .line 50
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    :cond_1
    sget-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->j:Lcom/sec/android/GeoLookout/widget/LifeModeWidget;

    return-object v0

    .line 50
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(I)V
    .locals 6

    .prologue
    const v2, 0x7f0a00e1

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 215
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_3

    .line 216
    const/16 v0, 0x1f5

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1f7

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1f6

    if-ne p1, v0, :cond_1

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v3}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02ac

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v4}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02ad

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v5}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02ae

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02af

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    :goto_0
    return-void

    .line 224
    :cond_1
    const/16 v0, 0x1f8

    if-ne p1, v0, :cond_2

    .line 225
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v3}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02b0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v4}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02b1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v5}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02b2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 230
    :cond_2
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v3}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02b3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v4}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02b4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v5}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02b5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 238
    :cond_3
    const/16 v0, 0x260

    if-ne p1, v0, :cond_4

    .line 239
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v3}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00e8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v4}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00e9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v5}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00ea

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00eb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00ec

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 246
    :cond_4
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v3}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00e2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v4}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00e3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v5}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00e4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00e5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->e:Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00e6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 794
    const-string v0, "LifeModeWidget > checkRefreshResult"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 795
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 796
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lage;->c(Z)V

    .line 797
    if-ne p2, v2, :cond_1

    .line 798
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 805
    :cond_0
    :goto_0
    return-void

    .line 800
    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 801
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a04ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 9

    .prologue
    const v8, 0x7f0e00e6

    const v7, 0x7f0a0020

    const/4 v6, 0x1

    const v5, 0x7f0e00e9

    const/4 v4, 0x0

    .line 492
    const v0, 0x7f030029

    .line 493
    sget-boolean v1, Laky;->b:Z

    if-eqz v1, :cond_0

    .line 494
    const v0, 0x7f03002a

    .line 496
    :cond_0
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 501
    const/16 v0, 0x8

    invoke-virtual {v1, v8, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 502
    sget-boolean v0, Lakv;->bQ:Z

    if-eqz v0, :cond_1

    invoke-static {p1}, Lakw;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 503
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a00b7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 505
    invoke-virtual {v1, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 542
    :goto_0
    sput-boolean v6, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->k:Z

    .line 543
    invoke-virtual {p2, p3, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 544
    return-void

    .line 506
    :cond_1
    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p1}, Lane;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 509
    :cond_2
    sget-boolean v0, Lakv;->bQ:Z

    if-eqz v0, :cond_3

    .line 510
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a02bc

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 514
    :goto_1
    invoke-virtual {v1, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 516
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_WIGET_APP_START"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 517
    invoke-static {p1, v4, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 518
    const v2, 0x7f0e00e7

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_0

    .line 512
    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a02bd

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 519
    :cond_4
    invoke-static {p1}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 520
    const-string v0, ""

    .line 521
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 522
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0008

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 529
    :goto_2
    invoke-virtual {v1, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 530
    invoke-virtual {v1, v8, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0

    .line 525
    :cond_5
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0560

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 532
    :cond_6
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0564

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 534
    invoke-virtual {v1, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 536
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_TAG_TO_START"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 537
    invoke-static {p1, v4, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 538
    const v2, 0x7f0e00e7

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 5

    .prologue
    const v4, 0x7f0e00e2

    const/4 v1, 0x0

    .line 611
    sget v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->l:I

    add-int/lit8 v0, v0, 0x1

    .line 614
    sget-boolean v2, Laky;->b:Z

    if-eqz v2, :cond_2

    .line 615
    const/4 v2, 0x1

    if-le v0, v2, :cond_1

    .line 617
    const v0, 0x7f020011

    invoke-virtual {p2, v4, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    move v0, v1

    .line 627
    :cond_0
    :goto_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_SHOW_NEXT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 628
    const-string v3, "nextPage"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 632
    const/high16 v0, 0x8000000

    invoke-static {p1, v1, v2, v0}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 634
    invoke-virtual {p2, v4, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 636
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_WIGET_APP_START"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 637
    invoke-static {p1, v1, v0, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 639
    const v1, 0x7f0e00e8

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 641
    return-void

    .line 619
    :cond_1
    const v2, 0x7f020012

    invoke-virtual {p2, v4, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    .line 622
    :cond_2
    const/4 v2, 0x2

    if-le v0, v2, :cond_0

    move v0, v1

    .line 623
    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/widget/RemoteViews;I)V
    .locals 3

    .prologue
    .line 647
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_WIGET_APP_START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 648
    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 649
    invoke-virtual {p2, p3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 651
    return-void
.end method

.method private a(Landroid/content/Context;Z)V
    .locals 10

    .prologue
    const v9, 0x7f0e00e5

    const v8, 0x7f0e00e4

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 454
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 455
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v2

    .line 457
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_1

    .line 458
    invoke-static {p1}, Lall;->E(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 459
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f030028

    invoke-direct {v0, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 466
    :goto_0
    if-eqz p2, :cond_2

    .line 467
    invoke-virtual {v0, v8, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 468
    invoke-virtual {v0, v9, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 478
    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 480
    return-void

    .line 461
    :cond_0
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f030027

    invoke-direct {v0, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    goto :goto_0

    .line 464
    :cond_1
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f030026

    invoke-direct {v0, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    goto :goto_0

    .line 470
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 471
    invoke-static {p1, v4, v5}, Lall;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .line 473
    invoke-static {p1, v4, v5}, Lall;->a(Landroid/content/Context;J)V

    .line 474
    const v4, 0x7f0e00e3

    invoke-virtual {v0, v4, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 475
    invoke-virtual {v0, v8, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 476
    invoke-virtual {v0, v9, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1
.end method

.method private b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 10

    .prologue
    const v9, 0x7f0e00e4

    const/16 v8, 0x8

    const v7, 0x7f0e00e5

    const/4 v6, 0x0

    .line 548
    const v0, 0x7f030026

    .line 549
    sget-boolean v1, Laky;->b:Z

    if-eqz v1, :cond_0

    .line 550
    invoke-static {p1}, Lall;->E(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 551
    const v0, 0x7f030028

    .line 557
    :cond_0
    :goto_0
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 559
    invoke-static {p1}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 560
    const v0, 0x7f0e00e6

    invoke-virtual {v1, v0, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 563
    :cond_1
    invoke-static {p1}, Lamu;->f(Landroid/content/Context;)Lamj;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->m:Lamj;

    .line 565
    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->b(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 568
    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 571
    invoke-static {p1}, Lall;->c(Landroid/content/Context;)J

    move-result-wide v2

    .line 572
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_4

    .line 573
    const-string v0, ""

    .line 578
    :goto_1
    const v2, 0x7f0e00e3

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 580
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 581
    invoke-virtual {v1, v9, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 582
    invoke-virtual {v1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 595
    :goto_2
    invoke-virtual {p2, p3, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 596
    sget-boolean v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->k:Z

    if-eqz v0, :cond_2

    .line 597
    const-string v0, "mFirstEnable is true, send Refresh Life Data"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 598
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 599
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 600
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 601
    sput-boolean v6, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->k:Z

    .line 603
    :cond_2
    return-void

    .line 553
    :cond_3
    const v0, 0x7f030027

    goto :goto_0

    .line 575
    :cond_4
    invoke-static {p1, v2, v3}, Lall;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 576
    invoke-static {p1, v2, v3}, Lall;->a(Landroid/content/Context;J)V

    goto :goto_1

    .line 585
    :cond_5
    invoke-virtual {v1, v9, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 586
    invoke-virtual {v1, v7, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 588
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 589
    invoke-static {p1, v6, v0, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 590
    invoke-virtual {v1, v7, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_2
.end method

.method private b(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 654
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LifeWidget > applyIndexData mCurrentPage : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 656
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_3

    .line 657
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->m:Lamj;

    invoke-virtual {v0}, Lamj;->b()[I

    move-result-object v0

    sput-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->o:[I

    .line 659
    sget-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->o:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->o:[I

    array-length v0, v0

    if-nez v0, :cond_1

    .line 660
    :cond_0
    const-string v0, "LifeWidget > applyIndexData cannot find current zone to order"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 661
    invoke-static {p1}, Lamu;->g(Landroid/content/Context;)[I

    move-result-object v0

    sput-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->o:[I

    :cond_1
    :goto_0
    move v2, v3

    .line 672
    :goto_1
    if-ge v2, v10, :cond_c

    .line 677
    sget v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->l:I

    mul-int/lit8 v0, v0, 0x4

    add-int/2addr v0, v2

    .line 679
    sget-boolean v1, Laky;->b:Z

    if-eqz v1, :cond_5

    .line 680
    sget-object v1, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->o:[I

    array-length v1, v1

    if-lt v0, v1, :cond_6

    .line 681
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v0, v0, v2

    aget v0, v0, v8

    invoke-virtual {p2, v0, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 672
    :cond_2
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 664
    :cond_3
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->m:Lamj;

    invoke-virtual {v0}, Lamj;->b()[I

    move-result-object v0

    sput-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->n:[I

    .line 666
    sget-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->n:[I

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->n:[I

    array-length v0, v0

    if-nez v0, :cond_1

    .line 667
    :cond_4
    const-string v0, "LifeWidget > applyIndexData cannot find current zone to order"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 668
    invoke-static {p1}, Lamu;->g(Landroid/content/Context;)[I

    move-result-object v0

    sput-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->n:[I

    goto :goto_0

    .line 685
    :cond_5
    sget-object v1, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->n:[I

    array-length v1, v1

    if-lt v0, v1, :cond_6

    .line 686
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v0, v0, v2

    aget v0, v0, v8

    invoke-virtual {p2, v0, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_2

    .line 691
    :cond_6
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v1, v1, v2

    aget v1, v1, v3

    invoke-virtual {p2, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 692
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v1, v1, v2

    aget v1, v1, v7

    invoke-virtual {p2, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 693
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v1, v1, v2

    aget v1, v1, v9

    invoke-virtual {p2, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 694
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v1, v1, v2

    aget v1, v1, v8

    invoke-virtual {p2, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 697
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->m:Lamj;

    invoke-virtual {v1}, Lamj;->f()I

    move-result v1

    .line 699
    if-le v1, v0, :cond_9

    .line 700
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->m:Lamj;

    invoke-virtual {v1, v0}, Lamj;->b(I)Lamq;

    move-result-object v0

    move-object v1, v0

    .line 707
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LifeWidget > current page = "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v4, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->l:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " & display index = "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " & type = "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 709
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v0, v0, v2

    aget v0, v0, v3

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(Landroid/content/Context;Landroid/widget/RemoteViews;I)V

    .line 710
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v0, v0, v2

    aget v0, v0, v7

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(Landroid/content/Context;Landroid/widget/RemoteViews;I)V

    .line 711
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v0, v0, v2

    aget v0, v0, v9

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(Landroid/content/Context;Landroid/widget/RemoteViews;I)V

    .line 712
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v0, v0, v2

    aget v0, v0, v8

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(Landroid/content/Context;Landroid/widget/RemoteViews;I)V

    .line 715
    if-eqz v1, :cond_b

    .line 716
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_a

    .line 717
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v1}, Lamq;->j()I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 722
    :goto_4
    iget-object v4, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v4, v4, v2

    aget v4, v4, v3

    invoke-virtual {p2, v4, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 724
    sget-boolean v0, Laky;->b:Z

    if-nez v0, :cond_7

    .line 725
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->h:Ljava/util/HashMap;

    invoke-virtual {v1}, Lamq;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 726
    iget-object v4, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v4, v4, v2

    aget v4, v4, v3

    const-string v5, "setBackgroundResource"

    invoke-virtual {p2, v4, v5, v0}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 730
    :cond_7
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->f:Ljava/util/HashMap;

    invoke-virtual {v1}, Lamq;->o()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 731
    iget-object v4, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v4, v4, v2

    aget v4, v4, v7

    invoke-virtual {p2, v4, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 733
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_8

    .line 734
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v0, v0, v2

    aget v4, v0, v7

    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->h:Ljava/util/HashMap;

    invoke-virtual {v1}, Lamq;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v4, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 738
    :cond_8
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Lamq;->o()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 739
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v1, v1, v2

    aget v1, v1, v9

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 702
    :cond_9
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "LifeModeWidget getNotUsedType "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->m:Lamj;

    sub-int v6, v0, v1

    invoke-virtual {v5, v6}, Lamj;->e(I)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lalj;->b(Ljava/lang/String;)V

    .line 704
    sget-object v4, Lami;->b:Landroid/util/SparseArray;

    iget-object v5, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->m:Lamj;

    sub-int/2addr v0, v1

    invoke-virtual {v5, v0}, Lamj;->e(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamq;

    move-object v1, v0

    goto/16 :goto_3

    .line 719
    :cond_a
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v1}, Lamq;->i()I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 741
    :cond_b
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a02a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 742
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v1, v1, v2

    aget v1, v1, v3

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 744
    sget-boolean v0, Laky;->b:Z

    if-nez v0, :cond_2

    .line 745
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v0, v0, v2

    aget v0, v0, v3

    const-string v1, "setBackgroundResource"

    const v4, 0x7f0201a1

    invoke-virtual {p2, v0, v1, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 746
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->d:[[I

    aget-object v0, v0, v2

    aget v1, v0, v7

    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->h:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_2

    .line 750
    :cond_c
    return-void
.end method

.method private b(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 753
    .line 754
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 755
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 757
    array-length v0, v0

    if-lez v0, :cond_0

    .line 758
    const/4 v0, 0x1

    .line 762
    :goto_0
    return v0

    .line 760
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 767
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 768
    const-string v1, "page"

    sget v2, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->l:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 769
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 771
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 778
    const-string v0, "refreshData"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 780
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "location_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 782
    if-nez v0, :cond_0

    .line 783
    const-string v0, "location mode is LOCATION_MODE_OFF! request allow location!"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 784
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    const/16 v1, 0xa

    const/16 v2, 0x3e9

    invoke-virtual {v0, p1, v1, v2}, Lanf;->a(Landroid/content/Context;II)V

    .line 791
    :goto_0
    return-void

    .line 788
    :cond_0
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0, v3}, Lage;->c(Z)V

    .line 789
    invoke-direct {p0, p1, v3}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V
    .locals 3

    .prologue
    .line 483
    const-string v0, "updateWidget()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 484
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    invoke-virtual {p2, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 487
    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 488
    return-void
.end method

.method public onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 260
    invoke-super {p0, p1, p2, p3, p4}, Landroid/appwidget/AppWidgetProvider;->onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V

    .line 261
    return-void
.end method

.method public onDeleted(Landroid/content/Context;[I)V
    .locals 0

    .prologue
    .line 266
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 267
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 272
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 273
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 278
    const-string v0, "LifeWidget> onEnabled"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 279
    invoke-static {p1}, Lall;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->c:Ljava/lang/String;

    .line 280
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 281
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 282
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 283
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 286
    :cond_0
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 287
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const v2, 0x7f0a0011

    const/4 v6, -0x1

    const v5, 0x10008000

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 293
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LifeWidget> onReceive : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 294
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 295
    const-string v0, "LifeWidget> onReceive : intent.getAction() is null"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 402
    :goto_0
    return-void

    .line 299
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 301
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 302
    const-string v0, "LifeWidget> onReceive : no instance"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 306
    :cond_1
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 307
    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p1}, Lane;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 308
    :cond_2
    sget-object v0, Lamu;->c:Landroid/net/Uri;

    if-eqz v0, :cond_3

    .line 309
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 311
    if-eqz v0, :cond_4

    .line 312
    sget-object v1, Lamu;->c:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 318
    :cond_3
    :goto_1
    const v0, 0x7f0a02bd

    invoke-static {p1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 314
    :cond_4
    const-string v0, "resolver is null !!"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 320
    :cond_5
    invoke-static {p1}, Lami;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 321
    invoke-static {p1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 324
    :cond_6
    invoke-virtual {p0, p1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(Landroid/content/Context;)V

    .line 401
    :cond_7
    :goto_2
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 326
    :cond_8
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA_RESULT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 327
    const-string v0, "error"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 328
    invoke-direct {p0, p1, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(Landroid/content/Context;I)V

    .line 329
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 330
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 331
    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_2

    .line 332
    :cond_9
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_SHOW_NEXT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 334
    const-string v0, "nextPage"

    invoke-virtual {p2, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->l:I

    .line 335
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LifeWidget> onReceive : mCurrentPage nextPage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 337
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->c(Landroid/content/Context;)V

    goto :goto_2

    .line 339
    :cond_a
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_VIEW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 340
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 341
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 343
    const-string v2, "page"

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->l:I

    .line 344
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LifeWidget> onReceive : mCurrentPage ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->l:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 346
    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto/16 :goto_2

    .line 347
    :cond_b
    const-string v1, "com.sec.android.GeoLookout.SETTING_CHANGE_START"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "com.sec.android.GeoLookout.SETTING_CHANGE_FINISH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "android.appwidget.action.APPWIDGET_UPDATE_OPTIONS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 351
    :cond_c
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 352
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 354
    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto/16 :goto_2

    .line 355
    :cond_d
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_TAG_TO_START"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 357
    invoke-static {p1}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 358
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 359
    const v0, 0x7f0a0008

    invoke-static {p1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    .line 361
    :cond_e
    const v0, 0x7f0a0016

    invoke-static {p1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    .line 365
    :cond_f
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "safety_care_disaster_user_agree"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_10

    .line 367
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 368
    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 369
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 370
    :cond_10
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "safetycare_earthquake"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_7

    .line 372
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 373
    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 374
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 382
    :cond_11
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_WIGET_APP_START"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 383
    invoke-static {p1}, Lami;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 384
    invoke-static {p1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    .line 386
    :cond_12
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;

    invoke-direct {v1, p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 388
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_13

    .line 389
    const-string v0, "indexType"

    sget-object v2, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->o:[I

    aget v2, v2, v3

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 394
    :goto_3
    const-string v2, "indexType"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 396
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 397
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 391
    :cond_13
    const-string v0, "indexType"

    sget-object v2, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->n:[I

    aget v2, v2, v3

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    goto :goto_3
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 407
    const-string v0, "LifeWidget> onUpdate"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 414
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 415
    const-string v0, "LifeWidget> onUpdate : FUNTION TURNED OFF"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 416
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 450
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 451
    return-void

    .line 417
    :cond_0
    invoke-static {p1}, Lami;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 418
    sget-object v0, Lamu;->c:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 419
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 421
    if-eqz v0, :cond_2

    .line 422
    sget-object v1, Lamu;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 428
    :cond_1
    :goto_1
    sget-boolean v0, Laky;->b:Z

    if-nez v0, :cond_3

    .line 429
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0

    .line 424
    :cond_2
    const-string v0, "resolver is null !!"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 431
    :cond_3
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0

    .line 433
    :cond_4
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "safety_care_disaster_user_agree"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_5

    .line 438
    const-string v0, "LifeWidget> onUpdate : USER AGREE CHECK FAIL"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 439
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0

    .line 442
    :cond_5
    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {p1}, Lane;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 443
    :cond_6
    const-string v0, "LifeWidget> onUpdate : ROAMING"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 444
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0

    .line 447
    :cond_7
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0
.end method
