.class public Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "DisasterMultiPointSaveActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final f:I = 0x1110


# instance fields
.field private a:Landroid/widget/EditText;

.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/LinearLayout;

.field private d:Landroid/net/Uri;

.field private e:[D

.field private g:Z

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Landroid/app/ProgressDialog;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:[D

.field private s:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 278
    new-instance v0, Lajx;

    invoke-direct {v0, p0}, Lajx;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->s:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->h:I

    return v0
.end method

.method private a(Landroid/database/Cursor;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 482
    .line 484
    :try_start_0
    const-string v0, "photo_thumb_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 486
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 492
    :goto_0
    return-object v0

    .line 487
    :catch_0
    move-exception v0

    .line 488
    const-string v0, "No contact image"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 489
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02001b

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->m:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 182
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->m:Landroid/app/ProgressDialog;

    .line 183
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->m:Landroid/app/ProgressDialog;

    const v1, 0x7f0a0004

    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->m:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->m:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 187
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->m:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 189
    :cond_1
    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 192
    .line 195
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lalz;->b:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "SLOT=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget v7, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->h:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 200
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    const/4 v0, 0x2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 202
    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->q:Ljava/lang/String;

    .line 203
    if-eqz v0, :cond_0

    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 204
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a(Landroid/net/Uri;)V

    .line 207
    :cond_0
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->l:Ljava/lang/String;

    .line 208
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->n:Ljava/lang/String;

    .line 209
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 212
    const/4 v0, 0x7

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->p:Ljava/lang/String;

    .line 213
    const/4 v0, 0x6

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->o:Ljava/lang/String;

    .line 214
    const/4 v0, 0x2

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->r:[D

    .line 215
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->r:[D

    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    aput-wide v4, v0, v2

    .line 216
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->r:[D

    const/4 v2, 0x1

    const/4 v3, 0x4

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    aput-wide v4, v0, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 221
    :cond_1
    if-eqz v1, :cond_2

    .line 222
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 225
    :cond_2
    return-void

    .line 221
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_0
    if-eqz v1, :cond_3

    .line 222
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 221
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->g:Z

    return v0
.end method

.method public static synthetic c(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->o:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 402
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 403
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    .line 404
    if-lez v1, :cond_0

    .line 405
    const-string v1, "TITLE"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    :cond_0
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->d:Landroid/net/Uri;

    if-nez v1, :cond_5

    .line 408
    const-string v1, "CONTACT_URI"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :goto_0
    const-string v1, "LATITUDE"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->e:[D

    const/4 v3, 0x0

    aget-wide v2, v2, v3

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 413
    const-string v1, "LONGITUDE"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->e:[D

    const/4 v3, 0x1

    aget-wide v2, v2, v3

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 414
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lalz;->c(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->h:I

    .line 415
    const-string v1, "SLOT"

    iget v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->h:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 416
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->i:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->i:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_2

    .line 417
    :cond_1
    const-string v1, "NoLocationId"

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->i:Ljava/lang/String;

    .line 419
    :cond_2
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->j:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->j:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_4

    .line 420
    :cond_3
    const-string v1, "NoLocationId"

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->j:Ljava/lang/String;

    .line 422
    :cond_4
    const-string v1, "ZONE"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    const-string v1, "COUNTY"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lalz;->a(Landroid/content/Context;Landroid/content/ContentValues;)V

    .line 425
    return-void

    .line 410
    :cond_5
    const-string v1, "CONTACT_URI"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->d:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static synthetic d(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->p:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 428
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 429
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    .line 430
    if-lez v1, :cond_0

    .line 431
    const-string v1, "TITLE"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    :cond_0
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->d:Landroid/net/Uri;

    if-nez v1, :cond_1

    .line 434
    const-string v1, "CONTACT_URI"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    :goto_0
    const-string v1, "LATITUDE"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->e:[D

    const/4 v3, 0x0

    aget-wide v2, v2, v3

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 439
    const-string v1, "LONGITUDE"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->e:[D

    const/4 v3, 0x1

    aget-wide v2, v2, v3

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 440
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->i:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 441
    const-string v1, "ZONE"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    :goto_1
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->j:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->j:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 447
    const-string v1, "COUNTY"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->h:I

    invoke-static {v1, v2, v0}, Lalz;->a(Landroid/content/Context;ILandroid/content/ContentValues;)V

    .line 454
    return-void

    .line 436
    :cond_1
    const-string v1, "CONTACT_URI"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->d:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 443
    :cond_2
    const-string v1, "NoLocationId"

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->i:Ljava/lang/String;

    .line 444
    const-string v1, "ZONE"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 449
    :cond_3
    const-string v1, "NoLocationId"

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->j:Ljava/lang/String;

    .line 450
    const-string v1, "COUNTY"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static synthetic e(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->i:Ljava/lang/String;

    return-object v0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 457
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 458
    const-string v1, "TITLE"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->q:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 460
    const-string v1, "CONTACT_URI"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    :goto_0
    const-string v1, "LATITUDE"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->r:[D

    const/4 v3, 0x0

    aget-wide v2, v2, v3

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 465
    const-string v1, "LONGITUDE"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->r:[D

    const/4 v3, 0x1

    aget-wide v2, v2, v3

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 466
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->o:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->o:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 467
    const-string v1, "ZONE"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    :goto_1
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->p:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->p:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 473
    const-string v1, "COUNTY"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->h:I

    invoke-static {v1, v2, v0}, Lalz;->a(Landroid/content/Context;ILandroid/content/ContentValues;)V

    .line 479
    return-void

    .line 462
    :cond_0
    const-string v1, "CONTACT_URI"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 469
    :cond_1
    const-string v1, "NoLocationId"

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->o:Ljava/lang/String;

    .line 470
    const-string v1, "ZONE"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 475
    :cond_2
    const-string v1, "NoLocationId"

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->p:Ljava/lang/String;

    .line 476
    const-string v1, "COUNTY"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public static synthetic f(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->j:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic g(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->e()V

    return-void
.end method

.method public static synthetic h(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->m:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method public static synthetic i(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->s:Landroid/content/BroadcastReceiver;

    return-object v0
.end method


# virtual methods
.method a(Landroid/net/Uri;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 354
    .line 355
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0e0047

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/TextView;

    .line 356
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->c:Landroid/widget/LinearLayout;

    const v1, 0x7f0e0046

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/ImageView;

    .line 358
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 359
    if-eqz v0, :cond_1

    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 360
    const-string v1, "display_name"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->k:Ljava/lang/String;

    .line 362
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->k:Ljava/lang/String;

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 364
    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a(Landroid/database/Cursor;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 371
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->c:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 372
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->b:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 373
    iput-object p1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->d:Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 384
    :goto_0
    if-eqz v0, :cond_0

    .line 385
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 388
    :cond_0
    :goto_1
    return-void

    .line 375
    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->c:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 376
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->b:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 377
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->d:Landroid/net/Uri;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 379
    :catch_0
    move-exception v1

    .line 380
    :goto_2
    :try_start_3
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->c:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 381
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->b:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 382
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->d:Landroid/net/Uri;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 384
    if-eqz v0, :cond_0

    .line 385
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 384
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v8, :cond_2

    .line 385
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 384
    :catchall_1
    move-exception v1

    move-object v8, v0

    move-object v0, v1

    goto :goto_3

    .line 379
    :catch_1
    move-exception v0

    move-object v0, v8

    goto :goto_2
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 393
    const/16 v0, 0x1110

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 395
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 397
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a(Landroid/net/Uri;)V

    .line 399
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 328
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 351
    :goto_0
    :pswitch_0
    return-void

    .line 332
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 334
    const/16 v1, 0x1110

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 337
    :pswitch_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->d:Landroid/net/Uri;

    .line 338
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->c:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 339
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->b:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 328
    :pswitch_data_0
    .packed-switch 0x7f0e0043
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 90
    const v0, 0x7f03000b

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->setContentView(I)V

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "multi_point_edit_id"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->h:I

    .line 93
    iget v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->h:I

    if-eq v0, v4, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->g:Z

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 97
    if-eqz v0, :cond_0

    .line 98
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080018

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 99
    iget-boolean v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->g:Z

    if-eqz v3, :cond_2

    .line 100
    const v3, 0x7f0a00a1

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setTitle(I)V

    .line 104
    :goto_1
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 105
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 106
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 109
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MPS Edit ID : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->h:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "latlong"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getDoubleArrayExtra(Ljava/lang/String;)[D

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->e:[D

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "query_zone"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->i:Ljava/lang/String;

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "query_county"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->j:Ljava/lang/String;

    .line 116
    const v0, 0x7f0e0042

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a:Landroid/widget/EditText;

    .line 120
    const v0, 0x7f0e0043

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->b:Landroid/widget/Button;

    .line 121
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->b:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    const v0, 0x7f0e0044

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->c:Landroid/widget/LinearLayout;

    .line 124
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    const v0, 0x7f0e0045

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    const v0, 0x7f0e0047

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->g:Z

    if-eqz v0, :cond_3

    .line 128
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->b()V

    .line 136
    :goto_2
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a:Landroid/widget/EditText;

    new-instance v3, Lajv;

    invoke-direct {v3, p0}, Lajv;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a:Landroid/widget/EditText;

    new-array v1, v1, [Landroid/text/InputFilter;

    new-instance v3, Lajw;

    const/16 v4, 0x41

    invoke-direct {v3, p0, v4}, Lajw;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;I)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 178
    return-void

    :cond_1
    move v0, v2

    .line 93
    goto/16 :goto_0

    .line 102
    :cond_2
    const v3, 0x7f0a0494

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setTitle(I)V

    goto/16 :goto_1

    .line 130
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "locationName"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->l:Ljava/lang/String;

    .line 132
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->l:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    goto :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0d0003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 231
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 247
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 273
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 249
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->finish()V

    goto :goto_0

    .line 252
    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/sec/android/GeoLookout/DisasterService;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 253
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->g:Z

    if-eqz v0, :cond_0

    .line 254
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->d()V

    .line 259
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a()V

    .line 261
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 263
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 265
    const-string v0, "com.sec.android.geolookout.updatelocation"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 266
    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 267
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.geolookout.resultlocation"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 268
    const-string v1, "com.sec.android.geolookout.resultrefresh"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 269
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->s:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0

    .line 256
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->c()V

    goto :goto_1

    .line 247
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0e011d -> :sswitch_1
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const v1, 0x7f0e011d

    const/4 v2, 0x1

    .line 236
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 237
    if-lez v0, :cond_0

    .line 238
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 242
    :goto_0
    return v2

    .line 240
    :cond_0
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method
