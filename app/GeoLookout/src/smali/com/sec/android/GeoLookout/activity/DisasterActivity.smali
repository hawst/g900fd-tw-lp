.class public Lcom/sec/android/GeoLookout/activity/DisasterActivity;
.super Landroid/preference/PreferenceActivity;
.source "DisasterActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 16
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 18
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x1020002

    new-instance v2, Lakf;

    invoke-direct {v2}, Lakf;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 20
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 21
    sget-boolean v1, Lakv;->bQ:Z

    if-eqz v1, :cond_0

    .line 22
    const v1, 0x7f0a0029

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 26
    :goto_0
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 27
    return-void

    .line 24
    :cond_0
    const v1, 0x7f0a00a9

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 31
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 36
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 33
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterActivity;->finish()V

    .line 34
    const/4 v0, 0x1

    goto :goto_0

    .line 31
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 47
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 48
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 43
    return-void
.end method
