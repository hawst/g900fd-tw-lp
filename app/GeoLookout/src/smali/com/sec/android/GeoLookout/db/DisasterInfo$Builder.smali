.class public Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
.super Ljava/lang/Object;
.source "DisasterInfo.java"


# instance fields
.field private mActionStatus:S

.field private mChnDetailsCity:Ljava/lang/String;

.field private mChnDetailsEqType:Ljava/lang/String;

.field private mChnDetailsProvince:Ljava/lang/String;

.field private mChnDetailsStationName:Ljava/lang/String;

.field private mChnSignificanceLevel:I

.field private mCity:Ljava/lang/String;

.field private mCountry:Ljava/lang/String;

.field private mDV:Ljava/lang/String;

.field private mDataType:S

.field private mDetailDirectionCardinal:Ljava/lang/String;

.field private mDetailDirectionDegrees:I

.field private mDetailForecastHour:I

.field private mDetailPressureMB:I

.field private mDetailStormSpeed:I

.field private mDetailWindGust:I

.field private mDetailWindMax:I

.field private mDetailsMagnitude:D

.field private mDetailsSpeed:Ljava/lang/String;

.field private mDid:Ljava/lang/String;

.field private mDisasterSource:Ljava/lang/String;

.field private mDistance:I

.field private mEndTime:J

.field private mHTHPId:Ljava/lang/String;

.field private mHeadline:Ljava/lang/String;

.field private mId:I

.field private mJpnDetailsDepth:Ljava/lang/String;

.field private mJpnDetailsLevel:Ljava/lang/String;

.field private mJpnDetailsZoneCity:Ljava/lang/String;

.field private mJpnDetailsZoneCountry:Ljava/lang/String;

.field private mLink:Ljava/lang/String;

.field private mLinkedBEDid:Ljava/lang/String;

.field private mLocation:Lamf;

.field private mLocationID:Ljava/lang/String;

.field private mLocationType:I

.field private mMsgId:Ljava/lang/String;

.field private mMultiPointUid:I

.field private mNotiStatus:S

.field private mPolling:S

.field private mPolygonJSON:Ljava/lang/String;

.field private mPolygonList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lamf;",
            ">;"
        }
    .end annotation
.end field

.field private mReceivedTime:J

.field private mSignificance:I

.field private mStartTime:J

.field private mTipId:I

.field private mTipLinkId:I

.field private mType:Ljava/lang/String;

.field private mTypeId:I

.field private mWarningIcon:I

.field private mWatchIcon:I


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    iput v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mId:I

    .line 157
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDid:Ljava/lang/String;

    .line 158
    iput-wide v6, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mStartTime:J

    .line 159
    iput-wide v6, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mEndTime:J

    .line 160
    iput-wide v6, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mReceivedTime:J

    .line 161
    iput-object v4, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocation:Lamf;

    .line 162
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mHeadline:Ljava/lang/String;

    .line 163
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mType:Ljava/lang/String;

    .line 164
    iput v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mTypeId:I

    .line 165
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLink:Ljava/lang/String;

    .line 166
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailsMagnitude:D

    .line 168
    iput v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mSignificance:I

    .line 169
    iput-short v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mActionStatus:S

    .line 170
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocationID:Ljava/lang/String;

    .line 171
    iput v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocationType:I

    .line 172
    iput-object v4, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolygonList:Ljava/util/ArrayList;

    .line 173
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolygonJSON:Ljava/lang/String;

    .line 174
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mCity:Ljava/lang/String;

    .line 175
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mCountry:Ljava/lang/String;

    .line 176
    iput v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDistance:I

    .line 177
    iput-short v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolling:S

    .line 178
    const-string v0, "01"

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDV:Ljava/lang/String;

    .line 179
    iput-short v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mNotiStatus:S

    .line 180
    iput v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailPressureMB:I

    .line 189
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsProvince:Ljava/lang/String;

    .line 190
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsCity:Ljava/lang/String;

    .line 191
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsStationName:Ljava/lang/String;

    .line 192
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsEqType:Ljava/lang/String;

    .line 193
    iput v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnSignificanceLevel:I

    .line 198
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsDepth:Ljava/lang/String;

    .line 199
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsLevel:Ljava/lang/String;

    .line 200
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsZoneCountry:Ljava/lang/String;

    .line 201
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsZoneCity:Ljava/lang/String;

    .line 203
    iput-short v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDataType:S

    .line 204
    iput v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailWindGust:I

    .line 205
    iput-object v4, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailDirectionCardinal:Ljava/lang/String;

    .line 206
    iput v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailDirectionDegrees:I

    .line 207
    iput v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailWindMax:I

    .line 208
    iput v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailStormSpeed:I

    .line 209
    iput v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailForecastHour:I

    .line 219
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    iput v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mId:I

    .line 157
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDid:Ljava/lang/String;

    .line 158
    iput-wide v6, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mStartTime:J

    .line 159
    iput-wide v6, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mEndTime:J

    .line 160
    iput-wide v6, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mReceivedTime:J

    .line 161
    iput-object v4, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocation:Lamf;

    .line 162
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mHeadline:Ljava/lang/String;

    .line 163
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mType:Ljava/lang/String;

    .line 164
    iput v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mTypeId:I

    .line 165
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLink:Ljava/lang/String;

    .line 166
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailsMagnitude:D

    .line 168
    iput v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mSignificance:I

    .line 169
    iput-short v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mActionStatus:S

    .line 170
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocationID:Ljava/lang/String;

    .line 171
    iput v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocationType:I

    .line 172
    iput-object v4, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolygonList:Ljava/util/ArrayList;

    .line 173
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolygonJSON:Ljava/lang/String;

    .line 174
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mCity:Ljava/lang/String;

    .line 175
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mCountry:Ljava/lang/String;

    .line 176
    iput v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDistance:I

    .line 177
    iput-short v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolling:S

    .line 178
    const-string v0, "01"

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDV:Ljava/lang/String;

    .line 179
    iput-short v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mNotiStatus:S

    .line 180
    iput v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailPressureMB:I

    .line 189
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsProvince:Ljava/lang/String;

    .line 190
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsCity:Ljava/lang/String;

    .line 191
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsStationName:Ljava/lang/String;

    .line 192
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsEqType:Ljava/lang/String;

    .line 193
    iput v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnSignificanceLevel:I

    .line 198
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsDepth:Ljava/lang/String;

    .line 199
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsLevel:Ljava/lang/String;

    .line 200
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsZoneCountry:Ljava/lang/String;

    .line 201
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsZoneCity:Ljava/lang/String;

    .line 203
    iput-short v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDataType:S

    .line 204
    iput v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailWindGust:I

    .line 205
    iput-object v4, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailDirectionCardinal:Ljava/lang/String;

    .line 206
    iput v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailDirectionDegrees:I

    .line 207
    iput v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailWindMax:I

    .line 208
    iput v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailStormSpeed:I

    .line 209
    iput v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailForecastHour:I

    .line 227
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mId:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$5000(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mId:I

    .line 228
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDid:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$5100(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDid:Ljava/lang/String;

    .line 229
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mStartTime:J
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$5200(Lcom/sec/android/GeoLookout/db/DisasterInfo;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mStartTime:J

    .line 230
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mEndTime:J
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$5300(Lcom/sec/android/GeoLookout/db/DisasterInfo;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mEndTime:J

    .line 231
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mReceivedTime:J
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$5400(Lcom/sec/android/GeoLookout/db/DisasterInfo;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mReceivedTime:J

    .line 232
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLocation:Lamf;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$5500(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Lamf;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocation:Lamf;

    .line 233
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mHeadline:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$5600(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mHeadline:Ljava/lang/String;

    .line 234
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mType:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$5700(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mType:Ljava/lang/String;

    .line 235
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mTypeId:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$5800(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mTypeId:I

    .line 236
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDV:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$5900(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDV:Ljava/lang/String;

    .line 237
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLink:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$6000(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLink:Ljava/lang/String;

    .line 238
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailsMagnitude:D
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$6100(Lcom/sec/android/GeoLookout/db/DisasterInfo;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailsMagnitude:D

    .line 240
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mSignificance:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$6200(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mSignificance:I

    .line 241
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLocationID:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$6300(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocationID:Ljava/lang/String;

    .line 242
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLocationType:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$6400(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocationType:I

    .line 243
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mPolygonList:Ljava/util/ArrayList;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$6500(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolygonList:Ljava/util/ArrayList;

    .line 244
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mPolygonJSON:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$6600(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolygonJSON:Ljava/lang/String;

    .line 245
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mCity:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$6700(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mCity:Ljava/lang/String;

    .line 246
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mCountry:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$6800(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mCountry:Ljava/lang/String;

    .line 247
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDistance:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$6900(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDistance:I

    .line 248
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mPolling:S
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$7000(Lcom/sec/android/GeoLookout/db/DisasterInfo;)S

    move-result v0

    iput-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolling:S

    .line 249
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mActionStatus:S
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$7100(Lcom/sec/android/GeoLookout/db/DisasterInfo;)S

    move-result v0

    iput-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mActionStatus:S

    .line 250
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mNotiStatus:S
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$7200(Lcom/sec/android/GeoLookout/db/DisasterInfo;)S

    move-result v0

    iput-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mNotiStatus:S

    .line 251
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailsSpeed:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$7300(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailsSpeed:Ljava/lang/String;

    .line 252
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDV:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$5900(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDV:Ljava/lang/String;

    .line 253
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLinkedBeDid:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$7400(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLinkedBEDid:Ljava/lang/String;

    .line 254
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mHTHPId:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$7500(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mHTHPId:Ljava/lang/String;

    .line 255
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mMsgId:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$7600(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mMsgId:Ljava/lang/String;

    .line 256
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDisasterSource:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$7700(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDisasterSource:Ljava/lang/String;

    .line 261
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsProvince:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$7800(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsProvince:Ljava/lang/String;

    .line 262
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsCity:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$7900(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsCity:Ljava/lang/String;

    .line 263
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsStationName:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$8000(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsStationName:Ljava/lang/String;

    .line 264
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsEqType:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$8100(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsEqType:Ljava/lang/String;

    .line 265
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnSignificanceLevel:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$8200(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnSignificanceLevel:I

    .line 270
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsDepth:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$8300(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsDepth:Ljava/lang/String;

    .line 271
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsLevel:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$8400(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsLevel:Ljava/lang/String;

    .line 272
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsZoneCountry:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$8500(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsZoneCountry:Ljava/lang/String;

    .line 273
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsZoneCity:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$8600(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsZoneCity:Ljava/lang/String;

    .line 275
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDataType:S
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$8700(Lcom/sec/android/GeoLookout/db/DisasterInfo;)S

    move-result v0

    iput-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDataType:S

    .line 276
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailWindGust:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$8800(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailWindGust:I

    .line 277
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailDirectionCardinal:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$8900(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailDirectionCardinal:Ljava/lang/String;

    .line 278
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailDirectionDegrees:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$9000(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailDirectionDegrees:I

    .line 279
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailWindMax:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$9100(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailWindMax:I

    .line 280
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailStormSpeed:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$9200(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailStormSpeed:I

    .line 281
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailForecastHour:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$9300(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailForecastHour:I

    .line 282
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailsSpeed:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$7300(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailsSpeed:Ljava/lang/String;

    .line 284
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mWatchIcon:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$9400(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mWatchIcon:I

    .line 285
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mWarningIcon:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$9500(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mWarningIcon:I

    .line 286
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mTipId:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$9600(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mTipId:I

    .line 287
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mTipLinkId:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$9700(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mTipLinkId:I

    .line 289
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo;->mMultiPointUid:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->access$9800(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mMultiPointUid:I

    .line 290
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mId:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLink:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)D
    .locals 2

    .prologue
    .line 154
    iget-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailsMagnitude:D

    return-wide v0
.end method

.method static synthetic access$1200(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mSignificance:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)S
    .locals 1

    .prologue
    .line 154
    iget-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mActionStatus:S

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocationID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocationType:I

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolygonList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolygonJSON:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mCity:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mCountry:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)J
    .locals 2

    .prologue
    .line 154
    iget-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mStartTime:J

    return-wide v0
.end method

.method static synthetic access$2000(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDistance:I

    return v0
.end method

.method static synthetic access$2100(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)S
    .locals 1

    .prologue
    .line 154
    iget-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolling:S

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)S
    .locals 1

    .prologue
    .line 154
    iget-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mNotiStatus:S

    return v0
.end method

.method static synthetic access$2300(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailPressureMB:I

    return v0
.end method

.method static synthetic access$2400(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailsSpeed:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLinkedBEDid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mHTHPId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mMsgId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDisasterSource:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsProvince:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)J
    .locals 2

    .prologue
    .line 154
    iget-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mEndTime:J

    return-wide v0
.end method

.method static synthetic access$3000(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsCity:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsStationName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsEqType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnSignificanceLevel:I

    return v0
.end method

.method static synthetic access$3400(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsDepth:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsLevel:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsZoneCountry:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsZoneCity:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)S
    .locals 1

    .prologue
    .line 154
    iget-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDataType:S

    return v0
.end method

.method static synthetic access$3900(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailWindGust:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)J
    .locals 2

    .prologue
    .line 154
    iget-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mReceivedTime:J

    return-wide v0
.end method

.method static synthetic access$4000(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailDirectionCardinal:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailDirectionDegrees:I

    return v0
.end method

.method static synthetic access$4200(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailWindMax:I

    return v0
.end method

.method static synthetic access$4300(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailStormSpeed:I

    return v0
.end method

.method static synthetic access$4400(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailForecastHour:I

    return v0
.end method

.method static synthetic access$4500(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mWatchIcon:I

    return v0
.end method

.method static synthetic access$4600(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mWarningIcon:I

    return v0
.end method

.method static synthetic access$4700(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mTipId:I

    return v0
.end method

.method static synthetic access$4800(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mTipLinkId:I

    return v0
.end method

.method static synthetic access$4900(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mMultiPointUid:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Lamf;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocation:Lamf;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mHeadline:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mTypeId:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDV:Ljava/lang/String;

    return-object v0
.end method

.method private initDataByType(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 298
    sget-object v0, Lakx;->eN:Landroid/util/SparseIntArray;

    const v1, 0x7f0a0501

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mTipId:I

    .line 299
    sget-object v0, Lakx;->eP:Landroid/util/SparseIntArray;

    const v1, 0x7f020027

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mWarningIcon:I

    .line 300
    sget-object v0, Lakx;->eO:Landroid/util/SparseIntArray;

    const v1, 0x7f020038

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mWatchIcon:I

    .line 301
    invoke-static {p1, p2}, Lall;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mTipLinkId:I

    .line 302
    return-void
.end method


# virtual methods
.method public build()Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDV:Ljava/lang/String;

    iget v1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mTypeId:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->initDataByType(Ljava/lang/String;I)V

    .line 294
    new-instance v0, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;-><init>(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;Lamb;)V

    return-object v0
.end method

.method public setActionStatus(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 370
    iput-short p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mActionStatus:S

    .line 371
    return-object p0
.end method

.method public setChnDetailsCity(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 458
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsCity:Ljava/lang/String;

    .line 459
    return-object p0
.end method

.method public setChnDetailsEqType(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 468
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsEqType:Ljava/lang/String;

    .line 469
    return-object p0
.end method

.method public setChnDetailsProvince(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 453
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsProvince:Ljava/lang/String;

    .line 454
    return-object p0
.end method

.method public setChnDetailsStationName(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 463
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsStationName:Ljava/lang/String;

    .line 464
    return-object p0
.end method

.method public setChnSignificanceLevel(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 473
    iput p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnSignificanceLevel:I

    .line 474
    return-object p0
.end method

.method public setCity(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mCity:Ljava/lang/String;

    .line 391
    return-object p0
.end method

.method public setCountry(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 395
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mCountry:Ljava/lang/String;

    .line 396
    return-object p0
.end method

.method public setDV(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 435
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDV:Ljava/lang/String;

    .line 436
    return-object p0
.end method

.method public setDataType(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 501
    iput-short p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDataType:S

    .line 502
    return-object p0
.end method

.method public setDetailDirectionCardinal(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 511
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailDirectionCardinal:Ljava/lang/String;

    .line 512
    return-object p0
.end method

.method public setDetailDirectionDegrees(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 516
    iput p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailDirectionDegrees:I

    .line 517
    return-object p0
.end method

.method public setDetailForecastHour(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 531
    iput p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailForecastHour:I

    .line 532
    return-object p0
.end method

.method public setDetailPressureMB(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 415
    iput p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailPressureMB:I

    .line 416
    return-object p0
.end method

.method public setDetailSpeed(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 420
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailsSpeed:Ljava/lang/String;

    .line 421
    return-object p0
.end method

.method public setDetailStormSpeed(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 526
    iput p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailStormSpeed:I

    .line 527
    return-object p0
.end method

.method public setDetailWindGust(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 506
    iput p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailWindGust:I

    .line 507
    return-object p0
.end method

.method public setDetailWindMax(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 521
    iput p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailWindMax:I

    .line 522
    return-object p0
.end method

.method public setDetailsMagnitude(D)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 1

    .prologue
    .line 360
    iput-wide p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailsMagnitude:D

    .line 361
    return-object p0
.end method

.method public setDid(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 310
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDid:Ljava/lang/String;

    .line 311
    return-object p0
.end method

.method public setDisasterSource(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 445
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDisasterSource:Ljava/lang/String;

    .line 446
    return-object p0
.end method

.method public setDistance(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 400
    iput p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDistance:I

    .line 401
    return-object p0
.end method

.method public setEndTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 1

    .prologue
    .line 320
    iput-wide p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mEndTime:J

    .line 321
    return-object p0
.end method

.method public setHTHPId(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 430
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mHTHPId:Ljava/lang/String;

    .line 431
    return-object p0
.end method

.method public setHeadline(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 340
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mHeadline:Ljava/lang/String;

    .line 341
    return-object p0
.end method

.method public setId(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 305
    iput p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mId:I

    .line 306
    return-object p0
.end method

.method public setJpnDetailsDepth(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 481
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsDepth:Ljava/lang/String;

    .line 482
    return-object p0
.end method

.method public setJpnDetailsLevel(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 486
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsLevel:Ljava/lang/String;

    .line 487
    return-object p0
.end method

.method public setJpnDetailsZoneCity(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 496
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsZoneCity:Ljava/lang/String;

    .line 497
    return-object p0
.end method

.method public setJpnDetailsZoneCountry(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 491
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsZoneCountry:Ljava/lang/String;

    .line 492
    return-object p0
.end method

.method public setLink(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 355
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLink:Ljava/lang/String;

    .line 356
    return-object p0
.end method

.method public setLinkedBEDId(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 425
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLinkedBEDid:Ljava/lang/String;

    .line 426
    return-object p0
.end method

.method public setLocation(Lamf;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 330
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocation:Lamf;

    .line 331
    return-object p0
.end method

.method public setLocationID(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 375
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocationID:Ljava/lang/String;

    .line 376
    return-object p0
.end method

.method public setLocationType(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 335
    iput p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocationType:I

    .line 336
    return-object p0
.end method

.method public setMsgId(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 440
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mMsgId:Ljava/lang/String;

    .line 441
    return-object p0
.end method

.method public setMultiPointUid(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 556
    iput p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mMultiPointUid:I

    .line 557
    return-object p0
.end method

.method public setNotiStatus(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 410
    iput-short p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mNotiStatus:S

    .line 411
    return-object p0
.end method

.method public setPolling(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 405
    iput-short p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolling:S

    .line 406
    return-object p0
.end method

.method public setPolygonJSON(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 385
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolygonJSON:Ljava/lang/String;

    .line 386
    return-object p0
.end method

.method public setPolygonList(Ljava/util/ArrayList;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lamf;",
            ">;)",
            "Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;"
        }
    .end annotation

    .prologue
    .line 380
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolygonList:Ljava/util/ArrayList;

    .line 381
    return-object p0
.end method

.method public setReceivedTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 1

    .prologue
    .line 325
    iput-wide p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mReceivedTime:J

    .line 326
    return-object p0
.end method

.method public setSignificance(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 365
    iput p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mSignificance:I

    .line 366
    return-object p0
.end method

.method public setStartTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 1

    .prologue
    .line 315
    iput-wide p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mStartTime:J

    .line 316
    return-object p0
.end method

.method public setTipId(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 546
    iput p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mTipId:I

    .line 547
    return-object p0
.end method

.method public setTipLinkId(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 551
    iput p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mTipLinkId:I

    .line 552
    return-object p0
.end method

.method public setType(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mType:Ljava/lang/String;

    .line 346
    return-object p0
.end method

.method public setTypeId(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 350
    iput p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mTypeId:I

    .line 351
    return-object p0
.end method

.method public setWarningIcon(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 541
    iput p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mWarningIcon:I

    .line 542
    return-object p0
.end method

.method public setWatchIcon(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 0

    .prologue
    .line 536
    iput p1, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mWatchIcon:I

    .line 537
    return-object p0
.end method
