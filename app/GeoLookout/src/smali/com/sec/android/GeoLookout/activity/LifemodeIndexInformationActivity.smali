.class public Lcom/sec/android/GeoLookout/activity/LifemodeIndexInformationActivity;
.super Landroid/app/Activity;
.source "LifemodeIndexInformationActivity.java"


# instance fields
.field a:Landroid/app/ActionBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 16
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 18
    const-string v0, "onCreate"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 19
    const v0, 0x7f03001f

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/LifemodeIndexInformationActivity;->setContentView(I)V

    .line 21
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/LifemodeIndexInformationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/LifemodeIndexInformationActivity;->a:Landroid/app/ActionBar;

    .line 22
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/LifemodeIndexInformationActivity;->a:Landroid/app/ActionBar;

    const v1, 0x7f0a0493

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 23
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 28
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 29
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 34
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 35
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 40
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 41
    return-void
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 46
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 47
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 52
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 53
    return-void
.end method
