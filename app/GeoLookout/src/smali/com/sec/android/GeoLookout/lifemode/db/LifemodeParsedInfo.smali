.class public Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo;
.super Ljava/lang/Object;
.source "LifemodeParsedInfo.java"


# instance fields
.field public life_cold:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;",
            ">;"
        }
    .end annotation
.end field

.field public life_dryskin:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;",
            ">;"
        }
    .end annotation
.end field

.field public life_f_dust:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;",
            ">;"
        }
    .end annotation
.end field

.field public life_f_poison:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;",
            ">;"
        }
    .end annotation
.end field

.field public life_freez:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;",
            ">;"
        }
    .end annotation
.end field

.field public life_fros:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;",
            ">;"
        }
    .end annotation
.end field

.field public life_hum:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;",
            ">;"
        }
    .end annotation
.end field

.field public life_pm10:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;",
            ">;"
        }
    .end annotation
.end field

.field public life_pollen:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;",
            ">;"
        }
    .end annotation
.end field

.field public life_putre:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;",
            ">;"
        }
    .end annotation
.end field

.field public life_s_temp:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;",
            ">;"
        }
    .end annotation
.end field

.field public life_temp:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;",
            ">;"
        }
    .end annotation
.end field

.field public life_umb:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;",
            ">;"
        }
    .end annotation
.end field

.field public life_uv:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;",
            ">;"
        }
    .end annotation
.end field

.field public life_w_acci:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;",
            ">;"
        }
    .end annotation
.end field

.field public life_y_dust:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/GeoLookout/lifemode/db/LifemodeParsedInfo$LifeItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method
