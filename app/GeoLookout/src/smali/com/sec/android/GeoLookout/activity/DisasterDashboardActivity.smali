.class public Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;
.super Landroid/app/Activity;
.source "DisasterDashboardActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TabHost$OnTabChangeListener;
.implements Landroid/widget/TabHost$TabContentFactory;
.implements Lwn;


# static fields
.field private static final J:Ljava/lang/String; = "com.kddi.android.cmail"

.field private static final K:Ljava/lang/String; = "android.intent.action.RESPOND_VIA_MESSAGE"

.field private static final f:I = 0x3

.field private static final g:I = 0x4

.field private static final h:I = 0x0

.field private static final i:I = 0x1

.field private static final j:I = 0x2

.field private static final k:I = 0x3


# instance fields
.field private A:Ljava/lang/String;

.field private B:J

.field private C:Laic;

.field private D:Lzw;

.field private E:Laks;

.field private F:Landroid/app/AlertDialog;

.field private G:Landroid/app/ProgressDialog;

.field private H:Ljava/util/TimerTask;

.field private I:Ljava/util/Timer;

.field private L:I

.field private M:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Laif;",
            ">;"
        }
    .end annotation
.end field

.field private N:Landroid/widget/TabHost;

.field private O:I

.field public a:I

.field b:Landroid/content/BroadcastReceiver;

.field public c:Lanx;

.field d:Landroid/content/BroadcastReceiver;

.field e:Lanw;

.field private l:Landroid/content/Context;

.field private m:Lapu;

.field private n:Landroid/widget/FrameLayout;

.field private o:Landroid/widget/ImageView;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/app/ActionBar;

.field private r:Lagw;

.field private s:I

.field private t:Lvs;

.field private u:Landroid/view/View;

.field private v:Laah;

.field private w:Lcom/google/android/gms/maps/model/LatLng;

.field private x:Z

.field private y:Ljava/lang/String;

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 147
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 176
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a:I

    .line 181
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->z:I

    .line 182
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->A:Ljava/lang/String;

    .line 183
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->B:J

    .line 199
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->L:I

    .line 2249
    new-instance v0, Lahu;

    invoke-direct {v0, p0}, Lahu;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b:Landroid/content/BroadcastReceiver;

    .line 2449
    new-instance v0, Laha;

    invoke-direct {v0, p0}, Laha;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->c:Lanx;

    .line 2488
    new-instance v0, Lahc;

    invoke-direct {v0, p0}, Lahc;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->d:Landroid/content/BroadcastReceiver;

    .line 2783
    new-instance v0, Lahi;

    invoke-direct {v0, p0}, Lahi;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->e:Lanw;

    .line 3130
    return-void
.end method

.method private a(D)F
    .locals 11

    .prologue
    const v9, 0x44992000    # 1225.0f

    const/high16 v8, 0x40000000    # 2.0f

    .line 2702
    const-string v0, "zoomLevel"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 2703
    const/high16 v3, 0x41600000    # 14.0f

    .line 2705
    const/high16 v1, 0x44190000    # 612.0f

    .line 2706
    const/4 v0, 0x1

    .line 2707
    div-float v2, v9, v8

    .line 2708
    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double v6, p1, v4

    move v10, v0

    move v0, v1

    move v1, v3

    move v3, v2

    move v2, v10

    .line 2710
    :goto_0
    float-to-double v4, v3

    cmpl-double v4, v6, v4

    if-lez v4, :cond_1

    .line 2711
    const/high16 v4, 0x3f000000    # 0.5f

    sub-float v4, v1, v4

    .line 2712
    add-float/2addr v3, v0

    .line 2713
    rem-int/lit8 v1, v2, 0x2

    if-nez v1, :cond_0

    .line 2714
    div-float v0, v3, v8

    .line 2716
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v4

    goto :goto_0

    .line 2718
    :cond_1
    cmpg-float v0, v3, v9

    if-gtz v0, :cond_2

    .line 2719
    const/high16 v0, 0x41500000    # 13.0f

    .line 2721
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "zoomLevel - distance1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 2722
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "zoomLevel - zoomLv1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 2723
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "zoomLevel - mScale1 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 2724
    return v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 11

    .prologue
    const/16 v10, 0x1f4

    .line 1496
    .line 1497
    const/4 v0, 0x0

    .line 1499
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1500
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getPolygonList()Ljava/util/ArrayList;

    move-result-object v1

    .line 1502
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1504
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setUpMap - Polygone, size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 1507
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1509
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1510
    new-instance v4, Laae;

    invoke-direct {v4}, Laae;-><init>()V

    move v1, v0

    .line 1512
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1513
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamf;

    .line 1514
    new-instance v5, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v6, v0, Lamf;->a:D

    iget-wide v8, v0, Lamf;->b:D

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1515
    new-instance v5, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v6, v0, Lamf;->a:D

    iget-wide v8, v0, Lamf;->b:D

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v4, v5}, Laae;->a(Lcom/google/android/gms/maps/model/LatLng;)Laae;

    .line 1517
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v5

    invoke-static {v5, v0}, Lall;->a(Lamf;Lamf;)I

    move-result v0

    .line 1518
    if-le v0, v1, :cond_1

    :goto_1
    move v1, v0

    .line 1521
    goto :goto_0

    .line 1523
    :cond_0
    invoke-virtual {v4}, Laae;->a()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v0

    .line 1524
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    new-instance v4, Lcom/google/android/gms/maps/model/PolygonOptions;

    invoke-direct {v4}, Lcom/google/android/gms/maps/model/PolygonOptions;-><init>()V

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/gms/maps/model/PolygonOptions;->a(F)Lcom/google/android/gms/maps/model/PolygonOptions;

    move-result-object v4

    const v5, 0x66ff6000

    invoke-virtual {v4, v5}, Lcom/google/android/gms/maps/model/PolygonOptions;->b(I)Lcom/google/android/gms/maps/model/PolygonOptions;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/android/gms/maps/model/PolygonOptions;->a(Ljava/lang/Iterable;)Lcom/google/android/gms/maps/model/PolygonOptions;

    move-result-object v3

    invoke-virtual {v2, v3}, Lvs;->a(Lcom/google/android/gms/maps/model/PolygonOptions;)Laaj;

    .line 1527
    const/16 v2, 0x32

    invoke-static {v0, v10, v10, v2}, Lvr;->a(Lcom/google/android/gms/maps/model/LatLngBounds;III)Lvq;

    move-result-object v0

    .line 1528
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    invoke-virtual {v2, v0}, Lvs;->a(Lvq;)V

    .line 1530
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    new-instance v2, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v2}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->w:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->D:Lzw;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lzw;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    invoke-virtual {v0, v2}, Lvs;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Laah;

    .line 1532
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1955
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "createIntentForShare(), file path = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1957
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lalz;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1959
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v0

    .line 1964
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1966
    const-string v2, "android.intent.action.SEND"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1967
    const-string v2, "android.intent.extra.STREAM"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1968
    const-string v2, "image/jpeg"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1969
    const-string v2, "exit_on_sent"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1970
    const-string v2, "android.intent.extra.TEXT"

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->k(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1971
    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1972
    return-object v1

    .line 1961
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Ljava/lang/String;J)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 2

    .prologue
    .line 147
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/lang/String;J)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;J)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 2

    .prologue
    .line 765
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3}, Lalx;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 761
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "DID"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->I:Ljava/util/Timer;

    return-object p1
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Ljava/util/TimerTask;)Ljava/util/TimerTask;
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->H:Ljava/util/TimerTask;

    return-object p1
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Lvs;)Lvs;
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    return-object p1
.end method

.method private a(I)V
    .locals 6

    .prologue
    .line 304
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MULTIPOINT : checkCurrentDisasterState "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mUid "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->O:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 305
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 306
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 307
    const/4 v2, -0x1

    .line 309
    invoke-direct {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 310
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-gtz v3, :cond_5

    :cond_0
    sget-boolean v3, Laky;->b:Z

    if-nez v3, :cond_1

    sget-boolean v3, Laky;->a:Z

    if-eqz v3, :cond_5

    .line 313
    :cond_1
    sget-boolean v3, Lakv;->bP:Z

    if-nez v3, :cond_2

    sget-boolean v3, Lakv;->bR:Z

    if-eqz v3, :cond_4

    .line 314
    :cond_2
    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    iget v4, v0, Laif;->m:I

    invoke-static {v3, v4}, Lalx;->c(Landroid/content/Context;I)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v3

    .line 319
    :goto_0
    if-eqz v3, :cond_3

    .line 320
    invoke-virtual {v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    .line 321
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "did - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lalj;->b(Ljava/lang/String;)V

    .line 339
    :cond_3
    :goto_1
    iget v3, v0, Laif;->m:I

    int-to-long v4, v3

    invoke-direct {p0, v1, v4, v5}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/lang/String;J)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v1

    iput-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    .line 340
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    if-nez v1, :cond_8

    .line 341
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MULTIPOINT : STATE_NO_DISASTER "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 342
    const/4 v1, 0x0

    iput v1, v0, Laif;->n:I

    move v1, v2

    .line 351
    :goto_2
    iget-object v0, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-direct {p0, v1, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(ILcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 352
    return-void

    .line 316
    :cond_4
    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    iget v4, v0, Laif;->m:I

    invoke-static {v3, v4}, Lalx;->b(Landroid/content/Context;I)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v3

    goto :goto_0

    .line 323
    :cond_5
    iget v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->O:I

    iget v4, v0, Laif;->m:I

    if-eq v3, v4, :cond_3

    .line 326
    sget-boolean v3, Lakv;->bP:Z

    if-nez v3, :cond_6

    sget-boolean v3, Lakv;->bR:Z

    if-eqz v3, :cond_7

    .line 327
    :cond_6
    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    iget v4, v0, Laif;->m:I

    invoke-static {v3, v4}, Lalx;->c(Landroid/content/Context;I)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v3

    .line 332
    :goto_3
    if-eqz v3, :cond_3

    .line 333
    invoke-virtual {v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    .line 334
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SUPPORT_WARNING_MULTI_POINT did - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 329
    :cond_7
    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    iget v4, v0, Laif;->m:I

    invoke-static {v3, v4}, Lalx;->b(Landroid/content/Context;I)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v3

    goto :goto_3

    .line 344
    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MULTIPOINT : checkCurrentDisasterState "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 345
    invoke-static {}, Lalk;->a()Lalk;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    iget-object v3, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getID()I

    move-result v3

    iget-object v4, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v4}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lalk;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 346
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(I)V

    .line 347
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v1

    .line 349
    invoke-static {}, Lalk;->a()Lalk;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    iget-object v4, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2, v3, v4}, Lalk;->c(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    goto/16 :goto_2
.end method

.method private a(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MULTIPOINT, setUpMap - type = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1170
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->x:Z

    if-nez v0, :cond_1

    .line 1171
    const-string v0, "setUpMap isGooglePlayServicesAvailable false"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 1205
    :cond_0
    :goto_0
    return-void

    .line 1175
    :cond_1
    new-instance v0, Laks;

    iget v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->z:I

    invoke-direct {v0, p0, v1}, Laks;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->E:Laks;

    .line 1176
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 1177
    invoke-direct {p0, p2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->g(I)V

    .line 1179
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    if-nez v1, :cond_2

    .line 1180
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h()Lvs;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    .line 1183
    :cond_2
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    if-eqz v1, :cond_0

    .line 1186
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    invoke-virtual {v1, p0}, Lvs;->a(Lwn;)V

    .line 1187
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    invoke-virtual {v1}, Lvs;->f()V

    .line 1188
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    invoke-virtual {v1}, Lvs;->m()Lxf;

    move-result-object v1

    invoke-virtual {v1, v2}, Lxf;->a(Z)V

    .line 1189
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    invoke-virtual {v1}, Lvs;->m()Lxf;

    move-result-object v1

    invoke-virtual {v1, v2}, Lxf;->b(Z)V

    .line 1191
    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Laif;)V

    .line 1193
    iget-object v1, v0, Laif;->u:Lamf;

    if-eqz v1, :cond_3

    .line 1194
    iget-object v1, v0, Laif;->u:Lamf;

    invoke-direct {p0, v1, p2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lamf;I)V

    .line 1197
    :cond_3
    iget v0, v0, Laif;->n:I

    if-nez v0, :cond_0

    .line 1198
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(ILcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 3

    .prologue
    const v1, 0x7f0a00a9

    const/4 v2, 0x0

    .line 419
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->q:Landroid/app/ActionBar;

    .line 420
    if-ltz p1, :cond_3

    .line 421
    if-eqz p2, :cond_0

    .line 422
    const-string v0, "03"

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 423
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v1

    invoke-static {v0, p1, v1}, Lall;->b(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    .line 424
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->q:Landroid/app/ActionBar;

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 425
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 437
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->q:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 438
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->q:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 440
    sget-boolean v0, Laky;->k:Z

    if-nez v0, :cond_1

    .line 441
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->q:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 442
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->q:Landroid/app/ActionBar;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 444
    :cond_1
    return-void

    .line 427
    :cond_2
    invoke-static {p1}, Lall;->c(I)I

    move-result v0

    .line 428
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->q:Landroid/app/ActionBar;

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setTitle(I)V

    .line 429
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->setTitle(I)V

    goto :goto_0

    .line 433
    :cond_3
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->q:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 434
    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->setTitle(I)V

    goto :goto_0
.end method

.method private a(Laif;)V
    .locals 2

    .prologue
    .line 1208
    const v0, 0x7f0e003c

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1209
    new-instance v1, Laht;

    invoke-direct {v1, p0, p1}, Laht;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Laif;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1220
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1221
    return-void
.end method

.method private a(Lamf;)V
    .locals 6

    .prologue
    .line 2663
    const-string v0, "updateMapCamera"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 2665
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    if-nez v0, :cond_0

    .line 2666
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h()Lvs;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    .line 2668
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    if-nez v0, :cond_2

    .line 2675
    :cond_1
    :goto_0
    return-void

    .line 2671
    :cond_2
    if-eqz p1, :cond_1

    .line 2672
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, p1, Lamf;->a:D

    iget-wide v4, p1, Lamf;->b:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-static {v0}, Lvr;->a(Lcom/google/android/gms/maps/model/LatLng;)Lvq;

    move-result-object v0

    .line 2673
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    invoke-virtual {v1, v0}, Lvs;->a(Lvq;)V

    goto :goto_0
.end method

.method private a(Lamf;I)V
    .locals 9

    .prologue
    const/high16 v8, 0x3f000000    # 0.5f

    .line 2622
    const-string v0, "updateMyLocation!!!"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 2623
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 2624
    iput-object p1, v0, Laif;->u:Lamf;

    .line 2627
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    if-nez v1, :cond_0

    .line 2628
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h()Lvs;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    .line 2630
    :cond_0
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    if-nez v1, :cond_2

    .line 2660
    :cond_1
    :goto_0
    return-void

    .line 2634
    :cond_2
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    invoke-virtual {v1}, Lvs;->m()Lxf;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lxf;->b(Z)V

    .line 2637
    iget-object v1, v0, Laif;->u:Lamf;

    if-eqz v1, :cond_4

    .line 2638
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->v:Laah;

    if-eqz v1, :cond_3

    .line 2639
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->v:Laah;

    invoke-virtual {v1}, Laah;->a()V

    .line 2641
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MULTIPOINT - updateMyLocation : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Laif;->u:Lamf;

    iget-wide v2, v2, Lamf;->a:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Laif;->u:Lamf;

    iget-wide v2, v2, Lamf;->b:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Laif;->x:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 2643
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    new-instance v2, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v2}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    iget-object v3, v0, Laif;->v:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v4, v0, Laif;->u:Lamf;

    iget-wide v4, v4, Lamf;->a:D

    iget-object v6, v0, Laif;->u:Lamf;

    iget-wide v6, v6, Lamf;->b:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    invoke-virtual {v2, v8, v8}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    const v3, 0x7f02001e

    invoke-static {v3}, Lzx;->a(I)Lzw;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lzw;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    invoke-virtual {v1, v2}, Lvs;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Laah;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->v:Laah;

    .line 2651
    :cond_4
    iget v1, v0, Laif;->n:I

    if-nez v1, :cond_1

    .line 2652
    sget-boolean v1, Lakv;->bQ:Z

    if-eqz v1, :cond_5

    .line 2653
    iget-object v0, v0, Laif;->u:Lamf;

    iget v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->L:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(Lamf;I)V

    goto/16 :goto_0

    .line 2655
    :cond_5
    const/16 v1, 0x4c9

    iput v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a:I

    .line 2656
    iget-object v0, v0, Laif;->u:Lamf;

    iget v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(Lamf;I)V

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 3077
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-static {v0}, Lall;->q(Landroid/content/Context;)I

    move-result v0

    .line 3079
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "createAlarmSettingPopup (Current alarm type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 3081
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a0027

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x7f070000

    new-instance v3, Lahn;

    invoke-direct {v3, p0}, Lahn;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 3091
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 3092
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 3093
    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->e()V

    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h(I)V

    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;II)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(II)V

    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Lamf;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lamf;)V

    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Lamf;I)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lamf;I)V

    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/lang/String;I)V

    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 2981
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2982
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2984
    const v1, 0x7f030018

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 2985
    const v0, 0x7f0e007b

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2986
    const v1, 0x7f0e007c

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 2988
    const v4, 0x7f0a04b1

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 2989
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2990
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 2991
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 2992
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2994
    new-instance v0, Lahl;

    invoke-direct {v0, p0, p2}, Lahl;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)V

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 3007
    const v0, 0x104000a

    new-instance v1, Lahm;

    invoke-direct {v1, p0, p2}, Lahm;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)V

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3024
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 3025
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 3027
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 3356
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sendMessageIntentJPN, msg = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 3359
    const-string v0, ","

    const-string v1, ";"

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 3361
    const-string v1, "KDI"

    invoke-static {}, Lall;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3362
    const-string v1, "-"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 3363
    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 3365
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 3366
    array-length v0, v1

    if-eqz v0, :cond_1

    .line 3367
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 3368
    const-string v2, "smsto"

    aget-object v3, v1, v0

    invoke-static {v2, v3, v5}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 3369
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.RESPOND_VIA_MESSAGE"

    invoke-direct {v3, v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 3370
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v3, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3371
    const-string v2, "com.kddi.android.cmail"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 3372
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 3373
    const-wide/16 v2, 0x1f4

    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    .line 3367
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3378
    :cond_0
    const-string v1, "sms"

    invoke-static {v1, v0, v5}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 3379
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.RESPOND_VIA_MESSAGE"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 3380
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3381
    const-string v0, "com.android.mms"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 3382
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 3385
    :cond_1
    return-void
.end method

.method private a(J)Z
    .locals 5

    .prologue
    .line 1057
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1058
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkEndTime, endTime = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", currentTime = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 1059
    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 1060
    const/4 v0, 0x0

    .line 1062
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Ljava/util/Locale;)Z
    .locals 3

    .prologue
    .line 2819
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 2820
    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    .line 2821
    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 2823
    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2824
    const/4 v0, 0x1

    .line 2827
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 18

    .prologue
    .line 1536
    .line 1537
    const/4 v3, 0x0

    .line 1540
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1548
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v7

    .line 1550
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lama;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    .line 1552
    const-string v2, "01"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1553
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    new-instance v4, Laid;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5, v9}, Laid;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Landroid/content/Context;Lagz;)V

    invoke-virtual {v2, v4}, Lvs;->a(Lwh;)V

    .line 1554
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    new-instance v4, Laig;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Laig;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Lagz;)V

    invoke-virtual {v2, v4}, Lvs;->a(Lwj;)V

    .line 1557
    :cond_0
    const/4 v2, 0x0

    move v4, v3

    move v3, v2

    :goto_0
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_6

    .line 1558
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v5, v2}, Lama;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    .line 1560
    if-eqz v9, :cond_5

    .line 1561
    new-instance v2, Laie;

    const/4 v5, 0x0

    invoke-direct {v2, v5}, Laie;-><init>(Lagz;)V

    invoke-static {v9, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1562
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 1563
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "hthpinfos count = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", size = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 1565
    const/4 v2, 0x0

    move v5, v2

    :goto_1
    if-ge v5, v10, :cond_3

    .line 1566
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "setUpMap HTHP data lat = "

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v2

    iget-wide v12, v2, Lamf;->a:D

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v11, ", long = "

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v2

    iget-wide v12, v2, Lamf;->b:D

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 1568
    new-instance v11, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v2

    iget-wide v12, v2, Lamf;->a:D

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v2

    iget-wide v14, v2, Lamf;->b:D

    invoke-direct {v11, v12, v13, v14, v15}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 1571
    const/4 v12, 0x2

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDataType()S

    move-result v2

    if-ne v12, v2, :cond_2

    const v2, 0x7f020020

    .line 1574
    :goto_2
    new-instance v12, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v12}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {v12, v11}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v12

    invoke-static {v2}, Lzx;->a(I)Lzw;

    move-result-object v2

    invoke-virtual {v12, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lzw;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    const/high16 v12, 0x3f000000    # 0.5f

    const/high16 v13, 0x3f000000    # 0.5f

    invoke-virtual {v2, v12, v13}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v12

    .line 1578
    const-string v2, "01"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1579
    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailStormSpeed()I

    move-result v13

    .line 1580
    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailDirectionDegrees()I

    move-result v14

    .line 1581
    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailWindMax()I

    move-result v2

    .line 1583
    new-instance v15, Ljava/lang/StringBuffer;

    invoke-direct {v15}, Ljava/lang/StringBuffer;-><init>()V

    .line 1584
    invoke-virtual {v15, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v16, ":"

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v13, ":"

    invoke-virtual {v2, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 1588
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->b(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 1591
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    invoke-virtual {v2, v12}, Lvs;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Laah;

    .line 1593
    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1595
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v2

    new-instance v12, Lamf;

    iget-wide v14, v11, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-wide v0, v11, Lcom/google/android/gms/maps/model/LatLng;->c:D

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-direct {v12, v14, v15, v0, v1}, Lamf;-><init>(DD)V

    invoke-static {v2, v12}, Lall;->a(Lamf;Lamf;)I

    move-result v2

    .line 1596
    if-le v2, v4, :cond_7

    .line 1565
    :goto_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v2

    goto/16 :goto_1

    .line 1571
    :cond_2
    const v2, 0x7f02001f

    goto/16 :goto_2

    .line 1601
    :cond_3
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1602
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 1603
    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1604
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/maps/model/LatLng;

    .line 1605
    new-instance v10, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v12, v2, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-wide v14, v2, Lcom/google/android/gms/maps/model/LatLng;->c:D

    invoke-direct {v10, v12, v13, v14, v15}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1607
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    new-instance v9, Lcom/google/android/gms/maps/model/PolylineOptions;

    invoke-direct {v9}, Lcom/google/android/gms/maps/model/PolylineOptions;-><init>()V

    invoke-virtual {v9, v5}, Lcom/google/android/gms/maps/model/PolylineOptions;->a(Ljava/lang/Iterable;)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v5

    const/4 v9, -0x1

    invoke-virtual {v5, v9}, Lcom/google/android/gms/maps/model/PolylineOptions;->a(I)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v5

    const/high16 v9, 0x40a00000    # 5.0f

    invoke-virtual {v5, v9}, Lcom/google/android/gms/maps/model/PolylineOptions;->a(F)Lcom/google/android/gms/maps/model/PolylineOptions;

    move-result-object v5

    invoke-virtual {v2, v5}, Lvs;->a(Lcom/google/android/gms/maps/model/PolylineOptions;)Laal;

    .line 1608
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 1557
    :cond_5
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_0

    .line 1612
    :cond_6
    return v4

    :cond_7
    move v2, v4

    goto :goto_3
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->y:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 355
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.sec.android.GeoLookout.ACTION_UPDATE_CURRENT_LOCATION"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 356
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->d:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.sec.android.GeoLookout.ACTION_UPDATE_PUSH_MESSAGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 357
    return-void
.end method

.method private b(I)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 360
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 361
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v1

    if-ne v1, v2, :cond_2

    .line 362
    iput v2, v0, Laif;->n:I

    .line 367
    :goto_0
    iget v1, v0, Laif;->n:I

    if-ne v1, v2, :cond_3

    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getWarningIcon()I

    move-result v1

    :goto_1
    iput v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->z:I

    .line 369
    iget-boolean v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->x:Z

    if-eqz v1, :cond_0

    .line 370
    iget v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->z:I

    invoke-static {v1}, Lzx;->a(I)Lzw;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->D:Lzw;

    .line 373
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTipLinkId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->A:Ljava/lang/String;

    .line 375
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationType()I

    move-result v1

    iput v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s:I

    .line 376
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getEndTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->B:J

    .line 378
    iget-wide v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->B:J

    invoke-direct {p0, v2, v3}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(J)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 379
    const-string v1, "Disaster is eneded!!!"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 383
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MULTIPOINT restoreLastDisaster "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / Significance : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / State : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v0, v0, Laif;->n:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / Icon : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->z:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 384
    return-void

    .line 364
    :cond_2
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v1

    iput v1, v0, Laif;->n:I

    goto/16 :goto_0

    .line 367
    :cond_3
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getWatchIcon()I

    move-result v1

    goto/16 :goto_1
.end method

.method private b(II)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1761
    const-string v0, "START = showMobileLink"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1762
    const-string v0, ""

    .line 1764
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 1765
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    if-nez v1, :cond_1

    .line 1766
    const-string v0, "mLastDisaster is null!!!"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1817
    :cond_0
    :goto_0
    return-void

    .line 1770
    :cond_1
    if-nez p1, :cond_3

    .line 1771
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tips is clicked!, mTipLink = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1773
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->A:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1774
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a04b0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1779
    :cond_2
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->A:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1780
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1781
    :catch_0
    move-exception v0

    .line 1782
    const-string v1, "ActivityNotFoundException "

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    .line 1783
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 1786
    :cond_3
    if-ne p1, v3, :cond_4

    .line 1787
    iget-object v0, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLink()Ljava/lang/String;

    move-result-object v0

    .line 1788
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tips is clicked!, mobileLink = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 1790
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1795
    :try_start_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1796
    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 1797
    :catch_1
    move-exception v0

    .line 1798
    const-string v1, "ActivityNotFoundException "

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    .line 1799
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 1801
    :cond_4
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1802
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CP Logo is clicked!, mCpLink = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1804
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1809
    :try_start_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->y:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1810
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 1811
    :catch_2
    move-exception v0

    .line 1812
    const-string v1, "ActivityNotFoundException "

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    .line 1813
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private b(Lamf;I)V
    .locals 6

    .prologue
    .line 2679
    const-string v0, "updateMapCamera"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 2682
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    if-nez v0, :cond_0

    .line 2683
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h()Lvs;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    .line 2685
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    if-nez v0, :cond_2

    .line 2699
    :cond_1
    :goto_0
    return-void

    .line 2689
    :cond_2
    if-eqz p1, :cond_1

    .line 2691
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "zoomLevel - disasterLoc : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p1, Lamf;->a:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p1, Lamf;->b:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 2692
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "zoomLevel - distance : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 2693
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, p1, Lamf;->a:D

    iget-wide v4, p1, Lamf;->b:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    int-to-double v2, p2

    invoke-direct {p0, v2, v3}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(D)F

    move-result v1

    invoke-static {v0, v1}, Lvr;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lvq;

    move-result-object v0

    .line 2694
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    invoke-virtual {v1, v0}, Lvs;->a(Lvq;)V

    goto :goto_0
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;II)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(II)V

    return-void
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Lamf;I)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(Lamf;I)V

    return-void
.end method

.method public static synthetic c(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)I
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t()I

    move-result v0

    return v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 387
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->m:Lapu;

    if-eqz v0, :cond_0

    .line 388
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->m:Lapu;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lapu;->cancel(Z)Z

    .line 390
    :cond_0
    new-instance v0, Lapu;

    invoke-direct {v0, p0}, Lapu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->m:Lapu;

    .line 391
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->m:Lapu;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lapu;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 392
    return-void
.end method

.method private c(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 582
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 583
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_0

    .line 584
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 586
    :cond_0
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    if-eqz v1, :cond_1

    .line 587
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    invoke-virtual {v1}, Lvs;->f()V

    .line 589
    :cond_1
    if-nez p1, :cond_6

    .line 590
    const-string v1, "set No Disaster Layout"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 592
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->e:Lanw;

    invoke-virtual {v1, p0, v2, v3}, Lalu;->a(Landroid/content/Context;Lanw;I)V

    .line 594
    const v1, 0x7f030003

    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->setContentView(I)V

    .line 595
    const v1, 0x7f0e000a

    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Laif;->w:Landroid/view/View;

    .line 596
    const v0, 0x7f0e000d

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n:Landroid/widget/FrameLayout;

    .line 597
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s:I

    .line 598
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_2

    .line 599
    const v0, 0x7f0e004b

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 600
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00c0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 609
    :cond_2
    :goto_0
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 611
    iget-boolean v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->x:Z

    if-eqz v1, :cond_4

    .line 612
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->u:Landroid/view/View;

    if-nez v1, :cond_3

    .line 613
    const v1, 0x7f030009

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->u:Landroid/view/View;

    .line 615
    :cond_3
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 616
    iget v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s:I

    invoke-direct {p0, v0, v3}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(II)V

    .line 617
    invoke-direct {p0, v3}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->i(I)V

    .line 620
    :cond_4
    sget-boolean v0, Laky;->k:Z

    if-eqz v0, :cond_5

    .line 621
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->q:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 622
    new-instance v0, Lagw;

    invoke-direct {v0, p0}, Lagw;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->r:Lagw;

    .line 624
    :cond_5
    return-void

    .line 604
    :cond_6
    const v1, 0x7f030001

    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->setContentView(I)V

    .line 605
    const v1, 0x7f0e0004

    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Laif;->w:Landroid/view/View;

    .line 606
    const v0, 0x7f0e0006

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n:Landroid/widget/FrameLayout;

    goto :goto_0
.end method

.method private c(II)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 1977
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sendMessage, msg_ID = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "multipoint = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1981
    const-string v0, ""

    .line 1982
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1985
    if-lez p2, :cond_0

    .line 1986
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    iget v0, v0, Laif;->m:I

    invoke-static {v2, v0}, Lalz;->g(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 1992
    :goto_0
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-static {v2}, Lall;->C(Landroid/content/Context;)Z

    move-result v2

    if-ne v2, v3, :cond_2

    .line 1993
    packed-switch p1, :pswitch_data_0

    .line 2012
    :goto_1
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2013
    invoke-direct {p0, p2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->k(I)Ljava/lang/String;

    move-result-object v2

    .line 2015
    if-nez v2, :cond_1

    .line 2016
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const v3, 0x7f0a048c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2021
    :goto_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2023
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.SENDTO"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2024
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "smsto:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2025
    const-string v0, "sms_body"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2026
    invoke-virtual {p0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startActivity(Landroid/content/Intent;)V

    .line 2030
    :goto_3
    return-void

    .line 1989
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1995
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const v3, 0x7f0a00af

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1999
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const v3, 0x7f0a00b0

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2003
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const v3, 0x7f0a002b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2007
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const v3, 0x7f0a0033

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 2018
    :cond_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 2028
    :cond_2
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const v1, 0x7f0a04a4

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_3

    .line 1993
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static synthetic c(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->p(I)V

    return-void
.end method

.method public static synthetic d(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 395
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->F:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 396
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 397
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00a4

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0002

    new-instance v2, Laho;

    invoke-direct {v2, p0}, Laho;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lagz;

    invoke-direct {v1, p0}, Lagz;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00a9

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->F:Landroid/app/AlertDialog;

    .line 409
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->F:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 410
    return-void
.end method

.method private d(I)V
    .locals 8

    .prologue
    const v7, 0x7f0e001a

    const v6, 0x7f0e0015

    const v5, 0x7f0e0014

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 627
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 628
    const-string v1, "03"

    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v1

    const/16 v2, 0x69

    if-ne v1, v2, :cond_0

    .line 682
    :goto_0
    return-void

    .line 633
    :cond_0
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v0, Laif;->o:Landroid/widget/LinearLayout;

    .line 634
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-static {v1}, Lall;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 635
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 636
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v0, Laif;->i:Landroid/widget/LinearLayout;

    .line 637
    iget-object v1, v0, Laif;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 638
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e0016

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Laif;->a:Landroid/widget/TextView;

    .line 639
    iget-object v1, v0, Laif;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, Lahr;

    invoke-direct {v2, p0, v0}, Lahr;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Laif;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 662
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e0017

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Laif;->b:Landroid/widget/TextView;

    .line 663
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e0019

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Laif;->c:Landroid/widget/TextView;

    .line 664
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e0018

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Laif;->d:Landroid/widget/TextView;

    .line 674
    :goto_1
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLink()Ljava/lang/String;

    move-result-object v1

    .line 675
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "null"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 676
    :cond_1
    iget-object v0, v0, Laif;->o:Landroid/widget/LinearLayout;

    const v1, 0x7f0e001f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 666
    :cond_2
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 667
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v0, Laif;->i:Landroid/widget/LinearLayout;

    .line 668
    iget-object v1, v0, Laif;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 669
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e001b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Laif;->a:Landroid/widget/TextView;

    .line 670
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e001c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Laif;->b:Landroid/widget/TextView;

    .line 671
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e001e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Laif;->c:Landroid/widget/TextView;

    .line 672
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e001d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Laif;->d:Landroid/widget/TextView;

    goto :goto_1

    .line 678
    :cond_3
    iget-object v1, v0, Laif;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 679
    iget-object v1, v0, Laif;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f02005d

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 680
    iget-object v0, v0, Laif;->o:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    goto/16 :goto_0
.end method

.method private d(II)V
    .locals 5

    .prologue
    const v3, 0x7f0a048c

    .line 3262
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sendJPNMessage, msg_ID = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "multipoint = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 3266
    const-string v0, ""

    .line 3269
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 3272
    packed-switch p1, :pswitch_data_0

    .line 3293
    :goto_0
    if-lez p2, :cond_1

    .line 3294
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    iget v0, v0, Laif;->m:I

    invoke-static {v2, v0}, Lalz;->g(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 3300
    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3301
    if-nez p2, :cond_0

    .line 3304
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3305
    invoke-direct {p0, p2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->k(I)Ljava/lang/String;

    move-result-object v0

    .line 3307
    if-nez v0, :cond_2

    .line 3308
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3313
    :cond_0
    :goto_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3315
    const-string v0, "KDI"

    invoke-static {}, Lall;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3317
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.SENDTO"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3318
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.kddi.android.cmail"

    const-string v4, "com.kddi.android.cmail.ui.detail.NewMessageActivity"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 3319
    const-string v2, "sms_body"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3327
    :goto_3
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startActivity(Landroid/content/Intent;)V

    .line 3353
    :goto_4
    return-void

    .line 3274
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const v2, 0x7f0a00af

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 3278
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const v2, 0x7f0a00b0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 3282
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const v2, 0x7f0a002b

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 3286
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const v2, 0x7f0a0033

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 3296
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 3310
    :cond_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 3322
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3323
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.android.mms"

    const-string v4, "com.android.mms.ui.ConversationComposer"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 3324
    const-string v2, "sms_body"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_3

    .line 3332
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3335
    const-string v1, "KDI"

    invoke-static {}, Lall;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 3336
    invoke-direct {p0, p2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->k(I)Ljava/lang/String;

    move-result-object v1

    .line 3337
    if-nez v1, :cond_5

    .line 3338
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 3340
    :cond_5
    invoke-direct {p0, v1, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3344
    :cond_6
    invoke-direct {p0, p2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t(I)Ljava/lang/String;

    move-result-object v1

    .line 3345
    if-eqz v1, :cond_7

    .line 3346
    invoke-direct {p0, v1, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 3349
    :cond_7
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00c2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a00b8

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 3351
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_4

    .line 3272
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static synthetic d(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->i(I)V

    return-void
.end method

.method public static synthetic e(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Lvs;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    return-object v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->F:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 414
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->F:Landroid/app/AlertDialog;

    .line 415
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->finish()V

    .line 416
    return-void
.end method

.method private e(I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 685
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 686
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e0028

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 688
    iget-object v2, v0, Laif;->w:Landroid/view/View;

    const v3, 0x7f0e0021

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, v0, Laif;->p:Landroid/widget/LinearLayout;

    .line 689
    iget-object v2, v0, Laif;->w:Landroid/view/View;

    const v3, 0x7f0e0026

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, v0, Laif;->q:Landroid/widget/LinearLayout;

    .line 692
    const v2, 0x7f0a0099

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 695
    iget-object v2, v0, Laif;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v2, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 696
    iget-object v2, v0, Laif;->p:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 698
    iget-boolean v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->x:Z

    if-nez v2, :cond_1

    .line 699
    iget-object v2, v0, Laif;->q:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 700
    const v2, -0x777778

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 707
    :goto_0
    const-string v1, "02"

    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 708
    iget-object v0, v0, Laif;->p:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 710
    :cond_0
    return-void

    .line 702
    :cond_1
    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 703
    iget-object v1, v0, Laif;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 704
    iget-object v1, v0, Laif;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    goto :goto_0
.end method

.method public static synthetic e(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->j(I)V

    return-void
.end method

.method public static synthetic f(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)I
    .locals 0

    .prologue
    .line 147
    iput p1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s:I

    return p1
.end method

.method public static synthetic f(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Lvs;
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h()Lvs;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 6

    .prologue
    const v5, 0x7f080018

    const/4 v4, 0x0

    .line 447
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 448
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 453
    :goto_0
    new-instance v0, Laif;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Laif;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Lagz;)V

    .line 454
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 455
    const v1, 0x7f0a0492

    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Laif;->v:Ljava/lang/String;

    .line 456
    iput v4, v0, Laif;->m:I

    .line 458
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 459
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 460
    if-eqz v1, :cond_0

    .line 461
    const-string v2, "uid"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->O:I

    .line 463
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MULTIPOINT mMultiPointUid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->O:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 465
    invoke-direct {p0, v4}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o(I)V

    .line 468
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->g()V

    .line 474
    iget v1, v0, Laif;->n:I

    if-nez v1, :cond_3

    .line 475
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 476
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/widget/TabWidget;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 477
    invoke-direct {p0, v4}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l(I)V

    .line 478
    iget-object v1, v0, Laif;->u:Lamf;

    invoke-direct {p0, v1, v4}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lamf;I)V

    .line 479
    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Laif;)V

    .line 489
    :cond_1
    :goto_1
    return-void

    .line 450
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 483
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lalz;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 484
    invoke-direct {p0, v4}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->d(I)V

    .line 485
    invoke-direct {p0, v4}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->e(I)V

    .line 486
    invoke-direct {p0, v4}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->f(I)V

    .line 487
    invoke-direct {p0, v4}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->j(I)V

    goto :goto_1
.end method

.method private f(I)V
    .locals 5

    .prologue
    const v4, 0x7f020056

    const/4 v3, 0x1

    .line 713
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 714
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e0029

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v0, Laif;->r:Landroid/widget/LinearLayout;

    .line 715
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e002f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v0, Laif;->t:Landroid/widget/LinearLayout;

    .line 716
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e002c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v0, Laif;->s:Landroid/widget/LinearLayout;

    .line 718
    iget v1, v0, Laif;->n:I

    if-ne v1, v3, :cond_3

    .line 719
    if-lez p1, :cond_4

    .line 720
    iget-object v1, v0, Laif;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 721
    iget-object v1, v0, Laif;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 727
    :goto_0
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e0032

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v0, Laif;->g:Landroid/widget/LinearLayout;

    .line 728
    iget-object v1, v0, Laif;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 729
    iget-object v1, v0, Laif;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 731
    if-eqz p1, :cond_0

    .line 732
    iget-object v1, v0, Laif;->g:Landroid/widget/LinearLayout;

    const v2, 0x7f0e0033

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0a0033

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 735
    :cond_0
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e0030

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v0, Laif;->h:Landroid/widget/LinearLayout;

    .line 736
    iget-object v1, v0, Laif;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 737
    iget-object v1, v0, Laif;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 739
    if-eqz p1, :cond_1

    .line 740
    iget-object v1, v0, Laif;->h:Landroid/widget/LinearLayout;

    const v2, 0x7f0e0031

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0a002b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 743
    :cond_1
    sget-boolean v1, Laky;->d:Z

    if-eqz v1, :cond_2

    sget-boolean v1, Laky;->e:Z

    if-eqz v1, :cond_2

    .line 744
    const-string v1, "makeEmergencyCallAndMsgLayout() : usa-vzw"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 745
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e002b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 746
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a003a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 750
    :cond_2
    invoke-static {}, Lall;->g()Ljava/lang/String;

    move-result-object v1

    .line 752
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MEssage icon style = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 753
    const-string v2, "ATT"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 754
    iget-object v1, v0, Laif;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 755
    iget-object v0, v0, Laif;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 758
    :cond_3
    return-void

    .line 723
    :cond_4
    iget-object v1, v0, Laif;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 724
    iget-object v1, v0, Laif;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    goto/16 :goto_0
.end method

.method public static synthetic g(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Laks;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->E:Laks;

    return-object v0
.end method

.method private g()V
    .locals 13

    .prologue
    const/16 v12, 0x8

    const/4 v11, 0x1

    const/4 v6, 0x0

    const v10, 0x7f0e0008

    const/4 v7, 0x0

    .line 492
    .line 495
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lalz;->b:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 497
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 498
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MULTIPOINT"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 501
    :cond_0
    new-instance v0, Laif;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Laif;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Lagz;)V

    .line 502
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Laif;->v:Ljava/lang/String;

    .line 503
    const/4 v2, 0x5

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v0, Laif;->m:I

    .line 504
    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    .line 505
    const/4 v4, 0x4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    .line 506
    new-instance v8, Lamf;

    invoke-direct {v8, v2, v3, v4, v5}, Lamf;-><init>(DD)V

    iput-object v8, v0, Laif;->u:Lamf;

    .line 507
    const/4 v8, 0x6

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v0, Laif;->x:Ljava/lang/String;

    .line 508
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MULTIPOINT LOC "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 509
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 510
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    .line 515
    :cond_1
    if-eqz v1, :cond_2

    .line 516
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 519
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_3

    .line 520
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 522
    :cond_3
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    if-eqz v0, :cond_4

    .line 523
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    invoke-virtual {v0}, Lvs;->f()V

    .line 524
    iput-object v7, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    .line 526
    :cond_4
    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->setContentView(I)V

    .line 527
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lalz;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 528
    invoke-virtual {p0, v10}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v12}, Landroid/view/View;->setVisibility(I)V

    .line 533
    :goto_1
    const v0, 0x7f0e0006

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n:Landroid/widget/FrameLayout;

    .line 535
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 536
    const v1, 0x7f0e0007

    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TabHost;

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    .line 537
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->setup()V

    .line 538
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laif;

    .line 540
    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    iget v4, v1, Laif;->m:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/widget/TabHost$TabContentFactory;)Landroid/widget/TabHost$TabSpec;

    move-result-object v3

    iget-object v1, v1, Laif;->v:Ljava/lang/String;

    invoke-virtual {v3, v1}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    .line 543
    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v3, v1}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    goto :goto_2

    .line 512
    :catch_0
    move-exception v0

    move-object v1, v7

    .line 513
    :goto_3
    :try_start_2
    invoke-static {v0}, Lalj;->e(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 515
    if-eqz v1, :cond_2

    .line 516
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 515
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_4
    if-eqz v1, :cond_5

    .line 516
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 530
    :cond_6
    invoke-virtual {p0, v10}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 546
    :cond_7
    iget-boolean v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->x:Z

    if-eqz v1, :cond_a

    .line 547
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->u:Landroid/view/View;

    if-nez v1, :cond_8

    .line 548
    const v1, 0x7f030009

    invoke-virtual {v0, v1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->u:Landroid/view/View;

    .line 550
    :cond_8
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 556
    :goto_5
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v0, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 557
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v3

    move v2, v6

    .line 558
    :goto_6
    invoke-virtual {v3}, Landroid/widget/TabWidget;->getTabCount()I

    move-result v0

    if-ge v2, v0, :cond_c

    .line 559
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v0

    .line 560
    const v1, 0x1020016

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 561
    if-eqz v0, :cond_9

    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_9

    .line 562
    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 563
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 564
    if-nez v2, :cond_b

    .line 565
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0800ab

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 569
    :goto_7
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laif;

    iput-object v0, v1, Laif;->y:Landroid/widget/TextView;

    .line 558
    :cond_9
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 553
    :cond_a
    invoke-virtual {p0, v10}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v12}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    .line 567
    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f080097

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_7

    .line 574
    :cond_c
    sget-boolean v0, Laky;->k:Z

    if-eqz v0, :cond_d

    .line 575
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->q:Landroid/app/ActionBar;

    invoke-virtual {v0, v11}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 576
    new-instance v0, Lagw;

    invoke-direct {v0, p0}, Lagw;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->r:Lagw;

    .line 578
    :cond_d
    return-void

    .line 515
    :catchall_1
    move-exception v0

    goto/16 :goto_4

    .line 512
    :catch_1
    move-exception v0

    goto/16 :goto_3
.end method

.method private g(I)V
    .locals 7

    .prologue
    const v4, 0x7f0a04f3

    const v3, 0x7f02000d

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 1067
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    .line 1068
    const v0, 0x7f0e003b

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o:Landroid/widget/ImageView;

    .line 1070
    if-nez v2, :cond_0

    .line 1071
    const-string v0, "makeCPLink : lastDisaster is null"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 1150
    :goto_0
    return-void

    .line 1074
    :cond_0
    invoke-static {}, Lall;->f()Ljava/lang/String;

    move-result-object v0

    .line 1076
    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_7

    .line 1077
    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1080
    :goto_1
    const-string v0, "01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1081
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1082
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o:Landroid/widget/ImageView;

    const v3, 0x7f02005e

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1083
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a04f2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1084
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a00ac

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->y:Ljava/lang/String;

    .line 1100
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o:Landroid/widget/ImageView;

    new-instance v3, Lahs;

    invoke-direct {v3, p0, v2, p1}, Lahs;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Lcom/sec/android/GeoLookout/db/DisasterInfo;I)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1128
    const v0, 0x7f0e003d

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->p:Landroid/widget/TextView;

    .line 1130
    const-string v0, "01"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1132
    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDisasterSource()Ljava/lang/String;

    move-result-object v0

    .line 1134
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Disaster source : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 1136
    if-nez v0, :cond_5

    .line 1137
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1085
    :cond_2
    const-string v0, "04"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1086
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1087
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1088
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1089
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->y:Ljava/lang/String;

    goto :goto_2

    .line 1090
    :cond_3
    const-string v0, "02"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1091
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1092
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->y:Ljava/lang/String;

    goto :goto_2

    .line 1093
    :cond_4
    const-string v0, "03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1094
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1095
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1096
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1097
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0a00ae

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->y:Ljava/lang/String;

    goto/16 :goto_2

    .line 1143
    :cond_5
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1144
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1147
    :cond_6
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_7
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public static synthetic g(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->r(I)V

    return-void
.end method

.method public static synthetic h(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    return-object v0
.end method

.method private h()Lvs;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1153
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 1156
    if-nez v0, :cond_0

    move-object v0, v1

    .line 1164
    :goto_0
    return-object v0

    .line 1159
    :cond_0
    const v2, 0x7f0e003a

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/MapFragment;

    .line 1161
    if-nez v0, :cond_1

    move-object v0, v1

    .line 1162
    goto :goto_0

    .line 1164
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapFragment;->c()Lvs;

    move-result-object v0

    goto :goto_0
.end method

.method private h(I)V
    .locals 1

    .prologue
    .line 1327
    const-string v0, "endAnimation()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1328
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->E:Laks;

    if-eqz v0, :cond_0

    .line 1329
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->E:Laks;

    invoke-virtual {v0}, Laks;->b()V

    .line 1331
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->C:Laic;

    if-eqz v0, :cond_1

    .line 1332
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->C:Laic;

    invoke-virtual {v0, p1}, Laic;->removeMessages(I)V

    .line 1334
    :cond_1
    return-void
.end method

.method public static synthetic h(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->q(I)V

    return-void
.end method

.method public static synthetic i(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 1337
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->j()V

    .line 1338
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->E:Laks;

    if-eqz v0, :cond_0

    .line 1339
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->E:Laks;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-virtual {v0, v1}, Laks;->c(Landroid/content/Context;)V

    .line 1341
    :cond_0
    return-void
.end method

.method private i(I)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    .line 1617
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->x:Z

    if-nez v0, :cond_1

    .line 1618
    const-string v0, "setMapInformation isGooglePlayServicesAvailable false"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 1687
    :cond_0
    :goto_0
    return-void

    .line 1622
    :cond_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 1623
    iget v1, v0, Laif;->n:I

    if-nez v1, :cond_2

    .line 1624
    iget-object v1, v0, Laif;->u:Lamf;

    if-eqz v1, :cond_0

    .line 1625
    iget-object v0, v0, Laif;->u:Lamf;

    invoke-direct {p0, v0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lamf;I)V

    goto :goto_0

    .line 1630
    :cond_2
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v1

    iput-object v1, v0, Laif;->l:Lamf;

    .line 1631
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v2, v0, Laif;->l:Lamf;

    iget-wide v2, v2, Lamf;->a:D

    iget-object v4, v0, Laif;->l:Lamf;

    iget-wide v4, v4, Lamf;->b:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->w:Lcom/google/android/gms/maps/model/LatLng;

    .line 1634
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a:I

    .line 1636
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s(I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1637
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MULTIPOINT setMapInformation withPolygon "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 1639
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-direct {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v1

    .line 1640
    iget v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a:I

    if-le v1, v2, :cond_3

    .line 1641
    iput v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a:I

    .line 1665
    :cond_3
    :goto_1
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getPolling()S

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 1666
    const-string v1, "setMapInformation : getPolling() is true"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 1668
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-direct {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I

    move-result v1

    .line 1669
    iget v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a:I

    if-le v1, v2, :cond_4

    .line 1670
    iput v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a:I

    .line 1674
    :cond_4
    iget-object v1, v0, Laif;->u:Lamf;

    if-eqz v1, :cond_5

    .line 1675
    iget-object v1, v0, Laif;->l:Lamf;

    iget-object v2, v0, Laif;->u:Lamf;

    invoke-static {v1, v2}, Lall;->a(Lamf;Lamf;)I

    move-result v1

    .line 1676
    iget v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a:I

    if-le v1, v2, :cond_5

    .line 1677
    iput v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a:I

    .line 1681
    :cond_5
    iget v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a:I

    if-ltz v1, :cond_9

    .line 1682
    iget-object v0, v0, Laif;->l:Lamf;

    iget v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a:I

    invoke-direct {p0, v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(Lamf;I)V

    goto/16 :goto_0

    .line 1644
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MULTIPOINT setMapInformation default "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / MapPinIcon : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->z:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 1646
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->E:Laks;

    if-nez v1, :cond_7

    .line 1647
    new-instance v1, Laks;

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    iget v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->z:I

    invoke-direct {v1, v2, v3}, Laks;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->E:Laks;

    .line 1651
    :cond_7
    const-string v1, "03"

    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1652
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v2

    iget-object v3, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v3

    invoke-static {v1, v2, v3}, Lall;->b(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v1

    .line 1658
    :goto_2
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->E:Laks;

    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    new-instance v4, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v4}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    iget-object v5, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->w:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v4

    invoke-virtual {v4, v6, v6}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v4

    const v5, 0x7f02015a

    invoke-static {v5}, Lzx;->a(I)Lzw;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lzw;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    invoke-virtual {v3, v1}, Lvs;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Laah;

    move-result-object v1

    iput-object v1, v2, Laks;->c:Laah;

    .line 1662
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->C:Laic;

    invoke-virtual {v1, p1}, Laic;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 1655
    :cond_8
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v1

    invoke-static {v1}, Lall;->c(I)I

    move-result v1

    .line 1656
    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 1684
    :cond_9
    iget-object v0, v0, Laif;->l:Lamf;

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lamf;)V

    goto/16 :goto_0
.end method

.method public static synthetic j(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Laic;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->C:Laic;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 1344
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1345
    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h(I)V

    .line 1344
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1347
    :cond_0
    return-void
.end method

.method private j(I)V
    .locals 11

    .prologue
    const v10, 0x7f020059

    const v9, 0x7f020058

    const v8, 0x7f0e0022

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 1690
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 1691
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setLayoutVisibility(), mDisasterState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Laif;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 1692
    const-string v1, "03"

    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v1

    const/16 v2, 0x69

    if-ne v1, v2, :cond_0

    .line 1693
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->m(I)V

    .line 1697
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0c0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 1701
    iget v2, v0, Laif;->n:I

    packed-switch v2, :pswitch_data_0

    .line 1754
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1757
    :goto_1
    return-void

    .line 1695
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l(I)V

    goto :goto_0

    .line 1704
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08008f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1705
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v2

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08008f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/widget/TabWidget;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1707
    const/16 v2, 0xf1

    const/16 v3, 0x60

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lall;->a(IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1708
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 1709
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1710
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v3, 0x7f0200a6

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1711
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v3, 0x7f0e001f

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1712
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v3, 0x7f0e0024

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1714
    if-lez p1, :cond_1

    .line 1715
    iget-object v1, v0, Laif;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1716
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v3, 0x7f0e002d

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1717
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e002d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f020057

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1723
    :goto_2
    iget-object v1, v0, Laif;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1725
    iget-object v1, v0, Laif;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1726
    iget-object v0, v0, Laif;->w:Landroid/view/View;

    const v1, 0x7f0e0025

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 1719
    :cond_1
    iget-object v1, v0, Laif;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1720
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v3, 0x7f0e002a

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1721
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e002a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f020054

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 1731
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0800a6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1732
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v2

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0800a6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/widget/TabWidget;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1733
    const/16 v2, 0xff

    const/16 v3, 0xc4

    invoke-static {v1, v2, v3, v6}, Lall;->a(IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1734
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 1735
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v3, 0x7f0e0027

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1736
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1737
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e0027

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f0200a5

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1738
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v2, 0x7f0200a6

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1739
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e001f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1740
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e0024

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1744
    iget-object v1, v0, Laif;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1745
    iget-object v1, v0, Laif;->t:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1746
    iget-object v1, v0, Laif;->s:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1748
    iget-object v1, v0, Laif;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1749
    iget-object v0, v0, Laif;->w:Landroid/view/View;

    const v1, 0x7f0e0025

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 1701
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static synthetic k(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/widget/TabHost;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    return-object v0
.end method

.method private k(I)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2034
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 2035
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getGoogleMapURL(), myLocation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Laif;->u:Lamf;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 2037
    iget-object v1, v0, Laif;->u:Lamf;

    if-nez v1, :cond_0

    .line 2038
    const/4 v0, 0x0

    .line 2048
    :goto_0
    return-object v0

    .line 2040
    :cond_0
    iget-object v1, v0, Laif;->u:Lamf;

    iget-wide v2, v1, Lamf;->a:D

    .line 2041
    iget-object v0, v0, Laif;->u:Lamf;

    iget-wide v0, v0, Lamf;->b:D

    .line 2043
    sget-boolean v4, Laky;->c:Z

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 2044
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "http://mo.amap.com/?q="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2046
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "http://maps.google.com/maps?f=q&q=("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")&z=15"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private k()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1373
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->j()V

    .line 1374
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 1375
    iput-object v2, v0, Laif;->l:Lamf;

    .line 1376
    iput-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    .line 1377
    iput-object v2, v0, Laif;->u:Lamf;

    goto :goto_0

    .line 1381
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    if-eqz v0, :cond_1

    .line 1382
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    invoke-virtual {v0}, Lvs;->f()V

    .line 1383
    iput-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    .line 1386
    :cond_1
    iput-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->D:Lzw;

    .line 1388
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->E:Laks;

    if-eqz v0, :cond_2

    .line 1389
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->E:Laks;

    invoke-virtual {v0}, Laks;->a()V

    .line 1390
    iput-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->E:Laks;

    .line 1392
    :cond_2
    return-void
.end method

.method public static synthetic l(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o:Landroid/widget/ImageView;

    return-object v0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1820
    const-string v0, "START - shareScreenShot"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1822
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    if-nez v0, :cond_0

    .line 1823
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h()Lvs;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    .line 1826
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    if-eqz v0, :cond_1

    .line 1827
    new-instance v0, Laih;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Laih;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Lagz;)V

    .line 1828
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    invoke-virtual {v1, v0}, Lvs;->a(Lwr;)V

    .line 1830
    :cond_1
    return-void
.end method

.method private l(I)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x8

    .line 2088
    const-string v0, "setDisasterDescription(), START"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 2090
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 2091
    iget v1, v0, Laif;->n:I

    if-nez v1, :cond_2

    .line 2092
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v2, 0x7f0e004e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Laif;->e:Landroid/widget/TextView;

    .line 2093
    if-nez p1, :cond_1

    .line 2096
    iget-object v0, v0, Laif;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a055f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2163
    :cond_0
    :goto_0
    return-void

    .line 2098
    :cond_1
    iget-object v1, v0, Laif;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2099
    iget-object v0, v0, Laif;->w:Landroid/view/View;

    const v1, 0x7f0e004d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2102
    :cond_2
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    if-eqz v1, :cond_0

    .line 2106
    iget-object v1, v0, Laif;->b:Landroid/widget/TextView;

    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-static {v2}, Lall;->b(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2107
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getStartTime()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lall;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 2108
    iget-object v2, v0, Laif;->c:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0044

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2110
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getEndTime()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lall;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 2111
    iget-object v2, v0, Laif;->d:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0040

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2113
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v2

    .line 2115
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-static {v1}, Lall;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2116
    iget-object v1, v0, Laif;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2117
    iget-object v1, v0, Laif;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2118
    iget-object v1, v0, Laif;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2119
    iget-object v1, v0, Laif;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2121
    iget-object v1, v0, Laif;->a:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2160
    :cond_3
    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "disaster Type ID: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " DescriptionState:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Laif;->a:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mDescriptionTime:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Laif;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mDescriptionLocation:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Laif;->b:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mDescriptionEffective:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Laif;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2124
    :cond_4
    iget-object v1, v0, Laif;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 2125
    const/4 v3, -0x2

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2126
    iget-object v3, v0, Laif;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2127
    iget-object v1, v0, Laif;->a:Landroid/widget/TextView;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 2128
    sparse-switch v2, :sswitch_data_0

    .line 2150
    iget-object v1, v0, Laif;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2154
    :goto_2
    const-string v1, "03"

    iget-object v3, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2155
    iget-object v1, v0, Laif;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2156
    iget-object v1, v0, Laif;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2130
    :sswitch_0
    iget-object v1, v0, Laif;->a:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2131
    iget-object v1, v0, Laif;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2132
    iget-object v1, v0, Laif;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 2135
    :sswitch_1
    iget-object v1, v0, Laif;->a:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2136
    iget-object v1, v0, Laif;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2137
    iget-object v1, v0, Laif;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 2141
    :sswitch_2
    iget-object v1, v0, Laif;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 2145
    :sswitch_3
    iget-object v1, v0, Laif;->a:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2146
    iget-object v1, v0, Laif;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 2128
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_1
        0x69 -> :sswitch_0
        0x6e -> :sswitch_3
        0x70 -> :sswitch_3
        0x71 -> :sswitch_2
        0xce -> :sswitch_2
    .end sparse-switch
.end method

.method private m()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2373
    const-string v0, "initializeProgressDialog"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 2375
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->G:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 2376
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->G:Landroid/app/ProgressDialog;

    .line 2379
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->G:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 2380
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->G:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 2381
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->G:Landroid/app/ProgressDialog;

    new-instance v1, Lahy;

    invoke-direct {v1, p0}, Lahy;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 2387
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->G:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 2388
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->G:Landroid/app/ProgressDialog;

    new-instance v1, Lahz;

    invoke-direct {v1, p0}, Lahz;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2394
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->G:Landroid/app/ProgressDialog;

    const v1, 0x7f0a0490

    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 2395
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->G:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 2399
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    if-eqz v0, :cond_3

    .line 2400
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v0

    move v1, v0

    .line 2403
    :goto_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    iget v4, v0, Laif;->m:I

    .line 2404
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    iget-object v0, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v5

    .line 2406
    if-nez v4, :cond_1

    .line 2408
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/DisasterService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2409
    const-string v1, "com.sec.android.GeoLookout.ACTION_REQUEST_CURRENT_LOCATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2410
    const-string v1, "reuqest_new_location"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2411
    const-string v1, "force_high_accuracy"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2412
    const-string v1, "uid"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2413
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 2447
    :goto_1
    return-void

    .line 2415
    :cond_1
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->I:Ljava/util/Timer;

    .line 2416
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v4}, Lall;->i(Landroid/content/Context;I)Lamf;

    move-result-object v0

    .line 2417
    new-instance v3, Landroid/location/Location;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "uid"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 2419
    if-eqz v0, :cond_2

    .line 2420
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MagicPd location :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v6, v0, Lamf;->a:D

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 2421
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MagicPd location :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v6, v0, Lamf;->b:D

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 2422
    iget-wide v6, v0, Lamf;->a:D

    invoke-virtual {v3, v6, v7}, Landroid/location/Location;->setLatitude(D)V

    .line 2423
    iget-wide v0, v0, Lamf;->b:D

    invoke-virtual {v3, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    .line 2426
    :cond_2
    new-instance v0, Laia;

    invoke-direct {v0, p0}, Laia;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->H:Ljava/util/TimerTask;

    .line 2443
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->c:Lanx;

    invoke-virtual/range {v0 .. v5}, Lalu;->a(Landroid/content/Context;Lanx;Landroid/location/Location;ILjava/lang/String;)V

    .line 2445
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->I:Ljava/util/Timer;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->H:Ljava/util/TimerTask;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_1

    :cond_3
    move v1, v2

    goto/16 :goto_0
.end method

.method private m(I)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2166
    const-string v0, "setDescriptionJpnEarthquake()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 2167
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 2168
    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 2169
    iget-object v2, v0, Laif;->w:Landroid/view/View;

    const v3, 0x7f0e0014

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 2171
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2172
    const v3, 0x7f030007

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2174
    iget-object v1, v0, Laif;->w:Landroid/view/View;

    const v3, 0x7f0e0035

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2175
    iget-object v3, v0, Laif;->w:Landroid/view/View;

    const v4, 0x7f0e0037

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 2176
    iget-object v4, v0, Laif;->w:Landroid/view/View;

    const v5, 0x7f0e0038

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 2178
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsZoneCity()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a00bb

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v11, [Ljava/lang/Object;

    iget-object v8, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v8}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsLevel()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2179
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v5}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getCity()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0562

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " %.1f"

    new-array v6, v11, [Ljava/lang/Object;

    iget-object v7, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailsMagnitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00ba

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v11, [Ljava/lang/Object;

    iget-object v7, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsDepth()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2182
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    iget-object v0, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getStartTime()J

    move-result-wide v6

    invoke-static {v1, v6, v7}, Lall;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 2183
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0a0044

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v10, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2185
    invoke-virtual {v2, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2186
    invoke-virtual {v2, v11}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 2187
    return-void
.end method

.method public static synthetic m(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n()V

    return-void
.end method

.method private n(I)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    .line 2190
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 2191
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v2

    .line 2193
    const-string v1, ""

    .line 2194
    iget-object v3, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-static {v3}, Lall;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Z

    move-result v3

    .line 2196
    sparse-switch v2, :sswitch_data_0

    .line 2241
    :cond_0
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v3, :cond_1

    .line 2242
    iget-object v0, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getHeadline()Ljava/lang/String;

    move-result-object v1

    .line 2245
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDescriptionDetail : bUseHeadline:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " detail : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 2246
    return-object v1

    .line 2199
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    iget-object v4, v0, Laif;->u:Lamf;

    invoke-static {v1, v2, v4, v6}, Lall;->a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;Lamf;Z)I

    move-result v1

    .line 2201
    if-eqz v3, :cond_2

    .line 2202
    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getHeadline()Ljava/lang/String;

    move-result-object v2

    .line 2203
    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p0, v1}, Lall;->f(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 2205
    :cond_2
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 2207
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0562

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2208
    const-string v4, " %.1f"

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v6, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailsMagnitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2209
    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2211
    invoke-static {p0, v1}, Lall;->e(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2213
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 2217
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    iget-object v4, v0, Laif;->u:Lamf;

    invoke-static {v1, v2, v4, v6}, Lall;->a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;Lamf;Z)I

    move-result v1

    .line 2219
    if-eqz v3, :cond_3

    .line 2220
    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getHeadline()Ljava/lang/String;

    move-result-object v2

    .line 2221
    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p0, v1}, Lall;->f(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 2223
    :cond_3
    invoke-static {p0, v1}, Lall;->e(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 2228
    :sswitch_2
    if-nez v3, :cond_0

    .line 2229
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailSpeed()Ljava/lang/String;

    move-result-object v1

    .line 2230
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2231
    const-string v1, "0"

    .line 2233
    :cond_4
    invoke-static {p0, v1}, Lall;->i(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 2196
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_1
        0x69 -> :sswitch_0
        0x6e -> :sswitch_2
        0x70 -> :sswitch_2
    .end sparse-switch
.end method

.method public static synthetic n(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->I:Ljava/util/Timer;

    return-object v0
.end method

.method private n()V
    .locals 1

    .prologue
    .line 2482
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->G:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->G:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2483
    const-string v0, "dbUpdateComplete() : ProgressDialog dismiss "

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 2484
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->G:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2486
    :cond_0
    return-void
.end method

.method public static synthetic o(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->G:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private o()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2539
    const-string v0, "showCancelDialog()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 2540
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0029

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0034

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lahe;

    invoke-direct {v2, p0}, Lahe;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 2548
    new-instance v1, Lahf;

    invoke-direct {v1, p0}, Lahf;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 2560
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 2561
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 2563
    return-void
.end method

.method private o(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2608
    const-string v0, "getMyLocation()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 2610
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 2611
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-static {v1}, Lanb;->c(Landroid/content/Context;)Lamf;

    move-result-object v1

    iput-object v1, v0, Laif;->u:Lamf;

    .line 2613
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/GeoLookout/DisasterService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2614
    const-string v2, "com.sec.android.GeoLookout.ACTION_REQUEST_CURRENT_LOCATION"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2615
    const-string v2, "reuqest_new_location"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2616
    const-string v2, "force_high_accuracy"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2617
    const-string v2, "uid"

    iget v0, v0, Laif;->m:I

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2618
    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 2619
    return-void
.end method

.method public static synthetic p(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/TimerTask;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->H:Ljava/util/TimerTask;

    return-object v0
.end method

.method private p()V
    .locals 3

    .prologue
    .line 2566
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2567
    const v1, 0x7f0a003e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0491

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 2568
    const v1, 0x7f0a004b

    new-instance v2, Lahg;

    invoke-direct {v2, p0}, Lahg;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2575
    const v1, 0x7f0a003d

    new-instance v2, Lahh;

    invoke-direct {v2, p0}, Lahh;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2604
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 2605
    return-void
.end method

.method private p(I)V
    .locals 6

    .prologue
    .line 2757
    const-string v0, "callHistoryActivity"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 2758
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 2762
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2763
    iget-object v2, v0, Laif;->u:Lamf;

    if-eqz v2, :cond_0

    .line 2764
    const-string v2, "Latitude"

    iget-object v3, v0, Laif;->u:Lamf;

    iget-wide v4, v3, Lamf;->a:D

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 2765
    const-string v2, "Logitude"

    iget-object v3, v0, Laif;->u:Lamf;

    iget-wide v4, v3, Lamf;->b:D

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 2766
    const-string v2, "uid"

    iget v0, v0, Laif;->m:I

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2776
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2781
    :goto_0
    return-void

    .line 2777
    :catch_0
    move-exception v0

    .line 2778
    const-string v1, "ActivityNotFoundException "

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    .line 2779
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private q()V
    .locals 0

    .prologue
    .line 2729
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->m()V

    .line 2730
    return-void
.end method

.method private q(I)V
    .locals 3

    .prologue
    .line 3030
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const-string v1, "pref_tip_not_supported_languages_dialog"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lall;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 3031
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const-string v1, "pref_tip_not_supported_languages_dialog"

    invoke-static {v0, v1, p1}, Lall;->b(Landroid/content/Context;Ljava/lang/String;I)V

    .line 3033
    :cond_0
    return-void
.end method

.method public static synthetic q(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o()V

    return-void
.end method

.method private r()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2831
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 2832
    const-string v0, ""

    .line 2834
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getNotSupportedHeadlineMsg current set language : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 2836
    new-instance v0, Ljava/util/Locale;

    const-string v2, "ar"

    const-string v3, "AE"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "bn"

    const-string v3, "IN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "ca"

    const-string v3, "ES"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "hr"

    const-string v3, "HR"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "cs"

    const-string v3, "CZ"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "da"

    const-string v3, "DK"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "nl"

    const-string v3, "NL"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "en"

    const-string v3, "GB"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "en"

    const-string v3, "US"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "fa"

    const-string v3, "IR"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "tl"

    const-string v3, "PH"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "fi"

    const-string v3, "FI"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "fr"

    const-string v3, "FR"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "fr"

    const-string v3, "CA"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "de"

    const-string v3, "DE"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "el"

    const-string v3, "GR"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "iw"

    const-string v3, "IL"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "hi"

    const-string v3, "IN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "hu"

    const-string v3, "HU"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "in"

    const-string v3, "ID"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "it"

    const-string v3, "IT"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "ja"

    const-string v3, "JP"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "kk"

    const-string v3, "KZ"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "ko"

    const-string v3, "KR"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "ms"

    const-string v3, "MY"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "nb"

    const-string v3, "NO"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "pl"

    const-string v3, "PL"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "pt"

    const-string v3, "PT"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "pt"

    const-string v3, "BR"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "ro"

    const-string v3, "RO"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "ru"

    const-string v3, "RU"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "zh"

    const-string v3, "CN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "sk"

    const-string v3, "SK"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "es"

    const-string v3, "ES"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "sv"

    const-string v3, "SE"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "th"

    const-string v3, "TH"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "zh"

    const-string v3, "HK"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "zh"

    const-string v3, "TW"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "tr"

    const-string v3, "TR"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "uk"

    const-string v3, "UA"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "ur"

    const-string v3, "PK"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "vi"

    const-string v3, "VN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2879
    :cond_0
    const/4 v0, 0x0

    .line 2948
    :goto_0
    return-object v0

    .line 2880
    :cond_1
    new-instance v0, Ljava/util/Locale;

    const-string v2, "sq"

    const-string v3, "AL"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2882
    new-instance v0, Ljava/util/Locale;

    const-string v2, "it"

    const-string v3, "IT"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/Locale;

    const-string v3, "it"

    const-string v4, "IT"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 2940
    :goto_1
    if-eqz v0, :cond_2

    .line 2941
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getNotSupportedHeadlineMsg mapping language : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 2948
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a04b2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0029

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x2

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2883
    :cond_3
    new-instance v0, Ljava/util/Locale;

    const-string v2, "hy"

    const-string v3, "AM"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/util/Locale;

    const-string v2, "az"

    const-string v3, "AZ"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/util/Locale;

    const-string v2, "ky"

    const-string v3, "KG"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/util/Locale;

    const-string v2, "tk"

    const-string v3, "TM"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/util/Locale;

    const-string v2, "uz"

    const-string v3, "UZ"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2889
    :cond_4
    new-instance v0, Ljava/util/Locale;

    const-string v2, "ru"

    const-string v3, "RU"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/Locale;

    const-string v3, "ru"

    const-string v4, "RU"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2890
    :cond_5
    new-instance v0, Ljava/util/Locale;

    const-string v2, "as"

    const-string v3, "IN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/util/Locale;

    const-string v2, "ka"

    const-string v3, "GE"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/util/Locale;

    const-string v2, "kn"

    const-string v3, "IN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/util/Locale;

    const-string v2, "ml"

    const-string v3, "IN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/util/Locale;

    const-string v2, "mn"

    const-string v3, "MN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/util/Locale;

    const-string v2, "ne"

    const-string v3, "IN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/util/Locale;

    const-string v2, "or"

    const-string v3, "IN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/util/Locale;

    const-string v2, "si"

    const-string v3, "IN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/util/Locale;

    const-string v2, "ta"

    const-string v3, "IN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/util/Locale;

    const-string v2, "te"

    const-string v3, "IN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2901
    :cond_6
    new-instance v0, Ljava/util/Locale;

    const-string v2, "en"

    const-string v3, "GB"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/Locale;

    const-string v3, "en"

    const-string v4, "GB"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2902
    :cond_7
    new-instance v0, Ljava/util/Locale;

    const-string v2, "eu"

    const-string v3, "ES"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_8

    new-instance v0, Ljava/util/Locale;

    const-string v2, "gl"

    const-string v3, "ES"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_8

    new-instance v0, Ljava/util/Locale;

    const-string v2, "es"

    const-string v3, "US"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2906
    :cond_8
    new-instance v0, Ljava/util/Locale;

    const-string v2, "es"

    const-string v3, "ES"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/Locale;

    const-string v3, "es"

    const-string v4, "ES"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2907
    :cond_9
    new-instance v0, Ljava/util/Locale;

    const-string v2, "bg"

    const-string v3, "BG"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/util/Locale;

    const-string v2, "en"

    const-string v3, "PH"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/util/Locale;

    const-string v2, "et"

    const-string v3, "EE"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/util/Locale;

    const-string v2, "is"

    const-string v3, "IS"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/util/Locale;

    const-string v2, "ga"

    const-string v3, "IE"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/util/Locale;

    const-string v2, "km"

    const-string v3, "KH"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/util/Locale;

    const-string v2, "lo"

    const-string v3, "LA"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/util/Locale;

    const-string v2, "lv"

    const-string v3, "LV"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/util/Locale;

    const-string v2, "lt"

    const-string v3, "LT"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/util/Locale;

    const-string v2, "mk"

    const-string v3, "MK"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/util/Locale;

    const-string v2, "my"

    const-string v3, "MM"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/util/Locale;

    const-string v2, "sr"

    const-string v3, "RS"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/util/Locale;

    const-string v2, "sl"

    const-string v3, "SI"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2921
    :cond_a
    new-instance v0, Ljava/util/Locale;

    const-string v2, "en"

    const-string v3, "US"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/Locale;

    const-string v3, "en"

    const-string v4, "US"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2922
    :cond_b
    new-instance v0, Ljava/util/Locale;

    const-string v2, "gu"

    const-string v3, "IN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_c

    new-instance v0, Ljava/util/Locale;

    const-string v2, "mr"

    const-string v3, "IN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_c

    new-instance v0, Ljava/util/Locale;

    const-string v2, "pa"

    const-string v3, "IN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2926
    :cond_c
    new-instance v0, Ljava/util/Locale;

    const-string v2, "hi"

    const-string v3, "IN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/Locale;

    const-string v3, "hi"

    const-string v4, "IN"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2927
    :cond_d
    new-instance v0, Ljava/util/Locale;

    const-string v2, "tg"

    const-string v3, "TJ"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 2929
    new-instance v0, Ljava/util/Locale;

    const-string v2, "fa"

    const-string v3, "IR"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/Locale;

    const-string v3, "fa"

    const-string v4, "IR"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2930
    :cond_e
    new-instance v0, Ljava/util/Locale;

    const-string v2, "no"

    const-string v3, "NO"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2932
    new-instance v0, Ljava/util/Locale;

    const-string v2, "nb"

    const-string v3, "NO"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/Locale;

    const-string v3, "nb"

    const-string v4, "NO"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2933
    :cond_f
    new-instance v0, Ljava/util/Locale;

    const-string v2, "de"

    const-string v3, "AT"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 2935
    new-instance v0, Ljava/util/Locale;

    const-string v2, "de"

    const-string v3, "DE"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/Locale;

    const-string v3, "de"

    const-string v4, "DE"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 2937
    :cond_10
    new-instance v0, Ljava/util/Locale;

    const-string v2, "en"

    const-string v3, "GB"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/Locale;

    const-string v3, "en"

    const-string v4, "GB"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method

.method private r(I)V
    .locals 3

    .prologue
    .line 3044
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const-string v1, "pref_headling_not_supported_languages_dialog"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lall;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 3045
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const-string v1, "pref_headling_not_supported_languages_dialog"

    invoke-static {v0, v1, p1}, Lall;->b(Landroid/content/Context;Ljava/lang/String;I)V

    .line 3047
    :cond_0
    return-void
.end method

.method public static synthetic r(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->f()V

    return-void
.end method

.method public static synthetic s(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s:I

    return v0
.end method

.method private s()Ljava/lang/String;
    .locals 6

    .prologue
    .line 2953
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 2954
    const-string v0, ""

    .line 2956
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getNotSupportedTipMsg current set language : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 2958
    new-instance v0, Ljava/util/Locale;

    const-string v2, "ur"

    const-string v3, "PK"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Locale;

    const-string v2, "bn"

    const-string v3, "IN"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2961
    :cond_0
    new-instance v0, Ljava/util/Locale;

    const-string v2, "en"

    const-string v3, "GB"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/Locale;

    const-string v3, "en"

    const-string v4, "GB"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 2972
    :goto_0
    if-eqz v0, :cond_1

    .line 2973
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getNotSupportedTipMsg mapping language : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 2976
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a04b2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0029

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x2

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    .line 2962
    :cond_2
    new-instance v0, Ljava/util/Locale;

    const-string v2, "tl"

    const-string v3, "PH"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2964
    new-instance v0, Ljava/util/Locale;

    const-string v2, "en"

    const-string v3, "US"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/Locale;

    const-string v3, "en"

    const-string v4, "US"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2965
    :cond_3
    new-instance v0, Ljava/util/Locale;

    const-string v2, "kk"

    const-string v3, "KZ"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2967
    new-instance v0, Ljava/util/Locale;

    const-string v2, "ru"

    const-string v3, "RU"

    const-string v4, ""

    invoke-direct {v0, v2, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/Locale;

    const-string v3, "ru"

    const-string v4, "RU"

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/Locale;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2969
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private s(I)Z
    .locals 2

    .prologue
    .line 3051
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 3052
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    if-eqz v1, :cond_0

    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getPolygonList()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getPolygonList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 3053
    const/4 v0, 0x1

    .line 3056
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()I
    .locals 3

    .prologue
    .line 3036
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const-string v1, "pref_tip_not_supported_languages_dialog"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lall;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private t(I)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 3388
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 3389
    const-string v7, ""

    .line 3390
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 3392
    new-instance v1, Landroid/location/Geocoder;

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    .line 3393
    iget-object v2, v0, Laif;->u:Lamf;

    iget-wide v2, v2, Lamf;->a:D

    .line 3394
    iget-object v0, v0, Laif;->u:Lamf;

    iget-wide v4, v0, Lamf;->b:D

    .line 3397
    const/4 v6, 0x1

    :try_start_0
    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v0

    .line 3398
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3399
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    move v1, v8

    .line 3400
    :goto_0
    invoke-virtual {v0}, Landroid/location/Address;->getMaxAddressLineIndex()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    if-ge v1, v2, :cond_1

    .line 3401
    invoke-virtual {v0, v1}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 3402
    invoke-virtual {v0, v1}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3400
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3406
    :cond_1
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3411
    :goto_1
    return-object v0

    .line 3408
    :catch_0
    move-exception v0

    .line 3409
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v7

    goto :goto_1
.end method

.method private u()I
    .locals 3

    .prologue
    .line 3040
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const-string v1, "pref_headling_not_supported_languages_dialog"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lall;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private v()V
    .locals 9

    .prologue
    const v1, 0x7f0a0495

    const v8, 0x7f0a0020

    const v7, 0x7f0a0029

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 3097
    const-string v0, ""

    .line 3098
    const-string v0, ""

    .line 3100
    sget-boolean v0, Laky;->j:Z

    if-eqz v0, :cond_0

    .line 3101
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 3103
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0496

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3110
    :goto_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lahq;

    invoke-direct {v2, p0}, Lahq;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v2, Lahp;

    invoke-direct {v2, p0}, Lahp;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 3126
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 3127
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 3128
    return-void

    .line 3106
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 3107
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0496

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 2052
    const-string v0, "getEmergencyContact"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 2054
    sget-object v0, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ICE"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "contacts"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "emergency"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "defaultId"

    const-string v2, "3"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 2057
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v0, ""

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2058
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "data1"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "display_name"

    aput-object v3, v2, v0

    .line 2062
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2066
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2067
    if-eqz v1, :cond_2

    .line 2068
    const/4 v0, -0x1

    :try_start_1
    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 2069
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2070
    const-string v0, "data1"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2071
    invoke-interface {v1}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2072
    const-string v0, ","

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 2076
    :catch_0
    move-exception v0

    .line 2077
    :goto_1
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2079
    if-eqz v1, :cond_1

    .line 2080
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2084
    :cond_1
    :goto_2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2079
    :cond_2
    if-eqz v1, :cond_1

    .line 2080
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 2079
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    if-eqz v1, :cond_3

    .line 2080
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 2079
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 2076
    :catch_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method public a(Laah;)Z
    .locals 2

    .prologue
    .line 3418
    invoke-virtual {p1}, Laah;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, ""

    invoke-virtual {p1}, Laah;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3419
    invoke-virtual {p1}, Laah;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3420
    invoke-virtual {p1}, Laah;->h()V

    .line 3426
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 3423
    :cond_1
    invoke-virtual {p1}, Laah;->g()V

    goto :goto_0
.end method

.method public createTabContent(Ljava/lang/String;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 3148
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    move v1, v2

    .line 3153
    :goto_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 3154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    iget v0, v0, Laif;->m:I

    if-ne v0, v4, :cond_2

    .line 3155
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    move v2, v1

    move-object v1, v0

    .line 3161
    :goto_1
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 3163
    invoke-direct {p0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(I)V

    .line 3165
    if-eqz v1, :cond_1

    .line 3166
    iget v4, v1, Laif;->n:I

    if-nez v4, :cond_4

    .line 3167
    const v4, 0x7f03000e

    invoke-virtual {v0, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 3168
    iput-object v3, v1, Laif;->w:Landroid/view/View;

    .line 3169
    invoke-direct {p0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l(I)V

    .line 3170
    const v0, 0x7f0e004b

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 3171
    sget-boolean v4, Laky;->b:Z

    if-eqz v4, :cond_3

    .line 3172
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a00c0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3176
    :cond_0
    :goto_2
    iget v0, v1, Laif;->m:I

    if-nez v0, :cond_1

    .line 3177
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->e:Lanw;

    invoke-virtual {v0, p0, v1, v2}, Lalu;->a(Landroid/content/Context;Lanw;I)V

    .line 3190
    :cond_1
    :goto_3
    return-object v3

    .line 3153
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3173
    :cond_3
    iget v4, v1, Laif;->m:I

    if-eqz v4, :cond_0

    .line 3174
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a04f8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 3181
    :cond_4
    const v4, 0x7f030006

    invoke-virtual {v0, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 3182
    iput-object v3, v1, Laif;->w:Landroid/view/View;

    .line 3183
    invoke-direct {p0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->d(I)V

    .line 3184
    invoke-direct {p0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->e(I)V

    .line 3185
    invoke-direct {p0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->f(I)V

    .line 3186
    invoke-direct {p0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->j(I)V

    goto :goto_3

    :cond_5
    move-object v1, v3

    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1012
    packed-switch p1, :pswitch_data_0

    .line 1053
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1054
    return-void

    .line 1015
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1016
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->k()V

    .line 1017
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->f()V

    .line 1018
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->invalidateOptionsMenu()V

    .line 1019
    if-eqz p3, :cond_3

    .line 1020
    const-string v0, "multi_point_edit_id"

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1021
    if-eqz v0, :cond_2

    .line 1022
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lalz;->f(Landroid/content/Context;I)I

    move-result v0

    .line 1023
    if-lez v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TabWidget;->getTabCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1025
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v1, v0}, Landroid/widget/TabHost;->setCurrentTab(I)V

    goto :goto_0

    .line 1027
    :cond_1
    iget v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s:I

    invoke-direct {p0, v0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(II)V

    .line 1028
    invoke-direct {p0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->i(I)V

    goto :goto_0

    .line 1031
    :cond_2
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 1032
    if-eqz v0, :cond_0

    iget v1, v0, Laif;->n:I

    if-eqz v1, :cond_0

    .line 1033
    iget-object v0, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationType()I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s:I

    .line 1034
    iget v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s:I

    invoke-direct {p0, v0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(II)V

    .line 1035
    invoke-direct {p0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->i(I)V

    goto :goto_0

    .line 1039
    :cond_3
    const-string v0, "MULTIPOINT onActivityResult data is null"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1040
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 1041
    if-eqz v0, :cond_0

    iget v1, v0, Laif;->n:I

    if-eqz v1, :cond_0

    .line 1042
    iget-object v0, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationType()I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s:I

    .line 1043
    iget v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s:I

    invoke-direct {p0, v0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(II)V

    goto :goto_0

    .line 1012
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 1406
    const-string v0, "onBackPressed!"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1407
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->finish()V

    .line 1408
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1411
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onClick : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1413
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lalz;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v0

    .line 1417
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 1493
    :goto_1
    :sswitch_0
    return-void

    :cond_0
    move v0, v1

    .line 1413
    goto :goto_0

    .line 1419
    :sswitch_1
    invoke-direct {p0, v3, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(II)V

    goto :goto_1

    .line 1423
    :sswitch_2
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->q()V

    goto :goto_1

    .line 1427
    :sswitch_3
    sget-boolean v2, Laky;->a:Z

    if-eq v2, v3, :cond_1

    sget-boolean v2, Laky;->b:Z

    if-eq v2, v3, :cond_1

    sget-boolean v2, Laky;->c:Z

    if-ne v2, v3, :cond_2

    :cond_1
    invoke-static {}, Lane;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1428
    invoke-direct {p0, v1, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(II)V

    goto :goto_1

    .line 1430
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t()I

    move-result v2

    const/16 v3, 0x15

    if-eq v2, v3, :cond_4

    .line 1431
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s()Ljava/lang/String;

    move-result-object v2

    .line 1433
    if-eqz v2, :cond_3

    .line 1434
    const/16 v0, 0x28

    invoke-direct {p0, v2, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/lang/String;I)V

    goto :goto_1

    .line 1436
    :cond_3
    invoke-direct {p0, v1, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(II)V

    goto :goto_1

    .line 1439
    :cond_4
    invoke-direct {p0, v1, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(II)V

    goto :goto_1

    .line 1446
    :sswitch_4
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.contacts.action.SHOW_EMERGENCY_CONTACT_LIST_DIALOG"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1447
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 1451
    :sswitch_5
    const-string v2, "Click - send OK message"

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 1452
    if-lez v0, :cond_6

    .line 1453
    sget-boolean v1, Lakv;->bQ:Z

    if-eqz v1, :cond_5

    .line 1454
    invoke-direct {p0, v5, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->d(II)V

    goto :goto_1

    .line 1456
    :cond_5
    invoke-direct {p0, v5, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->c(II)V

    goto :goto_1

    .line 1459
    :cond_6
    sget-boolean v2, Lakv;->bQ:Z

    if-eqz v2, :cond_7

    .line 1460
    invoke-direct {p0, v1, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->d(II)V

    goto :goto_1

    .line 1462
    :cond_7
    invoke-direct {p0, v1, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->c(II)V

    goto :goto_1

    .line 1468
    :sswitch_6
    const-string v1, "Click - send need help message"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 1469
    if-lez v0, :cond_9

    .line 1470
    sget-boolean v1, Lakv;->bQ:Z

    if-eqz v1, :cond_8

    .line 1471
    invoke-direct {p0, v4, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->d(II)V

    goto :goto_1

    .line 1473
    :cond_8
    invoke-direct {p0, v4, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->c(II)V

    goto :goto_1

    .line 1476
    :cond_9
    sget-boolean v1, Lakv;->bQ:Z

    if-eqz v1, :cond_a

    .line 1477
    invoke-direct {p0, v3, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->d(II)V

    goto/16 :goto_1

    .line 1479
    :cond_a
    invoke-direct {p0, v3, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->c(II)V

    goto/16 :goto_1

    .line 1487
    :sswitch_7
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l()V

    goto/16 :goto_1

    .line 1417
    :sswitch_data_0
    .sparse-switch
        0x7f0e000a -> :sswitch_0
        0x7f0e0014 -> :sswitch_1
        0x7f0e0021 -> :sswitch_3
        0x7f0e0026 -> :sswitch_2
        0x7f0e0029 -> :sswitch_4
        0x7f0e002c -> :sswitch_7
        0x7f0e0030 -> :sswitch_6
        0x7f0e0032 -> :sswitch_5
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2734
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2736
    sget-boolean v0, Laky;->b:Z

    if-nez v0, :cond_0

    sget-boolean v0, Laky;->a:Z

    if-eqz v0, :cond_1

    .line 2737
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->r:Lagw;

    if-eqz v0, :cond_1

    .line 2738
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->r:Lagw;

    invoke-virtual {v0, p1}, Lagw;->a(Landroid/content/res/Configuration;)V

    .line 2742
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 225
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 227
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b()V

    .line 229
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 231
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    .line 232
    new-instance v0, Laic;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v3}, Laic;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Lagz;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->C:Laic;

    .line 236
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lxa;->a(Landroid/content/Context;)V
    :try_end_0
    .catch Lcd; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :goto_0
    invoke-static {p0}, Lcf;->a(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->x:Z

    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GoogleMapAvailable : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->x:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 244
    sget-object v0, Lapd;->a:Lalb;

    if-eqz v0, :cond_0

    .line 245
    sget-object v0, Lapd;->a:Lalb;

    invoke-virtual {v0}, Lalb;->a()V

    .line 249
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->f()V

    .line 251
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "location_mode"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 252
    if-nez v0, :cond_1

    .line 253
    const-string v0, "location mode is LOCATION_MODE_OFF! request allow location!"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 254
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    const/16 v4, 0xa

    const/16 v5, 0x3e9

    invoke-virtual {v0, v3, v4, v5}, Lanf;->a(Landroid/content/Context;II)V

    .line 257
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->c()V

    .line 259
    sget-boolean v0, Laky;->a:Z

    if-eq v0, v1, :cond_2

    sget-boolean v0, Laky;->b:Z

    if-eq v0, v1, :cond_2

    sget-boolean v0, Laky;->c:Z

    if-ne v0, v1, :cond_8

    :cond_2
    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_8

    .line 270
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 272
    iget v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->O:I

    if-gez v0, :cond_4

    .line 273
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lalx;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->O:I

    .line 276
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MULTIPOINT Tab setting "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->O:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TabWidget;->getTabCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 278
    iget v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->O:I

    if-nez v0, :cond_9

    .line 279
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 280
    if-eqz v0, :cond_5

    iget v1, v0, Laif;->n:I

    if-eqz v1, :cond_5

    .line 281
    iget-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationType()I

    move-result v1

    iput v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s:I

    .line 282
    iget v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s:I

    invoke-direct {p0, v1, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(II)V

    .line 283
    invoke-direct {p0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->i(I)V

    .line 284
    iget-object v0, v0, Laif;->u:Lamf;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lamf;I)V

    .line 297
    :cond_5
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MULTIPOINT : mMultiPointUid "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->O:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 299
    :cond_6
    return-void

    .line 237
    :catch_0
    move-exception v0

    .line 238
    invoke-virtual {v0}, Lcd;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 241
    goto/16 :goto_1

    .line 262
    :cond_8
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->u()I

    move-result v0

    const/16 v1, 0xb

    if-eq v0, v1, :cond_3

    .line 263
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->r()Ljava/lang/String;

    move-result-object v0

    .line 264
    if-eqz v0, :cond_3

    .line 265
    const/16 v1, 0x1e

    invoke-direct {p0, v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Ljava/lang/String;I)V

    goto/16 :goto_2

    .line 287
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->O:I

    invoke-static {v0, v1}, Lalz;->f(Landroid/content/Context;I)I

    move-result v0

    .line 288
    if-lez v0, :cond_a

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TabWidget;->getTabCount()I

    move-result v1

    if-ge v0, v1, :cond_a

    .line 290
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v1, v0}, Landroid/widget/TabHost;->setCurrentTab(I)V

    goto :goto_3

    .line 292
    :cond_a
    iget v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s:I

    invoke-direct {p0, v0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(II)V

    goto :goto_3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 774
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0d0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 775
    const/4 v0, 0x1

    return v0
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 769
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 1396
    const-string v0, "onDestroy()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1397
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1398
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->d:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1399
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->k()V

    .line 1400
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->C:Laic;

    .line 1401
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1402
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const v4, 0x7f0a0495

    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 901
    sget-boolean v0, Laky;->b:Z

    if-nez v0, :cond_0

    sget-boolean v0, Laky;->a:Z

    if-eqz v0, :cond_1

    .line 902
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->r:Lagw;

    invoke-virtual {v0, p1}, Lagw;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1007
    :goto_0
    return v6

    .line 907
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 910
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->finish()V

    goto :goto_0

    .line 914
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->q()V

    goto :goto_0

    .line 918
    :sswitch_2
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l()V

    goto :goto_0

    .line 924
    :sswitch_3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.contacts.action.INTERACTION_EMERGENCY_MESSAGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 925
    const-string v1, "from_safety_setting"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 926
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 930
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 934
    :sswitch_5
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l()V

    goto :goto_0

    .line 938
    :sswitch_6
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->v()V

    goto :goto_0

    .line 942
    :sswitch_7
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "http://www.samsung.com/geonews/"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 943
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 949
    :sswitch_8
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-static {v0}, Lall;->D(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 950
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 955
    :sswitch_9
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.EMERGENCY_START_SERVICE_BY_ORDER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 956
    const-string v1, "enabled"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 957
    const-string v1, "flag"

    const/16 v2, 0x100

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 958
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 962
    :sswitch_a
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/settings/DisasterSettingsFilterListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 963
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 967
    :sswitch_b
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 968
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 971
    :sswitch_c
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    iget v0, v0, Laif;->n:I

    if-eqz v0, :cond_2

    .line 972
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->i()V

    .line 974
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->p()V

    goto/16 :goto_0

    .line 977
    :sswitch_d
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 978
    const-string v2, "multi_point_edit_id"

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v3}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    iget v0, v0, Laif;->m:I

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 979
    const/4 v0, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 982
    :sswitch_e
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/activity/OpenSourceLicenseActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 983
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 987
    :sswitch_f
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v6, :cond_3

    .line 988
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v0

    .line 990
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    iget v0, v0, Laif;->m:I

    invoke-static {v2, v0}, Lalx;->d(Landroid/content/Context;I)V

    .line 991
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a004a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 995
    :sswitch_10
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "safety_care_disaster_user_agree"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 996
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.GeoLookout.SETTING_CHANGE_START"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 997
    const-string v2, "isEnable"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 998
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 999
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a04b8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 907
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0e0102 -> :sswitch_d
        0x7f0e0103 -> :sswitch_c
        0x7f0e0104 -> :sswitch_1
        0x7f0e0105 -> :sswitch_2
        0x7f0e0106 -> :sswitch_3
        0x7f0e0107 -> :sswitch_6
        0x7f0e0108 -> :sswitch_4
        0x7f0e0109 -> :sswitch_7
        0x7f0e010a -> :sswitch_8
        0x7f0e010b -> :sswitch_e
        0x7f0e010c -> :sswitch_9
        0x7f0e010d -> :sswitch_a
        0x7f0e010e -> :sswitch_b
        0x7f0e0115 -> :sswitch_f
        0x7f0e0116 -> :sswitch_10
        0x7f0e0118 -> :sswitch_5
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 1362
    const-string v0, "onPause()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1363
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->G:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 1364
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->G:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1365
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    invoke-virtual {v0}, Lalu;->e()V

    .line 1366
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->G:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1369
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1370
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2746
    invoke-super {p0, p1}, Landroid/app/Activity;->onPostCreate(Landroid/os/Bundle;)V

    .line 2748
    sget-boolean v0, Laky;->b:Z

    if-nez v0, :cond_0

    sget-boolean v0, Laky;->a:Z

    if-eqz v0, :cond_1

    .line 2749
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->r:Lagw;

    if-eqz v0, :cond_1

    .line 2750
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->r:Lagw;

    invoke-virtual {v0}, Lagw;->b()V

    .line 2754
    :cond_1
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 12

    .prologue
    const v11, 0x7f0e0106

    const v6, 0x7f0e0105

    const v5, 0x7f0e0104

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 783
    .line 785
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lalz;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 787
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v0

    move v1, v0

    .line 789
    :goto_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    iget v3, v0, Laif;->n:I

    .line 791
    const v0, 0x7f0e0101

    invoke-interface {p1, v0, v10}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 793
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->x:Z

    if-nez v0, :cond_0

    .line 794
    const v0, 0x7f0e0101

    invoke-interface {p1, v0, v2}, Landroid/view/Menu;->setGroupEnabled(IZ)V

    .line 798
    :cond_0
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v4, 0x7f0a0099

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 801
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-static {v0}, Lalz;->c(Landroid/content/Context;)I

    move-result v0

    const/4 v4, 0x3

    if-le v0, v4, :cond_1

    .line 802
    const v0, 0x7f0e010e

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 805
    :cond_1
    if-eqz v1, :cond_7

    .line 806
    const v0, 0x7f0e0103

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 807
    const v0, 0x7f0e0102

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 808
    if-ne v3, v10, :cond_2

    .line 809
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 811
    :cond_2
    const v0, 0x7f0e0109

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 812
    invoke-interface {p1, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 819
    :goto_1
    if-nez v3, :cond_8

    .line 820
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 821
    invoke-interface {p1, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 822
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 823
    const v0, 0x7f0e010c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 831
    :cond_3
    :goto_2
    invoke-static {}, Land;->a()Land;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-virtual {v0, v4}, Land;->c(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v10, :cond_c

    .line 832
    const v0, 0x7f0e010f

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v4

    .line 833
    const v0, 0x7f0e010f

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const-string v5, "<!-Test Only Mode-!>"

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 834
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-static {v0}, Lall;->A(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 835
    const v0, 0x7f0e0110

    invoke-interface {v4, v0}, Landroid/view/SubMenu;->removeItem(I)V

    .line 840
    :goto_3
    const v0, 0x7f0e0116

    invoke-interface {v4, v0}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0495

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v10, [Ljava/lang/Object;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0029

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " & "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a04b8

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 844
    if-ltz v1, :cond_4

    .line 845
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 847
    if-eqz v0, :cond_4

    .line 848
    if-nez v1, :cond_b

    .line 849
    const v1, 0x7f0e0112

    invoke-interface {v4, v1}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Zone ID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->l:Landroid/content/Context;

    invoke-static {v6}, Lall;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 854
    :goto_4
    iget-object v1, v0, Laif;->u:Lamf;

    if-eqz v1, :cond_4

    .line 855
    const v1, 0x7f0e0113

    invoke-interface {v4, v1}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Lat : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Laif;->u:Lamf;

    iget-wide v6, v6, Lamf;->a:D

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 857
    const v1, 0x7f0e0114

    invoke-interface {v4, v1}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Long : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v0, v0, Laif;->u:Lamf;

    iget-wide v6, v0, Lamf;->b:D

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 864
    :cond_4
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 865
    iget v1, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 866
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 867
    const v5, 0x7f0e0117

    invoke-interface {v4, v5}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "/"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 876
    :goto_5
    sget-boolean v0, Laky;->b:Z

    if-nez v0, :cond_5

    .line 877
    const v0, 0x7f0e010a

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 878
    const v0, 0x7f0e010c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 879
    const v0, 0x7f0e010d

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 882
    :cond_5
    const v0, 0x7f0e0107

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 883
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0a0495

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v4, v10, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0029

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 885
    if-eqz v3, :cond_6

    .line 886
    sget-boolean v0, Laky;->d:Z

    if-eqz v0, :cond_6

    sget-boolean v0, Laky;->e:Z

    if-eqz v0, :cond_6

    .line 887
    const-string v0, "onCreate() : usa-vzw"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 888
    invoke-interface {p1, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 889
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a049a

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 892
    :cond_6
    sget-boolean v0, Laky;->t:Z

    if-eqz v0, :cond_d

    .line 893
    const v0, 0x7f0e010b

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 897
    :goto_6
    return v10

    .line 814
    :cond_7
    const v0, 0x7f0e0103

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 815
    const v0, 0x7f0e0102

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 816
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_1

    .line 824
    :cond_8
    const/4 v0, 0x3

    if-ne v3, v0, :cond_9

    .line 825
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 826
    const v0, 0x7f0e010c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 827
    :cond_9
    const/4 v0, 0x2

    if-ne v3, v0, :cond_3

    .line 828
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 837
    :cond_a
    const v0, 0x7f0e0111

    invoke-interface {v4, v0}, Landroid/view/SubMenu;->removeItem(I)V

    goto/16 :goto_3

    .line 852
    :cond_b
    const v1, 0x7f0e0112

    invoke-interface {v4, v1}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Zone ID : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Laif;->x:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto/16 :goto_4

    .line 868
    :catch_0
    move-exception v0

    .line 869
    const v0, 0x7f0e0117

    invoke-interface {v4, v0}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_5

    .line 873
    :cond_c
    const v0, 0x7f0e010f

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_5

    .line 895
    :cond_d
    const v0, 0x7f0e010b

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_6

    :cond_e
    move v1, v2

    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 1351
    const-string v0, " onResume "

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1352
    invoke-static {p0}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1353
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->d()V

    .line 1357
    :cond_1
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1358
    return-void

    .line 1354
    :cond_2
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->F:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 1355
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->F:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    goto :goto_0
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 8

    .prologue
    const v7, 0x7f0800a6

    const v6, 0x7f08008f

    const/4 v2, 0x0

    const v5, 0x7f080018

    .line 3196
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MULTIPOINT onTabChanged "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 3200
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    move v1, v2

    .line 3201
    :goto_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 3202
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    iget v0, v0, Laif;->m:I

    if-ne v0, v3, :cond_0

    .line 3208
    :goto_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 3209
    if-ne v2, v1, :cond_1

    .line 3210
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    iget-object v0, v0, Laif;->y:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0800ab

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 3208
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3201
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3212
    :cond_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    iget-object v0, v0, Laif;->y:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080097

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 3217
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->j()V

    .line 3218
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->x:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    if-eqz v0, :cond_3

    .line 3219
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->t:Lvs;

    invoke-virtual {v0}, Lvs;->f()V

    .line 3222
    :cond_3
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->M:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 3223
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MULTIPOINT onTabChanged "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Laif;->n:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 3225
    iget v2, v0, Laif;->n:I

    if-nez v2, :cond_4

    .line 3226
    iget v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s:I

    invoke-direct {p0, v2, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(II)V

    .line 3227
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3228
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v2

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/widget/TabWidget;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 3229
    iget-object v2, v0, Laif;->u:Lamf;

    invoke-direct {p0, v2, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lamf;I)V

    .line 3230
    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Laif;)V

    .line 3231
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(ILcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 3259
    :goto_3
    return-void

    .line 3234
    :cond_4
    invoke-direct {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(I)V

    .line 3235
    iget v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s:I

    invoke-direct {p0, v2, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(II)V

    .line 3236
    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationType()I

    move-result v2

    iput v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->s:I

    .line 3237
    iget v2, v0, Laif;->n:I

    packed-switch v2, :pswitch_data_0

    .line 3247
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3248
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v2

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/widget/TabWidget;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 3251
    :goto_4
    iget-object v2, v0, Laif;->u:Lamf;

    invoke-direct {p0, v2, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lamf;I)V

    .line 3252
    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v2

    iget-object v0, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-direct {p0, v2, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(ILcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 3253
    invoke-direct {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->i(I)V

    .line 3256
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->invalidateOptionsMenu()V

    goto :goto_3

    .line 3239
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3240
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v2

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/widget/TabWidget;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    .line 3243
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 3244
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->N:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v2

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Landroid/widget/TabWidget;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    :cond_5
    move v1, v2

    goto/16 :goto_1

    .line 3237
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
