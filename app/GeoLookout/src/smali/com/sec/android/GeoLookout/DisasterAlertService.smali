.class public Lcom/sec/android/GeoLookout/DisasterAlertService;
.super Landroid/app/Service;
.source "DisasterAlertService.java"


# static fields
.field private static final a:I = 0x2710


# instance fields
.field private b:Landroid/hardware/scontext/SContextListener;

.field private c:Ljava/util/Timer;

.field private d:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 148
    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/DisasterAlertService;)Landroid/hardware/scontext/SContextListener;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterAlertService;->b:Landroid/hardware/scontext/SContextListener;

    return-object v0
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/DisasterAlertService;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/android/GeoLookout/DisasterAlertService;->c:Ljava/util/Timer;

    return-object p1
.end method

.method private a(J)V
    .locals 7

    .prologue
    .line 125
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 126
    add-long v2, v0, p1

    .line 128
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setAlarm : timeNow "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timeInterval "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 130
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/DisasterAlertService;->c()Landroid/app/PendingIntent;

    move-result-object v1

    .line 132
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/DisasterAlertService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 133
    const/4 v4, 0x0

    invoke-virtual {v0, v4, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 134
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 56
    const-class v1, Lcom/sec/android/GeoLookout/DisasterAlertService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 57
    const-string v1, "com.sec.android.GeoLookout.DisasterAlertService.CancelCheckLocationAlarm"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 59
    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/DisasterAlertService;J)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/DisasterAlertService;->a(J)V

    return-void
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/DisasterAlertService;J)J
    .locals 1

    .prologue
    .line 44
    iput-wide p1, p0, Lcom/sec/android/GeoLookout/DisasterAlertService;->d:J

    return-wide p1
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/DisasterAlertService;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterAlertService;->c:Ljava/util/Timer;

    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 62
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 63
    const-class v1, Lcom/sec/android/GeoLookout/DisasterAlertService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 64
    const-string v1, "com.sec.android.GeoLookout.DisasterAlertService.CheckLocation"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 66
    return-void
.end method

.method public static synthetic c(Lcom/sec/android/GeoLookout/DisasterAlertService;)J
    .locals 2

    .prologue
    .line 44
    iget-wide v0, p0, Lcom/sec/android/GeoLookout/DisasterAlertService;->d:J

    return-wide v0
.end method

.method private c()Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 116
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 117
    const-class v1, Lcom/sec/android/GeoLookout/DisasterAlertService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 118
    const-string v1, "com.sec.android.GeoLookout.DisasterAlertService.CheckLocation"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 120
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 121
    return-object v0
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 137
    const-string v0, "cancelAlarm()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 139
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/DisasterAlertService;->c()Landroid/app/PendingIntent;

    move-result-object v1

    .line 140
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/DisasterAlertService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 142
    if-eqz v1, :cond_0

    .line 143
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 144
    invoke-virtual {v1}, Landroid/app/PendingIntent;->cancel()V

    .line 146
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/16 v6, 0x19

    .line 166
    const-wide/32 v2, 0x36ee80

    iput-wide v2, p0, Lcom/sec/android/GeoLookout/DisasterAlertService;->d:J

    .line 168
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterAlertService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_0

    const-string v2, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    const-string v0, "isDrivingMode: System do NOT have com.sec.feature.sensorhub feature"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    move v0, v1

    .line 219
    :goto_0
    return v0

    .line 174
    :cond_0
    const-string v0, "scontext"

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/DisasterAlertService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/scontext/SContextManager;

    .line 175
    invoke-virtual {v0, v6}, Landroid/hardware/scontext/SContextManager;->isAvailableService(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 176
    const-string v0, "isDrivingMode: SContextManager is NOT available for SContext.TYPE_ACTIVITY_TRACKER"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    move v0, v1

    .line 177
    goto :goto_0

    .line 180
    :cond_1
    new-instance v1, Lagc;

    invoke-direct {v1, p0}, Lagc;-><init>(Lcom/sec/android/GeoLookout/DisasterAlertService;)V

    .line 182
    new-instance v2, Ljava/util/Timer;

    invoke-direct {v2}, Ljava/util/Timer;-><init>()V

    iput-object v2, p0, Lcom/sec/android/GeoLookout/DisasterAlertService;->c:Ljava/util/Timer;

    .line 183
    iget-object v2, p0, Lcom/sec/android/GeoLookout/DisasterAlertService;->c:Ljava/util/Timer;

    const-wide/16 v4, 0x2710

    invoke-virtual {v2, v1, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 185
    new-instance v1, Lagb;

    invoke-direct {v1, p0, v0}, Lagb;-><init>(Lcom/sec/android/GeoLookout/DisasterAlertService;Landroid/hardware/scontext/SContextManager;)V

    iput-object v1, p0, Lcom/sec/android/GeoLookout/DisasterAlertService;->b:Landroid/hardware/scontext/SContextListener;

    .line 216
    iget-object v1, p0, Lcom/sec/android/GeoLookout/DisasterAlertService;->b:Landroid/hardware/scontext/SContextListener;

    invoke-virtual {v0, v1, v6}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    .line 217
    iget-object v1, p0, Lcom/sec/android/GeoLookout/DisasterAlertService;->b:Landroid/hardware/scontext/SContextListener;

    invoke-virtual {v0, v1, v6}, Landroid/hardware/scontext/SContextManager;->requestToUpdate(Landroid/hardware/scontext/SContextListener;I)V

    .line 219
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 71
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 112
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 113
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onStartCommand intent "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 77
    if-nez p1, :cond_0

    .line 102
    :goto_0
    return v2

    .line 81
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 83
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 85
    const-string v1, "com.sec.android.GeoLookout.DisasterAlertService.CheckLocation"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterAlertService;->a()V

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterAlertService;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 89
    const-wide/32 v0, 0x36ee80

    invoke-direct {p0, v0, v1}, Lcom/sec/android/GeoLookout/DisasterAlertService;->a(J)V

    .line 92
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/DisasterService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 93
    const-string v1, "com.sec.android.GeoLookout.ACTION_CHECK_CURRENT_LOCATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 94
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/DisasterAlertService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 100
    :cond_2
    :goto_1
    invoke-virtual {p0, p3}, Lcom/sec/android/GeoLookout/DisasterAlertService;->stopSelf(I)V

    goto :goto_0

    .line 96
    :cond_3
    const-string v1, "com.sec.android.GeoLookout.DisasterAlertService.CancelCheckLocationAlarm"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterAlertService;->a()V

    goto :goto_1
.end method
