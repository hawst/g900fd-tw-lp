.class public Lcom/sec/android/GeoLookout/db/DisasterInfo;
.super Ljava/lang/Object;
.source "DisasterInfo.java"


# instance fields
.field private final mActionStatus:S

.field private final mChnDetailsCity:Ljava/lang/String;

.field private final mChnDetailsEqType:Ljava/lang/String;

.field private final mChnDetailsProvince:Ljava/lang/String;

.field private final mChnDetailsStationName:Ljava/lang/String;

.field private final mChnSignificanceLevel:I

.field private final mCity:Ljava/lang/String;

.field private final mCountry:Ljava/lang/String;

.field private final mDV:Ljava/lang/String;

.field private final mDataType:S

.field private final mDetailDirectionCardinal:Ljava/lang/String;

.field private final mDetailDirectionDegrees:I

.field private final mDetailForecastHour:I

.field private final mDetailPressureMB:I

.field private final mDetailStormSpeed:I

.field private final mDetailWindGust:I

.field private final mDetailWindMax:I

.field private final mDetailsMagnitude:D

.field private final mDetailsSpeed:Ljava/lang/String;

.field private final mDid:Ljava/lang/String;

.field private final mDisasterSource:Ljava/lang/String;

.field private final mDistance:I

.field private final mEndTime:J

.field private final mHTHPId:Ljava/lang/String;

.field private final mHeadline:Ljava/lang/String;

.field private final mId:I

.field private final mJpnDetailsDepth:Ljava/lang/String;

.field private final mJpnDetailsLevel:Ljava/lang/String;

.field private final mJpnDetailsZoneCity:Ljava/lang/String;

.field private final mJpnDetailsZoneCountry:Ljava/lang/String;

.field private final mLink:Ljava/lang/String;

.field private final mLinkedBeDid:Ljava/lang/String;

.field private final mLocation:Lamf;

.field private final mLocationID:Ljava/lang/String;

.field private final mLocationType:I

.field private final mMsgId:Ljava/lang/String;

.field private final mMultiPointUid:I

.field private final mNotiStatus:S

.field private final mPolling:S

.field private final mPolygonJSON:Ljava/lang/String;

.field private final mPolygonList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lamf;",
            ">;"
        }
    .end annotation
.end field

.field private final mReceivedTime:J

.field private final mSignificance:I

.field private final mStartTime:J

.field private final mTipId:I

.field private final mTipLinkId:I

.field private final mType:Ljava/lang/String;

.field private final mTypeId:I

.field private final mWarningIcon:I

.field private final mWatchIcon:I


# direct methods
.method private constructor <init>(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)V
    .locals 2

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mId:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$000(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mId:I

    .line 91
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDid:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$100(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDid:Ljava/lang/String;

    .line 92
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mStartTime:J
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$200(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mStartTime:J

    .line 93
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mEndTime:J
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$300(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mEndTime:J

    .line 94
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mReceivedTime:J
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$400(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mReceivedTime:J

    .line 95
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocation:Lamf;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$500(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Lamf;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLocation:Lamf;

    .line 96
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mHeadline:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$600(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mHeadline:Ljava/lang/String;

    .line 97
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mType:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$700(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mType:Ljava/lang/String;

    .line 98
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mTypeId:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$800(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mTypeId:I

    .line 99
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDV:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$900(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDV:Ljava/lang/String;

    .line 100
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLink:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$1000(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLink:Ljava/lang/String;

    .line 101
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailsMagnitude:D
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$1100(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailsMagnitude:D

    .line 103
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mSignificance:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$1200(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mSignificance:I

    .line 104
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mActionStatus:S
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$1300(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)S

    move-result v0

    iput-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mActionStatus:S

    .line 105
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocationID:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$1400(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLocationID:Ljava/lang/String;

    .line 106
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLocationType:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$1500(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLocationType:I

    .line 107
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolygonList:Ljava/util/ArrayList;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$1600(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mPolygonList:Ljava/util/ArrayList;

    .line 108
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolygonJSON:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$1700(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mPolygonJSON:Ljava/lang/String;

    .line 109
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mCity:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$1800(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mCity:Ljava/lang/String;

    .line 110
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mCountry:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$1900(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mCountry:Ljava/lang/String;

    .line 111
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDistance:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$2000(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDistance:I

    .line 112
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mPolling:S
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$2100(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)S

    move-result v0

    iput-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mPolling:S

    .line 113
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mNotiStatus:S
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$2200(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)S

    move-result v0

    iput-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mNotiStatus:S

    .line 114
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailPressureMB:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$2300(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailPressureMB:I

    .line 115
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailsSpeed:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$2400(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailsSpeed:Ljava/lang/String;

    .line 116
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mLinkedBEDid:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$2500(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLinkedBeDid:Ljava/lang/String;

    .line 117
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mHTHPId:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$2600(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mHTHPId:Ljava/lang/String;

    .line 118
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mMsgId:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$2700(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mMsgId:Ljava/lang/String;

    .line 119
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDisasterSource:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$2800(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDisasterSource:Ljava/lang/String;

    .line 124
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsProvince:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$2900(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsProvince:Ljava/lang/String;

    .line 125
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsCity:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$3000(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsCity:Ljava/lang/String;

    .line 126
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsStationName:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$3100(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsStationName:Ljava/lang/String;

    .line 127
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnDetailsEqType:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$3200(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsEqType:Ljava/lang/String;

    .line 128
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mChnSignificanceLevel:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$3300(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnSignificanceLevel:I

    .line 133
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsDepth:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$3400(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsDepth:Ljava/lang/String;

    .line 134
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsLevel:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$3500(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsLevel:Ljava/lang/String;

    .line 135
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsZoneCountry:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$3600(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsZoneCountry:Ljava/lang/String;

    .line 136
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mJpnDetailsZoneCity:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$3700(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsZoneCity:Ljava/lang/String;

    .line 138
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDataType:S
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$3800(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)S

    move-result v0

    iput-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDataType:S

    .line 139
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailWindGust:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$3900(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailWindGust:I

    .line 140
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailDirectionCardinal:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$4000(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailDirectionCardinal:Ljava/lang/String;

    .line 141
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailDirectionDegrees:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$4100(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailDirectionDegrees:I

    .line 142
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailWindMax:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$4200(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailWindMax:I

    .line 143
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailStormSpeed:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$4300(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailStormSpeed:I

    .line 144
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mDetailForecastHour:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$4400(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailForecastHour:I

    .line 146
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mWatchIcon:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$4500(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mWatchIcon:I

    .line 147
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mWarningIcon:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$4600(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mWarningIcon:I

    .line 148
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mTipId:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$4700(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mTipId:I

    .line 149
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mTipLinkId:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$4800(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mTipLinkId:I

    .line 151
    # getter for: Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->mMultiPointUid:I
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->access$4900(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mMultiPointUid:I

    .line 152
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;Lamb;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;-><init>(Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;)V

    return-void
.end method

.method static synthetic access$5000(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mId:I

    return v0
.end method

.method static synthetic access$5100(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5200(Lcom/sec/android/GeoLookout/db/DisasterInfo;)J
    .locals 2

    .prologue
    .line 13
    iget-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mStartTime:J

    return-wide v0
.end method

.method static synthetic access$5300(Lcom/sec/android/GeoLookout/db/DisasterInfo;)J
    .locals 2

    .prologue
    .line 13
    iget-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mEndTime:J

    return-wide v0
.end method

.method static synthetic access$5400(Lcom/sec/android/GeoLookout/db/DisasterInfo;)J
    .locals 2

    .prologue
    .line 13
    iget-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mReceivedTime:J

    return-wide v0
.end method

.method static synthetic access$5500(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Lamf;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLocation:Lamf;

    return-object v0
.end method

.method static synthetic access$5600(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mHeadline:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5700(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5800(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mTypeId:I

    return v0
.end method

.method static synthetic access$5900(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDV:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6000(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLink:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6100(Lcom/sec/android/GeoLookout/db/DisasterInfo;)D
    .locals 2

    .prologue
    .line 13
    iget-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailsMagnitude:D

    return-wide v0
.end method

.method static synthetic access$6200(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mSignificance:I

    return v0
.end method

.method static synthetic access$6300(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLocationID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6400(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLocationType:I

    return v0
.end method

.method static synthetic access$6500(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mPolygonList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$6600(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mPolygonJSON:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6700(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mCity:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6800(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mCountry:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6900(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDistance:I

    return v0
.end method

.method static synthetic access$7000(Lcom/sec/android/GeoLookout/db/DisasterInfo;)S
    .locals 1

    .prologue
    .line 13
    iget-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mPolling:S

    return v0
.end method

.method static synthetic access$7100(Lcom/sec/android/GeoLookout/db/DisasterInfo;)S
    .locals 1

    .prologue
    .line 13
    iget-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mActionStatus:S

    return v0
.end method

.method static synthetic access$7200(Lcom/sec/android/GeoLookout/db/DisasterInfo;)S
    .locals 1

    .prologue
    .line 13
    iget-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mNotiStatus:S

    return v0
.end method

.method static synthetic access$7300(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailsSpeed:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7400(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLinkedBeDid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7500(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mHTHPId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7600(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mMsgId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7700(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDisasterSource:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7800(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsProvince:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7900(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsCity:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8000(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsStationName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8100(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsEqType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8200(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnSignificanceLevel:I

    return v0
.end method

.method static synthetic access$8300(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsDepth:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8400(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsLevel:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8500(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsZoneCountry:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8600(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsZoneCity:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$8700(Lcom/sec/android/GeoLookout/db/DisasterInfo;)S
    .locals 1

    .prologue
    .line 13
    iget-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDataType:S

    return v0
.end method

.method static synthetic access$8800(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailWindGust:I

    return v0
.end method

.method static synthetic access$8900(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailDirectionCardinal:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9000(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailDirectionDegrees:I

    return v0
.end method

.method static synthetic access$9100(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailWindMax:I

    return v0
.end method

.method static synthetic access$9200(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailStormSpeed:I

    return v0
.end method

.method static synthetic access$9300(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailForecastHour:I

    return v0
.end method

.method static synthetic access$9400(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mWatchIcon:I

    return v0
.end method

.method static synthetic access$9500(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mWarningIcon:I

    return v0
.end method

.method static synthetic access$9600(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mTipId:I

    return v0
.end method

.method static synthetic access$9700(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mTipLinkId:I

    return v0
.end method

.method static synthetic access$9800(Lcom/sec/android/GeoLookout/db/DisasterInfo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mMultiPointUid:I

    return v0
.end method


# virtual methods
.method public getActionStatus()I
    .locals 1

    .prologue
    .line 611
    iget-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mActionStatus:S

    return v0
.end method

.method public getChnDetailsCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 686
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsCity:Ljava/lang/String;

    return-object v0
.end method

.method public getChnDetailsEqType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsEqType:Ljava/lang/String;

    return-object v0
.end method

.method public getChnDetailsProvince()Ljava/lang/String;
    .locals 1

    .prologue
    .line 682
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsProvince:Ljava/lang/String;

    return-object v0
.end method

.method public getChnDetailsStationName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 690
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsStationName:Ljava/lang/String;

    return-object v0
.end method

.method public getChnSignificanceLevel()I
    .locals 1

    .prologue
    .line 698
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnSignificanceLevel:I

    return v0
.end method

.method public getCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mCity:Ljava/lang/String;

    return-object v0
.end method

.method public getCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 635
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mCountry:Ljava/lang/String;

    return-object v0
.end method

.method public getDV()Ljava/lang/String;
    .locals 1

    .prologue
    .line 663
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDV:Ljava/lang/String;

    return-object v0
.end method

.method public getDataType()S
    .locals 1

    .prologue
    .line 721
    iget-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDataType:S

    return v0
.end method

.method public getDetailDirectionCardinal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 729
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailDirectionCardinal:Ljava/lang/String;

    return-object v0
.end method

.method public getDetailDirectionDegrees()I
    .locals 1

    .prologue
    .line 733
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailDirectionDegrees:I

    return v0
.end method

.method public getDetailForecastHour()I
    .locals 1

    .prologue
    .line 745
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailForecastHour:I

    return v0
.end method

.method public getDetailPressureMB()I
    .locals 1

    .prologue
    .line 651
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailPressureMB:I

    return v0
.end method

.method public getDetailSpeed()Ljava/lang/String;
    .locals 1

    .prologue
    .line 655
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailsSpeed:Ljava/lang/String;

    return-object v0
.end method

.method public getDetailStormSpeed()I
    .locals 1

    .prologue
    .line 741
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailStormSpeed:I

    return v0
.end method

.method public getDetailWindGust()I
    .locals 1

    .prologue
    .line 725
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailWindGust:I

    return v0
.end method

.method public getDetailWindMax()I
    .locals 1

    .prologue
    .line 737
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailWindMax:I

    return v0
.end method

.method public getDetailsMagnitude()D
    .locals 2

    .prologue
    .line 603
    iget-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailsMagnitude:D

    return-wide v0
.end method

.method public getDid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 567
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDid:Ljava/lang/String;

    return-object v0
.end method

.method public getDisasterSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 675
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDisasterSource:Ljava/lang/String;

    return-object v0
.end method

.method public getDistance()I
    .locals 1

    .prologue
    .line 639
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDistance:I

    return v0
.end method

.method public getEndTime()J
    .locals 2

    .prologue
    .line 587
    iget-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mEndTime:J

    return-wide v0
.end method

.method public getHTHPId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 667
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mHTHPId:Ljava/lang/String;

    return-object v0
.end method

.method public getHeadline()Ljava/lang/String;
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mHeadline:Ljava/lang/String;

    return-object v0
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 563
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mId:I

    return v0
.end method

.method public getJpnDetailsDepth()Ljava/lang/String;
    .locals 1

    .prologue
    .line 705
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsDepth:Ljava/lang/String;

    return-object v0
.end method

.method public getJpnDetailsLevel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsLevel:Ljava/lang/String;

    return-object v0
.end method

.method public getJpnDetailsZoneCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 717
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsZoneCity:Ljava/lang/String;

    return-object v0
.end method

.method public getJpnDetailsZoneCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 713
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsZoneCountry:Ljava/lang/String;

    return-object v0
.end method

.method public getLink()Ljava/lang/String;
    .locals 1

    .prologue
    .line 595
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLink:Ljava/lang/String;

    return-object v0
.end method

.method public getLinkedBeDid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 659
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLinkedBeDid:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Lamf;
    .locals 1

    .prologue
    .line 579
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLocation:Lamf;

    return-object v0
.end method

.method public getLocationID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 615
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLocationID:Ljava/lang/String;

    return-object v0
.end method

.method public getLocationType()I
    .locals 1

    .prologue
    .line 627
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLocationType:I

    return v0
.end method

.method public getMsgId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mMsgId:Ljava/lang/String;

    return-object v0
.end method

.method public getMultiPointUid()I
    .locals 1

    .prologue
    .line 765
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mMultiPointUid:I

    return v0
.end method

.method public getNotiStatus()S
    .locals 1

    .prologue
    .line 643
    iget-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mNotiStatus:S

    return v0
.end method

.method public getPolling()S
    .locals 1

    .prologue
    .line 647
    iget-short v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mPolling:S

    return v0
.end method

.method public getPolygonJSON()Ljava/lang/String;
    .locals 1

    .prologue
    .line 623
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mPolygonJSON:Ljava/lang/String;

    return-object v0
.end method

.method public getPolygonList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lamf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 619
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mPolygonList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getReceivedTime()J
    .locals 2

    .prologue
    .line 591
    iget-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mReceivedTime:J

    return-wide v0
.end method

.method public getSignificance()I
    .locals 1

    .prologue
    .line 607
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mSignificance:I

    return v0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 583
    iget-wide v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mStartTime:J

    return-wide v0
.end method

.method public getTipId()I
    .locals 1

    .prologue
    .line 761
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mTipId:I

    return v0
.end method

.method public getTipLinkId()I
    .locals 1

    .prologue
    .line 757
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mTipLinkId:I

    return v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 571
    iget-object v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public getTypeId()I
    .locals 1

    .prologue
    .line 575
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mTypeId:I

    return v0
.end method

.method public getWarningIcon()I
    .locals 1

    .prologue
    .line 749
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mWarningIcon:I

    return v0
.end method

.method public getWatchIcon()I
    .locals 1

    .prologue
    .line 753
    iget v0, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mWatchIcon:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 771
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 772
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\n mID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mHTHPId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mHTHPId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mDID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mStartTime : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailsMagnitude:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mEndTime : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mReceivedTime:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mReceivedTime : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mStartTime:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mLocation : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLocation:Lamf;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mHeadline : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mHeadline:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mDV : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDV:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mLink : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLink:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mDetailsMagnitude : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mEndTime:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mSignificance : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mSignificance:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mLocationID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLocationID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mLocationType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mLocationType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mPolygonList : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mPolygonList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mPolygonJSON : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mPolygonJSON:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mCity : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mCity:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mCountry : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mCountry:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mDistance : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDistance:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mPolling : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mPolling:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mActionStatus : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mActionStatus:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mNotiStatus : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mNotiStatus:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mMsgId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mMsgId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mDisasterSource : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDisasterSource:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mChnDetailsProvince : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsProvince:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mChnDetailsCity : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsCity:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mChnDetailsStationName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsStationName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mChnDetailsEqType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnDetailsEqType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mChnSignificanceLevel : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mChnSignificanceLevel:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mJpnDetailsDepth : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsDepth:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mJpnDetailsLevel : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsLevel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mJpnDetailsZoneCountry : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsZoneCountry:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mJpnDetailsZoneCity : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mJpnDetailsZoneCity:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mDataType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-short v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDataType:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mDetailWindGust : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailWindGust:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mDetailDirectionCardinal : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailDirectionCardinal:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mDetailDirectionDegrees : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailDirectionDegrees:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mDetailWindMax : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailWindMax:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mDetailForecastHour : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailForecastHour:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mDetailPressureMB : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailPressureMB:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mDetailsSpeed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mDetailsSpeed:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n mMultiPointUid : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/GeoLookout/db/DisasterInfo;->mMultiPointUid:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 815
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
