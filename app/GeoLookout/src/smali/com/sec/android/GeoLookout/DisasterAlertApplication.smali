.class public Lcom/sec/android/GeoLookout/DisasterAlertApplication;
.super Landroid/app/Application;
.source "DisasterAlertApplication.java"


# static fields
.field private static b:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private a:Ljava/lang/Thread$UncaughtExceptionHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->b:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 39
    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/DisasterAlertApplication;)Ljava/lang/Thread$UncaughtExceptionHandler;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 73
    const-class v1, Lcom/sec/android/GeoLookout/DisasterAlertApplication;

    monitor-enter v1

    :try_start_0
    const-string v0, "power"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 74
    sget-object v2, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->b:Landroid/os/PowerManager$WakeLock;

    if-nez v2, :cond_0

    .line 75
    if-eqz v0, :cond_2

    .line 76
    const/4 v2, 0x1

    const-string v3, "GeoNews.mWakeLock"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->b:Landroid/os/PowerManager$WakeLock;

    .line 77
    sget-object v0, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->b:Landroid/os/PowerManager$WakeLock;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 84
    :cond_0
    sget-object v0, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->b:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_3

    .line 85
    const-string v0, "Gettting WL was failed"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    :cond_1
    :goto_0
    monitor-exit v1

    return-void

    .line 79
    :cond_2
    :try_start_1
    const-string v0, "Gettting PM was failed"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 88
    :cond_3
    if-ne p1, v4, :cond_4

    .line 90
    :try_start_2
    const-string v0, "Acquire WL"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 91
    sget-object v0, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    goto :goto_0

    .line 94
    :cond_4
    const-string v0, "Release WL"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 95
    sget-object v0, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96
    sget-object v0, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 97
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->b:Landroid/os/PowerManager$WakeLock;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public onCreate()V
    .locals 3

    .prologue
    .line 28
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    const-string v2, "log/geolookout_log/geolookout.log"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    invoke-static {p0}, Lalj;->a(Landroid/content/Context;)V

    .line 32
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 33
    new-instance v0, Laga;

    invoke-direct {v0, p0}, Laga;-><init>(Lcom/sec/android/GeoLookout/DisasterAlertApplication;)V

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lage;->c(Landroid/content/Context;)V

    .line 35
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 36
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    invoke-static {p0}, Lalo;->a(Landroid/content/Context;)Lalo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lalu;->a(Lalo;)V

    .line 37
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 61
    const-string v0, "onLowMemory()"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 62
    invoke-super {p0}, Landroid/app/Application;->onLowMemory()V

    .line 63
    return-void
.end method

.method public onTerminate()V
    .locals 2

    .prologue
    .line 50
    const-string v0, "onTerminate()"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 51
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->a(Landroid/content/Context;Z)V

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lage;->x(Landroid/content/Context;)V

    .line 53
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/DisasterService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 54
    const-string v1, "com.sec.android.GeoLookout.ACTION_CLOSE_LOCATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 56
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 57
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1

    .prologue
    .line 67
    const-string v0, "onTrimMemory()"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 68
    invoke-super {p0, p1}, Landroid/app/Application;->onTrimMemory(I)V

    .line 69
    return-void
.end method
