.class public Lcom/sec/android/GeoLookout/settings/DisasterSettingsFilterListActivity;
.super Landroid/preference/PreferenceActivity;
.source "DisasterSettingsFilterListActivity.java"


# static fields
.field public static final a:Ljava/lang/String; = "DisasterSettingsFilterListActivity"

.field private static b:Landroid/content/Context;

.field private static c:Landroid/widget/TextView;

.field private static d:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 69
    return-void
.end method

.method public static synthetic a()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/GeoLookout/settings/DisasterSettingsFilterListActivity;->d:Landroid/widget/ListView;

    return-object v0
.end method

.method public static synthetic a(Landroid/widget/ListView;)Landroid/widget/ListView;
    .locals 0

    .prologue
    .line 25
    sput-object p0, Lcom/sec/android/GeoLookout/settings/DisasterSettingsFilterListActivity;->d:Landroid/widget/ListView;

    return-object p0
.end method

.method public static synthetic a(Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0

    .prologue
    .line 25
    sput-object p0, Lcom/sec/android/GeoLookout/settings/DisasterSettingsFilterListActivity;->c:Landroid/widget/TextView;

    return-object p0
.end method

.method public static synthetic b()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/GeoLookout/settings/DisasterSettingsFilterListActivity;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method public static synthetic c()Landroid/content/Context;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/android/GeoLookout/settings/DisasterSettingsFilterListActivity;->b:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 148
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onBackPressed()V

    .line 149
    const-string v0, "onBackPressed()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 150
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/settings/DisasterSettingsFilterListActivity;->finish()V

    .line 151
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 35
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    sput-object p0, Lcom/sec/android/GeoLookout/settings/DisasterSettingsFilterListActivity;->b:Landroid/content/Context;

    .line 39
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/settings/DisasterSettingsFilterListActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x1020002

    new-instance v2, Laox;

    invoke-direct {v2}, Laox;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/settings/DisasterSettingsFilterListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_0

    .line 49
    const v1, 0x7f0a0096

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 50
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 51
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 52
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 54
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 58
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 66
    :goto_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 60
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/settings/DisasterSettingsFilterListActivity;->onBackPressed()V

    goto :goto_0

    .line 58
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
