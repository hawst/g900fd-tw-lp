.class public Lcom/sec/android/GeoLookout/view/StrokeTextView;
.super Landroid/widget/TextView;
.source "StrokeTextView.java"


# instance fields
.field private a:Ljava/lang/CharSequence;

.field private final b:F

.field private final c:F

.field private final d:F

.field private final e:F

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    const/high16 v0, 0x40c00000    # 6.0f

    iput v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->b:F

    .line 16
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->c:F

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->d:F

    .line 18
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->e:F

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->f:Z

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 15
    const/high16 v0, 0x40c00000    # 6.0f

    iput v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->b:F

    .line 16
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->c:F

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->d:F

    .line 18
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->e:F

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->f:Z

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 15
    const/high16 v0, 0x40c00000    # 6.0f

    iput v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->b:F

    .line 16
    const/high16 v0, 0x40800000    # 4.0f

    iput v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->c:F

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->d:F

    .line 18
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->e:F

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->f:Z

    .line 31
    iput-object p2, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->a:Ljava/lang/CharSequence;

    .line 32
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/view/StrokeTextView;->c()V

    .line 33
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/view/StrokeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/view/StrokeTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900f5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/view/StrokeTextView;->setTextSize(F)V

    .line 38
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->f:Z

    .line 59
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->f:Z

    .line 63
    return-void
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->f:Z

    if-nez v0, :cond_0

    .line 89
    invoke-super {p0}, Landroid/widget/TextView;->invalidate()V

    .line 91
    :cond_0
    return-void
.end method

.method public invalidate(IIII)V
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->f:Z

    if-nez v0, :cond_0

    .line 103
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->invalidate(IIII)V

    .line 105
    :cond_0
    return-void
.end method

.method public invalidate(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->f:Z

    if-nez v0, :cond_0

    .line 96
    invoke-super {p0, p1}, Landroid/widget/TextView;->invalidate(Landroid/graphics/Rect;)V

    .line 98
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const v4, 0x7f08000d

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/view/StrokeTextView;->a()V

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/view/StrokeTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 44
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/view/StrokeTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    const/high16 v1, 0x40c00000    # 6.0f

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/view/StrokeTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800ae

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/view/StrokeTextView;->setTextColor(I)V

    .line 46
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 48
    const/high16 v0, 0x40800000    # 4.0f

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/view/StrokeTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/android/GeoLookout/view/StrokeTextView;->setShadowLayer(FFFI)V

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/view/StrokeTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/view/StrokeTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/view/StrokeTextView;->setTextColor(I)V

    .line 52
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/view/StrokeTextView;->b()V

    .line 54
    return-void
.end method

.method public postInvalidate()V
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->f:Z

    if-nez v0, :cond_0

    .line 75
    invoke-super {p0}, Landroid/widget/TextView;->postInvalidate()V

    .line 77
    :cond_0
    return-void
.end method

.method public postInvalidate(IIII)V
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->f:Z

    if-nez v0, :cond_0

    .line 82
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->postInvalidate(IIII)V

    .line 84
    :cond_0
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/view/StrokeTextView;->f:Z

    if-nez v0, :cond_0

    .line 68
    invoke-super {p0}, Landroid/widget/TextView;->requestLayout()V

    .line 70
    :cond_0
    return-void
.end method
