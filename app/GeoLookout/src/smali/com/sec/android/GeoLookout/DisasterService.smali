.class public Lcom/sec/android/GeoLookout/DisasterService;
.super Landroid/app/Service;
.source "DisasterService.java"

# interfaces
.implements Landroid/location/LocationListener;
.implements Lcb;
.implements Lcc;
.implements Lvk;


# instance fields
.field private a:Laoa;

.field private b:Lvh;

.field private c:Ljava/util/concurrent/ExecutorService;

.field private d:Landroid/location/LocationManager;

.field private e:Ljava/util/concurrent/CountDownLatch;

.field private f:Z

.field private g:Landroid/location/Location;

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;ZJ)Landroid/location/Location;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 502
    .line 504
    iget-object v1, p0, Lcom/sec/android/GeoLookout/DisasterService;->d:Landroid/location/LocationManager;

    invoke-virtual {v1, p1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 505
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getLastLocationByLocationManager provider="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " disabled!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->a(Ljava/lang/String;)V

    .line 527
    :goto_0
    return-object v0

    .line 509
    :cond_0
    if-nez p2, :cond_1

    .line 510
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->d:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 513
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 514
    if-nez v0, :cond_2

    .line 515
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->e:Ljava/util/concurrent/CountDownLatch;

    .line 516
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "request single update... provider:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 517
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->d:Landroid/location/LocationManager;

    invoke-virtual {v0, p1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->g:Landroid/location/Location;

    .line 518
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->d:Landroid/location/LocationManager;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v0, p1, p0, v1}, Landroid/location/LocationManager;->requestSingleUpdate(Ljava/lang/String;Landroid/location/LocationListener;Landroid/os/Looper;)V

    .line 520
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->e:Ljava/util/concurrent/CountDownLatch;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p3, p4, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 523
    :goto_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->d:Landroid/location/LocationManager;

    invoke-virtual {v0, p0}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 524
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->g:Landroid/location/Location;

    .line 526
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getLastLocationByLocationManager "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " waitTime:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " location:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 521
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private a(DD)V
    .locals 7

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->a:Laoa;

    const/4 v1, 0x1

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Laoa;->a(ZDD)Z

    .line 223
    return-void
.end method

.method private a(II)V
    .locals 1

    .prologue
    .line 258
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lalu;->a(Landroid/content/Context;II)V

    .line 259
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 285
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "location_mode"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 286
    if-nez v0, :cond_0

    .line 287
    const-string v0, "location mode is LOCATION_MODE_OFF! request allow location!"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 288
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0, p1}, Lage;->l(Landroid/content/Context;)V

    .line 300
    :goto_0
    return-void

    .line 291
    :cond_0
    invoke-direct {p0, v2, v2, v3}, Lcom/sec/android/GeoLookout/DisasterService;->b(ZZI)Landroid/location/Location;

    move-result-object v0

    .line 293
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleGetStartCurrentLocation() location:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->a(Ljava/lang/String;)V

    .line 294
    if-eqz v0, :cond_1

    .line 295
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0, p1}, Lage;->k(Landroid/content/Context;)V

    goto :goto_0

    .line 297
    :cond_1
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0, p1, v3}, Lage;->a(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/DisasterService;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/DisasterService;->c()V

    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/DisasterService;DD)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/GeoLookout/DisasterService;->a(DD)V

    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/DisasterService;II)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/DisasterService;->a(II)V

    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/DisasterService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/DisasterService;->a(Landroid/content/Context;)V

    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/DisasterService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/DisasterService;->a(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/DisasterService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/GeoLookout/DisasterService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/DisasterService;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/DisasterService;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/DisasterService;ZZI)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/GeoLookout/DisasterService;->a(ZZI)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 186
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lalu;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 187
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 234
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->a:Laoa;

    invoke-virtual {v0}, Laoa;->c()Ljava/lang/String;

    move-result-object v2

    .line 235
    if-nez v2, :cond_3

    .line 236
    const-string v0, "SPP getRegId() null"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 237
    invoke-static {p0}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 238
    invoke-static {p0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0, p0, v3}, Lage;->e(Landroid/content/Context;Z)V

    .line 240
    :cond_0
    invoke-static {p0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    invoke-static {p0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0, p0, v3}, Lage;->a(Landroid/content/Context;I)V

    .line 243
    :cond_1
    invoke-static {p0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 244
    invoke-static {p0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0, p0, v3}, Lage;->b(Landroid/content/Context;I)V

    .line 249
    :cond_2
    :goto_0
    return-void

    .line 248
    :cond_3
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lalu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 167
    .line 168
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->a(Landroid/content/Context;Z)V

    .line 169
    invoke-direct {p0, v1, v1, v1}, Lcom/sec/android/GeoLookout/DisasterService;->b(ZZI)Landroid/location/Location;

    move-result-object v4

    .line 170
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->a:Laoa;

    invoke-virtual {v0}, Laoa;->c()Ljava/lang/String;

    move-result-object v3

    .line 171
    if-nez v3, :cond_0

    .line 172
    const-string v0, "SPP getRegId() null"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 173
    invoke-static {p0, v1}, Lcom/sec/android/GeoLookout/DisasterAlertApplication;->a(Landroid/content/Context;Z)V

    .line 177
    :goto_0
    return-void

    .line 176
    :cond_0
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    move-object v1, p0

    move-object v2, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lalu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/location/Location;Z)V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 0

    .prologue
    .line 375
    iput-boolean p1, p0, Lcom/sec/android/GeoLookout/DisasterService;->h:Z

    .line 376
    return-void
.end method

.method private a(ZZI)V
    .locals 3

    .prologue
    .line 307
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/GeoLookout/DisasterService;->b(ZZI)Landroid/location/Location;

    move-result-object v0

    .line 309
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleGetCurrentLocation() location:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->a(Ljava/lang/String;)V

    .line 310
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.GeoLookout.ACTION_UPDATE_CURRENT_LOCATION"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 311
    const-string v2, "location"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 312
    const-string v0, "uid"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 313
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 314
    return-void
.end method

.method private b(Z)Landroid/location/Location;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    .line 474
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/DisasterService;->k()Z

    move-result v1

    .line 475
    if-nez v1, :cond_0

    .line 476
    const-string v1, "GooglePlayService connect failed"

    invoke-static {v1}, Lalj;->a(Ljava/lang/String;)V

    .line 498
    :goto_0
    return-object v0

    .line 480
    :cond_0
    if-nez p1, :cond_1

    .line 481
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    invoke-virtual {v0}, Lvh;->e()Landroid/location/Location;

    move-result-object v0

    .line 484
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 485
    if-nez v0, :cond_2

    .line 486
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v5}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->e:Ljava/util/concurrent/CountDownLatch;

    .line 487
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    invoke-virtual {v0}, Lvh;->e()Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->g:Landroid/location/Location;

    .line 488
    const-string v0, "request location updates..."

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 489
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    invoke-static {}, Lcom/google/android/gms/location/LocationRequest;->a()Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    const/16 v4, 0x64

    invoke-virtual {v1, v4}, Lcom/google/android/gms/location/LocationRequest;->a(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/google/android/gms/location/LocationRequest;->b(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-virtual {v0, v1, p0, v4}, Lvh;->a(Lcom/google/android/gms/location/LocationRequest;Lvk;Landroid/os/Looper;)V

    .line 491
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->e:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v4, 0x2710

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 494
    :goto_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    invoke-virtual {v0, p0}, Lvh;->a(Lvk;)V

    .line 495
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->g:Landroid/location/Location;

    .line 497
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getLastLocationByGooglePlayService waitTime:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " location:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 492
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private b(ZZI)Landroid/location/Location;
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x2

    .line 385
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "location_mode"

    invoke-static {v1, v2, v6}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 386
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getLastLocation() requestNewLocation:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " forceHighAccuracy:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " locationMode:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->a(Ljava/lang/String;)V

    .line 390
    if-eqz p3, :cond_1

    .line 391
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p3}, Lall;->i(Landroid/content/Context;I)Lamf;

    move-result-object v1

    .line 392
    new-instance v0, Landroid/location/Location;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "uid"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 394
    if-eqz v1, :cond_0

    .line 395
    iget-wide v2, v1, Lamf;->a:D

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 396
    iget-wide v2, v1, Lamf;->b:D

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 397
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MagicPd : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, v1, Lamf;->a:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, v1, Lamf;->b:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 452
    :cond_0
    :goto_0
    return-object v0

    .line 403
    :cond_1
    if-nez v1, :cond_2

    .line 404
    const-string v1, "LOCATION_MODE is LOCATION_MODE_OFF! location=unknown"

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    .line 405
    if-nez p1, :cond_0

    .line 406
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/DisasterService;->h()Landroid/location/Location;

    move-result-object v0

    goto :goto_0

    .line 412
    :cond_2
    if-eqz p2, :cond_3

    .line 413
    if-ne v1, v5, :cond_3

    .line 414
    const-string v2, "force change LOCATION_MODE from BATTERY_SAVING to HIGH_ACCURACY"

    invoke-static {v2}, Lalj;->c(Ljava/lang/String;)V

    .line 415
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/sec/android/GeoLookout/DisasterService;->a(Z)V

    .line 416
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "location_mode"

    const/4 v4, 0x3

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 420
    :cond_3
    iget-boolean v2, p0, Lcom/sec/android/GeoLookout/DisasterService;->f:Z

    if-eqz v2, :cond_4

    .line 422
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/DisasterService;->b(Z)Landroid/location/Location;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 429
    :cond_4
    :goto_1
    if-nez v0, :cond_5

    .line 430
    const-string v0, "network"

    const-wide/16 v2, 0x2710

    invoke-direct {p0, v0, p1, v2, v3}, Lcom/sec/android/GeoLookout/DisasterService;->a(Ljava/lang/String;ZJ)Landroid/location/Location;

    move-result-object v0

    .line 432
    :cond_5
    if-nez v0, :cond_6

    .line 433
    const-string v0, "gps"

    const-wide/16 v2, 0x7530

    invoke-direct {p0, v0, p1, v2, v3}, Lcom/sec/android/GeoLookout/DisasterService;->a(Ljava/lang/String;ZJ)Landroid/location/Location;

    move-result-object v0

    .line 435
    :cond_6
    if-nez v0, :cond_7

    .line 436
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/DisasterService;->i()Landroid/location/Location;

    move-result-object v0

    .line 439
    :cond_7
    if-eqz p2, :cond_8

    .line 440
    if-ne v1, v5, :cond_8

    .line 441
    const-string v1, "restore LOCATION_MODE from HIGH_ACCURACY to BATTERY_SAVING"

    invoke-static {v1}, Lalj;->c(Ljava/lang/String;)V

    .line 442
    invoke-direct {p0, v6}, Lcom/sec/android/GeoLookout/DisasterService;->a(Z)V

    .line 443
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "location_mode"

    invoke-static {v1, v2, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 447
    :cond_8
    if-eqz v0, :cond_9

    .line 448
    new-instance v1, Lamf;

    invoke-direct {v1, v0}, Lamf;-><init>(Landroid/location/Location;)V

    invoke-static {p0, v1}, Lanb;->b(Landroid/content/Context;Lamf;)V

    goto :goto_0

    .line 423
    :catch_0
    move-exception v2

    .line 424
    invoke-static {v2}, Lalj;->e(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 449
    :cond_9
    if-nez p1, :cond_0

    .line 450
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/DisasterService;->h()Landroid/location/Location;

    move-result-object v0

    goto :goto_0
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/DisasterService;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/DisasterService;->d()V

    return-void
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/DisasterService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/DisasterService;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 266
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleNofiticationCanceled() did:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 269
    const/4 v0, 0x3

    invoke-static {p0, p1, v0}, Lalx;->b(Landroid/content/Context;Ljava/lang/String;I)V

    .line 271
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->a:Laoa;

    invoke-virtual {v0}, Laoa;->c()Ljava/lang/String;

    move-result-object v0

    .line 194
    if-nez v0, :cond_0

    .line 195
    const-string v0, "SPP getRegId() null"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterService;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterService;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v0, v1}, Lage;->v(Landroid/content/Context;)V

    .line 201
    :goto_0
    return-void

    .line 200
    :cond_0
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lalu;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static synthetic c(Lcom/sec/android/GeoLookout/DisasterService;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/DisasterService;->f()V

    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->a:Laoa;

    invoke-virtual {v0}, Laoa;->c()Ljava/lang/String;

    move-result-object v0

    .line 208
    if-nez v0, :cond_0

    .line 209
    const-string v0, "SPP getRegId() null"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 213
    :goto_0
    return-void

    .line 212
    :cond_0
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lalu;->c(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static synthetic d(Lcom/sec/android/GeoLookout/DisasterService;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/DisasterService;->e()V

    return-void
.end method

.method private e()V
    .locals 7

    .prologue
    .line 274
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->a:Laoa;

    invoke-virtual {v0}, Laoa;->c()Ljava/lang/String;

    move-result-object v2

    .line 275
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lall;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 276
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lall;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 277
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    move-object v5, v3

    move-object v6, v4

    invoke-virtual/range {v0 .. v6}, Lalu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    return-void
.end method

.method private f()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 320
    invoke-direct {p0, v2, v2, v3}, Lcom/sec/android/GeoLookout/DisasterService;->b(ZZI)Landroid/location/Location;

    move-result-object v6

    .line 321
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "handleCheckCurrentLocation() location:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 323
    if-eqz v6, :cond_2

    .line 324
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    invoke-virtual {v6}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v6}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lalu;->a(Landroid/content/Context;DD)V

    .line 326
    invoke-static {p0}, Lanb;->a(Landroid/content/Context;)Lamf;

    move-result-object v0

    .line 327
    new-instance v1, Lamf;

    invoke-direct {v1, v6}, Lamf;-><init>(Landroid/location/Location;)V

    invoke-static {v0, v1}, Lall;->a(Lamf;Lamf;)I

    move-result v1

    .line 330
    int-to-long v2, v1

    const-wide/16 v4, 0x14

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    if-nez v0, :cond_1

    .line 331
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/DisasterService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 332
    const-string v1, "com.sec.android.GeoLookout.ACTION_UPDATE_LOCATION_TO_SPP_SERVER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 333
    const-string v1, "latitude"

    invoke-virtual {v6}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 334
    const-string v1, "longitude"

    invoke-virtual {v6}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 335
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/DisasterService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 349
    :cond_1
    :goto_0
    return-void

    .line 338
    :cond_2
    const-string v0, "ignore handleCheckCurrentLocation"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 339
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 340
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lage;->a(Landroid/content/Context;I)V

    .line 342
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 343
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lage;->b(Landroid/content/Context;I)V

    .line 345
    :cond_4
    invoke-static {p0}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 346
    invoke-static {p0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0, p0, v3}, Lage;->a(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 371
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->h:Z

    return v0
.end method

.method private h()Landroid/location/Location;
    .locals 4

    .prologue
    .line 456
    const/4 v0, 0x0

    .line 457
    invoke-static {p0}, Lanb;->c(Landroid/content/Context;)Lamf;

    move-result-object v1

    .line 459
    if-nez v1, :cond_0

    .line 460
    invoke-static {p0}, Lanb;->a(Landroid/content/Context;)Lamf;

    move-result-object v1

    .line 463
    :cond_0
    if-eqz v1, :cond_1

    .line 464
    new-instance v0, Landroid/location/Location;

    const-string v2, "pref_saved"

    invoke-direct {v0, v2}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 465
    iget-wide v2, v1, Lamf;->a:D

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLatitude(D)V

    .line 466
    iget-wide v2, v1, Lamf;->b:D

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    .line 470
    :goto_0
    return-object v0

    .line 468
    :cond_1
    const-string v1, "mSavedLastLocation is null"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private i()Landroid/location/Location;
    .locals 2

    .prologue
    .line 531
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->d:Landroid/location/LocationManager;

    const-string v1, "passive"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method private j()Z
    .locals 3

    .prologue
    .line 541
    invoke-static {p0}, Lcf;->a(Landroid/content/Context;)I

    move-result v0

    .line 543
    if-nez v0, :cond_0

    .line 544
    const-string v0, "google play sevice available!"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 545
    const/4 v0, 0x1

    .line 548
    :goto_0
    return v0

    .line 547
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "google play sevice unavailable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 548
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 556
    iget-object v1, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    invoke-virtual {v1}, Lvh;->b()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    invoke-virtual {v1}, Lvh;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 557
    const-string v1, "try connect mLocationClient.."

    invoke-static {v1}, Lalj;->a(Ljava/lang/String;)V

    .line 558
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v1, v0}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/sec/android/GeoLookout/DisasterService;->e:Ljava/util/concurrent/CountDownLatch;

    .line 559
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    invoke-virtual {v0}, Lvh;->a()V

    .line 560
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 562
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/GeoLookout/DisasterService;->e:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v4, 0x1388

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 565
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mLocationConnect waitTime:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v0, v4, v0

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " connected:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    invoke-virtual {v1}, Lvh;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " connecting:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    invoke-virtual {v1}, Lvh;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 566
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    invoke-virtual {v0}, Lvh;->b()Z

    move-result v0

    .line 568
    :cond_0
    return v0

    .line 563
    :catch_0
    move-exception v2

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 586
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onConnected bundle:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 587
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " location:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    invoke-virtual {v1}, Lvh;->e()Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 588
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 589
    return-void
.end method

.method public a(Lbz;)V
    .locals 2

    .prologue
    .line 577
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onConnectionFailed result:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 578
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 579
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 358
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    invoke-virtual {v0}, Lvh;->d()V

    .line 360
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    .line 362
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/DisasterService;->g()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 363
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/DisasterService;->a(Z)V

    .line 364
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/DisasterService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "location_mode"

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 366
    :cond_1
    return-void
.end method

.method public b_()V
    .locals 1

    .prologue
    .line 596
    const-string v0, "onDisconnected"

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 597
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 598
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 57
    const-string v0, "onCreate"

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 58
    new-instance v0, Laoa;

    invoke-direct {v0, p0}, Laoa;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->a:Laoa;

    .line 59
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->c:Ljava/util/concurrent/ExecutorService;

    .line 61
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/DisasterService;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->f:Z

    .line 62
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->f:Z

    if-eqz v0, :cond_0

    .line 63
    new-instance v0, Lvh;

    invoke-direct {v0, p0, p0, p0}, Lvh;-><init>(Landroid/content/Context;Lcb;Lcc;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    .line 66
    :cond_0
    const-string v0, "location"

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/DisasterService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->d:Landroid/location/LocationManager;

    .line 67
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 72
    const-string v0, "onDestroy"

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->a:Laoa;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->a:Laoa;

    invoke-virtual {v0}, Laoa;->b()V

    .line 75
    iput-object v1, p0, Lcom/sec/android/GeoLookout/DisasterService;->a:Laoa;

    .line 77
    :cond_0
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    invoke-virtual {v0, p0}, Lalu;->a(Landroid/content/Context;)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 80
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    if-eqz v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    invoke-virtual {v0}, Lvh;->d()V

    .line 82
    iput-object v1, p0, Lcom/sec/android/GeoLookout/DisasterService;->b:Lvh;

    .line 84
    :cond_1
    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 605
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onLocationChanged:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 606
    iput-object p1, p0, Lcom/sec/android/GeoLookout/DisasterService;->g:Landroid/location/Location;

    .line 608
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterService;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 609
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 613
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onProviderDisabled provider="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 615
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 619
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onProviderEnabled provider="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 620
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    .line 88
    if-nez p1, :cond_0

    .line 89
    const-string v0, "intent null"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 90
    const/4 v0, -0x1

    .line 149
    :goto_0
    return v0

    .line 92
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 93
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "action="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->a(Ljava/lang/String;)V

    .line 94
    iget-object v1, p0, Lcom/sec/android/GeoLookout/DisasterService;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lagd;

    invoke-direct {v2, p0, v0, p1}, Lagd;-><init>(Lcom/sec/android/GeoLookout/DisasterService;Ljava/lang/String;Landroid/content/Intent;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 149
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 624
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onStatusChanged provider="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " extras="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 625
    return-void
.end method
