.class public Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;
.super Landroid/app/Activity;
.source "SafetyCareDisasterDisclaimerActivity.java"


# instance fields
.field private a:Landroid/widget/CheckBox;

.field private b:Landroid/widget/CheckBox;

.field private c:Landroid/widget/CheckBox;

.field private d:Landroid/widget/CheckBox;

.field private e:Landroid/widget/CheckBox;

.field private f:[Landroid/widget/TextView;

.field private g:I

.field private h:Landroid/app/AlertDialog;

.field private i:Landroid/app/AlertDialog;

.field private j:Landroid/content/Context;

.field private k:Z

.field private l:Z

.field private m:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->g:I

    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->e()V

    return-void
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->a:Landroid/widget/CheckBox;

    return-object v0
.end method

.method public static synthetic c(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->b:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private c()V
    .locals 11

    .prologue
    const/4 v1, 0x6

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/16 v8, 0x8

    const/4 v2, 0x0

    .line 121
    const v0, 0x7f0e006e

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 122
    new-instance v3, Laqd;

    invoke-direct {v3, p0}, Laqd;-><init>(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    const v0, 0x7f0e006f

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 131
    new-instance v3, Laqj;

    invoke-direct {v3, p0}, Laqj;-><init>(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    iget-object v3, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080050

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setTextColor(I)V

    .line 139
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 141
    iget-object v3, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    const v0, 0x7f0e0066

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v3, v2

    .line 142
    const v0, 0x7f0e0067

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->a:Landroid/widget/CheckBox;

    .line 144
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->a:Landroid/widget/CheckBox;

    new-instance v3, Laqk;

    invoke-direct {v3, p0}, Laqk;-><init>(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 153
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->k:Z

    if-eqz v0, :cond_9

    .line 154
    iget-object v3, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    const v0, 0x7f0e0069

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v3, v9

    .line 155
    iget-object v3, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    const v0, 0x7f0e006b

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v3, v10

    .line 156
    const v0, 0x7f0e006a

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->b:Landroid/widget/CheckBox;

    .line 157
    const v0, 0x7f0e006c

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->c:Landroid/widget/CheckBox;

    .line 159
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 160
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 162
    const/16 v0, 0x9

    new-array v5, v0, [I

    fill-array-data v5, :array_0

    .line 174
    const/16 v0, 0x9

    new-array v6, v0, [I

    fill-array-data v6, :array_1

    .line 186
    sget-boolean v0, Laky;->j:Z

    if-eqz v0, :cond_0

    move v0, v2

    .line 187
    :goto_0
    if-ge v0, v1, :cond_2

    .line 188
    aget v7, v6, v0

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    const-string v7, "\n\n"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 191
    :cond_0
    sget-boolean v0, Laky;->a:Z

    if-eqz v0, :cond_1

    move v0, v2

    .line 192
    :goto_1
    if-ge v0, v1, :cond_2

    .line 193
    aget v7, v5, v0

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    const-string v7, "\n\n"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 196
    :cond_1
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_2

    .line 197
    const v0, 0x7f0a04c3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "\n"

    const-string v7, "<br>"

    invoke-virtual {v0, v4, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 201
    iget-object v4, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v4, v4, v2

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v0, v0, v2

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 206
    sget-boolean v4, Laky;->j:Z

    if-eqz v4, :cond_4

    .line 207
    aget v4, v6, v8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    :cond_3
    :goto_2
    const-string v4, "<br><br>"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "\n"

    const-string v7, "<br>"

    invoke-virtual {v0, v4, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 217
    iget-object v4, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v4, v4, v9

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v0, v0, v9

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 220
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 222
    sget-boolean v0, Laky;->j:Z

    if-eqz v0, :cond_6

    .line 223
    :goto_3
    if-ge v1, v8, :cond_8

    .line 224
    aget v0, v6, v1

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    const-string v0, "<br><br>"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 208
    :cond_4
    sget-boolean v4, Laky;->a:Z

    if-eqz v4, :cond_5

    .line 209
    aget v4, v5, v8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 210
    :cond_5
    sget-boolean v4, Laky;->b:Z

    if-eqz v4, :cond_3

    .line 211
    const v4, 0x7f0a04c7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 227
    :cond_6
    sget-boolean v0, Laky;->a:Z

    if-eqz v0, :cond_7

    move v0, v1

    .line 228
    :goto_4
    if-ge v0, v8, :cond_8

    .line 229
    aget v1, v5, v0

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    const-string v1, "<br><br>"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 232
    :cond_7
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_8

    .line 233
    const v0, 0x7f0a04ca

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    :cond_8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    const-string v3, "<br>"

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 237
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v1, v1, v10

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v0, v0, v10

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 239
    new-instance v0, Laql;

    invoke-direct {v0, p0}, Laql;-><init>(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)V

    .line 253
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 254
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v1, v1, v9

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 255
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v1, v1, v10

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 257
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->b:Landroid/widget/CheckBox;

    new-instance v1, Laqm;

    invoke-direct {v1, p0}, Laqm;-><init>(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->c:Landroid/widget/CheckBox;

    new-instance v1, Laqn;

    invoke-direct {v1, p0}, Laqn;-><init>(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 422
    :goto_5
    return-void

    .line 274
    :cond_9
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->l:Z

    if-eqz v0, :cond_a

    .line 275
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v0, v0, v2

    const v1, 0x7f0a04c2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 276
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    const v0, 0x7f0e0069

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v1, v9

    .line 277
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v0, v0, v9

    const v1, 0x7f0a04c6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 278
    const v0, 0x7f0e006a

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->b:Landroid/widget/CheckBox;

    .line 279
    const v0, 0x7f0e00f1

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->c:Landroid/widget/CheckBox;

    .line 280
    const v0, 0x7f0e00f2

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->d:Landroid/widget/CheckBox;

    .line 281
    const v0, 0x7f0e00f3

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->e:Landroid/widget/CheckBox;

    .line 283
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v0, v0, v2

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v0, v0, v9

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 286
    new-instance v0, Laqo;

    invoke-direct {v0, p0}, Laqo;-><init>(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)V

    .line 300
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v1, v1, v2

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 301
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v1, v1, v9

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 303
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->b:Landroid/widget/CheckBox;

    new-instance v1, Laqp;

    invoke-direct {v1, p0}, Laqp;-><init>(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->c:Landroid/widget/CheckBox;

    new-instance v1, Laqq;

    invoke-direct {v1, p0}, Laqq;-><init>(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->d:Landroid/widget/CheckBox;

    new-instance v1, Laqe;

    invoke-direct {v1, p0}, Laqe;-><init>(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->e:Landroid/widget/CheckBox;

    new-instance v1, Laqf;

    invoke-direct {v1, p0}, Laqf;-><init>(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 339
    const v0, 0x7f0e00f1

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 340
    const v0, 0x7f0e00f2

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 341
    const v0, 0x7f0e00f3

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 342
    const v0, 0x7f0e00f6

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 343
    const v0, 0x7f0e006c

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5

    .line 345
    :cond_a
    sget-boolean v0, Laky;->d:Z

    if-eqz v0, :cond_e

    .line 347
    :try_start_0
    const-string v0, "DisaclaimerTest : USA"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 349
    const-class v0, Lagt;

    const-string v1, "safetycare_geonews_disclaimer_usa"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 350
    const-class v1, Lagt;

    const-string v3, "safetycare_terms_and_conditions_checkbox_text_usa"

    invoke-virtual {v1, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 353
    if-eqz v0, :cond_d

    .line 354
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 355
    iget-object v3, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(I)V

    .line 360
    :goto_6
    if-eqz v1, :cond_b

    .line 361
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 362
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setText(I)V

    .line 363
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->m:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 373
    :cond_b
    :goto_7
    const v0, 0x7f0e00f0

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 374
    new-instance v1, Laqg;

    invoke-direct {v1, p0}, Laqg;-><init>(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)V

    .line 388
    iget-object v3, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v2, v3, v2

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 390
    invoke-virtual {v0}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 391
    iget-object v2, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v2

    .line 394
    if-nez v2, :cond_10

    .line 396
    sget-boolean v2, Laky;->d:Z

    if-eqz v2, :cond_f

    iget-boolean v2, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->m:Z

    if-eqz v2, :cond_f

    .line 397
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09010e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 414
    :cond_c
    :goto_8
    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 415
    const v0, 0x7f0e0068

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 416
    const v0, 0x7f0e00f5

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 417
    const v0, 0x7f0e00f4

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 418
    const v0, 0x7f0e006a

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 419
    const v0, 0x7f0e00f6

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 420
    const v0, 0x7f0e006c

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5

    .line 357
    :cond_d
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    const v3, 0x7f0a04c1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_6

    .line 365
    :catch_0
    move-exception v0

    .line 366
    invoke-static {v0}, Lalj;->e(Ljava/lang/Throwable;)V

    .line 367
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v0, v0, v2

    const v1, 0x7f0a04c1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_7

    .line 370
    :cond_e
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    aget-object v0, v0, v2

    const v1, 0x7f0a04c1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_7

    .line 400
    :cond_f
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09010c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto :goto_8

    .line 403
    :cond_10
    const/4 v3, 0x3

    if-eq v2, v3, :cond_11

    if-ne v2, v9, :cond_c

    .line 405
    :cond_11
    sget-boolean v2, Laky;->d:Z

    if-eqz v2, :cond_12

    iget-boolean v2, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->m:Z

    if-eqz v2, :cond_12

    .line 406
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09010d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_8

    .line 409
    :cond_12
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09010b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto/16 :goto_8

    .line 162
    nop

    :array_0
    .array-data 4
        0x7f0a04d7
        0x7f0a04d8
        0x7f0a04d9
        0x7f0a04da
        0x7f0a04db
        0x7f0a04dc
        0x7f0a04dd
        0x7f0a04de
        0x7f0a04df
    .end array-data

    .line 174
    :array_1
    .array-data 4
        0x7f0a04ce
        0x7f0a04cf
        0x7f0a04d0
        0x7f0a04d1
        0x7f0a04d2
        0x7f0a04d3
        0x7f0a04d4
        0x7f0a04d5
        0x7f0a04d6
    .end array-data
.end method

.method public static synthetic d(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->c:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->h:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 512
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->h:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 513
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->h:Landroid/app/AlertDialog;

    .line 515
    :cond_0
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 518
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->d()V

    .line 520
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 521
    const v1, 0x7f0a04e1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a04e0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0002

    new-instance v2, Laqh;

    invoke-direct {v2, p0}, Laqh;-><init>(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->h:Landroid/app/AlertDialog;

    .line 533
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->h:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 534
    return-void
.end method

.method public static synthetic e(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->g()V

    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->i:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 538
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->i:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 539
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->i:Landroid/app/AlertDialog;

    .line 541
    :cond_0
    return-void
.end method

.method private g()V
    .locals 5

    .prologue
    .line 544
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f()V

    .line 546
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 547
    const v1, 0x7f0a00c7

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00c6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0002

    new-instance v2, Laqi;

    invoke-direct {v2, p0}, Laqi;-><init>(Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->i:Landroid/app/AlertDialog;

    .line 559
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->i:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 560
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 425
    const v0, 0x7f0e006f

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 428
    iget-boolean v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->k:Z

    if-eqz v1, :cond_1

    .line 429
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->a:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->b:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->c:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    .line 437
    :goto_0
    if-eqz v1, :cond_4

    .line 438
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f08000d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 439
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 444
    :goto_1
    return-void

    :cond_0
    move v1, v3

    .line 429
    goto :goto_0

    .line 430
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->l:Z

    if-eqz v1, :cond_3

    .line 431
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->a:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->b:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->c:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->e:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_0

    .line 434
    :cond_3
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->a:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    goto :goto_0

    .line 441
    :cond_4
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080050

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 442
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 491
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "safety_care_disaster_user_agree"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 494
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    invoke-static {v0}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 495
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    invoke-static {v0}, Lall;->x(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 496
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 497
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 498
    const/16 v1, 0x6f

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 508
    :goto_0
    return-void

    .line 500
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lage;->e(Landroid/content/Context;)V

    .line 501
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->setResult(ILandroid/content/Intent;)V

    .line 502
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->finish()V

    goto :goto_0

    .line 505
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v3, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->setResult(ILandroid/content/Intent;)V

    .line 506
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->finish()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 564
    const/16 v0, 0x6f

    if-ne p1, v0, :cond_1

    .line 565
    if-ne p2, v1, :cond_0

    .line 566
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->setResult(ILandroid/content/Intent;)V

    .line 567
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lage;->e(Landroid/content/Context;)V

    .line 568
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 569
    invoke-static {}, Laqs;->a()Laqs;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Laqs;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 571
    sget-boolean v1, Laky;->k:Z

    if-eqz v1, :cond_0

    .line 572
    invoke-static {}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a()Lcom/sec/android/GeoLookout/widget/LifeModeWidget;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 573
    invoke-static {}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a()Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 577
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->finish()V

    .line 580
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 581
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4

    .prologue
    .line 448
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 450
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->l:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->k:Z

    if-nez v0, :cond_0

    .line 451
    const v0, 0x7f0e00f0

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 452
    invoke-virtual {v0}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 454
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 456
    sget-boolean v2, Laky;->d:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->m:Z

    if-eqz v2, :cond_1

    .line 457
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09010e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 474
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 476
    :cond_0
    return-void

    .line 460
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09010c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto :goto_0

    .line 465
    :cond_2
    sget-boolean v2, Laky;->d:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->m:Z

    if-eqz v2, :cond_3

    .line 466
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09010d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto :goto_0

    .line 469
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09010b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 74
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 76
    iput-object p0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    .line 77
    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->setResult(I)V

    .line 79
    sget-boolean v0, Laky;->a:Z

    if-nez v0, :cond_0

    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->k:Z

    .line 80
    sget-boolean v0, Laky;->c:Z

    iput-boolean v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->l:Z

    .line 83
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 86
    if-eqz v0, :cond_1

    .line 87
    const-string v3, "appWidgetId"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->g:I

    .line 91
    :cond_1
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->f:[Landroid/widget/TextView;

    .line 92
    const v0, 0x7f0a04b8

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->setTitle(I)V

    .line 94
    const v0, 0x7f03002f

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->setContentView(I)V

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "safety_care_disaster_user_agree"

    invoke-static {v0, v2, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_3

    .line 102
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->c()V

    .line 118
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 79
    goto :goto_0

    .line 104
    :cond_3
    iget v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->g:I

    if-eqz v0, :cond_4

    .line 105
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 106
    invoke-static {}, Laqs;->a()Laqs;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->j:Landroid/content/Context;

    iget v3, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->g:I

    invoke-virtual {v1, v2, v0, v3}, Laqs;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V

    .line 109
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 110
    const-string v1, "appWidgetId"

    iget v2, p0, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->g:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 111
    invoke-virtual {p0, v4, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->setResult(ILandroid/content/Intent;)V

    .line 116
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->finish()V

    goto :goto_1

    .line 113
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v4, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 480
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 486
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 482
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->setResult(I)V

    .line 483
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;->finish()V

    goto :goto_0

    .line 480
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
