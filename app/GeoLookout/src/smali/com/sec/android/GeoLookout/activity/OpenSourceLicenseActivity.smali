.class public Lcom/sec/android/GeoLookout/activity/OpenSourceLicenseActivity;
.super Landroid/app/Activity;
.source "OpenSourceLicenseActivity.java"


# instance fields
.field private a:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 18
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    const v0, 0x7f03002d

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/OpenSourceLicenseActivity;->setContentView(I)V

    .line 22
    const v0, 0x7f0e00ed

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/OpenSourceLicenseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/OpenSourceLicenseActivity;->a:Landroid/webkit/WebView;

    .line 24
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/OpenSourceLicenseActivity;->a:Landroid/webkit/WebView;

    const-string v1, "file:///android_asset/NOTICE"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/OpenSourceLicenseActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setDefaultTextEncodingName(Ljava/lang/String;)V

    .line 27
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/OpenSourceLicenseActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 29
    const v1, 0x7f0a04b4

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 30
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 32
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 37
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 42
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 39
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/OpenSourceLicenseActivity;->finish()V

    goto :goto_0

    .line 37
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
