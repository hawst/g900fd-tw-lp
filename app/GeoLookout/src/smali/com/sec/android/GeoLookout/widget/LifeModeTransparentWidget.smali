.class public Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;
.super Landroid/appwidget/AppWidgetProvider;
.source "LifeModeTransparentWidget.java"


# static fields
.field private static volatile j:Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;

.field private static k:Z

.field private static l:I

.field private static n:[I

.field private static o:[I


# instance fields
.field a:Landroid/database/Cursor;

.field b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lamx;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/lang/String;

.field d:[[I

.field e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lamj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->j:Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;

    .line 58
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->k:Z

    .line 64
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->n:[I

    .line 73
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->o:[I

    return-void

    .line 64
    nop

    :array_0
    .array-data 4
        0x260
        0x261
        0x25e
        0x25f
        0x25a
        0x25c
        0x263
        0x25b
        0x25d
        0x262
        0x259
        0x260
    .end array-data

    .line 73
    :array_1
    .array-data 4
        0x1f5
        0x1f6
        0x1f7
        0x1f8
        0x1f9
        0x1fa
        0x1fb
        0x1fc
    .end array-data
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const v4, 0x7f020017

    const/4 v3, 0x4

    .line 111
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    .line 80
    new-array v0, v3, [[I

    new-array v1, v3, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v5

    new-array v1, v3, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v6

    new-array v1, v3, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v7

    const/4 v1, 0x3

    new-array v2, v3, [I

    fill-array-data v2, :array_3

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    .line 99
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    .line 103
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    .line 105
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->h:Ljava/util/HashMap;

    .line 107
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->i:Ljava/util/HashMap;

    .line 113
    sput v5, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->l:I

    .line 114
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_0

    .line 115
    const/16 v0, 0x1f5

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 116
    const/16 v0, 0x1f6

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 117
    const/16 v0, 0x1f7

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 118
    const/16 v0, 0x1f8

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 119
    const/16 v0, 0x1f9

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 120
    const/16 v0, 0x1fa

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 121
    const/16 v0, 0x1fb

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 122
    const/16 v0, 0x1fc

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 131
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->h:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020015

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->h:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020016

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->h:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020018

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->h:Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->h:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->h:Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x1f5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201ab

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x1f6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201a4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x1f7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201b5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x1f8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201b3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x1f9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x1fa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201ad

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x1fb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201b0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x1fc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201b1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x1f5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a029e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x1f6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a029b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x1f7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02a5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x1f8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x1f9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a029a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x1fa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a0295

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x1fb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02a1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x1fc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02a4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    :goto_0
    return-void

    .line 160
    :cond_0
    const/16 v0, 0x260

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 161
    const/16 v0, 0x261

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 162
    const/16 v0, 0x25e

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 163
    const/16 v0, 0x25f

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 164
    const/16 v0, 0x25a

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 165
    const/16 v0, 0x25c

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 166
    const/16 v0, 0x263

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 167
    const/16 v0, 0x25b

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 168
    const/16 v0, 0x25d

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 169
    const/16 v0, 0x262

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 170
    const/16 v0, 0x259

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(I)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->h:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020015

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->h:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020016

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->h:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f020018

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->h:Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->h:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->h:Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x260

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201a9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x261

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201a5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x25e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201a7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x25f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201a2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x25a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201a6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x25c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201af

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x263

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201aa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x25b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201ae

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x25d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201b4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x262

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201a8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->f:Ljava/util/HashMap;

    const/16 v1, 0x259

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0201b2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x260

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a029d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x261

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a0294

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x25e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a0298

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x25f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a0299

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x25a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a0297

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x25c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02a1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x263

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a029e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x25b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a029f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x25d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x262

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02a2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    const/16 v1, 0x259

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02a3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 80
    :array_0
    .array-data 4
        0x7f0e00d4
        0x7f0e00d3
        0x7f0e00d2
        0x7f0e00d1
    .end array-data

    :array_1
    .array-data 4
        0x7f0e00d8
        0x7f0e00d7
        0x7f0e00d6
        0x7f0e00d5
    .end array-data

    :array_2
    .array-data 4
        0x7f0e00dc
        0x7f0e00db
        0x7f0e00da
        0x7f0e00d9
    .end array-data

    :array_3
    .array-data 4
        0x7f0e00e0
        0x7f0e00df
        0x7f0e00de
        0x7f0e00dd
    .end array-data
.end method

.method private a(II)I
    .locals 1

    .prologue
    .line 779
    mul-int/lit8 v0, p1, 0xa

    add-int/2addr v0, p2

    return v0
.end method

.method public static a()Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;
    .locals 2

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->j:Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;

    if-nez v0, :cond_1

    .line 46
    const-class v1, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;

    monitor-enter v1

    .line 47
    :try_start_0
    sget-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->j:Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;

    invoke-direct {v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;-><init>()V

    sput-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->j:Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;

    .line 50
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    :cond_1
    sget-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->j:Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;

    return-object v0

    .line 50
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(I)V
    .locals 6

    .prologue
    const v2, 0x7f0a00e1

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 215
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_3

    .line 216
    const/16 v0, 0x1f5

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1f7

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1f6

    if-ne p1, v0, :cond_1

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v3}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02ac

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v4}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02ad

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v5}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02ae

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02af

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    :goto_0
    return-void

    .line 224
    :cond_1
    const/16 v0, 0x1f8

    if-ne p1, v0, :cond_2

    .line 225
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v3}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02b0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v4}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02b1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v5}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02b2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 230
    :cond_2
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v3}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02b3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v4}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02b4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v5}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a02b5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 238
    :cond_3
    const/16 v0, 0x260

    if-ne p1, v0, :cond_4

    .line 239
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v3}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00e8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v4}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00e9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v5}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00ea

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00eb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00ec

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 246
    :cond_4
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v3}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00e2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v4}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00e3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    invoke-direct {p0, p1, v5}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00e4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00e5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->e:Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x7f0a00e6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 799
    const-string v0, "LifemodeTransparent > checkRefreshResult"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 800
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 801
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lage;->c(Z)V

    .line 802
    if-ne p2, v2, :cond_1

    .line 803
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 810
    :cond_0
    :goto_0
    return-void

    .line 805
    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 806
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a04ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 9

    .prologue
    const v8, 0x7f0e00e6

    const v7, 0x7f0a0020

    const/4 v6, 0x1

    const v5, 0x7f0e00e9

    const/4 v4, 0x0

    .line 500
    const v0, 0x7f03002a

    .line 501
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 506
    const/16 v0, 0x8

    invoke-virtual {v1, v8, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 507
    sget-boolean v0, Lakv;->bQ:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lakw;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 508
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a00b7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 510
    invoke-virtual {v1, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 548
    :goto_0
    sput-boolean v6, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->k:Z

    .line 549
    invoke-virtual {p2, p3, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 550
    return-void

    .line 511
    :cond_0
    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lane;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 514
    :cond_1
    sget-boolean v0, Lakv;->bQ:Z

    if-eqz v0, :cond_2

    .line 515
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a02bc

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 520
    :goto_1
    invoke-virtual {v1, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 522
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_WIGET_APP_START"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 523
    invoke-static {p1, v4, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 524
    const v2, 0x7f0e00e7

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_0

    .line 517
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a02bd

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 525
    :cond_3
    invoke-static {p1}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 526
    const-string v0, ""

    .line 527
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 528
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0008

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 535
    :goto_2
    invoke-virtual {v1, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 536
    invoke-virtual {v1, v8, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0

    .line 531
    :cond_4
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0560

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 538
    :cond_5
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0564

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 540
    invoke-virtual {v1, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 542
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_TAG_TO_START"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 543
    invoke-static {p1, v4, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 544
    const v2, 0x7f0e00e7

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto/16 :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 5

    .prologue
    const v4, 0x7f0e00e2

    const/4 v1, 0x0

    .line 615
    sget v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->l:I

    add-int/lit8 v0, v0, 0x1

    .line 618
    sget-boolean v2, Laky;->b:Z

    if-eqz v2, :cond_2

    .line 619
    const/4 v2, 0x1

    if-le v0, v2, :cond_1

    .line 621
    const v0, 0x7f020011

    invoke-virtual {p2, v4, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    move v0, v1

    .line 631
    :cond_0
    :goto_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_SHOW_NEXT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 632
    const-string v3, "nextPage"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 636
    const/high16 v0, 0x8000000

    invoke-static {p1, v1, v2, v0}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 638
    invoke-virtual {p2, v4, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 640
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_WIGET_APP_START"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 641
    invoke-static {p1, v1, v0, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 643
    const v1, 0x7f0e00e8

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 645
    return-void

    .line 623
    :cond_1
    const v2, 0x7f020012

    invoke-virtual {p2, v4, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    .line 626
    :cond_2
    const/4 v2, 0x2

    if-le v0, v2, :cond_0

    move v0, v1

    .line 627
    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/widget/RemoteViews;I)V
    .locals 3

    .prologue
    .line 651
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_WIGET_APP_START"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 652
    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 653
    invoke-virtual {p2, p3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 655
    return-void
.end method

.method private a(Landroid/content/Context;Z)V
    .locals 10

    .prologue
    const v9, 0x7f0e00e5

    const v8, 0x7f0e00e4

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 462
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 463
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v2

    .line 465
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_1

    .line 466
    invoke-static {p1}, Lall;->E(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f030028

    invoke-direct {v0, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 474
    :goto_0
    if-eqz p2, :cond_2

    .line 475
    invoke-virtual {v0, v8, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 476
    invoke-virtual {v0, v9, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 486
    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 488
    return-void

    .line 469
    :cond_0
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f030027

    invoke-direct {v0, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    goto :goto_0

    .line 472
    :cond_1
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f03002b

    invoke-direct {v0, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    goto :goto_0

    .line 478
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 479
    invoke-static {p1, v4, v5}, Lall;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .line 481
    invoke-static {p1, v4, v5}, Lall;->a(Landroid/content/Context;J)V

    .line 482
    const v4, 0x7f0e00e3

    invoke-virtual {v0, v4, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 483
    invoke-virtual {v0, v8, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 484
    invoke-virtual {v0, v9, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1
.end method

.method private b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 10

    .prologue
    const v9, 0x7f0e00e4

    const/16 v8, 0x8

    const v7, 0x7f0e00e5

    const/4 v6, 0x0

    .line 554
    const v0, 0x7f03002b

    .line 555
    sget-boolean v1, Laky;->b:Z

    if-eqz v1, :cond_0

    .line 556
    invoke-static {p1}, Lall;->E(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 557
    const v0, 0x7f030028

    .line 563
    :cond_0
    :goto_0
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 565
    invoke-static {p1}, Lamu;->f(Landroid/content/Context;)Lamj;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->m:Lamj;

    .line 567
    invoke-static {p1}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 568
    const v0, 0x7f0e00e6

    invoke-virtual {v1, v0, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 571
    :cond_1
    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->b(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 574
    invoke-direct {p0, p1, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 577
    invoke-static {p1}, Lall;->c(Landroid/content/Context;)J

    move-result-wide v2

    .line 578
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_4

    .line 579
    const-string v0, ""

    .line 584
    :goto_1
    const v2, 0x7f0e00e3

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 587
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 588
    invoke-virtual {v1, v9, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 589
    invoke-virtual {v1, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 600
    :goto_2
    invoke-virtual {p2, p3, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V

    .line 601
    sget-boolean v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->k:Z

    if-eqz v0, :cond_2

    .line 602
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 603
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 604
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 605
    sput-boolean v6, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->k:Z

    .line 607
    :cond_2
    return-void

    .line 559
    :cond_3
    const v0, 0x7f030027

    goto :goto_0

    .line 581
    :cond_4
    invoke-static {p1, v2, v3}, Lall;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 582
    invoke-static {p1, v2, v3}, Lall;->a(Landroid/content/Context;J)V

    goto :goto_1

    .line 592
    :cond_5
    invoke-virtual {v1, v9, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 593
    invoke-virtual {v1, v7, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 595
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 596
    invoke-static {p1, v6, v0, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 597
    invoke-virtual {v1, v7, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_2
.end method

.method private b(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 658
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LifeTransparentWidget > applyIndexData mCurrentPage : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 660
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_3

    .line 661
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->m:Lamj;

    invoke-virtual {v0}, Lamj;->b()[I

    move-result-object v0

    sput-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->o:[I

    .line 663
    sget-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->o:[I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->o:[I

    array-length v0, v0

    if-nez v0, :cond_1

    .line 664
    :cond_0
    const-string v0, "LifeTransparentWidget > applyIndexData cannot find current zone to order"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 665
    invoke-static {p1}, Lamu;->g(Landroid/content/Context;)[I

    move-result-object v0

    sput-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->o:[I

    :cond_1
    :goto_0
    move v2, v3

    .line 676
    :goto_1
    if-ge v2, v10, :cond_c

    .line 680
    sget v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->l:I

    mul-int/lit8 v0, v0, 0x4

    add-int/2addr v0, v2

    .line 682
    sget-boolean v1, Laky;->b:Z

    if-eqz v1, :cond_5

    .line 683
    sget-object v1, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->o:[I

    array-length v1, v1

    if-lt v0, v1, :cond_6

    .line 684
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    aget-object v0, v0, v2

    aget v0, v0, v8

    invoke-virtual {p2, v0, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 676
    :cond_2
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 668
    :cond_3
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->m:Lamj;

    invoke-virtual {v0}, Lamj;->b()[I

    move-result-object v0

    sput-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->n:[I

    .line 670
    sget-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->n:[I

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->n:[I

    array-length v0, v0

    if-nez v0, :cond_1

    .line 671
    :cond_4
    const-string v0, "LifeTransparentWidget > applyIndexData cannot find current zone to order"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 672
    invoke-static {p1}, Lamu;->g(Landroid/content/Context;)[I

    move-result-object v0

    sput-object v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->n:[I

    goto :goto_0

    .line 688
    :cond_5
    sget-object v1, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->n:[I

    array-length v1, v1

    if-lt v0, v1, :cond_6

    .line 689
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    aget-object v0, v0, v2

    aget v0, v0, v8

    invoke-virtual {p2, v0, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_2

    .line 694
    :cond_6
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    aget-object v1, v1, v2

    aget v1, v1, v3

    invoke-virtual {p2, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 695
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    aget-object v1, v1, v2

    aget v1, v1, v7

    invoke-virtual {p2, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 696
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    aget-object v1, v1, v2

    aget v1, v1, v9

    invoke-virtual {p2, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 697
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    aget-object v1, v1, v2

    aget v1, v1, v8

    invoke-virtual {p2, v1, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 700
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->m:Lamj;

    invoke-virtual {v1}, Lamj;->f()I

    move-result v1

    .line 702
    if-le v1, v0, :cond_9

    .line 703
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->m:Lamj;

    invoke-virtual {v1, v0}, Lamj;->b(I)Lamq;

    move-result-object v0

    move-object v1, v0

    .line 710
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "LifeTransparentWidget > current page = "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v4, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->l:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " & display index = "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " & type = "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 713
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    aget-object v0, v0, v2

    aget v0, v0, v3

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(Landroid/content/Context;Landroid/widget/RemoteViews;I)V

    .line 714
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    aget-object v0, v0, v2

    aget v0, v0, v7

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(Landroid/content/Context;Landroid/widget/RemoteViews;I)V

    .line 715
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    aget-object v0, v0, v2

    aget v0, v0, v9

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(Landroid/content/Context;Landroid/widget/RemoteViews;I)V

    .line 716
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    aget-object v0, v0, v2

    aget v0, v0, v8

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(Landroid/content/Context;Landroid/widget/RemoteViews;I)V

    .line 719
    if-eqz v1, :cond_b

    .line 720
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_a

    .line 721
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v1}, Lamq;->j()I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 726
    :goto_4
    iget-object v4, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    aget-object v4, v4, v2

    aget v4, v4, v3

    invoke-virtual {p2, v4, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 728
    sget-boolean v0, Laky;->b:Z

    if-nez v0, :cond_7

    .line 729
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->h:Ljava/util/HashMap;

    invoke-virtual {v1}, Lamq;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 730
    iget-object v4, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    aget-object v4, v4, v2

    aget v4, v4, v3

    const-string v5, "setBackgroundResource"

    invoke-virtual {p2, v4, v5, v0}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 734
    :cond_7
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    aget-object v0, v0, v2

    aget v4, v0, v7

    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->h:Ljava/util/HashMap;

    invoke-virtual {v1}, Lamq;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v4, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 737
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Lamq;->o()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 739
    const-string v1, "ko_KR"

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 740
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v4, 0x5

    if-le v1, v4, :cond_8

    .line 741
    const-string v1, " "

    const-string v4, "\n"

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 744
    :cond_8
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    aget-object v1, v1, v2

    aget v1, v1, v9

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 705
    :cond_9
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "LifeModeWidget getNotUsedType "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->m:Lamj;

    sub-int v6, v0, v1

    invoke-virtual {v5, v6}, Lamj;->e(I)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lalj;->b(Ljava/lang/String;)V

    .line 707
    sget-object v4, Lami;->b:Landroid/util/SparseArray;

    iget-object v5, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->m:Lamj;

    sub-int/2addr v0, v1

    invoke-virtual {v5, v0}, Lamj;->e(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamq;

    move-object v1, v0

    goto/16 :goto_3

    .line 723
    :cond_a
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v1}, Lamq;->i()I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 746
    :cond_b
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a02a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 747
    iget-object v1, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    aget-object v1, v1, v2

    aget v1, v1, v3

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 749
    sget-boolean v0, Laky;->b:Z

    if-nez v0, :cond_2

    .line 750
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    aget-object v0, v0, v2

    aget v0, v0, v3

    const-string v1, "setBackgroundResource"

    const v4, 0x7f0201a1

    invoke-virtual {p2, v0, v1, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 751
    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->d:[[I

    aget-object v0, v0, v2

    aget v1, v0, v7

    iget-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->h:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto/16 :goto_2

    .line 755
    :cond_c
    return-void
.end method

.method private b(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 758
    .line 759
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 760
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 762
    array-length v0, v0

    if-lez v0, :cond_0

    .line 763
    const/4 v0, 0x1

    .line 767
    :goto_0
    return v0

    .line 765
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 772
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 773
    const-string v1, "page"

    sget v2, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->l:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 774
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 776
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 783
    const-string v0, "refreshData"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 785
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "location_mode"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 787
    if-nez v0, :cond_0

    .line 788
    const-string v0, "location mode is LOCATION_MODE_OFF! request allow location!"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 789
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    const/16 v1, 0xa

    const/16 v2, 0x3e9

    invoke-virtual {v0, p1, v1, v2}, Lanf;->a(Landroid/content/Context;II)V

    .line 796
    :goto_0
    return-void

    .line 793
    :cond_0
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0, v3}, Lage;->c(Z)V

    .line 794
    invoke-direct {p0, p1, v3}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V
    .locals 3

    .prologue
    .line 491
    const-string v0, "updateWidget()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 492
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    invoke-virtual {p2, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 495
    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 496
    return-void
.end method

.method public onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 260
    invoke-super {p0, p1, p2, p3, p4}, Landroid/appwidget/AppWidgetProvider;->onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V

    .line 261
    return-void
.end method

.method public onDeleted(Landroid/content/Context;[I)V
    .locals 0

    .prologue
    .line 266
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 267
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 272
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 273
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 278
    const-string v0, "LifeTransparentWidget> onEnabled"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 279
    invoke-static {p1}, Lall;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->c:Ljava/lang/String;

    .line 280
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 281
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 282
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 283
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 286
    :cond_0
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 287
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const v2, 0x7f0a0011

    const/4 v6, -0x1

    const v5, 0x10008000

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 293
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LifeTransparentWidget> onReceive : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 294
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 295
    const-string v0, "LifeWidget> onReceive : intent.getAction() is null"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 401
    :goto_0
    return-void

    .line 298
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 300
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 301
    const-string v0, "LifeTransparentWidget> onReceive : no instance"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 305
    :cond_1
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 306
    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p1}, Lane;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 307
    :cond_2
    sget-object v0, Lamu;->c:Landroid/net/Uri;

    if-eqz v0, :cond_3

    .line 308
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 310
    if-eqz v0, :cond_4

    .line 311
    sget-object v1, Lamu;->c:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 317
    :cond_3
    :goto_1
    const v0, 0x7f0a02bd

    invoke-static {p1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 313
    :cond_4
    const-string v0, "resolver is null !!"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 319
    :cond_5
    invoke-static {p1}, Lami;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 320
    invoke-static {p1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 323
    :cond_6
    invoke-virtual {p0, p1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(Landroid/content/Context;)V

    .line 400
    :cond_7
    :goto_2
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 325
    :cond_8
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA_RESULT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 326
    const-string v0, "error"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 327
    invoke-direct {p0, p1, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(Landroid/content/Context;I)V

    .line 328
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 329
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 330
    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_2

    .line 331
    :cond_9
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_SHOW_NEXT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 333
    const-string v0, "nextPage"

    invoke-virtual {p2, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->l:I

    .line 334
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LifeTransparentWidget> onReceive : mCurrentPage nextPage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 336
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->c(Landroid/content/Context;)V

    goto :goto_2

    .line 338
    :cond_a
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_VIEW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 339
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 340
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 342
    const-string v2, "page"

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->l:I

    .line 343
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LifeTransparentWidget> onReceive : mCurrentPage ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->l:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 345
    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto/16 :goto_2

    .line 346
    :cond_b
    const-string v1, "com.sec.android.GeoLookout.SETTING_CHANGE_START"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "com.sec.android.GeoLookout.SETTING_CHANGE_FINISH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "android.appwidget.action.APPWIDGET_UPDATE_OPTIONS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 350
    :cond_c
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 351
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 353
    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto/16 :goto_2

    .line 354
    :cond_d
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_TAG_TO_START"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 356
    invoke-static {p1}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 357
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 358
    const v0, 0x7f0a0008

    invoke-static {p1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    .line 360
    :cond_e
    const v0, 0x7f0a0016

    invoke-static {p1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    .line 364
    :cond_f
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "safety_care_disaster_user_agree"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_10

    .line 366
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 367
    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 368
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 369
    :cond_10
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "safetycare_earthquake"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_7

    .line 371
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 372
    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 373
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 381
    :cond_11
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_WIGET_APP_START"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 382
    invoke-static {p1}, Lami;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 383
    invoke-static {p1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_2

    .line 385
    :cond_12
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;

    invoke-direct {v1, p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 387
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_13

    .line 388
    const-string v0, "indexType"

    sget-object v2, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->o:[I

    aget v2, v2, v3

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 393
    :goto_3
    const-string v2, "indexType"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 395
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 396
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    .line 390
    :cond_13
    const-string v0, "indexType"

    sget-object v2, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->n:[I

    aget v2, v2, v3

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    goto :goto_3
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 406
    const-string v0, "LifeTransparentWidget> onUpdate"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 413
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 414
    invoke-static {p1}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    :cond_0
    const-string v0, "LifeTransparentWidget> onUpdate : FUNTION TURNED OFF"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 424
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 458
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 459
    return-void

    .line 425
    :cond_1
    invoke-static {p1}, Lami;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 426
    sget-object v0, Lamu;->c:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 427
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 429
    if-eqz v0, :cond_3

    .line 430
    sget-object v1, Lamu;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 436
    :cond_2
    :goto_1
    sget-boolean v0, Laky;->b:Z

    if-nez v0, :cond_4

    .line 437
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0

    .line 432
    :cond_3
    const-string v0, "resolver is null !!"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    goto :goto_1

    .line 439
    :cond_4
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0

    .line 441
    :cond_5
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "safety_care_disaster_user_agree"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_6

    .line 446
    const-string v0, "LifeTransparentWidget> onUpdate : USER AGREE CHECK FAIL"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 447
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0

    .line 450
    :cond_6
    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {p1}, Lane;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 451
    :cond_7
    const-string v0, "LifeTransparentWidget> onUpdate : ROAMING"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 452
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0

    .line 455
    :cond_8
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0
.end method
