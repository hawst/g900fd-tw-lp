.class public Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;
.super Landroid/app/Activity;
.source "DisasterDialogRegisterActivity.java"


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0xa

.field public static final d:I = 0x14

.field public static final e:I = 0x1e

.field public static final f:I = 0x28

.field private static g:Lais;


# instance fields
.field private h:Landroid/app/ProgressDialog;

.field private i:Landroid/app/AlertDialog;

.field private j:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->g:Lais;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 45
    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;

    .line 47
    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->i:Landroid/app/AlertDialog;

    .line 49
    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->j:Landroid/widget/Toast;

    .line 280
    return-void
.end method

.method public static a()Lais;
    .locals 1

    .prologue
    .line 209
    sget-object v0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->g:Lais;

    return-object v0
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->i:Landroid/app/AlertDialog;

    return-object p1
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 219
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 220
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;

    const v1, 0x7f0a0029

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;

    invoke-virtual {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 225
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;

    new-instance v1, Laii;

    invoke-direct {v1, p0}, Laii;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 232
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 237
    :goto_0
    return-void

    .line 233
    :catch_0
    move-exception v0

    .line 234
    invoke-static {v0}, Lalj;->e(Ljava/lang/Throwable;)V

    .line 235
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->finish()V

    goto :goto_0
.end method

.method private a(II)V
    .locals 3

    .prologue
    .line 262
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 263
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 264
    if-eqz v1, :cond_0

    .line 265
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 267
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 269
    invoke-static {p1, p2}, Laij;->a(II)Laij;

    move-result-object v1

    .line 270
    const-string v2, "dialog"

    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 275
    :goto_0
    return-void

    .line 271
    :catch_0
    move-exception v0

    .line 272
    invoke-static {v0}, Lalj;->e(Ljava/lang/Throwable;)V

    .line 273
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->finish()V

    goto :goto_0
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->b()V

    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->a(I)V

    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;II)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->a(II)V

    return-void
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->i:Landroid/app/AlertDialog;

    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 244
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 246
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    :cond_0
    :goto_0
    return-void

    .line 248
    :catch_0
    move-exception v0

    .line 249
    invoke-static {v0}, Lalj;->e(Ljava/lang/Throwable;)V

    .line 250
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 425
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->j:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 426
    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->j:Landroid/widget/Toast;

    .line 430
    :goto_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->j:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 431
    return-void

    .line 428
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->j:Landroid/widget/Toast;

    invoke-virtual {v0, p2}, Landroid/widget/Toast;->setText(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 173
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 174
    new-instance v0, Lais;

    invoke-direct {v0, p0}, Lais;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;)V

    sput-object v0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->g:Lais;

    .line 175
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 176
    const-string v1, "network_status"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 177
    const/16 v2, 0xa

    if-eq v1, v2, :cond_0

    const/16 v2, 0x1e

    if-ne v1, v2, :cond_1

    .line 178
    :cond_0
    const-string v2, "network_error_code"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 179
    sget-object v2, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->g:Lais;

    invoke-virtual {v2, v1, v0}, Lais;->a(II)V

    .line 184
    :goto_0
    return-void

    .line 181
    :cond_1
    sget-object v0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->g:Lais;

    invoke-virtual {v0, v1, v3}, Lais;->a(II)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 188
    const-string v0, "onDestroy()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 190
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->i:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->i:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->i:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->i:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 195
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->i:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    :cond_1
    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;

    .line 201
    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->i:Landroid/app/AlertDialog;

    .line 202
    sput-object v1, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->g:Lais;

    .line 203
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 205
    :goto_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 206
    return-void

    .line 197
    :catch_0
    move-exception v0

    .line 198
    :try_start_1
    invoke-static {v0}, Lalj;->e(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 200
    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;

    .line 201
    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->i:Landroid/app/AlertDialog;

    .line 202
    sput-object v1, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->g:Lais;

    .line 203
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    goto :goto_0

    .line 200
    :catchall_0
    move-exception v0

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->h:Landroid/app/ProgressDialog;

    .line 201
    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->i:Landroid/app/AlertDialog;

    .line 202
    sput-object v1, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->g:Lais;

    .line 203
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    throw v0
.end method
