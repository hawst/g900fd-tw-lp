.class public Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "DisasterHistoryActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Landroid/widget/AdapterView$OnItemClickListener;

.field private b:Landroid/content/Context;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/GeoLookout/db/DisasterInfo;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/app/ProgressDialog;

.field private e:Landroid/widget/ImageView;

.field private f:Ljava/lang/String;

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Laja;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/widget/ListView;

.field private i:Landroid/widget/LinearLayout;

.field private j:Landroid/widget/ImageView;

.field private k:Lvs;

.field private l:Lamf;

.field private m:Lamf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 359
    new-instance v0, Laiv;

    invoke-direct {v0, p0}, Laiv;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->a:Landroid/widget/AdapterView$OnItemClickListener;

    .line 727
    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;)Lamf;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->m:Lamf;

    return-object v0
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 476
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "createIntentForShare(), file path = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 477
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 479
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 480
    const-string v1, "android.intent.extra.STREAM"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 481
    const-string v1, "image/jpeg"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 482
    const-string v1, "exit_on_sent"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 483
    const-string v1, "android.intent.extra.TEXT"

    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 484
    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 485
    return-object v0
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 8

    .prologue
    const v3, 0x7f0a0562

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 676
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v1

    .line 678
    const-string v0, ""

    .line 680
    sparse-switch v1, :sswitch_data_0

    .line 724
    :goto_0
    return-object v0

    .line 682
    :sswitch_0
    const-string v0, "03"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 683
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 685
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00bd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 686
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 687
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getCity()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 688
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 689
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 690
    const-string v1, " %.1f"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailsMagnitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 691
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 692
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsDepth()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 694
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 699
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 700
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 701
    const-string v1, " %.1f"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailsMagnitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 702
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 703
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->l:Lamf;

    invoke-static {v1, p1, v2, v6}, Lall;->a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;Lamf;Z)I

    move-result v1

    .line 704
    invoke-static {p0, v1}, Lall;->e(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 705
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 709
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->l:Lamf;

    invoke-static {v0, p1, v1, v6}, Lall;->a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;Lamf;Z)I

    move-result v0

    .line 710
    invoke-static {p0, v0}, Lall;->e(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 714
    :sswitch_2
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailSpeed()Ljava/lang/String;

    move-result-object v0

    .line 715
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 716
    const-string v0, "0"

    .line 718
    :cond_1
    invoke-static {p0, v0}, Lall;->i(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 680
    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_1
        0x69 -> :sswitch_0
        0x6e -> :sswitch_2
        0x70 -> :sswitch_2
    .end sparse-switch
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 147
    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->setContentView(I)V

    .line 148
    const v0, 0x7f0e0010

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 149
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030009

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 151
    const v0, 0x7f0e0011

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->h:Landroid/widget/ListView;

    .line 153
    const v0, 0x7f0e003b

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->e:Landroid/widget/ImageView;

    .line 154
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->e:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 156
    sget-boolean v0, Laky;->a:Z

    if-eq v0, v4, :cond_0

    sget-boolean v0, Laky;->c:Z

    if-ne v0, v4, :cond_1

    :cond_0
    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 165
    :goto_0
    const v0, 0x7f0e0012

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->i:Landroid/widget/LinearLayout;

    .line 167
    return-void

    .line 158
    :cond_1
    sget-boolean v0, Laky;->b:Z

    if-ne v0, v4, :cond_2

    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 159
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->e:Landroid/widget/ImageView;

    const v1, 0x7f02000d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 160
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->f:Ljava/lang/String;

    goto :goto_0

    .line 162
    :cond_2
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private a(I)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 761
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 762
    invoke-static {v0}, Laja;->a(Laja;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 763
    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->m:Lamf;

    .line 765
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->m:Lamf;

    if-eqz v1, :cond_0

    .line 766
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    invoke-virtual {v1}, Lvs;->f()V

    .line 768
    const-string v1, "03"

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 769
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    new-instance v2, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v2}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    new-instance v3, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v4, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->m:Lamf;

    iget-wide v4, v4, Lamf;->a:D

    iget-object v6, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->m:Lamf;

    iget-wide v6, v6, Lamf;->b:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->b:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v4

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v5

    invoke-static {v3, v4, v5}, Lall;->b(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v3

    if-ne v3, v8, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getWarningIcon()I

    move-result v0

    :goto_0
    invoke-static {v0}, Lzx;->a(I)Lzw;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lzw;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v0

    invoke-virtual {v1, v0}, Lvs;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Laah;

    .line 777
    :goto_1
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->m:Lamf;

    iget-wide v2, v1, Lamf;->a:D

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->m:Lamf;

    iget-wide v4, v1, Lamf;->b:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-static {v0}, Lvr;->a(Lcom/google/android/gms/maps/model/LatLng;)Lvq;

    move-result-object v0

    .line 778
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    invoke-virtual {v1, v0}, Lvs;->a(Lvq;)V

    .line 780
    :cond_0
    return-void

    .line 769
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getWatchIcon()I

    move-result v0

    goto :goto_0

    .line 773
    :cond_2
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    new-instance v2, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v2}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    new-instance v3, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v4, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->m:Lamf;

    iget-wide v4, v4, Lamf;->a:D

    iget-object v6, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->m:Lamf;

    iget-wide v6, v6, Lamf;->b:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v3

    if-ne v3, v8, :cond_3

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getWarningIcon()I

    move-result v0

    :goto_2
    invoke-static {v0}, Lzx;->a(I)Lzw;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lzw;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v0

    invoke-virtual {v1, v0}, Lvs;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Laah;

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getWatchIcon()I

    move-result v0

    goto :goto_2
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;I)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->a(I)V

    return-void
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;)Lvs;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 288
    const-string v0, "START, setUpMap"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 290
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    if-nez v0, :cond_0

    .line 291
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0e003a

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/MapFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapFragment;->c()Lvs;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    .line 294
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    if-eqz v0, :cond_1

    .line 296
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    invoke-virtual {v0}, Lvs;->m()Lxf;

    move-result-object v0

    invoke-virtual {v0, v2}, Lxf;->a(Z)V

    .line 297
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    invoke-virtual {v0}, Lvs;->m()Lxf;

    move-result-object v0

    invoke-virtual {v0, v2}, Lxf;->b(Z)V

    .line 299
    const v0, 0x7f0e003c

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->j:Landroid/widget/ImageView;

    .line 300
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->j:Landroid/widget/ImageView;

    new-instance v1, Lait;

    invoke-direct {v1, p0}, Lait;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 311
    :cond_1
    return-void
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 746
    const-string v0, "START = showMobileLink"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 748
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 750
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 751
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 757
    :cond_0
    :goto_0
    return-void

    .line 752
    :catch_0
    move-exception v0

    .line 753
    const-string v1, "ActivityNotFoundException "

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    .line 754
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 314
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->b:Landroid/content/Context;

    invoke-static {v0}, Laly;->a(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->c:Ljava/util/ArrayList;

    .line 315
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 316
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->g:Ljava/util/ArrayList;

    .line 319
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    .line 321
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->g:Ljava/util/ArrayList;

    new-instance v3, Laja;

    invoke-direct {v3, p0, v0}, Laja;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 319
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 324
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DisasterHistoryActivity : disasterItem.size() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 326
    new-instance v0, Laiu;

    invoke-direct {v0, p0}, Laiu;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 336
    :goto_1
    return-void

    .line 334
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->g()V

    goto :goto_1
.end method

.method public static synthetic c(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->d()V

    return-void
.end method

.method public static synthetic d(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->h:Landroid/widget/ListView;

    return-object v0
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 340
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    .line 341
    new-instance v0, Laiy;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->g:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Laiy;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;Ljava/util/ArrayList;)V

    .line 342
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->h:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 343
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->h:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->a:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 344
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    .line 345
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->h:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/high16 v1, 0x43c10000    # 386.0f

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v3, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 347
    :cond_0
    invoke-direct {p0, v4}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->a(I)V

    .line 349
    sget-boolean v0, Laky;->a:Z

    if-eq v0, v3, :cond_1

    sget-boolean v0, Laky;->c:Z

    if-ne v0, v3, :cond_3

    :cond_1
    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 357
    :cond_2
    :goto_0
    return-void

    .line 351
    :cond_3
    sget-boolean v0, Laky;->b:Z

    if-ne v0, v3, :cond_4

    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 352
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 354
    :cond_4
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static synthetic e(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->b:Landroid/content/Context;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 371
    const-string v0, "START - shareScreenShot"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 373
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    if-nez v0, :cond_0

    .line 374
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0e003a

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/MapFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapFragment;->c()Lvs;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    if-eqz v0, :cond_1

    .line 378
    new-instance v0, Laiw;

    invoke-direct {v0, p0}, Laiw;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;)V

    .line 470
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    invoke-virtual {v1, v0}, Lvs;->a(Lwr;)V

    .line 472
    :cond_1
    return-void
.end method

.method public static synthetic f(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->e:Landroid/widget/ImageView;

    return-object v0
.end method

.method private f()Ljava/lang/String;
    .locals 6

    .prologue
    .line 490
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getGoogleMapURL(), myLocation = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->l:Lamf;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 492
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->l:Lamf;

    if-nez v0, :cond_0

    .line 493
    const/4 v0, 0x0

    .line 502
    :goto_0
    return-object v0

    .line 495
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->l:Lamf;

    iget-wide v0, v0, Lamf;->a:D

    .line 496
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->l:Lamf;

    iget-wide v2, v2, Lamf;->b:D

    .line 498
    sget-boolean v4, Laky;->c:Z

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 499
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "http://mo.amap.com/?q="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 501
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "http://maps.google.com/maps?f=q&q=("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")&z=15"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 787
    const-string v0, "setDbUpdateError"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 788
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->i:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 790
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->h:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->h:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 794
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 795
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 797
    :cond_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->j:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 798
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 800
    :cond_2
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 171
    const-string v0, "onBackPressed"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 174
    const-string v0, "onBackPressed not null"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 175
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->g()V

    .line 181
    :goto_0
    return-void

    .line 177
    :cond_0
    const-string v0, "onBackPressed null"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 178
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 250
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 252
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 253
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->f:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 254
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 250
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e003b
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x400

    const/4 v7, 0x0

    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    .line 100
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 104
    iput-object p0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->b:Landroid/content/Context;

    .line 105
    iput-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->l:Lamf;

    .line 107
    sget-boolean v0, Laky;->a:Z

    if-ne v0, v6, :cond_2

    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->f:Ljava/lang/String;

    .line 118
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 120
    const-string v1, "Latitude"

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 121
    const-string v2, "Logitude"

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 123
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_0

    .line 124
    new-instance v2, Lamf;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-direct {v2, v4, v5, v0, v1}, Lamf;-><init>(DD)V

    iput-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->l:Lamf;

    .line 127
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_1

    .line 129
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02015c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 130
    const v1, 0x7f0a0051

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 131
    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 132
    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 133
    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 137
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->a()V

    .line 138
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->b()V

    .line 142
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->c()V

    .line 144
    return-void

    .line 109
    :cond_2
    sget-boolean v0, Laky;->b:Z

    if-ne v0, v6, :cond_3

    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00ae

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->f:Ljava/lang/String;

    goto :goto_0

    .line 111
    :cond_3
    sget-boolean v0, Laky;->c:Z

    if-ne v0, v6, :cond_4

    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 112
    iput-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 114
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->f:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0d0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 186
    sget-boolean v0, Laky;->b:Z

    if-nez v0, :cond_0

    .line 187
    const v0, 0x7f0e0119

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 189
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 200
    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->l:Lamf;

    .line 201
    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->m:Lamf;

    .line 203
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 208
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 211
    :cond_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    if-eqz v0, :cond_2

    .line 212
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    invoke-virtual {v0}, Lvs;->f()V

    .line 213
    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->k:Lvs;

    .line 216
    :cond_2
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->j:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 217
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 220
    :cond_3
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 221
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 224
    :cond_4
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 225
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 227
    :cond_5
    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->d:Landroid/app/ProgressDialog;

    .line 229
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->h:Landroid/widget/ListView;

    if-eqz v0, :cond_6

    .line 230
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->h:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->h:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 236
    :cond_6
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 237
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 242
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->finish()V

    .line 243
    const/4 v0, 0x1

    .line 246
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 265
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 284
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 268
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->finish()V

    goto :goto_0

    .line 271
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->e()V

    goto :goto_0

    .line 275
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.helphub.HELP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 276
    const-string v1, "helphub:appid"

    const-string v2, "safety_assistance"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 277
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 265
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0e0118 -> :sswitch_1
        0x7f0e0119 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 194
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 195
    return-void
.end method
