.class public Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;
.super Landroid/app/Activity;
.source "DisasterLifeMainActivity.java"


# static fields
.field private static final c:Ljava/lang/String; = "DisasterLifeMainActivity"


# instance fields
.field public a:Z

.field b:Landroid/content/BroadcastReceiver;

.field private d:Landroid/content/Context;

.field private e:Landroid/app/ActionBar;

.field private f:Lagw;

.field private g:Lcom/sec/android/GeoLookout/view/LifeModeListView;

.field private h:Lapx;

.field private i:Landroid/widget/LinearLayout;

.field private j:Landroid/widget/LinearLayout;

.field private k:Landroid/app/ProgressDialog;

.field private l:Landroid/app/AlertDialog;

.field private m:Landroid/app/AlertDialog;

.field private n:Landroid/widget/CheckBox;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/RelativeLayout;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/ImageView;

.field private final t:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->a:Z

    .line 91
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->t:[I

    .line 199
    new-instance v0, Lajg;

    invoke-direct {v0, p0}, Lajg;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->b:Landroid/content/BroadcastReceiver;

    return-void

    .line 91
    nop

    :array_0
    .array-data 4
        0x260
        0x261
        0x25e
        0x25f
        0x25a
        0x25c
        0x263
        0x25b
        0x25d
        0x262
        0x259
    .end array-data
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->j()V

    return-void
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 219
    const-string v0, "initActionbarLayout"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->e:Landroid/app/ActionBar;

    .line 221
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->e:Landroid/app/ActionBar;

    const v1, 0x7f0a0020

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 222
    new-instance v0, Lagw;

    invoke-direct {v0, p0}, Lagw;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->f:Lagw;

    .line 223
    return-void
.end method

.method public static synthetic c(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->l()V

    return-void
.end method

.method public static synthetic d(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)Lapx;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->h:Lapx;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 226
    const-string v0, "initLifeModeLayout"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 228
    const v0, 0x7f0e00c5

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/view/LifeModeListView;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->g:Lcom/sec/android/GeoLookout/view/LifeModeListView;

    .line 229
    const v0, 0x7f0e00c4

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->i:Landroid/widget/LinearLayout;

    .line 230
    const v0, 0x7f0e00c6

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->j:Landroid/widget/LinearLayout;

    .line 231
    new-instance v0, Lapx;

    invoke-direct {v0, p0}, Lapx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->h:Lapx;

    .line 232
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->g:Lcom/sec/android/GeoLookout/view/LifeModeListView;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->h:Lapx;

    invoke-virtual {v0, v1}, Lcom/sec/android/GeoLookout/view/LifeModeListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 234
    const v0, 0x7f0e00bf

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->p:Landroid/widget/RelativeLayout;

    .line 235
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->p:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 236
    const v0, 0x7f0e00c1

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->q:Landroid/widget/TextView;

    .line 237
    const v0, 0x7f0e00c3

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->r:Landroid/widget/TextView;

    .line 238
    const v0, 0x7f0e00c2

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->s:Landroid/widget/ImageView;

    .line 239
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->s:Landroid/widget/ImageView;

    new-instance v1, Lajh;

    invoke-direct {v1, p0}, Lajh;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 247
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    invoke-static {v0}, Lakw;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->j:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 251
    const/4 v1, -0x1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 252
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->g:Lcom/sec/android/GeoLookout/view/LifeModeListView;

    invoke-virtual {v1, v0}, Lcom/sec/android/GeoLookout/view/LifeModeListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 255
    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 258
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "insertdata "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->t:[I

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    invoke-static {v0}, Lamu;->e(Landroid/content/Context;)Z

    .line 260
    return-void
.end method

.method public static synthetic e(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->h()V

    return-void
.end method

.method public static synthetic f(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->n:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 379
    const-string v0, "showAutoRefreshAlertDialog"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 381
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->a()V

    .line 382
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 385
    const v0, 0x7f0a049e

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 386
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 387
    const v2, 0x7f03001b

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 388
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 390
    const v0, 0x7f0e0084

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->n:Landroid/widget/CheckBox;

    .line 391
    const v0, 0x7f0e0085

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->o:Landroid/widget/TextView;

    .line 392
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->n:Landroid/widget/CheckBox;

    new-instance v2, Laji;

    invoke-direct {v2, p0}, Laji;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 402
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->o:Landroid/widget/TextView;

    new-instance v2, Lajj;

    invoke-direct {v2, p0}, Lajj;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 415
    const v0, 0x7f0a0002

    new-instance v2, Lajk;

    invoke-direct {v2, p0}, Lajk;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 427
    const v0, 0x7f0a0001

    new-instance v2, Lajl;

    invoke-direct {v2, p0}, Lajl;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 434
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->m:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 435
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->m:Landroid/app/AlertDialog;

    .line 436
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->m:Landroid/app/AlertDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 437
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 439
    :cond_0
    return-void
.end method

.method private g()V
    .locals 12

    .prologue
    const/4 v11, 0x6

    const/4 v10, 0x3

    const v9, 0x7f0a055e

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 451
    invoke-static {p0}, Lall;->p(Landroid/content/Context;)I

    move-result v0

    .line 452
    const-string v1, "showUpdateTimeDialog"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 454
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->b()V

    .line 455
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 457
    const v2, 0x7f0a049e

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 459
    new-array v2, v11, [Ljava/lang/CharSequence;

    .line 460
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a049f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    .line 461
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    .line 462
    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 463
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    .line 464
    const/4 v3, 0x4

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    const/16 v6, 0xc

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 465
    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    const/16 v6, 0x18

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 467
    new-instance v3, Lajm;

    invoke-direct {v3, p0}, Lajm;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 482
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->l:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 483
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->l:Landroid/app/AlertDialog;

    .line 484
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->l:Landroid/app/AlertDialog;

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 485
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->l:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 487
    :cond_0
    return-void
.end method

.method public static synthetic g(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->g()V

    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 527
    const-string v1, "handleRequestLifeInformation"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 529
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "location_mode"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 532
    if-nez v1, :cond_0

    .line 533
    const-string v0, "location mode is LOCATION_MODE_OFF! request allow location!"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 534
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    const/16 v2, 0xa

    const/16 v3, 0x3e9

    invoke-virtual {v0, v1, v2, v3}, Lanf;->a(Landroid/content/Context;II)V

    .line 549
    :goto_0
    return-void

    .line 538
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->i()V

    .line 540
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->g:Lcom/sec/android/GeoLookout/view/LifeModeListView;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/view/LifeModeListView;->getCount()I

    move-result v1

    .line 541
    :goto_1
    if-ge v0, v1, :cond_1

    .line 542
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->g:Lcom/sec/android/GeoLookout/view/LifeModeListView;

    invoke-virtual {v2, v0}, Lcom/sec/android/GeoLookout/view/LifeModeListView;->collapseGroup(I)Z

    .line 541
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 545
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 546
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 547
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->k:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 553
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->k:Landroid/app/ProgressDialog;

    .line 554
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->k:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    const v2, 0x7f0a04ee

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 555
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->k:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 558
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->k:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 559
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 561
    :cond_1
    return-void
.end method

.method private j()V
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->k:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 565
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->k:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 566
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->k:Landroid/app/ProgressDialog;

    .line 568
    :cond_0
    return-void
.end method

.method private k()V
    .locals 9

    .prologue
    const v1, 0x7f0a0495

    const v8, 0x7f0a0020

    const v7, 0x7f0a0029

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 572
    const-string v0, ""

    .line 573
    const-string v0, ""

    .line 575
    sget-boolean v0, Laky;->j:Z

    if-eqz v0, :cond_0

    .line 576
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 580
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0496

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 591
    :goto_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    new-instance v2, Lajf;

    invoke-direct {v2, p0}, Lajf;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v2, Lajn;

    invoke-direct {v2, p0}, Lajn;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 610
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 611
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 612
    return-void

    .line 585
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 587
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0496

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private l()V
    .locals 4

    .prologue
    .line 615
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->p:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->p:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 629
    :cond_0
    :goto_0
    return-void

    .line 619
    :cond_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    invoke-static {v0}, Lamu;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 620
    if-nez v0, :cond_2

    .line 621
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->q:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 626
    :goto_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    invoke-static {v1}, Lall;->c(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lall;->e(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 628
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->r:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 623
    :cond_2
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->q:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 370
    const-string v0, "clearAutoRefreshAlertDialog"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 372
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->m:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->m:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 374
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->m:Landroid/app/AlertDialog;

    .line 376
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 442
    const-string v0, "clearWatchEnvironmentPopup"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 444
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->l:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->l:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 446
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->l:Landroid/app/AlertDialog;

    .line 448
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 491
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onActivityResult : requestCode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", resultCode = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 493
    packed-switch p2, :pswitch_data_0

    .line 500
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 501
    return-void

    .line 495
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->finish()V

    goto :goto_0

    .line 493
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 310
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 311
    const-string v0, "onConfigurationChanged"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 312
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->f:Lagw;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->f:Lagw;

    invoke-virtual {v0, p1}, Lagw;->a(Landroid/content/res/Configuration;)V

    .line 315
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/high16 v6, 0x7f040000

    const v5, 0x7f030025

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 107
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 109
    const-string v0, "onCreate"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    .line 111
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 113
    sget-boolean v1, Laky;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    invoke-static {v1}, Lakw;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 115
    const v1, 0x7f0a04ba

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a00b7

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Laje;

    invoke-direct {v3, p0}, Laje;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 124
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 197
    :goto_0
    return-void

    .line 125
    :cond_0
    const-string v1, "safety_care_disaster_user_agree"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_1

    .line 127
    const-string v0, "TAG"

    const-string v1, "NeedtoAgree"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 129
    const-string v1, "com.sec.android.GeoLookout"

    const-string v2, "com.sec.android.GeoLookout.widget.SafetyCareDisasterDisclaimerActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 131
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->startActivity(Landroid/content/Intent;)V

    .line 132
    const v0, 0x7f040001

    invoke-virtual {p0, v6, v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->overridePendingTransition(II)V

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->finish()V

    goto :goto_0

    .line 134
    :cond_1
    invoke-static {}, Lane;->a()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    invoke-static {v1}, Lane;->c(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 135
    :cond_2
    if-eqz v0, :cond_3

    .line 136
    sget-object v1, Lamu;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 141
    :goto_1
    invoke-virtual {p0, v5}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->setContentView(I)V

    .line 142
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->c()V

    .line 144
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->b:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA_RESULT"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 146
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->b:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA_CANCEL"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    goto :goto_0

    .line 138
    :cond_3
    const-string v0, "contentResolver is NULL"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 148
    :cond_4
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    invoke-static {v1}, Lami;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 149
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    const v1, 0x7f0a0011

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 150
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->finish()V

    goto/16 :goto_0

    .line 151
    :cond_5
    invoke-static {p0}, Lage;->A(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 152
    invoke-static {p0}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 153
    const v0, 0x7f0a0008

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 154
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->finish()V

    goto/16 :goto_0

    .line 156
    :cond_6
    const v0, 0x7f0a0016

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 157
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->finish()V

    goto/16 :goto_0

    .line 160
    :cond_7
    invoke-static {p0}, Lage;->z(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 161
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    const-class v2, Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 162
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 163
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->startActivity(Landroid/content/Intent;)V

    .line 164
    const v0, 0x7f040001

    invoke-virtual {p0, v6, v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->overridePendingTransition(II)V

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->finish()V

    goto/16 :goto_0

    .line 166
    :cond_8
    sget-boolean v1, Laky;->b:Z

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    invoke-static {v1}, Lami;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 167
    if-eqz v0, :cond_9

    .line 168
    sget-object v1, Lamu;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 173
    :goto_2
    invoke-virtual {p0, v5}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->setContentView(I)V

    .line 174
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->c()V

    .line 176
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->b:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA_RESULT"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 178
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->b:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA_CANCEL"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    goto/16 :goto_0

    .line 170
    :cond_9
    const-string v0, "contentResolver is NULL"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_2

    .line 181
    :cond_a
    const v0, 0x7f030024

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->setContentView(I)V

    .line 183
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->c()V

    .line 184
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d()V

    .line 185
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->e()V

    .line 186
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->l()V

    .line 188
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->c()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 189
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->i()V

    .line 192
    :cond_b
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->b:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA_RESULT"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 194
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->b:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA_CANCEL"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 7

    .prologue
    const v3, 0x7f0e011c

    const v2, 0x7f0e0107

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 269
    const-string v0, "onCreateOptionsMenu"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 270
    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    invoke-static {v0}, Lane;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 305
    :cond_0
    :goto_0
    return v5

    .line 273
    :cond_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    invoke-static {v0}, Lami;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-boolean v0, Laky;->b:Z

    if-nez v0, :cond_0

    .line 276
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0d0002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 278
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_3

    .line 279
    const v0, 0x7f0e011b

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 281
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 286
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0495

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a04ba

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 292
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.samsung.helphub"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 293
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    rem-int/lit8 v0, v0, 0xa

    .line 294
    if-ne v0, v5, :cond_0

    .line 295
    const v0, 0x7f0e011c

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 297
    :catch_0
    move-exception v0

    .line 298
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 301
    :cond_3
    invoke-interface {p1, v2}, Landroid/view/Menu;->removeItem(I)V

    .line 302
    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 264
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 520
    const-string v0, "DisasterLifeMainActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 523
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 524
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 328
    const-string v0, "onOptionsItemSelected"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->f:Lagw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->f:Lagw;

    invoke-virtual {v0, p1}, Lagw;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    const/4 v0, 0x1

    .line 365
    :goto_0
    return v0

    .line 333
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 365
    :goto_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0

    .line 336
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->finish()V

    goto :goto_1

    .line 340
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lall;->o(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 341
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->f()V

    goto :goto_1

    .line 343
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->g()V

    goto :goto_1

    .line 348
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/activity/LifemodeIndexInformationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 349
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 353
    :sswitch_3
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->k()V

    goto :goto_1

    .line 357
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->d:Landroid/content/Context;

    invoke-static {v0}, Lall;->D(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 358
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 333
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0e0107 -> :sswitch_3
        0x7f0e011a -> :sswitch_1
        0x7f0e011b -> :sswitch_2
        0x7f0e011c -> :sswitch_4
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 514
    const-string v0, "DisasterLifeMainActivity"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 515
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 516
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 319
    invoke-super {p0, p1}, Landroid/app/Activity;->onPostCreate(Landroid/os/Bundle;)V

    .line 320
    const-string v0, "onPostCreate"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 321
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->f:Lagw;

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->f:Lagw;

    invoke-virtual {v0}, Lagw;->b()V

    .line 324
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 505
    sget-object v0, Lami;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamj;

    .line 506
    invoke-virtual {v0}, Lamj;->e()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lamj;->f()I

    move-result v0

    sget-object v1, Lami;->b:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 507
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;->e()V

    .line 509
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 510
    return-void
.end method
