.class public Lcom/sec/android/GeoLookout/db/DisasterParser;
.super Ljava/lang/Object;
.source "DisasterParser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4

    .prologue
    const/16 v0, 0x69

    const/4 v1, 0x0

    .line 446
    .line 448
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTypeId type : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " dv : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 450
    if-nez p0, :cond_1

    .line 451
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getTypeId() unkown type! type="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    move v0, v1

    .line 572
    :cond_0
    :goto_0
    return v0

    .line 456
    :cond_1
    const-string v2, "02"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 457
    const/16 v1, 0x141

    .line 458
    const-string v2, "Earthquake"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 460
    const-string v0, "Typhoons"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 461
    const/16 v0, 0x12d

    goto :goto_0

    .line 462
    :cond_2
    const-string v0, "Rainstorms"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 463
    const/16 v0, 0x12e

    goto :goto_0

    .line 464
    :cond_3
    const-string v0, "Snowstorms"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 465
    const/16 v0, 0x12f

    goto :goto_0

    .line 466
    :cond_4
    const-string v0, "Cold wave"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 467
    const/16 v0, 0x130

    goto :goto_0

    .line 468
    :cond_5
    const-string v0, "Gale"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 469
    const/16 v0, 0x131

    goto :goto_0

    .line 470
    :cond_6
    const-string v0, "Dust storms"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 471
    const/16 v0, 0x132

    goto :goto_0

    .line 472
    :cond_7
    const-string v0, "Heat"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 473
    const/16 v0, 0x133

    goto :goto_0

    .line 474
    :cond_8
    const-string v0, "Drought"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 475
    const/16 v0, 0x134

    goto :goto_0

    .line 476
    :cond_9
    const-string v0, "Frost"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 477
    const/16 v0, 0x135

    goto :goto_0

    .line 478
    :cond_a
    const-string v0, "Dense fog"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 479
    const/16 v0, 0x136

    goto :goto_0

    .line 480
    :cond_b
    const-string v0, "Haze"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 481
    const/16 v0, 0x137

    goto/16 :goto_0

    .line 482
    :cond_c
    const-string v0, "Lightning"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 483
    const/16 v0, 0x138

    goto/16 :goto_0

    .line 484
    :cond_d
    const-string v0, "Hail"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 485
    const/16 v0, 0x139

    goto/16 :goto_0

    .line 486
    :cond_e
    const-string v0, "Icy road"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 487
    const/16 v0, 0x13a

    goto/16 :goto_0

    .line 488
    :cond_f
    const-string v0, "Cold"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 489
    const/16 v0, 0x13b

    goto/16 :goto_0

    .line 490
    :cond_10
    const-string v0, "Dust haze"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 491
    const/16 v0, 0x13c

    goto/16 :goto_0

    .line 492
    :cond_11
    const-string v0, "Thunderstorm and gale"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 493
    const/16 v0, 0x13d

    goto/16 :goto_0

    .line 494
    :cond_12
    const-string v0, "Forest fire"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 495
    const/16 v0, 0x13e

    goto/16 :goto_0

    .line 496
    :cond_13
    const-string v0, "Cool down"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 497
    const/16 v0, 0x13f

    goto/16 :goto_0

    .line 498
    :cond_14
    const-string v0, "Road snow"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 499
    const/16 v0, 0x140

    goto/16 :goto_0

    .line 504
    :cond_15
    const-string v2, "Active Wildfire"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    const-string v2, "Wildfire"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 505
    :cond_16
    const/16 v0, 0x65

    goto/16 :goto_0

    .line 506
    :cond_17
    const-string v2, "Air Pollution"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    const-string v2, "Ozone"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 507
    :cond_18
    const/16 v0, 0x66

    goto/16 :goto_0

    .line 508
    :cond_19
    const-string v2, "Avalanche"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 509
    const/16 v0, 0x67

    goto/16 :goto_0

    .line 510
    :cond_1a
    const-string v2, "Dust Storm"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 511
    const/16 v0, 0x68

    goto/16 :goto_0

    .line 512
    :cond_1b
    const-string v2, "Earthquake"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 514
    const-string v0, "Extreme Cold"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 515
    const/16 v0, 0x6a

    goto/16 :goto_0

    .line 516
    :cond_1c
    const-string v0, "Extreme Heat"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 517
    const/16 v0, 0x6b

    goto/16 :goto_0

    .line 518
    :cond_1d
    const-string v0, "Flood"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 519
    const/16 v0, 0x6c

    goto/16 :goto_0

    .line 520
    :cond_1e
    const-string v0, "Extreme Wind"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1f

    const-string v0, "Gale"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 521
    :cond_1f
    const/16 v0, 0x6d

    goto/16 :goto_0

    .line 522
    :cond_20
    const-string v0, "Severe Thunderstorm"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 523
    const/16 v0, 0x6e

    goto/16 :goto_0

    .line 524
    :cond_21
    const-string v0, "Snow and Ice"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 525
    const/16 v0, 0x6f

    goto/16 :goto_0

    .line 526
    :cond_22
    const-string v0, "Tornado"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 527
    const/16 v0, 0x70

    goto/16 :goto_0

    .line 528
    :cond_23
    const-string v0, "Tropical Alert"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 529
    const/16 v0, 0x71

    goto/16 :goto_0

    .line 530
    :cond_24
    const-string v0, "Tsunami"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 531
    const/16 v0, 0x72

    goto/16 :goto_0

    .line 532
    :cond_25
    const-string v0, "Ashfall"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 533
    const/16 v0, 0x73

    goto/16 :goto_0

    .line 534
    :cond_26
    const-string v0, "Volcano"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 535
    const/16 v0, 0x74

    goto/16 :goto_0

    .line 536
    :cond_27
    const-string v0, "Winter Storm"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 537
    const/16 v0, 0x75

    goto/16 :goto_0

    .line 541
    :cond_28
    const-string v0, "Heavy Rain"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 542
    const/16 v0, 0xca

    goto/16 :goto_0

    .line 543
    :cond_29
    const-string v0, "Dry"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 544
    const/16 v0, 0xc9

    goto/16 :goto_0

    .line 545
    :cond_2a
    const-string v0, "High Seas"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 546
    const/16 v0, 0xcc

    goto/16 :goto_0

    .line 547
    :cond_2b
    const-string v0, "Heavy Snow"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 548
    const/16 v0, 0xcb

    goto/16 :goto_0

    .line 549
    :cond_2c
    const-string v0, "ThunderStorm"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 550
    const/16 v0, 0xcd

    goto/16 :goto_0

    .line 551
    :cond_2d
    const-string v0, "Typhoon"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 552
    const/16 v0, 0xce

    goto/16 :goto_0

    .line 556
    :cond_2e
    const-string v0, "Wind Storm"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 557
    const/16 v0, 0x191

    goto/16 :goto_0

    .line 558
    :cond_2f
    const-string v0, "Thick Fog"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 559
    const/16 v0, 0x192

    goto/16 :goto_0

    .line 560
    :cond_30
    const-string v0, "Freezing"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 561
    const/16 v0, 0x193

    goto/16 :goto_0

    .line 562
    :cond_31
    const-string v0, "Snow Accretion"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 563
    const/16 v0, 0x194

    goto/16 :goto_0

    .line 564
    :cond_32
    const-string v0, "Snow Melting"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 565
    const/16 v0, 0x195

    goto/16 :goto_0

    .line 566
    :cond_33
    const-string v0, "Frost"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 567
    const/16 v0, 0x196

    goto/16 :goto_0

    .line 568
    :cond_34
    const-string v0, "Storm Surge"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 569
    const/16 v0, 0x197

    goto/16 :goto_0

    :cond_35
    move v0, v1

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 12

    .prologue
    const/4 v4, 0x1

    .line 45
    new-instance v5, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    invoke-direct {v5}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;-><init>()V

    .line 46
    new-instance v6, Lcom/google/gson/Gson;

    invoke-direct {v6}, Lcom/google/gson/Gson;-><init>()V

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DISASTER_INFO - ServerData DisasterBEParsedInfo : ============================================================\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterParser;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n============================================================\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 51
    invoke-static {p1}, Lcom/sec/android/GeoLookout/db/DisasterParser;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Laow;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 53
    const-class v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;

    invoke-virtual {v6, p1, v0}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;

    .line 54
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->type:Ljava/lang/String;

    iget-object v2, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->dv:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterParser;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 55
    iget-object v2, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->did:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 56
    const/4 v0, 0x0

    .line 111
    :goto_0
    return-object v0

    .line 58
    :cond_0
    const-string v2, "04"

    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->dv:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "03"

    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->dv:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 59
    :cond_1
    const-string v2, "Make did for Korea & Japan"

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 60
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 62
    if-eqz v2, :cond_2

    .line 63
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "typeID = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", location.id = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v7, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->location:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Location;

    iget-object v7, v7, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Location;->id:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", parsedInfo.significance = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v7, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->significance:I

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lalj;->b(Ljava/lang/String;)V

    .line 64
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v7, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->location:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Location;

    iget-object v7, v7, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Location;->id:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget v7, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->significance:I

    invoke-virtual {v3, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 65
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->did:Ljava/lang/String;

    .line 66
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DID = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->did:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 70
    :cond_2
    const-string v2, "02"

    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->dv:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 71
    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->did:Ljava/lang/String;

    const-string v7, "/"

    const-string v8, "_"

    invoke-virtual {v3, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->did:Ljava/lang/String;

    .line 72
    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->did:Ljava/lang/String;

    invoke-virtual {v5, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDid(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 73
    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->type:Ljava/lang/String;

    invoke-virtual {v5, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setType(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 74
    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setTypeId(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 75
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->country:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setCountry(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 76
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->location:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Location;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Location;->id:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocationID(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 77
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->location:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Location;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Location;->type:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/db/DisasterParser;->f(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocationType(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 78
    new-instance v1, Lamf;

    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->location:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Location;

    iget-wide v8, v3, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Location;->lat:D

    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->location:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Location;

    iget-wide v10, v3, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Location;->lon:D

    invoke-direct {v1, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocation(Lamf;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 79
    if-ne v2, v4, :cond_3

    const/4 v1, 0x2

    :goto_1
    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setSignificance(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 80
    iget-wide v2, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->startTime:J

    invoke-virtual {v5, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setStartTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 81
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->status:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/db/DisasterParser;->g(Ljava/lang/String;)S

    move-result v1

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setActionStatus(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 82
    iget-wide v2, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->endTime:J

    const-wide/16 v8, 0x0

    cmp-long v1, v2, v8

    if-nez v1, :cond_4

    iget-wide v2, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->startTime:J

    const-wide/32 v8, 0x5265c00

    add-long/2addr v2, v8

    :goto_2
    invoke-virtual {v5, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setEndTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 83
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setCity(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 84
    iget-boolean v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->polling:Z

    if-eqz v1, :cond_5

    move v1, v4

    :goto_3
    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setPolling(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 85
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->headline:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setHeadline(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 86
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->details:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;

    iget-wide v2, v1, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;->magnitude:D

    invoke-virtual {v5, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailsMagnitude(D)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 87
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->details:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;->link:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLink(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 88
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->details:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;->polygon:Ljava/util/List;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/db/DisasterParser;->a(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setPolygonList(Ljava/util/ArrayList;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 89
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->details:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;->polygon:Ljava/util/List;

    invoke-virtual {v6, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setPolygonJSON(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 90
    iget-wide v2, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->rtime:J

    invoke-virtual {v5, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setReceivedTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 91
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->details:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;->speed:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailSpeed(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 92
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->dv:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDV(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 93
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->details:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;->eq_type:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setChnDetailsEqType(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 94
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->id:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setMsgId(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 95
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->details:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;->source:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDisasterSource(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 96
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->details:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;->province:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setChnDetailsProvince(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 97
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->details:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;->city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setChnDetailsCity(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 98
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->details:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;->stationName:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setChnDetailsStationName(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 99
    iget v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->significance:I

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setChnSignificanceLevel(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 100
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->details:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;->depth:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsDepth(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 101
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->details:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;->level:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsLevel(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 102
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->details:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;->zone_country:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsZoneCountry(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 103
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->details:Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$Details;->zone_city:Ljava/lang/String;

    invoke-virtual {v5, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsZoneCity(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 105
    iget v0, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->uid:I

    invoke-virtual {v5, v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setMultiPointUid(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 109
    invoke-virtual {v5}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->build()Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DISASTER_INFO - DisasterInfo : ============================================================\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n============================================================\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 79
    :cond_3
    iget v1, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->significance:I

    goto/16 :goto_1

    .line 82
    :cond_4
    iget-wide v2, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo;->endTime:J

    goto/16 :goto_2

    .line 84
    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_3
.end method

.method public static a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/GeoLookout/db/DisasterInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    .line 276
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    .line 278
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 280
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DISASTER_INFO - ServerData DisasterHPHTParsedInfo : ============================================================\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Lcom/sec/android/GeoLookout/db/DisasterParser;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n============================================================\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->d(Ljava/lang/String;)V

    .line 282
    const-class v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo;

    .line 283
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DISASTER_INFO - ParsedData DisasterHPHTParsedInfo : ============================================================\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n============================================================\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 285
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo;->msgs:Ljava/util/List;

    if-eqz v1, :cond_6

    .line 286
    iget-object v0, v0, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo;->msgs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 287
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 288
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message;

    .line 289
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message;->did:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 292
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message;->did:Ljava/lang/String;

    const-string v4, "/"

    const-string v5, "_"

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message;->did:Ljava/lang/String;

    .line 293
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message;->hp:Ljava/util/List;

    if-eqz v1, :cond_3

    .line 294
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message;->hp:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 295
    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 296
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;

    .line 297
    new-instance v5, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    invoke-direct {v5}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;-><init>()V

    .line 298
    iget-object v6, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->id:Ljava/lang/String;

    if-eqz v6, :cond_1

    .line 301
    iget-object v6, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->id:Ljava/lang/String;

    const-string v7, "/"

    const-string v8, "_"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->id:Ljava/lang/String;

    .line 302
    sget-boolean v6, Laky;->b:Z

    if-eqz v6, :cond_2

    invoke-static {}, Lane;->a()Z

    move-result v6

    if-nez v6, :cond_2

    .line 303
    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message;->did:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDid(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->id:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setHTHPId(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->type:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setType(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->rtime:J

    invoke-virtual {v6, v8, v9}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setStartTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;->windGust:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailWindGust(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    invoke-virtual {v6, v13}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDataType(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;

    iget-object v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;->loc:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setHeadline(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;

    iget-object v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;->cardinal:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailDirectionCardinal(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;->r15w:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailDirectionDegrees(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;->maxwindspeed:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailWindMax(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;->speed:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailStormSpeed(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;->forecastHour:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailForecastHour(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;->cpress:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailPressureMB(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    new-instance v7, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->lon:D

    invoke-direct {v7, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocation(Lamf;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 332
    :goto_1
    invoke-virtual {v5}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->build()Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 319
    :cond_2
    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message;->did:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDid(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->id:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setHTHPId(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->type:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setType(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->rtime:J

    invoke-virtual {v6, v8, v9}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setStartTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;->windGust:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailWindGust(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;->directionDegrees:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailDirectionDegrees(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;

    iget-object v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;->directionCardinal:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailDirectionCardinal(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;->forwardSpeed:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailStormSpeed(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;->windMax:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailWindMax(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP$Detail;->forecastHour:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailForecastHour(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    invoke-virtual {v6, v13}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDataType(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    new-instance v7, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HP;->lon:D

    invoke-direct {v7, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocation(Lamf;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    goto :goto_1

    .line 335
    :cond_3
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message;->ht:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 336
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message;->ht:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 337
    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 338
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;

    .line 339
    new-instance v5, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    invoke-direct {v5}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;-><init>()V

    .line 341
    iget-object v6, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->id:Ljava/lang/String;

    if-eqz v6, :cond_4

    .line 344
    iget-object v6, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->id:Ljava/lang/String;

    const-string v7, "/"

    const-string v8, "_"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->id:Ljava/lang/String;

    .line 345
    sget-boolean v6, Laky;->b:Z

    if-eqz v6, :cond_5

    invoke-static {}, Lane;->a()Z

    move-result v6

    if-nez v6, :cond_5

    .line 346
    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message;->did:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDid(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->id:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setHTHPId(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->type:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setType(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->rtime:J

    invoke-virtual {v6, v8, v9}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setStartTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;->windGust:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailWindGust(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    invoke-virtual {v6, v12}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDataType(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;

    iget-object v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;->loc:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setHeadline(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;

    iget-object v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;->cardinal:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailDirectionCardinal(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;->r15w:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailDirectionDegrees(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;->maxwindspeed:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailWindMax(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;->speed:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailStormSpeed(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;->cpress:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailPressureMB(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    new-instance v7, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->lon:D

    invoke-direct {v7, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocation(Lamf;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 374
    :goto_3
    invoke-virtual {v5}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->build()Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 361
    :cond_5
    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message;->did:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDid(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->id:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setHTHPId(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->type:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setType(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->rtime:J

    invoke-virtual {v6, v8, v9}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setStartTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;->windGust:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailWindGust(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;

    iget-object v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;->directionCardinal:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailDirectionCardinal(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;->directionDegrees:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailDirectionDegrees(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;->stormSpeed:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailStormSpeed(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;->windSpeed:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailWindMax(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    invoke-virtual {v6, v12}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDataType(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->details:Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;

    iget v7, v7, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT$Detail;->pressureMB:I

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailPressureMB(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    new-instance v7, Lamf;

    iget-wide v8, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->lat:D

    iget-wide v10, v1, Lcom/sec/android/GeoLookout/db/DisasterHPHTParsedInfo$Message$HT;->lon:D

    invoke-direct {v7, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocation(Lamf;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    goto :goto_3

    .line 379
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DISASTER_INFO - DisasterInfo : ============================================================\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n============================================================\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 380
    return-object v2
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/GeoLookout/db/DisasterInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 115
    invoke-static {p0}, Lcom/sec/android/GeoLookout/db/DisasterParser;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Laow;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 117
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 119
    new-instance v6, Lcom/google/gson/Gson;

    invoke-direct {v6}, Lcom/google/gson/Gson;-><init>()V

    .line 120
    const-class v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo;

    invoke-virtual {v6, p0, v0}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo;

    .line 122
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo;->msgs:Ljava/util/List;

    if-eqz v1, :cond_6

    .line 123
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo;->msgs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 124
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "number of disaster : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo;->msgs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 127
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 128
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;

    .line 129
    new-instance v8, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    invoke-direct {v8}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;-><init>()V

    .line 130
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->type:Ljava/lang/String;

    iget-object v2, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->dv:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterParser;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 132
    const-string v2, "04"

    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->dv:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "03"

    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->dv:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 133
    :cond_1
    const-string v2, "Make did for Korea & Japan"

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 134
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 136
    if-eqz v2, :cond_2

    .line 137
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "typeID = "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ", location.id = "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v9, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->location:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Location;

    iget-object v9, v9, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Location;->id:Ljava/lang/String;

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v9, ", parsedInfo.significance = "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v9, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->significance:I

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lalj;->b(Ljava/lang/String;)V

    .line 138
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v9, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->location:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Location;

    iget-object v9, v9, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Location;->id:Ljava/lang/String;

    invoke-virtual {v3, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget v9, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->significance:I

    invoke-virtual {v3, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 139
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->did:Ljava/lang/String;

    .line 140
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DID = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->did:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 144
    :cond_2
    iget-object v2, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->did:Ljava/lang/String;

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 148
    const-string v2, "02"

    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->dv:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 149
    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->did:Ljava/lang/String;

    const-string v9, "/"

    const-string v10, "_"

    invoke-virtual {v3, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->did:Ljava/lang/String;

    .line 150
    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->did:Ljava/lang/String;

    invoke-virtual {v8, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDid(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 151
    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->type:Ljava/lang/String;

    invoke-virtual {v8, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setType(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 152
    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setTypeId(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 153
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->country:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setCountry(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 154
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->location:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Location;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Location;->id:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocationID(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 155
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->location:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Location;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Location;->type:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/db/DisasterParser;->f(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocationType(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 156
    new-instance v1, Lamf;

    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->location:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Location;

    iget-wide v10, v3, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Location;->lat:D

    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->location:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Location;

    iget-wide v12, v3, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Location;->lon:D

    invoke-direct {v1, v10, v11, v12, v13}, Lamf;-><init>(DD)V

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocation(Lamf;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 157
    if-ne v2, v4, :cond_3

    const/4 v1, 0x2

    :goto_1
    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setSignificance(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 158
    iget-wide v2, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->startTime:J

    invoke-virtual {v8, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setStartTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 159
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->status:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/db/DisasterParser;->g(Ljava/lang/String;)S

    move-result v1

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setActionStatus(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 160
    iget-wide v2, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->endTime:J

    const-wide/16 v10, 0x0

    cmp-long v1, v2, v10

    if-nez v1, :cond_4

    iget-wide v2, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->startTime:J

    const-wide/32 v10, 0x5265c00

    add-long/2addr v2, v10

    :goto_2
    invoke-virtual {v8, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setEndTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 161
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->city:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setCity(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 162
    iget-boolean v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->polling:Z

    if-eqz v1, :cond_5

    move v1, v4

    :goto_3
    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setPolling(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 163
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->headline:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setHeadline(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 164
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->details:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;

    iget-wide v2, v1, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;->magnitude:D

    invoke-virtual {v8, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailsMagnitude(D)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 165
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->details:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;->link:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLink(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 167
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->details:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;->polygon:Ljava/util/List;

    invoke-virtual {v6, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setPolygonJSON(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 168
    iget-wide v2, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->rtime:J

    invoke-virtual {v8, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setReceivedTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 169
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->details:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;->speed:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailSpeed(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 170
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->dv:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDV(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 171
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->details:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;->eq_type:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setChnDetailsEqType(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 172
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->id:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setMsgId(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 173
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->details:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;->source:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDisasterSource(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 174
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->details:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;->province:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setChnDetailsProvince(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 175
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->details:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;->city:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setChnDetailsCity(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 176
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->details:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;->stationName:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setChnDetailsStationName(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 177
    iget v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->significance:I

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setChnSignificanceLevel(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 178
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->details:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;->depth:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsDepth(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 179
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->details:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;->level:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsLevel(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 180
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->details:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;->zone_country:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsZoneCountry(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 181
    iget-object v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->details:Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;

    iget-object v1, v1, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages$Details;->zone_city:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsZoneCity(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 183
    iget v0, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->uid:I

    invoke-virtual {v8, v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setMultiPointUid(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 187
    invoke-virtual {v8}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->build()Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 189
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 157
    :cond_3
    iget v1, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->significance:I

    goto/16 :goto_1

    .line 160
    :cond_4
    iget-wide v2, v0, Lcom/sec/android/GeoLookout/db/DisasterRefreshDataParsedInfo$Messages;->endTime:J

    goto/16 :goto_2

    .line 162
    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 192
    :cond_6
    return-object v5
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/GeoLookout/db/DisasterInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fromHistoryJsonToDisasterInfos msg : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/sec/android/GeoLookout/db/DisasterParser;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 210
    invoke-static {p0}, Lcom/sec/android/GeoLookout/db/DisasterParser;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Laow;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 212
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 214
    new-instance v2, Lcom/google/gson/Gson;

    invoke-direct {v2}, Lcom/google/gson/Gson;-><init>()V

    .line 215
    const-class v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo;

    invoke-virtual {v2, p0, v0}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo;

    .line 216
    iget v3, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo;->seq:I

    if-lez v3, :cond_0

    .line 218
    iget v3, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo;->seq:I

    invoke-static {p1, v3, p2}, Lall;->a(Landroid/content/Context;II)V

    .line 224
    :cond_0
    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo;->msgs:Ljava/util/List;

    if-eqz v3, :cond_2

    .line 225
    iget-object v3, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo;->msgs:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 226
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "number of disaster history : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v0, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo;->msgs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 229
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 230
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;

    .line 231
    new-instance v4, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    invoke-direct {v4}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;-><init>()V

    .line 232
    iget-object v5, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->type:Ljava/lang/String;

    invoke-static {}, Lall;->f()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterParser;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 233
    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->did:Ljava/lang/String;

    if-eqz v6, :cond_1

    if-eqz v5, :cond_1

    .line 236
    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->did:Ljava/lang/String;

    const-string v7, "/"

    const-string v8, "_"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->did:Ljava/lang/String;

    .line 237
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->did:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->did:Ljava/lang/String;

    .line 239
    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->did:Ljava/lang/String;

    invoke-virtual {v4, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDid(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->type:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setType(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    iget-object v7, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->status:Ljava/lang/String;

    invoke-static {v7}, Lcom/sec/android/GeoLookout/db/DisasterParser;->g(Ljava/lang/String;)S

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setActionStatus(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setTypeId(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    new-instance v6, Lamf;

    iget-wide v8, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->lat:D

    iget-wide v10, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->lon:D

    invoke-direct {v6, v8, v9, v10, v11}, Lamf;-><init>(DD)V

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocation(Lamf;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->locid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocationID(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->loctype:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/GeoLookout/db/DisasterParser;->f(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocationType(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget-wide v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->stime:J

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setStartTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget-wide v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->rtime:J

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setReceivedTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget-wide v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->etime:J

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setEndTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->headline:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setHeadline(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->city:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setCity(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->country:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setCountry(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->details:Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;

    iget-wide v6, v6, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;->magnitude:D

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailsMagnitude(D)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->details:Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;

    iget-object v6, v6, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;->link:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLink(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->details:Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;

    iget-object v6, v6, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;->polygon:Ljava/util/List;

    invoke-static {v6}, Lcom/sec/android/GeoLookout/db/DisasterParser;->a(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setPolygonList(Ljava/util/ArrayList;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->details:Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;

    iget-object v6, v6, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;->polygon:Ljava/util/List;

    invoke-virtual {v2, v6}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setPolygonJSON(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->details:Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;

    iget-object v6, v6, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;->speed:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailSpeed(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->significance:I

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setSignificance(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setPolling(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->details:Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;

    iget-object v6, v6, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;->depth:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsDepth(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->details:Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;

    iget-object v6, v6, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;->level:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsLevel(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->details:Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;

    iget-object v6, v6, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;->zone_country:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsZoneCountry(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v5

    iget-object v0, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message;->details:Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;

    iget-object v0, v0, Lcom/sec/android/GeoLookout/db/DisasterHistoryParsedInfo$Message$Detail;->zone_city:Ljava/lang/String;

    invoke-virtual {v5, v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsZoneCity(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 264
    invoke-virtual {v4, p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setMultiPointUid(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 268
    invoke-virtual {v4}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->build()Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 271
    :cond_2
    return-object v1
.end method

.method private static a(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$PolyGon;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lamf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 197
    if-eqz p0, :cond_0

    .line 198
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 199
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$PolyGon;

    .line 201
    new-instance v3, Lamf;

    iget-wide v4, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$PolyGon;->lat:D

    iget-wide v6, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$PolyGon;->lon:D

    invoke-direct {v3, v4, v5, v6, v7}, Lamf;-><init>(DD)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 204
    :cond_0
    return-object v1
.end method

.method public static b(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterQLParsedInfo;
    .locals 2

    .prologue
    .line 384
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    .line 385
    const-class v1, Lcom/sec/android/GeoLookout/db/DisasterQLParsedInfo;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterQLParsedInfo;

    .line 386
    return-object v0
.end method

.method public static c(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lamf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 390
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fromJsonToPolygonList message : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 394
    if-eqz p0, :cond_0

    const-string v0, "null"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 395
    :cond_0
    const/4 v0, 0x0

    .line 410
    :goto_0
    return-object v0

    .line 398
    :cond_1
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    .line 399
    new-instance v1, Lamc;

    invoke-direct {v1}, Lamc;-><init>()V

    invoke-virtual {v1}, Lamc;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 401
    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 402
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 403
    if-eqz v0, :cond_2

    .line 404
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 405
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 406
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$PolyGon;

    .line 407
    new-instance v3, Lamf;

    iget-wide v4, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$PolyGon;->lat:D

    iget-wide v6, v0, Lcom/sec/android/GeoLookout/db/DisasterBEParsedInfo$PolyGon;->lon:D

    invoke-direct {v3, v4, v5, v6, v7}, Lamf;-><init>(DD)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 410
    goto :goto_0
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 423
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    .line 424
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "fromJsonToAlertList message : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 427
    :try_start_0
    const-class v1, Lcom/sec/android/GeoLookout/db/DisasterParser$DisasterTypeParsedInfo;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterParser$DisasterTypeParsedInfo;

    .line 428
    if-eqz v0, :cond_0

    .line 429
    iget-object v0, v0, Lcom/sec/android/GeoLookout/db/DisasterParser$DisasterTypeParsedInfo;->types:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 435
    :goto_0
    return-object v0

    .line 431
    :catch_0
    move-exception v0

    .line 432
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 435
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 439
    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->setPrettyPrinting()Lcom/google/gson/GsonBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    .line 440
    new-instance v1, Lcom/google/gson/JsonParser;

    invoke-direct {v1}, Lcom/google/gson/JsonParser;-><init>()V

    .line 441
    invoke-virtual {v1, p0}, Lcom/google/gson/JsonParser;->parse(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v1

    .line 442
    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->toJson(Lcom/google/gson/JsonElement;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static f(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 576
    const-string v0, "Z"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 577
    const/4 v0, 0x1

    .line 587
    :goto_0
    return v0

    .line 578
    :cond_0
    const-string v0, "C"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 579
    const/4 v0, 0x2

    goto :goto_0

    .line 580
    :cond_1
    const-string v0, "K"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 581
    const/4 v0, 0x3

    goto :goto_0

    .line 582
    :cond_2
    const-string v0, "S"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 583
    const/4 v0, 0x4

    goto :goto_0

    .line 584
    :cond_3
    const-string v0, "J"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 585
    const/4 v0, 0x5

    goto :goto_0

    .line 587
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static g(Ljava/lang/String;)S
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 592
    .line 594
    const-string v1, "NEW"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 604
    :cond_0
    :goto_0
    return v0

    .line 596
    :cond_1
    const-string v1, "UPDATE"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 597
    const/4 v0, 0x1

    goto :goto_0

    .line 598
    :cond_2
    const-string v1, "END"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 599
    const/4 v0, 0x2

    goto :goto_0

    .line 600
    :cond_3
    const-string v1, "CANCEL"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 601
    const/4 v0, 0x3

    goto :goto_0
.end method
