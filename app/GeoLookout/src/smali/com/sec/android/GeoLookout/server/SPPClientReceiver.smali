.class public Lcom/sec/android/GeoLookout/server/SPPClientReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SPPClientReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 142
    const-string v0, "SPP received : SERVICE_ABNORMALLY_STOPPED_ACTION"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 143
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 89
    invoke-static {p1}, Lalj;->a(Landroid/content/Context;)V

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "received push msg from server : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "appId"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 93
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 94
    :cond_0
    const-string v0, "ACTION_SPP_PUSH_MESSAGE : ignore the push message"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 95
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0, p1}, Lage;->j(Landroid/content/Context;)V

    .line 112
    :goto_0
    return-void

    .line 99
    :cond_1
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_2

    invoke-static {p1}, Lakw;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 100
    const-string v0, "ACTION_SPP_PUSH_MESSAGE : No SIM, ignore the push message for Japan"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 104
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/DisasterService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 105
    const-string v1, "com.sec.android.GeoLookout.ACTION_HANDLE_PUSH_MESSAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    const-string v1, "msg"

    const-string v2, "msg"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 107
    const-string v1, "appId"

    const-string v2, "appId"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 108
    const-string v1, "appData"

    const-string v2, "appData"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    const-string v1, "notiId"

    const-string v2, "notificationId"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 110
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 146
    const-string v0, "SPP received : NotificationAck result"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 147
    return-void
.end method

.method private b(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 115
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 116
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SPP received : ACTION_PACKAGE_ADDED. mPkgName:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 119
    const-string v1, "com.sec.spp.push"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    const-string v0, "SPP received : installed SPP. Request register"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 121
    invoke-static {}, Laop;->a()Laop;

    move-result-object v0

    invoke-virtual {v0, p1}, Laop;->a(Landroid/content/Context;)V

    .line 123
    :cond_0
    return-void
.end method

.method private c(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 126
    invoke-static {p1}, Lalj;->a(Landroid/content/Context;)V

    .line 128
    const-string v0, "SPP PushRegistrationChange !!"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 129
    invoke-static {}, Laoi;->a()Laoi;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Laoi;->b()Laou;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 131
    invoke-virtual {v0, p1, p2}, Laoi;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    const-string v0, "getSPPReceiver null!!"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 134
    invoke-static {p1}, Laov;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    const/4 v0, 0x0

    invoke-static {p1, v0}, Laov;->a(Landroid/content/Context;Z)V

    .line 136
    const-string v0, "getSPPReceiver setSPPRegistered(false)"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 28
    if-nez p1, :cond_1

    .line 29
    const-string v0, "Content is null!!"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    if-nez p2, :cond_2

    .line 33
    const-string v0, "Intent is null!!"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 37
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 38
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceive action:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->c(Ljava/lang/String;)V

    .line 40
    invoke-static {}, Land;->a()Land;

    move-result-object v1

    invoke-virtual {v1, p1}, Land;->c(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 41
    const-string v1, "212b03054d866273"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 43
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/server/SPPClientReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 52
    :cond_3
    :goto_1
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "com.sec.spp.ServiceDataDeletedAction"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 54
    :cond_4
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/server/SPPClientReceiver;->b(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 46
    :cond_5
    const-string v1, "3ed1813a06da0e2a"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 48
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/server/SPPClientReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_1

    .line 56
    :cond_6
    const-string v1, "com.sec.spp.RegistrationChangedAction"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 57
    const-string v0, "appId"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 58
    invoke-static {}, Land;->a()Land;

    move-result-object v1

    invoke-virtual {v1, p1}, Land;->c(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 59
    const-string v1, "212b03054d866273"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 61
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/server/SPPClientReceiver;->c(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 63
    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown app id! appId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 66
    :cond_8
    const-string v1, "3ed1813a06da0e2a"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 68
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/server/SPPClientReceiver;->c(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 70
    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown app id! appId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 74
    :cond_a
    const-string v1, "com.sec.spp.ServiceAbnormallyStoppedAction"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 76
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/server/SPPClientReceiver;->a()V

    goto/16 :goto_0

    .line 78
    :cond_b
    const-string v1, "com.sec.spp.NotificationAckResultAction"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 80
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/server/SPPClientReceiver;->b()V

    goto/16 :goto_0

    .line 82
    :cond_c
    const-string v1, "com.sec.spp.push.event.SPP_CONNECTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Location update response to SPP : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ldUpdate"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
