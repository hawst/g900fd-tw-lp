.class public Lcom/sec/android/GeoLookout/DisasterAlertCommonReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DisasterAlertCommonReceiver.java"


# instance fields
.field a:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 89
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 90
    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_1

    invoke-static {p1}, Lakw;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    const-string v0, "handleBootCompleted : No SIM, for Japan"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0, p1}, Lage;->g(Landroid/content/Context;)V

    .line 98
    invoke-static {p1}, Lanb;->b(Landroid/content/Context;)V

    .line 99
    invoke-static {p1}, Lcom/sec/android/GeoLookout/DisasterAlertService;->b(Landroid/content/Context;)V

    .line 100
    const-wide/16 v0, 0x0

    invoke-static {p1, v0, v1}, Lalx;->a(Landroid/content/Context;J)V

    .line 101
    invoke-static {}, Lalk;->a()Lalk;

    move-result-object v0

    invoke-virtual {v0, p1}, Lalk;->a(Landroid/content/Context;)V

    .line 103
    sget-boolean v0, Laky;->k:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    invoke-static {p1}, Lcom/sec/android/GeoLookout/lifemode/LifemodeService;->a(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "startActivityToDashboardFromWearable : dbid : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " did : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 170
    const/4 v0, 0x0

    .line 172
    if-ltz p2, :cond_0

    .line 173
    invoke-static {p1, p2}, Lalx;->a(Landroid/content/Context;I)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 176
    :cond_0
    if-nez v0, :cond_1

    .line 177
    const-string v1, "//"

    invoke-virtual {p3, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 179
    array-length v2, v1

    const/4 v3, 0x3

    if-lt v2, v3, :cond_1

    .line 180
    const/4 v0, 0x2

    aget-object v0, v1, v0

    .line 181
    invoke-static {p1, v0}, Lalx;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    .line 182
    int-to-long v2, v1

    invoke-static {p1, v0, v2, v3}, Lalx;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 186
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 187
    const v2, 0x10008000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 188
    if-eqz v0, :cond_2

    .line 189
    const-string v2, "DID"

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    const-string v2, "uid"

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 193
    :cond_2
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 194
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 110
    invoke-static {p1}, Lalj;->a(Landroid/content/Context;)V

    .line 112
    const-string v0, "isEnable"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 113
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "safetycare_earthquake"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 114
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "state = ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], enable = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->d(Ljava/lang/String;)V

    .line 116
    iput-object p1, p0, Lcom/sec/android/GeoLookout/DisasterAlertCommonReceiver;->a:Landroid/content/Context;

    .line 118
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 119
    const-string v0, "onReceive : disaster setting is enabled"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterAlertCommonReceiver;->a:Landroid/content/Context;

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/DisasterAlertCommonReceiver;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lage;->e(Landroid/content/Context;)V

    .line 125
    :goto_0
    return-void

    .line 122
    :cond_0
    const-string v0, "onReceive : disaster setting is disabled"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/GeoLookout/DisasterAlertCommonReceiver;->a:Landroid/content/Context;

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/DisasterAlertCommonReceiver;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lage;->f(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 128
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 129
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/DisasterService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 130
    const-string v1, "com.sec.android.GeoLookout.ACTION_CHANGE_LOCALE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 131
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 133
    :cond_0
    return-void
.end method

.method private b(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 136
    const-string v0, "NOTIFICATION_PACKAGE_NAME"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 138
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 139
    const-string v0, "handleDisasterShowOnDevice : wrong package name"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    const-string v0, "NOTIFICATION_ID"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 145
    const-string v1, "DID"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 147
    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/GeoLookout/DisasterAlertCommonReceiver;->a(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0
.end method

.method private c(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 152
    const-string v0, "NOTIFICATION_PACKAGE_NAME"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 154
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 155
    const-string v0, "handleDisasterShowOnDeviceB2 : wrong package name"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    const-string v0, "NOTIFICATION_ID"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 161
    const-string v1, "NOTIFICATION_CUSTOM_FIELD2"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 163
    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/GeoLookout/DisasterAlertCommonReceiver;->a(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0
.end method

.method private d(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 197
    const-string v0, "NOTIFICATION_PACKAGE_NAME"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 199
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 200
    const-string v0, "handleDisasterRemoveNotificationFromB2 : wrong package name"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    const-string v0, "NOTIFICATION_ID"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 206
    const-string v0, ""

    .line 207
    const-string v2, "NOTIFICATION_CUSTOM_FIELD2"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 209
    if-nez v2, :cond_4

    .line 210
    invoke-static {p1, v1}, Lalx;->a(Landroid/content/Context;I)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v2

    .line 212
    if-eqz v2, :cond_2

    .line 213
    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v0

    .line 223
    :cond_2
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 225
    if-lez v1, :cond_3

    .line 226
    invoke-static {}, Lalk;->a()Lalk;

    move-result-object v2

    invoke-virtual {v2, p1, v1, v0}, Lalk;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 230
    :cond_3
    sget-object v1, Lapd;->a:Lalb;

    if-eqz v1, :cond_0

    .line 231
    sget-object v1, Lapd;->a:Lalb;

    invoke-virtual {v1, v0}, Lalb;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 216
    :cond_4
    const-string v3, "//"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 218
    array-length v3, v2

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    .line 219
    const/4 v0, 0x2

    aget-object v0, v2, v0

    goto :goto_1
.end method

.method private e(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 238
    const-string v0, "NOTIFICATION_PACKAGE_NAME"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 240
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 241
    const-string v0, "handleDisasterRemoveNotificationFromWingtip : wrong package name"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 245
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    const-string v0, "NOTIFICATION_ID"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 247
    const-string v1, "DID"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 249
    if-eqz v1, :cond_0

    .line 250
    const-string v2, "//"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 252
    array-length v2, v1

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 254
    if-lez v0, :cond_2

    .line 255
    invoke-static {}, Lalk;->a()Lalk;

    move-result-object v2

    aget-object v3, v1, v4

    invoke-virtual {v2, p1, v0, v3}, Lalk;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 259
    :cond_2
    sget-object v0, Lapd;->a:Lalb;

    if-eqz v0, :cond_0

    .line 260
    sget-object v0, Lapd;->a:Lalb;

    aget-object v1, v1, v4

    invoke-virtual {v0, v1}, Lalb;->a(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 32
    if-nez p1, :cond_1

    .line 33
    const-string v0, "Content is null!!"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    if-nez p2, :cond_2

    .line 38
    const-string v0, "Intent is null!!"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 42
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 43
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceive() action:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->c(Ljava/lang/String;)V

    .line 45
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 46
    invoke-static {}, Land;->a()Land;

    move-result-object v0

    invoke-virtual {v0, p1}, Land;->a(Landroid/content/Context;)V

    .line 48
    invoke-static {p1}, Lanc;->b(Landroid/content/Context;)I

    move-result v0

    .line 49
    invoke-static {}, Land;->a()Land;

    move-result-object v1

    invoke-virtual {v1, p1}, Land;->d(Landroid/content/Context;)I

    move-result v1

    .line 51
    if-ne v0, v3, :cond_3

    if-nez v1, :cond_3

    .line 53
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lanc;->a(Landroid/content/Context;Z)V

    .line 54
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0, p1}, Lage;->d(Landroid/content/Context;)V

    .line 55
    invoke-static {}, Lalk;->a()Lalk;

    move-result-object v0

    invoke-virtual {v0, p1}, Lalk;->b(Landroid/content/Context;)V

    goto :goto_0

    .line 56
    :cond_3
    if-nez v0, :cond_4

    if-nez v1, :cond_4

    .line 58
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/DisasterAlertCommonReceiver;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 59
    :cond_4
    if-nez v0, :cond_5

    if-ne v1, v3, :cond_5

    .line 61
    invoke-static {p1, v3}, Lanc;->a(Landroid/content/Context;Z)V

    .line 62
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0, p1}, Lage;->d(Landroid/content/Context;)V

    .line 63
    invoke-static {}, Lalk;->a()Lalk;

    move-result-object v0

    invoke-virtual {v0, p1}, Lalk;->b(Landroid/content/Context;)V

    goto :goto_0

    .line 64
    :cond_5
    if-ne v0, v3, :cond_0

    if-ne v1, v3, :cond_0

    .line 66
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/DisasterAlertCommonReceiver;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 68
    :cond_6
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 69
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/DisasterAlertCommonReceiver;->b(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 70
    :cond_7
    const-string v1, "com.sec.android.GeoLookout.SETTING_CHANGE_START"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 71
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/DisasterAlertCommonReceiver;->a(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 72
    :cond_8
    const-string v1, "com.sec.android.GeoLookout.SHOW_DISASTER"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 73
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/DisasterAlertCommonReceiver;->b(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 74
    :cond_9
    const-string v1, "com.samsung.accessory.intent.action.LAUNCH_NOTIFICATION_ITEM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 75
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/DisasterAlertCommonReceiver;->c(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 76
    :cond_a
    const-string v1, "com.sec.android.GeoLookout.REMOVE_NOTIFICATION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/DisasterAlertCommonReceiver;->e(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 78
    :cond_b
    const-string v1, "com.samsung.accessory.intent.action.UPDATE_NOTIFICATION_ITEM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 79
    invoke-direct {p0, p1, p2}, Lcom/sec/android/GeoLookout/DisasterAlertCommonReceiver;->d(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 80
    :cond_c
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    sget-boolean v0, Laky;->k:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lane;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0, v3}, Lage;->c(Z)V

    .line 83
    invoke-static {p1}, Lcom/sec/android/GeoLookout/DisasterAlertService;->b(Landroid/content/Context;)V

    goto/16 :goto_0
.end method
