.class public Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "DisasterReportActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TabHost$OnTabChangeListener;
.implements Landroid/widget/TabHost$TabContentFactory;
.implements Lwn;


# static fields
.field private static final p:Ljava/lang/String; = "Ongoing"

.field private static final q:Ljava/lang/String; = "Past"


# instance fields
.field a:Landroid/widget/AdapterView$OnItemClickListener;

.field private b:Landroid/content/Context;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/GeoLookout/db/DisasterInfo;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/app/ProgressDialog;

.field private e:Landroid/widget/ImageView;

.field private f:Ljava/lang/String;

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lake;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/widget/ListView;

.field private i:Landroid/widget/ImageView;

.field private j:Lvs;

.field private k:Lamf;

.field private l:Lamf;

.field private m:Landroid/widget/TabHost;

.field private n:Landroid/app/ActionBar;

.field private o:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 114
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->o:Z

    .line 414
    new-instance v0, Lajz;

    invoke-direct {v0, p0}, Lajz;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->a:Landroid/widget/AdapterView$OnItemClickListener;

    .line 800
    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;)Lamf;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->l:Lamf;

    return-object v0
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 547
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "createIntentForShare(), file path = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 548
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 550
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 551
    const-string v1, "android.intent.extra.STREAM"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 552
    const-string v1, "image/jpeg"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 553
    const-string v1, "exit_on_sent"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 554
    const-string v1, "android.intent.extra.TEXT"

    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 555
    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 556
    return-object v0
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 8

    .prologue
    const v3, 0x7f0a0562

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 746
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v1

    .line 748
    const-string v0, ""

    .line 750
    sparse-switch v1, :sswitch_data_0

    .line 797
    :goto_0
    return-object v0

    .line 752
    :sswitch_0
    const-string v0, "03"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 753
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 755
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00bd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 756
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 757
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getCity()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 758
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 759
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 760
    const-string v1, " %.1f"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailsMagnitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 761
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 762
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsDepth()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 766
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 771
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 772
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 773
    const-string v1, " %.1f"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailsMagnitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 774
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 775
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->k:Lamf;

    invoke-static {v1, p1, v2, v6}, Lall;->a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;Lamf;Z)I

    move-result v1

    .line 777
    invoke-static {p0, v1}, Lall;->e(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 778
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 782
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->k:Lamf;

    invoke-static {v0, p1, v1, v6}, Lall;->a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;Lamf;Z)I

    move-result v0

    .line 783
    invoke-static {p0, v0}, Lall;->e(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 787
    :sswitch_2
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailSpeed()Ljava/lang/String;

    move-result-object v0

    .line 788
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 789
    const-string v0, "0"

    .line 791
    :cond_1
    invoke-static {p0, v0}, Lall;->i(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 750
    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_1
        0x69 -> :sswitch_0
        0x6e -> :sswitch_2
        0x70 -> :sswitch_2
    .end sparse-switch
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 167
    const-string v0, "initializeView"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 168
    const v0, 0x7f03000c

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->setContentView(I)V

    .line 169
    const v0, 0x7f0e0049

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 170
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030009

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 171
    const v0, 0x7f0e0048

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->h:Landroid/widget/ListView;

    .line 172
    const v0, 0x7f0e003b

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->e:Landroid/widget/ImageView;

    .line 173
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->e:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 175
    sget-boolean v0, Laky;->a:Z

    if-eq v0, v4, :cond_0

    sget-boolean v0, Laky;->c:Z

    if-ne v0, v4, :cond_1

    :cond_0
    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 185
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->b()V

    .line 187
    const v0, 0x7f0e0007

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TabHost;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->m:Landroid/widget/TabHost;

    .line 188
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->m:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->setup()V

    .line 190
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->m:Landroid/widget/TabHost;

    const-string v1, "Ongoing"

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/widget/TabHost$TabContentFactory;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0098

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    .line 193
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->m:Landroid/widget/TabHost;

    invoke-virtual {v1, v0}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->m:Landroid/widget/TabHost;

    const-string v1, "Past"

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/widget/TabHost$TabContentFactory;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0097

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    .line 198
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->m:Landroid/widget/TabHost;

    invoke-virtual {v1, v0}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 199
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->m:Landroid/widget/TabHost;

    invoke-virtual {v0, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 200
    return-void

    .line 178
    :cond_1
    sget-boolean v0, Laky;->b:Z

    if-ne v0, v4, :cond_2

    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 180
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->e:Landroid/widget/ImageView;

    const v1, 0x7f02000d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 181
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->f:Ljava/lang/String;

    goto :goto_0

    .line 183
    :cond_2
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private a(I)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 839
    const v0, 0x4019999a    # 2.4f

    .line 840
    const-string v1, "04"

    invoke-static {}, Laof;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 842
    const/high16 v0, 0x40c00000    # 6.0f

    move v1, v0

    .line 845
    :goto_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 847
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lake;

    .line 848
    invoke-static {v0}, Lake;->a(Lake;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v2

    .line 849
    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->l:Lamf;

    .line 851
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->l:Lamf;

    if-eqz v0, :cond_0

    .line 852
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    invoke-virtual {v0}, Lvs;->f()V

    .line 855
    const-string v0, "03"

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 856
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->b:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v3

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v4

    invoke-static {v0, v3, v4}, Lall;->b(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    .line 863
    :goto_1
    iget-object v3, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    new-instance v4, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v4}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {v4, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v0

    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v5, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->l:Lamf;

    iget-wide v6, v5, Lamf;->a:D

    iget-object v5, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->l:Lamf;

    iget-wide v8, v5, Lamf;->b:D

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v0

    if-ne v0, v10, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getWarningIcon()I

    move-result v0

    :goto_2
    invoke-static {v0}, Lzx;->a(I)Lzw;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lzw;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v0

    invoke-virtual {v3, v0}, Lvs;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Laah;

    .line 872
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->l:Lamf;

    iget-wide v2, v2, Lamf;->a:D

    iget-object v4, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->l:Lamf;

    iget-wide v4, v4, Lamf;->b:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-static {v0, v1}, Lvr;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lvq;

    move-result-object v0

    .line 875
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    invoke-virtual {v1, v0}, Lvs;->a(Lvq;)V

    .line 877
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->o:Z

    .line 888
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->invalidateOptionsMenu()V

    .line 889
    return-void

    .line 859
    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v0

    invoke-static {v0}, Lall;->c(I)I

    move-result v0

    .line 860
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 863
    :cond_2
    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getWatchIcon()I

    move-result v0

    goto :goto_2

    .line 879
    :cond_3
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->k:Lamf;

    iget-wide v2, v2, Lamf;->a:D

    iget-object v4, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->k:Lamf;

    iget-wide v4, v4, Lamf;->b:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-static {v0, v1}, Lvr;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lvq;

    move-result-object v0

    .line 882
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    invoke-virtual {v1}, Lvs;->f()V

    .line 883
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    invoke-virtual {v1, v0}, Lvs;->a(Lvq;)V

    .line 885
    iput-boolean v10, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->o:Z

    goto :goto_3

    :cond_4
    move v1, v0

    goto/16 :goto_0
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;I)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->a(I)V

    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 350
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->c:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 351
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "uid"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 352
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initializeDisasterFromDB : uid : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 353
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->b:Landroid/content/Context;

    invoke-static {v2, v1}, Laly;->a(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->c:Ljava/util/ArrayList;

    .line 356
    :cond_0
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->c:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 357
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->g:Ljava/util/ArrayList;

    .line 359
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    move v1, v0

    .line 361
    :goto_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 362
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    .line 363
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initializeDisasterFromDB disasterInfo-EndTime : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getEndTime()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 364
    if-eqz p1, :cond_2

    .line 365
    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getStartTime()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getEndTime()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lall;->a(JJ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 367
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->g:Ljava/util/ArrayList;

    new-instance v3, Lake;

    invoke-direct {v3, p0, v0}, Lake;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 361
    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 370
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getStartTime()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getEndTime()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lall;->a(JJ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 372
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->g:Ljava/util/ArrayList;

    new-instance v3, Lake;

    invoke-direct {v3, p0, v0}, Lake;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 377
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DisasterHistoryActivity : disasterItem.size() : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 379
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->c()V

    .line 384
    :goto_2
    return-void

    .line 382
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->f()V

    goto :goto_2
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;)Lvs;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 320
    const-string v0, "START, setUpMap"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 322
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    if-nez v0, :cond_0

    .line 323
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0e003a

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/MapFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapFragment;->c()Lvs;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    if-eqz v0, :cond_1

    .line 328
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    invoke-virtual {v0, p0}, Lvs;->a(Lwn;)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    invoke-virtual {v0}, Lvs;->m()Lxf;

    move-result-object v0

    invoke-virtual {v0, v2}, Lxf;->a(Z)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    invoke-virtual {v0}, Lvs;->m()Lxf;

    move-result-object v0

    invoke-virtual {v0, v2}, Lxf;->b(Z)V

    .line 332
    const v0, 0x7f0e003c

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->i:Landroid/widget/ImageView;

    .line 333
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->i:Landroid/widget/ImageView;

    new-instance v1, Lajy;

    invoke-direct {v1, p0}, Lajy;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 346
    :cond_1
    return-void
.end method

.method public static synthetic b(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 825
    const-string v0, "START = showMobileLink"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 827
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 829
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 830
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 836
    :cond_0
    :goto_0
    return-void

    .line 831
    :catch_0
    move-exception v0

    .line 832
    const-string v1, "ActivityNotFoundException "

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    .line 833
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static synthetic c(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->h:Landroid/widget/ListView;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 387
    new-instance v0, Lakc;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->g:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lakc;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;Ljava/util/ArrayList;)V

    .line 388
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->h:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 389
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->h:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->a:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 391
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_1

    .line 392
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->h:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/high16 v1, 0x43c10000    # 386.0f

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v3, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 399
    :goto_0
    invoke-direct {p0, v4}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->a(I)V

    .line 401
    sget-boolean v0, Laky;->a:Z

    if-eq v0, v3, :cond_0

    sget-boolean v0, Laky;->c:Z

    if-ne v0, v3, :cond_2

    :cond_0
    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 412
    :goto_1
    return-void

    .line 396
    :cond_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->h:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    .line 404
    :cond_2
    sget-boolean v0, Laky;->b:Z

    if-ne v0, v3, :cond_3

    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 406
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 407
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->e:Landroid/widget/ImageView;

    const v1, 0x7f0a04f3

    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 409
    :cond_3
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 410
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->e:Landroid/widget/ImageView;

    const v1, 0x7f0a04f2

    invoke-virtual {p0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public static synthetic d(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->b:Landroid/content/Context;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 426
    const-string v0, "START - shareScreenShot"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 428
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    if-nez v0, :cond_0

    .line 429
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0e003a

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/MapFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapFragment;->c()Lvs;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    .line 432
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    if-eqz v0, :cond_1

    .line 433
    new-instance v0, Laka;

    invoke-direct {v0, p0}, Laka;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;)V

    .line 541
    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    invoke-virtual {v1, v0}, Lvs;->a(Lwr;)V

    .line 543
    :cond_1
    return-void
.end method

.method public static synthetic e(Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->e:Landroid/widget/ImageView;

    return-object v0
.end method

.method private e()Ljava/lang/String;
    .locals 6

    .prologue
    .line 561
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getGoogleMapURL(), myLocation = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->k:Lamf;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 563
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->k:Lamf;

    if-nez v0, :cond_0

    .line 564
    const/4 v0, 0x0

    .line 574
    :goto_0
    return-object v0

    .line 566
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->k:Lamf;

    iget-wide v0, v0, Lamf;->a:D

    .line 567
    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->k:Lamf;

    iget-wide v2, v2, Lamf;->b:D

    .line 569
    sget-boolean v4, Laky;->c:Z

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 570
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "http://mo.amap.com/?q="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 572
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "http://maps.google.com/maps?f=q&q=("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")&z=15"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 892
    const-string v0, "setDbUpdateError : there is no history record in this UID"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 894
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->h:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 895
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->h:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 898
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 899
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 901
    :cond_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 902
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 904
    :cond_2
    return-void
.end method


# virtual methods
.method public a(Laah;)Z
    .locals 1

    .prologue
    .line 927
    const/4 v0, 0x1

    return v0
.end method

.method public createTabContent(Ljava/lang/String;)Landroid/view/View;
    .locals 1

    .prologue
    .line 908
    const-string v0, "Ongoing"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 909
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->a(Z)V

    .line 913
    :goto_0
    const v0, 0x7f0e0048

    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    return-object v0

    .line 911
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->a(Z)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 204
    const-string v0, "onBackPressed"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 207
    const-string v0, "onBackPressed not null"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 208
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->f()V

    .line 214
    :goto_0
    return-void

    .line 210
    :cond_0
    const-string v0, "onBackPressed null"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 211
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 283
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 285
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 286
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->f:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 287
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 283
    nop

    :pswitch_data_0
    .packed-switch 0x7f0e003b
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x400

    const/4 v7, 0x0

    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    .line 121
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 122
    const-string v0, "onCreate"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 123
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 125
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Ljava/util/Locale;->setDefault(Ljava/util/Locale;)V

    .line 127
    iput-object p0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->b:Landroid/content/Context;

    .line 128
    iput-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->k:Lamf;

    .line 130
    sget-boolean v0, Laky;->a:Z

    if-ne v0, v6, :cond_2

    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 131
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->f:Ljava/lang/String;

    .line 143
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 145
    const-string v1, "Latitude"

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 146
    const-string v2, "Logitude"

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 148
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_0

    .line 149
    new-instance v2, Lamf;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-direct {v2, v4, v5, v0, v1}, Lamf;-><init>(DD)V

    iput-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->k:Lamf;

    .line 152
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->n:Landroid/app/ActionBar;

    .line 153
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->n:Landroid/app/ActionBar;

    if-eqz v0, :cond_1

    .line 154
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->n:Landroid/app/ActionBar;

    const v1, 0x7f0a0099

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->n:Landroid/app/ActionBar;

    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 157
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->n:Landroid/app/ActionBar;

    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 158
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->n:Landroid/app/ActionBar;

    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 162
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->a()V

    .line 164
    return-void

    .line 132
    :cond_2
    sget-boolean v0, Laky;->b:Z

    if-ne v0, v6, :cond_3

    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00ae

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 135
    :cond_3
    sget-boolean v0, Laky;->c:Z

    if-ne v0, v6, :cond_4

    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 137
    iput-object v2, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 139
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->f:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 218
    const-string v0, "onCreateOptionsMenu"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0d0001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 222
    sget-boolean v0, Laky;->b:Z

    if-nez v0, :cond_0

    .line 223
    const v0, 0x7f0e0119

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 226
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->o:Z

    if-ne v0, v2, :cond_1

    .line 227
    const v0, 0x7f0e0118

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 230
    :cond_1
    return v2
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 236
    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->k:Lamf;

    .line 237
    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->l:Lamf;

    .line 239
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 244
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 247
    :cond_1
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    if-eqz v0, :cond_2

    .line 248
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    invoke-virtual {v0}, Lvs;->f()V

    .line 249
    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->j:Lvs;

    .line 252
    :cond_2
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 253
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 256
    :cond_3
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 257
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    :cond_4
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 261
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 263
    :cond_5
    iput-object v1, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->d:Landroid/app/ProgressDialog;

    .line 265
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->h:Landroid/widget/ListView;

    if-eqz v0, :cond_6

    .line 266
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->h:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 267
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->h:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 270
    :cond_6
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 271
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 275
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 276
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->finish()V

    .line 277
    const/4 v0, 0x1

    .line 279
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 298
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 316
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 301
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->finish()V

    goto :goto_0

    .line 304
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->d()V

    goto :goto_0

    .line 308
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->b:Landroid/content/Context;

    invoke-static {v0}, Lall;->D(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 309
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 298
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0e0118 -> :sswitch_1
        0x7f0e0119 -> :sswitch_2
    .end sparse-switch
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 918
    const-string v0, "Ongoing"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 919
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->a(Z)V

    .line 923
    :goto_0
    return-void

    .line 921
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/GeoLookout/activity/DisasterReportActivity;->a(Z)V

    goto :goto_0
.end method
