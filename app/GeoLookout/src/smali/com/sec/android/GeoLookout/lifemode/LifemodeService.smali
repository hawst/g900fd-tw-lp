.class public Lcom/sec/android/GeoLookout/lifemode/LifemodeService;
.super Landroid/app/Service;
.source "LifemodeService.java"


# instance fields
.field a:Lany;

.field private b:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 31
    new-instance v0, Lamn;

    invoke-direct {v0, p0}, Lamn;-><init>(Lcom/sec/android/GeoLookout/lifemode/LifemodeService;)V

    iput-object v0, p0, Lcom/sec/android/GeoLookout/lifemode/LifemodeService;->a:Lany;

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 44
    const-string v0, "handleRequestLifeInformation"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 46
    invoke-static {p0}, Lall;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 47
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 49
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    invoke-virtual {p0, v0}, Lcom/sec/android/GeoLookout/lifemode/LifemodeService;->sendBroadcast(Landroid/content/Intent;)V

    .line 54
    :goto_0
    return-void

    .line 52
    :cond_0
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/GeoLookout/lifemode/LifemodeService;->a:Lany;

    invoke-virtual {v1, p0, v0, v2}, Lalu;->a(Landroid/content/Context;Ljava/lang/String;Lany;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 25
    const-class v1, Lcom/sec/android/GeoLookout/lifemode/LifemodeService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 26
    const-string v1, "com.sec.android.GeoLookout.ACTION_LIFEMODE_AUTO_DATASYNC"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 27
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 28
    return-void
.end method

.method public static synthetic a(Lcom/sec/android/GeoLookout/lifemode/LifemodeService;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/android/GeoLookout/lifemode/LifemodeService;->a()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 90
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 91
    invoke-static {p0}, Lalj;->a(Landroid/content/Context;)V

    .line 92
    const-string v0, "onCreate"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 98
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/GeoLookout/lifemode/LifemodeService;->b:Ljava/util/concurrent/ExecutorService;

    .line 99
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 104
    const-string v0, "onDestroyed"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/GeoLookout/lifemode/LifemodeService;->b:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 107
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onStartCommand intent "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 60
    if-nez p1, :cond_0

    .line 85
    :goto_0
    return v2

    .line 64
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 66
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 68
    const-string v1, "com.sec.android.GeoLookout.ACTION_START_LIFEMODE_SYNC_SCHEDULER"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/lifemode/LifemodeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lall;->t(Landroid/content/Context;)V

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/lifemode/LifemodeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lall;->s(Landroid/content/Context;)V

    .line 83
    :cond_1
    :goto_1
    invoke-virtual {p0, p3}, Lcom/sec/android/GeoLookout/lifemode/LifemodeService;->stopSelf(I)V

    goto :goto_0

    .line 71
    :cond_2
    const-string v1, "com.sec.android.GeoLookout.ACTION_CANCEL_LIFEMODE_SYNC_SCHEDULER"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 72
    invoke-virtual {p0}, Lcom/sec/android/GeoLookout/lifemode/LifemodeService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lall;->t(Landroid/content/Context;)V

    goto :goto_1

    .line 73
    :cond_3
    const-string v1, "com.sec.android.GeoLookout.ACTION_LIFEMODE_AUTO_DATASYNC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/sec/android/GeoLookout/lifemode/LifemodeService;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lamo;

    invoke-direct {v1, p0}, Lamo;-><init>(Lcom/sec/android/GeoLookout/lifemode/LifemodeService;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_1
.end method
