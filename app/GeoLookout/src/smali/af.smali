.class Laf;
.super Ljava/lang/Object;

# interfaces
.implements Laj;


# instance fields
.field final synthetic a:Lad;

.field private final b:Lad;

.field private final c:Lt;


# direct methods
.method public constructor <init>(Lad;Lad;Lt;)V
    .locals 0

    iput-object p1, p0, Laf;->a:Lad;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Laf;->b:Lad;

    iput-object p3, p0, Laf;->c:Lt;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    const-string v0, "Custom event adapter called onFailedToReceiveAd."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    iget-object v0, p0, Laf;->c:Lt;

    iget-object v1, p0, Laf;->b:Lad;

    sget-object v2, Lk;->b:Lk;

    invoke-interface {v0, v1, v2}, Lt;->a(Ls;Lk;)V

    return-void
.end method

.method public b()V
    .locals 2

    const-string v0, "Custom event adapter called onReceivedAd."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    iget-object v0, p0, Laf;->c:Lt;

    iget-object v1, p0, Laf;->a:Lad;

    invoke-interface {v0, v1}, Lt;->a(Ls;)V

    return-void
.end method

.method public c()V
    .locals 2

    const-string v0, "Custom event adapter called onPresentScreen."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    iget-object v0, p0, Laf;->c:Lt;

    iget-object v1, p0, Laf;->b:Lad;

    invoke-interface {v0, v1}, Lt;->b(Ls;)V

    return-void
.end method

.method public d()V
    .locals 2

    const-string v0, "Custom event adapter called onDismissScreen."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    iget-object v0, p0, Laf;->c:Lt;

    iget-object v1, p0, Laf;->b:Lad;

    invoke-interface {v0, v1}, Lt;->c(Ls;)V

    return-void
.end method

.method public e()V
    .locals 2

    const-string v0, "Custom event adapter called onLeaveApplication."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    iget-object v0, p0, Laf;->c:Lt;

    iget-object v1, p0, Laf;->b:Lad;

    invoke-interface {v0, v1}, Lt;->d(Ls;)V

    return-void
.end method
