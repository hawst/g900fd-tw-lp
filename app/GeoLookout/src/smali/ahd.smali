.class Lahd;
.super Ljava/lang/Object;
.source "DisasterDashboardActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Laif;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:I

.field final synthetic d:Lahc;


# direct methods
.method constructor <init>(Lahc;Laif;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2516
    iput-object p1, p0, Lahd;->d:Lahc;

    iput-object p2, p0, Lahd;->a:Laif;

    iput-object p3, p0, Lahd;->b:Ljava/lang/String;

    iput p4, p0, Lahd;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 2518
    iget-object v0, p0, Lahd;->a:Laif;

    iget-object v1, p0, Lahd;->d:Lahc;

    iget-object v1, v1, Lahc;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iget-object v2, p0, Lahd;->b:Ljava/lang/String;

    iget-object v3, p0, Lahd;->a:Laif;

    iget v3, v3, Laif;->m:I

    int-to-long v4, v3

    invoke-static {v1, v2, v4, v5}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Ljava/lang/String;J)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v1

    iput-object v1, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    .line 2519
    iget-object v0, p0, Lahd;->a:Laif;

    iget-object v0, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    if-eqz v0, :cond_0

    .line 2520
    iget-object v0, p0, Lahd;->a:Laif;

    iget-object v1, p0, Lahd;->a:Laif;

    iget-object v1, v1, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v1

    iput v1, v0, Laif;->n:I

    .line 2521
    iget-object v0, p0, Lahd;->d:Lahc;

    iget-object v0, v0, Lahc;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iget-object v1, p0, Lahd;->a:Laif;

    iget-object v1, v1, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationType()I

    move-result v1

    iget v2, p0, Lahd;->c:I

    invoke-static {v0, v1, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;II)V

    .line 2522
    iget-object v0, p0, Lahd;->d:Lahc;

    iget-object v0, v0, Lahc;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iget v1, p0, Lahd;->c:I

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->d(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)V

    .line 2523
    iget-object v0, p0, Lahd;->d:Lahc;

    iget-object v0, v0, Lahc;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iget v1, p0, Lahd;->c:I

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->e(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)V

    .line 2528
    :goto_0
    return-void

    .line 2526
    :cond_0
    const-string v0, "mLastDisaster is null!!! - update"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
