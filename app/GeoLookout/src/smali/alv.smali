.class public Lalv;
.super Ljava/lang/Object;
.source "CurrentsDbTable.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# static fields
.field public static final A:Ljava/lang/String; = "CHN_DETAILSCITY"

.field public static final B:Ljava/lang/String; = "CHN_DETAILSSTATIONNAME"

.field public static final C:Ljava/lang/String; = "CHN_DETAILSEQTYPE"

.field public static final D:Ljava/lang/String; = "CHN_SIGNIFICANCELEVEL"

.field public static final E:Ljava/lang/String; = "JPN_DETAILSDEPTH"

.field public static final F:Ljava/lang/String; = "JPN_DETAILSLEVEL"

.field public static final G:Ljava/lang/String; = "JPN_DETAILSZONECOUNTRY"

.field public static final H:Ljava/lang/String; = "JPN_DETAILSZONECITY"

.field public static final I:Ljava/lang/String; = "MESSAGE_ID"

.field public static final J:Ljava/lang/String; = "DISASTER_SOURCE"

.field public static final K:Ljava/lang/String; = "MULTI_POINT_ID"

.field public static final L:I = 0x0

.field public static final M:I = 0x1

.field public static final N:I = 0x2

.field public static final O:I = 0x3

.field public static final P:I = 0x4

.field public static final Q:I = 0x5

.field public static final R:I = 0x6

.field public static final S:I = 0x7

.field public static final T:I = 0x8

.field public static final U:I = 0x9

.field public static final V:I = 0xa

.field public static final W:I = 0xb

.field public static final X:I = 0xc

.field public static final Y:I = 0xd

.field public static final Z:I = 0xe

.field public static final a:Ljava/lang/String; = "STIME"

.field protected static aA:Ljava/lang/String; = null

.field protected static aB:Ljava/lang/String; = null

.field protected static aC:Ljava/lang/String; = null

.field protected static aD:Ljava/lang/String; = null

.field protected static aE:Ljava/lang/String; = null

.field protected static aF:[Ljava/lang/String; = null

.field public static aG:Ljava/lang/String; = null

.field public static aH:Ljava/lang/String; = null

.field public static final aa:I = 0xf

.field public static final ab:I = 0x10

.field public static final ac:I = 0x11

.field public static final ad:I = 0x12

.field public static final ae:I = 0x13

.field public static final af:I = 0x14

.field public static final ag:I = 0x15

.field public static final ah:I = 0x16

.field public static final ai:I = 0x17

.field public static final aj:I = 0x18

.field public static final ak:I = 0x19

.field public static final al:I = 0x1a

.field public static final am:I = 0x1b

.field public static final an:I = 0x1c

.field public static final ao:I = 0x1d

.field public static final ap:I = 0x1e

.field public static final aq:I = 0x1f

.field public static final ar:I = 0x20

.field public static final as:I = 0x21

.field public static final at:I = 0x22

.field public static final au:I = 0x23

.field public static final av:I = 0x24

.field public static final aw:I = 0x25

.field protected static ax:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static ay:Ljava/lang/String; = null

.field protected static az:Ljava/lang/String; = null

.field public static final b:Ljava/lang/String; = "ETIME"

.field public static final c:Ljava/lang/String; = "RTIME"

.field public static final d:Ljava/lang/String; = "DID"

.field public static final e:Ljava/lang/String; = "DTYPE"

.field public static final f:Ljava/lang/String; = "DTYPE_ID"

.field public static final g:Ljava/lang/String; = "DV"

.field public static final h:Ljava/lang/String; = "LINK"

.field public static final i:Ljava/lang/String; = "HEADLINE"

.field public static final j:Ljava/lang/String; = "LOCATIONTYPE"

.field public static final k:Ljava/lang/String; = "LOCATIONID"

.field public static final l:Ljava/lang/String; = "POLYGONJSON"

.field public static final m:Ljava/lang/String; = "CITY"

.field public static final n:Ljava/lang/String; = "COUNTRY"

.field public static final o:Ljava/lang/String; = "LATITUDE"

.field public static final p:Ljava/lang/String; = "LONGITUDE"

.field public static final q:Ljava/lang/String; = "DETAILSINFO"

.field public static final r:Ljava/lang/String; = "SIGNIFICANCE"

.field public static final s:Ljava/lang/String; = "DISTANCE"

.field public static final t:Ljava/lang/String; = "POLLING"

.field public static final u:Ljava/lang/String; = "ACTION_STATUS"

.field public static final v:Ljava/lang/String; = "NOTISTATUS"

.field public static final w:Ljava/lang/String; = "DETAILSPEED"

.field public static final x:Ljava/lang/String; = "created"

.field public static final y:Ljava/lang/String; = "modified"

.field public static final z:Ljava/lang/String; = "CHN_DETAILSPROVINCE"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 115
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lalv;->ax:Ljava/util/HashMap;

    .line 116
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "STIME"

    const-string v2, "STIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "ETIME"

    const-string v2, "ETIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "RTIME"

    const-string v2, "RTIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "DID"

    const-string v2, "DID"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "DTYPE"

    const-string v2, "DTYPE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "DTYPE_ID"

    const-string v2, "DTYPE_ID"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "DV"

    const-string v2, "DV"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "LINK"

    const-string v2, "LINK"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "HEADLINE"

    const-string v2, "HEADLINE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "LOCATIONID"

    const-string v2, "LOCATIONID"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "POLYGONJSON"

    const-string v2, "POLYGONJSON"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "CITY"

    const-string v2, "CITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "COUNTRY"

    const-string v2, "COUNTRY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "LATITUDE"

    const-string v2, "LATITUDE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "LONGITUDE"

    const-string v2, "LONGITUDE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "DETAILSINFO"

    const-string v2, "DETAILSINFO"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "SIGNIFICANCE"

    const-string v2, "SIGNIFICANCE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "LOCATIONTYPE"

    const-string v2, "LOCATIONTYPE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "DISTANCE"

    const-string v2, "DISTANCE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "POLLING"

    const-string v2, "POLLING"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "ACTION_STATUS"

    const-string v2, "ACTION_STATUS"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "NOTISTATUS"

    const-string v2, "NOTISTATUS"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "created"

    const-string v2, "created"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "modified"

    const-string v2, "modified"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "DETAILSPEED"

    const-string v2, "DETAILSPEED"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "CHN_DETAILSPROVINCE"

    const-string v2, "CHN_DETAILSPROVINCE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "CHN_DETAILSCITY"

    const-string v2, "CHN_DETAILSCITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "CHN_DETAILSSTATIONNAME"

    const-string v2, "CHN_DETAILSSTATIONNAME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "CHN_DETAILSEQTYPE"

    const-string v2, "CHN_DETAILSEQTYPE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "CHN_SIGNIFICANCELEVEL"

    const-string v2, "CHN_SIGNIFICANCELEVEL"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "JPN_DETAILSDEPTH"

    const-string v2, "JPN_DETAILSDEPTH"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "JPN_DETAILSLEVEL"

    const-string v2, "JPN_DETAILSLEVEL"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "JPN_DETAILSZONECOUNTRY"

    const-string v2, "JPN_DETAILSZONECOUNTRY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "JPN_DETAILSZONECITY"

    const-string v2, "JPN_DETAILSZONECITY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "MESSAGE_ID"

    const-string v2, "MESSAGE_ID"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "DISASTER_SOURCE"

    const-string v2, "DISASTER_SOURCE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    const-string v1, "MULTI_POINT_ID"

    const-string v2, "MULTI_POINT_ID"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    const-string v0, ","

    sput-object v0, Lalv;->aG:Ljava/lang/String;

    .line 159
    const-string v0, ");"

    sput-object v0, Lalv;->aH:Ljava/lang/String;

    .line 161
    const-string v0, "CREATE TABLE CURRENT_DISASTER_TBL (_id INTEGER PRIMARY KEY,STIME LONG,ETIME LONG,RTIME LONG,DID TEXT,DTYPE TEXT,DTYPE_ID INTEGER,DV TEXT DEFAULT \'01\',LINK TEXT,HEADLINE TEXT,LOCATIONID TEXT,POLYGONJSON TEXT,CITY TEXT,COUNTRY TEXT,LATITUDE DOUBLE,LONGITUDE DOUBLE,DETAILSINFO DOUBLE,SIGNIFICANCE INTEGER,LOCATIONTYPE INTEGER,DISTANCE INTEGER,POLLING SHORT,ACTION_STATUS SHORT,NOTISTATUS SHORT,DETAILSPEED TEXT,created INTEGER,modified INTEGER,CHN_DETAILSPROVINCE TEXT,CHN_DETAILSCITY TEXT,CHN_DETAILSSTATIONNAME TEXT,CHN_DETAILSEQTYPE TEXT,CHN_SIGNIFICANCELEVEL INTEGER,JPN_DETAILSDEPTH TEXT,JPN_DETAILSLEVEL TEXT,JPN_DETAILSZONECOUNTRY TEXT,JPN_DETAILSZONECITY TEXT,MESSAGE_ID TEXT,DISASTER_SOURCE TEXT,MULTI_POINT_ID LONG"

    sput-object v0, Lalv;->ay:Ljava/lang/String;

    .line 205
    const-string v0, "CREATE TABLE CURRENT_DISASTER_TBL (_id INTEGER PRIMARY KEY,STIME LONG,ETIME LONG,RTIME LONG,DID TEXT,DTYPE TEXT,DTYPE_ID INTEGER,DV TEXT DEFAULT \'01\',LINK TEXT,HEADLINE TEXT,LOCATIONID TEXT,POLYGONJSON TEXT,CITY TEXT,COUNTRY TEXT,LATITUDE DOUBLE,LONGITUDE DOUBLE,DETAILSINFO DOUBLE,SIGNIFICANCE INTEGER,LOCATIONTYPE INTEGER,DISTANCE INTEGER,POLLING SHORT,ACTION_STATUS SHORT,NOTISTATUS SHORT,DETAILSPEED TEXT,created INTEGER,modified INTEGER,CHN_DETAILSPROVINCE TEXT,CHN_DETAILSCITY TEXT,CHN_DETAILSSTATIONNAME TEXT,CHN_DETAILSEQTYPE TEXT,CHN_SIGNIFICANCELEVEL INTEGER,JPN_DETAILSDEPTH TEXT,JPN_DETAILSLEVEL TEXT,JPN_DETAILSZONECOUNTRY TEXT,JPN_DETAILSZONECITY TEXT"

    sput-object v0, Lalv;->az:Ljava/lang/String;

    .line 244
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lalv;->az:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MULTI_POINT_ID"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LONG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lalv;->aD:Ljava/lang/String;

    .line 247
    const-string v0, "_id,STIME,ETIME,RTIME,DID,DTYPE,DTYPE_ID,DV,LINK,HEADLINE,LOCATIONID,POLYGONJSON,CITY,COUNTRY,LATITUDE,LONGITUDE,DETAILSINFO,SIGNIFICANCE,LOCATIONTYPE,DISTANCE,POLLING,ACTION_STATUS,NOTISTATUS,DETAILSPEED,created,modified"

    sput-object v0, Lalv;->aA:Ljava/lang/String;

    .line 274
    const-string v0, "CHN_DETAILSPROVINCE,CHN_DETAILSCITY,CHN_DETAILSSTATIONNAME,CHN_DETAILSEQTYPE,CHN_SIGNIFICANCELEVEL"

    sput-object v0, Lalv;->aB:Ljava/lang/String;

    .line 280
    const-string v0, "JPN_DETAILSDEPTH,JPN_DETAILSLEVEL,JPN_DETAILSZONECOUNTRY,JPN_DETAILSZONECITY"

    sput-object v0, Lalv;->aC:Ljava/lang/String;

    .line 285
    const/16 v0, 0x26

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "STIME"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "ETIME"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "RTIME"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "DID"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "DTYPE"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "DTYPE_ID"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "DV"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "LINK"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "HEADLINE"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "LOCATIONTYPE"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "LOCATIONID"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "POLYGONJSON"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "CITY"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "COUNTRY"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "LATITUDE"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "LONGITUDE"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "DETAILSINFO"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "SIGNIFICANCE"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "DISTANCE"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "POLLING"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "ACTION_STATUS"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "NOTISTATUS"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "DETAILSPEED"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "created"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "modified"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "CHN_DETAILSPROVINCE"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "CHN_DETAILSCITY"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "CHN_DETAILSSTATIONNAME"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "CHN_DETAILSEQTYPE"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "CHN_SIGNIFICANCELEVEL"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "JPN_DETAILSDEPTH"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "JPN_DETAILSLEVEL"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "JPN_DETAILSZONECOUNTRY"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "JPN_DETAILSZONECITY"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "MESSAGE_ID"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "DISASTER_SOURCE"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "MULTI_POINT_ID"

    aput-object v2, v0, v1

    sput-object v0, Lalv;->aF:[Ljava/lang/String;

    .line 329
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 433
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 434
    const-string v1, "STIME"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getStartTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 435
    const-string v1, "ETIME"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getEndTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 436
    const-string v1, "RTIME"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getReceivedTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 437
    const-string v1, "DID"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const-string v1, "DTYPE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    const-string v1, "DTYPE_ID"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 440
    const-string v1, "DV"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    const-string v1, "LINK"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLink()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    const-string v1, "HEADLINE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getHeadline()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    const-string v1, "LOCATIONTYPE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 444
    const-string v1, "LOCATIONID"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocationID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    const-string v1, "POLYGONJSON"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getPolygonJSON()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    const-string v1, "CITY"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getCity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    const-string v1, "COUNTRY"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    const-string v1, "LATITUDE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v2

    iget-wide v2, v2, Lamf;->a:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 449
    const-string v1, "LONGITUDE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v2

    iget-wide v2, v2, Lamf;->b:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 450
    const-string v1, "DETAILSINFO"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailsMagnitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 451
    const-string v1, "SIGNIFICANCE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 452
    const-string v1, "DISTANCE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDistance()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 453
    const-string v1, "POLLING"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getPolling()S

    move-result v2

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    .line 454
    const-string v1, "ACTION_STATUS"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getActionStatus()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 455
    const-string v1, "NOTISTATUS"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getNotiStatus()S

    move-result v2

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    .line 456
    const-string v1, "DETAILSPEED"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailSpeed()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    const-string v1, "CHN_DETAILSPROVINCE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getChnDetailsProvince()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    const-string v1, "CHN_DETAILSCITY"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getChnDetailsCity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    const-string v1, "CHN_DETAILSSTATIONNAME"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getChnDetailsStationName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    const-string v1, "CHN_DETAILSEQTYPE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getChnDetailsEqType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    const-string v1, "CHN_SIGNIFICANCELEVEL"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getChnSignificanceLevel()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 464
    const-string v1, "JPN_DETAILSDEPTH"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsDepth()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    const-string v1, "JPN_DETAILSLEVEL"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsLevel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    const-string v1, "JPN_DETAILSZONECOUNTRY"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsZoneCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    const-string v1, "JPN_DETAILSZONECITY"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsZoneCity()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    const-string v1, "MESSAGE_ID"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMsgId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    const-string v1, "DISASTER_SOURCE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDisasterSource()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    const-string v1, "MULTI_POINT_ID"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 474
    return-object v0
.end method

.method protected a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 8

    .prologue
    const/16 v4, 0xc

    .line 383
    new-instance v0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    invoke-direct {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;-><init>()V

    .line 384
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setId(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setStartTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setEndTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setReceivedTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDid(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x5

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setType(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x6

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setTypeId(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x7

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDV(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x8

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLink(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x9

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setHeadline(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0xa

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocationType(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0xb

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocationID(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setPolygonJSON(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/GeoLookout/db/DisasterParser;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setPolygonList(Ljava/util/ArrayList;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0xd

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setCity(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0xe

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setCountry(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    new-instance v2, Lamf;

    const/16 v3, 0xf

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    const/16 v3, 0x10

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lamf;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocation(Lamf;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x11

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailsMagnitude(D)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x12

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setSignificance(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x13

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDistance(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x14

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getShort(I)S

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setPolling(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x15

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getShort(I)S

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setActionStatus(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x16

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getShort(I)S

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setNotiStatus(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x17

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailSpeed(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x1a

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setChnDetailsProvince(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x1b

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setChnDetailsCity(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x1c

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setChnDetailsStationName(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x1d

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setChnDetailsEqType(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x1e

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setChnSignificanceLevel(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x1f

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsDepth(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x20

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsLevel(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x21

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsZoneCountry(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x22

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setJpnDetailsZoneCity(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x23

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setMsgId(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x24

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDisasterSource(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x25

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setMultiPointUid(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 424
    return-object v0
.end method

.method public a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 332
    sget-object v0, Lalv;->aF:[Ljava/lang/String;

    return-object v0
.end method

.method public b(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 1

    .prologue
    .line 428
    invoke-virtual {p0, p1}, Lalv;->a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v0

    .line 429
    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->build()Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 336
    sget-object v0, Lalv;->ax:Ljava/util/HashMap;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 340
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lalv;->ay:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lalv;->aH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 341
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCreateDbTableString : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 342
    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 346
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lalv;->az:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lalv;->aH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 347
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCreateDbTableString : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 348
    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 3

    .prologue
    .line 352
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lalv;->aD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lalv;->aH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 353
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCreateDbTableString : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 354
    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 358
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lalv;->aE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lalv;->aH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 359
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCreateDbTableString : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 360
    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 3

    .prologue
    .line 364
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lalv;->aA:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lalv;->aB:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lalv;->aC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 367
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getUpgradeDbTableStringForV17 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 368
    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 3

    .prologue
    .line 372
    sget-object v0, Lalv;->aA:Ljava/lang/String;

    .line 373
    sget-boolean v1, Laky;->b:Z

    if-eqz v1, :cond_1

    .line 374
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lalv;->aC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 378
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getUpgradeDbTableStringForV19 : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 379
    return-object v0

    .line 375
    :cond_1
    sget-boolean v1, Laky;->c:Z

    if-eqz v1, :cond_0

    .line 376
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lalv;->aB:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
