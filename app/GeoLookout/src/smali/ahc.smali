.class public Lahc;
.super Landroid/content/BroadcastReceiver;
.source "DisasterDashboardActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V
    .locals 0

    .prologue
    .line 2488
    iput-object p1, p0, Lahc;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2490
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 2491
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onReceive action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 2494
    iget-object v0, p0, Lahc;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lalz;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2495
    iget-object v0, p0, Lahc;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->k(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/widget/TabHost;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v0

    move v1, v0

    .line 2500
    :goto_0
    iget-object v0, p0, Lahc;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->h(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laif;

    .line 2501
    const-string v5, "com.sec.android.GeoLookout.ACTION_UPDATE_PUSH_MESSAGE"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2502
    const-string v4, "canceled"

    invoke-virtual {p2, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 2503
    const-string v5, "DID"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2507
    const-string v6, "uid"

    invoke-virtual {p2, v6, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 2508
    if-eqz v0, :cond_0

    iget-object v7, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    if-eqz v7, :cond_0

    iget-object v7, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v7}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v7

    if-eq v7, v6, :cond_4

    .line 2514
    :cond_0
    :goto_1
    if-eqz v5, :cond_1

    if-eqz v0, :cond_1

    if-ne v2, v3, :cond_1

    iget-object v2, v0, Laif;->f:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2515
    if-nez v4, :cond_3

    .line 2516
    iget-object v2, p0, Lahc;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    new-instance v3, Lahd;

    invoke-direct {v3, p0, v0, v5, v1}, Lahd;-><init>(Lahc;Laif;Ljava/lang/String;I)V

    invoke-virtual {v2, v3}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2535
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v1, v2

    .line 2497
    goto :goto_0

    .line 2531
    :cond_3
    iget-object v0, p0, Lahc;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->q(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    goto :goto_2

    :cond_4
    move v2, v3

    goto :goto_1
.end method
