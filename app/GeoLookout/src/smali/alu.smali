.class public Lalu;
.super Ljava/lang/Object;
.source "DisasterRunner.java"


# static fields
.field private static volatile a:Lalu;


# instance fields
.field private b:Lalo;

.field private c:Ljava/util/concurrent/ExecutorService;

.field private d:Ljava/util/concurrent/ExecutorService;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    return-void
.end method

.method public static a()Lalu;
    .locals 2

    .prologue
    .line 44
    sget-object v0, Lalu;->a:Lalu;

    if-nez v0, :cond_1

    .line 45
    const-class v1, Lalu;

    monitor-enter v1

    .line 46
    :try_start_0
    sget-object v0, Lalu;->a:Lalu;

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lalu;

    invoke-direct {v0}, Lalu;-><init>()V

    sput-object v0, Lalu;->a:Lalu;

    .line 49
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    :cond_1
    sget-object v0, Lalu;->a:Lalu;

    return-object v0

    .line 49
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private f()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lalu;->c:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lalu;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    :cond_0
    invoke-static {}, Lalm;->b()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lalu;->c:Ljava/util/concurrent/ExecutorService;

    .line 81
    :cond_1
    iget-object v0, p0, Lalu;->d:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lalu;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 82
    :cond_2
    invoke-static {}, Lalm;->a()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lalu;->d:Ljava/util/concurrent/ExecutorService;

    .line 84
    :cond_3
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Lalo;)V
    .locals 2

    .prologue
    .line 58
    monitor-enter p0

    if-nez p1, :cond_0

    .line 59
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "TranscloudRunner can not be initialize. config is null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 61
    :cond_0
    :try_start_1
    iget-object v0, p0, Lalu;->b:Lalo;

    if-nez v0, :cond_1

    .line 62
    iput-object p1, p0, Lalu;->b:Lalo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 66
    monitor-exit p0

    return-void

    .line 64
    :cond_1
    :try_start_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "TranscloudRunner had already initialized! try destroy() call!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 161
    new-instance v0, Laoz;

    iget-object v1, p0, Lalu;->b:Lalo;

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Laoz;-><init>(Landroid/content/Context;Lalo;Lalr;)V

    invoke-virtual {p0, v0}, Lalu;->a(Lapb;)V

    .line 162
    return-void
.end method

.method public a(Landroid/content/Context;DD)V
    .locals 8

    .prologue
    .line 190
    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-virtual/range {v0 .. v6}, Lalu;->a(Landroid/content/Context;DDZ)V

    .line 191
    return-void
.end method

.method public a(Landroid/content/Context;DDZ)V
    .locals 4

    .prologue
    .line 195
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "queryLocationToDisasterServer "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 196
    new-instance v0, Lalt;

    invoke-direct {v0, p1}, Lalt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2, p3}, Lalt;->b(D)Lalt;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Lalt;->a(D)Lalt;

    move-result-object v0

    invoke-virtual {v0, p6}, Lalt;->b(Z)Lalt;

    move-result-object v0

    invoke-virtual {v0}, Lalt;->a()Lalr;

    move-result-object v0

    .line 198
    new-instance v1, Lapg;

    iget-object v2, p0, Lalu;->b:Lalo;

    invoke-direct {v1, v2, v0}, Lapg;-><init>(Lalo;Lalr;)V

    invoke-virtual {p0, v1}, Lalu;->a(Lapb;)V

    .line 199
    return-void
.end method

.method public a(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 180
    new-instance v0, Lalt;

    invoke-direct {v0, p1}, Lalt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lalt;->f(I)Lalt;

    move-result-object v0

    invoke-virtual {v0}, Lalt;->a()Lalr;

    move-result-object v0

    .line 181
    new-instance v1, Lapl;

    iget-object v2, p0, Lalu;->b:Lalo;

    invoke-direct {v1, p1, v2, v0}, Lapl;-><init>(Landroid/content/Context;Lalo;Lalr;)V

    invoke-virtual {p0, v1}, Lalu;->a(Lapb;)V

    .line 182
    return-void
.end method

.method public a(Landroid/content/Context;II)V
    .locals 3

    .prologue
    .line 244
    new-instance v0, Lalt;

    invoke-direct {v0, p1}, Lalt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lalt;->c(I)Lalt;

    move-result-object v0

    invoke-virtual {v0, p3}, Lalt;->d(I)Lalt;

    move-result-object v0

    invoke-virtual {v0}, Lalt;->a()Lalr;

    move-result-object v0

    .line 245
    new-instance v1, Lapc;

    iget-object v2, p0, Lalu;->b:Lalo;

    invoke-direct {v1, p1, v2, v0}, Lapc;-><init>(Landroid/content/Context;Lalo;Lalr;)V

    invoke-virtual {p0, v1}, Lalu;->a(Lapb;)V

    .line 246
    return-void
.end method

.method public a(Landroid/content/Context;Lanw;I)V
    .locals 3

    .prologue
    .line 239
    new-instance v0, Lalt;

    invoke-direct {v0, p1}, Lalt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lalt;->a(Lanw;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p3}, Lalt;->e(I)Lalt;

    move-result-object v0

    invoke-virtual {v0}, Lalt;->a()Lalr;

    move-result-object v0

    .line 240
    new-instance v1, Lapj;

    iget-object v2, p0, Lalu;->b:Lalo;

    invoke-direct {v1, p1, v2, v0}, Lapj;-><init>(Landroid/content/Context;Lalo;Lalr;)V

    invoke-virtual {p0, v1}, Lalu;->a(Lapb;)V

    .line 241
    return-void
.end method

.method public a(Landroid/content/Context;Lanx;Landroid/location/Location;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 175
    new-instance v0, Lalt;

    invoke-direct {v0, p1}, Lalt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lalt;->a(Lanx;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p3}, Lalt;->a(Landroid/location/Location;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p4}, Lalt;->f(I)Lalt;

    move-result-object v0

    invoke-virtual {v0, p5}, Lalt;->k(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0}, Lalt;->a()Lalr;

    move-result-object v0

    .line 176
    new-instance v1, Lapk;

    iget-object v2, p0, Lalu;->b:Lalo;

    invoke-direct {v1, p1, v2, v0}, Lapk;-><init>(Landroid/content/Context;Lalo;Lalr;)V

    invoke-virtual {p0, v1}, Lalu;->a(Lapb;)V

    .line 177
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 3

    .prologue
    .line 234
    new-instance v0, Lalt;

    invoke-direct {v0, p1}, Lalt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lalt;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Lalt;

    move-result-object v0

    invoke-virtual {v0}, Lalt;->a()Lalr;

    move-result-object v0

    .line 235
    new-instance v1, Lapn;

    iget-object v2, p0, Lalu;->b:Lalo;

    invoke-direct {v1, p1, v2, v0}, Lapn;-><init>(Landroid/content/Context;Lalo;Lalr;)V

    invoke-virtual {p0, v1}, Lalu;->a(Lapb;)V

    .line 236
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 165
    new-instance v0, Lalt;

    invoke-direct {v0, p1}, Lalt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lalt;->a(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0}, Lalt;->a()Lalr;

    move-result-object v0

    .line 166
    new-instance v1, Lapi;

    iget-object v2, p0, Lalu;->b:Lalo;

    invoke-direct {v1, v2, v0}, Lapi;-><init>(Lalo;Lalr;)V

    invoke-virtual {p0, v1}, Lalu;->a(Lapb;)V

    .line 167
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 220
    new-instance v0, Lalt;

    invoke-direct {v0, p1}, Lalt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lalt;->b(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p3}, Lalt;->f(I)Lalt;

    move-result-object v0

    invoke-virtual {v0}, Lalt;->a()Lalr;

    move-result-object v0

    .line 221
    new-instance v1, Laph;

    iget-object v2, p0, Lalu;->b:Lalo;

    invoke-direct {v1, p1, v2, v0}, Laph;-><init>(Landroid/content/Context;Lalo;Lalr;)V

    invoke-virtual {p0, v1}, Lalu;->a(Lapb;)V

    .line 222
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Lanx;Landroid/location/Location;I)V
    .locals 3

    .prologue
    .line 215
    new-instance v0, Lalt;

    invoke-direct {v0, p1}, Lalt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lalt;->b(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p3}, Lalt;->a(Lanx;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p4}, Lalt;->a(Landroid/location/Location;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p5}, Lalt;->f(I)Lalt;

    move-result-object v0

    invoke-virtual {v0}, Lalt;->a()Lalr;

    move-result-object v0

    .line 216
    new-instance v1, Lapp;

    iget-object v2, p0, Lalu;->b:Lalo;

    invoke-direct {v1, p1, v2, v0}, Lapp;-><init>(Landroid/content/Context;Lalo;Lalr;)V

    invoke-virtual {p0, v1}, Lalu;->a(Lapb;)V

    .line 217
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Lany;)V
    .locals 3

    .prologue
    .line 249
    new-instance v0, Lalt;

    invoke-direct {v0, p1}, Lalt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lalt;->c(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p3}, Lalt;->a(Lany;)Lalt;

    move-result-object v0

    invoke-virtual {v0}, Lalt;->a()Lalr;

    move-result-object v0

    .line 250
    new-instance v1, Lapm;

    iget-object v2, p0, Lalu;->b:Lalo;

    invoke-direct {v1, v2, v0}, Lapm;-><init>(Lalo;Lalr;)V

    invoke-virtual {p0, v1}, Lalu;->a(Lapb;)V

    .line 251
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/location/Location;Z)V
    .locals 3

    .prologue
    .line 156
    new-instance v0, Lalt;

    invoke-direct {v0, p1}, Lalt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lalt;->b(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p3}, Lalt;->a(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p4}, Lalt;->a(Landroid/location/Location;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p5}, Lalt;->a(Z)Lalt;

    move-result-object v0

    invoke-virtual {v0}, Lalt;->a()Lalr;

    move-result-object v0

    .line 157
    new-instance v1, Lapf;

    iget-object v2, p0, Lalu;->b:Lalo;

    invoke-direct {v1, p1, v2, v0}, Lapf;-><init>(Landroid/content/Context;Lalo;Lalr;)V

    invoke-virtual {p0, v1}, Lalu;->a(Lapb;)V

    .line 158
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lany;)V
    .locals 3

    .prologue
    .line 254
    new-instance v0, Lalt;

    invoke-direct {v0, p1}, Lalt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lalt;->b(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p3}, Lalt;->c(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p4}, Lalt;->a(Lany;)Lalt;

    move-result-object v0

    invoke-virtual {v0}, Lalt;->a()Lalr;

    move-result-object v0

    .line 255
    new-instance v1, Lapq;

    iget-object v2, p0, Lalu;->b:Lalo;

    invoke-direct {v1, p1, v2, v0}, Lapq;-><init>(Landroid/content/Context;Lalo;Lalr;)V

    invoke-virtual {p0, v1}, Lalu;->a(Lapb;)V

    .line 256
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 225
    new-instance v0, Lalt;

    invoke-direct {v0, p1}, Lalt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lalt;->b(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p3}, Lalt;->h(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p4}, Lalt;->g(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p5}, Lalt;->f(I)Lalt;

    move-result-object v0

    invoke-virtual {v0}, Lalt;->a()Lalr;

    move-result-object v0

    .line 226
    new-instance v1, Laps;

    iget-object v2, p0, Lalu;->b:Lalo;

    invoke-direct {v1, p1, v2, v0}, Laps;-><init>(Landroid/content/Context;Lalo;Lalr;)V

    invoke-virtual {p0, v1}, Lalu;->a(Lapb;)V

    .line 227
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 210
    new-instance v0, Lalt;

    invoke-direct {v0, p1}, Lalt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lalt;->a(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p3}, Lalt;->c(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p4}, Lalt;->d(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p5}, Lalt;->e(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0, p6}, Lalt;->f(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0}, Lalt;->a()Lalr;

    move-result-object v0

    .line 211
    new-instance v1, Lapr;

    iget-object v2, p0, Lalu;->b:Lalo;

    invoke-direct {v1, v2, v0}, Lapr;-><init>(Lalo;Lalr;)V

    invoke-virtual {p0, v1}, Lalu;->a(Lapb;)V

    .line 212
    return-void
.end method

.method public a(Lapb;)V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lalu;->f()V

    .line 94
    iget-object v0, p0, Lalu;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 95
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lalu;->d()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lalu;->b:Lalo;

    .line 71
    return-void
.end method

.method public b(Landroid/content/Context;DDZ)V
    .locals 4

    .prologue
    .line 203
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "queryLocationToDisasterServer "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 204
    new-instance v0, Lalt;

    invoke-direct {v0, p1}, Lalt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2, p3}, Lalt;->b(D)Lalt;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Lalt;->a(D)Lalt;

    move-result-object v0

    invoke-virtual {v0, p6}, Lalt;->b(Z)Lalt;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lalt;->c(Z)Lalt;

    move-result-object v0

    invoke-virtual {v0}, Lalt;->a()Lalr;

    move-result-object v0

    .line 206
    new-instance v1, Lapg;

    iget-object v2, p0, Lalu;->b:Lalo;

    invoke-direct {v1, v2, v0}, Lapg;-><init>(Lalo;Lalr;)V

    invoke-virtual {p0, v1}, Lalu;->a(Lapb;)V

    .line 207
    return-void
.end method

.method public b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 170
    new-instance v0, Lalt;

    invoke-direct {v0, p1}, Lalt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lalt;->a(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0}, Lalt;->a()Lalr;

    move-result-object v0

    .line 171
    new-instance v1, Lapo;

    iget-object v2, p0, Lalu;->b:Lalo;

    invoke-direct {v1, v2, v0}, Lapo;-><init>(Lalo;Lalr;)V

    invoke-virtual {p0, v1}, Lalu;->a(Lapb;)V

    .line 172
    return-void
.end method

.method public b(Lapb;)V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Lalu;->f()V

    .line 105
    iget-object v0, p0, Lalu;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 106
    return-void
.end method

.method public c()V
    .locals 6

    .prologue
    .line 112
    iget-object v0, p0, Lalu;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 113
    iget-object v0, p0, Lalu;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 115
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 116
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "wait for signle task shutdown...current thread:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 117
    iget-object v2, p0, Lalu;->c:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v4, 0x2710

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v4, v5, v3}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    .line 118
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "signle task shutdown complete! time:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 120
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "wait for multi task shutdown...current thread:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 121
    iget-object v2, p0, Lalu;->d:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v4, 0x2710

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v4, v5, v3}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    .line 122
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "multi task shutdown complete! time:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v0, v4, v0

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    :goto_0
    return-void

    .line 123
    :catch_0
    move-exception v0

    .line 124
    invoke-static {v0}, Lalj;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public c(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 185
    new-instance v0, Lalt;

    invoke-direct {v0, p1}, Lalt;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lalt;->a(Ljava/lang/String;)Lalt;

    move-result-object v0

    invoke-virtual {v0}, Lalt;->a()Lalr;

    move-result-object v0

    .line 186
    new-instance v1, Lapa;

    iget-object v2, p0, Lalu;->b:Lalo;

    invoke-direct {v1, v2, v0}, Lapa;-><init>(Lalo;Lalr;)V

    invoke-virtual {p0, v1}, Lalu;->a(Lapb;)V

    .line 187
    return-void
.end method

.method public d()V
    .locals 6

    .prologue
    .line 133
    iget-object v0, p0, Lalu;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    move-result-object v0

    .line 134
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "shutdown signle task engine! reamin:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lalu;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    move-result-object v0

    .line 136
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "shutdown multi task engine! reamin:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 138
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 139
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "wait for signle shutdownNow... current thread:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 140
    iget-object v2, p0, Lalu;->c:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v4, 0x2710

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v4, v5, v3}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    .line 141
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "signle task shutdown complete! time:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 143
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "wait for multi shutdownNow... current thread:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 144
    iget-object v2, p0, Lalu;->d:Ljava/util/concurrent/ExecutorService;

    const-wide/16 v4, 0x2710

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v4, v5, v3}, Ljava/util/concurrent/ExecutorService;->awaitTermination(JLjava/util/concurrent/TimeUnit;)Z

    .line 145
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "multi task shutdown complete! time:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v0, v4, v0

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :goto_0
    return-void

    .line 146
    :catch_0
    move-exception v0

    .line 147
    invoke-static {v0}, Lalj;->e(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public e()V
    .locals 3

    .prologue
    .line 230
    new-instance v0, Laoy;

    iget-object v1, p0, Lalu;->b:Lalo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Laoy;-><init>(Lalo;Lalr;)V

    invoke-virtual {p0, v0}, Lalu;->a(Lapb;)V

    .line 231
    return-void
.end method
