.class public Lvc;
.super Ljava/lang/Object;

# interfaces
.implements Lca;


# instance fields
.field private final a:Lrj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcb;Lcc;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lrj;

    const-string v1, "activity_recognition"

    invoke-direct {v0, p1, p2, p3, v1}, Lrj;-><init>(Landroid/content/Context;Lcb;Lcc;Ljava/lang/String;)V

    iput-object v0, p0, Lvc;->a:Lrj;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lvc;->a:Lrj;

    invoke-virtual {v0}, Lrj;->a()V

    return-void
.end method

.method public a(JLandroid/app/PendingIntent;)V
    .locals 1

    iget-object v0, p0, Lvc;->a:Lrj;

    invoke-virtual {v0, p1, p2, p3}, Lrj;->a(JLandroid/app/PendingIntent;)V

    return-void
.end method

.method public a(Landroid/app/PendingIntent;)V
    .locals 1

    iget-object v0, p0, Lvc;->a:Lrj;

    invoke-virtual {v0, p1}, Lrj;->a(Landroid/app/PendingIntent;)V

    return-void
.end method

.method public a(Lcb;)V
    .locals 1

    iget-object v0, p0, Lvc;->a:Lrj;

    invoke-virtual {v0, p1}, Lrj;->a(Lcb;)V

    return-void
.end method

.method public a(Lcc;)V
    .locals 1

    iget-object v0, p0, Lvc;->a:Lrj;

    invoke-virtual {v0, p1}, Lrj;->a(Lcc;)V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lvc;->a:Lrj;

    invoke-virtual {v0}, Lrj;->b()Z

    move-result v0

    return v0
.end method

.method public b(Lcb;)Z
    .locals 1

    iget-object v0, p0, Lvc;->a:Lrj;

    invoke-virtual {v0, p1}, Lrj;->b(Lcb;)Z

    move-result v0

    return v0
.end method

.method public b(Lcc;)Z
    .locals 1

    iget-object v0, p0, Lvc;->a:Lrj;

    invoke-virtual {v0, p1}, Lrj;->b(Lcc;)Z

    move-result v0

    return v0
.end method

.method public c(Lcb;)V
    .locals 1

    iget-object v0, p0, Lvc;->a:Lrj;

    invoke-virtual {v0, p1}, Lrj;->c(Lcb;)V

    return-void
.end method

.method public c(Lcc;)V
    .locals 1

    iget-object v0, p0, Lvc;->a:Lrj;

    invoke-virtual {v0, p1}, Lrj;->c(Lcc;)V

    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lvc;->a:Lrj;

    invoke-virtual {v0}, Lrj;->c()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lvc;->a:Lrj;

    invoke-virtual {v0}, Lrj;->d()V

    return-void
.end method
