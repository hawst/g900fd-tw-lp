.class public Lakt;
.super Ljava/lang/Object;
.source "AESCrypt.java"


# static fields
.field public static final a:Ljava/lang/String; = "DisasterLog"

.field private static final b:Ljava/lang/String; = "AES"

.field private static final c:Ljava/lang/String; = "SHA1PRNG"

.field private static final d:I = 0x80


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(II)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x2

    .line 209
    new-array v3, p1, [B

    .line 210
    const/16 v1, 0x10

    new-array v4, v1, [[I

    new-array v1, v6, [I

    fill-array-data v1, :array_0

    aput-object v1, v4, v0

    new-array v1, v6, [I

    fill-array-data v1, :array_1

    aput-object v1, v4, v7

    new-array v1, v6, [I

    fill-array-data v1, :array_2

    aput-object v1, v4, v6

    new-array v1, v6, [I

    fill-array-data v1, :array_3

    aput-object v1, v4, v8

    new-array v1, v6, [I

    fill-array-data v1, :array_4

    aput-object v1, v4, v9

    const/4 v1, 0x5

    new-array v2, v6, [I

    fill-array-data v2, :array_5

    aput-object v2, v4, v1

    const/4 v1, 0x6

    new-array v2, v6, [I

    fill-array-data v2, :array_6

    aput-object v2, v4, v1

    const/4 v1, 0x7

    new-array v2, v6, [I

    fill-array-data v2, :array_7

    aput-object v2, v4, v1

    const/16 v1, 0x8

    new-array v2, v6, [I

    fill-array-data v2, :array_8

    aput-object v2, v4, v1

    const/16 v1, 0x9

    new-array v2, v6, [I

    fill-array-data v2, :array_9

    aput-object v2, v4, v1

    const/16 v1, 0xa

    new-array v2, v6, [I

    fill-array-data v2, :array_a

    aput-object v2, v4, v1

    const/16 v1, 0xb

    new-array v2, v6, [I

    fill-array-data v2, :array_b

    aput-object v2, v4, v1

    const/16 v1, 0xc

    new-array v2, v6, [I

    fill-array-data v2, :array_c

    aput-object v2, v4, v1

    const/16 v1, 0xd

    new-array v2, v6, [I

    fill-array-data v2, :array_d

    aput-object v2, v4, v1

    const/16 v1, 0xe

    new-array v2, v6, [I

    fill-array-data v2, :array_e

    aput-object v2, v4, v1

    const/16 v1, 0xf

    new-array v2, v6, [I

    fill-array-data v2, :array_f

    aput-object v2, v4, v1

    .line 245
    const/16 v1, 0x10

    new-array v5, v1, [[C

    new-array v1, v6, [C

    fill-array-data v1, :array_10

    aput-object v1, v5, v0

    new-array v1, v6, [C

    fill-array-data v1, :array_11

    aput-object v1, v5, v7

    new-array v1, v6, [C

    fill-array-data v1, :array_12

    aput-object v1, v5, v6

    new-array v1, v6, [C

    fill-array-data v1, :array_13

    aput-object v1, v5, v8

    new-array v1, v6, [C

    fill-array-data v1, :array_14

    aput-object v1, v5, v9

    const/4 v1, 0x5

    new-array v2, v6, [C

    fill-array-data v2, :array_15

    aput-object v2, v5, v1

    const/4 v1, 0x6

    new-array v2, v6, [C

    fill-array-data v2, :array_16

    aput-object v2, v5, v1

    const/4 v1, 0x7

    new-array v2, v6, [C

    fill-array-data v2, :array_17

    aput-object v2, v5, v1

    const/16 v1, 0x8

    new-array v2, v6, [C

    fill-array-data v2, :array_18

    aput-object v2, v5, v1

    const/16 v1, 0x9

    new-array v2, v6, [C

    fill-array-data v2, :array_19

    aput-object v2, v5, v1

    const/16 v1, 0xa

    new-array v2, v6, [C

    fill-array-data v2, :array_1a

    aput-object v2, v5, v1

    const/16 v1, 0xb

    new-array v2, v6, [C

    fill-array-data v2, :array_1b

    aput-object v2, v5, v1

    const/16 v1, 0xc

    new-array v2, v6, [C

    fill-array-data v2, :array_1c

    aput-object v2, v5, v1

    const/16 v1, 0xd

    new-array v2, v6, [C

    fill-array-data v2, :array_1d

    aput-object v2, v5, v1

    const/16 v1, 0xe

    new-array v2, v6, [C

    fill-array-data v2, :array_1e

    aput-object v2, v5, v1

    const/16 v1, 0xf

    new-array v2, v6, [C

    fill-array-data v2, :array_1f

    aput-object v2, v5, v1

    move v2, v0

    .line 283
    :goto_0
    if-ge v0, p1, :cond_0

    .line 284
    and-int/lit8 v6, p0, 0x1

    .line 285
    shr-int/lit8 p0, p0, 0x1

    .line 286
    add-int/lit8 v1, v0, 0x1

    aget-object v7, v5, v2

    aget-char v7, v7, v6

    int-to-byte v7, v7

    aput-byte v7, v3, v0

    .line 287
    aget-object v0, v4, v2

    aget v0, v0, v6

    move v2, v0

    move v0, v1

    .line 288
    goto :goto_0

    .line 290
    :cond_0
    const-string v0, ""

    .line 292
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, v3, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 296
    :goto_1
    return-object v0

    .line 293
    :catch_0
    move-exception v0

    .line 294
    const-string v0, ""

    goto :goto_1

    .line 210
    :array_0
    .array-data 4
        0xb
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x4
    .end array-data

    :array_2
    .array-data 4
        0x8
        0xf
    .end array-data

    :array_3
    .array-data 4
        0xb
        0x2
    .end array-data

    :array_4
    .array-data 4
        0x0
        0x3
    .end array-data

    :array_5
    .array-data 4
        0x9
        0x0
    .end array-data

    :array_6
    .array-data 4
        0xf
        0x0
    .end array-data

    :array_7
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_8
    .array-data 4
        0x5
        0x0
    .end array-data

    :array_9
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_a
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_b
    .array-data 4
        0x1
        0x6
    .end array-data

    :array_c
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_d
    .array-data 4
        0x3
        0xd
    .end array-data

    :array_e
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_f
    .array-data 4
        0x2
        0xd
    .end array-data

    .line 245
    :array_10
    .array-data 2
        0x73s
        0x33s
    .end array-data

    :array_11
    .array-data 2
        0x76s
        0x6es
    .end array-data

    :array_12
    .array-data 2
        0x31s
        0x39s
    .end array-data

    :array_13
    .array-data 2
        0x6ds
        0x30s
    .end array-data

    :array_14
    .array-data 2
        0x65s
        0x63s
    .end array-data

    :array_15
    .array-data 2
        0x33s
        0x42s
    .end array-data

    :array_16
    .array-data 2
        0x37s
        0x4es
    .end array-data

    :array_17
    .array-data 2
        0x6bs
        0x32s
    .end array-data

    :array_18
    .array-data 2
        0x32s
        0x43s
    .end array-data

    :array_19
    .array-data 2
        0x61s
        0x43s
    .end array-data

    :array_1a
    .array-data 2
        0x4as
        0x32s
    .end array-data

    :array_1b
    .array-data 2
        0x79s
        0x6cs
    .end array-data

    :array_1c
    .array-data 2
        0x38s
        0x64s
    .end array-data

    :array_1d
    .array-data 2
        0x31s
        0x30s
    .end array-data

    :array_1e
    .array-data 2
        0x41s
        0x5es
    .end array-data

    :array_1f
    .array-data 2
        0x37s
        0x30s
    .end array-data
.end method

.method public static a([B)Ljava/lang/String;
    .locals 4

    .prologue
    .line 130
    const-string v1, ""

    .line 133
    const/4 v0, 0x2

    const/4 v2, 0x0

    :try_start_0
    invoke-static {p0, v0, v2}, Lakt;->a([BILjava/lang/String;)[B

    move-result-object v2

    .line 134
    if-eqz v2, :cond_0

    .line 135
    new-instance v0, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-direct {v0, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :goto_0
    return-object v0

    .line 137
    :catch_0
    move-exception v0

    .line 138
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static a([BLjava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 111
    const-string v1, ""

    .line 114
    const/4 v0, 0x2

    :try_start_0
    invoke-static {p0, v0, p1}, Lakt;->a([BILjava/lang/String;)[B

    move-result-object v2

    .line 115
    if-eqz v2, :cond_0

    .line 116
    new-instance v0, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-direct {v0, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_0
    return-object v0

    .line 118
    :catch_0
    move-exception v0

    .line 119
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)[B
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 48
    .line 50
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 51
    const-string v1, "UTF-8"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lakt;->a([BILjava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 56
    :cond_0
    :goto_0
    return-object v0

    .line 53
    :catch_0
    move-exception v1

    .line 54
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 3

    .prologue
    .line 31
    const/4 v0, 0x0

    .line 33
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 34
    const-string v1, "UTF-8"

    invoke-virtual {p0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2, p1}, Lakt;->a([BILjava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 39
    :cond_0
    :goto_0
    return-object v0

    .line 36
    :catch_0
    move-exception v1

    .line 37
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static a([BILjava/lang/String;)[B
    .locals 5

    .prologue
    .line 174
    .line 176
    const/4 v0, 0x0

    .line 184
    :try_start_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 185
    const-string v1, "AES"

    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v1

    .line 186
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const/16 v3, 0x172c

    const/16 v4, 0x10

    invoke-static {v3, v4}, Lakt;->a(II)Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    const-string v4, "AES"

    invoke-direct {v2, v3, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v1, p1, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 187
    invoke-virtual {v1, p0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    .line 205
    :goto_0
    return-object v0

    .line 189
    :cond_0
    const-string v1, "SHA1PRNG"

    invoke-static {v1}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v1

    .line 190
    const-string v2, "AES"

    invoke-static {v2}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v2

    .line 191
    const-string v3, "DisasterLog"

    const-string v4, "UTF-8"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/security/SecureRandom;->setSeed([B)V

    .line 192
    const/16 v3, 0x80

    invoke-virtual {v2, v3, v1}, Ljavax/crypto/KeyGenerator;->init(ILjava/security/SecureRandom;)V

    .line 193
    invoke-virtual {v2}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v1

    .line 194
    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v1

    .line 196
    const-string v2, "AES"

    invoke-static {v2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v2

    .line 197
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v4, "AES"

    invoke-direct {v3, v1, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    invoke-virtual {v2, p1, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 198
    invoke-virtual {v2, p0}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 201
    :catch_0
    move-exception v1

    .line 202
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 65
    const-string v1, ""

    .line 67
    :try_start_0
    invoke-static {p0}, Lakt;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 68
    if-eqz v0, :cond_0

    .line 69
    invoke-static {v0}, Laku;->a([B)[B

    move-result-object v2

    .line 70
    if-eqz v2, :cond_0

    .line 71
    new-instance v0, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-direct {v0, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    :goto_0
    return-object v0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 86
    const-string v1, ""

    .line 89
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lakt;->a([BILjava/lang/String;)[B

    move-result-object v0

    .line 91
    if-eqz v0, :cond_0

    .line 92
    invoke-static {v0}, Laku;->a([B)[B

    move-result-object v2

    .line 93
    if-eqz v2, :cond_0

    .line 94
    new-instance v0, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-direct {v0, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :goto_0
    return-object v0

    .line 98
    :catch_0
    move-exception v0

    .line 99
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 149
    const-string v1, ""

    .line 152
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    invoke-static {p0}, Laku;->b(Ljava/lang/String;)[B

    move-result-object v0

    .line 154
    if-eqz v0, :cond_0

    .line 155
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lakt;->a([BILjava/lang/String;)[B

    move-result-object v2

    .line 156
    if-eqz v2, :cond_0

    .line 157
    new-instance v0, Ljava/lang/String;

    const-string v3, "UTF-8"

    invoke-direct {v0, v2, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    :goto_0
    return-object v0

    .line 161
    :catch_0
    move-exception v0

    .line 162
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method
