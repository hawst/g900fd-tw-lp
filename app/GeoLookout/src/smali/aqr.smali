.class public Laqr;
.super Ljava/lang/Object;
.source "SafetyCareWidgetInfo.java"


# instance fields
.field a:Ljava/lang/String;

.field b:I

.field c:I

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:I

.field g:Ljava/lang/String;

.field h:J

.field i:J

.field j:D

.field k:Ljava/lang/String;

.field l:Ljava/lang/String;

.field m:D

.field n:D

.field o:Ljava/lang/String;

.field p:Ljava/lang/String;

.field q:Ljava/lang/String;

.field r:Ljava/lang/String;

.field s:Ljava/lang/String;

.field t:Ljava/lang/String;

.field u:I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const-string v0, ""

    iput-object v0, p0, Laqr;->a:Ljava/lang/String;

    .line 17
    iput v2, p0, Laqr;->b:I

    .line 18
    iput v1, p0, Laqr;->c:I

    .line 19
    const-string v0, ""

    iput-object v0, p0, Laqr;->d:Ljava/lang/String;

    .line 20
    const-string v0, ""

    iput-object v0, p0, Laqr;->e:Ljava/lang/String;

    .line 21
    iput v1, p0, Laqr;->f:I

    .line 22
    const-string v0, ""

    iput-object v0, p0, Laqr;->g:Ljava/lang/String;

    .line 23
    iput-wide v4, p0, Laqr;->h:J

    .line 24
    iput-wide v4, p0, Laqr;->i:J

    .line 25
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Laqr;->j:D

    .line 26
    const-string v0, ""

    iput-object v0, p0, Laqr;->k:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Laqr;->l:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Laqr;->o:Ljava/lang/String;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Laqr;->p:Ljava/lang/String;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Laqr;->q:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Laqr;->r:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Laqr;->s:Ljava/lang/String;

    .line 34
    const-string v0, ""

    iput-object v0, p0, Laqr;->t:Ljava/lang/String;

    .line 35
    iput v2, p0, Laqr;->u:I

    return-void
.end method

.method public static a(Landroid/content/Context;)Laqr;
    .locals 1

    .prologue
    .line 38
    const/4 v0, -0x1

    invoke-static {p0, v0}, Laqr;->a(Landroid/content/Context;I)Laqr;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;I)Laqr;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 42
    new-instance v0, Laqr;

    invoke-direct {v0}, Laqr;-><init>()V

    .line 44
    invoke-static {p0}, Lage;->z(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 45
    const-string v1, "createSafetyCareInfo : GeoNews is OFF"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 106
    :goto_0
    return-object v0

    .line 51
    :cond_0
    sget-boolean v1, Lakv;->bP:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lakv;->bR:Z

    if-eqz v1, :cond_4

    .line 52
    :cond_1
    invoke-static {p0, p1}, Lalx;->c(Landroid/content/Context;I)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v1

    .line 57
    :goto_1
    if-eqz v1, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getEndTime()J

    move-result-wide v4

    add-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    .line 58
    const-string v1, "end time is over!"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 60
    invoke-static {p0, v6, v7}, Lalx;->a(Landroid/content/Context;J)V

    .line 62
    sget-boolean v1, Lakv;->bP:Z

    if-nez v1, :cond_2

    sget-boolean v1, Lakv;->bR:Z

    if-eqz v1, :cond_5

    .line 63
    :cond_2
    const/4 v1, -0x1

    invoke-static {p0, v1}, Lalx;->c(Landroid/content/Context;I)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v1

    .line 69
    :cond_3
    :goto_2
    if-nez v1, :cond_6

    .line 70
    const-string v1, " DI is nulll..."

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 71
    const-string v1, ""

    iput-object v1, v0, Laqr;->a:Ljava/lang/String;

    goto :goto_0

    .line 54
    :cond_4
    invoke-static {p0, p1}, Lalx;->b(Landroid/content/Context;I)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v1

    goto :goto_1

    .line 65
    :cond_5
    invoke-static {p0, p1}, Lalx;->b(Landroid/content/Context;I)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v1

    goto :goto_2

    .line 76
    :cond_6
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v2

    iput v2, v0, Laqr;->b:I

    .line 77
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Laqr;->a:Ljava/lang/String;

    .line 78
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v2

    iput v2, v0, Laqr;->c:I

    .line 79
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getCountry()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Laqr;->d:Ljava/lang/String;

    .line 80
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getCity()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Laqr;->e:Ljava/lang/String;

    .line 81
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getStartTime()J

    move-result-wide v2

    iput-wide v2, v0, Laqr;->i:J

    .line 82
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getEndTime()J

    move-result-wide v2

    iput-wide v2, v0, Laqr;->h:J

    .line 83
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getStartTime()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lall;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Laqr;->k:Ljava/lang/String;

    .line 84
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getEndTime()J

    move-result-wide v2

    invoke-static {p0, v2, v3}, Lall;->d(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Laqr;->l:Ljava/lang/String;

    .line 85
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getHeadline()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Laqr;->g:Ljava/lang/String;

    .line 86
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailsMagnitude()D

    move-result-wide v2

    iput-wide v2, v0, Laqr;->j:D

    .line 87
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v2

    iget-wide v2, v2, Lamf;->a:D

    iput-wide v2, v0, Laqr;->m:D

    .line 88
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v2

    iget-wide v2, v2, Lamf;->b:D

    iput-wide v2, v0, Laqr;->n:D

    .line 89
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getHeadline()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Laqr;->s:Ljava/lang/String;

    .line 90
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Laqr;->t:Ljava/lang/String;

    .line 91
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v2

    iput v2, v0, Laqr;->u:I

    .line 93
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailSpeed()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailSpeed()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 94
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailSpeed()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Laqr;->o:Ljava/lang/String;

    .line 97
    :cond_7
    const-string v2, "03"

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 98
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsZoneCity()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Laqr;->p:Ljava/lang/String;

    .line 99
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsLevel()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Laqr;->q:Ljava/lang/String;

    .line 100
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsDepth()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Laqr;->r:Ljava/lang/String;

    .line 103
    :cond_8
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createSafetyCareInfo(), DID = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 104
    invoke-static {p0}, Lanb;->c(Landroid/content/Context;)Lamf;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {p0, v1, v2, v3}, Lall;->a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;Lamf;Z)I

    move-result v1

    iput v1, v0, Laqr;->f:I

    goto/16 :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    const-string v1, "[alertID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    iget-object v1, p0, Laqr;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string v1, ", typeId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    iget v1, p0, Laqr;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 116
    const-string v1, ", details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    iget-object v1, p0, Laqr;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    const-string v1, ", city="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    iget-object v1, p0, Laqr;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    const-string v1, ", startTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    iget-wide v2, p0, Laqr;->i:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 122
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    iget-object v1, p0, Laqr;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    const-string v1, ", endTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    iget-wide v2, p0, Laqr;->h:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 127
    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    iget-object v1, p0, Laqr;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
