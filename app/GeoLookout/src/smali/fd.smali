.class public final Lfd;
.super Lco;

# interfaces
.implements Let;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/data/d;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lco;-><init>(Lcom/google/android/gms/common/data/d;I)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const-string v0, "timespan"

    invoke-virtual {p0, v0}, Lfd;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    const-string v0, "collection"

    invoke-virtual {p0, v0}, Lfd;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    const-string v0, "player_raw_score"

    invoke-virtual {p0, v0}, Lfd;->g(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()J
    .locals 2

    const-string v0, "player_raw_score"

    invoke-virtual {p0, v0}, Lfd;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    const-string v0, "player_raw_score"

    invoke-virtual {p0, v0}, Lfd;->a(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    const-string v0, "player_display_score"

    invoke-virtual {p0, v0}, Lfd;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()J
    .locals 2

    const-string v0, "player_rank"

    invoke-virtual {p0, v0}, Lfd;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    const-string v0, "player_rank"

    invoke-virtual {p0, v0}, Lfd;->a(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    const-string v0, "player_display_rank"

    invoke-virtual {p0, v0}, Lfd;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    const-string v0, "player_score_tag"

    invoke-virtual {p0, v0}, Lfd;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i()J
    .locals 2

    const-string v0, "total_scores"

    invoke-virtual {p0, v0}, Lfd;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    const-string v0, "total_scores"

    invoke-virtual {p0, v0}, Lfd;->a(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    const-string v0, "top_page_token_next"

    invoke-virtual {p0, v0}, Lfd;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    const-string v0, "window_page_token_prev"

    invoke-virtual {p0, v0}, Lfd;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    const-string v0, "window_page_token_next"

    invoke-virtual {p0, v0}, Lfd;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    invoke-static {p0}, Lnu;->a(Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "TimeSpan"

    invoke-virtual {p0}, Lfd;->a()I

    move-result v2

    invoke-static {v2}, Lqz;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "Collection"

    invoke-virtual {p0}, Lfd;->b()I

    move-result v2

    invoke-static {v2}, Lqy;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v1

    const-string v2, "RawPlayerScore"

    invoke-virtual {p0}, Lfd;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lfd;->d()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v1

    const-string v2, "DisplayPlayerScore"

    invoke-virtual {p0}, Lfd;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lfd;->e()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v2, v0}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v1

    const-string v2, "PlayerRank"

    invoke-virtual {p0}, Lfd;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lfd;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v2, v0}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v1

    const-string v2, "DisplayPlayerRank"

    invoke-virtual {p0}, Lfd;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lfd;->g()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v2, v0}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "NumScores"

    invoke-virtual {p0}, Lfd;->i()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "TopPageNextToken"

    invoke-virtual {p0}, Lfd;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "WindowPageNextToken"

    invoke-virtual {p0}, Lfd;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "WindowPagePrevToken"

    invoke-virtual {p0}, Lfd;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    invoke-virtual {v0}, Lnw;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "none"

    goto :goto_0

    :cond_1
    const-string v0, "none"

    goto :goto_1

    :cond_2
    const-string v0, "none"

    goto :goto_2

    :cond_3
    const-string v0, "none"

    goto :goto_3
.end method
