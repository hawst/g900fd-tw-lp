.class Lakk;
.super Landroid/content/BroadcastReceiver;
.source "DisasterSettingsFragment.java"


# instance fields
.field final synthetic a:Lakf;


# direct methods
.method constructor <init>(Lakf;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lakk;->a:Lakf;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 151
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 152
    const-string v3, "com.sec.android.geolookout.Registered"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 153
    iget-object v2, p0, Lakk;->a:Lakf;

    invoke-static {v2}, Lakf;->a(Lakf;)Lakr;

    move-result-object v2

    invoke-virtual {v2, v0}, Lakr;->setEnabled(Z)V

    .line 155
    iget-object v2, p0, Lakk;->a:Lakf;

    invoke-virtual {v2}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "safetycare_earthquake"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_1

    .line 157
    :goto_0
    iget-object v1, p0, Lakk;->a:Lakf;

    invoke-static {v1}, Lakf;->a(Lakf;)Lakr;

    move-result-object v1

    invoke-virtual {v1, v0}, Lakr;->setChecked(Z)V

    .line 159
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 155
    goto :goto_0
.end method
