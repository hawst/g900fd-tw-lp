.class public Lage;
.super Ljava/lang/Object;
.source "DisasterState.java"


# static fields
.field public static final a:I = 0x2710

.field public static final b:I = 0x2710

.field public static final c:I = 0x7530

.field public static final d:I = 0x1adb0

.field public static final e:I = 0xd6d8

.field public static final f:I = 0xd6d8

.field public static final g:I = 0xd6d8

.field public static final h:I = 0x1adb0

.field public static final i:I = 0xd6d8

.field public static final j:I = 0x7530

.field public static k:Z

.field public static l:Z

.field private static volatile n:Lage;

.field private static o:Z

.field private static p:Z


# instance fields
.field private m:Landroid/content/Context;

.field private q:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    const/4 v0, 0x0

    sput-object v0, Lage;->n:Lage;

    .line 70
    sput-boolean v1, Lage;->k:Z

    .line 71
    sput-boolean v1, Lage;->l:Z

    .line 564
    sput-boolean v1, Lage;->o:Z

    .line 596
    sput-boolean v1, Lage;->p:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lage;->m:Landroid/content/Context;

    .line 633
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lage;->q:J

    .line 30
    iput-object p1, p0, Lage;->m:Landroid/content/Context;

    .line 31
    return-void
.end method

.method public static A(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 534
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "safetycare_geolookout_registering"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 538
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static B(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 553
    const-string v0, "broadcastIntentToSettings()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 554
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.geolookout.Registered"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 555
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 556
    return-void
.end method

.method public static a(Landroid/content/Context;)Lage;
    .locals 2

    .prologue
    .line 34
    sget-object v0, Lage;->n:Lage;

    if-nez v0, :cond_1

    .line 35
    const-class v1, Lage;

    monitor-enter v1

    .line 36
    :try_start_0
    sget-object v0, Lage;->n:Lage;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Lage;

    invoke-direct {v0, p0}, Lage;-><init>(Landroid/content/Context;)V

    sput-object v0, Lage;->n:Lage;

    .line 39
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41
    :cond_1
    sget-object v0, Lage;->n:Lage;

    return-object v0

    .line 39
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(Landroid/content/Context;II)V
    .locals 0

    .prologue
    .line 656
    return-void
.end method

.method private c(Landroid/content/Context;I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 543
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setEnableDisasterDim :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-ne p2, v0, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 544
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "safetycare_geolookout_registering"

    invoke-static {v0, v1, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 545
    return-void

    .line 543
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 0

    .prologue
    .line 641
    return-void
.end method

.method private d(Landroid/content/Context;I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 548
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setEnableDisaster :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-ne p2, v0, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 549
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "safetycare_earthquake"

    invoke-static {v0, v1, p2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 550
    return-void

    .line 548
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static z(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 525
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "safetycare_earthquake"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 529
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 576
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 577
    const-string v1, "com.sec.android.ACTION_REMOVE_ANIMATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 578
    const-string v1, "error"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 579
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 581
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 582
    const-string v1, "com.sec.android.geolookout.resultrefresh"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 583
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 586
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lage;->b(Z)V

    .line 587
    return-void
.end method

.method public a(Landroid/content/Context;IILjava/lang/String;)V
    .locals 0

    .prologue
    .line 631
    return-void
.end method

.method public a(Landroid/content/Context;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 254
    const-string v0, "checkLocationFailure"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 255
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    const/16 v1, 0xa

    const/16 v2, 0x3e8

    invoke-virtual {v0, p1, v1, v2}, Lanf;->a(Landroid/content/Context;II)V

    .line 256
    invoke-direct {p0, p1, v3}, Lage;->d(Landroid/content/Context;I)V

    .line 257
    if-nez p2, :cond_0

    .line 258
    const/16 v0, 0x34

    invoke-virtual {p0, p1, v4, v0, v5}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    .line 262
    :goto_0
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 263
    invoke-direct {p0, p1, v3}, Lage;->c(Landroid/content/Context;I)V

    .line 264
    invoke-virtual {p0, v3}, Lage;->a(Z)V

    .line 265
    return-void

    .line 260
    :cond_0
    const/16 v0, 0x35

    invoke-virtual {p0, p1, v4, v0, v5}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 78
    sput-boolean p1, Lage;->k:Z

    .line 79
    if-nez p1, :cond_0

    .line 80
    iget-object v0, p0, Lage;->m:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lage;->b(Landroid/content/Context;)V

    .line 81
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 74
    sget-boolean v0, Lage;->k:Z

    return v0
.end method

.method public b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 84
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "End state\nOnDisaster(): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lage;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", EnableDisaster(): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", \nSPPRegistered(): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Laov;->a(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", DserverRegistered(): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Laog;->a(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", \nSettings Dim: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lage;->A(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method public b(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 609
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 610
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA_RESULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 611
    const-string v1, "error"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 612
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 615
    invoke-static {p1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 616
    return-void
.end method

.method public b(Landroid/content/Context;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 280
    const-string v0, "registerFailureToSPP"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 281
    invoke-direct {p0, p1, v2}, Lage;->d(Landroid/content/Context;I)V

    .line 282
    invoke-direct {p0, p1, v2}, Lage;->c(Landroid/content/Context;I)V

    .line 283
    if-nez p2, :cond_0

    .line 284
    const/16 v0, 0x37

    invoke-virtual {p0, p1, v1, v0, v3}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    .line 288
    :goto_0
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 289
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, p1, v1, v2}, Lanf;->a(Landroid/content/Context;II)V

    .line 290
    invoke-virtual {p0, v2}, Lage;->a(Z)V

    .line 292
    return-void

    .line 286
    :cond_0
    const/16 v0, 0x38

    invoke-virtual {p0, p1, v1, v0, v3}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 571
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setRefreshLocationAndData :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 572
    sput-boolean p1, Lage;->o:Z

    .line 573
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 567
    sget-boolean v0, Lage;->o:Z

    return v0
.end method

.method public c(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 90
    invoke-virtual {p0, v0}, Lage;->a(Z)V

    .line 91
    invoke-direct {p0, p1, v0}, Lage;->c(Landroid/content/Context;I)V

    .line 92
    return-void
.end method

.method public c(Landroid/content/Context;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 302
    const-string v0, "registerFailureToDServer"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 303
    invoke-static {p1}, Lcom/sec/android/GeoLookout/DisasterAlertService;->a(Landroid/content/Context;)V

    .line 304
    invoke-static {p1}, Lall;->b(Landroid/content/Context;)V

    .line 306
    iget-object v0, p0, Lage;->m:Landroid/content/Context;

    invoke-direct {p0, v0, v3}, Lage;->d(Landroid/content/Context;I)V

    .line 307
    invoke-direct {p0, p1, v3}, Lage;->c(Landroid/content/Context;I)V

    .line 308
    if-nez p2, :cond_0

    .line 309
    const/16 v0, 0x39

    invoke-virtual {p0, p1, v1, v0, v2}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    .line 313
    :goto_0
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 314
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    iget-object v1, p0, Lage;->m:Landroid/content/Context;

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Lanf;->a(Landroid/content/Context;II)V

    .line 316
    invoke-static {}, Laop;->a()Laop;

    move-result-object v0

    iget-object v1, p0, Lage;->m:Landroid/content/Context;

    invoke-virtual {v0, v1}, Laop;->c(Landroid/content/Context;)V

    .line 317
    return-void

    .line 311
    :cond_0
    const/16 v0, 0x3a

    invoke-virtual {p0, p1, v1, v0, v2}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    goto :goto_0
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 604
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setRefreshLifeData :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 605
    sput-boolean p1, Lage;->p:Z

    .line 606
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 599
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getRefreshLifeData :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-boolean v1, Lage;->p:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 600
    sget-boolean v0, Lage;->p:Z

    return v0
.end method

.method public d(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 95
    const-string v0, "setDisasterInits"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 96
    invoke-virtual {p0, v1}, Lage;->a(Z)V

    .line 97
    invoke-direct {p0, p1, v1}, Lage;->d(Landroid/content/Context;I)V

    .line 98
    invoke-direct {p0, p1, v1}, Lage;->c(Landroid/content/Context;I)V

    .line 99
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 100
    invoke-static {p1, v1}, Laov;->a(Landroid/content/Context;Z)V

    .line 101
    invoke-static {p1, v1}, Laog;->a(Landroid/content/Context;Z)V

    .line 102
    invoke-static {p1}, Lall;->b(Landroid/content/Context;)V

    .line 105
    invoke-static {p1}, Lama;->c(Landroid/content/Context;)V

    .line 106
    invoke-static {p1}, Laly;->b(Landroid/content/Context;)V

    .line 107
    invoke-static {p1}, Lalx;->c(Landroid/content/Context;)V

    .line 108
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.sec.android.GeoLookout.SETTING_CHANGE_FINISH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 109
    invoke-virtual {p0, p1}, Lage;->b(Landroid/content/Context;)V

    .line 110
    return-void
.end method

.method public d(Landroid/content/Context;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 327
    const-string v0, "queryLocationFailureToDServer"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 328
    invoke-static {p1}, Lcom/sec/android/GeoLookout/DisasterAlertService;->a(Landroid/content/Context;)V

    .line 329
    invoke-static {p1}, Lall;->b(Landroid/content/Context;)V

    .line 331
    iget-object v0, p0, Lage;->m:Landroid/content/Context;

    invoke-direct {p0, v0, v3}, Lage;->d(Landroid/content/Context;I)V

    .line 332
    invoke-direct {p0, p1, v3}, Lage;->c(Landroid/content/Context;I)V

    .line 333
    if-nez p2, :cond_0

    .line 334
    const/16 v0, 0x3d

    invoke-virtual {p0, p1, v1, v0, v2}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    .line 338
    :goto_0
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 339
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    iget-object v1, p0, Lage;->m:Landroid/content/Context;

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Lanf;->a(Landroid/content/Context;II)V

    .line 341
    invoke-static {}, Laop;->a()Laop;

    move-result-object v0

    iget-object v1, p0, Lage;->m:Landroid/content/Context;

    invoke-virtual {v0, v1}, Laop;->c(Landroid/content/Context;)V

    .line 342
    return-void

    .line 336
    :cond_0
    const/16 v0, 0x3e

    invoke-virtual {p0, p1, v1, v0, v2}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    goto :goto_0
.end method

.method public e(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 119
    const-string v0, "settingOnDisasterStart"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0, v4}, Lage;->a(Z)V

    .line 121
    invoke-static {p1}, Lane;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    const/16 v1, 0xa

    const/16 v2, 0x190

    invoke-virtual {v0, p1, v1, v2}, Lanf;->a(Landroid/content/Context;II)V

    .line 123
    invoke-direct {p0, p1, v3}, Lage;->d(Landroid/content/Context;I)V

    .line 124
    const/16 v0, 0x33

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v4, v0, v1}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    .line 125
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 126
    invoke-direct {p0, p1, v3}, Lage;->c(Landroid/content/Context;I)V

    .line 127
    invoke-virtual {p0, v3}, Lage;->a(Z)V

    .line 136
    :goto_0
    return-void

    .line 130
    :cond_0
    invoke-direct {p0, p1, v4}, Lage;->c(Landroid/content/Context;I)V

    .line 131
    invoke-direct {p0}, Lage;->d()V

    .line 132
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/DisasterService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 133
    const-string v1, "com.sec.android.GeoLookout.ACTION_REQUEST_START_CURRENT_LOCATION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public e(Landroid/content/Context;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 367
    const-string v0, "updateLocationFailureToDServer"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 368
    invoke-static {p1}, Lcom/sec/android/GeoLookout/DisasterAlertService;->a(Landroid/content/Context;)V

    .line 369
    invoke-static {p1}, Lall;->b(Landroid/content/Context;)V

    .line 371
    iget-object v0, p0, Lage;->m:Landroid/content/Context;

    invoke-direct {p0, v0, v3}, Lage;->d(Landroid/content/Context;I)V

    .line 372
    invoke-direct {p0, p1, v3}, Lage;->c(Landroid/content/Context;I)V

    .line 373
    if-nez p2, :cond_0

    .line 374
    const/16 v0, 0x40

    invoke-virtual {p0, p1, v1, v0, v2}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    .line 378
    :goto_0
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 379
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    iget-object v1, p0, Lage;->m:Landroid/content/Context;

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Lanf;->a(Landroid/content/Context;II)V

    .line 381
    invoke-static {}, Laop;->a()Laop;

    move-result-object v0

    iget-object v1, p0, Lage;->m:Landroid/content/Context;

    invoke-virtual {v0, v1}, Laop;->c(Landroid/content/Context;)V

    .line 382
    return-void

    .line 376
    :cond_0
    const/16 v0, 0x41

    invoke-virtual {p0, p1, v1, v0, v2}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    goto :goto_0
.end method

.method public f(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 140
    const-string v0, "settingOffDisasterEnd"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 141
    invoke-virtual {p0, v3}, Lage;->a(Z)V

    .line 142
    invoke-static {p1}, Lane;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 143
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    const/16 v1, 0x1e

    const/16 v2, 0x190

    invoke-virtual {v0, p1, v1, v2}, Lanf;->a(Landroid/content/Context;II)V

    .line 144
    invoke-direct {p0, p1, v3}, Lage;->d(Landroid/content/Context;I)V

    .line 145
    const/4 v0, 0x3

    const/16 v1, 0x33

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    .line 146
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 147
    invoke-direct {p0, p1, v4}, Lage;->c(Landroid/content/Context;I)V

    .line 148
    invoke-virtual {p0, v4}, Lage;->a(Z)V

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    invoke-direct {p0, p1, v3}, Lage;->c(Landroid/content/Context;I)V

    .line 153
    invoke-direct {p0}, Lage;->d()V

    .line 155
    invoke-static {p1}, Lcom/sec/android/GeoLookout/DisasterAlertService;->a(Landroid/content/Context;)V

    .line 157
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/DisasterService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 158
    const-string v1, "com.sec.android.GeoLookout.ACTION_UNREGISTER_DISASTER_SERVER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 160
    sget-boolean v0, Laky;->k:Z

    if-eqz v0, :cond_0

    .line 161
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/lifemode/LifemodeService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 162
    const-string v1, "com.sec.android.GeoLookout.ACTION_CANCEL_LIFEMODE_SYNC_SCHEDULER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 163
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public f(Landroid/content/Context;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 404
    const-string v0, "unRegisterFailureToDServer"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 406
    iget-object v0, p0, Lage;->m:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lage;->d(Landroid/content/Context;I)V

    .line 407
    invoke-direct {p0, p1, v3}, Lage;->c(Landroid/content/Context;I)V

    .line 408
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    iget-object v1, p0, Lage;->m:Landroid/content/Context;

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v2, v3}, Lanf;->a(Landroid/content/Context;II)V

    .line 409
    if-nez p2, :cond_0

    .line 410
    const/16 v0, 0x42

    invoke-virtual {p0, p1, v4, v0, v5}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    .line 414
    :goto_0
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 415
    invoke-virtual {p0, v3}, Lage;->a(Z)V

    .line 416
    return-void

    .line 412
    :cond_0
    const/16 v0, 0x43

    invoke-virtual {p0, p1, v4, v0, v5}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    goto :goto_0
.end method

.method public g(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 169
    invoke-static {p1}, Laov;->a(Landroid/content/Context;)Z

    move-result v0

    .line 170
    invoke-static {p1}, Laog;->a(Landroid/content/Context;)Z

    move-result v1

    .line 172
    invoke-virtual {p0, v3}, Lage;->a(Z)V

    .line 173
    if-nez v0, :cond_0

    if-nez v1, :cond_0

    .line 174
    invoke-direct {p0, p1, v2}, Lage;->d(Landroid/content/Context;I)V

    .line 177
    :cond_0
    if-ne v0, v3, :cond_1

    if-nez v1, :cond_1

    .line 178
    invoke-direct {p0, p1, v2}, Lage;->d(Landroid/content/Context;I)V

    .line 180
    :cond_1
    invoke-virtual {p0, v2}, Lage;->a(Z)V

    .line 181
    return-void
.end method

.method public g(Landroid/content/Context;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 426
    const-string v0, "unRegisterFailureToSPP"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 427
    invoke-direct {p0, p1, v2}, Lage;->d(Landroid/content/Context;I)V

    .line 428
    invoke-direct {p0, p1, v2}, Lage;->c(Landroid/content/Context;I)V

    .line 429
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    const/16 v1, 0x28

    invoke-virtual {v0, p1, v1, v2}, Lanf;->a(Landroid/content/Context;II)V

    .line 430
    if-nez p2, :cond_0

    .line 431
    const/16 v0, 0x44

    invoke-virtual {p0, p1, v3, v0, v4}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    .line 435
    :goto_0
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 437
    invoke-static {}, Laop;->a()Laop;

    move-result-object v0

    iget-object v1, p0, Lage;->m:Landroid/content/Context;

    invoke-virtual {v0, v1}, Laop;->c(Landroid/content/Context;)V

    .line 438
    return-void

    .line 433
    :cond_0
    const/16 v0, 0x45

    invoke-virtual {p0, p1, v3, v0, v4}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    goto :goto_0
.end method

.method public h(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 185
    invoke-static {p1}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 186
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lage;->c(Landroid/content/Context;I)V

    .line 187
    :cond_0
    return-void
.end method

.method public i(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 191
    invoke-virtual {p0}, Lage;->a()Z

    move-result v0

    if-ne v0, v1, :cond_0

    .line 192
    const-string v0, "setNetworkChange() off"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 193
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v1, :cond_1

    .line 194
    invoke-direct {p0, p1, v2}, Lage;->d(Landroid/content/Context;I)V

    .line 196
    invoke-static {}, Laoi;->a()Laoi;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laoi;->a(Laou;)V

    .line 197
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, p1, v1, v2}, Lanf;->a(Landroid/content/Context;II)V

    .line 202
    :goto_0
    invoke-direct {p0, p1, v2}, Lage;->c(Landroid/content/Context;I)V

    .line 203
    invoke-virtual {p0, v2}, Lage;->a(Z)V

    .line 205
    :cond_0
    return-void

    .line 200
    :cond_1
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public j(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 214
    const-string v0, "retryManager()"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 215
    invoke-virtual {p0, p1}, Lage;->b(Landroid/content/Context;)V

    .line 217
    invoke-virtual {p0}, Lage;->a()Z

    move-result v0

    .line 218
    invoke-static {p1}, Laov;->a(Landroid/content/Context;)Z

    move-result v1

    .line 219
    invoke-static {p1}, Laog;->a(Landroid/content/Context;)Z

    move-result v2

    .line 221
    if-nez v0, :cond_2

    .line 222
    invoke-static {p1}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v3, :cond_0

    .line 223
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lage;->c(Landroid/content/Context;I)V

    .line 225
    :cond_0
    if-ne v1, v3, :cond_1

    .line 226
    const-string v0, "retryDeregisterSPP"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 227
    invoke-static {}, Laop;->a()Laop;

    move-result-object v0

    invoke-virtual {v0, p1}, Laop;->c(Landroid/content/Context;)V

    .line 229
    :cond_1
    if-ne v2, v3, :cond_2

    .line 230
    const-string v0, "UnDeregisterDserver"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 231
    invoke-static {p1}, Lcom/sec/android/GeoLookout/DisasterAlertService;->a(Landroid/content/Context;)V

    .line 232
    invoke-static {p1}, Lall;->b(Landroid/content/Context;)V

    .line 234
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/GeoLookout/DisasterService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 235
    const-string v1, "com.sec.android.GeoLookout.ACTION_UNREGISTER_DISASTER_SERVER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 236
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 239
    :cond_2
    return-void
.end method

.method public k(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 246
    const-string v0, "checkLocationSuccess"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 247
    const/4 v0, 0x4

    const/16 v1, 0x66

    invoke-direct {p0, p1, v0, v1}, Lage;->a(Landroid/content/Context;II)V

    .line 248
    invoke-direct {p0}, Lage;->d()V

    .line 249
    invoke-static {}, Laop;->a()Laop;

    move-result-object v0

    invoke-virtual {v0, p1}, Laop;->a(Landroid/content/Context;)V

    .line 250
    return-void
.end method

.method public l(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 269
    const-string v0, "checkLocationRequestTurnOn"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 270
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    const/16 v1, 0xa

    const/16 v2, 0x3e9

    invoke-virtual {v0, p1, v1, v2}, Lanf;->a(Landroid/content/Context;II)V

    .line 271
    invoke-direct {p0, p1, v3}, Lage;->d(Landroid/content/Context;I)V

    .line 272
    const/4 v0, 0x1

    const/16 v1, 0x36

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    .line 273
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 274
    invoke-direct {p0, p1, v3}, Lage;->c(Landroid/content/Context;I)V

    .line 275
    invoke-virtual {p0, v3}, Lage;->a(Z)V

    .line 276
    return-void
.end method

.method public m(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 295
    const-string v0, "registerSuccessToSPP"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 296
    const/4 v0, 0x4

    const/16 v1, 0x69

    invoke-direct {p0, p1, v0, v1}, Lage;->a(Landroid/content/Context;II)V

    .line 297
    invoke-direct {p0}, Lage;->d()V

    .line 298
    return-void
.end method

.method public n(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 320
    const-string v0, "registerSuccessToDServer"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 321
    const/4 v0, 0x4

    const/16 v1, 0x6b

    invoke-direct {p0, p1, v0, v1}, Lage;->a(Landroid/content/Context;II)V

    .line 322
    invoke-direct {p0}, Lage;->d()V

    .line 323
    return-void
.end method

.method public o(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 345
    const-string v0, "queryLocationSuccessToDServer"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 346
    const/4 v0, 0x4

    const/16 v1, 0x6f

    invoke-direct {p0, p1, v0, v1}, Lage;->a(Landroid/content/Context;II)V

    .line 347
    invoke-direct {p0}, Lage;->d()V

    .line 348
    return-void
.end method

.method public p(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 352
    const-string v0, "CheckZoneChangedFailure"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 353
    invoke-static {p1}, Lcom/sec/android/GeoLookout/DisasterAlertService;->a(Landroid/content/Context;)V

    .line 354
    invoke-static {p1}, Lall;->b(Landroid/content/Context;)V

    .line 356
    iget-object v0, p0, Lage;->m:Landroid/content/Context;

    invoke-direct {p0, v0, v3}, Lage;->d(Landroid/content/Context;I)V

    .line 357
    invoke-direct {p0, p1, v3}, Lage;->c(Landroid/content/Context;I)V

    .line 358
    const/4 v0, 0x1

    const/16 v1, 0x3f

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    .line 359
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 360
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    iget-object v1, p0, Lage;->m:Landroid/content/Context;

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Lanf;->a(Landroid/content/Context;II)V

    .line 362
    invoke-static {}, Laop;->a()Laop;

    move-result-object v0

    iget-object v1, p0, Lage;->m:Landroid/content/Context;

    invoke-virtual {v0, v1}, Laop;->c(Landroid/content/Context;)V

    .line 363
    return-void
.end method

.method public q(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 386
    const-string v0, "updateLocationSuccessToDServer"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 387
    const/4 v0, 0x4

    const/16 v1, 0x72

    invoke-direct {p0, p1, v0, v1}, Lage;->a(Landroid/content/Context;II)V

    .line 388
    iget-object v0, p0, Lage;->m:Landroid/content/Context;

    invoke-direct {p0, v0, v4}, Lage;->d(Landroid/content/Context;I)V

    .line 389
    invoke-direct {p0, p1, v3}, Lage;->c(Landroid/content/Context;I)V

    .line 390
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v3, v3, v0}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    .line 391
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 392
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    iget-object v1, p0, Lage;->m:Landroid/content/Context;

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Lanf;->a(Landroid/content/Context;I)V

    .line 393
    invoke-virtual {p0, v3}, Lage;->a(Z)V

    .line 394
    sget-boolean v0, Laky;->c:Z

    if-ne v0, v4, :cond_0

    .line 395
    iget-object v0, p0, Lage;->m:Landroid/content/Context;

    invoke-static {}, Lane;->a()Z

    move-result v1

    invoke-static {v0, v1}, Lall;->a(Landroid/content/Context;Z)V

    .line 397
    :cond_0
    return-void
.end method

.method public r(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 419
    const-string v0, "unRegisterSuccessToDServer"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 420
    const/4 v0, 0x4

    const/16 v1, 0x74

    invoke-direct {p0, p1, v0, v1}, Lage;->a(Landroid/content/Context;II)V

    .line 421
    invoke-direct {p0}, Lage;->d()V

    .line 422
    return-void
.end method

.method public s(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 442
    const-string v0, "unRegisterFailureToSPP"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 443
    const/4 v0, 0x4

    const/16 v1, 0x76

    invoke-direct {p0, p1, v0, v1}, Lage;->a(Landroid/content/Context;II)V

    .line 444
    invoke-direct {p0, p1, v2}, Lage;->d(Landroid/content/Context;I)V

    .line 445
    invoke-direct {p0, p1, v2}, Lage;->c(Landroid/content/Context;I)V

    .line 446
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    const/16 v1, 0x28

    invoke-virtual {v0, p1, v1}, Lanf;->a(Landroid/content/Context;I)V

    .line 447
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v2, v1}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    .line 448
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 449
    invoke-virtual {p0, v2}, Lage;->a(Z)V

    .line 450
    return-void
.end method

.method public t(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 458
    const-string v0, "registerTimeOutToSPP"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 459
    invoke-static {}, Laoi;->a()Laoi;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laoi;->a(Laou;)V

    .line 460
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lage;->b(Landroid/content/Context;Z)V

    .line 461
    return-void
.end method

.method public u(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 464
    const-string v0, "unRegisterTimeOutToSPP"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 465
    invoke-static {}, Laoi;->a()Laoi;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Laoi;->a(Laou;)V

    .line 466
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lage;->g(Landroid/content/Context;Z)V

    .line 467
    return-void
.end method

.method public v(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 475
    const-string v0, "exceptionUnRegisterFailureToDServer"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 476
    invoke-direct {p0, p1, v3}, Lage;->d(Landroid/content/Context;I)V

    .line 477
    invoke-direct {p0, p1, v3}, Lage;->c(Landroid/content/Context;I)V

    .line 478
    const/4 v0, 0x3

    const/16 v1, 0x46

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    .line 479
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 480
    invoke-virtual {p0, v3}, Lage;->a(Z)V

    .line 481
    return-void
.end method

.method public w(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 484
    const-string v0, "fatalExceptionApplication"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 485
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 486
    invoke-direct {p0, p1, v3}, Lage;->d(Landroid/content/Context;I)V

    .line 487
    invoke-direct {p0, p1, v3}, Lage;->c(Landroid/content/Context;I)V

    .line 488
    const/4 v0, 0x1

    const/16 v1, 0x47

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    .line 489
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 490
    invoke-virtual {p0, v3}, Lage;->a(Z)V

    .line 492
    :cond_0
    return-void
.end method

.method public x(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 495
    const-string v0, "taskKillApplication"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 496
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 497
    invoke-direct {p0, p1, v3}, Lage;->d(Landroid/content/Context;I)V

    .line 498
    invoke-direct {p0, p1, v3}, Lage;->c(Landroid/content/Context;I)V

    .line 499
    const/4 v0, 0x1

    const/16 v1, 0x48

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v1, v2}, Lage;->a(Landroid/content/Context;IILjava/lang/String;)V

    .line 500
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 501
    invoke-virtual {p0, v3}, Lage;->a(Z)V

    .line 504
    :cond_0
    return-void
.end method

.method public y(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 509
    const-string v0, "exceptionChangeDServer"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 510
    invoke-static {p1}, Lall;->b(Landroid/content/Context;)V

    .line 512
    iget-object v0, p0, Lage;->m:Landroid/content/Context;

    invoke-direct {p0, v0, v4}, Lage;->d(Landroid/content/Context;I)V

    .line 513
    invoke-direct {p0, p1, v4}, Lage;->c(Landroid/content/Context;I)V

    .line 514
    invoke-static {p1}, Lage;->B(Landroid/content/Context;)V

    .line 515
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    iget-object v1, p0, Lage;->m:Landroid/content/Context;

    const/16 v2, 0xa

    const/16 v3, 0x1bf

    invoke-virtual {v0, v1, v2, v3}, Lanf;->a(Landroid/content/Context;II)V

    .line 516
    invoke-virtual {p0, v4}, Lage;->a(Z)V

    .line 517
    return-void
.end method
