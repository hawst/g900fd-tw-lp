.class public final enum Lk;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lk;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lk;

.field public static final enum b:Lk;

.field public static final enum c:Lk;

.field public static final enum d:Lk;

.field private static final synthetic f:[Lk;


# instance fields
.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lk;

    const-string v1, "INVALID_REQUEST"

    const-string v2, "Invalid Ad request."

    invoke-direct {v0, v1, v3, v2}, Lk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lk;->a:Lk;

    new-instance v0, Lk;

    const-string v1, "NO_FILL"

    const-string v2, "Ad request successful, but no ad returned due to lack of ad inventory."

    invoke-direct {v0, v1, v4, v2}, Lk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lk;->b:Lk;

    new-instance v0, Lk;

    const-string v1, "NETWORK_ERROR"

    const-string v2, "A network error occurred."

    invoke-direct {v0, v1, v5, v2}, Lk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lk;->c:Lk;

    new-instance v0, Lk;

    const-string v1, "INTERNAL_ERROR"

    const-string v2, "There was an internal error."

    invoke-direct {v0, v1, v6, v2}, Lk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lk;->d:Lk;

    const/4 v0, 0x4

    new-array v0, v0, [Lk;

    sget-object v1, Lk;->a:Lk;

    aput-object v1, v0, v3

    sget-object v1, Lk;->b:Lk;

    aput-object v1, v0, v4

    sget-object v1, Lk;->c:Lk;

    aput-object v1, v0, v5

    sget-object v1, Lk;->d:Lk;

    aput-object v1, v0, v6

    sput-object v0, Lk;->f:[Lk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lk;->e:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lk;
    .locals 1

    const-class v0, Lk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lk;

    return-object v0
.end method

.method public static values()[Lk;
    .locals 1

    sget-object v0, Lk;->f:[Lk;

    invoke-virtual {v0}, [Lk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lk;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lk;->e:Ljava/lang/String;

    return-object v0
.end method
