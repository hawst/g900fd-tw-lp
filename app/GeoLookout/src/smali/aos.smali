.class Laos;
.super Ljava/lang/Object;
.source "SPPManager.java"

# interfaces
.implements Laou;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Laop;


# direct methods
.method constructor <init>(Laop;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Laos;->b:Laop;

    iput-object p2, p0, Laos;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Laot;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 125
    if-eqz p1, :cond_1

    .line 126
    :try_start_0
    invoke-virtual {p1}, Laot;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    const-string v0, "retry deregisterrmfoSPP Receive result: success in SPP requestID"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "retry spp id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Laot;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Laos;->a:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Laov;->a(Landroid/content/Context;Z)V

    .line 131
    iget-object v0, p0, Laos;->a:Landroid/content/Context;

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lage;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    invoke-static {}, Laoi;->a()Laoi;

    move-result-object v0

    invoke-virtual {v0, v2}, Laoi;->a(Laou;)V

    .line 143
    :goto_0
    return-void

    .line 134
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "retry Receive result: fail in SPP error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Laot;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Laos;->a:Landroid/content/Context;

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lage;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 141
    :goto_1
    invoke-static {}, Laoi;->a()Laoi;

    move-result-object v0

    invoke-virtual {v0, v2}, Laoi;->a(Laou;)V

    goto :goto_0

    .line 138
    :cond_1
    :try_start_2
    const-string v0, "retry onSPPResponse result null !!"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 141
    :catchall_0
    move-exception v0

    invoke-static {}, Laoi;->a()Laoi;

    move-result-object v1

    invoke-virtual {v1, v2}, Laoi;->a(Laou;)V

    throw v0
.end method
