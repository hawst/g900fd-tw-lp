.class Lakh;
.super Ljava/lang/Object;
.source "DisasterSettingsFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/CheckBox;

.field final synthetic b:Lakf;


# direct methods
.method constructor <init>(Lakf;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 593
    iput-object p1, p0, Lakh;->b:Lakf;

    iput-object p2, p0, Lakh;->a:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 595
    iget-object v0, p0, Lakh;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lakh;->b:Lakf;

    invoke-virtual {v0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "data_charging_Setting"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 598
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 599
    const-string v1, "data_charging_checkbox"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 600
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 604
    :cond_0
    iget-object v0, p0, Lakh;->b:Lakf;

    const-string v1, "com.sec.android.GeoLookout.SETTING_CHANGE_START"

    invoke-static {v0, v1, v2}, Lakf;->a(Lakf;Ljava/lang/String;Z)V

    .line 605
    iget-object v0, p0, Lakh;->b:Lakf;

    invoke-virtual {v0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "safetycare_geolookout_registering"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 607
    iget-object v0, p0, Lakh;->b:Lakf;

    invoke-static {v0}, Lakf;->a(Lakf;)Lakr;

    move-result-object v0

    invoke-virtual {v0, v3}, Lakr;->setEnabled(Z)V

    .line 608
    return-void
.end method
