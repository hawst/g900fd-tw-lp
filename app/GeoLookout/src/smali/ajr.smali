.class public Lajr;
.super Landroid/content/BroadcastReceiver;
.source "DisasterMultiPointActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)V
    .locals 0

    .prologue
    .line 614
    iput-object p1, p0, Lajr;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 619
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MULTIPOINT onReceive "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 620
    const-string v0, "com.sec.android.geolookout.querylocation"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 621
    const-string v0, "query_result"

    invoke-virtual {p2, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 622
    iget-object v0, p0, Lajr;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0, v7}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->b(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Z)Z

    .line 624
    const-string v0, "latlong"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getDoubleArrayExtra(Ljava/lang/String;)[D

    move-result-object v0

    .line 625
    if-eqz v0, :cond_2

    .line 626
    iget-object v1, p0, Lajr;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->i(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 627
    new-instance v1, Lajs;

    iget-object v2, p0, Lajr;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    iget-object v3, p0, Lajr;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-virtual {v3}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lajs;-><init>(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Landroid/content/Context;)V

    .line 629
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Double;

    aget-wide v4, v0, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v6

    aget-wide v4, v0, v7

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v2, v7

    invoke-virtual {v1, v2}, Lajs;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 636
    :goto_0
    iget-object v0, p0, Lajr;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0, v6}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->c(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Z)Z

    .line 637
    iget-object v0, p0, Lajr;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    const-string v1, "query_zone"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->b(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 638
    iget-object v0, p0, Lajr;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    const-string v1, "query_county"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->c(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 646
    :goto_1
    iget-object v0, p0, Lajr;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    iget-object v1, p0, Lajr;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->j(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 649
    :cond_0
    return-void

    .line 631
    :cond_1
    iget-object v0, p0, Lajr;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->invalidateOptionsMenu()V

    goto :goto_0

    .line 634
    :cond_2
    iget-object v0, p0, Lajr;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->invalidateOptionsMenu()V

    goto :goto_0

    .line 640
    :cond_3
    iget-object v0, p0, Lajr;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    const v1, 0x7f0a001d

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 642
    iget-object v0, p0, Lajr;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->b(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 643
    iget-object v0, p0, Lajr;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->c(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1
.end method
