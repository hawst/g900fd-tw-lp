.class public final Ljy;
.super Lla;

# interfaces
.implements Lke;
.implements Llq;


# instance fields
.field private final a:Lhq;

.field private final b:Ljx;

.field private final c:Lcom/google/android/gms/internal/cq;

.field private final d:Landroid/content/Context;

.field private final e:Ljava/lang/Object;

.field private final f:Lkj;

.field private final g:Lue;

.field private h:Lla;

.field private i:Lcom/google/android/gms/internal/bw;

.field private j:Z

.field private k:Lhe;

.field private l:Lhh;

.field private m:Lhm;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkj;Lue;Lcom/google/android/gms/internal/cq;Lhq;Ljx;)V
    .locals 1

    invoke-direct {p0}, Lla;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ljy;->e:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Ljy;->j:Z

    iput-object p5, p0, Ljy;->a:Lhq;

    iput-object p6, p0, Ljy;->b:Ljx;

    iput-object p4, p0, Ljy;->c:Lcom/google/android/gms/internal/cq;

    iput-object p1, p0, Ljy;->d:Landroid/content/Context;

    iput-object p2, p0, Ljy;->f:Lkj;

    iput-object p3, p0, Ljy;->g:Lue;

    return-void
.end method

.method static synthetic a(Ljy;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Ljy;->e:Ljava/lang/Object;

    return-object v0
.end method

.method private a(J)V
    .locals 3

    sget-object v0, Lll;->a:Landroid/os/Handler;

    new-instance v1, Lkb;

    invoke-direct {v1, p0}, Lkb;-><init>(Ljy;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-direct {p0, p1, p2}, Ljy;->c(J)V

    return-void
.end method

.method private a(Lcom/google/android/gms/internal/bu;J)V
    .locals 4

    new-instance v0, Lhe;

    iget-object v1, p0, Ljy;->d:Landroid/content/Context;

    iget-object v2, p0, Ljy;->a:Lhq;

    iget-object v3, p0, Ljy;->l:Lhh;

    invoke-direct {v0, v1, p1, v2, v3}, Lhe;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/bu;Lhq;Lhh;)V

    iput-object v0, p0, Ljy;->k:Lhe;

    iget-object v0, p0, Ljy;->k:Lhe;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, p2, p3, v2, v3}, Lhe;->a(JJ)Lhm;

    move-result-object v0

    iput-object v0, p0, Ljy;->m:Lhm;

    iget-object v0, p0, Ljy;->m:Lhm;

    iget v0, v0, Lhm;->a:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lkc;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected mediation result: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ljy;->m:Lhm;

    iget v2, v2, Lhm;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lkc;-><init>(Ljava/lang/String;I)V

    throw v0

    :pswitch_0
    new-instance v0, Lkc;

    const-string v1, "No fill from any mediation ad networks."

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lkc;-><init>(Ljava/lang/String;I)V

    throw v0

    :pswitch_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Ljy;)Ljx;
    .locals 1

    iget-object v0, p0, Ljy;->b:Ljx;

    return-object v0
.end method

.method private b(J)V
    .locals 3

    :cond_0
    invoke-direct {p0, p1, p2}, Ljy;->d(J)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lkc;

    const-string v1, "Timed out waiting for ad response."

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lkc;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_1
    iget-object v0, p0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Ljy;->h:Lla;

    iget-object v0, p0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget v0, v0, Lcom/google/android/gms/internal/bw;->f:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget v0, v0, Lcom/google/android/gms/internal/bw;->f:I

    const/4 v1, -0x3

    if-eq v0, v1, :cond_2

    new-instance v0, Lkc;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "There was a problem getting an ad response. ErrorCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget v2, v2, Lcom/google/android/gms/internal/bw;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget v2, v2, Lcom/google/android/gms/internal/bw;->f:I

    invoke-direct {v0, v1, v2}, Lkc;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_2
    return-void
.end method

.method static synthetic c(Ljy;)Lcom/google/android/gms/internal/bw;
    .locals 1

    iget-object v0, p0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    return-object v0
.end method

.method private c()V
    .locals 3

    iget-object v0, p0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget v0, v0, Lcom/google/android/gms/internal/bw;->f:I

    const/4 v1, -0x3

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget-object v0, v0, Lcom/google/android/gms/internal/bw;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lkc;

    const-string v1, "No fill from ad server."

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lkc;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_2
    iget-object v0, p0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/bw;->j:Z

    if-eqz v0, :cond_0

    :try_start_0
    new-instance v0, Lhh;

    iget-object v1, p0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget-object v1, v1, Lcom/google/android/gms/internal/bw;->d:Ljava/lang/String;

    invoke-direct {v0, v1}, Lhh;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ljy;->l:Lhh;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Lkc;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not parse mediation config: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget-object v2, v2, Lcom/google/android/gms/internal/bw;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lkc;-><init>(Ljava/lang/String;I)V

    throw v0
.end method

.method private c(J)V
    .locals 3

    :cond_0
    invoke-direct {p0, p1, p2}, Ljy;->d(J)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lkc;

    const-string v1, "Timed out waiting for WebView to finish loading."

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lkc;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_1
    iget-boolean v0, p0, Ljy;->j:Z

    if-eqz v0, :cond_0

    return-void
.end method

.method static synthetic d(Ljy;)Lcom/google/android/gms/internal/cq;
    .locals 1

    iget-object v0, p0, Ljy;->c:Lcom/google/android/gms/internal/cq;

    return-object v0
.end method

.method private d(J)Z
    .locals 5

    const-wide/32 v0, 0xea60

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, p1

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    :try_start_0
    iget-object v2, p0, Ljy;->e:Ljava/lang/Object;

    invoke-virtual {v2, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Lkc;

    const-string v1, "Ad request cancelled."

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lkc;-><init>(Ljava/lang/String;I)V

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 25

    move-object/from16 v0, p0

    iget-object v0, v0, Ljy;->e:Ljava/lang/Object;

    move-object/from16 v24, v0

    monitor-enter v24

    :try_start_0
    const-string v2, "AdLoaderBackgroundTask started."

    invoke-static {v2}, Llm;->a(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Ljy;->g:Lue;

    invoke-virtual {v2}, Lue;->a()Lmw;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Ljy;->d:Landroid/content/Context;

    invoke-interface {v2, v3}, Lmw;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    new-instance v13, Lcom/google/android/gms/internal/bu;

    move-object/from16 v0, p0

    iget-object v3, v0, Ljy;->f:Lkj;

    invoke-direct {v13, v3, v2}, Lcom/google/android/gms/internal/bu;-><init>(Lkj;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v7, -0x2

    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v4, v0, Ljy;->d:Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-static {v4, v13, v0}, Lkd;->a(Landroid/content/Context;Lcom/google/android/gms/internal/bu;Lke;)Lla;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Ljy;->h:Lla;

    move-object/from16 v0, p0

    iget-object v4, v0, Ljy;->h:Lla;

    if-nez v4, :cond_1

    new-instance v2, Lkc;

    const-string v3, "Could not start the ad request service."

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lkc;-><init>(Ljava/lang/String;I)V

    throw v2
    :try_end_1
    .catch Lkc; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    move-exception v2

    :try_start_2
    invoke-virtual {v2}, Lkc;->a()I

    move-result v7

    const/4 v3, 0x3

    if-eq v7, v3, :cond_0

    const/4 v3, -0x1

    if-ne v7, v3, :cond_3

    :cond_0
    invoke-virtual {v2}, Lkc;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Llm;->c(Ljava/lang/String;)V

    :goto_0
    new-instance v2, Lcom/google/android/gms/internal/bw;

    invoke-direct {v2, v7}, Lcom/google/android/gms/internal/bw;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    sget-object v2, Lll;->a:Landroid/os/Handler;

    new-instance v3, Ljz;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Ljz;-><init>(Ljy;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_1
    new-instance v3, Lky;

    iget-object v4, v13, Lcom/google/android/gms/internal/bu;->d:Lcom/google/android/gms/internal/v;

    move-object/from16 v0, p0

    iget-object v5, v0, Ljy;->c:Lcom/google/android/gms/internal/cq;

    move-object/from16 v0, p0

    iget-object v2, v0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget-object v6, v2, Lcom/google/android/gms/internal/bw;->e:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget-object v8, v2, Lcom/google/android/gms/internal/bw;->g:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget-object v9, v2, Lcom/google/android/gms/internal/bw;->l:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget v10, v2, Lcom/google/android/gms/internal/bw;->n:I

    move-object/from16 v0, p0

    iget-object v2, v0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget-wide v11, v2, Lcom/google/android/gms/internal/bw;->m:J

    iget-object v13, v13, Lcom/google/android/gms/internal/bu;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget-boolean v14, v2, Lcom/google/android/gms/internal/bw;->j:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Ljy;->m:Lhm;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Ljy;->m:Lhm;

    iget-object v15, v2, Lhm;->b:Lhg;

    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Ljy;->m:Lhm;

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Ljy;->m:Lhm;

    iget-object v0, v2, Lhm;->c:Lht;

    move-object/from16 v16, v0

    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Ljy;->m:Lhm;

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Ljy;->m:Lhm;

    iget-object v0, v2, Lhm;->d:Ljava/lang/String;

    move-object/from16 v17, v0

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Ljy;->l:Lhh;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Ljy;->m:Lhm;

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Ljy;->m:Lhm;

    iget-object v0, v2, Lhm;->e:Lhj;

    move-object/from16 v19, v0

    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget-wide v0, v2, Lcom/google/android/gms/internal/bw;->k:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget-wide v0, v2, Lcom/google/android/gms/internal/bw;->i:J

    move-wide/from16 v22, v0

    invoke-direct/range {v3 .. v23}, Lky;-><init>(Lcom/google/android/gms/internal/v;Lcom/google/android/gms/internal/cq;Ljava/util/List;ILjava/util/List;Ljava/util/List;IJLjava/lang/String;ZLhg;Lht;Ljava/lang/String;Lhh;Lhj;JJ)V

    sget-object v2, Lll;->a:Landroid/os/Handler;

    new-instance v4, Lka;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v3}, Lka;-><init>(Ljy;Lky;)V

    invoke-virtual {v2, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    monitor-exit v24
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    :cond_1
    :try_start_3
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Ljy;->b(J)V

    invoke-direct/range {p0 .. p0}, Ljy;->c()V

    move-object/from16 v0, p0

    iget-object v4, v0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget-boolean v4, v4, Lcom/google/android/gms/internal/bw;->j:Z

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v2, v3}, Ljy;->a(Lcom/google/android/gms/internal/bu;J)V
    :try_end_3
    .catch Lkc; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    :catchall_0
    move-exception v2

    :try_start_4
    monitor-exit v24
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v2

    :cond_2
    :try_start_5
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Ljy;->a(J)V
    :try_end_5
    .catch Lkc; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    :cond_3
    :try_start_6
    invoke-virtual {v2}, Lkc;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Llm;->e(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    :cond_4
    const/4 v15, 0x0

    goto :goto_2

    :cond_5
    const/16 v16, 0x0

    goto :goto_3

    :cond_6
    const/16 v17, 0x0

    goto :goto_4

    :cond_7
    const/16 v19, 0x0

    goto :goto_5
.end method

.method public a(Lcom/google/android/gms/internal/bw;)V
    .locals 2

    iget-object v1, p0, Ljy;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    const-string v0, "Received ad response."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    iput-object p1, p0, Ljy;->i:Lcom/google/android/gms/internal/bw;

    iget-object v0, p0, Ljy;->e:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/gms/internal/cq;)V
    .locals 2

    iget-object v1, p0, Ljy;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    const-string v0, "WebView finished loading."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljy;->j:Z

    iget-object v0, p0, Ljy;->e:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 2

    iget-object v1, p0, Ljy;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ljy;->h:Lla;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljy;->h:Lla;

    invoke-virtual {v0}, Lla;->f()V

    :cond_0
    iget-object v0, p0, Ljy;->c:Lcom/google/android/gms/internal/cq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cq;->stopLoading()V

    iget-object v0, p0, Ljy;->c:Lcom/google/android/gms/internal/cq;

    invoke-static {v0}, Llf;->a(Landroid/webkit/WebView;)V

    iget-object v0, p0, Ljy;->k:Lhe;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljy;->k:Lhe;

    invoke-virtual {v0}, Lhe;->a()V

    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
