.class public final Ly;
.super Ljava/lang/Object;

# interfaces
.implements Lq;
.implements Ls;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lq",
        "<",
        "Lbf;",
        "Lab;",
        ">;",
        "Ls",
        "<",
        "Lbf;",
        "Lab;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/google/android/gms/ads/AdView;

.field private b:Lba;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Lo;Lbf;Lab;)Law;
    .locals 5

    const/4 v1, 0x1

    new-instance v2, Lay;

    invoke-direct {v2}, Lay;-><init>()V

    invoke-virtual {p1}, Lo;->b()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v2, v0}, Lay;->a(Ljava/util/Date;)Lay;

    :cond_0
    invoke-virtual {p1}, Lo;->c()Ll;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Ljb;->a(Ll;)I

    move-result v0

    invoke-virtual {v2, v0}, Lay;->a(I)Lay;

    :cond_1
    invoke-virtual {p1}, Lo;->d()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lay;->a(Ljava/lang/String;)Lay;

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lo;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lll;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lay;->b(Ljava/lang/String;)Lay;

    :cond_3
    iget v0, p3, Lab;->c:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_4

    iget v0, p3, Lab;->c:I

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Lay;->a(Z)Lay;

    :cond_4
    if-eqz p2, :cond_6

    :goto_2
    invoke-virtual {p2}, Lbf;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "gw"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "mad_hac"

    iget-object v4, p3, Lab;->b:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "_noRefresh"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v2, p2}, Lay;->a(Lbe;)Lay;

    invoke-virtual {v2}, Lay;->a()Law;

    move-result-object v0

    return-object v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    new-instance p2, Lbf;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-direct {p2, v0}, Lbf;-><init>(Landroid/os/Bundle;)V

    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Ly;->a:Lcom/google/android/gms/ads/AdView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ly;->a:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->a()V

    iput-object v1, p0, Ly;->a:Lcom/google/android/gms/ads/AdView;

    :cond_0
    iget-object v0, p0, Ly;->b:Lba;

    if-eqz v0, :cond_1

    iput-object v1, p0, Ly;->b:Lba;

    :cond_1
    return-void
.end method

.method public a(Lr;Landroid/app/Activity;Lab;Lm;Lo;Lbf;)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/ads/AdView;

    invoke-direct {v0, p2}, Lcom/google/android/gms/ads/AdView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ly;->a:Lcom/google/android/gms/ads/AdView;

    iget-object v0, p0, Ly;->a:Lcom/google/android/gms/ads/AdView;

    new-instance v1, Laz;

    invoke-virtual {p4}, Lm;->a()I

    move-result v2

    invoke-virtual {p4}, Lm;->b()I

    move-result v3

    invoke-direct {v1, v2, v3}, Laz;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/AdView;->setAdSize(Laz;)V

    iget-object v0, p0, Ly;->a:Lcom/google/android/gms/ads/AdView;

    iget-object v1, p3, Lab;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/AdView;->setAdUnitId(Ljava/lang/String;)V

    iget-object v0, p0, Ly;->a:Lcom/google/android/gms/ads/AdView;

    new-instance v1, Lz;

    invoke-direct {v1, p0, p1}, Lz;-><init>(Ly;Lr;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/AdView;->setAdListener(Lav;)V

    iget-object v0, p0, Ly;->a:Lcom/google/android/gms/ads/AdView;

    invoke-static {p2, p5, p6, p3}, Ly;->a(Landroid/content/Context;Lo;Lbf;Lab;)Law;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/AdView;->a(Law;)V

    return-void
.end method

.method public bridge synthetic a(Lr;Landroid/app/Activity;Lu;Lm;Lo;Lx;)V
    .locals 7

    move-object v3, p3

    check-cast v3, Lab;

    move-object v6, p6

    check-cast v6, Lbf;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Ly;->a(Lr;Landroid/app/Activity;Lab;Lm;Lo;Lbf;)V

    return-void
.end method

.method public a(Lt;Landroid/app/Activity;Lab;Lo;Lbf;)V
    .locals 2

    new-instance v0, Lba;

    invoke-direct {v0, p2}, Lba;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ly;->b:Lba;

    iget-object v0, p0, Ly;->b:Lba;

    iget-object v1, p3, Lab;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lba;->a(Ljava/lang/String;)V

    iget-object v0, p0, Ly;->b:Lba;

    new-instance v1, Laa;

    invoke-direct {v1, p0, p1}, Laa;-><init>(Ly;Lt;)V

    invoke-virtual {v0, v1}, Lba;->a(Lav;)V

    iget-object v0, p0, Ly;->b:Lba;

    invoke-static {p2, p4, p5, p3}, Ly;->a(Landroid/content/Context;Lo;Lbf;Lab;)Law;

    move-result-object v1

    invoke-virtual {v0, v1}, Lba;->a(Law;)V

    return-void
.end method

.method public bridge synthetic a(Lt;Landroid/app/Activity;Lu;Lo;Lx;)V
    .locals 6

    move-object v3, p3

    check-cast v3, Lab;

    move-object v5, p5

    check-cast v5, Lbf;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Ly;->a(Lt;Landroid/app/Activity;Lab;Lo;Lbf;)V

    return-void
.end method

.method public b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lbf;",
            ">;"
        }
    .end annotation

    const-class v0, Lbf;

    return-object v0
.end method

.method public c()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lab;",
            ">;"
        }
    .end annotation

    const-class v0, Lab;

    return-object v0
.end method

.method public d()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Ly;->a:Lcom/google/android/gms/ads/AdView;

    return-object v0
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Ly;->b:Lba;

    invoke-virtual {v0}, Lba;->d()V

    return-void
.end method
