.class Lanv;
.super Lcom/loopj/android/http/AsyncHttpResponseHandler;
.source "DisasterServerInterface.java"


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lanh;


# direct methods
.method constructor <init>(Lanh;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 522
    iput-object p1, p0, Lanv;->d:Lanh;

    iput p2, p0, Lanv;->a:I

    iput-object p3, p0, Lanv;->b:Ljava/lang/String;

    iput-object p4, p0, Lanv;->c:Ljava/lang/String;

    invoke-direct {p0}, Lcom/loopj/android/http/AsyncHttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(ILjava/lang/Throwable;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 563
    invoke-super {p0, p1, p2, p3}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onFailure(ILjava/lang/Throwable;Ljava/lang/String;)V

    .line 564
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updateLocationToDisasterServer: Failure "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 565
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updateLocationToDisasterServer: content: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 566
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 567
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->e(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 568
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->e(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 569
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lanv;->d:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lage;->e(Landroid/content/Context;Z)V

    .line 571
    :cond_0
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 572
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lanv;->d:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lage;->a(Landroid/content/Context;I)V

    .line 574
    :cond_1
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 575
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lanv;->d:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lage;->b(Landroid/content/Context;I)V

    .line 578
    :cond_2
    sget-boolean v0, Laky;->c:Z

    if-ne v0, v2, :cond_3

    const/16 v0, 0x1bf

    if-ne p1, v0, :cond_3

    .line 579
    const/4 v0, 0x0

    .line 581
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 582
    const-string v2, "addr"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 583
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "newServerURL = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->f(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 587
    :goto_0
    invoke-static {}, Laoh;->a()Laoh;

    move-result-object v1

    iget-object v2, p0, Lanv;->d:Lanh;

    invoke-static {v2}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Laoh;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 588
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lanv;->d:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lage;->y(Landroid/content/Context;)V

    .line 592
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.geolookout.resultlocation"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 593
    const-string v1, "update_result"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 594
    iget-object v1, p0, Lanv;->d:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 596
    return-void

    .line 584
    :catch_0
    move-exception v1

    .line 585
    const-string v1, "JSONException"

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onFinish()V
    .locals 1

    .prologue
    .line 600
    const-string v0, "updateLocationToDisasterServer: onFinish()  "

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 602
    return-void
.end method

.method public onSuccess(I[Lorg/apache/http/Header;[B)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 525
    invoke-super {p0, p1, p2, p3}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onSuccess(I[Lorg/apache/http/Header;[B)V

    .line 526
    const-string v0, "updateLocationToDisasterServer: responseHandler : onSuccess "

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 527
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    if-ne v0, v4, :cond_1

    .line 528
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->e(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 529
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->e(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 530
    sget-boolean v0, Laky;->k:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lane;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 531
    const-string v0, "setAutoRefreshAlarm onSuccess "

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 532
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lall;->t(Landroid/content/Context;)V

    .line 533
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lall;->s(Landroid/content/Context;)V

    .line 535
    :cond_0
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lanv;->d:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lage;->q(Landroid/content/Context;)V

    .line 536
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0, v4}, Lage;->b(Z)V

    .line 537
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0, v4}, Lage;->c(Z)V

    .line 540
    :cond_1
    iget v0, p0, Lanv;->a:I

    if-nez v0, :cond_4

    .line 541
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lanv;->b:Ljava/lang/String;

    iget-object v2, p0, Lanv;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lall;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lall;->b(Landroid/content/Context;J)V

    .line 547
    :goto_0
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 548
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    iget-object v1, p0, Lanv;->d:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lalu;->a(Landroid/content/Context;I)V

    .line 550
    :cond_2
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 551
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    iget-object v1, p0, Lanv;->d:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lanv;->b:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lalu;->a(Landroid/content/Context;Ljava/lang/String;Lany;)V

    .line 555
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.geolookout.resultlocation"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 556
    const-string v1, "update_result"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 557
    iget-object v1, p0, Lanv;->d:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 558
    return-void

    .line 544
    :cond_4
    iget-object v0, p0, Lanv;->d:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lanv;->b:Ljava/lang/String;

    iget-object v2, p0, Lanv;->c:Ljava/lang/String;

    iget v3, p0, Lanv;->a:I

    invoke-static {v0, v1, v2, v3}, Lalz;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method
