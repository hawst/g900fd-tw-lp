.class Lann;
.super Lcom/loopj/android/http/AsyncHttpResponseHandler;
.source "DisasterServerInterface.java"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lany;

.field final synthetic c:Lanh;


# direct methods
.method constructor <init>(Lanh;Ljava/lang/String;Lany;)V
    .locals 0

    .prologue
    .line 1015
    iput-object p1, p0, Lann;->c:Lanh;

    iput-object p2, p0, Lann;->a:Ljava/lang/String;

    iput-object p3, p0, Lann;->b:Lany;

    invoke-direct {p0}, Lcom/loopj/android/http/AsyncHttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1026
    invoke-super {p0, p1, p2, p3, p4}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onFailure(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V

    .line 1027
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "requestLifeInformation: responseHandler : onFailure "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 1028
    iget-object v0, p0, Lann;->b:Lany;

    if-eqz v0, :cond_0

    .line 1029
    iget-object v0, p0, Lann;->b:Lany;

    invoke-interface {v0}, Lany;->b()V

    .line 1031
    :cond_0
    iget-object v0, p0, Lann;->c:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1032
    iget-object v0, p0, Lann;->c:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lann;->c:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lage;->b(Landroid/content/Context;I)V

    .line 1034
    :cond_1
    return-void
.end method

.method public onFinish()V
    .locals 2

    .prologue
    .line 1038
    const-string v0, "requestLifeInformation: onFinish()  "

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 1039
    iget-object v0, p0, Lann;->c:Lanh;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lanh;->a(Lanh;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 1040
    return-void
.end method

.method public onRetry()V
    .locals 1

    .prologue
    .line 1044
    invoke-super {p0}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onRetry()V

    .line 1045
    const-string v0, "requestLifeInformation: responseHandler : onRetry"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1046
    return-void
.end method

.method public onSuccess(I[Lorg/apache/http/Header;[B)V
    .locals 5

    .prologue
    .line 1018
    invoke-super {p0, p1, p2, p3}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onSuccess(I[Lorg/apache/http/Header;[B)V

    .line 1019
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p3}, Ljava/lang/String;-><init>([B)V

    .line 1020
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestLifeInformation: responseHandler : onSuccess "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 1021
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v1

    iget-object v2, p0, Lann;->c:Lanh;

    invoke-static {v2}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lann;->a:Ljava/lang/String;

    iget-object v4, p0, Lann;->b:Lany;

    invoke-virtual {v1, v2, v0, v3, v4}, Lalu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lany;)V

    .line 1022
    return-void
.end method
