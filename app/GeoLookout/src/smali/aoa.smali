.class public Laoa;
.super Ljava/lang/Object;
.source "SPPClientInterface.java"


# static fields
.field private static a:Laqv;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Z

.field private d:Z

.field private e:Laob;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput-object v0, Laoa;->a:Laqv;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Laoa;->b:Landroid/content/Context;

    .line 29
    iput-boolean v1, p0, Laoa;->c:Z

    .line 30
    iput-boolean v1, p0, Laoa;->d:Z

    .line 33
    iput-object p1, p0, Laoa;->b:Landroid/content/Context;

    .line 34
    return-void
.end method

.method static synthetic a(Laqv;)Laqv;
    .locals 0

    .prologue
    .line 25
    sput-object p0, Laoa;->a:Laqv;

    return-object p0
.end method

.method static synthetic a(Laoa;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Laoa;->c:Z

    return v0
.end method

.method static synthetic a(Laoa;Z)Z
    .locals 0

    .prologue
    .line 25
    iput-boolean p1, p0, Laoa;->d:Z

    return p1
.end method

.method static synthetic b(Laoa;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Laoa;->d:Z

    return v0
.end method

.method static synthetic b(Laoa;Z)Z
    .locals 0

    .prologue
    .line 25
    iput-boolean p1, p0, Laoa;->c:Z

    return p1
.end method

.method static synthetic f()Laqv;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Laoa;->a:Laqv;

    return-object v0
.end method

.method private g()V
    .locals 6

    .prologue
    .line 70
    monitor-enter p0

    .line 71
    :try_start_0
    invoke-direct {p0}, Laoa;->h()V

    .line 72
    const-string v0, "wait startPushService.... "

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 73
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 75
    :try_start_1
    iget-object v2, p0, Laoa;->e:Laob;

    invoke-static {v2}, Laob;->a(Laob;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v2

    const-wide/16 v4, 0x1388

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 78
    :goto_0
    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "started! elapsed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v0, v4, v0

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 79
    monitor-exit p0

    .line 80
    return-void

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 76
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private h()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Laoa;->j()V

    .line 84
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 91
    monitor-enter p0

    .line 92
    :try_start_0
    invoke-direct {p0}, Laoa;->k()V

    .line 93
    monitor-exit p0

    .line 94
    return-void

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private j()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bindPushService bounded:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Laoa;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " connected:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Laoa;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 100
    sget-object v0, Laoa;->a:Laqv;

    if-eqz v0, :cond_0

    .line 101
    const-string v0, "already bounded!"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 129
    :goto_0
    return-void

    .line 105
    :cond_0
    new-instance v0, Laob;

    invoke-direct {v0, p0}, Laob;-><init>(Laoa;)V

    iput-object v0, p0, Laoa;->e:Laob;

    .line 108
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 109
    iget-object v1, p0, Laoa;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 110
    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 113
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 114
    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v1, v1, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    .line 115
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    .line 116
    new-instance v2, Landroid/content/ComponentName;

    invoke-direct {v2, v1, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 118
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 119
    const-string v1, "com.sec.spp.push.PUSH_CLIENT_SERVICE_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 121
    iget-object v1, p0, Laoa;->b:Landroid/content/Context;

    iget-object v2, p0, Laoa;->e:Laob;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    iput-boolean v3, p0, Laoa;->c:Z

    goto :goto_0

    .line 124
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bindService failed. bounded:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Laoa;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " connected:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Laoa;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 127
    :cond_2
    const-string v0, "bindService failed. No implicit intent."

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unbindPushService bounded:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Laoa;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " connected:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Laoa;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 133
    sget-object v0, Laoa;->a:Laqv;

    if-eqz v0, :cond_0

    .line 134
    const-string v0, "unbindPushService(mPushClientConnection) requested."

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Laoa;->b:Landroid/content/Context;

    iget-object v1, p0, Laoa;->e:Laob;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Laoa;->e:Laob;

    .line 138
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Laoa;->g()V

    .line 67
    return-void
.end method

.method public a(ZDD)Z
    .locals 6

    .prologue
    .line 188
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updateLocationToSPPServer() "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Laoa;->b:Landroid/content/Context;

    invoke-static {v0}, Laof;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 191
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 192
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.sec.spp.push"

    const-string v4, "com.sec.spp.push.receiver.LdUpdateReceiver"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 193
    const-string v2, "com.sec.spp.push.ldUpdate"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    const-string v2, "package"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 195
    const-string v0, "update"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 197
    if-eqz p1, :cond_0

    .line 198
    invoke-virtual {p0}, Laoa;->c()Ljava/lang/String;

    move-result-object v0

    .line 199
    invoke-static {p2, p3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Laon;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 200
    invoke-static {p4, p5}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Laon;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 201
    iget-object v4, p0, Laoa;->b:Landroid/content/Context;

    invoke-static {v4, v0}, Laon;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 203
    const-string v4, "data1"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 204
    const-string v2, "data2"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 205
    const-string v2, "data3"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 207
    :cond_0
    iget-object v0, p0, Laoa;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 209
    iget-object v0, p0, Laoa;->b:Landroid/content/Context;

    new-instance v1, Lamf;

    invoke-direct {v1, p2, p3, p4, p5}, Lamf;-><init>(DD)V

    invoke-static {v0, v1}, Lanb;->a(Landroid/content/Context;Lamf;)V

    .line 210
    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Laoa;->i()V

    .line 88
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 141
    const-string v0, "getRegId()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 142
    invoke-virtual {p0}, Laoa;->a()V

    .line 144
    sget-object v0, Laoa;->a:Laqv;

    if-eqz v0, :cond_0

    .line 146
    :try_start_0
    iget-object v0, p0, Laoa;->b:Landroid/content/Context;

    invoke-static {v0}, Laof;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 147
    sget-object v1, Laoa;->a:Laqv;

    invoke-interface {v1, v0}, Laqv;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 153
    :goto_0
    return-object v0

    .line 148
    :catch_0
    move-exception v0

    .line 149
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mService.getRegId() failed - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lalj;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 153
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 152
    :cond_0
    const-string v0, "IPushClientService mService is null"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public d()Z
    .locals 3

    .prologue
    .line 157
    const-string v0, "isPushAvailable()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 158
    invoke-virtual {p0}, Laoa;->a()V

    .line 160
    sget-object v0, Laoa;->a:Laqv;

    if-eqz v0, :cond_0

    .line 162
    :try_start_0
    sget-object v0, Laoa;->a:Laqv;

    invoke-interface {v0}, Laqv;->a()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 169
    :goto_0
    return v0

    .line 163
    :catch_0
    move-exception v0

    .line 164
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isPushAvailable() failed - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lalj;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 169
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 167
    :cond_0
    const-string v0, "IPushClientService mService is null"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public e()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 173
    const-string v0, "getRegId()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 174
    sget-object v0, Laoa;->a:Laqv;

    if-eqz v0, :cond_0

    .line 176
    :try_start_0
    sget-object v0, Laoa;->a:Laqv;

    invoke-interface {v0}, Laqv;->b()[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 184
    :goto_0
    return-object v0

    .line 178
    :catch_0
    move-exception v0

    .line 179
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " getRegisteredAppIDs() failed - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lalj;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 184
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 182
    :cond_0
    const-string v0, "IPushClientService mService is null"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_1
.end method
