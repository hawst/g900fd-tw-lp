.class public Laop;
.super Ljava/lang/Object;
.source "SPPManager.java"


# static fields
.field private static volatile a:Laop;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/4 v0, 0x0

    sput-object v0, Laop;->a:Laop;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method

.method public static a()Laop;
    .locals 2

    .prologue
    .line 20
    sget-object v0, Laop;->a:Laop;

    if-nez v0, :cond_1

    .line 21
    const-class v1, Laop;

    monitor-enter v1

    .line 22
    :try_start_0
    sget-object v0, Laop;->a:Laop;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Laop;

    invoke-direct {v0}, Laop;-><init>()V

    sput-object v0, Laop;->a:Laop;

    .line 25
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    :cond_1
    sget-object v0, Laop;->a:Laop;

    return-object v0

    .line 25
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private d(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 45
    invoke-static {}, Laoi;->a()Laoi;

    move-result-object v0

    .line 47
    invoke-virtual {v0, p1}, Laoi;->f(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 48
    const-string v0, "SPP is NOT available"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 49
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lage;->b(Landroid/content/Context;Z)V

    .line 79
    :goto_0
    return-void

    .line 53
    :cond_0
    new-instance v1, Laoq;

    invoke-direct {v1, p0, p1}, Laoq;-><init>(Laop;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Laoi;->a(Laou;)V

    .line 77
    invoke-virtual {v0, p1}, Laoi;->c(Landroid/content/Context;)V

    .line 78
    invoke-virtual {v0, p1}, Laoi;->a(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private e(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 85
    invoke-static {}, Laoi;->a()Laoi;

    move-result-object v0

    .line 86
    new-instance v1, Laor;

    invoke-direct {v1, p0, p1}, Laor;-><init>(Laop;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Laoi;->a(Laou;)V

    .line 111
    invoke-virtual {v0, p1}, Laoi;->d(Landroid/content/Context;)V

    .line 112
    invoke-virtual {v0, p1}, Laoi;->b(Landroid/content/Context;)V

    .line 113
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    const-string v0, "registerSPPClient"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 32
    invoke-direct {p0, p1}, Laop;->d(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method public b(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 36
    const-string v0, "deregisterSPPClient"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 37
    invoke-direct {p0, p1}, Laop;->e(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method public c(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 119
    invoke-static {}, Laoi;->a()Laoi;

    move-result-object v0

    .line 120
    new-instance v1, Laos;

    invoke-direct {v1, p0, p1}, Laos;-><init>(Laop;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Laoi;->a(Laou;)V

    .line 146
    invoke-virtual {v0, p1}, Laoi;->e(Landroid/content/Context;)V

    .line 147
    return-void
.end method
