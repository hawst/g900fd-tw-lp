.class public Lwx;
.super Ldh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldh",
        "<",
        "Lww;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:Ldt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldt",
            "<",
            "Lww;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/app/Fragment;

.field private c:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Fragment;)V
    .locals 0

    invoke-direct {p0}, Ldh;-><init>()V

    iput-object p1, p0, Lwx;->b:Landroid/app/Fragment;

    return-void
.end method

.method private a(Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lwx;->c:Landroid/app/Activity;

    invoke-virtual {p0}, Lwx;->g()V

    return-void
.end method

.method public static synthetic a(Lwx;Landroid/app/Activity;)V
    .locals 0

    invoke-direct {p0, p1}, Lwx;->a(Landroid/app/Activity;)V

    return-void
.end method


# virtual methods
.method protected a(Ldt;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldt",
            "<",
            "Lww;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lwx;->a:Ldt;

    invoke-virtual {p0}, Lwx;->g()V

    return-void
.end method

.method public g()V
    .locals 4

    iget-object v0, p0, Lwx;->c:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lwx;->a:Ldt;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lwx;->a()Ldg;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lwx;->c:Landroid/app/Activity;

    invoke-static {v0}, Lxa;->a(Landroid/content/Context;)V

    iget-object v0, p0, Lwx;->c:Landroid/app/Activity;

    invoke-static {v0}, Lzu;->a(Landroid/content/Context;)Lyg;

    move-result-object v0

    iget-object v1, p0, Lwx;->c:Landroid/app/Activity;

    invoke-static {v1}, Lds;->a(Ljava/lang/Object;)Ldp;

    move-result-object v1

    invoke-interface {v0, v1}, Lyg;->b(Ldp;)Lxq;

    move-result-object v0

    iget-object v1, p0, Lwx;->a:Ldt;

    new-instance v2, Lww;

    iget-object v3, p0, Lwx;->b:Landroid/app/Fragment;

    invoke-direct {v2, v3, v0}, Lww;-><init>(Landroid/app/Fragment;Lxq;)V

    invoke-interface {v1, v2}, Ldt;->a(Ldg;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcd; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :catch_1
    move-exception v0

    goto :goto_0
.end method
