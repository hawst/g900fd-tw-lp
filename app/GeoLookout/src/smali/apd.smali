.class public abstract Lapd;
.super Lapb;
.source "NotifiableTask.java"


# static fields
.field public static a:Lalb;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    sput-object v0, Lapd;->a:Lalb;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lalo;Lalr;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0, p2, p3}, Lapb;-><init>(Lalo;Lalr;)V

    .line 27
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lapd;->c:Landroid/os/Handler;

    .line 31
    iput-object p1, p0, Lapd;->b:Landroid/content/Context;

    .line 32
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 88
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.accessory.intent.action.DISASTER_SVIEW_COVER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 89
    const-string v1, "title"

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0029

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 91
    return-void
.end method

.method public a(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updateWidget() start widget update - uId : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 81
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 82
    const-string v1, "android.appwidget.action.NEW_PUSH_RECIEVE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    const-string v1, "uid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 84
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 85
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 2

    .prologue
    .line 35
    const-string v0, "START - showEmergencyDialog"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lapd;->b:Landroid/content/Context;

    invoke-static {v0}, Lall;->v(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lapd;->c:Landroid/os/Handler;

    new-instance v1, Lape;

    invoke-direct {v1, p0, p1, p2}, Lape;-><init>(Lapd;Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 55
    :goto_0
    return-void

    .line 52
    :cond_0
    const-string v0, "Popup disabled"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Lcom/sec/android/GeoLookout/db/DisasterInfo;Z)V
    .locals 3

    .prologue
    .line 94
    if-nez p1, :cond_0

    .line 95
    const-string v0, "disaster info is null"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 107
    :goto_0
    return-void

    .line 100
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.GeoLookout.ACTION_UPDATE_PUSH_MESSAGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 101
    const-string v1, "canceled"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 102
    const-string v1, "DID"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 104
    const-string v1, "uid"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 106
    iget-object v1, p0, Lapd;->b:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lapd;->a:Lalb;

    if-eqz v0, :cond_0

    .line 59
    sget-object v0, Lapd;->a:Lalb;

    invoke-virtual {v0, p1}, Lalb;->a(Ljava/lang/String;)V

    .line 61
    :cond_0
    return-void
.end method

.method public b(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "showDisasterAlertNotification() : DID "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 67
    :try_start_0
    invoke-static {}, Lalk;->a()Lalk;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lalk;->a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 70
    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lalx;->b(Landroid/content/Context;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    :goto_0
    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 74
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
