.class public Lwz;
.super Ldh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldh",
        "<",
        "Lwy;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:Ldt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldt",
            "<",
            "Lwy;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/view/ViewGroup;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/maps/GoogleMapOptions;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V
    .locals 0

    invoke-direct {p0}, Ldh;-><init>()V

    iput-object p1, p0, Lwz;->b:Landroid/view/ViewGroup;

    iput-object p2, p0, Lwz;->c:Landroid/content/Context;

    iput-object p3, p0, Lwz;->d:Lcom/google/android/gms/maps/GoogleMapOptions;

    return-void
.end method


# virtual methods
.method protected a(Ldt;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldt",
            "<",
            "Lwy;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lwz;->a:Ldt;

    invoke-virtual {p0}, Lwz;->g()V

    return-void
.end method

.method public g()V
    .locals 4

    iget-object v0, p0, Lwz;->a:Ldt;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lwz;->a()Ldg;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lwz;->c:Landroid/content/Context;

    invoke-static {v0}, Lzu;->a(Landroid/content/Context;)Lyg;

    move-result-object v0

    iget-object v1, p0, Lwz;->c:Landroid/content/Context;

    invoke-static {v1}, Lds;->a(Ljava/lang/Object;)Ldp;

    move-result-object v1

    iget-object v2, p0, Lwz;->d:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-interface {v0, v1, v2}, Lyg;->a(Ldp;Lcom/google/android/gms/maps/GoogleMapOptions;)Lxt;

    move-result-object v0

    iget-object v1, p0, Lwz;->a:Ldt;

    new-instance v2, Lwy;

    iget-object v3, p0, Lwz;->b:Landroid/view/ViewGroup;

    invoke-direct {v2, v3, v0}, Lwy;-><init>(Landroid/view/ViewGroup;Lxt;)V

    invoke-interface {v1, v2}, Ldt;->a(Ldg;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcd; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :catch_1
    move-exception v0

    goto :goto_0
.end method
