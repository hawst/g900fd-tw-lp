.class public Laiy;
.super Landroid/widget/BaseAdapter;
.source "DisasterHistoryActivity.java"


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Laja;",
            ">;"
        }
    .end annotation
.end field

.field b:Laix;

.field final synthetic c:Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;

.field private d:I


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Laja;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 527
    iput-object p1, p0, Laiy;->c:Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 528
    iput-object p2, p0, Laiy;->a:Ljava/util/ArrayList;

    .line 529
    const/4 v0, 0x0

    iput v0, p0, Laiy;->d:I

    .line 530
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 519
    iget v0, p0, Laiy;->d:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 523
    iput p1, p0, Laiy;->d:I

    .line 524
    invoke-virtual {p0}, Laiy;->notifyDataSetChanged()V

    .line 525
    return-void
.end method

.method public b(I)Laja;
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Laiy;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Laiy;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 513
    invoke-virtual {p0, p1}, Laiy;->b(I)Laja;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 544
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 549
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getView() called : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 550
    iget-object v0, p0, Laiy;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laja;

    .line 551
    invoke-static {v0}, Laja;->a(Laja;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v2

    .line 553
    if-nez p2, :cond_1

    .line 554
    iget-object v1, p0, Laiy;->c:Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->e(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f030019

    invoke-virtual {v1, v3, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 556
    new-instance v1, Laix;

    invoke-direct {v1}, Laix;-><init>()V

    iput-object v1, p0, Laiy;->b:Laix;

    .line 557
    iget-object v3, p0, Laiy;->b:Laix;

    const v1, 0x7f0e0013

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Laix;->a:Landroid/widget/TextView;

    .line 558
    iget-object v3, p0, Laiy;->b:Laix;

    const v1, 0x7f0e007e

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Laix;->b:Landroid/widget/TextView;

    .line 559
    iget-object v3, p0, Laiy;->b:Laix;

    const v1, 0x7f0e007f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Laix;->c:Landroid/widget/TextView;

    .line 560
    iget-object v3, p0, Laiy;->b:Laix;

    const v1, 0x7f0e0080

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Laix;->e:Landroid/widget/TextView;

    .line 561
    iget-object v3, p0, Laiy;->b:Laix;

    const v1, 0x7f0e0081

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v3, Laix;->d:Landroid/widget/TextView;

    .line 563
    iget-object v1, p0, Laiy;->b:Laix;

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 568
    :goto_0
    const v1, 0x7f0e001f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 569
    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLink()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLink()Ljava/lang/String;

    move-result-object v3

    const-string v4, "null"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 570
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 572
    new-instance v3, Laiz;

    invoke-direct {v3, p0, v2}, Laiz;-><init>(Laiy;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 582
    :goto_1
    const-string v1, "03"

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 583
    iget-object v1, p0, Laiy;->b:Laix;

    iget-object v1, v1, Laix;->a:Landroid/widget/TextView;

    iget-object v3, p0, Laiy;->c:Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;

    invoke-static {v3}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->e(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v4

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v5

    invoke-static {v3, v4, v5}, Lall;->b(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 587
    :goto_2
    iget v1, p0, Laiy;->d:I

    if-ne p1, v1, :cond_4

    .line 588
    iget-object v1, p0, Laiy;->b:Laix;

    iget-object v1, v1, Laix;->a:Landroid/widget/TextView;

    iget-object v3, p0, Laiy;->c:Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;

    invoke-static {v3}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->e(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0800b7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 593
    :goto_3
    iget-object v1, p0, Laiy;->b:Laix;

    iget-object v3, v1, Laix;->c:Landroid/widget/TextView;

    iget-object v1, p0, Laiy;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laja;

    invoke-static {v1}, Laja;->b(Laja;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 595
    iget-object v1, p0, Laiy;->b:Laix;

    iget-object v1, v1, Laix;->d:Landroid/widget/TextView;

    invoke-static {v0}, Laja;->c(Laja;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 597
    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v1

    .line 598
    sparse-switch v1, :sswitch_data_0

    .line 656
    iget-object v1, p0, Laiy;->b:Laix;

    iget-object v1, v1, Laix;->e:Landroid/widget/TextView;

    invoke-static {v0}, Laja;->d(Laja;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 657
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 658
    const-string v0, "03"

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 659
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 662
    :cond_0
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 671
    :goto_4
    return-object p2

    .line 565
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laix;

    iput-object v1, p0, Laiy;->b:Laix;

    goto/16 :goto_0

    .line 579
    :cond_2
    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 585
    :cond_3
    iget-object v1, p0, Laiy;->b:Laix;

    iget-object v1, v1, Laix;->a:Landroid/widget/TextView;

    iget-object v3, p0, Laiy;->c:Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;

    invoke-static {v3}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->e(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v4

    invoke-static {v3, v4}, Lall;->h(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 590
    :cond_4
    iget-object v1, p0, Laiy;->b:Laix;

    iget-object v1, v1, Laix;->a:Landroid/widget/TextView;

    iget-object v3, p0, Laiy;->c:Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;

    invoke-static {v3}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->e(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f080000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_3

    .line 601
    :sswitch_0
    const-string v0, "03"

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 602
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 604
    iget-object v1, p0, Laiy;->c:Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a00b3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 605
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 606
    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsZoneCity()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 607
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 608
    iget-object v1, p0, Laiy;->c:Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0a00bb

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getJpnDetailsLevel()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 610
    iget-object v1, p0, Laiy;->b:Laix;

    iget-object v1, v1, Laix;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 613
    :cond_5
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 614
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->b:Landroid/widget/TextView;

    iget-object v1, p0, Laiy;->c:Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;

    invoke-static {v1, v2}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 615
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 619
    :sswitch_1
    iget-object v1, p0, Laiy;->b:Laix;

    iget-object v1, v1, Laix;->e:Landroid/widget/TextView;

    invoke-static {v0}, Laja;->d(Laja;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 620
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 621
    const-string v0, "03"

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 622
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 625
    :cond_6
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->b:Landroid/widget/TextView;

    iget-object v1, p0, Laiy;->c:Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;

    invoke-static {v1, v2}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 626
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 630
    :sswitch_2
    iget-object v1, p0, Laiy;->b:Laix;

    iget-object v1, v1, Laix;->e:Landroid/widget/TextView;

    invoke-static {v0}, Laja;->d(Laja;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 631
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 632
    const-string v0, "03"

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 633
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 636
    :cond_7
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 642
    :sswitch_3
    iget-object v1, p0, Laiy;->b:Laix;

    iget-object v1, v1, Laix;->e:Landroid/widget/TextView;

    invoke-static {v0}, Laja;->d(Laja;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 643
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 644
    const-string v0, "03"

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 645
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 647
    :cond_8
    const-string v0, "03"

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 648
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 651
    :cond_9
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->b:Landroid/widget/TextView;

    iget-object v1, p0, Laiy;->c:Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;

    invoke-static {v1, v2}, Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterHistoryActivity;Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 652
    iget-object v0, p0, Laiy;->b:Laix;

    iget-object v0, v0, Laix;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 598
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_1
        0x69 -> :sswitch_0
        0x6e -> :sswitch_3
        0x70 -> :sswitch_3
        0x71 -> :sswitch_2
        0xce -> :sswitch_2
    .end sparse-switch
.end method
