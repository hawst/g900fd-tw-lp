.class public final Lafu;
.super Ljava/lang/Object;
.source "SlookWritingBuddy.java"


# static fields
.field public static final a:I = 0x1

.field public static final b:I = 0x2

.field private static final c:Ljava/lang/String; = "WritingBuddy"


# instance fields
.field private d:Laew;

.field private e:Landroid/view/View;

.field private f:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

.field private g:Lafy;

.field private h:Lafx;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Laew;

    invoke-direct {v0}, Laew;-><init>()V

    iput-object v0, p0, Lafu;->d:Laew;

    .line 89
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lafu;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    const-string v0, "WritingBuddy"

    const-string v1, "This is not supported in device"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :goto_0
    return-void

    .line 93
    :cond_0
    iput-object p1, p0, Lafu;->e:Landroid/view/View;

    .line 94
    new-instance v0, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    invoke-direct {v0, p1}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lafu;->f:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/widget/EditText;)V
    .locals 2

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Laew;

    invoke-direct {v0}, Laew;-><init>()V

    iput-object v0, p0, Lafu;->d:Laew;

    .line 102
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lafu;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    const-string v0, "WritingBuddy"

    const-string v1, "This is not supported in device"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    iput-object p1, p0, Lafu;->e:Landroid/view/View;

    .line 107
    new-instance v0, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    invoke-direct {v0, p1}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lafu;->f:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    goto :goto_0
.end method

.method static synthetic a(Lafu;)Lafy;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lafu;->g:Lafy;

    return-object v0
.end method

.method static synthetic b(Lafu;)Lafx;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lafu;->h:Lafx;

    return-object v0
.end method

.method private b(I)Z
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lafu;->d:Laew;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    const/4 v0, 0x1

    .line 228
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lafu;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    const/4 v0, 0x0

    .line 130
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lafu;->f:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    invoke-virtual {v0}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->getEditorType()I

    move-result v0

    goto :goto_0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lafu;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 121
    :goto_0
    return-void

    .line 120
    :cond_0
    iget-object v0, p0, Lafu;->f:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    invoke-virtual {v0, p1}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->setEditorType(I)V

    goto :goto_0
.end method

.method public a(Lafx;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 163
    invoke-direct {p0, v1}, Lafu;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    :goto_0
    return-void

    .line 166
    :cond_0
    iput-object p1, p0, Lafu;->h:Lafx;

    .line 168
    if-nez p1, :cond_1

    .line 169
    iget-object v0, p0, Lafu;->f:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->setImageWritingEnabled(Z)V

    .line 170
    iget-object v0, p0, Lafu;->f:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->setOnImageWritingListener(Lcom/samsung/android/writingbuddy/WritingBuddyImpl$OnImageWritingListener;)V

    goto :goto_0

    .line 172
    :cond_1
    iget-object v0, p0, Lafu;->f:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    invoke-virtual {v0, v1}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->setImageWritingEnabled(Z)V

    .line 173
    iget-object v0, p0, Lafu;->f:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    new-instance v1, Lafw;

    invoke-direct {v1, p0}, Lafw;-><init>(Lafu;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->setOnImageWritingListener(Lcom/samsung/android/writingbuddy/WritingBuddyImpl$OnImageWritingListener;)V

    goto :goto_0
.end method

.method public a(Lafy;)V
    .locals 2

    .prologue
    .line 140
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lafu;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    :goto_0
    return-void

    .line 143
    :cond_0
    iput-object p1, p0, Lafu;->g:Lafy;

    .line 145
    iget-object v0, p0, Lafu;->f:Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    new-instance v1, Lafv;

    invoke-direct {v1, p0}, Lafv;-><init>(Lafu;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/writingbuddy/WritingBuddyImpl;->setOnTextWritingListener(Lcom/samsung/android/writingbuddy/WritingBuddyImpl$OnTextWritingListener;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 191
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lafu;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 204
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lafu;->e:Landroid/view/View;

    if-nez v0, :cond_1

    .line 195
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "mParentView is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :cond_1
    iget-object v0, p0, Lafu;->e:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/EditText;

    if-eqz v0, :cond_2

    .line 198
    iget-object v0, p0, Lafu;->e:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setWritingBuddyEnabled(Z)V

    goto :goto_0

    .line 199
    :cond_2
    iget-object v0, p0, Lafu;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->getWritingBuddy(Z)Lcom/samsung/android/writingbuddy/WritingBuddyImpl;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 200
    iget-object v0, p0, Lafu;->e:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setWritingBuddyEnabled(Z)V

    goto :goto_0

    .line 202
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "WritingBuddy was not enabled."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 212
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lafu;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    const/4 v0, 0x0

    .line 218
    :goto_0
    return v0

    .line 215
    :cond_0
    iget-object v0, p0, Lafu;->e:Landroid/view/View;

    if-nez v0, :cond_1

    .line 216
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "mParentView is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218
    :cond_1
    iget-object v0, p0, Lafu;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isWritingBuddyEnabled()Z

    move-result v0

    goto :goto_0
.end method
