.class public final Lagl;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final black:I = 0x7f080000

.field public static final color_black:I = 0x7f080001

.field public static final color_no_disaster_text:I = 0x7f080002

.field public static final color_popup_text_color:I = 0x7f080003

.field public static final color_text_shadow_black_20:I = 0x7f080004

.field public static final color_text_shadow_black_30:I = 0x7f080005

.field public static final color_text_shadow_black_50:I = 0x7f080006

.field public static final color_text_shadow_black_55:I = 0x7f080007

.field public static final color_text_shadow_black_66:I = 0x7f080008

.field public static final color_text_shadow_black_75:I = 0x7f080009

.field public static final color_text_shadow_black_80:I = 0x7f08000a

.field public static final color_transparent:I = 0x7f08000b

.field public static final color_white:I = 0x7f08000c

.field public static final colorlist_000000:I = 0x7f08000d

.field public static final colorlist_000084:I = 0x7f08000e

.field public static final colorlist_0000ff:I = 0x7f08000f

.field public static final colorlist_003000:I = 0x7f080010

.field public static final colorlist_003063:I = 0x7f080011

.field public static final colorlist_005064:I = 0x7f080012

.field public static final colorlist_007990:I = 0x7f080013

.field public static final colorlist_007ad2:I = 0x7f080014

.field public static final colorlist_007f97:I = 0x7f080015

.field public static final colorlist_008200:I = 0x7f080016

.field public static final colorlist_008284:I = 0x7f080017

.field public static final colorlist_00a5c4:I = 0x7f080018

.field public static final colorlist_00cfff:I = 0x7f080019

.field public static final colorlist_00ff00:I = 0x7f08001a

.field public static final colorlist_00ffff:I = 0x7f08001b

.field public static final colorlist_010101:I = 0x7f08001c

.field public static final colorlist_01942e:I = 0x7f08001d

.field public static final colorlist_055975:I = 0x7f08001e

.field public static final colorlist_058ad4:I = 0x7f08001f

.field public static final colorlist_0a296c:I = 0x7f080020

.field public static final colorlist_0b9b69:I = 0x7f080021

.field public static final colorlist_0e293f:I = 0x7f080022

.field public static final colorlist_0e8019:I = 0x7f080023

.field public static final colorlist_0e8064:I = 0x7f080024

.field public static final colorlist_111111:I = 0x7f080025

.field public static final colorlist_114b9d:I = 0x7f080026

.field public static final colorlist_11a900:I = 0x7f080027

.field public static final colorlist_131313:I = 0x7f080028

.field public static final colorlist_141414:I = 0x7f080029

.field public static final colorlist_1c7ec4:I = 0x7f08002a

.field public static final colorlist_262626:I = 0x7f08002b

.field public static final colorlist_296183:I = 0x7f08002c

.field public static final colorlist_2b2a27:I = 0x7f08002d

.field public static final colorlist_2b2b2b:I = 0x7f08002e

.field public static final colorlist_2e1f0c:I = 0x7f08002f

.field public static final colorlist_313000:I = 0x7f080030

.field public static final colorlist_313031:I = 0x7f080031

.field public static final colorlist_31309c:I = 0x7f080032

.field public static final colorlist_313131:I = 0x7f080033

.field public static final colorlist_3165ff:I = 0x7f080034

.field public static final colorlist_319a63:I = 0x7f080035

.field public static final colorlist_31cbce:I = 0x7f080036

.field public static final colorlist_3367fd:I = 0x7f080037

.field public static final colorlist_33f5f5f5:I = 0x7f080038

.field public static final colorlist_33fff5ee:I = 0x7f080039

.field public static final colorlist_353535:I = 0x7f08003a

.field public static final colorlist_363532:I = 0x7f08003b

.field public static final colorlist_364f67:I = 0x7f08003c

.field public static final colorlist_38668c:I = 0x7f08003d

.field public static final colorlist_38a8ff:I = 0x7f08003e

.field public static final colorlist_3c3222:I = 0x7f08003f

.field public static final colorlist_3d7b8a:I = 0x7f080040

.field public static final colorlist_41a526:I = 0x7f080041

.field public static final colorlist_43add4:I = 0x7f080042

.field public static final colorlist_494946:I = 0x7f080043

.field public static final colorlist_4b4b4b:I = 0x7f080044

.field public static final colorlist_4c402c:I = 0x7f080045

.field public static final colorlist_4e4e4e:I = 0x7f080046

.field public static final colorlist_513e34:I = 0x7f080047

.field public static final colorlist_554F43:I = 0x7f080048

.field public static final colorlist_56575b:I = 0x7f080049

.field public static final colorlist_585863:I = 0x7f08004a

.field public static final colorlist_594254:I = 0x7f08004b

.field public static final colorlist_5bdd38:I = 0x7f08004c

.field public static final colorlist_5c5c66:I = 0x7f08004d

.field public static final colorlist_5f5f5f:I = 0x7f08004e

.field public static final colorlist_63659c:I = 0x7f08004f

.field public static final colorlist_66000000:I = 0x7f080050

.field public static final colorlist_663010:I = 0x7f080051

.field public static final colorlist_666666:I = 0x7f080052

.field public static final colorlist_667f2a:I = 0x7f080053

.field public static final colorlist_708ba3:I = 0x7f080054

.field public static final colorlist_735c39:I = 0x7f080055

.field public static final colorlist_777777:I = 0x7f080056

.field public static final colorlist_778899:I = 0x7f080057

.field public static final colorlist_78838f:I = 0x7f080058

.field public static final colorlist_7a37de:I = 0x7f080059

.field public static final colorlist_808080:I = 0x7f08005a

.field public static final colorlist_828282:I = 0x7f08005b

.field public static final colorlist_840000:I = 0x7f08005c

.field public static final colorlist_840084:I = 0x7f08005d

.field public static final colorlist_848200:I = 0x7f08005e

.field public static final colorlist_848484:I = 0x7f08005f

.field public static final colorlist_8a8a8a:I = 0x7f080060

.field public static final colorlist_8b007b:I = 0x7f080061

.field public static final colorlist_8c537a:I = 0x7f080062

.field public static final colorlist_8e8e8e:I = 0x7f080063

.field public static final colorlist_935c39:I = 0x7f080064

.field public static final colorlist_943063:I = 0x7f080065

.field public static final colorlist_999999:I = 0x7f080066

.field public static final colorlist_9c3400:I = 0x7f080067

.field public static final colorlist_9c9a9c:I = 0x7f080068

.field public static final colorlist_9c9c9c:I = 0x7f080069

.field public static final colorlist_9ccb00:I = 0x7f08006a

.field public static final colorlist_9ccfff:I = 0x7f08006b

.field public static final colorlist_9d9c9c:I = 0x7f08006c

.field public static final colorlist_9e3d4c5a:I = 0x7f08006d

.field public static final colorlist_9e9e9e:I = 0x7f08006e

.field public static final colorlist_a11f1f:I = 0x7f08006f

.field public static final colorlist_a6a5a5:I = 0x7f080070

.field public static final colorlist_a9a59c:I = 0x7f080071

.field public static final colorlist_aba79c:I = 0x7f080072

.field public static final colorlist_b2000000:I = 0x7f080073

.field public static final colorlist_b25f5f5f:I = 0x7f080074

.field public static final colorlist_b2f5f5f5:I = 0x7f080075

.field public static final colorlist_b2fafafa:I = 0x7f080076

.field public static final colorlist_b2ffffff:I = 0x7f080077

.field public static final colorlist_bbb9b3:I = 0x7f080078

.field public static final colorlist_bc0421:I = 0x7f080079

.field public static final colorlist_bc045b:I = 0x7f08007a

.field public static final colorlist_bf4e4e4e:I = 0x7f08007b

.field public static final colorlist_bfffffff:I = 0x7f08007c

.field public static final colorlist_c6c6c6:I = 0x7f08007d

.field public static final colorlist_c8c8c8:I = 0x7f08007e

.field public static final colorlist_ca000000:I = 0x7f08007f

.field public static final colorlist_ca85ff:I = 0x7f080080

.field public static final colorlist_cbe36c:I = 0x7f080081

.field public static final colorlist_ccf5f5f5:I = 0x7f080082

.field public static final colorlist_ce9aff:I = 0x7f080083

.field public static final colorlist_ceffce:I = 0x7f080084

.field public static final colorlist_ceffff:I = 0x7f080085

.field public static final colorlist_d2d2d2:I = 0x7f080086

.field public static final colorlist_d3d3d3:I = 0x7f080087

.field public static final colorlist_dcb149:I = 0x7f080088

.field public static final colorlist_e1e1e1:I = 0x7f080089

.field public static final colorlist_ea7135:I = 0x7f08008a

.field public static final colorlist_ebc67f:I = 0x7f08008b

.field public static final colorlist_ed1a3b:I = 0x7f08008c

.field public static final colorlist_ededed:I = 0x7f08008d

.field public static final colorlist_eeeeee:I = 0x7f08008e

.field public static final colorlist_f16001:I = 0x7f08008f

.field public static final colorlist_f2d012:I = 0x7f080090

.field public static final colorlist_f2f2f2:I = 0x7f080091

.field public static final colorlist_f54d67:I = 0x7f080092

.field public static final colorlist_f57e60:I = 0x7f080093

.field public static final colorlist_f5f5f5:I = 0x7f080094

.field public static final colorlist_f9a48f:I = 0x7f080095

.field public static final colorlist_fac74b:I = 0x7f080096

.field public static final colorlist_fafafa:I = 0x7f080097

.field public static final colorlist_fce33c:I = 0x7f080098

.field public static final colorlist_fdff2d:I = 0x7f080099

.field public static final colorlist_ff0000:I = 0x7f08009a

.field public static final colorlist_ff00cf:I = 0x7f08009b

.field public static final colorlist_ff00ff:I = 0x7f08009c

.field public static final colorlist_ff018bd2:I = 0x7f08009d

.field public static final colorlist_ff3b5b:I = 0x7f08009e

.field public static final colorlist_ff49c9:I = 0x7f08009f

.field public static final colorlist_ff585863:I = 0x7f0800a0

.field public static final colorlist_ff6500:I = 0x7f0800a1

.field public static final colorlist_ff835d:I = 0x7f0800a2

.field public static final colorlist_ff95a5:I = 0x7f0800a3

.field public static final colorlist_ff9a00:I = 0x7f0800a4

.field public static final colorlist_ff9ace:I = 0x7f0800a5

.field public static final colorlist_ffc400:I = 0x7f0800a6

.field public static final colorlist_ffcdc1:I = 0x7f0800a7

.field public static final colorlist_ffcf00:I = 0x7f0800a8

.field public static final colorlist_ffcf9c:I = 0x7f0800a9

.field public static final colorlist_ffe57f:I = 0x7f0800aa

.field public static final colorlist_fff600:I = 0x7f0800ab

.field public static final colorlist_ffff00:I = 0x7f0800ac

.field public static final colorlist_ffff9c:I = 0x7f0800ad

.field public static final colorlist_ffffff:I = 0x7f0800ae

.field public static final dashboard_center_circle_color:I = 0x7f0800af

.field public static final dashboard_circle_color:I = 0x7f0800b0

.field public static final dashboard_no_disaster_alert_type_color:I = 0x7f0800b1

.field public static final dashboard_no_disaster_description_color:I = 0x7f0800b2

.field public static final dashboard_no_disaster_title_color:I = 0x7f0800b3

.field public static final dashboard_stroke_color:I = 0x7f0800b4

.field public static final dashboard_warning_color:I = 0x7f0800b5

.field public static final dashboard_watch_color:I = 0x7f0800b6

.field public static final history_highlight_color:I = 0x7f0800b7

.field public static final no_item_text:I = 0x7f0800b8

.field public static final popup_actionbar_bg:I = 0x7f0800b9

.field public static final red:I = 0x7f0800ba

.field public static final transparent:I = 0x7f0800bb

.field public static final white:I = 0x7f0800bc


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
