.class public Lalt;
.super Ljava/lang/Object;
.source "DisasterOptions.java"


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private a:Landroid/content/Context;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private f:D

.field private g:D

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Lcom/sec/android/GeoLookout/db/DisasterInfo;

.field private m:Lanx;

.field private n:Lanz;

.field private o:Lanw;

.field private p:Lany;

.field private q:I

.field private r:I

.field private s:Landroid/location/Location;

.field private t:Z

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Z

.field private x:Z

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    iput-object p1, p0, Lalt;->a:Landroid/content/Context;

    .line 124
    return-void
.end method

.method static synthetic A(Lalt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lalt;->B:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic B(Lalt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lalt;->C:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lalt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lalt;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lalt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lalt;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lalt;)I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lalt;->d:I

    return v0
.end method

.method static synthetic d(Lalt;)I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lalt;->e:I

    return v0
.end method

.method static synthetic e(Lalt;)D
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lalt;->g:D

    return-wide v0
.end method

.method static synthetic f(Lalt;)D
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lalt;->f:D

    return-wide v0
.end method

.method static synthetic g(Lalt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lalt;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lalt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lalt;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lalt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lalt;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lalt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lalt;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lalt;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lalt;->l:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    return-object v0
.end method

.method static synthetic l(Lalt;)Lanx;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lalt;->m:Lanx;

    return-object v0
.end method

.method static synthetic m(Lalt;)Lanz;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lalt;->n:Lanz;

    return-object v0
.end method

.method static synthetic n(Lalt;)Lanw;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lalt;->o:Lanw;

    return-object v0
.end method

.method static synthetic o(Lalt;)Lany;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lalt;->p:Lany;

    return-object v0
.end method

.method static synthetic p(Lalt;)I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lalt;->q:I

    return v0
.end method

.method static synthetic q(Lalt;)I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lalt;->r:I

    return v0
.end method

.method static synthetic r(Lalt;)Landroid/location/Location;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lalt;->s:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic s(Lalt;)Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lalt;->t:Z

    return v0
.end method

.method static synthetic t(Lalt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lalt;->u:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic u(Lalt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lalt;->v:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic v(Lalt;)Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lalt;->w:Z

    return v0
.end method

.method static synthetic w(Lalt;)Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lalt;->x:Z

    return v0
.end method

.method static synthetic x(Lalt;)I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lalt;->y:I

    return v0
.end method

.method static synthetic y(Lalt;)I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lalt;->z:I

    return v0
.end method

.method static synthetic z(Lalt;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lalt;->A:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()Lalr;
    .locals 2

    .prologue
    .line 127
    new-instance v0, Lalr;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lalr;-><init>(Lalt;Lals;)V

    return-object v0
.end method

.method public a(D)Lalt;
    .locals 1

    .prologue
    .line 151
    iput-wide p1, p0, Lalt;->f:D

    .line 152
    return-object p0
.end method

.method public a(I)Lalt;
    .locals 0

    .prologue
    .line 136
    iput p1, p0, Lalt;->d:I

    .line 137
    return-object p0
.end method

.method public a(Landroid/location/Location;)Lalt;
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lalt;->s:Landroid/location/Location;

    .line 217
    return-object p0
.end method

.method public a(Lanw;)Lalt;
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lalt;->o:Lanw;

    .line 197
    return-object p0
.end method

.method public a(Lanx;)Lalt;
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lalt;->m:Lanx;

    .line 187
    return-object p0
.end method

.method public a(Lany;)Lalt;
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lalt;->p:Lany;

    .line 202
    return-object p0
.end method

.method public a(Lanz;)Lalt;
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lalt;->n:Lanz;

    .line 192
    return-object p0
.end method

.method public a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Lalt;
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lalt;->l:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    .line 182
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lalt;
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lalt;->c:Ljava/lang/String;

    .line 132
    return-object p0
.end method

.method public a(Z)Lalt;
    .locals 0

    .prologue
    .line 221
    iput-boolean p1, p0, Lalt;->t:Z

    .line 222
    return-object p0
.end method

.method public b(D)Lalt;
    .locals 1

    .prologue
    .line 156
    iput-wide p1, p0, Lalt;->g:D

    .line 157
    return-object p0
.end method

.method public b(I)Lalt;
    .locals 0

    .prologue
    .line 141
    iput p1, p0, Lalt;->e:I

    .line 142
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lalt;
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lalt;->b:Ljava/lang/String;

    .line 147
    return-object p0
.end method

.method public b(Z)Lalt;
    .locals 0

    .prologue
    .line 236
    iput-boolean p1, p0, Lalt;->w:Z

    .line 237
    return-object p0
.end method

.method public c(I)Lalt;
    .locals 0

    .prologue
    .line 206
    iput p1, p0, Lalt;->q:I

    .line 207
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lalt;
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lalt;->h:Ljava/lang/String;

    .line 162
    return-object p0
.end method

.method public c(Z)Lalt;
    .locals 0

    .prologue
    .line 241
    iput-boolean p1, p0, Lalt;->x:Z

    .line 242
    return-object p0
.end method

.method public d(I)Lalt;
    .locals 0

    .prologue
    .line 211
    iput p1, p0, Lalt;->r:I

    .line 212
    return-object p0
.end method

.method public d(Ljava/lang/String;)Lalt;
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lalt;->i:Ljava/lang/String;

    .line 167
    return-object p0
.end method

.method public e(I)Lalt;
    .locals 0

    .prologue
    .line 246
    iput p1, p0, Lalt;->y:I

    .line 247
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lalt;
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lalt;->j:Ljava/lang/String;

    .line 172
    return-object p0
.end method

.method public f(I)Lalt;
    .locals 0

    .prologue
    .line 251
    iput p1, p0, Lalt;->z:I

    .line 252
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lalt;
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lalt;->k:Ljava/lang/String;

    .line 177
    return-object p0
.end method

.method public g(Ljava/lang/String;)Lalt;
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lalt;->u:Ljava/lang/String;

    .line 227
    return-object p0
.end method

.method public h(Ljava/lang/String;)Lalt;
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lalt;->v:Ljava/lang/String;

    .line 232
    return-object p0
.end method

.method public i(Ljava/lang/String;)Lalt;
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lalt;->B:Ljava/lang/String;

    .line 257
    return-object p0
.end method

.method public j(Ljava/lang/String;)Lalt;
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lalt;->C:Ljava/lang/String;

    .line 262
    return-object p0
.end method

.method public k(Ljava/lang/String;)Lalt;
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lalt;->A:Ljava/lang/String;

    .line 267
    return-object p0
.end method
