.class public Lajs;
.super Landroid/os/AsyncTask;
.source "DisasterMultiPointActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Double;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Landroid/location/Address;",
        ">;>;"
    }
.end annotation


# instance fields
.field a:Landroid/content/Context;

.field final synthetic b:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 392
    iput-object p1, p0, Lajs;->b:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    .line 393
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 394
    iput-object p2, p0, Lajs;->a:Landroid/content/Context;

    .line 395
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Double;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Double;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 399
    new-instance v1, Landroid/location/Geocoder;

    iget-object v2, p0, Lajs;->a:Landroid/content/Context;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    .line 403
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MULTIPOINT search "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p1, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p1, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 409
    const/4 v2, 0x0

    :try_start_0
    aget-object v2, p1, v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const/4 v4, 0x1

    aget-object v4, p1, v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 419
    :goto_0
    return-object v0

    .line 410
    :catch_0
    move-exception v1

    .line 411
    const-string v2, "LocationSampleActivity IO Exception in getFromLocation()"

    invoke-static {v2}, Lalj;->e(Ljava/lang/String;)V

    .line 412
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 414
    :catch_1
    move-exception v1

    .line 415
    const-string v2, "LocationSampleActivity"

    invoke-static {v2}, Lalj;->e(Ljava/lang/String;)V

    .line 416
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method protected a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/location/Address;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 425
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MULTIPOINT LongClick addresses "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 426
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 427
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 428
    invoke-virtual {v0}, Landroid/location/Address;->getMaxAddressLineIndex()I

    move-result v3

    .line 429
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    move v1, v2

    .line 430
    :goto_0
    if-gt v1, v3, :cond_1

    .line 431
    invoke-virtual {v0, v1}, Landroid/location/Address;->getAddressLine(I)Ljava/lang/String;

    move-result-object v5

    .line 433
    if-eqz v1, :cond_0

    .line 437
    const-string v6, " "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 439
    :cond_0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 430
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 442
    :cond_1
    iget-object v0, p0, Lajs;->b:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 443
    iget-object v0, p0, Lajs;->b:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->d(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Landroid/widget/SearchView;

    move-result-object v0

    iget-object v1, p0, Lajs;->b:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->c(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 444
    iget-object v0, p0, Lajs;->b:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;Z)Z

    .line 447
    iget-object v0, p0, Lajs;->b:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->e(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Laah;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 448
    iget-object v0, p0, Lajs;->b:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->e(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Laah;

    move-result-object v0

    iget-object v1, p0, Lajs;->b:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->c(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Laah;->a(Ljava/lang/String;)V

    .line 451
    :cond_2
    iget-object v0, p0, Lajs;->b:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointActivity;->invalidateOptionsMenu()V

    .line 452
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 389
    check-cast p1, [Ljava/lang/Double;

    invoke-virtual {p0, p1}, Lajs;->a([Ljava/lang/Double;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 389
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lajs;->a(Ljava/util/List;)V

    return-void
.end method
