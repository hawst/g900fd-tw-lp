.class public final Laeb;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/MaskedWalletRequest;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/wallet/MaskedWalletRequest;)V
    .locals 0

    iput-object p1, p0, Laeb;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/wallet/MaskedWalletRequest;Laea;)V
    .locals 0

    invoke-direct {p0, p1}, Laeb;-><init>(Lcom/google/android/gms/wallet/MaskedWalletRequest;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/wallet/Cart;)Laeb;
    .locals 1

    iget-object v0, p0, Laeb;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-object p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->i:Lcom/google/android/gms/wallet/Cart;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Laeb;
    .locals 1

    iget-object v0, p0, Laeb;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-object p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->a:Ljava/lang/String;

    return-object p0
.end method

.method public a(Z)Laeb;
    .locals 1

    iget-object v0, p0, Laeb;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->b:Z

    return-object p0
.end method

.method public a()Lcom/google/android/gms/wallet/MaskedWalletRequest;
    .locals 1

    iget-object v0, p0, Laeb;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Laeb;
    .locals 1

    iget-object v0, p0, Laeb;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-object p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->e:Ljava/lang/String;

    return-object p0
.end method

.method public b(Z)Laeb;
    .locals 1

    iget-object v0, p0, Laeb;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c:Z

    return-object p0
.end method

.method public c(Ljava/lang/String;)Laeb;
    .locals 1

    iget-object v0, p0, Laeb;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-object p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->f:Ljava/lang/String;

    return-object p0
.end method

.method public c(Z)Laeb;
    .locals 1

    iget-object v0, p0, Laeb;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->d:Z

    return-object p0
.end method

.method public d(Ljava/lang/String;)Laeb;
    .locals 1

    iget-object v0, p0, Laeb;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-object p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->g:Ljava/lang/String;

    return-object p0
.end method

.method public d(Z)Laeb;
    .locals 1

    iget-object v0, p0, Laeb;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->j:Z

    return-object p0
.end method

.method public e(Z)Laeb;
    .locals 1

    iget-object v0, p0, Laeb;->a:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;->k:Z

    return-object p0
.end method
