.class Laob;
.super Ljava/lang/Object;
.source "SPPClientInterface.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Laoa;

.field private b:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method public constructor <init>(Laoa;)V
    .locals 2

    .prologue
    .line 41
    iput-object p1, p0, Laob;->a:Laoa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Laob;->b:Ljava/util/concurrent/CountDownLatch;

    .line 42
    return-void
.end method

.method static synthetic a(Laob;)Ljava/util/concurrent/CountDownLatch;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Laob;->b:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "connected! bounded:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Laob;->a:Laoa;

    invoke-static {v1}, Laoa;->a(Laoa;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " oldConnected:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Laob;->a:Laoa;

    invoke-static {v1}, Laoa;->b(Laoa;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Laob;->a:Laoa;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Laoa;->a(Laoa;Z)Z

    .line 48
    invoke-static {p2}, Laqw;->a(Landroid/os/IBinder;)Laqv;

    move-result-object v0

    invoke-static {v0}, Laoa;->a(Laqv;)Laqv;

    .line 49
    invoke-static {}, Laoa;->f()Laqv;

    move-result-object v0

    if-nez v0, :cond_0

    .line 50
    const-string v0, "[ServiceConnection]ServiceConnection-mService is null"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 52
    :cond_0
    iget-object v0, p0, Laob;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 53
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "disconnected! oldBounded:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Laob;->a:Laoa;

    invoke-static {v1}, Laoa;->a(Laoa;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " oldConnected:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Laob;->a:Laoa;

    invoke-static {v1}, Laoa;->b(Laoa;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Laob;->a:Laoa;

    invoke-static {v0, v2}, Laoa;->a(Laoa;Z)Z

    .line 59
    iget-object v0, p0, Laob;->a:Laoa;

    invoke-static {v0, v2}, Laoa;->b(Laoa;Z)Z

    .line 60
    const/4 v0, 0x0

    invoke-static {v0}, Laoa;->a(Laqv;)Laqv;

    .line 61
    return-void
.end method
