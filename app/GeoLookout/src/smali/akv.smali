.class public Lakv;
.super Ljava/lang/Object;
.source "D.java"


# static fields
.field public static final A:Ljava/lang/String; = "android.appwidget.action.UPDATE_LIFEMODE_RECIEVE"

.field public static final B:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_START_LIFEMODE_SYNC_SCHEDULER"

.field public static final C:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_CANCEL_LIFEMODE_SYNC_SCHEDULER"

.field public static final D:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_LIFEMODE_SYNC"

.field public static final E:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_LIFEMODE_AUTO_DATASYNC"

.field public static final F:Ljava/lang/String; = "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_SHOW_NEXT"

.field public static final G:Ljava/lang/String; = "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA"

.field public static final H:Ljava/lang/String; = "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_VIEW"

.field public static final I:Ljava/lang/String; = "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_TAG_TO_START"

.field public static final J:Ljava/lang/String; = "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_WIGET_APP_START"

.field public static final K:Ljava/lang/String; = "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA_RESULT"

.field public static final L:Ljava/lang/String; = "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA_CANCEL"

.field public static final M:Ljava/lang/String; = "com.android.contacts.action.SHOW_EMERGENCY_CONTACTS"

.field public static final N:Ljava/lang/String; = "com.sec.android.app.contacts.action.INTERACTION_EMERGENCY_MESSAGE"

.field public static final O:Ljava/lang/String; = "com.android.phone.action.RECENT_CALLS"

.field public static final P:Ljava/lang/String; = "com.samsung.accessory.intent.action.ALERT_NOTIFICATION_ITEM"

.field public static final Q:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_HANDLE_PUSH_MESSAGE"

.field public static final R:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_UPDATE_PUSH_MESSAGE"

.field public static final S:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_REGISTER_DISASTER_SERVER"

.field public static final T:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_UNREGISTER_DISASTER_SERVER"

.field public static final U:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_CHANGE_LOCALE"

.field public static final V:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_UPDATE_LOCATION_TO_SPP_SERVER"

.field public static final W:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_UPDATE_LOCATION_TO_DISASTER_SERVER"

.field public static final X:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_REQUEST_START_CURRENT_LOCATION"

.field public static final Y:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_REQUEST_CURRENT_LOCATION"

.field public static final Z:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_UPDATE_CURRENT_LOCATION"

.field public static final a:Ljava/lang/String; = "com.sec.android.app.secsetupwizard.SETUPWIZARD_COMPLETE"

.field public static final aA:Ljava/lang/String; = "safetycare_disaster_popup"

.field public static final aB:I = 0x0

.field public static final aC:I = 0x1

.field public static final aD:Ljava/lang/String; = "flash_notification"

.field public static final aE:I = 0x0

.field public static final aF:I = 0x1

.field public static final aG:Ljava/lang/String; = "disasteralert_mode_state"

.field public static final aH:I = 0x0

.field public static final aI:I = 0x1

.field public static final aJ:Ljava/lang/String; = "disasteralert_test_mode_state"

.field public static final aK:I = 0x0

.field public static final aL:I = 0x1

.field public static final aM:Ljava/lang/String; = "pref_headling_not_supported_languages_dialog"

.field public static final aN:Ljava/lang/String; = "pref_tip_not_supported_languages_dialog"

.field public static final aO:Ljava/lang/String; = "pref_geonews_current_page"

.field public static final aP:Ljava/lang/String; = "notiId"

.field public static final aQ:Ljava/lang/String; = "appData"

.field public static final aR:Ljava/lang/String; = "regId"

.field public static final aS:Ljava/lang/String; = "appId"

.field public static final aT:Ljava/lang/String; = "msg"

.field public static final aU:Ljava/lang/String; = "istracked"

.field public static final aV:Ljava/lang/String; = "pushStatus"

.field public static final aW:Ljava/lang/String; = "error"

.field public static final aX:Ljava/lang/String; = "location"

.field public static final aY:Ljava/lang/String; = "latitude"

.field public static final aZ:Ljava/lang/String; = "longitude"

.field public static final aa:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_CHECK_CURRENT_LOCATION"

.field public static final ab:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_CLOSE_LOCATION"

.field public static final ac:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_NOTIFICATION_CANCELED"

.field public static final ad:Ljava/lang/String; = "com.sec.android.GeoLookout.ACTION_SHOW_NETWORK_DIALOG"

.field public static final ae:Ljava/lang/String; = "com.sec.android.geolookout.Registered"

.field public static final af:Ljava/lang/String; = "com.sec.android.GeoLookout.geolifeactivity"

.field public static final ag:Ljava/lang/String; = "com.samsung.android.GeoLookout.ACTION_REQUEST_GEOLIFE_DATA"

.field public static final ah:Ljava/lang/String; = "com.samsung.android.GeoLookout.ACTION_CHANGE_COCKTAILBAR_UI"

.field public static final ai:Ljava/lang/String; = "safetycare_earthquake"

.field public static final aj:I = 0x0

.field public static final ak:I = 0x1

.field public static final al:Ljava/lang/String; = "safetycare_geolookout_registering"

.field public static final am:I = 0x0

.field public static final an:I = 0x1

.field public static final ao:Ljava/lang/String; = "disasteralert_push_dserver_state"

.field public static final ap:I = 0x0

.field public static final aq:I = 0x1

.field public static final ar:Ljava/lang/String; = "disasteralert_push_spp_state"

.field public static final as:I = 0x0

.field public static final at:I = 0x1

.field public static final au:Ljava/lang/String; = "safety_care_user_agree"

.field public static final av:I = 0x0

.field public static final aw:I = 0x1

.field public static final ax:Ljava/lang/String; = "safety_care_disaster_user_agree"

.field public static final ay:I = 0x0

.field public static final az:I = 0x1

.field public static final b:Ljava/lang/String; = "android.intent.action.BOOT_COMPLETED"

.field public static final bA:Ljava/lang/String; = "lf_count"

.field public static final bB:Ljava/lang/String; = "lf_array"

.field public static final bC:I = 0xa

.field public static final bD:I = 0xb

.field public static final bE:I = 0x14

.field public static final bF:I = 0x15

.field public static final bG:I = 0x1e

.field public static final bH:I = 0x28

.field public static final bI:I = 0x32

.field public static final bJ:Ljava/lang/String; = "/"

.field public static final bK:Ljava/lang/String; = "GeoNews"

.field public static final bL:Ljava/lang/String; = "/GeoNews"

.field public static final bM:Ljava/lang/String; = ".jpg"

.field public static final bN:Ljava/lang/String; = "ATT"

.field public static final bO:Ljava/lang/String; = "http://www.samsung.com/geonews/"

.field public static final bP:Z

.field public static final bQ:Z

.field public static final bR:Z

.field public static final bS:Ljava/lang/String; = "com.android.mms"

.field public static final bT:Ljava/lang/String; = "com.nttdocomo.android.dhome"

.field public static final bU:Ljava/lang/String; = "com.nttdocomo.android.paletteui"

.field public static final bV:Ljava/lang/String; = "com.kddi.android.auhomelauncher"

.field public static final bW:Ljava/lang/String; = "com.samsung.safetytestapplication"

.field public static final bX:Ljava/lang/String; = "isTestMode"

.field public static final bY:Ljava/lang/String; = "/GeoNews/TestAppId"

.field public static final bZ:Ljava/lang/String; = "is_test_mode"

.field public static final ba:Ljava/lang/String; = "currentZone"

.field public static final bb:Ljava/lang/String; = "currentCounty"

.field public static final bc:Ljava/lang/String; = "newZone"

.field public static final bd:Ljava/lang/String; = "newCounty"

.field public static final be:Ljava/lang/String; = "canceled"

.field public static final bf:Ljava/lang/String; = "bSuccess"

.field public static final bg:Ljava/lang/String; = "notification_custom_field2"

.field public static final bh:Ljava/lang/String; = "DID"

.field public static final bi:Ljava/lang/String; = "NOTIFICATION_ID"

.field public static final bj:Ljava/lang/String; = "NOTIFICATION_PACKAGE_NAME"

.field public static final bk:Ljava/lang/String; = "uid"

.field public static final bl:Ljava/lang/String; = "dv"

.field public static final bm:Ljava/lang/String; = "appWidgetId"

.field public static final bn:Ljava/lang/String; = "from"

.field public static final bo:Ljava/lang/String; = "code"

.field public static final bp:Ljava/lang/String; = "network_status"

.field public static final bq:Ljava/lang/String; = "network_error_code"

.field public static final br:Ljava/lang/String; = "reuqest_new_location"

.field public static final bs:Ljava/lang/String; = "force_high_accuracy"

.field public static final bt:Ljava/lang/String; = "indexType"

.field public static final bu:Ljava/lang/String; = "problem"

.field public static final bv:Ljava/lang/String; = "problem_need_regist"

.field public static final bw:Ljava/lang/String; = "problem_no_sim"

.field public static final bx:Ljava/lang/String; = "refreshing"

.field public static final by:Ljava/lang/String; = "current_loc"

.field public static final bz:Ljava/lang/String; = "current_zone"

.field public static final c:Ljava/lang/String; = "android.intent.action.LOCALE_CHANGED"

.field public static final cA:I = 0x3f

.field public static final cB:I = 0x40

.field public static final cC:I = 0x41

.field public static final cD:I = 0x42

.field public static final cE:I = 0x43

.field public static final cF:I = 0x44

.field public static final cG:I = 0x45

.field public static final cH:I = 0x46

.field public static final cI:I = 0x47

.field public static final cJ:I = 0x48

.field public static final cK:I = 0x64

.field public static final cL:I = 0x65

.field public static final cM:I = 0x66

.field public static final cN:I = 0x69

.field public static final cO:I = 0x6b

.field public static final cP:I = 0x6d

.field public static final cQ:I = 0x6f

.field public static final cR:I = 0x71

.field public static final cS:I = 0x72

.field public static final cT:I = 0x74

.field public static final cU:I = 0x76

.field public static final cV:Ljava/lang/String; = "multi_point_edit_id"

.field public static final cW:Ljava/lang/String; = "latlong"

.field public static final cX:Ljava/lang/String; = "locationName"

.field public static final cY:Ljava/lang/String; = "com.sec.android.geolookout.querylocation"

.field public static final cZ:Ljava/lang/String; = "query_result"

.field public static final ca:Ljava/lang/String; = "TWC.txt"

.field public static final cb:Z = false

.field public static final cc:Ljava/lang/String; = "com.samsung.android.app.geonews.GEONEWS_AUTO_TEST"

.field public static final cd:Ljava/lang/String; = "result"

.field public static final ce:Ljava/lang/String; = "error_code"

.field public static final cf:Ljava/lang/String; = "error_message"

.field public static final cg:Ljava/lang/String; = "response_state"

.field public static final ch:Ljava/lang/String; = "response_time"

.field public static final ci:I = 0x0

.field public static final cj:I = 0x1

.field public static final ck:I = 0x2

.field public static final cl:I = 0x3

.field public static final cm:I = 0x4

.field public static final cn:I = 0x32

.field public static final co:I = 0x33

.field public static final cp:I = 0x34

.field public static final cq:I = 0x35

.field public static final cr:I = 0x36

.field public static final cs:I = 0x37

.field public static final ct:I = 0x38

.field public static final cu:I = 0x39

.field public static final cv:I = 0x3a

.field public static final cw:I = 0x3b

.field public static final cx:I = 0x3c

.field public static final cy:I = 0x3d

.field public static final cz:I = 0x3e

.field public static final d:Ljava/lang/String; = "com.sec.android.GeoLookout.SETTING_CHANGE_START"

.field public static final da:Ljava/lang/String; = "query_zone"

.field public static final db:Ljava/lang/String; = "query_county"

.field public static final dc:Ljava/lang/String; = "com.sec.android.geolookout.updatelocation"

.field public static final dd:Ljava/lang/String; = "com.sec.android.geolookout.resultlocation"

.field public static final de:Ljava/lang/String; = "com.sec.android.geolookout.resultrefresh"

.field public static final df:Ljava/lang/String; = "update_result"

.field public static final dg:I = -0xf

.field public static final dh:I = 0x0

.field public static final di:I = 0x1

.field public static final dj:I = 0x2

.field public static final e:Ljava/lang/String; = "com.sec.android.GeoLookout.SETTING_CHANGE_FINISH"

.field public static final f:Ljava/lang/String; = "com.sec.android.GeoLookout.SHOW_DISASTER"

.field public static final g:Ljava/lang/String; = "com.sec.android.GeoLookout.REMOVE_NOTIFICATION"

.field public static final h:Ljava/lang/String; = "com.samsung.accessory.intent.action.LAUNCH_NOTIFICATION_ITEM"

.field public static final i:Ljava/lang/String; = "com.samsung.accessory.intent.action.DISASTER_SVIEW_COVER"

.field public static final j:Ljava/lang/String; = "com.samsung.accessory.intent.action.CHECK_NOTIFICATION_ITEM"

.field public static final k:Ljava/lang/String; = "com.samsung.accessory.intent.action.UPDATE_NOTIFICATION_ITEM"

.field public static final l:Ljava/lang/String; = "com.sec.android.GeoLookout.DisasterAlertService.CheckLocation"

.field public static final m:Ljava/lang/String; = "com.sec.android.GeoLookout.DisasterAlertService.CancelCheckLocationAlarm"

.field public static final n:Ljava/lang/String; = "com.sec.android.GeoLookout.DisasterAlertService.CheckNotification"

.field public static final o:Ljava/lang/String; = "com.sec.android.intent.action.HOME_RESUME"

.field public static final p:Ljava/lang/String; = "android.appwidget.action.APPWIDGET_UPDATE"

.field public static final q:Ljava/lang/String; = "android.appwidget.action.APPWIDGET_UPDATE_OPTIONS"

.field public static final r:Ljava/lang/String; = "android.appwidget.action.NEW_PUSH_RECIEVE"

.field public static final s:Ljava/lang/String; = "com.sec.android.ACTION_CHECK_DAYS"

.field public static final t:Ljava/lang/String; = "com.sec.android.ACTION_REMOVE_ANIMATION"

.field public static final u:Ljava/lang/String; = "android.intent.action.PACKAGE_DATA_CLEARED"

.field public static final v:Ljava/lang/String; = "com.sec.android.safetywidget.ACTION_SHOW_TOAST_FOR_REGISTER"

.field public static final w:Ljava/lang/String; = "com.sec.android.GeoLookout.widget.ACTION_DISASTER_REFRESH_DATA"

.field public static final x:Ljava/lang/String; = "com.sec.android.GeoLookout.widget.ACTION_DISASTER_REFRESH_REMOVE_ANIMATION"

.field public static final y:Ljava/lang/String; = "com.sec.android.GeoLookout.widget.ACTION_GEONEWS_SHOW_NEXT"

.field public static final z:Ljava/lang/String; = "from_transparent"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 258
    sget-boolean v0, Laky;->a:Z

    sput-boolean v0, Lakv;->bP:Z

    .line 259
    sget-boolean v0, Laky;->b:Z

    sput-boolean v0, Lakv;->bQ:Z

    .line 260
    sget-boolean v0, Laky;->c:Z

    sput-boolean v0, Lakv;->bR:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
