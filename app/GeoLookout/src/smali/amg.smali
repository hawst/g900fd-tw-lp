.class public Lamg;
.super Ljava/lang/Object;
.source "MultiPointDbTable.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# static fields
.field public static final a:Ljava/lang/String; = "TITLE"

.field public static final b:Ljava/lang/String; = "CONTACT_URI"

.field public static final c:Ljava/lang/String; = "LATITUDE"

.field public static final d:Ljava/lang/String; = "LONGITUDE"

.field public static final e:Ljava/lang/String; = "SLOT"

.field public static final f:Ljava/lang/String; = "ZONE"

.field public static final g:Ljava/lang/String; = "COUNTY"

.field public static final h:I = 0x0

.field public static final i:I = 0x1

.field public static final j:I = 0x2

.field public static final k:I = 0x3

.field public static final l:I = 0x4

.field public static final m:I = 0x5

.field public static final n:I = 0x6

.field public static final o:I = 0x7

.field protected static p:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static q:Ljava/lang/String;

.field protected static r:Ljava/lang/String;

.field public static s:Ljava/lang/String;

.field protected static t:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lamg;->p:Ljava/util/HashMap;

    .line 37
    sget-object v0, Lamg;->p:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v0, Lamg;->p:Ljava/util/HashMap;

    const-string v1, "TITLE"

    const-string v2, "TITLE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v0, Lamg;->p:Ljava/util/HashMap;

    const-string v1, "CONTACT_URI"

    const-string v2, "CONTACT_URI"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v0, Lamg;->p:Ljava/util/HashMap;

    const-string v1, "LATITUDE"

    const-string v2, "LATITUDE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lamg;->p:Ljava/util/HashMap;

    const-string v1, "LONGITUDE"

    const-string v2, "LONGITUDE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lamg;->p:Ljava/util/HashMap;

    const-string v1, "SLOT"

    const-string v2, "SLOT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lamg;->p:Ljava/util/HashMap;

    const-string v1, "ZONE"

    const-string v2, "ZONE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lamg;->p:Ljava/util/HashMap;

    const-string v1, "COUNTY"

    const-string v2, "COUNTY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    const-string v0, "CREATE TABLE MULTI_POINT_DISASTER_TBL (_id INTEGER PRIMARY KEY,TITLE TEXT,CONTACT_URI TEXT,LATITUDE DOUBLE,LONGITUDE DOUBLE,SLOT INTEGER,ZONE TEXT,COUNTY TEXT"

    sput-object v0, Lamg;->q:Ljava/lang/String;

    .line 55
    const-string v0, ");"

    sput-object v0, Lamg;->s:Ljava/lang/String;

    .line 57
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "TITLE"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "CONTACT_URI"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "LATITUDE"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "LONGITUDE"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "SLOT"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ZONE"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "COUNTY"

    aput-object v2, v0, v1

    sput-object v0, Lamg;->t:[Ljava/lang/String;

    .line 67
    const-string v0, "_id,TITLE,CONTACT_URI,LATITUDE,LONGITUDE"

    sput-object v0, Lamg;->r:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lamg;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lamg;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 77
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCreateDbTableString : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 78
    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lamg;->r:Ljava/lang/String;

    return-object v0
.end method
