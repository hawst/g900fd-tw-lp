.class public Lxc;
.super Landroid/support/v4/app/Fragment;


# instance fields
.field private final a:Lxe;

.field private b:Lvs;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Lxe;

    invoke-direct {v0, p0}, Lxe;-><init>(Landroid/support/v4/app/Fragment;)V

    iput-object v0, p0, Lxc;->a:Lxe;

    return-void
.end method

.method public static a()Lxc;
    .locals 1

    new-instance v0, Lxc;

    invoke-direct {v0}, Lxc;-><init>()V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/maps/GoogleMapOptions;)Lxc;
    .locals 3

    new-instance v0, Lxc;

    invoke-direct {v0}, Lxc;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "MapOptions"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v0, v1}, Lxc;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method protected b()Lxq;
    .locals 1

    iget-object v0, p0, Lxc;->a:Lxe;

    invoke-virtual {v0}, Lxe;->g()V

    iget-object v0, p0, Lxc;->a:Lxe;

    invoke-virtual {v0}, Lxe;->a()Ldg;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lxc;->a:Lxe;

    invoke-virtual {v0}, Lxe;->a()Ldg;

    move-result-object v0

    check-cast v0, Lxd;

    invoke-virtual {v0}, Lxd;->f()Lxq;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Lvs;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lxc;->b()Lxq;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    invoke-interface {v1}, Lxq;->a()Lxk;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lxc;->b:Lvs;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lxc;->b:Lvs;

    invoke-virtual {v0}, Lvs;->a()Lxk;

    move-result-object v0

    invoke-interface {v0}, Lxk;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v1}, Lxk;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    if-eq v0, v2, :cond_3

    :cond_2
    new-instance v0, Lvs;

    invoke-direct {v0, v1}, Lvs;-><init>(Lxk;)V

    iput-object v0, p0, Lxc;->b:Lvs;

    :cond_3
    iget-object v0, p0, Lxc;->b:Lvs;

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    const-class v0, Lxc;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    iget-object v0, p0, Lxc;->a:Lxe;

    invoke-static {v0, p1}, Lxe;->a(Lxe;Landroid/app/Activity;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lxc;->a:Lxe;

    invoke-virtual {v0, p1}, Lxe;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lxc;->a:Lxe;

    invoke-virtual {v0, p1, p2, p3}, Lxe;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lxc;->a:Lxe;

    invoke-virtual {v0}, Lxe;->e()V

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onDestroyView()V
    .locals 1

    iget-object v0, p0, Lxc;->a:Lxe;

    invoke-virtual {v0}, Lxe;->d()V

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    return-void
.end method

.method public onInflate(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onInflate(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    iget-object v0, p0, Lxc;->a:Lxe;

    invoke-static {v0, p1}, Lxe;->a(Lxe;Landroid/app/Activity;)V

    invoke-static {p1, p2}, Lcom/google/android/gms/maps/GoogleMapOptions;->a(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/gms/maps/GoogleMapOptions;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "MapOptions"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lxc;->a:Lxe;

    invoke-virtual {v0, p1, v1, p3}, Lxe;->a(Landroid/app/Activity;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-void
.end method

.method public onLowMemory()V
    .locals 1

    iget-object v0, p0, Lxc;->a:Lxe;

    invoke-virtual {v0}, Lxe;->f()V

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onLowMemory()V

    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lxc;->a:Lxe;

    invoke-virtual {v0}, Lxe;->c()V

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    iget-object v0, p0, Lxc;->a:Lxe;

    invoke-virtual {v0}, Lxe;->b()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    const-class v0, Lxc;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lxc;->a:Lxe;

    invoke-virtual {v0, p1}, Lxe;->b(Landroid/os/Bundle;)V

    return-void
.end method

.method public setArguments(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-void
.end method
