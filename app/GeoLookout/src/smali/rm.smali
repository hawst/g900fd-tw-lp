.class final Lrm;
.super Lrb;


# instance fields
.field private a:Lvi;

.field private b:Lvj;

.field private c:Lrj;


# direct methods
.method public constructor <init>(Lvi;Lrj;)V
    .locals 1

    invoke-direct {p0}, Lrb;-><init>()V

    iput-object p1, p0, Lrm;->a:Lvi;

    const/4 v0, 0x0

    iput-object v0, p0, Lrm;->b:Lvj;

    iput-object p2, p0, Lrm;->c:Lrj;

    return-void
.end method

.method public constructor <init>(Lvj;Lrj;)V
    .locals 1

    invoke-direct {p0}, Lrb;-><init>()V

    iput-object p1, p0, Lrm;->b:Lvj;

    const/4 v0, 0x0

    iput-object v0, p0, Lrm;->a:Lvi;

    iput-object p2, p0, Lrm;->c:Lrj;

    return-void
.end method


# virtual methods
.method public a(ILandroid/app/PendingIntent;)V
    .locals 8

    const/4 v7, 0x0

    iget-object v0, p0, Lrm;->c:Lrj;

    if-nez v0, :cond_0

    const-string v0, "LocationClientImpl"

    const-string v1, "onRemoveGeofencesByPendingIntentResult called multiple times"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lrm;->c:Lrj;

    new-instance v0, Lro;

    iget-object v1, p0, Lrm;->c:Lrj;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    iget-object v3, p0, Lrm;->b:Lvj;

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lro;-><init>(Lrj;ILvj;ILandroid/app/PendingIntent;)V

    invoke-virtual {v6, v0}, Lrj;->a(Lnb;)V

    iput-object v7, p0, Lrm;->c:Lrj;

    iput-object v7, p0, Lrm;->a:Lvi;

    iput-object v7, p0, Lrm;->b:Lvj;

    goto :goto_0
.end method

.method public a(I[Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lrm;->c:Lrj;

    if-nez v0, :cond_0

    const-string v0, "LocationClientImpl"

    const-string v1, "onAddGeofenceResult called multiple times"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lrm;->c:Lrj;

    new-instance v1, Lrl;

    iget-object v2, p0, Lrm;->c:Lrj;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v3, p0, Lrm;->a:Lvi;

    invoke-direct {v1, v2, v3, p1, p2}, Lrl;-><init>(Lrj;Lvi;I[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lrj;->a(Lnb;)V

    iput-object v4, p0, Lrm;->c:Lrj;

    iput-object v4, p0, Lrm;->a:Lvi;

    iput-object v4, p0, Lrm;->b:Lvj;

    goto :goto_0
.end method

.method public b(I[Ljava/lang/String;)V
    .locals 8

    const/4 v7, 0x0

    iget-object v0, p0, Lrm;->c:Lrj;

    if-nez v0, :cond_0

    const-string v0, "LocationClientImpl"

    const-string v1, "onRemoveGeofencesByRequestIdsResult called multiple times"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v6, p0, Lrm;->c:Lrj;

    new-instance v0, Lro;

    iget-object v1, p0, Lrm;->c:Lrj;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x2

    iget-object v3, p0, Lrm;->b:Lvj;

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lro;-><init>(Lrj;ILvj;I[Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Lrj;->a(Lnb;)V

    iput-object v7, p0, Lrm;->c:Lrj;

    iput-object v7, p0, Lrm;->a:Lvi;

    iput-object v7, p0, Lrm;->b:Lvj;

    goto :goto_0
.end method
