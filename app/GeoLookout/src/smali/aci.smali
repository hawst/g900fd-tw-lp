.class public Laci;
.super Ljava/lang/Object;

# interfaces
.implements Lca;


# instance fields
.field final a:Lsq;


# direct methods
.method constructor <init>(Lsq;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Laci;->a:Lsq;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0}, Lsq;->a()V

    return-void
.end method

.method public a(Lack;)V
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0, p1}, Lsq;->a(Lack;)V

    return-void
.end method

.method public a(Lacl;)V
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0, p1}, Lsq;->a(Lacl;)V

    return-void
.end method

.method public a(Lacl;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    iget-object v0, p0, Laci;->a:Lsq;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lsq;->a(Lacl;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lacm;ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0, p1, p2, p3}, Lsq;->a(Lacm;ILjava/lang/String;)V

    return-void
.end method

.method public a(Lacm;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0, p1, p2}, Lsq;->a(Lacm;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lacm;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lacm;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0, p1, p2}, Lsq;->a(Lacm;Ljava/util/Collection;)V

    return-void
.end method

.method public varargs a(Lacm;[Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0, p1, p2}, Lsq;->a(Lacm;[Ljava/lang/String;)V

    return-void
.end method

.method public a(Lacz;)V
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0, p1}, Lsq;->a(Lacz;)V

    return-void
.end method

.method public a(Lcb;)V
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0, p1}, Lsq;->a(Lcb;)V

    return-void
.end method

.method public a(Lcc;)V
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0, p1}, Lsq;->a(Lcc;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0, p1}, Lsq;->a(Ljava/lang/String;)V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0}, Lsq;->b()Z

    move-result v0

    return v0
.end method

.method public b(Lcb;)Z
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0, p1}, Lsq;->b(Lcb;)Z

    move-result v0

    return v0
.end method

.method public b(Lcc;)Z
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0, p1}, Lsq;->b(Lcc;)Z

    move-result v0

    return v0
.end method

.method public c(Lcb;)V
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0, p1}, Lsq;->c(Lcb;)V

    return-void
.end method

.method public c(Lcc;)V
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0, p1}, Lsq;->c(Lcc;)V

    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0}, Lsq;->c()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0}, Lsq;->d()V

    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0}, Lsq;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()Ladc;
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0}, Lsq;->h()Ladc;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    invoke-virtual {v0}, Lsq;->p()V

    return-void
.end method

.method h()Lsq;
    .locals 1

    iget-object v0, p0, Laci;->a:Lsq;

    return-object v0
.end method
