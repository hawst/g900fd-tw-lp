.class public Laph;
.super Lapf;
.source "RefreshDisasterDataFromServerTask.java"


# instance fields
.field private b:Landroid/content/Context;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lalo;Lalr;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lapf;-><init>(Landroid/content/Context;Lalo;Lalr;)V

    .line 30
    iput-object p1, p0, Laph;->b:Landroid/content/Context;

    .line 31
    return-void
.end method

.method private e(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Laph;->b:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lall;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    const-string v0, "[refresh] handleNewPushMessage : Push message has different dv from current dv"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 144
    :goto_0
    return-void

    .line 127
    :cond_0
    const-string v0, "[refresh] handleNewPushMessage : create validate Disasterinfo"

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 128
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Laph;->b(Lcom/sec/android/GeoLookout/db/DisasterInfo;Z)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 130
    if-nez v0, :cond_1

    .line 131
    const-string v0, "[Refresh] handleNewPushMessage : createValidDisasterInfo return null"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 135
    :cond_1
    const-string v1, "[refresh] handleNewPushMessage : save disaster info to DB"

    invoke-static {v1}, Lalj;->a(Ljava/lang/String;)V

    .line 136
    invoke-virtual {p0, v0}, Laph;->c(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 138
    if-nez v0, :cond_2

    .line 139
    const-string v0, "[Refresh] handleNewPushMessage : saveValidDisasterInfo return null"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 143
    :cond_2
    invoke-direct {p0, v0}, Laph;->f(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    goto :goto_0
.end method

.method private f(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Laph;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 149
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laph;->c:Ljava/util/ArrayList;

    .line 152
    :cond_0
    iget-object v0, p0, Laph;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 153
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laph;->d:Ljava/util/ArrayList;

    .line 156
    :cond_1
    iget-object v0, p0, Laph;->c:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[refresh] addNotificationTrackingList REMOVE  : did "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Laph;->c:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 161
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[refresh] addNotificationTrackingList ADD  : did "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Laph;->c:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getPolling()S

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[refresh] addNotificationTrackingList tracking  : did "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Laph;->d:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    :cond_3
    return-void
.end method

.method private g(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 2

    .prologue
    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[refresh] deleteNotificationTrackingList : did "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Laph;->c:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 174
    iget-object v0, p0, Laph;->d:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 175
    return-void
.end method


# virtual methods
.method b(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 0

    .prologue
    .line 117
    invoke-super {p0, p1}, Lapf;->b(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 118
    invoke-direct {p0, p1}, Laph;->g(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 119
    return-void
.end method

.method public run()V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 35
    iget-object v0, p0, Laph;->b:Landroid/content/Context;

    invoke-static {v0}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laph;->b:Landroid/content/Context;

    invoke-static {v0}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 36
    :cond_0
    const-string v0, "GeoNews is OFF or DIM : ignore the refresh message"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 113
    :goto_0
    return-void

    .line 40
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 41
    invoke-virtual {p0}, Laph;->b()Lalr;

    move-result-object v0

    invoke-virtual {v0}, Lalr;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Laph;->b:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/db/DisasterParser;->a(Ljava/lang/String;Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v3

    .line 43
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_3

    .line 44
    :cond_2
    const-string v0, "[refresh] disasterInfos null"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Laph;->b:Landroid/content/Context;

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Laph;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Lage;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 49
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laph;->c:Ljava/util/ArrayList;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laph;->d:Ljava/util/ArrayList;

    .line 53
    invoke-virtual {p0}, Laph;->b()Lalr;

    move-result-object v0

    invoke-virtual {v0}, Lalr;->y()I

    move-result v4

    .line 56
    iget-object v0, p0, Laph;->b:Landroid/content/Context;

    invoke-static {v0, v4}, Lalx;->c(Landroid/content/Context;I)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 57
    if-eqz v0, :cond_4

    .line 58
    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getReceivedTime()J

    move-result-wide v0

    iput-wide v0, p0, Laph;->e:J

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Refresh] lastPushedTime :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v6, p0, Laph;->e:J

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    :cond_4
    move v1, v2

    .line 63
    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 64
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    .line 66
    new-instance v5, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    invoke-direct {v5, v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;-><init>(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 67
    invoke-virtual {v5, v4}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setMultiPointUid(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->build()Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 69
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[Refresh] disaster info ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] /////////////////////////////////////////////"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lalj;->c(Ljava/lang/String;)V

    .line 70
    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lalj;->c(Ljava/lang/String;)V

    .line 72
    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getActionStatus()I

    move-result v5

    const/4 v6, 0x3

    if-eq v5, v6, :cond_5

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getEndTime()J

    move-result-wide v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    cmp-long v5, v6, v8

    if-gez v5, :cond_6

    .line 73
    :cond_5
    invoke-virtual {p0, v0}, Laph;->b(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 63
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 75
    :cond_6
    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getActionStatus()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_9

    .line 76
    const-string v5, "04"

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    const-string v5, "03"

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 77
    :cond_7
    invoke-virtual {p0, v0}, Laph;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    goto :goto_2

    .line 79
    :cond_8
    invoke-direct {p0, v0}, Laph;->e(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    goto :goto_2

    .line 82
    :cond_9
    invoke-direct {p0, v0}, Laph;->e(Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    goto :goto_2

    .line 86
    :cond_a
    iget-object v0, p0, Laph;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_c

    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mNotificationList.size() is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Laph;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    move v1, v2

    .line 89
    :goto_3
    iget-object v0, p0, Laph;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 90
    iget-object v3, p0, Laph;->b:Landroid/content/Context;

    iget-object v0, p0, Laph;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    int-to-long v6, v4

    invoke-static {v3, v0, v6, v7}, Lalx;->a(Landroid/content/Context;Ljava/lang/String;J)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 91
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[refresh] notifyPushMessage: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " type: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " ReceivedTime: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getReceivedTime()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lalj;->c(Ljava/lang/String;)V

    .line 93
    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getReceivedTime()J

    move-result-wide v6

    iget-wide v8, p0, Laph;->e:J

    cmp-long v3, v6, v8

    if-lez v3, :cond_b

    .line 94
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[refresh] showDisasterAlertNotification!! "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getReceivedTime()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v6, p0, Laph;->e:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lalj;->b(Ljava/lang/String;)V

    .line 95
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v3

    iget-object v5, p0, Laph;->b:Landroid/content/Context;

    invoke-virtual {v3, v5, v0}, Lalu;->a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 89
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_3

    .line 97
    :cond_b
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[refresh] showDisasterAlertNotification no!! "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getReceivedTime()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v6, p0, Laph;->e:J

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_4

    .line 103
    :cond_c
    iget-object v0, p0, Laph;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_d

    move v1, v2

    .line 104
    :goto_5
    iget-object v0, p0, Laph;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_d

    .line 105
    iget-object v3, p0, Laph;->b:Landroid/content/Context;

    iget-object v0, p0, Laph;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v3, v0}, Lalx;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 107
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[refresh] requestTrackingInformation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 104
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 112
    :cond_d
    iget-object v0, p0, Laph;->b:Landroid/content/Context;

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Laph;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Lage;->a(Landroid/content/Context;I)V

    goto/16 :goto_0
.end method
