.class public final Lad;
.super Ljava/lang/Object;

# interfaces
.implements Lq;
.implements Ls;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lq",
        "<",
        "Lbg;",
        "Lal;",
        ">;",
        "Ls",
        "<",
        "Lbg;",
        "Lal;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/view/View;

.field private b:Lag;

.field private c:Lai;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not instantiate custom event adapter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Llm;->e(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lad;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lad;->a(Landroid/view/View;)V

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lad;->a:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lad;->b:Lag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lad;->b:Lag;

    invoke-interface {v0}, Lag;->a()V

    :cond_0
    iget-object v0, p0, Lad;->c:Lai;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lad;->c:Lai;

    invoke-interface {v0}, Lai;->a()V

    :cond_1
    return-void
.end method

.method public a(Lr;Landroid/app/Activity;Lal;Lm;Lo;Lbg;)V
    .locals 8

    iget-object v0, p3, Lal;->b:Ljava/lang/String;

    invoke-static {v0}, Lad;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lag;

    iput-object v0, p0, Lad;->b:Lag;

    iget-object v0, p0, Lad;->b:Lag;

    if-nez v0, :cond_0

    sget-object v0, Lk;->d:Lk;

    invoke-interface {p1, p0, v0}, Lr;->a(Lq;Lk;)V

    :goto_0
    return-void

    :cond_0
    if-nez p6, :cond_1

    const/4 v7, 0x0

    :goto_1
    iget-object v0, p0, Lad;->b:Lag;

    new-instance v1, Lae;

    invoke-direct {v1, p0, p1}, Lae;-><init>(Lad;Lr;)V

    iget-object v3, p3, Lal;->a:Ljava/lang/String;

    iget-object v4, p3, Lal;->c:Ljava/lang/String;

    move-object v2, p2

    move-object v5, p4

    move-object v6, p5

    invoke-interface/range {v0 .. v7}, Lag;->a(Lah;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lm;Lo;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v0, p3, Lal;->a:Ljava/lang/String;

    invoke-virtual {p6, v0}, Lbg;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    goto :goto_1
.end method

.method public bridge synthetic a(Lr;Landroid/app/Activity;Lu;Lm;Lo;Lx;)V
    .locals 7

    move-object v3, p3

    check-cast v3, Lal;

    move-object v6, p6

    check-cast v6, Lbg;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lad;->a(Lr;Landroid/app/Activity;Lal;Lm;Lo;Lbg;)V

    return-void
.end method

.method public a(Lt;Landroid/app/Activity;Lal;Lo;Lbg;)V
    .locals 7

    iget-object v0, p3, Lal;->b:Ljava/lang/String;

    invoke-static {v0}, Lad;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lai;

    iput-object v0, p0, Lad;->c:Lai;

    iget-object v0, p0, Lad;->c:Lai;

    if-nez v0, :cond_0

    sget-object v0, Lk;->d:Lk;

    invoke-interface {p1, p0, v0}, Lt;->a(Ls;Lk;)V

    :goto_0
    return-void

    :cond_0
    if-nez p5, :cond_1

    const/4 v6, 0x0

    :goto_1
    iget-object v0, p0, Lad;->c:Lai;

    new-instance v1, Laf;

    invoke-direct {v1, p0, p0, p1}, Laf;-><init>(Lad;Lad;Lt;)V

    iget-object v3, p3, Lal;->a:Ljava/lang/String;

    iget-object v4, p3, Lal;->c:Ljava/lang/String;

    move-object v2, p2

    move-object v5, p4

    invoke-interface/range {v0 .. v6}, Lai;->a(Laj;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lo;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v0, p3, Lal;->a:Ljava/lang/String;

    invoke-virtual {p5, v0}, Lbg;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    goto :goto_1
.end method

.method public bridge synthetic a(Lt;Landroid/app/Activity;Lu;Lo;Lx;)V
    .locals 6

    move-object v3, p3

    check-cast v3, Lal;

    move-object v5, p5

    check-cast v5, Lbg;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lad;->a(Lt;Landroid/app/Activity;Lal;Lo;Lbg;)V

    return-void
.end method

.method public b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lbg;",
            ">;"
        }
    .end annotation

    const-class v0, Lbg;

    return-object v0
.end method

.method public c()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lal;",
            ">;"
        }
    .end annotation

    const-class v0, Lal;

    return-object v0
.end method

.method public d()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lad;->a:Landroid/view/View;

    return-object v0
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lad;->c:Lai;

    invoke-interface {v0}, Lai;->b()V

    return-void
.end method
