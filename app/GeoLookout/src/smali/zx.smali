.class public final Lzx;
.super Ljava/lang/Object;


# static fields
.field public static final a:F = 0.0f

.field public static final b:F = 30.0f

.field public static final c:F = 60.0f

.field public static final d:F = 120.0f

.field public static final e:F = 180.0f

.field public static final f:F = 210.0f

.field public static final g:F = 240.0f

.field public static final h:F = 270.0f

.field public static final i:F = 300.0f

.field public static final j:F = 330.0f

.field private static k:Labi;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lzw;
    .locals 2

    :try_start_0
    new-instance v0, Lzw;

    invoke-static {}, Lzx;->b()Labi;

    move-result-object v1

    invoke-interface {v1}, Labi;->a()Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lzw;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(F)Lzw;
    .locals 2

    :try_start_0
    new-instance v0, Lzw;

    invoke-static {}, Lzx;->b()Labi;

    move-result-object v1

    invoke-interface {v1, p0}, Labi;->a(F)Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lzw;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(I)Lzw;
    .locals 2

    :try_start_0
    new-instance v0, Lzw;

    invoke-static {}, Lzx;->b()Labi;

    move-result-object v1

    invoke-interface {v1, p0}, Labi;->a(I)Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lzw;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(Landroid/graphics/Bitmap;)Lzw;
    .locals 2

    :try_start_0
    new-instance v0, Lzw;

    invoke-static {}, Lzx;->b()Labi;

    move-result-object v1

    invoke-interface {v1, p0}, Labi;->a(Landroid/graphics/Bitmap;)Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lzw;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(Ljava/lang/String;)Lzw;
    .locals 2

    :try_start_0
    new-instance v0, Lzw;

    invoke-static {}, Lzx;->b()Labi;

    move-result-object v1

    invoke-interface {v1, p0}, Labi;->a(Ljava/lang/String;)Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lzw;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(Labi;)V
    .locals 1

    sget-object v0, Lzx;->k:Labi;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lnx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labi;

    sput-object v0, Lzx;->k:Labi;

    goto :goto_0
.end method

.method private static b()Labi;
    .locals 2

    sget-object v0, Lzx;->k:Labi;

    const-string v1, "IBitmapDescriptorFactory is not initialized"

    invoke-static {v0, v1}, Lnx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labi;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lzw;
    .locals 2

    :try_start_0
    new-instance v0, Lzw;

    invoke-static {}, Lzx;->b()Labi;

    move-result-object v1

    invoke-interface {v1, p0}, Labi;->b(Ljava/lang/String;)Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lzw;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static c(Ljava/lang/String;)Lzw;
    .locals 2

    :try_start_0
    new-instance v0, Lzw;

    invoke-static {}, Lzx;->b()Labi;

    move-result-object v1

    invoke-interface {v1, p0}, Labi;->c(Ljava/lang/String;)Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lzw;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
