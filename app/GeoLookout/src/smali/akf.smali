.class public Lakf;
.super Landroid/preference/PreferenceFragment;
.source "DisasterSettingsFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field public static final a:Ljava/lang/String; = "com.sec.android.geolookout.Registered"

.field private static final c:Ljava/lang/String; = "earthquake"


# instance fields
.field private final A:F

.field private B:Landroid/database/ContentObserver;

.field private C:Landroid/database/ContentObserver;

.field private final D:Landroid/content/BroadcastReceiver;

.field final b:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:I

.field private h:I

.field private i:Landroid/app/Activity;

.field private j:Landroid/view/View;

.field private k:Lakr;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/ImageView;

.field private n:Landroid/widget/ImageView;

.field private o:Landroid/widget/ImageView;

.field private p:Landroid/widget/ImageView;

.field private q:Landroid/widget/LinearLayout;

.field private r:Landroid/widget/LinearLayout;

.field private s:Landroid/widget/ListView;

.field private t:Landroid/preference/CheckBoxPreference;

.field private u:Landroid/preference/Preference;

.field private v:Ljava/lang/String;

.field private w:F

.field private x:F

.field private y:F

.field private z:F


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 71
    const/16 v0, 0xb

    iput v0, p0, Lakf;->b:I

    .line 73
    const-string v0, "earthquake"

    iput-object v0, p0, Lakf;->d:Ljava/lang/String;

    .line 75
    const-string v0, "earthquake"

    iput-object v0, p0, Lakf;->e:Ljava/lang/String;

    .line 77
    iput v3, p0, Lakf;->f:I

    .line 79
    iput v3, p0, Lakf;->g:I

    .line 81
    iput v3, p0, Lakf;->h:I

    .line 91
    iput-object v1, p0, Lakf;->m:Landroid/widget/ImageView;

    .line 93
    iput-object v1, p0, Lakf;->n:Landroid/widget/ImageView;

    .line 95
    iput-object v1, p0, Lakf;->o:Landroid/widget/ImageView;

    .line 97
    iput-object v1, p0, Lakf;->p:Landroid/widget/ImageView;

    .line 99
    iput-object v1, p0, Lakf;->q:Landroid/widget/LinearLayout;

    .line 101
    iput-object v1, p0, Lakf;->r:Landroid/widget/LinearLayout;

    .line 103
    iput-object v1, p0, Lakf;->s:Landroid/widget/ListView;

    .line 109
    iput-object v1, p0, Lakf;->v:Ljava/lang/String;

    .line 111
    iput v2, p0, Lakf;->w:F

    .line 113
    iput v2, p0, Lakf;->x:F

    .line 115
    iput v2, p0, Lakf;->y:F

    .line 117
    iput v2, p0, Lakf;->z:F

    .line 119
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lakf;->A:F

    .line 121
    new-instance v0, Lakg;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lakg;-><init>(Lakf;Landroid/os/Handler;)V

    iput-object v0, p0, Lakf;->B:Landroid/database/ContentObserver;

    .line 130
    new-instance v0, Lakj;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lakj;-><init>(Lakf;Landroid/os/Handler;)V

    iput-object v0, p0, Lakf;->C:Landroid/database/ContentObserver;

    .line 148
    new-instance v0, Lakk;

    invoke-direct {v0, p0}, Lakk;-><init>(Lakf;)V

    iput-object v0, p0, Lakf;->D:Landroid/content/BroadcastReceiver;

    .line 630
    return-void
.end method

.method static synthetic a(Lakf;F)F
    .locals 0

    .prologue
    .line 65
    iput p1, p0, Lakf;->w:F

    return p1
.end method

.method static synthetic a(Lakf;I)I
    .locals 0

    .prologue
    .line 65
    iput p1, p0, Lakf;->h:I

    return p1
.end method

.method static synthetic a(Lakf;)Lakr;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lakf;->k:Lakr;

    return-object v0
.end method

.method private a()V
    .locals 6

    .prologue
    const v5, 0x7f02001a

    const v4, 0x7f020019

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 352
    iget v0, p0, Lakf;->h:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 354
    iget-object v0, p0, Lakf;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 355
    iget-object v0, p0, Lakf;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Lakf;->o:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lakf;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 359
    :cond_0
    iget-object v0, p0, Lakf;->p:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 360
    iget-object v0, p0, Lakf;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 362
    :cond_1
    iget-object v0, p0, Lakf;->l:Landroid/widget/TextView;

    const v1, 0x7f0a04b9

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 376
    :goto_0
    return-void

    .line 365
    :cond_2
    iget-object v0, p0, Lakf;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 366
    iget-object v0, p0, Lakf;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 368
    iget-object v0, p0, Lakf;->o:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 369
    iget-object v0, p0, Lakf;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 371
    :cond_3
    iget-object v0, p0, Lakf;->p:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 372
    iget-object v0, p0, Lakf;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 374
    :cond_4
    iget-object v0, p0, Lakf;->l:Landroid/widget/TextView;

    const v1, 0x7f0a04b7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method static synthetic a(Lakf;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lakf;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 620
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "safetycare_geolookout_registering"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 623
    :cond_0
    if-eqz v0, :cond_1

    .line 624
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 625
    const-string v1, "isEnable"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 626
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 628
    :cond_1
    return-void
.end method

.method private a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 558
    iget-object v0, p0, Lakf;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lakf;->v:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 559
    iget-object v0, p0, Lakf;->v:Ljava/lang/String;

    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 564
    :goto_0
    return v0

    .line 562
    :cond_0
    const-string v0, "ro.build.characteristics"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lakf;->v:Ljava/lang/String;

    .line 564
    iget-object v0, p0, Lakf;->v:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lakf;->v:Ljava/lang/String;

    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lakf;F)F
    .locals 0

    .prologue
    .line 65
    iput p1, p0, Lakf;->y:F

    return p1
.end method

.method static synthetic b(Lakf;)Landroid/preference/CheckBoxPreference;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lakf;->t:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 514
    .line 516
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lakf;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 518
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a048e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 525
    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a048f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0021

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lakp;

    invoke-direct {v2, p0}, Lakp;-><init>(Lakf;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0049

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lako;

    invoke-direct {v2, p0}, Lako;-><init>(Lakf;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lakn;

    invoke-direct {v1, p0}, Lakn;-><init>(Lakf;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 555
    return-void

    .line 521
    :cond_0
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a048d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic c(Lakf;)F
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lakf;->w:F

    return v0
.end method

.method static synthetic c(Lakf;F)F
    .locals 0

    .prologue
    .line 65
    iput p1, p0, Lakf;->x:F

    return p1
.end method

.method private c()V
    .locals 4

    .prologue
    .line 571
    .line 573
    invoke-virtual {p0}, Lakf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a001c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 574
    invoke-virtual {p0}, Lakf;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a001e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 578
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 579
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 580
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 581
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 582
    const v1, 0x7f03002e

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 583
    const v0, 0x7f0e00ee

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 585
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 586
    invoke-virtual {p0}, Lakf;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v3, 0x1040000

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lakq;

    invoke-direct {v3, p0}, Lakq;-><init>(Lakf;)V

    invoke-virtual {v2, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 592
    invoke-virtual {p0}, Lakf;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x104000a

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lakh;

    invoke-direct {v3, p0, v0}, Lakh;-><init>(Lakf;Landroid/widget/CheckBox;)V

    invoke-virtual {v2, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 610
    new-instance v0, Laki;

    invoke-direct {v0, p0}, Laki;-><init>(Lakf;)V

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 616
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 617
    return-void
.end method

.method static synthetic d(Lakf;)F
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lakf;->x:F

    return v0
.end method

.method static synthetic d(Lakf;F)F
    .locals 0

    .prologue
    .line 65
    iput p1, p0, Lakf;->z:F

    return p1
.end method

.method static synthetic e(Lakf;)F
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lakf;->y:F

    return v0
.end method

.method static synthetic f(Lakf;)F
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lakf;->z:F

    return v0
.end method

.method static synthetic g(Lakf;)I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lakf;->h:I

    return v0
.end method

.method static synthetic h(Lakf;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lakf;->a()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 274
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 276
    const v1, 0x7f030030

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 277
    const v0, 0x7f0e00fe

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lakf;->l:Landroid/widget/TextView;

    .line 278
    const v0, 0x7f0e00f9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lakf;->m:Landroid/widget/ImageView;

    .line 279
    const v0, 0x7f0e00fa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lakf;->n:Landroid/widget/ImageView;

    .line 280
    const v0, 0x7f0e00fb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lakf;->r:Landroid/widget/LinearLayout;

    .line 281
    const v0, 0x7f0e00fc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lakf;->o:Landroid/widget/ImageView;

    .line 282
    const v0, 0x7f0e00fd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lakf;->p:Landroid/widget/ImageView;

    .line 284
    invoke-virtual {p0}, Lakf;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iput-object v0, p0, Lakf;->s:Landroid/widget/ListView;

    .line 285
    iget-object v0, p0, Lakf;->s:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 286
    iget-object v0, p0, Lakf;->l:Landroid/widget/TextView;

    iget-object v2, p0, Lakf;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 288
    iget v0, p0, Lakf;->g:I

    if-eqz v0, :cond_1

    .line 289
    iget-object v0, p0, Lakf;->m:Landroid/widget/ImageView;

    iget v2, p0, Lakf;->g:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 290
    iget-object v0, p0, Lakf;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 295
    :goto_0
    sget-boolean v0, Laky;->j:Z

    if-nez v0, :cond_0

    sget-boolean v0, Laky;->b:Z

    if-eqz v0, :cond_2

    .line 296
    :cond_0
    iget-object v0, p0, Lakf;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 298
    const v0, 0x7f0e00f8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lakf;->q:Landroid/widget/LinearLayout;

    .line 299
    iget-object v0, p0, Lakf;->q:Landroid/widget/LinearLayout;

    new-instance v2, Lakm;

    invoke-direct {v2, p0}, Lakm;-><init>(Lakf;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 346
    :goto_1
    invoke-virtual {p0}, Lakf;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1, v5, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 348
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 349
    return-void

    .line 292
    :cond_1
    iget-object v0, p0, Lakf;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 343
    :cond_2
    iget-object v0, p0, Lakf;->r:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 218
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 220
    const/16 v0, 0xb

    if-ne p1, v0, :cond_6

    .line 221
    if-ne p2, v3, :cond_0

    .line 222
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, Lakf;->e:Ljava/lang/String;

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 225
    :goto_0
    iget-object v3, p0, Lakf;->k:Lakr;

    invoke-virtual {v3}, Lakr;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_5

    if-nez v0, :cond_5

    .line 228
    const-string v0, "ChinaNalSecurity"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 230
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "data_charging_Setting"

    invoke-virtual {v0, v3, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 232
    const-string v3, "data_charging_checkbox"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 234
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v4, "safetycare_geolookout_registering"

    invoke-static {v0, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 239
    :goto_1
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_3

    if-ne v0, v1, :cond_3

    .line 240
    invoke-direct {p0}, Lakf;->c()V

    .line 270
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 222
    goto :goto_0

    :cond_2
    move v0, v2

    .line 234
    goto :goto_1

    .line 245
    :cond_3
    const-string v0, "com.sec.android.GeoLookout.SETTING_CHANGE_START"

    invoke-direct {p0, v0, v1}, Lakf;->a(Ljava/lang/String;Z)V

    .line 246
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "safetycare_geolookout_registering"

    invoke-static {v0, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 248
    iget-object v0, p0, Lakf;->k:Lakr;

    invoke-virtual {v0, v2}, Lakr;->setEnabled(Z)V

    .line 258
    :cond_4
    :goto_3
    sget-boolean v0, Laky;->e:Z

    if-eqz v0, :cond_0

    .line 259
    invoke-direct {p0}, Lakf;->b()V

    goto :goto_2

    .line 249
    :cond_5
    iget-object v3, p0, Lakf;->k:Lakr;

    invoke-virtual {v3}, Lakr;->isChecked()Z

    move-result v3

    if-nez v3, :cond_4

    .line 250
    if-eqz v0, :cond_4

    .line 251
    const-string v0, "com.sec.android.GeoLookout.SETTING_CHANGE_START"

    invoke-direct {p0, v0, v2}, Lakf;->a(Ljava/lang/String;Z)V

    .line 252
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "safetycare_geolookout_registering"

    invoke-static {v0, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 254
    iget-object v0, p0, Lakf;->k:Lakr;

    invoke-virtual {v0, v2}, Lakr;->setEnabled(Z)V

    goto :goto_3

    .line 262
    :cond_6
    const/16 v0, 0x6f

    if-ne p1, v0, :cond_0

    .line 263
    if-ne p2, v3, :cond_0

    .line 264
    const-string v0, "com.sec.android.GeoLookout.SETTING_CHANGE_START"

    invoke-direct {p0, v0, v1}, Lakf;->a(Ljava/lang/String;Z)V

    .line 265
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "safetycare_geolookout_registering"

    invoke-static {v0, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 267
    iget-object v0, p0, Lakf;->k:Lakr;

    invoke-virtual {v0, v2}, Lakr;->setEnabled(Z)V

    goto :goto_2
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 451
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, Lakf;->e:Ljava/lang/String;

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 453
    :goto_0
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 455
    sget-boolean v4, Lakv;->bQ:Z

    if-eqz v4, :cond_2

    invoke-static {v3}, Lakw;->b(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 456
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 457
    const v1, 0x7f0a04ba

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v3, 0x7f0a04e3

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v3, 0x104000a

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 460
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 461
    iget-object v0, p0, Lakf;->k:Lakr;

    invoke-virtual {v0, v2}, Lakr;->setChecked(Z)V

    .line 511
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 451
    goto :goto_0

    .line 465
    :cond_2
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "safety_care_disaster_user_agree"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_3

    if-eqz p2, :cond_3

    .line 469
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 471
    const/16 v1, 0xb

    invoke-virtual {p0, v0, v1}, Lakf;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 473
    :cond_3
    if-eqz p2, :cond_7

    if-nez v0, :cond_7

    .line 476
    const-string v0, "ChinaNalSecurity"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 478
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "data_charging_Setting"

    invoke-virtual {v0, v3, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 480
    const-string v3, "data_charging_checkbox"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 482
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v4, "safetycare_geolookout_registering"

    invoke-static {v0, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 486
    :goto_2
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_5

    if-ne v0, v1, :cond_5

    .line 487
    invoke-direct {p0}, Lakf;->c()V

    goto :goto_1

    :cond_4
    move v0, v2

    .line 482
    goto :goto_2

    .line 492
    :cond_5
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lall;->x(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 493
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 494
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 495
    const/16 v1, 0x6f

    invoke-virtual {p0, v0, v1}, Lakf;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 497
    :cond_6
    const-string v0, "com.sec.android.GeoLookout.SETTING_CHANGE_START"

    invoke-direct {p0, v0, v1}, Lakf;->a(Ljava/lang/String;Z)V

    .line 498
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "safetycare_geolookout_registering"

    invoke-static {v0, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 500
    iget-object v0, p0, Lakf;->k:Lakr;

    invoke-virtual {v0, v2}, Lakr;->setEnabled(Z)V

    goto/16 :goto_1

    .line 502
    :cond_7
    if-nez p2, :cond_0

    .line 503
    if-eqz v0, :cond_0

    .line 504
    const-string v0, "com.sec.android.GeoLookout.SETTING_CHANGE_START"

    invoke-direct {p0, v0, v2}, Lakf;->a(Ljava/lang/String;Z)V

    .line 505
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "safetycare_geolookout_registering"

    invoke-static {v0, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 507
    iget-object v0, p0, Lakf;->k:Lakr;

    invoke-virtual {v0, v2}, Lakr;->setEnabled(Z)V

    goto/16 :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/16 v3, 0x10

    const/4 v4, -0x2

    const/4 v2, 0x0

    .line 164
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 166
    invoke-virtual {p0}, Lakf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 167
    const-string v1, "safetycare_earthquake"

    iput-object v1, p0, Lakf;->e:Ljava/lang/String;

    .line 168
    const v1, 0x7f0a04b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lakf;->d:Ljava/lang/String;

    .line 170
    sget-boolean v0, Lakv;->bQ:Z

    if-eqz v0, :cond_2

    .line 171
    const v0, 0x7f0a0029

    iput v0, p0, Lakf;->f:I

    .line 176
    :goto_0
    const v0, 0x7f020156

    iput v0, p0, Lakf;->g:I

    .line 177
    const v0, 0x7f050004

    invoke-virtual {p0, v0}, Lakf;->addPreferencesFromResource(I)V

    .line 179
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lakf;->i:Landroid/app/Activity;

    .line 180
    new-instance v0, Lakr;

    iget-object v1, p0, Lakf;->i:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lakr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lakf;->k:Lakr;

    .line 182
    iget-object v0, p0, Lakf;->i:Landroid/app/Activity;

    instance-of v0, v0, Landroid/preference/PreferenceActivity;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lakf;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f090000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 185
    iget-object v1, p0, Lakf;->k:Lakr;

    invoke-virtual {v1, v2, v2, v0, v2}, Lakr;->setPadding(IIII)V

    .line 186
    iget-object v0, p0, Lakf;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 188
    iget-object v0, p0, Lakf;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iget-object v1, p0, Lakf;->k:Lakr;

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    const/16 v3, 0x15

    invoke-direct {v2, v4, v4, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 193
    iget-object v0, p0, Lakf;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lakf;->j:Landroid/view/View;

    .line 196
    :cond_0
    iget-object v0, p0, Lakf;->k:Lakr;

    invoke-virtual {v0, p0}, Lakr;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 197
    iget-object v0, p0, Lakf;->i:Landroid/app/Activity;

    iget v1, p0, Lakf;->f:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(I)V

    .line 199
    const-string v0, "disaster_popup"

    invoke-virtual {p0, v0}, Lakf;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lakf;->t:Landroid/preference/CheckBoxPreference;

    .line 200
    const-string v0, "disaster_set_alert"

    invoke-virtual {p0, v0}, Lakf;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lakf;->u:Landroid/preference/Preference;

    .line 201
    iget-object v0, p0, Lakf;->u:Landroid/preference/Preference;

    new-instance v1, Lakl;

    invoke-direct {v1, p0}, Lakl;-><init>(Lakf;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 211
    sget-boolean v0, Lakv;->bQ:Z

    if-nez v0, :cond_1

    .line 212
    invoke-virtual {p0}, Lakf;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lakf;->u:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 214
    :cond_1
    return-void

    .line 173
    :cond_2
    const v0, 0x7f0a00a9

    iput v0, p0, Lakf;->f:I

    goto/16 :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 430
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 432
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lakf;->B:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 433
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lakf;->C:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 434
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lakf;->D:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 435
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 439
    iget-object v0, p0, Lakf;->t:Landroid/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_0

    .line 440
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "safetycare_disaster_popup"

    iget-object v0, p0, Lakf;->t:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 446
    :cond_0
    return v1

    .line 440
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 380
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 382
    iget-object v0, p0, Lakf;->j:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lakf;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 384
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 387
    :cond_0
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, Lakf;->e:Ljava/lang/String;

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 390
    :goto_0
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "safetycare_geolookout_registering"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_5

    move v3, v1

    .line 394
    :goto_1
    if-nez v3, :cond_1

    .line 396
    if-nez v0, :cond_6

    move v0, v1

    .line 399
    :cond_1
    :goto_2
    iget-object v4, p0, Lakf;->k:Lakr;

    invoke-virtual {v4, v0}, Lakr;->setChecked(Z)V

    .line 400
    iget-object v0, p0, Lakf;->k:Lakr;

    invoke-virtual {v0, v3}, Lakr;->setEnabled(Z)V

    .line 402
    invoke-direct {p0}, Lakf;->a()V

    .line 404
    iget-object v0, p0, Lakf;->t:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    .line 405
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "safetycare_earthquake"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_7

    .line 407
    const-string v0, "GeoNews is activated"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 408
    iget-object v0, p0, Lakf;->t:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 414
    :goto_3
    iget-object v0, p0, Lakf;->t:Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "safetycare_disaster_popup"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_2

    move v2, v1

    :cond_2
    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 418
    :cond_3
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "safetycare_earthquake"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lakf;->B:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 421
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "safetycare_geolookout_registering"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lakf;->C:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 424
    invoke-virtual {p0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lakf;->D:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.sec.android.geolookout.Registered"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 426
    return-void

    :cond_4
    move v0, v2

    .line 387
    goto/16 :goto_0

    :cond_5
    move v3, v2

    .line 390
    goto/16 :goto_1

    :cond_6
    move v0, v2

    .line 396
    goto :goto_2

    .line 410
    :cond_7
    const-string v0, "GeoNews is NOT activated"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 411
    iget-object v0, p0, Lakf;->t:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_3
.end method
