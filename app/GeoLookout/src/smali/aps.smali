.class public Laps;
.super Lapb;
.source "UpdateTrackingInfoFromServerTask.java"


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lalo;Lalr;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p2, p3}, Lapb;-><init>(Lalo;Lalr;)V

    .line 24
    iput-object p1, p0, Laps;->a:Landroid/content/Context;

    .line 25
    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 29
    invoke-virtual {p0}, Laps;->b()Lalr;

    move-result-object v0

    invoke-virtual {v0}, Lalr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/GeoLookout/db/DisasterParser;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 32
    iget-object v1, p0, Laps;->a:Landroid/content/Context;

    invoke-virtual {p0}, Laps;->b()Lalr;

    move-result-object v2

    invoke-virtual {v2}, Lalr;->t()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lama;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    iget-object v1, p0, Laps;->a:Landroid/content/Context;

    invoke-virtual {p0}, Laps;->b()Lalr;

    move-result-object v2

    invoke-virtual {v2}, Lalr;->t()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lama;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Laps;->a:Landroid/content/Context;

    invoke-virtual {p0}, Laps;->b()Lalr;

    move-result-object v1

    invoke-virtual {v1}, Lalr;->t()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lama;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 41
    iget-object v1, p0, Laps;->a:Landroid/content/Context;

    invoke-virtual {p0}, Laps;->b()Lalr;

    move-result-object v2

    invoke-virtual {v2}, Lalr;->t()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lama;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 43
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 44
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 45
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 47
    iget-object v1, p0, Laps;->a:Landroid/content/Context;

    invoke-virtual {p0}, Laps;->b()Lalr;

    move-result-object v4

    invoke-virtual {v4}, Lalr;->t()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4, v0}, Lama;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 48
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 49
    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    .line 51
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 53
    iget-object v6, p0, Laps;->a:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLinkedBeDid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lama;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    iget-object v6, p0, Laps;->a:Landroid/content/Context;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLinkedBeDid()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v4, v1}, Lama;->a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :cond_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Laps;->a:Landroid/content/Context;

    const-class v2, Lcom/sec/android/GeoLookout/DisasterService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 61
    const-string v1, "com.sec.android.GeoLookout.ACTION_HANDLE_PUSH_MESSAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 62
    const-string v1, "msg"

    invoke-virtual {p0}, Laps;->b()Lalr;

    move-result-object v2

    invoke-virtual {v2}, Lalr;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 63
    const-string v1, "istracked"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 64
    iget-object v1, p0, Laps;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 66
    return-void
.end method
