.class public Lamr;
.super Ljava/lang/Object;
.source "LifemodeValue.java"


# instance fields
.field private a:Lamq;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 257
    new-instance v0, Lamq;

    invoke-direct {v0}, Lamq;-><init>()V

    iput-object v0, p0, Lamr;->a:Lamq;

    .line 258
    iget-object v0, p0, Lamr;->a:Lamq;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lamq;->a(Lamq;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 259
    iget-object v0, p0, Lamr;->a:Lamq;

    const v1, 0x7f08008a

    invoke-static {v0, v1}, Lamq;->a(Lamq;I)I

    .line 260
    iget-object v0, p0, Lamr;->a:Lamq;

    const v1, 0x7f080015

    invoke-static {v0, v1}, Lamq;->b(Lamq;I)I

    .line 261
    return-void
.end method


# virtual methods
.method public a()Lamq;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lamr;->a:Lamq;

    return-object v0
.end method

.method public a(I)Lamr;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lamr;->a:Lamq;

    invoke-static {v0, p1}, Lamq;->c(Lamq;I)I

    .line 265
    return-object p0
.end method

.method public a(Ljava/lang/Integer;)Lamr;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lamr;->a:Lamq;

    invoke-static {v0}, Lamq;->a(Lamq;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 286
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lamr;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lamr;->a:Lamq;

    invoke-static {v0, p1}, Lamq;->a(Lamq;Ljava/lang/String;)Ljava/lang/String;

    .line 270
    return-object p0
.end method

.method public b(I)Lamr;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lamr;->a:Lamq;

    invoke-static {v0, p1}, Lamq;->d(Lamq;I)I

    .line 275
    return-object p0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 334
    new-instance v0, Lamq;

    invoke-direct {v0}, Lamq;-><init>()V

    iput-object v0, p0, Lamr;->a:Lamq;

    .line 335
    iget-object v0, p0, Lamr;->a:Lamq;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lamq;->a(Lamq;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 336
    iget-object v0, p0, Lamr;->a:Lamq;

    const v1, 0x7f08008a

    invoke-static {v0, v1}, Lamq;->a(Lamq;I)I

    .line 337
    iget-object v0, p0, Lamr;->a:Lamq;

    const v1, 0x7f080015

    invoke-static {v0, v1}, Lamq;->b(Lamq;I)I

    .line 338
    return-void
.end method

.method public c(I)Lamr;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lamr;->a:Lamq;

    invoke-static {v0, p1}, Lamq;->e(Lamq;I)I

    .line 280
    return-object p0
.end method

.method public d(I)Lamr;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lamr;->a:Lamq;

    invoke-static {v0, p1}, Lamq;->f(Lamq;I)I

    .line 291
    return-object p0
.end method

.method public e(I)Lamr;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lamr;->a:Lamq;

    invoke-static {v0, p1}, Lamq;->b(Lamq;I)I

    .line 296
    return-object p0
.end method

.method public f(I)Lamr;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lamr;->a:Lamq;

    invoke-static {v0, p1}, Lamq;->a(Lamq;I)I

    .line 301
    return-object p0
.end method

.method public g(I)Lamr;
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lamr;->a:Lamq;

    invoke-static {v0, p1}, Lamq;->g(Lamq;I)I

    .line 306
    return-object p0
.end method

.method public h(I)Lamr;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lamr;->a:Lamq;

    invoke-static {v0, p1}, Lamq;->h(Lamq;I)I

    .line 311
    return-object p0
.end method

.method public i(I)Lamr;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lamr;->a:Lamq;

    invoke-static {v0, p1}, Lamq;->i(Lamq;I)I

    .line 316
    return-object p0
.end method

.method public j(I)Lamr;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lamr;->a:Lamq;

    invoke-static {v0, p1}, Lamq;->j(Lamq;I)I

    .line 321
    return-object p0
.end method

.method public k(I)Lamr;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lamr;->a:Lamq;

    invoke-static {v0, p1}, Lamq;->k(Lamq;I)I

    .line 326
    return-object p0
.end method
