.class public Lamj;
.super Ljava/lang/Object;
.source "LifemodeCity.java"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:I

.field private d:I

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lamq;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private k:Landroid/content/Context;

.field private final l:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lamj;->g:Z

    .line 37
    new-instance v0, Lamk;

    invoke-direct {v0, p0}, Lamk;-><init>(Lamj;)V

    iput-object v0, p0, Lamj;->l:Ljava/util/Comparator;

    .line 56
    new-instance v0, Laml;

    invoke-direct {v0, p0}, Laml;-><init>(Lamj;)V

    iput-object v0, p0, Lamj;->m:Ljava/util/Comparator;

    .line 176
    return-void
.end method

.method static synthetic a(Lamj;I)I
    .locals 0

    .prologue
    .line 13
    iput p1, p0, Lamj;->a:I

    return p1
.end method

.method static synthetic a(Lamj;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 13
    iput-object p1, p0, Lamj;->b:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lamj;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 13
    iput-object p1, p0, Lamj;->i:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic a(Lamj;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lamj;->h:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic a(Lamj;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0

    .prologue
    .line 13
    iput-object p1, p0, Lamj;->h:Ljava/util/HashMap;

    return-object p1
.end method

.method static synthetic a(Lamj;Z)Z
    .locals 0

    .prologue
    .line 13
    iput-boolean p1, p0, Lamj;->g:Z

    return p1
.end method

.method static synthetic b(Lamj;I)I
    .locals 0

    .prologue
    .line 13
    iput p1, p0, Lamj;->c:I

    return p1
.end method

.method static synthetic b(Lamj;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lamj;->k:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lamj;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 13
    iput-object p1, p0, Lamj;->j:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic c(Lamj;I)I
    .locals 0

    .prologue
    .line 13
    iput p1, p0, Lamj;->d:I

    return p1
.end method

.method static synthetic d(Lamj;I)I
    .locals 0

    .prologue
    .line 13
    iput p1, p0, Lamj;->e:I

    return p1
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lamj;->i:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    iget-object v0, p0, Lamj;->j:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 86
    return-void
.end method

.method public a(ILamq;)V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lamj;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lamj;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    :cond_0
    iget-object v0, p0, Lamj;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 78
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lamj;->k:Landroid/content/Context;

    .line 79
    iget-object v0, p0, Lamj;->i:Ljava/util/ArrayList;

    iget-object v1, p0, Lamj;->l:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 80
    iget-object v0, p0, Lamj;->j:Ljava/util/ArrayList;

    iget-object v1, p0, Lamj;->m:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 81
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lamj;->f:Ljava/lang/String;

    .line 154
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lamj;->g:Z

    return v0
.end method

.method public b(I)Lamq;
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lamj;->h:Ljava/util/HashMap;

    iget-object v1, p0, Lamj;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamq;

    return-object v0
.end method

.method public b()[I
    .locals 6

    .prologue
    .line 105
    iget-object v0, p0, Lamj;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v2

    .line 106
    iget-object v0, p0, Lamj;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 107
    add-int v0, v2, v3

    new-array v4, v0, [I

    .line 110
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    .line 111
    invoke-virtual {p0, v0}, Lamj;->b(I)Lamq;

    move-result-object v1

    invoke-virtual {v1}, Lamq;->o()I

    move-result v1

    aput v1, v4, v0

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 113
    :goto_1
    add-int v0, v3, v2

    if-ge v1, v0, :cond_0

    .line 114
    iget-object v0, p0, Lamj;->j:Ljava/util/ArrayList;

    sub-int v5, v1, v2

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v4, v1

    .line 113
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 116
    :cond_0
    return-object v4

    :cond_1
    move v1, v0

    goto :goto_1
.end method

.method public c(I)I
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lamj;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lamj;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 121
    iget-object v0, p0, Lamj;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 122
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    sget-object v0, Lami;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 123
    iget-object v2, p0, Lamj;->j:Ljava/util/ArrayList;

    sget-object v0, Lami;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamq;

    invoke-virtual {v0}, Lamq;->o()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 125
    :cond_0
    return-void
.end method

.method public d(I)Lamq;
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lamj;->h:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamq;

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lamj;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 138
    return-void
.end method

.method public e(I)I
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lamj;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 129
    iget-object v0, p0, Lamj;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 131
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lamj;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lamj;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lamj;->a:I

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lamj;->f:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lamj;->b:Ljava/lang/String;

    return-object v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lamj;->c:I

    return v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lamj;->d:I

    return v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lamj;->e:I

    return v0
.end method
