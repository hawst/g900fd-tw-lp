.class public Lalk;
.super Ljava/lang/Object;
.source "NotificationUtils.java"


# static fields
.field private static volatile a:Lalk; = null

.field private static final b:Ljava/lang/String; = "alertoncall_mode"

.field private static c:Landroid/media/MediaPlayer; = null

.field private static e:I = 0x0

.field private static f:I = 0x0

.field private static g:I = 0x0

.field private static h:I = 0x0

.field private static i:I = 0x0

.field private static final j:I = 0xa

.field private static final k:I = 0x5


# instance fields
.field private d:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    sput-object v0, Lalk;->a:Lalk;

    .line 38
    const/4 v0, 0x0

    sput v0, Lalk;->e:I

    .line 39
    const/4 v0, 0x1

    sput v0, Lalk;->f:I

    .line 40
    const/4 v0, 0x2

    sput v0, Lalk;->g:I

    .line 41
    const/4 v0, 0x3

    sput v0, Lalk;->h:I

    .line 42
    const/4 v0, 0x4

    sput v0, Lalk;->i:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    return-void
.end method

.method public static a()Lalk;
    .locals 2

    .prologue
    .line 52
    sget-object v0, Lalk;->a:Lalk;

    if-nez v0, :cond_1

    .line 53
    const-class v1, Lalk;

    monitor-enter v1

    .line 54
    :try_start_0
    sget-object v0, Lalk;->a:Lalk;

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lalk;

    invoke-direct {v0}, Lalk;-><init>()V

    sput-object v0, Lalk;->a:Lalk;

    .line 57
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :cond_1
    sget-object v0, Lalk;->a:Lalk;

    return-object v0

    .line 57
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 306
    const-string v0, ""

    .line 307
    const-string v0, "02"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getHeadline()Ljava/lang/String;

    move-result-object v0

    .line 313
    :goto_0
    return-object v0

    .line 310
    :cond_0
    iget-object v0, p0, Lalk;->d:Landroid/content/Context;

    invoke-static {v0, p1}, Lall;->a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 7

    .prologue
    const v5, 0x3dcccccd    # 0.1f

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 352
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 353
    const-string v1, "vibrator"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    .line 354
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 391
    :cond_0
    :goto_0
    return-void

    .line 357
    :cond_1
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v2

    if-nez v2, :cond_2

    move v2, v3

    .line 358
    :goto_1
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v6

    if-ne v6, v3, :cond_3

    .line 361
    :goto_2
    if-nez v2, :cond_0

    .line 363
    if-eqz v3, :cond_4

    .line 364
    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    goto :goto_0

    :cond_2
    move v2, v4

    .line 357
    goto :goto_1

    :cond_3
    move v3, v4

    .line 358
    goto :goto_2

    .line 369
    :cond_4
    :try_start_0
    sget-object v1, Lalk;->c:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_5

    .line 370
    sget-object v1, Lalk;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 371
    const/4 v1, 0x0

    sput-object v1, Lalk;->c:Landroid/media/MediaPlayer;

    .line 374
    :cond_5
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    sput-object v1, Lalk;->c:Landroid/media/MediaPlayer;

    .line 375
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    int-to-float v0, v0

    .line 376
    mul-float/2addr v0, v5

    .line 377
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-nez v1, :cond_6

    move v0, v5

    .line 381
    :cond_6
    sget-object v1, Lalk;->c:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 382
    sget-object v1, Lalk;->c:Landroid/media/MediaPlayer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 383
    sget-object v1, Lalk;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1, p2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 384
    sget-object v1, Lalk;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V

    .line 385
    sget-object v1, Lalk;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0, v0}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 386
    sget-object v0, Lalk;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 388
    :catch_0
    move-exception v0

    .line 389
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "makeAlertSound() cated Exception : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 317
    iget-object v0, p0, Lalk;->d:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v1

    invoke-static {v0, v1}, Lall;->h(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 319
    const-string v1, "03"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 320
    iget-object v0, p0, Lalk;->d:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v1

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v2

    invoke-static {v0, v1, v2}, Lall;->b(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v0

    .line 323
    :cond_0
    return-object v0
.end method

.method public static b()V
    .locals 1

    .prologue
    .line 417
    sget-object v0, Lalk;->c:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 418
    sget-object v0, Lalk;->c:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 419
    const/4 v0, 0x0

    sput-object v0, Lalk;->c:Landroid/media/MediaPlayer;

    .line 421
    :cond_0
    return-void
.end method

.method private c(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 327
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 328
    iget-object v1, p0, Lalk;->d:Landroid/content/Context;

    const-string v2, "com.sec.android.GeoLookout.activity.DisasterDashboardActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 329
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 330
    const-string v1, "DID"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 332
    const-string v1, "uid"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 333
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MULTIPOINT UID : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 336
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    long-to-int v1, v2

    .line 337
    iget-object v2, p0, Lalk;->d:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v1, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 339
    return-object v0
.end method

.method private d(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 343
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.GeoLookout.ACTION_NOTIFICATION_CANCELED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 344
    const-string v1, "DID"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 346
    iget-object v1, p0, Lalk;->d:Landroid/content/Context;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    long-to-int v2, v2

    add-int/lit8 v2, v2, 0x1

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 348
    return-object v0
.end method

.method private d(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 394
    const-string v0, "makeVibrate"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 396
    iget-object v0, p0, Lalk;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "dormant_switch_onoff"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 397
    iget-object v1, p0, Lalk;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "dormant_disable_notifications"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 398
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "VIB_NOTIFICATION_MAGNITUDE"

    invoke-static {v2, v3, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 401
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "VIB_NOTIFICATION_MAGNITUDE"

    invoke-static {v3, v4, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 404
    if-ne v0, v5, :cond_0

    if-ne v1, v5, :cond_0

    .line 406
    const-string v0, "Blocking mode"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 414
    :goto_0
    return-void

    .line 408
    :cond_0
    const-string v0, "vibrator"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    .line 409
    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 410
    const-wide/16 v4, 0x3e8

    invoke-virtual {v0, v4, v5}, Landroid/os/Vibrator;->vibrate(J)V

    .line 411
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "VIB_NOTIFICATION_MAGNITUDE"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 192
    const-string v0, "checkNotification : info is null "

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 194
    invoke-static {p1}, Lalx;->b(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v2

    .line 196
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    .line 197
    :cond_0
    const-string v0, "checkNotification : info is null "

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 213
    :cond_1
    return-void

    .line 201
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "checkNotification : info size "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 204
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 205
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    .line 206
    if-eqz v0, :cond_3

    .line 207
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkNotification : id "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getID()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lalj;->b(Ljava/lang/String;)V

    .line 208
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v4

    invoke-virtual {v4, p1, v0}, Lalu;->a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V

    .line 204
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 210
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkNotification disasterInfo infos.get("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ") is null!"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Landroid/content/Context;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 216
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 217
    if-eqz v0, :cond_0

    .line 218
    invoke-virtual {v0, p2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 219
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cancelNotification(), ID = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 222
    :cond_0
    if-eqz p3, :cond_1

    .line 224
    const/4 v0, 0x2

    invoke-static {p1, p3, v0}, Lalx;->b(Landroid/content/Context;Ljava/lang/String;I)V

    .line 227
    :cond_1
    return-void
.end method

.method public a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "notifyInQuickPanel() : DID "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 65
    iput-object p1, p0, Lalk;->d:Landroid/content/Context;

    .line 66
    invoke-direct {p0, p2}, Lalk;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v1

    .line 67
    invoke-direct {p0, p2}, Lalk;->b(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v2

    .line 68
    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v3

    .line 69
    iget-object v0, p0, Lalk;->d:Landroid/content/Context;

    invoke-static {v0}, Lall;->q(Landroid/content/Context;)I

    move-result v4

    .line 72
    :try_start_0
    const-string v0, "ro.config.notification_sound"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "/system/media/audio/notifications/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "android.resource://"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "/"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v6, 0x7f060001

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 79
    const/4 v0, 0x2

    invoke-static {p1, v0}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v0

    .line 80
    if-nez v0, :cond_0

    .line 81
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 84
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v7, "alertoncall_mode"

    const/4 v8, 0x1

    invoke-static {v5, v7, v8}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v5

    .line 89
    iget-object v7, p0, Lalk;->d:Landroid/content/Context;

    invoke-static {v7}, Lall;->F(Landroid/content/Context;)Z

    move-result v7

    .line 91
    new-instance v8, Landroid/app/Notification$Builder;

    invoke-direct {v8, p1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v8

    const v9, 0x7f020006

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-direct {p0, p2}, Lalk;->c(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-direct {p0, p2}, Lalk;->d(Lcom/sec/android/GeoLookout/db/DisasterInfo;)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getReceivedTime()J

    move-result-wide v8

    invoke-virtual {v2, v8, v9}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v2

    const/4 v8, 0x1

    invoke-virtual {v2, v8}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;

    move-result-object v2

    .line 103
    invoke-virtual {p0, p1}, Lalk;->c(Landroid/content/Context;)I

    move-result v8

    if-lez v8, :cond_3

    if-ne v5, v10, :cond_3

    .line 104
    if-ne v3, v10, :cond_2

    .line 105
    invoke-direct {p0, p1, v6}, Lalk;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 171
    :cond_1
    :goto_0
    new-instance v0, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v0, v2}, Landroid/app/Notification$BigTextStyle;-><init>(Landroid/app/Notification$Builder;)V

    invoke-virtual {v0, v1}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$BigTextStyle;->build()Landroid/app/Notification;

    move-result-object v1

    .line 172
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 175
    if-eqz v7, :cond_f

    .line 176
    const-string v2, "isZenMode state is enabled"

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 184
    :goto_1
    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getID()I

    move-result v2

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 189
    :goto_2
    return-void

    .line 107
    :cond_2
    invoke-direct {p0, p1, v0}, Lalk;->a(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 186
    :catch_0
    move-exception v0

    .line 187
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 110
    :cond_3
    :try_start_1
    sget v5, Lalk;->f:I

    if-ne v4, v5, :cond_6

    .line 111
    const-string v4, "playAlarmSound (ALARM_TYPE_SOUND)"

    invoke-static {v4}, Lalj;->b(Ljava/lang/String;)V

    .line 113
    const/4 v4, 0x1

    new-array v4, v4, [J

    const/4 v5, 0x0

    const-wide/16 v8, 0x0

    aput-wide v8, v4, v5

    invoke-virtual {v2, v4}, Landroid/app/Notification$Builder;->setVibrate([J)Landroid/app/Notification$Builder;

    .line 115
    if-eqz v7, :cond_4

    .line 116
    const-string v0, "isZenMode state is enabled"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :cond_4
    new-instance v4, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v4}, Landroid/media/AudioAttributes$Builder;-><init>()V

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/media/AudioAttributes$Builder;->setUsage(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v4

    .line 122
    if-ne v3, v10, :cond_5

    .line 123
    invoke-virtual {v2, v6, v4}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)Landroid/app/Notification$Builder;

    goto :goto_0

    .line 125
    :cond_5
    invoke-virtual {v2, v0, v4}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)Landroid/app/Notification$Builder;

    goto :goto_0

    .line 128
    :cond_6
    sget v5, Lalk;->g:I

    if-ne v4, v5, :cond_8

    .line 129
    const-string v0, "playAlarmSound (ALARM_TYPE_VIBRATION)"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 130
    if-eqz v7, :cond_7

    .line 131
    const-string v0, "isZenMode state is enabled"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 133
    :cond_7
    invoke-direct {p0, p1}, Lalk;->d(Landroid/content/Context;)V

    goto :goto_0

    .line 135
    :cond_8
    sget v5, Lalk;->h:I

    if-ne v4, v5, :cond_b

    .line 136
    const-string v4, "playAlarmSound (ALARM_TYPE_VIBRATION_AND_SOUND)"

    invoke-static {v4}, Lalj;->b(Ljava/lang/String;)V

    .line 138
    if-eqz v7, :cond_9

    .line 139
    const-string v0, "isZenMode state is enabled"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 141
    :cond_9
    new-instance v4, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v4}, Landroid/media/AudioAttributes$Builder;-><init>()V

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/media/AudioAttributes$Builder;->setUsage(I)Landroid/media/AudioAttributes$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v4

    .line 145
    if-ne v3, v10, :cond_a

    .line 146
    invoke-virtual {v2, v6, v4}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)Landroid/app/Notification$Builder;

    .line 150
    :goto_3
    invoke-direct {p0, p1}, Lalk;->d(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 148
    :cond_a
    invoke-virtual {v2, v0, v4}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;Landroid/media/AudioAttributes;)Landroid/app/Notification$Builder;

    goto :goto_3

    .line 153
    :cond_b
    sget v5, Lalk;->i:I

    if-ne v4, v5, :cond_c

    .line 154
    const-string v0, "playAlarmSound (ANARM_TYPE_MUTE)"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 156
    :cond_c
    sget v5, Lalk;->e:I

    if-ne v4, v5, :cond_1

    .line 157
    const-string v4, "playAlarmSound (ALARM_TYPE_USING_DEVICE_SETTING)"

    invoke-static {v4}, Lalj;->b(Ljava/lang/String;)V

    .line 158
    if-eqz v7, :cond_d

    .line 159
    const-string v0, "isZenMode state is enabled"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 161
    :cond_d
    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    .line 162
    if-ne v3, v10, :cond_e

    .line 163
    invoke-virtual {v2, v6}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;)Landroid/app/Notification$Builder;

    goto/16 :goto_0

    .line 165
    :cond_e
    invoke-virtual {v2, v0}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;)Landroid/app/Notification$Builder;

    goto/16 :goto_0

    .line 178
    :cond_f
    iget v2, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Landroid/app/Notification;->flags:I

    .line 179
    const/16 v2, 0x3e8

    iput v2, v1, Landroid/app/Notification;->ledOnMS:I

    .line 180
    const/16 v2, 0x1388

    iput v2, v1, Landroid/app/Notification;->ledOffMS:I

    .line 181
    const/16 v2, 0xff

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xff

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    iput v2, v1, Landroid/app/Notification;->ledARGB:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method public b(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 230
    invoke-static {p1}, Lalx;->b(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v3

    .line 232
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    .line 233
    :cond_0
    const-string v0, "cancelAllNotification : info is null "

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 251
    :cond_1
    return-void

    .line 237
    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cancelAllNotification : info size "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 240
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 242
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    .line 243
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    .line 244
    if-eqz v1, :cond_3

    .line 245
    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getID()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 242
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 248
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cancelAllNotification disasterInfo infos.get("

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ") is null!"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->d(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public b(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 5

    .prologue
    const/16 v4, 0x4b

    .line 255
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sendNotificationService, di.getTypeId() = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 257
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.samsung.accessory.intent.action.ALERT_NOTIFICATION_ITEM"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 258
    const-string v0, "com.samsung.accessory.permission.TRANSPORTING_NOTIFICATION_ITEM"

    .line 260
    const-string v0, "NOTIFICATION_VERSION"

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 261
    const-string v0, "NOTIFICATION_PACKAGE_NAME"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 262
    const-string v0, "NOTIFICATION_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 263
    const-string v0, "NOTIFICATION_MAIN_TEXT"

    const v2, 0x7f0a0029

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 264
    const-string v0, "NOTIFICATION_TEXT_MESSAGE"

    invoke-static {p1, p2}, Lall;->a(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 266
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f020051

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 267
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 268
    const/4 v2, 0x0

    invoke-static {v0, v4, v4, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 269
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 270
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x32

    invoke-virtual {v0, v3, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 271
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 272
    const-string v2, "NOTIFICATION_APP_ICON"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 273
    const-string v0, "NOTIFICATION_ITEM_IDENTIFIER"

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 274
    const-string v0, "NOTIFICATION_LAUNCH_INTENT"

    const-string v2, "NO_LAUNCH"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 275
    const-string v0, "NOTIFICATION_ID"

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getID()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 277
    const-string v0, "uid"

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getMultiPointUid()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 279
    const-string v0, "03"

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280
    const-string v0, "NOTIFICATION_CUSTOM_FIELD1"

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v2

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v3

    invoke-static {p1, v2, v3}, Lall;->b(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 284
    :goto_0
    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getSignificance()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 285
    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getActionStatus()I

    move-result v0

    invoke-static {v0}, Lall;->d(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 286
    const-string v0, "NOTIFICATION_CUSTOM_FIELD2"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "warning//"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getActionStatus()I

    move-result v3

    invoke-static {v3}, Lall;->d(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "//"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 292
    :cond_0
    :goto_1
    const-string v0, "com.samsung.accessory.permission.TRANSPORTING_NOTIFICATION_ITEM"

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 293
    return-void

    .line 282
    :cond_1
    const-string v0, "NOTIFICATION_CUSTOM_FIELD1"

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getTypeId()I

    move-result v2

    invoke-static {p1, v2}, Lall;->h(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 288
    :cond_2
    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getActionStatus()I

    move-result v0

    invoke-static {v0}, Lall;->d(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 289
    const-string v0, "NOTIFICATION_CUSTOM_FIELD2"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "watching//"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getActionStatus()I

    move-result v3

    invoke-static {v3}, Lall;->d(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "//"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1
.end method

.method public c(Landroid/content/Context;)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 424
    .line 427
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 428
    if-eqz v0, :cond_2

    .line 429
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v1

    if-ne v1, v4, :cond_1

    move v1, v2

    .line 430
    :goto_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-ne v0, v2, :cond_0

    move v3, v4

    .line 433
    :cond_0
    :goto_1
    add-int v0, v1, v3

    return v0

    :cond_1
    move v1, v3

    .line 429
    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1
.end method

.method public c(Landroid/content/Context;Lcom/sec/android/GeoLookout/db/DisasterInfo;)V
    .locals 3

    .prologue
    .line 296
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 297
    const-string v1, "com.samsung.accessory.intent.action.CHECK_NOTIFICATION_ITEM"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 298
    const-string v1, "NOTIFICATION_PACKAGE_NAME"

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 299
    const-string v1, "NOTIFICATION_ID"

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getID()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 300
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 301
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "broadcast remove wearable ID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getID()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 302
    return-void
.end method
