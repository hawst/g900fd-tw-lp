.class public Lami;
.super Ljava/lang/Object;
.source "Lifemode.java"


# static fields
.field public static final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lamj;",
            ">;"
        }
    .end annotation
.end field

.field public static b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lamq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    invoke-static {}, Lamp;->a()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lami;->a:Ljava/util/ArrayList;

    .line 23
    invoke-static {}, Lamp;->b()Landroid/util/SparseArray;

    move-result-object v0

    sput-object v0, Lami;->b:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 12

    .prologue
    const/4 v0, 0x0

    const-wide/16 v10, 0x1

    const/4 v9, 0x1

    const/4 v8, -0x1

    const-wide/16 v6, 0x0

    .line 26
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 28
    invoke-static {p0, p1}, Lana;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 29
    invoke-static {p0}, Lamu;->b(Landroid/content/Context;)V

    .line 31
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/16 v3, 0x8c

    if-eq v2, v3, :cond_2

    .line 32
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processLifeInformation size"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 33
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 34
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v4, 0x82

    if-ne v3, v4, :cond_3

    .line 35
    :goto_0
    const/16 v3, 0xa

    if-ge v0, v3, :cond_1

    .line 36
    new-instance v3, Lamz;

    invoke-direct {v3}, Lamz;-><init>()V

    .line 37
    sget-boolean v4, Laky;->b:Z

    if-eqz v4, :cond_0

    .line 38
    const-string v4, "NOID"

    invoke-virtual {v3, v4}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v4

    invoke-virtual {v4, v8}, Lamz;->a(I)Lamz;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v4

    new-instance v5, Lamf;

    invoke-direct {v5, v6, v7, v6, v7}, Lamf;-><init>(DD)V

    invoke-virtual {v4, v5}, Lamz;->a(Lamf;)Lamz;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, Lamz;->a(J)Lamz;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Lamz;->c(Ljava/lang/String;)Lamz;

    move-result-object v4

    invoke-virtual {v4, v9}, Lamz;->b(I)Lamz;

    move-result-object v4

    add-int/lit16 v5, v0, 0x1f5

    invoke-virtual {v4, v5}, Lamz;->c(I)Lamz;

    .line 48
    :goto_1
    invoke-virtual {v3}, Lamz;->a()Lamx;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 43
    :cond_0
    const-string v4, "NOID"

    invoke-virtual {v3, v4}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v4

    invoke-virtual {v4, v8}, Lamz;->a(I)Lamz;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v4

    new-instance v5, Lamf;

    invoke-direct {v5, v6, v7, v6, v7}, Lamf;-><init>(DD)V

    invoke-virtual {v4, v5}, Lamz;->a(Lamf;)Lamz;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, Lamz;->a(J)Lamz;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Lamz;->c(Ljava/lang/String;)Lamz;

    move-result-object v4

    invoke-virtual {v4, v9}, Lamz;->b(I)Lamz;

    move-result-object v4

    add-int/lit16 v5, v0, 0x263

    invoke-virtual {v4, v5}, Lamz;->c(I)Lamz;

    goto :goto_1

    .line 50
    :cond_1
    invoke-static {p0, v2}, Lamu;->a(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 73
    :cond_2
    :goto_2
    invoke-static {p0, v1}, Lamu;->a(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 74
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 75
    invoke-static {p0, v0, v1}, Lall;->a(Landroid/content/Context;J)V

    .line 81
    return-void

    .line 51
    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v4, 0x68

    if-ne v3, v4, :cond_6

    .line 52
    :goto_3
    const/16 v3, 0x8

    if-ge v0, v3, :cond_5

    .line 53
    new-instance v3, Lamz;

    invoke-direct {v3}, Lamz;-><init>()V

    .line 54
    sget-boolean v4, Laky;->b:Z

    if-eqz v4, :cond_4

    .line 55
    const-string v4, "NOID"

    invoke-virtual {v3, v4}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v4

    invoke-virtual {v4, v8}, Lamz;->a(I)Lamz;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v4

    new-instance v5, Lamf;

    invoke-direct {v5, v6, v7, v6, v7}, Lamf;-><init>(DD)V

    invoke-virtual {v4, v5}, Lamz;->a(Lamf;)Lamz;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, Lamz;->a(J)Lamz;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Lamz;->c(Ljava/lang/String;)Lamz;

    move-result-object v4

    invoke-virtual {v4, v9}, Lamz;->b(I)Lamz;

    move-result-object v4

    add-int/lit16 v5, v0, 0x1f5

    invoke-virtual {v4, v5}, Lamz;->c(I)Lamz;

    .line 65
    :goto_4
    invoke-virtual {v3}, Lamz;->a()Lamx;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 60
    :cond_4
    const-string v4, "NOID"

    invoke-virtual {v3, v4}, Lamz;->a(Ljava/lang/String;)Lamz;

    move-result-object v4

    invoke-virtual {v4, v8}, Lamz;->a(I)Lamz;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Lamz;->b(Ljava/lang/String;)Lamz;

    move-result-object v4

    new-instance v5, Lamf;

    invoke-direct {v5, v6, v7, v6, v7}, Lamf;-><init>(DD)V

    invoke-virtual {v4, v5}, Lamz;->a(Lamf;)Lamz;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, Lamz;->a(J)Lamz;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Lamz;->c(Ljava/lang/String;)Lamz;

    move-result-object v4

    invoke-virtual {v4, v9}, Lamz;->b(I)Lamz;

    move-result-object v4

    add-int/lit16 v5, v0, 0x263

    invoke-virtual {v4, v5}, Lamz;->c(I)Lamz;

    goto :goto_4

    .line 67
    :cond_5
    invoke-static {p0, v2}, Lamu;->a(Landroid/content/Context;Ljava/util/ArrayList;)V

    goto/16 :goto_2

    .line 69
    :cond_6
    const-string v0, "processLifeInformation ???"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 85
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 86
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 87
    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 89
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 90
    :cond_1
    const-string v0, "isNetworkConnected> Network connect success"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    move v0, v1

    .line 95
    :goto_0
    return v0

    .line 94
    :cond_2
    const-string v0, "isNetworkConnected> Network connect fail"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    move v0, v2

    .line 95
    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 100
    invoke-static {p0}, Lall;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 101
    invoke-static {p0, v0}, Lall;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 102
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isSupportedZone> currentZoneId : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 104
    if-eqz v0, :cond_0

    const-string v1, "NOID"

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    :cond_0
    const-string v0, "isSupportedZone> Not supported zone"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 106
    const/4 v0, 0x0

    .line 108
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
