.class public Lamh;
.super Ljava/lang/Object;
.source "TrackingsDbTable.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# static fields
.field public static final A:I = 0x7

.field public static final B:I = 0x8

.field public static final C:I = 0x9

.field public static final D:I = 0xa

.field public static final E:I = 0xb

.field public static final F:I = 0xc

.field public static final G:I = 0xd

.field public static final H:I = 0xe

.field public static final I:I = 0xf

.field public static final J:I = 0x10

.field public static final K:I = 0x11

.field public static final L:I = 0x13

.field public static final M:I = 0x14

.field public static final N:I = 0x15

.field protected static O:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static P:Ljava/lang/String; = null

.field protected static Q:[Ljava/lang/String; = null

.field public static R:Ljava/lang/String; = null

.field public static S:Ljava/lang/String; = null

.field public static final a:Ljava/lang/String; = "STIME"

.field public static final b:Ljava/lang/String; = "RTIME"

.field public static final c:Ljava/lang/String; = "HTHPID"

.field public static final d:Ljava/lang/String; = "LINKEDBEDID"

.field public static final e:Ljava/lang/String; = "DID"

.field public static final f:Ljava/lang/String; = "LINK"

.field public static final g:Ljava/lang/String; = "HEADLINE"

.field public static final h:Ljava/lang/String; = "LATITUDE"

.field public static final i:Ljava/lang/String; = "LONGITUDE"

.field public static final j:Ljava/lang/String; = "WINDGUST"

.field public static final k:Ljava/lang/String; = "DIRECTIONDEGREES"

.field public static final l:Ljava/lang/String; = "WINDMAX"

.field public static final m:Ljava/lang/String; = "FORWARDSPEED"

.field public static final n:Ljava/lang/String; = "FORECASTHOUR"

.field public static final o:Ljava/lang/String; = "DIRECTIONCARDINAL"

.field public static final p:Ljava/lang/String; = "DATATYPE"

.field public static final q:Ljava/lang/String; = "PRESSUREMB"

.field public static final r:Ljava/lang/String; = "created"

.field public static final s:Ljava/lang/String; = "modified"

.field public static final t:I = 0x0

.field public static final u:I = 0x1

.field public static final v:I = 0x2

.field public static final w:I = 0x3

.field public static final x:I = 0x4

.field public static final y:I = 0x5

.field public static final z:I = 0x6


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lamh;->O:Ljava/util/HashMap;

    .line 65
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "STIME"

    const-string v2, "STIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "RTIME"

    const-string v2, "RTIME"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "HTHPID"

    const-string v2, "HTHPID"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "LINKEDBEDID"

    const-string v2, "LINKEDBEDID"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "DID"

    const-string v2, "DID"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "LINK"

    const-string v2, "LINK"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "HEADLINE"

    const-string v2, "HEADLINE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "LATITUDE"

    const-string v2, "LATITUDE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "LONGITUDE"

    const-string v2, "LONGITUDE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "WINDGUST"

    const-string v2, "WINDGUST"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "DIRECTIONDEGREES"

    const-string v2, "DIRECTIONDEGREES"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "WINDMAX"

    const-string v2, "WINDMAX"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "FORWARDSPEED"

    const-string v2, "FORWARDSPEED"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "FORECASTHOUR"

    const-string v2, "FORECASTHOUR"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "DIRECTIONCARDINAL"

    const-string v2, "DIRECTIONCARDINAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "DATATYPE"

    const-string v2, "DATATYPE"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "PRESSUREMB"

    const-string v2, "PRESSUREMB"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "created"

    const-string v2, "created"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    const-string v1, "modified"

    const-string v2, "modified"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    const-string v0, "CREATE TABLE TRACKING_DISASTER_TBL (_id INTEGER PRIMARY KEY,STIME LONG,RTIME LONG,HTHPID TEXT,LINKEDBEDID TEXT,DID TEXT,LINK TEXT,HEADLINE TEXT,LATITUDE DOUBLE,LONGITUDE DOUBLE,WINDGUST INTEGER,DIRECTIONDEGREES INTEGER,WINDMAX INTEGER,FORWARDSPEED INTEGER,FORECASTHOUR INTEGER,DIRECTIONCARDINAL TEXT,DATATYPE SHORT,PRESSUREMB INTEGER,created INTEGER,modified INTEGER"

    sput-object v0, Lamh;->P:Ljava/lang/String;

    .line 107
    const-string v0, ","

    sput-object v0, Lamh;->R:Ljava/lang/String;

    .line 108
    const-string v0, ");"

    sput-object v0, Lamh;->S:Ljava/lang/String;

    .line 110
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "STIME"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "RTIME"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "HTHPID"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "LINKEDBEDID"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "DID"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "LINK"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "HEADLINE"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "LATITUDE"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "LONGITUDE"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "WINDGUST"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "DIRECTIONDEGREES"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "WINDMAX"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "FORWARDSPEED"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "FORECASTHOUR"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "DIRECTIONCARDINAL"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "DATATYPE"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "PRESSUREMB"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "created"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "modified"

    aput-object v2, v0, v1

    sput-object v0, Lamh;->Q:[Ljava/lang/String;

    .line 132
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/sec/android/GeoLookout/db/DisasterInfo;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 177
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 178
    const-string v1, "STIME"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getStartTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 179
    const-string v1, "LINKEDBEDID"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string v1, "HTHPID"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getHTHPId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v1, "DID"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v1, "LINK"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLink()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v1, "HEADLINE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getHeadline()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v1, "LATITUDE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v2

    iget-wide v2, v2, Lamf;->a:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 185
    const-string v1, "LONGITUDE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getLocation()Lamf;

    move-result-object v2

    iget-wide v2, v2, Lamf;->b:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 186
    const-string v1, "WINDGUST"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailWindGust()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 187
    const-string v1, "DIRECTIONDEGREES"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailDirectionDegrees()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 188
    const-string v1, "WINDMAX"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailWindMax()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 189
    const-string v1, "FORWARDSPEED"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailStormSpeed()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 191
    const-string v1, "FORECASTHOUR"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailForecastHour()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 192
    const-string v1, "DIRECTIONCARDINAL"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailDirectionCardinal()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string v1, "DATATYPE"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDataType()S

    move-result v2

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    .line 194
    const-string v1, "PRESSUREMB"

    invoke-virtual {p1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDetailPressureMB()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 196
    return-object v0
.end method

.method protected a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;
    .locals 8

    .prologue
    .line 149
    new-instance v0, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    invoke-direct {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;-><init>()V

    .line 150
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setId(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setHTHPId(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setStartTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setReceivedTime(J)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x4

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLinkedBEDId(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x5

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDid(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x6

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLink(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/4 v2, 0x7

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setHeadline(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    new-instance v2, Lamf;

    const/16 v3, 0x8

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v4

    const/16 v3, 0x9

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v6

    invoke-direct {v2, v4, v5, v6, v7}, Lamf;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setLocation(Lamf;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0xa

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailWindGust(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0xb

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailDirectionDegrees(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0xc

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailWindMax(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0xd

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailStormSpeed(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0xe

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailForecastHour(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0xf

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailDirectionCardinal(Ljava/lang/String;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x11

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDetailPressureMB(I)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v1

    const/16 v2, 0x10

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getShort(I)S

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->setDataType(S)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    .line 168
    return-object v0
.end method

.method public a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lamh;->Q:[Ljava/lang/String;

    return-object v0
.end method

.method public b(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0, p1}, Lamh;->a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;

    move-result-object v0

    .line 173
    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/db/DisasterInfo$Builder;->build()Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    sget-object v0, Lamh;->O:Ljava/util/HashMap;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lamh;->P:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lamh;->S:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 144
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCreateDbTableString : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 145
    return-object v0
.end method
