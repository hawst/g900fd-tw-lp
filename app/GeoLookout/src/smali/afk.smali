.class public final Lafk;
.super Ljava/lang/Object;
.source "SlookCocktailManager.java"


# static fields
.field public static final a:I = 0x1

.field public static final b:I = 0x2

.field public static final c:I = 0x4

.field public static final d:I = 0x8

.field public static final e:I = 0x9f

.field public static final f:I = 0x1

.field public static final g:I = 0x10000

.field public static final h:I = 0x10001

.field public static final i:I = 0x10002

.field public static final j:I = 0x10003

.field public static final k:I = 0x10004

.field public static final l:I = 0x10005

.field public static final m:I = 0x10006

.field public static final n:I = 0x1

.field public static final o:I = 0x2

.field static p:Ljava/util/WeakHashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/content/Context;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lafk;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final q:Ljava/lang/String; = "SlookCocktailManager"


# instance fields
.field private r:Laew;

.field private s:Lcom/samsung/android/cocktailbar/CocktailBarManager;

.field private t:Lafm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 210
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lafk;->p:Ljava/util/WeakHashMap;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    new-instance v0, Laew;

    invoke-direct {v0}, Laew;-><init>()V

    iput-object v0, p0, Lafk;->r:Laew;

    .line 243
    iget-object v0, p0, Lafk;->r:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    invoke-static {p1}, Lcom/samsung/android/cocktailbar/CocktailBarManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/cocktailbar/CocktailBarManager;

    move-result-object v0

    iput-object v0, p0, Lafk;->s:Lcom/samsung/android/cocktailbar/CocktailBarManager;

    .line 246
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;)Lafk;
    .locals 5

    .prologue
    .line 220
    sget-object v3, Lafk;->p:Ljava/util/WeakHashMap;

    monitor-enter v3

    .line 221
    if-nez p0, :cond_0

    .line 222
    :try_start_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "context is null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 239
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 224
    :cond_0
    :try_start_1
    instance-of v1, p0, Landroid/content/ContextWrapper;

    if-eqz v1, :cond_1

    .line 225
    move-object v0, p0

    check-cast v0, Landroid/content/ContextWrapper;

    move-object v1, v0

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_1

    .line 226
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Base context is null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 229
    :cond_1
    sget-object v1, Lafk;->p:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 230
    const/4 v2, 0x0

    .line 231
    if-eqz v1, :cond_3

    .line 232
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lafk;

    .line 234
    :goto_0
    if-nez v1, :cond_2

    .line 235
    new-instance v1, Lafk;

    invoke-direct {v1, p0}, Lafk;-><init>(Landroid/content/Context;)V

    .line 236
    sget-object v2, Lafk;->p:Ljava/util/WeakHashMap;

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, p0, v4}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    :cond_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1

    :cond_3
    move-object v1, v2

    goto :goto_0
.end method

.method static synthetic a(Lafk;Lafm;)Lafm;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lafk;->t:Lafm;

    return-object p1
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lafk;->r:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lafk;->s:Lcom/samsung/android/cocktailbar/CocktailBarManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/cocktailbar/CocktailBarManager;->showCocktail(I)V

    .line 314
    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 324
    iget-object v0, p0, Lafk;->r:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lafk;->s:Lcom/samsung/android/cocktailbar/CocktailBarManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/cocktailbar/CocktailBarManager;->closeCocktail(II)V

    .line 327
    :cond_0
    return-void
.end method

.method public a(ILafn;)V
    .locals 6

    .prologue
    .line 264
    iget-object v0, p0, Lafk;->r:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    if-nez p2, :cond_1

    .line 271
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    iget-object v0, p0, Lafk;->s:Lcom/samsung/android/cocktailbar/CocktailBarManager;

    invoke-static {p2}, Lafn;->a(Lafn;)I

    move-result v2

    invoke-static {p2}, Lafn;->b(Lafn;)I

    move-result v3

    invoke-static {p2}, Lafn;->c(Lafn;)Landroid/widget/RemoteViews;

    move-result-object v4

    invoke-static {p2}, Lafn;->d(Lafn;)Landroid/os/Bundle;

    move-result-object v5

    move v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/cocktailbar/CocktailBarManager;->updateCocktail(IIILandroid/widget/RemoteViews;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(ILandroid/widget/RemoteViews;)V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lafk;->r:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    if-nez p2, :cond_1

    .line 302
    :cond_0
    :goto_0
    return-void

    .line 300
    :cond_1
    iget-object v0, p0, Lafk;->s:Lcom/samsung/android/cocktailbar/CocktailBarManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/cocktailbar/CocktailBarManager;->partiallyUpdateCocktail(ILandroid/widget/RemoteViews;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, Lafk;->r:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lafk;->t:Lafm;

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lafk;->s:Lcom/samsung/android/cocktailbar/CocktailBarManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/cocktailbar/CocktailBarManager;->updateCocktailItem(Landroid/os/Bundle;)Z

    move-result v0

    .line 386
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lafk;->r:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lafk;->t:Lafm;

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lafk;->s:Lcom/samsung/android/cocktailbar/CocktailBarManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/cocktailbar/CocktailBarManager;->unregisterCocktailCallback(Ljava/lang/String;)Z

    .line 372
    iget-object v0, p0, Lafk;->t:Lafm;

    invoke-virtual {v0}, Lafm;->b()V

    .line 373
    const/4 v0, 0x0

    iput-object v0, p0, Lafk;->t:Lafm;

    .line 374
    const/4 v0, 0x1

    .line 377
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lafp;)Z
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lafk;->r:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lafk;->t:Lafm;

    if-nez v0, :cond_0

    .line 360
    new-instance v0, Lafm;

    invoke-direct {v0, p0, p2}, Lafm;-><init>(Lafk;Lafp;)V

    iput-object v0, p0, Lafk;->t:Lafm;

    .line 361
    iget-object v0, p0, Lafk;->s:Lcom/samsung/android/cocktailbar/CocktailBarManager;

    iget-object v1, p0, Lafk;->t:Lafm;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/cocktailbar/CocktailBarManager;->registerCocktailCallback(Ljava/lang/String;Lcom/samsung/android/cocktailbar/CocktailBarManager$ICocktailCallback;)Z

    .line 362
    const/4 v0, 0x1

    .line 365
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/content/ComponentName;)[I
    .locals 2

    .prologue
    .line 349
    iget-object v0, p0, Lafk;->r:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lafk;->s:Lcom/samsung/android/cocktailbar/CocktailBarManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/cocktailbar/CocktailBarManager;->getCocktailIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 352
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [I

    goto :goto_0
.end method

.method public b(II)V
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lafk;->r:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lafk;->s:Lcom/samsung/android/cocktailbar/CocktailBarManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/cocktailbar/CocktailBarManager;->notifyCocktailViewDataChanged(II)V

    .line 340
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 390
    iget-object v0, p0, Lafk;->r:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lafk;->t:Lafm;

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lafk;->s:Lcom/samsung/android/cocktailbar/CocktailBarManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/cocktailbar/CocktailBarManager;->removeCocktailItem(Landroid/os/Bundle;)Z

    move-result v0

    .line 395
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 399
    iget-object v0, p0, Lafk;->r:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Lafk;->s:Lcom/samsung/android/cocktailbar/CocktailBarManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/cocktailbar/CocktailBarManager;->isAllowed(Ljava/lang/String;)Z

    move-result v0

    .line 402
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
