.class public final Lbl;
.super Ljava/lang/Object;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Lcb;

.field private d:Lcc;

.field private e:[Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "https://www.googleapis.com/auth/appstate"

    aput-object v2, v0, v1

    sput-object v0, Lbl;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcb;Lcc;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbl;->b:Landroid/content/Context;

    iput-object p2, p0, Lbl;->c:Lcb;

    iput-object p3, p0, Lbl;->d:Lcc;

    sget-object v0, Lbl;->a:[Ljava/lang/String;

    iput-object v0, p0, Lbl;->e:[Ljava/lang/String;

    const-string v0, "<<default account>>"

    iput-object v0, p0, Lbl;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Lbj;
    .locals 7

    new-instance v0, Lbj;

    iget-object v1, p0, Lbl;->b:Landroid/content/Context;

    iget-object v2, p0, Lbl;->c:Lcb;

    iget-object v3, p0, Lbl;->d:Lcc;

    iget-object v4, p0, Lbl;->f:Ljava/lang/String;

    iget-object v5, p0, Lbl;->e:[Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lbj;-><init>(Landroid/content/Context;Lcb;Lcc;Ljava/lang/String;[Ljava/lang/String;Lbk;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lbl;
    .locals 1

    invoke-static {p1}, Lnx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lbl;->f:Ljava/lang/String;

    return-object p0
.end method

.method public varargs a([Ljava/lang/String;)Lbl;
    .locals 0

    iput-object p1, p0, Lbl;->e:[Ljava/lang/String;

    return-object p0
.end method
