.class public Laoe;
.super Ljava/lang/Object;
.source "DserverError.java"


# static fields
.field public static final a:I = 0xc8

.field public static final b:I = 0x190

.field public static final c:I = 0x191

.field public static final d:I = 0x193

.field public static final e:I = 0x194

.field public static final f:I = 0x195

.field public static final g:I = 0x196

.field public static final h:I = 0x198

.field public static final i:I = 0x19b

.field public static final j:I = 0x19c

.field public static final k:I = 0x19d

.field public static final l:I = 0x19e

.field public static final m:I = 0x19f

.field public static final n:I = 0x1a0

.field public static final o:I = 0x1a4

.field public static final p:I = 0x1a5

.field public static final q:I = 0x1a6

.field public static final r:I = 0x1f4

.field public static final s:I = 0x1f7

.field public static final t:[I

.field public static final u:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v1, 0x13

    .line 30
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Laoe;->t:[I

    .line 43
    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "HTTP_BAD_REQUEST"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "HTTP_UNAUTHORIZED"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "HTTP_FORBIDDEN"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "HTTP_NOT_FOUND"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "HTTP_METHOD_NOT_ALLOWED"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "HTTP_NOT_ACCEPTABLE"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "HTTP_REQUEST_TIMEOUT"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "HTTP_LENGTH_REQUIRED"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "HTTP_PRECONDITION_FAILED"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "HTTP_REQUEST_ENTITY_TOO_LARGE"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "HTTP_REQUEST_URI_TOO_LONG"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "HTTP_UNSUPPORTED_MEDIA_TYPE"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "HTTP_UNSUPPORTED_MEDIA_TYPE"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "HTTP_REQUESTED_RANGE_NOT_SATISFIABLE"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "HTTP_PARAMETER_REQUIRED"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "HTTP_BAD_PARAMETER"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "HTTP_QUERY_SYNTAX_ERROR"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "HTTP_INTERNAL_SERVER_ERROR"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "HTTP_SERVICE_UNAVAILABLE"

    aput-object v2, v0, v1

    sput-object v0, Laoe;->u:[Ljava/lang/String;

    return-void

    .line 30
    :array_0
    .array-data 4
        0x190
        0x191
        0x193
        0x194
        0x195
        0x196
        0x198
        0x19b
        0x19c
        0x19d
        0x19e
        0x19f
        0x19f
        0x1a0
        0x1a4
        0x1a5
        0x1a6
        0x1f4
        0x1f7
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(I)V
    .locals 3

    .prologue
    .line 57
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Laoe;->t:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 58
    sget-object v1, Laoe;->t:[I

    aget v1, v1, v0

    if-ne v1, p0, :cond_0

    .line 59
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HTTP Error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Laoe;->u:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->c(Ljava/lang/String;)V

    .line 57
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_1
    return-void
.end method
