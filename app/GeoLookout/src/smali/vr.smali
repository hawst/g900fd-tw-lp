.class public final Lvr;
.super Ljava/lang/Object;


# static fields
.field private static a:Lxh;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lvq;
    .locals 2

    :try_start_0
    new-instance v0, Lvq;

    invoke-static {}, Lvr;->c()Lxh;

    move-result-object v1

    invoke-interface {v1}, Lxh;->a()Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lvq;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(F)Lvq;
    .locals 2

    :try_start_0
    new-instance v0, Lvq;

    invoke-static {}, Lvr;->c()Lxh;

    move-result-object v1

    invoke-interface {v1, p0}, Lxh;->a(F)Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lvq;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(FF)Lvq;
    .locals 2

    :try_start_0
    new-instance v0, Lvq;

    invoke-static {}, Lvr;->c()Lxh;

    move-result-object v1

    invoke-interface {v1, p0, p1}, Lxh;->a(FF)Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lvq;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(FLandroid/graphics/Point;)Lvq;
    .locals 4

    :try_start_0
    new-instance v0, Lvq;

    invoke-static {}, Lvr;->c()Lxh;

    move-result-object v1

    iget v2, p1, Landroid/graphics/Point;->x:I

    iget v3, p1, Landroid/graphics/Point;->y:I

    invoke-interface {v1, p0, v2, v3}, Lxh;->a(FII)Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lvq;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(Lcom/google/android/gms/maps/model/CameraPosition;)Lvq;
    .locals 2

    :try_start_0
    new-instance v0, Lvq;

    invoke-static {}, Lvr;->c()Lxh;

    move-result-object v1

    invoke-interface {v1, p0}, Lxh;->a(Lcom/google/android/gms/maps/model/CameraPosition;)Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lvq;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLng;)Lvq;
    .locals 2

    :try_start_0
    new-instance v0, Lvq;

    invoke-static {}, Lvr;->c()Lxh;

    move-result-object v1

    invoke-interface {v1, p0}, Lxh;->a(Lcom/google/android/gms/maps/model/LatLng;)Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lvq;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLng;F)Lvq;
    .locals 2

    :try_start_0
    new-instance v0, Lvq;

    invoke-static {}, Lvr;->c()Lxh;

    move-result-object v1

    invoke-interface {v1, p0, p1}, Lxh;->a(Lcom/google/android/gms/maps/model/LatLng;F)Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lvq;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLngBounds;I)Lvq;
    .locals 2

    :try_start_0
    new-instance v0, Lvq;

    invoke-static {}, Lvr;->c()Lxh;

    move-result-object v1

    invoke-interface {v1, p0, p1}, Lxh;->a(Lcom/google/android/gms/maps/model/LatLngBounds;I)Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lvq;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLngBounds;III)Lvq;
    .locals 2

    :try_start_0
    new-instance v0, Lvq;

    invoke-static {}, Lvr;->c()Lxh;

    move-result-object v1

    invoke-interface {v1, p0, p1, p2, p3}, Lxh;->a(Lcom/google/android/gms/maps/model/LatLngBounds;III)Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lvq;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method static a(Lxh;)V
    .locals 1

    sget-object v0, Lvr;->a:Lxh;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lnx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxh;

    sput-object v0, Lvr;->a:Lxh;

    goto :goto_0
.end method

.method public static b()Lvq;
    .locals 2

    :try_start_0
    new-instance v0, Lvq;

    invoke-static {}, Lvr;->c()Lxh;

    move-result-object v1

    invoke-interface {v1}, Lxh;->b()Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lvq;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static b(F)Lvq;
    .locals 2

    :try_start_0
    new-instance v0, Lvq;

    invoke-static {}, Lvr;->c()Lxh;

    move-result-object v1

    invoke-interface {v1, p0}, Lxh;->b(F)Ldp;

    move-result-object v1

    invoke-direct {v0, v1}, Lvq;-><init>(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method private static c()Lxh;
    .locals 2

    sget-object v0, Lvr;->a:Lxh;

    const-string v1, "CameraUpdateFactory is not initialized"

    invoke-static {v0, v1}, Lnx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxh;

    return-object v0
.end method
