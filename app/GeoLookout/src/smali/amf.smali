.class public Lamf;
.super Ljava/lang/Object;
.source "LatLng.java"


# instance fields
.field public a:D

.field public b:D


# direct methods
.method public constructor <init>(DD)V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-wide p1, p0, Lamf;->a:D

    .line 12
    iput-wide p3, p0, Lamf;->b:D

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/location/Location;)V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    iput-wide v0, p0, Lamf;->a:D

    .line 17
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    iput-wide v0, p0, Lamf;->b:D

    .line 18
    return-void
.end method
