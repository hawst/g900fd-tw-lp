.class public Laij;
.super Landroid/app/DialogFragment;
.source "DisasterDialogRegisterActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 280
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(II)Laij;
    .locals 3

    .prologue
    .line 283
    new-instance v0, Laij;

    invoke-direct {v0}, Laij;-><init>()V

    .line 284
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 286
    const-string v2, "title"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 287
    const-string v2, "msg"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 288
    invoke-virtual {v0, v1}, Laij;->setArguments(Landroid/os/Bundle;)V

    .line 289
    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 387
    const-string v0, "show WiFi setting"

    invoke-static {v0}, Lalj;->c(Ljava/lang/String;)V

    .line 388
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.WIFI_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 389
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 390
    invoke-virtual {p0, v0}, Laij;->startActivity(Landroid/content/Intent;)V

    .line 391
    return-void
.end method

.method static synthetic a(Laij;)V
    .locals 0

    .prologue
    .line 280
    invoke-direct {p0}, Laij;->a()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 397
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 398
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 399
    invoke-virtual {p0, v0}, Laij;->startActivity(Landroid/content/Intent;)V

    .line 400
    return-void
.end method

.method static synthetic b(Laij;)V
    .locals 0

    .prologue
    .line 280
    invoke-direct {p0}, Laij;->b()V

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 406
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 407
    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_LIFEMODE_REFRESH_DATA_CANCEL"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 408
    invoke-virtual {p0}, Laij;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 409
    return-void
.end method

.method static synthetic c(Laij;)V
    .locals 0

    .prologue
    .line 280
    invoke-direct {p0}, Laij;->c()V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 415
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 416
    const-string v1, "com.sec.android.GeoLookout"

    const-string v2, "com.sec.android.GeoLookout.activity.DisasterActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 417
    invoke-virtual {p0, v0}, Laij;->startActivity(Landroid/content/Intent;)V

    .line 418
    return-void
.end method

.method static synthetic d(Laij;)V
    .locals 0

    .prologue
    .line 280
    invoke-direct {p0}, Laij;->d()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const v5, 0x7f0a0001

    const v0, 0x7f0a0002

    .line 294
    invoke-virtual {p0}, Laij;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "msg"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 295
    invoke-virtual {p0}, Laij;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "title"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 297
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Laij;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 299
    if-lez v2, :cond_1

    .line 300
    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 305
    :goto_0
    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 307
    new-instance v2, Laik;

    invoke-direct {v2, p0}, Laik;-><init>(Laij;)V

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 323
    const v2, 0x7f0a0017

    if-eq v1, v2, :cond_0

    const v2, 0x7f0a0019

    if-ne v1, v2, :cond_2

    .line 324
    :cond_0
    new-instance v1, Lail;

    invoke-direct {v1, p0}, Lail;-><init>(Laij;)V

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 331
    new-instance v0, Laim;

    invoke-direct {v0, p0}, Laim;-><init>(Laij;)V

    invoke-virtual {v3, v5, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 378
    :goto_1
    invoke-virtual {p0}, Laij;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;

    .line 379
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 380
    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;->b(Lcom/sec/android/GeoLookout/activity/DisasterDialogRegisterActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 302
    :cond_1
    const v2, 0x7f0a0029

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 336
    :cond_2
    const v2, 0x7f0a000c

    if-eq v1, v2, :cond_3

    const v2, 0x7f0a000e

    if-ne v1, v2, :cond_5

    .line 338
    :cond_3
    sget-boolean v1, Laky;->d:Z

    if-eqz v1, :cond_4

    sget-boolean v1, Laky;->e:Z

    if-eqz v1, :cond_4

    .line 339
    const v0, 0x7f0a0003

    .line 344
    :cond_4
    new-instance v1, Lain;

    invoke-direct {v1, p0}, Lain;-><init>(Laij;)V

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 351
    new-instance v0, Laio;

    invoke-direct {v0, p0}, Laio;-><init>(Laij;)V

    invoke-virtual {v3, v5, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 357
    :cond_5
    const v2, 0x7f0a0037

    if-ne v1, v2, :cond_6

    .line 358
    new-instance v1, Laip;

    invoke-direct {v1, p0}, Laip;-><init>(Laij;)V

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 365
    new-instance v0, Laiq;

    invoke-direct {v0, p0}, Laiq;-><init>(Laij;)V

    invoke-virtual {v3, v5, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 371
    :cond_6
    new-instance v1, Lair;

    invoke-direct {v1, p0}, Lair;-><init>(Laij;)V

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method
