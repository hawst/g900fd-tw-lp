.class public final Lama;
.super Ljava/lang/Object;
.source "Disaster.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# static fields
.field public static final a:Ljava/lang/String; = "TRACKING_DISASTER_TBL"

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field public static final d:Landroid/net/Uri;

.field public static final e:Landroid/net/Uri;

.field public static final f:Landroid/net/Uri;

.field public static final g:Landroid/net/Uri;

.field public static final h:Landroid/net/Uri;

.field public static final i:Landroid/net/Uri;

.field public static final j:Landroid/net/Uri;

.field public static final k:Ljava/lang/String; = "vnd.android.cursor.dir/tracking"

.field public static final l:Ljava/lang/String; = "vnd.android.cursor.item/tracking"

.field public static final m:Ljava/lang/String; = "_id DESC"

.field public static n:Lamh; = null

.field private static final o:Ljava/lang/String; = "content://"

.field private static final p:Ljava/lang/String; = "/tracking"

.field private static final q:Ljava/lang/String; = "/tracking/"

.field private static final r:Ljava/lang/String; = "/tracking/linkedbedid/"

.field private static final s:Ljava/lang/String; = "/tracking/exceptlinkedbedid/"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 838
    const-string v0, "content://com.sec.android.GeoLookout.db/tracking"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lama;->d:Landroid/net/Uri;

    .line 840
    const-string v0, "content://com.sec.android.GeoLookout.db/tracking/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lama;->e:Landroid/net/Uri;

    .line 842
    const-string v0, "content://com.sec.android.GeoLookout.db/tracking/linkedbedid/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lama;->f:Landroid/net/Uri;

    .line 844
    const-string v0, "content://com.sec.android.GeoLookout.db/tracking/exceptlinkedbedid/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lama;->g:Landroid/net/Uri;

    .line 846
    const-string v0, "content://com.sec.android.GeoLookout.db/tracking//#"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lama;->h:Landroid/net/Uri;

    .line 848
    const-string v0, "content://com.sec.android.GeoLookout.db/tracking/linkedbedid//*"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lama;->i:Landroid/net/Uri;

    .line 850
    const-string v0, "content://com.sec.android.GeoLookout.db/tracking/exceptlinkedbedid//*"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lama;->j:Landroid/net/Uri;

    .line 861
    new-instance v0, Lamh;

    invoke-direct {v0}, Lamh;-><init>()V

    sput-object v0, Lama;->n:Lamh;

    .line 862
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 820
    return-void
.end method

.method private static a(Lcom/sec/android/GeoLookout/db/DisasterInfo;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 1

    .prologue
    .line 1087
    sget-object v0, Lama;->n:Lamh;

    invoke-virtual {v0, p0, p1}, Lamh;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1095
    invoke-static {p0, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1091
    sget-object v0, Lama;->f:Landroid/net/Uri;

    invoke-static {v0, p0}, Lama;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;
    .locals 1

    .prologue
    .line 1036
    const-string v0, "getHTHPDisasterInfoFromCursor"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1037
    sget-object v0, Lama;->n:Lamh;

    invoke-virtual {v0, p0}, Lamh;->b(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/GeoLookout/db/DisasterInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 865
    const-string v0, "getAllHTHPDisasterInfo"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 866
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 867
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 870
    :try_start_0
    sget-object v1, Lama;->d:Landroid/net/Uri;

    sget-object v2, Lama;->n:Lamh;

    invoke-virtual {v2}, Lamh;->a()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "_id DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 871
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 873
    :cond_0
    invoke-static {v1}, Lama;->a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 874
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 875
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    .line 881
    if-eqz v1, :cond_1

    .line 882
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object v0, v6

    .line 885
    :goto_0
    return-object v0

    .line 881
    :cond_2
    if-eqz v1, :cond_3

    .line 882
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    move-object v0, v7

    .line 885
    goto :goto_0

    .line 878
    :catch_0
    move-exception v0

    move-object v1, v7

    .line 879
    :goto_2
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 881
    if-eqz v1, :cond_3

    .line 882
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 881
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_3
    if-eqz v1, :cond_4

    .line 882
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 881
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 878
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/GeoLookout/db/DisasterInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 889
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 890
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 894
    :try_start_0
    sget-object v1, Lama;->d:Landroid/net/Uri;

    sget-object v2, Lama;->n:Lamh;

    invoke-virtual {v2}, Lamh;->a()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "DID=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const-string v5, "_id DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 897
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 899
    :cond_0
    invoke-static {v1}, Lama;->a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 900
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 901
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    .line 907
    if-eqz v1, :cond_1

    .line 908
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object v0, v6

    .line 911
    :goto_0
    return-object v0

    .line 907
    :cond_2
    if-eqz v1, :cond_3

    .line 908
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    move-object v0, v7

    .line 911
    goto :goto_0

    .line 904
    :catch_0
    move-exception v0

    move-object v1, v7

    .line 905
    :goto_2
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 907
    if-eqz v1, :cond_3

    .line 908
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 907
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v7, :cond_4

    .line 908
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 907
    :catchall_1
    move-exception v0

    move-object v7, v1

    goto :goto_3

    .line 904
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/GeoLookout/db/DisasterInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 915
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 916
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 918
    const/4 v7, 0x0

    .line 920
    :try_start_0
    sget-object v1, Lama;->d:Landroid/net/Uri;

    sget-object v2, Lama;->n:Lamh;

    invoke-virtual {v2}, Lamh;->a()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "LINKEDBEDID=? AND DID=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    const-string v5, "_id DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 924
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 926
    :cond_0
    invoke-static {v1}, Lama;->a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 927
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 928
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    .line 934
    if-eqz v1, :cond_1

    .line 935
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object v0, v6

    .line 938
    :goto_0
    return-object v0

    .line 934
    :cond_2
    if-eqz v1, :cond_3

    .line 935
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    move-object v0, v6

    .line 938
    goto :goto_0

    .line 931
    :catch_0
    move-exception v0

    move-object v1, v7

    .line 932
    :goto_2
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 934
    if-eqz v1, :cond_3

    .line 935
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 934
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v7, :cond_4

    .line 935
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 934
    :catchall_1
    move-exception v0

    move-object v7, v1

    goto :goto_3

    .line 931
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/GeoLookout/db/DisasterInfo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1041
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1042
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1043
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-static {v0, p2}, Lama;->a(Lcom/sec/android/GeoLookout/db/DisasterInfo;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 1044
    sget-object v3, Lama;->d:Landroid/net/Uri;

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1042
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1046
    :cond_0
    return-void
.end method

.method public static b(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1103
    invoke-static {p0, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1099
    sget-object v0, Lama;->g:Landroid/net/Uri;

    invoke-static {v0, p0}, Lama;->b(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/GeoLookout/db/DisasterInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 1013
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1014
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1017
    :try_start_0
    sget-object v1, Lama;->d:Landroid/net/Uri;

    sget-object v2, Lama;->n:Lamh;

    invoke-virtual {v2}, Lamh;->a()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "_id DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1018
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1020
    :cond_0
    invoke-static {v1}, Lama;->a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 1021
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1022
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    .line 1028
    if-eqz v1, :cond_1

    .line 1029
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object v0, v6

    .line 1032
    :goto_0
    return-object v0

    .line 1028
    :cond_2
    if-eqz v1, :cond_3

    .line 1029
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    move-object v0, v6

    .line 1032
    goto :goto_0

    .line 1025
    :catch_0
    move-exception v0

    move-object v1, v7

    .line 1026
    :goto_2
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1028
    if-eqz v1, :cond_3

    .line 1029
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1028
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_3
    if-eqz v1, :cond_4

    .line 1029
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 1028
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 1025
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 942
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 943
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 947
    :try_start_0
    invoke-static {p1}, Lama;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lama;->n:Lamh;

    invoke-virtual {v2}, Lamh;->a()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "_id DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 948
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 950
    :cond_0
    const/4 v0, 0x5

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 951
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    .line 957
    if-eqz v1, :cond_1

    .line 958
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object v0, v6

    .line 962
    :goto_0
    return-object v0

    .line 957
    :cond_2
    if-eqz v1, :cond_3

    .line 958
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    move-object v0, v6

    .line 962
    goto :goto_0

    .line 954
    :catch_0
    move-exception v0

    move-object v1, v7

    .line 955
    :goto_2
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 957
    if-eqz v1, :cond_3

    .line 958
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 957
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_3
    if-eqz v1, :cond_4

    .line 958
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 957
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 954
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1073
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1074
    if-eqz p2, :cond_0

    .line 1075
    sget-object v1, Lama;->d:Landroid/net/Uri;

    const-string v2, "LINKEDBEDID=? AND DID=?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    aput-object p1, v3, v4

    aput-object p2, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1084
    :goto_0
    return-void

    .line 1080
    :cond_0
    sget-object v1, Lama;->d:Landroid/net/Uri;

    const-string v2, "LINKEDBEDID=?"

    new-array v3, v5, [Ljava/lang/String;

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/GeoLookout/db/DisasterInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 966
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 967
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 971
    :try_start_0
    invoke-static {p1}, Lama;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lama;->n:Lamh;

    invoke-virtual {v2}, Lamh;->a()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "_id DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 972
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 974
    :cond_0
    invoke-static {v1}, Lama;->a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 975
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 976
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    .line 982
    if-eqz v1, :cond_1

    .line 983
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object v0, v6

    .line 986
    :goto_0
    return-object v0

    .line 982
    :cond_2
    if-eqz v1, :cond_3

    .line 983
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    move-object v0, v6

    .line 986
    goto :goto_0

    .line 979
    :catch_0
    move-exception v0

    move-object v1, v7

    .line 980
    :goto_2
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 982
    if-eqz v1, :cond_3

    .line 983
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 982
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_3
    if-eqz v1, :cond_4

    .line 983
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 982
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 979
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static c(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1049
    const-string v0, "deleteAll"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1050
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1052
    if-eqz v0, :cond_0

    .line 1053
    sget-object v1, Lama;->d:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1057
    :goto_0
    return-void

    .line 1055
    :cond_0
    const-string v0, "resolver is null !!"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/GeoLookout/db/DisasterInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 990
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 991
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 994
    :try_start_0
    invoke-static {p1}, Lama;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lama;->n:Lamh;

    invoke-virtual {v2}, Lamh;->a()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "_id DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 995
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 997
    :cond_0
    invoke-static {v1}, Lama;->a(Landroid/database/Cursor;)Lcom/sec/android/GeoLookout/db/DisasterInfo;

    move-result-object v0

    .line 998
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 999
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    .line 1005
    if-eqz v1, :cond_1

    .line 1006
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    move-object v0, v6

    .line 1009
    :goto_0
    return-object v0

    .line 1005
    :cond_2
    if-eqz v1, :cond_3

    .line 1006
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    move-object v0, v6

    .line 1009
    goto :goto_0

    .line 1002
    :catch_0
    move-exception v0

    move-object v1, v7

    .line 1003
    :goto_2
    :try_start_2
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1005
    if-eqz v1, :cond_3

    .line 1006
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1005
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_3
    if-eqz v1, :cond_4

    .line 1006
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 1005
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 1002
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static e(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1060
    const-string v0, "deleteOldByDid"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1061
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1064
    :try_start_0
    sget-object v1, Lama;->d:Landroid/net/Uri;

    const-string v2, "LINKEDBEDID=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1070
    :goto_0
    return-void

    .line 1067
    :catch_0
    move-exception v0

    .line 1068
    invoke-static {v0}, Lalj;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
