.class public final enum Lamw;
.super Ljava/lang/Enum;
.source "LifemodeDBStructure.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lamw;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lamw;

.field public static final enum b:Lamw;

.field public static final enum c:Lamw;

.field public static final enum d:Lamw;

.field public static final enum e:Lamw;

.field public static final enum f:Lamw;

.field public static final enum g:Lamw;

.field public static final enum h:Lamw;

.field public static final enum i:Lamw;

.field public static final enum j:Lamw;

.field public static final enum k:Lamw;

.field private static final synthetic l:[Lamw;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 67
    new-instance v0, Lamw;

    const-string v1, "life_pm10"

    invoke-direct {v0, v1, v3}, Lamw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamw;->a:Lamw;

    new-instance v0, Lamw;

    const-string v1, "life_cold"

    invoke-direct {v0, v1, v4}, Lamw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamw;->b:Lamw;

    new-instance v0, Lamw;

    const-string v1, "life_freez"

    invoke-direct {v0, v1, v5}, Lamw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamw;->c:Lamw;

    new-instance v0, Lamw;

    const-string v1, "life_fros"

    invoke-direct {v0, v1, v6}, Lamw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamw;->d:Lamw;

    new-instance v0, Lamw;

    const-string v1, "life_f_poison"

    invoke-direct {v0, v1, v7}, Lamw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamw;->e:Lamw;

    new-instance v0, Lamw;

    const-string v1, "life_hum"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lamw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamw;->f:Lamw;

    new-instance v0, Lamw;

    const-string v1, "life_pollen"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lamw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamw;->g:Lamw;

    new-instance v0, Lamw;

    const-string v1, "life_putre"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lamw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamw;->h:Lamw;

    new-instance v0, Lamw;

    const-string v1, "life_s_temp"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lamw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamw;->i:Lamw;

    new-instance v0, Lamw;

    const-string v1, "life_temp"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lamw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamw;->j:Lamw;

    new-instance v0, Lamw;

    const-string v1, "life_uv"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lamw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lamw;->k:Lamw;

    .line 66
    const/16 v0, 0xb

    new-array v0, v0, [Lamw;

    sget-object v1, Lamw;->a:Lamw;

    aput-object v1, v0, v3

    sget-object v1, Lamw;->b:Lamw;

    aput-object v1, v0, v4

    sget-object v1, Lamw;->c:Lamw;

    aput-object v1, v0, v5

    sget-object v1, Lamw;->d:Lamw;

    aput-object v1, v0, v6

    sget-object v1, Lamw;->e:Lamw;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lamw;->f:Lamw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lamw;->g:Lamw;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lamw;->h:Lamw;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lamw;->i:Lamw;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lamw;->j:Lamw;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lamw;->k:Lamw;

    aput-object v2, v0, v1

    sput-object v0, Lamw;->l:[Lamw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lamw;
    .locals 1

    .prologue
    .line 66
    const-class v0, Lamw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lamw;

    return-object v0
.end method

.method public static values()[Lamw;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lamw;->l:[Lamw;

    invoke-virtual {v0}, [Lamw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lamw;

    return-object v0
.end method
