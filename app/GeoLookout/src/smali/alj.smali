.class public Lalj;
.super Ljava/lang/Object;
.source "L.java"


# static fields
.field public static final a:Z = true

.field public static b:Z = false

.field private static final c:Ljava/lang/String; = "GeoLookout"

.field private static final d:Z

.field private static e:Lorg/slf4j/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    const/4 v0, 0x0

    sput-boolean v0, Lalj;->b:Z

    .line 29
    const-string v0, "user"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lalj;->d:Z

    .line 31
    const/4 v0, 0x0

    sput-object v0, Lalj;->e:Lorg/slf4j/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()Lorg/slf4j/Logger;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation

    .prologue
    .line 35
    const-string v0, "GeoLookout"

    invoke-static {v0}, Lorg/slf4j/LoggerFactory;->getLogger(Ljava/lang/String;)Lorg/slf4j/Logger;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation

    .prologue
    .line 40
    invoke-static {}, Lorg/slf4j/LoggerFactory;->getILoggerFactory()Lorg/slf4j/ILoggerFactory;

    move-result-object v0

    check-cast v0, Lch/qos/logback/classic/LoggerContext;

    .line 41
    invoke-virtual {v0}, Lch/qos/logback/classic/LoggerContext;->reset()V

    .line 43
    new-instance v1, Lch/qos/logback/classic/joran/JoranConfigurator;

    invoke-direct {v1}, Lch/qos/logback/classic/joran/JoranConfigurator;-><init>()V

    .line 44
    invoke-virtual {v1, v0}, Lch/qos/logback/classic/joran/JoranConfigurator;->setContext(Lch/qos/logback/core/Context;)V

    .line 47
    :try_start_0
    invoke-static {}, Land;->a()Land;

    move-result-object v0

    invoke-virtual {v0, p0}, Land;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v2, "logback_debug.xml"

    invoke-virtual {v0, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, Lch/qos/logback/classic/joran/JoranConfigurator;->doConfigure(Ljava/io/InputStream;)V

    .line 60
    :goto_0
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-nez v0, :cond_0

    .line 61
    invoke-static {}, Lalj;->a()Lorg/slf4j/Logger;

    move-result-object v0

    sput-object v0, Lalj;->e:Lorg/slf4j/Logger;

    .line 62
    const-string v0, "GeoLookout"

    const-string v1, "startLogger : log is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :cond_0
    :goto_1
    return-void

    .line 51
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    const-string v3, "logback_geolookout.xml"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 53
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 54
    invoke-virtual {v1, v0}, Lch/qos/logback/classic/joran/JoranConfigurator;->doConfigure(Ljava/io/File;)V
    :try_end_0
    .catch Lch/qos/logback/core/joran/spi/JoranException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 64
    :catch_0
    move-exception v0

    .line 65
    invoke-virtual {v0}, Lch/qos/logback/core/joran/spi/JoranException;->printStackTrace()V

    goto :goto_1

    .line 56
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v2, "logback.xml"

    invoke-virtual {v0, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v1, v0}, Lch/qos/logback/classic/joran/JoranConfigurator;->doConfigure(Ljava/io/InputStream;)V
    :try_end_1
    .catch Lch/qos/logback/core/joran/spi/JoranException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 66
    :catch_1
    move-exception v0

    .line 67
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 74
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 75
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;)V

    .line 82
    :goto_0
    return-void

    .line 78
    :cond_0
    const-string v0, "GeoLookout"

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 85
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 86
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 92
    :goto_0
    return-void

    .line 89
    :cond_0
    const-string v0, "GeoLookout"

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 95
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 96
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lorg/slf4j/Logger;->trace(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 102
    :goto_0
    return-void

    .line 99
    :cond_0
    const-string v0, "GeoLookout"

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 107
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 108
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;)V

    .line 114
    :goto_0
    return-void

    .line 111
    :cond_0
    const-string v0, "GeoLookout"

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 117
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 118
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 124
    :goto_0
    return-void

    .line 121
    :cond_0
    const-string v0, "GeoLookout"

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static b(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 127
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 128
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lorg/slf4j/Logger;->debug(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 134
    :goto_0
    return-void

    .line 131
    :cond_0
    const-string v0, "GeoLookout"

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 139
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 140
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 146
    :goto_0
    return-void

    .line 143
    :cond_0
    const-string v0, "GeoLookout"

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 149
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 150
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 156
    :goto_0
    return-void

    .line 153
    :cond_0
    const-string v0, "GeoLookout"

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static c(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 159
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 160
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 166
    :goto_0
    return-void

    .line 163
    :cond_0
    const-string v0, "GeoLookout"

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 171
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 172
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    .line 178
    :goto_0
    return-void

    .line 175
    :cond_0
    const-string v0, "GeoLookout"

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 181
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 182
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 188
    :goto_0
    return-void

    .line 185
    :cond_0
    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static d(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 191
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 192
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 198
    :goto_0
    return-void

    .line 195
    :cond_0
    const-string v0, "GeoLookout"

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 203
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 204
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;)V

    .line 210
    :goto_0
    return-void

    .line 207
    :cond_0
    const-string v0, "GeoLookout"

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 213
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 214
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 220
    :goto_0
    return-void

    .line 217
    :cond_0
    const-string v0, "GeoLookout"

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static e(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 223
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 224
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lorg/slf4j/Logger;->error(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 230
    :goto_0
    return-void

    .line 227
    :cond_0
    const-string v0, "GeoLookout"

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static f(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 234
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 235
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;)V

    .line 236
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;)V

    .line 243
    :goto_0
    return-void

    .line 239
    :cond_0
    const-string v0, "GeoLookout"

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    const-string v0, "GeoLookout"

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static f(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 246
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 247
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 248
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 255
    :goto_0
    return-void

    .line 251
    :cond_0
    const-string v0, "GeoLookout"

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 252
    const-string v0, "GeoLookout"

    invoke-static {p0}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static f(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 258
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    if-eqz v0, :cond_0

    .line 259
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lorg/slf4j/Logger;->info(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 260
    sget-object v0, Lalj;->e:Lorg/slf4j/Logger;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Lorg/slf4j/Logger;->warn(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 267
    :goto_0
    return-void

    .line 263
    :cond_0
    const-string v0, "GeoLookout"

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 264
    const-string v0, "GeoLookout"

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static g(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 270
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    const/4 v1, 0x4

    aget-object v0, v0, v1

    .line 271
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "at ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 273
    sget-boolean v1, Lalj;->d:Z

    if-eqz v1, :cond_0

    .line 274
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "H: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v0}, Lakt;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 277
    :cond_0
    return-object v0
.end method
