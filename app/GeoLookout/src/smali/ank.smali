.class Lank;
.super Lcom/loopj/android/http/AsyncHttpResponseHandler;
.source "DisasterServerInterface.java"


# instance fields
.field final synthetic a:Z

.field final synthetic b:D

.field final synthetic c:D

.field final synthetic d:Z

.field final synthetic e:Lanh;


# direct methods
.method constructor <init>(Lanh;ZDDZ)V
    .locals 1

    .prologue
    .line 659
    iput-object p1, p0, Lank;->e:Lanh;

    iput-boolean p2, p0, Lank;->a:Z

    iput-wide p3, p0, Lank;->b:D

    iput-wide p5, p0, Lank;->c:D

    iput-boolean p7, p0, Lank;->d:Z

    invoke-direct {p0}, Lcom/loopj/android/http/AsyncHttpResponseHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 681
    invoke-super {p0, p1, p2, p3, p4}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onFailure(I[Lorg/apache/http/Header;[BLjava/lang/Throwable;)V

    .line 682
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "queryLocationToDisasterServer: responseHandler : onFailure "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 683
    iget-object v0, p0, Lank;->e:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 684
    iget-object v0, p0, Lank;->e:Lanh;

    invoke-static {v0}, Lanh;->f(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 685
    iget-object v0, p0, Lank;->e:Lanh;

    invoke-static {v0}, Lanh;->f(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 686
    iget-object v0, p0, Lank;->e:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lank;->e:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lage;->d(Landroid/content/Context;Z)V

    .line 690
    :cond_0
    iget-object v0, p0, Lank;->e:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 691
    iget-object v0, p0, Lank;->e:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lank;->e:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lage;->a(Landroid/content/Context;I)V

    .line 693
    :cond_1
    iget-object v0, p0, Lank;->e:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 694
    iget-object v0, p0, Lank;->e:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lank;->e:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lage;->b(Landroid/content/Context;I)V

    .line 696
    :cond_2
    return-void
.end method

.method public onFinish()V
    .locals 2

    .prologue
    .line 700
    const-string v0, "queryLocationToDisasterServer: onFinish()  "

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 701
    iget-object v0, p0, Lank;->e:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 702
    iget-object v0, p0, Lank;->e:Lanh;

    invoke-static {v0}, Lanh;->f(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 703
    iget-object v0, p0, Lank;->e:Lanh;

    invoke-static {v0}, Lanh;->f(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 705
    :cond_0
    return-void
.end method

.method public onRetry()V
    .locals 1

    .prologue
    .line 709
    invoke-super {p0}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onRetry()V

    .line 710
    const-string v0, "queryLocationToDisasterServer: responseHandler : onRetry"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 711
    return-void
.end method

.method public onSuccess(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 663
    invoke-super {p0, p1}, Lcom/loopj/android/http/AsyncHttpResponseHandler;->onSuccess(Ljava/lang/String;)V

    .line 664
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "queryLocationToDisasterServer: responseHandler : onSuccess "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->f(Ljava/lang/String;)V

    .line 666
    iget-boolean v0, p0, Lank;->a:Z

    if-eqz v0, :cond_1

    .line 667
    iget-object v0, p0, Lank;->e:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->A(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 668
    iget-object v0, p0, Lank;->e:Lanh;

    invoke-static {v0}, Lanh;->f(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 669
    iget-object v0, p0, Lank;->e:Lanh;

    invoke-static {v0}, Lanh;->f(Lanh;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 670
    iget-object v0, p0, Lank;->e:Lanh;

    invoke-static {v0}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lank;->e:Lanh;

    invoke-static {v1}, Lanh;->b(Lanh;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lage;->o(Landroid/content/Context;)V

    .line 672
    :cond_0
    iget-object v0, p0, Lank;->e:Lanh;

    iget-wide v2, p0, Lank;->b:D

    iget-wide v4, p0, Lank;->c:D

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lanh;->a(Ljava/lang/String;DD)V

    .line 676
    :goto_0
    return-void

    .line 674
    :cond_1
    iget-object v1, p0, Lank;->e:Lanh;

    iget-boolean v3, p0, Lank;->d:Z

    iget-wide v4, p0, Lank;->b:D

    iget-wide v6, p0, Lank;->c:D

    move-object v2, p1

    invoke-static/range {v1 .. v7}, Lanh;->a(Lanh;Ljava/lang/String;ZDD)V

    goto :goto_0
.end method
