.class public final Luy;
.super Ldu;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldu",
        "<",
        "Lgj;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Luy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Luy;

    invoke-direct {v0}, Luy;-><init>()V

    sput-object v0, Luy;->a:Luy;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const-string v0, "com.google.android.gms.ads.AdManagerCreatorImpl"

    invoke-direct {p0, v0}, Ldu;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/internal/x;Ljava/lang/String;Lhp;)Lgg;
    .locals 6

    const v1, 0x3d8024

    invoke-static {p0}, Lcf;->a(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Luy;->a:Luy;

    invoke-direct {v0, p0, p1, p2, p3}, Luy;->b(Landroid/content/Context;Lcom/google/android/gms/internal/x;Ljava/lang/String;Lhp;)Lgg;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "Using AdManager from the client jar."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    new-instance v5, Lcom/google/android/gms/internal/co;

    const/4 v0, 0x1

    invoke-direct {v5, v1, v1, v0}, Lcom/google/android/gms/internal/co;-><init>(IIZ)V

    new-instance v0, Lut;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lut;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/x;Ljava/lang/String;Lhq;Lcom/google/android/gms/internal/co;)V

    :cond_1
    return-object v0
.end method

.method private b(Landroid/content/Context;Lcom/google/android/gms/internal/x;Ljava/lang/String;Lhp;)Lgg;
    .locals 7

    const/4 v6, 0x0

    :try_start_0
    invoke-static {p1}, Lds;->a(Ljava/lang/Object;)Ldp;

    move-result-object v1

    invoke-virtual {p0, p1}, Luy;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgj;

    const v5, 0x3d8024

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-interface/range {v0 .. v5}, Lgj;->a(Ldp;Lcom/google/android/gms/internal/x;Ljava/lang/String;Lhq;I)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lgh;->a(Landroid/os/IBinder;)Lgg;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ldv; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "Could not create remote AdManager."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v6

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "Could not create remote AdManager."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v6

    goto :goto_0
.end method


# virtual methods
.method protected synthetic a(Landroid/os/IBinder;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Luy;->b(Landroid/os/IBinder;)Lgj;

    move-result-object v0

    return-object v0
.end method

.method protected b(Landroid/os/IBinder;)Lgj;
    .locals 1

    invoke-static {p1}, Lgk;->a(Landroid/os/IBinder;)Lgj;

    move-result-object v0

    return-object v0
.end method
