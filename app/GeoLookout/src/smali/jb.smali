.class public final Ljb;
.super Ljava/lang/Object;


# direct methods
.method public static a(Lk;)I
    .locals 2

    sget-object v0, Ljc;->b:[I

    invoke-virtual {p0}, Lk;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Ll;)I
    .locals 2

    sget-object v0, Ljc;->a:[I

    invoke-virtual {p0}, Ll;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(I)Ll;
    .locals 1

    packed-switch p0, :pswitch_data_0

    sget-object v0, Ll;->a:Ll;

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Ll;->c:Ll;

    goto :goto_0

    :pswitch_1
    sget-object v0, Ll;->b:Ll;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/google/android/gms/internal/x;)Lm;
    .locals 5

    new-instance v0, Lm;

    new-instance v1, Laz;

    iget v2, p0, Lcom/google/android/gms/internal/x;->g:I

    iget v3, p0, Lcom/google/android/gms/internal/x;->d:I

    iget-object v4, p0, Lcom/google/android/gms/internal/x;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Laz;-><init>(IILjava/lang/String;)V

    invoke-direct {v0, v1}, Lm;-><init>(Laz;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/internal/v;)Lo;
    .locals 6

    iget-object v0, p0, Lcom/google/android/gms/internal/v;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/internal/v;->f:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    :goto_0
    new-instance v1, Lo;

    new-instance v2, Ljava/util/Date;

    iget-wide v4, p0, Lcom/google/android/gms/internal/v;->c:J

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    iget v3, p0, Lcom/google/android/gms/internal/v;->e:I

    invoke-static {v3}, Ljb;->a(I)Ll;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/gms/internal/v;->g:Z

    invoke-direct {v1, v2, v3, v0, v4}, Lo;-><init>(Ljava/util/Date;Ll;Ljava/util/Set;Z)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final b(I)Lk;
    .locals 1

    packed-switch p0, :pswitch_data_0

    sget-object v0, Lk;->d:Lk;

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lk;->a:Lk;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lk;->c:Lk;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lk;->b:Lk;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
