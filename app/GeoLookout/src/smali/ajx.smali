.class public Lajx;
.super Landroid/content/BroadcastReceiver;
.source "DisasterMultiPointSaveActivity.java"


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 282
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MULTIPOINT mUpdateMultiPointReceiver "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 283
    const-string v0, "com.sec.android.geolookout.resultlocation"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 284
    const-string v0, "update_result"

    invoke-virtual {p2, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 287
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 288
    const-string v2, "multi_point_edit_id"

    iget-object v3, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v3}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 289
    if-eqz v0, :cond_3

    .line 290
    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->setResult(ILandroid/content/Intent;)V

    .line 291
    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->b(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->c(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->d(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 292
    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->c(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->e(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->d(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->f(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 293
    :cond_0
    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)I

    move-result v1

    invoke-static {v0, v1}, Lalx;->d(Landroid/content/Context;I)V

    .line 296
    :cond_1
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    iget-object v1, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lalu;->a(Landroid/content/Context;I)V

    .line 322
    :cond_2
    :goto_0
    return-void

    .line 298
    :cond_3
    const v0, 0x7f0a0014

    invoke-static {p1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 299
    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->b(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 300
    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->g(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)V

    .line 304
    :goto_1
    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    const/16 v2, -0xf

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->setResult(ILandroid/content/Intent;)V

    .line 305
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    const-class v2, Lcom/sec/android/GeoLookout/DisasterService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 307
    const-string v1, "com.sec.android.geolookout.updatelocation"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 308
    iget-object v1, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 309
    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->h(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->h(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 310
    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->h(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 312
    :cond_4
    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->finish()V

    goto :goto_0

    .line 302
    :cond_5
    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v2}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)I

    move-result v2

    invoke-static {v0, v2}, Lalz;->a(Landroid/content/Context;I)V

    goto :goto_1

    .line 314
    :cond_6
    const-string v0, "com.sec.android.geolookout.resultrefresh"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 315
    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    iget-object v1, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->i(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 316
    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->h(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->h(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 317
    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->h(Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 319
    :cond_7
    iget-object v0, p0, Lajx;->a:Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterMultiPointSaveActivity;->finish()V

    goto/16 :goto_0
.end method
