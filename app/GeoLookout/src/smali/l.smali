.class public final enum Ll;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ll;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ll;

.field public static final enum b:Ll;

.field public static final enum c:Ll;

.field private static final synthetic d:[Ll;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Ll;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Ll;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ll;->a:Ll;

    new-instance v0, Ll;

    const-string v1, "MALE"

    invoke-direct {v0, v1, v3}, Ll;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ll;->b:Ll;

    new-instance v0, Ll;

    const-string v1, "FEMALE"

    invoke-direct {v0, v1, v4}, Ll;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ll;->c:Ll;

    const/4 v0, 0x3

    new-array v0, v0, [Ll;

    sget-object v1, Ll;->a:Ll;

    aput-object v1, v0, v2

    sget-object v1, Ll;->b:Ll;

    aput-object v1, v0, v3

    sget-object v1, Ll;->c:Ll;

    aput-object v1, v0, v4

    sput-object v0, Ll;->d:[Ll;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ll;
    .locals 1

    const-class v0, Ll;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ll;

    return-object v0
.end method

.method public static values()[Ll;
    .locals 1

    sget-object v0, Ll;->d:[Ll;

    invoke-virtual {v0}, [Ll;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ll;

    return-object v0
.end method
