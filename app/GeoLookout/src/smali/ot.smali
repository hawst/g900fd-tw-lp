.class public final Lot;
.super Lmz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmz",
        "<",
        "Lqq;",
        ">;"
    }
.end annotation


# instance fields
.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lqx;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lcom/google/android/gms/games/PlayerEntity;

.field private j:Lcom/google/android/gms/games/GameEntity;

.field private final k:Lqt;

.field private l:Z

.field private final m:Landroid/os/Binder;

.field private final n:J

.field private final o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcb;Lcc;[Ljava/lang/String;ILandroid/view/View;Z)V
    .locals 2

    invoke-direct {p0, p1, p4, p5, p6}, Lmz;-><init>(Landroid/content/Context;Lcb;Lcc;[Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lot;->l:Z

    iput-object p2, p0, Lot;->f:Ljava/lang/String;

    invoke-static {p3}, Lnx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lot;->g:Ljava/lang/String;

    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lot;->m:Landroid/os/Binder;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lot;->h:Ljava/util/Map;

    invoke-static {p0, p7}, Lqt;->a(Lot;I)Lqt;

    move-result-object v0

    iput-object v0, p0, Lot;->k:Lqt;

    invoke-virtual {p0, p8}, Lot;->a(Landroid/view/View;)V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lot;->n:J

    iput-boolean p9, p0, Lot;->o:Z

    return-void
.end method

.method private a(Lcom/google/android/gms/common/data/d;)Lcom/google/android/gms/games/multiplayer/realtime/Room;
    .locals 3

    new-instance v1, Lfy;

    invoke-direct {v1, p1}, Lfy;-><init>(Lcom/google/android/gms/common/data/d;)V

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1}, Lfy;->a()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lfy;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/Room;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {v1}, Lfy;->b()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lfy;->b()V

    throw v0
.end method

.method static synthetic a(Lot;Lcom/google/android/gms/common/data/d;)Lcom/google/android/gms/games/multiplayer/realtime/Room;
    .locals 1

    invoke-direct {p0, p1}, Lot;->a(Lcom/google/android/gms/common/data/d;)Lcom/google/android/gms/games/multiplayer/realtime/Room;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)Lqx;
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    invoke-interface {v0, p1}, Lqq;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    const-string v2, "GamesClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Creating a socket to bind to:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lqm;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Landroid/net/LocalSocket;

    invoke-direct {v2}, Landroid/net/LocalSocket;-><init>()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    new-instance v3, Landroid/net/LocalSocketAddress;

    invoke-direct {v3, v0}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    new-instance v0, Lqx;

    invoke-direct {v0, v2, p1}, Lqx;-><init>(Landroid/net/LocalSocket;Ljava/lang/String;)V

    iget-object v2, p0, Lot;->h:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v2, "Unable to create socket. Service died."

    invoke-static {v0, v2}, Lqm;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v2, "GamesClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "connect() call failed on socket: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lqm;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    move-object v0, v1

    goto :goto_0
.end method

.method private y()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lot;->i:Lcom/google/android/gms/games/PlayerEntity;

    return-void
.end method

.method private z()V
    .locals 4

    iget-object v0, p0, Lot;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqx;

    :try_start_0
    invoke-virtual {v0}, Lqx;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "GamesClient"

    const-string v3, "IOException:"

    invoke-static {v2, v3, v0}, Lqm;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lot;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method


# virtual methods
.method public a(Lfq;[BLjava/lang/String;Ljava/lang/String;)I
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    new-instance v1, Lpc;

    invoke-direct {v1, p0, p1}, Lpc;-><init>(Lot;Lfq;)V

    invoke-interface {v0, v1, p2, p3, p4}, Lqq;->a(Lqn;[BLjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a([BLjava/lang/String;)I
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1}, Lqq;->a([BLjava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a([BLjava/lang/String;[Ljava/lang/String;)I
    .locals 2

    const-string v0, "Participant IDs must not be null"

    invoke-static {p3, v0}, Lnx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    invoke-interface {v0, p1, p2, p3}, Lqq;->a([BLjava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(II)Landroid/content/Intent;
    .locals 2

    invoke-virtual {p0}, Lot;->n()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SELECT_PLAYERS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.games.MIN_SELECTIONS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.MAX_SELECTIONS"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {v0}, Lql;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/games/multiplayer/realtime/Room;I)Landroid/content/Intent;
    .locals 3

    invoke-virtual {p0}, Lot;->n()V

    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.google.android.gms.games.SHOW_REAL_TIME_WAITING_ROOM"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "Room parameter must not be null"

    invoke-static {p1, v0}, Lnx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "room"

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "minParticipantsToStart must be >= 0"

    invoke-static {v0, v2}, Lnx;->a(ZLjava/lang/Object;)V

    const-string v0, "com.google.android.gms.games.MIN_PARTICIPANTS_TO_START"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {v1}, Lql;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    invoke-virtual {p0}, Lot;->n()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.VIEW_LEADERBOARD_SCORES"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.games.LEADERBOARD_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {v0}, Lql;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lfr;
    .locals 2

    if-eqz p2, :cond_0

    invoke-static {p2}, Lfk;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad participant ID"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lot;->h:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqx;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lqx;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    invoke-direct {p0, p2}, Lot;->b(Ljava/lang/String;)Lqx;

    move-result-object v0

    :cond_3
    return-object v0
.end method

.method protected a(Landroid/os/IBinder;)Lqq;
    .locals 1

    invoke-static {p1}, Lqr;->a(Landroid/os/IBinder;)Lqq;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 0

    invoke-direct {p0}, Lot;->y()V

    invoke-super {p0}, Lmz;->a()V

    return-void
.end method

.method public a(I)V
    .locals 1

    iget-object v0, p0, Lot;->k:Lqt;

    invoke-virtual {v0, p1}, Lqt;->b(I)V

    return-void
.end method

.method protected a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "show_welcome_popup"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lot;->l:Z

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lmz;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 2

    invoke-virtual {p0}, Lot;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    invoke-interface {v0, p1, p2}, Lqq;->a(Landroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lot;->k:Lqt;

    invoke-virtual {v0, p1}, Lqt;->a(Landroid/view/View;)V

    return-void
.end method

.method protected a(Lbz;)V
    .locals 1

    invoke-super {p0, p1}, Lmz;->a(Lbz;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lot;->l:Z

    return-void
.end method

.method public a(Lec;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    new-instance v1, Lpu;

    invoke-direct {v1, p0, p1}, Lpu;-><init>(Lot;Lec;)V

    invoke-interface {v0, v1}, Lqq;->d(Lqn;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Led;IZZ)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    new-instance v1, Loz;

    invoke-direct {v1, p0, p1}, Loz;-><init>(Lot;Led;)V

    invoke-interface {v0, v1, p2, p3, p4}, Lqq;->a(Lqn;IZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Led;Ljava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    new-instance v1, Loz;

    invoke-direct {v1, p0, p1}, Loz;-><init>(Lot;Led;)V

    invoke-interface {v0, v1, p2}, Lqq;->c(Lqn;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lee;)V
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    move-object v1, v0

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    invoke-interface {v0, v1}, Lqq;->a(Lqn;)V

    :goto_1
    return-void

    :cond_0
    new-instance v0, Lpi;

    invoke-direct {v0, p0, p1}, Lpi;-><init>(Lot;Lee;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Lel;Ljava/lang/String;)V
    .locals 4

    if-nez p1, :cond_0

    const/4 v0, 0x0

    move-object v1, v0

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    iget-object v2, p0, Lot;->k:Lqt;

    invoke-virtual {v2}, Lqt;->c()Landroid/os/IBinder;

    move-result-object v2

    iget-object v3, p0, Lot;->k:Lqt;

    invoke-virtual {v3}, Lqt;->b()Landroid/os/Bundle;

    move-result-object v3

    invoke-interface {v0, v1, p2, v2, v3}, Lqq;->a(Lqn;Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V

    :goto_1
    return-void

    :cond_0
    new-instance v0, Lpo;

    invoke-direct {v0, p0, p1}, Lpo;-><init>(Lot;Lel;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Lel;Ljava/lang/String;I)V
    .locals 6

    if-nez p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    iget-object v2, p0, Lot;->k:Lqt;

    invoke-virtual {v2}, Lqt;->c()Landroid/os/IBinder;

    move-result-object v4

    iget-object v2, p0, Lot;->k:Lqt;

    invoke-virtual {v2}, Lqt;->b()Landroid/os/Bundle;

    move-result-object v5

    move-object v2, p2

    move v3, p3

    invoke-interface/range {v0 .. v5}, Lqq;->a(Lqn;Ljava/lang/String;ILandroid/os/IBinder;Landroid/os/Bundle;)V

    :goto_1
    return-void

    :cond_0
    new-instance v1, Lpo;

    invoke-direct {v1, p0, p1}, Lpo;-><init>(Lot;Lel;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Lem;Z)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    new-instance v1, Lpq;

    invoke-direct {v1, p0, p1}, Lpq;-><init>(Lot;Lem;)V

    invoke-interface {v0, v1, p2}, Lqq;->b(Lqn;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Leu;Ljava/lang/String;Z)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    new-instance v1, Lqd;

    invoke-direct {v1, p0, p1}, Lqd;-><init>(Lot;Leu;)V

    invoke-interface {v0, v1, p2, p3}, Lqq;->c(Lqn;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Leu;Z)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    new-instance v1, Lqd;

    invoke-direct {v1, p0, p1}, Lqd;-><init>(Lot;Leu;)V

    invoke-interface {v0, v1, p2}, Lqq;->c(Lqn;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lev;Les;II)V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    new-instance v1, Lqb;

    invoke-direct {v1, p0, p1}, Lqb;-><init>(Lot;Lev;)V

    invoke-virtual {p2}, Les;->e()Lfa;

    move-result-object v2

    invoke-virtual {v2}, Lfa;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-interface {v0, v1, v2, p3, p4}, Lqq;->a(Lqn;Landroid/os/Bundle;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lev;Ljava/lang/String;IIIZ)V
    .locals 7

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    new-instance v1, Lqb;

    invoke-direct {v1, p0, p1}, Lqb;-><init>(Lot;Lev;)V

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lqq;->a(Lqn;Ljava/lang/String;IIIZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lew;Ljava/lang/String;JLjava/lang/String;)V
    .locals 7

    if-nez p1, :cond_0

    const/4 v2, 0x0

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lqq;

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-interface/range {v1 .. v6}, Lqq;->a(Lqn;Ljava/lang/String;JLjava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    new-instance v2, Lpk;

    invoke-direct {v2, p0, p1}, Lpk;-><init>(Lot;Lew;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Lfg;)V
    .locals 4

    :try_start_0
    new-instance v1, Lpw;

    invoke-direct {v1, p0, p1}, Lpw;-><init>(Lot;Lfg;)V

    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    iget-wide v2, p0, Lot;->n:J

    invoke-interface {v0, v1, v2, v3}, Lqq;->a(Lqn;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lfh;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    new-instance v1, Lpy;

    invoke-direct {v1, p0, p1}, Lpy;-><init>(Lot;Lfh;)V

    invoke-interface {v0, v1}, Lqq;->e(Lqn;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lfs;)V
    .locals 10

    :try_start_0
    new-instance v2, Lpe;

    invoke-virtual {p1}, Lfs;->a()Lfx;

    move-result-object v0

    invoke-virtual {p1}, Lfs;->c()Lfw;

    move-result-object v1

    invoke-virtual {p1}, Lfs;->d()Lfp;

    move-result-object v3

    invoke-direct {v2, p0, v0, v1, v3}, Lpe;-><init>(Lot;Lfx;Lfw;Lfp;)V

    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lqq;

    iget-object v3, p0, Lot;->m:Landroid/os/Binder;

    invoke-virtual {p1}, Lfs;->e()I

    move-result v4

    invoke-virtual {p1}, Lfs;->f()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lfs;->g()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {p1}, Lfs;->h()Z

    move-result v7

    iget-wide v8, p0, Lot;->n:J

    invoke-interface/range {v1 .. v9}, Lqq;->a(Lqn;Landroid/os/IBinder;I[Ljava/lang/String;Landroid/os/Bundle;ZJ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lfx;Ljava/lang/String;)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    new-instance v1, Lpe;

    invoke-direct {v1, p0, p1}, Lpe;-><init>(Lot;Lfx;)V

    invoke-interface {v0, v1, p2}, Lqq;->e(Lqn;Ljava/lang/String;)V

    invoke-direct {p0}, Lot;->z()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;I)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    invoke-interface {v0, p1, p2}, Lqq;->b(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Lno;Lnd;)V
    .locals 10

    invoke-virtual {p0}, Lot;->i()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    const-string v0, "com.google.android.gms.games.key.isHeadless"

    iget-boolean v1, p0, Lot;->o:Z

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const v2, 0x3d8024

    invoke-virtual {p0}, Lot;->i()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lot;->g:Ljava/lang/String;

    invoke-virtual {p0}, Lot;->j()[Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lot;->f:Ljava/lang/String;

    iget-object v0, p0, Lot;->k:Lqt;

    invoke-virtual {v0}, Lqt;->c()Landroid/os/IBinder;

    move-result-object v7

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v9}, Lno;->a(Lnl;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    invoke-interface {v0, p1}, Lqq;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected varargs a([Ljava/lang/String;)V
    .locals 7

    const/4 v4, 0x1

    const/4 v1, 0x0

    move v0, v1

    move v2, v1

    move v3, v1

    :goto_0
    array-length v5, p1

    if-ge v0, v5, :cond_2

    aget-object v5, p1, v0

    const-string v6, "https://www.googleapis.com/auth/games"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v3, v4

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v6, "https://www.googleapis.com/auth/games.firstparty"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v2, v4

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_4

    if-nez v3, :cond_3

    move v0, v4

    :goto_2
    const-string v2, "Cannot have both %s and %s!"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v5, "https://www.googleapis.com/auth/games"

    aput-object v5, v3, v1

    const-string v1, "https://www.googleapis.com/auth/games.firstparty"

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lnx;->a(ZLjava/lang/Object;)V

    :goto_3
    return-void

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    const-string v0, "GamesClient requires %s to function."

    new-array v2, v4, [Ljava/lang/Object;

    const-string v4, "https://www.googleapis.com/auth/games"

    aput-object v4, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lnx;->a(ZLjava/lang/Object;)V

    goto :goto_3
.end method

.method protected synthetic b(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lot;->a(Landroid/os/IBinder;)Lqq;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    invoke-interface {v0, p1}, Lqq;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Lel;Ljava/lang/String;)V
    .locals 4

    if-nez p1, :cond_0

    const/4 v0, 0x0

    move-object v1, v0

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    iget-object v2, p0, Lot;->k:Lqt;

    invoke-virtual {v2}, Lqt;->c()Landroid/os/IBinder;

    move-result-object v2

    iget-object v3, p0, Lot;->k:Lqt;

    invoke-virtual {v3}, Lqt;->b()Landroid/os/Bundle;

    move-result-object v3

    invoke-interface {v0, v1, p2, v2, v3}, Lqq;->b(Lqn;Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V

    :goto_1
    return-void

    :cond_0
    new-instance v0, Lpo;

    invoke-direct {v0, p0, p1}, Lpo;-><init>(Lot;Lel;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public b(Lel;Ljava/lang/String;I)V
    .locals 6

    if-nez p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    iget-object v2, p0, Lot;->k:Lqt;

    invoke-virtual {v2}, Lqt;->c()Landroid/os/IBinder;

    move-result-object v4

    iget-object v2, p0, Lot;->k:Lqt;

    invoke-virtual {v2}, Lqt;->b()Landroid/os/Bundle;

    move-result-object v5

    move-object v2, p2

    move v3, p3

    invoke-interface/range {v0 .. v5}, Lqq;->b(Lqn;Ljava/lang/String;ILandroid/os/IBinder;Landroid/os/Bundle;)V

    :goto_1
    return-void

    :cond_0
    new-instance v1, Lpo;

    invoke-direct {v1, p0, p1}, Lpo;-><init>(Lot;Lel;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public b(Lev;Ljava/lang/String;IIIZ)V
    .locals 7

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    new-instance v1, Lqb;

    invoke-direct {v1, p0, p1}, Lqb;-><init>(Lot;Lev;)V

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lqq;->b(Lqn;Ljava/lang/String;IIIZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Lfs;)V
    .locals 8

    :try_start_0
    new-instance v2, Lpe;

    invoke-virtual {p1}, Lfs;->a()Lfx;

    move-result-object v0

    invoke-virtual {p1}, Lfs;->c()Lfw;

    move-result-object v1

    invoke-virtual {p1}, Lfs;->d()Lfp;

    move-result-object v3

    invoke-direct {v2, p0, v0, v1, v3}, Lpe;-><init>(Lot;Lfx;Lfw;Lfp;)V

    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lqq;

    iget-object v3, p0, Lot;->m:Landroid/os/Binder;

    invoke-virtual {p1}, Lfs;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lfs;->h()Z

    move-result v5

    iget-wide v6, p0, Lot;->n:J

    invoke-interface/range {v1 .. v7}, Lqq;->a(Lqn;Landroid/os/IBinder;Ljava/lang/String;ZJ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;I)V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    invoke-interface {v0, p1, p2}, Lqq;->a(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public d()V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lot;->l:Z

    invoke-virtual {p0}, Lot;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    invoke-interface {v0}, Lqq;->c()V

    iget-wide v2, p0, Lot;->n:J

    invoke-interface {v0, v2, v3}, Lqq;->b(J)V

    iget-wide v2, p0, Lot;->n:J

    invoke-interface {v0, v2, v3}, Lqq;->a(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-direct {p0}, Lot;->z()V

    invoke-super {p0}, Lmz;->d()V

    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "Failed to notify client disconnect."

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.games.service.START"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    invoke-interface {v0}, Lqq;->d()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    invoke-interface {v0}, Lqq;->e()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected k()V
    .locals 1

    invoke-super {p0}, Lmz;->k()V

    iget-boolean v0, p0, Lot;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lot;->k:Lqt;

    invoke-virtual {v0}, Lqt;->a()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lot;->l:Z

    :cond_0
    return-void
.end method

.method protected l()Landroid/os/Bundle;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    invoke-interface {v0}, Lqq;->b()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-class v1, Lot;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Lcom/google/android/gms/games/Player;
    .locals 2

    invoke-virtual {p0}, Lot;->n()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lot;->i:Lcom/google/android/gms/games/PlayerEntity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_1

    :try_start_1
    new-instance v1, Leg;

    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    invoke-interface {v0}, Lqq;->f()Lcom/google/android/gms/common/data/d;

    move-result-object v0

    invoke-direct {v1, v0}, Leg;-><init>(Lcom/google/android/gms/common/data/d;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v1}, Leg;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Leg;->a(I)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/PlayerEntity;

    iput-object v0, p0, Lot;->i:Lcom/google/android/gms/games/PlayerEntity;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    invoke-virtual {v1}, Leg;->b()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_1
    :goto_0
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    iget-object v0, p0, Lot;->i:Lcom/google/android/gms/games/PlayerEntity;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {v1}, Leg;->b()V

    throw v0
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catch_0
    move-exception v0

    :try_start_6
    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0
.end method

.method public q()Lcom/google/android/gms/games/Game;
    .locals 2

    invoke-virtual {p0}, Lot;->n()V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lot;->j:Lcom/google/android/gms/games/GameEntity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_1

    :try_start_1
    new-instance v1, Ldw;

    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    invoke-interface {v0}, Lqq;->h()Lcom/google/android/gms/common/data/d;

    move-result-object v0

    invoke-direct {v1, v0}, Ldw;-><init>(Lcom/google/android/gms/common/data/d;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v1}, Ldw;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ldw;->a(I)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/GameEntity;

    iput-object v0, p0, Lot;->j:Lcom/google/android/gms/games/GameEntity;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :try_start_3
    invoke-virtual {v1}, Ldw;->b()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_1
    :goto_0
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    iget-object v0, p0, Lot;->j:Lcom/google/android/gms/games/GameEntity;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {v1}, Ldw;->b()V

    throw v0
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catch_0
    move-exception v0

    :try_start_6
    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0
.end method

.method public r()Landroid/content/Intent;
    .locals 3

    invoke-virtual {p0}, Lot;->n()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.VIEW_LEADERBOARDS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Lot;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {v0}, Lql;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public s()Landroid/content/Intent;
    .locals 2

    invoke-virtual {p0}, Lot;->n()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.VIEW_ACHIEVEMENTS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {v0}, Lql;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public t()Landroid/content/Intent;
    .locals 3

    invoke-virtual {p0}, Lot;->n()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_INVITATIONS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Lot;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {v0}, Lql;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public u()V
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    iget-wide v2, p0, Lot;->n:J

    invoke-interface {v0, v2, v3}, Lqq;->b(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public v()Landroid/content/Intent;
    .locals 3

    invoke-virtual {p0}, Lot;->n()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    iget-object v2, p0, Lot;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {v0}, Lql;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public w()Ljava/lang/String;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    invoke-interface {v0}, Lqq;->a()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public x()V
    .locals 2

    invoke-virtual {p0}, Lot;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lot;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lqq;

    invoke-interface {v0}, Lqq;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GamesClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lqm;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
