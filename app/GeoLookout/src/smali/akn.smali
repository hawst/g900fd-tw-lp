.class Lakn;
.super Ljava/lang/Object;
.source "DisasterSettingsFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field final synthetic a:Lakf;


# direct methods
.method constructor <init>(Lakf;)V
    .locals 0

    .prologue
    .line 545
    iput-object p1, p0, Lakn;->a:Lakf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 548
    iget-object v0, p0, Lakn;->a:Lakf;

    invoke-virtual {v0}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "safety_care_disaster_user_agree"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 550
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lakn;->a:Lakf;

    invoke-virtual {v1}, Lakf;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/sec/android/GeoLookout/widget/SafetyCareDisasterDisclaimerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 552
    iget-object v1, p0, Lakn;->a:Lakf;

    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, Lakf;->startActivityForResult(Landroid/content/Intent;I)V

    .line 553
    return-void
.end method
