.class public Lahr;
.super Ljava/lang/Object;
.source "DisasterDashboardActivity.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Laif;

.field final synthetic b:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Laif;)V
    .locals 0

    .prologue
    .line 640
    iput-object p1, p0, Lahr;->b:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iput-object p2, p0, Lahr;->a:Laif;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 5

    .prologue
    const v4, 0x7f09010a

    const/4 v3, -0x2

    .line 643
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Layout getLineCount "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lahr;->a:Laif;

    iget-object v1, v1, Laif;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLineCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 644
    iget-object v0, p0, Lahr;->a:Laif;

    iget-object v0, v0, Laif;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 646
    iget-object v1, p0, Lahr;->a:Laif;

    iget-object v1, v1, Laif;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLineCount()I

    move-result v1

    const/4 v2, 0x4

    if-le v1, v2, :cond_2

    .line 647
    iget v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    iget-object v2, p0, Lahr;->b:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-virtual {v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 660
    :cond_0
    :goto_0
    return-void

    .line 651
    :cond_1
    iget-object v1, p0, Lahr;->b:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 659
    :goto_1
    iget-object v1, p0, Lahr;->a:Laif;

    iget-object v1, v1, Laif;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 654
    :cond_2
    iget v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    if-eq v1, v3, :cond_0

    .line 657
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    goto :goto_1
.end method
