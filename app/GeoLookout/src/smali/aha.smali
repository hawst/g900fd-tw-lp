.class public Laha;
.super Ljava/lang/Object;
.source "DisasterDashboardActivity.java"

# interfaces
.implements Lanx;


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V
    .locals 0

    .prologue
    .line 2449
    iput-object p1, p0, Laha;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 2452
    const-string v0, "historyListener update Success"

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 2453
    iget-object v0, p0, Laha;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laha;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laha;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->o(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2455
    iget-object v0, p0, Laha;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lalz;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2456
    iget-object v0, p0, Laha;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->k(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Landroid/widget/TabHost;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v0

    .line 2460
    :goto_0
    iget-object v1, p0, Laha;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v1, v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->c(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;I)V

    .line 2462
    :cond_0
    iget-object v0, p0, Laha;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->m(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    .line 2463
    iget-object v0, p0, Laha;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 2464
    iget-object v0, p0, Laha;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 2465
    return-void

    .line 2458
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 2469
    const-string v0, "historyListener update Failed, Original call: dbUpdateCompleted(false)"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 2470
    iget-object v0, p0, Laha;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    new-instance v1, Lahb;

    invoke-direct {v1, p0}, Lahb;-><init>(Laha;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2475
    iget-object v0, p0, Laha;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->m(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)V

    .line 2476
    iget-object v0, p0, Laha;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 2477
    iget-object v0, p0, Laha;->a:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->n(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 2478
    return-void
.end method
