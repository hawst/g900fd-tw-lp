.class public final Lagn;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final ab_warning:I = 0x7f020000

.field public static final ab_warning_focus:I = 0x7f020001

.field public static final ab_warning_press:I = 0x7f020002

.field public static final ab_watch:I = 0x7f020003

.field public static final ab_watch_focus:I = 0x7f020004

.field public static final ab_watch_press:I = 0x7f020005

.field public static final action_bar_icon:I = 0x7f020006

.field public static final action_bar_icon_menu_jpn:I = 0x7f020007

.field public static final actionbar_ic_share:I = 0x7f020008

.field public static final animation_blank:I = 0x7f020009

.field public static final contact_list_focused_holo_light:I = 0x7f02000a

.field public static final contact_list_pressed_holo_light:I = 0x7f02000b

.field public static final contact_selector:I = 0x7f02000c

.field public static final dashboard_cp:I = 0x7f02000d

.field public static final direction:I = 0x7f02000e

.field public static final drawer_shadow:I = 0x7f02000f

.field public static final garget_preview_image_safety:I = 0x7f020010

.field public static final geo_life_expander_close:I = 0x7f020011

.field public static final geo_life_expander_open:I = 0x7f020012

.field public static final geo_life_ic_refresh:I = 0x7f020013

.field public static final geo_life_round_bg:I = 0x7f020014

.field public static final geo_life_round_dim:I = 0x7f020015

.field public static final geo_life_round_normal:I = 0x7f020016

.field public static final geo_life_round_warning:I = 0x7f020017

.field public static final geo_life_round_watch:I = 0x7f020018

.field public static final home_menu_page_navi:I = 0x7f020019

.field public static final home_menu_page_navi_focus:I = 0x7f02001a

.field public static final ic_contact_picture_180_holo:I = 0x7f02001b

.field public static final ic_pin_add_location:I = 0x7f02001c

.field public static final ic_pin_disaster:I = 0x7f02001d

.field public static final ic_pin_location:I = 0x7f02001e

.field public static final ic_pin_traking:I = 0x7f02001f

.field public static final ic_pin_traking_02:I = 0x7f020020

.field public static final ico_handler:I = 0x7f020021

.field public static final ico_settings_more_open:I = 0x7f020022

.field public static final lifemode_widget_arrow_selector:I = 0x7f020023

.field public static final map_ic_warning_air_pollution:I = 0x7f020024

.field public static final map_ic_warning_avalanche:I = 0x7f020025

.field public static final map_ic_warning_drought:I = 0x7f020026

.field public static final map_ic_warning_earthquake:I = 0x7f020027

.field public static final map_ic_warning_excessive_heat:I = 0x7f020028

.field public static final map_ic_warning_extreme_cold:I = 0x7f020029

.field public static final map_ic_warning_extreme_wind:I = 0x7f02002a

.field public static final map_ic_warning_flood:I = 0x7f02002b

.field public static final map_ic_warning_heavy_rain:I = 0x7f02002c

.field public static final map_ic_warning_heavy_snow:I = 0x7f02002d

.field public static final map_ic_warning_high_seas:I = 0x7f02002e

.field public static final map_ic_warning_icing:I = 0x7f02002f

.field public static final map_ic_warning_severe_thunderstorm:I = 0x7f020030

.field public static final map_ic_warning_tsunami:I = 0x7f020031

.field public static final map_ic_warning_typhoon:I = 0x7f020032

.field public static final map_ic_warning_volcano:I = 0x7f020033

.field public static final map_ic_warning_wildfire:I = 0x7f020034

.field public static final map_ic_watch_air_pollution:I = 0x7f020035

.field public static final map_ic_watch_avalanche:I = 0x7f020036

.field public static final map_ic_watch_drought:I = 0x7f020037

.field public static final map_ic_watch_earthquake:I = 0x7f020038

.field public static final map_ic_watch_excessive_heat:I = 0x7f020039

.field public static final map_ic_watch_extreme_cold:I = 0x7f02003a

.field public static final map_ic_watch_extreme_wind:I = 0x7f02003b

.field public static final map_ic_watch_flood:I = 0x7f02003c

.field public static final map_ic_watch_heavy_rain:I = 0x7f02003d

.field public static final map_ic_watch_heavy_snow:I = 0x7f02003e

.field public static final map_ic_watch_high_seas:I = 0x7f02003f

.field public static final map_ic_watch_icing:I = 0x7f020040

.field public static final map_ic_watch_severe_thunderstorm:I = 0x7f020041

.field public static final map_ic_watch_tsunami:I = 0x7f020042

.field public static final map_ic_watch_typhoon:I = 0x7f020043

.field public static final map_ic_watch_volcano:I = 0x7f020044

.field public static final map_ic_watch_wildfire:I = 0x7f020045

.field public static final minus_button_selector:I = 0x7f020046

.field public static final multi_point_access_ic_location:I = 0x7f020047

.field public static final no_item_bg_light:I = 0x7f020048

.field public static final no_text_safety:I = 0x7f020049

.field public static final preview_widget_geo_life_transparent:I = 0x7f02004a

.field public static final preview_widget_safety:I = 0x7f02004b

.field public static final preview_widget_safety_transparent:I = 0x7f02004c

.field public static final ripple_bottom_btn:I = 0x7f02004d

.field public static final ripple_liferefresh_btn:I = 0x7f02004e

.field public static final ripple_list_textview:I = 0x7f02004f

.field public static final ripple_tab_watch:I = 0x7f020050

.field public static final safety:I = 0x7f020051

.field public static final safety_action_bar_warning_selector:I = 0x7f020052

.field public static final safety_action_bar_watch_selector:I = 0x7f020053

.field public static final safety_btn_emergency_call_icon_zero:I = 0x7f020054

.field public static final safety_btn_emergency_msg_icon:I = 0x7f020055

.field public static final safety_btn_emergency_msg_icon_att:I = 0x7f020056

.field public static final safety_btn_emergency_share_icon:I = 0x7f020057

.field public static final safety_btn_warning_arrow_normal:I = 0x7f020058

.field public static final safety_btn_watch_arrow_normal:I = 0x7f020059

.field public static final safety_cocktailbar_appicon_selector:I = 0x7f02005a

.field public static final safety_cocktailbar_refresh_btn_selector:I = 0x7f02005b

.field public static final safety_dashboard_line:I = 0x7f02005c

.field public static final safety_dashboard_list_selector:I = 0x7f02005d

.field public static final safety_dashboard_rogo_the_weather_channel:I = 0x7f02005e

.field public static final safety_dashboard_state_bg:I = 0x7f02005f

.field public static final safety_disaster_spot_dummy:I = 0x7f020060

.field public static final safety_disaster_spot_warning0001:I = 0x7f020061

.field public static final safety_disaster_spot_warning0002:I = 0x7f020062

.field public static final safety_disaster_spot_warning0003:I = 0x7f020063

.field public static final safety_disaster_spot_warning0004:I = 0x7f020064

.field public static final safety_disaster_spot_warning0005:I = 0x7f020065

.field public static final safety_disaster_spot_warning0006:I = 0x7f020066

.field public static final safety_disaster_spot_warning0007:I = 0x7f020067

.field public static final safety_disaster_spot_warning0008:I = 0x7f020068

.field public static final safety_disaster_spot_warning0009:I = 0x7f020069

.field public static final safety_disaster_spot_warning0010:I = 0x7f02006a

.field public static final safety_disaster_spot_warning0011:I = 0x7f02006b

.field public static final safety_disaster_spot_warning0012:I = 0x7f02006c

.field public static final safety_disaster_spot_warning0013:I = 0x7f02006d

.field public static final safety_disaster_spot_warning0014:I = 0x7f02006e

.field public static final safety_disaster_spot_warning0015:I = 0x7f02006f

.field public static final safety_disaster_spot_warning0016:I = 0x7f020070

.field public static final safety_disaster_spot_warning0017:I = 0x7f020071

.field public static final safety_disaster_spot_warning0018:I = 0x7f020072

.field public static final safety_disaster_spot_warning0019:I = 0x7f020073

.field public static final safety_disaster_spot_warning0020:I = 0x7f020074

.field public static final safety_disaster_spot_warning0021:I = 0x7f020075

.field public static final safety_disaster_spot_warning0022:I = 0x7f020076

.field public static final safety_disaster_spot_warning0023:I = 0x7f020077

.field public static final safety_disaster_spot_warning0024:I = 0x7f020078

.field public static final safety_disaster_spot_warning0025:I = 0x7f020079

.field public static final safety_disaster_spot_warning0026:I = 0x7f02007a

.field public static final safety_disaster_spot_warning0027:I = 0x7f02007b

.field public static final safety_disaster_spot_warning0028:I = 0x7f02007c

.field public static final safety_disaster_spot_warning0029:I = 0x7f02007d

.field public static final safety_disaster_spot_warning0030:I = 0x7f02007e

.field public static final safety_disaster_spot_watching0001:I = 0x7f02007f

.field public static final safety_disaster_spot_watching0002:I = 0x7f020080

.field public static final safety_disaster_spot_watching0003:I = 0x7f020081

.field public static final safety_disaster_spot_watching0004:I = 0x7f020082

.field public static final safety_disaster_spot_watching0005:I = 0x7f020083

.field public static final safety_disaster_spot_watching0006:I = 0x7f020084

.field public static final safety_disaster_spot_watching0007:I = 0x7f020085

.field public static final safety_disaster_spot_watching0008:I = 0x7f020086

.field public static final safety_disaster_spot_watching0009:I = 0x7f020087

.field public static final safety_disaster_spot_watching0010:I = 0x7f020088

.field public static final safety_disaster_spot_watching0011:I = 0x7f020089

.field public static final safety_disaster_spot_watching0012:I = 0x7f02008a

.field public static final safety_disaster_spot_watching0013:I = 0x7f02008b

.field public static final safety_disaster_spot_watching0014:I = 0x7f02008c

.field public static final safety_disaster_spot_watching0015:I = 0x7f02008d

.field public static final safety_disaster_spot_watching0016:I = 0x7f02008e

.field public static final safety_disaster_spot_watching0017:I = 0x7f02008f

.field public static final safety_disaster_spot_watching0018:I = 0x7f020090

.field public static final safety_disaster_spot_watching0019:I = 0x7f020091

.field public static final safety_disaster_spot_watching0020:I = 0x7f020092

.field public static final safety_disaster_spot_watching0021:I = 0x7f020093

.field public static final safety_disaster_spot_watching0022:I = 0x7f020094

.field public static final safety_disaster_spot_watching0023:I = 0x7f020095

.field public static final safety_disaster_spot_watching0024:I = 0x7f020096

.field public static final safety_disaster_spot_watching0025:I = 0x7f020097

.field public static final safety_disaster_spot_watching0026:I = 0x7f020098

.field public static final safety_disaster_spot_watching0027:I = 0x7f020099

.field public static final safety_disaster_spot_watching0028:I = 0x7f02009a

.field public static final safety_disaster_spot_watching0029:I = 0x7f02009b

.field public static final safety_disaster_spot_watching0030:I = 0x7f02009c

.field public static final safety_disclaimer_button_selector:I = 0x7f02009d

.field public static final safety_emergency_call_selector:I = 0x7f02009e

.field public static final safety_emergency_msg_selector:I = 0x7f02009f

.field public static final safety_fade_out_bg:I = 0x7f0200a0

.field public static final safety_ic_aflood:I = 0x7f0200a1

.field public static final safety_ic_air_pollution:I = 0x7f0200a2

.field public static final safety_ic_avalanche:I = 0x7f0200a3

.field public static final safety_ic_bg:I = 0x7f0200a4

.field public static final safety_ic_dashboard_history:I = 0x7f0200a5

.field public static final safety_ic_dashboard_tips:I = 0x7f0200a6

.field public static final safety_ic_default:I = 0x7f0200a7

.field public static final safety_ic_default_bg:I = 0x7f0200a8

.field public static final safety_ic_drought:I = 0x7f0200a9

.field public static final safety_ic_earthquake:I = 0x7f0200aa

.field public static final safety_ic_excessive_heat:I = 0x7f0200ab

.field public static final safety_ic_extreme_cold:I = 0x7f0200ac

.field public static final safety_ic_extreme_wind:I = 0x7f0200ad

.field public static final safety_ic_family:I = 0x7f0200ae

.field public static final safety_ic_fog:I = 0x7f0200af

.field public static final safety_ic_heavy_rain:I = 0x7f0200b0

.field public static final safety_ic_heavy_snow:I = 0x7f0200b1

.field public static final safety_ic_high_seas:I = 0x7f0200b2

.field public static final safety_ic_icing:I = 0x7f0200b3

.field public static final safety_ic_manual:I = 0x7f0200b4

.field public static final safety_ic_map:I = 0x7f0200b5

.field public static final safety_ic_no_disaster:I = 0x7f0200b6

.field public static final safety_ic_no_disaster_bg:I = 0x7f0200b7

.field public static final safety_ic_other:I = 0x7f0200b8

.field public static final safety_ic_severe_thunderstorm:I = 0x7f0200b9

.field public static final safety_ic_tsunami:I = 0x7f0200ba

.field public static final safety_ic_typhoon:I = 0x7f0200bb

.field public static final safety_ic_update:I = 0x7f0200bc

.field public static final safety_ic_volcano:I = 0x7f0200bd

.field public static final safety_ic_warning_1:I = 0x7f0200be

.field public static final safety_ic_warning_2:I = 0x7f0200bf

.field public static final safety_ic_warning_bg:I = 0x7f0200c0

.field public static final safety_ic_watching:I = 0x7f0200c1

.field public static final safety_ic_watching_bg:I = 0x7f0200c2

.field public static final safety_ic_wildfire:I = 0x7f0200c3

.field public static final safety_icon_cold:I = 0x7f0200c4

.field public static final safety_icon_comfort:I = 0x7f0200c5

.field public static final safety_icon_dim_bg:I = 0x7f0200c6

.field public static final safety_icon_excessive_heat:I = 0x7f0200c7

.field public static final safety_icon_fine_dust:I = 0x7f0200c8

.field public static final safety_icon_food_poisoning:I = 0x7f0200c9

.field public static final safety_icon_freezing_burst:I = 0x7f0200ca

.field public static final safety_icon_frostbite:I = 0x7f0200cb

.field public static final safety_icon_pm10:I = 0x7f0200cc

.field public static final safety_icon_pollen:I = 0x7f0200cd

.field public static final safety_icon_sensory_temperature:I = 0x7f0200ce

.field public static final safety_icon_skin:I = 0x7f0200cf

.field public static final safety_icon_spoil:I = 0x7f0200d0

.field public static final safety_icon_sweat:I = 0x7f0200d1

.field public static final safety_icon_umbrella:I = 0x7f0200d2

.field public static final safety_icon_uv:I = 0x7f0200d3

.field public static final safety_icon_warning_bg:I = 0x7f0200d4

.field public static final safety_icon_watch_bg:I = 0x7f0200d5

.field public static final safety_icon_yellow_dust:I = 0x7f0200d6

.field public static final safety_life_dim:I = 0x7f0200d7

.field public static final safety_life_ic_dim_excessive_heat:I = 0x7f0200d8

.field public static final safety_life_ic_dim_fine_dust:I = 0x7f0200d9

.field public static final safety_life_ic_dim_pollen:I = 0x7f0200da

.field public static final safety_life_ic_dim_skin:I = 0x7f0200db

.field public static final safety_life_ic_dim_sweat:I = 0x7f0200dc

.field public static final safety_life_ic_dim_umbrella:I = 0x7f0200dd

.field public static final safety_life_ic_dim_uv:I = 0x7f0200de

.field public static final safety_life_ic_dim_yellow_dust:I = 0x7f0200df

.field public static final safety_life_ic_excessive_heat:I = 0x7f0200e0

.field public static final safety_life_ic_fine_dust:I = 0x7f0200e1

.field public static final safety_life_ic_normal_excessive_chilblain:I = 0x7f0200e2

.field public static final safety_life_ic_normal_excessive_chilblain_dim:I = 0x7f0200e3

.field public static final safety_life_ic_normal_excessive_flu:I = 0x7f0200e4

.field public static final safety_life_ic_normal_excessive_flu_dim:I = 0x7f0200e5

.field public static final safety_life_ic_normal_excessive_freeze:I = 0x7f0200e6

.field public static final safety_life_ic_normal_excessive_freeze_dim:I = 0x7f0200e7

.field public static final safety_life_ic_normal_excessive_heat:I = 0x7f0200e8

.field public static final safety_life_ic_normal_excessive_heat_dim:I = 0x7f0200e9

.field public static final safety_life_ic_normal_excessive_poisoning:I = 0x7f0200ea

.field public static final safety_life_ic_normal_excessive_poisoning_dim:I = 0x7f0200eb

.field public static final safety_life_ic_normal_excessive_pollution:I = 0x7f0200ec

.field public static final safety_life_ic_normal_excessive_pollution_dim:I = 0x7f0200ed

.field public static final safety_life_ic_normal_excessive_spoil:I = 0x7f0200ee

.field public static final safety_life_ic_normal_excessive_spoil_dim:I = 0x7f0200ef

.field public static final safety_life_ic_normal_excessive_wind_chill:I = 0x7f0200f0

.field public static final safety_life_ic_normal_excessive_wind_chill_dim:I = 0x7f0200f1

.field public static final safety_life_ic_normal_fine_dust:I = 0x7f0200f2

.field public static final safety_life_ic_normal_fine_dust_dim:I = 0x7f0200f3

.field public static final safety_life_ic_normal_pollen:I = 0x7f0200f4

.field public static final safety_life_ic_normal_pollen_dim:I = 0x7f0200f5

.field public static final safety_life_ic_normal_sweat:I = 0x7f0200f6

.field public static final safety_life_ic_normal_sweat_dim:I = 0x7f0200f7

.field public static final safety_life_ic_normal_uv:I = 0x7f0200f8

.field public static final safety_life_ic_normal_uv_dim:I = 0x7f0200f9

.field public static final safety_life_ic_pollen:I = 0x7f0200fa

.field public static final safety_life_ic_skin:I = 0x7f0200fb

.field public static final safety_life_ic_sweat:I = 0x7f0200fc

.field public static final safety_life_ic_umbrella:I = 0x7f0200fd

.field public static final safety_life_ic_uv:I = 0x7f0200fe

.field public static final safety_life_ic_yellow_dust:I = 0x7f0200ff

.field public static final safety_life_jp_map:I = 0x7f020100

.field public static final safety_life_ko_map:I = 0x7f020101

.field public static final safety_life_location:I = 0x7f020102

.field public static final safety_life_normal:I = 0x7f020103

.field public static final safety_life_reported_bg:I = 0x7f020104

.field public static final safety_life_safe:I = 0x7f020105

.field public static final safety_life_status_line_ic_refresh_img:I = 0x7f020106

.field public static final safety_life_sub_bg:I = 0x7f020107

.field public static final safety_life_sub_line:I = 0x7f020108

.field public static final safety_life_update:I = 0x7f020109

.field public static final safety_life_update_press:I = 0x7f02010a

.field public static final safety_life_warning:I = 0x7f02010b

.field public static final safety_map_current_location_btn:I = 0x7f02010c

.field public static final safety_map_current_location_btn_press:I = 0x7f02010d

.field public static final safety_map_current_location_btn_selector:I = 0x7f02010e

.field public static final safety_map_current_location_focus:I = 0x7f02010f

.field public static final safety_map_ic_location_01:I = 0x7f020110

.field public static final safety_map_ic_location_02:I = 0x7f020111

.field public static final safety_map_ic_location_03:I = 0x7f020112

.field public static final safety_map_ic_location_04:I = 0x7f020113

.field public static final safety_map_ic_location_05:I = 0x7f020114

.field public static final safety_map_ic_normal_excessive_chilblain:I = 0x7f020115

.field public static final safety_map_ic_normal_excessive_flu:I = 0x7f020116

.field public static final safety_map_ic_normal_excessive_freeze:I = 0x7f020117

.field public static final safety_map_ic_normal_excessive_heat:I = 0x7f020118

.field public static final safety_map_ic_normal_excessive_heat_jpn:I = 0x7f020119

.field public static final safety_map_ic_normal_excessive_poisoning:I = 0x7f02011a

.field public static final safety_map_ic_normal_excessive_pollution:I = 0x7f02011b

.field public static final safety_map_ic_normal_excessive_spoil:I = 0x7f02011c

.field public static final safety_map_ic_normal_excessive_wind_chill:I = 0x7f02011d

.field public static final safety_map_ic_normal_fine_dust:I = 0x7f02011e

.field public static final safety_map_ic_normal_fine_dust_jpn:I = 0x7f02011f

.field public static final safety_map_ic_normal_pollen:I = 0x7f020120

.field public static final safety_map_ic_normal_pollen_jpn:I = 0x7f020121

.field public static final safety_map_ic_normal_skin_jpn:I = 0x7f020122

.field public static final safety_map_ic_normal_sweat:I = 0x7f020123

.field public static final safety_map_ic_normal_sweat_jpn:I = 0x7f020124

.field public static final safety_map_ic_normal_umbrella_jpn:I = 0x7f020125

.field public static final safety_map_ic_normal_uv:I = 0x7f020126

.field public static final safety_map_ic_normal_uv_jpn:I = 0x7f020127

.field public static final safety_map_ic_normal_yellow_dust_jpn:I = 0x7f020128

.field public static final safety_map_ic_safe_air_pollution:I = 0x7f020129

.field public static final safety_map_ic_safe_chilblain:I = 0x7f02012a

.field public static final safety_map_ic_safe_excessive_heat:I = 0x7f02012b

.field public static final safety_map_ic_safe_fine_dust:I = 0x7f02012c

.field public static final safety_map_ic_safe_flu:I = 0x7f02012d

.field public static final safety_map_ic_safe_food_poisoning:I = 0x7f02012e

.field public static final safety_map_ic_safe_freeze:I = 0x7f02012f

.field public static final safety_map_ic_safe_pollen:I = 0x7f020130

.field public static final safety_map_ic_safe_spoil:I = 0x7f020131

.field public static final safety_map_ic_safe_sweat:I = 0x7f020132

.field public static final safety_map_ic_safe_uv:I = 0x7f020133

.field public static final safety_map_ic_safe_wind_chill:I = 0x7f020134

.field public static final safety_map_ic_temperature_bg_lv4:I = 0x7f020135

.field public static final safety_map_ic_temperature_bg_lv5:I = 0x7f020136

.field public static final safety_map_ic_warning_excessive_chilblain:I = 0x7f020137

.field public static final safety_map_ic_warning_excessive_flu:I = 0x7f020138

.field public static final safety_map_ic_warning_excessive_freeze:I = 0x7f020139

.field public static final safety_map_ic_warning_excessive_heat:I = 0x7f02013a

.field public static final safety_map_ic_warning_excessive_heat_jpn:I = 0x7f02013b

.field public static final safety_map_ic_warning_excessive_poisoning:I = 0x7f02013c

.field public static final safety_map_ic_warning_excessive_pollution:I = 0x7f02013d

.field public static final safety_map_ic_warning_excessive_spoil:I = 0x7f02013e

.field public static final safety_map_ic_warning_excessive_wind_chill:I = 0x7f02013f

.field public static final safety_map_ic_warning_fine_dust:I = 0x7f020140

.field public static final safety_map_ic_warning_fine_dust_jpn:I = 0x7f020141

.field public static final safety_map_ic_warning_pollen:I = 0x7f020142

.field public static final safety_map_ic_warning_pollen_jpn:I = 0x7f020143

.field public static final safety_map_ic_warning_skin_jpn:I = 0x7f020144

.field public static final safety_map_ic_warning_sweat:I = 0x7f020145

.field public static final safety_map_ic_warning_sweat_jpn:I = 0x7f020146

.field public static final safety_map_ic_warning_umbrella_jpn:I = 0x7f020147

.field public static final safety_map_ic_warning_uv:I = 0x7f020148

.field public static final safety_map_ic_warning_uv_jpn:I = 0x7f020149

.field public static final safety_map_ic_warning_yellow_dust_jpn:I = 0x7f02014a

.field public static final safety_map_ic_watch_excessive_heat_jpn:I = 0x7f02014b

.field public static final safety_map_ic_watch_fine_dust_jpn:I = 0x7f02014c

.field public static final safety_map_ic_watch_pollen_jpn:I = 0x7f02014d

.field public static final safety_map_ic_watch_skin_jpn:I = 0x7f02014e

.field public static final safety_map_ic_watch_sweat_jpn:I = 0x7f02014f

.field public static final safety_map_ic_watch_umbrella_jpn:I = 0x7f020150

.field public static final safety_map_ic_watch_uv_jpn:I = 0x7f020151

.field public static final safety_map_ic_watch_yellow_dust_jpn:I = 0x7f020152

.field public static final safety_rotate:I = 0x7f020153

.field public static final safety_rotate_press:I = 0x7f020154

.field public static final setting_safety_geoinfo_preview:I = 0x7f020155

.field public static final setting_safety_geolookout_preview:I = 0x7f020156

.field public static final stit_safety_no:I = 0x7f020157

.field public static final stit_safety_no_press:I = 0x7f020158

.field public static final tab_background_selector:I = 0x7f020159

.field public static final transparent_point:I = 0x7f02015a

.field public static final tw_ab_bottom_transparent_holo_light:I = 0x7f02015b

.field public static final tw_ab_transparent_light:I = 0x7f02015c

.field public static final tw_action_bar_sub_tab_bg_holo_light:I = 0x7f02015d

.field public static final tw_action_bar_tab_selected_bg_holo_light:I = 0x7f02015e

.field public static final tw_action_item_background_focused_holo_light:I = 0x7f02015f

.field public static final tw_action_item_background_pressed_holo_light:I = 0x7f020160

.field public static final tw_action_item_background_selected_holo_light:I = 0x7f020161

.field public static final tw_actionbar_btn_add_normal:I = 0x7f020162

.field public static final tw_background_light:I = 0x7f020163

.field public static final tw_btn_arrow_focused_holo:I = 0x7f020164

.field public static final tw_btn_arrow_holo:I = 0x7f020165

.field public static final tw_btn_arrow_pressed_holo:I = 0x7f020166

.field public static final tw_btn_arrow_selector:I = 0x7f020167

.field public static final tw_btn_next_depth_holo_dark:I = 0x7f020168

.field public static final tw_btn_next_depth_holo_dark_selector:I = 0x7f020169

.field public static final tw_btn_next_depth_pressed_holo_dark:I = 0x7f02016a

.field public static final tw_divider_ab_holo_light:I = 0x7f02016b

.field public static final tw_drawer_bg_holo_light:I = 0x7f02016c

.field public static final tw_drawer_list_line_holo_light:I = 0x7f02016d

.field public static final tw_drawer_list_shadow_holo_light:I = 0x7f02016e

.field public static final tw_expander_close_holo_dark:I = 0x7f02016f

.field public static final tw_expander_open_holo_dark:I = 0x7f020170

.field public static final tw_ic_ab_drawer_holo_dark:I = 0x7f020171

.field public static final tw_list_divider_holo_dark:I = 0x7f020172

.field public static final tw_list_divider_holo_light:I = 0x7f020173

.field public static final tw_list_focused_holo_light:I = 0x7f020174

.field public static final tw_list_icon_create_holo_light:I = 0x7f020175

.field public static final tw_list_icon_minus_focused_holo_light:I = 0x7f020176

.field public static final tw_list_icon_minus_holo_light:I = 0x7f020177

.field public static final tw_list_icon_minus_pressed_holo_light:I = 0x7f020178

.field public static final tw_list_pressed_holo_light:I = 0x7f020179

.field public static final tw_list_selected_holo_light:I = 0x7f02017a

.field public static final tw_searchfiled_background_holo_light:I = 0x7f02017b

.field public static final tw_tab_divider_holo_dark:I = 0x7f02017c

.field public static final tw_tab_selected_focused_holo_light:I = 0x7f02017d

.field public static final tw_tab_selected_focused_pressed_holo_light:I = 0x7f02017e

.field public static final tw_tab_selected_pressed_holo_light:I = 0x7f02017f

.field public static final tw_textfield_default_holo_light:I = 0x7f020180

.field public static final warning_btn_01:I = 0x7f020181

.field public static final warning_btn_01_press:I = 0x7f020182

.field public static final warning_btn_02:I = 0x7f020183

.field public static final warning_btn_02_press:I = 0x7f020184

.field public static final warning_btn_divider:I = 0x7f020185

.field public static final widget_geo_index_preview:I = 0x7f020186

.field public static final widget_img_active_wildfire:I = 0x7f020187

.field public static final widget_img_air_pollution:I = 0x7f020188

.field public static final widget_img_ashfall:I = 0x7f020189

.field public static final widget_img_avalanche:I = 0x7f02018a

.field public static final widget_img_dense_fog:I = 0x7f02018b

.field public static final widget_img_duststorm:I = 0x7f02018c

.field public static final widget_img_earthquake:I = 0x7f02018d

.field public static final widget_img_extreme_cold:I = 0x7f02018e

.field public static final widget_img_extreme_heat:I = 0x7f02018f

.field public static final widget_img_extreme_wind:I = 0x7f020190

.field public static final widget_img_flood:I = 0x7f020191

.field public static final widget_img_heavy_rain:I = 0x7f020192

.field public static final widget_img_nodisaster:I = 0x7f020193

.field public static final widget_img_snownice:I = 0x7f020194

.field public static final widget_img_thunderstorm:I = 0x7f020195

.field public static final widget_img_tornado:I = 0x7f020196

.field public static final widget_img_tropical_alert:I = 0x7f020197

.field public static final widget_img_tsunami:I = 0x7f020198

.field public static final widget_img_volcano:I = 0x7f020199

.field public static final widget_img_welcome:I = 0x7f02019a

.field public static final widget_img_welcome_lifemode:I = 0x7f02019b

.field public static final widget_img_winterstorm:I = 0x7f02019c

.field public static final widget_monitor_alert_normalcy_bg:I = 0x7f02019d

.field public static final widget_monitor_alert_warning_bg:I = 0x7f02019e

.field public static final widget_monitor_arrow_down:I = 0x7f02019f

.field public static final widget_monitor_arrow_down_press:I = 0x7f0201a0

.field public static final widget_monitor_dim:I = 0x7f0201a1

.field public static final widget_monitor_ic_chilblain:I = 0x7f0201a2

.field public static final widget_monitor_ic_excessive_heat_jpn:I = 0x7f0201a3

.field public static final widget_monitor_ic_fine_dust_jpn:I = 0x7f0201a4

.field public static final widget_monitor_ic_flu:I = 0x7f0201a5

.field public static final widget_monitor_ic_food_poisoning:I = 0x7f0201a6

.field public static final widget_monitor_ic_freeze:I = 0x7f0201a7

.field public static final widget_monitor_ic_heat:I = 0x7f0201a8

.field public static final widget_monitor_ic_pm10:I = 0x7f0201a9

.field public static final widget_monitor_ic_pollen:I = 0x7f0201aa

.field public static final widget_monitor_ic_pollen_jpn:I = 0x7f0201ab

.field public static final widget_monitor_ic_refresh_light:I = 0x7f0201ac

.field public static final widget_monitor_ic_skin_jpn:I = 0x7f0201ad

.field public static final widget_monitor_ic_spoil:I = 0x7f0201ae

.field public static final widget_monitor_ic_sweat:I = 0x7f0201af

.field public static final widget_monitor_ic_sweat_jpn:I = 0x7f0201b0

.field public static final widget_monitor_ic_umbrella_jpn:I = 0x7f0201b1

.field public static final widget_monitor_ic_uv:I = 0x7f0201b2

.field public static final widget_monitor_ic_uv_jpn:I = 0x7f0201b3

.field public static final widget_monitor_ic_wind_chill:I = 0x7f0201b4

.field public static final widget_monitor_ic_yellow_dust_jpn:I = 0x7f0201b5

.field public static final widget_monitor_low:I = 0x7f0201b6

.field public static final widget_monitor_normal:I = 0x7f0201b7

.field public static final widget_monitor_warning:I = 0x7f0201b8

.field public static final widget_safety_bg:I = 0x7f0201b9

.field public static final widget_safety_monitor_bg:I = 0x7f0201ba

.field public static final widget_shadow:I = 0x7f0201bb


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 557
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
