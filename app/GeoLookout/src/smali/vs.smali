.class public final Lvs;
.super Ljava/lang/Object;


# static fields
.field public static final a:I = 0x0

.field public static final b:I = 0x1

.field public static final c:I = 0x2

.field public static final d:I = 0x3

.field public static final e:I = 0x4


# instance fields
.field private final f:Lxk;

.field private g:Lxf;


# direct methods
.method public constructor <init>(Lxk;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lnx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxk;

    iput-object v0, p0, Lvs;->f:Lxk;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/maps/model/CircleOptions;)Laaa;
    .locals 2

    :try_start_0
    new-instance v0, Laaa;

    iget-object v1, p0, Lvs;->f:Lxk;

    invoke-interface {v1, p1}, Lxk;->a(Lcom/google/android/gms/maps/model/CircleOptions;)Labl;

    move-result-object v1

    invoke-direct {v0, v1}, Laaa;-><init>(Labl;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Laac;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0, p1}, Lxk;->a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Labo;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Laac;

    invoke-direct {v0, v1}, Laac;-><init>(Labo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/maps/model/MarkerOptions;)Laah;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0, p1}, Lxk;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Labr;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Laah;

    invoke-direct {v0, v1}, Laah;-><init>(Labr;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/maps/model/PolygonOptions;)Laaj;
    .locals 2

    :try_start_0
    new-instance v0, Laaj;

    iget-object v1, p0, Lvs;->f:Lxk;

    invoke-interface {v1, p1}, Lxk;->a(Lcom/google/android/gms/maps/model/PolygonOptions;)Labu;

    move-result-object v1

    invoke-direct {v0, v1}, Laaj;-><init>(Labu;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/maps/model/PolylineOptions;)Laal;
    .locals 2

    :try_start_0
    new-instance v0, Laal;

    iget-object v1, p0, Lvs;->f:Lxk;

    invoke-interface {v1, p1}, Lxk;->a(Lcom/google/android/gms/maps/model/PolylineOptions;)Labf;

    move-result-object v1

    invoke-direct {v0, v1}, Laal;-><init>(Labf;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/maps/model/TileOverlayOptions;)Laap;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0, p1}, Lxk;->a(Lcom/google/android/gms/maps/model/TileOverlayOptions;)Labx;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Laap;

    invoke-direct {v0, v1}, Laap;-><init>(Labx;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public a()Lxk;
    .locals 1

    iget-object v0, p0, Lvs;->f:Lxk;

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0, p1}, Lxk;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(IIII)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0, p1, p2, p3, p4}, Lxk;->a(IIII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lvq;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-virtual {p1}, Lvq;->a()Ldp;

    move-result-object v1

    invoke-interface {v0, v1}, Lxk;->a(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lvq;ILwg;)V
    .locals 3

    :try_start_0
    iget-object v1, p0, Lvs;->f:Lxk;

    invoke-virtual {p1}, Lvq;->a()Ldp;

    move-result-object v2

    if-nez p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v2, p2, v0}, Lxk;->a(Ldp;ILyd;)V

    return-void

    :cond_0
    new-instance v0, Lws;

    invoke-direct {v0, p3}, Lws;-><init>(Lwg;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lvq;Lwg;)V
    .locals 3

    :try_start_0
    iget-object v1, p0, Lvs;->f:Lxk;

    invoke-virtual {p1}, Lvq;->a()Ldp;

    move-result-object v2

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v2, v0}, Lxk;->a(Ldp;Lyd;)V

    return-void

    :cond_0
    new-instance v0, Lws;

    invoke-direct {v0, p2}, Lws;-><init>(Lwg;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lwh;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lxk;->a(Lyj;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lvs;->f:Lxk;

    new-instance v1, Lvw;

    invoke-direct {v1, p0, p1}, Lvw;-><init>(Lvs;Lwh;)V

    invoke-interface {v0, v1}, Lxk;->a(Lyj;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lwi;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lxk;->a(Lym;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lvs;->f:Lxk;

    new-instance v1, Lwb;

    invoke-direct {v1, p0, p1}, Lwb;-><init>(Lvs;Lwi;)V

    invoke-interface {v0, v1}, Lxk;->a(Lym;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lwj;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lxk;->a(Lyp;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lvs;->f:Lxk;

    new-instance v1, Lvv;

    invoke-direct {v1, p0, p1}, Lvv;-><init>(Lvs;Lwj;)V

    invoke-interface {v0, v1}, Lxk;->a(Lyp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lwk;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lxk;->a(Lyv;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lvs;->f:Lxk;

    new-instance v1, Lwc;

    invoke-direct {v1, p0, p1}, Lwc;-><init>(Lvs;Lwk;)V

    invoke-interface {v0, v1}, Lxk;->a(Lyv;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public a(Lwl;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lxk;->a(Lyy;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lvs;->f:Lxk;

    new-instance v1, Lvz;

    invoke-direct {v1, p0, p1}, Lvz;-><init>(Lvs;Lwl;)V

    invoke-interface {v0, v1}, Lxk;->a(Lyy;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lwm;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lxk;->a(Lzb;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lvs;->f:Lxk;

    new-instance v1, Lwd;

    invoke-direct {v1, p0, p1}, Lwd;-><init>(Lvs;Lwm;)V

    invoke-interface {v0, v1}, Lxk;->a(Lzb;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lwn;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lxk;->a(Lze;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lvs;->f:Lxk;

    new-instance v1, Lwe;

    invoke-direct {v1, p0, p1}, Lwe;-><init>(Lvs;Lwn;)V

    invoke-interface {v0, v1}, Lxk;->a(Lze;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lwo;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lxk;->a(Lzh;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lvs;->f:Lxk;

    new-instance v1, Lwf;

    invoke-direct {v1, p0, p1}, Lwf;-><init>(Lvs;Lwo;)V

    invoke-interface {v0, v1}, Lxk;->a(Lzh;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lwp;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lxk;->a(Lzk;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lvs;->f:Lxk;

    new-instance v1, Lvy;

    invoke-direct {v1, p0, p1}, Lvy;-><init>(Lvs;Lwp;)V

    invoke-interface {v0, v1}, Lxk;->a(Lzk;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lwq;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lxk;->a(Lzn;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lvs;->f:Lxk;

    new-instance v1, Lvx;

    invoke-direct {v1, p0, p1}, Lvx;-><init>(Lvs;Lwq;)V

    invoke-interface {v0, v1}, Lxk;->a(Lzn;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lwr;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lvs;->a(Lwr;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public final a(Lwr;Landroid/graphics/Bitmap;)V
    .locals 3

    if-eqz p2, :cond_0

    invoke-static {p2}, Lds;->a(Ljava/lang/Object;)Ldp;

    move-result-object v0

    :goto_0
    check-cast v0, Lds;

    :try_start_0
    iget-object v1, p0, Lvs;->f:Lxk;

    new-instance v2, Lwa;

    invoke-direct {v2, p0, p1}, Lwa;-><init>(Lvs;Lwr;)V

    invoke-interface {v1, v2, v0}, Lxk;->a(Lzq;Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lwu;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lxk;->a(Lxn;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lvs;->f:Lxk;

    new-instance v1, Lvt;

    invoke-direct {v1, p0, p1}, Lvt;-><init>(Lvs;Lwu;)V

    invoke-interface {v0, v1}, Lxk;->a(Lxn;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Z)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0, p1}, Lxk;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final b()Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0}, Lxk;->a()Lcom/google/android/gms/maps/model/CameraPosition;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final b(Lvq;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-virtual {p1}, Lvq;->a()Ldp;

    move-result-object v1

    invoke-interface {v0, v1}, Lxk;->b(Ldp;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final b(Z)Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0, p1}, Lxk;->b(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final c()F
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0}, Lxk;->b()F
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final c(Z)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0, p1}, Lxk;->d(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final d()F
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0}, Lxk;->c()F
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final d(Z)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0, p1}, Lxk;->c(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final e()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0}, Lxk;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final f()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0}, Lxk;->e()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final g()I
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0}, Lxk;->f()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final h()Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0}, Lxk;->g()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final i()Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0}, Lxk;->h()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final j()Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0}, Lxk;->n()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final k()Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0}, Lxk;->i()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final l()Landroid/location/Location;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    :try_start_0
    iget-object v0, p0, Lvs;->f:Lxk;

    invoke-interface {v0}, Lxk;->j()Landroid/location/Location;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final m()Lxf;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lvs;->g:Lxf;

    if-nez v0, :cond_0

    new-instance v0, Lxf;

    iget-object v1, p0, Lvs;->f:Lxk;

    invoke-interface {v1}, Lxk;->k()Lxz;

    move-result-object v1

    invoke-direct {v0, v1}, Lxf;-><init>(Lxz;)V

    iput-object v0, p0, Lvs;->g:Lxf;

    :cond_0
    iget-object v0, p0, Lvs;->g:Lxf;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final n()Lxb;
    .locals 2

    :try_start_0
    new-instance v0, Lxb;

    iget-object v1, p0, Lvs;->f:Lxk;

    invoke-interface {v1}, Lxk;->l()Lxw;

    move-result-object v1

    invoke-direct {v0, v1}, Lxb;-><init>(Lxw;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Laan;

    invoke-direct {v1, v0}, Laan;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
