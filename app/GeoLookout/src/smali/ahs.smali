.class public Lahs;
.super Ljava/lang/Object;
.source "DisasterDashboardActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/db/DisasterInfo;

.field final synthetic b:I

.field final synthetic c:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Lcom/sec/android/GeoLookout/db/DisasterInfo;I)V
    .locals 0

    .prologue
    .line 1100
    iput-object p1, p0, Lahs;->c:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iput-object p2, p0, Lahs;->a:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    iput p3, p0, Lahs;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 1103
    const-string v0, "onClick() - cpLink"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 1104
    iget-object v0, p0, Lahs;->c:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lahs;->c:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->b(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_1

    .line 1105
    :cond_0
    const-string v0, "mMapCPLink.setOnClickListener : CP link is null "

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 1123
    :goto_0
    return-void

    .line 1109
    :cond_1
    const-string v0, "01"

    iget-object v1, p0, Lahs;->a:Lcom/sec/android/GeoLookout/db/DisasterInfo;

    invoke-virtual {v1}, Lcom/sec/android/GeoLookout/db/DisasterInfo;->getDV()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lahs;->c:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->c(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)I

    move-result v0

    const/16 v1, 0x15

    if-eq v0, v1, :cond_3

    .line 1112
    iget-object v0, p0, Lahs;->c:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->d(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;)Ljava/lang/String;

    move-result-object v0

    .line 1114
    if-eqz v0, :cond_2

    .line 1115
    iget-object v1, p0, Lahs;->c:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    const/16 v2, 0x32

    invoke-static {v1, v0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;Ljava/lang/String;I)V

    goto :goto_0

    .line 1117
    :cond_2
    iget-object v0, p0, Lahs;->c:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iget v1, p0, Lahs;->b:I

    invoke-static {v0, v2, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;II)V

    goto :goto_0

    .line 1121
    :cond_3
    iget-object v0, p0, Lahs;->c:Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    iget v1, p0, Lahs;->b:I

    invoke-static {v0, v2, v1}, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;->a(Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;II)V

    goto :goto_0
.end method
