.class public Lty;
.super Lmz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmz",
        "<",
        "Lts;",
        ">;"
    }
.end annotation


# instance fields
.field private final f:Landroid/app/Activity;

.field private final g:I

.field private final h:Ljava/lang/String;

.field private final i:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcb;Lcc;ILjava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lmz;-><init>(Landroid/content/Context;Lcb;Lcc;[Ljava/lang/String;)V

    iput-object p1, p0, Lty;->f:Landroid/app/Activity;

    iput p4, p0, Lty;->g:I

    iput-object p5, p0, Lty;->h:Ljava/lang/String;

    iput p6, p0, Lty;->i:I

    return-void
.end method

.method static synthetic a(Lty;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lty;->f:Landroid/app/Activity;

    return-object v0
.end method

.method private g()Landroid/os/Bundle;
    .locals 5

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "com.google.android.gms.wallet.EXTRA_ENVIRONMENT"

    iget v2, p0, Lty;->g:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "androidPackageName"

    iget-object v2, p0, Lty;->f:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lty;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    new-instance v2, Landroid/accounts/Account;

    iget-object v3, p0, Lty;->h:Ljava/lang/String;

    const-string v4, "com.google"

    invoke-direct {v2, v3, v4}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    const-string v1, "com.google.android.gms.wallet.EXTRA_THEME"

    iget v2, p0, Lty;->i:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/os/IBinder;)Lts;
    .locals 1

    invoke-static {p1}, Ltt;->a(Landroid/os/IBinder;)Lts;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .locals 4

    invoke-direct {p0}, Lty;->g()Landroid/os/Bundle;

    move-result-object v1

    new-instance v2, Ltz;

    invoke-direct {v2, p0, p1}, Ltz;-><init>(Lty;I)V

    :try_start_0
    invoke-virtual {p0}, Lty;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lts;

    invoke-interface {v0, v1, v2}, Lts;->a(Landroid/os/Bundle;Ltv;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "WalletClientImpl"

    const-string v3, "RemoteException during checkForPreAuthorization"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/16 v0, 0x8

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v1, v3}, Ltz;->a(IZLandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/wallet/FullWalletRequest;I)V
    .locals 5

    const/4 v4, 0x0

    new-instance v1, Ltz;

    invoke-direct {v1, p0, p2}, Ltz;-><init>(Lty;I)V

    invoke-direct {p0}, Lty;->g()Landroid/os/Bundle;

    move-result-object v2

    :try_start_0
    invoke-virtual {p0}, Lty;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lts;

    invoke-interface {v0, p1, v2, v1}, Lts;->a(Lcom/google/android/gms/wallet/FullWalletRequest;Landroid/os/Bundle;Ltv;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "WalletClientImpl"

    const-string v3, "RemoteException getting full wallet"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v4, v4}, Ltz;->a(ILcom/google/android/gms/wallet/FullWallet;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/wallet/MaskedWalletRequest;I)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0}, Lty;->g()Landroid/os/Bundle;

    move-result-object v1

    new-instance v2, Ltz;

    invoke-direct {v2, p0, p2}, Ltz;-><init>(Lty;I)V

    :try_start_0
    invoke-virtual {p0}, Lty;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lts;

    invoke-interface {v0, p1, v1, v2}, Lts;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;Ltv;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "WalletClientImpl"

    const-string v3, "RemoteException getting masked wallet"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/16 v0, 0x8

    invoke-virtual {v2, v0, v4, v4}, Ltz;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/wallet/NotifyTransactionStatusRequest;)V
    .locals 2

    invoke-direct {p0}, Lty;->g()Landroid/os/Bundle;

    move-result-object v1

    :try_start_0
    invoke-virtual {p0}, Lty;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lts;

    invoke-interface {v0, p1, v1}, Lts;->a(Lcom/google/android/gms/wallet/NotifyTransactionStatusRequest;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0}, Lty;->g()Landroid/os/Bundle;

    move-result-object v1

    new-instance v2, Ltz;

    invoke-direct {v2, p0, p3}, Ltz;-><init>(Lty;I)V

    :try_start_0
    invoke-virtual {p0}, Lty;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lts;

    invoke-interface {v0, p1, p2, v1, v2}, Lts;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ltv;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "WalletClientImpl"

    const-string v3, "RemoteException changing masked wallet"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/16 v0, 0x8

    invoke-virtual {v2, v0, v4, v4}, Ltz;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected a(Lno;Lnd;)V
    .locals 1

    const v0, 0x3d8024

    invoke-interface {p1, p2, v0}, Lno;->a(Lnl;I)V

    return-void
.end method

.method protected synthetic b(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lty;->a(Landroid/os/IBinder;)Lts;

    move-result-object v0

    return-object v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.wallet.service.BIND"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.wallet.internal.IOwService"

    return-object v0
.end method
