.class public final Lfb;
.super Ljava/lang/Object;

# interfaces
.implements Ler;


# instance fields
.field private final a:J

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:J

.field private final f:Ljava/lang/String;

.field private final g:Landroid/net/Uri;

.field private final h:Landroid/net/Uri;

.field private final i:Lcom/google/android/gms/games/PlayerEntity;

.field private final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ler;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-interface {p1}, Ler;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lfb;->a:J

    invoke-interface {p1}, Ler;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lnx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lfb;->b:Ljava/lang/String;

    invoke-interface {p1}, Ler;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lnx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lfb;->c:Ljava/lang/String;

    invoke-interface {p1}, Ler;->d()J

    move-result-wide v0

    iput-wide v0, p0, Lfb;->d:J

    invoke-interface {p1}, Ler;->e()J

    move-result-wide v0

    iput-wide v0, p0, Lfb;->e:J

    invoke-interface {p1}, Ler;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfb;->f:Ljava/lang/String;

    invoke-interface {p1}, Ler;->g()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lfb;->g:Landroid/net/Uri;

    invoke-interface {p1}, Ler;->j()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lfb;->h:Landroid/net/Uri;

    invoke-interface {p1}, Ler;->k()Lcom/google/android/gms/games/Player;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lfb;->i:Lcom/google/android/gms/games/PlayerEntity;

    invoke-interface {p1}, Ler;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfb;->j:Ljava/lang/String;

    return-void

    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->i()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/PlayerEntity;

    goto :goto_0
.end method

.method static a(Ler;)I
    .locals 4

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-interface {p0}, Ler;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-interface {p0}, Ler;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-interface {p0}, Ler;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-interface {p0}, Ler;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-interface {p0}, Ler;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-interface {p0}, Ler;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    invoke-interface {p0}, Ler;->g()Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    invoke-interface {p0}, Ler;->j()Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    invoke-interface {p0}, Ler;->k()Lcom/google/android/gms/games/Player;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lnu;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method static a(Ler;Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    instance-of v2, p1, Ler;

    if-nez v2, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eq p0, p1, :cond_0

    check-cast p1, Ler;

    invoke-interface {p1}, Ler;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p0}, Ler;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lnu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ler;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, Ler;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lnu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ler;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p0}, Ler;->d()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lnu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ler;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, Ler;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lnu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ler;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p0}, Ler;->e()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lnu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ler;->f()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, Ler;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lnu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ler;->g()Landroid/net/Uri;

    move-result-object v2

    invoke-interface {p0}, Ler;->g()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, Lnu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ler;->j()Landroid/net/Uri;

    move-result-object v2

    invoke-interface {p0}, Ler;->j()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, Lnu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ler;->k()Lcom/google/android/gms/games/Player;

    move-result-object v2

    invoke-interface {p0}, Ler;->k()Lcom/google/android/gms/games/Player;

    move-result-object v3

    invoke-static {v2, v3}, Lnu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ler;->l()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, Ler;->l()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lnu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto/16 :goto_0
.end method

.method static b(Ler;)Ljava/lang/String;
    .locals 4

    invoke-static {p0}, Lnu;->a(Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "Rank"

    invoke-interface {p0}, Ler;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "DisplayRank"

    invoke-interface {p0}, Ler;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "Score"

    invoke-interface {p0}, Ler;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "DisplayScore"

    invoke-interface {p0}, Ler;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "Timestamp"

    invoke-interface {p0}, Ler;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "DisplayName"

    invoke-interface {p0}, Ler;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "IconImageUri"

    invoke-interface {p0}, Ler;->g()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "HiResImageUri"

    invoke-interface {p0}, Ler;->j()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v1

    const-string v2, "Player"

    invoke-interface {p0}, Ler;->k()Lcom/google/android/gms/games/Player;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "ScoreTag"

    invoke-interface {p0}, Ler;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    invoke-virtual {v0}, Lnw;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-interface {p0}, Ler;->k()Lcom/google/android/gms/games/Player;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lfb;->a:J

    return-wide v0
.end method

.method public a(Landroid/database/CharArrayBuffer;)V
    .locals 1

    iget-object v0, p0, Lfb;->b:Ljava/lang/String;

    invoke-static {v0, p1}, Lon;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    return-void
.end method

.method public a_()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfb;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b(Landroid/database/CharArrayBuffer;)V
    .locals 1

    iget-object v0, p0, Lfb;->c:Ljava/lang/String;

    invoke-static {v0, p1}, Lon;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfb;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c(Landroid/database/CharArrayBuffer;)V
    .locals 1

    iget-object v0, p0, Lfb;->i:Lcom/google/android/gms/games/PlayerEntity;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfb;->f:Ljava/lang/String;

    invoke-static {v0, p1}, Lon;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lfb;->i:Lcom/google/android/gms/games/PlayerEntity;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/PlayerEntity;->a(Landroid/database/CharArrayBuffer;)V

    goto :goto_0
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, Lfb;->d:J

    return-wide v0
.end method

.method public e()J
    .locals 2

    iget-wide v0, p0, Lfb;->e:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    invoke-static {p0, p1}, Lfb;->a(Ler;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfb;->i:Lcom/google/android/gms/games/PlayerEntity;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfb;->f:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfb;->i:Lcom/google/android/gms/games/PlayerEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerEntity;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public g()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lfb;->i:Lcom/google/android/gms/games/PlayerEntity;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfb;->g:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfb;->i:Lcom/google/android/gms/games/PlayerEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerEntity;->d()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    invoke-static {p0}, Lfb;->a(Ler;)I

    move-result v0

    return v0
.end method

.method public synthetic i()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lfb;->m()Ler;

    move-result-object v0

    return-object v0
.end method

.method public j()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lfb;->i:Lcom/google/android/gms/games/PlayerEntity;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfb;->h:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfb;->i:Lcom/google/android/gms/games/PlayerEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerEntity;->f()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public k()Lcom/google/android/gms/games/Player;
    .locals 1

    iget-object v0, p0, Lfb;->i:Lcom/google/android/gms/games/PlayerEntity;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfb;->j:Ljava/lang/String;

    return-object v0
.end method

.method public m()Ler;
    .locals 0

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lfb;->b(Ler;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
