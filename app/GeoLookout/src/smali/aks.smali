.class public Laks;
.super Ljava/lang/Object;
.source "MapAnimation.java"


# static fields
.field public static final e:[I

.field public static final f:[I


# instance fields
.field public a:I

.field public b:Z

.field public c:Laah;

.field public d:Laah;

.field private g:I

.field private h:Landroid/graphics/Bitmap;

.field private i:Landroid/graphics/Bitmap;

.field private j:Landroid/graphics/Canvas;

.field private k:Landroid/graphics/Bitmap;

.field private l:Landroid/graphics/BitmapFactory$Options;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x1e

    .line 114
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Laks;->e:[I

    .line 125
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Laks;->f:[I

    return-void

    .line 114
    nop

    :array_0
    .array-data 4
        0x7f020061
        0x7f020062
        0x7f020063
        0x7f020064
        0x7f020065
        0x7f020066
        0x7f020067
        0x7f020068
        0x7f020069
        0x7f02006a
        0x7f02006b
        0x7f02006c
        0x7f02006d
        0x7f02006e
        0x7f02006f
        0x7f020070
        0x7f020071
        0x7f020072
        0x7f020073
        0x7f020074
        0x7f020075
        0x7f020076
        0x7f020077
        0x7f020078
        0x7f020079
        0x7f02007a
        0x7f02007b
        0x7f02007c
        0x7f02007d
        0x7f02007e
    .end array-data

    .line 125
    :array_1
    .array-data 4
        0x7f02007f
        0x7f020080
        0x7f020081
        0x7f020082
        0x7f020083
        0x7f020084
        0x7f020085
        0x7f020086
        0x7f020087
        0x7f020088
        0x7f020089
        0x7f02008a
        0x7f02008b
        0x7f02008c
        0x7f02008d
        0x7f02008e
        0x7f02008f
        0x7f020090
        0x7f020091
        0x7f020092
        0x7f020093
        0x7f020094
        0x7f020095
        0x7f020096
        0x7f020097
        0x7f020098
        0x7f020099
        0x7f02009a
        0x7f02009b
        0x7f02009c
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Laks;->h:Landroid/graphics/Bitmap;

    .line 31
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 34
    sget-object v0, Laks;->e:[I

    iget v1, p0, Laks;->g:I

    aget v0, v0, v1

    invoke-virtual {p0, p1, v0}, Laks;->a(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 45
    iget-object v0, p0, Laks;->k:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Laks;->k:Landroid/graphics/Bitmap;

    .line 47
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Laks;->l:Landroid/graphics/BitmapFactory$Options;

    .line 48
    iget-object v0, p0, Laks;->l:Landroid/graphics/BitmapFactory$Options;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 49
    iget-object v0, p0, Laks;->l:Landroid/graphics/BitmapFactory$Options;

    iget-object v1, p0, Laks;->k:Landroid/graphics/Bitmap;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 54
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Laks;->l:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v0, p2, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 59
    :goto_0
    iget-object v1, p0, Laks;->l:Landroid/graphics/BitmapFactory$Options;

    iput-object v0, v1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 61
    iget-object v1, p0, Laks;->i:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    iget-object v1, p0, Laks;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Laks;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 62
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Laks;->i:Landroid/graphics/Bitmap;

    .line 63
    new-instance v1, Landroid/graphics/Canvas;

    iget-object v2, p0, Laks;->i:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Laks;->j:Landroid/graphics/Canvas;

    .line 66
    :cond_2
    iget-object v1, p0, Laks;->j:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 67
    iget-object v1, p0, Laks;->j:Landroid/graphics/Canvas;

    invoke-virtual {v1, v0, v4, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 68
    iget-object v0, p0, Laks;->j:Landroid/graphics/Canvas;

    iget-object v1, p0, Laks;->h:Landroid/graphics/Bitmap;

    iget-object v2, p0, Laks;->j:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Laks;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Laks;->j:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Laks;->h:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 70
    iget-object v0, p0, Laks;->i:Landroid/graphics/Bitmap;

    return-object v0

    .line 55
    :catch_0
    move-exception v0

    .line 56
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 74
    iput-object v0, p0, Laks;->c:Laah;

    .line 75
    iput-object v0, p0, Laks;->d:Laah;

    .line 76
    iput-object v0, p0, Laks;->h:Landroid/graphics/Bitmap;

    .line 77
    iput-object v0, p0, Laks;->i:Landroid/graphics/Bitmap;

    .line 78
    iput-object v0, p0, Laks;->j:Landroid/graphics/Canvas;

    .line 79
    return-void
.end method

.method public b(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 38
    sget-object v0, Laks;->f:[I

    iget v1, p0, Laks;->g:I

    aget v0, v0, v1

    invoke-virtual {p0, p1, v0}, Laks;->a(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Laks;->b:Z

    .line 103
    const/4 v0, 0x0

    iput v0, p0, Laks;->a:I

    .line 104
    return-void
.end method

.method public b(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 82
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    .line 83
    invoke-virtual {p0, p1}, Laks;->d(Landroid/content/Context;)V

    .line 87
    :goto_0
    iget v0, p0, Laks;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Laks;->g:I

    .line 89
    iget v0, p0, Laks;->g:I

    const/16 v1, 0x1d

    if-le v0, v1, :cond_0

    .line 90
    const/4 v0, 0x0

    iput v0, p0, Laks;->g:I

    .line 91
    iget v0, p0, Laks;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Laks;->a:I

    .line 93
    :cond_0
    return-void

    .line 85
    :cond_1
    invoke-virtual {p0, p1}, Laks;->e(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public c(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Laks;->c:Laah;

    const v1, 0x7f020060

    invoke-virtual {p0, p1, v1}, Laks;->a(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lzx;->a(Landroid/graphics/Bitmap;)Lzw;

    move-result-object v1

    invoke-virtual {v0, v1}, Laah;->a(Lzw;)V

    .line 99
    return-void
.end method

.method public d(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Laks;->c:Laah;

    invoke-virtual {p0, p1}, Laks;->a(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lzx;->a(Landroid/graphics/Bitmap;)Lzw;

    move-result-object v1

    invoke-virtual {v0, v1}, Laah;->a(Lzw;)V

    .line 108
    return-void
.end method

.method public e(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Laks;->c:Laah;

    invoke-virtual {p0, p1}, Laks;->a(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lzx;->a(Landroid/graphics/Bitmap;)Lzw;

    move-result-object v1

    invoke-virtual {v0, v1}, Laah;->a(Lzw;)V

    .line 112
    return-void
.end method
