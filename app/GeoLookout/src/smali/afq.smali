.class public final Lafq;
.super Ljava/lang/Object;
.source "SlookCocktailSubWindow.java"


# static fields
.field private static a:Laew;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Laew;

    invoke-direct {v0}, Laew;-><init>()V

    sput-object v0, Lafq;->a:Laew;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/app/Activity;)Landroid/view/Window;
    .locals 2

    .prologue
    .line 71
    sget-object v0, Lafq;->a:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    if-nez p0, :cond_0

    .line 73
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "activity is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getSubWindow()Landroid/view/Window;

    move-result-object v0

    .line 77
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/app/Activity;I)V
    .locals 2

    .prologue
    .line 43
    sget-object v0, Lafq;->a:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    if-nez p0, :cond_0

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "activity is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    invoke-virtual {p0, p1}, Landroid/app/Activity;->setSubContentView(I)V

    .line 49
    :cond_1
    return-void
.end method

.method public static a(Landroid/app/Activity;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 57
    sget-object v0, Lafq;->a:Laew;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Laew;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    if-nez p0, :cond_0

    .line 59
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "activity is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    invoke-virtual {p0, p1}, Landroid/app/Activity;->setSubContentView(Landroid/view/View;)V

    .line 63
    :cond_1
    return-void
.end method
