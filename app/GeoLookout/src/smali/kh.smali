.class public final Lkh;
.super Lkf;

# interfaces
.implements Lcb;
.implements Lcc;


# instance fields
.field private final a:Lke;

.field private final b:Lki;

.field private final c:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/bu;Lke;)V
    .locals 2

    invoke-direct {p0, p2, p3}, Lkf;-><init>(Lcom/google/android/gms/internal/bu;Lke;)V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lkh;->c:Ljava/lang/Object;

    iput-object p3, p0, Lkh;->a:Lke;

    new-instance v0, Lki;

    iget-object v1, p2, Lcom/google/android/gms/internal/bu;->m:Lcom/google/android/gms/internal/co;

    iget v1, v1, Lcom/google/android/gms/internal/co;->e:I

    invoke-direct {v0, p1, p0, p0, v1}, Lki;-><init>(Landroid/content/Context;Lcb;Lcc;I)V

    iput-object v0, p0, Lkh;->b:Lki;

    iget-object v0, p0, Lkh;->b:Lki;

    invoke-virtual {v0}, Lki;->a()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 0

    invoke-virtual {p0}, Lkh;->e()V

    return-void
.end method

.method public a(Lbz;)V
    .locals 3

    iget-object v0, p0, Lkh;->a:Lke;

    new-instance v1, Lcom/google/android/gms/internal/bw;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/internal/bw;-><init>(I)V

    invoke-interface {v0, v1}, Lke;->a(Lcom/google/android/gms/internal/bw;)V

    return-void
.end method

.method public b_()V
    .locals 1

    const-string v0, "Disconnected from remote ad request service."

    invoke-static {v0}, Llm;->a(Ljava/lang/String;)V

    return-void
.end method

.method public c()V
    .locals 2

    iget-object v1, p0, Lkh;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lkh;->b:Lki;

    invoke-virtual {v0}, Lki;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lkh;->b:Lki;

    invoke-virtual {v0}, Lki;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lkh;->b:Lki;

    invoke-virtual {v0}, Lki;->d()V

    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()Lkm;
    .locals 2

    iget-object v1, p0, Lkh;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lkh;->b:Lki;

    invoke-virtual {v0}, Lki;->g()Lkm;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    monitor-exit v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
