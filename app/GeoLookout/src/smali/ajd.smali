.class public Lajd;
.super Ljava/lang/Object;
.source "DisasterLifeConnectAgreeDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;


# direct methods
.method public constructor <init>(Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lajd;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Lajd;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;->a(Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->z(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 59
    iget-object v0, p0, Lajd;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;->a(Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lall;->x(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 61
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 62
    iget-object v1, p0, Lajd;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;

    const/16 v2, 0x6f

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;->startActivityForResult(Landroid/content/Intent;I)V

    .line 76
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lajd;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;->a(Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    iget-object v1, p0, Lajd;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;

    invoke-static {v1}, Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;->a(Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lage;->e(Landroid/content/Context;)V

    .line 65
    sget-boolean v0, Laky;->k:Z

    if-eqz v0, :cond_1

    .line 66
    iget-object v0, p0, Lajd;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;->a(Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 67
    invoke-static {}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a()Lcom/sec/android/GeoLookout/widget/LifeModeWidget;

    move-result-object v1

    iget-object v2, p0, Lajd;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;

    invoke-static {v2}, Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;->a(Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeWidget;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 68
    invoke-static {}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a()Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;

    move-result-object v1

    iget-object v2, p0, Lajd;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;

    invoke-static {v2}, Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;->a(Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/GeoLookout/widget/LifeModeTransparentWidget;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 70
    :cond_1
    iget-object v0, p0, Lajd;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;->finish()V

    goto :goto_0

    .line 73
    :cond_2
    iget-object v0, p0, Lajd;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;

    invoke-static {v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;->a(Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0016

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 74
    iget-object v0, p0, Lajd;->a:Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;

    invoke-virtual {v0}, Lcom/sec/android/GeoLookout/activity/DisasterLifeConnectAgreeDialog;->finish()V

    goto :goto_0
.end method
