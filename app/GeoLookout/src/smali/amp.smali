.class public Lamp;
.super Ljava/lang/Object;
.source "LifemodeSetup.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lamj;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 16
    sget-boolean v1, Laky;->a:Z

    if-eqz v1, :cond_0

    .line 17
    new-instance v1, Lamm;

    invoke-direct {v1}, Lamm;-><init>()V

    .line 20
    const-string v2, ""

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->a()Lamm;

    move-result-object v2

    invoke-virtual {v2, v6}, Lamm;->a(I)Lamm;

    move-result-object v2

    invoke-virtual {v2, v6, v6, v6}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 21
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 24
    const-string v2, "L1010100"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a04e9

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900f0

    const v4, 0x7f0900c0

    const v5, 0x7f0900d8

    invoke-virtual {v2, v3, v4, v5}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 30
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 33
    const-string v2, "L1030100"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a0039

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900e0

    const v4, 0x7f0900b0

    const v5, 0x7f0900c8

    invoke-virtual {v2, v3, v4, v5}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 42
    const-string v2, "L1020110"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a00a8

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900e3

    const v4, 0x7f0900b3

    const v5, 0x7f0900cb

    invoke-virtual {v2, v3, v4, v5}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 51
    const-string v2, "L1040100"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a0036

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900de

    const v4, 0x7f0900ae

    const v5, 0x7f0900c6

    invoke-virtual {v2, v3, v4, v5}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 60
    const-string v2, "L1080100"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a0032

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900dd

    const v4, 0x7f0900ad

    const v5, 0x7f0900c5

    invoke-virtual {v2, v3, v4, v5}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 69
    const-string v2, "L1072100"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a055b

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900f2

    const v4, 0x7f0900da

    invoke-virtual {v2, v3, v6, v4}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 77
    const-string v2, "L1061300"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a00b2

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900e8

    const v4, 0x7f0900b8

    const v5, 0x7f0900d0

    invoke-virtual {v2, v3, v4, v5}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 86
    const-string v2, "L1070100"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a0038

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900df

    const v4, 0x7f0900af

    const v5, 0x7f0900c7

    invoke-virtual {v2, v3, v4, v5}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 95
    const-string v2, "L1050100"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a00aa

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900e4

    const v4, 0x7f0900b4

    const v5, 0x7f0900cc

    invoke-virtual {v2, v3, v4, v5}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 104
    const-string v2, "L1090700"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a00b1

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900e7

    const v4, 0x7f0900b7

    const v5, 0x7f0900cf

    invoke-virtual {v2, v3, v4, v5}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 113
    const-string v2, "L1014100"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a0030

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900dc

    const v4, 0x7f0900ac

    const v5, 0x7f0900c4

    invoke-virtual {v2, v3, v4, v5}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 252
    :goto_0
    return-object v0

    .line 121
    :cond_0
    sget-boolean v1, Laky;->b:Z

    if-eqz v1, :cond_1

    .line 122
    new-instance v1, Lamm;

    invoke-direct {v1}, Lamm;-><init>()V

    .line 125
    const-string v2, ""

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->a()Lamm;

    move-result-object v2

    invoke-virtual {v2, v6}, Lamm;->a(I)Lamm;

    move-result-object v2

    invoke-virtual {v2, v6, v6, v6}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 129
    const-string v2, "SAPP"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a00d2

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900ee

    const v4, 0x7f0900d6

    invoke-virtual {v2, v3, v6, v4}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 138
    const-string v2, "KUS"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a00ce

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900ea

    const v4, 0x7f0900ba

    const v5, 0x7f0900d2

    invoke-virtual {v2, v3, v4, v5}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 147
    const-string v2, "AKI"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a00c9

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900db

    const v4, 0x7f0900c3

    invoke-virtual {v2, v3, v6, v4}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 156
    const-string v2, "SEN"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a00d3

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900ef

    const v4, 0x7f0900bf

    const v5, 0x7f0900d7

    invoke-virtual {v2, v3, v4, v5}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 165
    const-string v2, "KNA"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a00cd

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900e9

    const v4, 0x7f0900d1

    invoke-virtual {v2, v3, v6, v4}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 174
    const-string v2, "SHNJK"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a00d5

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900f1

    const v4, 0x7f0900c1

    const v5, 0x7f0900d9

    invoke-virtual {v2, v3, v4, v5}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 183
    const-string v2, "OSA"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a00d1

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900ed

    const v4, 0x7f0900d5

    invoke-virtual {v2, v3, v6, v4}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 192
    const-string v2, "NAG"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a00cf

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900eb

    const v4, 0x7f0900bb

    const v5, 0x7f0900d3

    invoke-virtual {v2, v3, v4, v5}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 201
    const-string v2, "TKM"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a00d4

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900e1

    const v4, 0x7f0900b1

    const v5, 0x7f0900c9

    invoke-virtual {v2, v3, v4, v5}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 207
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 210
    const-string v2, "HIR"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a00cb

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900e5

    const v4, 0x7f0900cd

    invoke-virtual {v2, v3, v6, v4}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 216
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 219
    const-string v2, "FUK"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a00ca

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900e6

    const v4, 0x7f0900ce

    invoke-virtual {v2, v3, v6, v4}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 228
    const-string v2, "KAG"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a00cc

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900e2

    const v4, 0x7f0900ca

    invoke-virtual {v2, v3, v6, v4}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    invoke-virtual {v1}, Lamm;->c()Lamm;

    .line 237
    const-string v2, "NAH"

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    const v3, 0x7f0a00d0

    invoke-virtual {v2, v3}, Lamm;->a(I)Lamm;

    move-result-object v2

    const v3, 0x7f0900ec

    const v4, 0x7f0900d4

    invoke-virtual {v2, v3, v6, v4}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 243
    invoke-virtual {v1}, Lamm;->c()Lamm;

    goto/16 :goto_0

    .line 246
    :cond_1
    new-instance v1, Lamm;

    invoke-direct {v1}, Lamm;-><init>()V

    .line 248
    const-string v2, ""

    invoke-virtual {v1, v2}, Lamm;->a(Ljava/lang/String;)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->a()Lamm;

    move-result-object v2

    invoke-virtual {v2, v6}, Lamm;->a(I)Lamm;

    move-result-object v2

    invoke-virtual {v2, v6, v6, v6}, Lamm;->a(III)Lamm;

    move-result-object v2

    invoke-virtual {v2}, Lamm;->b()Lamj;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 249
    invoke-virtual {v1}, Lamm;->c()Lamm;

    goto/16 :goto_0
.end method

.method public static b()Landroid/util/SparseArray;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Lamq;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v8, 0x1f7

    const/16 v7, 0x1f6

    const/16 v4, 0x1f5

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 256
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 258
    sget-boolean v1, Laky;->a:Z

    if-eqz v1, :cond_1

    .line 261
    new-instance v1, Lamr;

    invoke-direct {v1}, Lamr;-><init>()V

    .line 264
    const/16 v2, 0x259

    const/16 v3, 0x259

    invoke-virtual {v1, v3}, Lamr;->a(I)Lamr;

    move-result-object v3

    const-string v4, "life_uv"

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v3

    const v4, 0x7f0200f8

    invoke-virtual {v3, v4}, Lamr;->b(I)Lamr;

    move-result-object v3

    const v4, 0x7f020133

    invoke-virtual {v3, v4}, Lamr;->i(I)Lamr;

    move-result-object v3

    invoke-virtual {v3, v5}, Lamr;->j(I)Lamr;

    move-result-object v3

    const v4, 0x7f020126

    invoke-virtual {v3, v4}, Lamr;->g(I)Lamr;

    move-result-object v3

    const v4, 0x7f020148

    invoke-virtual {v3, v4}, Lamr;->h(I)Lamr;

    move-result-object v3

    const v4, 0x7f070045

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070046

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070047

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f0200f9

    invoke-virtual {v3, v4}, Lamr;->c(I)Lamr;

    move-result-object v3

    const v4, 0x7f070048

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070049

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f0a02a3

    invoke-virtual {v3, v4}, Lamr;->d(I)Lamr;

    move-result-object v3

    invoke-virtual {v3}, Lamr;->a()Lamq;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 277
    invoke-virtual {v1}, Lamr;->b()V

    .line 280
    const/16 v2, 0x25a

    const/16 v3, 0x25a

    invoke-virtual {v1, v3}, Lamr;->a(I)Lamr;

    move-result-object v3

    const-string v4, "life_f_temp"

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v3

    const v4, 0x7f0a0297

    invoke-virtual {v3, v4}, Lamr;->d(I)Lamr;

    move-result-object v3

    const v4, 0x7f0200ea

    invoke-virtual {v3, v4}, Lamr;->b(I)Lamr;

    move-result-object v3

    invoke-virtual {v3, v6}, Lamr;->j(I)Lamr;

    move-result-object v3

    const v4, 0x7f02011a

    invoke-virtual {v3, v4}, Lamr;->g(I)Lamr;

    move-result-object v3

    const v4, 0x7f02013c

    invoke-virtual {v3, v4}, Lamr;->h(I)Lamr;

    move-result-object v3

    const v4, 0x7f0200eb

    invoke-virtual {v3, v4}, Lamr;->c(I)Lamr;

    move-result-object v3

    const v4, 0x7f070025

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070026

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070027

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070028

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f02012e

    invoke-virtual {v3, v4}, Lamr;->i(I)Lamr;

    move-result-object v3

    invoke-virtual {v3}, Lamr;->a()Lamq;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 292
    invoke-virtual {v1}, Lamr;->b()V

    .line 295
    const/16 v2, 0x262

    const/16 v3, 0x262

    invoke-virtual {v1, v3}, Lamr;->a(I)Lamr;

    move-result-object v3

    const-string v4, "life_temp"

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v3

    const v4, 0x7f0a02a2

    invoke-virtual {v3, v4}, Lamr;->d(I)Lamr;

    move-result-object v3

    const v4, 0x7f0200e8

    invoke-virtual {v3, v4}, Lamr;->b(I)Lamr;

    move-result-object v3

    invoke-virtual {v3, v6}, Lamr;->j(I)Lamr;

    move-result-object v3

    const v4, 0x7f020118

    invoke-virtual {v3, v4}, Lamr;->g(I)Lamr;

    move-result-object v3

    const v4, 0x7f02013a

    invoke-virtual {v3, v4}, Lamr;->h(I)Lamr;

    move-result-object v3

    const v4, 0x7f0200e9

    invoke-virtual {v3, v4}, Lamr;->c(I)Lamr;

    move-result-object v3

    const v4, 0x7f070041

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070042

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070043

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070044

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f02012b

    invoke-virtual {v3, v4}, Lamr;->i(I)Lamr;

    move-result-object v3

    invoke-virtual {v3}, Lamr;->a()Lamq;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 307
    invoke-virtual {v1}, Lamr;->b()V

    .line 310
    const/16 v2, 0x25b

    const/16 v3, 0x25b

    invoke-virtual {v1, v3}, Lamr;->a(I)Lamr;

    move-result-object v3

    const-string v4, "life_putre"

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v3

    const v4, 0x7f0200ee

    invoke-virtual {v3, v4}, Lamr;->b(I)Lamr;

    move-result-object v3

    const v4, 0x7f0200ef

    invoke-virtual {v3, v4}, Lamr;->c(I)Lamr;

    move-result-object v3

    const v4, 0x7f0a029f

    invoke-virtual {v3, v4}, Lamr;->d(I)Lamr;

    move-result-object v3

    invoke-virtual {v3, v6}, Lamr;->j(I)Lamr;

    move-result-object v3

    const v4, 0x7f02011c

    invoke-virtual {v3, v4}, Lamr;->g(I)Lamr;

    move-result-object v3

    const v4, 0x7f02013e

    invoke-virtual {v3, v4}, Lamr;->h(I)Lamr;

    move-result-object v3

    const v4, 0x7f070039

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f07003a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f07003b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f07003c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f020131

    invoke-virtual {v3, v4}, Lamr;->i(I)Lamr;

    move-result-object v3

    invoke-virtual {v3}, Lamr;->a()Lamq;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 323
    invoke-virtual {v1}, Lamr;->b()V

    .line 326
    const/16 v2, 0x25c

    const/16 v3, 0x25c

    invoke-virtual {v1, v3}, Lamr;->a(I)Lamr;

    move-result-object v3

    const-string v4, "life_hum"

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v3

    const v4, 0x7f0200f6

    invoke-virtual {v3, v4}, Lamr;->b(I)Lamr;

    move-result-object v3

    const v4, 0x7f0200f7

    invoke-virtual {v3, v4}, Lamr;->c(I)Lamr;

    move-result-object v3

    const v4, 0x7f0a02a1

    invoke-virtual {v3, v4}, Lamr;->d(I)Lamr;

    move-result-object v3

    invoke-virtual {v3, v6}, Lamr;->j(I)Lamr;

    move-result-object v3

    const v4, 0x7f020123

    invoke-virtual {v3, v4}, Lamr;->g(I)Lamr;

    move-result-object v3

    const v4, 0x7f020145

    invoke-virtual {v3, v4}, Lamr;->h(I)Lamr;

    move-result-object v3

    const v4, 0x7f070031

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070032

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070033

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070034

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f020132

    invoke-virtual {v3, v4}, Lamr;->i(I)Lamr;

    move-result-object v3

    invoke-virtual {v3}, Lamr;->a()Lamq;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 339
    invoke-virtual {v1}, Lamr;->b()V

    .line 342
    const/16 v2, 0x25d

    const/16 v3, 0x25d

    invoke-virtual {v1, v3}, Lamr;->a(I)Lamr;

    move-result-object v3

    const-string v4, "life_s_temp"

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v3

    const v4, 0x7f0200f0

    invoke-virtual {v3, v4}, Lamr;->b(I)Lamr;

    move-result-object v3

    const v4, 0x7f0200f1

    invoke-virtual {v3, v4}, Lamr;->c(I)Lamr;

    move-result-object v3

    const v4, 0x7f0a02a0

    invoke-virtual {v3, v4}, Lamr;->d(I)Lamr;

    move-result-object v3

    invoke-virtual {v3, v6}, Lamr;->j(I)Lamr;

    move-result-object v3

    const v4, 0x7f02011d

    invoke-virtual {v3, v4}, Lamr;->g(I)Lamr;

    move-result-object v3

    const v4, 0x7f02013f

    invoke-virtual {v3, v4}, Lamr;->h(I)Lamr;

    move-result-object v3

    const v4, 0x7f07003d

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f07003e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f07003f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070040

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f020134

    invoke-virtual {v3, v4}, Lamr;->i(I)Lamr;

    move-result-object v3

    const v4, 0x7f0a02c1

    invoke-virtual {v3, v4}, Lamr;->k(I)Lamr;

    move-result-object v3

    invoke-virtual {v3}, Lamr;->a()Lamq;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 355
    invoke-virtual {v1}, Lamr;->b()V

    .line 358
    const/16 v2, 0x25e

    const/16 v3, 0x25e

    invoke-virtual {v1, v3}, Lamr;->a(I)Lamr;

    move-result-object v3

    const-string v4, "life_freez"

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v3

    const v4, 0x7f0200e6

    invoke-virtual {v3, v4}, Lamr;->b(I)Lamr;

    move-result-object v3

    const v4, 0x7f0200e7

    invoke-virtual {v3, v4}, Lamr;->c(I)Lamr;

    move-result-object v3

    const v4, 0x7f0a0298

    invoke-virtual {v3, v4}, Lamr;->d(I)Lamr;

    move-result-object v3

    invoke-virtual {v3, v6}, Lamr;->j(I)Lamr;

    move-result-object v3

    const v4, 0x7f020117

    invoke-virtual {v3, v4}, Lamr;->g(I)Lamr;

    move-result-object v3

    const v4, 0x7f020139

    invoke-virtual {v3, v4}, Lamr;->h(I)Lamr;

    move-result-object v3

    const v4, 0x7f070029

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f07002a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f07002b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f07002c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f02012f

    invoke-virtual {v3, v4}, Lamr;->i(I)Lamr;

    move-result-object v3

    invoke-virtual {v3}, Lamr;->a()Lamq;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 371
    invoke-virtual {v1}, Lamr;->b()V

    .line 374
    const/16 v2, 0x25f

    const/16 v3, 0x25f

    invoke-virtual {v1, v3}, Lamr;->a(I)Lamr;

    move-result-object v3

    const-string v4, "life_fros"

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v3

    const v4, 0x7f0200e2

    invoke-virtual {v3, v4}, Lamr;->b(I)Lamr;

    move-result-object v3

    const v4, 0x7f0200e3

    invoke-virtual {v3, v4}, Lamr;->c(I)Lamr;

    move-result-object v3

    const v4, 0x7f0a0299

    invoke-virtual {v3, v4}, Lamr;->d(I)Lamr;

    move-result-object v3

    invoke-virtual {v3, v6}, Lamr;->j(I)Lamr;

    move-result-object v3

    const v4, 0x7f020115

    invoke-virtual {v3, v4}, Lamr;->g(I)Lamr;

    move-result-object v3

    const v4, 0x7f020137

    invoke-virtual {v3, v4}, Lamr;->h(I)Lamr;

    move-result-object v3

    const v4, 0x7f07002d

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f07002e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f07002f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070030

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f02012a

    invoke-virtual {v3, v4}, Lamr;->i(I)Lamr;

    move-result-object v3

    invoke-virtual {v3}, Lamr;->a()Lamq;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 386
    invoke-virtual {v1}, Lamr;->b()V

    .line 389
    const/16 v2, 0x260

    const/16 v3, 0x260

    invoke-virtual {v1, v3}, Lamr;->a(I)Lamr;

    move-result-object v3

    const-string v4, "life_pm10"

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v3

    const v4, 0x7f0200f2

    invoke-virtual {v3, v4}, Lamr;->b(I)Lamr;

    move-result-object v3

    const v4, 0x7f0200ed

    invoke-virtual {v3, v4}, Lamr;->c(I)Lamr;

    move-result-object v3

    const v4, 0x7f0a029d

    invoke-virtual {v3, v4}, Lamr;->d(I)Lamr;

    move-result-object v3

    invoke-virtual {v3, v5}, Lamr;->j(I)Lamr;

    move-result-object v3

    const v4, 0x7f02011e

    invoke-virtual {v3, v4}, Lamr;->g(I)Lamr;

    move-result-object v3

    const v4, 0x7f020140

    invoke-virtual {v3, v4}, Lamr;->h(I)Lamr;

    move-result-object v3

    const v4, 0x7f07001d

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f07001e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f07001f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070020

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070020

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f02012c

    invoke-virtual {v3, v4}, Lamr;->i(I)Lamr;

    move-result-object v3

    const v4, 0x7f0a02c0

    invoke-virtual {v3, v4}, Lamr;->k(I)Lamr;

    move-result-object v3

    invoke-virtual {v3}, Lamr;->a()Lamq;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 403
    invoke-virtual {v1}, Lamr;->b()V

    .line 406
    const/16 v2, 0x261

    const/16 v3, 0x261

    invoke-virtual {v1, v3}, Lamr;->a(I)Lamr;

    move-result-object v3

    const-string v4, "life_cold"

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v3

    const v4, 0x7f0200e4

    invoke-virtual {v3, v4}, Lamr;->b(I)Lamr;

    move-result-object v3

    const v4, 0x7f0200e5

    invoke-virtual {v3, v4}, Lamr;->c(I)Lamr;

    move-result-object v3

    const v4, 0x7f0a0294

    invoke-virtual {v3, v4}, Lamr;->d(I)Lamr;

    move-result-object v3

    invoke-virtual {v3, v6}, Lamr;->j(I)Lamr;

    move-result-object v3

    const v4, 0x7f020116

    invoke-virtual {v3, v4}, Lamr;->g(I)Lamr;

    move-result-object v3

    const v4, 0x7f020138

    invoke-virtual {v3, v4}, Lamr;->h(I)Lamr;

    move-result-object v3

    const v4, 0x7f070021

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070022

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070023

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070024

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f02012d

    invoke-virtual {v3, v4}, Lamr;->i(I)Lamr;

    move-result-object v3

    invoke-virtual {v3}, Lamr;->a()Lamq;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 419
    invoke-virtual {v1}, Lamr;->b()V

    .line 422
    const/16 v2, 0x263

    const/16 v3, 0x263

    invoke-virtual {v1, v3}, Lamr;->a(I)Lamr;

    move-result-object v3

    const-string v4, "life_pollen"

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v3

    const v4, 0x7f0200f4

    invoke-virtual {v3, v4}, Lamr;->b(I)Lamr;

    move-result-object v3

    const v4, 0x7f0200f5

    invoke-virtual {v3, v4}, Lamr;->c(I)Lamr;

    move-result-object v3

    const v4, 0x7f0a029e

    invoke-virtual {v3, v4}, Lamr;->d(I)Lamr;

    move-result-object v3

    invoke-virtual {v3, v6}, Lamr;->j(I)Lamr;

    move-result-object v3

    const v4, 0x7f020120

    invoke-virtual {v3, v4}, Lamr;->g(I)Lamr;

    move-result-object v3

    const v4, 0x7f020142

    invoke-virtual {v3, v4}, Lamr;->h(I)Lamr;

    move-result-object v3

    const v4, 0x7f070035

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070036

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070037

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070038

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f020130

    invoke-virtual {v3, v4}, Lamr;->i(I)Lamr;

    move-result-object v3

    invoke-virtual {v3}, Lamr;->a()Lamq;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 435
    invoke-virtual {v1}, Lamr;->b()V

    .line 573
    :cond_0
    :goto_0
    return-object v0

    .line 437
    :cond_1
    sget-boolean v1, Laky;->b:Z

    if-eqz v1, :cond_0

    .line 438
    new-instance v1, Lamr;

    invoke-direct {v1}, Lamr;-><init>()V

    .line 441
    invoke-virtual {v1, v4}, Lamr;->a(I)Lamr;

    move-result-object v2

    const-string v3, "life_pollen"

    invoke-virtual {v2, v3}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v2

    const v3, 0x7f0200fa

    invoke-virtual {v2, v3}, Lamr;->b(I)Lamr;

    move-result-object v2

    const v3, 0x7f0200da

    invoke-virtual {v2, v3}, Lamr;->c(I)Lamr;

    move-result-object v2

    invoke-virtual {v2, v5}, Lamr;->j(I)Lamr;

    move-result-object v2

    const v3, 0x7f020121

    invoke-virtual {v2, v3}, Lamr;->i(I)Lamr;

    move-result-object v2

    const v3, 0x7f02014d

    invoke-virtual {v2, v3}, Lamr;->g(I)Lamr;

    move-result-object v2

    const v3, 0x7f020143

    invoke-virtual {v2, v3}, Lamr;->h(I)Lamr;

    move-result-object v2

    const v3, 0x7f07000c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v2

    const v3, 0x7f07000d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v2

    const v3, 0x7f07000e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v2

    const v3, 0x7f07000f

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v2

    const v3, 0x7f0a029e

    invoke-virtual {v2, v3}, Lamr;->d(I)Lamr;

    move-result-object v2

    invoke-virtual {v2}, Lamr;->a()Lamq;

    move-result-object v2

    invoke-virtual {v0, v4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 455
    invoke-virtual {v1}, Lamr;->b()V

    .line 458
    invoke-virtual {v1, v7}, Lamr;->a(I)Lamr;

    move-result-object v2

    const-string v3, "life_f_dust"

    invoke-virtual {v2, v3}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v2

    const v3, 0x7f0200e1

    invoke-virtual {v2, v3}, Lamr;->b(I)Lamr;

    move-result-object v2

    const v3, 0x7f0200d9

    invoke-virtual {v2, v3}, Lamr;->c(I)Lamr;

    move-result-object v2

    invoke-virtual {v2, v5}, Lamr;->j(I)Lamr;

    move-result-object v2

    const v3, 0x7f02011f

    invoke-virtual {v2, v3}, Lamr;->i(I)Lamr;

    move-result-object v2

    const v3, 0x7f02014c

    invoke-virtual {v2, v3}, Lamr;->g(I)Lamr;

    move-result-object v2

    const v3, 0x7f020141

    invoke-virtual {v2, v3}, Lamr;->h(I)Lamr;

    move-result-object v2

    const v3, 0x7f070005

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v2

    const v3, 0x7f070006

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v2

    const v3, 0x7f070007

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v2

    const v3, 0x7f070008

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v2

    const v3, 0x7f0a029b

    invoke-virtual {v2, v3}, Lamr;->d(I)Lamr;

    move-result-object v2

    invoke-virtual {v2}, Lamr;->a()Lamq;

    move-result-object v2

    invoke-virtual {v0, v7, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 472
    invoke-virtual {v1}, Lamr;->b()V

    .line 475
    invoke-virtual {v1, v8}, Lamr;->a(I)Lamr;

    move-result-object v2

    const-string v3, "life_y_dust"

    invoke-virtual {v2, v3}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v2

    const v3, 0x7f0200ff

    invoke-virtual {v2, v3}, Lamr;->b(I)Lamr;

    move-result-object v2

    const v3, 0x7f0200df

    invoke-virtual {v2, v3}, Lamr;->c(I)Lamr;

    move-result-object v2

    invoke-virtual {v2, v5}, Lamr;->j(I)Lamr;

    move-result-object v2

    const v3, 0x7f020128

    invoke-virtual {v2, v3}, Lamr;->i(I)Lamr;

    move-result-object v2

    const v3, 0x7f020152

    invoke-virtual {v2, v3}, Lamr;->g(I)Lamr;

    move-result-object v2

    const v3, 0x7f02014a

    invoke-virtual {v2, v3}, Lamr;->h(I)Lamr;

    move-result-object v2

    const v3, 0x7f070019

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v2

    const v3, 0x7f07001a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v2

    const v3, 0x7f07001b

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v2

    const v3, 0x7f07001c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v2

    const v3, 0x7f0a02a5

    invoke-virtual {v2, v3}, Lamr;->d(I)Lamr;

    move-result-object v2

    invoke-virtual {v2}, Lamr;->a()Lamq;

    move-result-object v2

    invoke-virtual {v0, v8, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 489
    invoke-virtual {v1}, Lamr;->b()V

    .line 492
    const/16 v2, 0x1f8

    const/16 v3, 0x1f8

    invoke-virtual {v1, v3}, Lamr;->a(I)Lamr;

    move-result-object v3

    const-string v4, "life_uv"

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v3

    const v4, 0x7f0200fe

    invoke-virtual {v3, v4}, Lamr;->b(I)Lamr;

    move-result-object v3

    const v4, 0x7f0200de

    invoke-virtual {v3, v4}, Lamr;->c(I)Lamr;

    move-result-object v3

    invoke-virtual {v3, v5}, Lamr;->j(I)Lamr;

    move-result-object v3

    const v4, 0x7f020127

    invoke-virtual {v3, v4}, Lamr;->i(I)Lamr;

    move-result-object v3

    const v4, 0x7f020151

    invoke-virtual {v3, v4}, Lamr;->g(I)Lamr;

    move-result-object v3

    const v4, 0x7f020149

    invoke-virtual {v3, v4}, Lamr;->h(I)Lamr;

    move-result-object v3

    const v4, 0x7f070016

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070017

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070018

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f0a02a3

    invoke-virtual {v3, v4}, Lamr;->d(I)Lamr;

    move-result-object v3

    invoke-virtual {v3}, Lamr;->a()Lamq;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 505
    invoke-virtual {v1}, Lamr;->b()V

    .line 508
    const/16 v2, 0x1f9

    const/16 v3, 0x1f9

    invoke-virtual {v1, v3}, Lamr;->a(I)Lamr;

    move-result-object v3

    const-string v4, "life_w_acci"

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v3

    const v4, 0x7f0200e0

    invoke-virtual {v3, v4}, Lamr;->b(I)Lamr;

    move-result-object v3

    const v4, 0x7f0200d8

    invoke-virtual {v3, v4}, Lamr;->c(I)Lamr;

    move-result-object v3

    invoke-virtual {v3, v5}, Lamr;->j(I)Lamr;

    move-result-object v3

    const v4, 0x7f020119

    invoke-virtual {v3, v4}, Lamr;->i(I)Lamr;

    move-result-object v3

    const v4, 0x7f02014b

    invoke-virtual {v3, v4}, Lamr;->g(I)Lamr;

    move-result-object v3

    const v4, 0x7f02013b

    invoke-virtual {v3, v4}, Lamr;->h(I)Lamr;

    move-result-object v3

    const v4, 0x7f070002

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070003

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070004

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f0a029a

    invoke-virtual {v3, v4}, Lamr;->d(I)Lamr;

    move-result-object v3

    invoke-virtual {v3}, Lamr;->a()Lamq;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 521
    invoke-virtual {v1}, Lamr;->b()V

    .line 524
    const/16 v2, 0x1fa

    const/16 v3, 0x1fa

    invoke-virtual {v1, v3}, Lamr;->a(I)Lamr;

    move-result-object v3

    const-string v4, "life_dryskin"

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v3

    const v4, 0x7f0200fb

    invoke-virtual {v3, v4}, Lamr;->b(I)Lamr;

    move-result-object v3

    const v4, 0x7f0200db

    invoke-virtual {v3, v4}, Lamr;->c(I)Lamr;

    move-result-object v3

    invoke-virtual {v3, v5}, Lamr;->j(I)Lamr;

    move-result-object v3

    const v4, 0x7f020122

    invoke-virtual {v3, v4}, Lamr;->i(I)Lamr;

    move-result-object v3

    const v4, 0x7f02014e

    invoke-virtual {v3, v4}, Lamr;->g(I)Lamr;

    move-result-object v3

    const v4, 0x7f020144

    invoke-virtual {v3, v4}, Lamr;->h(I)Lamr;

    move-result-object v3

    const v4, 0x7f070010

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070011

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070012

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f0a0295

    invoke-virtual {v3, v4}, Lamr;->d(I)Lamr;

    move-result-object v3

    invoke-virtual {v3}, Lamr;->a()Lamq;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 537
    invoke-virtual {v1}, Lamr;->b()V

    .line 540
    const/16 v2, 0x1fb

    const/16 v3, 0x1fb

    invoke-virtual {v1, v3}, Lamr;->a(I)Lamr;

    move-result-object v3

    const-string v4, "life_hum"

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v3

    const v4, 0x7f0200fc

    invoke-virtual {v3, v4}, Lamr;->b(I)Lamr;

    move-result-object v3

    const v4, 0x7f0200dc

    invoke-virtual {v3, v4}, Lamr;->c(I)Lamr;

    move-result-object v3

    invoke-virtual {v3, v5}, Lamr;->j(I)Lamr;

    move-result-object v3

    const v4, 0x7f020124

    invoke-virtual {v3, v4}, Lamr;->i(I)Lamr;

    move-result-object v3

    const v4, 0x7f02014f

    invoke-virtual {v3, v4}, Lamr;->g(I)Lamr;

    move-result-object v3

    const v4, 0x7f020146

    invoke-virtual {v3, v4}, Lamr;->h(I)Lamr;

    move-result-object v3

    const v4, 0x7f070009

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f07000a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f07000b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f0a02a1

    invoke-virtual {v3, v4}, Lamr;->d(I)Lamr;

    move-result-object v3

    invoke-virtual {v3}, Lamr;->a()Lamq;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 553
    invoke-virtual {v1}, Lamr;->b()V

    .line 556
    const/16 v2, 0x1fc

    const/16 v3, 0x1fc

    invoke-virtual {v1, v3}, Lamr;->a(I)Lamr;

    move-result-object v3

    const-string v4, "life_umb"

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/String;)Lamr;

    move-result-object v3

    const v4, 0x7f0200fd

    invoke-virtual {v3, v4}, Lamr;->b(I)Lamr;

    move-result-object v3

    const v4, 0x7f0200dd

    invoke-virtual {v3, v4}, Lamr;->c(I)Lamr;

    move-result-object v3

    invoke-virtual {v3, v5}, Lamr;->j(I)Lamr;

    move-result-object v3

    const v4, 0x7f020125

    invoke-virtual {v3, v4}, Lamr;->i(I)Lamr;

    move-result-object v3

    const v4, 0x7f020150

    invoke-virtual {v3, v4}, Lamr;->g(I)Lamr;

    move-result-object v3

    const v4, 0x7f020147

    invoke-virtual {v3, v4}, Lamr;->h(I)Lamr;

    move-result-object v3

    const v4, 0x7f070013

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070014

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f070015

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lamr;->a(Ljava/lang/Integer;)Lamr;

    move-result-object v3

    const v4, 0x7f0a02a4

    invoke-virtual {v3, v4}, Lamr;->d(I)Lamr;

    move-result-object v3

    invoke-virtual {v3}, Lamr;->a()Lamq;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 569
    invoke-virtual {v1}, Lamr;->b()V

    goto/16 :goto_0
.end method
