.class public Lacf;
.super Ljava/lang/Object;

# interfaces
.implements Lca;


# instance fields
.field private final a:Lrz;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcb;Lcc;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lrz;

    invoke-direct {v0, p1, p2, p3}, Lrz;-><init>(Landroid/content/Context;Lcb;Lcc;)V

    iput-object v0, p0, Lacf;->a:Lrz;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lacf;->a:Lrz;

    invoke-virtual {v0}, Lrz;->a()V

    return-void
.end method

.method public a(Lacg;Landroid/net/Uri;)V
    .locals 2

    iget-object v0, p0, Lacf;->a:Lrz;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lrz;->a(Lacg;Landroid/net/Uri;Z)V

    return-void
.end method

.method public a(Lcb;)V
    .locals 1

    iget-object v0, p0, Lacf;->a:Lrz;

    invoke-virtual {v0, p1}, Lrz;->a(Lcb;)V

    return-void
.end method

.method public a(Lcc;)V
    .locals 1

    iget-object v0, p0, Lacf;->a:Lrz;

    invoke-virtual {v0, p1}, Lrz;->a(Lcc;)V

    return-void
.end method

.method public b(Lacg;Landroid/net/Uri;)V
    .locals 2

    iget-object v0, p0, Lacf;->a:Lrz;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lrz;->a(Lacg;Landroid/net/Uri;Z)V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lacf;->a:Lrz;

    invoke-virtual {v0}, Lrz;->b()Z

    move-result v0

    return v0
.end method

.method public b(Lcb;)Z
    .locals 1

    iget-object v0, p0, Lacf;->a:Lrz;

    invoke-virtual {v0, p1}, Lrz;->b(Lcb;)Z

    move-result v0

    return v0
.end method

.method public b(Lcc;)Z
    .locals 1

    iget-object v0, p0, Lacf;->a:Lrz;

    invoke-virtual {v0, p1}, Lrz;->b(Lcc;)Z

    move-result v0

    return v0
.end method

.method public c(Lcb;)V
    .locals 1

    iget-object v0, p0, Lacf;->a:Lrz;

    invoke-virtual {v0, p1}, Lrz;->c(Lcb;)V

    return-void
.end method

.method public c(Lcc;)V
    .locals 1

    iget-object v0, p0, Lacf;->a:Lrz;

    invoke-virtual {v0, p1}, Lrz;->c(Lcc;)V

    return-void
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lacf;->a:Lrz;

    invoke-virtual {v0}, Lrz;->c()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lacf;->a:Lrz;

    invoke-virtual {v0}, Lrz;->d()V

    return-void
.end method
