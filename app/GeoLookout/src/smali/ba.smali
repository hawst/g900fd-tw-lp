.class public final Lba;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lhp;

.field private final b:Landroid/content/Context;

.field private c:Lav;

.field private d:Lgg;

.field private e:Ljava/lang/String;

.field private f:Lbb;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lhp;

    invoke-direct {v0}, Lhp;-><init>()V

    iput-object v0, p0, Lba;->a:Lhp;

    iput-object p1, p0, Lba;->b:Landroid/content/Context;

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lba;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lba;->c(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lba;->b:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/internal/x;

    invoke-direct {v1}, Lcom/google/android/gms/internal/x;-><init>()V

    iget-object v2, p0, Lba;->e:Ljava/lang/String;

    iget-object v3, p0, Lba;->a:Lhp;

    invoke-static {v0, v1, v2, v3}, Luy;->a(Landroid/content/Context;Lcom/google/android/gms/internal/x;Ljava/lang/String;Lhp;)Lgg;

    move-result-object v0

    iput-object v0, p0, Lba;->d:Lgg;

    iget-object v0, p0, Lba;->c:Lav;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lba;->d:Lgg;

    new-instance v1, Lux;

    iget-object v2, p0, Lba;->c:Lav;

    invoke-direct {v1, v2}, Lux;-><init>(Lav;)V

    invoke-interface {v0, v1}, Lgg;->a(Lgd;)V

    :cond_1
    iget-object v0, p0, Lba;->f:Lbb;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lba;->d:Lgg;

    new-instance v1, Lvb;

    iget-object v2, p0, Lba;->f:Lbb;

    invoke-direct {v1, v2}, Lvb;-><init>(Lbb;)V

    invoke-interface {v0, v1}, Lgg;->a(Lgm;)V

    :cond_2
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lba;->d:Lgg;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The ad unit ID must be set on InterstitialAd before "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is called."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lav;
    .locals 1

    iget-object v0, p0, Lba;->c:Lav;

    return-object v0
.end method

.method public a(Lav;)V
    .locals 2

    :try_start_0
    iput-object p1, p0, Lba;->c:Lav;

    iget-object v0, p0, Lba;->d:Lgg;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lba;->d:Lgg;

    if-eqz p1, :cond_1

    new-instance v0, Lux;

    invoke-direct {v0, p1}, Lux;-><init>(Lav;)V

    :goto_0
    invoke-interface {v1, v0}, Lgg;->a(Lgd;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Failed to set the AdListener."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public a(Law;)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lba;->d:Lgg;

    if-nez v0, :cond_0

    const-string v0, "loadAd"

    invoke-direct {p0, v0}, Lba;->b(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lba;->d:Lgg;

    new-instance v1, Lcom/google/android/gms/internal/v;

    iget-object v2, p0, Lba;->b:Landroid/content/Context;

    invoke-direct {v1, v2, p1}, Lcom/google/android/gms/internal/v;-><init>(Landroid/content/Context;Law;)V

    invoke-interface {v0, v1}, Lgg;->a(Lcom/google/android/gms/internal/v;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lba;->a:Lhp;

    invoke-virtual {p1}, Law;->d()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhp;->a(Ljava/util/Map;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to load ad."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lba;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The ad unit ID can only be set once on InterstitialAd."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lba;->e:Ljava/lang/String;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lba;->e:Ljava/lang/String;

    return-object v0
.end method

.method public c()Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lba;->d:Lgg;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lba;->d:Lgg;

    invoke-interface {v1}, Lgg;->c()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "Failed to check if ad is ready."

    invoke-static {v2, v1}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public d()V
    .locals 2

    :try_start_0
    const-string v0, "show"

    invoke-direct {p0, v0}, Lba;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lba;->d:Lgg;

    invoke-interface {v0}, Lgg;->f()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to show interstitial."

    invoke-static {v1, v0}, Llm;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
