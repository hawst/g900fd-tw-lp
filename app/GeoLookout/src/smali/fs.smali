.class public final Lfs;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lfx;

.field private final b:Lfw;

.field private final c:Lfp;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:[Ljava/lang/String;

.field private final g:Landroid/os/Bundle;

.field private final h:Z


# direct methods
.method private constructor <init>(Lfu;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lfu;->a:Lfx;

    iput-object v0, p0, Lfs;->a:Lfx;

    iget-object v0, p1, Lfu;->b:Lfw;

    iput-object v0, p0, Lfs;->b:Lfw;

    iget-object v0, p1, Lfu;->c:Lfp;

    iput-object v0, p0, Lfs;->c:Lfp;

    iget-object v0, p1, Lfu;->d:Ljava/lang/String;

    iput-object v0, p0, Lfs;->d:Ljava/lang/String;

    iget v0, p1, Lfu;->e:I

    iput v0, p0, Lfs;->e:I

    iget-object v0, p1, Lfu;->g:Landroid/os/Bundle;

    iput-object v0, p0, Lfs;->g:Landroid/os/Bundle;

    iget-boolean v0, p1, Lfu;->h:Z

    iput-boolean v0, p0, Lfs;->h:Z

    iget-object v0, p1, Lfu;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p1, Lfu;->f:Ljava/util/ArrayList;

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lfs;->f:[Ljava/lang/String;

    iget-object v0, p0, Lfs;->c:Lfp;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lfs;->h:Z

    const-string v1, "Must either enable sockets OR specify a message listener"

    invoke-static {v0, v1}, Lnx;->a(ZLjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Lfu;Lft;)V
    .locals 0

    invoke-direct {p0, p1}, Lfs;-><init>(Lfu;)V

    return-void
.end method

.method public static a(IIJ)Landroid/os/Bundle;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "min_automatch_players"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "max_automatch_players"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "exclusive_bit_mask"

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-object v0
.end method

.method public static a(Lfx;)Lfu;
    .locals 2

    new-instance v0, Lfu;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lfu;-><init>(Lfx;Lft;)V

    return-object v0
.end method


# virtual methods
.method public a()Lfx;
    .locals 1

    iget-object v0, p0, Lfs;->a:Lfx;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfs;->d:Ljava/lang/String;

    return-object v0
.end method

.method public c()Lfw;
    .locals 1

    iget-object v0, p0, Lfs;->b:Lfw;

    return-object v0
.end method

.method public d()Lfp;
    .locals 1

    iget-object v0, p0, Lfs;->c:Lfp;

    return-object v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lfs;->e:I

    return v0
.end method

.method public f()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lfs;->f:[Ljava/lang/String;

    return-object v0
.end method

.method public g()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lfs;->g:Landroid/os/Bundle;

    return-object v0
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, Lfs;->h:Z

    return v0
.end method
