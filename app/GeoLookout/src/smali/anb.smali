.class public Lanb;
.super Ljava/lang/Object;
.source "DisasterLocation.java"


# static fields
.field private static a:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/high16 v0, 0x447a0000    # 1000.0f

    sput v0, Lanb;->a:F

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Lamf;
    .locals 8

    .prologue
    .line 16
    const-string v0, "pref_spp_latitude"

    sget v1, Lanb;->a:F

    invoke-static {p0, v0, v1}, Lall;->a(Landroid/content/Context;Ljava/lang/String;F)F

    move-result v1

    .line 17
    const-string v0, "pref_spp_longitude"

    sget v2, Lanb;->a:F

    invoke-static {p0, v0, v2}, Lall;->a(Landroid/content/Context;Ljava/lang/String;F)F

    move-result v2

    .line 19
    sget v0, Lanb;->a:F

    cmpl-float v0, v1, v0

    if-eqz v0, :cond_0

    sget v0, Lanb;->a:F

    cmpl-float v0, v2, v0

    if-nez v0, :cond_1

    .line 20
    :cond_0
    const/4 v0, 0x0

    .line 26
    :goto_0
    return-object v0

    .line 23
    :cond_1
    new-instance v0, Lamf;

    float-to-double v4, v1

    float-to-double v6, v2

    invoke-direct {v0, v4, v5, v6, v7}, Lamf;-><init>(DD)V

    .line 24
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getLastPositionUpdatedToSPP : ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lamf;)V
    .locals 4

    .prologue
    .line 30
    iget-wide v0, p1, Lamf;->a:D

    double-to-float v0, v0

    .line 31
    iget-wide v2, p1, Lamf;->b:D

    double-to-float v1, v2

    .line 33
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setLastPositionUpdatedToSPP : ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 35
    const-string v2, "pref_spp_latitude"

    invoke-static {p0, v2, v0}, Lall;->b(Landroid/content/Context;Ljava/lang/String;F)V

    .line 36
    const-string v0, "pref_spp_longitude"

    invoke-static {p0, v0, v1}, Lall;->b(Landroid/content/Context;Ljava/lang/String;F)V

    .line 37
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 40
    const-string v0, "resetLastPositionUpdatedToSPP() "

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 42
    const-string v0, "pref_spp_latitude"

    sget v1, Lanb;->a:F

    invoke-static {p0, v0, v1}, Lall;->b(Landroid/content/Context;Ljava/lang/String;F)V

    .line 43
    const-string v0, "pref_spp_longitude"

    sget v1, Lanb;->a:F

    invoke-static {p0, v0, v1}, Lall;->b(Landroid/content/Context;Ljava/lang/String;F)V

    .line 44
    return-void
.end method

.method public static b(Landroid/content/Context;Lamf;)V
    .locals 4

    .prologue
    .line 61
    iget-wide v0, p1, Lamf;->a:D

    double-to-float v0, v0

    .line 62
    iget-wide v2, p1, Lamf;->b:D

    double-to-float v1, v2

    .line 64
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setLastPositionFromLocationManager : ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 66
    const-string v2, "latitude"

    invoke-static {p0, v2, v0}, Lall;->b(Landroid/content/Context;Ljava/lang/String;F)V

    .line 67
    const-string v0, "longitude"

    invoke-static {p0, v0, v1}, Lall;->b(Landroid/content/Context;Ljava/lang/String;F)V

    .line 68
    return-void
.end method

.method public static c(Landroid/content/Context;)Lamf;
    .locals 8

    .prologue
    .line 47
    const-string v0, "latitude"

    sget v1, Lanb;->a:F

    invoke-static {p0, v0, v1}, Lall;->a(Landroid/content/Context;Ljava/lang/String;F)F

    move-result v1

    .line 48
    const-string v0, "longitude"

    sget v2, Lanb;->a:F

    invoke-static {p0, v0, v2}, Lall;->a(Landroid/content/Context;Ljava/lang/String;F)F

    move-result v2

    .line 50
    sget v0, Lanb;->a:F

    cmpl-float v0, v1, v0

    if-eqz v0, :cond_0

    sget v0, Lanb;->a:F

    cmpl-float v0, v2, v0

    if-nez v0, :cond_1

    .line 51
    :cond_0
    const/4 v0, 0x0

    .line 57
    :goto_0
    return-object v0

    .line 54
    :cond_1
    new-instance v0, Lamf;

    float-to-double v4, v1

    float-to-double v6, v2

    invoke-direct {v0, v4, v5, v6, v7}, Lamf;-><init>(DD)V

    .line 55
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getLastPositionFromLocationManager : ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
