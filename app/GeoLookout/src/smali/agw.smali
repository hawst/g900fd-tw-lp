.class public Lagw;
.super Ljava/lang/Object;
.source "NavigationDrawer.java"


# static fields
.field private static final b:I = 0x0

.field private static final c:I = 0x1


# instance fields
.field a:Landroid/app/Activity;

.field private d:Landroid/view/View;

.field private e:Landroid/support/v4/widget/DrawerLayout;

.field private f:Ljava/lang/CharSequence;

.field private g:Landroid/widget/ListView;

.field private h:Landroid/support/v4/app/ActionBarDrawerToggle;

.field private i:I

.field private j:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lagw;->a:Landroid/app/Activity;

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lagw;->d:Landroid/view/View;

    .line 41
    invoke-virtual {p0}, Lagw;->a()V

    .line 42
    invoke-direct {p0}, Lagw;->e()V

    .line 43
    return-void
.end method

.method private a(I)V
    .locals 6

    .prologue
    const v5, 0x10008000

    const/4 v4, 0x1

    .line 146
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 147
    iget-object v1, p0, Lagw;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 149
    packed-switch p1, :pswitch_data_0

    .line 174
    :cond_0
    :goto_0
    iget-object v0, p0, Lagw;->g:Landroid/widget/ListView;

    invoke-virtual {v0, p1, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 178
    iget-object v0, p0, Lagw;->e:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lagw;->g:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->closeDrawer(Landroid/view/View;)V

    .line 179
    return-void

    .line 151
    :pswitch_0
    iget-object v2, p0, Lagw;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00a9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lagw;->i:I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lagw;->a:Landroid/app/Activity;

    invoke-static {v1}, Lage;->z(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    iget-object v1, p0, Lagw;->a:Landroid/app/Activity;

    const-class v2, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 156
    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 157
    iget-object v1, p0, Lagw;->a:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 161
    :pswitch_1
    iget-object v2, p0, Lagw;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0020

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lagw;->i:I

    if-eq v1, v4, :cond_0

    iget-object v1, p0, Lagw;->a:Landroid/app/Activity;

    invoke-static {v1}, Lage;->z(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    iget-object v1, p0, Lagw;->a:Landroid/app/Activity;

    const-class v2, Lcom/sec/android/GeoLookout/activity/DisasterLifeMainActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 166
    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 167
    iget-object v1, p0, Lagw;->a:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 149
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lagw;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lagw;->c()V

    return-void
.end method

.method static synthetic a(Lagw;I)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lagw;->a(I)V

    return-void
.end method

.method static synthetic b(Lagw;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lagw;->d()V

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lagw;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 93
    iget-object v0, p0, Lagw;->d:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 94
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lagw;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 98
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 125
    iget-object v0, p0, Lagw;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lagw;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0020

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    const/4 v0, 0x1

    iput v0, p0, Lagw;->i:I

    .line 130
    :goto_0
    return-void

    .line 128
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lagw;->i:I

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v3, -0x1

    .line 47
    iget-object v0, p0, Lagw;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lagw;->a:Landroid/app/Activity;

    iget-object v1, p0, Lagw;->d:Landroid/view/View;

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 89
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lagw;->a:Landroid/app/Activity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 52
    const v1, 0x7f030015

    invoke-virtual {v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lagw;->d:Landroid/view/View;

    .line 53
    iget-object v0, p0, Lagw;->a:Landroid/app/Activity;

    iget-object v1, p0, Lagw;->d:Landroid/view/View;

    new-instance v2, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 55
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lagw;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00a9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v1, p0, Lagw;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0020

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    iput-object v0, p0, Lagw;->j:[Ljava/lang/String;

    .line 59
    iget-object v0, p0, Lagw;->a:Landroid/app/Activity;

    const v1, 0x7f0e0070

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, Lagw;->e:Landroid/support/v4/widget/DrawerLayout;

    .line 60
    iget-object v0, p0, Lagw;->a:Landroid/app/Activity;

    const v1, 0x7f0e0072

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lagw;->g:Landroid/widget/ListView;

    .line 61
    iget-object v0, p0, Lagw;->g:Landroid/widget/ListView;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lagw;->a:Landroid/app/Activity;

    const v3, 0x7f030014

    iget-object v4, p0, Lagw;->j:[Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 63
    iget-object v0, p0, Lagw;->g:Landroid/widget/ListView;

    new-instance v1, Lagy;

    invoke-direct {v1, p0, v6}, Lagy;-><init>(Lagw;Lagx;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 65
    iget-object v0, p0, Lagw;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 66
    iget-object v0, p0, Lagw;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 68
    new-instance v0, Lagx;

    iget-object v2, p0, Lagw;->a:Landroid/app/Activity;

    iget-object v3, p0, Lagw;->e:Landroid/support/v4/widget/DrawerLayout;

    const v4, 0x7f020171

    const v5, 0x7f0a00a0

    const v6, 0x7f0a009f

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lagx;-><init>(Lagw;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;III)V

    iput-object v0, p0, Lagw;->h:Landroid/support/v4/app/ActionBarDrawerToggle;

    .line 83
    iget-object v0, p0, Lagw;->d:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 84
    iget-object v0, p0, Lagw;->e:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lagw;->h:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 86
    iget-object v0, p0, Lagw;->e:Landroid/support/v4/widget/DrawerLayout;

    const v1, 0x7f02016e

    const v2, 0x800003

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/DrawerLayout;->setDrawerShadow(II)V

    goto/16 :goto_0
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lagw;->h:Landroid/support/v4/app/ActionBarDrawerToggle;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lagw;->h:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 110
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 133
    iput-object p1, p0, Lagw;->f:Ljava/lang/CharSequence;

    .line 134
    iget-object v0, p0, Lagw;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iget-object v1, p0, Lagw;->f:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 135
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 113
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lagw;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    iget-object v1, p0, Lagw;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 117
    :cond_0
    iget-object v1, p0, Lagw;->h:Landroid/support/v4/app/ActionBarDrawerToggle;

    if-eqz v1, :cond_1

    .line 118
    iget-object v0, p0, Lagw;->h:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/ActionBarDrawerToggle;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 120
    :cond_1
    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lagw;->h:Landroid/support/v4/app/ActionBarDrawerToggle;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lagw;->h:Landroid/support/v4/app/ActionBarDrawerToggle;

    invoke-virtual {v0}, Landroid/support/v4/app/ActionBarDrawerToggle;->syncState()V

    .line 104
    :cond_0
    return-void
.end method
