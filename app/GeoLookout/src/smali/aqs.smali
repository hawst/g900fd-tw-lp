.class public Laqs;
.super Ljava/lang/Object;
.source "SafetyCareWidgetUpdater.java"


# static fields
.field public static a:I

.field private static volatile b:Laqs;


# instance fields
.field private c:Laqr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    sput-object v0, Laqs;->b:Laqs;

    .line 39
    const/4 v0, 0x0

    sput v0, Laqs;->a:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method private static a(I)I
    .locals 2

    .prologue
    .line 661
    sget-object v0, Lakx;->eQ:Landroid/util/SparseIntArray;

    const v1, 0x7f020193

    invoke-virtual {v0, p0, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    return v0
.end method

.method private a(Landroid/content/Context;IZ)Landroid/widget/RemoteViews;
    .locals 4

    .prologue
    .line 246
    const-string v0, "updateViewStatusSimAbsent"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 248
    if-eqz p3, :cond_0

    .line 249
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030010

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 250
    const-string v1, "Widget layout id : 2130903056 disaster_widget_status_off_transparent"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 256
    :goto_0
    const v1, 0x7f0e0051

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00b7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 258
    return-object v0

    .line 252
    :cond_0
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f03000f

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 253
    const-string v1, "Widget layout id : 2130903055 disaster_widget_status_off"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Z)Landroid/widget/RemoteViews;
    .locals 4

    .prologue
    .line 285
    const-string v0, "updateViewStatusDisasterDisabled()"

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 288
    invoke-static {}, Lalk;->a()Lalk;

    move-result-object v0

    invoke-virtual {v0, p1}, Lalk;->b(Landroid/content/Context;)V

    .line 293
    if-eqz p2, :cond_0

    .line 294
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030010

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 295
    const-string v1, "Widget layout id : 2130903056 disaster_widget_status_off_transparent"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 301
    :goto_0
    const v1, 0x7f0e0052

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 302
    const v1, 0x7f0e0051

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0564

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 304
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/GeoLookout/widget/SafetyCareConnectAgreeDialog;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 305
    const v2, 0x10008000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 307
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    long-to-int v2, v2

    .line 309
    const/4 v3, 0x0

    invoke-static {p1, v2, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 310
    const v2, 0x7f0e004f

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 312
    return-object v0

    .line 297
    :cond_0
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f03000f

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 298
    const-string v1, "Widget layout id : 2130903055 disaster_widget_status_off"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a()Laqs;
    .locals 2

    .prologue
    .line 45
    sget-object v0, Laqs;->b:Laqs;

    if-nez v0, :cond_1

    .line 46
    const-class v1, Laqs;

    monitor-enter v1

    .line 47
    :try_start_0
    sget-object v0, Laqs;->b:Laqs;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Laqs;

    invoke-direct {v0}, Laqs;-><init>()V

    sput-object v0, Laqs;->b:Laqs;

    .line 50
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    :cond_1
    sget-object v0, Laqs;->b:Laqs;

    return-object v0

    .line 50
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 2

    .prologue
    .line 172
    const-string v0, "removeRefreshAnimation"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 174
    const v0, 0x7f0e0063

    const/16 v1, 0x8

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 175
    const v0, 0x7f0e0062

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 177
    invoke-direct {p0, p1, p2}, Laqs;->b(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 178
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/widget/RemoteViews;Laqr;)V
    .locals 4

    .prologue
    const v3, 0x7f0e0054

    .line 664
    const-string v0, "getImgByDisaster"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 665
    iget v0, p2, Laqr;->c:I

    .line 666
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 667
    const-string v0, "setBackgroundResource"

    const v1, 0x7f0200c0

    invoke-virtual {p1, v3, v0, v1}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 671
    :goto_0
    iget v0, p2, Laqr;->b:I

    .line 672
    sget-object v1, Lakx;->eT:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 673
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getImgByDisaster"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 674
    invoke-virtual {p1, v3, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 675
    return-void

    .line 669
    :cond_0
    const-string v0, "setBackgroundResource"

    const v1, 0x7f0200c2

    invoke-virtual {p1, v3, v0, v1}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x1

    const v8, 0x7f0e005c

    const v7, 0x7f0e005d

    const/4 v6, 0x0

    .line 522
    const-string v0, "03"

    iget-object v1, p0, Laqs;->c:Laqr;

    iget-object v1, v1, Laqr;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 523
    const v0, 0x7f0e005b

    iget-object v1, p0, Laqs;->c:Laqr;

    iget v1, v1, Laqr;->b:I

    iget-object v2, p0, Laqs;->c:Laqr;

    iget v2, v2, Laqr;->c:I

    invoke-static {p1, v1, v2}, Lall;->b(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 528
    :goto_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 530
    const-string v1, ""

    iget-object v2, p0, Laqs;->c:Laqr;

    iget-object v2, v2, Laqr;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 531
    const-string v1, ""

    iget-object v2, p0, Laqs;->c:Laqr;

    iget-object v2, v2, Laqr;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 532
    const-string v1, "Loc.("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 533
    iget-object v1, p0, Laqs;->c:Laqr;

    iget-wide v2, v1, Laqr;->m:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    .line 534
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 535
    iget-object v1, p0, Laqs;->c:Laqr;

    iget-wide v2, v1, Laqr;->n:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    .line 536
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 560
    :goto_1
    invoke-virtual {p2, v7, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 562
    iget-object v1, p0, Laqs;->c:Laqr;

    iget v1, v1, Laqr;->b:I

    const/16 v2, 0x69

    if-ne v1, v2, :cond_9

    .line 563
    const-string v1, "03"

    iget-object v2, p0, Laqs;->c:Laqr;

    iget-object v2, v2, Laqr;->t:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 564
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 565
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 567
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00b3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 568
    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 569
    iget-object v2, p0, Laqs;->c:Laqr;

    iget-object v2, v2, Laqr;->p:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 570
    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 571
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00bb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Laqs;->c:Laqr;

    iget-object v4, v4, Laqr;->q:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 573
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00bd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 574
    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 575
    iget-object v2, p0, Laqs;->c:Laqr;

    iget-object v2, v2, Laqr;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 576
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 577
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0562

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 578
    const-string v2, " %.1f"

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Laqs;->c:Laqr;

    iget-wide v4, v4, Laqr;->j:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 579
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 580
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00ba

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Laqs;->c:Laqr;

    iget-object v4, v4, Laqr;->r:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 582
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v8, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 583
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v7, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 609
    :goto_2
    const-string v0, "03"

    iget-object v1, p0, Laqs;->c:Laqr;

    iget-object v1, v1, Laqr;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Laqs;->c:Laqr;

    iget v0, v0, Laqr;->b:I

    const/16 v1, 0x6e

    if-ne v0, v1, :cond_d

    .line 610
    invoke-virtual {p2, v8, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 614
    :goto_3
    iget-object v0, p0, Laqs;->c:Laqr;

    iget-object v0, v0, Laqr;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 615
    const v0, 0x7f0e0060

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0044

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Laqs;->c:Laqr;

    iget-object v2, v2, Laqr;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 618
    :cond_0
    if-eqz p3, :cond_e

    .line 619
    const v0, 0x7f0e004f

    const-string v1, "setBackgroundResource"

    const v2, 0x7f0201b9

    invoke-virtual {p2, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 623
    :goto_4
    const-string v0, "03"

    iget-object v1, p0, Laqs;->c:Laqr;

    iget-object v1, v1, Laqr;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 624
    iget-object v0, p0, Laqs;->c:Laqr;

    iget v0, v0, Laqr;->b:I

    const/16 v1, 0x69

    if-ne v0, v1, :cond_f

    .line 625
    invoke-virtual {p2, v7, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 631
    :cond_1
    :goto_5
    iget-object v0, p0, Laqs;->c:Laqr;

    iget v0, v0, Laqr;->c:I

    if-ne v0, v9, :cond_11

    if-nez p3, :cond_11

    .line 632
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "isBink = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Laqs;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 633
    sget v0, Laqs;->a:I

    if-eqz v0, :cond_10

    .line 634
    const v0, 0x7f0e0055

    invoke-virtual {p2, v0, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 635
    const v0, 0x7f0e0054

    const v1, 0x7f0200be

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 644
    :goto_6
    return-void

    .line 525
    :cond_2
    const v0, 0x7f0e005b

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Laqs;->c:Laqr;

    iget v2, v2, Laqr;->b:I

    invoke-static {v2}, Lall;->c(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 538
    :cond_3
    iget-object v1, p0, Laqs;->c:Laqr;

    iget-object v1, v1, Laqr;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 541
    :cond_4
    const-string v1, ""

    iget-object v2, p0, Laqs;->c:Laqr;

    iget-object v2, v2, Laqr;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 542
    iget-object v1, p0, Laqs;->c:Laqr;

    iget-object v1, v1, Laqr;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 544
    :cond_5
    const-string v1, "04"

    iget-object v2, p0, Laqs;->c:Laqr;

    iget-object v2, v2, Laqr;->t:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 545
    iget-object v1, p0, Laqs;->c:Laqr;

    iget-object v1, v1, Laqr;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 546
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 547
    iget-object v1, p0, Laqs;->c:Laqr;

    iget-object v1, v1, Laqr;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 548
    :cond_6
    const-string v1, "03"

    iget-object v2, p0, Laqs;->c:Laqr;

    iget-object v2, v2, Laqr;->t:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 549
    iget-object v1, p0, Laqs;->c:Laqr;

    iget-object v1, v1, Laqr;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 550
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 551
    iget-object v1, p0, Laqs;->c:Laqr;

    iget-object v1, v1, Laqr;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 553
    :cond_7
    iget-object v1, p0, Laqs;->c:Laqr;

    iget-object v1, v1, Laqr;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 554
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 555
    iget-object v1, p0, Laqs;->c:Laqr;

    iget-object v1, v1, Laqr;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    .line 585
    :cond_8
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 586
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0562

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 587
    const-string v2, " %.1f"

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Laqs;->c:Laqr;

    iget-wide v4, v4, Laqr;->j:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 588
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 589
    iget-object v2, p0, Laqs;->c:Laqr;

    iget v2, v2, Laqr;->f:I

    invoke-static {p1, v2}, Lall;->e(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 590
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v8, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 591
    invoke-virtual {p2, v7, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 593
    :cond_9
    iget-object v1, p0, Laqs;->c:Laqr;

    iget v1, v1, Laqr;->b:I

    const/16 v2, 0x65

    if-ne v1, v2, :cond_a

    .line 594
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 595
    iget-object v2, p0, Laqs;->c:Laqr;

    iget v2, v2, Laqr;->f:I

    invoke-static {p1, v2}, Lall;->e(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 597
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v8, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 598
    invoke-virtual {p2, v7, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 599
    :cond_a
    iget-object v1, p0, Laqs;->c:Laqr;

    iget v1, v1, Laqr;->b:I

    const/16 v2, 0x70

    if-eq v1, v2, :cond_b

    iget-object v1, p0, Laqs;->c:Laqr;

    iget v1, v1, Laqr;->b:I

    const/16 v2, 0x6e

    if-ne v1, v2, :cond_c

    .line 600
    :cond_b
    iget-object v1, p0, Laqs;->c:Laqr;

    iget-object v1, v1, Laqr;->o:Ljava/lang/String;

    invoke-static {p1, v1}, Lall;->i(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 601
    invoke-virtual {p2, v8, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 602
    invoke-virtual {p2, v7, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 604
    :cond_c
    invoke-virtual {p2, v8, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 605
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0040

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Laqs;->c:Laqr;

    iget-object v1, v1, Laqr;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v7, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 612
    :cond_d
    invoke-virtual {p2, v8, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_3

    .line 621
    :cond_e
    const v0, 0x7f0e004f

    const-string v1, "setBackgroundResource"

    iget-object v2, p0, Laqs;->c:Laqr;

    iget v2, v2, Laqr;->b:I

    invoke-static {v2}, Laqs;->a(I)I

    move-result v2

    invoke-virtual {p2, v0, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    goto/16 :goto_4

    .line 627
    :cond_f
    const/4 v0, 0x4

    invoke-virtual {p2, v7, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_5

    .line 637
    :cond_10
    const v0, 0x7f0e0054

    const v1, 0x7f0200a4

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 638
    const v0, 0x7f0e0055

    invoke-virtual {p2, v0, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_6

    .line 641
    :cond_11
    const v0, 0x7f0e0054

    const v1, 0x7f0200c1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 642
    const v0, 0x7f0e0055

    invoke-virtual {p2, v0, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_6
.end method

.method private a(Landroid/widget/RemoteViews;)V
    .locals 2

    .prologue
    .line 165
    const-string v0, "startRefreshAnimation"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 167
    const v0, 0x7f0e0063

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 168
    const v0, 0x7f0e0062

    const/16 v1, 0x8

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 169
    return-void
.end method

.method private b(Landroid/content/Context;IZ)Landroid/widget/RemoteViews;
    .locals 4

    .prologue
    .line 262
    const-string v0, "updateViewStatusOff()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 264
    if-eqz p3, :cond_0

    .line 265
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030010

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 266
    const-string v1, "Widget layout id : 2130903056 disaster_widget_status_off_transparent"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 273
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 274
    const-string v2, "com.sec.android.GeoLookout"

    const-string v3, "com.sec.android.GeoLookout.widget.SafetyCareDisasterDisclaimerActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 275
    const-string v2, "appWidgetId"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 276
    const-string v2, "from"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 277
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    long-to-int v2, v2

    .line 278
    const/4 v3, 0x0

    invoke-static {p1, v2, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 280
    const v2, 0x7f0e004f

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 281
    return-object v0

    .line 268
    :cond_0
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f03000f

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 269
    const-string v1, "Widget layout id : 2130903055 disaster_widget_status_off"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(Landroid/content/Context;Z)Landroid/widget/RemoteViews;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 316
    const-string v0, "updateViewStatusRegistering()"

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 319
    if-eqz p2, :cond_0

    .line 320
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030010

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 321
    const-string v1, "Widget layout id : 2130903056 disaster_widget_status_off_transparent"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 326
    :goto_0
    const v1, 0x7f0e0052

    invoke-virtual {v0, v1, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 327
    const v1, 0x7f0e0051

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0560

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0561

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 329
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.safetywidget.ACTION_SHOW_TOAST_FOR_REGISTER"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 330
    const-string v2, "isRegister"

    invoke-virtual {v1, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 331
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    long-to-int v2, v2

    .line 332
    invoke-static {p1, v2, v1, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 333
    const v2, 0x7f0e004f

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 334
    return-object v0

    .line 323
    :cond_0
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f03000f

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 324
    const-string v1, "Widget layout id : 2130903055 disaster_widget_status_off"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V
    .locals 4

    .prologue
    .line 647
    const-string v0, "EndAnimation"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 649
    new-instance v0, Laqt;

    invoke-direct {v0, p0, p1, p2}, Laqt;-><init>(Laqs;Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 657
    const/4 v1, 0x0

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 658
    return-void
.end method

.method private b(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 4

    .prologue
    .line 233
    const-string v0, "addRefreshIntent"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 238
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.GeoLookout.widget.ACTION_DISASTER_REFRESH_DATA"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 239
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    long-to-int v1, v2

    .line 240
    const/4 v2, 0x0

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 241
    const v1, 0x7f0e0062

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 242
    const v1, 0x7f0e005f

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 243
    return-void
.end method

.method private c(Landroid/content/Context;Z)Landroid/widget/RemoteViews;
    .locals 10

    .prologue
    const v5, 0x7f0e0054

    const v9, 0x7f0e004f

    const v1, 0x7f0e0061

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 338
    const-string v0, "updateViewStatusEnabled()"

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 342
    const-string v0, ""

    iget-object v2, p0, Laqs;->c:Laqr;

    iget-object v2, v2, Laqr;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 344
    if-eqz p2, :cond_2

    .line 345
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f030012

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 347
    const-string v2, "Widget layout id : 2130903058 disaster_widget_status_on_transparent"

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 348
    const-string v2, "setBackgroundResource"

    const v3, 0x7f0201b9

    invoke-virtual {v0, v9, v2, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 350
    const-string v2, "setBackgroundResource"

    const v3, 0x7f0200a8

    invoke-virtual {v0, v5, v2, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 361
    :goto_0
    const v2, 0x7f0e0056

    invoke-virtual {v0, v2, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 362
    const v2, 0x7f0e005a

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 363
    const v2, 0x7f0e0058

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0561

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 365
    sget-boolean v2, Laky;->b:Z

    if-eqz v2, :cond_3

    .line 366
    const v2, 0x7f0e0059

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00c0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 378
    :goto_1
    const v2, 0x7f0200b6

    invoke-virtual {v0, v5, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 380
    const v2, 0x7f0e0056

    invoke-virtual {v0, v2, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 381
    const v2, 0x7f0e005a

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 382
    const v2, 0x7f0e0058

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0561

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 383
    const v2, 0x7f0e0055

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 384
    const v2, 0x7f0e0060

    const/4 v3, 0x4

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 405
    :cond_0
    :goto_2
    const v2, 0x7f0e0052

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 410
    iget-object v2, p0, Laqs;->c:Laqr;

    iget v2, v2, Laqr;->u:I

    if-nez v2, :cond_1

    .line 411
    iget-object v2, p0, Laqs;->c:Laqr;

    const-string v3, "pref_geonews_current_page"

    invoke-static {p1, v3, v7}, Lall;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v3

    iput v3, v2, Laqr;->u:I

    .line 413
    :cond_1
    invoke-virtual {v0, v1, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 414
    const/high16 v2, 0x42b40000    # 90.0f

    const/high16 v3, 0x40400000    # 3.0f

    const v4, 0x40a66666    # 5.2f

    const/high16 v5, -0x1000000

    const v6, 0x3f4ccccd    # 0.8f

    invoke-virtual/range {v0 .. v6}, Landroid/widget/RemoteViews;->addOuterShadowTextEffect(IFFFIF)V

    .line 416
    invoke-static {p1}, Lall;->z(Landroid/content/Context;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_7

    .line 417
    const v2, 0x7f0e0057

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 418
    invoke-virtual {v0, v1, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 440
    :goto_3
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/GeoLookout/activity/DisasterDashboardActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 441
    const v2, 0x10008000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 442
    const-string v2, "DID"

    iget-object v3, p0, Laqs;->c:Laqr;

    iget-object v3, v3, Laqr;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 443
    const-string v2, "uid"

    iget-object v3, p0, Laqs;->c:Laqr;

    iget v3, v3, Laqr;->u:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 444
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    long-to-int v2, v2

    .line 445
    invoke-static {p1, v2, v1, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 446
    invoke-virtual {v0, v9, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 449
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v1

    invoke-virtual {v1}, Lage;->b()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    .line 450
    invoke-direct {p0, v0}, Laqs;->a(Landroid/widget/RemoteViews;)V

    .line 459
    :goto_4
    return-object v0

    .line 354
    :cond_2
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f030011

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 356
    const-string v2, "Widget layout id : 2130903057 disaster_widget_status_on"

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 357
    const-string v2, "setBackgroundResource"

    const v3, 0x7f020193

    invoke-virtual {v0, v9, v2, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    goto/16 :goto_0

    .line 368
    :cond_3
    const-string v2, "pref_geonews_current_page"

    invoke-static {p1, v2, v7}, Lall;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v2

    .line 369
    if-eqz v2, :cond_4

    .line 370
    const v2, 0x7f0e0059

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a04f8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 373
    :cond_4
    const v2, 0x7f0e0059

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0565

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 388
    :cond_5
    if-eqz p2, :cond_6

    .line 389
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f030012

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 390
    const-string v2, "setBackgroundResource"

    const v3, 0x7f0201b9

    invoke-virtual {v0, v9, v2, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 391
    const-string v2, "setBackgroundResource"

    const v3, 0x7f0200a8

    invoke-virtual {v0, v5, v2, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 396
    :goto_5
    const v2, 0x7f0e005a

    invoke-virtual {v0, v2, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 397
    const v2, 0x7f0e0056

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 398
    const v2, 0x7f0e0060

    invoke-virtual {v0, v2, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 399
    invoke-direct {p0, p1, v0, p2}, Laqs;->a(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V

    .line 400
    if-eqz p2, :cond_0

    .line 401
    iget-object v2, p0, Laqs;->c:Laqr;

    invoke-static {p1, v0, v2}, Laqs;->a(Landroid/content/Context;Landroid/widget/RemoteViews;Laqr;)V

    goto/16 :goto_2

    .line 393
    :cond_6
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f030011

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    goto :goto_5

    .line 420
    :cond_7
    invoke-virtual {v0, v1, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 421
    iget-object v2, p0, Laqs;->c:Laqr;

    iget v2, v2, Laqr;->u:I

    if-nez v2, :cond_8

    .line 422
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0492

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 428
    :goto_6
    const v1, 0x7f0e0057

    invoke-virtual {v0, v1, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 429
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.GeoLookout.widget.ACTION_GEONEWS_SHOW_NEXT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 430
    const-string v2, "from_transparent"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 431
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    long-to-int v2, v2

    .line 432
    invoke-static {p1, v2, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 434
    const v2, 0x7f0e0057

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto/16 :goto_3

    .line 425
    :cond_8
    iget-object v2, p0, Laqs;->c:Laqr;

    iget v2, v2, Laqr;->u:I

    invoke-static {p1, v2}, Lall;->l(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_6

    .line 452
    :cond_9
    invoke-direct {p0, p1, v0}, Laqs;->a(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    goto/16 :goto_4
.end method

.method private d(Landroid/content/Context;Z)Landroid/widget/RemoteViews;
    .locals 8

    .prologue
    const v2, 0x7f030011

    const/16 v7, 0x8

    const v6, 0x7f0e005c

    const v5, 0x7f0e004f

    const/4 v4, 0x0

    .line 463
    const-string v0, "updateViewStatusDeregistering()"

    invoke-static {v0}, Lalj;->a(Ljava/lang/String;)V

    .line 467
    const-string v0, ""

    iget-object v1, p0, Laqs;->c:Laqr;

    iget-object v1, v1, Laqr;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 468
    if-eqz p2, :cond_1

    .line 469
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030012

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 470
    const-string v1, "Widget layout id : 2130903058 disaster_widget_status_on_transparent"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 476
    :goto_0
    const v1, 0x7f0e005b

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0561

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 478
    sget-boolean v1, Laky;->b:Z

    if-eqz v1, :cond_2

    .line 479
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00c0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 484
    :goto_1
    const v1, 0x7f0e005d

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 485
    if-eqz p2, :cond_3

    .line 486
    const-string v1, "setBackgroundResource"

    const v2, 0x7f0201b9

    invoke-virtual {v0, v5, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 491
    :goto_2
    const v1, 0x7f0e0054

    const v2, 0x7f0200b6

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 492
    const v1, 0x7f0e0055

    invoke-virtual {v0, v1, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 493
    invoke-virtual {v0, v6, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 494
    const v1, 0x7f0e005d

    invoke-virtual {v0, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 510
    :cond_0
    :goto_3
    const v1, 0x7f0e0062

    invoke-virtual {v0, v1, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 511
    const v1, 0x7f0e0052

    invoke-virtual {v0, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 512
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.safetywidget.ACTION_SHOW_TOAST_FOR_REGISTER"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 513
    const-string v2, "isRegister"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 514
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    long-to-int v2, v2

    .line 515
    invoke-static {p1, v2, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 516
    invoke-virtual {v0, v5, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 518
    return-object v0

    .line 472
    :cond_1
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 473
    const-string v1, "Widget layout id : 2130903057 disaster_widget_status_on"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 481
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0565

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_1

    .line 489
    :cond_3
    const-string v1, "setBackgroundResource"

    const v2, 0x7f020193

    invoke-virtual {v0, v5, v1, v2}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    goto :goto_2

    .line 496
    :cond_4
    if-eqz p2, :cond_5

    .line 497
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030012

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 498
    const-string v1, "Widget layout id : 2130903058 disaster_widget_status_on_transparent"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 504
    :goto_4
    invoke-direct {p0, p1, v0, p2}, Laqs;->a(Landroid/content/Context;Landroid/widget/RemoteViews;Z)V

    .line 505
    if-eqz p2, :cond_0

    .line 506
    iget-object v1, p0, Laqs;->c:Laqr;

    invoke-static {p1, v0, v1}, Laqs;->a(Landroid/content/Context;Landroid/widget/RemoteViews;Laqr;)V

    goto :goto_3

    .line 500
    :cond_5
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 501
    const-string v1, "Widget layout id : 2130903057 disaster_widget_status_on"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    goto :goto_4
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 58
    const-string v0, "updateWidget()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 59
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/sec/android/GeoLookout/widget/SafetyCareWidget;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/sec/android/GeoLookout/widget/SafetyCareTransparentWidget;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v3, "pref_geonews_current_page"

    const/4 v4, -0x1

    invoke-static {p1, v3, v4}, Lall;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v3

    .line 65
    invoke-virtual {p2, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v4

    move v0, v1

    .line 66
    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_0

    .line 67
    aget v5, v4, v0

    invoke-virtual {p0, p1, p2, v5, v3}, Laqs;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;II)V

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_0
    invoke-virtual {p2, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 71
    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 72
    aget v2, v0, v1

    invoke-virtual {p0, p1, p2, v2, v3}, Laqs;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;II)V

    .line 71
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 74
    :cond_1
    return-void
.end method

.method public a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 77
    const-string v0, "updateWidget()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 78
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/sec/android/GeoLookout/widget/SafetyCareWidget;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/sec/android/GeoLookout/widget/SafetyCareTransparentWidget;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v3

    move v0, v1

    .line 82
    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_0

    .line 83
    aget v4, v3, v0

    invoke-virtual {p0, p1, p2, v4, p3}, Laqs;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;II)V

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 86
    :cond_0
    invoke-virtual {p2, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 87
    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 88
    aget v2, v0, v1

    invoke-virtual {p0, p1, p2, v2, p3}, Laqs;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;II)V

    .line 87
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 90
    :cond_1
    return-void
.end method

.method public a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;II)V
    .locals 5

    .prologue
    .line 120
    invoke-static {p1, p4}, Laqr;->a(Landroid/content/Context;I)Laqr;

    move-result-object v0

    iput-object v0, p0, Laqs;->c:Laqr;

    .line 122
    if-nez p2, :cond_1

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    invoke-virtual {p2, p3}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v0

    .line 126
    iget-object v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    if-eqz v1, :cond_0

    .line 129
    iget-object v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 130
    const-class v1, Lcom/sec/android/GeoLookout/widget/SafetyCareTransparentWidget;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 131
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateView widget name : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 132
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateView widget name : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 134
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 136
    const/4 v0, 0x0

    .line 137
    sget-boolean v2, Laky;->b:Z

    if-eqz v2, :cond_4

    invoke-static {p1}, Lakw;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 138
    invoke-direct {p0, p1, p3, v1}, Laqs;->a(Landroid/content/Context;IZ)Landroid/widget/RemoteViews;

    move-result-object v0

    .line 153
    :cond_2
    :goto_1
    if-eqz v0, :cond_3

    .line 154
    const-string v1, "update widget!!!"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 155
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Update widget with layout id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/widget/RemoteViews;->getLayoutId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 156
    invoke-virtual {p2, p3, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 158
    :cond_3
    iget-object v0, p0, Laqs;->c:Laqr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqs;->c:Laqr;

    iget-object v0, v0, Laqr;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget v0, Laqs;->a:I

    if-nez v0, :cond_0

    .line 159
    invoke-direct {p0, p1, p2}, Laqs;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    goto/16 :goto_0

    .line 139
    :cond_4
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "safety_care_disaster_user_agree"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_5

    .line 140
    invoke-direct {p0, p1, p3, v1}, Laqs;->b(Landroid/content/Context;IZ)Landroid/widget/RemoteViews;

    move-result-object v0

    goto :goto_1

    .line 142
    :cond_5
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-static {p1}, Lage;->A(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 143
    invoke-direct {p0, p1, v1}, Laqs;->a(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    goto :goto_1

    .line 144
    :cond_6
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_7

    invoke-static {p1}, Lage;->A(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 145
    invoke-direct {p0, p1, v1}, Laqs;->b(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    goto :goto_1

    .line 146
    :cond_7
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-static {p1}, Lage;->A(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 147
    invoke-direct {p0, p1, v1}, Laqs;->c(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    goto :goto_1

    .line 148
    :cond_8
    invoke-static {p1}, Lage;->z(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p1}, Lage;->A(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 149
    invoke-direct {p0, p1, v1}, Laqs;->d(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    goto/16 :goto_1
.end method

.method public a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 181
    const-string v1, "removeRefreshAnimation"

    invoke-static {v1}, Lalj;->b(Ljava/lang/String;)V

    .line 183
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v1

    invoke-virtual {v1, v0}, Lage;->b(Z)V

    .line 184
    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_0

    .line 185
    aget v1, p3, v0

    invoke-virtual {p0, p1, p2, v1}, Laqs;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V

    .line 184
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 187
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;[II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 93
    const-string v0, "removeAnimationView()"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 95
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    invoke-virtual {v0}, Lage;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lage;->b(Z)V

    .line 97
    if-ne p3, v2, :cond_2

    .line 98
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 104
    :cond_0
    :goto_0
    const-string v0, "Remove widget blinking from activity"

    invoke-static {v0}, Lalj;->b(Ljava/lang/String;)V

    .line 105
    sput v2, Laqs;->a:I

    .line 106
    if-eqz p2, :cond_1

    array-length v0, p2

    if-eqz v0, :cond_1

    .line 107
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Laqs;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 109
    :cond_1
    return-void

    .line 99
    :cond_2
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    .line 100
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a04ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V
    .locals 2

    .prologue
    .line 112
    .line 114
    const-string v0, "pref_geonews_current_page"

    const/4 v1, -0x1

    invoke-static {p1, v0, v1}, Lall;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 116
    invoke-virtual {p0, p1, p2, p3, v0}, Laqs;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;II)V

    .line 117
    return-void
.end method

.method public b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 190
    const-string v1, "pref_geonews_current_page"

    invoke-static {p1, v1, v0}, Lall;->a(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    .line 192
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "refreshData currentPage:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lalj;->b(Ljava/lang/String;)V

    .line 194
    invoke-static {p1}, Lane;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 195
    const-string v0, "refreshData : Network is NOT connected!!!"

    invoke-static {v0}, Lalj;->d(Ljava/lang/String;)V

    .line 196
    const v0, 0x7f0a0011

    invoke-static {p1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v2

    invoke-virtual {v2}, Lage;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 201
    const-string v1, "refreshData : already refreshing"

    invoke-static {v1}, Lalj;->e(Ljava/lang/String;)V

    .line 202
    :goto_1
    array-length v1, p3

    if-ge v0, v1, :cond_0

    .line 203
    aget v1, p3, v0

    invoke-virtual {p0, p1, p2, v1}, Laqs;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V

    .line 202
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 208
    :cond_2
    if-nez v1, :cond_5

    .line 209
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "location_mode"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 210
    if-nez v1, :cond_3

    .line 211
    const-string v0, "location mode is LOCATION_MODE_OFF! request allow location!"

    invoke-static {v0}, Lalj;->e(Ljava/lang/String;)V

    .line 212
    invoke-static {}, Lanf;->a()Lanf;

    move-result-object v0

    const/16 v1, 0xa

    const/16 v2, 0x3e9

    invoke-virtual {v0, p1, v1, v2}, Lanf;->a(Landroid/content/Context;II)V

    goto :goto_0

    .line 214
    :cond_3
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v1

    invoke-virtual {v1, v4}, Lage;->b(Z)V

    .line 215
    :goto_2
    array-length v1, p3

    if-ge v0, v1, :cond_4

    .line 216
    aget v1, p3, v0

    invoke-virtual {p0, p1, p2, v1}, Laqs;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V

    .line 215
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 219
    :cond_4
    invoke-static {p1}, Lcom/sec/android/GeoLookout/DisasterAlertService;->b(Landroid/content/Context;)V

    goto :goto_0

    .line 223
    :cond_5
    invoke-static {p1}, Lage;->a(Landroid/content/Context;)Lage;

    move-result-object v2

    invoke-virtual {v2, v4}, Lage;->b(Z)V

    .line 224
    :goto_3
    array-length v2, p3

    if-ge v0, v2, :cond_6

    .line 225
    aget v2, p3, v0

    invoke-virtual {p0, p1, p2, v2}, Laqs;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V

    .line 224
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 228
    :cond_6
    invoke-static {}, Lalu;->a()Lalu;

    move-result-object v0

    invoke-virtual {v0, p1, v1}, Lalu;->a(Landroid/content/Context;I)V

    goto :goto_0
.end method
