.class public final Lads;
.super Lck;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lck",
        "<",
        "Ladc;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Lcp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcp",
            "<",
            "Lcom/google/android/gms/internal/fv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/d;)V
    .locals 3

    invoke-direct {p0, p1}, Lck;-><init>(Lcom/google/android/gms/common/data/d;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/d;->f()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/d;->f()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.IsSafeParcelable"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcp;

    sget-object v1, Lcom/google/android/gms/internal/fv;->a:Ltf;

    invoke-direct {v0, p1, v1}, Lcp;-><init>(Lcom/google/android/gms/common/data/d;Landroid/os/Parcelable$Creator;)V

    iput-object v0, p0, Lads;->b:Lcp;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lads;->b:Lcp;

    goto :goto_0
.end method


# virtual methods
.method public a(I)Ladc;
    .locals 2

    iget-object v0, p0, Lads;->b:Lcp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lads;->b:Lcp;

    invoke-virtual {v0, p1}, Lcp;->a(I)Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;

    move-result-object v0

    check-cast v0, Ladc;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ltr;

    iget-object v1, p0, Lads;->a:Lcom/google/android/gms/common/data/d;

    invoke-direct {v0, v1, p1}, Ltr;-><init>(Lcom/google/android/gms/common/data/d;I)V

    goto :goto_0
.end method

.method public synthetic b(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lads;->a(I)Ladc;

    move-result-object v0

    return-object v0
.end method
