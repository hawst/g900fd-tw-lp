.class public Lki;
.super Lmz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmz",
        "<",
        "Lkm;",
        ">;"
    }
.end annotation


# instance fields
.field private final f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcb;Lcc;I)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lmz;-><init>(Landroid/content/Context;Lcb;Lcc;[Ljava/lang/String;)V

    iput p4, p0, Lki;->f:I

    return-void
.end method


# virtual methods
.method protected a(Landroid/os/IBinder;)Lkm;
    .locals 1

    invoke-static {p1}, Lkn;->a(Landroid/os/IBinder;)Lkm;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lno;Lnd;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget v1, p0, Lki;->f:I

    invoke-virtual {p0}, Lki;->i()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lno;->g(Lnl;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method protected synthetic b(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lki;->a(Landroid/os/IBinder;)Lkm;

    move-result-object v0

    return-object v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.ads.service.START"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.ads.internal.request.IAdRequestService"

    return-object v0
.end method

.method public g()Lkm;
    .locals 1

    invoke-super {p0}, Lmz;->o()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lkm;

    return-object v0
.end method
