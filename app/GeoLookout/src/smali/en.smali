.class public final Len;
.super Lco;

# interfaces
.implements Lej;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/data/d;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lco;-><init>(Lcom/google/android/gms/common/data/d;I)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const-string v0, "external_achievement_id"

    invoke-virtual {p0, v0}, Len;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/CharArrayBuffer;)V
    .locals 1

    const-string v0, "name"

    invoke-virtual {p0, v0, p1}, Len;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    return-void
.end method

.method public b()I
    .locals 1

    const-string v0, "type"

    invoke-virtual {p0, v0}, Len;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public b(Landroid/database/CharArrayBuffer;)V
    .locals 1

    const-string v0, "description"

    invoke-virtual {p0, v0, p1}, Len;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    const-string v0, "name"

    invoke-virtual {p0, v0}, Len;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/database/CharArrayBuffer;)V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Len;->b()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lmx;->a(Z)V

    const-string v0, "formatted_total_steps"

    invoke-virtual {p0, v0, p1}, Len;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    const-string v0, "description"

    invoke-virtual {p0, v0}, Len;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Landroid/database/CharArrayBuffer;)V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Len;->b()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lmx;->a(Z)V

    const-string v0, "formatted_current_steps"

    invoke-virtual {p0, v0, p1}, Len;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Landroid/net/Uri;
    .locals 1

    const-string v0, "unlocked_icon_image_uri"

    invoke-virtual {p0, v0}, Len;->f(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public f()Landroid/net/Uri;
    .locals 1

    const-string v0, "revealed_icon_image_uri"

    invoke-virtual {p0, v0}, Len;->f(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public g()I
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Len;->b()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lmx;->a(Z)V

    const-string v0, "total_steps"

    invoke-virtual {p0, v0}, Len;->b(Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Len;->b()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lmx;->a(Z)V

    const-string v0, "formatted_total_steps"

    invoke-virtual {p0, v0}, Len;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Lcom/google/android/gms/games/Player;
    .locals 3

    new-instance v0, Lcom/google/android/gms/games/d;

    iget-object v1, p0, Len;->a_:Lcom/google/android/gms/common/data/d;

    iget v2, p0, Len;->b_:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/d;-><init>(Lcom/google/android/gms/common/data/d;I)V

    return-object v0
.end method

.method public j()I
    .locals 1

    const-string v0, "state"

    invoke-virtual {p0, v0}, Len;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public k()I
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Len;->b()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lmx;->a(Z)V

    const-string v0, "current_steps"

    invoke-virtual {p0, v0}, Len;->b(Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Ljava/lang/String;
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0}, Len;->b()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lmx;->a(Z)V

    const-string v0, "formatted_current_steps"

    invoke-virtual {p0, v0}, Len;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()J
    .locals 2

    const-string v0, "last_updated_timestamp"

    invoke-virtual {p0, v0}, Len;->a(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    invoke-static {p0}, Lnu;->a(Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {p0}, Len;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {p0}, Len;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "state"

    invoke-virtual {p0}, Len;->j()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    const-string v1, "type"

    invoke-virtual {p0}, Len;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    move-result-object v0

    invoke-virtual {p0}, Len;->b()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const-string v1, "steps"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Len;->k()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Len;->g()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lnw;->a(Ljava/lang/String;Ljava/lang/Object;)Lnw;

    :cond_0
    invoke-virtual {v0}, Lnw;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
