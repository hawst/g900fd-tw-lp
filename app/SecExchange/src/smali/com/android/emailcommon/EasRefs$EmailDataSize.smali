.class public final enum Lcom/android/emailcommon/EasRefs$EmailDataSize;
.super Ljava/lang/Enum;
.source "EasRefs.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/emailcommon/EasRefs;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EmailDataSize"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/emailcommon/EasRefs$EmailDataSize;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/emailcommon/EasRefs$EmailDataSize;

.field public static final enum ALL:Lcom/android/emailcommon/EasRefs$EmailDataSize;

.field public static final enum ALL_WITH_ATTACHMENT:Lcom/android/emailcommon/EasRefs$EmailDataSize;

.field public static final enum AUTOMATIC:Lcom/android/emailcommon/EasRefs$EmailDataSize;

.field public static final enum FIFTY_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

.field public static final enum FIVE_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

.field public static final enum HALF_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

.field public static final enum HEADERS_ONLY:Lcom/android/emailcommon/EasRefs$EmailDataSize;

.field public static final enum HUNDRED_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

.field public static final enum ONE_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

.field public static final enum TEN_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

.field public static final enum TWENTY_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

.field public static final enum TWO_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;


# instance fields
.field private final mText:Ljava/lang/String;

.field private final mValue:B


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 192
    new-instance v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;

    const-string v1, "HEADERS_ONLY"

    const-string v2, "0"

    invoke-direct {v0, v1, v5, v5, v2}, Lcom/android/emailcommon/EasRefs$EmailDataSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->HEADERS_ONLY:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    new-instance v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;

    const-string v1, "HALF_KB"

    const-string v2, "1"

    invoke-direct {v0, v1, v6, v6, v2}, Lcom/android/emailcommon/EasRefs$EmailDataSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->HALF_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    new-instance v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;

    const-string v1, "ONE_KB"

    const-string v2, "2"

    invoke-direct {v0, v1, v7, v7, v2}, Lcom/android/emailcommon/EasRefs$EmailDataSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->ONE_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    new-instance v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;

    const-string v1, "TWO_KB"

    const-string v2, "3"

    invoke-direct {v0, v1, v8, v8, v2}, Lcom/android/emailcommon/EasRefs$EmailDataSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->TWO_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    new-instance v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;

    const-string v1, "FIVE_KB"

    const-string v2, "4"

    invoke-direct {v0, v1, v9, v9, v2}, Lcom/android/emailcommon/EasRefs$EmailDataSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->FIVE_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    new-instance v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;

    const-string v1, "TEN_KB"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const-string v4, "5"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/EasRefs$EmailDataSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->TEN_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    .line 193
    new-instance v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;

    const-string v1, "TWENTY_KB"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const-string v4, "6"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/EasRefs$EmailDataSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->TWENTY_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    new-instance v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;

    const-string v1, "FIFTY_KB"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const-string v4, "7"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/EasRefs$EmailDataSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->FIFTY_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    new-instance v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;

    const-string v1, "HUNDRED_KB"

    const/16 v2, 0x8

    const/16 v3, 0x8

    const-string v4, "8"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/EasRefs$EmailDataSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->HUNDRED_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    new-instance v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;

    const-string v1, "ALL"

    const/16 v2, 0x9

    const/16 v3, 0x9

    const-string v4, "9"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/EasRefs$EmailDataSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->ALL:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    new-instance v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;

    const-string v1, "ALL_WITH_ATTACHMENT"

    const/16 v2, 0xa

    const/16 v3, 0xa

    const-string v4, "10"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/EasRefs$EmailDataSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->ALL_WITH_ATTACHMENT:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    .line 195
    new-instance v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;

    const-string v1, "AUTOMATIC"

    const/16 v2, 0xb

    const/16 v3, 0xb

    const-string v4, "11"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/EasRefs$EmailDataSize;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->AUTOMATIC:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    .line 190
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/android/emailcommon/EasRefs$EmailDataSize;

    sget-object v1, Lcom/android/emailcommon/EasRefs$EmailDataSize;->HEADERS_ONLY:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/emailcommon/EasRefs$EmailDataSize;->HALF_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/emailcommon/EasRefs$EmailDataSize;->ONE_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    aput-object v1, v0, v7

    sget-object v1, Lcom/android/emailcommon/EasRefs$EmailDataSize;->TWO_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    aput-object v1, v0, v8

    sget-object v1, Lcom/android/emailcommon/EasRefs$EmailDataSize;->FIVE_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/android/emailcommon/EasRefs$EmailDataSize;->TEN_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/android/emailcommon/EasRefs$EmailDataSize;->TWENTY_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/android/emailcommon/EasRefs$EmailDataSize;->FIFTY_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/android/emailcommon/EasRefs$EmailDataSize;->HUNDRED_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/android/emailcommon/EasRefs$EmailDataSize;->ALL:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/android/emailcommon/EasRefs$EmailDataSize;->ALL_WITH_ATTACHMENT:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/android/emailcommon/EasRefs$EmailDataSize;->AUTOMATIC:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->$VALUES:[Lcom/android/emailcommon/EasRefs$EmailDataSize;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 1
    .param p3, "value"    # I
    .param p4, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 205
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 206
    int-to-byte v0, p3

    iput-byte v0, p0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->mValue:B

    .line 207
    iput-object p4, p0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->mText:Ljava/lang/String;

    .line 208
    return-void
.end method

.method public static parse(B)Lcom/android/emailcommon/EasRefs$EmailDataSize;
    .locals 1
    .param p0, "value"    # B

    .prologue
    .line 333
    packed-switch p0, :pswitch_data_0

    .line 374
    :pswitch_0
    sget-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->ALL:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    :goto_0
    return-object v0

    .line 336
    :pswitch_1
    sget-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->HEADERS_ONLY:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    goto :goto_0

    .line 339
    :pswitch_2
    sget-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->HALF_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    goto :goto_0

    .line 342
    :pswitch_3
    sget-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->ONE_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    goto :goto_0

    .line 345
    :pswitch_4
    sget-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->TWO_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    goto :goto_0

    .line 348
    :pswitch_5
    sget-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->FIVE_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    goto :goto_0

    .line 351
    :pswitch_6
    sget-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->TEN_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    goto :goto_0

    .line 354
    :pswitch_7
    sget-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->TWENTY_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    goto :goto_0

    .line 357
    :pswitch_8
    sget-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->FIFTY_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    goto :goto_0

    .line 360
    :pswitch_9
    sget-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->HUNDRED_KB:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    goto :goto_0

    .line 363
    :pswitch_a
    sget-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->ALL_WITH_ATTACHMENT:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    goto :goto_0

    .line 366
    :pswitch_b
    sget-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->AUTOMATIC:Lcom/android/emailcommon/EasRefs$EmailDataSize;

    goto :goto_0

    .line 333
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public static parse(I)Lcom/android/emailcommon/EasRefs$EmailDataSize;
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 378
    int-to-byte v0, p0

    invoke-static {v0}, Lcom/android/emailcommon/EasRefs$EmailDataSize;->parse(B)Lcom/android/emailcommon/EasRefs$EmailDataSize;

    move-result-object v0

    return-object v0
.end method

.method public static parseToByte(I)B
    .locals 5
    .param p0, "size"    # I

    .prologue
    const/16 v4, 0x2800

    const/16 v3, 0x1400

    const/16 v2, 0x800

    const/16 v1, 0x400

    const/16 v0, 0x200

    .line 393
    if-nez p0, :cond_0

    .line 394
    const/16 v0, 0x9

    .line 412
    :goto_0
    return v0

    .line 395
    :cond_0
    if-lez p0, :cond_1

    if-ge p0, v0, :cond_1

    .line 396
    const/4 v0, 0x0

    goto :goto_0

    .line 397
    :cond_1
    if-lt p0, v0, :cond_2

    if-ge p0, v1, :cond_2

    .line 398
    const/4 v0, 0x1

    goto :goto_0

    .line 399
    :cond_2
    if-lt p0, v1, :cond_3

    if-ge p0, v2, :cond_3

    .line 400
    const/4 v0, 0x2

    goto :goto_0

    .line 401
    :cond_3
    if-lt p0, v2, :cond_4

    if-ge p0, v3, :cond_4

    .line 402
    const/4 v0, 0x3

    goto :goto_0

    .line 403
    :cond_4
    if-lt p0, v3, :cond_5

    if-ge p0, v4, :cond_5

    .line 404
    const/4 v0, 0x4

    goto :goto_0

    .line 405
    :cond_5
    if-lt p0, v4, :cond_6

    const/16 v0, 0x5000

    if-ge p0, v0, :cond_6

    .line 406
    const/4 v0, 0x5

    goto :goto_0

    .line 407
    :cond_6
    const/16 v0, 0x5000

    if-lt p0, v0, :cond_7

    const v0, 0xc800

    if-ge p0, v0, :cond_7

    .line 408
    const/4 v0, 0x6

    goto :goto_0

    .line 409
    :cond_7
    const v0, 0xc800

    if-lt p0, v0, :cond_8

    const v0, 0x19000

    if-ge p0, v0, :cond_8

    .line 410
    const/4 v0, 0x7

    goto :goto_0

    .line 412
    :cond_8
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/emailcommon/EasRefs$EmailDataSize;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 190
    const-class v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;

    return-object v0
.end method

.method public static values()[Lcom/android/emailcommon/EasRefs$EmailDataSize;
    .locals 1

    .prologue
    .line 190
    sget-object v0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->$VALUES:[Lcom/android/emailcommon/EasRefs$EmailDataSize;

    invoke-virtual {v0}, [Lcom/android/emailcommon/EasRefs$EmailDataSize;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/emailcommon/EasRefs$EmailDataSize;

    return-object v0
.end method


# virtual methods
.method public toEas12Text()Ljava/lang/String;
    .locals 1

    .prologue
    .line 283
    iget-byte v0, p0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->mValue:B

    packed-switch v0, :pswitch_data_0

    .line 329
    const-string v0, "2147483647"

    :goto_0
    return-object v0

    .line 286
    :pswitch_0
    const-string v0, "0"

    goto :goto_0

    .line 289
    :pswitch_1
    const-string v0, "512"

    goto :goto_0

    .line 292
    :pswitch_2
    const-string v0, "1024"

    goto :goto_0

    .line 295
    :pswitch_3
    const-string v0, "2048"

    goto :goto_0

    .line 298
    :pswitch_4
    const-string v0, "5120"

    goto :goto_0

    .line 301
    :pswitch_5
    const-string v0, "10240"

    goto :goto_0

    .line 304
    :pswitch_6
    const-string v0, "20480"

    goto :goto_0

    .line 307
    :pswitch_7
    const-string v0, "51200"

    goto :goto_0

    .line 310
    :pswitch_8
    const-string v0, "102400"

    goto :goto_0

    .line 283
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public toEas12Value()I
    .locals 1

    .prologue
    .line 233
    iget-byte v0, p0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->mValue:B

    packed-switch v0, :pswitch_data_0

    .line 279
    const v0, 0x7fffffff

    :goto_0
    return v0

    .line 236
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 239
    :pswitch_1
    const/16 v0, 0x200

    goto :goto_0

    .line 242
    :pswitch_2
    const/16 v0, 0x400

    goto :goto_0

    .line 245
    :pswitch_3
    const/16 v0, 0x800

    goto :goto_0

    .line 248
    :pswitch_4
    const/16 v0, 0x1400

    goto :goto_0

    .line 251
    :pswitch_5
    const/16 v0, 0x2800

    goto :goto_0

    .line 254
    :pswitch_6
    const/16 v0, 0x5000

    goto :goto_0

    .line 257
    :pswitch_7
    const v0, 0xc800

    goto :goto_0

    .line 260
    :pswitch_8
    const v0, 0x19000

    goto :goto_0

    .line 233
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public toEas2_5Text()Ljava/lang/String;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/android/emailcommon/EasRefs$EmailDataSize;->mText:Ljava/lang/String;

    return-object v0
.end method
