.class public Lcom/android/emailcommon/cac/CACException;
.super Ljava/lang/Exception;
.source "CACException.java"


# static fields
.field private static final serialVersionUID:J = -0x3514b1a5d04c0892L


# instance fields
.field private mStatus:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "status"    # I

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/emailcommon/cac/CACException;->mStatus:I

    .line 29
    iput p1, p0, Lcom/android/emailcommon/cac/CACException;->mStatus:I

    .line 30
    return-void
.end method


# virtual methods
.method public getStatus()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/android/emailcommon/cac/CACException;->mStatus:I

    return v0
.end method
