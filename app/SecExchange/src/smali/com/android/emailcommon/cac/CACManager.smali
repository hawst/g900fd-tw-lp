.class public Lcom/android/emailcommon/cac/CACManager;
.super Ljava/lang/Object;
.source "CACManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/cac/CACManager$OnStatusChangeListener;,
        Lcom/android/emailcommon/cac/CACManager$InitializingInfo;
    }
.end annotation


# static fields
.field private static _instance:Lcom/android/emailcommon/cac/CACManager;

.field private static sInitInfo:Lcom/android/emailcommon/cac/CACManager$InitializingInfo;


# instance fields
.field protected mContext:Landroid/content/Context;

.field private mErrorCode:I

.field private final mKeyStoreName:Ljava/lang/String;

.field private mListeners:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/emailcommon/cac/CACManager$OnStatusChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field private mProviderName:Ljava/lang/String;

.field private mSCEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

.field private mSCHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    sput-object v1, Lcom/android/emailcommon/cac/CACManager;->_instance:Lcom/android/emailcommon/cac/CACManager;

    .line 53
    new-instance v0, Lcom/android/emailcommon/cac/CACManager$InitializingInfo;

    invoke-direct {v0, v1}, Lcom/android/emailcommon/cac/CACManager$InitializingInfo;-><init>(Lcom/android/emailcommon/cac/CACManager$1;)V

    sput-object v0, Lcom/android/emailcommon/cac/CACManager;->sInitInfo:Lcom/android/emailcommon/cac/CACManager$InitializingInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object v1, p0, Lcom/android/emailcommon/cac/CACManager;->mContext:Landroid/content/Context;

    .line 39
    const-string v0, "PKCS11"

    iput-object v0, p0, Lcom/android/emailcommon/cac/CACManager;->mKeyStoreName:Ljava/lang/String;

    .line 41
    iput-object v1, p0, Lcom/android/emailcommon/cac/CACManager;->mProviderName:Ljava/lang/String;

    .line 45
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/emailcommon/cac/CACManager;->mErrorCode:I

    .line 47
    iput-object v1, p0, Lcom/android/emailcommon/cac/CACManager;->mSCEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    .line 48
    iput-object v1, p0, Lcom/android/emailcommon/cac/CACManager;->mSCHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    .line 55
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/emailcommon/cac/CACManager;->mListeners:Ljava/util/HashSet;

    .line 511
    return-void
.end method

.method static synthetic access$100(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$202(Lcom/android/emailcommon/cac/CACManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/android/emailcommon/cac/CACManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/android/emailcommon/cac/CACManager;->mProviderName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/android/emailcommon/cac/CACManager;)Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;
    .locals 1
    .param p0, "x0"    # Lcom/android/emailcommon/cac/CACManager;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/android/emailcommon/cac/CACManager;->mSCHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    return-object v0
.end method

.method static synthetic access$400()Lcom/android/emailcommon/cac/CACManager$InitializingInfo;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/android/emailcommon/cac/CACManager;->sInitInfo:Lcom/android/emailcommon/cac/CACManager$InitializingInfo;

    return-object v0
.end method

.method static synthetic access$502(Lcom/android/emailcommon/cac/CACManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/emailcommon/cac/CACManager;
    .param p1, "x1"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/android/emailcommon/cac/CACManager;->mErrorCode:I

    return p1
.end method

.method static synthetic access$600(Lcom/android/emailcommon/cac/CACManager;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/emailcommon/cac/CACManager;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/android/emailcommon/cac/CACManager;->feedback(I)V

    return-void
.end method

.method private feedback(I)V
    .locals 5
    .param p1, "code"    # I

    .prologue
    .line 107
    const/4 v4, 0x4

    if-eq p1, v4, :cond_0

    const/16 v4, 0x9

    if-ne p1, v4, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/android/emailcommon/cac/CACManager;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashSet;

    .line 119
    .local v3, "listeners":Ljava/util/HashSet;, "Ljava/util/HashSet<*>;"
    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 120
    .local v2, "l":Ljava/lang/Object;
    check-cast v2, Lcom/android/emailcommon/cac/CACManager$OnStatusChangeListener;

    .end local v2    # "l":Ljava/lang/Object;
    invoke-interface {v2, p1}, Lcom/android/emailcommon/cac/CACManager$OnStatusChangeListener;->onStatusChanged(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 122
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "listeners":Ljava/util/HashSet;, "Ljava/util/HashSet<*>;"
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getAliasForSignature(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "emailAddress"    # Ljava/lang/String;

    .prologue
    .line 440
    const-string v2, "CACManager"

    const-string v3, "getAliasForSignature function"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    const/4 v1, 0x0

    .line 442
    .local v1, "sig_cert":Ljava/lang/String;
    sget-object v2, Lcom/android/emailcommon/cac/CACManager;->_instance:Lcom/android/emailcommon/cac/CACManager;

    iget-object v2, v2, Lcom/android/emailcommon/cac/CACManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    move-result-object v0

    .line 443
    .local v0, "SCEmailPolicy":Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;
    if-eqz v0, :cond_0

    if-nez p0, :cond_1

    .line 444
    :cond_0
    const-string v2, "CACManager"

    const-string v3, "internal library was not found. SCEmailPolicy is null."

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    invoke-static {}, Lcom/android/emailcommon/cac/CACManager;->getAliasForSignatureUsingKeyusage()Ljava/lang/String;

    move-result-object v2

    .line 455
    :goto_0
    return-object v2

    .line 448
    :cond_1
    invoke-virtual {v0, p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->getSMIMESignatureCertificate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 449
    if-nez v1, :cond_2

    .line 450
    const-string v2, "CACManager"

    const-string v3, "getSMIMESignatureCertificate returned null."

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    invoke-static {}, Lcom/android/emailcommon/cac/CACManager;->getAliasForSignatureUsingKeyusage()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 454
    :cond_2
    const-string v2, "CACManager"

    const-string v3, "getAliasForSignature function returning alias got from email policy"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    .line 455
    goto :goto_0
.end method

.method public static getAliasForSignatureUsingKeyusage()Ljava/lang/String;
    .locals 16

    .prologue
    .line 345
    const-string v14, "CACManager"

    const-string v15, "getAliasForSignatureUsingKeyusage function"

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    :try_start_0
    const-string v14, "PKCS11"

    const-string v15, "SECPkcs11"

    invoke-static {v14, v15}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v12

    .line 348
    .local v12, "pkcs11KS":Ljava/security/KeyStore;
    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v12, v14, v15}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 349
    invoke-virtual {v12}, Ljava/security/KeyStore;->aliases()Ljava/util/Enumeration;

    move-result-object v2

    .line 350
    .local v2, "aliases":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 351
    .local v1, "alias":Ljava/lang/String;
    :cond_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 352
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "alias":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .line 353
    .restart local v1    # "alias":Ljava/lang/String;
    invoke-virtual {v12, v1}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v13

    check-cast v13, Ljava/security/cert/X509Certificate;

    .line 354
    .local v13, "pubCert":Ljava/security/cert/X509Certificate;
    invoke-virtual {v13}, Ljava/security/cert/X509Certificate;->getKeyUsage()[Z

    move-result-object v9

    .line 355
    .local v9, "keyUsages":[Z
    if-eqz v9, :cond_0

    .line 358
    const/4 v14, 0x0

    aget-boolean v14, v9, v14

    const/4 v15, 0x1

    if-eq v14, v15, :cond_1

    const/4 v14, 0x1

    aget-boolean v14, v9, v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_0

    .line 366
    :cond_1
    invoke-virtual {v13}, Ljava/security/cert/X509Certificate;->getExtendedKeyUsage()Ljava/util/List;

    move-result-object v6

    .line 367
    .local v6, "extendedKeyUsages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v6, :cond_0

    .line 369
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .line 370
    .local v8, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    if-eqz v8, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 371
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 373
    .local v11, "oid":Ljava/lang/String;
    const-string v14, "1.3.6.1.5.5.7.3.4"

    invoke-virtual {v11, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_3

    const-string v14, "2.5.29.37.0"

    invoke-virtual {v11, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 374
    :cond_3
    const-string v14, "CACManager"

    const-string v15, "getAliasForSignatureUsingKeyusage function returning alias found"

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateParsingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_5

    .line 397
    .end local v1    # "alias":Ljava/lang/String;
    .end local v2    # "aliases":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    .end local v6    # "extendedKeyUsages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v8    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v9    # "keyUsages":[Z
    .end local v11    # "oid":Ljava/lang/String;
    .end local v13    # "pubCert":Ljava/security/cert/X509Certificate;
    :goto_0
    return-object v1

    .line 383
    :catch_0
    move-exception v5

    .line 384
    .local v5, "e":Ljava/security/KeyStoreException;
    const-string v14, "CACManager"

    const-string v15, "getAliasForSignatureUsingKeyusage function KeyStoreException"

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    .end local v5    # "e":Ljava/security/KeyStoreException;
    :cond_4
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 385
    :catch_1
    move-exception v3

    .line 386
    .local v3, "c":Ljava/security/cert/CertificateParsingException;
    const-string v14, "CACManager"

    const-string v15, "getAliasForSignatureUsingKeyusage function CertificateParsingException"

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 387
    .end local v3    # "c":Ljava/security/cert/CertificateParsingException;
    :catch_2
    move-exception v10

    .line 388
    .local v10, "n":Ljava/security/NoSuchProviderException;
    const-string v14, "CACManager"

    const-string v15, "getAliasForSignatureUsingKeyusage function NoSuchProviderException"

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 389
    .end local v10    # "n":Ljava/security/NoSuchProviderException;
    :catch_3
    move-exception v7

    .line 390
    .local v7, "io":Ljava/io/IOException;
    const-string v14, "CACManager"

    const-string v15, "getAliasForSignatureUsingKeyusage function IOException"

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 391
    .end local v7    # "io":Ljava/io/IOException;
    :catch_4
    move-exception v0

    .line 392
    .local v0, "a":Ljava/security/NoSuchAlgorithmException;
    const-string v14, "CACManager"

    const-string v15, "getAliasForSignatureUsingKeyusage function NoSuchAlgorithmException"

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 393
    .end local v0    # "a":Ljava/security/NoSuchAlgorithmException;
    :catch_5
    move-exception v4

    .line 394
    .local v4, "ce":Ljava/security/cert/CertificateException;
    const-string v14, "CACManager"

    const-string v15, "getAliasForSignatureUsingKeyusage function CertificateException"

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static getCACState(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 188
    const/4 v0, 0x2

    .line 189
    .local v0, "retVal":I
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v1

    iget-object v1, v1, Lcom/android/emailcommon/cac/CACManager;->mProviderName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 190
    const/4 v0, 0x0

    .line 195
    :cond_0
    :goto_0
    const-string v1, "CACManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCACState : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v3

    iget v3, v3, Lcom/android/emailcommon/cac/CACManager;->mErrorCode:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    return v0

    .line 191
    :cond_1
    sget-object v1, Lcom/android/emailcommon/cac/CACManager;->sInitInfo:Lcom/android/emailcommon/cac/CACManager$InitializingInfo;

    invoke-virtual {v1}, Lcom/android/emailcommon/cac/CACManager$InitializingInfo;->isInitProgress()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 192
    const/4 v0, 0x1

    goto :goto_0

    .line 193
    :cond_2
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v1

    iget v1, v1, Lcom/android/emailcommon/cac/CACManager;->mErrorCode:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 194
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public static getCertificateMgr(Landroid/content/Context;)Lcom/android/emailcommon/smime/CertificateMgr;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/smime/CertificateManagerException;
        }
    .end annotation

    .prologue
    .line 235
    const/4 v0, 0x0

    .line 238
    .local v0, "mgr":Lcom/android/emailcommon/smime/CertificateMgr;
    :try_start_0
    new-instance v0, Lcom/android/emailcommon/smime/CertificateMgr;

    .end local v0    # "mgr":Lcom/android/emailcommon/smime/CertificateMgr;
    const-string v1, ""

    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v2

    iget-object v2, v2, Lcom/android/emailcommon/cac/CACManager;->mContext:Landroid/content/Context;

    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v3

    iget-object v3, v3, Lcom/android/emailcommon/cac/CACManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/emailcommon/cac/CACManager;->getKeyStore(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v4

    iget-object v4, v4, Lcom/android/emailcommon/cac/CACManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/android/emailcommon/cac/CACManager;->getProvider(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/android/emailcommon/smime/CertificateMgr;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Lcom/android/emailcommon/smime/CertificateManagerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    .restart local v0    # "mgr":Lcom/android/emailcommon/smime/CertificateMgr;
    invoke-virtual {v0}, Lcom/android/emailcommon/smime/CertificateMgr;->getAliases()Ljava/util/Enumeration;

    move-result-object v7

    .line 253
    .local v7, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    if-nez v7, :cond_3

    .line 269
    :cond_0
    :goto_0
    return-object v0

    .line 241
    .end local v0    # "mgr":Lcom/android/emailcommon/smime/CertificateMgr;
    .end local v7    # "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :catch_0
    move-exception v6

    .line 242
    .local v6, "e":Lcom/android/emailcommon/smime/CertificateManagerException;
    const-string v1, "CACManager"

    invoke-virtual {v6}, Lcom/android/emailcommon/smime/CertificateManagerException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v1

    iget-object v1, v1, Lcom/android/emailcommon/cac/CACManager;->mProviderName:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 244
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v1

    iget-object v1, v1, Lcom/android/emailcommon/cac/CACManager;->mSCHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    if-eqz v1, :cond_1

    .line 245
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v1

    iget-object v1, v1, Lcom/android/emailcommon/cac/CACManager;->mSCHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->unregisterProvider()Z

    .line 246
    :cond_1
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v1

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/android/emailcommon/cac/CACManager;->mProviderName:Ljava/lang/String;

    .line 248
    :cond_2
    throw v6

    .line 257
    .end local v6    # "e":Lcom/android/emailcommon/smime/CertificateManagerException;
    .restart local v0    # "mgr":Lcom/android/emailcommon/smime/CertificateMgr;
    .restart local v7    # "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :cond_3
    invoke-interface {v7}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-nez v1, :cond_0

    .line 258
    const-string v1, "CACManager"

    const-string v2, "Empty Keystore!!!"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;
    .locals 5
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 74
    sget-object v2, Lcom/android/emailcommon/cac/CACManager;->_instance:Lcom/android/emailcommon/cac/CACManager;

    if-nez v2, :cond_0

    .line 75
    new-instance v2, Lcom/android/emailcommon/cac/CACManager;

    invoke-direct {v2}, Lcom/android/emailcommon/cac/CACManager;-><init>()V

    sput-object v2, Lcom/android/emailcommon/cac/CACManager;->_instance:Lcom/android/emailcommon/cac/CACManager;

    .line 76
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 77
    .local v0, "applicationContext":Landroid/content/Context;
    sget-object v2, Lcom/android/emailcommon/cac/CACManager;->_instance:Lcom/android/emailcommon/cac/CACManager;

    iput-object v0, v2, Lcom/android/emailcommon/cac/CACManager;->mContext:Landroid/content/Context;

    .line 79
    :try_start_0
    sget-object v2, Lcom/android/emailcommon/cac/CACManager;->_instance:Lcom/android/emailcommon/cac/CACManager;

    invoke-static {v0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    move-result-object v3

    iput-object v3, v2, Lcom/android/emailcommon/cac/CACManager;->mSCEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    .line 80
    sget-object v2, Lcom/android/emailcommon/cac/CACManager;->_instance:Lcom/android/emailcommon/cac/CACManager;

    invoke-static {v0}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    move-result-object v3

    iput-object v3, v2, Lcom/android/emailcommon/cac/CACManager;->mSCHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessError; {:try_start_0 .. :try_end_0} :catch_1

    .line 91
    .end local v0    # "applicationContext":Landroid/content/Context;
    :cond_0
    :goto_0
    sget-object v2, Lcom/android/emailcommon/cac/CACManager;->_instance:Lcom/android/emailcommon/cac/CACManager;

    return-object v2

    .line 81
    .restart local v0    # "applicationContext":Landroid/content/Context;
    :catch_0
    move-exception v1

    .line 82
    .local v1, "e":Ljava/lang/NoClassDefFoundError;
    const-string v2, "CACManager"

    invoke-virtual {v1}, Ljava/lang/NoClassDefFoundError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    sget-object v2, Lcom/android/emailcommon/cac/CACManager;->_instance:Lcom/android/emailcommon/cac/CACManager;

    iput-object v4, v2, Lcom/android/emailcommon/cac/CACManager;->mSCEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    .line 84
    sget-object v2, Lcom/android/emailcommon/cac/CACManager;->_instance:Lcom/android/emailcommon/cac/CACManager;

    iput-object v4, v2, Lcom/android/emailcommon/cac/CACManager;->mSCHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    goto :goto_0

    .line 85
    .end local v1    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v1

    .line 86
    .local v1, "e":Ljava/lang/IllegalAccessError;
    const-string v2, "CACManager"

    invoke-virtual {v1}, Ljava/lang/IllegalAccessError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    sget-object v2, Lcom/android/emailcommon/cac/CACManager;->_instance:Lcom/android/emailcommon/cac/CACManager;

    iput-object v4, v2, Lcom/android/emailcommon/cac/CACManager;->mSCEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    .line 88
    sget-object v2, Lcom/android/emailcommon/cac/CACManager;->_instance:Lcom/android/emailcommon/cac/CACManager;

    iput-object v4, v2, Lcom/android/emailcommon/cac/CACManager;->mSCHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    goto :goto_0
.end method

.method public static getKeyStore(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 220
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "PKCS11"

    return-object v0
.end method

.method public static getProvider(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 202
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v0

    iget-object v0, v0, Lcom/android/emailcommon/cac/CACManager;->mProviderName:Ljava/lang/String;

    return-object v0
.end method

.method public static initCAC(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 273
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v1

    iget-object v1, v1, Lcom/android/emailcommon/cac/CACManager;->mSCHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    if-nez v1, :cond_0

    .line 274
    const-string v1, "CACManager"

    const-string v2, "internal library was not found"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    :goto_0
    return-void

    .line 278
    :cond_0
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v1

    invoke-direct {v1}, Lcom/android/emailcommon/cac/CACManager;->isSCAuthEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 279
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v1

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/android/emailcommon/cac/CACManager;->mProviderName:Ljava/lang/String;

    .line 280
    const-string v1, "CACManager"

    const-string v2, "getInstance(context).isSCAuthEnabled() is false"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 284
    :cond_1
    sget-object v1, Lcom/android/emailcommon/cac/CACManager;->sInitInfo:Lcom/android/emailcommon/cac/CACManager$InitializingInfo;

    invoke-virtual {v1}, Lcom/android/emailcommon/cac/CACManager$InitializingInfo;->isInitProgress()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 285
    const-string v1, "CACManager"

    const-string v2, "initialize progressing"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 289
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 291
    .local v0, "factx":Landroid/content/Context;
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v1

    iget-object v1, v1, Lcom/android/emailcommon/cac/CACManager;->mProviderName:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 292
    const-string v1, "CACManager"

    const-string v2, "try to initialize CAC"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v1

    iget-object v1, v1, Lcom/android/emailcommon/cac/CACManager;->mSCHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    new-instance v2, Lcom/android/emailcommon/cac/CACManager$1;

    invoke-direct {v2, v0}, Lcom/android/emailcommon/cac/CACManager$1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->initialize(Lcom/sec/enterprise/knox/smartcard/SmartCardHelper$Callback;)V

    .line 328
    sget-object v1, Lcom/android/emailcommon/cac/CACManager;->sInitInfo:Lcom/android/emailcommon/cac/CACManager$InitializingInfo;

    invoke-virtual {v1}, Lcom/android/emailcommon/cac/CACManager$InitializingInfo;->initStart()V

    goto :goto_0

    .line 330
    :cond_3
    const-string v1, "CACManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getInstance(context).mProviderName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v3

    iget-object v3, v3, Lcom/android/emailcommon/cac/CACManager;->mProviderName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isCredentialAccount(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 184
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/emailcommon/cac/CACManager;->isCredentialAccount(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private isSCAuthEnabled()Z
    .locals 4

    .prologue
    .line 95
    iget-object v1, p0, Lcom/android/emailcommon/cac/CACManager;->mSCEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    if-nez v1, :cond_0

    .line 96
    const-string v1, "CACManager"

    const-string v2, "internal library was not found"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const/4 v0, 0x0

    .line 103
    :goto_0
    return v0

    .line 99
    :cond_0
    iget-object v1, p0, Lcom/android/emailcommon/cac/CACManager;->mSCEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->isAuthenticationEnabled()Z

    move-result v0

    .line 100
    .local v0, "val":Z
    const-string v1, "Email"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CAC isSCAuthEnabled :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static setErrorCode(Landroid/content/Context;I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "errorCode"    # I

    .prologue
    .line 228
    const-string v0, "CACManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setErrorCode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    invoke-static {p0}, Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v0

    iput p1, v0, Lcom/android/emailcommon/cac/CACManager;->mErrorCode:I

    .line 230
    return-void
.end method

.method private static shiftChar(C)C
    .locals 3
    .param p0, "org"    # C

    .prologue
    .line 128
    const-string v1, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@."

    invoke-virtual {v1, p0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 129
    .local v0, "index":I
    if-lez v0, :cond_0

    .line 130
    const-string v1, "bcdefghijklmnopqrstuvwxyzaBCDEFGHIJKLMNOPQRSTUVWXYZA^="

    const-string v2, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@."

    invoke-virtual {v2, p0}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result p0

    .line 131
    .end local p0    # "org":C
    :cond_0
    return p0
.end method

.method private static shiftString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "org"    # Ljava/lang/String;

    .prologue
    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 136
    .local v0, "b":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "cnt":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 137
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/android/emailcommon/cac/CACManager;->shiftChar(C)C

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 136
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 139
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public isCredentialAccount(Ljava/lang/String;)Z
    .locals 4
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 143
    iget-object v1, p0, Lcom/android/emailcommon/cac/CACManager;->mSCEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    if-nez v1, :cond_0

    .line 144
    const-string v1, "CACManager"

    const-string v2, "internal library was not found. mSCEmailPolicy is null."

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    :goto_0
    return v0

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/android/emailcommon/cac/CACManager;->mSCEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->isAuthenticationEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 148
    const-string v1, "CACManager"

    const-string v2, "mSCEmailPolicy.isAuthenticationEnabled() is false"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 151
    :cond_1
    const/4 v0, 0x0

    .line 158
    .local v0, "credentialRequired":Z
    iget-object v1, p0, Lcom/android/emailcommon/cac/CACManager;->mSCEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    invoke-virtual {v1, p1}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->isCredentialRequired(Ljava/lang/String;)Z

    move-result v0

    .line 160
    const-string v1, "CACManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isCredentialAccount - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/android/emailcommon/cac/CACManager;->shiftString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : credentialRequired = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 336
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CACManager [mContext="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/emailcommon/cac/CACManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mKeyStoreName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "PKCS11"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mProviderName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/emailcommon/cac/CACManager;->mProviderName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mErrorCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/emailcommon/cac/CACManager;->mErrorCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSCEmailPolicy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/emailcommon/cac/CACManager;->mSCEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSCHelper="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/emailcommon/cac/CACManager;->mSCHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mListeners="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/emailcommon/cac/CACManager;->mListeners:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
