.class public Lcom/android/emailcommon/cac/EmailPKCS11KeyStore;
.super Ljava/lang/Object;
.source "EmailPKCS11KeyStore.java"


# instance fields
.field private mOpenSSLEngineClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private mOpenSSLEngineMethodGetPrivateKeyById:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v0, p0, Lcom/android/emailcommon/cac/EmailPKCS11KeyStore;->mOpenSSLEngineClass:Ljava/lang/Class;

    .line 33
    iput-object v0, p0, Lcom/android/emailcommon/cac/EmailPKCS11KeyStore;->mOpenSSLEngineMethodGetPrivateKeyById:Ljava/lang/reflect/Method;

    return-void
.end method
