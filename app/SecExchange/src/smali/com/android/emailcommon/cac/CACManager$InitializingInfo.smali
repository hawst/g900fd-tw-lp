.class Lcom/android/emailcommon/cac/CACManager$InitializingInfo;
.super Ljava/lang/Object;
.source "CACManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/emailcommon/cac/CACManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InitializingInfo"
.end annotation


# instance fields
.field isInitializing:Z

.field final threshhold:J

.field timeStamp:J


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486
    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lcom/android/emailcommon/cac/CACManager$InitializingInfo;->threshhold:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/emailcommon/cac/CACManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/emailcommon/cac/CACManager$1;

    .prologue
    .line 484
    invoke-direct {p0}, Lcom/android/emailcommon/cac/CACManager$InitializingInfo;-><init>()V

    return-void
.end method


# virtual methods
.method public initEnd()V
    .locals 2

    .prologue
    .line 506
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/emailcommon/cac/CACManager$InitializingInfo;->isInitializing:Z

    .line 507
    const-string v0, "CACManager"

    const-string v1, "initializing end"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    return-void
.end method

.method public initStart()V
    .locals 2

    .prologue
    .line 500
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/emailcommon/cac/CACManager$InitializingInfo;->isInitializing:Z

    .line 501
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/emailcommon/cac/CACManager$InitializingInfo;->timeStamp:J

    .line 502
    const-string v0, "CACManager"

    const-string v1, "initializing start"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    return-void
.end method

.method public isInitProgress()Z
    .locals 6

    .prologue
    .line 492
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 493
    .local v0, "t":J
    iget-wide v2, p0, Lcom/android/emailcommon/cac/CACManager$InitializingInfo;->timeStamp:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x2710

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 494
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/emailcommon/cac/CACManager$InitializingInfo;->isInitializing:Z

    .line 496
    :cond_0
    iget-boolean v2, p0, Lcom/android/emailcommon/cac/CACManager$InitializingInfo;->isInitializing:Z

    return v2
.end method
