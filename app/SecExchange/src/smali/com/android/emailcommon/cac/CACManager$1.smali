.class final Lcom/android/emailcommon/cac/CACManager$1;
.super Ljava/lang/Object;
.source "CACManager.java"

# interfaces
.implements Lcom/sec/enterprise/knox/smartcard/SmartCardHelper$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/emailcommon/cac/CACManager;->initCAC(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$factx:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 293
    iput-object p1, p0, Lcom/android/emailcommon/cac/CACManager$1;->val$factx:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInitComplete()V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/android/emailcommon/cac/CACManager$1;->val$factx:Landroid/content/Context;

    # invokes: Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;
    invoke-static {v0}, Lcom/android/emailcommon/cac/CACManager;->access$100(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/emailcommon/cac/CACManager$1;->val$factx:Landroid/content/Context;

    # invokes: Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;
    invoke-static {v1}, Lcom/android/emailcommon/cac/CACManager;->access$100(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v1

    # getter for: Lcom/android/emailcommon/cac/CACManager;->mSCHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;
    invoke-static {v1}, Lcom/android/emailcommon/cac/CACManager;->access$300(Lcom/android/emailcommon/cac/CACManager;)Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->registerProvider()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/android/emailcommon/cac/CACManager;->mProviderName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/android/emailcommon/cac/CACManager;->access$202(Lcom/android/emailcommon/cac/CACManager;Ljava/lang/String;)Ljava/lang/String;

    .line 299
    const-string v0, "CACManager"

    const-string v1, "CAC onInitComplete"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    # getter for: Lcom/android/emailcommon/cac/CACManager;->sInitInfo:Lcom/android/emailcommon/cac/CACManager$InitializingInfo;
    invoke-static {}, Lcom/android/emailcommon/cac/CACManager;->access$400()Lcom/android/emailcommon/cac/CACManager$InitializingInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/emailcommon/cac/CACManager$InitializingInfo;->initEnd()V

    .line 301
    iget-object v0, p0, Lcom/android/emailcommon/cac/CACManager$1;->val$factx:Landroid/content/Context;

    # invokes: Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;
    invoke-static {v0}, Lcom/android/emailcommon/cac/CACManager;->access$100(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v0

    const/4 v1, 0x0

    # setter for: Lcom/android/emailcommon/cac/CACManager;->mErrorCode:I
    invoke-static {v0, v1}, Lcom/android/emailcommon/cac/CACManager;->access$502(Lcom/android/emailcommon/cac/CACManager;I)I

    .line 302
    return-void
.end method

.method public onStatusChanged(I)V
    .locals 3
    .param p1, "arg0"    # I

    .prologue
    .line 305
    const-string v0, "CACManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CAC onStatusChanged : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    packed-switch p1, :pswitch_data_0

    .line 323
    :goto_0
    :pswitch_0
    # getter for: Lcom/android/emailcommon/cac/CACManager;->sInitInfo:Lcom/android/emailcommon/cac/CACManager$InitializingInfo;
    invoke-static {}, Lcom/android/emailcommon/cac/CACManager;->access$400()Lcom/android/emailcommon/cac/CACManager$InitializingInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/emailcommon/cac/CACManager$InitializingInfo;->initEnd()V

    .line 324
    iget-object v0, p0, Lcom/android/emailcommon/cac/CACManager$1;->val$factx:Landroid/content/Context;

    # invokes: Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;
    invoke-static {v0}, Lcom/android/emailcommon/cac/CACManager;->access$100(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v0

    # setter for: Lcom/android/emailcommon/cac/CACManager;->mErrorCode:I
    invoke-static {v0, p1}, Lcom/android/emailcommon/cac/CACManager;->access$502(Lcom/android/emailcommon/cac/CACManager;I)I

    .line 325
    iget-object v0, p0, Lcom/android/emailcommon/cac/CACManager$1;->val$factx:Landroid/content/Context;

    # invokes: Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;
    invoke-static {v0}, Lcom/android/emailcommon/cac/CACManager;->access$100(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v0

    # invokes: Lcom/android/emailcommon/cac/CACManager;->feedback(I)V
    invoke-static {v0, p1}, Lcom/android/emailcommon/cac/CACManager;->access$600(Lcom/android/emailcommon/cac/CACManager;I)V

    .line 326
    return-void

    .line 320
    :pswitch_1
    iget-object v0, p0, Lcom/android/emailcommon/cac/CACManager$1;->val$factx:Landroid/content/Context;

    # invokes: Lcom/android/emailcommon/cac/CACManager;->getInstance(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;
    invoke-static {v0}, Lcom/android/emailcommon/cac/CACManager;->access$100(Landroid/content/Context;)Lcom/android/emailcommon/cac/CACManager;

    move-result-object v0

    const/4 v1, 0x0

    # setter for: Lcom/android/emailcommon/cac/CACManager;->mProviderName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/android/emailcommon/cac/CACManager;->access$202(Lcom/android/emailcommon/cac/CACManager;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 307
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
