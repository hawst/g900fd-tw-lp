.class public Lcom/android/emailcommon/Logging;
.super Ljava/lang/Object;
.source "Logging.java"


# static fields
.field public static prevDownTime:J

.field public static prevOpenTime:J

.field public static prevTime:J

.field public static startDownTime:J

.field public static startOpenTime:J

.field public static startTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 22
    sput-wide v0, Lcom/android/emailcommon/Logging;->startTime:J

    sput-wide v0, Lcom/android/emailcommon/Logging;->prevTime:J

    .line 23
    sput-wide v0, Lcom/android/emailcommon/Logging;->startDownTime:J

    sput-wide v0, Lcom/android/emailcommon/Logging;->prevDownTime:J

    .line 24
    sput-wide v0, Lcom/android/emailcommon/Logging;->startOpenTime:J

    sput-wide v0, Lcom/android/emailcommon/Logging;->prevOpenTime:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
