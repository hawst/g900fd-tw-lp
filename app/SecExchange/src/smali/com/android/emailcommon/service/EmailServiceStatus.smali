.class public Lcom/android/emailcommon/service/EmailServiceStatus;
.super Ljava/lang/Object;
.source "EmailServiceStatus.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decodeIOException(Ljava/io/IOException;)I
    .locals 3
    .param p0, "ioe"    # Ljava/io/IOException;

    .prologue
    const/16 v1, 0x5c

    .line 234
    if-eqz p0, :cond_3

    .line 235
    invoke-virtual {p0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, "msg":Ljava/lang/String;
    instance-of v2, p0, Ljava/net/SocketTimeoutException;

    if-eqz v2, :cond_1

    .line 238
    const/16 v1, 0x59

    .line 255
    .end local v0    # "msg":Ljava/lang/String;
    :cond_0
    :goto_0
    return v1

    .line 239
    .restart local v0    # "msg":Ljava/lang/String;
    :cond_1
    instance-of v2, p0, Ljava/net/UnknownHostException;

    if-eqz v2, :cond_2

    .line 240
    const/16 v1, 0x58

    goto :goto_0

    .line 241
    :cond_2
    instance-of v2, p0, Ljava/net/SocketException;

    if-eqz v2, :cond_4

    .line 242
    if-eqz v0, :cond_3

    .line 243
    const-string v2, "Broken pipe"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 255
    .end local v0    # "msg":Ljava/lang/String;
    :cond_3
    const/16 v1, 0x32

    goto :goto_0

    .line 247
    .restart local v0    # "msg":Ljava/lang/String;
    :cond_4
    instance-of v2, p0, Ljava/io/IOException;

    if-eqz v2, :cond_3

    .line 248
    if-eqz v0, :cond_3

    .line 249
    const-string v2, "Connection already shutdown"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "Broken pipe"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method
