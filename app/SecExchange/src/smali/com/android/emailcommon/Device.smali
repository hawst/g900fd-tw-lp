.class public Lcom/android/emailcommon/Device;
.super Ljava/lang/Object;
.source "Device.java"


# static fields
.field private static sDeviceId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    sput-object v0, Lcom/android/emailcommon/Device;->sDeviceId:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkDeviceIdNumForKNOX(Landroid/content/Context;I)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "deviceIdNum"    # I

    .prologue
    .line 239
    invoke-static {p0}, Lcom/android/emailcommon/utility/Utility;->isInContainer(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    const-string v0, "Email"

    const-string v1, "containerized, set deviceIdNum for KNOX"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const/4 p1, 0x5

    .line 245
    :goto_0
    return p1

    .line 243
    :cond_0
    const-string v0, "Email"

    const-string v1, "non-containerized, set deviceIdNum for normal"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getConsistentDeviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/16 v8, 0xe

    .line 112
    const/4 v2, 0x0

    .line 114
    .local v2, "deviceType":I
    :try_start_0
    const-string v6, "phone"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 116
    .local v5, "tm":Landroid/telephony/TelephonyManager;
    if-nez v5, :cond_0

    .line 117
    const-string v6, "Email"

    const-string v7, "TelephonyManager is null in getConsistentDeviceId"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SEC"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 152
    .end local v5    # "tm":Landroid/telephony/TelephonyManager;
    :goto_0
    return-object v6

    .line 120
    .restart local v5    # "tm":Landroid/telephony/TelephonyManager;
    :cond_0
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    .line 121
    .local v1, "deviceId":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 122
    const-string v6, "Email"

    const-string v7, "tm.getDeviceId() is null in getConsistentDeviceId"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SEC"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 125
    :cond_1
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getPhoneType()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 126
    .local v4, "phoneType":I
    const/4 v6, 0x1

    if-ne v4, v6, :cond_2

    .line 127
    const/4 v2, 0x1

    .line 146
    :goto_1
    invoke-static {v1}, Lcom/android/emailcommon/utility/Utility;->getSmallHashForDeviceId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 147
    .local v0, "consistentDeviceId":Ljava/lang/String;
    const-string v6, "Email"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "return unique deviceID : SEC"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    invoke-static {p0, v2}, Lcom/android/emailcommon/Device;->checkDeviceIdNumForKNOX(Landroid/content/Context;I)I

    move-result v2

    .line 152
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SEC"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 128
    .end local v0    # "consistentDeviceId":Ljava/lang/String;
    :cond_2
    const/4 v6, 0x2

    if-ne v4, v6, :cond_4

    .line 129
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-ne v6, v8, :cond_3

    .line 130
    const/4 v2, 0x3

    goto :goto_1

    .line 132
    :cond_3
    const/4 v2, 0x2

    goto :goto_1

    .line 135
    :cond_4
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0xf

    if-ne v6, v7, :cond_5

    .line 136
    const/4 v2, 0x1

    goto :goto_1

    .line 137
    :cond_5
    invoke-virtual {v1}, Ljava/lang/String;->length()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v6

    if-ne v6, v8, :cond_6

    .line 138
    const/4 v2, 0x3

    goto :goto_1

    .line 140
    :cond_6
    const/4 v2, 0x2

    goto :goto_1

    .line 142
    .end local v1    # "deviceId":Ljava/lang/String;
    .end local v4    # "phoneType":I
    .end local v5    # "tm":Landroid/telephony/TelephonyManager;
    :catch_0
    move-exception v3

    .line 143
    .local v3, "e":Ljava/lang/Exception;
    const-string v6, "Email"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception in getConsistentDeviceId"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SEC"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0
.end method

.method public static declared-synchronized getDeviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    const-class v1, Lcom/android/emailcommon/Device;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/emailcommon/Device;->sDeviceId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 53
    invoke-static {p0}, Lcom/android/emailcommon/Device;->getDeviceIdInternal(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/Device;->sDeviceId:Ljava/lang/String;

    .line 55
    :cond_0
    sget-object v0, Lcom/android/emailcommon/Device;->sDeviceId:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static getDeviceIdInternal(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v9, 0x80

    .line 59
    if-nez p0, :cond_0

    .line 60
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v7, "getDeviceId requires a Context"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 63
    :cond_0
    const-string v6, "deviceName"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 64
    .local v0, "f":Ljava/io/File;
    const/4 v3, 0x0

    .line 66
    .local v3, "rdr":Ljava/io/BufferedReader;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 67
    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 69
    :try_start_0
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    const/16 v7, 0x80

    invoke-direct {v4, v6, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    .end local v3    # "rdr":Ljava/io/BufferedReader;
    .local v4, "rdr":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v1

    .line 72
    .local v1, "id":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 73
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 76
    :cond_1
    if-nez v1, :cond_4

    .line 79
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v6

    if-nez v6, :cond_6

    .line 80
    const-string v6, "Email"

    const-string v7, "Can\'t delete null deviceName file; try overwrite."

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v4

    .line 94
    .end local v1    # "id":Ljava/lang/String;
    .end local v4    # "rdr":Ljava/io/BufferedReader;
    .restart local v3    # "rdr":Ljava/io/BufferedReader;
    :cond_2
    :goto_0
    new-instance v5, Ljava/io/BufferedWriter;

    new-instance v6, Ljava/io/FileWriter;

    invoke-direct {v6, v0}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-direct {v5, v6, v9}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    .line 96
    .local v5, "w":Ljava/io/BufferedWriter;
    :try_start_2
    invoke-static {p0}, Lcom/android/emailcommon/Device;->getConsistentDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 97
    .restart local v1    # "id":Ljava/lang/String;
    invoke-virtual {v5, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 98
    invoke-virtual {v5}, Ljava/io/BufferedWriter;->flush()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 100
    invoke-virtual {v5}, Ljava/io/BufferedWriter;->close()V

    move-object v2, v1

    .line 102
    .end local v1    # "id":Ljava/lang/String;
    .end local v5    # "w":Ljava/io/BufferedWriter;
    .local v2, "id":Ljava/lang/String;
    :goto_1
    return-object v2

    .line 72
    .end local v2    # "id":Ljava/lang/String;
    :catchall_0
    move-exception v6

    :goto_2
    if-eqz v3, :cond_3

    .line 73
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    :cond_3
    throw v6

    .end local v3    # "rdr":Ljava/io/BufferedReader;
    .restart local v1    # "id":Ljava/lang/String;
    .restart local v4    # "rdr":Ljava/io/BufferedReader;
    :cond_4
    move-object v3, v4

    .end local v4    # "rdr":Ljava/io/BufferedReader;
    .restart local v3    # "rdr":Ljava/io/BufferedReader;
    move-object v2, v1

    .line 83
    .end local v1    # "id":Ljava/lang/String;
    .restart local v2    # "id":Ljava/lang/String;
    goto :goto_1

    .line 86
    .end local v2    # "id":Ljava/lang/String;
    :cond_5
    const-string v6, "Email"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ": File exists, but can\'t read?"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "  Trying to remove."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v6

    if-nez v6, :cond_2

    .line 89
    const-string v6, "Email"

    const-string v7, "Remove failed. Tring to overwrite."

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 100
    .restart local v5    # "w":Ljava/io/BufferedWriter;
    :catchall_1
    move-exception v6

    invoke-virtual {v5}, Ljava/io/BufferedWriter;->close()V

    throw v6

    .line 72
    .end local v3    # "rdr":Ljava/io/BufferedReader;
    .end local v5    # "w":Ljava/io/BufferedWriter;
    .restart local v4    # "rdr":Ljava/io/BufferedReader;
    :catchall_2
    move-exception v6

    move-object v3, v4

    .end local v4    # "rdr":Ljava/io/BufferedReader;
    .restart local v3    # "rdr":Ljava/io/BufferedReader;
    goto :goto_2

    .end local v3    # "rdr":Ljava/io/BufferedReader;
    .restart local v1    # "id":Ljava/lang/String;
    .restart local v4    # "rdr":Ljava/io/BufferedReader;
    :cond_6
    move-object v3, v4

    .end local v4    # "rdr":Ljava/io/BufferedReader;
    .restart local v3    # "rdr":Ljava/io/BufferedReader;
    goto :goto_0
.end method
