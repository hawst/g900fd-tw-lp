.class public Lcom/android/emailcommon/pgp/PGPHelper$FileAttachmentBody;
.super Lcom/android/emailcommon/pgp/PGPHelper$AttachmentBody;
.source "PGPHelper.java"

# interfaces
.implements Lcom/android/emailcommon/mail/Body;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/emailcommon/pgp/PGPHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FileAttachmentBody"
.end annotation


# instance fields
.field private mFile:Ljava/io/File;


# virtual methods
.method public getInputStream()Ljava/io/InputStream;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 825
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v3, p0, Lcom/android/emailcommon/pgp/PGPHelper$FileAttachmentBody;->mFile:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 827
    :goto_0
    return-object v2

    .line 826
    :catch_0
    move-exception v1

    .line 827
    .local v1, "fnfe":Ljava/io/FileNotFoundException;
    new-instance v2, Ljava/io/ByteArrayInputStream;

    const/4 v3, 0x0

    new-array v3, v3, [B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    goto :goto_0

    .line 828
    .end local v1    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 829
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/android/emailcommon/mail/MessagingException;

    const-string v3, "Invalid attachment err."

    invoke-direct {v2, v3}, Lcom/android/emailcommon/mail/MessagingException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
