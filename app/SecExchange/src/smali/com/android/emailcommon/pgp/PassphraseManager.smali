.class public Lcom/android/emailcommon/pgp/PassphraseManager;
.super Ljava/lang/Object;
.source "PassphraseManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public decryptContent(Ljava/io/InputStream;Ljava/io/OutputStream;[C)V
    .locals 6
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "out"    # Ljava/io/OutputStream;
    .param p3, "passPhrase"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getDecoderStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object p1

    .line 26
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;

    invoke-direct {v4, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;-><init>(Ljava/io/InputStream;)V

    .line 28
    .local v4, "pgpF":Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;
    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->nextObject()Ljava/lang/Object;

    move-result-object v2

    .line 30
    .local v2, "o":Ljava/lang/Object;
    instance-of v5, v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;

    if-eqz v5, :cond_1

    move-object v1, v2

    .line 31
    check-cast v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;

    .line 36
    .local v1, "enc":Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;
    :goto_0
    if-eqz v1, :cond_0

    .line 37
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;

    .line 38
    .local v3, "pbe":Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;
    new-instance v5, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v5}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-virtual {v3, p3, v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;->getDataStream([CLjava/security/Provider;)Ljava/io/InputStream;

    move-result-object v0

    .line 39
    .local v0, "clear":Ljava/io/InputStream;
    invoke-static {v0, p2}, Lcom/android/sec/org/bouncycastle/util/io/Streams;->pipeAll(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 42
    .end local v0    # "clear":Ljava/io/InputStream;
    .end local v3    # "pbe":Lcom/android/sec/org/bouncycastle/openpgp/PGPPBEEncryptedData;
    :cond_0
    invoke-virtual {p2}, Ljava/io/OutputStream;->close()V

    .line 43
    return-void

    .line 33
    .end local v1    # "enc":Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;
    :cond_1
    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->nextObject()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;

    .restart local v1    # "enc":Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;
    goto :goto_0
.end method
