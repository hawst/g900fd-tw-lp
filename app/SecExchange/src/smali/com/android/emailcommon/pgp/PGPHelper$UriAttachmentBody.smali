.class public Lcom/android/emailcommon/pgp/PGPHelper$UriAttachmentBody;
.super Lcom/android/emailcommon/pgp/PGPHelper$AttachmentBody;
.source "PGPHelper.java"

# interfaces
.implements Lcom/android/emailcommon/mail/Body;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/emailcommon/pgp/PGPHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UriAttachmentBody"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 842
    invoke-direct {p0}, Lcom/android/emailcommon/pgp/PGPHelper$AttachmentBody;-><init>()V

    .line 843
    iput-object p1, p0, Lcom/android/emailcommon/pgp/PGPHelper$UriAttachmentBody;->mContext:Landroid/content/Context;

    .line 844
    iput-object p2, p0, Lcom/android/emailcommon/pgp/PGPHelper$UriAttachmentBody;->mUri:Landroid/net/Uri;

    .line 845
    return-void
.end method


# virtual methods
.method public getInputStream()Ljava/io/InputStream;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 849
    :try_start_0
    iget-object v2, p0, Lcom/android/emailcommon/pgp/PGPHelper$UriAttachmentBody;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/android/emailcommon/pgp/PGPHelper$UriAttachmentBody;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 851
    :goto_0
    return-object v2

    .line 850
    :catch_0
    move-exception v1

    .line 851
    .local v1, "fnfe":Ljava/io/FileNotFoundException;
    new-instance v2, Ljava/io/ByteArrayInputStream;

    const/4 v3, 0x0

    new-array v3, v3, [B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    goto :goto_0

    .line 852
    .end local v1    # "fnfe":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 853
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/android/emailcommon/mail/MessagingException;

    const-string v3, "Invalid attachment err."

    invoke-direct {v2, v3}, Lcom/android/emailcommon/mail/MessagingException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
