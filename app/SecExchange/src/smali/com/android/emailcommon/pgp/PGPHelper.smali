.class public Lcom/android/emailcommon/pgp/PGPHelper;
.super Ljava/lang/Object;
.source "PGPHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/pgp/PGPHelper$UriAttachmentBody;,
        Lcom/android/emailcommon/pgp/PGPHelper$FileAttachmentBody;,
        Lcom/android/emailcommon/pgp/PGPHelper$AttachmentBody;,
        Lcom/android/emailcommon/pgp/PGPHelper$Attachment;,
        Lcom/android/emailcommon/pgp/PGPHelper$Message;
    }
.end annotation


# static fields
.field private static final PATTERN_ENDLINE_CRLF:Ljava/util/regex/Pattern;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final encryptLock:Ljava/util/concurrent/locks/Lock;

.field private mDecryptedFile:Ljava/io/File;

.field private mEncryptedContent:Ljava/io/File;

.field private mIsSignKeyExists:Z

.field private mSignature:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/android/emailcommon/pgp/PGPHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/pgp/PGPHelper;->TAG:Ljava/lang/String;

    .line 63
    const-string v0, "\n"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/pgp/PGPHelper;->PATTERN_ENDLINE_CRLF:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mIsSignKeyExists:Z

    .line 68
    iput-object v1, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    .line 69
    iput-object v1, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mSignature:Ljava/io/File;

    .line 70
    iput-object v1, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mDecryptedFile:Ljava/io/File;

    .line 76
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/android/emailcommon/pgp/PGPHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    .line 82
    return-void
.end method

.method static synthetic access$002(Lcom/android/emailcommon/pgp/PGPHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/emailcommon/pgp/PGPHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mIsSignKeyExists:Z

    return p1
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/android/emailcommon/pgp/PGPHelper;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private createEncryptedMimeMessage(Lcom/android/emailcommon/pgp/PGPHelper$Message;Ljava/io/File;Ljava/io/OutputStream;)V
    .locals 9
    .param p1, "msg"    # Lcom/android/emailcommon/pgp/PGPHelper$Message;
    .param p2, "encryptedFile"    # Ljava/io/File;
    .param p3, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;,
            Lcom/android/sec/org/bouncycastle/mail/smime/SMIMEException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 444
    const/4 v1, 0x0

    .line 446
    .local v1, "fileInputStrm":Ljava/io/FileInputStream;
    new-instance v3, Ljava/io/OutputStreamWriter;

    invoke-direct {v3, p3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    .line 447
    .local v3, "writer":Ljava/io/Writer;
    invoke-direct {p0, v3, p1}, Lcom/android/emailcommon/pgp/PGPHelper;->writeMessageHeader(Ljava/io/Writer;Lcom/android/emailcommon/pgp/PGPHelper$Message;)V

    .line 448
    const-string v4, "MIME-Version"

    const-string v5, "1.0"

    invoke-direct {p0, v3, v4, v5}, Lcom/android/emailcommon/pgp/PGPHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "--_com.android.email_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 454
    .local v0, "boundary":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuffer;

    const-string v4, "encrypted; protocol=\"application/pgp-encrypted\""

    invoke-direct {v2, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 456
    .local v2, "header":Ljava/lang/StringBuffer;
    const-string v4, "Content-Type"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "multipart/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; boundary=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lcom/android/emailcommon/pgp/PGPHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 460
    invoke-direct {p0, v3, v0, v8}, Lcom/android/emailcommon/pgp/PGPHelper;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 462
    const-string v4, "Content-Type"

    const-string v5, "application/pgp-encrypted"

    invoke-direct {p0, v3, v4, v5}, Lcom/android/emailcommon/pgp/PGPHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 463
    const-string v4, "Content-Transfer-Encoding"

    const-string v5, "7bit"

    invoke-direct {p0, v3, v4, v5}, Lcom/android/emailcommon/pgp/PGPHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 465
    const-string v4, "Version: 1\r\n"

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 467
    invoke-direct {p0, v3, v0, v8}, Lcom/android/emailcommon/pgp/PGPHelper;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 468
    const-string v4, "Content-Type"

    const-string v5, "application/octet-stream"

    invoke-direct {p0, v3, v4, v5}, Lcom/android/emailcommon/pgp/PGPHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    const-string v4, "Content-Transfer-Encoding"

    const-string v5, "7bit"

    invoke-direct {p0, v3, v4, v5}, Lcom/android/emailcommon/pgp/PGPHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    const-string v4, "Content-Disposition"

    const-string v5, "attachment; filename=\"encrypted.asc\""

    invoke-direct {p0, v3, v4, v5}, Lcom/android/emailcommon/pgp/PGPHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 472
    invoke-virtual {v3}, Ljava/io/Writer;->flush()V

    .line 474
    new-instance v1, Ljava/io/FileInputStream;

    .end local v1    # "fileInputStrm":Ljava/io/FileInputStream;
    invoke-direct {v1, p2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 475
    .restart local v1    # "fileInputStrm":Ljava/io/FileInputStream;
    invoke-static {v1, p3}, Lorg/apache/commons/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 476
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 477
    invoke-virtual {v3}, Ljava/io/Writer;->flush()V

    .line 479
    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 480
    const/4 v4, 0x1

    invoke-direct {p0, v3, v0, v4}, Lcom/android/emailcommon/pgp/PGPHelper;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 481
    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 482
    invoke-virtual {v3}, Ljava/io/Writer;->flush()V

    .line 483
    return-void
.end method

.method private createPGPEncryptedMessageWithMime(Landroid/content/Context;Lcom/android/emailcommon/pgp/PGPHelper$Message;Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lcom/android/emailcommon/pgp/PGPHelper$Message;
    .param p3, "in"    # Ljava/io/InputStream;
    .param p4, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 192
    const/4 v1, 0x0

    .line 194
    .local v1, "encStream":Ljava/io/OutputStream;
    :try_start_0
    new-instance v5, Ljava/io/BufferedOutputStream;

    const v6, 0x8000

    invoke-direct {v5, p4, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    .line 195
    .local v5, "outStream":Ljava/io/OutputStream;
    const-string v6, "tempEncryptedMessage"

    const/4 v7, 0x0

    invoke-static {}, Lcom/android/emailcommon/TempDirectory;->getTempDirectory()Ljava/io/File;

    move-result-object v8

    invoke-static {v6, v7, v8}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v6

    iput-object v6, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    .line 197
    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->deleteOnExit()V

    .line 198
    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    invoke-direct {v3, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 199
    .local v3, "encryptMsgStream":Ljava/io/OutputStream;
    new-instance v2, Ljava/io/BufferedOutputStream;

    const v6, 0x8000

    invoke-direct {v2, v3, v6}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    .end local v1    # "encStream":Ljava/io/OutputStream;
    .local v2, "encStream":Ljava/io/OutputStream;
    :try_start_1
    invoke-direct {p0, p1, p2, p3, v2}, Lcom/android/emailcommon/pgp/PGPHelper;->encryptContent(Landroid/content/Context;Lcom/android/emailcommon/pgp/PGPHelper$Message;Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 201
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 202
    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v6

    iput-object v6, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    .line 204
    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    invoke-direct {p0, p2, v6, v5}, Lcom/android/emailcommon/pgp/PGPHelper;->createEncryptedMimeMessage(Lcom/android/emailcommon/pgp/PGPHelper$Message;Ljava/io/File;Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 209
    if-eqz v2, :cond_0

    .line 210
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 212
    :cond_0
    if-eqz p3, :cond_1

    .line 214
    :try_start_2
    invoke-virtual {p3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 219
    :cond_1
    :goto_0
    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 220
    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 221
    iput-object v9, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    move-object v1, v2

    .line 224
    .end local v2    # "encStream":Ljava/io/OutputStream;
    .end local v3    # "encryptMsgStream":Ljava/io/OutputStream;
    .end local v5    # "outStream":Ljava/io/OutputStream;
    .restart local v1    # "encStream":Ljava/io/OutputStream;
    :cond_2
    :goto_1
    return-void

    .line 215
    .end local v1    # "encStream":Ljava/io/OutputStream;
    .restart local v2    # "encStream":Ljava/io/OutputStream;
    .restart local v3    # "encryptMsgStream":Ljava/io/OutputStream;
    .restart local v5    # "outStream":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 216
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 205
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "encStream":Ljava/io/OutputStream;
    .end local v3    # "encryptMsgStream":Ljava/io/OutputStream;
    .end local v5    # "outStream":Ljava/io/OutputStream;
    .restart local v1    # "encStream":Ljava/io/OutputStream;
    :catch_1
    move-exception v4

    .line 206
    .local v4, "ex":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    sget-object v6, Lcom/android/emailcommon/pgp/PGPHelper;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "createPGPEncryptedMessageWithMime- Exception - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 209
    if-eqz v1, :cond_3

    .line 210
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 212
    :cond_3
    if-eqz p3, :cond_4

    .line 214
    :try_start_4
    invoke-virtual {p3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 219
    :cond_4
    :goto_3
    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 220
    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 221
    iput-object v9, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    goto :goto_1

    .line 215
    :catch_2
    move-exception v0

    .line 216
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 209
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v4    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    :goto_4
    if-eqz v1, :cond_5

    .line 210
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 212
    :cond_5
    if-eqz p3, :cond_6

    .line 214
    :try_start_5
    invoke-virtual {p3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 219
    :cond_6
    :goto_5
    iget-object v7, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    if-eqz v7, :cond_7

    iget-object v7, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 220
    iget-object v7, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 221
    iput-object v9, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mEncryptedContent:Ljava/io/File;

    :cond_7
    throw v6

    .line 215
    :catch_3
    move-exception v0

    .line 216
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 209
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "encStream":Ljava/io/OutputStream;
    .restart local v2    # "encStream":Ljava/io/OutputStream;
    .restart local v3    # "encryptMsgStream":Ljava/io/OutputStream;
    .restart local v5    # "outStream":Ljava/io/OutputStream;
    :catchall_1
    move-exception v6

    move-object v1, v2

    .end local v2    # "encStream":Ljava/io/OutputStream;
    .restart local v1    # "encStream":Ljava/io/OutputStream;
    goto :goto_4

    .line 205
    .end local v1    # "encStream":Ljava/io/OutputStream;
    .restart local v2    # "encStream":Ljava/io/OutputStream;
    :catch_4
    move-exception v4

    move-object v1, v2

    .end local v2    # "encStream":Ljava/io/OutputStream;
    .restart local v1    # "encStream":Ljava/io/OutputStream;
    goto :goto_2

    .end local v1    # "encStream":Ljava/io/OutputStream;
    .restart local v2    # "encStream":Ljava/io/OutputStream;
    :cond_8
    move-object v1, v2

    .end local v2    # "encStream":Ljava/io/OutputStream;
    .restart local v1    # "encStream":Ljava/io/OutputStream;
    goto :goto_1
.end method

.method private createSignedMessageWithMime(Landroid/content/Context;Lcom/android/emailcommon/pgp/PGPHelper$Message;Ljava/io/File;Ljava/io/OutputStream;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lcom/android/emailcommon/pgp/PGPHelper$Message;
    .param p3, "tempMimeBodyFile"    # Ljava/io/File;
    .param p4, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 275
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, p3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const v6, 0x8000

    invoke-direct {v1, v5, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 278
    .local v1, "inStream":Ljava/io/InputStream;
    const/4 v3, 0x0

    .line 280
    .local v3, "signatureOutStream":Ljava/io/OutputStream;
    :try_start_0
    const-string v5, "tempSignature"

    const/4 v6, 0x0

    invoke-static {}, Lcom/android/emailcommon/TempDirectory;->getTempDirectory()Ljava/io/File;

    move-result-object v7

    invoke-static {v5, v6, v7}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v5

    iput-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mSignature:Ljava/io/File;

    .line 282
    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mSignature:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->deleteOnExit()V

    .line 283
    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mSignature:Ljava/io/File;

    invoke-direct {v2, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 284
    .local v2, "msgSignatureStream":Ljava/io/OutputStream;
    new-instance v4, Ljava/io/BufferedOutputStream;

    const/16 v5, 0x400

    invoke-direct {v4, v2, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    .end local v3    # "signatureOutStream":Ljava/io/OutputStream;
    .local v4, "signatureOutStream":Ljava/io/OutputStream;
    :try_start_1
    invoke-direct {p0, p1, p2, v1, v4}, Lcom/android/emailcommon/pgp/PGPHelper;->generateSignature(Landroid/content/Context;Lcom/android/emailcommon/pgp/PGPHelper$Message;Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 286
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 287
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 290
    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mSignature:Ljava/io/File;

    invoke-direct {p0, p2, p3, v5, p4}, Lcom/android/emailcommon/pgp/PGPHelper;->createSignedMimeMessage(Lcom/android/emailcommon/pgp/PGPHelper$Message;Ljava/io/File;Ljava/io/File;Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 295
    if-eqz v4, :cond_0

    .line 296
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 298
    :cond_0
    if-eqz v1, :cond_1

    .line 299
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 301
    :cond_1
    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mSignature:Ljava/io/File;

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mSignature:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 302
    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mSignature:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 303
    iput-object v8, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mSignature:Ljava/io/File;

    move-object v3, v4

    .line 306
    .end local v2    # "msgSignatureStream":Ljava/io/OutputStream;
    .end local v4    # "signatureOutStream":Ljava/io/OutputStream;
    .restart local v3    # "signatureOutStream":Ljava/io/OutputStream;
    :cond_2
    :goto_0
    return-void

    .line 291
    :catch_0
    move-exception v0

    .line 292
    .local v0, "ex":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    sget-object v5, Lcom/android/emailcommon/pgp/PGPHelper;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "createSignedMessageWithMime - Exception - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 295
    if-eqz v3, :cond_3

    .line 296
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    .line 298
    :cond_3
    if-eqz v1, :cond_4

    .line 299
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 301
    :cond_4
    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mSignature:Ljava/io/File;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mSignature:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 302
    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mSignature:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 303
    iput-object v8, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mSignature:Ljava/io/File;

    goto :goto_0

    .line 295
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    :goto_2
    if-eqz v3, :cond_5

    .line 296
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    .line 298
    :cond_5
    if-eqz v1, :cond_6

    .line 299
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 301
    :cond_6
    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mSignature:Ljava/io/File;

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mSignature:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 302
    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mSignature:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 303
    iput-object v8, p0, Lcom/android/emailcommon/pgp/PGPHelper;->mSignature:Ljava/io/File;

    :cond_7
    throw v5

    .line 295
    .end local v3    # "signatureOutStream":Ljava/io/OutputStream;
    .restart local v2    # "msgSignatureStream":Ljava/io/OutputStream;
    .restart local v4    # "signatureOutStream":Ljava/io/OutputStream;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "signatureOutStream":Ljava/io/OutputStream;
    .restart local v3    # "signatureOutStream":Ljava/io/OutputStream;
    goto :goto_2

    .line 291
    .end local v3    # "signatureOutStream":Ljava/io/OutputStream;
    .restart local v4    # "signatureOutStream":Ljava/io/OutputStream;
    :catch_1
    move-exception v0

    move-object v3, v4

    .end local v4    # "signatureOutStream":Ljava/io/OutputStream;
    .restart local v3    # "signatureOutStream":Ljava/io/OutputStream;
    goto :goto_1

    .end local v3    # "signatureOutStream":Ljava/io/OutputStream;
    .restart local v4    # "signatureOutStream":Ljava/io/OutputStream;
    :cond_8
    move-object v3, v4

    .end local v4    # "signatureOutStream":Ljava/io/OutputStream;
    .restart local v3    # "signatureOutStream":Ljava/io/OutputStream;
    goto :goto_0
.end method

.method private createSignedMimeMessage(Lcom/android/emailcommon/pgp/PGPHelper$Message;Ljava/io/File;Ljava/io/File;Ljava/io/OutputStream;)V
    .locals 9
    .param p1, "msg"    # Lcom/android/emailcommon/pgp/PGPHelper$Message;
    .param p2, "tempMimeBodyFile"    # Ljava/io/File;
    .param p3, "signatureFile"    # Ljava/io/File;
    .param p4, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 311
    const/4 v1, 0x0

    .line 313
    .local v1, "fileInputStrm":Ljava/io/FileInputStream;
    new-instance v3, Ljava/io/OutputStreamWriter;

    invoke-direct {v3, p4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    .line 314
    .local v3, "writer":Ljava/io/Writer;
    invoke-direct {p0, v3, p1}, Lcom/android/emailcommon/pgp/PGPHelper;->writeMessageHeader(Ljava/io/Writer;Lcom/android/emailcommon/pgp/PGPHelper$Message;)V

    .line 315
    const-string v4, "MIME-Version"

    const-string v5, "1.0"

    invoke-direct {p0, v3, v4, v5}, Lcom/android/emailcommon/pgp/PGPHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "--_com.android.email_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 321
    .local v0, "boundary":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuffer;

    const-string v4, "signed; micalg=pgp-sha1; protocol=\"application/pgp-signature\""

    invoke-direct {v2, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 323
    .local v2, "header":Ljava/lang/StringBuffer;
    const-string v4, "Content-Type"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "multipart/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; boundary=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lcom/android/emailcommon/pgp/PGPHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 327
    invoke-direct {p0, v3, v0, v8}, Lcom/android/emailcommon/pgp/PGPHelper;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 331
    invoke-virtual {v3}, Ljava/io/Writer;->flush()V

    .line 333
    new-instance v1, Ljava/io/FileInputStream;

    .end local v1    # "fileInputStrm":Ljava/io/FileInputStream;
    invoke-direct {v1, p2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 334
    .restart local v1    # "fileInputStrm":Ljava/io/FileInputStream;
    invoke-static {v1, p4}, Lorg/apache/commons/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 335
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 336
    invoke-virtual {v3}, Ljava/io/Writer;->flush()V

    .line 337
    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 339
    invoke-direct {p0, v3, v0, v8}, Lcom/android/emailcommon/pgp/PGPHelper;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 342
    const-string v4, "Content-Type"

    const-string v5, "application/pgp-signature"

    invoke-direct {p0, v3, v4, v5}, Lcom/android/emailcommon/pgp/PGPHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    const-string v4, "Content-Transfer-Encoding"

    const-string v5, "7bit"

    invoke-direct {p0, v3, v4, v5}, Lcom/android/emailcommon/pgp/PGPHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const-string v4, "Content-Disposition"

    const-string v5, "attachment; filename=\"signature.asc\""

    invoke-direct {p0, v3, v4, v5}, Lcom/android/emailcommon/pgp/PGPHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 347
    invoke-virtual {v3}, Ljava/io/Writer;->flush()V

    .line 349
    new-instance v1, Ljava/io/FileInputStream;

    .end local v1    # "fileInputStrm":Ljava/io/FileInputStream;
    invoke-direct {v1, p3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 350
    .restart local v1    # "fileInputStrm":Ljava/io/FileInputStream;
    invoke-static {v1, p4}, Lorg/apache/commons/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 351
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 352
    invoke-virtual {v3}, Ljava/io/Writer;->flush()V

    .line 354
    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 355
    const/4 v4, 0x1

    invoke-direct {p0, v3, v0, v4}, Lcom/android/emailcommon/pgp/PGPHelper;->writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V

    .line 356
    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 357
    invoke-virtual {v3}, Ljava/io/Writer;->flush()V

    .line 358
    return-void
.end method

.method private encryptContent(Landroid/content/Context;Lcom/android/emailcommon/pgp/PGPHelper$Message;Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 32
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "msg"    # Lcom/android/emailcommon/pgp/PGPHelper$Message;
    .param p3, "in"    # Ljava/io/InputStream;
    .param p4, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchProviderException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/SignatureException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Lcom/android/emailcommon/pgp/PGPKeyException;
        }
    .end annotation

    .prologue
    .line 364
    :try_start_0
    new-instance v29, Lcom/android/emailcommon/pgp/PGPManager;

    const/4 v3, 0x1

    move-object/from16 v0, v29

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/android/emailcommon/pgp/PGPManager;-><init>(Landroid/content/Context;Z)V

    .line 365
    .local v29, "pgpMgr":Lcom/android/emailcommon/pgp/PGPManager;
    new-instance v30, Lcom/android/emailcommon/pgp/PGPHelper$1;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/emailcommon/pgp/PGPHelper$1;-><init>(Lcom/android/emailcommon/pgp/PGPHelper;)V

    .line 372
    .local v30, "pgpUpdatr":Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;
    invoke-virtual/range {v29 .. v30}, Lcom/android/emailcommon/pgp/PGPManager;->setPGPUpdater(Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;)V

    .line 374
    const/16 v27, 0x0

    .line 375
    .local v27, "len":I
    const/16 v31, 0x0

    .line 376
    .local v31, "strTok":Ljava/util/StringTokenizer;
    new-instance v31, Ljava/util/StringTokenizer;

    .end local v31    # "strTok":Ljava/util/StringTokenizer;
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mKeyIds:Ljava/lang/String;

    const-string v4, ";"

    move-object/from16 v0, v31

    invoke-direct {v0, v3, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    .restart local v31    # "strTok":Ljava/util/StringTokenizer;
    :goto_0
    invoke-virtual/range {v31 .. v31}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 378
    add-int/lit8 v27, v27, 0x1

    .line 379
    invoke-virtual/range {v31 .. v31}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 415
    .end local v27    # "len":I
    .end local v29    # "pgpMgr":Lcom/android/emailcommon/pgp/PGPManager;
    .end local v30    # "pgpUpdatr":Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;
    .end local v31    # "strTok":Ljava/util/StringTokenizer;
    :catchall_0
    move-exception v3

    invoke-virtual/range {p3 .. p3}, Ljava/io/InputStream;->close()V

    throw v3

    .line 381
    .restart local v27    # "len":I
    .restart local v29    # "pgpMgr":Lcom/android/emailcommon/pgp/PGPManager;
    .restart local v30    # "pgpUpdatr":Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;
    .restart local v31    # "strTok":Ljava/util/StringTokenizer;
    :cond_0
    const/4 v11, 0x0

    .line 382
    .local v11, "encKeyIds":[J
    const-wide/16 v6, 0x0

    .line 383
    .local v6, "secretKey":J
    :try_start_1
    move-object/from16 v0, p2

    iget-boolean v3, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mSigned:Z

    if-eqz v3, :cond_2

    .line 384
    add-int/lit8 v3, v27, -0x1

    new-array v11, v3, [J

    .line 388
    :goto_1
    const/16 v25, 0x0

    .line 389
    .local v25, "i":I
    new-instance v31, Ljava/util/StringTokenizer;

    .end local v31    # "strTok":Ljava/util/StringTokenizer;
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mKeyIds:Ljava/lang/String;

    const-string v4, ";"

    move-object/from16 v0, v31

    invoke-direct {v0, v3, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    .restart local v31    # "strTok":Ljava/util/StringTokenizer;
    :goto_2
    invoke-virtual/range {v31 .. v31}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 391
    add-int/lit8 v3, v27, -0x1

    move/from16 v0, v25

    if-ne v0, v3, :cond_3

    move-object/from16 v0, p2

    iget-boolean v3, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mSigned:Z

    if-eqz v3, :cond_3

    .line 392
    invoke-virtual/range {v31 .. v31}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 398
    :cond_1
    invoke-virtual/range {v29 .. v29}, Lcom/android/emailcommon/pgp/PGPManager;->getPGPKeyManager()Lcom/android/emailcommon/pgp/PGPKeyManager;

    move-result-object v28

    .line 400
    .local v28, "pgpKeyMgr":Lcom/android/emailcommon/pgp/PGPKeyManager;
    const/4 v5, 0x0

    .line 401
    .local v5, "passPharse":Ljava/lang/String;
    move-object/from16 v0, p2

    iget-boolean v3, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mSigned:Z

    if-eqz v3, :cond_4

    .line 402
    const/16 v31, 0x0

    .line 403
    new-instance v31, Ljava/util/StringTokenizer;

    .end local v31    # "strTok":Ljava/util/StringTokenizer;
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mFromAddress:Ljava/lang/String;

    const-string v4, "\u0002"

    move-object/from16 v0, v31

    invoke-direct {v0, v3, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    .restart local v31    # "strTok":Ljava/util/StringTokenizer;
    invoke-static/range {p1 .. p1}, Lcom/android/emailcommon/Device;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v26

    .line 405
    .local v26, "keystorepaswrd":Ljava/lang/String;
    move-object/from16 v0, v28

    move-object/from16 v1, v26

    invoke-virtual {v0, v6, v7, v1}, Lcom/android/emailcommon/pgp/PGPKeyManager;->getPassPhrase(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 406
    new-instance v2, Lcom/android/emailcommon/pgp/EncryptSignParams;

    const/4 v8, 0x1

    const-wide/16 v9, 0x0

    const/4 v12, 0x0

    const-string v13, "encrypted.asc"

    new-instance v14, Ljava/util/Date;

    invoke-direct {v14}, Ljava/util/Date;-><init>()V

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-direct/range {v2 .. v14}, Lcom/android/emailcommon/pgp/EncryptSignParams;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;JZJ[JLjava/lang/String;Ljava/lang/String;Ljava/util/Date;)V

    .line 408
    .local v2, "params":Lcom/android/emailcommon/pgp/EncryptSignParams;
    const/4 v3, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v0, v2, v3}, Lcom/android/emailcommon/pgp/PGPManager;->encryptSign(Lcom/android/emailcommon/pgp/EncryptSignParams;Z)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 415
    .end local v26    # "keystorepaswrd":Ljava/lang/String;
    :goto_3
    invoke-virtual/range {p3 .. p3}, Ljava/io/InputStream;->close()V

    .line 417
    return-void

    .line 386
    .end local v2    # "params":Lcom/android/emailcommon/pgp/EncryptSignParams;
    .end local v5    # "passPharse":Ljava/lang/String;
    .end local v25    # "i":I
    .end local v28    # "pgpKeyMgr":Lcom/android/emailcommon/pgp/PGPKeyManager;
    :cond_2
    :try_start_2
    move/from16 v0, v27

    new-array v11, v0, [J

    goto :goto_1

    .line 395
    .restart local v25    # "i":I
    :cond_3
    invoke-virtual/range {v31 .. v31}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    aput-wide v8, v11, v25

    .line 396
    add-int/lit8 v25, v25, 0x1

    goto :goto_2

    .line 410
    .restart local v5    # "passPharse":Ljava/lang/String;
    .restart local v28    # "pgpKeyMgr":Lcom/android/emailcommon/pgp/PGPKeyManager;
    :cond_4
    new-instance v2, Lcom/android/emailcommon/pgp/EncryptSignParams;

    const-wide/16 v16, 0x0

    const/16 v18, 0x1

    const-wide/16 v19, 0x0

    const/16 v22, 0x0

    const-string v23, "encrypted.asc"

    new-instance v24, Ljava/util/Date;

    invoke-direct/range {v24 .. v24}, Ljava/util/Date;-><init>()V

    move-object v12, v2

    move-object/from16 v13, p3

    move-object/from16 v14, p4

    move-object v15, v5

    move-object/from16 v21, v11

    invoke-direct/range {v12 .. v24}, Lcom/android/emailcommon/pgp/EncryptSignParams;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;JZJ[JLjava/lang/String;Ljava/lang/String;Ljava/util/Date;)V

    .line 412
    .restart local v2    # "params":Lcom/android/emailcommon/pgp/EncryptSignParams;
    const/4 v3, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v2, v3}, Lcom/android/emailcommon/pgp/PGPManager;->encryptSign(Lcom/android/emailcommon/pgp/EncryptSignParams;Z)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3
.end method

.method private generateSignature(Landroid/content/Context;Lcom/android/emailcommon/pgp/PGPHelper$Message;Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lcom/android/emailcommon/pgp/PGPHelper$Message;
    .param p3, "inStream"    # Ljava/io/InputStream;
    .param p4, "signatureOutStream"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/SignatureException;,
            Ljava/security/NoSuchProviderException;,
            Ljava/security/NoSuchAlgorithmException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Lcom/android/emailcommon/pgp/PGPKeyException;
        }
    .end annotation

    .prologue
    .line 422
    new-instance v17, Lcom/android/emailcommon/pgp/PGPManager;

    const/4 v3, 0x1

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/android/emailcommon/pgp/PGPManager;-><init>(Landroid/content/Context;Z)V

    .line 423
    .local v17, "pgpMgr":Lcom/android/emailcommon/pgp/PGPManager;
    new-instance v18, Lcom/android/emailcommon/pgp/PGPHelper$2;

    invoke-direct/range {v18 .. v19}, Lcom/android/emailcommon/pgp/PGPHelper$2;-><init>(Lcom/android/emailcommon/pgp/PGPHelper;)V

    .line 430
    .local v18, "pgpUpdatr":Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;
    invoke-virtual/range {v17 .. v18}, Lcom/android/emailcommon/pgp/PGPManager;->setPGPUpdater(Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;)V

    .line 431
    invoke-virtual/range {v17 .. v17}, Lcom/android/emailcommon/pgp/PGPManager;->getPGPKeyManager()Lcom/android/emailcommon/pgp/PGPKeyManager;

    move-result-object v16

    .line 433
    .local v16, "pgpKeyMgr":Lcom/android/emailcommon/pgp/PGPKeyManager;
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mKeyIds:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 434
    .local v6, "secKeyId":J
    invoke-static/range {p1 .. p1}, Lcom/android/emailcommon/Device;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    .line 435
    .local v15, "keystorepaswrd":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v7, v15}, Lcom/android/emailcommon/pgp/PGPKeyManager;->getPassPhrase(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 436
    .local v5, "passPharse":Ljava/lang/String;
    new-instance v2, Lcom/android/emailcommon/pgp/EncryptSignParams;

    const/4 v8, 0x1

    const-wide/16 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    new-instance v14, Ljava/util/Date;

    invoke-direct {v14}, Ljava/util/Date;-><init>()V

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-direct/range {v2 .. v14}, Lcom/android/emailcommon/pgp/EncryptSignParams;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;JZJ[JLjava/lang/String;Ljava/lang/String;Ljava/util/Date;)V

    .line 438
    .local v2, "params":Lcom/android/emailcommon/pgp/EncryptSignParams;
    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Lcom/android/emailcommon/pgp/PGPManager;->signOnly(Lcom/android/emailcommon/pgp/EncryptSignParams;Z)I

    .line 439
    return-void
.end method

.method private handleAttachmentPGP(Lcom/android/emailcommon/mail/Part;Lcom/android/emailcommon/pgp/PGPHelper$Message;)V
    .locals 16
    .param p1, "part"    # Lcom/android/emailcommon/mail/Part;
    .param p2, "message"    # Lcom/android/emailcommon/pgp/PGPHelper$Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;,
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 708
    new-instance v2, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;

    invoke-direct {v2}, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;-><init>()V

    .line 709
    .local v2, "att":Lcom/android/emailcommon/pgp/PGPHelper$Attachment;
    const-string v13, "X-MS-UrlCompName"

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Lcom/android/emailcommon/mail/Part;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 710
    .local v12, "values":[Ljava/lang/String;
    if-eqz v12, :cond_0

    array-length v13, v12

    if-nez v13, :cond_4

    :cond_0
    const/4 v10, 0x0

    .line 711
    .local v10, "urlCompName":Ljava/lang/String;
    :goto_0
    if-eqz v10, :cond_5

    .line 712
    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 713
    .local v9, "url":Landroid/net/Uri;
    invoke-virtual {v9}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v13

    iput-object v13, v2, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mFileName:Ljava/lang/String;

    .line 718
    .end local v9    # "url":Landroid/net/Uri;
    :goto_1
    invoke-interface/range {p1 .. p1}, Lcom/android/emailcommon/mail/Part;->getSize()I

    move-result v13

    iput v13, v2, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mSize:I

    .line 719
    const-string v13, "Content-Transfer-Encoding"

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Lcom/android/emailcommon/mail/Part;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 720
    .local v5, "headerValues":[Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 721
    const/4 v13, 0x0

    aget-object v13, v5, v13

    iput-object v13, v2, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mEncoding:Ljava/lang/String;

    .line 723
    :cond_1
    const-string v13, "Content-ID"

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Lcom/android/emailcommon/mail/Part;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_2

    .line 724
    const-string v13, "Content-ID"

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Lcom/android/emailcommon/mail/Part;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 725
    .local v4, "contentId":[Ljava/lang/String;
    const/4 v13, 0x0

    aget-object v13, v4, v13

    const/4 v14, 0x1

    const/4 v15, 0x0

    aget-object v15, v4, v15

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    invoke-virtual {v13, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    iput-object v13, v2, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mContentId:Ljava/lang/String;

    .line 727
    .end local v4    # "contentId":[Ljava/lang/String;
    :cond_2
    const-string v13, "Content-Disposition"

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Lcom/android/emailcommon/mail/Part;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_6

    .line 728
    const-string v13, "Content-Disposition"

    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Lcom/android/emailcommon/mail/Part;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 729
    .local v3, "contentDispositionHeader":[Ljava/lang/String;
    const/4 v13, 0x0

    aget-object v13, v3, v13

    const-string v14, ";"

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 730
    .local v8, "tokens":[Ljava/lang/String;
    move-object v1, v8

    .local v1, "arr$":[Ljava/lang/String;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_2
    if-ge v6, v7, :cond_6

    aget-object v11, v1, v6

    .line 731
    .local v11, "val":Ljava/lang/String;
    const-string v13, "inline"

    invoke-virtual {v13, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 732
    const/4 v13, 0x1

    iput v13, v2, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mIsInline:I

    .line 730
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 710
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v3    # "contentDispositionHeader":[Ljava/lang/String;
    .end local v5    # "headerValues":[Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    .end local v8    # "tokens":[Ljava/lang/String;
    .end local v10    # "urlCompName":Ljava/lang/String;
    .end local v11    # "val":Ljava/lang/String;
    :cond_4
    const/4 v13, 0x0

    aget-object v10, v12, v13

    goto :goto_0

    .line 715
    .restart local v10    # "urlCompName":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {p0 .. p1}, Lcom/android/emailcommon/pgp/PGPHelper;->getFileName(Lcom/android/emailcommon/mail/Part;)Ljava/lang/String;

    move-result-object v13

    iput-object v13, v2, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mFileName:Ljava/lang/String;

    .line 716
    iget-object v13, v2, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mFileName:Ljava/lang/String;

    invoke-static {v13}, Lcom/android/emailcommon/internet/MimeUtility;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    iput-object v13, v2, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mFileName:Ljava/lang/String;

    goto :goto_1

    .line 736
    .restart local v5    # "headerValues":[Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p1

    iput-object v0, v2, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mAttachmentBody:Lcom/android/emailcommon/mail/Part;

    .line 737
    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mAttachments:Ljava/util/ArrayList;

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 738
    return-void
.end method

.method private handleMultipartPGP(Lcom/android/emailcommon/mail/Multipart;Lcom/android/emailcommon/pgp/PGPHelper$Message;)V
    .locals 3
    .param p1, "part"    # Lcom/android/emailcommon/mail/Multipart;
    .param p2, "message"    # Lcom/android/emailcommon/pgp/PGPHelper$Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;,
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 742
    invoke-virtual {p1}, Lcom/android/emailcommon/mail/Multipart;->getCount()I

    move-result v1

    .line 743
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 744
    invoke-virtual {p1, v2}, Lcom/android/emailcommon/mail/Multipart;->getBodyPart(I)Lcom/android/emailcommon/mail/BodyPart;

    move-result-object v0

    .line 745
    .local v0, "bodyPart":Lcom/android/emailcommon/mail/BodyPart;
    invoke-virtual {p0, v0, p2}, Lcom/android/emailcommon/pgp/PGPHelper;->handleBodyPartPGP(Lcom/android/emailcommon/mail/Part;Lcom/android/emailcommon/pgp/PGPHelper$Message;)V

    .line 743
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 747
    .end local v0    # "bodyPart":Lcom/android/emailcommon/mail/BodyPart;
    :cond_0
    return-void
.end method

.method private static validateText(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 554
    if-nez p0, :cond_0

    .line 555
    const/4 v1, 0x0

    .line 558
    :goto_0
    return-object v1

    .line 557
    :cond_0
    sget-object v1, Lcom/android/emailcommon/pgp/PGPHelper;->PATTERN_ENDLINE_CRLF:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 558
    .local v0, "matcher":Ljava/util/regex/Matcher;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\r\n"

    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "writer"    # Ljava/io/Writer;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 894
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 895
    invoke-virtual {p0, p1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 896
    const-string v1, ": "

    invoke-virtual {p0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 897
    invoke-static {p2}, Lcom/android/emailcommon/mail/Address;->packedToHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 898
    .local v0, "packedToHeader":Ljava/lang/String;
    if-eqz v0, :cond_1

    .end local v0    # "packedToHeader":Ljava/lang/String;
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MimeUtility;->fold(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 899
    const-string v1, "\r\n"

    invoke-virtual {p0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 901
    :cond_0
    return-void

    .line 898
    .restart local v0    # "packedToHeader":Ljava/lang/String;
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method private writeBoundary(Ljava/io/Writer;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "writer"    # Ljava/io/Writer;
    .param p2, "boundary"    # Ljava/lang/String;
    .param p3, "end"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 578
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 579
    const-string v0, "--"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 580
    invoke-virtual {p1, p2}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 581
    if-eqz p3, :cond_0

    .line 582
    const-string v0, "--"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 584
    :cond_0
    if-nez p3, :cond_1

    .line 585
    const-string v0, "\r\n"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 587
    :cond_1
    return-void
.end method

.method private static writeEncodedHeader2(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "writer"    # Ljava/io/Writer;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 905
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 906
    invoke-virtual {p0, p1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 907
    const-string v1, ": "

    invoke-virtual {p0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 908
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-static {p2, v1}, Lcom/android/emailcommon/internet/MimeUtility;->foldAndEncode2(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 910
    .local v0, "foldedString":Ljava/lang/String;
    const-string v1, "\r\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 912
    invoke-virtual {p0, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 913
    const-string v1, "\r\n"

    invoke-virtual {p0, v1}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 915
    .end local v0    # "foldedString":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "writer"    # Ljava/io/Writer;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 562
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 563
    invoke-virtual {p1, p2}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 564
    const-string v0, ": "

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 565
    const-string v0, "Content-ID"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 566
    const-string v0, "<"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 568
    :cond_0
    invoke-virtual {p1, p3}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 569
    const-string v0, "Content-ID"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 570
    const-string v0, ">"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 572
    :cond_1
    const-string v0, "\r\n"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 574
    :cond_2
    return-void
.end method

.method private writeMessageHeader(Ljava/io/Writer;Lcom/android/emailcommon/pgp/PGPHelper$Message;)V
    .locals 7
    .param p1, "writer"    # Ljava/io/Writer;
    .param p2, "message"    # Lcom/android/emailcommon/pgp/PGPHelper$Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 863
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 890
    :cond_0
    :goto_0
    return-void

    .line 867
    :cond_1
    const-string v2, "To"

    iget-object v3, p2, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mTo:[Lcom/android/emailcommon/mail/Address;

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/pgp/PGPHelper;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 869
    const-string v2, "From"

    new-array v3, v6, [Lcom/android/emailcommon/mail/Address;

    iget-object v4, p2, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mFrom:Lcom/android/emailcommon/mail/Address;

    aput-object v4, v3, v5

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/pgp/PGPHelper;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    const-string v2, "Cc"

    iget-object v3, p2, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mCC:[Lcom/android/emailcommon/mail/Address;

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/pgp/PGPHelper;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    const-string v2, "Bcc"

    iget-object v3, p2, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mBCC:[Lcom/android/emailcommon/mail/Address;

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/pgp/PGPHelper;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 873
    const-string v2, "Reply-To"

    iget-object v3, p2, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mReplyTo:[Lcom/android/emailcommon/mail/Address;

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/pgp/PGPHelper;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    const-string v2, "Subject"

    iget-object v3, p2, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mSubject:Ljava/lang/String;

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/pgp/PGPHelper;->writeEncodedHeader2(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    const-string v2, "Message-ID"

    iget-object v3, p2, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mMessageID:Ljava/lang/String;

    invoke-direct {p0, p1, v2, v3}, Lcom/android/emailcommon/pgp/PGPHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "EEE, dd MMM yyyy HH:mm:ss Z"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 879
    .local v1, "dateFormat":Ljava/text/SimpleDateFormat;
    new-instance v2, Ljava/util/Date;

    iget-object v3, p2, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mDate:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/util/Date;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 881
    .local v0, "date":Ljava/lang/String;
    const-string v2, "Date"

    invoke-direct {p0, p1, v2, v0}, Lcom/android/emailcommon/pgp/PGPHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 883
    iget-boolean v2, p2, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mDelivery:Z

    if-eqz v2, :cond_2

    .line 884
    const-string v2, "Return-Receipt-To"

    new-array v3, v6, [Lcom/android/emailcommon/mail/Address;

    iget-object v4, p2, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mFrom:Lcom/android/emailcommon/mail/Address;

    aput-object v4, v3, v5

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/pgp/PGPHelper;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 886
    :cond_2
    iget-boolean v2, p2, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mRead:Z

    if-eqz v2, :cond_3

    .line 887
    const-string v2, "Disposition-Notification-To"

    new-array v3, v6, [Lcom/android/emailcommon/mail/Address;

    iget-object v4, p2, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mFrom:Lcom/android/emailcommon/mail/Address;

    aput-object v4, v3, v5

    invoke-static {v3}, Lcom/android/emailcommon/mail/Address;->pack([Lcom/android/emailcommon/mail/Address;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/android/emailcommon/pgp/PGPHelper;->writeAddressHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    :cond_3
    const-string v2, "Importance"

    iget-object v3, p2, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mImportance:Ljava/lang/String;

    invoke-direct {p0, p1, v2, v3}, Lcom/android/emailcommon/pgp/PGPHelper;->writeHeader(Ljava/io/Writer;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public createMimeBodyPart(Landroid/content/Context;Lcom/android/emailcommon/pgp/PGPHelper$Message;)Lcom/android/emailcommon/internet/MimeBodyPart;
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lcom/android/emailcommon/pgp/PGPHelper$Message;

    .prologue
    .line 487
    const/4 v10, 0x0

    .line 489
    .local v10, "ret":Lcom/android/emailcommon/internet/MimeBodyPart;
    :try_start_0
    new-instance v11, Lcom/android/emailcommon/internet/TextBody;

    move-object/from16 v0, p2

    iget-boolean v14, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mIsHtml:Z

    if-eqz v14, :cond_3

    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mHtmlBodyText:Ljava/lang/String;

    invoke-static {v14}, Lcom/android/emailcommon/pgp/PGPHelper;->validateText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    :goto_0
    invoke-direct {v11, v14}, Lcom/android/emailcommon/internet/TextBody;-><init>(Ljava/lang/String;)V

    .line 491
    .local v11, "textBody":Lcom/android/emailcommon/internet/TextBody;
    new-instance v3, Lcom/android/emailcommon/internet/MimeBodyPart;

    move-object/from16 v0, p2

    iget-boolean v14, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mIsHtml:Z

    if-eqz v14, :cond_4

    const-string v14, "text/html; charset=\"utf-8\""

    :goto_1
    invoke-direct {v3, v11, v14}, Lcom/android/emailcommon/internet/MimeBodyPart;-><init>(Lcom/android/emailcommon/mail/Body;Ljava/lang/String;)V

    .line 494
    .local v3, "body":Lcom/android/emailcommon/internet/MimeBodyPart;
    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mAttachments:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 495
    .local v5, "count":I
    if-lez v5, :cond_7

    .line 496
    new-instance v9, Lcom/android/emailcommon/internet/MimeMultipart;

    invoke-direct {v9}, Lcom/android/emailcommon/internet/MimeMultipart;-><init>()V

    .line 497
    .local v9, "multiPart":Lcom/android/emailcommon/internet/MimeMultipart;
    invoke-virtual {v9, v3}, Lcom/android/emailcommon/internet/MimeMultipart;->addBodyPart(Lcom/android/emailcommon/mail/BodyPart;)V

    .line 498
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_2
    if-ge v8, v5, :cond_6

    .line 499
    const/4 v2, 0x0

    .line 500
    .local v2, "attPart":Lcom/android/emailcommon/internet/MimeBodyPart;
    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mAttachments:Ljava/util/ArrayList;

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;

    .line 502
    .local v1, "att":Lcom/android/emailcommon/pgp/PGPHelper$Attachment;
    iget-object v14, v1, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mUri:Landroid/net/Uri;

    if-eqz v14, :cond_2

    .line 503
    new-instance v13, Lcom/android/emailcommon/pgp/PGPHelper$UriAttachmentBody;

    iget-object v14, v1, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mUri:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-direct {v13, v0, v14}, Lcom/android/emailcommon/pgp/PGPHelper$UriAttachmentBody;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 504
    .local v13, "uriAttmt":Lcom/android/emailcommon/pgp/PGPHelper$UriAttachmentBody;
    iget-object v14, v1, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mUri:Landroid/net/Uri;

    iget-object v15, v1, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/android/emailcommon/smime/UriSchemaUtils;->getFile(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Lcom/android/emailcommon/smime/UriSchemaUtils$UtilFile;

    move-result-object v7

    .line 507
    .local v7, "fileName":Lcom/android/emailcommon/smime/UriSchemaUtils$UtilFile;
    if-eqz v7, :cond_2

    .line 509
    new-instance v2, Lcom/android/emailcommon/internet/MimeBodyPart;

    .end local v2    # "attPart":Lcom/android/emailcommon/internet/MimeBodyPart;
    invoke-direct {v2, v13}, Lcom/android/emailcommon/internet/MimeBodyPart;-><init>(Lcom/android/emailcommon/mail/Body;)V

    .line 511
    .restart local v2    # "attPart":Lcom/android/emailcommon/internet/MimeBodyPart;
    const/4 v4, 0x0

    .line 513
    .local v4, "contentDisposition":Ljava/lang/String;
    iget-object v14, v1, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mMimeType:Ljava/lang/String;

    if-eqz v14, :cond_0

    .line 514
    const-string v14, "Content-Type"

    iget-object v15, v1, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mMimeType:Ljava/lang/String;

    invoke-virtual {v2, v14, v15}, Lcom/android/emailcommon/internet/MimeBodyPart;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    :cond_0
    iget-object v14, v1, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mContentId:Ljava/lang/String;

    if-eqz v14, :cond_5

    .line 517
    const-string v4, "inline"

    .line 518
    const-string v14, "Content-ID"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "<"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v1, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mContentId:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ">"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v14, v15}, Lcom/android/emailcommon/internet/MimeBodyPart;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    :goto_3
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ";\n filename=\"%s\";\n size=%d"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    iget-object v0, v1, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mFileName:Ljava/lang/String;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/internet/MimeUtility;->foldAndEncode2(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1

    iget v0, v1, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mSize:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 527
    const-string v14, "Content-Disposition"

    invoke-virtual {v2, v14, v4}, Lcom/android/emailcommon/internet/MimeBodyPart;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    iget-object v14, v1, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mEncoding:Ljava/lang/String;

    if-nez v14, :cond_1

    .line 533
    const-string v14, "base64"

    iput-object v14, v1, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mEncoding:Ljava/lang/String;

    .line 535
    :cond_1
    const-string v14, "Content-Transfer-Encoding"

    iget-object v15, v1, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mEncoding:Ljava/lang/String;

    invoke-virtual {v2, v14, v15}, Lcom/android/emailcommon/internet/MimeBodyPart;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    const-string v14, "X-MS-UrlCompName"

    iget-object v15, v1, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mFileName:Ljava/lang/String;

    const-string v16, "UTF-8"

    invoke-static/range {v15 .. v16}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v14, v15}, Lcom/android/emailcommon/internet/MimeBodyPart;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    invoke-virtual {v9, v2}, Lcom/android/emailcommon/internet/MimeMultipart;->addBodyPart(Lcom/android/emailcommon/mail/BodyPart;)V

    .line 498
    .end local v4    # "contentDisposition":Ljava/lang/String;
    .end local v7    # "fileName":Lcom/android/emailcommon/smime/UriSchemaUtils$UtilFile;
    .end local v13    # "uriAttmt":Lcom/android/emailcommon/pgp/PGPHelper$UriAttachmentBody;
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_2

    .line 489
    .end local v1    # "att":Lcom/android/emailcommon/pgp/PGPHelper$Attachment;
    .end local v2    # "attPart":Lcom/android/emailcommon/internet/MimeBodyPart;
    .end local v3    # "body":Lcom/android/emailcommon/internet/MimeBodyPart;
    .end local v5    # "count":I
    .end local v8    # "i":I
    .end local v9    # "multiPart":Lcom/android/emailcommon/internet/MimeMultipart;
    .end local v11    # "textBody":Lcom/android/emailcommon/internet/TextBody;
    :cond_3
    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mBodyText:Ljava/lang/String;

    invoke-static {v14}, Lcom/android/emailcommon/pgp/PGPHelper;->validateText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_0

    .line 491
    .restart local v11    # "textBody":Lcom/android/emailcommon/internet/TextBody;
    :cond_4
    const-string v14, "text/plain; charset=utf-8"

    goto/16 :goto_1

    .line 521
    .restart local v1    # "att":Lcom/android/emailcommon/pgp/PGPHelper$Attachment;
    .restart local v2    # "attPart":Lcom/android/emailcommon/internet/MimeBodyPart;
    .restart local v3    # "body":Lcom/android/emailcommon/internet/MimeBodyPart;
    .restart local v4    # "contentDisposition":Ljava/lang/String;
    .restart local v5    # "count":I
    .restart local v7    # "fileName":Lcom/android/emailcommon/smime/UriSchemaUtils$UtilFile;
    .restart local v8    # "i":I
    .restart local v9    # "multiPart":Lcom/android/emailcommon/internet/MimeMultipart;
    .restart local v13    # "uriAttmt":Lcom/android/emailcommon/pgp/PGPHelper$UriAttachmentBody;
    :cond_5
    const-string v4, "attachment"

    goto :goto_3

    .line 543
    .end local v1    # "att":Lcom/android/emailcommon/pgp/PGPHelper$Attachment;
    .end local v2    # "attPart":Lcom/android/emailcommon/internet/MimeBodyPart;
    .end local v4    # "contentDisposition":Ljava/lang/String;
    .end local v7    # "fileName":Lcom/android/emailcommon/smime/UriSchemaUtils$UtilFile;
    .end local v13    # "uriAttmt":Lcom/android/emailcommon/pgp/PGPHelper$UriAttachmentBody;
    :cond_6
    new-instance v12, Lcom/android/emailcommon/internet/MimeBodyPart;

    invoke-direct {v12, v9}, Lcom/android/emailcommon/internet/MimeBodyPart;-><init>(Lcom/android/emailcommon/mail/Body;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 544
    .local v12, "tmp":Lcom/android/emailcommon/internet/MimeBodyPart;
    move-object v3, v12

    .line 546
    .end local v8    # "i":I
    .end local v9    # "multiPart":Lcom/android/emailcommon/internet/MimeMultipart;
    .end local v12    # "tmp":Lcom/android/emailcommon/internet/MimeBodyPart;
    :cond_7
    move-object v10, v3

    .line 550
    .end local v3    # "body":Lcom/android/emailcommon/internet/MimeBodyPart;
    .end local v5    # "count":I
    .end local v11    # "textBody":Lcom/android/emailcommon/internet/TextBody;
    :goto_4
    return-object v10

    .line 547
    :catch_0
    move-exception v6

    .line 548
    .local v6, "ex":Ljava/lang/Exception;
    sget-object v14, Lcom/android/emailcommon/pgp/PGPHelper;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Exception caught: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4
.end method

.method public encryptMessage(Landroid/content/Context;Lcom/android/emailcommon/pgp/PGPHelper$Message;Ljava/io/OutputStream;)Z
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lcom/android/emailcommon/pgp/PGPHelper$Message;
    .param p3, "out"    # Ljava/io/OutputStream;

    .prologue
    .line 130
    const/4 v9, 0x0

    .line 131
    .local v9, "encryptedTempFile":Ljava/io/File;
    const/4 v11, 0x0

    .line 132
    .local v11, "fOutStream":Ljava/io/FileOutputStream;
    const/4 v5, 0x0

    .line 133
    .local v5, "bos":Ljava/io/BufferedOutputStream;
    const/4 v13, 0x0

    .line 135
    .local v13, "in":Ljava/io/FileInputStream;
    const/16 v16, 0x0

    .line 139
    .local v16, "ret":Z
    :try_start_0
    invoke-virtual/range {p0 .. p2}, Lcom/android/emailcommon/pgp/PGPHelper;->createMimeBodyPart(Landroid/content/Context;Lcom/android/emailcommon/pgp/PGPHelper$Message;)Lcom/android/emailcommon/internet/MimeBodyPart;

    move-result-object v15

    .line 140
    .local v15, "pgpbodypart":Lcom/android/emailcommon/internet/MimeBodyPart;
    if-nez v15, :cond_4

    .line 141
    new-instance v17, Ljava/io/IOException;

    invoke-direct/range {v17 .. v17}, Ljava/io/IOException;-><init>()V

    throw v17
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    .end local v15    # "pgpbodypart":Lcom/android/emailcommon/internet/MimeBodyPart;
    :catch_0
    move-exception v10

    .line 157
    .local v10, "ex":Ljava/lang/Exception;
    :goto_0
    :try_start_1
    sget-object v17, Lcom/android/emailcommon/pgp/PGPHelper;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "encryptMessage - Exception - "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v10}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    const/16 v16, 0x0

    .line 161
    if-eqz v5, :cond_0

    .line 163
    :try_start_2
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 167
    :cond_0
    :goto_1
    if-eqz v11, :cond_1

    .line 169
    :try_start_3
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5

    .line 174
    :cond_1
    :goto_2
    if-eqz v13, :cond_2

    .line 176
    :try_start_4
    invoke-virtual {v13}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6

    .line 182
    :cond_2
    :goto_3
    if-eqz v9, :cond_3

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_3

    .line 183
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 185
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 187
    .end local v10    # "ex":Ljava/lang/Exception;
    :goto_4
    return v16

    .line 144
    .restart local v15    # "pgpbodypart":Lcom/android/emailcommon/internet/MimeBodyPart;
    :cond_4
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 145
    const-string v17, "pgp_"

    const-string v18, "tmp"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v19

    invoke-static/range {v17 .. v19}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v9

    .line 146
    new-instance v12, Ljava/io/FileOutputStream;

    invoke-direct {v12, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 147
    .end local v11    # "fOutStream":Ljava/io/FileOutputStream;
    .local v12, "fOutStream":Ljava/io/FileOutputStream;
    :try_start_6
    new-instance v6, Ljava/io/BufferedOutputStream;

    const v17, 0x8000

    move/from16 v0, v17

    invoke-direct {v6, v12, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_a
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 148
    .end local v5    # "bos":Ljava/io/BufferedOutputStream;
    .local v6, "bos":Ljava/io/BufferedOutputStream;
    :try_start_7
    invoke-virtual {v15, v6}, Lcom/android/emailcommon/internet/MimeBodyPart;->writeTo(Ljava/io/OutputStream;)V

    .line 149
    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->flush()V

    .line 151
    new-instance v14, Ljava/io/FileInputStream;

    invoke-direct {v14, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_b
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 153
    .end local v13    # "in":Ljava/io/FileInputStream;
    .local v14, "in":Ljava/io/FileInputStream;
    :try_start_8
    new-instance v17, Ljava/io/BufferedInputStream;

    const v18, 0x8000

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v0, v14, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v17

    move-object/from16 v4, p3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/pgp/PGPHelper;->createPGPEncryptedMessageWithMime(Landroid/content/Context;Lcom/android/emailcommon/pgp/PGPHelper$Message;Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_c
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 155
    const/16 v16, 0x1

    .line 161
    if-eqz v6, :cond_5

    .line 163
    :try_start_9
    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    .line 167
    :cond_5
    :goto_5
    if-eqz v12, :cond_6

    .line 169
    :try_start_a
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2

    .line 174
    :cond_6
    :goto_6
    if-eqz v14, :cond_7

    .line 176
    :try_start_b
    invoke-virtual {v14}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3

    .line 182
    :cond_7
    :goto_7
    if-eqz v9, :cond_8

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_8

    .line 183
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 185
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move-object v13, v14

    .end local v14    # "in":Ljava/io/FileInputStream;
    .restart local v13    # "in":Ljava/io/FileInputStream;
    move-object v5, v6

    .end local v6    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v5    # "bos":Ljava/io/BufferedOutputStream;
    move-object v11, v12

    .line 186
    .end local v12    # "fOutStream":Ljava/io/FileOutputStream;
    .restart local v11    # "fOutStream":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 164
    .end local v5    # "bos":Ljava/io/BufferedOutputStream;
    .end local v11    # "fOutStream":Ljava/io/FileOutputStream;
    .end local v13    # "in":Ljava/io/FileInputStream;
    .restart local v6    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v12    # "fOutStream":Ljava/io/FileOutputStream;
    .restart local v14    # "in":Ljava/io/FileInputStream;
    :catch_1
    move-exception v8

    .line 165
    .local v8, "e1":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 170
    .end local v8    # "e1":Ljava/io/IOException;
    :catch_2
    move-exception v7

    .line 171
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    .line 177
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v7

    .line 178
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_7

    .line 164
    .end local v6    # "bos":Ljava/io/BufferedOutputStream;
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v12    # "fOutStream":Ljava/io/FileOutputStream;
    .end local v14    # "in":Ljava/io/FileInputStream;
    .end local v15    # "pgpbodypart":Lcom/android/emailcommon/internet/MimeBodyPart;
    .restart local v5    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v10    # "ex":Ljava/lang/Exception;
    .restart local v11    # "fOutStream":Ljava/io/FileOutputStream;
    .restart local v13    # "in":Ljava/io/FileInputStream;
    :catch_4
    move-exception v8

    .line 165
    .restart local v8    # "e1":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 170
    .end local v8    # "e1":Ljava/io/IOException;
    :catch_5
    move-exception v7

    .line 171
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 177
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_6
    move-exception v7

    .line 178
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3

    .line 161
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v10    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v17

    :goto_8
    if-eqz v5, :cond_9

    .line 163
    :try_start_c
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7

    .line 167
    :cond_9
    :goto_9
    if-eqz v11, :cond_a

    .line 169
    :try_start_d
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_8

    .line 174
    :cond_a
    :goto_a
    if-eqz v13, :cond_b

    .line 176
    :try_start_e
    invoke-virtual {v13}, Ljava/io/FileInputStream;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_9

    .line 182
    :cond_b
    :goto_b
    if-eqz v9, :cond_c

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_c

    .line 183
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 185
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v17

    .line 164
    :catch_7
    move-exception v8

    .line 165
    .restart local v8    # "e1":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 170
    .end local v8    # "e1":Ljava/io/IOException;
    :catch_8
    move-exception v7

    .line 171
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_a

    .line 177
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_9
    move-exception v7

    .line 178
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_b

    .line 161
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v11    # "fOutStream":Ljava/io/FileOutputStream;
    .restart local v12    # "fOutStream":Ljava/io/FileOutputStream;
    .restart local v15    # "pgpbodypart":Lcom/android/emailcommon/internet/MimeBodyPart;
    :catchall_1
    move-exception v17

    move-object v11, v12

    .end local v12    # "fOutStream":Ljava/io/FileOutputStream;
    .restart local v11    # "fOutStream":Ljava/io/FileOutputStream;
    goto :goto_8

    .end local v5    # "bos":Ljava/io/BufferedOutputStream;
    .end local v11    # "fOutStream":Ljava/io/FileOutputStream;
    .restart local v6    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v12    # "fOutStream":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v17

    move-object v5, v6

    .end local v6    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v5    # "bos":Ljava/io/BufferedOutputStream;
    move-object v11, v12

    .end local v12    # "fOutStream":Ljava/io/FileOutputStream;
    .restart local v11    # "fOutStream":Ljava/io/FileOutputStream;
    goto :goto_8

    .end local v5    # "bos":Ljava/io/BufferedOutputStream;
    .end local v11    # "fOutStream":Ljava/io/FileOutputStream;
    .end local v13    # "in":Ljava/io/FileInputStream;
    .restart local v6    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v12    # "fOutStream":Ljava/io/FileOutputStream;
    .restart local v14    # "in":Ljava/io/FileInputStream;
    :catchall_3
    move-exception v17

    move-object v13, v14

    .end local v14    # "in":Ljava/io/FileInputStream;
    .restart local v13    # "in":Ljava/io/FileInputStream;
    move-object v5, v6

    .end local v6    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v5    # "bos":Ljava/io/BufferedOutputStream;
    move-object v11, v12

    .end local v12    # "fOutStream":Ljava/io/FileOutputStream;
    .restart local v11    # "fOutStream":Ljava/io/FileOutputStream;
    goto :goto_8

    .line 156
    .end local v11    # "fOutStream":Ljava/io/FileOutputStream;
    .restart local v12    # "fOutStream":Ljava/io/FileOutputStream;
    :catch_a
    move-exception v10

    move-object v11, v12

    .end local v12    # "fOutStream":Ljava/io/FileOutputStream;
    .restart local v11    # "fOutStream":Ljava/io/FileOutputStream;
    goto/16 :goto_0

    .end local v5    # "bos":Ljava/io/BufferedOutputStream;
    .end local v11    # "fOutStream":Ljava/io/FileOutputStream;
    .restart local v6    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v12    # "fOutStream":Ljava/io/FileOutputStream;
    :catch_b
    move-exception v10

    move-object v5, v6

    .end local v6    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v5    # "bos":Ljava/io/BufferedOutputStream;
    move-object v11, v12

    .end local v12    # "fOutStream":Ljava/io/FileOutputStream;
    .restart local v11    # "fOutStream":Ljava/io/FileOutputStream;
    goto/16 :goto_0

    .end local v5    # "bos":Ljava/io/BufferedOutputStream;
    .end local v11    # "fOutStream":Ljava/io/FileOutputStream;
    .end local v13    # "in":Ljava/io/FileInputStream;
    .restart local v6    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v12    # "fOutStream":Ljava/io/FileOutputStream;
    .restart local v14    # "in":Ljava/io/FileInputStream;
    :catch_c
    move-exception v10

    move-object v13, v14

    .end local v14    # "in":Ljava/io/FileInputStream;
    .restart local v13    # "in":Ljava/io/FileInputStream;
    move-object v5, v6

    .end local v6    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v5    # "bos":Ljava/io/BufferedOutputStream;
    move-object v11, v12

    .end local v12    # "fOutStream":Ljava/io/FileOutputStream;
    .restart local v11    # "fOutStream":Ljava/io/FileOutputStream;
    goto/16 :goto_0
.end method

.method public getFileName(Lcom/android/emailcommon/mail/Part;)Ljava/lang/String;
    .locals 4
    .param p1, "part"    # Lcom/android/emailcommon/mail/Part;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 775
    invoke-interface {p1}, Lcom/android/emailcommon/mail/Part;->getDisposition()Ljava/lang/String;

    move-result-object v1

    .line 776
    .local v1, "disposition":Ljava/lang/String;
    const/4 v2, 0x0

    .line 777
    .local v2, "dispositionFilename":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 778
    const-string v3, "filename"

    invoke-static {v1, v3}, Lcom/android/emailcommon/internet/MimeUtility;->getHeaderParameter(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 779
    if-nez v2, :cond_0

    .line 780
    invoke-interface {p1}, Lcom/android/emailcommon/mail/Part;->getContentType()Ljava/lang/String;

    move-result-object v0

    .line 781
    .local v0, "contentType":Ljava/lang/String;
    const-string v3, "name"

    invoke-static {v0, v3}, Lcom/android/emailcommon/internet/MimeUtility;->getHeaderParameter(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 784
    .end local v0    # "contentType":Ljava/lang/String;
    :cond_0
    return-object v2
.end method

.method public handleBodyPartPGP(Lcom/android/emailcommon/mail/Part;Lcom/android/emailcommon/pgp/PGPHelper$Message;)V
    .locals 4
    .param p1, "part"    # Lcom/android/emailcommon/mail/Part;
    .param p2, "message"    # Lcom/android/emailcommon/pgp/PGPHelper$Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;,
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 751
    invoke-interface {p1}, Lcom/android/emailcommon/mail/Part;->getContentType()Ljava/lang/String;

    move-result-object v0

    .line 752
    .local v0, "contentType":Ljava/lang/String;
    const-string v3, "multipart"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 753
    invoke-interface {p1}, Lcom/android/emailcommon/mail/Part;->getBody()Lcom/android/emailcommon/mail/Body;

    move-result-object v1

    check-cast v1, Lcom/android/emailcommon/mail/Multipart;

    .line 754
    .local v1, "multipart":Lcom/android/emailcommon/mail/Multipart;
    invoke-direct {p0, v1, p2}, Lcom/android/emailcommon/pgp/PGPHelper;->handleMultipartPGP(Lcom/android/emailcommon/mail/Multipart;Lcom/android/emailcommon/pgp/PGPHelper$Message;)V

    .line 772
    .end local v1    # "multipart":Lcom/android/emailcommon/mail/Multipart;
    :cond_0
    :goto_0
    return-void

    .line 755
    :cond_1
    const-string v3, "text"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 756
    invoke-virtual {p0, p1}, Lcom/android/emailcommon/pgp/PGPHelper;->getFileName(Lcom/android/emailcommon/mail/Part;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 757
    invoke-direct {p0, p1, p2}, Lcom/android/emailcommon/pgp/PGPHelper;->handleAttachmentPGP(Lcom/android/emailcommon/mail/Part;Lcom/android/emailcommon/pgp/PGPHelper$Message;)V

    goto :goto_0

    .line 759
    :cond_2
    invoke-static {p1}, Lcom/android/emailcommon/internet/MimeUtility;->getTextFromPart(Lcom/android/emailcommon/mail/Part;)Ljava/lang/String;

    move-result-object v2

    .line 760
    .local v2, "text":Ljava/lang/String;
    const-string v3, "html"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    iput-boolean v3, p2, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mIsHtml:Z

    .line 761
    iget-boolean v3, p2, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mIsHtml:Z

    if-eqz v3, :cond_3

    .line 762
    iput-object v2, p2, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mHtmlBodyText:Ljava/lang/String;

    goto :goto_0

    .line 764
    :cond_3
    iput-object v2, p2, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mBodyText:Ljava/lang/String;

    goto :goto_0

    .line 767
    .end local v2    # "text":Ljava/lang/String;
    :cond_4
    const-string v3, "application"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 768
    invoke-direct {p0, p1, p2}, Lcom/android/emailcommon/pgp/PGPHelper;->handleAttachmentPGP(Lcom/android/emailcommon/mail/Part;Lcom/android/emailcommon/pgp/PGPHelper$Message;)V

    goto :goto_0

    .line 769
    :cond_5
    invoke-virtual {p0, p1}, Lcom/android/emailcommon/pgp/PGPHelper;->getFileName(Lcom/android/emailcommon/mail/Part;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 770
    invoke-direct {p0, p1, p2}, Lcom/android/emailcommon/pgp/PGPHelper;->handleAttachmentPGP(Lcom/android/emailcommon/mail/Part;Lcom/android/emailcommon/pgp/PGPHelper$Message;)V

    goto :goto_0
.end method

.method public signMessage(Landroid/content/Context;Lcom/android/emailcommon/pgp/PGPHelper$Message;Ljava/io/OutputStream;)Z
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lcom/android/emailcommon/pgp/PGPHelper$Message;
    .param p3, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/mail/MessagingException;,
            Lcom/android/sec/org/bouncycastle/mail/smime/SMIMEException;
        }
    .end annotation

    .prologue
    .line 229
    const/4 v11, 0x0

    .line 230
    .local v11, "signedTempFile":Ljava/io/File;
    const/4 v6, 0x0

    .line 231
    .local v6, "fileInputStrm":Ljava/io/FileInputStream;
    const/4 v13, 0x0

    .line 232
    .local v13, "srcMimeTempFile":Ljava/io/File;
    const/4 v8, 0x0

    .line 235
    .local v8, "fos":Lcom/android/emailcommon/utility/EOLConvertingOutputStream;
    :try_start_0
    invoke-virtual/range {p0 .. p2}, Lcom/android/emailcommon/pgp/PGPHelper;->createMimeBodyPart(Landroid/content/Context;Lcom/android/emailcommon/pgp/PGPHelper$Message;)Lcom/android/emailcommon/internet/MimeBodyPart;

    move-result-object v10

    .line 236
    .local v10, "pgpbodypart":Lcom/android/emailcommon/internet/MimeBodyPart;
    if-nez v10, :cond_2

    .line 237
    new-instance v14, Ljava/io/IOException;

    invoke-direct {v14}, Ljava/io/IOException;-><init>()V

    throw v14
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    .end local v10    # "pgpbodypart":Lcom/android/emailcommon/internet/MimeBodyPart;
    :catch_0
    move-exception v5

    .line 261
    .local v5, "ex":Ljava/lang/Exception;
    :goto_0
    :try_start_1
    sget-object v14, Lcom/android/emailcommon/pgp/PGPHelper;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "signMessage - Exception - "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 263
    const/4 v14, 0x0

    .line 265
    if-eqz v13, :cond_0

    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_0

    .line 266
    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 267
    :cond_0
    if-eqz v11, :cond_1

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_1

    .line 268
    invoke-virtual {v11}, Ljava/io/File;->delete()Z

    .end local v5    # "ex":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return v14

    .line 240
    .restart local v10    # "pgpbodypart":Lcom/android/emailcommon/internet/MimeBodyPart;
    :cond_2
    :try_start_2
    const-string v14, "pgp_"

    const-string v15, "tmp"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v16

    invoke-static/range {v14 .. v16}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v11

    .line 241
    const-string v14, "pgp_src"

    const-string v15, "tmp"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v16

    invoke-static/range {v14 .. v16}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v13

    .line 242
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/emailcommon/pgp/PGPHelper;->encryptLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v14}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 244
    new-instance v4, Ljava/io/BufferedOutputStream;

    new-instance v14, Ljava/io/FileOutputStream;

    invoke-direct {v14, v11}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const v15, 0x8000

    invoke-direct {v4, v14, v15}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    .line 246
    .local v4, "bos":Ljava/io/BufferedOutputStream;
    invoke-virtual {v10, v4}, Lcom/android/emailcommon/internet/MimeBodyPart;->writeTo(Ljava/io/OutputStream;)V

    .line 247
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->flush()V

    .line 248
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V

    .line 250
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, v11}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 251
    .end local v6    # "fileInputStrm":Ljava/io/FileInputStream;
    .local v7, "fileInputStrm":Ljava/io/FileInputStream;
    :try_start_3
    new-instance v12, Lcom/android/emailcommon/internet/MimeMessage;

    new-instance v14, Ljava/io/BufferedInputStream;

    const v15, 0x8000

    invoke-direct {v14, v7, v15}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    invoke-direct {v12, v14}, Lcom/android/emailcommon/internet/MimeMessage;-><init>(Ljava/io/InputStream;)V

    .line 253
    .local v12, "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    new-instance v9, Lcom/android/emailcommon/utility/EOLConvertingOutputStream;

    new-instance v14, Ljava/io/FileOutputStream;

    invoke-direct {v14, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v9, v14}, Lcom/android/emailcommon/utility/EOLConvertingOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 254
    .end local v8    # "fos":Lcom/android/emailcommon/utility/EOLConvertingOutputStream;
    .local v9, "fos":Lcom/android/emailcommon/utility/EOLConvertingOutputStream;
    :try_start_4
    invoke-virtual {v12, v9}, Lcom/android/emailcommon/internet/MimeMessage;->writeTo(Ljava/io/OutputStream;)V

    .line 255
    invoke-virtual {v9}, Lcom/android/emailcommon/utility/EOLConvertingOutputStream;->close()V

    .line 257
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v13, v3}, Lcom/android/emailcommon/pgp/PGPHelper;->createSignedMessageWithMime(Landroid/content/Context;Lcom/android/emailcommon/pgp/PGPHelper$Message;Ljava/io/File;Ljava/io/OutputStream;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 259
    const/4 v14, 0x1

    .line 265
    if-eqz v13, :cond_3

    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_3

    .line 266
    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 267
    :cond_3
    if-eqz v11, :cond_4

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_4

    .line 268
    invoke-virtual {v11}, Ljava/io/File;->delete()Z

    :cond_4
    move-object v8, v9

    .end local v9    # "fos":Lcom/android/emailcommon/utility/EOLConvertingOutputStream;
    .restart local v8    # "fos":Lcom/android/emailcommon/utility/EOLConvertingOutputStream;
    move-object v6, v7

    .end local v7    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v6    # "fileInputStrm":Ljava/io/FileInputStream;
    goto :goto_1

    .line 265
    .end local v4    # "bos":Ljava/io/BufferedOutputStream;
    .end local v10    # "pgpbodypart":Lcom/android/emailcommon/internet/MimeBodyPart;
    .end local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    :catchall_0
    move-exception v14

    :goto_2
    if-eqz v13, :cond_5

    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_5

    .line 266
    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 267
    :cond_5
    if-eqz v11, :cond_6

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_6

    .line 268
    invoke-virtual {v11}, Ljava/io/File;->delete()Z

    :cond_6
    throw v14

    .line 265
    .end local v6    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v4    # "bos":Ljava/io/BufferedOutputStream;
    .restart local v7    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v10    # "pgpbodypart":Lcom/android/emailcommon/internet/MimeBodyPart;
    :catchall_1
    move-exception v14

    move-object v6, v7

    .end local v7    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v6    # "fileInputStrm":Ljava/io/FileInputStream;
    goto :goto_2

    .end local v6    # "fileInputStrm":Ljava/io/FileInputStream;
    .end local v8    # "fos":Lcom/android/emailcommon/utility/EOLConvertingOutputStream;
    .restart local v7    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v9    # "fos":Lcom/android/emailcommon/utility/EOLConvertingOutputStream;
    .restart local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    :catchall_2
    move-exception v14

    move-object v8, v9

    .end local v9    # "fos":Lcom/android/emailcommon/utility/EOLConvertingOutputStream;
    .restart local v8    # "fos":Lcom/android/emailcommon/utility/EOLConvertingOutputStream;
    move-object v6, v7

    .end local v7    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v6    # "fileInputStrm":Ljava/io/FileInputStream;
    goto :goto_2

    .line 260
    .end local v6    # "fileInputStrm":Ljava/io/FileInputStream;
    .end local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    .restart local v7    # "fileInputStrm":Ljava/io/FileInputStream;
    :catch_1
    move-exception v5

    move-object v6, v7

    .end local v7    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v6    # "fileInputStrm":Ljava/io/FileInputStream;
    goto/16 :goto_0

    .end local v6    # "fileInputStrm":Ljava/io/FileInputStream;
    .end local v8    # "fos":Lcom/android/emailcommon/utility/EOLConvertingOutputStream;
    .restart local v7    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v9    # "fos":Lcom/android/emailcommon/utility/EOLConvertingOutputStream;
    .restart local v12    # "srcMessage":Lcom/android/emailcommon/internet/MimeMessage;
    :catch_2
    move-exception v5

    move-object v8, v9

    .end local v9    # "fos":Lcom/android/emailcommon/utility/EOLConvertingOutputStream;
    .restart local v8    # "fos":Lcom/android/emailcommon/utility/EOLConvertingOutputStream;
    move-object v6, v7

    .end local v7    # "fileInputStrm":Ljava/io/FileInputStream;
    .restart local v6    # "fileInputStrm":Ljava/io/FileInputStream;
    goto/16 :goto_0
.end method
