.class Lcom/android/emailcommon/pgp/PGPHelper$3;
.super Ljava/lang/Object;
.source "PGPHelper.java"

# interfaces
.implements Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/emailcommon/pgp/PGPHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/emailcommon/pgp/PGPHelper;

.field final synthetic val$bProcessFile:Z

.field final synthetic val$decryptedFile:Ljava/io/File;

.field final synthetic val$msg:Lcom/android/emailcommon/pgp/PGPHelper$Message;

.field final synthetic val$out:Ljava/io/OutputStream;


# virtual methods
.method public onProgressUpdate(I)V
    .locals 0
    .param p1, "percentComplete"    # I

    .prologue
    .line 614
    return-void
.end method

.method public onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V
    .locals 8
    .param p1, "resultCode"    # I
    .param p2, "e"    # Ljava/lang/Exception;
    .param p3, "result"    # Lcom/android/emailcommon/pgp/PGPOperationResultData;

    .prologue
    .line 617
    const/4 v3, 0x0

    .line 619
    .local v3, "mimeMessage":Lcom/android/emailcommon/internet/MimeMessage;
    const/4 v1, 0x0

    .line 620
    .local v1, "is":Ljava/io/InputStream;
    :try_start_0
    iget-boolean v5, p0, Lcom/android/emailcommon/pgp/PGPHelper$3;->val$bProcessFile:Z

    if-eqz v5, :cond_2

    .line 621
    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper$3;->val$out:Ljava/io/OutputStream;

    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 624
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper$3;->val$decryptedFile:Ljava/io/File;

    invoke-direct {v2, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .end local v1    # "is":Ljava/io/InputStream;
    .local v2, "is":Ljava/io/InputStream;
    move-object v1, v2

    .line 631
    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    :goto_0
    new-instance v4, Lcom/android/emailcommon/internet/MimeMessage;

    invoke-direct {v4, v1}, Lcom/android/emailcommon/internet/MimeMessage;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 632
    .end local v3    # "mimeMessage":Lcom/android/emailcommon/internet/MimeMessage;
    .local v4, "mimeMessage":Lcom/android/emailcommon/internet/MimeMessage;
    if-eqz v1, :cond_0

    .line 633
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 635
    :cond_0
    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper$3;->this$0:Lcom/android/emailcommon/pgp/PGPHelper;

    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPHelper$3;->val$msg:Lcom/android/emailcommon/pgp/PGPHelper$Message;

    invoke-virtual {v5, v4, v6}, Lcom/android/emailcommon/pgp/PGPHelper;->handleBodyPartPGP(Lcom/android/emailcommon/mail/Part;Lcom/android/emailcommon/pgp/PGPHelper$Message;)V

    .line 636
    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper$3;->val$msg:Lcom/android/emailcommon/pgp/PGPHelper$Message;

    invoke-virtual {p3}, Lcom/android/emailcommon/pgp/PGPOperationResultData;->getIsEncrypted()Z

    move-result v6

    iput-boolean v6, v5, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mEncrypted:Z

    .line 637
    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper$3;->val$msg:Lcom/android/emailcommon/pgp/PGPHelper$Message;

    invoke-virtual {p3}, Lcom/android/emailcommon/pgp/PGPOperationResultData;->getIsSigned()Z

    move-result v6

    iput-boolean v6, v5, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mSigned:Z

    .line 638
    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper$3;->val$msg:Lcom/android/emailcommon/pgp/PGPHelper$Message;

    iget-boolean v5, v5, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mSigned:Z

    if-eqz v5, :cond_1

    .line 639
    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper$3;->this$0:Lcom/android/emailcommon/pgp/PGPHelper;

    invoke-virtual {p3}, Lcom/android/emailcommon/pgp/PGPOperationResultData;->getIsSignKeyExists()Z

    move-result v6

    # setter for: Lcom/android/emailcommon/pgp/PGPHelper;->mIsSignKeyExists:Z
    invoke-static {v5, v6}, Lcom/android/emailcommon/pgp/PGPHelper;->access$002(Lcom/android/emailcommon/pgp/PGPHelper;Z)Z

    .line 641
    :cond_1
    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper$3;->val$msg:Lcom/android/emailcommon/pgp/PGPHelper$Message;

    invoke-virtual {p3}, Lcom/android/emailcommon/pgp/PGPOperationResultData;->getIsSignatureVerified()Z

    move-result v6

    iput-boolean v6, v5, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mVerified:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v3, v4

    .line 646
    .end local v4    # "mimeMessage":Lcom/android/emailcommon/internet/MimeMessage;
    .restart local v3    # "mimeMessage":Lcom/android/emailcommon/internet/MimeMessage;
    :goto_1
    return-void

    .line 627
    :cond_2
    :try_start_2
    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPHelper$3;->val$out:Ljava/io/OutputStream;

    check-cast v5, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .end local v1    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    move-object v1, v2

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    goto :goto_0

    .line 642
    :catch_0
    move-exception v0

    .line 643
    .local v0, "ex":Ljava/lang/Exception;
    :goto_2
    # getter for: Lcom/android/emailcommon/pgp/PGPHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/emailcommon/pgp/PGPHelper;->access$100()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "decryptMessage - Exception - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 642
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v3    # "mimeMessage":Lcom/android/emailcommon/internet/MimeMessage;
    .restart local v4    # "mimeMessage":Lcom/android/emailcommon/internet/MimeMessage;
    :catch_1
    move-exception v0

    move-object v3, v4

    .end local v4    # "mimeMessage":Lcom/android/emailcommon/internet/MimeMessage;
    .restart local v3    # "mimeMessage":Lcom/android/emailcommon/internet/MimeMessage;
    goto :goto_2
.end method
