.class public Lcom/android/emailcommon/pgp/EncryptSignParams;
.super Ljava/lang/Object;
.source "EncryptSignParams.java"


# instance fields
.field private mArmor:Z

.field private mEncryptKeyIds:[J

.field private mFileModTime:Ljava/util/Date;

.field private mFileName:Ljava/lang/String;

.field private mInputStream:Ljava/io/InputStream;

.field private mOutputStream:Ljava/io/OutputStream;

.field private mSignPassPhrase:Ljava/lang/String;

.field private mSignSecretKeyId:J

.field private mSize:J

.field private mSymEncPassPhrase:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/lang/String;JZJ[JLjava/lang/String;Ljava/lang/String;Ljava/util/Date;)V
    .locals 0
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "out"    # Ljava/io/OutputStream;
    .param p3, "signPassPhrase"    # Ljava/lang/String;
    .param p4, "signKeyId"    # J
    .param p6, "isArmor"    # Z
    .param p7, "dataSize"    # J
    .param p9, "encryptKeyIds"    # [J
    .param p10, "symEncPassPhrase"    # Ljava/lang/String;
    .param p11, "inputFileName"    # Ljava/lang/String;
    .param p12, "inputFileModTime"    # Ljava/util/Date;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mInputStream:Ljava/io/InputStream;

    .line 24
    iput-object p2, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mOutputStream:Ljava/io/OutputStream;

    .line 25
    iput-object p3, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mSignPassPhrase:Ljava/lang/String;

    .line 26
    iput-wide p4, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mSignSecretKeyId:J

    .line 27
    iput-boolean p6, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mArmor:Z

    .line 28
    iput-wide p7, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mSize:J

    .line 29
    iput-object p9, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mEncryptKeyIds:[J

    .line 30
    iput-object p10, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mSymEncPassPhrase:Ljava/lang/String;

    .line 31
    iput-object p11, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mFileName:Ljava/lang/String;

    .line 32
    iput-object p12, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mFileModTime:Ljava/util/Date;

    .line 33
    return-void
.end method


# virtual methods
.method public getArmor()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mArmor:Z

    return v0
.end method

.method public getEncryptKeyIds()[J
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mEncryptKeyIds:[J

    return-object v0
.end method

.method public getInputDataSize()J
    .locals 2

    .prologue
    .line 88
    iget-wide v0, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mSize:J

    return-wide v0
.end method

.method public getInputFileModTime()Ljava/util/Date;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mFileModTime:Ljava/util/Date;

    return-object v0
.end method

.method public getInputFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mInputStream:Ljava/io/InputStream;

    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mOutputStream:Ljava/io/OutputStream;

    return-object v0
.end method

.method public getSignPassphrase()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mSignPassPhrase:Ljava/lang/String;

    return-object v0
.end method

.method public getSignSecretKeyId()J
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/android/emailcommon/pgp/EncryptSignParams;->mSignSecretKeyId:J

    return-wide v0
.end method
