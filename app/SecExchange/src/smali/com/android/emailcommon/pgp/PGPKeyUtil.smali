.class public Lcom/android/emailcommon/pgp/PGPKeyUtil;
.super Ljava/lang/Object;
.source "PGPKeyUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkForUsername(Ljava/lang/String;)Lcom/android/emailcommon/pgp/PGPUserInfo;
    .locals 7
    .param p0, "userId"    # Ljava/lang/String;

    .prologue
    const/4 v6, -0x1

    .line 126
    const/4 v4, 0x0

    .line 127
    .local v4, "temp":Ljava/lang/String;
    const/4 v0, 0x0

    .line 128
    .local v0, "displayname":Ljava/lang/String;
    const/4 v1, 0x0

    .line 129
    .local v1, "emailId":Ljava/lang/String;
    const/4 v3, 0x0

    .line 130
    .local v3, "indexstart":I
    const/4 v2, 0x0

    .line 132
    .local v2, "indexend":I
    if-eqz p0, :cond_2

    .line 133
    move-object v4, p0

    .line 135
    const-string v5, "<"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 136
    const/16 v5, 0x3c

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 137
    const/16 v5, 0x3e

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 139
    if-eq v3, v6, :cond_1

    if-eq v2, v6, :cond_1

    .line 141
    if-ge v3, v2, :cond_0

    .line 142
    add-int/lit8 v5, v3, 0x1

    invoke-virtual {v4, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 143
    const/4 v5, 0x0

    invoke-virtual {v4, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 144
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 145
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 158
    :cond_0
    :goto_0
    new-instance v5, Lcom/android/emailcommon/pgp/PGPUserInfo;

    invoke-direct {v5, v0, v1}, Lcom/android/emailcommon/pgp/PGPUserInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v5

    .line 148
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 149
    const/4 v0, 0x0

    goto :goto_0

    .line 154
    :cond_2
    move-object v1, p0

    .line 155
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getCreationDate(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Ljava/util/Date;
    .locals 1
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getCreationTime()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public static getCreationDate(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;)Ljava/util/Date;
    .locals 1
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getCreationTime()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public static getExpirationDate(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Ljava/util/Date;
    .locals 5
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .prologue
    .line 37
    invoke-static {p0}, Lcom/android/emailcommon/pgp/PGPKeyUtil;->getCreationDate(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Ljava/util/Date;

    move-result-object v1

    .line 39
    .local v1, "createDate":Ljava/util/Date;
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getValidDays()I

    move-result v3

    if-nez v3, :cond_0

    .line 40
    const/4 v2, 0x0

    .line 48
    :goto_0
    return-object v2

    .line 43
    :cond_0
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 44
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 45
    const/4 v3, 0x5

    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getValidDays()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->add(II)V

    .line 46
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    .line 48
    .local v2, "expiryDate":Ljava/util/Date;
    goto :goto_0
.end method

.method public static getExpirationDate(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;)Ljava/util/Date;
    .locals 1
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v0

    invoke-static {v0}, Lcom/android/emailcommon/pgp/PGPKeyUtil;->getExpirationDate(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public static getKeyValidity(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)I
    .locals 6
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .prologue
    const/16 v4, 0x23

    .line 245
    invoke-static {p0}, Lcom/android/emailcommon/pgp/PGPKeyUtil;->getCreationDate(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Ljava/util/Date;

    move-result-object v0

    .line 246
    .local v0, "creationDate":Ljava/util/Date;
    invoke-static {p0}, Lcom/android/emailcommon/pgp/PGPKeyUtil;->getExpirationDate(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Ljava/util/Date;

    move-result-object v1

    .line 247
    .local v1, "expiryDate":Ljava/util/Date;
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 249
    .local v2, "now":Ljava/util/Date;
    if-nez v0, :cond_1

    .line 260
    :cond_0
    :goto_0
    return v4

    .line 253
    :cond_1
    invoke-virtual {v2, v0}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v3

    .line 255
    .local v3, "result":I
    if-ltz v3, :cond_3

    if-eqz v1, :cond_2

    invoke-virtual {v2, v1}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v5

    if-gtz v5, :cond_3

    .line 256
    :cond_2
    const/16 v4, 0x21

    goto :goto_0

    .line 257
    :cond_3
    if-lez v3, :cond_0

    .line 260
    const/16 v4, 0x22

    goto :goto_0
.end method

.method public static getMainUserId(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Lcom/android/emailcommon/pgp/PGPUserInfo;
    .locals 4
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getUserIDs()Ljava/util/Iterator;

    move-result-object v0

    .line 115
    .local v0, "itr":Ljava/util/Iterator;
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 116
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 117
    .local v1, "userId":Ljava/lang/String;
    invoke-static {v1}, Lcom/android/emailcommon/pgp/PGPKeyUtil;->checkForUsername(Ljava/lang/String;)Lcom/android/emailcommon/pgp/PGPUserInfo;

    move-result-object v2

    .line 121
    .end local v1    # "userId":Ljava/lang/String;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getMainUserId(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;)Lcom/android/emailcommon/pgp/PGPUserInfo;
    .locals 4
    .param p0, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getUserIDs()Ljava/util/Iterator;

    move-result-object v0

    .line 102
    .local v0, "itr":Ljava/util/Iterator;
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 103
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 104
    .local v1, "userId":Ljava/lang/String;
    invoke-static {v1}, Lcom/android/emailcommon/pgp/PGPKeyUtil;->checkForUsername(Ljava/lang/String;)Lcom/android/emailcommon/pgp/PGPUserInfo;

    move-result-object v2

    .line 108
    .end local v1    # "userId":Ljava/lang/String;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getMainUserIdForRing(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;)Lcom/android/emailcommon/pgp/PGPUserInfo;
    .locals 2
    .param p0, "pubkeyring"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v0

    .line 195
    .local v0, "pubkey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    invoke-static {v0}, Lcom/android/emailcommon/pgp/PGPKeyUtil;->getMainUserId(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Lcom/android/emailcommon/pgp/PGPUserInfo;

    move-result-object v1

    return-object v1
.end method

.method public static getMainUserIdForRing(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;)Lcom/android/emailcommon/pgp/PGPUserInfo;
    .locals 2
    .param p0, "seckeyring"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->getSecretKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-result-object v0

    .line 200
    .local v0, "seckey":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    invoke-static {v0}, Lcom/android/emailcommon/pgp/PGPKeyUtil;->getMainUserId(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;)Lcom/android/emailcommon/pgp/PGPUserInfo;

    move-result-object v1

    return-object v1
.end method
