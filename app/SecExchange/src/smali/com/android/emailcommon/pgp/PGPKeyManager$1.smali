.class Lcom/android/emailcommon/pgp/PGPKeyManager$1;
.super Ljava/lang/Object;
.source "PGPKeyManager.java"

# interfaces
.implements Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/emailcommon/pgp/PGPKeyManager;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/emailcommon/pgp/PGPKeyManager;


# direct methods
.method constructor <init>(Lcom/android/emailcommon/pgp/PGPKeyManager;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/android/emailcommon/pgp/PGPKeyManager$1;->this$0:Lcom/android/emailcommon/pgp/PGPKeyManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressUpdate(I)V
    .locals 1
    .param p1, "percentComplete"    # I

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/emailcommon/pgp/PGPKeyManager$1;->this$0:Lcom/android/emailcommon/pgp/PGPKeyManager;

    # getter for: Lcom/android/emailcommon/pgp/PGPKeyManager;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;
    invoke-static {v0}, Lcom/android/emailcommon/pgp/PGPKeyManager;->access$000(Lcom/android/emailcommon/pgp/PGPKeyManager;)Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onProgressUpdate(I)V

    .line 68
    return-void
.end method

.method public onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V
    .locals 2
    .param p1, "resultCode"    # I
    .param p2, "e"    # Ljava/lang/Exception;
    .param p3, "res"    # Lcom/android/emailcommon/pgp/PGPOperationResultData;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/emailcommon/pgp/PGPKeyManager$1;->this$0:Lcom/android/emailcommon/pgp/PGPKeyManager;

    const/4 v1, 0x0

    # setter for: Lcom/android/emailcommon/pgp/PGPKeyManager;->mOpActive:Z
    invoke-static {v0, v1}, Lcom/android/emailcommon/pgp/PGPKeyManager;->access$102(Lcom/android/emailcommon/pgp/PGPKeyManager;Z)Z

    .line 72
    iget-object v0, p0, Lcom/android/emailcommon/pgp/PGPKeyManager$1;->this$0:Lcom/android/emailcommon/pgp/PGPKeyManager;

    # getter for: Lcom/android/emailcommon/pgp/PGPKeyManager;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;
    invoke-static {v0}, Lcom/android/emailcommon/pgp/PGPKeyManager;->access$000(Lcom/android/emailcommon/pgp/PGPKeyManager;)Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    .line 73
    return-void
.end method
