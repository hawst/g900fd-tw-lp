.class public Lcom/android/emailcommon/pgp/PGPImportKeys;
.super Lcom/android/emailcommon/pgp/PGPOperation;
.source "PGPImportKeys.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/pgp/PGPImportKeys$PGPImportCode;
    }
.end annotation


# instance fields
.field private mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

.field private mInputfilename:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/android/emailcommon/pgp/PGPOperation;-><init>()V

    .line 47
    return-void
.end method

.method private saveKeys(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;)I
    .locals 9
    .param p1, "pubKeyRing"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 229
    const/4 v8, 0x2

    .line 230
    .local v8, "ret":I
    iget-object v0, p0, Lcom/android/emailcommon/pgp/PGPImportKeys;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    invoke-virtual {v0, p1}, Lcom/android/emailcommon/pgp/PGPDatabase;->saveKeyRing(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;)I

    move-result v8

    .line 232
    const/4 v0, 0x2

    if-ne v8, v0, :cond_1

    .line 233
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->getPublicKeys()Ljava/util/Iterator;

    move-result-object v6

    .line 235
    .local v6, "itr":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    .line 237
    .local v7, "key":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    iget-object v0, p0, Lcom/android/emailcommon/pgp/PGPImportKeys;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    invoke-virtual {v0, v7, p1}, Lcom/android/emailcommon/pgp/PGPDatabase;->saveKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;)I

    move-result v8

    .line 239
    const/16 v0, 0x8

    if-eq v8, v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/android/emailcommon/pgp/PGPImportKeys;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    sget-object v1, Lcom/android/emailcommon/pgp/PGPDatabase;->mPublicKeys:Landroid/net/Uri;

    const-string v2, "c_key_id"

    const-string v3, "c_id"

    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lcom/android/emailcommon/pgp/PGPDatabase;->updateKeyRing(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;J)I

    goto :goto_0

    .line 245
    .end local v6    # "itr":Ljava/util/Iterator;
    .end local v7    # "key":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :cond_1
    return v8
.end method

.method private saveKeys(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;)I
    .locals 9
    .param p1, "secKeyRing"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    const/4 v8, 0x2

    .line 209
    .local v8, "ret":I
    iget-object v0, p0, Lcom/android/emailcommon/pgp/PGPImportKeys;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    invoke-virtual {v0, p1}, Lcom/android/emailcommon/pgp/PGPDatabase;->saveKeyRing(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;)I

    move-result v8

    .line 211
    const/4 v0, 0x2

    if-ne v8, v0, :cond_1

    .line 212
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->getSecretKeys()Ljava/util/Iterator;

    move-result-object v6

    .line 214
    .local v6, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;>;"
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    .line 216
    .local v7, "key":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    iget-object v0, p0, Lcom/android/emailcommon/pgp/PGPImportKeys;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    invoke-virtual {v0, v7, p1}, Lcom/android/emailcommon/pgp/PGPDatabase;->saveKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;)I

    move-result v8

    .line 218
    const/16 v0, 0x8

    if-eq v8, v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/android/emailcommon/pgp/PGPImportKeys;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    sget-object v1, Lcom/android/emailcommon/pgp/PGPDatabase;->mPrivateKeys:Landroid/net/Uri;

    const-string v2, "c_key_id"

    const-string v3, "_id"

    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getKeyID()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lcom/android/emailcommon/pgp/PGPDatabase;->updateKeyRing(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;J)I

    goto :goto_0

    .line 225
    .end local v6    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;>;"
    .end local v7    # "key":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    :cond_1
    return v8
.end method


# virtual methods
.method public importAllKeys(Ljava/io/InputStream;)V
    .locals 10
    .param p1, "mfileInputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/pgp/PGPKeyException;
        }
    .end annotation

    .prologue
    .line 103
    const/4 v0, 0x0

    .line 104
    .local v0, "addkeycount":I
    const/4 v1, 0x0

    .line 105
    .local v1, "badkeycount":I
    const/4 v6, 0x0

    .line 106
    .local v6, "returnvalue":I
    const/4 v2, 0x0

    .line 107
    .local v2, "instream":Ljava/io/InputStream;
    invoke-static {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getDecoderStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v2

    .line 110
    :cond_0
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;

    invoke-direct {v4, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;-><init>(Ljava/io/InputStream;)V

    .line 111
    .local v4, "objFactory":Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;
    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->nextObject()Ljava/lang/Object;

    move-result-object v3

    .line 113
    .local v3, "obj":Ljava/lang/Object;
    if-nez v3, :cond_1

    .line 142
    const/4 v8, 0x0

    invoke-virtual {p0, v0, v1, v6, v8}, Lcom/android/emailcommon/pgp/PGPImportKeys;->sendResponseData(IIILjava/lang/Exception;)V

    .line 144
    return-void

    .line 117
    :cond_1
    :goto_0
    if-eqz v3, :cond_0

    .line 121
    instance-of v8, v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    if-eqz v8, :cond_5

    move-object v7, v3

    .line 122
    check-cast v7, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    .line 123
    .local v7, "secretKeyRing":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    invoke-direct {p0, v7}, Lcom/android/emailcommon/pgp/PGPImportKeys;->saveKeys(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;)I

    move-result v6

    .line 131
    .end local v7    # "secretKeyRing":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    :cond_2
    :goto_1
    const/4 v8, 0x3

    if-ne v6, v8, :cond_3

    .line 132
    add-int/lit8 v1, v1, 0x1

    .line 135
    :cond_3
    const/4 v8, 0x2

    if-ne v6, v8, :cond_4

    .line 136
    add-int/lit8 v0, v0, 0x1

    .line 139
    :cond_4
    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->nextObject()Ljava/lang/Object;

    move-result-object v3

    goto :goto_0

    .line 124
    :cond_5
    instance-of v8, v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

    if-eqz v8, :cond_6

    move-object v5, v3

    .line 125
    check-cast v5, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

    .line 126
    .local v5, "publicKeyRing":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    invoke-direct {p0, v5}, Lcom/android/emailcommon/pgp/PGPImportKeys;->saveKeys(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;)I

    move-result v6

    goto :goto_1

    .line 127
    .end local v5    # "publicKeyRing":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    :cond_6
    instance-of v8, v3, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;

    if-eqz v8, :cond_2

    .line 128
    new-instance v8, Lcom/android/emailcommon/pgp/PGPKeyException;

    const-string v9, "Invalid file content.No Keys Imported."

    invoke-direct {v8, v9}, Lcom/android/emailcommon/pgp/PGPKeyException;-><init>(Ljava/lang/String;)V

    throw v8
.end method

.method public importKeysStart()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/io/FileNotFoundException;,
            Lcom/android/emailcommon/pgp/PGPKeyException;
        }
    .end annotation

    .prologue
    .line 50
    const/4 v1, 0x0

    .line 51
    .local v1, "mfileInputStream":Ljava/io/InputStream;
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/android/emailcommon/pgp/PGPImportKeys;->mInputfilename:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 53
    .local v0, "file":Ljava/io/File;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    .end local v1    # "mfileInputStream":Ljava/io/InputStream;
    .local v2, "mfileInputStream":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {p0, v2}, Lcom/android/emailcommon/pgp/PGPImportKeys;->importAllKeys(Ljava/io/InputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 56
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 58
    return-void

    .line 56
    .end local v2    # "mfileInputStream":Ljava/io/InputStream;
    .restart local v1    # "mfileInputStream":Ljava/io/InputStream;
    :catchall_0
    move-exception v3

    :goto_0
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v3

    .end local v1    # "mfileInputStream":Ljava/io/InputStream;
    .restart local v2    # "mfileInputStream":Ljava/io/InputStream;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "mfileInputStream":Ljava/io/InputStream;
    .restart local v1    # "mfileInputStream":Ljava/io/InputStream;
    goto :goto_0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 96
    :try_start_0
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPImportKeys;->importKeysStart()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    return-void

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/android/emailcommon/pgp/PGPImportKeys;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v3}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    goto :goto_0
.end method

.method public sendResponseData(IIILjava/lang/Exception;)V
    .locals 4
    .param p1, "addkeycount"    # I
    .param p2, "badkeycount"    # I
    .param p3, "returnvalue"    # I
    .param p4, "e"    # Ljava/lang/Exception;

    .prologue
    .line 168
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 169
    .local v1, "returnData":Landroid/os/Bundle;
    const-string v2, "addkeycount"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 170
    const-string v2, "badkeycount"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 171
    const-string v2, "errorvalue"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 172
    const-string v2, "Status"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 173
    new-instance v0, Lcom/android/emailcommon/pgp/PGPOperationResultData;

    invoke-direct {v0}, Lcom/android/emailcommon/pgp/PGPOperationResultData;-><init>()V

    .line 174
    .local v0, "res":Lcom/android/emailcommon/pgp/PGPOperationResultData;
    const/4 v2, 0x0

    iput v2, v0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mPgpOperation:I

    .line 175
    iput-object v1, v0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mMsg:Landroid/os/Bundle;

    .line 176
    iget-object v2, p0, Lcom/android/emailcommon/pgp/PGPImportKeys;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    invoke-interface {v2, p3, p4, v0}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    .line 177
    return-void
.end method
