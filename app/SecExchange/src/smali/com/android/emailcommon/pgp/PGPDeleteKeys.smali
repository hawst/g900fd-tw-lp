.class public Lcom/android/emailcommon/pgp/PGPDeleteKeys;
.super Lcom/android/emailcommon/pgp/PGPOperation;
.source "PGPDeleteKeys.java"


# instance fields
.field private DELETE_KEYS_BY_EMAIL_ID:Z

.field private DELETE_KEYS_BY_RID:Z

.field private mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

.field private mEmailIDs:[Ljava/lang/String;

.field private mRIds:[J


# virtual methods
.method public deletekeys()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x2

    .line 44
    const/4 v5, 0x2

    .line 45
    .local v5, "retvalue":I
    const/4 v1, 0x0

    .line 46
    .local v1, "deletecount":I
    const/4 v0, 0x0

    .line 47
    .local v0, "badcount":I
    new-instance v4, Lcom/android/emailcommon/pgp/PGPOperationResultData;

    invoke-direct {v4}, Lcom/android/emailcommon/pgp/PGPOperationResultData;-><init>()V

    .line 48
    .local v4, "res":Lcom/android/emailcommon/pgp/PGPOperationResultData;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 50
    .local v3, "msg":Landroid/os/Bundle;
    iget-boolean v6, p0, Lcom/android/emailcommon/pgp/PGPDeleteKeys;->DELETE_KEYS_BY_RID:Z

    if-eqz v6, :cond_3

    .line 51
    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPDeleteKeys;->mRIds:[J

    array-length v6, v6

    if-nez v6, :cond_1

    .line 52
    const/4 v5, 0x4

    .line 80
    :cond_0
    :goto_0
    const-string v6, "deletecount"

    invoke-virtual {v3, v6, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 81
    const-string v6, "badcount"

    invoke-virtual {v3, v6, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 82
    const-string v6, "errorvalue"

    invoke-virtual {v3, v6, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 83
    const-string v6, "Status"

    const/4 v7, 0x1

    invoke-virtual {v3, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 84
    iput-object v3, v4, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mMsg:Landroid/os/Bundle;

    .line 85
    const/16 v6, 0x9

    iput v6, v4, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mPgpOperation:I

    .line 87
    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPDeleteKeys;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    const/4 v7, 0x0

    invoke-interface {v6, v5, v7, v4}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    .line 88
    return-void

    .line 54
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPDeleteKeys;->mRIds:[J

    array-length v6, v6

    if-ge v2, v6, :cond_0

    .line 55
    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPDeleteKeys;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    iget-object v7, p0, Lcom/android/emailcommon/pgp/PGPDeleteKeys;->mRIds:[J

    aget-wide v8, v7, v2

    invoke-virtual {v6, v8, v9}, Lcom/android/emailcommon/pgp/PGPDatabase;->deleteKeyFromDB(J)I

    move-result v5

    .line 56
    if-eq v5, v10, :cond_2

    .line 57
    add-int/lit8 v0, v0, 0x1

    .line 54
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 59
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 63
    .end local v2    # "i":I
    :cond_3
    iget-boolean v6, p0, Lcom/android/emailcommon/pgp/PGPDeleteKeys;->DELETE_KEYS_BY_EMAIL_ID:Z

    if-eqz v6, :cond_6

    .line 64
    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPDeleteKeys;->mEmailIDs:[Ljava/lang/String;

    array-length v6, v6

    if-nez v6, :cond_4

    .line 65
    const/4 v5, 0x4

    goto :goto_0

    .line 67
    :cond_4
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPDeleteKeys;->mEmailIDs:[Ljava/lang/String;

    array-length v6, v6

    if-ge v2, v6, :cond_0

    .line 68
    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPDeleteKeys;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    iget-object v7, p0, Lcom/android/emailcommon/pgp/PGPDeleteKeys;->mEmailIDs:[Ljava/lang/String;

    aget-object v7, v7, v2

    invoke-virtual {v6, v7}, Lcom/android/emailcommon/pgp/PGPDatabase;->deleteKeyFromDB(Ljava/lang/String;)I

    move-result v5

    .line 69
    if-eq v5, v10, :cond_5

    .line 70
    add-int/lit8 v0, v0, 0x1

    .line 67
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 72
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 77
    .end local v2    # "i":I
    :cond_6
    iget-object v6, p0, Lcom/android/emailcommon/pgp/PGPDeleteKeys;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    invoke-virtual {v6}, Lcom/android/emailcommon/pgp/PGPDatabase;->deleteAllKeys()I

    goto :goto_0
.end method

.method public run()V
    .locals 1

    .prologue
    .line 37
    :try_start_0
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPDeleteKeys;->deletekeys()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    :goto_0
    return-void

    .line 38
    :catch_0
    move-exception v0

    .line 39
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
