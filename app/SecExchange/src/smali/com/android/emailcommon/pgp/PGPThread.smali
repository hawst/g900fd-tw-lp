.class public Lcom/android/emailcommon/pgp/PGPThread;
.super Ljava/lang/Thread;
.source "PGPThread.java"


# instance fields
.field private mCancel:Z


# direct methods
.method constructor <init>(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 8
    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPThread;->mCancel:Z

    .line 9
    return-void
.end method


# virtual methods
.method public isCancel()Z
    .locals 1

    .prologue
    .line 18
    monitor-enter p0

    .line 19
    :try_start_0
    iget-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPThread;->mCancel:Z

    monitor-exit p0

    return v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
