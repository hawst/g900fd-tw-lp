.class public Lcom/android/emailcommon/pgp/DecryptVerifyParams;
.super Ljava/lang/Object;
.source "DecryptVerifyParams.java"


# instance fields
.field private mDetSignInStream:Ljava/io/InputStream;

.field private mInputStream:Ljava/io/InputStream;

.field private mOutputStream:Ljava/io/OutputStream;

.field private mPassPhrase:Ljava/lang/String;

.field private mSecretKeyId:J

.field private mSize:J


# virtual methods
.method public getDetSignInStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/emailcommon/pgp/DecryptVerifyParams;->mDetSignInStream:Ljava/io/InputStream;

    return-object v0
.end method

.method public getInputDataSize()J
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/android/emailcommon/pgp/DecryptVerifyParams;->mSize:J

    return-wide v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/android/emailcommon/pgp/DecryptVerifyParams;->mInputStream:Ljava/io/InputStream;

    return-object v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/emailcommon/pgp/DecryptVerifyParams;->mOutputStream:Ljava/io/OutputStream;

    return-object v0
.end method

.method public getPassphrase()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/emailcommon/pgp/DecryptVerifyParams;->mPassPhrase:Ljava/lang/String;

    return-object v0
.end method

.method public getSecretKeyId()J
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/android/emailcommon/pgp/DecryptVerifyParams;->mSecretKeyId:J

    return-wide v0
.end method
