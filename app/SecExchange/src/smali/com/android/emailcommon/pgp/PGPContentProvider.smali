.class public Lcom/android/emailcommon/pgp/PGPContentProvider;
.super Landroid/content/ContentProvider;
.source "PGPContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final sUriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mOpenHelper:Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 44
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/android/emailcommon/pgp/PGPContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 45
    sget-object v0, Lcom/android/emailcommon/pgp/PGPContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.android.emailcommon.pgp.provider.PGPContentProvider"

    const-string v2, "publickeys"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 46
    sget-object v0, Lcom/android/emailcommon/pgp/PGPContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.android.emailcommon.pgp.provider.PGPContentProvider"

    const-string v2, "privatekeys"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 47
    sget-object v0, Lcom/android/emailcommon/pgp/PGPContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.android.emailcommon.pgp.provider.PGPContentProvider"

    const-string v2, "keyrings"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 48
    sget-object v0, Lcom/android/emailcommon/pgp/PGPContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.android.emailcommon.pgp.provider.PGPContentProvider"

    const-string v2, "innerjoin"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 49
    sget-object v0, Lcom/android/emailcommon/pgp/PGPContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.android.emailcommon.pgp.provider.PGPContentProvider"

    const-string v2, "defaultkeys"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 50
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 53
    return-void
.end method

.method private InsertorUpdateKeyRing(Landroid/content/ContentValues;)J
    .locals 14
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 425
    iget-object v1, p0, Lcom/android/emailcommon/pgp/PGPContentProvider;->mOpenHelper:Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;

    if-nez v1, :cond_1

    .line 426
    const-string v1, "PGPContentProvider"

    const-string v2, "mOpenHelper instance null"

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 427
    const-wide/16 v12, -0x1

    .line 475
    :cond_0
    :goto_0
    return-wide v12

    .line 430
    :cond_1
    iget-object v1, p0, Lcom/android/emailcommon/pgp/PGPContentProvider;->mOpenHelper:Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;

    invoke-virtual {v1}, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 432
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_2

    .line 433
    const-string v1, "PGPContentProvider"

    const-string v2, "SQLiteDatabase instance null"

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 434
    const-wide/16 v12, -0x1

    goto :goto_0

    .line 437
    :cond_2
    const/4 v8, 0x0

    .line 438
    .local v8, "cursor":Landroid/database/Cursor;
    const-wide/16 v12, -0x1

    .line 441
    .local v12, "rowId":J
    :try_start_0
    const-string v1, "keyrings"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "c_master_key_id = ? AND c_type = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "c_master_key_id"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "c_type"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 449
    if-eqz v8, :cond_6

    .line 451
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 452
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 453
    const-string v1, "keyrings"

    const-string v2, "_id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 463
    :goto_1
    if-eqz v8, :cond_0

    .line 464
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 469
    :catch_0
    move-exception v9

    .line 470
    .local v9, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v9}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 471
    const-string v1, "PGPContentProvider"

    const-string v2, "RuntimeException in updatePublicKey"

    invoke-static {v1, v2, v9}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 472
    const-wide/16 v12, -0x1

    goto/16 :goto_0

    .line 457
    .end local v9    # "e":Ljava/lang/RuntimeException;
    :cond_3
    :try_start_3
    const-string v1, "keyrings"

    const-string v2, "c_key_ring_data"

    invoke-virtual {v0, v1, v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-wide v12

    goto :goto_1

    .line 459
    :catch_1
    move-exception v10

    .line 460
    .local v10, "ne":Ljava/lang/NullPointerException;
    :try_start_4
    invoke-virtual {v10}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 461
    const-wide/16 v2, -0x1

    .line 463
    if-eqz v8, :cond_4

    .line 464
    :try_start_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    move-wide v12, v2

    goto/16 :goto_0

    .line 463
    .end local v10    # "ne":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_5

    .line 464
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v1

    .line 467
    :cond_6
    const-string v1, "keyrings"

    const-string v2, "c_key_ring_data"

    invoke-virtual {v0, v1, v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_0

    move-result-wide v12

    goto/16 :goto_0
.end method

.method private InsertorUpdatePrivateKey(Landroid/content/ContentValues;)J
    .locals 14
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 536
    iget-object v1, p0, Lcom/android/emailcommon/pgp/PGPContentProvider;->mOpenHelper:Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;

    if-nez v1, :cond_1

    .line 537
    const-string v1, "PGPContentProvider"

    const-string v2, "mOpenHelper instance null"

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 538
    const-wide/16 v12, -0x1

    .line 588
    :cond_0
    :goto_0
    return-wide v12

    .line 541
    :cond_1
    iget-object v1, p0, Lcom/android/emailcommon/pgp/PGPContentProvider;->mOpenHelper:Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;

    invoke-virtual {v1}, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 543
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_2

    .line 544
    const-string v1, "PGPContentProvider"

    const-string v2, "SQLiteDatabase instance null"

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 545
    const-wide/16 v12, -0x1

    goto :goto_0

    .line 548
    :cond_2
    const/4 v8, 0x0

    .line 549
    .local v8, "cursor":Landroid/database/Cursor;
    const-wide/16 v12, -0x1

    .line 552
    .local v12, "rowId":J
    :try_start_0
    const-string v1, "privatekeys"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "c_key_id = ? "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "c_key_id"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 560
    if-eqz v8, :cond_6

    .line 562
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 563
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 564
    const-string v1, "privatekeys"

    const-string v2, "_id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 576
    :goto_1
    if-eqz v8, :cond_0

    .line 577
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 582
    :catch_0
    move-exception v9

    .line 583
    .local v9, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v9}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 584
    const-string v1, "PGPContentProvider"

    const-string v2, "RuntimeException in updatePublicKey"

    invoke-static {v1, v2, v9}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 585
    const-wide/16 v12, -0x1

    goto :goto_0

    .line 569
    .end local v9    # "e":Ljava/lang/RuntimeException;
    :cond_3
    :try_start_3
    const-string v1, "privatekeys"

    const-string v2, "c_key_ring"

    invoke-virtual {v0, v1, v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-wide v12

    goto :goto_1

    .line 572
    :catch_1
    move-exception v10

    .line 573
    .local v10, "ne":Ljava/lang/NullPointerException;
    :try_start_4
    invoke-virtual {v10}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 574
    const-wide/16 v2, -0x1

    .line 576
    if-eqz v8, :cond_4

    .line 577
    :try_start_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    move-wide v12, v2

    goto/16 :goto_0

    .line 576
    .end local v10    # "ne":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_5

    .line 577
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v1

    .line 580
    :cond_6
    const-string v1, "privatekeys"

    const-string v2, "c_key_ring"

    invoke-virtual {v0, v1, v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_0

    move-result-wide v12

    goto/16 :goto_0
.end method

.method private InsertorUpdatePublicKey(Landroid/content/ContentValues;)J
    .locals 14
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 480
    iget-object v1, p0, Lcom/android/emailcommon/pgp/PGPContentProvider;->mOpenHelper:Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;

    if-nez v1, :cond_1

    .line 481
    const-string v1, "PGPContentProvider"

    const-string v2, "mOpenHelper instance null"

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 482
    const-wide/16 v12, -0x1

    .line 531
    :cond_0
    :goto_0
    return-wide v12

    .line 485
    :cond_1
    iget-object v1, p0, Lcom/android/emailcommon/pgp/PGPContentProvider;->mOpenHelper:Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;

    invoke-virtual {v1}, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 487
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_2

    .line 488
    const-string v1, "PGPContentProvider"

    const-string v2, "SQLiteDatabase instance null"

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v1, v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 489
    const-wide/16 v12, -0x1

    goto :goto_0

    .line 492
    :cond_2
    const/4 v8, 0x0

    .line 493
    .local v8, "cursor":Landroid/database/Cursor;
    const-wide/16 v12, -0x1

    .line 496
    .local v12, "rowId":J
    :try_start_0
    const-string v1, "publickeys"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "c_key_id = ? "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "c_key_id"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 504
    if-eqz v8, :cond_6

    .line 506
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 507
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 508
    const-string v1, "publickeys"

    const-string v2, "_id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 519
    :goto_1
    if-eqz v8, :cond_0

    .line 520
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 525
    :catch_0
    move-exception v9

    .line 526
    .local v9, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v9}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 527
    const-string v1, "PGPContentProvider"

    const-string v2, "RuntimeException in updatePublicKey"

    invoke-static {v1, v2, v9}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 528
    const-wide/16 v12, -0x1

    goto :goto_0

    .line 513
    .end local v9    # "e":Ljava/lang/RuntimeException;
    :cond_3
    :try_start_3
    const-string v1, "publickeys"

    const-string v2, "c_key_ring"

    invoke-virtual {v0, v1, v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-wide v12

    goto :goto_1

    .line 515
    :catch_1
    move-exception v10

    .line 516
    .local v10, "ne":Ljava/lang/NullPointerException;
    :try_start_4
    invoke-virtual {v10}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 517
    const-wide/16 v2, -0x1

    .line 519
    if-eqz v8, :cond_4

    .line 520
    :try_start_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    move-wide v12, v2

    goto/16 :goto_0

    .line 519
    .end local v10    # "ne":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v1

    if-eqz v8, :cond_5

    .line 520
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v1

    .line 523
    :cond_6
    const-string v1, "publickeys"

    const-string v2, "c_key_ring"

    invoke-virtual {v0, v1, v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_0

    move-result-wide v12

    goto/16 :goto_0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "where"    # Ljava/lang/String;
    .param p3, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 197
    iget-object v2, p0, Lcom/android/emailcommon/pgp/PGPContentProvider;->mOpenHelper:Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;

    invoke-virtual {v2}, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 199
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v2, Lcom/android/emailcommon/pgp/PGPContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 213
    :pswitch_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid URI:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 201
    :pswitch_1
    const-string v2, "publickeys"

    invoke-virtual {v1, v2, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 215
    .local v0, "count":I
    :goto_0
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 216
    return v0

    .line 204
    .end local v0    # "count":I
    :pswitch_2
    const-string v2, "privatekeys"

    invoke-virtual {v1, v2, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 205
    .restart local v0    # "count":I
    goto :goto_0

    .line 207
    .end local v0    # "count":I
    :pswitch_3
    const-string v2, "keyrings"

    invoke-virtual {v1, v2, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 208
    .restart local v0    # "count":I
    goto :goto_0

    .line 210
    .end local v0    # "count":I
    :pswitch_4
    const-string v2, "defaultkeys"

    invoke-virtual {v1, v2, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 211
    .restart local v0    # "count":I
    goto :goto_0

    .line 199
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 220
    sget-object v0, Lcom/android/emailcommon/pgp/PGPContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 232
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid URI"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 222
    :pswitch_0
    const-string v0, "vnd.android.cursor.dir/pgp.contentprovider.org.public.key"

    .line 226
    :goto_0
    return-object v0

    .line 224
    :pswitch_1
    const-string v0, "vnd.android.cursor.dir/pgp.contentprovider.org.private.key"

    goto :goto_0

    .line 226
    :pswitch_2
    const-string v0, "vnd.android.cursor.dir/pgp.contentprovider.org.keyring"

    goto :goto_0

    .line 220
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "initialValues"    # Landroid/content/ContentValues;

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 357
    if-eqz p2, :cond_1

    .line 358
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4, p2}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 363
    .local v4, "values":Landroid/content/ContentValues;
    :goto_0
    sget-object v5, Lcom/android/emailcommon/pgp/PGPContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 418
    :cond_0
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknow URI"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 360
    .end local v4    # "values":Landroid/content/ContentValues;
    :cond_1
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .restart local v4    # "values":Landroid/content/ContentValues;
    goto :goto_0

    .line 365
    :pswitch_0
    invoke-direct {p0, v4}, Lcom/android/emailcommon/pgp/PGPContentProvider;->InsertorUpdatePublicKey(Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 366
    .local v2, "rowId":J
    cmp-long v5, v2, v8

    if-lez v5, :cond_2

    .line 367
    sget-object v5, Lcom/android/emailcommon/pgp/PublicKey$PublicKeys;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v5, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 373
    .local v0, "pubUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, v0, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    move-object v1, v0

    .line 407
    .end local v0    # "pubUri":Landroid/net/Uri;
    .local v1, "pubUri":Landroid/net/Uri;
    :goto_1
    return-object v1

    .line 378
    .end local v1    # "pubUri":Landroid/net/Uri;
    .end local v2    # "rowId":J
    :pswitch_1
    invoke-direct {p0, v4}, Lcom/android/emailcommon/pgp/PGPContentProvider;->InsertorUpdatePrivateKey(Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 379
    .restart local v2    # "rowId":J
    cmp-long v5, v2, v8

    if-lez v5, :cond_2

    .line 384
    sget-object v5, Lcom/android/emailcommon/pgp/PrivateKey$PrivateKeys;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v5, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 390
    .restart local v0    # "pubUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, v0, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    move-object v1, v0

    .line 391
    .end local v0    # "pubUri":Landroid/net/Uri;
    .restart local v1    # "pubUri":Landroid/net/Uri;
    goto :goto_1

    .line 395
    .end local v1    # "pubUri":Landroid/net/Uri;
    .end local v2    # "rowId":J
    :pswitch_2
    invoke-direct {p0, v4}, Lcom/android/emailcommon/pgp/PGPContentProvider;->InsertorUpdateKeyRing(Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 396
    .restart local v2    # "rowId":J
    cmp-long v5, v2, v8

    if-lez v5, :cond_0

    .line 401
    sget-object v5, Lcom/android/emailcommon/pgp/KeyRing$KeyRings;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v5, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 406
    .restart local v0    # "pubUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPContentProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, v0, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    move-object v1, v0

    .line 407
    .end local v0    # "pubUri":Landroid/net/Uri;
    .restart local v1    # "pubUri":Landroid/net/Uri;
    goto :goto_1

    .line 420
    .end local v1    # "pubUri":Landroid/net/Uri;
    :cond_2
    new-instance v5, Landroid/database/SQLException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed to insert row into"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 363
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 2

    .prologue
    .line 237
    new-instance v0, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;

    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/emailcommon/pgp/PGPContentProvider;->mOpenHelper:Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;

    .line 238
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 243
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 245
    .local v1, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    sget-object v2, Lcom/android/emailcommon/pgp/PGPContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 299
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknow URI"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 247
    :pswitch_0
    const-string v2, "publickeys"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 302
    :goto_0
    iget-object v2, p0, Lcom/android/emailcommon/pgp/PGPContentProvider;->mOpenHelper:Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;

    if-nez v2, :cond_3

    .line 303
    const-string v2, "PGPContentProvider"

    const-string v3, "mOpenHelper instance null"

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v2, v3, v4}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 304
    const/4 v10, 0x0

    .line 329
    .end local v1    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    :goto_1
    return-object v10

    .line 250
    .restart local v1    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    :pswitch_1
    const-string v2, "privatekeys"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 253
    :pswitch_2
    const-string v2, "keyrings"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 256
    :pswitch_3
    const-string v2, "defaultkeys"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 265
    :pswitch_4
    iget-object v2, p0, Lcom/android/emailcommon/pgp/PGPContentProvider;->mOpenHelper:Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;

    if-nez v2, :cond_0

    .line 266
    const-string v2, "PGPContentProvider"

    const-string v3, "mOpenHelper instance null"

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v2, v3, v4}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 267
    const/4 v10, 0x0

    goto :goto_1

    .line 270
    :cond_0
    iget-object v2, p0, Lcom/android/emailcommon/pgp/PGPContentProvider;->mOpenHelper:Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;

    invoke-virtual {v2}, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 272
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_1

    .line 273
    const-string v2, "PGPContentProvider"

    const-string v3, "SQLiteDatabase instance null"

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v2, v3, v4}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 274
    const/4 v10, 0x0

    goto :goto_1

    .line 277
    :cond_1
    const/4 v10, 0x0

    .line 280
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v1, "publickeys INNER JOIN privatekeys ON (publickeys.c_key_id = privatekeys.c_key_id)"

    .end local v1    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 286
    if-eqz v10, :cond_2

    .line 287
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-interface {v10, v2, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 292
    :catch_0
    move-exception v11

    .line 293
    .local v11, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v11}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_1

    .line 289
    .end local v11    # "e":Ljava/lang/RuntimeException;
    :cond_2
    :try_start_1
    const-string v2, "PGPContentProvider"

    const-string v3, "cursor is null at PUBPRIVATE_INNERJOIN_QUERY condition"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 307
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v10    # "cursor":Landroid/database/Cursor;
    .restart local v1    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    :cond_3
    iget-object v2, p0, Lcom/android/emailcommon/pgp/PGPContentProvider;->mOpenHelper:Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;

    invoke-virtual {v2}, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 309
    .restart local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    if-nez v0, :cond_4

    .line 310
    const-string v2, "PGPContentProvider"

    const-string v3, "SQLiteDatabase instance null"

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v2, v3, v4}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 311
    const/4 v10, 0x0

    goto :goto_1

    .line 314
    :cond_4
    const/4 v9, 0x0

    .line 317
    .local v9, "c":Landroid/database/Cursor;
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, v0

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v8, p5

    :try_start_2
    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 320
    if-eqz v9, :cond_5

    .line 321
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-interface {v9, v2, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    :goto_2
    move-object v10, v9

    .line 329
    goto/16 :goto_1

    .line 323
    :cond_5
    const-string v2, "PGPContentProvider"

    const-string v3, "cursor is null"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 325
    :catch_1
    move-exception v11

    .line 326
    .restart local v11    # "e":Ljava/lang/RuntimeException;
    invoke-virtual {v11}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_2

    .line 245
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "where"    # Ljava/lang/String;
    .param p4, "whereArgs"    # [Ljava/lang/String;

    .prologue
    .line 333
    iget-object v2, p0, Lcom/android/emailcommon/pgp/PGPContentProvider;->mOpenHelper:Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;

    invoke-virtual {v2}, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 335
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v2, Lcom/android/emailcommon/pgp/PGPContentProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 349
    :pswitch_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid URI"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 337
    :pswitch_1
    const-string v2, "publickeys"

    invoke-virtual {v1, v2, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 351
    .local v0, "count":I
    :goto_0
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 352
    return v0

    .line 340
    .end local v0    # "count":I
    :pswitch_2
    const-string v2, "privatekeys"

    invoke-virtual {v1, v2, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 341
    .restart local v0    # "count":I
    goto :goto_0

    .line 343
    .end local v0    # "count":I
    :pswitch_3
    const-string v2, "keyrings"

    invoke-virtual {v1, v2, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 344
    .restart local v0    # "count":I
    goto :goto_0

    .line 346
    .end local v0    # "count":I
    :pswitch_4
    const-string v2, "defaultkeys"

    invoke-virtual {v1, v2, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 347
    .restart local v0    # "count":I
    goto :goto_0

    .line 335
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
