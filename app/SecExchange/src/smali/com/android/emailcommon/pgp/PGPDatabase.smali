.class public Lcom/android/emailcommon/pgp/PGPDatabase;
.super Ljava/lang/Object;
.source "PGPDatabase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/pgp/PGPDatabase$KeyRingType;
    }
.end annotation


# static fields
.field private static mContentResolver:Landroid/content/ContentResolver;

.field public static final mKeyRings:Landroid/net/Uri;

.field public static final mPrivateKeys:Landroid/net/Uri;

.field public static final mPublicKeys:Landroid/net/Uri;


# instance fields
.field private mStatus:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/android/emailcommon/pgp/PrivateKey$PrivateKeys;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/android/emailcommon/pgp/PGPDatabase;->mPrivateKeys:Landroid/net/Uri;

    .line 38
    sget-object v0, Lcom/android/emailcommon/pgp/PublicKey$PublicKeys;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/android/emailcommon/pgp/PGPDatabase;->mPublicKeys:Landroid/net/Uri;

    .line 40
    sget-object v0, Lcom/android/emailcommon/pgp/KeyRing$KeyRings;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/android/emailcommon/pgp/PGPDatabase;->mKeyRings:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/emailcommon/pgp/PGPDatabase;->mStatus:I

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    .line 54
    return-void
.end method

.method public static getPublicKeyRing(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    .locals 12
    .param p0, "keyId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v10, 0x0

    const/4 v5, 0x0

    .line 642
    const/4 v6, 0x0

    .line 643
    .local v6, "cursor":Landroid/database/Cursor;
    new-array v2, v1, [Ljava/lang/String;

    const-string v0, "c_key_ring"

    aput-object v0, v2, v5

    .line 646
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "c_key_id=?"

    .line 647
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v1, [Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 650
    .local v4, "selArgs":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 653
    .local v8, "keyRing":[B
    :try_start_0
    sget-object v0, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/pgp/PGPDatabase;->mPublicKeys:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v6

    .line 655
    if-eqz v6, :cond_0

    .line 657
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 658
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v8

    .line 659
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 666
    :goto_0
    if-eqz v6, :cond_0

    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 667
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 676
    :cond_0
    :goto_1
    if-eqz v6, :cond_1

    .line 677
    :try_start_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 682
    :cond_1
    :goto_2
    if-nez v8, :cond_6

    move-object v0, v10

    .line 686
    :goto_3
    return-object v0

    .line 661
    :cond_2
    :try_start_4
    const-string v0, "PGPDataBase"

    const-string v1, "Cursor is empty."

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 663
    :catch_0
    move-exception v9

    .line 664
    .local v9, "ne":Ljava/lang/NullPointerException;
    :try_start_5
    invoke-virtual {v9}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 666
    if-eqz v6, :cond_0

    :try_start_6
    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 667
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    .line 670
    .end local v9    # "ne":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v7

    .line 671
    .local v7, "e":Ljava/lang/RuntimeException;
    :try_start_7
    invoke-virtual {v7}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 672
    const-string v0, "PGPDataBase"

    const-string v1, "RuntimeException in getSecretKeyRing"

    invoke-static {v0, v1, v7}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 676
    if-eqz v6, :cond_3

    .line 677
    :try_start_8
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3

    :cond_3
    :goto_4
    move-object v0, v10

    .line 679
    goto :goto_3

    .line 666
    .end local v7    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    :try_start_9
    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_4

    .line 667
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 675
    :catchall_1
    move-exception v0

    .line 676
    if-eqz v6, :cond_5

    .line 677
    :try_start_a
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4

    .line 679
    :cond_5
    :goto_5
    throw v0

    .line 686
    :cond_6
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

    invoke-direct {v0, v8}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;-><init>([B)V

    goto :goto_3

    .line 678
    :catch_2
    move-exception v0

    goto :goto_2

    .restart local v7    # "e":Ljava/lang/RuntimeException;
    :catch_3
    move-exception v0

    goto :goto_4

    .end local v7    # "e":Ljava/lang/RuntimeException;
    :catch_4
    move-exception v1

    goto :goto_5
.end method

.method public static getSecretKeyRing(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    .locals 12
    .param p0, "keyId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/pgp/PGPKeyException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v10, 0x0

    const/4 v5, 0x0

    .line 564
    const/4 v6, 0x0

    .line 565
    .local v6, "cursor":Landroid/database/Cursor;
    new-array v2, v1, [Ljava/lang/String;

    const-string v0, "c_key_ring"

    aput-object v0, v2, v5

    .line 568
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "c_key_id=?"

    .line 569
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v1, [Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 573
    .local v4, "selArgs":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 576
    .local v8, "keyRing":[B
    :try_start_0
    sget-object v0, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/pgp/PGPDatabase;->mPrivateKeys:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v6

    .line 578
    if-eqz v6, :cond_0

    .line 580
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 581
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v8

    .line 582
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 589
    :goto_0
    if-eqz v6, :cond_0

    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 590
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 599
    :cond_0
    :goto_1
    if-eqz v6, :cond_1

    .line 600
    :try_start_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 605
    :cond_1
    :goto_2
    if-nez v8, :cond_6

    move-object v0, v10

    .line 610
    :goto_3
    return-object v0

    .line 584
    :cond_2
    :try_start_4
    const-string v0, "PGPDataBase"

    const-string v1, "Cursor is empty."

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 586
    :catch_0
    move-exception v9

    .line 587
    .local v9, "ne":Ljava/lang/NullPointerException;
    :try_start_5
    invoke-virtual {v9}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 589
    if-eqz v6, :cond_0

    :try_start_6
    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 590
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    .line 593
    .end local v9    # "ne":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v7

    .line 594
    .local v7, "e":Ljava/lang/RuntimeException;
    :try_start_7
    invoke-virtual {v7}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 595
    const-string v0, "PGPDataBase"

    const-string v1, "RuntimeException in getSecretKeyRing"

    invoke-static {v0, v1, v7}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 599
    if-eqz v6, :cond_3

    .line 600
    :try_start_8
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    :cond_3
    :goto_4
    move-object v0, v10

    .line 602
    goto :goto_3

    .line 589
    .end local v7    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    :try_start_9
    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_4

    .line 590
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 598
    :catchall_1
    move-exception v0

    .line 599
    if-eqz v6, :cond_5

    .line 600
    :try_start_a
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5

    .line 602
    :cond_5
    :goto_5
    throw v0

    .line 610
    :cond_6
    :try_start_b
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    invoke-direct {v0, v8}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;-><init>([B)V
    :try_end_b
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_b .. :try_end_b} :catch_2

    goto :goto_3

    .line 611
    :catch_2
    move-exception v7

    .line 612
    .local v7, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    new-instance v0, Lcom/android/emailcommon/pgp/PGPKeyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "KeyRing Read failed"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/emailcommon/pgp/PGPKeyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 601
    .end local v7    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    :catch_3
    move-exception v0

    goto :goto_2

    .local v7, "e":Ljava/lang/RuntimeException;
    :catch_4
    move-exception v0

    goto :goto_4

    .end local v7    # "e":Ljava/lang/RuntimeException;
    :catch_5
    move-exception v1

    goto :goto_5
.end method

.method private updatePublicKey(J)V
    .locals 31
    .param p1, "keyId"    # J

    .prologue
    .line 102
    const-wide/16 v22, -0x1

    .line 103
    .local v22, "keyID":J
    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "c_key_id"

    aput-object v5, v6, v4

    .line 106
    .local v6, "projection":[Ljava/lang/String;
    const-string v7, "c_id=?"

    .line 107
    .local v7, "selection":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p1

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v4

    .line 111
    .local v8, "selectionArgs":[Ljava/lang/String;
    const/16 v20, 0x0

    .line 113
    .local v20, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v4, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/pgp/PGPDatabase;->mPublicKeys:Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v20

    .line 116
    if-nez v20, :cond_1

    .line 117
    const-string v4, "PGPDataBase"

    const-string v5, "Cursor is NULL. Query failed"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_3

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    :try_start_1
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 122
    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 123
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 124
    const/4 v4, 0x1

    new-array v11, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v11, v4
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 127
    .end local v6    # "projection":[Ljava/lang/String;
    .local v11, "projection":[Ljava/lang/String;
    :try_start_2
    const-string v7, "c_key_id=?"

    .line 128
    const/4 v4, 0x1

    new-array v13, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p1

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v13, v4
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_9
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 140
    .end local v8    # "selectionArgs":[Ljava/lang/String;
    .local v13, "selectionArgs":[Ljava/lang/String;
    if-eqz v20, :cond_2

    .line 141
    :try_start_3
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_8

    .line 151
    :cond_2
    :goto_1
    const-wide/16 v26, -0x1

    .line 154
    .local v26, "rID":J
    :try_start_4
    sget-object v9, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v10, Lcom/android/emailcommon/pgp/PGPDatabase;->mPrivateKeys:Landroid/net/Uri;

    const/4 v14, 0x0

    move-object v12, v7

    invoke-virtual/range {v9 .. v14}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v20

    .line 157
    if-nez v20, :cond_5

    .line 158
    const-string v4, "PGPDataBase"

    const-string v5, "Cursor is NULL. Query failed"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_5

    move-object v8, v13

    .end local v13    # "selectionArgs":[Ljava/lang/String;
    .restart local v8    # "selectionArgs":[Ljava/lang/String;
    move-object v6, v11

    .line 159
    .end local v11    # "projection":[Ljava/lang/String;
    .restart local v6    # "projection":[Ljava/lang/String;
    goto :goto_0

    .line 132
    .end local v26    # "rID":J
    :cond_3
    :try_start_5
    const-string v4, "PGPDataBase"

    const-string v5, "Cursor is empty."

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 140
    if-eqz v20, :cond_0

    .line 141
    :try_start_6
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_0

    .line 142
    :catch_0
    move-exception v4

    goto :goto_0

    .line 135
    :catch_1
    move-exception v24

    .line 136
    .local v24, "ne":Ljava/lang/NullPointerException;
    :goto_2
    :try_start_7
    invoke-virtual/range {v24 .. v24}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 140
    if-eqz v20, :cond_0

    .line 141
    :try_start_8
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_0

    .line 142
    :catch_2
    move-exception v4

    goto :goto_0

    .line 139
    .end local v24    # "ne":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v4

    .line 140
    :goto_3
    if-eqz v20, :cond_4

    .line 141
    :try_start_9
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_7
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_3

    .line 143
    :cond_4
    :goto_4
    :try_start_a
    throw v4
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_3

    .line 145
    :catch_3
    move-exception v21

    .line 146
    .local v21, "e":Ljava/lang/RuntimeException;
    :goto_5
    invoke-virtual/range {v21 .. v21}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 147
    const-string v4, "PGPDataBase"

    const-string v5, "RuntimeException in updatePublicKey"

    move-object/from16 v0, v21

    invoke-static {v4, v5, v0}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 162
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v8    # "selectionArgs":[Ljava/lang/String;
    .end local v21    # "e":Ljava/lang/RuntimeException;
    .restart local v11    # "projection":[Ljava/lang/String;
    .restart local v13    # "selectionArgs":[Ljava/lang/String;
    .restart local v26    # "rID":J
    :cond_5
    :try_start_b
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 163
    const/4 v4, 0x0

    move-object/from16 v0, v20

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_b
    .catch Ljava/lang/NullPointerException; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-result-wide v26

    .line 172
    if-eqz v20, :cond_6

    .line 173
    :try_start_c
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_5

    .line 181
    :cond_6
    new-instance v25, Landroid/content/ContentValues;

    invoke-direct/range {v25 .. v25}, Landroid/content/ContentValues;-><init>()V

    .line 182
    .local v25, "values":Landroid/content/ContentValues;
    const-string v4, "c_id"

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 183
    const-string v28, "c_key_id=?"

    .line 184
    .local v28, "where":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v29, v0

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v22

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v29, v4

    .line 187
    .local v29, "whereArgs":[Ljava/lang/String;
    sget-object v4, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/pgp/PGPDatabase;->mPublicKeys:Landroid/net/Uri;

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v4, v5, v0, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 189
    sget-object v15, Lcom/android/emailcommon/pgp/PGPDatabase;->mPublicKeys:Landroid/net/Uri;

    const-string v16, "c_key_id"

    const-string v17, "c_id"

    move-object/from16 v14, p0

    move-wide/from16 v18, p1

    invoke-virtual/range {v14 .. v19}, Lcom/android/emailcommon/pgp/PGPDatabase;->updateKeyRing(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;J)I

    move-object v8, v13

    .end local v13    # "selectionArgs":[Ljava/lang/String;
    .restart local v8    # "selectionArgs":[Ljava/lang/String;
    move-object v6, v11

    .line 190
    .end local v11    # "projection":[Ljava/lang/String;
    .restart local v6    # "projection":[Ljava/lang/String;
    goto/16 :goto_0

    .line 165
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v8    # "selectionArgs":[Ljava/lang/String;
    .end local v25    # "values":Landroid/content/ContentValues;
    .end local v28    # "where":Ljava/lang/String;
    .end local v29    # "whereArgs":[Ljava/lang/String;
    .restart local v11    # "projection":[Ljava/lang/String;
    .restart local v13    # "selectionArgs":[Ljava/lang/String;
    :cond_7
    :try_start_d
    const-string v4, "PGPDataBase"

    const-string v5, "Cursor is empty."

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/NullPointerException; {:try_start_d .. :try_end_d} :catch_4
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 172
    if-eqz v20, :cond_8

    .line 173
    :try_start_e
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V
    :try_end_e
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_e} :catch_5

    :cond_8
    move-object v8, v13

    .end local v13    # "selectionArgs":[Ljava/lang/String;
    .restart local v8    # "selectionArgs":[Ljava/lang/String;
    move-object v6, v11

    .end local v11    # "projection":[Ljava/lang/String;
    .restart local v6    # "projection":[Ljava/lang/String;
    goto/16 :goto_0

    .line 168
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v8    # "selectionArgs":[Ljava/lang/String;
    .restart local v11    # "projection":[Ljava/lang/String;
    .restart local v13    # "selectionArgs":[Ljava/lang/String;
    :catch_4
    move-exception v24

    .line 169
    .restart local v24    # "ne":Ljava/lang/NullPointerException;
    :try_start_f
    invoke-virtual/range {v24 .. v24}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 172
    if-eqz v20, :cond_9

    .line 173
    :try_start_10
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    :cond_9
    move-object v8, v13

    .end local v13    # "selectionArgs":[Ljava/lang/String;
    .restart local v8    # "selectionArgs":[Ljava/lang/String;
    move-object v6, v11

    .end local v11    # "projection":[Ljava/lang/String;
    .restart local v6    # "projection":[Ljava/lang/String;
    goto/16 :goto_0

    .line 172
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v8    # "selectionArgs":[Ljava/lang/String;
    .end local v24    # "ne":Ljava/lang/NullPointerException;
    .restart local v11    # "projection":[Ljava/lang/String;
    .restart local v13    # "selectionArgs":[Ljava/lang/String;
    :catchall_1
    move-exception v4

    if-eqz v20, :cond_a

    .line 173
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v4
    :try_end_10
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_5

    .line 175
    :catch_5
    move-exception v21

    .line 176
    .restart local v21    # "e":Ljava/lang/RuntimeException;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 177
    const-string v4, "PGPDataBase"

    const-string v5, "RuntimeException in updatePublicKey"

    move-object/from16 v0, v21

    invoke-static {v4, v5, v0}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v8, v13

    .end local v13    # "selectionArgs":[Ljava/lang/String;
    .restart local v8    # "selectionArgs":[Ljava/lang/String;
    move-object v6, v11

    .line 178
    .end local v11    # "projection":[Ljava/lang/String;
    .restart local v6    # "projection":[Ljava/lang/String;
    goto/16 :goto_0

    .line 142
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v8    # "selectionArgs":[Ljava/lang/String;
    .end local v21    # "e":Ljava/lang/RuntimeException;
    .end local v26    # "rID":J
    .restart local v11    # "projection":[Ljava/lang/String;
    .restart local v13    # "selectionArgs":[Ljava/lang/String;
    :catch_6
    move-exception v4

    goto/16 :goto_1

    .end local v11    # "projection":[Ljava/lang/String;
    .end local v13    # "selectionArgs":[Ljava/lang/String;
    .restart local v6    # "projection":[Ljava/lang/String;
    .restart local v8    # "selectionArgs":[Ljava/lang/String;
    :catch_7
    move-exception v5

    goto/16 :goto_4

    .line 145
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v8    # "selectionArgs":[Ljava/lang/String;
    .restart local v11    # "projection":[Ljava/lang/String;
    .restart local v13    # "selectionArgs":[Ljava/lang/String;
    :catch_8
    move-exception v21

    move-object v8, v13

    .end local v13    # "selectionArgs":[Ljava/lang/String;
    .restart local v8    # "selectionArgs":[Ljava/lang/String;
    move-object v6, v11

    .end local v11    # "projection":[Ljava/lang/String;
    .restart local v6    # "projection":[Ljava/lang/String;
    goto/16 :goto_5

    .line 139
    .end local v6    # "projection":[Ljava/lang/String;
    .restart local v11    # "projection":[Ljava/lang/String;
    :catchall_2
    move-exception v4

    move-object v6, v11

    .end local v11    # "projection":[Ljava/lang/String;
    .restart local v6    # "projection":[Ljava/lang/String;
    goto/16 :goto_3

    .line 135
    .end local v6    # "projection":[Ljava/lang/String;
    .restart local v11    # "projection":[Ljava/lang/String;
    :catch_9
    move-exception v24

    move-object v6, v11

    .end local v11    # "projection":[Ljava/lang/String;
    .restart local v6    # "projection":[Ljava/lang/String;
    goto/16 :goto_2
.end method


# virtual methods
.method public deleteAllKeys()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1042
    const/4 v1, 0x2

    .line 1043
    .local v1, "retvalue":I
    const/4 v0, 0x0

    .line 1045
    .local v0, "count":I
    sget-object v2, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    if-nez v2, :cond_0

    .line 1046
    const/16 v2, 0x1f

    .line 1064
    :goto_0
    return v2

    .line 1049
    :cond_0
    sget-object v2, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/android/emailcommon/pgp/PGPDatabase;->mPublicKeys:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1050
    if-gez v0, :cond_1

    .line 1051
    const/16 v1, 0x1a

    .line 1054
    :cond_1
    sget-object v2, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/android/emailcommon/pgp/PGPDatabase;->mPrivateKeys:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1055
    if-gez v0, :cond_2

    .line 1056
    const/16 v1, 0x1b

    .line 1059
    :cond_2
    sget-object v2, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/android/emailcommon/pgp/PGPDatabase;->mKeyRings:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1060
    if-gez v0, :cond_3

    .line 1061
    const/16 v1, 0x1f

    :cond_3
    move v2, v1

    .line 1064
    goto :goto_0
.end method

.method public deleteKeyFromDB(J)I
    .locals 7
    .param p1, "rId"    # J

    .prologue
    .line 956
    const/4 v1, 0x2

    .line 957
    .local v1, "retvalue":I
    const/4 v0, 0x0

    .line 958
    .local v0, "count":I
    const/4 v4, 0x1

    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    .line 962
    .local v2, "selArgs":[Ljava/lang/String;
    const-string v3, "c_id=?"

    .line 963
    .local v3, "selection":Ljava/lang/String;
    sget-object v4, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/pgp/PGPDatabase;->mPublicKeys:Landroid/net/Uri;

    invoke-virtual {v4, v5, v3, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 964
    if-gez v0, :cond_0

    .line 965
    const/16 v1, 0x1a

    .line 968
    :cond_0
    const-string v3, "_id=?"

    .line 969
    sget-object v4, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/pgp/PGPDatabase;->mPrivateKeys:Landroid/net/Uri;

    invoke-virtual {v4, v5, v3, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 970
    if-gez v0, :cond_1

    .line 971
    const/16 v1, 0x1b

    .line 974
    :cond_1
    const-string v3, "c_id=?"

    .line 975
    sget-object v4, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/pgp/PGPDatabase;->mKeyRings:Landroid/net/Uri;

    invoke-virtual {v4, v5, v3, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 976
    if-gez v0, :cond_2

    .line 977
    const/16 v1, 0x1f

    .line 979
    :cond_2
    return v1
.end method

.method public deleteKeyFromDB(Ljava/lang/String;)I
    .locals 22
    .param p1, "emailId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 983
    const/4 v15, 0x2

    .line 984
    .local v15, "retvalue":I
    const/4 v10, 0x0

    .line 985
    .local v10, "count":I
    const-string v7, "c_email_id=?"

    .line 986
    .local v7, "selection":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v4

    .line 991
    .local v8, "selArgs":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 992
    .local v11, "cursor":Landroid/database/Cursor;
    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "c_key_id"

    aput-object v5, v6, v4

    .line 997
    .local v6, "projection":[Ljava/lang/String;
    :try_start_0
    sget-object v4, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/pgp/PGPDatabase;->mPublicKeys:Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v11

    .line 999
    if-eqz v11, :cond_2

    .line 1001
    const/4 v14, 0x0

    .line 1002
    .local v14, "pubKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :cond_0
    :goto_0
    :try_start_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1003
    const/4 v4, 0x0

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/android/emailcommon/pgp/PGPDatabase;->getPublicKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v14

    .line 1004
    if-eqz v14, :cond_0

    .line 1005
    sget-object v4, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/pgp/PGPDatabase;->mKeyRings:Landroid/net/Uri;

    const-string v9, "c_master_key_id=?"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, ""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v14}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    move-object/from16 v0, v16

    invoke-virtual {v4, v5, v9, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v10

    .line 1009
    if-gez v10, :cond_0

    .line 1010
    const/16 v15, 0x1f

    goto :goto_0

    .line 1017
    :cond_1
    if-eqz v11, :cond_2

    .line 1018
    :try_start_2
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1027
    .end local v14    # "pubKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :cond_2
    :goto_1
    sget-object v4, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/pgp/PGPDatabase;->mPublicKeys:Landroid/net/Uri;

    invoke-virtual {v4, v5, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    .line 1028
    if-gez v10, :cond_3

    .line 1029
    const/16 v15, 0x1a

    .line 1032
    :cond_3
    const-string v7, "c_email_id=?"

    .line 1033
    sget-object v4, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/pgp/PGPDatabase;->mPrivateKeys:Landroid/net/Uri;

    invoke-virtual {v4, v5, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    .line 1034
    if-gez v10, :cond_4

    .line 1035
    const/16 v15, 0x1b

    :cond_4
    move v4, v15

    .line 1038
    :goto_2
    return v4

    .line 1014
    .restart local v14    # "pubKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :catch_0
    move-exception v13

    .line 1015
    .local v13, "ne":Ljava/lang/NullPointerException;
    :try_start_3
    invoke-virtual {v13}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1017
    if-eqz v11, :cond_2

    .line 1018
    :try_start_4
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 1021
    .end local v13    # "ne":Ljava/lang/NullPointerException;
    .end local v14    # "pubKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :catch_1
    move-exception v12

    .line 1022
    .local v12, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v12}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 1023
    const-string v4, "PGPDataBase"

    const-string v5, "RuntimeException in getDefaultKeyIdByEmailId"

    invoke-static {v4, v5, v12}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1024
    const/16 v4, 0x1f

    goto :goto_2

    .line 1017
    .end local v12    # "e":Ljava/lang/RuntimeException;
    .restart local v14    # "pubKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :catchall_0
    move-exception v4

    if-eqz v11, :cond_5

    .line 1018
    :try_start_5
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v4
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1
.end method

.method public getKeyRings()Ljava/util/Vector;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 810
    new-instance v9, Ljava/util/Vector;

    invoke-direct {v9}, Ljava/util/Vector;-><init>()V

    .line 811
    .local v9, "keyring":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Object;>;"
    const/4 v6, 0x0

    .line 812
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "c_type"

    aput-object v0, v2, v1

    const-string v0, "c_key_ring_data"

    aput-object v0, v2, v13

    .line 817
    .local v2, "projection":[Ljava/lang/String;
    :try_start_0
    sget-object v0, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/pgp/PGPDatabase;->mKeyRings:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 819
    if-eqz v6, :cond_0

    .line 821
    const/4 v11, 0x0

    .line 822
    .local v11, "obj":Ljava/lang/Object;
    const/4 v7, 0x0

    .line 824
    .end local v11    # "obj":Ljava/lang/Object;
    .local v7, "data":[B
    :goto_0
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 825
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v7

    .line 827
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v13, :cond_1

    .line 828
    new-instance v11, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    invoke-direct {v11, v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;-><init>([B)V

    .line 832
    :goto_1
    invoke-virtual {v9, v11}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 834
    :catch_0
    move-exception v10

    .line 835
    .local v10, "ne":Ljava/lang/NullPointerException;
    :try_start_2
    invoke-virtual {v10}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 837
    if-eqz v6, :cond_0

    .line 838
    :try_start_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1

    .line 847
    .end local v7    # "data":[B
    .end local v9    # "keyring":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Object;>;"
    .end local v10    # "ne":Ljava/lang/NullPointerException;
    :cond_0
    :goto_2
    return-object v9

    .line 830
    .restart local v7    # "data":[B
    .restart local v9    # "keyring":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Object;>;"
    :cond_1
    :try_start_4
    new-instance v11, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

    invoke-direct {v11, v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;-><init>([B)V
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .local v11, "obj":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    goto :goto_1

    .line 837
    .end local v11    # "obj":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    :cond_2
    if-eqz v6, :cond_0

    .line 838
    :try_start_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_2

    .line 841
    .end local v7    # "data":[B
    :catch_1
    move-exception v8

    .line 842
    .local v8, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v8}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 843
    const-string v0, "PGPDataBase"

    const-string v1, "RuntimeException in getKeyRings"

    invoke-static {v0, v1, v8}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v9, v12

    .line 844
    goto :goto_2

    .line 837
    .end local v8    # "e":Ljava/lang/RuntimeException;
    .restart local v7    # "data":[B
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 838
    :try_start_6
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_1
.end method

.method public getPassPhrase(J)[B
    .locals 13
    .param p1, "keyId"    # J

    .prologue
    const/4 v11, 0x0

    .line 527
    const/4 v10, 0x0

    .line 528
    .local v10, "passPhrase":[B
    const/4 v7, 0x0

    .line 529
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "c_pass_phrase"

    aput-object v1, v2, v0

    .line 532
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "c_key_id=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 535
    .local v3, "selection":Ljava/lang/String;
    :try_start_0
    sget-object v0, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/android/emailcommon/pgp/PGPDatabase;->mPrivateKeys:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    .line 537
    if-eqz v7, :cond_0

    .line 539
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 540
    const-string v0, "c_pass_phrase"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 541
    .local v6, "col_ind":I
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v10

    .line 548
    .end local v6    # "col_ind":I
    :goto_0
    if-eqz v7, :cond_0

    .line 549
    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_0
    :goto_1
    move-object v0, v10

    .line 558
    :goto_2
    return-object v0

    .line 543
    :cond_1
    :try_start_3
    const-string v0, "PGPDataBase"

    const-string v1, "Cursor is empty."

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 545
    :catch_0
    move-exception v9

    .line 546
    .local v9, "ne":Ljava/lang/NullPointerException;
    :try_start_4
    invoke-virtual {v9}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 548
    if-eqz v7, :cond_0

    .line 549
    :try_start_5
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 552
    .end local v9    # "ne":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v8

    .line 553
    .local v8, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v8}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 554
    const-string v0, "PGPDataBase"

    const-string v1, "RuntimeException in getPassPhrase"

    invoke-static {v0, v1, v8}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v11

    .line 555
    goto :goto_2

    .line 548
    .end local v8    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 549
    :try_start_6
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_1
.end method

.method public getPrivateKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    .locals 3
    .param p1, "keyId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/pgp/PGPKeyException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 514
    invoke-static {p1, p2}, Lcom/android/emailcommon/pgp/PGPDatabase;->getSecretKeyRing(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    move-result-object v0

    .line 516
    .local v0, "keyRing":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    if-nez v0, :cond_0

    .line 518
    const/4 v1, 0x0

    .line 522
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->getSecretKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-result-object v1

    goto :goto_0
.end method

.method public getPublicKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 3
    .param p1, "keyId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 617
    invoke-static {p1, p2}, Lcom/android/emailcommon/pgp/PGPDatabase;->getPublicKeyRing(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

    move-result-object v0

    .line 619
    .local v0, "keyRing":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    if-nez v0, :cond_0

    .line 620
    const/4 v1, 0x0

    .line 622
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->getPublicKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v1

    goto :goto_0
.end method

.method public getValidPublicKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .locals 5
    .param p1, "keyId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 627
    invoke-static {p1, p2}, Lcom/android/emailcommon/pgp/PGPDatabase;->getPublicKeyRing(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

    move-result-object v0

    .line 628
    .local v0, "keyRing":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    if-nez v0, :cond_1

    move-object v1, v2

    .line 636
    :cond_0
    :goto_0
    return-object v1

    .line 631
    :cond_1
    invoke-virtual {v0, p1, p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->getPublicKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v1

    .line 632
    .local v1, "pubKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    if-eqz v1, :cond_2

    const/16 v3, 0x21

    invoke-static {v1}, Lcom/android/emailcommon/pgp/PGPKeyUtil;->getKeyValidity(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)I

    move-result v4

    if-eq v3, v4, :cond_0

    :cond_2
    move-object v1, v2

    .line 636
    goto :goto_0
.end method

.method public saveKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;)I
    .locals 29
    .param p1, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p2, "pubkeyRing"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 195
    const/16 v25, 0x0

    .line 196
    .local v25, "userinfo":Lcom/android/emailcommon/pgp/PGPUserInfo;
    new-instance v26, Landroid/content/ContentValues;

    invoke-direct/range {v26 .. v26}, Landroid/content/ContentValues;-><init>()V

    .line 197
    .local v26, "values":Landroid/content/ContentValues;
    const/16 v21, 0x2

    .line 199
    .local v21, "returnvalue":I
    const-string v4, "c_algorithm"

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getAlgorithm()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 200
    const-string v4, "c_can_encrypt"

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->isEncryptionKey()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 201
    const-string v4, "c_is_default"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 203
    invoke-static/range {p1 .. p1}, Lcom/android/emailcommon/pgp/PGPKeyUtil;->getCreationDate(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Ljava/util/Date;

    move-result-object v16

    .line 204
    .local v16, "createdate":Ljava/util/Date;
    const-string v4, "c_creation"

    invoke-virtual/range {v16 .. v16}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 206
    invoke-static/range {p1 .. p1}, Lcom/android/emailcommon/pgp/PGPKeyUtil;->getExpirationDate(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Ljava/util/Date;

    move-result-object v19

    .line 208
    .local v19, "expdate":Ljava/util/Date;
    if-eqz v19, :cond_7

    .line 209
    const-string v4, "c_expiry"

    invoke-virtual/range {v19 .. v19}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 214
    :goto_0
    const-string v4, "c_key_id"

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 215
    const-string v4, "c_key_ring"

    invoke-virtual/range {p2 .. p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->getEncoded()[B

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 218
    invoke-static/range {p1 .. p1}, Lcom/android/emailcommon/pgp/PGPKeyUtil;->getMainUserId(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Lcom/android/emailcommon/pgp/PGPUserInfo;

    move-result-object v25

    .line 220
    if-nez v25, :cond_0

    .line 222
    invoke-static/range {p2 .. p2}, Lcom/android/emailcommon/pgp/PGPKeyUtil;->getMainUserIdForRing(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;)Lcom/android/emailcommon/pgp/PGPUserInfo;

    move-result-object v25

    .line 226
    :cond_0
    if-eqz v25, :cond_1

    .line 227
    const-string v4, "c_email_id"

    move-object/from16 v0, v25

    iget-object v5, v0, Lcom/android/emailcommon/pgp/PGPUserInfo;->mUserId:Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const-string v4, "c_user_name"

    move-object/from16 v0, v25

    iget-object v5, v0, Lcom/android/emailcommon/pgp/PGPUserInfo;->mUserName:Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    :cond_1
    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v6, v4

    .line 234
    .local v6, "projection":[Ljava/lang/String;
    const-string v7, "c_key_id=?"

    .line 235
    .local v7, "selection":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v10

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v4

    .line 238
    .local v8, "selectionArgs":[Ljava/lang/String;
    const-wide/16 v22, -0x1

    .line 240
    .local v22, "rowID":J
    const/16 v17, 0x0

    .line 242
    .local v17, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v4, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/pgp/PGPDatabase;->mPrivateKeys:Landroid/net/Uri;

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v17

    .line 245
    if-eqz v17, :cond_2

    .line 247
    :try_start_1
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 248
    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v22

    .line 255
    :goto_1
    if-eqz v17, :cond_2

    .line 256
    :try_start_2
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    .line 265
    :cond_2
    :goto_2
    const-wide/16 v4, -0x1

    cmp-long v4, v22, v4

    if-eqz v4, :cond_3

    .line 266
    const-string v4, "c_id"

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 269
    :cond_3
    sget-object v4, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/pgp/PGPDatabase;->mPublicKeys:Landroid/net/Uri;

    move-object/from16 v0, v26

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v24

    .line 271
    .local v24, "rowId":Landroid/net/Uri;
    const-wide/16 v4, -0x1

    cmp-long v4, v22, v4

    if-nez v4, :cond_5

    .line 273
    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    .end local v6    # "projection":[Ljava/lang/String;
    const/4 v4, 0x0

    const-string v5, "c_key_id"

    aput-object v5, v6, v4

    .line 278
    .restart local v6    # "projection":[Ljava/lang/String;
    :try_start_3
    sget-object v9, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v10, Lcom/android/emailcommon/pgp/PGPDatabase;->mPublicKeys:Landroid/net/Uri;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-string v14, "_id DESC limit 1"

    move-object v11, v6

    invoke-virtual/range {v9 .. v14}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v17

    .line 281
    if-eqz v17, :cond_4

    .line 283
    :try_start_4
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 284
    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-wide v22

    .line 291
    :goto_3
    if-eqz v17, :cond_4

    .line 292
    :try_start_5
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_3

    .line 301
    :cond_4
    :goto_4
    new-instance v26, Landroid/content/ContentValues;

    .end local v26    # "values":Landroid/content/ContentValues;
    invoke-direct/range {v26 .. v26}, Landroid/content/ContentValues;-><init>()V

    .line 302
    .restart local v26    # "values":Landroid/content/ContentValues;
    const-string v4, "c_id"

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 303
    const-string v27, "c_key_id=?"

    .line 304
    .local v27, "where":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v28, v0

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v22

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v28, v4

    .line 307
    .local v28, "whereArgs":[Ljava/lang/String;
    sget-object v4, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v5, Lcom/android/emailcommon/pgp/PGPDatabase;->mPublicKeys:Landroid/net/Uri;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v4, v5, v0, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 310
    .end local v27    # "where":Ljava/lang/String;
    .end local v28    # "whereArgs":[Ljava/lang/String;
    :cond_5
    sget-object v11, Lcom/android/emailcommon/pgp/PGPDatabase;->mPublicKeys:Landroid/net/Uri;

    const-string v12, "c_key_id"

    const-string v13, "c_id"

    invoke-virtual/range {p1 .. p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v14

    move-object/from16 v10, p0

    invoke-virtual/range {v10 .. v15}, Lcom/android/emailcommon/pgp/PGPDatabase;->updateKeyRing(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;J)I

    .line 312
    if-nez v24, :cond_6

    .line 313
    const/16 v21, 0x8

    :cond_6
    move/from16 v4, v21

    .line 316
    .end local v24    # "rowId":Landroid/net/Uri;
    :goto_5
    return v4

    .line 211
    .end local v6    # "projection":[Ljava/lang/String;
    .end local v7    # "selection":Ljava/lang/String;
    .end local v8    # "selectionArgs":[Ljava/lang/String;
    .end local v17    # "cursor":Landroid/database/Cursor;
    .end local v22    # "rowID":J
    :cond_7
    const-string v4, "c_expiry"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_0

    .line 250
    .restart local v6    # "projection":[Ljava/lang/String;
    .restart local v7    # "selection":Ljava/lang/String;
    .restart local v8    # "selectionArgs":[Ljava/lang/String;
    .restart local v17    # "cursor":Landroid/database/Cursor;
    .restart local v22    # "rowID":J
    :cond_8
    :try_start_6
    const-string v4, "PGPDataBase"

    const-string v5, "Cursor is empty."

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    .line 252
    :catch_0
    move-exception v20

    .line 253
    .local v20, "ne":Ljava/lang/NullPointerException;
    :try_start_7
    invoke-virtual/range {v20 .. v20}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 255
    if-eqz v17, :cond_2

    .line 256
    :try_start_8
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_1

    goto/16 :goto_2

    .line 259
    .end local v20    # "ne":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v18

    .line 260
    .local v18, "e":Ljava/lang/RuntimeException;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 261
    const-string v4, "PGPDataBase"

    const-string v5, "RuntimeException in saveKey"

    move-object/from16 v0, v18

    invoke-static {v4, v5, v0}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 262
    const/16 v4, 0x8

    goto :goto_5

    .line 255
    .end local v18    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    if-eqz v17, :cond_9

    .line 256
    :try_start_9
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v4
    :try_end_9
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_1

    .line 286
    .restart local v24    # "rowId":Landroid/net/Uri;
    :cond_a
    :try_start_a
    const-string v4, "PGPDataBase"

    const-string v5, "Cursor is empty."

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/NullPointerException; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_3

    .line 288
    :catch_2
    move-exception v20

    .line 289
    .restart local v20    # "ne":Ljava/lang/NullPointerException;
    :try_start_b
    invoke-virtual/range {v20 .. v20}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 291
    if-eqz v17, :cond_4

    .line 292
    :try_start_c
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_3

    goto/16 :goto_4

    .line 295
    .end local v20    # "ne":Ljava/lang/NullPointerException;
    :catch_3
    move-exception v18

    .line 296
    .restart local v18    # "e":Ljava/lang/RuntimeException;
    invoke-virtual/range {v18 .. v18}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 297
    const-string v4, "PGPDataBase"

    const-string v5, "RuntimeException in saveKey"

    move-object/from16 v0, v18

    invoke-static {v4, v5, v0}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 298
    const/16 v4, 0x8

    goto :goto_5

    .line 291
    .end local v18    # "e":Ljava/lang/RuntimeException;
    :catchall_1
    move-exception v4

    if-eqz v17, :cond_b

    .line 292
    :try_start_d
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v4
    :try_end_d
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_3
.end method

.method public saveKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;)I
    .locals 12
    .param p1, "key"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    .param p2, "seckeyRing"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v10, 0x3e8

    const/4 v8, 0x0

    .line 58
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 59
    .local v4, "values":Landroid/content/ContentValues;
    const/4 v5, 0x2

    iput v5, p0, Lcom/android/emailcommon/pgp/PGPDatabase;->mStatus:I

    .line 60
    const/4 v3, 0x0

    .line 62
    .local v3, "userinfo":Lcom/android/emailcommon/pgp/PGPUserInfo;
    const-string v5, "c_algorithm"

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getAlgorithm()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 63
    const-string v5, "c_can_sign"

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->isSigningKey()Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 65
    invoke-static {p1}, Lcom/android/emailcommon/pgp/PGPKeyUtil;->getCreationDate(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;)Ljava/util/Date;

    move-result-object v0

    .line 66
    .local v0, "createdate":Ljava/util/Date;
    const-string v5, "c_creation"

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    div-long/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 68
    invoke-static {p1}, Lcom/android/emailcommon/pgp/PGPKeyUtil;->getExpirationDate(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;)Ljava/util/Date;

    move-result-object v1

    .line 70
    .local v1, "expdate":Ljava/util/Date;
    if-eqz v1, :cond_3

    .line 71
    const-string v5, "c_expiry"

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    div-long/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 76
    :goto_0
    const-string v5, "c_key_id"

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getKeyID()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 77
    const-string v5, "c_key_ring"

    invoke-virtual {p2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->getEncoded()[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 78
    const-string v5, "c_is_default"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 79
    invoke-static {p1}, Lcom/android/emailcommon/pgp/PGPKeyUtil;->getMainUserId(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;)Lcom/android/emailcommon/pgp/PGPUserInfo;

    move-result-object v3

    .line 81
    if-nez v3, :cond_0

    .line 82
    invoke-static {p2}, Lcom/android/emailcommon/pgp/PGPKeyUtil;->getMainUserIdForRing(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;)Lcom/android/emailcommon/pgp/PGPUserInfo;

    move-result-object v3

    .line 85
    :cond_0
    if-eqz v3, :cond_1

    .line 86
    const-string v5, "c_email_id"

    iget-object v6, v3, Lcom/android/emailcommon/pgp/PGPUserInfo;->mUserId:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string v5, "c_user_name"

    iget-object v6, v3, Lcom/android/emailcommon/pgp/PGPUserInfo;->mUserName:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :cond_1
    sget-object v5, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v6, Lcom/android/emailcommon/pgp/PGPDatabase;->mPrivateKeys:Landroid/net/Uri;

    invoke-virtual {v5, v6, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    .line 91
    .local v2, "rowId":Landroid/net/Uri;
    if-nez v2, :cond_2

    .line 92
    const/16 v5, 0x8

    iput v5, p0, Lcom/android/emailcommon/pgp/PGPDatabase;->mStatus:I

    .line 95
    :cond_2
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getKeyID()J

    move-result-wide v6

    invoke-direct {p0, v6, v7}, Lcom/android/emailcommon/pgp/PGPDatabase;->updatePublicKey(J)V

    .line 97
    iget v5, p0, Lcom/android/emailcommon/pgp/PGPDatabase;->mStatus:I

    return v5

    .line 73
    .end local v2    # "rowId":Landroid/net/Uri;
    :cond_3
    const-string v5, "c_expiry"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public saveKeyRing(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;)I
    .locals 8
    .param p1, "pubkeyring"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 325
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 327
    .local v5, "values":Landroid/content/ContentValues;
    const/4 v1, 0x2

    .line 329
    .local v1, "returnvalue":I
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v0

    .line 331
    .local v0, "masterKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v2

    .line 333
    .local v2, "masterkeyid":J
    const-string v6, "c_master_key_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 335
    const-string v6, "c_type"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 337
    const-string v6, "c_key_ring_data"

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->getEncoded()[B

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 339
    sget-object v6, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v7, Lcom/android/emailcommon/pgp/PGPDatabase;->mKeyRings:Landroid/net/Uri;

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v4

    .line 341
    .local v4, "rowId":Landroid/net/Uri;
    if-nez v4, :cond_0

    .line 343
    const/16 v1, 0x8

    .line 347
    :cond_0
    return v1
.end method

.method public saveKeyRing(Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;)I
    .locals 8
    .param p1, "seckeyring"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 357
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 359
    .local v5, "values":Landroid/content/ContentValues;
    const/4 v1, 0x2

    .line 361
    .local v1, "returnvalue":I
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->getSecretKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-result-object v0

    .line 363
    .local v0, "masterKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getKeyID()J

    move-result-wide v2

    .line 365
    .local v2, "masterkeyid":J
    const-string v6, "c_master_key_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 367
    const-string v6, "c_type"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 369
    const-string v6, "c_key_ring_data"

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->getEncoded()[B

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 371
    sget-object v6, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v7, Lcom/android/emailcommon/pgp/PGPDatabase;->mKeyRings:Landroid/net/Uri;

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v4

    .line 373
    .local v4, "rowId":Landroid/net/Uri;
    if-nez v4, :cond_0

    .line 375
    const/16 v1, 0x8

    .line 379
    :cond_0
    return v1
.end method

.method public updateKeyRing(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;J)I
    .locals 16
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "keyId"    # J

    .prologue
    .line 458
    const/4 v13, -0x1

    .line 459
    .local v13, "result":I
    const-wide/16 v10, -0x1

    .line 460
    .local v10, "data":J
    const/4 v14, 0x2

    .line 461
    .local v14, "returnvalue":I
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p3, v4, v2

    .line 464
    .local v4, "projection":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 465
    .local v5, "where":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p4

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 469
    .local v6, "whereArgs":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 472
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v2, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v7, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v8

    .line 474
    if-eqz v8, :cond_0

    .line 476
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 477
    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v10

    .line 484
    :goto_0
    if-eqz v8, :cond_0

    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 485
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    .line 494
    :cond_0
    :goto_1
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 495
    .local v15, "values":Landroid/content/ContentValues;
    const-string v2, "c_id"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v15, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 497
    const-string v5, "c_master_key_id=?"

    .line 498
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    .end local v6    # "whereArgs":[Ljava/lang/String;
    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p4

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 502
    .restart local v6    # "whereArgs":[Ljava/lang/String;
    sget-object v2, Lcom/android/emailcommon/pgp/PGPDatabase;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/android/emailcommon/pgp/PGPDatabase;->mKeyRings:Landroid/net/Uri;

    invoke-virtual {v2, v3, v15, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v13

    .line 503
    const/4 v2, -0x1

    if-ne v13, v2, :cond_1

    .line 504
    const/16 v14, 0x8

    :cond_1
    move v2, v14

    .line 506
    .end local v15    # "values":Landroid/content/ContentValues;
    :goto_2
    return v2

    .line 479
    :cond_2
    :try_start_3
    const-string v2, "PGPDataBase"

    const-string v3, "Cursor is empty."

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 481
    :catch_0
    move-exception v12

    .line 482
    .local v12, "ne":Ljava/lang/NullPointerException;
    :try_start_4
    invoke-virtual {v12}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 484
    if-eqz v8, :cond_0

    :try_start_5
    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 485
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 488
    .end local v12    # "ne":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v9

    .line 489
    .local v9, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v9}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 490
    const-string v2, "PGPDataBase"

    const-string v3, "RuntimeException in updateKeyRing"

    invoke-static {v2, v3, v9}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 491
    const/16 v2, 0x8

    goto :goto_2

    .line 484
    .end local v9    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v2

    if-eqz v8, :cond_3

    :try_start_6
    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_3

    .line 485
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_1
.end method
