.class public Lcom/android/emailcommon/pgp/PGPHelper$Attachment;
.super Ljava/lang/Object;
.source "PGPHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/emailcommon/pgp/PGPHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Attachment"
.end annotation


# instance fields
.field public mAttachmentBody:Lcom/android/emailcommon/mail/Part;

.field public mContentId:Ljava/lang/String;

.field public mEncoding:Ljava/lang/String;

.field public mFileName:Ljava/lang/String;

.field public mIsInline:I

.field public mMimeType:Ljava/lang/String;

.field public mSize:I

.field public mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    const-string v0, "base64"

    iput-object v0, p0, Lcom/android/emailcommon/pgp/PGPHelper$Attachment;->mEncoding:Ljava/lang/String;

    return-void
.end method
