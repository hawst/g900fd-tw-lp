.class public Lcom/android/emailcommon/pgp/PGPOperation;
.super Ljava/lang/Object;
.source "PGPOperation.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/pgp/PGPOperation$opCode;
    }
.end annotation


# instance fields
.field mSyncOp:Z

.field mThread:Lcom/android/emailcommon/pgp/PGPThread;

.field mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-object v0, p0, Lcom/android/emailcommon/pgp/PGPOperation;->mThread:Lcom/android/emailcommon/pgp/PGPThread;

    .line 7
    iput-object v0, p0, Lcom/android/emailcommon/pgp/PGPOperation;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    .line 8
    iput-boolean v1, p0, Lcom/android/emailcommon/pgp/PGPOperation;->mSyncOp:Z

    .line 11
    iput-object v0, p0, Lcom/android/emailcommon/pgp/PGPOperation;->mThread:Lcom/android/emailcommon/pgp/PGPThread;

    .line 12
    iput-object v0, p0, Lcom/android/emailcommon/pgp/PGPOperation;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    .line 13
    iput-boolean v1, p0, Lcom/android/emailcommon/pgp/PGPOperation;->mSyncOp:Z

    .line 14
    return-void
.end method


# virtual methods
.method public run()V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method public setPGPUpdater(Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;)V
    .locals 0
    .param p1, "updater"    # Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/android/emailcommon/pgp/PGPOperation;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    .line 38
    return-void
.end method

.method public setSynOp(Z)V
    .locals 0
    .param p1, "isSynOp"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/android/emailcommon/pgp/PGPOperation;->mSyncOp:Z

    .line 34
    return-void
.end method

.method public startOperation()I
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/android/emailcommon/pgp/PGPThread;

    invoke-direct {v0, p0}, Lcom/android/emailcommon/pgp/PGPThread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/android/emailcommon/pgp/PGPOperation;->mThread:Lcom/android/emailcommon/pgp/PGPThread;

    .line 42
    iget-object v0, p0, Lcom/android/emailcommon/pgp/PGPOperation;->mThread:Lcom/android/emailcommon/pgp/PGPThread;

    invoke-virtual {v0}, Lcom/android/emailcommon/pgp/PGPThread;->start()V

    .line 43
    const/4 v0, 0x2

    return v0
.end method
