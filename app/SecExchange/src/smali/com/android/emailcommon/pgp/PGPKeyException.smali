.class public Lcom/android/emailcommon/pgp/PGPKeyException;
.super Ljava/lang/Exception;
.source "PGPKeyException.java"


# static fields
.field private static final serialVersionUID:J = -0x6e4fa14e03d818f5L


# instance fields
.field private mErrcode:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 11
    return-void
.end method


# virtual methods
.method public setErrorcode(I)V
    .locals 0
    .param p1, "errcode"    # I

    .prologue
    .line 14
    iput p1, p0, Lcom/android/emailcommon/pgp/PGPKeyException;->mErrcode:I

    .line 15
    return-void
.end method
