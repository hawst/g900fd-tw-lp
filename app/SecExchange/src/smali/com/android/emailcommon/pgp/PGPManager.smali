.class public Lcom/android/emailcommon/pgp/PGPManager;
.super Ljava/lang/Object;
.source "PGPManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

.field private mKeyManager:Lcom/android/emailcommon/pgp/PGPKeyManager;

.field private mLocalUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

.field private mOpActive:Z

.field private mSyncOp:Z

.field private mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1, "ctxt"    # Landroid/content/Context;
    .param p2, "isSyncOp"    # Z

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPManager;->mSyncOp:Z

    .line 36
    iput-object p1, p0, Lcom/android/emailcommon/pgp/PGPManager;->mContext:Landroid/content/Context;

    .line 37
    iput-boolean p2, p0, Lcom/android/emailcommon/pgp/PGPManager;->mSyncOp:Z

    .line 38
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPManager;->init()V

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/android/emailcommon/pgp/PGPManager;)Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;
    .locals 1
    .param p0, "x0"    # Lcom/android/emailcommon/pgp/PGPManager;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/android/emailcommon/pgp/PGPManager;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/emailcommon/pgp/PGPManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/emailcommon/pgp/PGPManager;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/android/emailcommon/pgp/PGPManager;->mOpActive:Z

    return p1
.end method


# virtual methods
.method public encryptSign(Lcom/android/emailcommon/pgp/EncryptSignParams;Z)I
    .locals 7
    .param p1, "params"    # Lcom/android/emailcommon/pgp/EncryptSignParams;
    .param p2, "encryptOnly"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchProviderException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/SignatureException;,
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Lcom/android/emailcommon/pgp/PGPKeyException;
        }
    .end annotation

    .prologue
    .line 126
    iget-object v1, p0, Lcom/android/emailcommon/pgp/PGPManager;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    if-nez v1, :cond_0

    .line 127
    const/4 v6, 0x7

    .line 145
    :goto_0
    return v6

    .line 129
    :cond_0
    if-nez p1, :cond_1

    .line 130
    const/4 v6, 0x4

    goto :goto_0

    .line 132
    :cond_1
    iget-boolean v1, p0, Lcom/android/emailcommon/pgp/PGPManager;->mOpActive:Z

    if-eqz v1, :cond_2

    .line 133
    const/4 v6, 0x6

    goto :goto_0

    .line 135
    :cond_2
    const/4 v6, 0x2

    .line 136
    .local v6, "ret":I
    new-instance v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/emailcommon/pgp/PGPManager;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPManager;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/emailcommon/pgp/PGPEncryptSign;-><init>(Lcom/android/emailcommon/pgp/EncryptSignParams;ZZLandroid/content/Context;Lcom/android/emailcommon/pgp/PGPDatabase;)V

    .line 137
    .local v0, "encOpr":Lcom/android/emailcommon/pgp/PGPEncryptSign;
    iget-object v1, p0, Lcom/android/emailcommon/pgp/PGPManager;->mLocalUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/pgp/PGPEncryptSign;->setPGPUpdater(Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;)V

    .line 138
    iget-boolean v1, p0, Lcom/android/emailcommon/pgp/PGPManager;->mSyncOp:Z

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/pgp/PGPEncryptSign;->setSynOp(Z)V

    .line 139
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/emailcommon/pgp/PGPManager;->mOpActive:Z

    .line 140
    iget-boolean v1, p0, Lcom/android/emailcommon/pgp/PGPManager;->mSyncOp:Z

    if-eqz v1, :cond_3

    .line 141
    invoke-virtual {v0}, Lcom/android/emailcommon/pgp/PGPEncryptSign;->encryptAndSign()I

    move-result v6

    goto :goto_0

    .line 143
    :cond_3
    invoke-virtual {v0}, Lcom/android/emailcommon/pgp/PGPEncryptSign;->startOperation()I

    move-result v6

    goto :goto_0
.end method

.method public getPGPKeyManager()Lcom/android/emailcommon/pgp/PGPKeyManager;
    .locals 5

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/emailcommon/pgp/PGPManager;->mKeyManager:Lcom/android/emailcommon/pgp/PGPKeyManager;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lcom/android/emailcommon/pgp/PGPKeyManager;

    iget-object v1, p0, Lcom/android/emailcommon/pgp/PGPManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/emailcommon/pgp/PGPManager;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    iget-object v3, p0, Lcom/android/emailcommon/pgp/PGPManager;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    iget-boolean v4, p0, Lcom/android/emailcommon/pgp/PGPManager;->mSyncOp:Z

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/emailcommon/pgp/PGPKeyManager;-><init>(Landroid/content/Context;Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;Lcom/android/emailcommon/pgp/PGPDatabase;Z)V

    iput-object v0, p0, Lcom/android/emailcommon/pgp/PGPManager;->mKeyManager:Lcom/android/emailcommon/pgp/PGPKeyManager;

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/android/emailcommon/pgp/PGPManager;->mKeyManager:Lcom/android/emailcommon/pgp/PGPKeyManager;

    return-object v0
.end method

.method public final init()V
    .locals 2

    .prologue
    .line 53
    new-instance v0, Lcom/android/emailcommon/pgp/PGPManager$1;

    invoke-direct {v0, p0}, Lcom/android/emailcommon/pgp/PGPManager$1;-><init>(Lcom/android/emailcommon/pgp/PGPManager;)V

    iput-object v0, p0, Lcom/android/emailcommon/pgp/PGPManager;->mLocalUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    .line 65
    new-instance v0, Lcom/android/emailcommon/pgp/PGPDatabase;

    iget-object v1, p0, Lcom/android/emailcommon/pgp/PGPManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/emailcommon/pgp/PGPDatabase;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/emailcommon/pgp/PGPManager;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    .line 66
    return-void
.end method

.method public setPGPUpdater(Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;)V
    .locals 0
    .param p1, "updater"    # Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/android/emailcommon/pgp/PGPManager;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    .line 70
    return-void
.end method

.method public signOnly(Lcom/android/emailcommon/pgp/EncryptSignParams;Z)I
    .locals 7
    .param p1, "params"    # Lcom/android/emailcommon/pgp/EncryptSignParams;
    .param p2, "isDetached"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;,
            Ljava/security/NoSuchProviderException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Lcom/android/emailcommon/pgp/PGPKeyException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 175
    iget-object v1, p0, Lcom/android/emailcommon/pgp/PGPManager;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    if-nez v1, :cond_0

    .line 176
    const/4 v6, 0x7

    .line 195
    :goto_0
    return v6

    .line 178
    :cond_0
    if-nez p1, :cond_1

    .line 179
    const/4 v6, 0x4

    goto :goto_0

    .line 181
    :cond_1
    iget-boolean v1, p0, Lcom/android/emailcommon/pgp/PGPManager;->mOpActive:Z

    if-eqz v1, :cond_2

    .line 182
    const/4 v6, 0x6

    goto :goto_0

    .line 184
    :cond_2
    new-instance v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/android/emailcommon/pgp/PGPManager;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPManager;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/emailcommon/pgp/PGPEncryptSign;-><init>(Lcom/android/emailcommon/pgp/EncryptSignParams;ZZLandroid/content/Context;Lcom/android/emailcommon/pgp/PGPDatabase;)V

    .line 185
    .local v0, "encOpr":Lcom/android/emailcommon/pgp/PGPEncryptSign;
    iget-object v1, p0, Lcom/android/emailcommon/pgp/PGPManager;->mLocalUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/pgp/PGPEncryptSign;->setPGPUpdater(Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;)V

    .line 186
    iget-boolean v1, p0, Lcom/android/emailcommon/pgp/PGPManager;->mSyncOp:Z

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/pgp/PGPEncryptSign;->setSynOp(Z)V

    .line 187
    invoke-virtual {v0, p2}, Lcom/android/emailcommon/pgp/PGPEncryptSign;->setDetached(Z)V

    .line 188
    iput-boolean v3, p0, Lcom/android/emailcommon/pgp/PGPManager;->mOpActive:Z

    .line 190
    iget-boolean v1, p0, Lcom/android/emailcommon/pgp/PGPManager;->mSyncOp:Z

    if-eqz v1, :cond_3

    .line 191
    invoke-virtual {v0, p2}, Lcom/android/emailcommon/pgp/PGPEncryptSign;->sign(Z)I

    move-result v6

    .local v6, "ret":I
    goto :goto_0

    .line 193
    .end local v6    # "ret":I
    :cond_3
    invoke-virtual {v0}, Lcom/android/emailcommon/pgp/PGPEncryptSign;->startOperation()I

    move-result v6

    .restart local v6    # "ret":I
    goto :goto_0
.end method
