.class public Lcom/android/emailcommon/pgp/PGPDecryptVerify;
.super Lcom/android/emailcommon/pgp/PGPOperation;
.source "PGPDecryptVerify.java"


# instance fields
.field private mParams:Lcom/android/emailcommon/pgp/DecryptVerifyParams;

.field private mVerifySignOnly:Z

.field private mdatabase:Lcom/android/emailcommon/pgp/PGPDatabase;


# virtual methods
.method public decryptAndVerify()I
    .locals 56
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchProviderException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/SignatureException;,
            Lcom/android/emailcommon/pgp/PGPKeyException;
        }
    .end annotation

    .prologue
    .line 143
    new-instance v50, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct/range {v50 .. v50}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-static/range {v50 .. v50}, Ljava/security/Security;->addProvider(Ljava/security/Provider;)I

    .line 144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mParams:Lcom/android/emailcommon/pgp/DecryptVerifyParams;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Lcom/android/emailcommon/pgp/DecryptVerifyParams;->getInputStream()Ljava/io/InputStream;

    move-result-object v50

    invoke-static/range {v50 .. v50}, Lcom/android/sec/org/bouncycastle/openpgp/PGPUtil;->getDecoderStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v25

    .line 145
    .local v25, "in":Ljava/io/InputStream;
    new-instance v36, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;

    move-object/from16 v0, v36

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;-><init>(Ljava/io/InputStream;)V

    .line 146
    .local v36, "pgpF":Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;
    const/16 v18, 0x0

    .line 147
    .local v18, "enc":Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;
    const/16 v27, 0x0

    .line 148
    .local v27, "isCompressed":Z
    const/4 v6, 0x0

    .line 149
    .local v6, "isSigned":Z
    const/4 v5, 0x0

    .line 150
    .local v5, "isEncrypted":Z
    const/16 v34, 0x0

    .line 151
    .local v34, "onePassSign":Z
    const/4 v10, 0x0

    .line 152
    .local v10, "isDetachedSignature":Z
    const/4 v8, 0x0

    .line 153
    .local v8, "isSignKeyExists":Z
    const/4 v9, 0x0

    .line 154
    .local v9, "isSecKeyExists":Z
    invoke-virtual/range {v36 .. v36}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->nextObject()Ljava/lang/Object;

    move-result-object v32

    .line 156
    .local v32, "obj":Ljava/lang/Object;
    move-object/from16 v0, v32

    instance-of v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;

    move/from16 v50, v0

    if-eqz v50, :cond_0

    .line 157
    const/4 v5, 0x1

    move-object/from16 v18, v32

    .line 158
    check-cast v18, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;

    .line 168
    :goto_0
    if-nez v18, :cond_4

    if-nez v27, :cond_4

    if-nez v6, :cond_4

    .line 172
    new-instance v50, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v51, "invalid content"

    invoke-direct/range {v50 .. v51}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v50

    .line 159
    :cond_0
    move-object/from16 v0, v32

    instance-of v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;

    move/from16 v50, v0

    if-eqz v50, :cond_1

    .line 160
    const/16 v27, 0x1

    goto :goto_0

    .line 161
    :cond_1
    move-object/from16 v0, v32

    instance-of v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;

    move/from16 v50, v0

    if-nez v50, :cond_2

    move-object/from16 v0, v32

    instance-of v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignatureList;

    move/from16 v50, v0

    if-eqz v50, :cond_3

    .line 163
    :cond_2
    const/4 v6, 0x1

    goto :goto_0

    .line 165
    :cond_3
    invoke-virtual/range {v36 .. v36}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->nextObject()Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "enc":Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;
    check-cast v18, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;

    .restart local v18    # "enc":Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;
    goto :goto_0

    .line 176
    :cond_4
    const/16 v42, 0x0

    .line 177
    .local v42, "sKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    const/16 v35, 0x0

    .line 178
    .local v35, "pbe":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;
    const/16 v38, 0x0

    .line 181
    .local v38, "pgpSecKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    if-nez v6, :cond_8

    .line 182
    invoke-virtual/range {v18 .. v18}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataList;->getEncryptedDataObjects()Ljava/util/Iterator;

    move-result-object v28

    .line 184
    .local v28, "it":Ljava/util/Iterator;
    :cond_5
    if-nez v42, :cond_7

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v50

    if-eqz v50, :cond_7

    .line 185
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    .end local v35    # "pbe":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;
    check-cast v35, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;

    .line 186
    .restart local v35    # "pbe":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;
    invoke-virtual/range {v35 .. v35}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->getKeyID()J

    move-result-wide v50

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mParams:Lcom/android/emailcommon/pgp/DecryptVerifyParams;

    move-object/from16 v52, v0

    invoke-virtual/range {v52 .. v52}, Lcom/android/emailcommon/pgp/DecryptVerifyParams;->getSecretKeyId()J

    move-result-wide v52

    cmp-long v50, v50, v52

    if-nez v50, :cond_5

    .line 187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mdatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    move-object/from16 v50, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mParams:Lcom/android/emailcommon/pgp/DecryptVerifyParams;

    move-object/from16 v51, v0

    invoke-virtual/range {v51 .. v51}, Lcom/android/emailcommon/pgp/DecryptVerifyParams;->getSecretKeyId()J

    move-result-wide v52

    move-object/from16 v0, v50

    move-wide/from16 v1, v52

    invoke-virtual {v0, v1, v2}, Lcom/android/emailcommon/pgp/PGPDatabase;->getPrivateKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-result-object v38

    .line 189
    if-nez v38, :cond_6

    .line 194
    new-instance v50, Lcom/android/emailcommon/pgp/PGPKeyException;

    const-string v51, "secret key for content not found"

    invoke-direct/range {v50 .. v51}, Lcom/android/emailcommon/pgp/PGPKeyException;-><init>(Ljava/lang/String;)V

    throw v50

    .line 198
    :cond_6
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mParams:Lcom/android/emailcommon/pgp/DecryptVerifyParams;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Lcom/android/emailcommon/pgp/DecryptVerifyParams;->getPassphrase()Ljava/lang/String;

    move-result-object v50

    invoke-virtual/range {v50 .. v50}, Ljava/lang/String;->toCharArray()[C

    move-result-object v50

    new-instance v51, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct/range {v51 .. v51}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    move-object/from16 v0, v38

    move-object/from16 v1, v50

    move-object/from16 v2, v51

    invoke-virtual {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->extractPrivateKey([CLjava/security/Provider;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    :try_end_0
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v42

    .line 210
    :cond_7
    if-nez v42, :cond_8

    .line 216
    new-instance v50, Lcom/android/emailcommon/pgp/PGPKeyException;

    const-string v51, "secret key for content not found, password is wrong"

    invoke-direct/range {v50 .. v51}, Lcom/android/emailcommon/pgp/PGPKeyException;-><init>(Ljava/lang/String;)V

    throw v50

    .line 201
    :catch_0
    move-exception v17

    .line 202
    .local v17, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    new-instance v19, Lcom/android/emailcommon/pgp/PGPKeyException;

    invoke-virtual/range {v17 .. v17}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;->getMessage()Ljava/lang/String;

    move-result-object v50

    move-object/from16 v0, v19

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/android/emailcommon/pgp/PGPKeyException;-><init>(Ljava/lang/String;)V

    .line 203
    .local v19, "ex":Lcom/android/emailcommon/pgp/PGPKeyException;
    const/16 v50, 0x1d

    move-object/from16 v0, v19

    move/from16 v1, v50

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/pgp/PGPKeyException;->setErrorcode(I)V

    .line 204
    throw v19

    .line 222
    .end local v17    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    .end local v19    # "ex":Lcom/android/emailcommon/pgp/PGPKeyException;
    .end local v28    # "it":Ljava/util/Iterator;
    :cond_8
    const/16 v44, 0x0

    .line 223
    .local v44, "signature":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    const/16 v46, 0x0

    .line 224
    .local v46, "signatureKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    const/16 v33, 0x0

    .line 225
    .local v33, "onePassSig":Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;
    const/16 v45, -0x1

    .line 226
    .local v45, "signatureIndex":I
    const/4 v14, 0x0

    .line 227
    .local v14, "cData":Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;
    const/16 v30, 0x0

    .line 228
    .local v30, "message":Ljava/lang/Object;
    const/16 v16, 0x0

    .line 229
    .local v16, "comprsdStream":Ljava/io/InputStream;
    const/16 v37, 0x0

    .line 230
    .local v37, "pgpFact":Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;
    const/16 v39, 0x0

    .line 231
    .local v39, "plainFact":Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;
    const/4 v7, 0x0

    .line 232
    .local v7, "isSignVerified":Z
    const/4 v11, 0x1

    .line 233
    .local v11, "isInegCheckPassed":Z
    const/4 v12, 0x0

    .line 235
    .local v12, "outFileName":Ljava/lang/String;
    if-nez v6, :cond_a

    .line 236
    new-instance v50, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct/range {v50 .. v50}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    move-object/from16 v0, v35

    move-object/from16 v1, v42

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->getDataStream(Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;Ljava/security/Provider;)Ljava/io/InputStream;

    move-result-object v15

    .line 237
    .local v15, "clear":Ljava/io/InputStream;
    new-instance v39, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;

    .end local v39    # "plainFact":Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;
    move-object/from16 v0, v39

    invoke-direct {v0, v15}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;-><init>(Ljava/io/InputStream;)V

    .line 238
    .restart local v39    # "plainFact":Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;
    invoke-virtual/range {v39 .. v39}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->nextObject()Ljava/lang/Object;

    move-result-object v30

    .line 239
    move-object/from16 v0, v30

    instance-of v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;

    move/from16 v50, v0

    if-eqz v50, :cond_9

    move-object/from16 v14, v30

    .line 240
    check-cast v14, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;

    .line 241
    const/16 v27, 0x1

    .line 256
    .end local v15    # "clear":Ljava/io/InputStream;
    .end local v30    # "message":Ljava/lang/Object;
    :goto_1
    if-eqz v27, :cond_2e

    if-eqz v14, :cond_2e

    .line 257
    new-instance v16, Ljava/io/BufferedInputStream;

    .end local v16    # "comprsdStream":Ljava/io/InputStream;
    invoke-virtual {v14}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;->getDataStream()Ljava/io/InputStream;

    move-result-object v50

    move-object/from16 v0, v16

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 258
    .restart local v16    # "comprsdStream":Ljava/io/InputStream;
    new-instance v37, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;

    .end local v37    # "pgpFact":Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;
    move-object/from16 v0, v37

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;-><init>(Ljava/io/InputStream;)V

    .line 259
    .restart local v37    # "pgpFact":Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;
    invoke-virtual/range {v37 .. v37}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->nextObject()Ljava/lang/Object;

    move-result-object v30

    .restart local v30    # "message":Ljava/lang/Object;
    move-object/from16 v43, v30

    .line 263
    .end local v30    # "message":Ljava/lang/Object;
    :goto_2
    move-object/from16 v0, v43

    instance-of v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;

    move/from16 v50, v0

    if-eqz v50, :cond_12

    .line 264
    const/4 v6, 0x1

    .line 265
    check-cast v43, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;

    .line 266
    .local v43, "sigList":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;
    invoke-virtual/range {v43 .. v43}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;->size()I

    move-result v48

    .line 267
    .local v48, "size":I
    const/16 v24, 0x0

    .local v24, "i":I
    :goto_3
    move/from16 v0, v24

    move/from16 v1, v48

    if-ge v0, v1, :cond_d

    .line 268
    move-object/from16 v0, v43

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;->get(I)Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    move-result-object v44

    .line 271
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mdatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    move-object/from16 v50, v0

    invoke-virtual/range {v44 .. v44}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->getKeyID()J

    move-result-wide v52

    move-object/from16 v0, v50

    move-wide/from16 v1, v52

    invoke-virtual {v0, v1, v2}, Lcom/android/emailcommon/pgp/PGPDatabase;->getValidPublicKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v46

    .line 272
    if-nez v46, :cond_c

    .line 273
    const/16 v44, 0x0

    .line 267
    add-int/lit8 v24, v24, 0x1

    goto :goto_3

    .line 243
    .end local v24    # "i":I
    .end local v43    # "sigList":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;
    .end local v48    # "size":I
    .restart local v15    # "clear":Ljava/io/InputStream;
    .restart local v30    # "message":Ljava/lang/Object;
    :cond_9
    move-object/from16 v37, v39

    goto :goto_1

    .line 247
    .end local v15    # "clear":Ljava/io/InputStream;
    :cond_a
    if-eqz v27, :cond_b

    move-object/from16 v14, v32

    .line 248
    check-cast v14, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedData;

    goto :goto_1

    .line 250
    :cond_b
    move-object/from16 v37, v36

    .line 251
    move-object/from16 v30, v32

    goto :goto_1

    .line 275
    .end local v30    # "message":Ljava/lang/Object;
    .restart local v24    # "i":I
    .restart local v43    # "sigList":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;
    .restart local v48    # "size":I
    :cond_c
    move/from16 v45, v24

    .line 282
    :cond_d
    if-eqz v44, :cond_11

    .line 283
    const/4 v8, 0x1

    .line 284
    new-instance v50, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct/range {v50 .. v50}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    move-object/from16 v0, v44

    move-object/from16 v1, v46

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->initVerify(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/security/Provider;)V

    .line 287
    invoke-virtual/range {v44 .. v44}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->verify()Z

    move-result v50

    if-eqz v50, :cond_e

    .line 289
    const/4 v7, 0x1

    .line 297
    :cond_e
    :goto_4
    invoke-virtual/range {v37 .. v37}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->nextObject()Ljava/lang/Object;

    move-result-object v30

    .restart local v30    # "message":Ljava/lang/Object;
    move-object/from16 v29, v30

    .line 325
    .end local v24    # "i":I
    .end local v30    # "message":Ljava/lang/Object;
    .end local v43    # "sigList":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;
    .end local v48    # "size":I
    :goto_5
    if-eqz v29, :cond_1e

    move-object/from16 v0, v29

    instance-of v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;

    move/from16 v50, v0

    if-eqz v50, :cond_1e

    .line 326
    const/4 v5, 0x1

    .line 327
    check-cast v29, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;

    .line 328
    .local v29, "ld":Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;
    invoke-virtual/range {v29 .. v29}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;->getFileName()Ljava/lang/String;

    move-result-object v12

    .line 329
    invoke-virtual/range {v29 .. v29}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;->getInputStream()Ljava/io/InputStream;

    move-result-object v49

    .line 330
    .local v49, "unc":Ljava/io/InputStream;
    new-instance v20, Ljava/io/BufferedOutputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mParams:Lcom/android/emailcommon/pgp/DecryptVerifyParams;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Lcom/android/emailcommon/pgp/DecryptVerifyParams;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v50

    move-object/from16 v0, v20

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 332
    .local v20, "fOut":Ljava/io/OutputStream;
    const/high16 v50, 0x10000

    move/from16 v0, v50

    new-array v13, v0, [B

    .line 334
    .local v13, "bs":[B
    const-wide/16 v40, 0x0

    .line 335
    .local v40, "progress":D
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mParams:Lcom/android/emailcommon/pgp/DecryptVerifyParams;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Lcom/android/emailcommon/pgp/DecryptVerifyParams;->getInputDataSize()J

    move-result-wide v22

    .line 337
    .local v22, "fileSize":J
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mSyncOp:Z

    move/from16 v50, v0

    if-eqz v50, :cond_17

    .line 338
    :cond_f
    :goto_6
    const/16 v50, 0x0

    array-length v0, v13

    move/from16 v51, v0

    move-object/from16 v0, v49

    move/from16 v1, v50

    move/from16 v2, v51

    invoke-virtual {v0, v13, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v31

    .local v31, "numRead":I
    if-ltz v31, :cond_1a

    .line 339
    const/16 v50, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v50

    move/from16 v2, v31

    invoke-virtual {v0, v13, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 341
    if-eqz v34, :cond_16

    .line 342
    if-eqz v33, :cond_10

    .line 343
    const/16 v50, 0x0

    move-object/from16 v0, v33

    move/from16 v1, v50

    move/from16 v2, v31

    invoke-virtual {v0, v13, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->update([BII)V

    .line 355
    :cond_10
    :goto_7
    const-wide/16 v50, 0x0

    cmp-long v50, v22, v50

    if-lez v50, :cond_f

    .line 356
    move/from16 v0, v31

    int-to-double v0, v0

    move-wide/from16 v50, v0

    add-double v40, v40, v50

    .line 357
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    move-object/from16 v50, v0

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v52, v0

    div-double v52, v40, v52

    const-wide/high16 v54, 0x4059000000000000L    # 100.0

    mul-double v52, v52, v54

    move-wide/from16 v0, v52

    double-to-int v0, v0

    move/from16 v51, v0

    invoke-interface/range {v50 .. v51}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onProgressUpdate(I)V

    goto :goto_6

    .line 294
    .end local v13    # "bs":[B
    .end local v20    # "fOut":Ljava/io/OutputStream;
    .end local v22    # "fileSize":J
    .end local v29    # "ld":Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;
    .end local v31    # "numRead":I
    .end local v40    # "progress":D
    .end local v49    # "unc":Ljava/io/InputStream;
    .restart local v24    # "i":I
    .restart local v43    # "sigList":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;
    .restart local v48    # "size":I
    :cond_11
    const/4 v8, 0x0

    goto/16 :goto_4

    .line 298
    .end local v24    # "i":I
    .end local v43    # "sigList":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;
    .end local v48    # "size":I
    :cond_12
    move-object/from16 v0, v43

    instance-of v0, v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignatureList;

    move/from16 v50, v0

    if-eqz v50, :cond_2d

    .line 299
    const/4 v6, 0x1

    .line 300
    const/16 v34, 0x1

    .line 301
    check-cast v43, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignatureList;

    .line 302
    .local v43, "sigList":Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignatureList;
    invoke-virtual/range {v43 .. v43}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignatureList;->size()I

    move-result v48

    .line 303
    .restart local v48    # "size":I
    const/16 v24, 0x0

    .restart local v24    # "i":I
    :goto_8
    move/from16 v0, v24

    move/from16 v1, v48

    if-ge v0, v1, :cond_14

    .line 304
    move-object/from16 v0, v43

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignatureList;->get(I)Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;

    move-result-object v33

    .line 305
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mdatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    move-object/from16 v50, v0

    invoke-virtual/range {v33 .. v33}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->getKeyID()J

    move-result-wide v52

    move-object/from16 v0, v50

    move-wide/from16 v1, v52

    invoke-virtual {v0, v1, v2}, Lcom/android/emailcommon/pgp/PGPDatabase;->getValidPublicKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v46

    .line 307
    if-nez v46, :cond_13

    .line 308
    const/16 v33, 0x0

    .line 303
    add-int/lit8 v24, v24, 0x1

    goto :goto_8

    .line 310
    :cond_13
    move/from16 v45, v24

    .line 315
    :cond_14
    if-eqz v33, :cond_15

    .line 316
    const/4 v8, 0x1

    .line 317
    new-instance v50, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct/range {v50 .. v50}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    move-object/from16 v0, v33

    move-object/from16 v1, v46

    move-object/from16 v2, v50

    invoke-virtual {v0, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->initVerify(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Ljava/security/Provider;)V

    .line 322
    :goto_9
    invoke-virtual/range {v37 .. v37}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->nextObject()Ljava/lang/Object;

    move-result-object v30

    .restart local v30    # "message":Ljava/lang/Object;
    move-object/from16 v29, v30

    goto/16 :goto_5

    .line 319
    .end local v30    # "message":Ljava/lang/Object;
    :cond_15
    const/4 v8, 0x0

    goto :goto_9

    .line 346
    .end local v24    # "i":I
    .end local v43    # "sigList":Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignatureList;
    .end local v48    # "size":I
    .restart local v13    # "bs":[B
    .restart local v20    # "fOut":Ljava/io/OutputStream;
    .restart local v22    # "fileSize":J
    .restart local v29    # "ld":Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;
    .restart local v31    # "numRead":I
    .restart local v40    # "progress":D
    .restart local v49    # "unc":Ljava/io/InputStream;
    :cond_16
    if-eqz v44, :cond_10

    .line 348
    const/16 v50, 0x0

    :try_start_1
    move-object/from16 v0, v44

    move/from16 v1, v50

    move/from16 v2, v31

    invoke-virtual {v0, v13, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update([BII)V
    :try_end_1
    .catch Ljava/security/SignatureException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_7

    .line 349
    :catch_1
    move-exception v17

    .line 350
    .local v17, "e":Ljava/security/SignatureException;
    const/16 v44, 0x0

    goto/16 :goto_7

    .line 363
    .end local v17    # "e":Ljava/security/SignatureException;
    .end local v31    # "numRead":I
    :cond_17
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mThread:Lcom/android/emailcommon/pgp/PGPThread;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Lcom/android/emailcommon/pgp/PGPThread;->isCancel()Z

    move-result v50

    if-nez v50, :cond_1a

    const/16 v50, 0x0

    array-length v0, v13

    move/from16 v51, v0

    move-object/from16 v0, v49

    move/from16 v1, v50

    move/from16 v2, v51

    invoke-virtual {v0, v13, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v31

    .restart local v31    # "numRead":I
    if-ltz v31, :cond_1a

    .line 364
    const/16 v50, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v50

    move/from16 v2, v31

    invoke-virtual {v0, v13, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    .line 365
    move/from16 v0, v31

    int-to-double v0, v0

    move-wide/from16 v50, v0

    add-double v40, v40, v50

    .line 367
    if-eqz v34, :cond_19

    .line 368
    if-eqz v33, :cond_18

    .line 369
    const/16 v50, 0x0

    move-object/from16 v0, v33

    move/from16 v1, v50

    move/from16 v2, v31

    invoke-virtual {v0, v13, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->update([BII)V

    .line 382
    :cond_18
    :goto_b
    const-wide/16 v50, 0x0

    cmp-long v50, v22, v50

    if-lez v50, :cond_17

    .line 383
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    move-object/from16 v50, v0

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v52, v0

    div-double v52, v40, v52

    const-wide/high16 v54, 0x4059000000000000L    # 100.0

    mul-double v52, v52, v54

    move-wide/from16 v0, v52

    double-to-int v0, v0

    move/from16 v51, v0

    invoke-interface/range {v50 .. v51}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onProgressUpdate(I)V

    goto :goto_a

    .line 373
    :cond_19
    if-eqz v44, :cond_18

    .line 375
    const/16 v50, 0x0

    :try_start_2
    move-object/from16 v0, v44

    move/from16 v1, v50

    move/from16 v2, v31

    invoke-virtual {v0, v13, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update([BII)V
    :try_end_2
    .catch Ljava/security/SignatureException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_b

    .line 376
    :catch_2
    move-exception v17

    .line 377
    .restart local v17    # "e":Ljava/security/SignatureException;
    const/16 v44, 0x0

    goto :goto_b

    .line 388
    .end local v17    # "e":Ljava/security/SignatureException;
    .end local v31    # "numRead":I
    :cond_1a
    if-eqz v33, :cond_1b

    .line 389
    invoke-virtual/range {v37 .. v37}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->nextObject()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;

    .line 390
    .local v21, "finalList":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;
    move-object/from16 v0, v21

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;->get(I)Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    move-result-object v47

    .line 393
    .local v47, "signatureV":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    move-object/from16 v0, v33

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->verify(Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Z

    move-result v50

    if-eqz v50, :cond_1b

    .line 396
    const/4 v7, 0x1

    .line 400
    .end local v21    # "finalList":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;
    .end local v47    # "signatureV":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    :cond_1b
    invoke-virtual/range {v20 .. v20}, Ljava/io/OutputStream;->close()V

    .line 486
    .end local v20    # "fOut":Ljava/io/OutputStream;
    .end local v29    # "ld":Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralData;
    .end local v49    # "unc":Ljava/io/InputStream;
    :cond_1c
    :goto_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mSyncOp:Z

    move/from16 v50, v0

    if-eqz v50, :cond_29

    .line 487
    if-eqz v35, :cond_1d

    .line 488
    invoke-virtual/range {v35 .. v35}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->isIntegrityProtected()Z

    move-result v50

    if-eqz v50, :cond_28

    .line 489
    invoke-virtual/range {v35 .. v35}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->verify()Z

    move-result v50

    if-nez v50, :cond_1d

    .line 490
    const/4 v11, 0x0

    .line 497
    :cond_1d
    :goto_d
    new-instance v4, Lcom/android/emailcommon/pgp/PGPOperationResultData;

    invoke-direct/range {v4 .. v12}, Lcom/android/emailcommon/pgp/PGPOperationResultData;-><init>(ZZZZZZZLjava/lang/String;)V

    .line 500
    .local v4, "res":Lcom/android/emailcommon/pgp/PGPOperationResultData;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    move-object/from16 v50, v0

    const/16 v51, 0x1

    const/16 v52, 0x0

    move-object/from16 v0, v50

    move/from16 v1, v51

    move-object/from16 v2, v52

    invoke-interface {v0, v1, v2, v4}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    .line 523
    .end local v4    # "res":Lcom/android/emailcommon/pgp/PGPOperationResultData;
    :goto_e
    const/16 v50, 0x2

    return v50

    .line 402
    .end local v13    # "bs":[B
    .end local v22    # "fileSize":J
    .end local v40    # "progress":D
    :cond_1e
    if-nez v29, :cond_27

    if-eqz v6, :cond_27

    .line 404
    const/4 v10, 0x1

    .line 405
    const/4 v6, 0x0

    .line 406
    const/high16 v50, 0x10000

    move/from16 v0, v50

    new-array v13, v0, [B

    .line 407
    .restart local v13    # "bs":[B
    const/16 v31, 0x0

    .line 408
    .restart local v31    # "numRead":I
    const-wide/16 v40, 0x0

    .line 409
    .restart local v40    # "progress":D
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mParams:Lcom/android/emailcommon/pgp/DecryptVerifyParams;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Lcom/android/emailcommon/pgp/DecryptVerifyParams;->getInputDataSize()J

    move-result-wide v22

    .line 410
    .restart local v22    # "fileSize":J
    new-instance v26, Ljava/io/BufferedInputStream;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mParams:Lcom/android/emailcommon/pgp/DecryptVerifyParams;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Lcom/android/emailcommon/pgp/DecryptVerifyParams;->getDetSignInStream()Ljava/io/InputStream;

    move-result-object v50

    move-object/from16 v0, v26

    move-object/from16 v1, v50

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 417
    .local v26, "inSig":Ljava/io/InputStream;
    if-eqz v26, :cond_1c

    .line 418
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mSyncOp:Z

    move/from16 v50, v0

    if-eqz v50, :cond_22

    .line 419
    :cond_1f
    :goto_f
    const/16 v50, 0x0

    array-length v0, v13

    move/from16 v51, v0

    move-object/from16 v0, v26

    move/from16 v1, v50

    move/from16 v2, v51

    invoke-virtual {v0, v13, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v31

    if-ltz v31, :cond_25

    .line 420
    move/from16 v0, v31

    int-to-double v0, v0

    move-wide/from16 v50, v0

    add-double v40, v40, v50

    .line 421
    if-eqz v34, :cond_21

    .line 422
    if-eqz v33, :cond_20

    .line 423
    const/16 v50, 0x0

    move-object/from16 v0, v33

    move/from16 v1, v50

    move/from16 v2, v31

    invoke-virtual {v0, v13, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->update([BII)V

    .line 435
    :cond_20
    :goto_10
    const-wide/16 v50, 0x0

    cmp-long v50, v22, v50

    if-lez v50, :cond_1f

    .line 436
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    move-object/from16 v50, v0

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v52, v0

    div-double v52, v40, v52

    const-wide/high16 v54, 0x4059000000000000L    # 100.0

    mul-double v52, v52, v54

    move-wide/from16 v0, v52

    double-to-int v0, v0

    move/from16 v51, v0

    invoke-interface/range {v50 .. v51}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onProgressUpdate(I)V

    goto :goto_f

    .line 426
    :cond_21
    if-eqz v44, :cond_20

    .line 428
    const/16 v50, 0x0

    :try_start_3
    move-object/from16 v0, v44

    move/from16 v1, v50

    move/from16 v2, v31

    invoke-virtual {v0, v13, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update([BII)V
    :try_end_3
    .catch Ljava/security/SignatureException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_10

    .line 429
    :catch_3
    move-exception v17

    .line 430
    .restart local v17    # "e":Ljava/security/SignatureException;
    const/16 v44, 0x0

    goto :goto_10

    .line 441
    .end local v17    # "e":Ljava/security/SignatureException;
    :cond_22
    :goto_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mThread:Lcom/android/emailcommon/pgp/PGPThread;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Lcom/android/emailcommon/pgp/PGPThread;->isCancel()Z

    move-result v50

    if-nez v50, :cond_25

    const/16 v50, 0x0

    array-length v0, v13

    move/from16 v51, v0

    move-object/from16 v0, v26

    move/from16 v1, v50

    move/from16 v2, v51

    invoke-virtual {v0, v13, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v31

    if-ltz v31, :cond_25

    .line 442
    move/from16 v0, v31

    int-to-double v0, v0

    move-wide/from16 v50, v0

    add-double v40, v40, v50

    .line 444
    if-eqz v34, :cond_24

    .line 445
    if-eqz v33, :cond_23

    .line 446
    const/16 v50, 0x0

    move-object/from16 v0, v33

    move/from16 v1, v50

    move/from16 v2, v31

    invoke-virtual {v0, v13, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->update([BII)V

    .line 459
    :cond_23
    :goto_12
    const-wide/16 v50, 0x0

    cmp-long v50, v22, v50

    if-lez v50, :cond_22

    .line 460
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    move-object/from16 v50, v0

    move-wide/from16 v0, v22

    long-to-double v0, v0

    move-wide/from16 v52, v0

    div-double v52, v40, v52

    const-wide/high16 v54, 0x4059000000000000L    # 100.0

    mul-double v52, v52, v54

    move-wide/from16 v0, v52

    double-to-int v0, v0

    move/from16 v51, v0

    invoke-interface/range {v50 .. v51}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onProgressUpdate(I)V

    goto :goto_11

    .line 450
    :cond_24
    if-eqz v44, :cond_23

    .line 452
    const/16 v50, 0x0

    :try_start_4
    move-object/from16 v0, v44

    move/from16 v1, v50

    move/from16 v2, v31

    invoke-virtual {v0, v13, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->update([BII)V
    :try_end_4
    .catch Ljava/security/SignatureException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_12

    .line 453
    :catch_4
    move-exception v17

    .line 454
    .restart local v17    # "e":Ljava/security/SignatureException;
    const/16 v44, 0x0

    goto :goto_12

    .line 465
    .end local v17    # "e":Ljava/security/SignatureException;
    :cond_25
    if-eqz v33, :cond_26

    .line 466
    invoke-virtual/range {v37 .. v37}, Lcom/android/sec/org/bouncycastle/openpgp/PGPObjectFactory;->nextObject()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;

    .line 467
    .restart local v21    # "finalList":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;
    move-object/from16 v0, v21

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;->get(I)Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    move-result-object v47

    .line 470
    .restart local v47    # "signatureV":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    move-object/from16 v0, v33

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->verify(Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;)Z

    move-result v50

    if-eqz v50, :cond_1c

    .line 472
    const/4 v7, 0x1

    goto/16 :goto_c

    .line 475
    .end local v21    # "finalList":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureList;
    .end local v47    # "signatureV":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    :cond_26
    if-eqz v44, :cond_1c

    .line 476
    invoke-virtual/range {v44 .. v44}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->verify()Z

    move-result v50

    if-eqz v50, :cond_1c

    .line 477
    const/4 v7, 0x1

    goto/16 :goto_c

    .line 483
    .end local v13    # "bs":[B
    .end local v22    # "fileSize":J
    .end local v26    # "inSig":Ljava/io/InputStream;
    .end local v31    # "numRead":I
    .end local v40    # "progress":D
    :cond_27
    new-instance v50, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v51, "content is not encrypted"

    invoke-direct/range {v50 .. v51}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v50

    .line 493
    .restart local v13    # "bs":[B
    .restart local v22    # "fileSize":J
    .restart local v40    # "progress":D
    :cond_28
    const/4 v11, 0x0

    goto/16 :goto_d

    .line 503
    :cond_29
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mThread:Lcom/android/emailcommon/pgp/PGPThread;

    move-object/from16 v50, v0

    invoke-virtual/range {v50 .. v50}, Lcom/android/emailcommon/pgp/PGPThread;->isCancel()Z

    move-result v50

    if-eqz v50, :cond_2a

    .line 504
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    move-object/from16 v50, v0

    const/16 v51, 0xd

    const/16 v52, 0x0

    const/16 v53, 0x0

    invoke-interface/range {v50 .. v53}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    goto/16 :goto_e

    .line 506
    :cond_2a
    if-eqz v35, :cond_2b

    .line 507
    invoke-virtual/range {v35 .. v35}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->isIntegrityProtected()Z

    move-result v50

    if-eqz v50, :cond_2c

    .line 508
    invoke-virtual/range {v35 .. v35}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyEncryptedData;->verify()Z

    move-result v50

    if-nez v50, :cond_2b

    .line 509
    const/4 v11, 0x0

    .line 516
    :cond_2b
    :goto_13
    new-instance v4, Lcom/android/emailcommon/pgp/PGPOperationResultData;

    invoke-direct/range {v4 .. v12}, Lcom/android/emailcommon/pgp/PGPOperationResultData;-><init>(ZZZZZZZLjava/lang/String;)V

    .line 519
    .restart local v4    # "res":Lcom/android/emailcommon/pgp/PGPOperationResultData;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    move-object/from16 v50, v0

    const/16 v51, 0x1

    const/16 v52, 0x0

    move-object/from16 v0, v50

    move/from16 v1, v51

    move-object/from16 v2, v52

    invoke-interface {v0, v1, v2, v4}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    goto/16 :goto_e

    .line 512
    .end local v4    # "res":Lcom/android/emailcommon/pgp/PGPOperationResultData;
    :cond_2c
    const/4 v11, 0x0

    goto :goto_13

    .end local v13    # "bs":[B
    .end local v22    # "fileSize":J
    .end local v40    # "progress":D
    :cond_2d
    move-object/from16 v29, v43

    goto/16 :goto_5

    :cond_2e
    move-object/from16 v43, v30

    goto/16 :goto_2
.end method

.method public run()V
    .locals 5

    .prologue
    .line 52
    const/4 v1, 0x0

    .line 55
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_0
    iget-boolean v2, p0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mVerifySignOnly:Z

    if-eqz v2, :cond_1

    .line 56
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->verifySign()V
    :try_end_0
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/android/emailcommon/pgp/PGPKeyException; {:try_start_0 .. :try_end_0} :catch_5

    .line 80
    :goto_0
    if-eqz v1, :cond_0

    .line 81
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 82
    iget-object v2, p0, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v1, v4}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    .line 84
    :cond_0
    return-void

    .line 58
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPDecryptVerify;->decryptAndVerify()I
    :try_end_1
    .catch Ljava/security/NoSuchProviderException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/security/SignatureException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lcom/android/emailcommon/pgp/PGPKeyException; {:try_start_1 .. :try_end_1} :catch_5

    goto :goto_0

    .line 60
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/security/NoSuchProviderException;
    invoke-virtual {v0}, Ljava/security/NoSuchProviderException;->printStackTrace()V

    .line 62
    move-object v1, v0

    .line 78
    goto :goto_0

    .line 63
    .end local v0    # "e":Ljava/security/NoSuchProviderException;
    :catch_1
    move-exception v0

    .line 64
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 65
    move-object v1, v0

    .line 78
    goto :goto_0

    .line 66
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 67
    .local v0, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;->printStackTrace()V

    .line 68
    move-object v1, v0

    .line 78
    goto :goto_0

    .line 69
    .end local v0    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    :catch_3
    move-exception v0

    .line 70
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 71
    move-object v1, v0

    .line 78
    goto :goto_0

    .line 72
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_4
    move-exception v0

    .line 73
    .local v0, "e":Ljava/security/SignatureException;
    move-object v1, v0

    .line 74
    invoke-virtual {v0}, Ljava/security/SignatureException;->printStackTrace()V

    goto :goto_0

    .line 75
    .end local v0    # "e":Ljava/security/SignatureException;
    :catch_5
    move-exception v0

    .line 76
    .local v0, "e":Lcom/android/emailcommon/pgp/PGPKeyException;
    move-object v1, v0

    .line 77
    invoke-virtual {v0}, Lcom/android/emailcommon/pgp/PGPKeyException;->printStackTrace()V

    goto :goto_0
.end method

.method public verifySign()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/SignatureException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Lcom/android/emailcommon/pgp/PGPKeyException;
        }
    .end annotation

    .prologue
    .line 571
    return-void
.end method
