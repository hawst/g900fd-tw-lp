.class public Lcom/android/emailcommon/pgp/PGPExportKeys;
.super Lcom/android/emailcommon/pgp/PGPOperation;
.source "PGPExportKeys.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/pgp/PGPExportKeys$PGPExportkeyType;
    }
.end annotation


# instance fields
.field private mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

.field private mExportType:I

.field private mKeys:[J

.field private mOutstream:Ljava/io/OutputStream;


# direct methods
.method private getSecretKeyList([J)Ljava/util/List;
    .locals 6
    .param p1, "keys"    # [J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([J)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/pgp/PGPKeyException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 167
    .local v1, "seckeys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 168
    iget-object v3, p0, Lcom/android/emailcommon/pgp/PGPExportKeys;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    iget-object v4, p0, Lcom/android/emailcommon/pgp/PGPExportKeys;->mKeys:[J

    aget-wide v4, v4, v0

    invoke-virtual {v3, v4, v5}, Lcom/android/emailcommon/pgp/PGPDatabase;->getPrivateKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-result-object v2

    .line 169
    .local v2, "secretkey":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 171
    .end local v2    # "secretkey":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    :cond_0
    return-object v1
.end method

.method private getpublicKeyList([J)Ljava/util/List;
    .locals 8
    .param p1, "keys"    # [J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([J)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/pgp/PGPKeyException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 153
    .local v2, "pubkeys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p1

    if-ge v1, v4, :cond_0

    .line 154
    const/4 v3, 0x0

    .line 156
    .local v3, "publickey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :try_start_0
    iget-object v4, p0, Lcom/android/emailcommon/pgp/PGPExportKeys;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPExportKeys;->mKeys:[J

    aget-wide v6, v5, v1

    invoke-virtual {v4, v6, v7}, Lcom/android/emailcommon/pgp/PGPDatabase;->getPublicKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :try_end_0
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 160
    :goto_1
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 157
    :catch_0
    move-exception v0

    .line 158
    .local v0, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;->printStackTrace()V

    goto :goto_1

    .line 162
    .end local v0    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    .end local v3    # "publickey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :cond_0
    return-object v2
.end method


# virtual methods
.method public exportKeys()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 43
    iget-object v12, p0, Lcom/android/emailcommon/pgp/PGPExportKeys;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    invoke-virtual {v12}, Lcom/android/emailcommon/pgp/PGPDatabase;->getKeyRings()Ljava/util/Vector;

    move-result-object v2

    .line 45
    .local v2, "keydata":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Object;>;"
    const/4 v6, 0x0

    .line 46
    .local v6, "pubkeyring":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    const/4 v10, 0x0

    .line 48
    .local v10, "seckeyring":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    const/4 v0, 0x0

    .line 49
    .local v0, "ex":Ljava/lang/Exception;
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 51
    .local v8, "returnData":Landroid/os/Bundle;
    const/4 v9, 0x2

    .line 52
    .local v9, "returnvalue":I
    const/4 v3, 0x0

    .line 54
    .local v3, "numKeys":I
    const/4 v5, 0x0

    .line 55
    .local v5, "outstream":Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;
    new-instance v5, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;

    .end local v5    # "outstream":Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;
    iget-object v12, p0, Lcom/android/emailcommon/pgp/PGPExportKeys;->mOutstream:Ljava/io/OutputStream;

    invoke-direct {v5, v12}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 57
    .restart local v5    # "outstream":Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;
    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v11

    .line 58
    .local v11, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v11, :cond_2

    .line 59
    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 60
    .local v4, "obj":Ljava/lang/Object;
    instance-of v12, v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

    if-eqz v12, :cond_1

    move-object v6, v4

    .line 61
    check-cast v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

    .line 62
    invoke-virtual {v6, v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->encode(Ljava/io/OutputStream;)V

    .line 71
    :goto_1
    add-int/lit8 v3, v3, 0x1

    .line 58
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 64
    :cond_1
    instance-of v12, v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    if-eqz v12, :cond_0

    move-object v10, v4

    .line 65
    check-cast v10, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    .line 66
    invoke-virtual {v10, v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->encode(Ljava/io/OutputStream;)V

    goto :goto_1

    .line 73
    .end local v4    # "obj":Ljava/lang/Object;
    :cond_2
    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->close()V

    .line 75
    const-string v12, "keycount"

    invoke-virtual {v8, v12, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 76
    const-string v12, "errorvalue"

    invoke-virtual {v8, v12, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 77
    const-string v12, "Status"

    const/4 v13, 0x1

    invoke-virtual {v8, v12, v13}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 79
    new-instance v7, Lcom/android/emailcommon/pgp/PGPOperationResultData;

    invoke-direct {v7}, Lcom/android/emailcommon/pgp/PGPOperationResultData;-><init>()V

    .line 81
    .local v7, "res":Lcom/android/emailcommon/pgp/PGPOperationResultData;
    const/16 v12, 0x8

    iput v12, v7, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mPgpOperation:I

    .line 82
    iput-object v8, v7, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mMsg:Landroid/os/Bundle;

    .line 84
    iget-object v12, p0, Lcom/android/emailcommon/pgp/PGPExportKeys;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    invoke-interface {v12, v9, v0, v7}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    .line 85
    return-void
.end method

.method public exportSelectedKeys()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/pgp/PGPKeyException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 115
    const/4 v0, 0x0

    .line 116
    .local v0, "ex":Ljava/lang/Exception;
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 118
    .local v6, "returnData":Landroid/os/Bundle;
    const/4 v7, 0x2

    .line 119
    .local v7, "returnvalue":I
    const/4 v3, 0x0

    .line 121
    .local v3, "numKeys":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 122
    .local v1, "keylist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    const/4 v4, 0x0

    .line 124
    .local v4, "outstream":Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;
    new-instance v4, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;

    .end local v4    # "outstream":Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;
    iget-object v8, p0, Lcom/android/emailcommon/pgp/PGPExportKeys;->mOutstream:Ljava/io/OutputStream;

    invoke-direct {v4, v8}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 126
    .restart local v4    # "outstream":Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;
    iget v8, p0, Lcom/android/emailcommon/pgp/PGPExportKeys;->mExportType:I

    const/4 v9, 0x3

    if-ne v8, v9, :cond_0

    .line 127
    iget-object v8, p0, Lcom/android/emailcommon/pgp/PGPExportKeys;->mKeys:[J

    invoke-direct {p0, v8}, Lcom/android/emailcommon/pgp/PGPExportKeys;->getSecretKeyList([J)Ljava/util/List;

    move-result-object v1

    .line 128
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    .line 129
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    invoke-direct {v2, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;-><init>(Ljava/util/List;)V

    .line 130
    .local v2, "keyring":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    invoke-virtual {v2, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->encode(Ljava/io/OutputStream;)V

    .line 137
    .end local v2    # "keyring":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    :goto_0
    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->close()V

    .line 139
    const-string v8, "errorvalue"

    invoke-virtual {v6, v8, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 140
    const-string v8, "keycount"

    invoke-virtual {v6, v8, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 141
    const-string v8, "Status"

    const/4 v9, 0x1

    invoke-virtual {v6, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 143
    new-instance v5, Lcom/android/emailcommon/pgp/PGPOperationResultData;

    invoke-direct {v5}, Lcom/android/emailcommon/pgp/PGPOperationResultData;-><init>()V

    .line 145
    .local v5, "res":Lcom/android/emailcommon/pgp/PGPOperationResultData;
    const/16 v8, 0x8

    iput v8, v5, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mPgpOperation:I

    .line 146
    iput-object v6, v5, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mMsg:Landroid/os/Bundle;

    .line 148
    iget-object v8, p0, Lcom/android/emailcommon/pgp/PGPExportKeys;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    invoke-interface {v8, v7, v0, v5}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    .line 149
    return-void

    .line 132
    .end local v5    # "res":Lcom/android/emailcommon/pgp/PGPOperationResultData;
    :cond_0
    iget-object v8, p0, Lcom/android/emailcommon/pgp/PGPExportKeys;->mKeys:[J

    invoke-direct {p0, v8}, Lcom/android/emailcommon/pgp/PGPExportKeys;->getpublicKeyList([J)Ljava/util/List;

    move-result-object v1

    .line 133
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

    invoke-direct {v2, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;-><init>(Ljava/util/List;)V

    .line 134
    .local v2, "keyring":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    .line 135
    invoke-virtual {v2, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;->encode(Ljava/io/OutputStream;)V

    goto :goto_0
.end method

.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    .line 88
    const/4 v1, 0x0

    .line 90
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_0
    iget v4, p0, Lcom/android/emailcommon/pgp/PGPExportKeys;->mExportType:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 91
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPExportKeys;->exportKeys()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/emailcommon/pgp/PGPKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 104
    :goto_0
    if-eqz v1, :cond_0

    .line 105
    new-instance v2, Lcom/android/emailcommon/pgp/PGPOperationResultData;

    invoke-direct {v2}, Lcom/android/emailcommon/pgp/PGPOperationResultData;-><init>()V

    .line 106
    .local v2, "res":Lcom/android/emailcommon/pgp/PGPOperationResultData;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 107
    .local v3, "returnData":Landroid/os/Bundle;
    const-string v4, "errorvalue"

    invoke-virtual {v3, v4, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 108
    const/16 v4, 0x8

    iput v4, v2, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mPgpOperation:I

    .line 109
    iput-object v3, v2, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mMsg:Landroid/os/Bundle;

    .line 110
    iget-object v4, p0, Lcom/android/emailcommon/pgp/PGPExportKeys;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    invoke-interface {v4, v6, v1, v2}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    .line 112
    .end local v2    # "res":Lcom/android/emailcommon/pgp/PGPOperationResultData;
    .end local v3    # "returnData":Landroid/os/Bundle;
    :cond_0
    return-void

    .line 93
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPExportKeys;->exportSelectedKeys()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/android/emailcommon/pgp/PGPKeyException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 95
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Ljava/io/IOException;
    move-object v1, v0

    .line 103
    goto :goto_0

    .line 97
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 98
    .local v0, "e":Lcom/android/emailcommon/pgp/PGPKeyException;
    move-object v1, v0

    .line 103
    goto :goto_0

    .line 99
    .end local v0    # "e":Lcom/android/emailcommon/pgp/PGPKeyException;
    :catch_2
    move-exception v0

    .line 100
    .local v0, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    move-object v1, v0

    .line 103
    goto :goto_0

    .line 101
    .end local v0    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    :catch_3
    move-exception v0

    .line 102
    .local v0, "e":Ljava/lang/Exception;
    move-object v1, v0

    goto :goto_0
.end method
