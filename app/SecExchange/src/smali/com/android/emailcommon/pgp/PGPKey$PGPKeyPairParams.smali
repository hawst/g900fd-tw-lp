.class public Lcom/android/emailcommon/pgp/PGPKey$PGPKeyPairParams;
.super Ljava/lang/Object;
.source "PGPKey.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/emailcommon/pgp/PGPKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PGPKeyPairParams"
.end annotation


# instance fields
.field private pubKeyRing:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

.field private secKeyRing:Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;


# direct methods
.method public constructor <init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;)V
    .locals 0
    .param p1, "publicKeyRing"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    .param p2, "secretKeyRing"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/android/emailcommon/pgp/PGPKey$PGPKeyPairParams;->pubKeyRing:Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

    .line 40
    iput-object p2, p0, Lcom/android/emailcommon/pgp/PGPKey$PGPKeyPairParams;->secKeyRing:Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    .line 41
    return-void
.end method
