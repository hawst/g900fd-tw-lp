.class Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "PGPContentProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/emailcommon/pgp/PGPContentProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DatabaseHelper"
.end annotation


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    const-string v0, "pgp.db"

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 59
    return-void
.end method

.method private addColumnToTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "table_name"    # Ljava/lang/String;
    .param p3, "col_name"    # Ljava/lang/String;
    .param p4, "col_type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 140
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " add column "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 141
    .local v0, "cmd":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "alter table "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 142
    return-void
.end method

.method private copyDataKeyrings(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 14
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 145
    const/4 v8, 0x0

    .line 146
    .local v8, "c":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 147
    .local v9, "c_id":I
    const/4 v13, 0x0

    .line 149
    .local v13, "previousMasterKeyID":Ljava/lang/String;
    :try_start_0
    const-string v1, "keyrings"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "c_master_key_id"

    aput-object v3, v2, v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 150
    :cond_0
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 151
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 152
    .local v10, "cv":Landroid/content/ContentValues;
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 153
    .local v12, "masterKeyID":Ljava/lang/String;
    if-eqz v12, :cond_1

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 155
    :cond_1
    add-int/lit8 v9, v9, 0x1

    .line 156
    const-string v0, "c_id"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 157
    const-string v0, "keyrings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "c_master_key_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v10, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 158
    move-object v13, v12

    .line 159
    goto :goto_0

    .line 163
    .end local v10    # "cv":Landroid/content/ContentValues;
    .end local v12    # "masterKeyID":Ljava/lang/String;
    :cond_2
    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 164
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 167
    :cond_3
    :goto_1
    return-void

    .line 160
    :catch_0
    move-exception v11

    .line 161
    .local v11, "e":Landroid/database/SQLException;
    :try_start_1
    const-string v0, "PGPContentProvider"

    const-string v1, "error while copying Keyrings data from previous db"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 164
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 163
    .end local v11    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    invoke-interface {v8}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_4

    .line 164
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method private copyDataPrivateKeys(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 185
    const-string v0, "update privatekeys set c_email_id=c_user_id, c_user_name=c_name"

    .line 188
    .local v0, "cmd":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    :goto_0
    return-void

    .line 189
    :catch_0
    move-exception v1

    .line 190
    .local v1, "e":Landroid/database/SQLException;
    const-string v2, "PGPContentProvider"

    const-string v3, "error while copying data from previous db"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private copyDataPublicKeys(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 171
    const-string v0, "update publickeys set c_email_id=c_user_id, c_user_name=c_name, c_id=(select c_id from keyrings where publickeys.c_key_id=c_master_key_id)"

    .line 176
    .local v0, "cmd":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    :goto_0
    return-void

    .line 177
    :catch_0
    move-exception v1

    .line 178
    .local v1, "e":Landroid/database/SQLException;
    :try_start_1
    const-string v2, "PGPContentProvider"

    const-string v3, "error while copying data from previous db"

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 179
    .end local v1    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v2

    throw v2
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 65
    const-string v0, "CREATE TABLE publickeys (_id INTEGER PRIMARY KEY,c_id INTEGER,c_key_id INT64,c_key_ring BLOB,c_email_id TEXT,c_user_name TEXT,c_expiry INTEGER,c_algorithm INTEGER,c_creation INTEGER,c_can_encrypt INTEGER,c_is_default INT64);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 81
    const-string v0, "CREATE TABLE privatekeys (_id INTEGER PRIMARY KEY,c_key_id INT64,c_key_ring BLOB,c_email_id TEXT,c_user_name TEXT,c_expiry INTEGER,c_algorithm INTEGER,c_creation INTEGER,c_can_sign INTEGER,c_is_default INTEGER,c_pass_phrase TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 98
    const-string v0, "CREATE TABLE keyrings (_id INTEGER PRIMARY KEY,c_id INTEGER,c_master_key_id INT64,c_key_ring_data BLOB,c_type INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 111
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 115
    const/4 v1, 0x0

    .line 116
    .local v1, "isColumnExist":Z
    const/4 v2, 0x1

    if-ne p2, v2, :cond_1

    .line 118
    :try_start_0
    const-string v2, "keyrings"

    const-string v3, "c_id"

    const-string v4, "INTEGER"

    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;->addColumnToTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v2, "publickeys"

    const-string v3, "c_id"

    const-string v4, "INTEGER"

    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;->addColumnToTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v2, "publickeys"

    const-string v3, "c_user_name"

    const-string v4, "TEXT"

    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;->addColumnToTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v2, "publickeys"

    const-string v3, "c_email_id"

    const-string v4, "TEXT"

    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;->addColumnToTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v2, "privatekeys"

    const-string v3, "c_user_name"

    const-string v4, "TEXT"

    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;->addColumnToTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v2, "privatekeys"

    const-string v3, "c_email_id"

    const-string v4, "TEXT"

    invoke-direct {p0, p1, v2, v3, v4}, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;->addColumnToTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :goto_0
    if-nez v1, :cond_0

    .line 128
    invoke-direct {p0, p1}, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;->copyDataKeyrings(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 129
    invoke-direct {p0, p1}, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;->copyDataPublicKeys(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 130
    invoke-direct {p0, p1}, Lcom/android/emailcommon/pgp/PGPContentProvider$DatabaseHelper;->copyDataPrivateKeys(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 132
    :cond_0
    const/4 p2, 0x2

    .line 135
    :cond_1
    const-string v2, "PGPContentProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "upgrade complete to new version "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    return-void

    .line 124
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Landroid/database/SQLException;
    const/4 v1, 0x1

    goto :goto_0
.end method
