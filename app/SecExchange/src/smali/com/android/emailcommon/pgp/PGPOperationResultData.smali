.class public Lcom/android/emailcommon/pgp/PGPOperationResultData;
.super Ljava/lang/Object;
.source "PGPOperationResultData.java"


# instance fields
.field private final mIntegerityCheckPassed:Z

.field private final mIsDetachedSignature:Z

.field private final mIsEncrypted:Z

.field private final mIsSeckeyExists:Z

.field private final mIsSigned:Z

.field private final mIsSignkeyExists:Z

.field public mKeypair:Ljava/lang/Object;

.field public mMsg:Landroid/os/Bundle;

.field private final mOutFileName:Ljava/lang/String;

.field public mPgpOperation:I

.field private final mSignatureVerified:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mSignatureVerified:Z

    .line 23
    iput-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mIntegerityCheckPassed:Z

    .line 24
    iput-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mIsDetachedSignature:Z

    .line 25
    iput-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mIsEncrypted:Z

    .line 26
    iput-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mIsSigned:Z

    .line 27
    iput-object v1, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mOutFileName:Ljava/lang/String;

    .line 28
    iput v0, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mPgpOperation:I

    .line 29
    iput-object v1, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mMsg:Landroid/os/Bundle;

    .line 30
    iput-object v1, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mKeypair:Ljava/lang/Object;

    .line 33
    iput-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mIsSignkeyExists:Z

    .line 34
    iput-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mIsSeckeyExists:Z

    .line 35
    return-void
.end method

.method public constructor <init>(ZZZZZZZLjava/lang/String;)V
    .locals 2
    .param p1, "isEncrypted"    # Z
    .param p2, "isSigned"    # Z
    .param p3, "isSignVerified"    # Z
    .param p4, "isSignKeyExists"    # Z
    .param p5, "isSecKeyExists"    # Z
    .param p6, "isDetSign"    # Z
    .param p7, "isIntegCheckPassed"    # Z
    .param p8, "outFileName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-boolean p3, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mSignatureVerified:Z

    .line 42
    iput-boolean p7, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mIntegerityCheckPassed:Z

    .line 43
    iput-boolean p6, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mIsDetachedSignature:Z

    .line 44
    iput-boolean p1, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mIsEncrypted:Z

    .line 45
    iput-boolean p2, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mIsSigned:Z

    .line 46
    iput-object p8, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mOutFileName:Ljava/lang/String;

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mPgpOperation:I

    .line 48
    iput-object v1, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mMsg:Landroid/os/Bundle;

    .line 49
    iput-object v1, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mKeypair:Ljava/lang/Object;

    .line 50
    iput-boolean p4, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mIsSignkeyExists:Z

    .line 51
    iput-boolean p5, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mIsSeckeyExists:Z

    .line 52
    return-void
.end method


# virtual methods
.method public getIsDetachedSignature()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mIsDetachedSignature:Z

    return v0
.end method

.method public getIsEncrypted()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mIsEncrypted:Z

    return v0
.end method

.method public getIsSignKeyExists()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mIsSignkeyExists:Z

    return v0
.end method

.method public getIsSignatureVerified()Z
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mSignatureVerified:Z

    return v0
.end method

.method public getIsSigned()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mIsSigned:Z

    return v0
.end method
