.class public Lcom/android/emailcommon/pgp/PGPRCertificate;
.super Lcom/android/emailcommon/pgp/PGPOperation;
.source "PGPRCertificate.java"


# instance fields
.field mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

.field private mKey:Ljava/lang/String;

.field public mKeyId:J

.field mOutstream:Ljava/io/OutputStream;

.field public mPassphrase:Ljava/lang/String;

.field private mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLcom/android/emailcommon/pgp/PGPDatabase;Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "passphrase"    # Ljava/lang/String;
    .param p2, "keyId"    # J
    .param p4, "database"    # Lcom/android/emailcommon/pgp/PGPDatabase;
    .param p5, "outstream"    # Ljava/io/OutputStream;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/android/emailcommon/pgp/PGPOperation;-><init>()V

    .line 26
    const-string v0, "Comment"

    iput-object v0, p0, Lcom/android/emailcommon/pgp/PGPRCertificate;->mKey:Ljava/lang/String;

    .line 27
    const-string v0, "A revocation certificate should follow"

    iput-object v0, p0, Lcom/android/emailcommon/pgp/PGPRCertificate;->mValue:Ljava/lang/String;

    .line 31
    iput-object p1, p0, Lcom/android/emailcommon/pgp/PGPRCertificate;->mPassphrase:Ljava/lang/String;

    .line 32
    iput-wide p2, p0, Lcom/android/emailcommon/pgp/PGPRCertificate;->mKeyId:J

    .line 33
    iput-object p4, p0, Lcom/android/emailcommon/pgp/PGPRCertificate;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    .line 34
    iput-object p5, p0, Lcom/android/emailcommon/pgp/PGPRCertificate;->mOutstream:Ljava/io/OutputStream;

    .line 35
    return-void
.end method


# virtual methods
.method public createrevocationCertificate()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/emailcommon/pgp/PGPKeyException;,
            Ljava/security/NoSuchAlgorithmException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 58
    iget-object v2, p0, Lcom/android/emailcommon/pgp/PGPRCertificate;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    iget-wide v4, p0, Lcom/android/emailcommon/pgp/PGPRCertificate;->mKeyId:J

    invoke-virtual {v2, v4, v5}, Lcom/android/emailcommon/pgp/PGPDatabase;->getPublicKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v0

    .line 59
    .local v0, "pubkey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    iget-object v2, p0, Lcom/android/emailcommon/pgp/PGPRCertificate;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    iget-wide v4, p0, Lcom/android/emailcommon/pgp/PGPRCertificate;->mKeyId:J

    invoke-virtual {v2, v4, v5}, Lcom/android/emailcommon/pgp/PGPDatabase;->getPrivateKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-result-object v1

    .line 61
    .local v1, "seckey":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 62
    invoke-virtual {p0, v0, v1}, Lcom/android/emailcommon/pgp/PGPRCertificate;->generaterevcert(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;)V

    .line 64
    :cond_0
    return-void
.end method

.method public generaterevcert(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;)V
    .locals 7
    .param p1, "pubkey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .param p2, "seckey"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Lcom/android/emailcommon/pgp/PGPKeyException;,
            Ljava/security/SignatureException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;

    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getAlgorithm()I

    move-result v4

    const/4 v5, 0x2

    new-instance v6, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v6}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-direct {v2, v4, v5, v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;-><init>(IILjava/security/Provider;)V

    .line 73
    .local v2, "signGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;
    iget-object v4, p0, Lcom/android/emailcommon/pgp/PGPRCertificate;->mPassphrase:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    new-instance v5, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v5}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-virtual {p2, v4, v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->extractPrivateKey([CLjava/security/Provider;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    move-result-object v1

    .line 76
    .local v1, "privkey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    if-nez v1, :cond_0

    .line 77
    new-instance v4, Lcom/android/emailcommon/pgp/PGPKeyException;

    const-string v5, "Invalid Passphrase"

    invoke-direct {v4, v5}, Lcom/android/emailcommon/pgp/PGPKeyException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 79
    :cond_0
    const/16 v4, 0x20

    invoke-virtual {v2, v4, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->initSign(ILcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;)V

    .line 81
    invoke-virtual {v2, p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->generateCertification(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    move-result-object v3

    .line 84
    .local v3, "signature":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;
    const/4 v0, 0x0

    .line 85
    .local v0, "outstream":Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;
    new-instance v0, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;

    .end local v0    # "outstream":Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;
    iget-object v4, p0, Lcom/android/emailcommon/pgp/PGPRCertificate;->mOutstream:Ljava/io/OutputStream;

    invoke-direct {v0, v4}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 87
    .restart local v0    # "outstream":Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;
    iget-object v4, p0, Lcom/android/emailcommon/pgp/PGPRCertificate;->mKey:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/emailcommon/pgp/PGPRCertificate;->mValue:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-virtual {v3, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->encode(Ljava/io/OutputStream;)V

    .line 90
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;->close()V

    .line 91
    return-void
.end method

.method public run()V
    .locals 1

    .prologue
    .line 40
    :try_start_0
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPRCertificate;->createrevocationCertificate()V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/android/emailcommon/pgp/PGPKeyException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 53
    :goto_0
    return-void

    .line 41
    :catch_0
    move-exception v0

    .line 42
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    :try_start_1
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 50
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v0

    .line 51
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 43
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 44
    .local v0, "e":Ljava/security/SignatureException;
    :try_start_2
    invoke-virtual {v0}, Ljava/security/SignatureException;->printStackTrace()V

    goto :goto_0

    .line 45
    .end local v0    # "e":Ljava/security/SignatureException;
    :catch_3
    move-exception v0

    .line 46
    .local v0, "e":Lcom/android/emailcommon/pgp/PGPKeyException;
    invoke-virtual {v0}, Lcom/android/emailcommon/pgp/PGPKeyException;->printStackTrace()V

    goto :goto_0

    .line 47
    .end local v0    # "e":Lcom/android/emailcommon/pgp/PGPKeyException;
    :catch_4
    move-exception v0

    .line 48
    .local v0, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;->printStackTrace()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0
.end method
