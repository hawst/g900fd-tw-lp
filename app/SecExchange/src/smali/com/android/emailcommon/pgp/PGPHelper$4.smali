.class Lcom/android/emailcommon/pgp/PGPHelper$4;
.super Ljava/lang/Object;
.source "PGPHelper.java"

# interfaces
.implements Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/emailcommon/pgp/PGPHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/emailcommon/pgp/PGPHelper;

.field final synthetic val$msg:Lcom/android/emailcommon/pgp/PGPHelper$Message;

.field final synthetic val$out:Ljava/io/OutputStream;


# virtual methods
.method public onProgressUpdate(I)V
    .locals 0
    .param p1, "percentComplete"    # I

    .prologue
    .line 674
    return-void
.end method

.method public onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V
    .locals 5
    .param p1, "resultCode"    # I
    .param p2, "e"    # Ljava/lang/Exception;
    .param p3, "result"    # Lcom/android/emailcommon/pgp/PGPOperationResultData;

    .prologue
    .line 678
    :try_start_0
    iget-object v3, p0, Lcom/android/emailcommon/pgp/PGPHelper$4;->this$0:Lcom/android/emailcommon/pgp/PGPHelper;

    invoke-virtual {p3}, Lcom/android/emailcommon/pgp/PGPOperationResultData;->getIsSignKeyExists()Z

    move-result v4

    # setter for: Lcom/android/emailcommon/pgp/PGPHelper;->mIsSignKeyExists:Z
    invoke-static {v3, v4}, Lcom/android/emailcommon/pgp/PGPHelper;->access$002(Lcom/android/emailcommon/pgp/PGPHelper;Z)Z

    .line 679
    invoke-virtual {p3}, Lcom/android/emailcommon/pgp/PGPOperationResultData;->getIsDetachedSignature()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 680
    iget-object v3, p0, Lcom/android/emailcommon/pgp/PGPHelper$4;->val$msg:Lcom/android/emailcommon/pgp/PGPHelper$Message;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/android/emailcommon/pgp/PGPHelper$Message;->isDetachedSig:Z

    .line 681
    iget-object v3, p0, Lcom/android/emailcommon/pgp/PGPHelper$4;->val$msg:Lcom/android/emailcommon/pgp/PGPHelper$Message;

    invoke-virtual {p3}, Lcom/android/emailcommon/pgp/PGPOperationResultData;->getIsSignatureVerified()Z

    move-result v4

    iput-boolean v4, v3, Lcom/android/emailcommon/pgp/PGPHelper$Message;->isSigVerfied:Z

    .line 697
    :goto_0
    return-void

    .line 684
    :cond_0
    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v3, p0, Lcom/android/emailcommon/pgp/PGPHelper$4;->val$out:Ljava/io/OutputStream;

    check-cast v3, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 686
    .local v1, "is":Ljava/io/InputStream;
    new-instance v2, Lcom/android/emailcommon/internet/MimeMessage;

    invoke-direct {v2, v1}, Lcom/android/emailcommon/internet/MimeMessage;-><init>(Ljava/io/InputStream;)V

    .line 687
    .local v2, "mimeMessage":Lcom/android/emailcommon/internet/MimeMessage;
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 689
    iget-object v3, p0, Lcom/android/emailcommon/pgp/PGPHelper$4;->this$0:Lcom/android/emailcommon/pgp/PGPHelper;

    iget-object v4, p0, Lcom/android/emailcommon/pgp/PGPHelper$4;->val$msg:Lcom/android/emailcommon/pgp/PGPHelper$Message;

    invoke-virtual {v3, v2, v4}, Lcom/android/emailcommon/pgp/PGPHelper;->handleBodyPartPGP(Lcom/android/emailcommon/mail/Part;Lcom/android/emailcommon/pgp/PGPHelper$Message;)V

    .line 690
    iget-object v3, p0, Lcom/android/emailcommon/pgp/PGPHelper$4;->val$msg:Lcom/android/emailcommon/pgp/PGPHelper$Message;

    invoke-virtual {p3}, Lcom/android/emailcommon/pgp/PGPOperationResultData;->getIsEncrypted()Z

    move-result v4

    iput-boolean v4, v3, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mEncrypted:Z

    .line 691
    iget-object v3, p0, Lcom/android/emailcommon/pgp/PGPHelper$4;->val$msg:Lcom/android/emailcommon/pgp/PGPHelper$Message;

    invoke-virtual {p3}, Lcom/android/emailcommon/pgp/PGPOperationResultData;->getIsSigned()Z

    move-result v4

    iput-boolean v4, v3, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mSigned:Z

    .line 692
    iget-object v3, p0, Lcom/android/emailcommon/pgp/PGPHelper$4;->val$msg:Lcom/android/emailcommon/pgp/PGPHelper$Message;

    invoke-virtual {p3}, Lcom/android/emailcommon/pgp/PGPOperationResultData;->getIsSignatureVerified()Z

    move-result v4

    iput-boolean v4, v3, Lcom/android/emailcommon/pgp/PGPHelper$Message;->mVerified:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 694
    .end local v1    # "is":Ljava/io/InputStream;
    .end local v2    # "mimeMessage":Lcom/android/emailcommon/internet/MimeMessage;
    :catch_0
    move-exception v0

    .line 695
    .local v0, "ex":Ljava/lang/Exception;
    # getter for: Lcom/android/emailcommon/pgp/PGPHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/emailcommon/pgp/PGPHelper;->access$100()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
