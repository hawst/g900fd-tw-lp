.class public Lcom/android/emailcommon/pgp/PGPKeyManager;
.super Ljava/lang/Object;
.source "PGPKeyManager.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

.field private mLocalUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

.field private mOpActive:Z

.field private mSyncOp:Z

.field private mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;Lcom/android/emailcommon/pgp/PGPDatabase;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appUpdater"    # Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;
    .param p3, "database"    # Lcom/android/emailcommon/pgp/PGPDatabase;
    .param p4, "isSyncOp"    # Z

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPKeyManager;->mSyncOp:Z

    .line 57
    iput-object p1, p0, Lcom/android/emailcommon/pgp/PGPKeyManager;->mContext:Landroid/content/Context;

    .line 58
    iput-object p3, p0, Lcom/android/emailcommon/pgp/PGPKeyManager;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    .line 59
    iput-object p2, p0, Lcom/android/emailcommon/pgp/PGPKeyManager;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    .line 60
    iput-boolean p4, p0, Lcom/android/emailcommon/pgp/PGPKeyManager;->mSyncOp:Z

    .line 61
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPKeyManager;->init()V

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/android/emailcommon/pgp/PGPKeyManager;)Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;
    .locals 1
    .param p0, "x0"    # Lcom/android/emailcommon/pgp/PGPKeyManager;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/android/emailcommon/pgp/PGPKeyManager;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/emailcommon/pgp/PGPKeyManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/emailcommon/pgp/PGPKeyManager;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/android/emailcommon/pgp/PGPKeyManager;->mOpActive:Z

    return p1
.end method

.method private getDecryptedPassPhrase([BLjava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "passphrase"    # [B
    .param p2, "decryptpaswrd"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchProviderException;,
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 196
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 197
    .local v1, "in":Ljava/io/InputStream;
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 198
    .local v4, "outbytes":Ljava/io/OutputStream;
    new-instance v2, Lcom/android/emailcommon/pgp/PassphraseManager;

    invoke-direct {v2}, Lcom/android/emailcommon/pgp/PassphraseManager;-><init>()V

    .line 199
    .local v2, "mgr":Lcom/android/emailcommon/pgp/PassphraseManager;
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    invoke-virtual {v2, v1, v4, v6}, Lcom/android/emailcommon/pgp/PassphraseManager;->decryptContent(Ljava/io/InputStream;Ljava/io/OutputStream;[C)V

    move-object v3, v4

    .line 200
    check-cast v3, Ljava/io/ByteArrayOutputStream;

    .line 201
    .local v3, "out":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 202
    .local v0, "buffer":[B
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([B)V

    .line 203
    .local v5, "pasword":Ljava/lang/String;
    return-object v5
.end method


# virtual methods
.method public getPassPhrase(JLjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "keyId"    # J
    .param p3, "pwrdtodecrypt"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchProviderException;,
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
        }
    .end annotation

    .prologue
    .line 188
    iget-object v1, p0, Lcom/android/emailcommon/pgp/PGPKeyManager;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    invoke-virtual {v1, p1, p2}, Lcom/android/emailcommon/pgp/PGPDatabase;->getPassPhrase(J)[B

    move-result-object v0

    .line 189
    .local v0, "PassPhrase":[B
    if-nez v0, :cond_0

    .line 190
    new-instance v1, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;

    const-string v2, "PGPKeyManager getPassPhrase : passPhrase is null"

    invoke-direct {v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 191
    :cond_0
    invoke-direct {p0, v0, p3}, Lcom/android/emailcommon/pgp/PGPKeyManager;->getDecryptedPassPhrase([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public final init()V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/android/emailcommon/pgp/PGPKeyManager$1;

    invoke-direct {v0, p0}, Lcom/android/emailcommon/pgp/PGPKeyManager$1;-><init>(Lcom/android/emailcommon/pgp/PGPKeyManager;)V

    iput-object v0, p0, Lcom/android/emailcommon/pgp/PGPKeyManager;->mLocalUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    .line 75
    return-void
.end method
