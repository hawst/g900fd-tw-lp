.class public Lcom/android/emailcommon/pgp/PGPEncryptSign;
.super Lcom/android/emailcommon/pgp/PGPOperation;
.source "PGPEncryptSign.java"


# instance fields
.field private mDetached:Z

.field private mEncryptOnly:Z

.field private mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

.field private mdatabase:Lcom/android/emailcommon/pgp/PGPDatabase;


# direct methods
.method constructor <init>(Lcom/android/emailcommon/pgp/EncryptSignParams;ZZLandroid/content/Context;Lcom/android/emailcommon/pgp/PGPDatabase;)V
    .locals 1
    .param p1, "params"    # Lcom/android/emailcommon/pgp/EncryptSignParams;
    .param p2, "encryptOnly"    # Z
    .param p3, "signOnly"    # Z
    .param p4, "mctxt"    # Landroid/content/Context;
    .param p5, "database"    # Lcom/android/emailcommon/pgp/PGPDatabase;

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Lcom/android/emailcommon/pgp/PGPOperation;-><init>()V

    .line 37
    iput-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mEncryptOnly:Z

    .line 39
    iput-boolean v0, p0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mDetached:Z

    .line 46
    iput-object p1, p0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    .line 47
    iput-boolean p2, p0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mEncryptOnly:Z

    .line 48
    iput-object p5, p0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mdatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    .line 49
    return-void
.end method


# virtual methods
.method public encryptAndSign()I
    .locals 38
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/SignatureException;,
            Lcom/android/emailcommon/pgp/PGPKeyException;
        }
    .end annotation

    .prologue
    .line 109
    new-instance v4, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v4}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-static {v4}, Ljava/security/Security;->addProvider(Ljava/security/Provider;)I

    .line 110
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getInputStream()Ljava/io/InputStream;

    move-result-object v18

    .line 111
    .local v18, "in":Ljava/io/InputStream;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v22

    .line 112
    .local v22, "out":Ljava/io/OutputStream;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getEncryptKeyIds()[J

    move-result-object v20

    .line 114
    .local v20, "keyIds":[J
    const/16 v33, -0x1

    .line 115
    .local v33, "symmetricAlg":I
    const/4 v12, 0x0

    .line 117
    .local v12, "comprAlg":I
    if-nez v20, :cond_0

    .line 122
    new-instance v4, Lcom/android/emailcommon/pgp/PGPKeyException;

    const-string v5, "KeyId arrray is NULL"

    invoke-direct {v4, v5}, Lcom/android/emailcommon/pgp/PGPKeyException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 127
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getArmor()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 128
    new-instance v23, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;-><init>(Ljava/io/OutputStream;)V

    .end local v22    # "out":Ljava/io/OutputStream;
    .local v23, "out":Ljava/io/OutputStream;
    move-object/from16 v22, v23

    .line 131
    .end local v23    # "out":Ljava/io/OutputStream;
    .restart local v22    # "out":Ljava/io/OutputStream;
    :cond_1
    move-object/from16 v0, v20

    array-length v4, v0

    const/4 v5, 0x1

    if-le v4, v5, :cond_5

    .line 132
    const/16 v33, 0x2

    .line 133
    const/4 v12, 0x1

    .line 166
    :cond_2
    :goto_0
    new-instance v10, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;

    const/4 v4, 0x1

    new-instance v5, Ljava/security/SecureRandom;

    invoke-direct {v5}, Ljava/security/SecureRandom;-><init>()V

    new-instance v34, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct/range {v34 .. v34}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    move/from16 v0, v33

    move-object/from16 v1, v34

    invoke-direct {v10, v0, v4, v5, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;-><init>(IZLjava/security/SecureRandom;Ljava/security/Provider;)V

    .line 169
    .local v10, "cPk":Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;
    const/4 v15, 0x0

    .line 172
    .local v15, "i":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mdatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    aget-wide v34, v20, v15

    move-wide/from16 v0, v34

    invoke-virtual {v4, v0, v1}, Lcom/android/emailcommon/pgp/PGPDatabase;->getPublicKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v13

    .line 173
    .local v13, "enKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    if-eqz v13, :cond_4

    invoke-virtual {v10, v13}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->addMethod(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;)V

    .line 174
    :cond_4
    add-int/lit8 v15, v15, 0x1

    .line 175
    move-object/from16 v0, v20

    array-length v4, v0

    if-gt v4, v15, :cond_3

    .line 177
    const/high16 v4, 0x10000

    new-array v4, v4, [B

    move-object/from16 v0, v22

    invoke-virtual {v10, v0, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;->open(Ljava/io/OutputStream;[B)Ljava/io/OutputStream;

    move-result-object v9

    .line 179
    .local v9, "cOut":Ljava/io/OutputStream;
    const/16 v29, 0x0

    .line 183
    .local v29, "sGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mEncryptOnly:Z

    if-nez v4, :cond_9

    .line 184
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mdatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v5}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getSignSecretKeyId()J

    move-result-wide v34

    move-wide/from16 v0, v34

    invoke-virtual {v4, v0, v1}, Lcom/android/emailcommon/pgp/PGPDatabase;->getPrivateKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-result-object v26

    .line 185
    .local v26, "pgpSec":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    if-eqz v26, :cond_9

    .line 186
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getSignPassphrase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    new-instance v5, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v5}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    move-object/from16 v0, v26

    invoke-virtual {v0, v4, v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->extractPrivateKey([CLjava/security/Provider;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    move-result-object v25

    .line 189
    .local v25, "pgpPrivKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    if-nez v25, :cond_8

    .line 195
    new-instance v4, Lcom/android/emailcommon/pgp/PGPKeyException;

    const-string v5, "Signing Failed - Invalid Passphrase"

    invoke-direct {v4, v5}, Lcom/android/emailcommon/pgp/PGPKeyException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 137
    .end local v9    # "cOut":Ljava/io/OutputStream;
    .end local v10    # "cPk":Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;
    .end local v13    # "enKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .end local v15    # "i":I
    .end local v25    # "pgpPrivKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .end local v26    # "pgpSec":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    .end local v29    # "sGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mdatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    const/4 v5, 0x0

    aget-wide v34, v20, v5

    move-wide/from16 v0, v34

    invoke-virtual {v4, v0, v1}, Lcom/android/emailcommon/pgp/PGPDatabase;->getPublicKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v14

    .line 139
    .local v14, "encKey1":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    if-nez v14, :cond_6

    .line 144
    new-instance v4, Lcom/android/emailcommon/pgp/PGPKeyException;

    const-string v5, "Encryption Key not found"

    invoke-direct {v4, v5}, Lcom/android/emailcommon/pgp/PGPKeyException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 150
    :cond_6
    invoke-virtual {v14}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getPreferredSymmetricAlgorithms()[I

    move-result-object v28

    .line 152
    .local v28, "prefSymAlgs":[I
    if-eqz v28, :cond_7

    .line 153
    const/4 v4, 0x0

    aget v33, v28, v4

    .line 159
    :goto_1
    invoke-virtual {v14}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getPreferredCompressionAlgorithms()[I

    move-result-object v27

    .line 161
    .local v27, "prefComprAlgs":[I
    if-eqz v27, :cond_2

    .line 162
    const/4 v4, 0x0

    aget v12, v27, v4

    goto/16 :goto_0

    .line 155
    .end local v27    # "prefComprAlgs":[I
    :cond_7
    const/16 v33, 0x2

    goto :goto_1

    .line 200
    .end local v14    # "encKey1":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .end local v28    # "prefSymAlgs":[I
    .restart local v9    # "cOut":Ljava/io/OutputStream;
    .restart local v10    # "cPk":Lcom/android/sec/org/bouncycastle/openpgp/PGPEncryptedDataGenerator;
    .restart local v13    # "enKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .restart local v15    # "i":I
    .restart local v25    # "pgpPrivKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .restart local v26    # "pgpSec":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    .restart local v29    # "sGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;
    :cond_8
    new-instance v29, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;

    .end local v29    # "sGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;
    invoke-virtual/range {v26 .. v26}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getAlgorithm()I

    move-result v4

    const/4 v5, 0x2

    new-instance v34, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct/range {v34 .. v34}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    move-object/from16 v0, v29

    move-object/from16 v1, v34

    invoke-direct {v0, v4, v5, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;-><init>(IILjava/security/Provider;)V

    .line 203
    .restart local v29    # "sGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;
    const/4 v4, 0x0

    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-virtual {v0, v4, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->initSign(ILcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;)V

    .line 205
    invoke-virtual/range {v26 .. v26}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getUserIDs()Ljava/util/Iterator;

    move-result-object v19

    .line 207
    .local v19, "it":Ljava/util/Iterator;
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 208
    new-instance v32, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;

    invoke-direct/range {v32 .. v32}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;-><init>()V

    .line 209
    .local v32, "spGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;
    const/4 v5, 0x0

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v32

    invoke-virtual {v0, v5, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->setSignerUserID(ZLjava/lang/String;)V

    .line 210
    invoke-virtual/range {v32 .. v32}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->setHashedSubpackets(Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;)V

    .line 215
    .end local v19    # "it":Ljava/util/Iterator;
    .end local v25    # "pgpPrivKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .end local v26    # "pgpSec":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    .end local v32    # "spGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getInputDataSize()J

    move-result-wide v16

    .line 216
    .local v16, "fileSize":J
    const/high16 v4, 0x10000

    new-array v7, v4, [B

    .line 217
    .local v7, "buffer":[B
    const/4 v11, 0x0

    .line 218
    .local v11, "comData":Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;
    const/4 v3, 0x0

    .line 220
    .local v3, "bcpgOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    if-nez v12, :cond_e

    .line 221
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .end local v3    # "bcpgOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    invoke-direct {v3, v9}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 227
    .restart local v3    # "bcpgOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mEncryptOnly:Z

    if-nez v4, :cond_a

    if-eqz v29, :cond_a

    .line 228
    invoke-virtual/range {v29 .. v29}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->encode(Ljava/io/OutputStream;)V

    .line 231
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getInputFileModTime()Ljava/util/Date;

    move-result-object v6

    .line 233
    .local v6, "dt":Ljava/util/Date;
    if-nez v6, :cond_b

    .line 234
    new-instance v6, Ljava/util/Date;

    .end local v6    # "dt":Ljava/util/Date;
    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    .line 237
    .restart local v6    # "dt":Ljava/util/Date;
    :cond_b
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;

    invoke-direct {v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;-><init>()V

    .line 238
    .local v2, "lData":Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;
    const/16 v4, 0x62

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v5}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getInputFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v2 .. v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->open(Ljava/io/OutputStream;CLjava/lang/String;Ljava/util/Date;[B)Ljava/io/OutputStream;

    move-result-object v24

    .line 241
    .local v24, "pOut":Ljava/io/OutputStream;
    array-length v4, v7

    new-array v8, v4, [B

    .line 242
    .local v8, "buf":[B
    const/16 v21, 0x0

    .line 243
    .local v21, "len":I
    const-wide/16 v30, 0x0

    .line 245
    .local v30, "progress":D
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mSyncOp:Z

    if-eqz v4, :cond_f

    .line 246
    :cond_c
    :goto_3
    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/io/InputStream;->read([B)I

    move-result v21

    if-lez v21, :cond_11

    .line 247
    const/4 v4, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v21

    invoke-virtual {v0, v8, v4, v1}, Ljava/io/OutputStream;->write([BII)V

    .line 249
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mEncryptOnly:Z

    if-nez v4, :cond_d

    if-eqz v29, :cond_d

    .line 250
    const/4 v4, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v21

    invoke-virtual {v0, v8, v4, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update([BII)V

    .line 253
    :cond_d
    move/from16 v0, v21

    int-to-double v4, v0

    add-double v30, v30, v4

    .line 255
    const-wide/16 v4, 0x0

    cmp-long v4, v16, v4

    if-lez v4, :cond_c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    if-eqz v4, :cond_c

    .line 256
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v34, v0

    div-double v34, v30, v34

    const-wide/high16 v36, 0x4059000000000000L    # 100.0

    mul-double v34, v34, v36

    move-wide/from16 v0, v34

    double-to-int v5, v0

    invoke-interface {v4, v5}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onProgressUpdate(I)V

    goto :goto_3

    .line 223
    .end local v2    # "lData":Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;
    .end local v6    # "dt":Ljava/util/Date;
    .end local v8    # "buf":[B
    .end local v21    # "len":I
    .end local v24    # "pOut":Ljava/io/OutputStream;
    .end local v30    # "progress":D
    :cond_e
    new-instance v11, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;

    .end local v11    # "comData":Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;
    invoke-direct {v11, v12}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;-><init>(I)V

    .line 224
    .restart local v11    # "comData":Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    .end local v3    # "bcpgOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    invoke-virtual {v11, v9}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->open(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .restart local v3    # "bcpgOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    goto/16 :goto_2

    .line 261
    .restart local v2    # "lData":Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;
    .restart local v6    # "dt":Ljava/util/Date;
    .restart local v8    # "buf":[B
    .restart local v21    # "len":I
    .restart local v24    # "pOut":Ljava/io/OutputStream;
    .restart local v30    # "progress":D
    :cond_f
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mThread:Lcom/android/emailcommon/pgp/PGPThread;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/PGPThread;->isCancel()Z

    move-result v4

    if-nez v4, :cond_11

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/io/InputStream;->read([B)I

    move-result v21

    if-lez v21, :cond_11

    .line 262
    const/4 v4, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v21

    invoke-virtual {v0, v8, v4, v1}, Ljava/io/OutputStream;->write([BII)V

    .line 264
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mEncryptOnly:Z

    if-nez v4, :cond_10

    if-eqz v29, :cond_10

    .line 265
    const/4 v4, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v21

    invoke-virtual {v0, v8, v4, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update([BII)V

    .line 268
    :cond_10
    move/from16 v0, v21

    int-to-double v4, v0

    add-double v30, v30, v4

    .line 270
    const-wide/16 v4, 0x0

    cmp-long v4, v16, v4

    if-lez v4, :cond_f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    if-eqz v4, :cond_f

    .line 271
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v34, v0

    div-double v34, v30, v34

    const-wide/high16 v36, 0x4059000000000000L    # 100.0

    mul-double v34, v34, v36

    move-wide/from16 v0, v34

    double-to-int v5, v0

    invoke-interface {v4, v5}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onProgressUpdate(I)V

    goto :goto_4

    .line 276
    :cond_11
    if-eqz v24, :cond_12

    .line 277
    invoke-virtual/range {v24 .. v24}, Ljava/io/OutputStream;->close()V

    .line 280
    :cond_12
    if-eqz v18, :cond_13

    .line 281
    invoke-virtual/range {v18 .. v18}, Ljava/io/InputStream;->close()V

    .line 284
    :cond_13
    if-eqz v11, :cond_14

    .line 285
    invoke-virtual {v11}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->close()V

    .line 288
    :cond_14
    if-eqz v9, :cond_15

    .line 289
    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V

    .line 292
    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getArmor()Z

    move-result v4

    if-eqz v4, :cond_16

    if-eqz v22, :cond_16

    .line 293
    invoke-virtual/range {v22 .. v22}, Ljava/io/OutputStream;->close()V

    .line 296
    :cond_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    if-eqz v4, :cond_17

    .line 297
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mSyncOp:Z

    if-eqz v4, :cond_18

    .line 298
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    const/4 v5, 0x1

    const/16 v34, 0x0

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    invoke-interface {v4, v5, v0, v1}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    .line 309
    :cond_17
    :goto_5
    const/4 v4, 0x2

    return v4

    .line 301
    :cond_18
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mThread:Lcom/android/emailcommon/pgp/PGPThread;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/PGPThread;->isCancel()Z

    move-result v4

    if-eqz v4, :cond_19

    .line 302
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    const/16 v5, 0xd

    const/16 v34, 0x0

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    invoke-interface {v4, v5, v0, v1}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    goto :goto_5

    .line 304
    :cond_19
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    const/4 v5, 0x1

    const/16 v34, 0x0

    const/16 v35, 0x0

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    invoke-interface {v4, v5, v0, v1}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    goto :goto_5
.end method

.method public run()V
    .locals 5

    .prologue
    .line 61
    const/4 v1, 0x0

    .line 64
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_0
    iget-boolean v2, p0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mEncryptOnly:Z

    if-nez v2, :cond_1

    .line 65
    iget-boolean v2, p0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mDetached:Z

    invoke-virtual {p0, v2}, Lcom/android/emailcommon/pgp/PGPEncryptSign;->sign(Z)I
    :try_end_0
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/android/emailcommon/pgp/PGPKeyException; {:try_start_0 .. :try_end_0} :catch_5

    .line 90
    :goto_0
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    if-eqz v2, :cond_0

    .line 91
    iget-object v2, p0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v1, v4}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    .line 93
    :cond_0
    return-void

    .line 67
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPEncryptSign;->encryptAndSign()I
    :try_end_1
    .catch Ljava/security/NoSuchProviderException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/security/SignatureException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lcom/android/emailcommon/pgp/PGPKeyException; {:try_start_1 .. :try_end_1} :catch_5

    goto :goto_0

    .line 70
    :catch_0
    move-exception v0

    .line 71
    .local v0, "e":Ljava/security/NoSuchProviderException;
    invoke-virtual {v0}, Ljava/security/NoSuchProviderException;->printStackTrace()V

    .line 72
    move-object v1, v0

    .line 88
    goto :goto_0

    .line 73
    .end local v0    # "e":Ljava/security/NoSuchProviderException;
    :catch_1
    move-exception v0

    .line 74
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 75
    move-object v1, v0

    .line 88
    goto :goto_0

    .line 76
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_2
    move-exception v0

    .line 77
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 78
    move-object v1, v0

    .line 88
    goto :goto_0

    .line 79
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 80
    .local v0, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;->printStackTrace()V

    .line 81
    move-object v1, v0

    .line 88
    goto :goto_0

    .line 82
    .end local v0    # "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    :catch_4
    move-exception v0

    .line 83
    .local v0, "e":Ljava/security/SignatureException;
    move-object v1, v0

    .line 84
    invoke-virtual {v0}, Ljava/security/SignatureException;->printStackTrace()V

    goto :goto_0

    .line 85
    .end local v0    # "e":Ljava/security/SignatureException;
    :catch_5
    move-exception v0

    .line 86
    .local v0, "e":Lcom/android/emailcommon/pgp/PGPKeyException;
    move-object v1, v0

    .line 87
    invoke-virtual {v0}, Lcom/android/emailcommon/pgp/PGPKeyException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDetached(Z)V
    .locals 0
    .param p1, "isDetached"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mDetached:Z

    .line 53
    return-void
.end method

.method public sign(Z)I
    .locals 28
    .param p1, "isDetached"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/SignatureException;,
            Ljava/security/NoSuchProviderException;,
            Ljava/security/NoSuchAlgorithmException;,
            Lcom/android/emailcommon/pgp/PGPKeyException;
        }
    .end annotation

    .prologue
    .line 328
    new-instance v4, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v4}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-static {v4}, Ljava/security/Security;->addProvider(Ljava/security/Provider;)I

    .line 330
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getInputStream()Ljava/io/InputStream;

    move-result-object v12

    .line 332
    .local v12, "in":Ljava/io/InputStream;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v16

    .line 334
    .local v16, "out":Ljava/io/OutputStream;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getArmor()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 335
    new-instance v17, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/android/sec/org/bouncycastle/bcpg/ArmoredOutputStream;-><init>(Ljava/io/OutputStream;)V

    .end local v16    # "out":Ljava/io/OutputStream;
    .local v17, "out":Ljava/io/OutputStream;
    move-object/from16 v16, v17

    .line 338
    .end local v17    # "out":Ljava/io/OutputStream;
    .restart local v16    # "out":Ljava/io/OutputStream;
    :cond_0
    const/16 v19, 0x0

    .line 340
    .local v19, "pgpSec":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mdatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v5}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getSignSecretKeyId()J

    move-result-wide v24

    move-wide/from16 v0, v24

    invoke-virtual {v4, v0, v1}, Lcom/android/emailcommon/pgp/PGPDatabase;->getPrivateKey(J)Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-result-object v19

    .line 342
    if-eqz v19, :cond_5

    .line 343
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getSignPassphrase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    new-instance v5, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v5}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->extractPrivateKey([CLjava/security/Provider;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    move-result-object v18

    .line 346
    .local v18, "pgpPrivKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    if-nez v18, :cond_1

    .line 351
    new-instance v4, Lcom/android/emailcommon/pgp/PGPKeyException;

    const-string v5, "Signing Failed - Invalid Passphrase"

    invoke-direct {v4, v5}, Lcom/android/emailcommon/pgp/PGPKeyException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 356
    :cond_1
    new-instance v22, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;

    invoke-virtual/range {v19 .. v19}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getAlgorithm()I

    move-result v4

    const/4 v5, 0x2

    new-instance v24, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct/range {v24 .. v24}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-direct {v0, v4, v5, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;-><init>(IILjava/security/Provider;)V

    .line 359
    .local v22, "sGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;
    const/4 v4, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v4, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->initSign(ILcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;)V

    .line 362
    if-eqz p1, :cond_8

    .line 363
    const/high16 v4, 0x10000

    new-array v7, v4, [B

    .line 364
    .local v7, "buffer":[B
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getInputDataSize()J

    move-result-wide v10

    .line 365
    .local v10, "fileSize":J
    const/4 v15, 0x0

    .line 366
    .local v15, "len":I
    const-wide/16 v20, 0x0

    .line 367
    .local v20, "progress":D
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    move-object/from16 v0, v16

    invoke-direct {v3, v0}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 369
    .local v3, "bOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mSyncOp:Z

    if-eqz v4, :cond_3

    .line 370
    :cond_2
    :goto_0
    invoke-virtual {v12, v7}, Ljava/io/InputStream;->read([B)I

    move-result v15

    if-ltz v15, :cond_4

    .line 371
    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v7, v4, v15}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update([BII)V

    .line 372
    int-to-double v4, v15

    add-double v20, v20, v4

    .line 374
    const-wide/16 v4, 0x0

    cmp-long v4, v10, v4

    if-lez v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    if-eqz v4, :cond_2

    .line 375
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    long-to-double v0, v10

    move-wide/from16 v24, v0

    div-double v24, v20, v24

    const-wide/high16 v26, 0x4059000000000000L    # 100.0

    mul-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-int v5, v0

    invoke-interface {v4, v5}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onProgressUpdate(I)V

    goto :goto_0

    .line 380
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mThread:Lcom/android/emailcommon/pgp/PGPThread;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/PGPThread;->isCancel()Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v12, v7}, Ljava/io/InputStream;->read([B)I

    move-result v15

    if-ltz v15, :cond_4

    .line 381
    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v7, v4, v15}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update([BII)V

    .line 382
    int-to-double v4, v15

    add-double v20, v20, v4

    .line 384
    const-wide/16 v4, 0x0

    cmp-long v4, v10, v4

    if-lez v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    if-eqz v4, :cond_3

    .line 385
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    long-to-double v0, v10

    move-wide/from16 v24, v0

    div-double v24, v20, v24

    const-wide/high16 v26, 0x4059000000000000L    # 100.0

    mul-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-int v5, v0

    invoke-interface {v4, v5}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onProgressUpdate(I)V

    goto :goto_1

    .line 390
    :cond_4
    invoke-virtual/range {v22 .. v22}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->encode(Ljava/io/OutputStream;)V

    .line 458
    .end local v3    # "bOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .end local v7    # "buffer":[B
    .end local v10    # "fileSize":J
    .end local v15    # "len":I
    .end local v18    # "pgpPrivKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .end local v20    # "progress":D
    .end local v22    # "sGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getArmor()Z

    move-result v4

    if-eqz v4, :cond_6

    if-eqz v16, :cond_6

    .line 459
    invoke-virtual/range {v16 .. v16}, Ljava/io/OutputStream;->close()V

    .line 462
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    if-eqz v4, :cond_7

    .line 463
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mSyncOp:Z

    if-eqz v4, :cond_f

    .line 464
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    const/4 v5, 0x1

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-interface {v4, v5, v0, v1}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    .line 474
    :cond_7
    :goto_3
    const/4 v4, 0x2

    return v4

    .line 394
    .restart local v18    # "pgpPrivKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .restart local v22    # "sGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;
    :cond_8
    invoke-virtual/range {v19 .. v19}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getUserIDs()Ljava/util/Iterator;

    move-result-object v13

    .line 396
    .local v13, "it":Ljava/util/Iterator;
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 397
    new-instance v23, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;

    invoke-direct/range {v23 .. v23}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;-><init>()V

    .line 398
    .local v23, "spGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;
    const/4 v5, 0x0

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v5, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->setSignerUserID(ZLjava/lang/String;)V

    .line 399
    invoke-virtual/range {v23 .. v23}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->setHashedSubpackets(Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;)V

    .line 402
    .end local v23    # "spGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;
    :cond_9
    new-instance v9, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;

    const/4 v4, 0x2

    invoke-direct {v9, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;-><init>(I)V

    .line 403
    .local v9, "cGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;
    new-instance v3, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->open(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 404
    .restart local v3    # "bOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->generateOnePassVersion(Z)Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPOnePassSignature;->encode(Ljava/io/OutputStream;)V

    .line 406
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getInputFileModTime()Ljava/util/Date;

    move-result-object v6

    .line 408
    .local v6, "dt":Ljava/util/Date;
    if-nez v6, :cond_a

    .line 409
    new-instance v6, Ljava/util/Date;

    .end local v6    # "dt":Ljava/util/Date;
    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    .line 412
    .restart local v6    # "dt":Ljava/util/Date;
    :cond_a
    const/high16 v4, 0x10000

    new-array v7, v4, [B

    .line 413
    .restart local v7    # "buffer":[B
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getInputDataSize()J

    move-result-wide v10

    .line 414
    .restart local v10    # "fileSize":J
    new-instance v2, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;

    invoke-direct {v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;-><init>()V

    .line 415
    .local v2, "lGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;
    const/16 v4, 0x62

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mParams:Lcom/android/emailcommon/pgp/EncryptSignParams;

    invoke-virtual {v5}, Lcom/android/emailcommon/pgp/EncryptSignParams;->getInputFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v2 .. v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->open(Ljava/io/OutputStream;CLjava/lang/String;Ljava/util/Date;[B)Ljava/io/OutputStream;

    move-result-object v14

    .line 418
    .local v14, "lOut":Ljava/io/OutputStream;
    array-length v4, v7

    new-array v8, v4, [B

    .line 419
    .local v8, "buf":[B
    const/4 v15, 0x0

    .line 420
    .restart local v15    # "len":I
    const-wide/16 v20, 0x0

    .line 422
    .restart local v20    # "progress":D
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mSyncOp:Z

    if-eqz v4, :cond_c

    .line 423
    :cond_b
    :goto_4
    invoke-virtual {v12, v8}, Ljava/io/InputStream;->read([B)I

    move-result v15

    if-ltz v15, :cond_d

    .line 424
    const/4 v4, 0x0

    invoke-virtual {v14, v8, v4, v15}, Ljava/io/OutputStream;->write([BII)V

    .line 425
    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v4, v15}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update([BII)V

    .line 426
    int-to-double v4, v15

    add-double v20, v20, v4

    .line 428
    const-wide/16 v4, 0x0

    cmp-long v4, v10, v4

    if-lez v4, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    if-eqz v4, :cond_b

    .line 429
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    long-to-double v0, v10

    move-wide/from16 v24, v0

    div-double v24, v20, v24

    const-wide/high16 v26, 0x4059000000000000L    # 100.0

    mul-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-int v5, v0

    invoke-interface {v4, v5}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onProgressUpdate(I)V

    goto :goto_4

    .line 435
    :cond_c
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mThread:Lcom/android/emailcommon/pgp/PGPThread;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/PGPThread;->isCancel()Z

    move-result v4

    if-nez v4, :cond_d

    invoke-virtual {v12, v8}, Ljava/io/InputStream;->read([B)I

    move-result v15

    if-ltz v15, :cond_d

    .line 436
    const/4 v4, 0x0

    invoke-virtual {v14, v8, v4, v15}, Ljava/io/OutputStream;->write([BII)V

    .line 437
    const/4 v4, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v4, v15}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->update([BII)V

    .line 438
    int-to-double v4, v15

    add-double v20, v20, v4

    .line 440
    const-wide/16 v4, 0x0

    cmp-long v4, v10, v4

    if-lez v4, :cond_c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    if-eqz v4, :cond_c

    .line 441
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    long-to-double v0, v10

    move-wide/from16 v24, v0

    div-double v24, v20, v24

    const-wide/high16 v26, 0x4059000000000000L    # 100.0

    mul-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-int v5, v0

    invoke-interface {v4, v5}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onProgressUpdate(I)V

    goto :goto_5

    .line 446
    :cond_d
    if-eqz v2, :cond_e

    .line 447
    invoke-virtual {v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;->close()V

    .line 450
    :cond_e
    invoke-virtual/range {v22 .. v22}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;->generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignature;->encode(Ljava/io/OutputStream;)V

    .line 452
    if-eqz v9, :cond_5

    .line 453
    invoke-virtual {v9}, Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;->close()V

    goto/16 :goto_2

    .line 467
    .end local v2    # "lGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPLiteralDataGenerator;
    .end local v3    # "bOut":Lcom/android/sec/org/bouncycastle/bcpg/BCPGOutputStream;
    .end local v6    # "dt":Ljava/util/Date;
    .end local v7    # "buffer":[B
    .end local v8    # "buf":[B
    .end local v9    # "cGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPCompressedDataGenerator;
    .end local v10    # "fileSize":J
    .end local v13    # "it":Ljava/util/Iterator;
    .end local v14    # "lOut":Ljava/io/OutputStream;
    .end local v15    # "len":I
    .end local v18    # "pgpPrivKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .end local v20    # "progress":D
    .end local v22    # "sGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureGenerator;
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mThread:Lcom/android/emailcommon/pgp/PGPThread;

    invoke-virtual {v4}, Lcom/android/emailcommon/pgp/PGPThread;->isCancel()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 468
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    const/16 v5, 0xd

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-interface {v4, v5, v0, v1}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    goto/16 :goto_3

    .line 470
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/pgp/PGPEncryptSign;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    const/4 v5, 0x1

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-interface {v4, v5, v0, v1}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    goto/16 :goto_3
.end method
