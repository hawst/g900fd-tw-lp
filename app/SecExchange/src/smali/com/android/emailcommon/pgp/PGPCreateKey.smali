.class public Lcom/android/emailcommon/pgp/PGPCreateKey;
.super Lcom/android/emailcommon/pgp/PGPOperation;
.source "PGPCreateKey.java"


# static fields
.field private static KEY_REV_FILEPATH:Ljava/lang/String;

.field private static NO_MINUTES_IN_DAY:I

.field private static final PREFERRED_COMPRESSION_ALGORITHMS:[I

.field private static final PREFERRED_HASH_ALGORITHMS:[I

.field private static final PREFERRED_SYMMETRIC_ALGORITHMS:[I


# instance fields
.field private mAlgChoice:I

.field private mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

.field private mExpiryDate:Ljava/util/GregorianCalendar;

.field private mIsGencertificate:Z

.field private mIsMasterkey:Z

.field private mKeyLength:I

.field private mKeypair:Lcom/android/emailcommon/pgp/PGPKey$PGPKeyPairParams;

.field private mMasterkeyPair:Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

.field private mPassphrase:Ljava/lang/String;

.field private mSubkeyPair:Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

.field private mUserId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/openpgp/rc/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->KEY_REV_FILEPATH:Ljava/lang/String;

    .line 62
    const v0, 0x15180

    sput v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->NO_MINUTES_IN_DAY:I

    .line 64
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->PREFERRED_SYMMETRIC_ALGORITHMS:[I

    .line 69
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->PREFERRED_HASH_ALGORITHMS:[I

    .line 73
    new-array v0, v2, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->PREFERRED_COMPRESSION_ALGORITHMS:[I

    return-void

    .line 64
    :array_0
    .array-data 4
        0x9
        0x2
        0x3
    .end array-data

    .line 69
    :array_1
    .array-data 4
        0x2
        0x8
    .end array-data

    .line 73
    :array_2
    .array-data 4
        0x1
        0x2
    .end array-data
.end method

.method private createRevStoragelocation()V
    .locals 5

    .prologue
    .line 223
    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/openpgp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 224
    .local v0, "pgpfolder":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    sget-object v3, Lcom/android/emailcommon/pgp/PGPCreateKey;->KEY_REV_FILEPATH:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 226
    .local v1, "revFile":Ljava/io/File;
    const/4 v2, 0x0

    .line 227
    .local v2, "success":Z
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 228
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v2

    .line 230
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 231
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v2

    .line 233
    :cond_1
    return-void
.end method

.method private generateMKeyPair(ILjava/security/KeyPairGenerator;)V
    .locals 36
    .param p1, "algorithm"    # I
    .param p2, "keyGen"    # Ljava/security/KeyPairGenerator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;,
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/SignatureException;,
            Lcom/android/emailcommon/pgp/PGPKeyException;
        }
    .end annotation

    .prologue
    .line 272
    const/4 v6, 0x0

    .line 273
    .local v6, "keypair":Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    const/4 v4, 0x0

    .line 274
    .local v4, "seckey":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    const/16 v32, 0x0

    .line 276
    .local v32, "revkey":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    const-wide/16 v26, 0x0

    .line 277
    .local v26, "numDays":J
    new-instance v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

    .end local v6    # "keypair":Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    invoke-virtual/range {p2 .. p2}, Ljava/security/KeyPairGenerator;->generateKeyPair()Ljava/security/KeyPair;

    move-result-object v5

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9}, Ljava/util/Date;-><init>()V

    move/from16 v0, p1

    invoke-direct {v6, v0, v5, v9}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;-><init>(ILjava/security/KeyPair;Ljava/util/Date;)V

    .line 279
    .restart local v6    # "keypair":Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    if-eqz v6, :cond_0

    .line 281
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    .end local v4    # "seckey":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    const/16 v5, 0x10

    const-string v7, ""

    const/4 v8, 0x3

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mPassphrase:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    new-instance v13, Ljava/security/SecureRandom;

    invoke-direct {v13}, Ljava/security/SecureRandom;-><init>()V

    new-instance v14, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v14}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-direct/range {v4 .. v14}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;-><init>(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;I[CZLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/security/Provider;)V

    .line 284
    .restart local v4    # "seckey":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    move-object/from16 v32, v4

    .line 289
    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v34

    .line 290
    .local v34, "temppubkey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    new-instance v25, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    invoke-virtual/range {v34 .. v34}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getAlgorithm()I

    move-result v5

    new-instance v9, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v9}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    move-object/from16 v0, v34

    invoke-virtual {v0, v9}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKey(Ljava/security/Provider;)Ljava/security/PublicKey;

    move-result-object v9

    invoke-virtual/range {v34 .. v34}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getCreationTime()Ljava/util/Date;

    move-result-object v10

    move-object/from16 v0, v25

    invoke-direct {v0, v5, v9, v10}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;-><init>(ILjava/security/PublicKey;Ljava/util/Date;)V

    .line 297
    .local v25, "masterpubkey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mPassphrase:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    new-instance v9, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v9}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-virtual {v4, v5, v9}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->extractPrivateKey([CLjava/security/Provider;)Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;

    move-result-object v24

    .line 300
    .local v24, "masterPrivateKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    if-nez v24, :cond_1

    .line 376
    .end local v24    # "masterPrivateKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .end local v25    # "masterpubkey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .end local v34    # "temppubkey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :cond_0
    :goto_0
    return-void

    .line 303
    .restart local v24    # "masterPrivateKey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;
    .restart local v25    # "masterpubkey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    .restart local v34    # "temppubkey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    :cond_1
    new-instance v5, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-direct {v5, v0, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPPrivateKey;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mMasterkeyPair:Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

    .line 309
    new-instance v21, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;

    invoke-direct/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;-><init>()V

    .line 310
    .local v21, "hashedPacketsGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;
    new-instance v35, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;

    invoke-direct/range {v35 .. v35}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;-><init>()V

    .line 311
    .local v35, "unhashedPacketsGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;
    const/16 v22, 0x3

    .line 312
    .local v22, "keyFlags":I
    const/4 v5, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v0, v5, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->setKeyFlags(ZI)V

    .line 315
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mExpiryDate:Ljava/util/GregorianCalendar;

    if-eqz v5, :cond_2

    .line 316
    new-instance v18, Ljava/util/GregorianCalendar;

    invoke-direct/range {v18 .. v18}, Ljava/util/GregorianCalendar;-><init>()V

    .line 317
    .local v18, "creationDate":Ljava/util/GregorianCalendar;
    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getCreationTime()Ljava/util/Date;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    .line 318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mExpiryDate:Ljava/util/GregorianCalendar;

    move-object/from16 v19, v0

    .line 319
    .local v19, "expiryDate":Ljava/util/GregorianCalendar;
    invoke-static/range {v18 .. v19}, Lcom/android/emailcommon/pgp/PGPCreateKey;->getNumDaysBetween(Ljava/util/GregorianCalendar;Ljava/util/GregorianCalendar;)J

    move-result-wide v26

    .line 320
    const/4 v5, 0x1

    sget v9, Lcom/android/emailcommon/pgp/PGPCreateKey;->NO_MINUTES_IN_DAY:I

    int-to-long v10, v9

    mul-long v10, v10, v26

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v10, v11}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->setKeyExpirationTime(ZJ)V

    .line 323
    .end local v18    # "creationDate":Ljava/util/GregorianCalendar;
    .end local v19    # "expiryDate":Ljava/util/GregorianCalendar;
    :cond_2
    const/4 v5, 0x1

    sget-object v9, Lcom/android/emailcommon/pgp/PGPCreateKey;->PREFERRED_SYMMETRIC_ALGORITHMS:[I

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v9}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->setPreferredSymmetricAlgorithms(Z[I)V

    .line 324
    const/4 v5, 0x1

    sget-object v9, Lcom/android/emailcommon/pgp/PGPCreateKey;->PREFERRED_COMPRESSION_ALGORITHMS:[I

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v9}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->setPreferredCompressionAlgorithms(Z[I)V

    .line 326
    const/4 v5, 0x1

    sget-object v9, Lcom/android/emailcommon/pgp/PGPCreateKey;->PREFERRED_HASH_ALGORITHMS:[I

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v9}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->setPreferredHashAlgorithms(Z[I)V

    .line 328
    new-instance v7, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;

    const/16 v8, 0x13

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mMasterkeyPair:Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mUserId:Ljava/lang/String;

    const/4 v11, 0x3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mPassphrase:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toCharArray()[C

    move-result-object v12

    const/4 v13, 0x1

    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    move-result-object v14

    invoke-virtual/range {v35 .. v35}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    move-result-object v15

    new-instance v16, Ljava/security/SecureRandom;

    invoke-direct/range {v16 .. v16}, Ljava/security/SecureRandom;-><init>()V

    new-instance v17, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct/range {v17 .. v17}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-direct/range {v7 .. v17}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;-><init>(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;I[CZLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/security/Provider;)V

    .line 335
    .local v7, "keyringGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;
    invoke-direct/range {p0 .. p2}, Lcom/android/emailcommon/pgp/PGPCreateKey;->generateSubKeyPair(ILjava/security/KeyPairGenerator;)Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mSubkeyPair:Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

    .line 338
    new-instance v21, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;

    .end local v21    # "hashedPacketsGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;
    invoke-direct/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;-><init>()V

    .line 339
    .restart local v21    # "hashedPacketsGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;
    new-instance v35, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;

    .end local v35    # "unhashedPacketsGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;
    invoke-direct/range {v35 .. v35}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;-><init>()V

    .line 341
    .restart local v35    # "unhashedPacketsGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;
    const/16 v22, 0x0

    .line 342
    const/16 v22, 0xc

    .line 343
    const/4 v5, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v0, v5, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->setKeyFlags(ZI)V

    .line 344
    const/4 v5, 0x1

    sget v9, Lcom/android/emailcommon/pgp/PGPCreateKey;->NO_MINUTES_IN_DAY:I

    int-to-long v10, v9

    mul-long v10, v10, v26

    move-object/from16 v0, v21

    invoke-virtual {v0, v5, v10, v11}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->setKeyExpirationTime(ZJ)V

    .line 345
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mSubkeyPair:Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

    invoke-virtual/range {v21 .. v21}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    move-result-object v9

    invoke-virtual/range {v35 .. v35}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    move-result-object v10

    invoke-virtual {v7, v5, v9, v10}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->addSubKey(Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;)V

    .line 348
    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->generateSecretKeyRing()Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    move-result-object v33

    .line 349
    .local v33, "secretKeyRing":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    invoke-virtual {v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->generatePublicKeyRing()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

    move-result-object v28

    .line 353
    .local v28, "publicKeyRing":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    const/16 v23, 0x0

    .line 354
    .local v23, "keyparams":Lcom/android/emailcommon/pgp/PGPKey$PGPKeyPairParams;
    new-instance v30, Landroid/os/Bundle;

    invoke-direct/range {v30 .. v30}, Landroid/os/Bundle;-><init>()V

    .line 355
    .local v30, "returnData":Landroid/os/Bundle;
    const/16 v31, 0x2

    .line 357
    .local v31, "returnvalue":I
    new-instance v23, Lcom/android/emailcommon/pgp/PGPKey$PGPKeyPairParams;

    .end local v23    # "keyparams":Lcom/android/emailcommon/pgp/PGPKey$PGPKeyPairParams;
    move-object/from16 v0, v23

    move-object/from16 v1, v28

    move-object/from16 v2, v33

    invoke-direct {v0, v1, v2}, Lcom/android/emailcommon/pgp/PGPKey$PGPKeyPairParams;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;)V

    .line 359
    .restart local v23    # "keyparams":Lcom/android/emailcommon/pgp/PGPKey$PGPKeyPairParams;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mMasterkeyPair:Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/emailcommon/pgp/PGPCreateKey;->generatefilenameFromkeyPair(Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;)Ljava/lang/String;

    move-result-object v20

    .line 360
    .local v20, "filename":Ljava/lang/String;
    new-instance v13, Ljava/io/FileOutputStream;

    move-object/from16 v0, v20

    invoke-direct {v13, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 361
    .local v13, "outstream":Ljava/io/OutputStream;
    new-instance v8, Lcom/android/emailcommon/pgp/PGPRCertificate;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mPassphrase:Ljava/lang/String;

    invoke-virtual/range {v32 .. v32}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getKeyID()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    invoke-direct/range {v8 .. v13}, Lcom/android/emailcommon/pgp/PGPRCertificate;-><init>(Ljava/lang/String;JLcom/android/emailcommon/pgp/PGPDatabase;Ljava/io/OutputStream;)V

    .line 363
    .local v8, "revcert":Lcom/android/emailcommon/pgp/PGPRCertificate;
    invoke-virtual/range {v32 .. v32}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v5

    move-object/from16 v0, v32

    invoke-virtual {v8, v5, v0}, Lcom/android/emailcommon/pgp/PGPRCertificate;->generaterevcert(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;)V

    .line 365
    new-instance v29, Lcom/android/emailcommon/pgp/PGPOperationResultData;

    invoke-direct/range {v29 .. v29}, Lcom/android/emailcommon/pgp/PGPOperationResultData;-><init>()V

    .line 366
    .local v29, "res":Lcom/android/emailcommon/pgp/PGPOperationResultData;
    const/4 v5, 0x1

    move-object/from16 v0, v29

    iput v5, v0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mPgpOperation:I

    .line 368
    const-string v5, "errorvalue"

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 369
    const-string v5, "Status"

    const/4 v9, 0x1

    move-object/from16 v0, v30

    invoke-virtual {v0, v5, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 371
    move-object/from16 v0, v30

    move-object/from16 v1, v29

    iput-object v0, v1, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mMsg:Landroid/os/Bundle;

    .line 372
    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/emailcommon/pgp/PGPCreateKey;->mKeypair:Lcom/android/emailcommon/pgp/PGPKey$PGPKeyPairParams;

    .line 373
    move-object/from16 v0, v23

    move-object/from16 v1, v29

    iput-object v0, v1, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mKeypair:Ljava/lang/Object;

    .line 374
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    const/4 v9, 0x0

    move/from16 v0, v31

    move-object/from16 v1, v29

    invoke-interface {v5, v0, v9, v1}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    goto/16 :goto_0
.end method

.method private generateSubKeyPair(ILjava/security/KeyPairGenerator;)Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    .locals 3
    .param p1, "algorithm"    # I
    .param p2, "keyGen"    # Ljava/security/KeyPairGenerator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 380
    const/4 v0, 0x0

    .line 381
    .local v0, "keypair":Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    new-instance v0, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

    .end local v0    # "keypair":Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    invoke-virtual {p2}, Ljava/security/KeyPairGenerator;->generateKeyPair()Ljava/security/KeyPair;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-direct {v0, p1, v1, v2}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;-><init>(ILjava/security/KeyPair;Ljava/util/Date;)V

    .line 382
    .restart local v0    # "keypair":Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    return-object v0
.end method

.method private generatefilenameFromkeyPair(Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;)Ljava/lang/String;
    .locals 10
    .param p1, "keypair"    # Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v9, 0x20

    .line 386
    invoke-virtual {p1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v3

    .line 387
    .local v3, "pubkey":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;
    const/4 v6, 0x0

    .line 388
    .local v6, "tempfile":Ljava/lang/String;
    iget-object v7, p0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mUserId:Ljava/lang/String;

    const/16 v8, 0x3c

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v6

    .line 389
    const/16 v7, 0x3e

    invoke-virtual {v6, v7, v9}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v6

    .line 390
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 392
    invoke-virtual {v3}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getKeyID()J

    move-result-wide v4

    .line 394
    .local v4, "keyId":J
    sget-object v1, Lcom/android/emailcommon/pgp/PGPCreateKey;->KEY_REV_FILEPATH:Ljava/lang/String;

    .line 395
    .local v1, "filename":Ljava/lang/String;
    invoke-virtual {p0, v4, v5}, Lcom/android/emailcommon/pgp/PGPCreateKey;->getSmallFingerPrint(J)Ljava/lang/String;

    move-result-object v2

    .line 397
    .local v2, "fingerprint":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "0x"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " rev"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".asc"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 399
    invoke-virtual {v1, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 401
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 402
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 403
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 405
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 406
    return-object v1
.end method

.method private static getNumDaysBetween(Ljava/util/GregorianCalendar;Ljava/util/GregorianCalendar;)J
    .locals 9
    .param p0, "first"    # Ljava/util/GregorianCalendar;
    .param p1, "second"    # Ljava/util/GregorianCalendar;

    .prologue
    const/4 v8, 0x5

    .line 418
    new-instance v2, Ljava/util/GregorianCalendar;

    invoke-direct {v2}, Ljava/util/GregorianCalendar;-><init>()V

    .line 419
    .local v2, "tmp":Ljava/util/GregorianCalendar;
    invoke-virtual {p0}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    .line 421
    invoke-virtual {p1}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {p0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    const-wide/32 v6, 0x15180

    div-long v0, v4, v6

    .line 422
    .local v0, "numDays":J
    long-to-int v3, v0

    invoke-virtual {v2, v8, v3}, Ljava/util/GregorianCalendar;->add(II)V

    .line 424
    :goto_0
    invoke-virtual {v2, p1}, Ljava/util/GregorianCalendar;->before(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 425
    const/4 v3, 0x1

    invoke-virtual {v2, v8, v3}, Ljava/util/GregorianCalendar;->add(II)V

    .line 426
    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    goto :goto_0

    .line 428
    :cond_0
    return-wide v0
.end method


# virtual methods
.method public createKeys()V
    .locals 33
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;,
            Ljava/io/IOException;,
            Ljava/security/SignatureException;,
            Lcom/android/emailcommon/pgp/PGPKeyException;
        }
    .end annotation

    .prologue
    .line 129
    new-instance v5, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v5}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-static {v5}, Ljava/security/Security;->addProvider(Ljava/security/Provider;)I

    .line 130
    const/4 v15, 0x0

    .line 131
    .local v15, "algorithm":I
    const/16 v22, 0x0

    .line 132
    .local v22, "keyGen":Ljava/security/KeyPairGenerator;
    const/16 v29, 0x2

    .line 133
    .local v29, "returnvalue":I
    new-instance v28, Landroid/os/Bundle;

    invoke-direct/range {v28 .. v28}, Landroid/os/Bundle;-><init>()V

    .line 134
    .local v28, "returnData":Landroid/os/Bundle;
    const/16 v23, 0x0

    .line 135
    .local v23, "keyparams":Lcom/android/emailcommon/pgp/PGPKey$PGPKeyPairParams;
    const/16 v17, 0x0

    .line 137
    .local v17, "ex":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mAlgChoice:I

    packed-switch v5, :pswitch_data_0

    .line 154
    :goto_0
    if-nez v22, :cond_0

    .line 155
    new-instance v5, Lcom/android/emailcommon/pgp/PGPKeyException;

    const-string v7, "keygen is null"

    invoke-direct {v5, v7}, Lcom/android/emailcommon/pgp/PGPKeyException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 139
    :pswitch_0
    const-string v5, "DSA"

    new-instance v7, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v7}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-static {v5, v7}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyPairGenerator;

    move-result-object v22

    .line 140
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mKeyLength:I

    new-instance v7, Ljava/security/SecureRandom;

    invoke-direct {v7}, Ljava/security/SecureRandom;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v5, v7}, Ljava/security/KeyPairGenerator;->initialize(ILjava/security/SecureRandom;)V

    .line 141
    const/16 v15, 0x11

    .line 142
    goto :goto_0

    .line 145
    :pswitch_1
    const-string v5, "RSA"

    new-instance v7, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v7}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-static {v5, v7}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyPairGenerator;

    move-result-object v22

    .line 146
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mKeyLength:I

    new-instance v7, Ljava/security/SecureRandom;

    invoke-direct {v7}, Ljava/security/SecureRandom;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v5, v7}, Ljava/security/KeyPairGenerator;->initialize(ILjava/security/SecureRandom;)V

    .line 147
    const/4 v15, 0x1

    .line 148
    goto :goto_0

    .line 157
    :cond_0
    new-instance v6, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;

    invoke-virtual/range {v22 .. v22}, Ljava/security/KeyPairGenerator;->generateKeyPair()Ljava/security/KeyPair;

    move-result-object v5

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    invoke-direct {v6, v15, v5, v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;-><init>(ILjava/security/KeyPair;Ljava/util/Date;)V

    .line 163
    .local v6, "keyPair":Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;
    new-instance v20, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;

    invoke-direct/range {v20 .. v20}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;-><init>()V

    .line 164
    .local v20, "hashedPacketsGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;
    new-instance v32, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;

    invoke-direct/range {v32 .. v32}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;-><init>()V

    .line 166
    .local v32, "unhashedPacketsGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;
    const/16 v21, 0xf

    .line 169
    .local v21, "keyFlags":I
    const/4 v5, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v5, v1}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->setKeyFlags(ZI)V

    .line 170
    const/4 v5, 0x1

    sget-object v7, Lcom/android/emailcommon/pgp/PGPCreateKey;->PREFERRED_SYMMETRIC_ALGORITHMS:[I

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->setPreferredSymmetricAlgorithms(Z[I)V

    .line 171
    const/4 v5, 0x1

    sget-object v7, Lcom/android/emailcommon/pgp/PGPCreateKey;->PREFERRED_COMPRESSION_ALGORITHMS:[I

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->setPreferredCompressionAlgorithms(Z[I)V

    .line 172
    const/4 v5, 0x1

    sget-object v7, Lcom/android/emailcommon/pgp/PGPCreateKey;->PREFERRED_HASH_ALGORITHMS:[I

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v7}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->setPreferredHashAlgorithms(Z[I)V

    .line 174
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mExpiryDate:Ljava/util/GregorianCalendar;

    if-eqz v5, :cond_1

    .line 175
    new-instance v16, Ljava/util/GregorianCalendar;

    invoke-direct/range {v16 .. v16}, Ljava/util/GregorianCalendar;-><init>()V

    .line 176
    .local v16, "creationDate":Ljava/util/GregorianCalendar;
    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;->getCreationTime()Ljava/util/Date;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    .line 177
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mExpiryDate:Ljava/util/GregorianCalendar;

    move-object/from16 v18, v0

    .line 178
    .local v18, "expiryDate":Ljava/util/GregorianCalendar;
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/android/emailcommon/pgp/PGPCreateKey;->getNumDaysBetween(Ljava/util/GregorianCalendar;Ljava/util/GregorianCalendar;)J

    move-result-wide v24

    .line 179
    .local v24, "numDays":J
    const/4 v5, 0x1

    sget v7, Lcom/android/emailcommon/pgp/PGPCreateKey;->NO_MINUTES_IN_DAY:I

    int-to-long v10, v7

    mul-long v10, v10, v24

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v10, v11}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->setKeyExpirationTime(ZJ)V

    .line 186
    .end local v16    # "creationDate":Ljava/util/GregorianCalendar;
    .end local v18    # "expiryDate":Ljava/util/GregorianCalendar;
    .end local v24    # "numDays":J
    :cond_1
    new-instance v4, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;

    const/16 v5, 0x13

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mUserId:Ljava/lang/String;

    const/4 v8, 0x3

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mPassphrase:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->toCharArray()[C

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual/range {v20 .. v20}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    move-result-object v11

    invoke-virtual/range {v32 .. v32}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketGenerator;->generate()Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;

    move-result-object v12

    new-instance v13, Ljava/security/SecureRandom;

    invoke-direct {v13}, Ljava/security/SecureRandom;-><init>()V

    new-instance v14, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v14}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-direct/range {v4 .. v14}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;-><init>(ILcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;Ljava/lang/String;I[CZLcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Lcom/android/sec/org/bouncycastle/openpgp/PGPSignatureSubpacketVector;Ljava/security/SecureRandom;Ljava/security/Provider;)V

    .line 192
    .local v4, "keyRingGen":Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;
    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->generateSecretKeyRing()Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;

    move-result-object v30

    .line 193
    .local v30, "secKeyRing":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;
    invoke-virtual {v4}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyRingGenerator;->generatePublicKeyRing()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;

    move-result-object v26

    .line 197
    .local v26, "pubKeyRing":Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;
    new-instance v23, Lcom/android/emailcommon/pgp/PGPKey$PGPKeyPairParams;

    .end local v23    # "keyparams":Lcom/android/emailcommon/pgp/PGPKey$PGPKeyPairParams;
    move-object/from16 v0, v23

    move-object/from16 v1, v26

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/android/emailcommon/pgp/PGPKey$PGPKeyPairParams;-><init>(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKeyRing;Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;)V

    .line 199
    .restart local v23    # "keyparams":Lcom/android/emailcommon/pgp/PGPKey$PGPKeyPairParams;
    invoke-virtual/range {v30 .. v30}, Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKeyRing;->getSecretKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;

    move-result-object v31

    .line 201
    .local v31, "seckey":Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mIsGencertificate:Z

    if-eqz v5, :cond_2

    .line 202
    invoke-direct/range {p0 .. p0}, Lcom/android/emailcommon/pgp/PGPCreateKey;->createRevStoragelocation()V

    .line 203
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/android/emailcommon/pgp/PGPCreateKey;->generatefilenameFromkeyPair(Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;)Ljava/lang/String;

    move-result-object v19

    .line 204
    .local v19, "filename":Ljava/lang/String;
    new-instance v13, Ljava/io/FileOutputStream;

    move-object/from16 v0, v19

    invoke-direct {v13, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 205
    .local v13, "outstream":Ljava/io/OutputStream;
    new-instance v8, Lcom/android/emailcommon/pgp/PGPRCertificate;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mPassphrase:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->getKeyID()J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mDatabase:Lcom/android/emailcommon/pgp/PGPDatabase;

    invoke-direct/range {v8 .. v13}, Lcom/android/emailcommon/pgp/PGPRCertificate;-><init>(Ljava/lang/String;JLcom/android/emailcommon/pgp/PGPDatabase;Ljava/io/OutputStream;)V

    .line 207
    .local v8, "revcert":Lcom/android/emailcommon/pgp/PGPRCertificate;
    invoke-virtual {v6}, Lcom/android/sec/org/bouncycastle/openpgp/PGPKeyPair;->getPublicKey()Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-virtual {v8, v5, v0}, Lcom/android/emailcommon/pgp/PGPRCertificate;->generaterevcert(Lcom/android/sec/org/bouncycastle/openpgp/PGPPublicKey;Lcom/android/sec/org/bouncycastle/openpgp/PGPSecretKey;)V

    .line 210
    .end local v8    # "revcert":Lcom/android/emailcommon/pgp/PGPRCertificate;
    .end local v13    # "outstream":Ljava/io/OutputStream;
    .end local v19    # "filename":Ljava/lang/String;
    :cond_2
    new-instance v27, Lcom/android/emailcommon/pgp/PGPOperationResultData;

    invoke-direct/range {v27 .. v27}, Lcom/android/emailcommon/pgp/PGPOperationResultData;-><init>()V

    .line 211
    .local v27, "res":Lcom/android/emailcommon/pgp/PGPOperationResultData;
    const/4 v5, 0x1

    move-object/from16 v0, v27

    iput v5, v0, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mPgpOperation:I

    .line 213
    const-string v5, "errorvalue"

    move-object/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v0, v5, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 214
    const-string v5, "Status"

    const/4 v7, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 216
    move-object/from16 v0, v28

    move-object/from16 v1, v27

    iput-object v0, v1, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mMsg:Landroid/os/Bundle;

    .line 217
    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/emailcommon/pgp/PGPCreateKey;->mKeypair:Lcom/android/emailcommon/pgp/PGPKey$PGPKeyPairParams;

    .line 218
    move-object/from16 v0, v23

    move-object/from16 v1, v27

    iput-object v0, v1, Lcom/android/emailcommon/pgp/PGPOperationResultData;->mKeypair:Ljava/lang/Object;

    .line 219
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    move/from16 v0, v29

    move-object/from16 v1, v17

    move-object/from16 v2, v27

    invoke-interface {v5, v0, v1, v2}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    .line 220
    return-void

    .line 137
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public createMasterSubKeys()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Lcom/android/sec/org/bouncycastle/openpgp/PGPException;,
            Ljava/security/NoSuchProviderException;,
            Ljava/io/IOException;,
            Ljava/security/SignatureException;,
            Lcom/android/emailcommon/pgp/PGPKeyException;
        }
    .end annotation

    .prologue
    .line 240
    new-instance v2, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v2}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-static {v2}, Ljava/security/Security;->addProvider(Ljava/security/Provider;)I

    .line 242
    const/4 v0, 0x0

    .line 243
    .local v0, "algorithm":I
    const/4 v1, 0x0

    .line 245
    .local v1, "keyGen":Ljava/security/KeyPairGenerator;
    iget v2, p0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mAlgChoice:I

    packed-switch v2, :pswitch_data_0

    .line 262
    :goto_0
    if-nez v1, :cond_0

    .line 263
    new-instance v2, Lcom/android/emailcommon/pgp/PGPKeyException;

    const-string v3, "keyGen is null"

    invoke-direct {v2, v3}, Lcom/android/emailcommon/pgp/PGPKeyException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 247
    :pswitch_0
    const-string v2, "DSA"

    new-instance v3, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v3}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-static {v2, v3}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyPairGenerator;

    move-result-object v1

    .line 248
    iget v2, p0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mKeyLength:I

    new-instance v3, Ljava/security/SecureRandom;

    invoke-direct {v3}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v1, v2, v3}, Ljava/security/KeyPairGenerator;->initialize(ILjava/security/SecureRandom;)V

    .line 249
    const/16 v0, 0x11

    .line 250
    goto :goto_0

    .line 253
    :pswitch_1
    const-string v2, "RSA"

    new-instance v3, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v3}, Lcom/android/sec/org/bouncycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-static {v2, v3}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/KeyPairGenerator;

    move-result-object v1

    .line 254
    iget v2, p0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mKeyLength:I

    new-instance v3, Ljava/security/SecureRandom;

    invoke-direct {v3}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v1, v2, v3}, Ljava/security/KeyPairGenerator;->initialize(ILjava/security/SecureRandom;)V

    .line 255
    const/4 v0, 0x1

    .line 256
    goto :goto_0

    .line 265
    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/android/emailcommon/pgp/PGPCreateKey;->generateMKeyPair(ILjava/security/KeyPairGenerator;)V

    .line 266
    return-void

    .line 245
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getSmallFingerPrint(J)Ljava/lang/String;
    .locals 5
    .param p1, "keyId"    # J

    .prologue
    .line 410
    const-wide v2, 0xffffffffL

    and-long/2addr v2, p1

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 411
    .local v0, "fingerPrint":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x8

    if-ge v1, v2, :cond_0

    .line 412
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 414
    :cond_0
    return-object v0
.end method

.method public run()V
    .locals 5

    .prologue
    .line 90
    const/4 v1, 0x0

    .line 93
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_0
    iget-boolean v2, p0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mIsMasterkey:Z

    if-eqz v2, :cond_1

    .line 94
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPCreateKey;->createMasterSubKeys()V
    :try_end_0
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/android/emailcommon/pgp/PGPKeyException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_0 .. :try_end_0} :catch_5

    .line 118
    :goto_0
    if-eqz v1, :cond_0

    .line 119
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 120
    iget-object v2, p0, Lcom/android/emailcommon/pgp/PGPCreateKey;->mUpdater:Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v1, v4}, Lcom/android/emailcommon/pgp/PGPManager$PGPUpdater;->onResult(ILjava/lang/Exception;Lcom/android/emailcommon/pgp/PGPOperationResultData;)V

    .line 122
    :cond_0
    return-void

    .line 96
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/android/emailcommon/pgp/PGPCreateKey;->createKeys()V
    :try_end_1
    .catch Ljava/security/SignatureException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/android/emailcommon/pgp/PGPKeyException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/NoSuchProviderException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_1 .. :try_end_1} :catch_5

    goto :goto_0

    .line 98
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Ljava/security/SignatureException;
    move-object v1, v0

    .line 100
    :try_start_2
    invoke-virtual {v0}, Ljava/security/SignatureException;->printStackTrace()V
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/security/NoSuchProviderException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_2 .. :try_end_2} :catch_5

    goto :goto_0

    .line 108
    .end local v0    # "e":Ljava/security/SignatureException;
    :catch_1
    move-exception v0

    .line 109
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    move-object v1, v0

    .line 110
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 101
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_2
    move-exception v0

    .line 102
    .local v0, "e":Ljava/io/IOException;
    move-object v1, v0

    .line 103
    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/security/NoSuchProviderException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_3 .. :try_end_3} :catch_5

    goto :goto_0

    .line 111
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 112
    .local v0, "e":Ljava/security/NoSuchProviderException;
    move-object v1, v0

    .line 113
    invoke-virtual {v0}, Ljava/security/NoSuchProviderException;->printStackTrace()V

    goto :goto_0

    .line 104
    .end local v0    # "e":Ljava/security/NoSuchProviderException;
    :catch_4
    move-exception v0

    .line 105
    .local v0, "e":Lcom/android/emailcommon/pgp/PGPKeyException;
    move-object v1, v0

    .line 106
    :try_start_4
    invoke-virtual {v0}, Lcom/android/emailcommon/pgp/PGPKeyException;->printStackTrace()V
    :try_end_4
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/security/NoSuchProviderException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/android/sec/org/bouncycastle/openpgp/PGPException; {:try_start_4 .. :try_end_4} :catch_5

    goto :goto_0

    .line 114
    .end local v0    # "e":Lcom/android/emailcommon/pgp/PGPKeyException;
    :catch_5
    move-exception v0

    .line 115
    .local v0, "e":Lcom/android/sec/org/bouncycastle/openpgp/PGPException;
    move-object v1, v0

    .line 116
    invoke-virtual {v0}, Lcom/android/sec/org/bouncycastle/openpgp/PGPException;->printStackTrace()V

    goto :goto_0
.end method
