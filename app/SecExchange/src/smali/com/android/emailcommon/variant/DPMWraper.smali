.class public Lcom/android/emailcommon/variant/DPMWraper;
.super Ljava/lang/Object;
.source "DPMWraper.java"


# static fields
.field private static sInstance:Lcom/android/emailcommon/variant/DPMWraper;


# instance fields
.field private mDEM:Landroid/dirEncryption/DirEncryptionManager;

.field private mDPM:Landroid/app/admin/DevicePolicyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    sput-object v0, Lcom/android/emailcommon/variant/DPMWraper;->sInstance:Lcom/android/emailcommon/variant/DPMWraper;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object v0, p0, Lcom/android/emailcommon/variant/DPMWraper;->mDPM:Landroid/app/admin/DevicePolicyManager;

    .line 39
    iput-object v0, p0, Lcom/android/emailcommon/variant/DPMWraper;->mDEM:Landroid/dirEncryption/DirEncryptionManager;

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "device_policy"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    iput-object v0, p0, Lcom/android/emailcommon/variant/DPMWraper;->mDPM:Landroid/app/admin/DevicePolicyManager;

    .line 45
    new-instance v0, Landroid/dirEncryption/DirEncryptionManager;

    invoke-direct {v0, p1}, Landroid/dirEncryption/DirEncryptionManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/emailcommon/variant/DPMWraper;->mDEM:Landroid/dirEncryption/DirEncryptionManager;

    .line 47
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/emailcommon/variant/DPMWraper;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    sget-object v0, Lcom/android/emailcommon/variant/DPMWraper;->sInstance:Lcom/android/emailcommon/variant/DPMWraper;

    if-nez v0, :cond_0

    .line 51
    new-instance v0, Lcom/android/emailcommon/variant/DPMWraper;

    invoke-direct {v0, p0}, Lcom/android/emailcommon/variant/DPMWraper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/emailcommon/variant/DPMWraper;->sInstance:Lcom/android/emailcommon/variant/DPMWraper;

    .line 53
    :cond_0
    sget-object v0, Lcom/android/emailcommon/variant/DPMWraper;->sInstance:Lcom/android/emailcommon/variant/DPMWraper;

    return-object v0
.end method


# virtual methods
.method public getRecoveryPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/android/emailcommon/variant/DPMWraper;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getRecoveryPassword()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
