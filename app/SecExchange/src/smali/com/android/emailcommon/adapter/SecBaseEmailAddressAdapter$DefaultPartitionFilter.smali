.class final Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;
.super Landroid/widget/Filter;
.source "SecBaseEmailAddressAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DefaultPartitionFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;


# virtual methods
.method public convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "resultValue"    # Ljava/lang/Object;

    .prologue
    .line 523
    iget-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    check-cast p1, Landroid/database/Cursor;

    .end local p1    # "resultValue":Ljava/lang/Object;
    # invokes: Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->makeDisplayString(Landroid/database/Cursor;)Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->access$600(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 24
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 418
    const/4 v12, 0x0

    .line 419
    .local v12, "directoryCursor":Landroid/database/Cursor;
    const/16 v20, 0x0

    .line 420
    .local v20, "recentCursor":Landroid/database/Cursor;
    const/16 v21, 0x0

    .line 421
    .local v21, "reducedRICCursor":Landroid/database/MatrixCursor;
    const/16 v22, 0x0

    .line 423
    .local v22, "reducedRecentEmailCursor":Landroid/database/MatrixCursor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    # getter for: Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mDirectoriesLoaded:Z
    invoke-static {v2}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->access$000(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 424
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    iget-object v2, v2, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v3, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryListQuery;->URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryListQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 426
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    const/4 v4, 0x1

    # setter for: Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mDirectoriesLoaded:Z
    invoke-static {v2, v4}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->access$002(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;Z)Z

    .line 428
    :cond_0
    const/16 v18, 0x0

    .line 429
    .local v18, "mergeResultCursor":Landroid/database/MergeCursor;
    new-instance v23, Landroid/widget/Filter$FilterResults;

    invoke-direct/range {v23 .. v23}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 430
    .local v23, "results":Landroid/widget/Filter$FilterResults;
    const/4 v11, 0x0

    .line 431
    .local v11, "cursor":Landroid/database/Cursor;
    const/16 v17, 0x0

    .line 432
    .local v17, "groupCursor":Landroid/database/Cursor;
    const/16 v19, 0x0

    .line 433
    .local v19, "rICursor1":Landroid/database/Cursor;
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 434
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mInputText:Ljava/lang/String;
    invoke-static {v2, v4}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->access$102(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;Ljava/lang/String;)Ljava/lang/String;

    .line 435
    const/4 v3, 0x0

    .line 436
    .local v3, "uri":Landroid/net/Uri;
    const/4 v10, 0x0

    .line 437
    .local v10, "builder":Landroid/net/Uri$Builder;
    const-string v15, ""

    .line 439
    .local v15, "endoceInputText":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    # getter for: Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mInputText:Ljava/lang/String;
    invoke-static {v2}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->access$100(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "utf-8"

    invoke-static {v2, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v15

    .line 444
    :goto_0
    sget-boolean v2, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->ExpandResultMaxAndShowMoreMode:Z

    if-eqz v2, :cond_5

    .line 445
    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v15}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v10

    .line 453
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    # getter for: Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mAccount:Landroid/accounts/Account;
    invoke-static {v2}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->access$300(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;)Landroid/accounts/Account;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 454
    const-string v2, "name_for_primary_account"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    # getter for: Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mAccount:Landroid/accounts/Account;
    invoke-static {v4}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->access$300(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;)Landroid/accounts/Account;

    move-result-object v4

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v10, v2, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 455
    const-string v2, "type_for_primary_account"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    # getter for: Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mAccount:Landroid/accounts/Account;
    invoke-static {v4}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->access$300(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;)Landroid/accounts/Account;

    move-result-object v4

    iget-object v4, v4, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v10, v2, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 457
    :cond_1
    invoke-virtual {v10}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 458
    sget-boolean v2, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->isEnableGroupSearch:Z

    if-eqz v2, :cond_2

    .line 459
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getGroupNameCursor(Ljava/lang/CharSequence;)Landroid/database/Cursor;

    move-result-object v17

    .line 461
    :cond_2
    sget-boolean v2, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->ExpandResultMaxAndShowMoreMode:Z

    if-eqz v2, :cond_6

    .line 462
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    iget-object v2, v2, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery2;->PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 469
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    iget-boolean v2, v2, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->isOnlineSearchDisabled:Z

    if-nez v2, :cond_3

    .line 470
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    # getter for: Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->Acc_Id:Ljava/lang/Long;
    invoke-static {v4}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->access$400(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object/from16 v0, p1

    invoke-virtual {v2, v6, v7, v0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->RIemailAddress(JLjava/lang/CharSequence;)Landroid/database/Cursor;

    move-result-object v19

    .line 471
    if-eqz v19, :cond_3

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_3

    .line 472
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    move-object/from16 v0, v19

    # invokes: Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->removeDuplicatesEmailQueryCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/MatrixCursor;
    invoke-static {v2, v11, v0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->access$500(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v21

    .line 478
    :cond_3
    :try_start_1
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v16, ""

    .line 480
    .local v16, "filter":Ljava/lang/String;
    :goto_3
    const-string v2, "content://com.android.email.provider/emailaddresscache/filter"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static/range {v16 .. v16}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 484
    .local v5, "emailCacheUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    iget-object v4, v2, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x3

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v7, "accountName as display_name"

    aput-object v7, v6, v2

    const/4 v2, 0x1

    const-string v7, "accountAddress as data1"

    aput-object v7, v6, v2

    const/4 v2, 0x2

    const-string v7, "photocontentbytes as cachedpictureData"

    aput-object v7, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "usageCount DESC, accountName"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v20

    .line 491
    .end local v5    # "emailCacheUri":Landroid/net/Uri;
    .end local v16    # "filter":Ljava/lang/String;
    :goto_4
    if-eqz v20, :cond_4

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_4

    .line 492
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    move-object/from16 v0, v20

    # invokes: Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->removeDuplicatesEmailQueryCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/MatrixCursor;
    invoke-static {v2, v11, v0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->access$500(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v22

    .line 493
    if-eqz v22, :cond_4

    invoke-virtual/range {v22 .. v22}, Landroid/database/MatrixCursor;->getCount()I

    move-result v2

    if-lez v2, :cond_4

    .line 494
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    # invokes: Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->removeDuplicatesEmailQueryCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/MatrixCursor;
    invoke-static {v2, v0, v1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->access$500(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v22

    .line 503
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v10    # "builder":Landroid/net/Uri$Builder;
    .end local v15    # "endoceInputText":Ljava/lang/String;
    :cond_4
    :goto_5
    new-instance v18, Landroid/database/MergeCursor;

    .end local v18    # "mergeResultCursor":Landroid/database/MergeCursor;
    const/4 v2, 0x4

    new-array v2, v2, [Landroid/database/Cursor;

    const/4 v4, 0x0

    aput-object v17, v2, v4

    const/4 v4, 0x1

    aput-object v11, v2, v4

    const/4 v4, 0x2

    aput-object v21, v2, v4

    const/4 v4, 0x3

    aput-object v22, v2, v4

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 506
    .restart local v18    # "mergeResultCursor":Landroid/database/MergeCursor;
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/database/Cursor;

    const/4 v4, 0x0

    aput-object v12, v2, v4

    const/4 v4, 0x1

    aput-object v18, v2, v4

    move-object/from16 v0, v23

    iput-object v2, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 507
    invoke-virtual/range {v18 .. v18}, Landroid/database/MergeCursor;->getCount()I

    move-result v2

    move-object/from16 v0, v23

    iput v2, v0, Landroid/widget/Filter$FilterResults;->count:I

    .line 509
    return-object v23

    .line 440
    .restart local v3    # "uri":Landroid/net/Uri;
    .restart local v10    # "builder":Landroid/net/Uri$Builder;
    .restart local v15    # "endoceInputText":Ljava/lang/String;
    :catch_0
    move-exception v14

    .line 441
    .local v14, "e1":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v14}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto/16 :goto_0

    .line 448
    .end local v14    # "e1":Ljava/io/UnsupportedEncodingException;
    :cond_5
    sget-object v2, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v15}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "limit"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    # getter for: Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mPreferredMaxResultCount:I
    invoke-static {v6}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->access$200(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v4, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v10

    goto/16 :goto_1

    .line 464
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    iget-object v2, v2, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    goto/16 :goto_2

    .line 478
    :cond_7
    :try_start_2
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v16

    goto/16 :goto_3

    .line 488
    :catch_1
    move-exception v13

    .line 489
    .local v13, "e":Ljava/lang/Exception;
    const/16 v20, 0x0

    goto/16 :goto_4

    .line 500
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v10    # "builder":Landroid/net/Uri$Builder;
    .end local v13    # "e":Ljava/lang/Exception;
    .end local v15    # "endoceInputText":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    const-string v4, ""

    # setter for: Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mInputText:Ljava/lang/String;
    invoke-static {v2, v4}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->access$102(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_5
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 4
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "results"    # Landroid/widget/Filter$FilterResults;

    .prologue
    .line 514
    iget-object v1, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 515
    iget-object v1, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v1, [Landroid/database/Cursor;

    move-object v0, v1

    check-cast v0, [Landroid/database/Cursor;

    .line 516
    .local v0, "cursors":[Landroid/database/Cursor;
    iget-object v1, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    const/4 v3, 0x1

    aget-object v3, v0, v3

    invoke-virtual {v1, p1, v2, v3}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->onDirectoryLoadFinished(Ljava/lang/CharSequence;Landroid/database/Cursor;Landroid/database/Cursor;)V

    .line 518
    .end local v0    # "cursors":[Landroid/database/Cursor;
    :cond_0
    iget-object v1, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;->this$0:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    invoke-virtual {v1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getCount()I

    move-result v1

    iput v1, p2, Landroid/widget/Filter$FilterResults;->count:I

    .line 519
    return-void
.end method
