.class public Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$SearchResult;
.super Ljava/lang/Object;
.source "SecBaseEmailAddressAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SearchResult"
.end annotation


# instance fields
.field public alias:Ljava/lang/String;

.field public company:Ljava/lang/String;

.field public displayName:Ljava/lang/String;

.field public displayname:Ljava/lang/String;

.field public emailAddress:Ljava/lang/String;

.field public emailaddress:Ljava/lang/String;

.field public firstname:Ljava/lang/String;

.field public homephone:Ljava/lang/String;

.field public lastname:Ljava/lang/String;

.field public mobilephone:Ljava/lang/String;

.field public office:Ljava/lang/String;

.field public pictureData:Ljava/lang/String;

.field public thumbnailUriAsString:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public workphone:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 355
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 356
    iput-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$SearchResult;->displayName:Ljava/lang/String;

    .line 357
    iput-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$SearchResult;->emailAddress:Ljava/lang/String;

    .line 359
    iput-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$SearchResult;->pictureData:Ljava/lang/String;

    .line 360
    iput-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$SearchResult;->thumbnailUriAsString:Ljava/lang/String;

    .line 362
    iput-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$SearchResult;->displayname:Ljava/lang/String;

    .line 363
    iput-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$SearchResult;->emailaddress:Ljava/lang/String;

    .line 364
    iput-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$SearchResult;->workphone:Ljava/lang/String;

    .line 365
    iput-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$SearchResult;->homephone:Ljava/lang/String;

    .line 366
    iput-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$SearchResult;->mobilephone:Ljava/lang/String;

    .line 367
    iput-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$SearchResult;->firstname:Ljava/lang/String;

    .line 368
    iput-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$SearchResult;->lastname:Ljava/lang/String;

    .line 369
    iput-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$SearchResult;->company:Ljava/lang/String;

    .line 370
    iput-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$SearchResult;->title:Ljava/lang/String;

    .line 371
    iput-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$SearchResult;->office:Ljava/lang/String;

    .line 372
    iput-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$SearchResult;->alias:Ljava/lang/String;

    return-void
.end method
