.class public abstract Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;
.super Lcom/android/common/widget/CompositeCursorAdapter;
.source "SecBaseEmailAddressAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$PhotoQuery;,
        Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartitionFilter;,
        Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DefaultPartitionFilter;,
        Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$SearchResult;,
        Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryListQuery;,
        Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery_Group;,
        Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery3_1;,
        Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery3;,
        Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery2_1;,
        Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery2;,
        Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;,
        Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1;,
        Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery;,
        Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    }
.end annotation


# static fields
.field private static BackupOfSearchedString:Ljava/lang/String;

.field public static ExpandResultMaxAndShowMoreMode:Z

.field public static final RIC_URI:Landroid/net/Uri;

.field public static isEnableGroupSearch:Z


# instance fields
.field private Acc_Id:Ljava/lang/Long;

.field public isOnlineSearchDisabled:Z

.field private mAccount:Landroid/accounts/Account;

.field protected final mContentResolver:Landroid/content/ContentResolver;

.field private mDirectoriesLoaded:Z

.field private mHandler:Landroid/os/Handler;

.field private mInputText:Ljava/lang/String;

.field private mPreferredMaxResultCount:I

.field protected final mSynchronizer:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 105
    const-string v0, "content://com.android.exchange.directory.provider/recipientInformation cache/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->RIC_URI:Landroid/net/Uri;

    .line 130
    sput-boolean v1, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->ExpandResultMaxAndShowMoreMode:Z

    .line 131
    sput-boolean v1, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->isEnableGroupSearch:Z

    return-void
.end method

.method static synthetic access$000(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mDirectoriesLoaded:Z

    return v0
.end method

.method static synthetic access$002(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mDirectoriesLoaded:Z

    return p1
.end method

.method static synthetic access$100(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mInputText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mInputText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    .prologue
    .line 61
    iget v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mPreferredMaxResultCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;)Ljava/lang/Long;
    .locals 1
    .param p0, "x0"    # Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->Acc_Id:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/MatrixCursor;
    .locals 1
    .param p0, "x0"    # Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;
    .param p1, "x1"    # Landroid/database/Cursor;
    .param p2, "x2"    # Landroid/database/Cursor;

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->removeDuplicatesEmailQueryCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/MatrixCursor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->makeDisplayString(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private addNoMatchesAtlastPatition()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 1378
    invoke-virtual {p0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getPartitionCount()I

    move-result v3

    .line 1380
    .local v3, "totalPatitionCount":I
    if-ge v3, v7, :cond_0

    .line 1381
    const-string v4, "SecBaseEmailAddressAdapter"

    const-string v5, "addNoMatchesAtlastPatition : partition is none"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1393
    :goto_0
    return-void

    .line 1385
    :cond_0
    add-int/lit8 v2, v3, -0x1

    .line 1386
    .local v2, "partionindex":I
    invoke-virtual {p0, v2}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v0

    .line 1388
    .local v0, "lastCursor":Landroid/database/Cursor;
    new-instance v1, Landroid/database/MatrixCursor;

    sget-object v4, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v1, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1389
    .local v1, "newCursor":Landroid/database/MatrixCursor;
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "NoMatches"

    aput-object v6, v4, v5

    const-string v5, ""

    aput-object v5, v4, v7

    const/4 v5, 0x2

    const/4 v6, 0x0

    aput-object v6, v4, v5

    invoke-virtual {v1, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 1391
    invoke-virtual {p0, v2, v1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->changeCursor(ILandroid/database/Cursor;)V

    goto :goto_0
.end method

.method private addShowMoreButtonAtlastPatition()V
    .locals 17

    .prologue
    .line 1512
    invoke-virtual/range {p0 .. p0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getPartitionCount()I

    move-result v13

    .line 1514
    .local v13, "totalPatitionCount":I
    const/4 v14, 0x1

    if-gt v13, v14, :cond_0

    .line 1515
    const-string v14, "SecBaseEmailAddressAdapter"

    const-string v15, "addShowMoreButtonAtlastPatition : partition is none"

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1591
    :goto_0
    return-void

    .line 1519
    :cond_0
    add-int/lit8 v10, v13, -0x1

    .line 1521
    .local v10, "partionindex":I
    const/4 v5, 0x0

    .line 1522
    .local v5, "enbleShowMore":Z
    const/4 v6, 0x1

    .local v6, "i":I
    :goto_1
    if-ge v6, v13, :cond_2

    .line 1523
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getPartition(I)Lcom/android/common/widget/CompositeCursorAdapter$Partition;

    move-result-object v11

    check-cast v11, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;

    .line 1524
    .local v11, "partition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    if-eqz v11, :cond_1

    .line 1526
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v1

    .line 1527
    .local v1, "curCursor":Landroid/database/Cursor;
    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v14

    if-ltz v14, :cond_1

    .line 1529
    const-string v14, "SecBaseEmailAddressAdapter"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "addShowMoreButtonAtlastPatition : cursor is not null. + ("

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ")"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1531
    iget-boolean v14, v11, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->moreResultIsRemained:Z

    if-eqz v14, :cond_1

    .line 1532
    const-string v14, "SecBaseEmailAddressAdapter"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "addShowMoreButtonAtlastPatition : showmore does not need. + ("

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ")"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1533
    const/4 v5, 0x1

    .line 1522
    .end local v1    # "curCursor":Landroid/database/Cursor;
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1540
    .end local v11    # "partition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    :cond_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v8

    .line 1542
    .local v8, "lastCursor":Landroid/database/Cursor;
    new-instance v9, Landroid/database/MatrixCursor;

    sget-object v14, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v9, v14}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1543
    .local v9, "newCursor":Landroid/database/MatrixCursor;
    if-eqz v8, :cond_6

    .line 1545
    const/4 v14, -0x1

    invoke-interface {v8, v14}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1546
    :cond_3
    :goto_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1550
    const/4 v14, 0x0

    invoke-interface {v8, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1551
    .local v2, "displayName":Ljava/lang/String;
    const/4 v14, 0x1

    invoke-interface {v8, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1554
    .local v4, "emailAddress":Ljava/lang/String;
    sget-object v14, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1;->PROJECTION:[Ljava/lang/String;

    const/4 v15, 0x2

    aget-object v14, v14, v15

    invoke-interface {v8, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 1555
    .local v7, "imageIndex":I
    const/4 v12, 0x0

    .line 1558
    .local v12, "pictureData":Ljava/lang/String;
    if-eqz v2, :cond_4

    const-string v14, "gal_search_show_more"

    invoke-virtual {v2, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_3

    .line 1563
    :cond_4
    const/4 v14, -0x1

    if-eq v7, v14, :cond_5

    .line 1564
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1569
    :cond_5
    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v2, v14, v15

    const/4 v15, 0x1

    aput-object v4, v14, v15

    const/4 v15, 0x2

    aput-object v12, v14, v15

    invoke-virtual {v9, v14}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_2

    .line 1576
    .end local v2    # "displayName":Ljava/lang/String;
    .end local v4    # "emailAddress":Ljava/lang/String;
    .end local v7    # "imageIndex":I
    .end local v12    # "pictureData":Ljava/lang/String;
    :cond_6
    if-eqz v5, :cond_7

    .line 1579
    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const-string v16, "gal_search_show_more"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const-string v16, "-1"

    aput-object v16, v14, v15

    const/4 v15, 0x2

    const-string v16, "gal_search_show_more"

    aput-object v16, v14, v15

    invoke-virtual {v9, v14}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 1585
    :goto_3
    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v9}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->changeCursor(ILandroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1586
    :catch_0
    move-exception v3

    .line 1587
    .local v3, "e":Ljava/lang/Exception;
    const-string v14, "SecBaseEmailAddressAdapter"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "addShowMoreButtonAtlastPatition : partionindex : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", newCursor.getCount() : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v9}, Landroid/database/MatrixCursor;->getCount()I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1588
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1581
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_7
    const-string v14, "SecBaseEmailAddressAdapter"

    const-string v15, "addShowMoreButtonAtlastPatition : showmore does not need."

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private hasDuplicates(Landroid/database/Cursor;I)Z
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "partition"    # I

    .prologue
    const/4 v1, 0x1

    .line 1626
    const/4 v2, -0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1627
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1628
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1629
    .local v0, "emailAddress":Ljava/lang/String;
    invoke-direct {p0, v0, p2}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->isDuplicate(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1633
    .end local v0    # "emailAddress":Ljava/lang/String;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isAllPartitionEmpty()Z
    .locals 3

    .prologue
    .line 1612
    invoke-virtual {p0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getPartitionCount()I

    move-result v1

    .line 1614
    .local v1, "totalCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 1616
    invoke-virtual {p0, v0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->isPartitionEmpty(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1618
    const/4 v2, 0x0

    .line 1622
    :goto_1
    return v2

    .line 1614
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1622
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private isAllPartitionLoadFinished()Z
    .locals 4

    .prologue
    .line 1596
    invoke-virtual {p0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getPartitionCount()I

    move-result v2

    .line 1598
    .local v2, "totalCount":I
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 1600
    invoke-virtual {p0, v1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getPartition(I)Lcom/android/common/widget/CompositeCursorAdapter$Partition;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;

    .line 1601
    .local v0, "curpartition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    iget-boolean v3, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->loading:Z

    if-eqz v3, :cond_0

    .line 1603
    const/4 v3, 0x0

    .line 1607
    .end local v0    # "curpartition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    :goto_1
    return v3

    .line 1598
    .restart local v0    # "curpartition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1607
    .end local v0    # "curpartition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    :cond_1
    const/4 v3, 0x1

    goto :goto_1
.end method

.method private isDuplicate(Ljava/lang/String;I)Z
    .locals 6
    .param p1, "emailAddress"    # Ljava/lang/String;
    .param p2, "excludePartition"    # I

    .prologue
    const/4 v4, 0x1

    .line 1641
    invoke-virtual {p0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getPartitionCount()I

    move-result v3

    .line 1643
    .local v3, "partitionCount":I
    const/4 v2, 0x0

    .local v2, "partition":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 1644
    if-eq v2, p2, :cond_1

    invoke-direct {p0, v2}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->isLoading(I)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1645
    invoke-virtual {p0, v2}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v1

    .line 1646
    .local v1, "cursor":Landroid/database/Cursor;
    if-eqz v1, :cond_1

    .line 1647
    const/4 v5, -0x1

    invoke-interface {v1, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1648
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1649
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1650
    .local v0, "address":Ljava/lang/String;
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1658
    .end local v0    # "address":Ljava/lang/String;
    .end local v1    # "cursor":Landroid/database/Cursor;
    :goto_1
    return v4

    .line 1643
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1658
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private isDuplicateAddPictureDate(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 56
    .param p1, "excludePartition"    # I
    .param p2, "displayName"    # Ljava/lang/String;
    .param p3, "emailAddress"    # Ljava/lang/String;
    .param p4, "newpictureData"    # Ljava/lang/String;
    .param p5, "displayname"    # Ljava/lang/String;
    .param p6, "emailaddress"    # Ljava/lang/String;
    .param p7, "workphone"    # Ljava/lang/String;
    .param p8, "homephone"    # Ljava/lang/String;
    .param p9, "mobilephone"    # Ljava/lang/String;
    .param p10, "firstname"    # Ljava/lang/String;
    .param p11, "lastname"    # Ljava/lang/String;
    .param p12, "company"    # Ljava/lang/String;
    .param p13, "title"    # Ljava/lang/String;
    .param p14, "office"    # Ljava/lang/String;
    .param p15, "alias"    # Ljava/lang/String;

    .prologue
    .line 1666
    const/16 v36, 0x0

    .line 1667
    .local v36, "isduplicated":Z
    invoke-virtual/range {p0 .. p0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getPartitionCount()I

    move-result v51

    .line 1669
    .local v51, "partitionCount":I
    const/16 v38, 0x0

    .line 1670
    .local v38, "mergeResultCursor":Landroid/database/MergeCursor;
    const/16 v42, 0x0

    .line 1671
    .local v42, "newCursor1":Landroid/database/MatrixCursor;
    const/16 v44, 0x0

    .line 1672
    .local v44, "newCursor2":Landroid/database/MatrixCursor;
    const/16 v46, 0x0

    .line 1675
    .local v46, "newCursor3":Landroid/database/MatrixCursor;
    :try_start_0
    new-instance v43, Landroid/database/MatrixCursor;

    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, v43

    move-object/from16 v1, v54

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_a
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1676
    .end local v42    # "newCursor1":Landroid/database/MatrixCursor;
    .local v43, "newCursor1":Landroid/database/MatrixCursor;
    :try_start_1
    new-instance v45, Landroid/database/MatrixCursor;

    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery2_1;->PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, v45

    move-object/from16 v1, v54

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_b
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 1677
    .end local v44    # "newCursor2":Landroid/database/MatrixCursor;
    .local v45, "newCursor2":Landroid/database/MatrixCursor;
    :try_start_2
    new-instance v47, Landroid/database/MatrixCursor;

    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery3_1;->PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, v47

    move-object/from16 v1, v54

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_c
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 1679
    .end local v46    # "newCursor3":Landroid/database/MatrixCursor;
    .local v47, "newCursor3":Landroid/database/MatrixCursor;
    const/16 v23, 0x0

    .line 1680
    .local v23, "defaultPatitionCursorChanged":Z
    const/16 v48, 0x0

    .line 1682
    .local v48, "nowFounded":Z
    const/16 v33, -0x1

    .line 1683
    .local v33, "imageIndex1":I
    const/16 v34, -0x1

    .line 1684
    .local v34, "imageIndex2":I
    const/16 v35, -0x1

    .line 1685
    .local v35, "imageIndex3":I
    const/16 v32, -0x1

    .line 1686
    .local v32, "imageIndex":I
    const/16 v24, -0x1

    .line 1687
    .local v24, "displaynameIndex":I
    const/16 v29, -0x1

    .line 1688
    .local v29, "emailaddressIndex":I
    const/16 v53, -0x1

    .line 1689
    .local v53, "workphoneIndex":I
    const/16 v31, -0x1

    .line 1690
    .local v31, "homephoneIndex":I
    const/16 v40, -0x1

    .line 1691
    .local v40, "mobilephoneIndex":I
    const/16 v30, -0x1

    .line 1692
    .local v30, "firstnameIndex":I
    const/16 v37, -0x1

    .line 1693
    .local v37, "lastnameIndex":I
    const/4 v5, -0x1

    .line 1694
    .local v5, "companyIndex":I
    const/16 v52, -0x1

    .line 1695
    .local v52, "titleIndex":I
    const/16 v49, -0x1

    .line 1696
    .local v49, "officeIndex":I
    const/4 v4, -0x1

    .line 1698
    .local v4, "aliasIndex":I
    const/4 v8, 0x0

    .line 1699
    .local v8, "curdisplayName":Ljava/lang/String;
    const/4 v10, 0x0

    .line 1701
    .local v10, "curemailAddress":Ljava/lang/String;
    const/16 v18, 0x0

    .line 1702
    .local v18, "curpictureData":Ljava/lang/String;
    const/16 v20, 0x0

    .line 1703
    .local v20, "curthumbnailUriAsString":Ljava/lang/String;
    const/16 v17, 0x0

    .line 1705
    .local v17, "curpictureBytes":[B
    const/4 v9, 0x0

    .line 1706
    .local v9, "curdisplayname":Ljava/lang/String;
    const/4 v11, 0x0

    .line 1707
    .local v11, "curemailaddress":Ljava/lang/String;
    const/16 v22, 0x0

    .line 1708
    .local v22, "curworkphone":Ljava/lang/String;
    const/4 v13, 0x0

    .line 1709
    .local v13, "curhomephone":Ljava/lang/String;
    const/4 v15, 0x0

    .line 1710
    .local v15, "curmobilephone":Ljava/lang/String;
    const/4 v12, 0x0

    .line 1711
    .local v12, "curfirstname":Ljava/lang/String;
    const/4 v14, 0x0

    .line 1712
    .local v14, "curlastname":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1713
    .local v7, "curcompany":Ljava/lang/String;
    const/16 v21, 0x0

    .line 1714
    .local v21, "curtitle":Ljava/lang/String;
    const/16 v16, 0x0

    .line 1715
    .local v16, "curoffice":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1717
    .local v6, "curalias":Ljava/lang/String;
    const/16 v50, 0x0

    .local v50, "partition":I
    :goto_0
    move/from16 v0, v50

    move/from16 v1, v51

    if-ge v0, v1, :cond_26

    .line 1718
    move/from16 v0, v50

    move/from16 v1, p1

    if-eq v0, v1, :cond_25

    :try_start_3
    move-object/from16 v0, p0

    move/from16 v1, v50

    invoke-direct {v0, v1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->isLoading(I)Z

    move-result v54

    if-nez v54, :cond_25

    .line 1719
    move-object/from16 v0, p0

    move/from16 v1, v50

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v19

    .line 1720
    .local v19, "cursor":Landroid/database/Cursor;
    if-eqz v19, :cond_25

    .line 1721
    const/16 v54, -0x1

    move-object/from16 v0, v19

    move/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1722
    :cond_0
    :goto_1
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z

    move-result v54

    if-eqz v54, :cond_25

    .line 1723
    const/16 v18, 0x0

    .line 1724
    const/16 v20, 0x0

    .line 1725
    const/16 v17, 0x0

    .line 1727
    const/16 v54, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v41

    .line 1728
    .local v41, "name":Ljava/lang/String;
    const/16 v54, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1729
    .local v3, "address":Ljava/lang/String;
    if-nez v50, :cond_22

    .line 1731
    const/16 v54, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1732
    const/16 v54, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1733
    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v55, 0x2

    aget-object v54, v54, v55

    move-object/from16 v0, v19

    move-object/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v33

    .line 1734
    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery2_1;->PROJECTION:[Ljava/lang/String;

    const/16 v55, 0x2

    aget-object v54, v54, v55

    move-object/from16 v0, v19

    move-object/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    .line 1735
    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery3_1;->PROJECTION:[Ljava/lang/String;

    const/16 v55, 0x2

    aget-object v54, v54, v55

    move-object/from16 v0, v19

    move-object/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v35

    .line 1737
    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v55, 0x2

    aget-object v54, v54, v55

    move-object/from16 v0, v19

    move-object/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    .line 1738
    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v55, 0x3

    aget-object v54, v54, v55

    move-object/from16 v0, v19

    move-object/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 1739
    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v55, 0x4

    aget-object v54, v54, v55

    move-object/from16 v0, v19

    move-object/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    .line 1740
    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v55, 0x5

    aget-object v54, v54, v55

    move-object/from16 v0, v19

    move-object/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v53

    .line 1741
    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v55, 0x6

    aget-object v54, v54, v55

    move-object/from16 v0, v19

    move-object/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    .line 1742
    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v55, 0x7

    aget-object v54, v54, v55

    move-object/from16 v0, v19

    move-object/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v40

    .line 1743
    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v55, 0x8

    aget-object v54, v54, v55

    move-object/from16 v0, v19

    move-object/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    .line 1744
    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v55, 0x9

    aget-object v54, v54, v55

    move-object/from16 v0, v19

    move-object/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v37

    .line 1745
    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v55, 0xa

    aget-object v54, v54, v55

    move-object/from16 v0, v19

    move-object/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 1746
    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v55, 0xb

    aget-object v54, v54, v55

    move-object/from16 v0, v19

    move-object/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v52

    .line 1747
    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v55, 0xc

    aget-object v54, v54, v55

    move-object/from16 v0, v19

    move-object/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v49

    .line 1748
    sget-object v54, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v55, 0xd

    aget-object v54, v54, v55

    move-object/from16 v0, v19

    move-object/from16 v1, v54

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 1750
    if-ltz v33, :cond_2c

    .line 1751
    move-object/from16 v0, v19

    move/from16 v1, v33

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 1758
    :cond_1
    :goto_2
    if-lez v24, :cond_2

    .line 1759
    move-object/from16 v0, v19

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1761
    :cond_2
    if-eqz v9, :cond_3

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v54

    if-gtz v54, :cond_4

    .line 1762
    :cond_3
    move-object/from16 v9, p2

    .line 1765
    :cond_4
    if-lez v29, :cond_5

    .line 1766
    move-object/from16 v0, v19

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1768
    :cond_5
    if-eqz v11, :cond_6

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v54

    if-gtz v54, :cond_7

    .line 1769
    :cond_6
    move-object/from16 v11, p6

    .line 1772
    :cond_7
    if-lez v53, :cond_8

    .line 1773
    move-object/from16 v0, v19

    move/from16 v1, v53

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 1776
    :cond_8
    if-eqz v22, :cond_9

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v54

    if-gtz v54, :cond_a

    .line 1777
    :cond_9
    move-object/from16 v22, p7

    .line 1780
    :cond_a
    if-lez v31, :cond_b

    .line 1781
    move-object/from16 v0, v19

    move/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 1783
    :cond_b
    if-eqz v13, :cond_c

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v54

    if-gtz v54, :cond_d

    .line 1784
    :cond_c
    move-object/from16 v13, p8

    .line 1787
    :cond_d
    if-lez v40, :cond_e

    .line 1788
    move-object/from16 v0, v19

    move/from16 v1, v40

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1790
    :cond_e
    if-eqz v15, :cond_f

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v54

    if-gtz v54, :cond_10

    .line 1791
    :cond_f
    move-object/from16 v15, p9

    .line 1794
    :cond_10
    if-lez v30, :cond_11

    .line 1795
    move-object/from16 v0, v19

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1797
    :cond_11
    if-eqz v12, :cond_12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v54

    if-gtz v54, :cond_13

    .line 1798
    :cond_12
    move-object/from16 v12, p10

    .line 1801
    :cond_13
    if-lez v37, :cond_14

    .line 1802
    move-object/from16 v0, v19

    move/from16 v1, v37

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 1804
    :cond_14
    if-eqz v14, :cond_15

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v54

    if-gtz v54, :cond_16

    .line 1805
    :cond_15
    move-object/from16 v14, p11

    .line 1808
    :cond_16
    if-lez v5, :cond_17

    .line 1809
    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1811
    :cond_17
    if-eqz v7, :cond_18

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v54

    if-gtz v54, :cond_19

    .line 1812
    :cond_18
    move-object/from16 v7, p12

    .line 1815
    :cond_19
    if-lez v52, :cond_1a

    .line 1816
    move-object/from16 v0, v19

    move/from16 v1, v52

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 1818
    :cond_1a
    if-eqz v21, :cond_1b

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v54

    if-gtz v54, :cond_1c

    .line 1819
    :cond_1b
    move-object/from16 v21, p13

    .line 1822
    :cond_1c
    if-lez v49, :cond_1d

    .line 1823
    move-object/from16 v0, v19

    move/from16 v1, v49

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1825
    :cond_1d
    if-eqz v16, :cond_1e

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v54

    if-gtz v54, :cond_1f

    .line 1826
    :cond_1e
    move-object/from16 v16, p14

    .line 1829
    :cond_1f
    if-lez v4, :cond_20

    .line 1830
    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1832
    :cond_20
    if-eqz v6, :cond_21

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v54

    if-gtz v54, :cond_22

    .line 1833
    :cond_21
    move-object/from16 v6, p15

    .line 1837
    :cond_22
    const/16 v48, 0x0

    .line 1838
    move-object/from16 v0, p3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v54

    if-eqz v54, :cond_2e

    .line 1839
    if-eqz v41, :cond_23

    move-object/from16 v0, p2

    move-object/from16 v1, v41

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v54

    if-nez v54, :cond_23

    move-object/from16 v0, v41

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v54

    if-eqz v54, :cond_2e

    .line 1840
    :cond_23
    const/16 v36, 0x1

    .line 1841
    const/16 v48, 0x1

    .line 1843
    move-object/from16 v0, v41

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v54

    if-eqz v54, :cond_24

    if-eqz p2, :cond_24

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v54

    if-lez v54, :cond_24

    .line 1844
    move-object/from16 v8, p2

    .line 1847
    :cond_24
    if-eqz v50, :cond_2e

    .line 1929
    .end local v3    # "address":Ljava/lang/String;
    .end local v19    # "cursor":Landroid/database/Cursor;
    .end local v41    # "name":Ljava/lang/String;
    :cond_25
    if-eqz v36, :cond_3e

    .line 1935
    :cond_26
    if-eqz v23, :cond_27

    .line 1936
    new-instance v39, Landroid/database/MergeCursor;

    const/16 v54, 0x3

    move/from16 v0, v54

    new-array v0, v0, [Landroid/database/Cursor;

    move-object/from16 v54, v0

    const/16 v55, 0x0

    aput-object v45, v54, v55

    const/16 v55, 0x1

    aput-object v43, v54, v55

    const/16 v55, 0x2

    aput-object v47, v54, v55

    move-object/from16 v0, v39

    move-object/from16 v1, v54

    invoke-direct {v0, v1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1938
    .end local v38    # "mergeResultCursor":Landroid/database/MergeCursor;
    .local v39, "mergeResultCursor":Landroid/database/MergeCursor;
    const/16 v54, 0x0

    :try_start_4
    move-object/from16 v0, p0

    move/from16 v1, v54

    move-object/from16 v2, v39

    invoke-virtual {v0, v1, v2}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->changeCursor(ILandroid/database/Cursor;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_d
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    move-object/from16 v38, v39

    .line 1944
    .end local v39    # "mergeResultCursor":Landroid/database/MergeCursor;
    .restart local v38    # "mergeResultCursor":Landroid/database/MergeCursor;
    :cond_27
    if-eqz v43, :cond_28

    :try_start_5
    invoke-virtual/range {v43 .. v43}, Landroid/database/MatrixCursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 1949
    :cond_28
    :goto_3
    if-eqz v45, :cond_29

    :try_start_6
    invoke-virtual/range {v45 .. v45}, Landroid/database/MatrixCursor;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3

    .line 1954
    :cond_29
    :goto_4
    if-eqz v47, :cond_2a

    :try_start_7
    invoke-virtual/range {v47 .. v47}, Landroid/database/MatrixCursor;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    :cond_2a
    move-object/from16 v46, v47

    .end local v47    # "newCursor3":Landroid/database/MatrixCursor;
    .restart local v46    # "newCursor3":Landroid/database/MatrixCursor;
    move-object/from16 v44, v45

    .end local v45    # "newCursor2":Landroid/database/MatrixCursor;
    .restart local v44    # "newCursor2":Landroid/database/MatrixCursor;
    move-object/from16 v42, v43

    .line 1960
    .end local v4    # "aliasIndex":I
    .end local v5    # "companyIndex":I
    .end local v6    # "curalias":Ljava/lang/String;
    .end local v7    # "curcompany":Ljava/lang/String;
    .end local v8    # "curdisplayName":Ljava/lang/String;
    .end local v9    # "curdisplayname":Ljava/lang/String;
    .end local v10    # "curemailAddress":Ljava/lang/String;
    .end local v11    # "curemailaddress":Ljava/lang/String;
    .end local v12    # "curfirstname":Ljava/lang/String;
    .end local v13    # "curhomephone":Ljava/lang/String;
    .end local v14    # "curlastname":Ljava/lang/String;
    .end local v15    # "curmobilephone":Ljava/lang/String;
    .end local v16    # "curoffice":Ljava/lang/String;
    .end local v17    # "curpictureBytes":[B
    .end local v18    # "curpictureData":Ljava/lang/String;
    .end local v20    # "curthumbnailUriAsString":Ljava/lang/String;
    .end local v21    # "curtitle":Ljava/lang/String;
    .end local v22    # "curworkphone":Ljava/lang/String;
    .end local v23    # "defaultPatitionCursorChanged":Z
    .end local v24    # "displaynameIndex":I
    .end local v29    # "emailaddressIndex":I
    .end local v30    # "firstnameIndex":I
    .end local v31    # "homephoneIndex":I
    .end local v32    # "imageIndex":I
    .end local v33    # "imageIndex1":I
    .end local v34    # "imageIndex2":I
    .end local v35    # "imageIndex3":I
    .end local v37    # "lastnameIndex":I
    .end local v40    # "mobilephoneIndex":I
    .end local v43    # "newCursor1":Landroid/database/MatrixCursor;
    .end local v48    # "nowFounded":Z
    .end local v49    # "officeIndex":I
    .end local v50    # "partition":I
    .end local v52    # "titleIndex":I
    .end local v53    # "workphoneIndex":I
    .restart local v42    # "newCursor1":Landroid/database/MatrixCursor;
    :cond_2b
    :goto_5
    return v36

    .line 1752
    .end local v42    # "newCursor1":Landroid/database/MatrixCursor;
    .end local v44    # "newCursor2":Landroid/database/MatrixCursor;
    .end local v46    # "newCursor3":Landroid/database/MatrixCursor;
    .restart local v3    # "address":Ljava/lang/String;
    .restart local v4    # "aliasIndex":I
    .restart local v5    # "companyIndex":I
    .restart local v6    # "curalias":Ljava/lang/String;
    .restart local v7    # "curcompany":Ljava/lang/String;
    .restart local v8    # "curdisplayName":Ljava/lang/String;
    .restart local v9    # "curdisplayname":Ljava/lang/String;
    .restart local v10    # "curemailAddress":Ljava/lang/String;
    .restart local v11    # "curemailaddress":Ljava/lang/String;
    .restart local v12    # "curfirstname":Ljava/lang/String;
    .restart local v13    # "curhomephone":Ljava/lang/String;
    .restart local v14    # "curlastname":Ljava/lang/String;
    .restart local v15    # "curmobilephone":Ljava/lang/String;
    .restart local v16    # "curoffice":Ljava/lang/String;
    .restart local v17    # "curpictureBytes":[B
    .restart local v18    # "curpictureData":Ljava/lang/String;
    .restart local v19    # "cursor":Landroid/database/Cursor;
    .restart local v20    # "curthumbnailUriAsString":Ljava/lang/String;
    .restart local v21    # "curtitle":Ljava/lang/String;
    .restart local v22    # "curworkphone":Ljava/lang/String;
    .restart local v23    # "defaultPatitionCursorChanged":Z
    .restart local v24    # "displaynameIndex":I
    .restart local v29    # "emailaddressIndex":I
    .restart local v30    # "firstnameIndex":I
    .restart local v31    # "homephoneIndex":I
    .restart local v32    # "imageIndex":I
    .restart local v33    # "imageIndex1":I
    .restart local v34    # "imageIndex2":I
    .restart local v35    # "imageIndex3":I
    .restart local v37    # "lastnameIndex":I
    .restart local v40    # "mobilephoneIndex":I
    .restart local v41    # "name":Ljava/lang/String;
    .restart local v43    # "newCursor1":Landroid/database/MatrixCursor;
    .restart local v45    # "newCursor2":Landroid/database/MatrixCursor;
    .restart local v47    # "newCursor3":Landroid/database/MatrixCursor;
    .restart local v48    # "nowFounded":Z
    .restart local v49    # "officeIndex":I
    .restart local v50    # "partition":I
    .restart local v52    # "titleIndex":I
    .restart local v53    # "workphoneIndex":I
    :cond_2c
    if-ltz v34, :cond_2d

    .line 1753
    :try_start_8
    move-object/from16 v0, v19

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    goto/16 :goto_2

    .line 1754
    :cond_2d
    if-lez v35, :cond_1

    .line 1755
    move-object/from16 v0, v19

    move/from16 v1, v35

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v17

    goto/16 :goto_2

    .line 1853
    :cond_2e
    if-nez v50, :cond_0

    .line 1854
    if-eqz v48, :cond_3a

    .line 1855
    if-eqz v8, :cond_2f

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v54

    if-gtz v54, :cond_30

    .line 1856
    :cond_2f
    move-object/from16 v8, p2

    .line 1858
    :cond_30
    if-eqz v18, :cond_31

    .line 1859
    const/16 v54, 0xe

    move/from16 v0, v54

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v54, v0

    const/16 v55, 0x0

    aput-object v8, v54, v55

    const/16 v55, 0x1

    aput-object v10, v54, v55

    const/16 v55, 0x2

    aput-object v18, v54, v55

    const/16 v55, 0x3

    aput-object v9, v54, v55

    const/16 v55, 0x4

    aput-object v11, v54, v55

    const/16 v55, 0x5

    aput-object v22, v54, v55

    const/16 v55, 0x6

    aput-object v13, v54, v55

    const/16 v55, 0x7

    aput-object v15, v54, v55

    const/16 v55, 0x8

    aput-object v12, v54, v55

    const/16 v55, 0x9

    aput-object v14, v54, v55

    const/16 v55, 0xa

    aput-object v7, v54, v55

    const/16 v55, 0xb

    aput-object v21, v54, v55

    const/16 v55, 0xc

    aput-object v16, v54, v55

    const/16 v55, 0xd

    aput-object v6, v54, v55

    move-object/from16 v0, v43

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 1890
    :goto_6
    const/16 v23, 0x1

    goto/16 :goto_1

    .line 1865
    :cond_31
    if-eqz v20, :cond_34

    .line 1866
    const/16 v54, 0xf

    move/from16 v0, v54

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v54, v0

    const/16 v55, 0x0

    aput-object v8, v54, v55

    const/16 v55, 0x1

    aput-object v10, v54, v55

    const/16 v55, 0x2

    aput-object v20, v54, v55

    const/16 v55, 0x3

    aput-object p4, v54, v55

    const/16 v55, 0x4

    aput-object v9, v54, v55

    const/16 v55, 0x5

    aput-object v11, v54, v55

    const/16 v55, 0x6

    aput-object v22, v54, v55

    const/16 v55, 0x7

    aput-object v13, v54, v55

    const/16 v55, 0x8

    aput-object v15, v54, v55

    const/16 v55, 0x9

    aput-object v12, v54, v55

    const/16 v55, 0xa

    aput-object v14, v54, v55

    const/16 v55, 0xb

    aput-object v7, v54, v55

    const/16 v55, 0xc

    aput-object v21, v54, v55

    const/16 v55, 0xd

    aput-object v16, v54, v55

    const/16 v55, 0xe

    aput-object v6, v54, v55

    move-object/from16 v0, v45

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_6

    .line 1940
    .end local v3    # "address":Ljava/lang/String;
    .end local v19    # "cursor":Landroid/database/Cursor;
    .end local v41    # "name":Ljava/lang/String;
    :catch_0
    move-exception v25

    move-object/from16 v46, v47

    .end local v47    # "newCursor3":Landroid/database/MatrixCursor;
    .restart local v46    # "newCursor3":Landroid/database/MatrixCursor;
    move-object/from16 v44, v45

    .end local v45    # "newCursor2":Landroid/database/MatrixCursor;
    .restart local v44    # "newCursor2":Landroid/database/MatrixCursor;
    move-object/from16 v42, v43

    .line 1941
    .end local v4    # "aliasIndex":I
    .end local v5    # "companyIndex":I
    .end local v6    # "curalias":Ljava/lang/String;
    .end local v7    # "curcompany":Ljava/lang/String;
    .end local v8    # "curdisplayName":Ljava/lang/String;
    .end local v9    # "curdisplayname":Ljava/lang/String;
    .end local v10    # "curemailAddress":Ljava/lang/String;
    .end local v11    # "curemailaddress":Ljava/lang/String;
    .end local v12    # "curfirstname":Ljava/lang/String;
    .end local v13    # "curhomephone":Ljava/lang/String;
    .end local v14    # "curlastname":Ljava/lang/String;
    .end local v15    # "curmobilephone":Ljava/lang/String;
    .end local v16    # "curoffice":Ljava/lang/String;
    .end local v17    # "curpictureBytes":[B
    .end local v18    # "curpictureData":Ljava/lang/String;
    .end local v20    # "curthumbnailUriAsString":Ljava/lang/String;
    .end local v21    # "curtitle":Ljava/lang/String;
    .end local v22    # "curworkphone":Ljava/lang/String;
    .end local v23    # "defaultPatitionCursorChanged":Z
    .end local v24    # "displaynameIndex":I
    .end local v29    # "emailaddressIndex":I
    .end local v30    # "firstnameIndex":I
    .end local v31    # "homephoneIndex":I
    .end local v32    # "imageIndex":I
    .end local v33    # "imageIndex1":I
    .end local v34    # "imageIndex2":I
    .end local v35    # "imageIndex3":I
    .end local v37    # "lastnameIndex":I
    .end local v40    # "mobilephoneIndex":I
    .end local v43    # "newCursor1":Landroid/database/MatrixCursor;
    .end local v48    # "nowFounded":Z
    .end local v49    # "officeIndex":I
    .end local v50    # "partition":I
    .end local v52    # "titleIndex":I
    .end local v53    # "workphoneIndex":I
    .local v25, "e":Ljava/lang/Exception;
    .restart local v42    # "newCursor1":Landroid/database/MatrixCursor;
    :goto_7
    :try_start_9
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 1944
    if-eqz v42, :cond_32

    :try_start_a
    invoke-virtual/range {v42 .. v42}, Landroid/database/MatrixCursor;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_5

    .line 1949
    :cond_32
    :goto_8
    if-eqz v44, :cond_33

    :try_start_b
    invoke-virtual/range {v44 .. v44}, Landroid/database/MatrixCursor;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6

    .line 1954
    :cond_33
    :goto_9
    if-eqz v46, :cond_2b

    :try_start_c
    invoke-virtual/range {v46 .. v46}, Landroid/database/MatrixCursor;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1

    goto/16 :goto_5

    .line 1955
    :catch_1
    move-exception v28

    .line 1956
    .local v28, "e3":Ljava/lang/Exception;
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_5

    .line 1873
    .end local v25    # "e":Ljava/lang/Exception;
    .end local v28    # "e3":Ljava/lang/Exception;
    .end local v42    # "newCursor1":Landroid/database/MatrixCursor;
    .end local v44    # "newCursor2":Landroid/database/MatrixCursor;
    .end local v46    # "newCursor3":Landroid/database/MatrixCursor;
    .restart local v3    # "address":Ljava/lang/String;
    .restart local v4    # "aliasIndex":I
    .restart local v5    # "companyIndex":I
    .restart local v6    # "curalias":Ljava/lang/String;
    .restart local v7    # "curcompany":Ljava/lang/String;
    .restart local v8    # "curdisplayName":Ljava/lang/String;
    .restart local v9    # "curdisplayname":Ljava/lang/String;
    .restart local v10    # "curemailAddress":Ljava/lang/String;
    .restart local v11    # "curemailaddress":Ljava/lang/String;
    .restart local v12    # "curfirstname":Ljava/lang/String;
    .restart local v13    # "curhomephone":Ljava/lang/String;
    .restart local v14    # "curlastname":Ljava/lang/String;
    .restart local v15    # "curmobilephone":Ljava/lang/String;
    .restart local v16    # "curoffice":Ljava/lang/String;
    .restart local v17    # "curpictureBytes":[B
    .restart local v18    # "curpictureData":Ljava/lang/String;
    .restart local v19    # "cursor":Landroid/database/Cursor;
    .restart local v20    # "curthumbnailUriAsString":Ljava/lang/String;
    .restart local v21    # "curtitle":Ljava/lang/String;
    .restart local v22    # "curworkphone":Ljava/lang/String;
    .restart local v23    # "defaultPatitionCursorChanged":Z
    .restart local v24    # "displaynameIndex":I
    .restart local v29    # "emailaddressIndex":I
    .restart local v30    # "firstnameIndex":I
    .restart local v31    # "homephoneIndex":I
    .restart local v32    # "imageIndex":I
    .restart local v33    # "imageIndex1":I
    .restart local v34    # "imageIndex2":I
    .restart local v35    # "imageIndex3":I
    .restart local v37    # "lastnameIndex":I
    .restart local v40    # "mobilephoneIndex":I
    .restart local v41    # "name":Ljava/lang/String;
    .restart local v43    # "newCursor1":Landroid/database/MatrixCursor;
    .restart local v45    # "newCursor2":Landroid/database/MatrixCursor;
    .restart local v47    # "newCursor3":Landroid/database/MatrixCursor;
    .restart local v48    # "nowFounded":Z
    .restart local v49    # "officeIndex":I
    .restart local v50    # "partition":I
    .restart local v52    # "titleIndex":I
    .restart local v53    # "workphoneIndex":I
    :cond_34
    if-eqz v17, :cond_39

    if-eqz p4, :cond_35

    :try_start_d
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    move-result v54

    if-gtz v54, :cond_39

    .line 1875
    :cond_35
    const/16 v54, 0xf

    move/from16 v0, v54

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v54, v0

    const/16 v55, 0x0

    aput-object v8, v54, v55

    const/16 v55, 0x1

    aput-object v10, v54, v55

    const/16 v55, 0x2

    aput-object v17, v54, v55

    const/16 v55, 0x3

    aput-object p4, v54, v55

    const/16 v55, 0x4

    aput-object v9, v54, v55

    const/16 v55, 0x5

    aput-object v11, v54, v55

    const/16 v55, 0x6

    aput-object v22, v54, v55

    const/16 v55, 0x7

    aput-object v13, v54, v55

    const/16 v55, 0x8

    aput-object v15, v54, v55

    const/16 v55, 0x9

    aput-object v12, v54, v55

    const/16 v55, 0xa

    aput-object v14, v54, v55

    const/16 v55, 0xb

    aput-object v7, v54, v55

    const/16 v55, 0xc

    aput-object v21, v54, v55

    const/16 v55, 0xd

    aput-object v16, v54, v55

    const/16 v55, 0xe

    aput-object v6, v54, v55

    move-object/from16 v0, v47

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_6

    .line 1943
    .end local v3    # "address":Ljava/lang/String;
    .end local v19    # "cursor":Landroid/database/Cursor;
    .end local v41    # "name":Ljava/lang/String;
    :catchall_0
    move-exception v54

    move-object/from16 v46, v47

    .end local v47    # "newCursor3":Landroid/database/MatrixCursor;
    .restart local v46    # "newCursor3":Landroid/database/MatrixCursor;
    move-object/from16 v44, v45

    .end local v45    # "newCursor2":Landroid/database/MatrixCursor;
    .restart local v44    # "newCursor2":Landroid/database/MatrixCursor;
    move-object/from16 v42, v43

    .line 1944
    .end local v4    # "aliasIndex":I
    .end local v5    # "companyIndex":I
    .end local v6    # "curalias":Ljava/lang/String;
    .end local v7    # "curcompany":Ljava/lang/String;
    .end local v8    # "curdisplayName":Ljava/lang/String;
    .end local v9    # "curdisplayname":Ljava/lang/String;
    .end local v10    # "curemailAddress":Ljava/lang/String;
    .end local v11    # "curemailaddress":Ljava/lang/String;
    .end local v12    # "curfirstname":Ljava/lang/String;
    .end local v13    # "curhomephone":Ljava/lang/String;
    .end local v14    # "curlastname":Ljava/lang/String;
    .end local v15    # "curmobilephone":Ljava/lang/String;
    .end local v16    # "curoffice":Ljava/lang/String;
    .end local v17    # "curpictureBytes":[B
    .end local v18    # "curpictureData":Ljava/lang/String;
    .end local v20    # "curthumbnailUriAsString":Ljava/lang/String;
    .end local v21    # "curtitle":Ljava/lang/String;
    .end local v22    # "curworkphone":Ljava/lang/String;
    .end local v23    # "defaultPatitionCursorChanged":Z
    .end local v24    # "displaynameIndex":I
    .end local v29    # "emailaddressIndex":I
    .end local v30    # "firstnameIndex":I
    .end local v31    # "homephoneIndex":I
    .end local v32    # "imageIndex":I
    .end local v33    # "imageIndex1":I
    .end local v34    # "imageIndex2":I
    .end local v35    # "imageIndex3":I
    .end local v37    # "lastnameIndex":I
    .end local v40    # "mobilephoneIndex":I
    .end local v43    # "newCursor1":Landroid/database/MatrixCursor;
    .end local v48    # "nowFounded":Z
    .end local v49    # "officeIndex":I
    .end local v50    # "partition":I
    .end local v52    # "titleIndex":I
    .end local v53    # "workphoneIndex":I
    .restart local v42    # "newCursor1":Landroid/database/MatrixCursor;
    :goto_a
    if-eqz v42, :cond_36

    :try_start_e
    invoke-virtual/range {v42 .. v42}, Landroid/database/MatrixCursor;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_7

    .line 1949
    :cond_36
    :goto_b
    if-eqz v44, :cond_37

    :try_start_f
    invoke-virtual/range {v44 .. v44}, Landroid/database/MatrixCursor;->close()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_8

    .line 1954
    :cond_37
    :goto_c
    if-eqz v46, :cond_38

    :try_start_10
    invoke-virtual/range {v46 .. v46}, Landroid/database/MatrixCursor;->close()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_9

    .line 1957
    :cond_38
    :goto_d
    throw v54

    .line 1883
    .end local v42    # "newCursor1":Landroid/database/MatrixCursor;
    .end local v44    # "newCursor2":Landroid/database/MatrixCursor;
    .end local v46    # "newCursor3":Landroid/database/MatrixCursor;
    .restart local v3    # "address":Ljava/lang/String;
    .restart local v4    # "aliasIndex":I
    .restart local v5    # "companyIndex":I
    .restart local v6    # "curalias":Ljava/lang/String;
    .restart local v7    # "curcompany":Ljava/lang/String;
    .restart local v8    # "curdisplayName":Ljava/lang/String;
    .restart local v9    # "curdisplayname":Ljava/lang/String;
    .restart local v10    # "curemailAddress":Ljava/lang/String;
    .restart local v11    # "curemailaddress":Ljava/lang/String;
    .restart local v12    # "curfirstname":Ljava/lang/String;
    .restart local v13    # "curhomephone":Ljava/lang/String;
    .restart local v14    # "curlastname":Ljava/lang/String;
    .restart local v15    # "curmobilephone":Ljava/lang/String;
    .restart local v16    # "curoffice":Ljava/lang/String;
    .restart local v17    # "curpictureBytes":[B
    .restart local v18    # "curpictureData":Ljava/lang/String;
    .restart local v19    # "cursor":Landroid/database/Cursor;
    .restart local v20    # "curthumbnailUriAsString":Ljava/lang/String;
    .restart local v21    # "curtitle":Ljava/lang/String;
    .restart local v22    # "curworkphone":Ljava/lang/String;
    .restart local v23    # "defaultPatitionCursorChanged":Z
    .restart local v24    # "displaynameIndex":I
    .restart local v29    # "emailaddressIndex":I
    .restart local v30    # "firstnameIndex":I
    .restart local v31    # "homephoneIndex":I
    .restart local v32    # "imageIndex":I
    .restart local v33    # "imageIndex1":I
    .restart local v34    # "imageIndex2":I
    .restart local v35    # "imageIndex3":I
    .restart local v37    # "lastnameIndex":I
    .restart local v40    # "mobilephoneIndex":I
    .restart local v41    # "name":Ljava/lang/String;
    .restart local v43    # "newCursor1":Landroid/database/MatrixCursor;
    .restart local v45    # "newCursor2":Landroid/database/MatrixCursor;
    .restart local v47    # "newCursor3":Landroid/database/MatrixCursor;
    .restart local v48    # "nowFounded":Z
    .restart local v49    # "officeIndex":I
    .restart local v50    # "partition":I
    .restart local v52    # "titleIndex":I
    .restart local v53    # "workphoneIndex":I
    :cond_39
    const/16 v54, 0xe

    :try_start_11
    move/from16 v0, v54

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v54, v0

    const/16 v55, 0x0

    aput-object v8, v54, v55

    const/16 v55, 0x1

    aput-object v10, v54, v55

    const/16 v55, 0x2

    aput-object p4, v54, v55

    const/16 v55, 0x3

    aput-object v9, v54, v55

    const/16 v55, 0x4

    aput-object v11, v54, v55

    const/16 v55, 0x5

    aput-object v22, v54, v55

    const/16 v55, 0x6

    aput-object v13, v54, v55

    const/16 v55, 0x7

    aput-object v15, v54, v55

    const/16 v55, 0x8

    aput-object v12, v54, v55

    const/16 v55, 0x9

    aput-object v14, v54, v55

    const/16 v55, 0xa

    aput-object v7, v54, v55

    const/16 v55, 0xb

    aput-object v21, v54, v55

    const/16 v55, 0xc

    aput-object v16, v54, v55

    const/16 v55, 0xd

    aput-object v6, v54, v55

    move-object/from16 v0, v43

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_6

    .line 1892
    :cond_3a
    if-eqz v18, :cond_3b

    .line 1893
    const/16 v54, 0xe

    move/from16 v0, v54

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v54, v0

    const/16 v55, 0x0

    aput-object v8, v54, v55

    const/16 v55, 0x1

    aput-object v10, v54, v55

    const/16 v55, 0x2

    aput-object v18, v54, v55

    const/16 v55, 0x3

    aput-object v9, v54, v55

    const/16 v55, 0x4

    aput-object v11, v54, v55

    const/16 v55, 0x5

    aput-object v22, v54, v55

    const/16 v55, 0x6

    aput-object v13, v54, v55

    const/16 v55, 0x7

    aput-object v15, v54, v55

    const/16 v55, 0x8

    aput-object v12, v54, v55

    const/16 v55, 0x9

    aput-object v14, v54, v55

    const/16 v55, 0xa

    aput-object v7, v54, v55

    const/16 v55, 0xb

    aput-object v21, v54, v55

    const/16 v55, 0xc

    aput-object v16, v54, v55

    const/16 v55, 0xd

    aput-object v6, v54, v55

    move-object/from16 v0, v43

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1899
    :cond_3b
    if-eqz v20, :cond_3c

    .line 1900
    const/16 v54, 0xf

    move/from16 v0, v54

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v54, v0

    const/16 v55, 0x0

    aput-object v8, v54, v55

    const/16 v55, 0x1

    aput-object v10, v54, v55

    const/16 v55, 0x2

    aput-object v20, v54, v55

    const/16 v55, 0x3

    aput-object p4, v54, v55

    const/16 v55, 0x4

    aput-object v9, v54, v55

    const/16 v55, 0x5

    aput-object v11, v54, v55

    const/16 v55, 0x6

    aput-object v22, v54, v55

    const/16 v55, 0x7

    aput-object v13, v54, v55

    const/16 v55, 0x8

    aput-object v15, v54, v55

    const/16 v55, 0x9

    aput-object v12, v54, v55

    const/16 v55, 0xa

    aput-object v14, v54, v55

    const/16 v55, 0xb

    aput-object v7, v54, v55

    const/16 v55, 0xc

    aput-object v21, v54, v55

    const/16 v55, 0xd

    aput-object v16, v54, v55

    const/16 v55, 0xe

    aput-object v6, v54, v55

    move-object/from16 v0, v45

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1907
    :cond_3c
    if-eqz v17, :cond_3d

    .line 1908
    const/16 v54, 0xf

    move/from16 v0, v54

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v54, v0

    const/16 v55, 0x0

    aput-object v8, v54, v55

    const/16 v55, 0x1

    aput-object v10, v54, v55

    const/16 v55, 0x2

    aput-object v17, v54, v55

    const/16 v55, 0x3

    aput-object p4, v54, v55

    const/16 v55, 0x4

    aput-object v9, v54, v55

    const/16 v55, 0x5

    aput-object v11, v54, v55

    const/16 v55, 0x6

    aput-object v22, v54, v55

    const/16 v55, 0x7

    aput-object v13, v54, v55

    const/16 v55, 0x8

    aput-object v15, v54, v55

    const/16 v55, 0x9

    aput-object v12, v54, v55

    const/16 v55, 0xa

    aput-object v14, v54, v55

    const/16 v55, 0xb

    aput-object v7, v54, v55

    const/16 v55, 0xc

    aput-object v21, v54, v55

    const/16 v55, 0xd

    aput-object v16, v54, v55

    const/16 v55, 0xe

    aput-object v6, v54, v55

    move-object/from16 v0, v47

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 1916
    :cond_3d
    const/16 v54, 0xe

    move/from16 v0, v54

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v54, v0

    const/16 v55, 0x0

    aput-object v8, v54, v55

    const/16 v55, 0x1

    aput-object v10, v54, v55

    const/16 v55, 0x2

    aput-object v18, v54, v55

    const/16 v55, 0x3

    aput-object v9, v54, v55

    const/16 v55, 0x4

    aput-object v11, v54, v55

    const/16 v55, 0x5

    aput-object v22, v54, v55

    const/16 v55, 0x6

    aput-object v13, v54, v55

    const/16 v55, 0x7

    aput-object v15, v54, v55

    const/16 v55, 0x8

    aput-object v12, v54, v55

    const/16 v55, 0x9

    aput-object v14, v54, v55

    const/16 v55, 0xa

    aput-object v7, v54, v55

    const/16 v55, 0xb

    aput-object v21, v54, v55

    const/16 v55, 0xc

    aput-object v16, v54, v55

    const/16 v55, 0xd

    aput-object v6, v54, v55

    move-object/from16 v0, v43

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_0
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto/16 :goto_1

    .line 1717
    .end local v3    # "address":Ljava/lang/String;
    .end local v19    # "cursor":Landroid/database/Cursor;
    .end local v41    # "name":Ljava/lang/String;
    :cond_3e
    add-int/lit8 v50, v50, 0x1

    goto/16 :goto_0

    .line 1945
    :catch_2
    move-exception v26

    .line 1946
    .local v26, "e1":Ljava/lang/Exception;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3

    .line 1950
    .end local v26    # "e1":Ljava/lang/Exception;
    :catch_3
    move-exception v27

    .line 1951
    .local v27, "e2":Ljava/lang/Exception;
    invoke-virtual/range {v27 .. v27}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4

    .line 1955
    .end local v27    # "e2":Ljava/lang/Exception;
    :catch_4
    move-exception v28

    .line 1956
    .restart local v28    # "e3":Ljava/lang/Exception;
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Exception;->printStackTrace()V

    move-object/from16 v46, v47

    .end local v47    # "newCursor3":Landroid/database/MatrixCursor;
    .restart local v46    # "newCursor3":Landroid/database/MatrixCursor;
    move-object/from16 v44, v45

    .end local v45    # "newCursor2":Landroid/database/MatrixCursor;
    .restart local v44    # "newCursor2":Landroid/database/MatrixCursor;
    move-object/from16 v42, v43

    .line 1958
    .end local v43    # "newCursor1":Landroid/database/MatrixCursor;
    .restart local v42    # "newCursor1":Landroid/database/MatrixCursor;
    goto/16 :goto_5

    .line 1945
    .end local v4    # "aliasIndex":I
    .end local v5    # "companyIndex":I
    .end local v6    # "curalias":Ljava/lang/String;
    .end local v7    # "curcompany":Ljava/lang/String;
    .end local v8    # "curdisplayName":Ljava/lang/String;
    .end local v9    # "curdisplayname":Ljava/lang/String;
    .end local v10    # "curemailAddress":Ljava/lang/String;
    .end local v11    # "curemailaddress":Ljava/lang/String;
    .end local v12    # "curfirstname":Ljava/lang/String;
    .end local v13    # "curhomephone":Ljava/lang/String;
    .end local v14    # "curlastname":Ljava/lang/String;
    .end local v15    # "curmobilephone":Ljava/lang/String;
    .end local v16    # "curoffice":Ljava/lang/String;
    .end local v17    # "curpictureBytes":[B
    .end local v18    # "curpictureData":Ljava/lang/String;
    .end local v20    # "curthumbnailUriAsString":Ljava/lang/String;
    .end local v21    # "curtitle":Ljava/lang/String;
    .end local v22    # "curworkphone":Ljava/lang/String;
    .end local v23    # "defaultPatitionCursorChanged":Z
    .end local v24    # "displaynameIndex":I
    .end local v28    # "e3":Ljava/lang/Exception;
    .end local v29    # "emailaddressIndex":I
    .end local v30    # "firstnameIndex":I
    .end local v31    # "homephoneIndex":I
    .end local v32    # "imageIndex":I
    .end local v33    # "imageIndex1":I
    .end local v34    # "imageIndex2":I
    .end local v35    # "imageIndex3":I
    .end local v37    # "lastnameIndex":I
    .end local v40    # "mobilephoneIndex":I
    .end local v48    # "nowFounded":Z
    .end local v49    # "officeIndex":I
    .end local v50    # "partition":I
    .end local v52    # "titleIndex":I
    .end local v53    # "workphoneIndex":I
    .restart local v25    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v26

    .line 1946
    .restart local v26    # "e1":Ljava/lang/Exception;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_8

    .line 1950
    .end local v26    # "e1":Ljava/lang/Exception;
    :catch_6
    move-exception v27

    .line 1951
    .restart local v27    # "e2":Ljava/lang/Exception;
    invoke-virtual/range {v27 .. v27}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_9

    .line 1945
    .end local v25    # "e":Ljava/lang/Exception;
    .end local v27    # "e2":Ljava/lang/Exception;
    :catch_7
    move-exception v26

    .line 1946
    .restart local v26    # "e1":Ljava/lang/Exception;
    invoke-virtual/range {v26 .. v26}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_b

    .line 1950
    .end local v26    # "e1":Ljava/lang/Exception;
    :catch_8
    move-exception v27

    .line 1951
    .restart local v27    # "e2":Ljava/lang/Exception;
    invoke-virtual/range {v27 .. v27}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_c

    .line 1955
    .end local v27    # "e2":Ljava/lang/Exception;
    :catch_9
    move-exception v28

    .line 1956
    .restart local v28    # "e3":Ljava/lang/Exception;
    invoke-virtual/range {v28 .. v28}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_d

    .line 1943
    .end local v28    # "e3":Ljava/lang/Exception;
    :catchall_1
    move-exception v54

    goto/16 :goto_a

    .end local v42    # "newCursor1":Landroid/database/MatrixCursor;
    .restart local v43    # "newCursor1":Landroid/database/MatrixCursor;
    :catchall_2
    move-exception v54

    move-object/from16 v42, v43

    .end local v43    # "newCursor1":Landroid/database/MatrixCursor;
    .restart local v42    # "newCursor1":Landroid/database/MatrixCursor;
    goto/16 :goto_a

    .end local v42    # "newCursor1":Landroid/database/MatrixCursor;
    .end local v44    # "newCursor2":Landroid/database/MatrixCursor;
    .restart local v43    # "newCursor1":Landroid/database/MatrixCursor;
    .restart local v45    # "newCursor2":Landroid/database/MatrixCursor;
    :catchall_3
    move-exception v54

    move-object/from16 v44, v45

    .end local v45    # "newCursor2":Landroid/database/MatrixCursor;
    .restart local v44    # "newCursor2":Landroid/database/MatrixCursor;
    move-object/from16 v42, v43

    .end local v43    # "newCursor1":Landroid/database/MatrixCursor;
    .restart local v42    # "newCursor1":Landroid/database/MatrixCursor;
    goto/16 :goto_a

    .end local v38    # "mergeResultCursor":Landroid/database/MergeCursor;
    .end local v42    # "newCursor1":Landroid/database/MatrixCursor;
    .end local v44    # "newCursor2":Landroid/database/MatrixCursor;
    .end local v46    # "newCursor3":Landroid/database/MatrixCursor;
    .restart local v4    # "aliasIndex":I
    .restart local v5    # "companyIndex":I
    .restart local v6    # "curalias":Ljava/lang/String;
    .restart local v7    # "curcompany":Ljava/lang/String;
    .restart local v8    # "curdisplayName":Ljava/lang/String;
    .restart local v9    # "curdisplayname":Ljava/lang/String;
    .restart local v10    # "curemailAddress":Ljava/lang/String;
    .restart local v11    # "curemailaddress":Ljava/lang/String;
    .restart local v12    # "curfirstname":Ljava/lang/String;
    .restart local v13    # "curhomephone":Ljava/lang/String;
    .restart local v14    # "curlastname":Ljava/lang/String;
    .restart local v15    # "curmobilephone":Ljava/lang/String;
    .restart local v16    # "curoffice":Ljava/lang/String;
    .restart local v17    # "curpictureBytes":[B
    .restart local v18    # "curpictureData":Ljava/lang/String;
    .restart local v20    # "curthumbnailUriAsString":Ljava/lang/String;
    .restart local v21    # "curtitle":Ljava/lang/String;
    .restart local v22    # "curworkphone":Ljava/lang/String;
    .restart local v23    # "defaultPatitionCursorChanged":Z
    .restart local v24    # "displaynameIndex":I
    .restart local v29    # "emailaddressIndex":I
    .restart local v30    # "firstnameIndex":I
    .restart local v31    # "homephoneIndex":I
    .restart local v32    # "imageIndex":I
    .restart local v33    # "imageIndex1":I
    .restart local v34    # "imageIndex2":I
    .restart local v35    # "imageIndex3":I
    .restart local v37    # "lastnameIndex":I
    .restart local v39    # "mergeResultCursor":Landroid/database/MergeCursor;
    .restart local v40    # "mobilephoneIndex":I
    .restart local v43    # "newCursor1":Landroid/database/MatrixCursor;
    .restart local v45    # "newCursor2":Landroid/database/MatrixCursor;
    .restart local v47    # "newCursor3":Landroid/database/MatrixCursor;
    .restart local v48    # "nowFounded":Z
    .restart local v49    # "officeIndex":I
    .restart local v50    # "partition":I
    .restart local v52    # "titleIndex":I
    .restart local v53    # "workphoneIndex":I
    :catchall_4
    move-exception v54

    move-object/from16 v46, v47

    .end local v47    # "newCursor3":Landroid/database/MatrixCursor;
    .restart local v46    # "newCursor3":Landroid/database/MatrixCursor;
    move-object/from16 v44, v45

    .end local v45    # "newCursor2":Landroid/database/MatrixCursor;
    .restart local v44    # "newCursor2":Landroid/database/MatrixCursor;
    move-object/from16 v42, v43

    .end local v43    # "newCursor1":Landroid/database/MatrixCursor;
    .restart local v42    # "newCursor1":Landroid/database/MatrixCursor;
    move-object/from16 v38, v39

    .end local v39    # "mergeResultCursor":Landroid/database/MergeCursor;
    .restart local v38    # "mergeResultCursor":Landroid/database/MergeCursor;
    goto/16 :goto_a

    .line 1940
    .end local v4    # "aliasIndex":I
    .end local v5    # "companyIndex":I
    .end local v6    # "curalias":Ljava/lang/String;
    .end local v7    # "curcompany":Ljava/lang/String;
    .end local v8    # "curdisplayName":Ljava/lang/String;
    .end local v9    # "curdisplayname":Ljava/lang/String;
    .end local v10    # "curemailAddress":Ljava/lang/String;
    .end local v11    # "curemailaddress":Ljava/lang/String;
    .end local v12    # "curfirstname":Ljava/lang/String;
    .end local v13    # "curhomephone":Ljava/lang/String;
    .end local v14    # "curlastname":Ljava/lang/String;
    .end local v15    # "curmobilephone":Ljava/lang/String;
    .end local v16    # "curoffice":Ljava/lang/String;
    .end local v17    # "curpictureBytes":[B
    .end local v18    # "curpictureData":Ljava/lang/String;
    .end local v20    # "curthumbnailUriAsString":Ljava/lang/String;
    .end local v21    # "curtitle":Ljava/lang/String;
    .end local v22    # "curworkphone":Ljava/lang/String;
    .end local v23    # "defaultPatitionCursorChanged":Z
    .end local v24    # "displaynameIndex":I
    .end local v29    # "emailaddressIndex":I
    .end local v30    # "firstnameIndex":I
    .end local v31    # "homephoneIndex":I
    .end local v32    # "imageIndex":I
    .end local v33    # "imageIndex1":I
    .end local v34    # "imageIndex2":I
    .end local v35    # "imageIndex3":I
    .end local v37    # "lastnameIndex":I
    .end local v40    # "mobilephoneIndex":I
    .end local v48    # "nowFounded":Z
    .end local v49    # "officeIndex":I
    .end local v50    # "partition":I
    .end local v52    # "titleIndex":I
    .end local v53    # "workphoneIndex":I
    :catch_a
    move-exception v25

    goto/16 :goto_7

    .end local v42    # "newCursor1":Landroid/database/MatrixCursor;
    .restart local v43    # "newCursor1":Landroid/database/MatrixCursor;
    :catch_b
    move-exception v25

    move-object/from16 v42, v43

    .end local v43    # "newCursor1":Landroid/database/MatrixCursor;
    .restart local v42    # "newCursor1":Landroid/database/MatrixCursor;
    goto/16 :goto_7

    .end local v42    # "newCursor1":Landroid/database/MatrixCursor;
    .end local v44    # "newCursor2":Landroid/database/MatrixCursor;
    .restart local v43    # "newCursor1":Landroid/database/MatrixCursor;
    .restart local v45    # "newCursor2":Landroid/database/MatrixCursor;
    :catch_c
    move-exception v25

    move-object/from16 v44, v45

    .end local v45    # "newCursor2":Landroid/database/MatrixCursor;
    .restart local v44    # "newCursor2":Landroid/database/MatrixCursor;
    move-object/from16 v42, v43

    .end local v43    # "newCursor1":Landroid/database/MatrixCursor;
    .restart local v42    # "newCursor1":Landroid/database/MatrixCursor;
    goto/16 :goto_7

    .end local v38    # "mergeResultCursor":Landroid/database/MergeCursor;
    .end local v42    # "newCursor1":Landroid/database/MatrixCursor;
    .end local v44    # "newCursor2":Landroid/database/MatrixCursor;
    .end local v46    # "newCursor3":Landroid/database/MatrixCursor;
    .restart local v4    # "aliasIndex":I
    .restart local v5    # "companyIndex":I
    .restart local v6    # "curalias":Ljava/lang/String;
    .restart local v7    # "curcompany":Ljava/lang/String;
    .restart local v8    # "curdisplayName":Ljava/lang/String;
    .restart local v9    # "curdisplayname":Ljava/lang/String;
    .restart local v10    # "curemailAddress":Ljava/lang/String;
    .restart local v11    # "curemailaddress":Ljava/lang/String;
    .restart local v12    # "curfirstname":Ljava/lang/String;
    .restart local v13    # "curhomephone":Ljava/lang/String;
    .restart local v14    # "curlastname":Ljava/lang/String;
    .restart local v15    # "curmobilephone":Ljava/lang/String;
    .restart local v16    # "curoffice":Ljava/lang/String;
    .restart local v17    # "curpictureBytes":[B
    .restart local v18    # "curpictureData":Ljava/lang/String;
    .restart local v20    # "curthumbnailUriAsString":Ljava/lang/String;
    .restart local v21    # "curtitle":Ljava/lang/String;
    .restart local v22    # "curworkphone":Ljava/lang/String;
    .restart local v23    # "defaultPatitionCursorChanged":Z
    .restart local v24    # "displaynameIndex":I
    .restart local v29    # "emailaddressIndex":I
    .restart local v30    # "firstnameIndex":I
    .restart local v31    # "homephoneIndex":I
    .restart local v32    # "imageIndex":I
    .restart local v33    # "imageIndex1":I
    .restart local v34    # "imageIndex2":I
    .restart local v35    # "imageIndex3":I
    .restart local v37    # "lastnameIndex":I
    .restart local v39    # "mergeResultCursor":Landroid/database/MergeCursor;
    .restart local v40    # "mobilephoneIndex":I
    .restart local v43    # "newCursor1":Landroid/database/MatrixCursor;
    .restart local v45    # "newCursor2":Landroid/database/MatrixCursor;
    .restart local v47    # "newCursor3":Landroid/database/MatrixCursor;
    .restart local v48    # "nowFounded":Z
    .restart local v49    # "officeIndex":I
    .restart local v50    # "partition":I
    .restart local v52    # "titleIndex":I
    .restart local v53    # "workphoneIndex":I
    :catch_d
    move-exception v25

    move-object/from16 v46, v47

    .end local v47    # "newCursor3":Landroid/database/MatrixCursor;
    .restart local v46    # "newCursor3":Landroid/database/MatrixCursor;
    move-object/from16 v44, v45

    .end local v45    # "newCursor2":Landroid/database/MatrixCursor;
    .restart local v44    # "newCursor2":Landroid/database/MatrixCursor;
    move-object/from16 v42, v43

    .end local v43    # "newCursor1":Landroid/database/MatrixCursor;
    .restart local v42    # "newCursor1":Landroid/database/MatrixCursor;
    move-object/from16 v38, v39

    .end local v39    # "mergeResultCursor":Landroid/database/MergeCursor;
    .restart local v38    # "mergeResultCursor":Landroid/database/MergeCursor;
    goto/16 :goto_7
.end method

.method private isLoading(I)Z
    .locals 1
    .param p1, "partitionIndex"    # I

    .prologue
    .line 1125
    invoke-virtual {p0, p1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getPartition(I)Lcom/android/common/widget/CompositeCursorAdapter$Partition;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;

    iget-boolean v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->loading:Z

    return v0
.end method

.method private final makeDisplayString(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v4, 0x0

    .line 1964
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "searching"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1965
    const-string v1, ""

    .line 1973
    :cond_0
    :goto_0
    return-object v1

    .line 1968
    :cond_1
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1969
    .local v0, "displayName":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1970
    .local v1, "emailAddress":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1973
    new-instance v2, Landroid/text/util/Rfc822Token;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Landroid/text/util/Rfc822Token;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/text/util/Rfc822Token;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private removeDuplicatesAndTruncate(ILandroid/database/Cursor;)Landroid/database/Cursor;
    .locals 34
    .param p1, "partition"    # I
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1401
    invoke-virtual/range {p0 .. p1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getPartition(I)Lcom/android/common/widget/CompositeCursorAdapter$Partition;

    move-result-object v22

    check-cast v22, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;

    .line 1402
    .local v22, "curpartition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    const/4 v3, 0x0

    move-object/from16 v0, v22

    iput-boolean v3, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->moreResultIsRemained:Z

    .line 1404
    if-nez p2, :cond_0

    .line 1405
    const-string v3, "SecBaseEmailAddressAdapter"

    const-string v4, "addShowMoreButtonAtlastPatition : cursor is null"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1406
    const/16 p2, 0x0

    .line 1508
    .end local p2    # "cursor":Landroid/database/Cursor;
    :goto_0
    return-object p2

    .line 1409
    .restart local p2    # "cursor":Landroid/database/Cursor;
    :cond_0
    sget-boolean v3, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->ExpandResultMaxAndShowMoreMode:Z

    if-nez v3, :cond_1

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    const/16 v4, 0xa

    if-gt v3, v4, :cond_1

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->hasDuplicates(Landroid/database/Cursor;I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1412
    const-string v3, "SecBaseEmailAddressAdapter"

    const-string v4, "addShowMoreButtonAtlastPatition : this partition has no duplication."

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1416
    :cond_1
    const/16 v21, 0x0

    .line 1419
    .local v21, "count":I
    new-instance v30, Landroid/database/MatrixCursor;

    sget-object v3, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, v30

    invoke-direct {v0, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1421
    .local v30, "newCursor":Landroid/database/MatrixCursor;
    const/4 v3, -0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1423
    :cond_2
    :goto_1
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_11

    sget-boolean v3, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->ExpandResultMaxAndShowMoreMode:Z

    if-nez v3, :cond_3

    const/16 v3, 0xa

    move/from16 v0, v21

    if-ge v0, v3, :cond_11

    .line 1424
    :cond_3
    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1425
    .local v5, "displayName":Ljava/lang/String;
    const/4 v3, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1426
    .local v6, "emailAddress":Ljava/lang/String;
    const/4 v7, 0x0

    .line 1427
    .local v7, "pictureData":Ljava/lang/String;
    const/4 v8, 0x0

    .line 1428
    .local v8, "displayname":Ljava/lang/String;
    const/4 v9, 0x0

    .line 1429
    .local v9, "emailaddress":Ljava/lang/String;
    const/4 v10, 0x0

    .line 1430
    .local v10, "workphone":Ljava/lang/String;
    const/4 v11, 0x0

    .line 1431
    .local v11, "homephone":Ljava/lang/String;
    const/4 v12, 0x0

    .line 1432
    .local v12, "mobilephone":Ljava/lang/String;
    const/4 v13, 0x0

    .line 1433
    .local v13, "firstname":Ljava/lang/String;
    const/4 v14, 0x0

    .line 1434
    .local v14, "lastname":Ljava/lang/String;
    const/4 v15, 0x0

    .line 1435
    .local v15, "company":Ljava/lang/String;
    const/16 v16, 0x0

    .line 1436
    .local v16, "title":Ljava/lang/String;
    const/16 v17, 0x0

    .line 1437
    .local v17, "office":Ljava/lang/String;
    const/16 v18, 0x0

    .line 1439
    .local v18, "alias":Ljava/lang/String;
    sget-object v3, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v27

    .line 1440
    .local v27, "imageIndex":I
    sget-object v3, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 1441
    .local v23, "displaynameIndex":I
    sget-object v3, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x4

    aget-object v3, v3, v4

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 1442
    .local v24, "emailaddressIndex":I
    sget-object v3, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x5

    aget-object v3, v3, v4

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v33

    .line 1443
    .local v33, "workphoneIndex":I
    sget-object v3, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x6

    aget-object v3, v3, v4

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 1444
    .local v26, "homephoneIndex":I
    sget-object v3, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x7

    aget-object v3, v3, v4

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    .line 1445
    .local v29, "mobilephoneIndex":I
    sget-object v3, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v4, 0x8

    aget-object v3, v3, v4

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 1446
    .local v25, "firstnameIndex":I
    sget-object v3, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v4, 0x9

    aget-object v3, v3, v4

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v28

    .line 1447
    .local v28, "lastnameIndex":I
    sget-object v3, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v4, 0xa

    aget-object v3, v3, v4

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 1448
    .local v20, "companyIndex":I
    sget-object v3, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v4, 0xb

    aget-object v3, v3, v4

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    .line 1449
    .local v32, "titleIndex":I
    sget-object v3, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v4, 0xc

    aget-object v3, v3, v4

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    .line 1450
    .local v31, "officeIndex":I
    sget-object v3, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery1_1;->PROJECTION:[Ljava/lang/String;

    const/16 v4, 0xd

    aget-object v3, v3, v4

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 1452
    .local v19, "aliasIndex":I
    const-string v3, "gal_search_show_more"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1453
    const/4 v3, 0x1

    move-object/from16 v0, v22

    iput-boolean v3, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->moreResultIsRemained:Z

    goto/16 :goto_1

    .line 1457
    :cond_4
    const/4 v3, -0x1

    move/from16 v0, v27

    if-eq v0, v3, :cond_5

    .line 1458
    move-object/from16 v0, p2

    move/from16 v1, v27

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1460
    :cond_5
    const/4 v3, -0x1

    move/from16 v0, v23

    if-eq v0, v3, :cond_6

    .line 1461
    move-object/from16 v0, p2

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1463
    :cond_6
    const/4 v3, -0x1

    move/from16 v0, v24

    if-eq v0, v3, :cond_7

    .line 1464
    move-object/from16 v0, p2

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1466
    :cond_7
    const/4 v3, -0x1

    move/from16 v0, v33

    if-eq v0, v3, :cond_8

    .line 1467
    move-object/from16 v0, p2

    move/from16 v1, v33

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1469
    :cond_8
    const/4 v3, -0x1

    move/from16 v0, v26

    if-eq v0, v3, :cond_9

    .line 1470
    move-object/from16 v0, p2

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1472
    :cond_9
    const/4 v3, -0x1

    move/from16 v0, v29

    if-eq v0, v3, :cond_a

    .line 1473
    move-object/from16 v0, p2

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1475
    :cond_a
    const/4 v3, -0x1

    move/from16 v0, v25

    if-eq v0, v3, :cond_b

    .line 1476
    move-object/from16 v0, p2

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 1478
    :cond_b
    const/4 v3, -0x1

    move/from16 v0, v28

    if-eq v0, v3, :cond_c

    .line 1479
    move-object/from16 v0, p2

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 1481
    :cond_c
    const/4 v3, -0x1

    move/from16 v0, v20

    if-eq v0, v3, :cond_d

    .line 1482
    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1484
    :cond_d
    const/4 v3, -0x1

    move/from16 v0, v32

    if-eq v0, v3, :cond_e

    .line 1485
    move-object/from16 v0, p2

    move/from16 v1, v32

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1487
    :cond_e
    const/4 v3, -0x1

    move/from16 v0, v31

    if-eq v0, v3, :cond_f

    .line 1488
    move-object/from16 v0, p2

    move/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 1490
    :cond_f
    const/4 v3, -0x1

    move/from16 v0, v19

    if-eq v0, v3, :cond_10

    .line 1491
    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    :cond_10
    move-object/from16 v3, p0

    move/from16 v4, p1

    .line 1494
    invoke-direct/range {v3 .. v18}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->isDuplicateAddPictureDate(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1497
    const/16 v3, 0xe

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v6, v3, v4

    const/4 v4, 0x2

    aput-object v7, v3, v4

    const/4 v4, 0x3

    aput-object v8, v3, v4

    const/4 v4, 0x4

    aput-object v9, v3, v4

    const/4 v4, 0x5

    aput-object v10, v3, v4

    const/4 v4, 0x6

    aput-object v11, v3, v4

    const/4 v4, 0x7

    aput-object v12, v3, v4

    const/16 v4, 0x8

    aput-object v13, v3, v4

    const/16 v4, 0x9

    aput-object v14, v3, v4

    const/16 v4, 0xa

    aput-object v15, v3, v4

    const/16 v4, 0xb

    aput-object v16, v3, v4

    const/16 v4, 0xc

    aput-object v17, v3, v4

    const/16 v4, 0xd

    aput-object v18, v3, v4

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 1502
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_1

    .line 1506
    .end local v5    # "displayName":Ljava/lang/String;
    .end local v6    # "emailAddress":Ljava/lang/String;
    .end local v7    # "pictureData":Ljava/lang/String;
    .end local v8    # "displayname":Ljava/lang/String;
    .end local v9    # "emailaddress":Ljava/lang/String;
    .end local v10    # "workphone":Ljava/lang/String;
    .end local v11    # "homephone":Ljava/lang/String;
    .end local v12    # "mobilephone":Ljava/lang/String;
    .end local v13    # "firstname":Ljava/lang/String;
    .end local v14    # "lastname":Ljava/lang/String;
    .end local v15    # "company":Ljava/lang/String;
    .end local v16    # "title":Ljava/lang/String;
    .end local v17    # "office":Ljava/lang/String;
    .end local v18    # "alias":Ljava/lang/String;
    .end local v19    # "aliasIndex":I
    .end local v20    # "companyIndex":I
    .end local v23    # "displaynameIndex":I
    .end local v24    # "emailaddressIndex":I
    .end local v25    # "firstnameIndex":I
    .end local v26    # "homephoneIndex":I
    .end local v27    # "imageIndex":I
    .end local v28    # "lastnameIndex":I
    .end local v29    # "mobilephoneIndex":I
    .end local v31    # "officeIndex":I
    .end local v32    # "titleIndex":I
    .end local v33    # "workphoneIndex":I
    :cond_11
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->close()V

    move-object/from16 p2, v30

    .line 1508
    goto/16 :goto_0
.end method

.method private removeDuplicatesEmailQueryCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/MatrixCursor;
    .locals 13
    .param p1, "baseCursor"    # Landroid/database/Cursor;
    .param p2, "refCursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v8, 0x0

    const/4 v12, 0x2

    const/4 v0, 0x1

    const/4 v10, 0x0

    .line 528
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v11

    if-gtz v11, :cond_1

    .line 530
    :try_start_0
    invoke-interface {p2}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 531
    const/4 p2, 0x0

    .line 622
    :cond_0
    :goto_0
    return-object v8

    .line 532
    :catch_0
    move-exception v7

    .line 533
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 537
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_1
    if-eqz p2, :cond_0

    .line 541
    const/4 v8, 0x0

    .line 543
    .local v8, "newCursor":Landroid/database/MatrixCursor;
    sget-object v11, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery3;->PROJECTION:[Ljava/lang/String;

    aget-object v11, v11, v12

    invoke-interface {p2, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 545
    .local v3, "cacheImageIndex":I
    if-gez v3, :cond_8

    .line 546
    new-instance v8, Landroid/database/MatrixCursor;

    .end local v8    # "newCursor":Landroid/database/MatrixCursor;
    sget-object v11, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v8, v11}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 552
    .restart local v8    # "newCursor":Landroid/database/MatrixCursor;
    :goto_1
    if-eqz p1, :cond_9

    :try_start_1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v11

    if-lez v11, :cond_9

    .line 554
    .local v0, "baseExist":Z
    :goto_2
    const/4 v10, -0x1

    invoke-interface {p2, v10}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 555
    :cond_2
    :goto_3
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-eqz v10, :cond_e

    .line 556
    const/4 v10, 0x0

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 557
    .local v6, "curdisplayName":Ljava/lang/String;
    const/4 v10, 0x1

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 558
    .local v4, "curaddress":Ljava/lang/String;
    const/4 v5, 0x0

    .line 561
    .local v5, "curcontentbytes":[B
    if-lez v3, :cond_3

    .line 562
    const/4 v10, 0x2

    :try_start_2
    invoke-interface {p2, v10}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v5

    .line 568
    :cond_3
    :goto_4
    const/4 v9, 0x0

    .line 570
    .local v9, "remove":Z
    if-eqz v4, :cond_2

    :try_start_3
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_2

    .line 571
    if-eqz v0, :cond_5

    .line 572
    const/4 v10, -0x1

    invoke-interface {p1, v10}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 573
    :cond_4
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 574
    const/4 v10, 0x0

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 575
    .local v2, "basedisplayName":Ljava/lang/String;
    const/4 v10, 0x1

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 576
    .local v1, "baseaddress":Ljava/lang/String;
    if-eqz v1, :cond_4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 577
    const/4 v9, 0x1

    .line 583
    .end local v1    # "baseaddress":Ljava/lang/String;
    .end local v2    # "basedisplayName":Ljava/lang/String;
    :cond_5
    if-nez v9, :cond_2

    .line 587
    if-gez v3, :cond_a

    .line 588
    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v6, v10, v11

    const/4 v11, 0x1

    aput-object v4, v10, v11

    invoke-virtual {v8, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 598
    .end local v0    # "baseExist":Z
    .end local v4    # "curaddress":Ljava/lang/String;
    .end local v5    # "curcontentbytes":[B
    .end local v6    # "curdisplayName":Ljava/lang/String;
    .end local v9    # "remove":Z
    :catch_1
    move-exception v7

    .line 599
    .restart local v7    # "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 601
    if-eqz v8, :cond_6

    invoke-virtual {v8}, Landroid/database/MatrixCursor;->getCount()I

    move-result v10

    if-gtz v10, :cond_7

    .line 602
    :cond_6
    if-eqz v8, :cond_7

    .line 604
    :try_start_5
    invoke-virtual {v8}, Landroid/database/MatrixCursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    .line 605
    const/4 v8, 0x0

    .line 612
    :cond_7
    :goto_5
    if-eqz p2, :cond_0

    .line 614
    :try_start_6
    invoke-interface {p2}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    .line 615
    const/4 p2, 0x0

    goto/16 :goto_0

    .line 548
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_8
    new-instance v8, Landroid/database/MatrixCursor;

    .end local v8    # "newCursor":Landroid/database/MatrixCursor;
    sget-object v11, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery3;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v8, v11}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .restart local v8    # "newCursor":Landroid/database/MatrixCursor;
    goto :goto_1

    :cond_9
    move v0, v10

    .line 552
    goto :goto_2

    .line 564
    .restart local v0    # "baseExist":Z
    .restart local v4    # "curaddress":Ljava/lang/String;
    .restart local v5    # "curcontentbytes":[B
    .restart local v6    # "curdisplayName":Ljava/lang/String;
    :catch_2
    move-exception v7

    .line 565
    .restart local v7    # "e":Ljava/lang/Exception;
    const/4 v5, 0x0

    goto :goto_4

    .line 592
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v9    # "remove":Z
    :cond_a
    const/4 v10, 0x3

    :try_start_7
    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v6, v10, v11

    const/4 v11, 0x1

    aput-object v4, v10, v11

    const/4 v11, 0x2

    aput-object v5, v10, v11

    invoke-virtual {v8, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_3

    .line 601
    .end local v0    # "baseExist":Z
    .end local v4    # "curaddress":Ljava/lang/String;
    .end local v5    # "curcontentbytes":[B
    .end local v6    # "curdisplayName":Ljava/lang/String;
    .end local v9    # "remove":Z
    :catchall_0
    move-exception v10

    if-eqz v8, :cond_b

    invoke-virtual {v8}, Landroid/database/MatrixCursor;->getCount()I

    move-result v11

    if-gtz v11, :cond_c

    .line 602
    :cond_b
    if-eqz v8, :cond_c

    .line 604
    :try_start_8
    invoke-virtual {v8}, Landroid/database/MatrixCursor;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7

    .line 605
    const/4 v8, 0x0

    .line 612
    :cond_c
    :goto_6
    if-eqz p2, :cond_d

    .line 614
    :try_start_9
    invoke-interface {p2}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_8

    .line 615
    const/4 p2, 0x0

    .line 618
    :cond_d
    :goto_7
    throw v10

    .line 601
    .restart local v0    # "baseExist":Z
    :cond_e
    if-eqz v8, :cond_f

    invoke-virtual {v8}, Landroid/database/MatrixCursor;->getCount()I

    move-result v10

    if-gtz v10, :cond_10

    .line 602
    :cond_f
    if-eqz v8, :cond_10

    .line 604
    :try_start_a
    invoke-virtual {v8}, Landroid/database/MatrixCursor;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    .line 605
    const/4 v8, 0x0

    .line 612
    :cond_10
    :goto_8
    if-eqz p2, :cond_0

    .line 614
    :try_start_b
    invoke-interface {p2}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_4

    .line 615
    const/4 p2, 0x0

    goto/16 :goto_0

    .line 606
    :catch_3
    move-exception v7

    .line 607
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_8

    .line 616
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v7

    .line 617
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 606
    .end local v0    # "baseExist":Z
    :catch_5
    move-exception v7

    .line 607
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 616
    :catch_6
    move-exception v7

    .line 617
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 606
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_7
    move-exception v7

    .line 607
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    .line 616
    .end local v7    # "e":Ljava/lang/Exception;
    :catch_8
    move-exception v7

    .line 617
    .restart local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_7
.end method

.method public static setBackupOfSearchedString(Ljava/lang/String;)V
    .locals 0
    .param p0, "backupOfSearchedString"    # Ljava/lang/String;

    .prologue
    .line 1141
    sput-object p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->BackupOfSearchedString:Ljava/lang/String;

    .line 1142
    return-void
.end method


# virtual methods
.method public RIemailAddress(JLjava/lang/CharSequence;)Landroid/database/Cursor;
    .locals 11
    .param p1, "acckey"    # J
    .param p3, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 627
    if-nez p3, :cond_0

    const-string v7, ""

    .line 628
    .local v7, "filter":Ljava/lang/String;
    :goto_0
    const/4 v9, 0x0

    .line 631
    .local v9, "ric":Landroid/database/Cursor;
    :try_start_0
    const-string v0, "utf-8"

    invoke-static {v7, v0}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 632
    .local v8, "inputtext":Ljava/lang/String;
    const-string v0, "-"

    const-string v2, "%2D"

    invoke-virtual {v8, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    .line 633
    sget-object v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->RIC_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 634
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 639
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v8    # "inputtext":Ljava/lang/String;
    :goto_1
    return-object v9

    .line 627
    .end local v7    # "filter":Ljava/lang/String;
    .end local v9    # "ric":Landroid/database/Cursor;
    :cond_0
    invoke-interface {p3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 635
    .restart local v7    # "filter":Ljava/lang/String;
    .restart local v9    # "ric":Landroid/database/Cursor;
    :catch_0
    move-exception v6

    .line 636
    .local v6, "e":Ljava/lang/Exception;
    const/4 v9, 0x0

    goto :goto_1
.end method

.method public getGroupNameCursor(Ljava/lang/CharSequence;)Landroid/database/Cursor;
    .locals 17
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 1978
    new-instance v11, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery_Group;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v11, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1979
    .local v11, "newCursor":Landroid/database/MatrixCursor;
    const/4 v9, 0x0

    .line 1983
    .local v9, "groupCursor":Landroid/database/Cursor;
    if-eqz p1, :cond_4

    .line 1984
    :try_start_0
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1988
    .local v10, "inputText":Ljava/lang/String;
    :goto_0
    const-string v12, ""

    .line 1990
    .local v12, "str":Ljava/lang/String;
    const-string v1, "\'"

    invoke-virtual {v10, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1991
    const-string v1, "utf-8"

    invoke-static {v10, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 1992
    const-string v1, "-"

    const-string v3, "%2D"

    invoke-virtual {v12, v1, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v12

    .line 1996
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v2, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$EmailQuery_Group;->PROJECTION:[Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "(title like \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%%\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1999
    if-eqz v9, :cond_a

    .line 2000
    const/4 v1, -0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 2001
    :cond_0
    :goto_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2002
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 2003
    .local v15, "title":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 2004
    .local v7, "accType":Ljava/lang/String;
    const/4 v1, 0x2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 2007
    .local v13, "systemId":Ljava/lang/String;
    sget-object v1, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "title"

    invoke-virtual {v1, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, v15}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "primary_emails"

    invoke-virtual {v1, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 2010
    .local v2, "groupUri":Landroid/net/Uri;
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2011
    .local v16, "titleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v14, 0x0

    .line 2013
    .local v14, "tempcur":Landroid/database/Cursor;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "data1"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "display_name"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 2018
    if-eqz v14, :cond_1

    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-gtz v1, :cond_6

    .line 2032
    :cond_1
    if-eqz v14, :cond_0

    .line 2033
    :try_start_2
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 2038
    .end local v2    # "groupUri":Landroid/net/Uri;
    .end local v7    # "accType":Ljava/lang/String;
    .end local v10    # "inputText":Ljava/lang/String;
    .end local v12    # "str":Ljava/lang/String;
    .end local v13    # "systemId":Ljava/lang/String;
    .end local v14    # "tempcur":Landroid/database/Cursor;
    .end local v15    # "title":Ljava/lang/String;
    .end local v16    # "titleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v8

    .line 2039
    .local v8, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2040
    const/4 v11, 0x0

    .line 2042
    if-eqz v9, :cond_2

    .line 2043
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 2047
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_3
    if-eqz v11, :cond_3

    invoke-virtual {v11}, Landroid/database/MatrixCursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_3

    .line 2048
    invoke-virtual {v11}, Landroid/database/MatrixCursor;->close()V

    .line 2049
    const/4 v11, 0x0

    .line 2052
    .end local v11    # "newCursor":Landroid/database/MatrixCursor;
    :cond_3
    return-object v11

    .line 1986
    .restart local v11    # "newCursor":Landroid/database/MatrixCursor;
    :cond_4
    :try_start_4
    const-string v10, ""
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .restart local v10    # "inputText":Ljava/lang/String;
    goto/16 :goto_0

    .line 1994
    .restart local v12    # "str":Ljava/lang/String;
    :cond_5
    move-object v12, v10

    goto/16 :goto_1

    .line 2022
    .restart local v2    # "groupUri":Landroid/net/Uri;
    .restart local v7    # "accType":Ljava/lang/String;
    .restart local v13    # "systemId":Ljava/lang/String;
    .restart local v14    # "tempcur":Landroid/database/Cursor;
    .restart local v15    # "title":Ljava/lang/String;
    .restart local v16    # "titleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6
    :try_start_5
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2023
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v15, v1, v3

    const/4 v3, 0x1

    aput-object v7, v1, v3

    const/4 v3, 0x2

    aput-object v13, v1, v3

    invoke-virtual {v11, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 2026
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2032
    :cond_7
    if-eqz v14, :cond_0

    .line 2033
    :try_start_6
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_2

    .line 2042
    .end local v2    # "groupUri":Landroid/net/Uri;
    .end local v7    # "accType":Ljava/lang/String;
    .end local v10    # "inputText":Ljava/lang/String;
    .end local v12    # "str":Ljava/lang/String;
    .end local v13    # "systemId":Ljava/lang/String;
    .end local v14    # "tempcur":Landroid/database/Cursor;
    .end local v15    # "title":Ljava/lang/String;
    .end local v16    # "titleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catchall_0
    move-exception v1

    if-eqz v9, :cond_8

    .line 2043
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v1

    .line 2029
    .restart local v2    # "groupUri":Landroid/net/Uri;
    .restart local v7    # "accType":Ljava/lang/String;
    .restart local v10    # "inputText":Ljava/lang/String;
    .restart local v12    # "str":Ljava/lang/String;
    .restart local v13    # "systemId":Ljava/lang/String;
    .restart local v14    # "tempcur":Landroid/database/Cursor;
    .restart local v15    # "title":Ljava/lang/String;
    .restart local v16    # "titleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_1
    move-exception v8

    .line 2030
    .restart local v8    # "e":Ljava/lang/Exception;
    :try_start_7
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2032
    if-eqz v14, :cond_0

    .line 2033
    :try_start_8
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 2032
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v1

    if-eqz v14, :cond_9

    .line 2033
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v1
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 2042
    .end local v2    # "groupUri":Landroid/net/Uri;
    .end local v7    # "accType":Ljava/lang/String;
    .end local v13    # "systemId":Ljava/lang/String;
    .end local v14    # "tempcur":Landroid/database/Cursor;
    .end local v15    # "title":Ljava/lang/String;
    .end local v16    # "titleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_a
    if-eqz v9, :cond_2

    .line 2043
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_3
.end method

.method protected onDirectoryLoadFinished(Ljava/lang/CharSequence;Landroid/database/Cursor;Landroid/database/Cursor;)V
    .locals 24
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "directoryCursor"    # Landroid/database/Cursor;
    .param p3, "defaultPartitionCursor"    # Landroid/database/Cursor;

    .prologue
    .line 1151
    if-eqz p2, :cond_5

    .line 1152
    invoke-virtual/range {p0 .. p0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getContext()Landroid/content/Context;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    .line 1153
    .local v14, "packageManager":Landroid/content/pm/PackageManager;
    const/16 v17, 0x0

    .line 1154
    .local v17, "preferredDirectory":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1155
    .local v6, "directories":Ljava/util/List;, "Ljava/util/List<Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;>;"
    :cond_0
    :goto_0
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v20

    if-eqz v20, :cond_3

    .line 1156
    const/16 v20, 0x0

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 1160
    .local v10, "id":J
    const-wide/16 v20, 0x1

    cmp-long v20, v10, v20

    if-eqz v20, :cond_0

    .line 1164
    new-instance v16, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;

    invoke-direct/range {v16 .. v16}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;-><init>()V

    .line 1165
    .local v16, "partition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    move-object/from16 v0, v16

    iput-wide v10, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->directoryId:J

    .line 1166
    const/16 v20, 0x3

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->displayName:Ljava/lang/String;

    .line 1167
    const/16 v20, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->accountName:Ljava/lang/String;

    .line 1168
    const/16 v20, 0x2

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->accountType:Ljava/lang/String;

    .line 1169
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v16

    iput-boolean v0, v1, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->moreResultIsRemained:Z

    .line 1170
    const/16 v20, 0x4

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1171
    .local v15, "packageName":Ljava/lang/String;
    const/16 v20, 0x5

    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 1172
    .local v18, "resourceId":I
    if-eqz v15, :cond_1

    if-eqz v18, :cond_1

    .line 1174
    :try_start_0
    invoke-virtual {v14, v15}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v19

    .line 1176
    .local v19, "resources":Landroid/content/res/Resources;
    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->directoryType:Ljava/lang/String;

    .line 1177
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->directoryType:Ljava/lang/String;

    move-object/from16 v20, v0

    if-nez v20, :cond_1

    .line 1178
    const-string v20, "SecBaseEmailAddressAdapter"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Cannot resolve directory name: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "@"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1190
    .end local v19    # "resources":Landroid/content/res/Resources;
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mAccount:Landroid/accounts/Account;

    move-object/from16 v20, v0

    if-eqz v20, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mAccount:Landroid/accounts/Account;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->accountName:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mAccount:Landroid/accounts/Account;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->accountType:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 1192
    move-object/from16 v17, v16

    goto/16 :goto_0

    .line 1181
    :catch_0
    move-exception v7

    .line 1182
    .local v7, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v20, "SecBaseEmailAddressAdapter"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Cannot resolve directory name: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "@"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v0, v1, v7}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1194
    .end local v7    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    move-object/from16 v0, v16

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1198
    .end local v10    # "id":J
    .end local v15    # "packageName":Ljava/lang/String;
    .end local v16    # "partition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    .end local v18    # "resourceId":I
    :cond_3
    if-eqz v17, :cond_4

    .line 1199
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, v17

    invoke-interface {v6, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1202
    :cond_4
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;

    .line 1203
    .restart local v16    # "partition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->addPartition(Lcom/android/common/widget/CompositeCursorAdapter$Partition;)V

    goto :goto_2

    .line 1207
    .end local v6    # "directories":Ljava/util/List;, "Ljava/util/List<Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;>;"
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v14    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v16    # "partition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    .end local v17    # "preferredDirectory":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getPartitionCount()I

    move-result v4

    .line 1208
    .local v4, "count":I
    const/4 v12, 0x0

    .line 1212
    .local v12, "limit":I
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->setNotificationsEnabled(Z)V

    .line 1215
    if-eqz p3, :cond_6

    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getPartitionCount()I

    move-result v20

    if-lez v20, :cond_6

    .line 1216
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->changeCursor(ILandroid/database/Cursor;)V

    .line 1219
    :cond_6
    if-nez p3, :cond_9

    const/4 v5, 0x0

    .line 1222
    .local v5, "defaultPartitionCount":I
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mPreferredMaxResultCount:I

    move/from16 v20, v0

    sub-int v12, v20, v5

    .line 1226
    const/4 v8, 0x1

    .local v8, "i":I
    :goto_4
    if-ge v8, v4, :cond_b

    .line 1227
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getPartition(I)Lcom/android/common/widget/CompositeCursorAdapter$Partition;

    move-result-object v16

    check-cast v16, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;

    .line 1228
    .restart local v16    # "partition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->constraint:Ljava/lang/CharSequence;

    .line 1230
    if-gtz v12, :cond_7

    sget-boolean v20, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->ExpandResultMaxAndShowMoreMode:Z

    if-eqz v20, :cond_a

    .line 1231
    :cond_7
    move-object/from16 v0, v16

    iget-boolean v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->loading:Z

    move/from16 v20, v0

    if-nez v20, :cond_8

    .line 1232
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, v16

    iput-boolean v0, v1, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->loading:Z

    .line 1233
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v8, v1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->changeCursor(ILandroid/database/Cursor;)V

    .line 1226
    :cond_8
    :goto_5
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 1219
    .end local v5    # "defaultPartitionCount":I
    .end local v8    # "i":I
    .end local v16    # "partition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    :cond_9
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v5

    goto :goto_3

    .line 1236
    .restart local v5    # "defaultPartitionCount":I
    .restart local v8    # "i":I
    .restart local v16    # "partition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    :cond_a
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v16

    iput-boolean v0, v1, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->loading:Z

    .line 1237
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v8, v1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->changeCursor(ILandroid/database/Cursor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    .line 1241
    .end local v5    # "defaultPartitionCount":I
    .end local v8    # "i":I
    .end local v16    # "partition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    :catchall_0
    move-exception v20

    const/16 v21, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->setNotificationsEnabled(Z)V

    throw v20

    .restart local v5    # "defaultPartitionCount":I
    .restart local v8    # "i":I
    :cond_b
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->setNotificationsEnabled(Z)V

    .line 1246
    const/4 v8, 0x1

    :goto_6
    if-ge v8, v4, :cond_11

    .line 1247
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getPartition(I)Lcom/android/common/widget/CompositeCursorAdapter$Partition;

    move-result-object v16

    check-cast v16, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;

    .line 1250
    .restart local v16    # "partition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->isOnlineSearchDisabled:Z

    move/from16 v20, v0

    if-eqz v20, :cond_d

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->accountType:Ljava/lang/String;

    move-object/from16 v20, v0

    if-eqz v20, :cond_d

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->accountType:Ljava/lang/String;

    move-object/from16 v20, v0

    const-string v21, "com.android.exchange"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 1246
    :cond_c
    :goto_7
    add-int/lit8 v8, v8, 0x1

    goto :goto_6

    .line 1255
    :cond_d
    move-object/from16 v0, v16

    iget-boolean v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->loading:Z

    move/from16 v20, v0

    if-eqz v20, :cond_10

    .line 1256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mHandler:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 1257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mHandler:Landroid/os/Handler;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move-object/from16 v3, v16

    invoke-virtual {v0, v1, v8, v2, v3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v13

    .line 1258
    .local v13, "msg":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mHandler:Landroid/os/Handler;

    move-object/from16 v20, v0

    const-wide/16 v22, 0x3e8

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v13, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1259
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->filter:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartitionFilter;

    move-object/from16 v20, v0

    if-nez v20, :cond_e

    .line 1260
    new-instance v20, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartitionFilter;

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->directoryId:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-wide/from16 v2, v22

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartitionFilter;-><init>(Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;IJ)V

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->filter:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartitionFilter;

    .line 1262
    :cond_e
    sget-boolean v20, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->ExpandResultMaxAndShowMoreMode:Z

    if-eqz v20, :cond_f

    .line 1264
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->filter:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartitionFilter;

    move-object/from16 v20, v0

    const/16 v21, 0x14

    invoke-virtual/range {v20 .. v21}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartitionFilter;->setLimit(I)V

    .line 1270
    :goto_8
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->filter:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartitionFilter;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartitionFilter;->filter(Ljava/lang/CharSequence;)V

    goto :goto_7

    .line 1268
    :cond_f
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->filter:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartitionFilter;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartitionFilter;->setLimit(I)V

    goto :goto_8

    .line 1272
    .end local v13    # "msg":Landroid/os/Message;
    :cond_10
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->filter:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartitionFilter;

    move-object/from16 v20, v0

    if-eqz v20, :cond_c

    .line 1274
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->filter:Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartitionFilter;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartitionFilter;->filter(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 1279
    .end local v16    # "partition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    :cond_11
    if-eqz p2, :cond_12

    .line 1280
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->close()V

    .line 1281
    :cond_12
    return-void
.end method

.method public onPartitionLoadFinished(Ljava/lang/CharSequence;ILandroid/database/Cursor;)V
    .locals 4
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "partitionIndex"    # I
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v3, 0x1

    .line 1338
    invoke-virtual {p0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getPartitionCount()I

    move-result v1

    if-ge p2, v1, :cond_5

    .line 1339
    invoke-virtual {p0, p2}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->getPartition(I)Lcom/android/common/widget/CompositeCursorAdapter$Partition;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;

    .line 1344
    .local v0, "partition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    iget-boolean v1, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->loading:Z

    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->constraint:Ljava/lang/CharSequence;

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1346
    iget-object v1, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3, v0}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 1347
    if-eqz p1, :cond_0

    .line 1348
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->setBackupOfSearchedString(Ljava/lang/String;)V

    .line 1349
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->removeDuplicatesAndTruncate(ILandroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {p0, p2, v1}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->changeCursor(ILandroid/database/Cursor;)V

    .line 1350
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;->loading:Z

    .line 1352
    sget-boolean v1, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->ExpandResultMaxAndShowMoreMode:Z

    if-eqz v1, :cond_2

    .line 1354
    iget-object v2, p0, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->mSynchronizer:Ljava/lang/Object;

    monitor-enter v2

    .line 1355
    :try_start_0
    invoke-direct {p0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->isAllPartitionLoadFinished()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->isAllPartitionEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1357
    invoke-direct {p0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->addShowMoreButtonAtlastPatition()V

    .line 1362
    :cond_1
    :goto_0
    monitor-exit v2

    .line 1374
    .end local v0    # "partition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    :cond_2
    :goto_1
    return-void

    .line 1358
    .restart local v0    # "partition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    :cond_3
    invoke-direct {p0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->isAllPartitionLoadFinished()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->isAllPartitionEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1359
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v3, :cond_1

    .line 1360
    invoke-direct {p0}, Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter;->addNoMatchesAtlastPatition()V

    goto :goto_0

    .line 1362
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1367
    :cond_4
    if-eqz p3, :cond_2

    .line 1368
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 1371
    .end local v0    # "partition":Lcom/android/emailcommon/adapter/SecBaseEmailAddressAdapter$DirectoryPartition;
    :cond_5
    if-eqz p3, :cond_2

    .line 1372
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method
