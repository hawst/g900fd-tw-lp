.class public Lcom/android/emailcommon/EasRefs;
.super Ljava/lang/Object;
.source "EasRefs.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/EasRefs$EmailDataSize;
    }
.end annotation


# static fields
.field public static DEBUG:Z

.field public static FILE_LOG:Z

.field public static final FILTER_1_DAY:Ljava/lang/String;

.field public static final FILTER_1_MONTH:Ljava/lang/String;

.field public static final FILTER_1_WEEK:Ljava/lang/String;

.field public static final FILTER_2_WEEKS:Ljava/lang/String;

.field public static final FILTER_3_DAYS:Ljava/lang/String;

.field public static final FILTER_AUTO:Ljava/lang/String;

.field public static LOGD:Z

.field public static LOG_TAG:Ljava/lang/String;

.field public static PARSER_LOG:Z

.field public static final PLATFORM_VERSION:Ljava/lang/String;

.field public static REFRESH_BODY_TEST_ENABLE:Z

.field public static TIME_CHECK_LOG:Z

.field public static USER_LOG:Z

.field public static VIEW_FILE_LOG:Z

.field public static WAIT_DEBUG:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 36
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->WAIT_DEBUG:Z

    .line 38
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    .line 41
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    .line 43
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    .line 45
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->FILE_LOG:Z

    .line 48
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    .line 51
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->VIEW_FILE_LOG:Z

    .line 54
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->REFRESH_BODY_TEST_ENABLE:Z

    .line 104
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "."

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/EasRefs;->PLATFORM_VERSION:Ljava/lang/String;

    .line 145
    const/4 v0, -0x2

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/EasRefs;->FILTER_AUTO:Ljava/lang/String;

    .line 147
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/EasRefs;->FILTER_1_DAY:Ljava/lang/String;

    .line 148
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/EasRefs;->FILTER_3_DAYS:Ljava/lang/String;

    .line 149
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/EasRefs;->FILTER_1_WEEK:Ljava/lang/String;

    .line 150
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/EasRefs;->FILTER_2_WEEKS:Ljava/lang/String;

    .line 151
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/EasRefs;->FILTER_1_MONTH:Ljava/lang/String;

    .line 177
    sput-boolean v3, Lcom/android/emailcommon/EasRefs;->LOGD:Z

    .line 178
    const-string v0, "FixIt"

    sput-object v0, Lcom/android/emailcommon/EasRefs;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    return-void
.end method

.method public static getProtocolVersionDouble(Ljava/lang/String;)Ljava/lang/Double;
    .locals 2
    .param p0, "version"    # Ljava/lang/String;

    .prologue
    .line 440
    const-string v0, "2.5"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 441
    const-wide/high16 v0, 0x4004000000000000L    # 2.5

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 449
    :goto_0
    return-object v0

    .line 442
    :cond_0
    const-string v0, "12.0"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 443
    const-wide/high16 v0, 0x4028000000000000L    # 12.0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 444
    :cond_1
    const-string v0, "12.1"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 445
    const-wide v0, 0x4028333333333333L    # 12.1

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 446
    :cond_2
    const-string v0, "14.0"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 447
    const-wide/high16 v0, 0x402c000000000000L    # 14.0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 448
    :cond_3
    const-string v0, "14.1"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 449
    const-wide v0, 0x402c333333333333L    # 14.1

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    .line 451
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "illegal protocol version"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static setUserDebug(I)V
    .locals 4
    .param p0, "state"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 66
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->DEBUG:Z

    if-nez v0, :cond_4

    .line 67
    and-int/lit8 v0, p0, 0x1

    if-eqz v0, :cond_8

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    .line 68
    and-int/lit8 v0, p0, 0x2

    if-eqz v0, :cond_9

    move v0, v1

    :goto_1
    sput-boolean v0, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    .line 69
    and-int/lit8 v0, p0, 0x4

    if-eqz v0, :cond_0

    move v2, v1

    :cond_0
    sput-boolean v2, Lcom/android/emailcommon/EasRefs;->FILE_LOG:Z

    .line 70
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->FILE_LOG:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    if-eqz v0, :cond_2

    .line 71
    :cond_1
    sput-boolean v1, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    .line 74
    :cond_2
    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->FILE_LOG:Z

    if-nez v0, :cond_3

    .line 75
    invoke-static {}, Lcom/android/emailcommon/utility/FileLogger;->close()V

    .line 78
    :cond_3
    const-string v2, "Eas Debug"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Logging: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->USER_LOG:Z

    if-eqz v0, :cond_a

    const-string v0, "User "

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->PARSER_LOG:Z

    if-eqz v0, :cond_b

    const-string v0, "Parser "

    :goto_3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v0, Lcom/android/emailcommon/EasRefs;->FILE_LOG:Z

    if-eqz v0, :cond_c

    const-string v0, "File"

    :goto_4
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_4
    and-int/lit8 v0, p0, 0x10

    if-eqz v0, :cond_5

    .line 82
    sput-boolean v1, Lcom/android/emailcommon/EasRefs;->TIME_CHECK_LOG:Z

    .line 85
    :cond_5
    and-int/lit8 v0, p0, 0x20

    if-eqz v0, :cond_6

    .line 86
    sput-boolean v1, Lcom/android/emailcommon/EasRefs;->VIEW_FILE_LOG:Z

    .line 89
    :cond_6
    and-int/lit8 v0, p0, 0x40

    if-eqz v0, :cond_7

    .line 90
    sput-boolean v1, Lcom/android/emailcommon/EasRefs;->REFRESH_BODY_TEST_ENABLE:Z

    .line 92
    :cond_7
    return-void

    :cond_8
    move v0, v2

    .line 67
    goto :goto_0

    :cond_9
    move v0, v2

    .line 68
    goto :goto_1

    .line 78
    :cond_a
    const-string v0, ""

    goto :goto_2

    :cond_b
    const-string v0, ""

    goto :goto_3

    :cond_c
    const-string v0, ""

    goto :goto_4
.end method
