.class public Lcom/android/emailcommon/CursorManager;
.super Ljava/lang/Object;
.source "CursorManager.java"


# static fields
.field private static _context:Landroid/content/Context;

.field private static _inst:Lcom/android/emailcommon/CursorManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inst(Landroid/content/Context;)Lcom/android/emailcommon/CursorManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 12
    sget-object v0, Lcom/android/emailcommon/CursorManager;->_inst:Lcom/android/emailcommon/CursorManager;

    if-nez v0, :cond_0

    .line 13
    new-instance v0, Lcom/android/emailcommon/CursorManager;

    invoke-direct {v0}, Lcom/android/emailcommon/CursorManager;-><init>()V

    sput-object v0, Lcom/android/emailcommon/CursorManager;->_inst:Lcom/android/emailcommon/CursorManager;

    .line 15
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/CursorManager;->_context:Landroid/content/Context;

    .line 16
    sget-object v0, Lcom/android/emailcommon/CursorManager;->_inst:Lcom/android/emailcommon/CursorManager;

    return-object v0
.end method


# virtual methods
.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "proj"    # [Ljava/lang/String;
    .param p3, "sel"    # Ljava/lang/String;
    .param p4, "args"    # [Ljava/lang/String;
    .param p5, "aa"    # Ljava/lang/String;

    .prologue
    .line 19
    sget-object v0, Lcom/android/emailcommon/CursorManager;->_context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 20
    .local v6, "c":Landroid/database/Cursor;
    sget-boolean v0, Lcom/android/emailcommon/LeakTestCursor;->ENABLE_LEAK_MONITOR:Z

    if-eqz v0, :cond_0

    .line 21
    new-instance v0, Lcom/android/emailcommon/LeakTestCursor;

    invoke-direct {v0, v6}, Lcom/android/emailcommon/LeakTestCursor;-><init>(Landroid/database/Cursor;)V

    move-object v6, v0

    .line 22
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_0
    return-object v6
.end method
