.class public Lcom/android/emailcommon/utility/calendar/ICalendar$Component;
.super Ljava/lang/Object;
.source "ICalendar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/emailcommon/utility/calendar/ICalendar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Component"
.end annotation


# instance fields
.field private mChildren:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/android/emailcommon/utility/calendar/ICalendar$Component;",
            ">;"
        }
    .end annotation
.end field

.field private final mName:Ljava/lang/String;

.field private final mPropsMap:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/utility/calendar/ICalendar$Property;",
            ">;>;"
        }
    .end annotation
.end field


# virtual methods
.method public getProperties(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/android/emailcommon/utility/calendar/ICalendar$Property;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Lcom/android/emailcommon/utility/calendar/ICalendar$Component;->mPropsMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getPropertyNames()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 149
    iget-object v0, p0, Lcom/android/emailcommon/utility/calendar/ICalendar$Component;->mPropsMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 179
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0, v0}, Lcom/android/emailcommon/utility/calendar/ICalendar$Component;->toString(Ljava/lang/StringBuilder;)V

    .line 180
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public toString(Ljava/lang/StringBuilder;)V
    .locals 8
    .param p1, "sb"    # Ljava/lang/StringBuilder;

    .prologue
    const/16 v7, 0x3a

    .line 190
    const-string v6, "BEGIN"

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 192
    iget-object v6, p0, Lcom/android/emailcommon/utility/calendar/ICalendar$Component;->mName:Ljava/lang/String;

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    const-string v6, "\n"

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    const/4 v3, 0x0

    .line 197
    .local v3, "properties":Ljava/util/List;, "Ljava/util/List<Lcom/android/emailcommon/utility/calendar/ICalendar$Property;>;"
    invoke-virtual {p0}, Lcom/android/emailcommon/utility/calendar/ICalendar$Component;->getPropertyNames()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 198
    .local v5, "propertyName":Ljava/lang/String;
    invoke-virtual {p0, v5}, Lcom/android/emailcommon/utility/calendar/ICalendar$Component;->getProperties(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 199
    if-eqz v3, :cond_0

    .line 200
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/emailcommon/utility/calendar/ICalendar$Property;

    .line 201
    .local v4, "property":Lcom/android/emailcommon/utility/calendar/ICalendar$Property;
    invoke-virtual {v4, p1}, Lcom/android/emailcommon/utility/calendar/ICalendar$Property;->toString(Ljava/lang/StringBuilder;)V

    .line 202
    const-string v6, "\n"

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 204
    .end local v4    # "property":Lcom/android/emailcommon/utility/calendar/ICalendar$Property;
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 207
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v5    # "propertyName":Ljava/lang/String;
    :cond_2
    const/4 v3, 0x0

    .line 209
    iget-object v6, p0, Lcom/android/emailcommon/utility/calendar/ICalendar$Component;->mChildren:Ljava/util/LinkedList;

    if-eqz v6, :cond_3

    .line 210
    iget-object v6, p0, Lcom/android/emailcommon/utility/calendar/ICalendar$Component;->mChildren:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/utility/calendar/ICalendar$Component;

    .line 211
    .local v0, "component":Lcom/android/emailcommon/utility/calendar/ICalendar$Component;
    invoke-virtual {v0, p1}, Lcom/android/emailcommon/utility/calendar/ICalendar$Component;->toString(Ljava/lang/StringBuilder;)V

    .line 212
    const-string v6, "\n"

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 216
    .end local v0    # "component":Lcom/android/emailcommon/utility/calendar/ICalendar$Component;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    const-string v6, "END"

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    invoke-virtual {p1, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 218
    iget-object v6, p0, Lcom/android/emailcommon/utility/calendar/ICalendar$Component;->mName:Ljava/lang/String;

    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 219
    return-void
.end method
