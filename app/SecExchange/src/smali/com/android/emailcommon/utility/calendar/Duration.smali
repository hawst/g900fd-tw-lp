.class public Lcom/android/emailcommon/utility/calendar/Duration;
.super Ljava/lang/Object;
.source "Duration.java"


# instance fields
.field public days:I

.field public hours:I

.field public minutes:I

.field public seconds:I

.field public sign:I

.field public weeks:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/emailcommon/utility/calendar/Duration;->sign:I

    .line 41
    return-void
.end method


# virtual methods
.method public getMillis()J
    .locals 5

    .prologue
    .line 131
    iget v2, p0, Lcom/android/emailcommon/utility/calendar/Duration;->sign:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v0, v2

    .line 132
    .local v0, "factor":J
    const v2, 0x93a80

    iget v3, p0, Lcom/android/emailcommon/utility/calendar/Duration;->weeks:I

    mul-int/2addr v2, v3

    const v3, 0x15180

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/Duration;->days:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/emailcommon/utility/calendar/Duration;->hours:I

    mul-int/lit16 v3, v3, 0xe10

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/emailcommon/utility/calendar/Duration;->minutes:I

    mul-int/lit8 v3, v3, 0x3c

    add-int/2addr v2, v3

    iget v3, p0, Lcom/android/emailcommon/utility/calendar/Duration;->seconds:I

    add-int/2addr v2, v3

    int-to-long v2, v2

    mul-long/2addr v2, v0

    return-wide v2
.end method

.method public parse(Ljava/lang/String;)V
    .locals 8
    .param p1, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 48
    iput v6, p0, Lcom/android/emailcommon/utility/calendar/Duration;->sign:I

    .line 49
    iput v5, p0, Lcom/android/emailcommon/utility/calendar/Duration;->weeks:I

    .line 50
    iput v5, p0, Lcom/android/emailcommon/utility/calendar/Duration;->days:I

    .line 51
    iput v5, p0, Lcom/android/emailcommon/utility/calendar/Duration;->hours:I

    .line 52
    iput v5, p0, Lcom/android/emailcommon/utility/calendar/Duration;->minutes:I

    .line 53
    iput v5, p0, Lcom/android/emailcommon/utility/calendar/Duration;->seconds:I

    .line 55
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 56
    .local v2, "len":I
    const/4 v1, 0x0

    .line 59
    .local v1, "index":I
    if-ge v2, v6, :cond_1

    .line 113
    :cond_0
    return-void

    .line 63
    :cond_1
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 64
    .local v0, "c":C
    const/16 v5, 0x2d

    if-ne v0, v5, :cond_3

    .line 65
    const/4 v5, -0x1

    iput v5, p0, Lcom/android/emailcommon/utility/calendar/Duration;->sign:I

    .line 66
    add-int/lit8 v1, v1, 0x1

    .line 71
    :cond_2
    :goto_0
    if-lt v2, v1, :cond_0

    .line 75
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 76
    const/16 v5, 0x50

    if-eq v0, v5, :cond_4

    .line 77
    new-instance v5, Ljava/text/ParseException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Duration.parse(str=\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\') expected \'P\' at index="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v5

    .line 67
    :cond_3
    const/16 v5, 0x2b

    if-ne v0, v5, :cond_2

    .line 68
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 80
    :cond_4
    add-int/lit8 v1, v1, 0x1

    .line 82
    const/4 v3, 0x0

    .line 83
    .local v3, "n":I
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 84
    .local v4, "tempException":Ljava/lang/StringBuffer;
    :goto_1
    if-ge v1, v2, :cond_0

    .line 85
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 86
    const/16 v5, 0x30

    if-lt v0, v5, :cond_6

    const/16 v5, 0x39

    if-gt v0, v5, :cond_6

    .line 87
    mul-int/lit8 v3, v3, 0xa

    .line 88
    add-int/lit8 v5, v0, -0x30

    add-int/2addr v3, v5

    .line 84
    :cond_5
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 89
    :cond_6
    const/16 v5, 0x57

    if-ne v0, v5, :cond_7

    .line 90
    iput v3, p0, Lcom/android/emailcommon/utility/calendar/Duration;->weeks:I

    .line 91
    const/4 v3, 0x0

    goto :goto_2

    .line 92
    :cond_7
    const/16 v5, 0x48

    if-ne v0, v5, :cond_8

    .line 93
    iput v3, p0, Lcom/android/emailcommon/utility/calendar/Duration;->hours:I

    .line 94
    const/4 v3, 0x0

    goto :goto_2

    .line 95
    :cond_8
    const/16 v5, 0x4d

    if-ne v0, v5, :cond_9

    .line 96
    iput v3, p0, Lcom/android/emailcommon/utility/calendar/Duration;->minutes:I

    .line 97
    const/4 v3, 0x0

    goto :goto_2

    .line 98
    :cond_9
    const/16 v5, 0x53

    if-ne v0, v5, :cond_a

    .line 99
    iput v3, p0, Lcom/android/emailcommon/utility/calendar/Duration;->seconds:I

    .line 100
    const/4 v3, 0x0

    goto :goto_2

    .line 101
    :cond_a
    const/16 v5, 0x44

    if-ne v0, v5, :cond_b

    .line 102
    iput v3, p0, Lcom/android/emailcommon/utility/calendar/Duration;->days:I

    .line 103
    const/4 v3, 0x0

    goto :goto_2

    .line 104
    :cond_b
    const/16 v5, 0x54

    if-eq v0, v5, :cond_5

    .line 106
    new-instance v5, Ljava/text/ParseException;

    const-string v6, "Duration.parse(str=\'"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, "\') unexpected char \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, "\' at index="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, v1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v5
.end method
