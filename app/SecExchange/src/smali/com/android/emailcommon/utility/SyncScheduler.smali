.class public Lcom/android/emailcommon/utility/SyncScheduler;
.super Ljava/lang/Object;
.source "SyncScheduler.java"


# static fields
.field private static final sCalendar:Ljava/util/Calendar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/utility/SyncScheduler;->sCalendar:Ljava/util/Calendar;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    return-void
.end method

.method public static getIsPeakAndNextAlarm(Lcom/android/emailcommon/utility/SyncScheduleData;)Landroid/util/Pair;
    .locals 32
    .param p0, "syncScheduleData"    # Lcom/android/emailcommon/utility/SyncScheduleData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/emailcommon/utility/SyncScheduleData;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16
    const/4 v7, 0x0

    .line 18
    .local v7, "isPeak":Z
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    .line 19
    .local v4, "date":Ljava/util/Date;
    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v18

    .line 22
    .local v18, "now":J
    const-wide/16 v16, 0x0

    .line 23
    .local v16, "nextAlarm":J
    invoke-virtual/range {p0 .. p0}, Lcom/android/emailcommon/utility/SyncScheduleData;->getStartMinute()I

    move-result v25

    .line 24
    .local v25, "peakStartMinute":I
    invoke-virtual/range {p0 .. p0}, Lcom/android/emailcommon/utility/SyncScheduleData;->getEndMinute()I

    move-result v20

    .line 26
    .local v20, "offPeakStartMinute":I
    move/from16 v0, v25

    invoke-static {v4, v0}, Lcom/android/emailcommon/utility/SyncScheduler;->getMinuteInMillis(Ljava/util/Date;I)J

    move-result-wide v26

    .line 28
    .local v26, "peakTime":J
    move/from16 v0, v20

    invoke-static {v4, v0}, Lcom/android/emailcommon/utility/SyncScheduler;->getMinuteInMillis(Ljava/util/Date;I)J

    move-result-wide v22

    .line 30
    .local v22, "offPeakTime":J
    new-instance v28, Ljava/util/Date;

    move-object/from16 v0, v28

    move-wide/from16 v1, v26

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 31
    .local v28, "peakdate":Ljava/util/Date;
    new-instance v21, Ljava/util/Date;

    invoke-direct/range {v21 .. v23}, Ljava/util/Date;-><init>(J)V

    .line 33
    .local v21, "offpeakdate":Ljava/util/Date;
    sub-long v14, v26, v18

    .line 34
    .local v14, "millisToPeakTimeStart":J
    sub-long v12, v22, v18

    .line 35
    .local v12, "millisToPeakTimeEnd":J
    const-wide/16 v10, 0x0

    .line 36
    .local v10, "millisToNextAlarm":J
    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v5

    .line 37
    .local v5, "isAfterPeak":Z
    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v6

    .line 41
    .local v6, "isBeforeOffPeak":Z
    invoke-static/range {p0 .. p0}, Lcom/android/emailcommon/utility/SyncScheduler;->getPeakDays(Lcom/android/emailcommon/utility/SyncScheduleData;)Ljava/util/ArrayList;

    move-result-object v24

    .line 43
    .local v24, "peakDaysArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    sget-object v29, Lcom/android/emailcommon/utility/SyncScheduler;->sCalendar:Ljava/util/Calendar;

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 44
    const/4 v8, 0x1

    .line 45
    .local v8, "isPeakDay":Z
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v29

    sget-object v30, Lcom/android/emailcommon/utility/SyncScheduler;->sCalendar:Ljava/util/Calendar;

    const/16 v31, 0x7

    invoke-virtual/range {v30 .. v31}, Ljava/util/Calendar;->get(I)I

    move-result v30

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v29

    if-gez v29, :cond_0

    .line 47
    const/4 v8, 0x0

    .line 50
    :cond_0
    sget-object v29, Lcom/android/emailcommon/utility/SyncScheduler;->sCalendar:Ljava/util/Calendar;

    const/16 v30, 0x5

    const/16 v31, -0x1

    invoke-virtual/range {v29 .. v31}, Ljava/util/Calendar;->add(II)V

    .line 51
    const/4 v9, 0x1

    .line 52
    .local v9, "isYesterdayPeakDay":Z
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v29

    sget-object v30, Lcom/android/emailcommon/utility/SyncScheduler;->sCalendar:Ljava/util/Calendar;

    const/16 v31, 0x7

    invoke-virtual/range {v30 .. v31}, Ljava/util/Calendar;->get(I)I

    move-result v30

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v29

    if-gez v29, :cond_1

    .line 54
    const/4 v9, 0x0

    .line 58
    :cond_1
    cmp-long v29, v14, v12

    if-nez v29, :cond_6

    .line 61
    if-eqz v6, :cond_4

    .line 64
    if-eqz v9, :cond_2

    .line 65
    const/4 v7, 0x1

    .line 69
    :cond_2
    move-wide v10, v12

    .line 122
    :goto_0
    move-wide/from16 v16, v10

    .line 124
    invoke-virtual/range {p0 .. p0}, Lcom/android/emailcommon/utility/SyncScheduleData;->getIsPeakScheduleOn()Z

    move-result v29

    if-nez v29, :cond_3

    .line 125
    const/4 v7, 0x0

    .line 127
    :cond_3
    new-instance v29, Landroid/util/Pair;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v30

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    invoke-direct/range {v29 .. v31}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v29

    .line 73
    :cond_4
    if-eqz v8, :cond_5

    .line 74
    const/4 v7, 0x1

    .line 78
    :cond_5
    const-wide/32 v30, 0x5265c00

    add-long v10, v14, v30

    goto :goto_0

    .line 80
    :cond_6
    if-eqz v5, :cond_a

    .line 85
    cmp-long v29, v12, v14

    if-gez v29, :cond_7

    .line 86
    const-wide/32 v30, 0x5265c00

    add-long v12, v12, v30

    .line 89
    :cond_7
    if-eqz v6, :cond_9

    .line 91
    if-eqz v8, :cond_8

    .line 92
    const/4 v7, 0x1

    .line 96
    :cond_8
    move-wide v10, v12

    goto :goto_0

    .line 100
    :cond_9
    const-wide/32 v30, 0x5265c00

    add-long v10, v14, v30

    goto :goto_0

    .line 102
    :cond_a
    if-eqz v6, :cond_c

    cmp-long v29, v12, v14

    if-gez v29, :cond_c

    .line 107
    if-eqz v9, :cond_b

    .line 108
    const/4 v7, 0x1

    .line 112
    :cond_b
    move-wide v10, v12

    goto :goto_0

    .line 119
    :cond_c
    move-wide v10, v14

    goto :goto_0
.end method

.method private static getMinuteInMillis(Ljava/util/Date;I)J
    .locals 6
    .param p0, "date"    # Ljava/util/Date;
    .param p1, "minute"    # I

    .prologue
    const/4 v4, 0x0

    .line 156
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 157
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 158
    div-int/lit8 v1, p1, 0x3c

    .line 159
    .local v1, "hr":I
    rem-int/lit8 v2, p1, 0x3c

    .line 160
    .local v2, "min":I
    const/16 v3, 0xb

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->set(II)V

    .line 161
    const/16 v3, 0xc

    invoke-virtual {v0, v3, v2}, Ljava/util/Calendar;->set(II)V

    .line 162
    const/16 v3, 0xd

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 163
    const/16 v3, 0xe

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 164
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    return-wide v4
.end method

.method private static getPeakDays(Lcom/android/emailcommon/utility/SyncScheduleData;)Ljava/util/ArrayList;
    .locals 9
    .param p0, "syncSchedulerData"    # Lcom/android/emailcommon/utility/SyncScheduleData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/emailcommon/utility/SyncScheduleData;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x7

    .line 132
    invoke-virtual {p0}, Lcom/android/emailcommon/utility/SyncScheduleData;->getPeakDay()I

    move-result v5

    .line 133
    .local v5, "peakDays":I
    const/4 v3, 0x1

    .line 134
    .local v3, "mask":I
    const/4 v4, 0x0

    .line 135
    .local v4, "numberOfPeakDays":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v8, :cond_1

    .line 136
    and-int v7, v5, v3

    if-eqz v7, :cond_0

    .line 137
    add-int/lit8 v4, v4, 0x1

    .line 139
    :cond_0
    shl-int/lit8 v3, v3, 0x1

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 143
    :cond_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 144
    .local v6, "peakDaysArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v3, 0x1

    .line 145
    const/4 v1, 0x0

    .line 146
    .local v1, "j":I
    const/4 v0, 0x0

    move v2, v1

    .end local v1    # "j":I
    .local v2, "j":I
    :goto_1
    if-ge v0, v8, :cond_2

    .line 147
    and-int v7, v5, v3

    if-eqz v7, :cond_3

    .line 148
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "j":I
    .restart local v1    # "j":I
    add-int/lit8 v7, v0, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v2, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 150
    :goto_2
    shl-int/lit8 v3, v3, 0x1

    .line 146
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    .end local v1    # "j":I
    .restart local v2    # "j":I
    goto :goto_1

    .line 152
    :cond_2
    return-object v6

    :cond_3
    move v1, v2

    .end local v2    # "j":I
    .restart local v1    # "j":I
    goto :goto_2
.end method
