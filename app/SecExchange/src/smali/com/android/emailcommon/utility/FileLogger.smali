.class public Lcom/android/emailcommon/utility/FileLogger;
.super Ljava/lang/Object;
.source "FileLogger.java"


# static fields
.field private static LOGGER:Lcom/android/emailcommon/utility/FileLogger;

.field public static LOG_FILE_NAME:Ljava/lang/String;

.field private static file:Ljava/io/File;

.field private static sLogWriter:Ljava/io/FileWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 30
    sput-object v0, Lcom/android/emailcommon/utility/FileLogger;->LOGGER:Lcom/android/emailcommon/utility/FileLogger;

    .line 32
    sput-object v0, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;

    .line 34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/emaillog.txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/utility/FileLogger;->LOG_FILE_NAME:Ljava/lang/String;

    .line 37
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/android/emailcommon/utility/FileLogger;->LOG_FILE_NAME:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/emailcommon/utility/FileLogger;->file:Ljava/io/File;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    :try_start_0
    sget-object v0, Lcom/android/emailcommon/utility/FileLogger;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/utility/FileLogger;->file:Ljava/io/File;

    .line 47
    new-instance v0, Ljava/io/FileWriter;

    sget-object v1, Lcom/android/emailcommon/utility/FileLogger;->file:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_0
    return-void

    .line 48
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static declared-synchronized close()V
    .locals 2

    .prologue
    .line 54
    const-class v1, Lcom/android/emailcommon/utility/FileLogger;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 56
    :try_start_1
    sget-object v0, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;

    invoke-virtual {v0}, Ljava/io/FileWriter;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 60
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    sput-object v0, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;

    .line 61
    const/4 v0, 0x0

    sput-object v0, Lcom/android/emailcommon/utility/FileLogger;->LOGGER:Lcom/android/emailcommon/utility/FileLogger;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 63
    :cond_0
    monitor-exit v1

    return-void

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 57
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static declared-synchronized log(Ljava/lang/String;Ljava/lang/String;)V
    .locals 17
    .param p0, "prefix"    # Ljava/lang/String;
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 80
    const-class v13, Lcom/android/emailcommon/utility/FileLogger;

    monitor-enter v13

    :try_start_0
    sget-object v12, Lcom/android/emailcommon/utility/FileLogger;->LOGGER:Lcom/android/emailcommon/utility/FileLogger;

    if-eqz v12, :cond_0

    const-string v12, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    sget-object v12, Lcom/android/emailcommon/utility/FileLogger;->file:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_1

    .line 82
    :cond_0
    new-instance v12, Lcom/android/emailcommon/utility/FileLogger;

    invoke-direct {v12}, Lcom/android/emailcommon/utility/FileLogger;-><init>()V

    sput-object v12, Lcom/android/emailcommon/utility/FileLogger;->LOGGER:Lcom/android/emailcommon/utility/FileLogger;

    .line 83
    sget-object v12, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v12, :cond_1

    .line 85
    :try_start_1
    sget-object v12, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;

    const-string v14, "Logger \r\n"

    invoke-virtual {v12, v14}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 86
    sget-object v12, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;

    const-string v14, "Logger     -------------- New Log --------------\r\n"

    invoke-virtual {v12, v14}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 87
    sget-object v12, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Logger     Model      :"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "ro.product.model"

    const-string v16, "Unknown"

    invoke-static/range {v15 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\r\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 88
    sget-object v12, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Logger     Build      :"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "ro.build.PDA"

    const-string v16, "Unknown"

    invoke-static/range {v15 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\r\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 89
    sget-object v12, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Logger     ChangeList :"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "ro.build.changelist"

    const-string v16, "Unknown"

    invoke-static/range {v15 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\r\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 90
    sget-object v12, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;

    const-string v14, "Logger     -------------------------------------\r\n"

    invoke-virtual {v12, v14}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 91
    sget-object v12, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;

    invoke-virtual {v12}, Ljava/io/FileWriter;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    :cond_1
    :goto_0
    :try_start_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 103
    .local v1, "c":Ljava/util/Calendar;
    const/4 v12, 0x5

    invoke-virtual {v1, v12}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 104
    .local v2, "date":I
    const/4 v12, 0x2

    invoke-virtual {v1, v12}, Ljava/util/Calendar;->get(I)I

    move-result v12

    add-int/lit8 v7, v12, 0x1

    .line 105
    .local v7, "month":I
    const/4 v12, 0x1

    invoke-virtual {v1, v12}, Ljava/util/Calendar;->get(I)I

    move-result v11

    .line 106
    .local v11, "year":I
    const/16 v12, 0xb

    invoke-virtual {v1, v12}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 107
    .local v4, "hr":I
    const/16 v12, 0xc

    invoke-virtual {v1, v12}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 108
    .local v6, "min":I
    const/16 v12, 0xd

    invoke-virtual {v1, v12}, Ljava/util/Calendar;->get(I)I

    move-result v10

    .line 109
    .local v10, "sec":I
    const/16 v12, 0xe

    invoke-virtual {v1, v12}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 113
    .local v5, "mSec":I
    new-instance v9, Ljava/lang/StringBuffer;

    const/16 v12, 0x100

    invoke-direct {v9, v12}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 115
    .local v9, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v9, v11}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 116
    const/16 v12, 0x2d

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 118
    const/16 v12, 0xa

    if-ge v7, v12, :cond_2

    .line 119
    const/16 v12, 0x30

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 122
    :cond_2
    invoke-virtual {v9, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 123
    const/16 v12, 0x2d

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 125
    const/16 v12, 0xa

    if-ge v2, v12, :cond_3

    .line 126
    const/16 v12, 0x30

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 129
    :cond_3
    invoke-virtual {v9, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 130
    const/16 v12, 0x20

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 132
    invoke-virtual {v9, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 134
    const/16 v12, 0x3a

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 136
    const/16 v12, 0xa

    if-ge v6, v12, :cond_4

    .line 137
    const/16 v12, 0x30

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 140
    :cond_4
    invoke-virtual {v9, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 141
    const/16 v12, 0x3a

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 143
    const/16 v12, 0xa

    if-ge v10, v12, :cond_5

    .line 144
    const/16 v12, 0x30

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 146
    :cond_5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 147
    const/16 v12, 0x3a

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 149
    const/16 v12, 0xa

    if-ge v5, v12, :cond_9

    .line 150
    const-string v12, "00"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 156
    :cond_6
    :goto_1
    invoke-virtual {v9, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 158
    const/16 v12, 0x20

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 159
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v12

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 160
    const/16 v12, 0x20

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 161
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v12

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 162
    const-string v12, " ["

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 163
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 164
    const-string v12, "] "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 166
    if-eqz p0, :cond_7

    .line 167
    move-object/from16 v0, p0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 168
    const-string v12, "| "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 170
    :cond_7
    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 171
    const-string v12, "\r\n"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 172
    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    .line 174
    .local v8, "s":Ljava/lang/String;
    sget-object v12, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v12, :cond_8

    .line 176
    :try_start_3
    sget-object v12, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;

    invoke-virtual {v12, v8}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 177
    sget-object v12, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;

    invoke-virtual {v12}, Ljava/io/FileWriter;->flush()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 191
    .end local v1    # "c":Ljava/util/Calendar;
    .end local v2    # "date":I
    .end local v4    # "hr":I
    .end local v5    # "mSec":I
    .end local v6    # "min":I
    .end local v7    # "month":I
    .end local v8    # "s":Ljava/lang/String;
    .end local v9    # "sb":Ljava/lang/StringBuffer;
    .end local v10    # "sec":I
    .end local v11    # "year":I
    :cond_8
    :goto_2
    monitor-exit v13

    return-void

    .line 92
    :catch_0
    move-exception v3

    .line 94
    .local v3, "e":Ljava/io/IOException;
    :try_start_4
    const-string v12, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 97
    new-instance v12, Lcom/android/emailcommon/utility/FileLogger;

    invoke-direct {v12}, Lcom/android/emailcommon/utility/FileLogger;-><init>()V

    sput-object v12, Lcom/android/emailcommon/utility/FileLogger;->LOGGER:Lcom/android/emailcommon/utility/FileLogger;

    goto/16 :goto_0

    .line 187
    .end local v3    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v12

    goto :goto_2

    .line 152
    .restart local v1    # "c":Ljava/util/Calendar;
    .restart local v2    # "date":I
    .restart local v4    # "hr":I
    .restart local v5    # "mSec":I
    .restart local v6    # "min":I
    .restart local v7    # "month":I
    .restart local v9    # "sb":Ljava/lang/StringBuffer;
    .restart local v10    # "sec":I
    .restart local v11    # "year":I
    :cond_9
    const/16 v12, 0x64

    if-ge v5, v12, :cond_6

    .line 153
    const/16 v12, 0x30

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 80
    .end local v1    # "c":Ljava/util/Calendar;
    .end local v2    # "date":I
    .end local v4    # "hr":I
    .end local v5    # "mSec":I
    .end local v6    # "min":I
    .end local v7    # "month":I
    .end local v9    # "sb":Ljava/lang/StringBuffer;
    .end local v10    # "sec":I
    .end local v11    # "year":I
    :catchall_0
    move-exception v12

    monitor-exit v13

    throw v12

    .line 178
    .restart local v1    # "c":Ljava/util/Calendar;
    .restart local v2    # "date":I
    .restart local v4    # "hr":I
    .restart local v5    # "mSec":I
    .restart local v6    # "min":I
    .restart local v7    # "month":I
    .restart local v8    # "s":Ljava/lang/String;
    .restart local v9    # "sb":Ljava/lang/StringBuffer;
    .restart local v10    # "sec":I
    .restart local v11    # "year":I
    :catch_2
    move-exception v3

    .line 180
    .restart local v3    # "e":Ljava/io/IOException;
    :try_start_5
    const-string v12, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 183
    new-instance v12, Lcom/android/emailcommon/utility/FileLogger;

    invoke-direct {v12}, Lcom/android/emailcommon/utility/FileLogger;-><init>()V

    sput-object v12, Lcom/android/emailcommon/utility/FileLogger;->LOGGER:Lcom/android/emailcommon/utility/FileLogger;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2
.end method

.method public static declared-synchronized log(Ljava/lang/Throwable;)V
    .locals 4
    .param p0, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 66
    const-class v2, Lcom/android/emailcommon/utility/FileLogger;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;

    if-eqz v1, :cond_0

    .line 67
    const-string v1, "Exception"

    const-string v3, "Stack trace follows..."

    invoke-static {v1, v3}, Lcom/android/emailcommon/utility/FileLogger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    new-instance v0, Ljava/io/PrintWriter;

    sget-object v1, Lcom/android/emailcommon/utility/FileLogger;->sLogWriter:Ljava/io/FileWriter;

    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 69
    .local v0, "pw":Ljava/io/PrintWriter;
    invoke-virtual {p0, v0}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 70
    invoke-virtual {v0}, Ljava/io/PrintWriter;->flush()V

    .line 72
    invoke-virtual {v0}, Ljava/io/PrintWriter;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    .end local v0    # "pw":Ljava/io/PrintWriter;
    :cond_0
    monitor-exit v2

    return-void

    .line 66
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method
