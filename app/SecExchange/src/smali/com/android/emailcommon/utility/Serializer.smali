.class public Lcom/android/emailcommon/utility/Serializer;
.super Ljava/lang/Object;
.source "Serializer.java"


# instance fields
.field buf:Ljava/io/ByteArrayOutputStream;

.field depth:I

.field private logging:Z

.field nameStack:[Ljava/lang/String;

.field out:Ljava/io/ByteArrayOutputStream;

.field pendingTag:I

.field private tagPage:I

.field tagTable:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/emailcommon/utility/Serializer;-><init>(Z)V

    .line 61
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2
    .param p1, "startDocument"    # Z

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-boolean v1, p0, Lcom/android/emailcommon/utility/Serializer;->logging:Z

    .line 41
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/android/emailcommon/utility/Serializer;->out:Ljava/io/ByteArrayOutputStream;

    .line 43
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/android/emailcommon/utility/Serializer;->buf:Ljava/io/ByteArrayOutputStream;

    .line 47
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/emailcommon/utility/Serializer;->pendingTag:I

    .line 53
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/android/emailcommon/utility/Serializer;->nameStack:[Ljava/lang/String;

    .line 55
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/android/emailcommon/utility/Serializer;->tagTable:Ljava/util/Hashtable;

    .line 70
    if-eqz p1, :cond_0

    .line 72
    :try_start_0
    invoke-virtual {p0}, Lcom/android/emailcommon/utility/Serializer;->startDocument()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_0
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/android/emailcommon/utility/Serializer;->out:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0

    .line 74
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public checkPendingTag(Z)V
    .locals 6
    .param p1, "degenerated"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    .line 117
    iget v3, p0, Lcom/android/emailcommon/utility/Serializer;->pendingTag:I

    if-ne v3, v5, :cond_0

    .line 135
    :goto_0
    return-void

    .line 120
    :cond_0
    iget v3, p0, Lcom/android/emailcommon/utility/Serializer;->pendingTag:I

    shr-int/lit8 v1, v3, 0x6

    .line 121
    .local v1, "page":I
    iget v3, p0, Lcom/android/emailcommon/utility/Serializer;->pendingTag:I

    and-int/lit8 v2, v3, 0x3f

    .line 122
    .local v2, "tag":I
    iget v3, p0, Lcom/android/emailcommon/utility/Serializer;->tagPage:I

    if-eq v1, v3, :cond_1

    .line 123
    iput v1, p0, Lcom/android/emailcommon/utility/Serializer;->tagPage:I

    .line 124
    iget-object v3, p0, Lcom/android/emailcommon/utility/Serializer;->buf:Ljava/io/ByteArrayOutputStream;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 125
    iget-object v3, p0, Lcom/android/emailcommon/utility/Serializer;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v3, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 128
    :cond_1
    iget-object v4, p0, Lcom/android/emailcommon/utility/Serializer;->buf:Ljava/io/ByteArrayOutputStream;

    if-eqz p1, :cond_3

    move v3, v2

    :goto_1
    invoke-virtual {v4, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 129
    iget-boolean v3, p0, Lcom/android/emailcommon/utility/Serializer;->logging:Z

    if-eqz v3, :cond_2

    .line 130
    sget-object v3, Lcom/android/emailcommon/utility/Tags;->pages:[[Ljava/lang/String;

    aget-object v3, v3, v1

    add-int/lit8 v4, v2, -0x5

    aget-object v0, v3, v4

    .line 131
    .local v0, "name":Ljava/lang/String;
    iget-object v3, p0, Lcom/android/emailcommon/utility/Serializer;->nameStack:[Ljava/lang/String;

    iget v4, p0, Lcom/android/emailcommon/utility/Serializer;->depth:I

    aput-object v0, v3, v4

    .line 132
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x3e

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/emailcommon/utility/Serializer;->log(Ljava/lang/String;)V

    .line 134
    .end local v0    # "name":Ljava/lang/String;
    :cond_2
    iput v5, p0, Lcom/android/emailcommon/utility/Serializer;->pendingTag:I

    goto :goto_0

    .line 128
    :cond_3
    or-int/lit8 v3, v2, 0x40

    goto :goto_1
.end method

.method public data(ILjava/lang/String;)Lcom/android/emailcommon/utility/Serializer;
    .locals 3
    .param p1, "tag"    # I
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    if-nez p2, :cond_0

    .line 165
    const-string v0, "Serializer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Writing null data for tag: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/emailcommon/utility/Serializer;->start(I)Lcom/android/emailcommon/utility/Serializer;

    .line 168
    invoke-virtual {p0, p2}, Lcom/android/emailcommon/utility/Serializer;->text(Ljava/lang/String;)Lcom/android/emailcommon/utility/Serializer;

    .line 169
    invoke-virtual {p0}, Lcom/android/emailcommon/utility/Serializer;->end()Lcom/android/emailcommon/utility/Serializer;

    .line 170
    return-object p0
.end method

.method public done()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    iget v0, p0, Lcom/android/emailcommon/utility/Serializer;->depth:I

    if-eqz v0, :cond_0

    .line 103
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Done received with unclosed tags"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/android/emailcommon/utility/Serializer;->out:Ljava/io/ByteArrayOutputStream;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/emailcommon/utility/Serializer;->writeInteger(Ljava/io/OutputStream;I)V

    .line 106
    iget-object v0, p0, Lcom/android/emailcommon/utility/Serializer;->out:Ljava/io/ByteArrayOutputStream;

    iget-object v1, p0, Lcom/android/emailcommon/utility/Serializer;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 107
    iget-object v0, p0, Lcom/android/emailcommon/utility/Serializer;->out:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 108
    return-void
.end method

.method public end()Lcom/android/emailcommon/utility/Serializer;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 145
    iget v0, p0, Lcom/android/emailcommon/utility/Serializer;->pendingTag:I

    if-ltz v0, :cond_1

    .line 146
    invoke-virtual {p0, v1}, Lcom/android/emailcommon/utility/Serializer;->checkPendingTag(Z)V

    .line 153
    :cond_0
    :goto_0
    iget v0, p0, Lcom/android/emailcommon/utility/Serializer;->depth:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/emailcommon/utility/Serializer;->depth:I

    .line 154
    return-object p0

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/android/emailcommon/utility/Serializer;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 149
    iget-boolean v0, p0, Lcom/android/emailcommon/utility/Serializer;->logging:Z

    if-eqz v0, :cond_0

    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "</"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/emailcommon/utility/Serializer;->nameStack:[Ljava/lang/String;

    iget v2, p0, Lcom/android/emailcommon/utility/Serializer;->depth:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/emailcommon/utility/Serializer;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method log(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 91
    if-nez p1, :cond_0

    .line 99
    :goto_0
    return-void

    .line 93
    :cond_0
    const/16 v1, 0xa

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 94
    .local v0, "cr":I
    if-lez v0, :cond_1

    .line 95
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 97
    :cond_1
    const-string v1, "Serializer"

    invoke-static {v1, p1}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public start(I)Lcom/android/emailcommon/utility/Serializer;
    .locals 1
    .param p1, "tag"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/emailcommon/utility/Serializer;->checkPendingTag(Z)V

    .line 139
    iput p1, p0, Lcom/android/emailcommon/utility/Serializer;->pendingTag:I

    .line 140
    iget v0, p0, Lcom/android/emailcommon/utility/Serializer;->depth:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/emailcommon/utility/Serializer;->depth:I

    .line 141
    return-object p0
.end method

.method public final startDocument()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/emailcommon/utility/Serializer;->out:Ljava/io/ByteArrayOutputStream;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 112
    iget-object v0, p0, Lcom/android/emailcommon/utility/Serializer;->out:Ljava/io/ByteArrayOutputStream;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 113
    iget-object v0, p0, Lcom/android/emailcommon/utility/Serializer;->out:Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x6a

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 114
    return-void
.end method

.method public text(Ljava/lang/String;)Lcom/android/emailcommon/utility/Serializer;
    .locals 3
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 196
    if-nez p1, :cond_0

    .line 197
    const-string v0, "Serializer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Writing null text for pending tag: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/emailcommon/utility/Serializer;->pendingTag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/emailcommon/utility/Serializer;->checkPendingTag(Z)V

    .line 200
    iget-object v0, p0, Lcom/android/emailcommon/utility/Serializer;->buf:Ljava/io/ByteArrayOutputStream;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 201
    iget-object v0, p0, Lcom/android/emailcommon/utility/Serializer;->buf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p0, v0, p1}, Lcom/android/emailcommon/utility/Serializer;->writeLiteralString(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 202
    iget-boolean v0, p0, Lcom/android/emailcommon/utility/Serializer;->logging:Z

    if-eqz v0, :cond_1

    .line 203
    invoke-virtual {p0, p1}, Lcom/android/emailcommon/utility/Serializer;->log(Ljava/lang/String;)V

    .line 205
    :cond_1
    return-object p0
.end method

.method public toByteArray()[B
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/android/emailcommon/utility/Serializer;->out:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/android/emailcommon/utility/Serializer;->out:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method writeInteger(Ljava/io/OutputStream;I)V
    .locals 4
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "i"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 245
    const/4 v3, 0x5

    new-array v0, v3, [B

    .line 246
    .local v0, "buf":[B
    const/4 v1, 0x0

    .line 249
    .local v1, "idx":I
    :goto_0
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "idx":I
    .local v2, "idx":I
    and-int/lit8 v3, p2, 0x7f

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 250
    shr-int/lit8 p2, p2, 0x7

    .line 251
    if-nez p2, :cond_2

    move v1, v2

    .line 253
    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    :goto_1
    const/4 v3, 0x1

    if-le v1, v3, :cond_0

    .line 254
    add-int/lit8 v1, v1, -0x1

    aget-byte v3, v0, v1

    or-int/lit16 v3, v3, 0x80

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    goto :goto_1

    .line 256
    :cond_0
    const/4 v3, 0x0

    aget-byte v3, v0, v3

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    .line 257
    iget-boolean v3, p0, Lcom/android/emailcommon/utility/Serializer;->logging:Z

    if-eqz v3, :cond_1

    .line 258
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/emailcommon/utility/Serializer;->log(Ljava/lang/String;)V

    .line 260
    :cond_1
    return-void

    .end local v1    # "idx":I
    .restart local v2    # "idx":I
    :cond_2
    move v1, v2

    .end local v2    # "idx":I
    .restart local v1    # "idx":I
    goto :goto_0
.end method

.method writeLiteralString(Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 2
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 263
    if-eqz p2, :cond_0

    .line 264
    const-string v1, "UTF-8"

    invoke-virtual {p2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 265
    .local v0, "data":[B
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 267
    .end local v0    # "data":[B
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write(I)V

    .line 268
    return-void
.end method
