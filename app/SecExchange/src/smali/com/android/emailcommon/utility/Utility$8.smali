.class final Lcom/android/emailcommon/utility/Utility$8;
.super Ljava/lang/Object;
.source "Utility.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/emailcommon/utility/Utility;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$dbPath:Ljava/lang/String;


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 5150
    :try_start_0
    invoke-static {}, Lcom/android/sdp/SdpManager;->getSdpManager()Lcom/android/sdp/SdpManager;

    move-result-object v1

    .line 5151
    .local v1, "sdp":Lcom/android/sdp/SdpManager;
    if-eqz v1, :cond_0

    .line 5152
    const-string v2, "Utility"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setSdpState: call updateSDPStateToDB(), state = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/android/sdp/SdpManager;->SDP_GetState()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5153
    iget-object v2, p0, Lcom/android/emailcommon/utility/Utility$8;->val$dbPath:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/android/sdp/SdpManager;->SDP_GetState()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/android/sdp/SdpManager;->updateSDPStateToDB(Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5158
    .end local v1    # "sdp":Lcom/android/sdp/SdpManager;
    :cond_0
    :goto_0
    return-void

    .line 5155
    :catch_0
    move-exception v0

    .line 5156
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "Email"

    invoke-static {v2, v0}, Lcom/android/emailcommon/utility/Log;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
