.class public Lcom/android/emailcommon/utility/BodyUtilites;
.super Ljava/lang/Object;
.source "BodyUtilites.java"


# static fields
.field private static sCutText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-string v0, "..."

    sput-object v0, Lcom/android/emailcommon/utility/BodyUtilites;->sCutText:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deleteAllAccountBodyFilesUri(Landroid/content/Context;J)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J

    .prologue
    const/4 v2, 0x0

    .line 184
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/android/emailcommon/utility/BodyUtilites;->getAccountFileBodyDeleteUri(J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 185
    return-void
.end method

.method public static deleteAllMailboxBodyFiles(Landroid/content/Context;JJZ)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "mailboxId"    # J
    .param p5, "useUriApi"    # Z

    .prologue
    const/4 v7, 0x0

    .line 138
    const-string v0, "BodyUtilites"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "deleteAllMailboxBodyFiles accountId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mailboxId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Message;->ID_COLUMN_PROJECTION:[Ljava/lang/String;

    const-string v3, "mailboxKey=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 144
    .local v6, "c":Landroid/database/Cursor;
    :goto_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 146
    .local v8, "messageId":J
    if-eqz p5, :cond_0

    .line 147
    invoke-static {p0, p1, p2, v8, v9}, Lcom/android/emailcommon/utility/BodyUtilites;->deleteAllMessageBodyFilesUri(Landroid/content/Context;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 153
    .end local v8    # "messageId":J
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    .line 149
    .restart local v8    # "messageId":J
    :cond_0
    :try_start_1
    invoke-static {p0, p1, p2, v8, v9}, Lcom/android/emailcommon/utility/BodyUtilites;->deleteAllMessageBodyFiles(Landroid/content/Context;JJ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 153
    .end local v8    # "messageId":J
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 155
    return-void
.end method

.method public static deleteAllMessageBodyFiles(Landroid/content/Context;JJ)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J

    .prologue
    .line 92
    invoke-static {p0, p3, p4}, Lcom/android/emailcommon/provider/EmailContent$Body;->restoreFileSaveFlags(Landroid/content/Context;J)I

    move-result v2

    .line 93
    .local v2, "flags":I
    const-string v3, "BodyUtilites"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deleteAllMessageBodyFiles accountId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " messageId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " flags = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    if-lez v2, :cond_4

    .line 95
    invoke-static {p0, p1, p2}, Lcom/android/emailcommon/utility/BodyUtilites;->getBodyDirectory(Landroid/content/Context;J)Ljava/io/File;

    move-result-object v0

    .line 96
    .local v0, "directory":Ljava/io/File;
    const/4 v1, 0x0

    .line 97
    .local v1, "file":Ljava/io/File;
    if-eqz v0, :cond_5

    .line 98
    const-string v3, "BodyUtilites"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "directory = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    new-instance v1, Ljava/io/File;

    .end local v1    # "file":Ljava/io/File;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "fileHtmlContent"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 101
    .restart local v1    # "file":Ljava/io/File;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 102
    :cond_0
    new-instance v1, Ljava/io/File;

    .end local v1    # "file":Ljava/io/File;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "fileTextContent"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 103
    .restart local v1    # "file":Ljava/io/File;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 104
    :cond_1
    new-instance v1, Ljava/io/File;

    .end local v1    # "file":Ljava/io/File;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "fileHtmlReply"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 105
    .restart local v1    # "file":Ljava/io/File;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 106
    :cond_2
    new-instance v1, Ljava/io/File;

    .end local v1    # "file":Ljava/io/File;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "fileTextReply"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 107
    .restart local v1    # "file":Ljava/io/File;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 108
    :cond_3
    new-instance v1, Ljava/io/File;

    .end local v1    # "file":Ljava/io/File;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "fileIntro"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 109
    .restart local v1    # "file":Ljava/io/File;
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 114
    .end local v0    # "directory":Ljava/io/File;
    .end local v1    # "file":Ljava/io/File;
    :cond_4
    :goto_0
    return-void

    .line 111
    .restart local v0    # "directory":Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    :cond_5
    const-string v3, "BodyUtilites"

    const-string v4, "deleteAllMessageBodyFiles directory is null"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static deleteAllMessageBodyFilesUri(Landroid/content/Context;J)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "messageId"    # J

    .prologue
    const/4 v4, 0x0

    .line 125
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p0, p1, p2}, Lcom/android/emailcommon/provider/EmailContent$Account;->getAccountIdForMessageId(Landroid/content/Context;J)J

    move-result-wide v2

    invoke-static {v2, v3, p1, p2}, Lcom/android/emailcommon/utility/BodyUtilites;->getFileBodyDeleteUri(JJ)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 126
    return-void
.end method

.method public static deleteAllMessageBodyFilesUri(Landroid/content/Context;JJ)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J

    .prologue
    const/4 v2, 0x0

    .line 121
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1, p2, p3, p4}, Lcom/android/emailcommon/utility/BodyUtilites;->getFileBodyDeleteUri(JJ)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 122
    return-void
.end method

.method private static getAccountFileBodyDeleteUri(J)Landroid/net/Uri;
    .locals 2
    .param p0, "accountId"    # J

    .prologue
    .line 87
    sget-object v0, Lcom/android/emailcommon/provider/EmailContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "body"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getBodyDirectory(Landroid/content/Context;J)Ljava/io/File;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J

    .prologue
    .line 49
    const/4 v0, 0x0

    .line 50
    .local v0, "directory":Ljava/io/File;
    if-eqz p0, :cond_0

    .line 51
    new-instance v0, Ljava/io/File;

    .end local v0    # "directory":Ljava/io/File;
    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".msgs_body"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 53
    .restart local v0    # "directory":Ljava/io/File;
    :cond_0
    return-object v0
.end method

.method public static getCutText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 358
    sget-object v0, Lcom/android/emailcommon/utility/BodyUtilites;->sCutText:Ljava/lang/String;

    return-object v0
.end method

.method private static getFileBodyDeleteUri(JJ)Landroid/net/Uri;
    .locals 2
    .param p0, "accountId"    # J
    .param p2, "messageId"    # J

    .prologue
    .line 81
    sget-object v0, Lcom/android/emailcommon/provider/EmailContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "body"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getFileBodyUri(JJLjava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "accountId"    # J
    .param p2, "messageId"    # J
    .param p4, "part"    # Ljava/lang/String;

    .prologue
    .line 57
    sget-object v0, Lcom/android/emailcommon/provider/EmailContent;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "body"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static isBodyFileExists(Landroid/content/Context;JJLjava/lang/String;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J
    .param p5, "part"    # Ljava/lang/String;

    .prologue
    const-wide/16 v4, 0x0

    .line 277
    const/4 v2, 0x1

    .line 278
    .local v2, "result":Z
    if-eqz p0, :cond_0

    cmp-long v3, p1, v4

    if-lez v3, :cond_0

    cmp-long v3, p3, v4

    if-lez v3, :cond_0

    if-eqz p5, :cond_0

    .line 279
    const/4 v1, 0x0

    .line 281
    .local v1, "inputStream":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {p1, p2, p3, p4, p5}, Lcom/android/emailcommon/utility/BodyUtilites;->getFileBodyUri(JJLjava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 285
    if-eqz v1, :cond_0

    .line 287
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 293
    .end local v1    # "inputStream":Ljava/io/InputStream;
    :cond_0
    :goto_0
    const-string v3, "BodyUtilites"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isBodyFileExists accountId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " messageId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " part = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " exist = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    return v2

    .line 282
    .restart local v1    # "inputStream":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 283
    .local v0, "e":Ljava/io/FileNotFoundException;
    const/4 v2, 0x0

    .line 285
    if-eqz v1, :cond_0

    .line 287
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 288
    :catch_1
    move-exception v3

    goto :goto_0

    .line 285
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 287
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 290
    :cond_1
    :goto_1
    throw v3

    .line 288
    :catch_2
    move-exception v3

    goto :goto_0

    :catch_3
    move-exception v4

    goto :goto_1
.end method

.method public static isHtmlContentFileExists(Landroid/content/Context;JJ)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J

    .prologue
    .line 342
    const-string v6, "fileHtmlContent"

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v1 .. v6}, Lcom/android/emailcommon/utility/BodyUtilites;->isBodyFileExists(Landroid/content/Context;JJLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isHtmlReplyFileExists(Landroid/content/Context;JJ)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J

    .prologue
    .line 350
    const-string v6, "fileHtmlReply"

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v1 .. v6}, Lcom/android/emailcommon/utility/BodyUtilites;->isBodyFileExists(Landroid/content/Context;JJLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isIntroFileExists(Landroid/content/Context;JJ)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J

    .prologue
    .line 354
    const-string v6, "fileIntro"

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v1 .. v6}, Lcom/android/emailcommon/utility/BodyUtilites;->isBodyFileExists(Landroid/content/Context;JJLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isTextContentFileExists(Landroid/content/Context;JJ)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J

    .prologue
    .line 338
    const-string v6, "fileTextContent"

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v1 .. v6}, Lcom/android/emailcommon/utility/BodyUtilites;->isBodyFileExists(Landroid/content/Context;JJLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isTextReplyFileExists(Landroid/content/Context;JJ)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J

    .prologue
    .line 346
    const-string v6, "fileTextReply"

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v1 .. v6}, Lcom/android/emailcommon/utility/BodyUtilites;->isBodyFileExists(Landroid/content/Context;JJLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static readBodyContentFromFile(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/high16 v9, 0x40000

    .line 230
    const-string v6, "BodyUtilites"

    const-string v7, "readBodyContentFromFile() : start"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    const/4 v5, 0x0

    .line 232
    .local v5, "readContent":Ljava/lang/String;
    if-eqz p0, :cond_2

    .line 233
    const/4 v2, 0x0

    .line 236
    .local v2, "inputStream":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {v6, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 241
    :goto_0
    if-eqz v2, :cond_0

    .line 242
    new-array v0, v9, [B

    .line 243
    .local v0, "buffer":[B
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4, v9}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 244
    .local v4, "outputStream":Ljava/io/ByteArrayOutputStream;
    const/4 v3, 0x0

    .line 246
    .local v3, "n":I
    :goto_1
    const/4 v6, -0x1

    :try_start_1
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-eq v6, v3, :cond_1

    .line 247
    const/4 v6, 0x0

    invoke-virtual {v4, v0, v6, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 251
    :catch_0
    move-exception v1

    .line 252
    .local v1, "e":Ljava/io/IOException;
    :try_start_2
    const-string v6, "BodyUtilites"

    invoke-static {v6, v1}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 255
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 261
    :goto_2
    :try_start_4
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 272
    .end local v0    # "buffer":[B
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "inputStream":Ljava/io/InputStream;
    .end local v3    # "n":I
    .end local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :cond_0
    :goto_3
    const-string v6, "BodyUtilites"

    const-string v7, "readBodyContentFromFile() : finish"

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    return-object v5

    .line 237
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    :catch_1
    move-exception v1

    .line 238
    .local v1, "e":Ljava/io/FileNotFoundException;
    const-string v6, "BodyUtilites"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "readBodyContentFromFile() FileNotFoundException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 249
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .restart local v0    # "buffer":[B
    .restart local v3    # "n":I
    .restart local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :cond_1
    :try_start_5
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 250
    const-string v6, "UTF-8"

    invoke-virtual {v4, v6}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v5

    .line 255
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 261
    :goto_4
    :try_start_7
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_3

    .line 262
    :catch_2
    move-exception v1

    .line 263
    .local v1, "e":Ljava/io/IOException;
    const-string v6, "BodyUtilites"

    invoke-static {v6, v1}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_3

    .line 256
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 257
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v6, "BodyUtilites"

    invoke-static {v6, v1}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_4

    .line 256
    :catch_4
    move-exception v1

    .line 257
    const-string v6, "BodyUtilites"

    invoke-static {v6, v1}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    .line 262
    :catch_5
    move-exception v1

    .line 263
    const-string v6, "BodyUtilites"

    invoke-static {v6, v1}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_3

    .line 254
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 255
    :try_start_8
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 261
    :goto_5
    :try_start_9
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_7

    .line 264
    :goto_6
    throw v6

    .line 256
    :catch_6
    move-exception v1

    .line 257
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v7, "BodyUtilites"

    invoke-static {v7, v1}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_5

    .line 262
    .end local v1    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v1

    .line 263
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v7, "BodyUtilites"

    invoke-static {v7, v1}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_6

    .line 269
    .end local v0    # "buffer":[B
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "inputStream":Ljava/io/InputStream;
    .end local v3    # "n":I
    .end local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :cond_2
    const-string v6, "BodyUtilites"

    const-string v7, "readBodyContentFromFile() : context is null! Return null."

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public static readHtmlContentFromFile(Landroid/content/Context;JJ)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J

    .prologue
    .line 318
    const-string v0, "fileHtmlContent"

    invoke-static {p1, p2, p3, p4, v0}, Lcom/android/emailcommon/utility/BodyUtilites;->getFileBodyUri(JJLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/emailcommon/utility/BodyUtilites;->readBodyContentFromFile(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static readHtmlReplyFromFile(Landroid/content/Context;JJ)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J

    .prologue
    .line 326
    const-string v0, "fileHtmlReply"

    invoke-static {p1, p2, p3, p4, v0}, Lcom/android/emailcommon/utility/BodyUtilites;->getFileBodyUri(JJLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/emailcommon/utility/BodyUtilites;->readBodyContentFromFile(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static readIntroFromFile(Landroid/content/Context;JJ)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J

    .prologue
    .line 334
    const-string v0, "fileIntro"

    invoke-static {p1, p2, p3, p4, v0}, Lcom/android/emailcommon/utility/BodyUtilites;->getFileBodyUri(JJLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/emailcommon/utility/BodyUtilites;->readBodyContentFromFile(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static readTextContentFromFile(Landroid/content/Context;JJ)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J

    .prologue
    .line 322
    const-string v0, "fileTextContent"

    invoke-static {p1, p2, p3, p4, v0}, Lcom/android/emailcommon/utility/BodyUtilites;->getFileBodyUri(JJLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/emailcommon/utility/BodyUtilites;->readBodyContentFromFile(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static readTextReplyFromFile(Landroid/content/Context;JJ)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J

    .prologue
    .line 330
    const-string v0, "fileTextReply"

    invoke-static {p1, p2, p3, p4, v0}, Lcom/android/emailcommon/utility/BodyUtilites;->getFileBodyUri(JJLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/emailcommon/utility/BodyUtilites;->readBodyContentFromFile(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static setCutText(Ljava/lang/String;)V
    .locals 0
    .param p0, "cutText"    # Ljava/lang/String;

    .prologue
    .line 362
    sput-object p0, Lcom/android/emailcommon/utility/BodyUtilites;->sCutText:Ljava/lang/String;

    .line 363
    return-void
.end method

.method private static writeBodyContentToFile(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "contentToWrite"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 188
    const-string v3, "BodyUtilites"

    const-string v4, "writeBodyContentToFile() : start"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    if-nez p0, :cond_0

    .line 190
    const-string v3, "BodyUtilites"

    const-string v4, "writeBodyContentToFile() : context is null! Do nothing."

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :goto_0
    return-void

    .line 194
    :cond_0
    if-nez p2, :cond_1

    .line 195
    const-string v3, "BodyUtilites"

    const-string v4, "writeBodyContentToFile() : contentToWrite is null. Replace by empty string"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const-string p2, ""

    .line 199
    :cond_1
    const/4 v2, 0x0

    .line 202
    .local v2, "outputStream":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 207
    :goto_1
    if-eqz v2, :cond_2

    .line 209
    :try_start_1
    const-string v3, "UTF-8"

    invoke-virtual {p2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write([B)V

    .line 210
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 215
    :try_start_2
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 221
    :goto_2
    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isSdpEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 222
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 223
    .local v0, "cv":Landroid/content/ContentValues;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, p1, v0, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 226
    .end local v0    # "cv":Landroid/content/ContentValues;
    :cond_2
    const-string v3, "BodyUtilites"

    const-string v4, "writeBodyContentToFile() : finish"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 203
    :catch_0
    move-exception v1

    .line 204
    .local v1, "e":Ljava/io/FileNotFoundException;
    const-string v3, "BodyUtilites"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FileNotFoundException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 216
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 217
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "BodyUtilites"

    invoke-static {v3, v1}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    .line 211
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 212
    .restart local v1    # "e":Ljava/io/IOException;
    :try_start_3
    const-string v3, "BodyUtilites"

    invoke-static {v3, v1}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 215
    :try_start_4
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_2

    .line 216
    :catch_3
    move-exception v1

    .line 217
    const-string v3, "BodyUtilites"

    invoke-static {v3, v1}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    .line 214
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 215
    :try_start_5
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 218
    :goto_3
    throw v3

    .line 216
    :catch_4
    move-exception v1

    .line 217
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v4, "BodyUtilites"

    invoke-static {v4, v1}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_3
.end method

.method public static writeHtmlContentToFile(Landroid/content/Context;JJLjava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J
    .param p5, "contentToWrite"    # Ljava/lang/String;

    .prologue
    .line 298
    const-string v0, "fileHtmlContent"

    invoke-static {p1, p2, p3, p4, v0}, Lcom/android/emailcommon/utility/BodyUtilites;->getFileBodyUri(JJLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0, p5}, Lcom/android/emailcommon/utility/BodyUtilites;->writeBodyContentToFile(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V

    .line 299
    return-void
.end method

.method public static writeHtmlReplyToFile(Landroid/content/Context;JJLjava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J
    .param p5, "contentToWrite"    # Ljava/lang/String;

    .prologue
    .line 306
    const-string v0, "fileHtmlReply"

    invoke-static {p1, p2, p3, p4, v0}, Lcom/android/emailcommon/utility/BodyUtilites;->getFileBodyUri(JJLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0, p5}, Lcom/android/emailcommon/utility/BodyUtilites;->writeBodyContentToFile(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V

    .line 307
    return-void
.end method

.method public static writeIntroToFile(Landroid/content/Context;JJLjava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J
    .param p5, "contentToWrite"    # Ljava/lang/String;

    .prologue
    .line 314
    const-string v0, "fileIntro"

    invoke-static {p1, p2, p3, p4, v0}, Lcom/android/emailcommon/utility/BodyUtilites;->getFileBodyUri(JJLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0, p5}, Lcom/android/emailcommon/utility/BodyUtilites;->writeBodyContentToFile(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V

    .line 315
    return-void
.end method

.method public static writeTextContentToFile(Landroid/content/Context;JJLjava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J
    .param p5, "contentToWrite"    # Ljava/lang/String;

    .prologue
    .line 302
    const-string v0, "fileTextContent"

    invoke-static {p1, p2, p3, p4, v0}, Lcom/android/emailcommon/utility/BodyUtilites;->getFileBodyUri(JJLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0, p5}, Lcom/android/emailcommon/utility/BodyUtilites;->writeBodyContentToFile(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V

    .line 303
    return-void
.end method

.method public static writeTextReplyToFile(Landroid/content/Context;JJLjava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J
    .param p3, "messageId"    # J
    .param p5, "contentToWrite"    # Ljava/lang/String;

    .prologue
    .line 310
    const-string v0, "fileTextReply"

    invoke-static {p1, p2, p3, p4, v0}, Lcom/android/emailcommon/utility/BodyUtilites;->getFileBodyUri(JJLjava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0, p5}, Lcom/android/emailcommon/utility/BodyUtilites;->writeBodyContentToFile(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V

    .line 311
    return-void
.end method
