.class public Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor;
.super Ljava/lang/Object;
.source "RecurrenceProcessor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor$DaySet;
    }
.end annotation


# static fields
.field private static final DAYS_IN_YEAR_PRECEDING_MONTH:[I

.field private static final DAYS_PER_MONTH:[I


# instance fields
.field private Monthly_Lastday:Z

.field private Monthly_week_day:Z

.field private Monthly_weekend:Z

.field private mDays:Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor$DaySet;

.field private mGenerated:Landroid/text/format/Time;

.field private mIterator:Landroid/text/format/Time;

.field private mStringBuilder:Ljava/lang/StringBuilder;

.field private mUntil:Landroid/text/format/Time;

.field private yearly_anyday:Z

.field private yearly_first_second_third_fourth_day_of_month:Z

.field private yearly_lastday_of_month:Z

.field private yearly_weekday:Z

.field private yearly_weekend:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xc

    .line 46
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor;->DAYS_PER_MONTH:[I

    .line 47
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor;->DAYS_IN_YEAR_PRECEDING_MONTH:[I

    return-void

    .line 46
    nop

    :array_0
    .array-data 4
        0x1f
        0x1c
        0x1f
        0x1e
        0x1f
        0x1e
        0x1f
        0x1f
        0x1e
        0x1f
        0x1e
        0x1f
    .end array-data

    .line 47
    :array_1
    .array-data 4
        0x0
        0x1f
        0x3b
        0x5a
        0x78
        0x97
        0xb4
        0xd4
        0xf3
        0x111
        0x130
        0x14e
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor;->mIterator:Landroid/text/format/Time;

    .line 24
    new-instance v0, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor;->mUntil:Landroid/text/format/Time;

    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor;->mStringBuilder:Ljava/lang/StringBuilder;

    .line 26
    new-instance v0, Landroid/text/format/Time;

    const-string v1, "UTC"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor;->mGenerated:Landroid/text/format/Time;

    .line 27
    new-instance v0, Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor$DaySet;

    invoke-direct {v0, v2}, Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor$DaySet;-><init>(Z)V

    iput-object v0, p0, Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor;->mDays:Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor$DaySet;

    .line 28
    iput-boolean v2, p0, Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor;->Monthly_Lastday:Z

    .line 29
    iput-boolean v2, p0, Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor;->Monthly_weekend:Z

    .line 30
    iput-boolean v2, p0, Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor;->Monthly_week_day:Z

    .line 31
    iput-boolean v2, p0, Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor;->yearly_anyday:Z

    .line 32
    iput-boolean v2, p0, Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor;->yearly_weekday:Z

    .line 33
    iput-boolean v2, p0, Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor;->yearly_weekend:Z

    .line 34
    iput-boolean v2, p0, Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor;->yearly_first_second_third_fourth_day_of_month:Z

    .line 35
    iput-boolean v2, p0, Lcom/android/emailcommon/utility/calendar/RecurrenceProcessor;->yearly_lastday_of_month:Z

    .line 41
    return-void
.end method
