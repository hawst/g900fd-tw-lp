.class public Lcom/android/emailcommon/utility/calendar/EventRecurrence;
.super Ljava/lang/Object;
.source "EventRecurrence.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/utility/calendar/EventRecurrence$1;,
        Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseWkst;,
        Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseBySetPos;,
        Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByMonth;,
        Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByWeekNo;,
        Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByYearDay;,
        Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByMonthDay;,
        Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByDay;,
        Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByHour;,
        Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByMinute;,
        Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseBySecond;,
        Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseInterval;,
        Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseCount;,
        Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseUntil;,
        Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseFreq;,
        Lcom/android/emailcommon/utility/calendar/EventRecurrence$PartParser;,
        Lcom/android/emailcommon/utility/calendar/EventRecurrence$InvalidFormatException;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;

.field private static final sParseFreqMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static sParsePartMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/emailcommon/utility/calendar/EventRecurrence$PartParser;",
            ">;"
        }
    .end annotation
.end field

.field private static final sParseWeekdayMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public byday:[I

.field public bydayCount:I

.field public bydayNum:[I

.field public byhour:[I

.field public byhourCount:I

.field public byminute:[I

.field public byminuteCount:I

.field public bymonth:[I

.field public bymonthCount:I

.field public bymonthday:[I

.field public bymonthdayCount:I

.field public bysecond:[I

.field public bysecondCount:I

.field public bysetpos:[I

.field public bysetposCount:I

.field public byweekno:[I

.field public byweeknoCount:I

.field public byyearday:[I

.field public byyeardayCount:I

.field public count:I

.field public freq:I

.field public interval:I

.field public startDate:Landroid/text/format/Time;

.field public until:Ljava/lang/String;

.field public wkst:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 33
    const-string v0, "EventRecur"

    sput-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->TAG:Ljava/lang/String;

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParsePartMap:Ljava/util/HashMap;

    .line 84
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParsePartMap:Ljava/util/HashMap;

    const-string v1, "FREQ"

    new-instance v2, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseFreq;

    invoke-direct {v2, v3}, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseFreq;-><init>(Lcom/android/emailcommon/utility/calendar/EventRecurrence$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParsePartMap:Ljava/util/HashMap;

    const-string v1, "UNTIL"

    new-instance v2, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseUntil;

    invoke-direct {v2, v3}, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseUntil;-><init>(Lcom/android/emailcommon/utility/calendar/EventRecurrence$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParsePartMap:Ljava/util/HashMap;

    const-string v1, "COUNT"

    new-instance v2, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseCount;

    invoke-direct {v2, v3}, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseCount;-><init>(Lcom/android/emailcommon/utility/calendar/EventRecurrence$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParsePartMap:Ljava/util/HashMap;

    const-string v1, "INTERVAL"

    new-instance v2, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseInterval;

    invoke-direct {v2, v3}, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseInterval;-><init>(Lcom/android/emailcommon/utility/calendar/EventRecurrence$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParsePartMap:Ljava/util/HashMap;

    const-string v1, "BYSECOND"

    new-instance v2, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseBySecond;

    invoke-direct {v2, v3}, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseBySecond;-><init>(Lcom/android/emailcommon/utility/calendar/EventRecurrence$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParsePartMap:Ljava/util/HashMap;

    const-string v1, "BYMINUTE"

    new-instance v2, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByMinute;

    invoke-direct {v2, v3}, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByMinute;-><init>(Lcom/android/emailcommon/utility/calendar/EventRecurrence$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParsePartMap:Ljava/util/HashMap;

    const-string v1, "BYHOUR"

    new-instance v2, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByHour;

    invoke-direct {v2, v3}, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByHour;-><init>(Lcom/android/emailcommon/utility/calendar/EventRecurrence$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParsePartMap:Ljava/util/HashMap;

    const-string v1, "BYDAY"

    new-instance v2, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByDay;

    invoke-direct {v2, v3}, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByDay;-><init>(Lcom/android/emailcommon/utility/calendar/EventRecurrence$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParsePartMap:Ljava/util/HashMap;

    const-string v1, "BYMONTHDAY"

    new-instance v2, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByMonthDay;

    invoke-direct {v2, v3}, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByMonthDay;-><init>(Lcom/android/emailcommon/utility/calendar/EventRecurrence$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParsePartMap:Ljava/util/HashMap;

    const-string v1, "BYYEARDAY"

    new-instance v2, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByYearDay;

    invoke-direct {v2, v3}, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByYearDay;-><init>(Lcom/android/emailcommon/utility/calendar/EventRecurrence$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParsePartMap:Ljava/util/HashMap;

    const-string v1, "BYWEEKNO"

    new-instance v2, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByWeekNo;

    invoke-direct {v2, v3}, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByWeekNo;-><init>(Lcom/android/emailcommon/utility/calendar/EventRecurrence$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParsePartMap:Ljava/util/HashMap;

    const-string v1, "BYMONTH"

    new-instance v2, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByMonth;

    invoke-direct {v2, v3}, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseByMonth;-><init>(Lcom/android/emailcommon/utility/calendar/EventRecurrence$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParsePartMap:Ljava/util/HashMap;

    const-string v1, "BYSETPOS"

    new-instance v2, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseBySetPos;

    invoke-direct {v2, v3}, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseBySetPos;-><init>(Lcom/android/emailcommon/utility/calendar/EventRecurrence$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParsePartMap:Ljava/util/HashMap;

    const-string v1, "WKST"

    new-instance v2, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseWkst;

    invoke-direct {v2, v3}, Lcom/android/emailcommon/utility/calendar/EventRecurrence$ParseWkst;-><init>(Lcom/android/emailcommon/utility/calendar/EventRecurrence$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParseFreqMap:Ljava/util/HashMap;

    .line 119
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParseFreqMap:Ljava/util/HashMap;

    const-string v1, "SECONDLY"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParseFreqMap:Ljava/util/HashMap;

    const-string v1, "MINUTELY"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParseFreqMap:Ljava/util/HashMap;

    const-string v1, "HOURLY"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParseFreqMap:Ljava/util/HashMap;

    const-string v1, "DAILY"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParseFreqMap:Ljava/util/HashMap;

    const-string v1, "WEEKLY"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParseFreqMap:Ljava/util/HashMap;

    const-string v1, "MONTHLY"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParseFreqMap:Ljava/util/HashMap;

    const-string v1, "YEARLY"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParseWeekdayMap:Ljava/util/HashMap;

    .line 131
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParseWeekdayMap:Ljava/util/HashMap;

    const-string v1, "SU"

    const/high16 v2, 0x10000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParseWeekdayMap:Ljava/util/HashMap;

    const-string v1, "MO"

    const/high16 v2, 0x20000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParseWeekdayMap:Ljava/util/HashMap;

    const-string v1, "TU"

    const/high16 v2, 0x40000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParseWeekdayMap:Ljava/util/HashMap;

    const-string v1, "WE"

    const/high16 v2, 0x80000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParseWeekdayMap:Ljava/util/HashMap;

    const-string v1, "TH"

    const/high16 v2, 0x100000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParseWeekdayMap:Ljava/util/HashMap;

    const-string v1, "FR"

    const/high16 v2, 0x200000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->sParseWeekdayMap:Ljava/util/HashMap;

    const-string v1, "SA"

    const/high16 v2, 0x400000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 899
    return-void
.end method

.method private appendByDay(Ljava/lang/StringBuilder;I)V
    .locals 3
    .param p1, "s"    # Ljava/lang/StringBuilder;
    .param p2, "i"    # I

    .prologue
    .line 313
    iget-object v2, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bydayNum:[I

    aget v0, v2, p2

    .line 314
    .local v0, "n":I
    if-eqz v0, :cond_0

    .line 315
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 318
    :cond_0
    iget-object v2, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byday:[I

    aget v2, v2, p2

    invoke-static {v2}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->day2String(I)Ljava/lang/String;

    move-result-object v1

    .line 319
    .local v1, "str":Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    return-void
.end method

.method private static appendNumbers(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V
    .locals 2
    .param p0, "s"    # Ljava/lang/StringBuilder;
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "count"    # I
    .param p3, "values"    # [I

    .prologue
    .line 300
    if-lez p2, :cond_1

    .line 301
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    add-int/lit8 p2, p2, -0x1

    .line 303
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 304
    aget v1, p3, v0

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 305
    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 303
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 307
    :cond_0
    aget v1, p3, p2

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 309
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method private static arraysEqual([II[II)Z
    .locals 4
    .param p0, "array1"    # [I
    .param p1, "count1"    # I
    .param p2, "array2"    # [I
    .param p3, "count2"    # I

    .prologue
    const/4 v1, 0x0

    .line 458
    if-eq p1, p3, :cond_1

    .line 467
    :cond_0
    :goto_0
    return v1

    .line 462
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, p1, :cond_2

    .line 463
    aget v2, p0, v0

    aget v3, p2, v0

    if-ne v2, v3, :cond_0

    .line 462
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 467
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static day2String(I)Ljava/lang/String;
    .locals 3
    .param p0, "day"    # I

    .prologue
    .line 277
    sparse-switch p0, :sswitch_data_0

    .line 293
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bad day argument: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 279
    :sswitch_0
    const-string v0, "SU"

    .line 291
    :goto_0
    return-object v0

    .line 281
    :sswitch_1
    const-string v0, "MO"

    goto :goto_0

    .line 283
    :sswitch_2
    const-string v0, "TU"

    goto :goto_0

    .line 285
    :sswitch_3
    const-string v0, "WE"

    goto :goto_0

    .line 287
    :sswitch_4
    const-string v0, "TH"

    goto :goto_0

    .line 289
    :sswitch_5
    const-string v0, "FR"

    goto :goto_0

    .line 291
    :sswitch_6
    const-string v0, "SA"

    goto :goto_0

    .line 277
    nop

    :sswitch_data_0
    .sparse-switch
        0x10000 -> :sswitch_0
        0x20000 -> :sswitch_1
        0x40000 -> :sswitch_2
        0x80000 -> :sswitch_3
        0x100000 -> :sswitch_4
        0x200000 -> :sswitch_5
        0x400000 -> :sswitch_6
    .end sparse-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 472
    if-ne p0, p1, :cond_1

    .line 480
    :cond_0
    :goto_0
    return v1

    .line 475
    :cond_1
    instance-of v3, p1, Lcom/android/emailcommon/utility/calendar/EventRecurrence;

    if-nez v3, :cond_2

    move v1, v2

    .line 476
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 479
    check-cast v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;

    .line 480
    .local v0, "er":Lcom/android/emailcommon/utility/calendar/EventRecurrence;
    iget-object v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->startDate:Landroid/text/format/Time;

    if-nez v3, :cond_4

    iget-object v3, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->startDate:Landroid/text/format/Time;

    if-nez v3, :cond_3

    :goto_1
    iget v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->freq:I

    iget v4, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->freq:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->until:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->until:Ljava/lang/String;

    if-nez v3, :cond_3

    :goto_2
    iget v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->count:I

    iget v4, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->count:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->interval:I

    iget v4, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->interval:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->wkst:I

    iget v4, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->wkst:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bysecond:[I

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bysecondCount:I

    iget-object v5, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bysecond:[I

    iget v6, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bysecondCount:I

    invoke-static {v3, v4, v5, v6}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->arraysEqual([II[II)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byminute:[I

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byminuteCount:I

    iget-object v5, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byminute:[I

    iget v6, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byminuteCount:I

    invoke-static {v3, v4, v5, v6}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->arraysEqual([II[II)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byhour:[I

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byhourCount:I

    iget-object v5, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byhour:[I

    iget v6, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byhourCount:I

    invoke-static {v3, v4, v5, v6}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->arraysEqual([II[II)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byday:[I

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bydayCount:I

    iget-object v5, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byday:[I

    iget v6, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bydayCount:I

    invoke-static {v3, v4, v5, v6}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->arraysEqual([II[II)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bydayNum:[I

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bydayCount:I

    iget-object v5, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bydayNum:[I

    iget v6, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bydayCount:I

    invoke-static {v3, v4, v5, v6}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->arraysEqual([II[II)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bymonthday:[I

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bymonthdayCount:I

    iget-object v5, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bymonthday:[I

    iget v6, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bymonthdayCount:I

    invoke-static {v3, v4, v5, v6}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->arraysEqual([II[II)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byyearday:[I

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byyeardayCount:I

    iget-object v5, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byyearday:[I

    iget v6, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byyeardayCount:I

    invoke-static {v3, v4, v5, v6}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->arraysEqual([II[II)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byweekno:[I

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byweeknoCount:I

    iget-object v5, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byweekno:[I

    iget v6, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byweeknoCount:I

    invoke-static {v3, v4, v5, v6}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->arraysEqual([II[II)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bymonth:[I

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bymonthCount:I

    iget-object v5, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bymonth:[I

    iget v6, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bymonthCount:I

    invoke-static {v3, v4, v5, v6}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->arraysEqual([II[II)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bysetpos:[I

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bysetposCount:I

    iget-object v5, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bysetpos:[I

    iget v6, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bysetposCount:I

    invoke-static {v3, v4, v5, v6}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->arraysEqual([II[II)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto/16 :goto_0

    :cond_4
    iget-object v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->startDate:Landroid/text/format/Time;

    iget-object v4, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->startDate:Landroid/text/format/Time;

    invoke-static {v3, v4}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v3

    if-nez v3, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->until:Ljava/lang/String;

    iget-object v4, v0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->until:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_2
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 501
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 325
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 327
    .local v2, "s":Ljava/lang/StringBuilder;
    const-string v3, "FREQ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 328
    iget v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->freq:I

    packed-switch v3, :pswitch_data_0

    .line 353
    :goto_0
    iget-object v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->until:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 354
    const-string v3, ";UNTIL="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 355
    iget-object v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->until:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 358
    :cond_0
    iget v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->count:I

    if-eqz v3, :cond_1

    .line 359
    const-string v3, ";COUNT="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 360
    iget v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->count:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 363
    :cond_1
    iget v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->interval:I

    if-eqz v3, :cond_2

    .line 364
    const-string v3, ";INTERVAL="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    iget v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->interval:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 368
    :cond_2
    iget v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->wkst:I

    if-eqz v3, :cond_3

    .line 369
    const-string v3, ";WKST="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    iget v3, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->wkst:I

    invoke-static {v3}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->day2String(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    :cond_3
    const-string v3, ";BYSECOND="

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bysecondCount:I

    iget-object v5, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bysecond:[I

    invoke-static {v2, v3, v4, v5}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->appendNumbers(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V

    .line 374
    const-string v3, ";BYMINUTE="

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byminuteCount:I

    iget-object v5, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byminute:[I

    invoke-static {v2, v3, v4, v5}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->appendNumbers(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V

    .line 375
    const-string v3, ";BYSECOND="

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byhourCount:I

    iget-object v5, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byhour:[I

    invoke-static {v2, v3, v4, v5}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->appendNumbers(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V

    .line 378
    iget v0, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bydayCount:I

    .line 379
    .local v0, "count":I
    if-lez v0, :cond_5

    .line 380
    const-string v3, ";BYDAY="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 381
    add-int/lit8 v0, v0, -0x1

    .line 382
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_4

    .line 383
    invoke-direct {p0, v2, v1}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->appendByDay(Ljava/lang/StringBuilder;I)V

    .line 384
    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 382
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 331
    .end local v0    # "count":I
    .end local v1    # "i":I
    :pswitch_0
    const-string v3, "SECONDLY"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 334
    :pswitch_1
    const-string v3, "MINUTELY"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 337
    :pswitch_2
    const-string v3, "HOURLY"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 340
    :pswitch_3
    const-string v3, "DAILY"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 343
    :pswitch_4
    const-string v3, "WEEKLY"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 346
    :pswitch_5
    const-string v3, "MONTHLY"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 349
    :pswitch_6
    const-string v3, "YEARLY"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 386
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_4
    invoke-direct {p0, v2, v0}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->appendByDay(Ljava/lang/StringBuilder;I)V

    .line 389
    .end local v1    # "i":I
    :cond_5
    const-string v3, ";BYMONTHDAY="

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bymonthdayCount:I

    iget-object v5, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bymonthday:[I

    invoke-static {v2, v3, v4, v5}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->appendNumbers(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V

    .line 390
    const-string v3, ";BYYEARDAY="

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byyeardayCount:I

    iget-object v5, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byyearday:[I

    invoke-static {v2, v3, v4, v5}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->appendNumbers(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V

    .line 391
    const-string v3, ";BYWEEKNO="

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byweeknoCount:I

    iget-object v5, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->byweekno:[I

    invoke-static {v2, v3, v4, v5}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->appendNumbers(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V

    .line 392
    const-string v3, ";BYMONTH="

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bymonthCount:I

    iget-object v5, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bymonth:[I

    invoke-static {v2, v3, v4, v5}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->appendNumbers(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V

    .line 393
    const-string v3, ";BYSETPOS="

    iget v4, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bysetposCount:I

    iget-object v5, p0, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->bysetpos:[I

    invoke-static {v2, v3, v4, v5}, Lcom/android/emailcommon/utility/calendar/EventRecurrence;->appendNumbers(Ljava/lang/StringBuilder;Ljava/lang/String;I[I)V

    .line 395
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 328
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
