.class public Lcom/android/emailcommon/utility/AppLogging;
.super Ljava/lang/Object;
.source "AppLogging.java"


# static fields
.field private static enableFloatingFeature:Z

.field private static sFloatingFeatureInstance:Lcom/samsung/android/feature/FloatingFeature;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    sput-object v0, Lcom/android/emailcommon/utility/AppLogging;->sFloatingFeatureInstance:Lcom/samsung/android/feature/FloatingFeature;

    .line 69
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/emailcommon/utility/AppLogging;->enableFloatingFeature:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "feature"    # Ljava/lang/String;

    .prologue
    .line 72
    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/android/emailcommon/utility/AppLogging;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 73
    return-void
.end method

.method static insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "feature"    # Ljava/lang/String;
    .param p3, "extra"    # Ljava/lang/String;
    .param p4, "value"    # J

    .prologue
    .line 91
    invoke-static {}, Lcom/android/emailcommon/utility/AppLogging;->isEnableSurveyMode()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 95
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 96
    .local v1, "cr":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 97
    .local v2, "cv":Landroid/content/ContentValues;
    const-string v4, "app_id"

    invoke-virtual {v2, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v4, "feature"

    invoke-virtual {v2, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    if-eqz p3, :cond_0

    const-string v4, "extra"

    invoke-virtual {v2, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_0
    const-wide/16 v4, -0x1

    cmp-long v4, p4, v4

    if-eqz v4, :cond_1

    const-string v4, "value"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 102
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 104
    .local v0, "broadcastIntent":Landroid/content/Intent;
    const-string v4, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 105
    const-string v4, "data"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 107
    const-string v4, "com.samsung.android.providers.context"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    .end local v0    # "broadcastIntent":Landroid/content/Intent;
    .end local v1    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "cv":Landroid/content/ContentValues;
    :cond_2
    :goto_0
    return-void

    .line 110
    :catch_0
    move-exception v3

    .line 111
    .local v3, "ex":Ljava/lang/Exception;
    const-string v4, "AppLogging"

    const-string v5, "Error while using insertLog"

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v4, "AppLogging"

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isEnableSurveyMode()Z
    .locals 2

    .prologue
    .line 81
    sget-object v0, Lcom/android/emailcommon/utility/AppLogging;->sFloatingFeatureInstance:Lcom/samsung/android/feature/FloatingFeature;

    if-nez v0, :cond_0

    .line 82
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/utility/AppLogging;->sFloatingFeatureInstance:Lcom/samsung/android/feature/FloatingFeature;

    .line 83
    sget-object v0, Lcom/android/emailcommon/utility/AppLogging;->sFloatingFeatureInstance:Lcom/samsung/android/feature/FloatingFeature;

    const-string v1, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/AppLogging;->enableFloatingFeature:Z

    .line 85
    :cond_0
    sget-boolean v0, Lcom/android/emailcommon/utility/AppLogging;->enableFloatingFeature:Z

    return v0
.end method
