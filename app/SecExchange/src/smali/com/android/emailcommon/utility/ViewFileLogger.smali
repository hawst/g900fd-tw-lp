.class public Lcom/android/emailcommon/utility/ViewFileLogger;
.super Ljava/lang/Object;
.source "ViewFileLogger.java"


# static fields
.field private static LOGGER:Lcom/android/emailcommon/utility/ViewFileLogger;

.field public static LOG_FILE_NAME:Ljava/lang/String;

.field private static file:Ljava/io/File;

.field private static sLogWriter:Ljava/io/FileWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 15
    sput-object v0, Lcom/android/emailcommon/utility/ViewFileLogger;->LOGGER:Lcom/android/emailcommon/utility/ViewFileLogger;

    .line 17
    sput-object v0, Lcom/android/emailcommon/utility/ViewFileLogger;->sLogWriter:Ljava/io/FileWriter;

    .line 19
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/emailviewlog.txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/utility/ViewFileLogger;->LOG_FILE_NAME:Ljava/lang/String;

    .line 22
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/android/emailcommon/utility/ViewFileLogger;->LOG_FILE_NAME:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/emailcommon/utility/ViewFileLogger;->file:Ljava/io/File;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    :try_start_0
    sget-object v0, Lcom/android/emailcommon/utility/ViewFileLogger;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/utility/ViewFileLogger;->file:Ljava/io/File;

    .line 32
    new-instance v0, Ljava/io/FileWriter;

    sget-object v1, Lcom/android/emailcommon/utility/ViewFileLogger;->file:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Lcom/android/emailcommon/utility/ViewFileLogger;->sLogWriter:Ljava/io/FileWriter;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :goto_0
    return-void

    .line 33
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static declared-synchronized log(Ljava/lang/String;Ljava/lang/String;)V
    .locals 17
    .param p0, "prefix"    # Ljava/lang/String;
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 64
    const-class v13, Lcom/android/emailcommon/utility/ViewFileLogger;

    monitor-enter v13

    :try_start_0
    sget-object v12, Lcom/android/emailcommon/utility/ViewFileLogger;->LOGGER:Lcom/android/emailcommon/utility/ViewFileLogger;

    if-eqz v12, :cond_0

    const-string v12, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    sget-object v12, Lcom/android/emailcommon/utility/ViewFileLogger;->file:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_1

    .line 67
    :cond_0
    new-instance v12, Lcom/android/emailcommon/utility/ViewFileLogger;

    invoke-direct {v12}, Lcom/android/emailcommon/utility/ViewFileLogger;-><init>()V

    sput-object v12, Lcom/android/emailcommon/utility/ViewFileLogger;->LOGGER:Lcom/android/emailcommon/utility/ViewFileLogger;

    .line 68
    const-string v12, "Logger"

    const-string v14, "\n"

    invoke-static {v12, v14}, Lcom/android/emailcommon/utility/ViewFileLogger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v12, "Logger"

    const-string v14, "-------------- New Log --------------"

    invoke-static {v12, v14}, Lcom/android/emailcommon/utility/ViewFileLogger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v12, "Logger"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Model      :"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "ro.product.model"

    const-string v16, "Unknown"

    invoke-static/range {v15 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v14}, Lcom/android/emailcommon/utility/ViewFileLogger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v12, "Logger"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Build      :"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "ro.build.PDA"

    const-string v16, "Unknown"

    invoke-static/range {v15 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v14}, Lcom/android/emailcommon/utility/ViewFileLogger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    const-string v12, "Logger"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "ChangeList :"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "ro.build.changelist"

    const-string v16, "Unknown"

    invoke-static/range {v15 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v14}, Lcom/android/emailcommon/utility/ViewFileLogger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v12, "Logger"

    const-string v14, "-------------------------------------\n"

    invoke-static {v12, v14}, Lcom/android/emailcommon/utility/ViewFileLogger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 77
    .local v1, "c":Ljava/util/Calendar;
    const/4 v12, 0x5

    invoke-virtual {v1, v12}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 78
    .local v2, "date":I
    const/4 v12, 0x2

    invoke-virtual {v1, v12}, Ljava/util/Calendar;->get(I)I

    move-result v12

    add-int/lit8 v7, v12, 0x1

    .line 80
    .local v7, "month":I
    const/4 v12, 0x1

    invoke-virtual {v1, v12}, Ljava/util/Calendar;->get(I)I

    move-result v11

    .line 81
    .local v11, "year":I
    const/16 v12, 0xb

    invoke-virtual {v1, v12}, Ljava/util/Calendar;->get(I)I

    move-result v4

    .line 82
    .local v4, "hr":I
    const/16 v12, 0xc

    invoke-virtual {v1, v12}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 83
    .local v6, "min":I
    const/16 v12, 0xd

    invoke-virtual {v1, v12}, Ljava/util/Calendar;->get(I)I

    move-result v10

    .line 84
    .local v10, "sec":I
    const/16 v12, 0xe

    invoke-virtual {v1, v12}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 88
    .local v5, "mSec":I
    new-instance v9, Ljava/lang/StringBuffer;

    const/16 v12, 0x100

    invoke-direct {v9, v12}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 90
    .local v9, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v9, v11}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 91
    const/16 v12, 0x2d

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 93
    const/16 v12, 0xa

    if-ge v7, v12, :cond_2

    .line 94
    const/16 v12, 0x30

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 97
    :cond_2
    invoke-virtual {v9, v7}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 98
    const/16 v12, 0x2d

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 100
    const/16 v12, 0xa

    if-ge v2, v12, :cond_3

    .line 101
    const/16 v12, 0x30

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 104
    :cond_3
    invoke-virtual {v9, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 105
    const/16 v12, 0x20

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 107
    invoke-virtual {v9, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 109
    const/16 v12, 0x3a

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 111
    const/16 v12, 0xa

    if-ge v6, v12, :cond_4

    .line 112
    const/16 v12, 0x30

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 115
    :cond_4
    invoke-virtual {v9, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 116
    const/16 v12, 0x3a

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 118
    const/16 v12, 0xa

    if-ge v10, v12, :cond_5

    .line 119
    const/16 v12, 0x30

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 121
    :cond_5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 122
    const/16 v12, 0x3a

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 124
    const/16 v12, 0xa

    if-ge v5, v12, :cond_9

    .line 125
    const-string v12, "00"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 130
    :cond_6
    :goto_0
    invoke-virtual {v9, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 132
    const/16 v12, 0x20

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 133
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v12

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 134
    const/16 v12, 0x20

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 135
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v12

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 136
    const-string v12, " ["

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 137
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 138
    const-string v12, "] "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 140
    if-eqz p0, :cond_7

    .line 141
    move-object/from16 v0, p0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 142
    const-string v12, "| "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 144
    :cond_7
    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 145
    const-string v12, "\r\n"

    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 146
    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    .line 148
    .local v8, "s":Ljava/lang/String;
    sget-object v12, Lcom/android/emailcommon/utility/ViewFileLogger;->sLogWriter:Ljava/io/FileWriter;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v12, :cond_8

    .line 150
    :try_start_1
    sget-object v12, Lcom/android/emailcommon/utility/ViewFileLogger;->sLogWriter:Ljava/io/FileWriter;

    invoke-virtual {v12, v8}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 151
    sget-object v12, Lcom/android/emailcommon/utility/ViewFileLogger;->sLogWriter:Ljava/io/FileWriter;

    invoke-virtual {v12}, Ljava/io/FileWriter;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 166
    .end local v1    # "c":Ljava/util/Calendar;
    .end local v2    # "date":I
    .end local v4    # "hr":I
    .end local v5    # "mSec":I
    .end local v6    # "min":I
    .end local v7    # "month":I
    .end local v8    # "s":Ljava/lang/String;
    .end local v9    # "sb":Ljava/lang/StringBuffer;
    .end local v10    # "sec":I
    .end local v11    # "year":I
    :cond_8
    :goto_1
    monitor-exit v13

    return-void

    .line 126
    .restart local v1    # "c":Ljava/util/Calendar;
    .restart local v2    # "date":I
    .restart local v4    # "hr":I
    .restart local v5    # "mSec":I
    .restart local v6    # "min":I
    .restart local v7    # "month":I
    .restart local v9    # "sb":Ljava/lang/StringBuffer;
    .restart local v10    # "sec":I
    .restart local v11    # "year":I
    :cond_9
    const/16 v12, 0x64

    if-ge v5, v12, :cond_6

    .line 127
    const/16 v12, 0x30

    :try_start_2
    invoke-virtual {v9, v12}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 161
    .end local v1    # "c":Ljava/util/Calendar;
    .end local v2    # "date":I
    .end local v4    # "hr":I
    .end local v5    # "mSec":I
    .end local v6    # "min":I
    .end local v7    # "month":I
    .end local v9    # "sb":Ljava/lang/StringBuffer;
    .end local v10    # "sec":I
    .end local v11    # "year":I
    :catch_0
    move-exception v12

    goto :goto_1

    .line 152
    .restart local v1    # "c":Ljava/util/Calendar;
    .restart local v2    # "date":I
    .restart local v4    # "hr":I
    .restart local v5    # "mSec":I
    .restart local v6    # "min":I
    .restart local v7    # "month":I
    .restart local v8    # "s":Ljava/lang/String;
    .restart local v9    # "sb":Ljava/lang/StringBuffer;
    .restart local v10    # "sec":I
    .restart local v11    # "year":I
    :catch_1
    move-exception v3

    .line 154
    .local v3, "e":Ljava/io/IOException;
    const-string v12, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 157
    new-instance v12, Lcom/android/emailcommon/utility/ViewFileLogger;

    invoke-direct {v12}, Lcom/android/emailcommon/utility/ViewFileLogger;-><init>()V

    sput-object v12, Lcom/android/emailcommon/utility/ViewFileLogger;->LOGGER:Lcom/android/emailcommon/utility/ViewFileLogger;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 64
    .end local v1    # "c":Ljava/util/Calendar;
    .end local v2    # "date":I
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "hr":I
    .end local v5    # "mSec":I
    .end local v6    # "min":I
    .end local v7    # "month":I
    .end local v8    # "s":Ljava/lang/String;
    .end local v9    # "sb":Ljava/lang/StringBuffer;
    .end local v10    # "sec":I
    .end local v11    # "year":I
    :catchall_0
    move-exception v12

    monitor-exit v13

    throw v12
.end method
