.class public final enum Lcom/android/emailcommon/utility/Utility$VerifyStatus;
.super Ljava/lang/Enum;
.source "Utility.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/emailcommon/utility/Utility;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VerifyStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/emailcommon/utility/Utility$VerifyStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/emailcommon/utility/Utility$VerifyStatus;

.field public static final enum Executed:Lcom/android/emailcommon/utility/Utility$VerifyStatus;

.field public static final enum Initialized:Lcom/android/emailcommon/utility/Utility$VerifyStatus;

.field public static final enum OnCreate:Lcom/android/emailcommon/utility/Utility$VerifyStatus;

.field public static final enum OnResume:Lcom/android/emailcommon/utility/Utility$VerifyStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4050
    new-instance v0, Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    const-string v1, "Initialized"

    invoke-direct {v0, v1, v2}, Lcom/android/emailcommon/utility/Utility$VerifyStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/emailcommon/utility/Utility$VerifyStatus;->Initialized:Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    new-instance v0, Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    const-string v1, "OnCreate"

    invoke-direct {v0, v1, v3}, Lcom/android/emailcommon/utility/Utility$VerifyStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/emailcommon/utility/Utility$VerifyStatus;->OnCreate:Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    new-instance v0, Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    const-string v1, "OnResume"

    invoke-direct {v0, v1, v4}, Lcom/android/emailcommon/utility/Utility$VerifyStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/emailcommon/utility/Utility$VerifyStatus;->OnResume:Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    new-instance v0, Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    const-string v1, "Executed"

    invoke-direct {v0, v1, v5}, Lcom/android/emailcommon/utility/Utility$VerifyStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/emailcommon/utility/Utility$VerifyStatus;->Executed:Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    .line 4049
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    sget-object v1, Lcom/android/emailcommon/utility/Utility$VerifyStatus;->Initialized:Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/emailcommon/utility/Utility$VerifyStatus;->OnCreate:Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/emailcommon/utility/Utility$VerifyStatus;->OnResume:Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/emailcommon/utility/Utility$VerifyStatus;->Executed:Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/emailcommon/utility/Utility$VerifyStatus;->$VALUES:[Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 4049
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/emailcommon/utility/Utility$VerifyStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 4049
    const-class v0, Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    return-object v0
.end method

.method public static values()[Lcom/android/emailcommon/utility/Utility$VerifyStatus;
    .locals 1

    .prologue
    .line 4049
    sget-object v0, Lcom/android/emailcommon/utility/Utility$VerifyStatus;->$VALUES:[Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    invoke-virtual {v0}, [Lcom/android/emailcommon/utility/Utility$VerifyStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/emailcommon/utility/Utility$VerifyStatus;

    return-object v0
.end method
