.class public Lcom/android/emailcommon/utility/SyncScheduleData;
.super Ljava/lang/Object;
.source "SyncScheduleData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/android/emailcommon/utility/SyncScheduleData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mEndMinute:I

.field private mIsPeakScheduleOn:Z

.field private mOffPeakSchedule:I

.field private mPeakDays:I

.field private mPeakSchedule:I

.field private mRoamingSchedule:I

.field private mStartMinute:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 134
    new-instance v0, Lcom/android/emailcommon/utility/SyncScheduleData$1;

    invoke-direct {v0}, Lcom/android/emailcommon/utility/SyncScheduleData$1;-><init>()V

    sput-object v0, Lcom/android/emailcommon/utility/SyncScheduleData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIIIII)V
    .locals 1
    .param p1, "peakStartMinute"    # I
    .param p2, "peakEndMinute"    # I
    .param p3, "peakDays"    # I
    .param p4, "peakSchedule"    # I
    .param p5, "offPeakSchedule"    # I
    .param p6, "roamingSchedule"    # I

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput p1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mStartMinute:I

    .line 35
    iput p2, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mEndMinute:I

    .line 36
    iput p3, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mPeakDays:I

    .line 37
    iput p4, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mPeakSchedule:I

    .line 38
    iput p5, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mOffPeakSchedule:I

    .line 39
    iput p6, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mRoamingSchedule:I

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mIsPeakScheduleOn:Z

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x1

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mStartMinute:I

    .line 126
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mEndMinute:I

    .line 127
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mPeakDays:I

    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mPeakSchedule:I

    .line 129
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mOffPeakSchedule:I

    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mRoamingSchedule:I

    .line 131
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mIsPeakScheduleOn:Z

    .line 132
    return-void

    .line 131
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public getEndMinute()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mEndMinute:I

    return v0
.end method

.method public getIsPeakScheduleOn()Z
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mIsPeakScheduleOn:Z

    return v0
.end method

.method public getOffPeakSchedule()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mOffPeakSchedule:I

    return v0
.end method

.method public getPeakDay()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mPeakDays:I

    return v0
.end method

.method public getPeakSchedule()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mPeakSchedule:I

    return v0
.end method

.method public getRoamingSchedule()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mRoamingSchedule:I

    return v0
.end method

.method public getStartMinute()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mStartMinute:I

    return v0
.end method

.method public setEndMinute(I)V
    .locals 0
    .param p1, "endMinute"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mEndMinute:I

    .line 68
    return-void
.end method

.method public setIsPeakScheduleOn(Z)V
    .locals 0
    .param p1, "isPeakScheduleOn"    # Z

    .prologue
    .line 107
    iput-boolean p1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mIsPeakScheduleOn:Z

    .line 108
    return-void
.end method

.method public setOffPeakSchedule(I)V
    .locals 0
    .param p1, "offPeakSchedule"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mOffPeakSchedule:I

    .line 92
    return-void
.end method

.method public setPeakDay(I)V
    .locals 0
    .param p1, "peakDay"    # I

    .prologue
    .line 75
    iput p1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mPeakDays:I

    .line 76
    return-void
.end method

.method public setPeakSchedule(I)V
    .locals 0
    .param p1, "peakSchedule"    # I

    .prologue
    .line 83
    iput p1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mPeakSchedule:I

    .line 84
    return-void
.end method

.method public setRoamingSchedule(I)V
    .locals 0
    .param p1, "roamingSchedule"    # I

    .prologue
    .line 99
    iput p1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mRoamingSchedule:I

    .line 100
    return-void
.end method

.method public setStartMinute(I)V
    .locals 0
    .param p1, "startMinute"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mStartMinute:I

    .line 60
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\n  mStartMinute="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mStartMinute:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mEndMinute="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mEndMinute:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mPeakDays="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mPeakDays:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mPeakSchedule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mPeakSchedule:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mOffPeakSchedule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mOffPeakSchedule:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mRoamingSchedule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mRoamingSchedule:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mIsPeakScheduleOn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mIsPeakScheduleOn:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 115
    iget v0, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mStartMinute:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 116
    iget v0, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mEndMinute:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 117
    iget v0, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mPeakDays:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 118
    iget v0, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mPeakSchedule:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 119
    iget v0, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mOffPeakSchedule:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 120
    iget v0, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mRoamingSchedule:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 121
    iget-boolean v0, p0, Lcom/android/emailcommon/utility/SyncScheduleData;->mIsPeakScheduleOn:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 122
    return-void

    .line 121
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
