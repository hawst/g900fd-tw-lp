.class public Lcom/android/emailcommon/utility/calendar/RecurrenceUtilities;
.super Ljava/lang/Object;
.source "RecurrenceUtilities.java"


# instance fields
.field private final MINIMUM_EXPANSION_SPAN:J

.field private instance:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/utility/calendar/RecurrenceInstance;",
            ">;"
        }
    .end annotation
.end field

.field lastEnd:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/emailcommon/utility/calendar/RecurrenceUtilities;->instance:Ljava/util/ArrayList;

    .line 15
    const-wide v0, 0x13f4a4800L

    iput-wide v0, p0, Lcom/android/emailcommon/utility/calendar/RecurrenceUtilities;->MINIMUM_EXPANSION_SPAN:J

    .line 16
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/emailcommon/utility/calendar/RecurrenceUtilities;->lastEnd:J

    return-void
.end method
