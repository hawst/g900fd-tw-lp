.class public Lcom/android/emailcommon/utility/SecFeatureWrapper;
.super Ljava/lang/Object;
.source "SecFeatureWrapper.java"


# static fields
.field private static BUILD_TYPE:I

.field public static CARRIER_CODE:I

.field public static CARRIER_CODE_STRING:Ljava/lang/String;

.field public static COUNTRY_CODE_STRING:Ljava/lang/String;

.field static operator_features:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static operator_features_string:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static product_model:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    const/4 v0, -0x1

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    .line 40
    sput-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    .line 41
    sput-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->COUNTRY_CODE_STRING:Ljava/lang/String;

    .line 43
    sput-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->product_model:Ljava/lang/String;

    .line 45
    const/4 v0, 0x0

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->BUILD_TYPE:I

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->operator_features:Ljava/util/HashMap;

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->operator_features_string:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCarrierId()I
    .locals 2

    .prologue
    .line 132
    sget v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 133
    const-string v0, "ro.csc.sales_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    .line 134
    const/4 v0, 0x0

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    .line 136
    const-string v0, "VZW"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    const/4 v0, 0x1

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    .line 186
    :cond_0
    :goto_0
    sget v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    return v0

    .line 138
    :cond_1
    const-string v0, "TMB"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 139
    const/4 v0, 0x2

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto :goto_0

    .line 140
    :cond_2
    const-string v0, "ATT"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 141
    const/4 v0, 0x3

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto :goto_0

    .line 142
    :cond_3
    const-string v0, "SPR"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 143
    const/4 v0, 0x4

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto :goto_0

    .line 144
    :cond_4
    const-string v0, "SKT"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 145
    const/4 v0, 0x5

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto :goto_0

    .line 146
    :cond_5
    const-string v0, "KT"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 147
    const/4 v0, 0x6

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto :goto_0

    .line 148
    :cond_6
    const-string v0, "LGT"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 149
    const/4 v0, 0x7

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto :goto_0

    .line 150
    :cond_7
    const-string v0, "DCM"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 151
    const/16 v0, 0x8

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto :goto_0

    .line 152
    :cond_8
    const-string v0, "BRI"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 153
    const/16 v0, 0x9

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto :goto_0

    .line 154
    :cond_9
    const-string v0, "CHM"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 155
    const/16 v0, 0xa

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto/16 :goto_0

    .line 156
    :cond_a
    const-string v0, "CHN"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 157
    const/16 v0, 0xb

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto/16 :goto_0

    .line 158
    :cond_b
    const-string v0, "CTC"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 159
    const/16 v0, 0xc

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto/16 :goto_0

    .line 160
    :cond_c
    const-string v0, "TGY"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 161
    const/16 v0, 0xd

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto/16 :goto_0

    .line 162
    :cond_d
    const-string v0, "USC"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 163
    const/16 v0, 0xe

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto/16 :goto_0

    .line 164
    :cond_e
    const-string v0, "ACG"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 165
    const/16 v0, 0x16

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto/16 :goto_0

    .line 166
    :cond_f
    const-string v0, "VMU"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 167
    const/16 v0, 0xf

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto/16 :goto_0

    .line 168
    :cond_10
    const-string v0, "H3G"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 169
    const/16 v0, 0x10

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto/16 :goto_0

    .line 170
    :cond_11
    const-string v0, "3IE"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 171
    const/16 v0, 0x11

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto/16 :goto_0

    .line 172
    :cond_12
    const-string v0, "DRE"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 173
    const/16 v0, 0x12

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto/16 :goto_0

    .line 174
    :cond_13
    const-string v0, "HUI"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 175
    const/16 v0, 0x13

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto/16 :goto_0

    .line 176
    :cond_14
    const-string v0, "BST"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 177
    const/16 v0, 0x14

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto/16 :goto_0

    .line 178
    :cond_15
    const-string v0, "XAS"

    sget-object v1, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    const/16 v0, 0x15

    sput v0, Lcom/android/emailcommon/utility/SecFeatureWrapper;->CARRIER_CODE:I

    goto/16 :goto_0
.end method
