.class public Lcom/android/emailcommon/utility/Utility;
.super Ljava/lang/Object;
.source "Utility.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/utility/Utility$9;,
        Lcom/android/emailcommon/utility/Utility$VerifLogModule;,
        Lcom/android/emailcommon/utility/Utility$VerifyStatus;,
        Lcom/android/emailcommon/utility/Utility$CloseTraceCursorWrapper;,
        Lcom/android/emailcommon/utility/Utility$ForEachAccount;,
        Lcom/android/emailcommon/utility/Utility$ListStateSaver;,
        Lcom/android/emailcommon/utility/Utility$CursorGetter;,
        Lcom/android/emailcommon/utility/Utility$NewFileCreator;
    }
.end annotation


# static fields
.field public static final ASCII:Ljava/nio/charset/Charset;

.field private static final ATTACHMENT_META_NAME_PROJECTION:[Ljava/lang/String;

.field private static final BLOB_GETTER:Lcom/android/emailcommon/utility/Utility$CursorGetter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/emailcommon/utility/Utility$CursorGetter",
            "<[B>;"
        }
    .end annotation
.end field

.field public static final CSC_SALES_CODE:Ljava/lang/String;

.field private static final DATE_CLEANUP_PATTERN_WRONG_TIMEZONE:Ljava/util/regex/Pattern;

.field public static final DOMAIN_NAME:Ljava/util/regex/Pattern;

.field private static final EMAILADDRESS_ACCOUNTID_PROJECTION:[Ljava/lang/String;

.field public static final EMAIL_ADDRESS:Ljava/util/regex/Pattern;

.field public static final EMPTY_LONGS:[Ljava/lang/Long;

.field public static final EMPTY_STRINGS:[Ljava/lang/String;

.field public static FILTER_PROJECTION:[Ljava/lang/String;

.field private static final INT_GETTER:Lcom/android/emailcommon/utility/Utility$CursorGetter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/emailcommon/utility/Utility$CursorGetter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final IP_ADDRESS:Ljava/util/regex/Pattern;

.field public static final IS_BUILD_TYPE_ENG:Z

.field public static final IS_CARRIER_AIO:Z

.field public static final IS_CARRIER_ATT:Z

.field public static final IS_CARRIER_BELL:Z

.field public static final IS_CARRIER_BRI:Z

.field public static final IS_CARRIER_BST:Z

.field public static final IS_CARRIER_CANADA:Z

.field public static final IS_CARRIER_CANADA_OTHER:Z

.field public static final IS_CARRIER_CHC:Z

.field public static final IS_CARRIER_CHINA:Z

.field public static final IS_CARRIER_CHM:Z

.field public static final IS_CARRIER_CHN:Z

.field public static final IS_CARRIER_CHU:Z

.field public static final IS_CARRIER_CTC:Z

.field public static final IS_CARRIER_DCM:Z

.field public static final IS_CARRIER_FMC:Z

.field public static final IS_CARRIER_LIVEDEMOUNIT:Z

.field public static final IS_CARRIER_MTA:Z

.field public static final IS_CARRIER_MTR:Z

.field public static final IS_CARRIER_NA:Z

.field public static final IS_CARRIER_QHN:Z

.field public static final IS_CARRIER_ROGERS:Z

.field public static final IS_CARRIER_RWC:Z

.field public static final IS_CARRIER_SPR:Z

.field public static final IS_CARRIER_SPRPRE:Z

.field public static final IS_CARRIER_TELLUS:Z

.field public static final IS_CARRIER_TGY:Z

.field public static final IS_CARRIER_TMB:Z

.field public static final IS_CARRIER_VMU:Z

.field public static final IS_CARRIER_VZW:Z

.field public static final IS_CARRIER_WHOLE_SPRINT:Z

.field public static final IS_CARRIER_XAC:Z

.field public static final IS_CARRIER_XAR:Z

.field public static final IS_CARRIER_XAS:Z

.field public static final IS_CARRIER_ZZH:Z

.field public static final IS_CHARACTERISTICS_MTR:Z

.field public static final IS_CHARACTERISTICS_TMO:Z

.field public static final IS_DEVICE_AEGIS2:Z

.field public static final IS_DEVICE_AFYON:Z

.field public static final IS_DEVICE_ATTD2:Z

.field public static final IS_DEVICE_ATTHLTE:Z

.field public static final IS_DEVICE_ATTJ:Z

.field public static final IS_DEVICE_ATTJ_ACTIVE:Z

.field public static final IS_DEVICE_ATTJ_MINI:Z

.field public static final IS_DEVICE_ATTT0:Z

.field public static final IS_DEVICE_CANE:Z

.field public static final IS_DEVICE_CHAGALL:Z

.field public static final IS_DEVICE_CRATER:Z

.field public static final IS_DEVICE_DEGAS:Z

.field public static final IS_DEVICE_ESPRESSO:Z

.field public static final IS_DEVICE_FLTE:Z

.field public static final IS_DEVICE_GOLDEN:Z

.field public static final IS_DEVICE_H:Z

.field public static final IS_DEVICE_H3GDUOSCTC:Z

.field public static final IS_DEVICE_JAORJF:Z

.field public static final IS_DEVICE_K:Z

.field public static final IS_DEVICE_K3G:Z

.field public static final IS_DEVICE_KLIMT:Z

.field public static final IS_DEVICE_KLTE:Z

.field public static final IS_DEVICE_KMINI:Z

.field public static final IS_DEVICE_KONA:Z

.field public static final IS_DEVICE_LEVITDOPEN:Z

.field public static final IS_DEVICE_LT01:Z

.field public static final IS_DEVICE_LT03:Z

.field public static final IS_DEVICE_M2:Z

.field public static final IS_DEVICE_MATISSE:Z

.field public static final IS_DEVICE_MATISSELTE_ATT:Z

.field public static final IS_DEVICE_MEGA2:Z

.field public static final IS_DEVICE_MELIUS:Z

.field public static final IS_DEVICE_MILLET:Z

.field public static final IS_DEVICE_MILLETLTE_ATT:Z

.field public static final IS_DEVICE_MS01:Z

.field public static final IS_DEVICE_P4NOTE:Z

.field public static final IS_DEVICE_SKOMER:Z

.field public static final IS_DEVICE_SLTE:Z

.field public static final IS_DEVICE_T:Z

.field public static final IS_DEVICE_T0:Z

.field public static final IS_DEVICE_T0_VZW:Z

.field public static final IS_DEVICE_TBLTE:Z

.field public static final IS_DEVICE_TR:Z

.field public static final IS_DEVICE_TRE3G:Z

.field public static final IS_DEVICE_TRELTE:Z

.field public static final IS_DEVICE_TRHLTE:Z

.field public static final IS_DEVICE_TRHP0LTE:Z

.field public static final IS_DEVICE_TRLTE:Z

.field public static final IS_DEVICE_V:Z

.field public static final IS_DEVICE_VASTA:Z

.field public static final IS_DEVICE_VASTADUOSCTC:Z

.field public static final IS_DEVICE_VASTATDOPEN:Z

.field public static final IS_DEVICE_VLTE_ATT:Z

.field public static final IS_DEVICE_VZWGS3_MINI:Z

.field public static final IS_DEVICE_VZWJ:Z

.field public static final IS_DEVICE_VZWJ_MINI:Z

.field private static final LONG_GETTER:Lcom/android/emailcommon/utility/Utility$CursorGetter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/emailcommon/utility/Utility$CursorGetter",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static final MYSINGLE_CONTENT_URI:Landroid/net/Uri;

.field public static final PRODUCT_NAME:Ljava/lang/String;

.field public static final SEND_OUTBOX_DURATION_MINUTES:[I

.field private static final STRING_GETTER:Lcom/android/emailcommon/utility/Utility$CursorGetter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/emailcommon/utility/Utility$CursorGetter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final UTF_8:Ljava/nio/charset/Charset;

.field public static VIP_PROJECTION:[Ljava/lang/String;

.field protected static commitTime:J

.field private static digits:[C

.field private static mIsSdpEnabled:Z

.field private static mLastClickTime:J

.field public static mStartWithIntent:Landroid/content/Intent;

.field protected static parseTime:J

.field private static sVerifySet:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/emailcommon/utility/Utility$VerifLogModule;",
            ">;"
        }
    .end annotation
.end field

.field protected static sendHttpTime:J

.field protected static syncTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 154
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->UTF_8:Ljava/nio/charset/Charset;

    .line 155
    const-string v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->ASCII:Ljava/nio/charset/Charset;

    .line 157
    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->EMPTY_STRINGS:[Ljava/lang/String;

    .line 158
    new-array v0, v1, [Ljava/lang/Long;

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->EMPTY_LONGS:[Ljava/lang/Long;

    .line 161
    const-string v0, "GMT([-+]\\d{4})$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->DATE_CLEANUP_PATTERN_WRONG_TIMEZONE:Ljava/util/regex/Pattern;

    .line 164
    const-string v0, "ro.csc.sales_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    .line 165
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    .line 167
    const-string v0, "VZW"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_VZW:Z

    .line 168
    const-string v0, "ATT"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_ATT:Z

    .line 169
    const-string v0, "TMB"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_TMB:Z

    .line 170
    const-string v0, "RWC"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_RWC:Z

    .line 171
    const-string v0, "FMC"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_FMC:Z

    .line 172
    const-string v0, "MTA"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_MTA:Z

    .line 173
    const-string v0, "MTR"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_MTR:Z

    .line 174
    const-string v0, "SPR"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_SPR:Z

    .line 175
    const-string v0, "BST"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_BST:Z

    .line 176
    const-string v0, "VMU"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_VMU:Z

    .line 177
    const-string v0, "XAS"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_XAS:Z

    .line 178
    const-string v0, "SPRPRE"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_SPRPRE:Z

    .line 179
    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_SPR:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_BST:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_VMU:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_XAS:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_SPRPRE:Z

    if-eqz v0, :cond_13

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_WHOLE_SPRINT:Z

    .line 180
    const-string v0, "DCM"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_DCM:Z

    .line 183
    const-string v0, "CHN"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CHN:Z

    .line 184
    const-string v0, "CHM"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CHM:Z

    .line 185
    const-string v0, "CHU"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CHU:Z

    .line 186
    const-string v0, "QHN"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_QHN:Z

    .line 187
    const-string v0, "CHC"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CHC:Z

    .line 188
    const-string v0, "CTC"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CTC:Z

    .line 189
    const-string v0, "TGY"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_TGY:Z

    .line 190
    const-string v0, "BRI"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_BRI:Z

    .line 191
    const-string v0, "ZZH"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_ZZH:Z

    .line 192
    const-string v0, "XAR"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_XAR:Z

    .line 193
    const-string v0, "XAC"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_XAC:Z

    .line 194
    const-string v0, "BMC"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "BWA"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "PCM"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "SOL"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "VMC"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_1
    move v0, v2

    :goto_1
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_BELL:Z

    .line 198
    const-string v0, "RWC"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "FMC"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "MTA"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "CHR"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    :cond_2
    move v0, v2

    :goto_2
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_ROGERS:Z

    .line 202
    const-string v0, "TLS"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "KDO"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "SPC"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_3
    move v0, v2

    :goto_3
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_TELLUS:Z

    .line 205
    const-string v0, "GLW"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "ESK"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "MCT"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "VTR"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    :cond_4
    move v0, v2

    :goto_4
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CANADA_OTHER:Z

    .line 209
    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_BELL:Z

    if-nez v0, :cond_5

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_ROGERS:Z

    if-nez v0, :cond_5

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_TELLUS:Z

    if-nez v0, :cond_5

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CANADA_OTHER:Z

    if-eqz v0, :cond_18

    :cond_5
    move v0, v2

    :goto_5
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CANADA:Z

    .line 212
    const-string v0, "AIO"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_AIO:Z

    .line 214
    const-string v0, "PAP"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "FOP"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->CSC_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    :cond_6
    move v0, v2

    :goto_6
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_LIVEDEMOUNIT:Z

    .line 216
    const-string v0, "jfltevzw"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_VZWJ:Z

    .line 217
    const-string v0, "serranoltevzw"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_VZWJ_MINI:Z

    .line 218
    const-string v0, "goldenltevzw"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_VZWGS3_MINI:Z

    .line 219
    const-string v0, "jflteatt"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_ATTJ:Z

    .line 220
    const-string v0, "serranolteatt"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_ATTJ_MINI:Z

    .line 221
    const-string v0, "t0lteatt"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_ATTT0:Z

    .line 222
    const-string v0, "d2att"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_ATTD2:Z

    .line 223
    const-string v0, "jactivelteatt"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_ATTJ_ACTIVE:Z

    .line 224
    const-string v0, "hlteatt"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_ATTHLTE:Z

    .line 225
    const-string v0, "aegis2vzw"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_AEGIS2:Z

    .line 226
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "t0"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_T0:Z

    .line 227
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "golden"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_GOLDEN:Z

    .line 228
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "kona"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_KONA:Z

    .line 229
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "p4note"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_P4NOTE:Z

    .line 230
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "ja"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "jf"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "SC-04E"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    :cond_7
    move v0, v2

    :goto_7
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_JAORJF:Z

    .line 232
    const-string v0, "ro.build.type"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "eng"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_BUILD_TYPE_ENG:Z

    .line 234
    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_ATT:Z

    if-nez v0, :cond_8

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_AIO:Z

    if-nez v0, :cond_8

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_VZW:Z

    if-nez v0, :cond_8

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_TMB:Z

    if-nez v0, :cond_8

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_SPR:Z

    if-eqz v0, :cond_1b

    :cond_8
    move v0, v2

    :goto_8
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_NA:Z

    .line 236
    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CHN:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CHM:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CHU:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_QHN:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CHC:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CTC:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_TGY:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_BRI:Z

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_ZZH:Z

    if-eqz v0, :cond_1c

    :cond_9
    move v0, v2

    :goto_9
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_CHINA:Z

    .line 238
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "melius"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_MELIUS:Z

    .line 239
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "mega2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "vasta"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    :cond_a
    move v0, v2

    :goto_a
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_MEGA2:Z

    .line 240
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "vasta"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_VASTA:Z

    .line 241
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "crater"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_CRATER:Z

    .line 242
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "flte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_FLTE:Z

    .line 243
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "hlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "ha3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "h3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "hl3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "hllte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "ms01"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "ASH"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "SC-01F"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "Madrid"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "SCL22"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "JS01"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "SC-02F"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_FLTE:Z

    if-nez v0, :cond_b

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "fresco"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    :cond_b
    move v0, v2

    :goto_b
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_H:Z

    .line 258
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "h3gduosctc"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_H3GDUOSCTC:Z

    .line 259
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "lt01"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_LT01:Z

    .line 260
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "lt03"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_LT03:Z

    .line 261
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "vienna"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "v1a"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    :cond_c
    move v0, v2

    :goto_c
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_V:Z

    .line 264
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "viennalteatt"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_VLTE_ATT:Z

    .line 265
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "cane"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_CANE:Z

    .line 266
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "ms01"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_MS01:Z

    .line 267
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "klte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    const-string v0, "SC-04F"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    const-string v0, "SCL23"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "kqlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_d

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "lentislte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    :cond_d
    move v0, v2

    :goto_d
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_KLTE:Z

    .line 272
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "k3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_K3G:Z

    .line 273
    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_KLTE:Z

    if-nez v0, :cond_e

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_K3G:Z

    if-eqz v0, :cond_21

    :cond_e
    move v0, v2

    :goto_e
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_K:Z

    .line 275
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "m2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_M2:Z

    .line 277
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "kmini"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_KMINI:Z

    .line 279
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "tblte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    const-string v0, "SC-01G"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    const-string v0, "SCL24"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "tbelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    :cond_f
    move v0, v2

    :goto_f
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_TBLTE:Z

    .line 284
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "trlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "SC-01G"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "SCL24"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    :cond_10
    move v0, v2

    :goto_10
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_TRLTE:Z

    .line 287
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "trhlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_TRHLTE:Z

    .line 288
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "trhp0lte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_TRHP0LTE:Z

    .line 289
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "trelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_TRELTE:Z

    .line 290
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "tre3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_TRE3G:Z

    .line 291
    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_TRLTE:Z

    if-nez v0, :cond_11

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_TRHLTE:Z

    if-nez v0, :cond_11

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_TRHP0LTE:Z

    if-nez v0, :cond_11

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_TRELTE:Z

    if-nez v0, :cond_11

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_TRE3G:Z

    if-eqz v0, :cond_24

    :cond_11
    move v0, v2

    :goto_11
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_TR:Z

    .line 295
    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_T0:Z

    if-eqz v0, :cond_25

    sget-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CARRIER_VZW:Z

    if-eqz v0, :cond_25

    move v0, v2

    :goto_12
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_T0_VZW:Z

    .line 297
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "espresso"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_ESPRESSO:Z

    .line 299
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "skomer"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_SKOMER:Z

    .line 301
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "degas"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_DEGAS:Z

    .line 303
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "millet"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_MILLET:Z

    .line 304
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "milletlteatt"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_MILLETLTE_ATT:Z

    .line 306
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "matisse"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_MATISSE:Z

    .line 307
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "matisselteatt"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_MATISSELTE_ATT:Z

    .line 309
    const-string v0, "mtr"

    const-string v3, "ro.build.characteristics"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CHARACTERISTICS_MTR:Z

    .line 311
    const-string v0, "tmo"

    const-string v3, "ro.build.characteristics"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_CHARACTERISTICS_TMO:Z

    .line 313
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "chagall"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_CHAGALL:Z

    .line 314
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "klimt"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_KLIMT:Z

    .line 316
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "vastaltetdzc"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_VASTATDOPEN:Z

    .line 318
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "vastalteduosctc"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_VASTADUOSCTC:Z

    .line 320
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "leviltetdzc"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_LEVITDOPEN:Z

    .line 322
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "afyon"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_AFYON:Z

    .line 324
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "slte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_SLTE:Z

    .line 328
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "tblte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "tb3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "tbelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "trlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "tre3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "trhlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "trhp0lte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "tbhplte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "trhplte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "trelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    sget-object v0, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "trwifi"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    const-string v0, "SC-01G"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    const-string v0, "SCL24"

    sget-object v3, Lcom/android/emailcommon/utility/Utility;->PRODUCT_NAME:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    :cond_12
    move v0, v2

    :goto_13
    sput-boolean v0, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_T:Z

    .line 344
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->SEND_OUTBOX_DURATION_MINUTES:[I

    .line 387
    const-string v0, "content://com.sds.mobiledesk.provider.mobiledeskextends/knoxContent"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->MYSINGLE_CONTENT_URI:Landroid/net/Uri;

    .line 399
    sput-boolean v1, Lcom/android/emailcommon/utility/Utility;->mIsSdpEnabled:Z

    .line 410
    const/4 v0, 0x0

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->mStartWithIntent:Landroid/content/Intent;

    .line 412
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v0, v1

    const-string v3, "Contact_Id"

    aput-object v3, v0, v2

    const-string v3, "Email_Id"

    aput-object v3, v0, v5

    const-string v3, "EmailAddress"

    aput-object v3, v0, v6

    const-string v3, "DisplayName"

    aput-object v3, v0, v7

    const/4 v3, 0x5

    const-string v4, "Sender_Order"

    aput-object v4, v0, v3

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->VIP_PROJECTION:[Ljava/lang/String;

    .line 418
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v0, v1

    const-string v3, "Contact_Id"

    aput-object v3, v0, v2

    const-string v3, "Email_Id"

    aput-object v3, v0, v5

    const-string v3, "EmailAddress"

    aput-object v3, v0, v6

    const-string v3, "DisplayName"

    aput-object v3, v0, v7

    const/4 v3, 0x5

    const-string v4, "SubjectName"

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const-string v4, "FolderName"

    aput-object v4, v0, v3

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->FILTER_PROJECTION:[Ljava/lang/String;

    .line 782
    new-array v0, v7, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v0, v1

    const-string v3, "emailAddress"

    aput-object v3, v0, v2

    const-string v3, "displayName"

    aput-object v3, v0, v5

    const-string v3, "hostAuthKeyRecv"

    aput-object v3, v0, v6

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->EMAILADDRESS_ACCOUNTID_PROJECTION:[Ljava/lang/String;

    .line 1560
    new-instance v0, Lcom/android/emailcommon/utility/Utility$3;

    invoke-direct {v0}, Lcom/android/emailcommon/utility/Utility$3;-><init>()V

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->LONG_GETTER:Lcom/android/emailcommon/utility/Utility$CursorGetter;

    .line 1566
    new-instance v0, Lcom/android/emailcommon/utility/Utility$4;

    invoke-direct {v0}, Lcom/android/emailcommon/utility/Utility$4;-><init>()V

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->INT_GETTER:Lcom/android/emailcommon/utility/Utility$CursorGetter;

    .line 1572
    new-instance v0, Lcom/android/emailcommon/utility/Utility$5;

    invoke-direct {v0}, Lcom/android/emailcommon/utility/Utility$5;-><init>()V

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->STRING_GETTER:Lcom/android/emailcommon/utility/Utility$CursorGetter;

    .line 1578
    new-instance v0, Lcom/android/emailcommon/utility/Utility$6;

    invoke-direct {v0}, Lcom/android/emailcommon/utility/Utility$6;-><init>()V

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->BLOB_GETTER:Lcom/android/emailcommon/utility/Utility$CursorGetter;

    .line 2064
    new-array v0, v2, [Ljava/lang/String;

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->ATTACHMENT_META_NAME_PROJECTION:[Ljava/lang/String;

    .line 3682
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->digits:[C

    .line 3797
    const-string v0, "((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9]))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->IP_ADDRESS:Ljava/util/regex/Pattern;

    .line 3805
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "^([a-zA-Z0-9]([a-zA-Z0-9\\-]{0,320}[a-zA-Z0-9])?\\.)+[a-zA-Z]{2,320}$|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/utility/Utility;->IP_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->DOMAIN_NAME:Ljava/util/regex/Pattern;

    .line 3811
    const-string v0, "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,320}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,320}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,320})+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    .line 4077
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/android/emailcommon/utility/Utility;->sVerifySet:Landroid/util/SparseArray;

    .line 4842
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/android/emailcommon/utility/Utility;->mLastClickTime:J

    return-void

    :cond_13
    move v0, v1

    .line 179
    goto/16 :goto_0

    :cond_14
    move v0, v1

    .line 194
    goto/16 :goto_1

    :cond_15
    move v0, v1

    .line 198
    goto/16 :goto_2

    :cond_16
    move v0, v1

    .line 202
    goto/16 :goto_3

    :cond_17
    move v0, v1

    .line 205
    goto/16 :goto_4

    :cond_18
    move v0, v1

    .line 209
    goto/16 :goto_5

    :cond_19
    move v0, v1

    .line 214
    goto/16 :goto_6

    :cond_1a
    move v0, v1

    .line 230
    goto/16 :goto_7

    :cond_1b
    move v0, v1

    .line 234
    goto/16 :goto_8

    :cond_1c
    move v0, v1

    .line 236
    goto/16 :goto_9

    :cond_1d
    move v0, v1

    .line 239
    goto/16 :goto_a

    :cond_1e
    move v0, v1

    .line 243
    goto/16 :goto_b

    :cond_1f
    move v0, v1

    .line 261
    goto/16 :goto_c

    :cond_20
    move v0, v1

    .line 267
    goto/16 :goto_d

    :cond_21
    move v0, v1

    .line 273
    goto/16 :goto_e

    :cond_22
    move v0, v1

    .line 279
    goto/16 :goto_f

    :cond_23
    move v0, v1

    .line 284
    goto/16 :goto_10

    :cond_24
    move v0, v1

    .line 291
    goto/16 :goto_11

    :cond_25
    move v0, v1

    .line 295
    goto/16 :goto_12

    :cond_26
    move v0, v1

    .line 328
    goto/16 :goto_13

    .line 344
    :array_0
    .array-data 4
        0x0
        0x1
        0x3
        0x5
        0xa
        0x1e
        0x3c
        0xb4
        0x168
        0x2d0
        0x5a0
    .end array-data

    .line 3682
    :array_1
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4053
    return-void
.end method

.method public static areStringsEqual(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0, "s1"    # Ljava/lang/String;
    .param p1, "s2"    # Ljava/lang/String;

    .prologue
    .line 2251
    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    if-nez p0, :cond_2

    if-nez p1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final arrayContains([Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 3
    .param p0, "a"    # [Ljava/lang/Object;
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 481
    const/4 v1, 0x0

    .local v1, "i":I
    array-length v0, p0

    .local v0, "count":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 482
    aget-object v2, p0, v1

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 483
    const/4 v2, 0x1

    .line 486
    :goto_1
    return v2

    .line 481
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 486
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static attachmentExists(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Attachment;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "attachment"    # Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1747
    if-nez p1, :cond_1

    .line 1769
    :cond_0
    :goto_0
    return v4

    .line 1749
    :cond_1
    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentBytes:[B

    if-eqz v6, :cond_2

    move v4, v5

    .line 1750
    goto :goto_0

    .line 1751
    :cond_2
    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1755
    :try_start_0
    iget-object v6, p1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 1757
    .local v1, "fileUri":Landroid/net/Uri;
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 1759
    .local v2, "inStream":Ljava/io/InputStream;
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    move v4, v5

    .line 1763
    goto :goto_0

    .line 1764
    .end local v2    # "inStream":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 1765
    .local v0, "e":Ljava/io/FileNotFoundException;
    goto :goto_0

    .line 1767
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    .end local v1    # "fileUri":Landroid/net/Uri;
    :catch_1
    move-exception v3

    .line 1768
    .local v3, "re":Ljava/lang/RuntimeException;
    const-string v5, "Email"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "attachmentExists RuntimeException="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1760
    .end local v3    # "re":Ljava/lang/RuntimeException;
    .restart local v1    # "fileUri":Landroid/net/Uri;
    .restart local v2    # "inStream":Ljava/io/InputStream;
    :catch_2
    move-exception v4

    goto :goto_1
.end method

.method public static attachmentExistsAndHasRealData(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Attachment;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "attachment"    # Lcom/android/emailcommon/provider/EmailContent$Attachment;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1774
    if-nez p1, :cond_1

    .line 1806
    :cond_0
    :goto_0
    return v5

    .line 1776
    :cond_1
    iget-object v7, p1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentBytes:[B

    if-eqz v7, :cond_2

    move v5, v6

    .line 1777
    goto :goto_0

    .line 1778
    :cond_2
    iget-object v7, p1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1782
    :try_start_0
    iget-object v7, p1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v2

    .line 1784
    .local v2, "fileUri":Landroid/net/Uri;
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v3

    .line 1786
    .local v3, "inStream":Ljava/io/InputStream;
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->available()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    .line 1787
    .local v0, "bytes":I
    if-gtz v0, :cond_3

    .line 1795
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_0

    .line 1796
    :catch_0
    move-exception v6

    goto :goto_0

    .line 1795
    :cond_3
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_4

    :goto_1
    move v5, v6

    .line 1800
    goto :goto_0

    .line 1790
    .end local v0    # "bytes":I
    :catch_1
    move-exception v1

    .line 1791
    .local v1, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1795
    :try_start_6
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 1796
    :catch_2
    move-exception v6

    goto :goto_0

    .line 1794
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    .line 1795
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_4

    .line 1798
    :goto_2
    :try_start_8
    throw v6
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_4

    .line 1801
    .end local v3    # "inStream":Ljava/io/InputStream;
    :catch_3
    move-exception v1

    .line 1802
    .local v1, "e":Ljava/io/FileNotFoundException;
    goto :goto_0

    .line 1804
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .end local v2    # "fileUri":Landroid/net/Uri;
    :catch_4
    move-exception v4

    .line 1805
    .local v4, "re":Ljava/lang/RuntimeException;
    const-string v6, "Email"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "attachmentExists RuntimeException="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1796
    .end local v4    # "re":Ljava/lang/RuntimeException;
    .restart local v0    # "bytes":I
    .restart local v2    # "fileUri":Landroid/net/Uri;
    .restart local v3    # "inStream":Ljava/io/InputStream;
    :catch_5
    move-exception v5

    goto :goto_1

    .end local v0    # "bytes":I
    :catch_6
    move-exception v7

    goto :goto_2
.end method

.method static buildLimitOneUri(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3
    .param p0, "original"    # Landroid/net/Uri;

    .prologue
    .line 1591
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1592
    .local v0, "uriString":Ljava/lang/String;
    const-string v1, "content"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "com.android.email.provider"

    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "/-1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1595
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/android/emailcommon/provider/EmailContent;->uriWithLimit(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object p0

    .line 1597
    .end local p0    # "original":Landroid/net/Uri;
    :cond_0
    return-object p0
.end method

.method public static byteArrayToInt([BI)I
    .locals 4
    .param p0, "b"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 2456
    const/4 v2, 0x0

    .line 2457
    .local v2, "value":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v3, 0x4

    if-ge v0, v3, :cond_0

    .line 2458
    mul-int/lit8 v1, v0, 0x8

    .line 2459
    .local v1, "shift":I
    add-int v3, v0, p1

    aget-byte v3, p0, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/2addr v3, v1

    add-int/2addr v2, v3

    .line 2457
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2461
    .end local v1    # "shift":I
    :cond_0
    return v2
.end method

.method public static byteToHex(I)Ljava/lang/String;
    .locals 1
    .param p0, "b"    # I

    .prologue
    .line 1269
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, p0}, Lcom/android/emailcommon/utility/Utility;->byteToHex(Ljava/lang/StringBuilder;I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static byteToHex(Ljava/lang/StringBuilder;I)Ljava/lang/StringBuilder;
    .locals 2
    .param p0, "sb"    # Ljava/lang/StringBuilder;
    .param p1, "b"    # I

    .prologue
    .line 1273
    and-int/lit16 p1, p1, 0xff

    .line 1274
    const-string v0, "0123456789ABCDEF"

    shr-int/lit8 v1, p1, 0x4

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1275
    const-string v0, "0123456789ABCDEF"

    and-int/lit8 v1, p1, 0xf

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1276
    return-object p0
.end method

.method public static cleanUpMimeDate(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "date"    # Ljava/lang/String;

    .prologue
    .line 1368
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1372
    :goto_0
    return-object p0

    .line 1371
    :cond_0
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->DATE_CLEANUP_PATTERN_WRONG_TIMEZONE:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, "$1"

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1372
    goto :goto_0
.end method

.method public static debugTime(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "tag2"    # Ljava/lang/String;
    .param p2, "log"    # Ljava/lang/String;

    .prologue
    const-wide v8, 0x408f400000000000L    # 1000.0

    .line 4851
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 4853
    .local v0, "curTime":J
    const-string v6, "DEBUG_MESSAGE_EXCHANGE_SYNCTIME"

    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 4854
    sget-wide v6, Lcom/android/emailcommon/utility/Utility;->syncTime:J

    sub-long v2, v0, v6

    .line 4855
    .local v2, "debugTime":J
    sget-wide v6, Lcom/android/emailcommon/utility/Utility;->syncTime:J

    sub-long v6, v0, v6

    long-to-double v6, v6

    div-double v4, v6, v8

    .line 4856
    .local v4, "totalTime":D
    sput-wide v0, Lcom/android/emailcommon/utility/Utility;->syncTime:J

    .line 4857
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " => debugTime("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "), totalTime("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4874
    .end local v2    # "debugTime":J
    .end local v4    # "totalTime":D
    :cond_0
    :goto_0
    return-void

    .line 4858
    :cond_1
    const-string v6, "DEBUG_MESSAGE_EXCHANGE_SEND_HTTP"

    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 4859
    sget-wide v6, Lcom/android/emailcommon/utility/Utility;->sendHttpTime:J

    sub-long v2, v0, v6

    .line 4860
    .restart local v2    # "debugTime":J
    sget-wide v6, Lcom/android/emailcommon/utility/Utility;->sendHttpTime:J

    sub-long v6, v0, v6

    long-to-double v6, v6

    div-double v4, v6, v8

    .line 4861
    .restart local v4    # "totalTime":D
    sput-wide v0, Lcom/android/emailcommon/utility/Utility;->sendHttpTime:J

    .line 4862
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " => debugTime("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "), totalTime("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4863
    .end local v2    # "debugTime":J
    .end local v4    # "totalTime":D
    :cond_2
    const-string v6, "DEBUG_MESSAGE_EXCHANGE_COMMIT"

    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 4864
    sget-wide v6, Lcom/android/emailcommon/utility/Utility;->commitTime:J

    sub-long v2, v0, v6

    .line 4865
    .restart local v2    # "debugTime":J
    sget-wide v6, Lcom/android/emailcommon/utility/Utility;->commitTime:J

    sub-long v6, v0, v6

    long-to-double v6, v6

    div-double v4, v6, v8

    .line 4866
    .restart local v4    # "totalTime":D
    sput-wide v0, Lcom/android/emailcommon/utility/Utility;->commitTime:J

    .line 4867
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " => debugTime("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "), totalTime("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4868
    .end local v2    # "debugTime":J
    .end local v4    # "totalTime":D
    :cond_3
    const-string v6, "DEBUG_MESSAGE_EXCHANGE_PARSE"

    invoke-virtual {p1, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 4869
    sget-wide v6, Lcom/android/emailcommon/utility/Utility;->parseTime:J

    sub-long v2, v0, v6

    .line 4870
    .restart local v2    # "debugTime":J
    sget-wide v6, Lcom/android/emailcommon/utility/Utility;->parseTime:J

    sub-long v6, v0, v6

    long-to-double v6, v6

    div-double v4, v6, v8

    .line 4871
    .restart local v4    # "totalTime":D
    sput-wide v0, Lcom/android/emailcommon/utility/Utility;->parseTime:J

    .line 4872
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " => debugTime("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "), totalTime("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static decode(Ljava/nio/charset/Charset;[B)Ljava/lang/String;
    .locals 6
    .param p0, "charset"    # Ljava/nio/charset/Charset;
    .param p1, "b"    # [B

    .prologue
    const/4 v2, 0x0

    .line 1223
    if-nez p1, :cond_0

    .line 1236
    :goto_0
    return-object v2

    .line 1227
    :cond_0
    const/4 v0, 0x0

    .line 1230
    .local v0, "cb":Ljava/nio/CharBuffer;
    :try_start_0
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {p0, v3}, Ljava/nio/charset/Charset;->decode(Ljava/nio/ByteBuffer;)Ljava/nio/CharBuffer;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1236
    new-instance v2, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->array()[C

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->length()I

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Ljava/lang/String;-><init>([CII)V

    goto :goto_0

    .line 1231
    :catch_0
    move-exception v1

    .line 1232
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method

.method private static encode(Ljava/nio/charset/Charset;Ljava/lang/String;)[B
    .locals 3
    .param p0, "charset"    # Ljava/nio/charset/Charset;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1213
    if-nez p1, :cond_0

    .line 1214
    const/4 v1, 0x0

    .line 1219
    :goto_0
    return-object v1

    .line 1216
    :cond_0
    invoke-static {p1}, Ljava/nio/CharBuffer;->wrap(Ljava/lang/CharSequence;)Ljava/nio/CharBuffer;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/nio/charset/Charset;->encode(Ljava/nio/CharBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1217
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    new-array v1, v2, [B

    .line 1218
    .local v1, "bytes":[B
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    goto :goto_0
.end method

.method public static findDuplicateAccount(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "email"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 2302
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2303
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$Account;->EMAILADDRESS_ACCOUNTID_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2306
    .local v6, "c":Landroid/database/Cursor;
    :cond_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2307
    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2308
    .local v8, "emailAddress":Ljava/lang/String;
    invoke-virtual {v8, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2310
    const/4 v1, 0x2

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 2311
    .local v7, "displayName":Ljava/lang/String;
    if-eqz v7, :cond_1

    .line 2315
    .end local v7    # "displayName":Ljava/lang/String;
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2317
    .end local v8    # "emailAddress":Ljava/lang/String;
    :goto_1
    return-object v7

    .restart local v7    # "displayName":Ljava/lang/String;
    .restart local v8    # "emailAddress":Ljava/lang/String;
    :cond_1
    move-object v7, p1

    .line 2311
    goto :goto_0

    .line 2315
    .end local v7    # "displayName":Ljava/lang/String;
    .end local v8    # "emailAddress":Ljava/lang/String;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v7, v3

    .line 2317
    goto :goto_1

    .line 2315
    :catchall_0
    move-exception v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static findExistingAccount(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Account;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "allowAccountId"    # J
    .param p3, "hostName"    # Ljava/lang/String;
    .param p4, "userLogin"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 861
    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v7, v6

    invoke-static/range {v1 .. v7}, Lcom/android/emailcommon/utility/Utility;->findExistingAccount(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v0

    return-object v0
.end method

.method public static findExistingAccount(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Account;
    .locals 27
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "allowAccountId"    # J
    .param p3, "hostName"    # Ljava/lang/String;
    .param p4, "userLogin"    # Ljava/lang/String;
    .param p5, "email"    # Ljava/lang/String;
    .param p6, "protocol"    # Ljava/lang/String;

    .prologue
    .line 866
    const-string v5, "@"

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 867
    .local v16, "emailParts":[Ljava/lang/String;
    const/4 v5, 0x0

    aget-object v5, v16, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    .line 870
    .local v22, "username":Ljava/lang/String;
    if-eqz v22, :cond_0

    .line 871
    const-string v5, "\\\\"

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v21

    .line 872
    .local v21, "userNameParts":[Ljava/lang/String;
    if-eqz v21, :cond_0

    move-object/from16 v0, v21

    array-length v5, v0

    const/4 v6, 0x1

    if-le v5, v6, :cond_0

    .line 873
    const/4 v5, 0x1

    aget-object v5, v21, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    .line 877
    .end local v21    # "userNameParts":[Ljava/lang/String;
    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 878
    .local v4, "resolver":Landroid/content/ContentResolver;
    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "login"

    aput-object v8, v6, v7

    const-string v7, "address like ? and login like ? and protocol not like \"smtp\""

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object p3, v8, v9

    const/4 v9, 0x1

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v25, 0x25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const/16 v25, 0x25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    aput-object v24, v8, v9

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 887
    .local v14, "c":Landroid/database/Cursor;
    :cond_1
    :goto_0
    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_a

    .line 888
    const/4 v5, 0x0

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 889
    .local v18, "hostAuthId":J
    const/4 v5, 0x1

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 891
    .local v10, "Login":Ljava/lang/String;
    const-string v5, "@"

    invoke-virtual {v10, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    .line 892
    .local v17, "emailPartsDB":[Ljava/lang/String;
    const/4 v5, 0x0

    aget-object v5, v17, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v23

    .line 896
    .local v23, "usernameDB":Ljava/lang/String;
    if-eqz v23, :cond_2

    .line 897
    const-string v5, "\\\\"

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 898
    .local v20, "userNameDbParts":[Ljava/lang/String;
    if-eqz v20, :cond_2

    move-object/from16 v0, v20

    array-length v5, v0

    const/4 v6, 0x1

    if-le v5, v6, :cond_2

    .line 899
    const/4 v5, 0x1

    aget-object v5, v20, v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v23

    .line 904
    .end local v20    # "userNameDbParts":[Ljava/lang/String;
    :cond_2
    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 911
    if-eqz p5, :cond_3

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move-wide/from16 v2, v18

    invoke-static {v0, v1, v2, v3}, Lcom/android/emailcommon/utility/Utility;->findExistingAccountEmail(Landroid/content/Context;Ljava/lang/String;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v5

    if-eqz v5, :cond_1

    .line 922
    :cond_3
    const/4 v15, 0x0

    .line 924
    .local v15, "c2":Landroid/database/Cursor;
    :try_start_1
    sget-object v5, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lcom/android/emailcommon/provider/EmailContent$Account;->ID_PROJECTION:[Ljava/lang/String;

    const-string v7, "hostAuthKeyRecv=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v8, v9

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 929
    :cond_4
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 930
    const/4 v5, 0x0

    invoke-interface {v15, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 931
    .local v12, "accountId":J
    cmp-long v5, v12, p1

    if-eqz v5, :cond_4

    .line 932
    move-object/from16 v0, p0

    invoke-static {v0, v12, v13}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Account;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v11

    .line 933
    .local v11, "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v11, :cond_4

    .line 941
    if-eqz v15, :cond_5

    :try_start_2
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 961
    :cond_5
    :goto_1
    if-eqz v14, :cond_6

    :try_start_3
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7

    .line 965
    .end local v10    # "Login":Ljava/lang/String;
    .end local v11    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v12    # "accountId":J
    .end local v15    # "c2":Landroid/database/Cursor;
    .end local v17    # "emailPartsDB":[Ljava/lang/String;
    .end local v18    # "hostAuthId":J
    .end local v23    # "usernameDB":Ljava/lang/String;
    :cond_6
    :goto_2
    return-object v11

    .line 941
    .restart local v10    # "Login":Ljava/lang/String;
    .restart local v15    # "c2":Landroid/database/Cursor;
    .restart local v17    # "emailPartsDB":[Ljava/lang/String;
    .restart local v18    # "hostAuthId":J
    .restart local v23    # "usernameDB":Ljava/lang/String;
    :cond_7
    if-eqz v15, :cond_1

    :try_start_4
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 942
    :catch_0
    move-exception v5

    goto/16 :goto_0

    .line 938
    :catch_1
    move-exception v5

    .line 941
    if-eqz v15, :cond_1

    :try_start_5
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto/16 :goto_0

    .line 942
    :catch_2
    move-exception v5

    goto/16 :goto_0

    .line 940
    :catchall_0
    move-exception v5

    .line 941
    if-eqz v15, :cond_8

    :try_start_6
    invoke-interface {v15}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_8
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 942
    :cond_8
    :goto_3
    :try_start_7
    throw v5
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 958
    .end local v10    # "Login":Ljava/lang/String;
    .end local v15    # "c2":Landroid/database/Cursor;
    .end local v17    # "emailPartsDB":[Ljava/lang/String;
    .end local v18    # "hostAuthId":J
    .end local v23    # "usernameDB":Ljava/lang/String;
    :catch_3
    move-exception v5

    .line 961
    if-eqz v14, :cond_9

    :try_start_8
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_9

    .line 965
    :cond_9
    :goto_4
    const/4 v11, 0x0

    goto :goto_2

    .line 948
    :cond_a
    if-eqz p5, :cond_b

    .line 950
    :try_start_9
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Utility;->findDuplicateAccount(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_b

    .line 951
    const-string v5, "Utility"

    const-string v6, "Duplicate Account !!!!"

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 952
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-static {v0, v1}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreAccountWithEmailAddress(Landroid/content/Context;Ljava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v11

    .line 953
    .restart local v11    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-eqz v11, :cond_b

    iget-wide v6, v11, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    cmp-long v5, v6, p1

    if-eqz v5, :cond_b

    .line 961
    if-eqz v14, :cond_6

    :try_start_a
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4

    goto :goto_2

    .line 962
    :catch_4
    move-exception v5

    goto :goto_2

    .line 961
    .end local v11    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    :cond_b
    if-eqz v14, :cond_9

    :try_start_b
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_5

    goto :goto_4

    .line 962
    :catch_5
    move-exception v5

    goto :goto_4

    .line 960
    :catchall_1
    move-exception v5

    .line 961
    if-eqz v14, :cond_c

    :try_start_c
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_a

    .line 962
    :cond_c
    :goto_5
    throw v5

    .line 942
    .restart local v10    # "Login":Ljava/lang/String;
    .restart local v11    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .restart local v12    # "accountId":J
    .restart local v15    # "c2":Landroid/database/Cursor;
    .restart local v17    # "emailPartsDB":[Ljava/lang/String;
    .restart local v18    # "hostAuthId":J
    .restart local v23    # "usernameDB":Ljava/lang/String;
    :catch_6
    move-exception v5

    goto :goto_1

    .line 962
    :catch_7
    move-exception v5

    goto :goto_2

    .line 942
    .end local v11    # "account":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v12    # "accountId":J
    :catch_8
    move-exception v6

    goto :goto_3

    .line 962
    .end local v10    # "Login":Ljava/lang/String;
    .end local v15    # "c2":Landroid/database/Cursor;
    .end local v17    # "emailPartsDB":[Ljava/lang/String;
    .end local v18    # "hostAuthId":J
    .end local v23    # "usernameDB":Ljava/lang/String;
    :catch_9
    move-exception v5

    goto :goto_4

    :catch_a
    move-exception v6

    goto :goto_5
.end method

.method public static findExistingAccountEmail(Landroid/content/Context;Ljava/lang/String;J)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "email"    # Ljava/lang/String;
    .param p2, "hostAuthId"    # J

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x1

    .line 798
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 799
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/utility/Utility;->EMAILADDRESS_ACCOUNTID_PROJECTION:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "hostAuthKeyRecv="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 802
    .local v6, "c":Landroid/database/Cursor;
    :cond_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 803
    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 805
    .local v7, "emailAddress":Ljava/lang/String;
    if-eqz v7, :cond_0

    .line 808
    invoke-virtual {v7, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 814
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v1, v8

    .line 816
    .end local v7    # "emailAddress":Ljava/lang/String;
    :goto_0
    return v1

    .line 814
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 816
    const/4 v1, 0x0

    goto :goto_0

    .line 814
    :catchall_0
    move-exception v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static fromUtf8([B)Ljava/lang/String;
    .locals 1
    .param p0, "b"    # [B

    .prologue
    .line 1246
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, p0}, Lcom/android/emailcommon/utility/Utility;->decode(Ljava/nio/charset/Charset;[B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getFirstRowColumn(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;Lcom/android/emailcommon/utility/Utility$CursorGetter;)Ljava/lang/Object;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "column"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ITT;",
            "Lcom/android/emailcommon/utility/Utility$CursorGetter",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1609
    .local p7, "defaultValue":Ljava/lang/Object;, "TT;"
    .local p8, "getter":Lcom/android/emailcommon/utility/Utility$CursorGetter;, "Lcom/android/emailcommon/utility/Utility$CursorGetter<TT;>;"
    const/4 v7, 0x0

    .line 1611
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-static {p1}, Lcom/android/emailcommon/utility/Utility;->buildLimitOneUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p1

    .line 1612
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1614
    if-eqz v7, :cond_1

    .line 1615
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1616
    move-object/from16 v0, p8

    invoke-interface {v0, v7, p6}, Lcom/android/emailcommon/utility/Utility$CursorGetter;->get(Landroid/database/Cursor;I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object p7

    .line 1624
    .end local p7    # "defaultValue":Ljava/lang/Object;, "TT;"
    if-eqz v7, :cond_0

    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 1627
    :cond_0
    :goto_0
    return-object p7

    .line 1624
    .restart local p7    # "defaultValue":Ljava/lang/Object;, "TT;"
    :cond_1
    if-eqz v7, :cond_0

    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 1625
    :catch_0
    move-exception v1

    goto :goto_0

    .line 1619
    :catch_1
    move-exception v8

    .line 1621
    .local v8, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v1, "Email"

    const-string v2, "getFirstRowColumn Exception!!"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1624
    if-eqz v7, :cond_0

    :try_start_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1625
    :catch_2
    move-exception v1

    goto :goto_0

    .line 1623
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    .line 1624
    if-eqz v7, :cond_2

    :try_start_5
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 1625
    :cond_2
    :goto_1
    throw v1

    .end local p7    # "defaultValue":Ljava/lang/Object;, "TT;"
    :catch_3
    move-exception v1

    goto :goto_0

    .restart local p7    # "defaultValue":Ljava/lang/Object;, "TT;"
    :catch_4
    move-exception v2

    goto :goto_1
.end method

.method public static getFirstRowInt(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;)Ljava/lang/Integer;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "column"    # I
    .param p7, "defaultValue"    # Ljava/lang/Integer;

    .prologue
    .line 1664
    sget-object v8, Lcom/android/emailcommon/utility/Utility;->INT_GETTER:Lcom/android/emailcommon/utility/Utility$CursorGetter;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move-object/from16 v7, p7

    invoke-static/range {v0 .. v8}, Lcom/android/emailcommon/utility/Utility;->getFirstRowColumn(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;Lcom/android/emailcommon/utility/Utility$CursorGetter;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public static getFirstRowLong(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;)Ljava/lang/Long;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;
    .param p6, "column"    # I
    .param p7, "defaultValue"    # Ljava/lang/Long;

    .prologue
    .line 1645
    sget-object v8, Lcom/android/emailcommon/utility/Utility;->LONG_GETTER:Lcom/android/emailcommon/utility/Utility$CursorGetter;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    move-object/from16 v7, p7

    invoke-static/range {v0 .. v8}, Lcom/android/emailcommon/utility/Utility;->getFirstRowColumn(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;Lcom/android/emailcommon/utility/Utility$CursorGetter;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public static getHexString([B)Ljava/lang/String;
    .locals 6
    .param p0, "b"    # [B

    .prologue
    .line 1332
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1333
    .local v1, "result":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x6

    if-ge v0, v2, :cond_0

    .line 1335
    const-string v2, "%02x"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aget-byte v5, p0, v0

    and-int/lit16 v5, v5, 0xff

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1333
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1337
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private static getRecipientAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "addressList"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 4502
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 4504
    .local v1, "addressListBuff":Ljava/lang/StringBuffer;
    if-eqz p0, :cond_1

    .line 4505
    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 4506
    .local v6, "toAddress":[Ljava/lang/String;
    move-object v2, v6

    .local v2, "arr$":[Ljava/lang/String;
    array-length v4, v2

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v0, v2, v3

    .line 4507
    .local v0, "address":Ljava/lang/String;
    const/4 v7, 0x2

    invoke-static {v7}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 4508
    .local v5, "splitAddress":[Ljava/lang/String;
    array-length v7, v5

    if-lez v7, :cond_0

    .line 4509
    aget-object v7, v5, v9

    const-string v8, "@"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 4510
    aget-object v7, v5, v9

    invoke-virtual {v1, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const/16 v8, 0x3b

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 4506
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 4516
    .end local v0    # "address":Ljava/lang/String;
    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "splitAddress":[Ljava/lang/String;
    .end local v6    # "toAddress":[Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method public static varargs getRowColumns(Landroid/content/Context;Landroid/net/Uri;J[Ljava/lang/String;)[Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "baseUri"    # Landroid/net/Uri;
    .param p2, "id"    # J
    .param p4, "projection"    # [Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1979
    invoke-static {p1, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0, p4, v1, v1}, Lcom/android/emailcommon/utility/Utility;->getRowColumns(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getRowColumns(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 1954
    array-length v1, p2

    new-array v8, v1, [Ljava/lang/String;

    .line 1955
    .local v8, "values":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .local v0, "cr":Landroid/content/ContentResolver;
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 1956
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1958
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1959
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    array-length v1, p2

    if-ge v7, v1, :cond_1

    .line 1960
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v8, v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1959
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 1966
    .end local v7    # "i":I
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v8, v5

    .line 1968
    .end local v8    # "values":[Ljava/lang/String;
    :goto_1
    return-object v8

    .line 1966
    .restart local v7    # "i":I
    .restart local v8    # "values":[Ljava/lang/String;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .end local v7    # "i":I
    :catchall_0
    move-exception v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static getSmallHashForDeviceId(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 1309
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    .line 1310
    .local v3, "userId":I
    const-string v4, "Utility"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "myUserId : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1311
    if-eqz v3, :cond_0

    .line 1313
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 1318
    :cond_0
    :try_start_0
    const-string v4, "SHA-1"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1323
    .local v2, "sha":Ljava/security/MessageDigest;
    invoke-static {p0}, Lcom/android/emailcommon/utility/Utility;->toUtf8(Ljava/lang/String;)[B

    move-result-object v0

    .line 1324
    .local v0, "bValue":[B
    if-eqz v0, :cond_1

    .line 1325
    invoke-virtual {v2, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 1328
    :cond_1
    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v4

    invoke-static {v4}, Lcom/android/emailcommon/utility/Utility;->getHexString([B)Ljava/lang/String;

    move-result-object v4

    .end local v0    # "bValue":[B
    .end local v2    # "sha":Ljava/security/MessageDigest;
    :goto_0
    return-object v4

    .line 1319
    :catch_0
    move-exception v1

    .line 1320
    .local v1, "impossible":Ljava/security/NoSuchAlgorithmException;
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static hasUnloadedAttachments(Landroid/content/Context;J)Z
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "messageId"    # J

    .prologue
    .line 1821
    invoke-static/range {p0 .. p2}, Lcom/android/emailcommon/provider/EmailContent$Message;->restoreMessageWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/EmailContent$Message;

    move-result-object v9

    .line 1822
    .local v9, "msg":Lcom/android/emailcommon/provider/EmailContent$Message;
    if-nez v9, :cond_0

    .line 1823
    const-string v11, "Email"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "hasUnloadedAttachments. messageId is "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-wide/from16 v0, p1

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " but msg is null"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1824
    const/4 v11, 0x0

    .line 1893
    :goto_0
    return v11

    .line 1827
    :cond_0
    invoke-static/range {p0 .. p2}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentsWithMessageId(Landroid/content/Context;J)[Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v4

    .line 1828
    .local v4, "atts":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-nez v4, :cond_1

    .line 1829
    const-string v11, "Email"

    const-string v12, "hasUnloadedAttachments. Attachment[] atts got null"

    invoke-static {v11, v12}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1830
    const/4 v11, 0x0

    goto :goto_0

    .line 1833
    :cond_1
    move-object v2, v4

    .local v2, "arr$":[Lcom/android/emailcommon/provider/EmailContent$Attachment;
    array-length v8, v2

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_1
    if-ge v7, v8, :cond_7

    aget-object v3, v2, v7

    .line 1834
    .local v3, "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/android/emailcommon/utility/Utility;->attachmentExists(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Attachment;)Z

    move-result v11

    if-nez v11, :cond_6

    .line 1842
    iget v11, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    and-int/lit8 v11, v11, 0x6

    if-nez v11, :cond_3

    .line 1843
    const-string v11, "Email"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Unloaded attachment isn\'t marked for download: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFileName:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", #"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-wide v14, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1849
    sget-object v11, Lcom/android/emailcommon/provider/EmailContent$Attachment;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v12, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-object/from16 v0, p0

    invoke-static {v0, v11, v12, v13}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->delete(Landroid/content/Context;Landroid/net/Uri;J)I

    .line 1876
    :cond_2
    :goto_2
    const/4 v11, 0x1

    goto :goto_0

    .line 1856
    :cond_3
    iget-object v11, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    if-eqz v11, :cond_4

    .line 1860
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 1861
    .local v5, "cv":Landroid/content/ContentValues;
    const-string v11, "contentUri"

    invoke-virtual {v5, v11}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1862
    sget-object v11, Lcom/android/emailcommon/provider/EmailContent$Attachment;->CONTENT_URI:Landroid/net/Uri;

    iget-wide v12, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mId:J

    move-object/from16 v0, p0

    invoke-static {v0, v11, v12, v13, v5}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->update(Landroid/content/Context;Landroid/net/Uri;JLandroid/content/ContentValues;)I

    goto :goto_2

    .line 1863
    .end local v5    # "cv":Landroid/content/ContentValues;
    :cond_4
    iget-object v11, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mContentUri:Ljava/lang/String;

    if-nez v11, :cond_2

    .line 1865
    :try_start_0
    iget-wide v12, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMessageKey:J

    iget-object v11, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mLocation:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13, v11}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentWithMessageIdAndLocation(Landroid/content/Context;JLjava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v10

    .line 1868
    .local v10, "sourceAtt":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v10, :cond_2

    iget v11, v10, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    and-int/lit16 v11, v11, 0x200

    if-eqz v11, :cond_2

    .line 1833
    .end local v10    # "sourceAtt":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :cond_5
    :goto_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 1872
    :catch_0
    move-exception v6

    .line 1873
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 1878
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_6
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/android/emailcommon/utility/Utility;->attachmentExistsAndHasRealData(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Attachment;)Z

    move-result v11

    if-nez v11, :cond_5

    .line 1880
    :try_start_1
    iget-wide v12, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mMessageKey:J

    iget-object v11, v3, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mLocation:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v12, v13, v11}, Lcom/android/emailcommon/provider/EmailContent$Attachment;->restoreAttachmentWithMessageIdAndLocation(Landroid/content/Context;JLjava/lang/String;)Lcom/android/emailcommon/provider/EmailContent$Attachment;

    move-result-object v10

    .line 1883
    .restart local v10    # "sourceAtt":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    if-eqz v10, :cond_5

    iget v11, v10, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    and-int/lit8 v11, v11, 0x2

    if-eqz v11, :cond_5

    .line 1885
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 1887
    .end local v10    # "sourceAtt":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    :catch_1
    move-exception v6

    .line 1888
    .restart local v6    # "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 1893
    .end local v3    # "att":Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_7
    const/4 v11, 0x0

    goto/16 :goto_0
.end method

.method public static isExistEasAccount(Landroid/content/Context;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    .line 4521
    if-nez p0, :cond_0

    move v1, v8

    .line 4541
    :goto_0
    return v1

    .line 4525
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 4526
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->ID_PROJECTION:[Ljava/lang/String;

    const-string v3, "protocol=\"eas\""

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 4529
    .local v6, "c":Landroid/database/Cursor;
    if-nez v6, :cond_1

    move v1, v8

    .line 4530
    goto :goto_0

    .line 4533
    :cond_1
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    .line 4534
    const/4 v1, 0x1

    .line 4538
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 4540
    .end local v0    # "resolver":Landroid/content/ContentResolver;
    .end local v6    # "c":Landroid/database/Cursor;
    :catch_0
    move-exception v7

    .local v7, "se":Ljava/lang/SecurityException;
    move v1, v8

    .line 4541
    goto :goto_0

    .line 4538
    .end local v7    # "se":Ljava/lang/SecurityException;
    .restart local v0    # "resolver":Landroid/content/ContentResolver;
    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v1, v8

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_0
.end method

.method public static isFirstUtf8Byte(B)Z
    .locals 2
    .param p0, "b"    # B

    .prologue
    .line 1265
    and-int/lit16 v0, p0, 0xc0

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFullMessageBodyLoadDisabled()Z
    .locals 1

    .prologue
    .line 5051
    const/4 v0, 0x0

    return v0
.end method

.method public static isHotmailAccount(Lcom/android/emailcommon/provider/EmailContent$Account;)Z
    .locals 2
    .param p0, "account"    # Lcom/android/emailcommon/provider/EmailContent$Account;

    .prologue
    .line 4097
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 4099
    iget-object v0, p0, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mAddress:Ljava/lang/String;

    const-string v1, ".hotmail.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4100
    const/4 v0, 0x1

    .line 4103
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isInContainer(Landroid/content/Context;)Z
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 4220
    const/4 v2, 0x0

    .line 4221
    .local v2, "result":Z
    const/4 v1, 0x0

    .line 4223
    .local v1, "knoxVersion":Ljava/lang/String;
    if-nez p0, :cond_1

    .line 4224
    const-string v3, "Utility"

    const-string v4, "isInContainer - ctx is null"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4266
    :cond_0
    :goto_0
    const-string v3, "Utility"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isInContainer return "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", KNOX version = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4267
    return v2

    .line 4226
    :cond_1
    invoke-static {p0}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 4227
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_2

    .line 4228
    const-string v3, "Utility"

    const-string v4, "PersonaManager.getKnoxInfoForApp() returned null"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4230
    :cond_2
    const-string v3, "version"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 4231
    if-nez v1, :cond_3

    .line 4232
    const-string v3, "Utility"

    const-string v4, "KNOX version is null"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 4234
    :cond_3
    const-string v3, "2.0"

    const-string v4, "version"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 4235
    sget-boolean v3, Lcom/android/emailcommon/utility/Utility;->IS_DEVICE_VZWJ_MINI:Z

    if-eqz v3, :cond_4

    .line 4236
    const-string v3, "true"

    const-string v4, "isSupportMoveTo"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 4237
    const-string v3, "true"

    const-string v4, "isKnoxMode"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 4240
    const/4 v2, 0x1

    goto :goto_0

    .line 4244
    :cond_4
    const-string v3, "true"

    const-string v4, "isKnoxMode"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 4247
    const/4 v2, 0x1

    goto :goto_0

    .line 4250
    :cond_5
    const-string v3, "1.0"

    const-string v4, "version"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 4252
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_6

    .line 4253
    const-string v3, "Utility"

    const-string v4, "isInContainer - PKG name is null"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4254
    :cond_6
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "sec_container_"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 4256
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 4260
    :cond_7
    const-string v3, "Utility"

    const-string v4, "isInContainer - unknown KNOX version"

    invoke-static {v3, v4}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static isInstalledApp(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 3880
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/16 v4, 0x80

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v1

    .line 3881
    .local v1, "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 3883
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3884
    const/4 v3, 0x1

    .line 3887
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static isNonPhone(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2479
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "config_voice_capable"

    const-string v3, "bool"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "config_sms_capable"

    const-string v3, "bool"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2482
    const/4 v0, 0x1

    .line 2484
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isRoaming(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 3844
    const-string v2, "phone"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 3845
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    const/4 v0, 0x0

    .line 3847
    .local v0, "isRoaming":Z
    invoke-static {p0}, Lcom/android/emailcommon/utility/Utility;->isWifiConnected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3848
    const-string v2, "Utility"

    const-string v3, "isRoaming(): WiFi is connected."

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3873
    :goto_0
    return v0

    .line 3865
    :cond_0
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v0

    .line 3867
    const-string v2, "Utility"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isRoaming = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/EmailLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static isSamsungAccount(Landroid/content/Context;J)Z
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountId"    # J

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 3910
    invoke-static {p0}, Lcom/android/emailcommon/utility/Utility;->isSamsungSingleuser(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3911
    const/4 v6, 0x0

    .line 3913
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "companyname"

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 3917
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3918
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 3919
    .local v7, "companyname":Ljava/lang/String;
    const-string v0, "samsungsingle"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3920
    const-string v0, "Email"

    const-string v1, "Single isSamsungAccount true"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3929
    if-eqz v6, :cond_1

    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :cond_1
    :goto_0
    move v0, v9

    .line 3934
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v7    # "companyname":Ljava/lang/String;
    :goto_1
    return v0

    .line 3929
    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_2
    if-eqz v6, :cond_3

    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 3933
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_3
    :goto_2
    const-string v0, "Email"

    const-string v1, "Single isSamsungAccount false"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v10

    .line 3934
    goto :goto_1

    .line 3924
    .restart local v6    # "c":Landroid/database/Cursor;
    :catch_0
    move-exception v8

    .line 3925
    .local v8, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v0, "Email"

    const-string v1, "Single isSamsungAccount false"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3929
    if-eqz v6, :cond_3

    :try_start_4
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    .line 3930
    :catch_1
    move-exception v0

    goto :goto_2

    .line 3928
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    .line 3929
    if-eqz v6, :cond_4

    :try_start_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 3930
    :cond_4
    :goto_3
    throw v0

    .restart local v7    # "companyname":Ljava/lang/String;
    :catch_2
    move-exception v0

    goto :goto_0

    .end local v7    # "companyname":Ljava/lang/String;
    :catch_3
    move-exception v0

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_3
.end method

.method public static isSamsungSingleuser(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 3892
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 3894
    .local v0, "pm":Landroid/content/pm/PackageManager;
    invoke-static {p0}, Lcom/android/emailcommon/utility/Utility;->isInContainer(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "sec_container_1.com.sec.knox.ssoagent"

    invoke-static {p0, v1}, Lcom/android/emailcommon/utility/Utility;->isInstalledApp(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "sec_container_1.com.sds.mobiledesk"

    invoke-static {p0, v1}, Lcom/android/emailcommon/utility/Utility;->isInstalledApp(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3898
    const-string v1, "Email"

    const-string v2, "Single isSamsungSingleuser true"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3899
    const/4 v1, 0x1

    .line 3903
    :goto_0
    return v1

    .line 3902
    :cond_0
    const-string v1, "Email"

    const-string v2, "Single isSamsungSingleuser false"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3903
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isSdpEnabled()Z
    .locals 1

    .prologue
    .line 5055
    const/4 v0, 0x0

    return v0
.end method

.method public static isWifiConnected(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3833
    const-string v4, "connectivity"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 3834
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 3835
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    if-nez v1, :cond_1

    .line 3839
    :cond_0
    :goto_0
    return v2

    .line 3836
    :cond_1
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    if-ne v4, v3, :cond_0

    move v2, v3

    .line 3837
    goto :goto_0
.end method

.method public static killMyProcess(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 4910
    const-string v0, "Utility"

    const-string v1, "KillMyProcess : Killed"

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 4911
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 4912
    return-void
.end method

.method public static nextTimeAfterNextSendDuration(IJ)J
    .locals 7
    .param p0, "retrySendTimes"    # I
    .param p1, "currentmilliTime"    # J

    .prologue
    .line 1921
    const-string v4, "Utility"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nextTimeAfterNextSendDuration() : retrySendTimes :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1922
    const-string v4, "Utility"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nextTimeAfterNextSendDuration() : currentmilliTime "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1926
    const-string v4, "Utility"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nextTimeAfterNextSendDuration() : currentmilliTime "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1928
    sget-object v4, Lcom/android/emailcommon/utility/Utility;->SEND_OUTBOX_DURATION_MINUTES:[I

    array-length v1, v4

    .line 1929
    .local v1, "durationTableSize":I
    const/4 v0, 0x0

    .line 1931
    .local v0, "durationIndex":I
    if-lt p0, v1, :cond_0

    .line 1932
    add-int/lit8 v0, v1, -0x1

    .line 1936
    :goto_0
    const-string v4, "Utility"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nextTimeAfterNextSendDuration() : durationIndex "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1938
    sget-object v4, Lcom/android/emailcommon/utility/Utility;->SEND_OUTBOX_DURATION_MINUTES:[I

    aget v4, v4, v0

    const v5, 0xea60

    mul-int/2addr v4, v5

    int-to-long v4, v4

    add-long v2, p1, v4

    .line 1940
    .local v2, "nextTime":J
    const-string v4, "Utility"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nextTimeAfterNextSendDuration() : nextTime : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1942
    return-wide v2

    .line 1934
    .end local v2    # "nextTime":J
    :cond_0
    move v0, p0

    goto :goto_0
.end method

.method public static notSupportSMS(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 2490
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.android.mms"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2495
    :goto_0
    return v1

    .line 2491
    :catch_0
    move-exception v0

    .line 2492
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v1, "Utility"

    const-string v2, "notSupportSMS"

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2493
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static parseDateTimeToCalendar(Ljava/lang/String;)Ljava/util/GregorianCalendar;
    .locals 8
    .param p0, "date"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0xd

    const/16 v5, 0xb

    const/4 v4, 0x6

    const/4 v2, 0x4

    .line 1003
    new-instance v0, Ljava/util/GregorianCalendar;

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0x8

    invoke-virtual {p0, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0x9

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p0, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/16 v6, 0xf

    invoke-virtual {p0, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-direct/range {v0 .. v6}, Ljava/util/GregorianCalendar;-><init>(IIIIII)V

    .line 1007
    .local v0, "cal":Ljava/util/GregorianCalendar;
    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1008
    return-object v0
.end method

.method public static parseDateTimeToMillis(Ljava/lang/String;)J
    .locals 4
    .param p0, "date"    # Ljava/lang/String;

    .prologue
    .line 991
    invoke-static {p0}, Lcom/android/emailcommon/utility/Utility;->parseDateTimeToCalendar(Ljava/lang/String;)Ljava/util/GregorianCalendar;

    move-result-object v0

    .line 992
    .local v0, "cal":Ljava/util/GregorianCalendar;
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v2

    return-wide v2
.end method

.method public static parseEmailDateTimeToMillis(Ljava/lang/String;)J
    .locals 12
    .param p0, "date"    # Ljava/lang/String;

    .prologue
    .line 1020
    const-wide/16 v10, 0x0

    .line 1021
    .local v10, "retval":J
    if-eqz p0, :cond_0

    .line 1022
    const-string v1, "Email Input Date format"

    invoke-static {v1, p0}, Lcom/android/emailcommon/utility/Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1023
    const/4 v7, 0x0

    .line 1025
    .local v7, "cal":Ljava/util/GregorianCalendar;
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x10

    if-le v1, v2, :cond_1

    .line 1026
    new-instance v0, Ljava/util/GregorianCalendar;

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x5

    const/4 v3, 0x7

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0x8

    const/16 v4, 0xa

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0xb

    const/16 v5, 0xd

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/16 v5, 0xe

    const/16 v6, 0x10

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/16 v6, 0x11

    const/16 v9, 0x13

    invoke-virtual {p0, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-direct/range {v0 .. v6}, Ljava/util/GregorianCalendar;-><init>(IIIIII)V
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1032
    .end local v7    # "cal":Ljava/util/GregorianCalendar;
    .local v0, "cal":Ljava/util/GregorianCalendar;
    :try_start_1
    const-string v1, "GMT"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1041
    :goto_0
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v10

    .line 1042
    const-wide/16 v2, 0x0

    cmp-long v1, v10, v2

    if-gez v1, :cond_0

    .line 1043
    const-string v1, "Email Input Date format"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bad transform : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1050
    .end local v0    # "cal":Ljava/util/GregorianCalendar;
    :cond_0
    :goto_1
    return-wide v10

    .line 1034
    .restart local v7    # "cal":Ljava/util/GregorianCalendar;
    :cond_1
    :try_start_2
    new-instance v0, Ljava/util/GregorianCalendar;

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x4

    const/4 v3, 0x6

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    const/4 v3, 0x6

    const/16 v4, 0x8

    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0x9

    const/16 v5, 0xb

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/16 v5, 0xb

    const/16 v6, 0xd

    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/16 v6, 0xd

    const/16 v9, 0xf

    invoke-virtual {p0, v6, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-direct/range {v0 .. v6}, Ljava/util/GregorianCalendar;-><init>(IIIIII)V
    :try_end_2
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0

    .end local v7    # "cal":Ljava/util/GregorianCalendar;
    .restart local v0    # "cal":Ljava/util/GregorianCalendar;
    goto :goto_0

    .line 1045
    .end local v0    # "cal":Ljava/util/GregorianCalendar;
    .restart local v7    # "cal":Ljava/util/GregorianCalendar;
    :catch_0
    move-exception v8

    move-object v0, v7

    .line 1046
    .end local v7    # "cal":Ljava/util/GregorianCalendar;
    .restart local v0    # "cal":Ljava/util/GregorianCalendar;
    .local v8, "e":Ljava/lang/StringIndexOutOfBoundsException;
    :goto_2
    const-string v1, "Utility"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception occured due to incorrect date format, date = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1047
    invoke-virtual {v8}, Ljava/lang/StringIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1

    .line 1045
    .end local v8    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    :catch_1
    move-exception v8

    goto :goto_2
.end method

.method public static quoteString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 576
    if-nez p0, :cond_1

    .line 577
    const/4 p0, 0x0

    .line 582
    .end local p0    # "s":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 579
    .restart local p0    # "s":Ljava/lang/String;
    :cond_1
    const-string v0, "^\".*\"$"

    invoke-virtual {p0, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 580
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static registerSensitiveFileWithSdpIfNecessary(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fullFileName"    # Ljava/lang/String;

    .prologue
    .line 5083
    if-eqz p0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isSdpEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 5085
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    .line 5086
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    .line 5087
    .local v2, "uid":I
    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    .line 5088
    .local v3, "userId":I
    const-string v4, "Utility"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Call SdpManager.SDP_setSensitiveFile() for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Binder data: uid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " userId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pid = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5090
    invoke-static {}, Lcom/android/sdp/SdpManager;->getSdpManager()Lcom/android/sdp/SdpManager;

    move-result-object v1

    .line 5091
    .local v1, "sdp":Lcom/android/sdp/SdpManager;
    if-eqz v1, :cond_0

    .line 5092
    invoke-virtual {v1, p1}, Lcom/android/sdp/SdpManager;->SDP_SetSensitiveFile(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5098
    .end local v1    # "sdp":Lcom/android/sdp/SdpManager;
    .end local v2    # "uid":I
    .end local v3    # "userId":I
    :cond_0
    :goto_0
    return-void

    .line 5094
    :catch_0
    move-exception v0

    .line 5095
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "Utility"

    invoke-static {v4, v0}, Lcom/android/emailcommon/utility/Log;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static remainedTimeUntilNextSendDuration(IJJ)J
    .locals 9
    .param p0, "retrySendTimes"    # I
    .param p1, "timestamp"    # J
    .param p3, "currentmilliTime"    # J

    .prologue
    .line 1898
    const-string v4, "Utility"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "remainedTimeUntilNextSendDuration(): retrySendTimes :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1899
    const-string v4, "Utility"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "remainedTimeUntilNextSendDuration(): timestamp "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1900
    const-string v4, "Utility"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "remainedTimeUntilNextSendDuration(): currentmilliTime "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1902
    const-wide/32 v4, 0xea60

    rem-long v4, p3, v4

    sub-long v4, p3, v4

    const-wide/32 v6, 0xea60

    div-long v0, v4, v6

    .line 1903
    .local v0, "currentMinuteTime":J
    const-string v4, "Utility"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "remainedTimeUntilNextSendDuration(): currentMinuteTime "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1906
    sub-long v2, p1, p3

    .line 1908
    .local v2, "remainedTime":J
    if-lez p0, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-lez v4, :cond_0

    const-wide/16 v4, 0x7530

    cmp-long v4, v2, v4

    if-gez v4, :cond_1

    .line 1909
    :cond_0
    const-string v4, "Utility"

    const-string v5, "remainedTimeUntilNextSendDuration(): sendDurationExceeded return 0."

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1910
    const-wide/16 v2, 0x0

    .line 1917
    .end local v2    # "remainedTime":J
    :goto_0
    return-wide v2

    .line 1913
    .restart local v2    # "remainedTime":J
    :cond_1
    const-string v4, "Utility"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "remainedTimeUntilNextSendDuration(): sendDurationExceeded return remainedTime : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static replaceBareLfWithCrlf(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 1280
    const-string v0, "\r"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    const-string v2, "\r\n"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static runAsync(Ljava/lang/Runnable;)Landroid/os/AsyncTask;
    .locals 2
    .param p0, "r"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            ")",
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1416
    new-instance v0, Lcom/android/emailcommon/utility/Utility$2;

    invoke-direct {v0, p0}, Lcom/android/emailcommon/utility/Utility$2;-><init>(Ljava/lang/Runnable;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/emailcommon/utility/Utility$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    return-object v0
.end method

.method public static runningTask(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 5031
    const/4 v3, 0x0

    .line 5033
    .local v3, "found":Z
    :try_start_0
    const-string v7, "activity"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 5034
    .local v0, "am":Landroid/app/ActivityManager;
    const v7, 0x7fffffff

    invoke-virtual {v0, v7}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v6

    .line 5035
    .local v6, "taskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v1, 0x0

    .line 5037
    .local v1, "componentInfo":Landroid/content/ComponentName;
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 5038
    .local v5, "runningTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v7, v5, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.android.email"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 5039
    iget-object v1, v5, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 5040
    const-string v7, "Utility"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "packageName : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "className : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5041
    const/4 v3, 0x1

    goto :goto_0

    .line 5044
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v1    # "componentInfo":Landroid/content/ComponentName;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "runningTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v6    # "taskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :catch_0
    move-exception v2

    .line 5045
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 5047
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    return v3
.end method

.method public static sendReportToAgent(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0, "action"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "emailaddress"    # Ljava/lang/String;

    .prologue
    .line 3938
    const-string v1, "Email"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SingleReceiver sendReportToAgent >>>>> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3940
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 3941
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3942
    const-string v1, "application.type"

    const-string v2, "eas"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3943
    const-string v1, "user.email"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3944
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 3946
    const-string v1, "com.sec.knox.ssoagent.USE_KNOX"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 3947
    return-void
.end method

.method public static setSdpEnabled(Landroid/content/Context;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 5059
    if-eqz p0, :cond_0

    .line 5060
    const-string v4, "persona"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PersonaManager;

    .line 5061
    .local v1, "pm":Landroid/os/PersonaManager;
    if-eqz v1, :cond_0

    .line 5062
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    .line 5063
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    .line 5064
    .local v2, "uid":I
    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    .line 5065
    .local v3, "userId":I
    invoke-virtual {v1, v3}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v0

    .line 5066
    .local v0, "pinfo":Landroid/content/pm/PersonaInfo;
    if-eqz v0, :cond_0

    .line 5067
    const-string v4, "Utility"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setSdpEnabled userId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " PersonaInfo.sdpEnabled = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, v0, Landroid/content/pm/PersonaInfo;->sdpEnabled:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5069
    iget-boolean v4, v0, Landroid/content/pm/PersonaInfo;->sdpEnabled:Z

    sput-boolean v4, Lcom/android/emailcommon/utility/Utility;->mIsSdpEnabled:Z

    .line 5073
    .end local v0    # "pinfo":Landroid/content/pm/PersonaInfo;
    .end local v1    # "pm":Landroid/os/PersonaManager;
    .end local v2    # "uid":I
    .end local v3    # "userId":I
    :cond_0
    return-void
.end method

.method public static setSensitiveApp(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 5128
    if-eqz p0, :cond_0

    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isSdpEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5130
    :try_start_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    .line 5131
    const-string v2, "Utility"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setSensitiveApp: pid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5132
    invoke-static {}, Lcom/android/sdp/SdpManager;->getSdpManager()Lcom/android/sdp/SdpManager;

    move-result-object v1

    .line 5133
    .local v1, "sdp":Lcom/android/sdp/SdpManager;
    if-eqz v1, :cond_0

    .line 5134
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/sdp/SdpManager;->SDP_SetSensitiveApp(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5140
    .end local v1    # "sdp":Lcom/android/sdp/SdpManager;
    :cond_0
    :goto_0
    return-void

    .line 5136
    :catch_0
    move-exception v0

    .line 5137
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "Email"

    invoke-static {v2, v0}, Lcom/android/emailcommon/utility/Log;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static toUtf8(Ljava/lang/String;)[B
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 1241
    sget-object v0, Lcom/android/emailcommon/utility/Utility;->UTF_8:Ljava/nio/charset/Charset;

    invoke-static {v0, p0}, Lcom/android/emailcommon/utility/Utility;->encode(Ljava/nio/charset/Charset;Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public static updateLog4LifeTimesApp(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Message;Z)V
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "message"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .param p2, "isSending"    # Z

    .prologue
    .line 4408
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-static/range {p0 .. p0}, Lcom/android/emailcommon/utility/Utility;->isInContainer(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 4496
    :cond_0
    :goto_0
    return-void

    .line 4411
    :cond_1
    const/4 v5, -0x1

    .line 4414
    .local v5, "contextProviderVersion":I
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    const-string v15, "com.samsung.android.providers.context"

    const/16 v16, 0x80

    invoke-virtual/range {v14 .. v16}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v12

    .line 4415
    .local v12, "pInfo":Landroid/content/pm/PackageInfo;
    iget v5, v12, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 4417
    const-string v14, "Utility"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "contextProviderVersion = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4423
    .end local v12    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_1
    const/4 v14, 0x2

    if-lt v5, v14, :cond_0

    .line 4424
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 4427
    .local v7, "cv":Landroid/content/ContentValues;
    :try_start_1
    const-string v14, "Utility"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "updateLog4LifeTimesApp "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mAccountKey:J

    move-wide/from16 v16, v0

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-wide/from16 v16, v0

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4429
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 4430
    .local v13, "uri":Ljava/lang/String;
    const-string v14, "custom_id"

    invoke-virtual {v7, v14, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4433
    if-eqz p2, :cond_2

    .line 4434
    const-string v14, "type"

    const-string v15, "2"

    invoke-virtual {v7, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4436
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTo:Ljava/lang/String;

    invoke-static {v14}, Lcom/android/emailcommon/utility/Utility;->getRecipientAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 4437
    .local v10, "email_address":Ljava/lang/String;
    const-string v14, "email_address"

    invoke-virtual {v7, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4439
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mCc:Ljava/lang/String;

    invoke-static {v14}, Lcom/android/emailcommon/utility/Utility;->getRecipientAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 4440
    .local v4, "cc_address":Ljava/lang/String;
    const-string v14, "cc_address"

    invoke-virtual {v7, v14, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4458
    .end local v4    # "cc_address":Ljava/lang/String;
    :goto_2
    new-instance v11, Ljava/text/SimpleDateFormat;

    const-string v14, "yyyy-MM-dd HH:mm:ss.SSS"

    sget-object v15, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v11, v14, v15}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 4459
    .local v11, "mTimeFormatUtc":Ljava/text/SimpleDateFormat;
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    .line 4460
    .local v6, "currentTime":Ljava/util/Date;
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mTimeStamp:J

    invoke-virtual {v6, v14, v15}, Ljava/util/Date;->setTime(J)V

    .line 4461
    const-string v14, "UTC"

    invoke-static {v14}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v14

    invoke-virtual {v11, v14}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 4462
    const-string v14, "date"

    invoke-virtual {v11, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v7, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4483
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 4484
    .local v2, "APP_ID":Ljava/lang/String;
    const-string v14, "app_id"

    invoke-virtual {v7, v14, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4486
    const-string v14, "content://com.samsung.android.providers.context.log.exchange_email"

    invoke-static {v14}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 4487
    .local v3, "CONTENT_URI_EXCHANGEEMAIL":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    invoke-virtual {v14, v3, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 4494
    .end local v2    # "APP_ID":Ljava/lang/String;
    .end local v3    # "CONTENT_URI_EXCHANGEEMAIL":Landroid/net/Uri;
    .end local v6    # "currentTime":Ljava/util/Date;
    .end local v10    # "email_address":Ljava/lang/String;
    .end local v11    # "mTimeFormatUtc":Ljava/text/SimpleDateFormat;
    .end local v13    # "uri":Ljava/lang/String;
    :goto_3
    invoke-virtual {v7}, Landroid/content/ContentValues;->clear()V

    goto/16 :goto_0

    .line 4418
    .end local v7    # "cv":Landroid/content/ContentValues;
    :catch_0
    move-exception v9

    .line 4419
    .local v9, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v14, "Utility"

    const-string v15, "NameNotFoundException is occured."

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4420
    invoke-virtual {v9}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_1

    .line 4443
    .end local v9    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v7    # "cv":Landroid/content/ContentValues;
    .restart local v13    # "uri":Ljava/lang/String;
    :cond_2
    :try_start_2
    const-string v14, "type"

    const-string v15, "1"

    invoke-virtual {v7, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4445
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFrom:Ljava/lang/String;

    invoke-static {v14}, Lcom/android/emailcommon/utility/Utility;->getRecipientAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 4446
    .restart local v10    # "email_address":Ljava/lang/String;
    const-string v14, "email_address"

    invoke-virtual {v7, v14, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 4489
    .end local v10    # "email_address":Ljava/lang/String;
    .end local v13    # "uri":Ljava/lang/String;
    :catch_1
    move-exception v8

    .line 4490
    .local v8, "e":Ljava/lang/Exception;
    const-string v14, "Utility"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "updateLog4LifeTimesApp failed e: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4491
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method
