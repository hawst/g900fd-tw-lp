.class public Lcom/android/emailcommon/utility/AccountReconciler;
.super Ljava/lang/Object;
.source "AccountReconciler.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static reconcileAccounts(Landroid/content/Context;Ljava/util/List;[Landroid/accounts/Account;Landroid/content/ContentResolver;)Z
    .locals 26
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "accountManagerAccounts"    # [Landroid/accounts/Account;
    .param p3, "resolver"    # Landroid/content/ContentResolver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Account;",
            ">;[",
            "Landroid/accounts/Account;",
            "Landroid/content/ContentResolver;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "emailProviderAccounts":Ljava/util/List;, "Ljava/util/List<Lcom/android/emailcommon/provider/EmailContent$Account;>;"
    const/4 v6, 0x0

    .line 56
    .local v6, "accountsDeleted":Z
    const/4 v11, 0x0

    .line 58
    .local v11, "exchangeAccountDeleted":Z
    if-nez p1, :cond_0

    .line 59
    const/4 v6, 0x0

    move v7, v6

    .line 169
    .end local v6    # "accountsDeleted":Z
    .local v7, "accountsDeleted":I
    :goto_0
    return v7

    .line 61
    .end local v7    # "accountsDeleted":I
    .restart local v6    # "accountsDeleted":Z
    :cond_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_1
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_a

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 62
    .local v19, "providerAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-nez v19, :cond_2

    .line 63
    const/4 v6, 0x0

    move v7, v6

    .restart local v7    # "accountsDeleted":I
    goto :goto_0

    .line 65
    .end local v7    # "accountsDeleted":I
    :cond_2
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 66
    .local v20, "providerAccountName":Ljava/lang/String;
    const/4 v12, 0x0

    .line 67
    .local v12, "found":Z
    move-object/from16 v8, p2

    .local v8, "arr$":[Landroid/accounts/Account;
    array-length v0, v8

    move/from16 v18, v0

    .local v18, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_2
    move/from16 v0, v18

    if-ge v14, v0, :cond_3

    aget-object v4, v8, v14

    .line 68
    .local v4, "accountManagerAccount":Landroid/accounts/Account;
    iget-object v0, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 69
    const/4 v12, 0x1

    .line 73
    .end local v4    # "accountManagerAccount":Landroid/accounts/Account;
    :cond_3
    if-nez v12, :cond_1

    .line 74
    move-object/from16 v0, v19

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    move/from16 v22, v0

    and-int/lit8 v22, v22, 0x10

    if-eqz v22, :cond_5

    .line 75
    const-string v22, "Email"

    const-string v23, "Account reconciler noticed incomplete account; ignoring"

    invoke-static/range {v22 .. v23}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 67
    .restart local v4    # "accountManagerAccount":Landroid/accounts/Account;
    :cond_4
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 86
    .end local v4    # "accountManagerAccount":Landroid/accounts/Account;
    :cond_5
    :try_start_0
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-object/from16 v22, v0

    if-eqz v22, :cond_8

    const-string v22, "snc"

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mProtocol:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_8

    const/16 v17, 0x1

    .line 90
    .local v17, "isSncAccount":Z
    :goto_3
    if-nez v17, :cond_1

    .line 91
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-object/from16 v22, v0

    if-eqz v22, :cond_9

    const-string v22, "eas"

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mProtocol:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_9

    const/16 v16, 0x1

    .line 94
    .local v16, "isEasAccount":Z
    :goto_4
    const-string v22, "Email"

    const-string v23, "Account deleted in AccountManager; deleting from provider: "

    invoke-static/range {v22 .. v23}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/utility/Utility;->isSamsungAccount(Landroid/content/Context;J)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 99
    const-string v22, "com.sec.knox.ssoagent.del.ACCOUNT_DELETED"

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/utility/Utility;->sendReportToAgent(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;)V

    .line 102
    :cond_6
    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/utility/AttachmentUtilities;->deleteAllAccountAttachmentFilesUri(Landroid/content/Context;J)V

    .line 104
    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/utility/BodyUtilites;->deleteAllAccountBodyFilesUri(Landroid/content/Context;J)V

    .line 105
    sget-object v22, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 108
    new-instance v22, Lcom/android/emailcommon/service/AccountServiceProxy;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/android/emailcommon/service/AccountServiceProxy;->cancelAllNotifications(J)V

    .line 109
    if-eqz v16, :cond_7

    .line 110
    const/4 v11, 0x1

    .line 112
    :cond_7
    const/4 v6, 0x1

    .line 113
    const-string v22, "Email"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "account="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " source=reconciler"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " type="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v19

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mAccountType:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " action=deleted"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/android/emailcommon/utility/EmailLog;->logToDropBox(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 116
    .end local v16    # "isEasAccount":Z
    .end local v17    # "isSncAccount":Z
    :catch_0
    move-exception v10

    .line 117
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 86
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_8
    const/16 v17, 0x0

    goto/16 :goto_3

    .line 91
    .restart local v17    # "isSncAccount":Z
    :cond_9
    const/16 v16, 0x0

    goto/16 :goto_4

    .line 123
    .end local v8    # "arr$":[Landroid/accounts/Account;
    .end local v12    # "found":Z
    .end local v14    # "i$":I
    .end local v17    # "isSncAccount":Z
    .end local v18    # "len$":I
    .end local v19    # "providerAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v20    # "providerAccountName":Ljava/lang/String;
    :cond_a
    if-eqz v11, :cond_b

    invoke-static/range {p0 .. p0}, Lcom/android/emailcommon/provider/EmailContent$Account;->restoreUnifiedAccount(Landroid/content/Context;)Lcom/android/emailcommon/provider/EmailContent$Account;

    move-result-object v22

    if-eqz v22, :cond_b

    .line 124
    const-string v22, "Email"

    const-string v23, "Post broadcast to S&C module to update Exchange profile"

    invoke-static/range {v22 .. v23}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    new-instance v15, Landroid/content/Intent;

    invoke-direct {v15}, Landroid/content/Intent;-><init>()V

    .line 126
    .local v15, "intent":Landroid/content/Intent;
    const-string v22, "com.android.email.syncnconnect.receiver.ExchangeProfileUpdate"

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    const-string v22, "com.android.email.syncnconnect.receiver.updateExchangeProfile"

    const/16 v23, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 128
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 133
    .end local v15    # "intent":Landroid/content/Intent;
    :cond_b
    move-object/from16 v8, p2

    .restart local v8    # "arr$":[Landroid/accounts/Account;
    array-length v0, v8

    move/from16 v18, v0

    .restart local v18    # "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    move v14, v13

    .end local v13    # "i$":I
    .restart local v14    # "i$":I
    :goto_5
    move/from16 v0, v18

    if-ge v14, v0, :cond_f

    aget-object v4, v8, v14

    .line 134
    .restart local v4    # "accountManagerAccount":Landroid/accounts/Account;
    iget-object v5, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 135
    .local v5, "accountManagerAccountName":Ljava/lang/String;
    const/4 v12, 0x0

    .line 136
    .restart local v12    # "found":Z
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .end local v14    # "i$":I
    .local v13, "i$":Ljava/util/Iterator;
    :cond_c
    :goto_6
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_d

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 137
    .local v9, "cachedEasAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    iget-object v0, v9, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_c

    .line 138
    const/4 v12, 0x1

    goto :goto_6

    .line 141
    .end local v9    # "cachedEasAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    :cond_d
    if-nez v12, :cond_e

    .line 143
    const-string v22, "Email"

    const-string v23, "Account deleted from provider; deleting from AccountManager: "

    invoke-static/range {v22 .. v23}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :try_start_1
    invoke-static/range {p0 .. p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v4, v1, v2}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    .line 166
    :goto_7
    const/4 v6, 0x1

    .line 133
    :cond_e
    add-int/lit8 v13, v14, 0x1

    .local v13, "i$":I
    move v14, v13

    .end local v13    # "i$":I
    .restart local v14    # "i$":I
    goto :goto_5

    .line 150
    .end local v14    # "i$":I
    .local v13, "i$":Ljava/util/Iterator;
    :catch_1
    move-exception v21

    .line 151
    .local v21, "se":Ljava/lang/SecurityException;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_7

    .end local v4    # "accountManagerAccount":Landroid/accounts/Account;
    .end local v5    # "accountManagerAccountName":Ljava/lang/String;
    .end local v12    # "found":Z
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v21    # "se":Ljava/lang/SecurityException;
    .restart local v14    # "i$":I
    :cond_f
    move v7, v6

    .line 169
    .restart local v7    # "accountsDeleted":I
    goto/16 :goto_0
.end method

.method public static reconcileSingleAccounts(Landroid/content/Context;Ljava/util/List;[Landroid/accounts/Account;Landroid/content/ContentResolver;)Z
    .locals 26
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "accountManagerAccounts"    # [Landroid/accounts/Account;
    .param p3, "resolver"    # Landroid/content/ContentResolver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/android/emailcommon/provider/EmailContent$Account;",
            ">;[",
            "Landroid/accounts/Account;",
            "Landroid/content/ContentResolver;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 244
    .local p1, "emailProviderAccounts":Ljava/util/List;, "Ljava/util/List<Lcom/android/emailcommon/provider/EmailContent$Account;>;"
    const/4 v7, 0x0

    .line 245
    .local v7, "accountsDeleted":Z
    const/4 v12, 0x0

    .line 247
    .local v12, "exchangeAccountDeleted":Z
    invoke-static/range {p0 .. p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v22

    const-string v23, "com.seven.Z7.work"

    invoke-virtual/range {v22 .. v23}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    .line 249
    .local v6, "accountMgrMySingleList":[Landroid/accounts/Account;
    if-nez p1, :cond_0

    .line 250
    const/4 v7, 0x0

    move v8, v7

    .line 340
    .end local v7    # "accountsDeleted":Z
    .local v8, "accountsDeleted":I
    :goto_0
    return v8

    .line 252
    .end local v8    # "accountsDeleted":I
    .restart local v7    # "accountsDeleted":Z
    :cond_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_1
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_b

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/android/emailcommon/provider/EmailContent$Account;

    .line 253
    .local v19, "providerAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    if-nez v19, :cond_2

    .line 254
    const/4 v7, 0x0

    move v8, v7

    .restart local v8    # "accountsDeleted":I
    goto :goto_0

    .line 256
    .end local v8    # "accountsDeleted":I
    :cond_2
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mEmailAddress:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 257
    .local v20, "providerAccountName":Ljava/lang/String;
    const/4 v13, 0x0

    .line 258
    .local v13, "found":Z
    move-object v9, v6

    .local v9, "arr$":[Landroid/accounts/Account;
    array-length v0, v9

    move/from16 v18, v0

    .local v18, "len$":I
    const/4 v15, 0x0

    .local v15, "i$":I
    :goto_2
    move/from16 v0, v18

    if-ge v15, v0, :cond_3

    aget-object v5, v9, v15

    .line 259
    .local v5, "accountManagerSingleAccount":Landroid/accounts/Account;
    iget-object v0, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 260
    const/4 v13, 0x1

    .line 264
    .end local v5    # "accountManagerSingleAccount":Landroid/accounts/Account;
    :cond_3
    if-nez v13, :cond_1

    .line 265
    move-object/from16 v0, v19

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mFlags:I

    move/from16 v22, v0

    and-int/lit8 v22, v22, 0x10

    if-eqz v22, :cond_5

    .line 266
    const-string v22, "Email"

    const-string v23, "Account reconciler noticed incomplete account; ignoring"

    invoke-static/range {v22 .. v23}, Lcom/android/emailcommon/utility/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 258
    .restart local v5    # "accountManagerSingleAccount":Landroid/accounts/Account;
    :cond_4
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 276
    .end local v5    # "accountManagerSingleAccount":Landroid/accounts/Account;
    :cond_5
    :try_start_0
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-object/from16 v22, v0

    if-eqz v22, :cond_8

    const-string v22, "snc"

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mProtocol:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_8

    const/16 v17, 0x1

    .line 280
    .local v17, "isSncAccount":Z
    :goto_3
    if-nez v17, :cond_1

    .line 281
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-object/from16 v22, v0

    if-eqz v22, :cond_9

    const-string v22, "eas"

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mHostAuthRecv:Lcom/android/emailcommon/provider/EmailContent$HostAuth;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$HostAuth;->mProtocol:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_9

    const/16 v16, 0x1

    .line 286
    .local v16, "isEasAccount":Z
    :goto_4
    move-object/from16 v9, p2

    array-length v0, v9

    move/from16 v18, v0

    const/4 v15, 0x0

    :goto_5
    move/from16 v0, v18

    if-ge v15, v0, :cond_6

    aget-object v4, v9, v15

    .line 287
    .local v4, "accountManagerAccount":Landroid/accounts/Account;
    iget-object v0, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_a

    .line 289
    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/utility/Utility;->isSamsungAccount(Landroid/content/Context;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v22

    if-eqz v22, :cond_6

    .line 291
    :try_start_1
    invoke-static/range {p0 .. p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v4, v1, v2}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v10

    .line 293
    .local v10, "blockingResult":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Ljava/lang/Boolean;>;"
    const-string v22, "Email"

    const-string v23, "MySingle account deleted in AccountManager; deleting eas account from AccountManager:  "

    invoke-static/range {v22 .. v23}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 305
    .end local v4    # "accountManagerAccount":Landroid/accounts/Account;
    .end local v10    # "blockingResult":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Ljava/lang/Boolean;>;"
    :cond_6
    :goto_6
    :try_start_2
    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/utility/Utility;->isSamsungAccount(Landroid/content/Context;J)Z

    move-result v22

    if-eqz v22, :cond_7

    .line 306
    sget-object v22, Lcom/android/emailcommon/provider/EmailContent$Account;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v22

    const/16 v23, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    move-object/from16 v3, v24

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 309
    const-string v22, "Email"

    const-string v23, "MySingle Account deleted in AccountManager; deleting eas account  from provider: "

    invoke-static/range {v22 .. v23}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    const/4 v7, 0x1

    .line 313
    :cond_7
    new-instance v22, Lcom/android/emailcommon/service/AccountServiceProxy;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/emailcommon/service/AccountServiceProxy;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Account;->mId:J

    move-wide/from16 v24, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/android/emailcommon/service/AccountServiceProxy;->cancelAllNotifications(J)V

    .line 314
    if-eqz v16, :cond_1

    .line 315
    const/4 v12, 0x1

    goto/16 :goto_1

    .line 276
    .end local v16    # "isEasAccount":Z
    .end local v17    # "isSncAccount":Z
    :cond_8
    const/16 v17, 0x0

    goto/16 :goto_3

    .line 281
    .restart local v17    # "isSncAccount":Z
    :cond_9
    const/16 v16, 0x0

    goto/16 :goto_4

    .line 295
    .restart local v4    # "accountManagerAccount":Landroid/accounts/Account;
    .restart local v16    # "isEasAccount":Z
    :catch_0
    move-exception v21

    .line 296
    .local v21, "se":Ljava/lang/SecurityException;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/SecurityException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_6

    .line 318
    .end local v4    # "accountManagerAccount":Landroid/accounts/Account;
    .end local v16    # "isEasAccount":Z
    .end local v17    # "isSncAccount":Z
    .end local v21    # "se":Ljava/lang/SecurityException;
    :catch_1
    move-exception v11

    .line 319
    .local v11, "e":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 286
    .end local v11    # "e":Ljava/lang/Exception;
    .restart local v4    # "accountManagerAccount":Landroid/accounts/Account;
    .restart local v16    # "isEasAccount":Z
    .restart local v17    # "isSncAccount":Z
    :cond_a
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_5

    .end local v4    # "accountManagerAccount":Landroid/accounts/Account;
    .end local v9    # "arr$":[Landroid/accounts/Account;
    .end local v13    # "found":Z
    .end local v15    # "i$":I
    .end local v16    # "isEasAccount":Z
    .end local v17    # "isSncAccount":Z
    .end local v18    # "len$":I
    .end local v19    # "providerAccount":Lcom/android/emailcommon/provider/EmailContent$Account;
    .end local v20    # "providerAccountName":Ljava/lang/String;
    :cond_b
    move v8, v7

    .line 340
    .restart local v8    # "accountsDeleted":I
    goto/16 :goto_0
.end method
