.class public Lcom/android/emailcommon/utility/ConversionUtilities;
.super Ljava/lang/Object;
.source "ConversionUtilities.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static appendHtmlPart(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/lang/StringBuffer;
    .locals 0
    .param p0, "sb"    # Ljava/lang/StringBuffer;
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    .line 59
    if-nez p1, :cond_0

    .line 66
    :goto_0
    return-object p0

    .line 61
    :cond_0
    if-nez p0, :cond_1

    .line 62
    new-instance p0, Ljava/lang/StringBuffer;

    .end local p0    # "sb":Ljava/lang/StringBuffer;
    invoke-direct {p0, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .restart local p0    # "sb":Ljava/lang/StringBuffer;
    goto :goto_0

    .line 64
    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method private static appendTextPart(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/lang/StringBuffer;
    .locals 1
    .param p0, "sb"    # Ljava/lang/StringBuffer;
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    .line 45
    if-nez p1, :cond_0

    .line 55
    :goto_0
    return-object p0

    .line 47
    :cond_0
    if-nez p0, :cond_1

    .line 48
    new-instance p0, Ljava/lang/StringBuffer;

    .end local p0    # "sb":Ljava/lang/StringBuffer;
    invoke-direct {p0, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .restart local p0    # "sb":Ljava/lang/StringBuffer;
    goto :goto_0

    .line 50
    :cond_1
    invoke-virtual {p0}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 51
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 53
    :cond_2
    invoke-virtual {p0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public static cutText(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;
    .locals 3
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "length"    # I
    .param p2, "overflowMsg"    # Ljava/lang/String;
    .param p3, "isHtml"    # Z

    .prologue
    const/4 v2, 0x0

    .line 75
    if-eqz p0, :cond_0

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-gt v1, p1, :cond_1

    :cond_0
    move-object v0, p0

    .line 84
    :goto_0
    return-object v0

    .line 78
    :cond_1
    const/4 v0, 0x0

    .line 79
    .local v0, "resultText":Ljava/lang/String;
    if-eqz p3, :cond_2

    .line 80
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "<BR><BR>...<BR><BR>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "<BR>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 82
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v2, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\r\n\r...\n\r\n\r"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n\r"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static updateBodyFields(Ljava/lang/String;Lcom/android/emailcommon/provider/EmailContent$Body;Lcom/android/emailcommon/provider/EmailContent$Message;Ljava/util/ArrayList;)Z
    .locals 24
    .param p0, "overflowMsg"    # Ljava/lang/String;
    .param p1, "body"    # Lcom/android/emailcommon/provider/EmailContent$Body;
    .param p2, "localMessage"    # Lcom/android/emailcommon/provider/EmailContent$Message;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/android/emailcommon/provider/EmailContent$Body;",
            "Lcom/android/emailcommon/provider/EmailContent$Message;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/mail/Part;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 97
    .local p3, "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mId:J

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, p1

    iput-wide v0, v2, Lcom/android/emailcommon/provider/EmailContent$Body;->mMessageKey:J

    .line 99
    const/4 v13, 0x0

    .line 100
    .local v13, "sbHtml":Ljava/lang/StringBuffer;
    const/16 v16, 0x0

    .line 101
    .local v16, "sbText":Ljava/lang/StringBuffer;
    const/4 v14, 0x0

    .line 102
    .local v14, "sbHtmlReply":Ljava/lang/StringBuffer;
    const/16 v17, 0x0

    .line 103
    .local v17, "sbTextReply":Ljava/lang/StringBuffer;
    const/4 v15, 0x0

    .line 105
    .local v15, "sbIntroText":Ljava/lang/StringBuffer;
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/android/emailcommon/mail/Part;

    .line 106
    .local v20, "viewable":Lcom/android/emailcommon/mail/Part;
    const/16 v18, 0x0

    .line 107
    .local v18, "text":Ljava/lang/String;
    const-string v21, "X-Android-Body-Quoted-Part"

    invoke-interface/range {v20 .. v21}, Lcom/android/emailcommon/mail/Part;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 108
    .local v12, "replyTags":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 109
    .local v11, "replyTag":Ljava/lang/String;
    if-eqz v12, :cond_0

    array-length v0, v12

    move/from16 v21, v0

    if-lez v21, :cond_0

    .line 110
    const/16 v21, 0x0

    aget-object v11, v12, v21

    .line 113
    :cond_0
    const-string v21, "text/html"

    invoke-interface/range {v20 .. v20}, Lcom/android/emailcommon/mail/Part;->getMimeType()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    .line 116
    .local v7, "isHtml":Z
    :try_start_0
    invoke-static/range {v20 .. v20}, Lcom/android/emailcommon/internet/MimeUtility;->getTextFromPart(Lcom/android/emailcommon/mail/Part;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v18

    .line 121
    :goto_1
    if-eqz v11, :cond_5

    .line 122
    const-string v21, "quoted-reply"

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    .line 123
    .local v10, "isQuotedReply":Z
    const-string v21, "quoted-forward"

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    .line 124
    .local v8, "isQuotedForward":Z
    const-string v21, "quoted-intro"

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    .line 126
    .local v9, "isQuotedIntro":Z
    if-nez v10, :cond_1

    if-eqz v8, :cond_4

    .line 127
    :cond_1
    if-eqz v7, :cond_2

    .line 128
    move-object/from16 v0, v18

    invoke-static {v14, v0}, Lcom/android/emailcommon/utility/ConversionUtilities;->appendHtmlPart(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v14

    .line 133
    :goto_2
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    move/from16 v21, v0

    and-int/lit8 v21, v21, -0x4

    move/from16 v0, v21

    move-object/from16 v1, p2

    iput v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    .line 134
    move-object/from16 v0, p2

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    move/from16 v22, v0

    if-eqz v10, :cond_3

    const/16 v21, 0x1

    :goto_3
    or-int v21, v21, v22

    move/from16 v0, v21

    move-object/from16 v1, p2

    iput v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mFlags:I

    goto :goto_0

    .line 117
    .end local v8    # "isQuotedForward":Z
    .end local v9    # "isQuotedIntro":Z
    .end local v10    # "isQuotedReply":Z
    :catch_0
    move-exception v5

    .line 118
    .local v5, "e":Ljava/lang/Exception;
    const-string v21, "updateBodyFields"

    move-object/from16 v0, v21

    invoke-static {v0, v5}, Lcom/android/emailcommon/utility/EmailLog;->dumpException(Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 130
    .end local v5    # "e":Ljava/lang/Exception;
    .restart local v8    # "isQuotedForward":Z
    .restart local v9    # "isQuotedIntro":Z
    .restart local v10    # "isQuotedReply":Z
    :cond_2
    invoke-static/range {v17 .. v18}, Lcom/android/emailcommon/utility/ConversionUtilities;->appendTextPart(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v17

    goto :goto_2

    .line 134
    :cond_3
    const/16 v21, 0x2

    goto :goto_3

    .line 138
    :cond_4
    if-eqz v9, :cond_5

    .line 139
    move-object/from16 v0, v18

    invoke-static {v15, v0}, Lcom/android/emailcommon/utility/ConversionUtilities;->appendTextPart(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v15

    .line 140
    goto/16 :goto_0

    .line 145
    .end local v8    # "isQuotedForward":Z
    .end local v9    # "isQuotedIntro":Z
    .end local v10    # "isQuotedReply":Z
    :cond_5
    if-eqz v7, :cond_6

    .line 146
    move-object/from16 v0, v18

    invoke-static {v13, v0}, Lcom/android/emailcommon/utility/ConversionUtilities;->appendHtmlPart(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    goto/16 :goto_0

    .line 148
    :cond_6
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/android/emailcommon/utility/ConversionUtilities;->appendTextPart(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v16

    goto/16 :goto_0

    .line 152
    .end local v7    # "isHtml":Z
    .end local v11    # "replyTag":Ljava/lang/String;
    .end local v12    # "replyTags":[Ljava/lang/String;
    .end local v18    # "text":Ljava/lang/String;
    .end local v20    # "viewable":Lcom/android/emailcommon/mail/Part;
    :cond_7
    const v4, 0x32000

    .line 154
    .local v4, "availableReplyIntroLength":I
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_a

    .line 155
    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    .line 156
    .restart local v18    # "text":Ljava/lang/String;
    move-object/from16 v0, v18

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    .line 157
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    sub-int v4, v4, v21

    .line 158
    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isFullMessageBodyLoadDisabled()Z

    move-result v21

    if-nez v21, :cond_8

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlContent:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    const v22, 0x32000

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_8

    .line 159
    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    move/from16 v21, v0

    or-int/lit8 v21, v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p1

    iput v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    .line 161
    :cond_8
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    move-object/from16 v21, v0

    if-eqz v21, :cond_9

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 162
    :cond_9
    const-string v21, "</DIV>|</Div>|</div>"

    const-string v22, " </DIV>"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 163
    .local v19, "textForSnippet":Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/android/emailcommon/mail/Snippet;->fromHtmlText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    .line 168
    .end local v18    # "text":Ljava/lang/String;
    .end local v19    # "textForSnippet":Ljava/lang/String;
    :cond_a
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-eqz v21, :cond_d

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_d

    .line 169
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    .line 170
    .restart local v18    # "text":Ljava/lang/String;
    move-object/from16 v0, v18

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    .line 171
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    sub-int v4, v4, v21

    .line 172
    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isFullMessageBodyLoadDisabled()Z

    move-result v21

    if-nez v21, :cond_b

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextContent:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    const v22, 0x32000

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_b

    .line 173
    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    move/from16 v21, v0

    or-int/lit8 v21, v21, 0x2

    move/from16 v0, v21

    move-object/from16 v1, p1

    iput v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    .line 175
    :cond_b
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    move-object/from16 v21, v0

    if-eqz v21, :cond_c

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_c

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "&"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_d

    .line 176
    :cond_c
    invoke-static/range {v18 .. v18}, Lcom/android/emailcommon/mail/Snippet;->fromPlainText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    .line 179
    .end local v18    # "text":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    move-object/from16 v21, v0

    if-nez v21, :cond_e

    .line 180
    const-string v21, ""

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Message;->mSnippet:Ljava/lang/String;

    .line 183
    :cond_e
    const-string v21, "updateBodyFields"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "availableReplyLength = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    if-eqz v14, :cond_10

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->length()I

    move-result v21

    if-eqz v21, :cond_10

    .line 186
    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlReply:Ljava/lang/String;

    .line 187
    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isFullMessageBodyLoadDisabled()Z

    move-result v21

    if-nez v21, :cond_f

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlReply:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    if-le v0, v4, :cond_f

    .line 188
    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    move/from16 v21, v0

    or-int/lit8 v21, v21, 0x4

    move/from16 v0, v21

    move-object/from16 v1, p1

    iput v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    .line 190
    :cond_f
    if-lez v4, :cond_10

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mHtmlReply:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    sub-int v4, v4, v21

    .line 192
    :cond_10
    if-lez v4, :cond_12

    if-eqz v17, :cond_12

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->length()I

    move-result v21

    if-eqz v21, :cond_12

    .line 193
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextReply:Ljava/lang/String;

    .line 194
    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isFullMessageBodyLoadDisabled()Z

    move-result v21

    if-nez v21, :cond_11

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextReply:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    if-le v0, v4, :cond_11

    .line 195
    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    move/from16 v21, v0

    or-int/lit8 v21, v21, 0x8

    move/from16 v0, v21

    move-object/from16 v1, p1

    iput v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    .line 197
    :cond_11
    if-lez v4, :cond_12

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mTextReply:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    sub-int v4, v4, v21

    .line 199
    :cond_12
    if-lez v4, :cond_13

    if-eqz v15, :cond_13

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->length()I

    move-result v21

    if-eqz v21, :cond_13

    .line 200
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    iput-object v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mIntroText:Ljava/lang/String;

    .line 201
    invoke-static {}, Lcom/android/emailcommon/utility/Utility;->isFullMessageBodyLoadDisabled()Z

    move-result v21

    if-nez v21, :cond_13

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mIntroText:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    if-le v0, v4, :cond_13

    .line 202
    move-object/from16 v0, p1

    iget v0, v0, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    move/from16 v21, v0

    or-int/lit8 v21, v21, 0x10

    move/from16 v0, v21

    move-object/from16 v1, p1

    iput v0, v1, Lcom/android/emailcommon/provider/EmailContent$Body;->mFileSaveFlags:I

    .line 205
    :cond_13
    const/16 v21, 0x1

    return v21
.end method
