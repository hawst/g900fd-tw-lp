.class public Lcom/android/emailcommon/utility/TimeLap;
.super Ljava/lang/Object;
.source "TimeLap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/emailcommon/utility/TimeLap$TimeLapData;
    }
.end annotation


# static fields
.field public static ID_INNER_CONTAINER:I

.field public static ID_LAUNCH:I

.field public static ID_LISTVIEW:I

.field public static ID_OUTER_CONTAINER:I

.field public static LAPTIME_ID_DECRYPTION:I

.field public static LAPTIME_ID_SEND_REVOCATION:I

.field private static _insts:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/android/emailcommon/utility/TimeLap$TimeLapData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x1

    sput v0, Lcom/android/emailcommon/utility/TimeLap;->LAPTIME_ID_DECRYPTION:I

    .line 10
    const/4 v0, 0x2

    sput v0, Lcom/android/emailcommon/utility/TimeLap;->ID_LISTVIEW:I

    .line 11
    const/4 v0, 0x3

    sput v0, Lcom/android/emailcommon/utility/TimeLap;->ID_OUTER_CONTAINER:I

    .line 12
    const/4 v0, 0x4

    sput v0, Lcom/android/emailcommon/utility/TimeLap;->ID_INNER_CONTAINER:I

    .line 13
    const/4 v0, 0x5

    sput v0, Lcom/android/emailcommon/utility/TimeLap;->ID_LAUNCH:I

    .line 15
    const/4 v0, 0x6

    sput v0, Lcom/android/emailcommon/utility/TimeLap;->LAPTIME_ID_SEND_REVOCATION:I

    .line 16
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/android/emailcommon/utility/TimeLap;->_insts:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method
