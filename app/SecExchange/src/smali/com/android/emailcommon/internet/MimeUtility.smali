.class public Lcom/android/emailcommon/internet/MimeUtility;
.super Ljava/lang/Object;
.source "MimeUtility.java"


# static fields
.field private static final PATTERN_CR_OR_LF:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-string v0, "\r|\n"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/android/emailcommon/internet/MimeUtility;->PATTERN_CR_OR_LF:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static collectParts(Lcom/android/emailcommon/mail/Part;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 24
    .param p0, "part"    # Lcom/android/emailcommon/mail/Part;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/emailcommon/mail/Part;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/mail/Part;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/emailcommon/mail/Part;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 590
    .local p1, "viewables":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    .local p2, "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/android/emailcommon/mail/Part;>;"
    invoke-interface/range {p0 .. p0}, Lcom/android/emailcommon/mail/Part;->getDisposition()Ljava/lang/String;

    move-result-object v10

    .line 591
    .local v10, "disposition":Ljava/lang/String;
    const/4 v12, 0x0

    .line 592
    .local v12, "dispositionType":Ljava/lang/String;
    const/4 v11, 0x0

    .line 593
    .local v11, "dispositionFilename":Ljava/lang/String;
    if-eqz v10, :cond_0

    .line 594
    const/16 v22, 0x0

    move-object/from16 v0, v22

    invoke-static {v10, v0}, Lcom/android/emailcommon/internet/MimeUtility;->getHeaderParameter(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 595
    const-string v22, "filename"

    move-object/from16 v0, v22

    invoke-static {v10, v0}, Lcom/android/emailcommon/internet/MimeUtility;->getHeaderParameter(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 602
    :cond_0
    if-nez v11, :cond_1

    .line 603
    invoke-interface/range {p0 .. p0}, Lcom/android/emailcommon/mail/Part;->getContentType()Ljava/lang/String;

    move-result-object v8

    .line 604
    .local v8, "contentType":Ljava/lang/String;
    const-string v22, "name"

    move-object/from16 v0, v22

    invoke-static {v8, v0}, Lcom/android/emailcommon/internet/MimeUtility;->getHeaderParameter(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 606
    .end local v8    # "contentType":Ljava/lang/String;
    :cond_1
    const-string v22, "attachment"

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    .line 608
    .local v5, "attachmentDisposition":Z
    if-eqz v12, :cond_2

    const-string v22, "inline"

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_5

    :cond_2
    const/16 v16, 0x1

    .line 612
    .local v16, "inlineDisposition":Z
    :goto_0
    if-nez v5, :cond_3

    if-eqz v11, :cond_6

    if-nez v16, :cond_6

    :cond_3
    const/4 v4, 0x1

    .line 616
    .local v4, "attachment":Z
    :goto_1
    if-eqz v16, :cond_7

    if-eqz v11, :cond_7

    const/4 v15, 0x1

    .line 619
    .local v15, "inline":Z
    :goto_2
    if-nez v4, :cond_4

    if-eqz v15, :cond_8

    :cond_4
    const/4 v6, 0x1

    .line 621
    .local v6, "attachmentOrInline":Z
    :goto_3
    invoke-interface/range {p0 .. p0}, Lcom/android/emailcommon/mail/Part;->getBody()Lcom/android/emailcommon/mail/Body;

    move-result-object v22

    move-object/from16 v0, v22

    instance-of v0, v0, Lcom/android/emailcommon/mail/Multipart;

    move/from16 v22, v0

    if-eqz v22, :cond_9

    .line 625
    invoke-interface/range {p0 .. p0}, Lcom/android/emailcommon/mail/Part;->getBody()Lcom/android/emailcommon/mail/Body;

    move-result-object v19

    check-cast v19, Lcom/android/emailcommon/mail/Multipart;

    .line 626
    .local v19, "mp":Lcom/android/emailcommon/mail/Multipart;
    invoke-virtual/range {v19 .. v19}, Lcom/android/emailcommon/mail/Multipart;->getCount()I

    move-result v9

    .line 627
    .local v9, "count":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_4
    if-ge v13, v9, :cond_a

    .line 628
    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Lcom/android/emailcommon/mail/Multipart;->getBodyPart(I)Lcom/android/emailcommon/mail/BodyPart;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/MimeUtility;->collectParts(Lcom/android/emailcommon/mail/Part;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 627
    add-int/lit8 v13, v13, 0x1

    goto :goto_4

    .line 608
    .end local v4    # "attachment":Z
    .end local v6    # "attachmentOrInline":Z
    .end local v9    # "count":I
    .end local v13    # "i":I
    .end local v15    # "inline":Z
    .end local v16    # "inlineDisposition":Z
    .end local v19    # "mp":Lcom/android/emailcommon/mail/Multipart;
    :cond_5
    const/16 v16, 0x0

    goto :goto_0

    .line 612
    .restart local v16    # "inlineDisposition":Z
    :cond_6
    const/4 v4, 0x0

    goto :goto_1

    .line 616
    .restart local v4    # "attachment":Z
    :cond_7
    const/4 v15, 0x0

    goto :goto_2

    .line 619
    .restart local v15    # "inline":Z
    :cond_8
    const/4 v6, 0x0

    goto :goto_3

    .line 630
    .restart local v6    # "attachmentOrInline":Z
    :cond_9
    invoke-interface/range {p0 .. p0}, Lcom/android/emailcommon/mail/Part;->getBody()Lcom/android/emailcommon/mail/Body;

    move-result-object v22

    move-object/from16 v0, v22

    instance-of v0, v0, Lcom/android/emailcommon/mail/Message;

    move/from16 v22, v0

    if-eqz v22, :cond_b

    .line 633
    invoke-interface/range {p0 .. p0}, Lcom/android/emailcommon/mail/Part;->getBody()Lcom/android/emailcommon/mail/Body;

    move-result-object v18

    check-cast v18, Lcom/android/emailcommon/mail/Message;

    .line 634
    .local v18, "message":Lcom/android/emailcommon/mail/Message;
    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lcom/android/emailcommon/internet/MimeUtility;->collectParts(Lcom/android/emailcommon/mail/Part;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 675
    .end local v18    # "message":Lcom/android/emailcommon/mail/Message;
    :cond_a
    :goto_5
    return-void

    .line 635
    :cond_b
    if-nez v6, :cond_c

    const-string v22, "text/html"

    invoke-interface/range {p0 .. p0}, Lcom/android/emailcommon/mail/Part;->getMimeType()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_c

    .line 638
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 639
    :cond_c
    if-nez v6, :cond_d

    const-string v22, "text/plain"

    invoke-interface/range {p0 .. p0}, Lcom/android/emailcommon/mail/Part;->getMimeType()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_d

    .line 642
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 644
    :cond_d
    invoke-interface/range {p0 .. p0}, Lcom/android/emailcommon/mail/Part;->getMimeType()Ljava/lang/String;

    move-result-object v22

    if-eqz v22, :cond_11

    if-nez v6, :cond_11

    invoke-interface/range {p0 .. p0}, Lcom/android/emailcommon/mail/Part;->getMimeType()Ljava/lang/String;

    move-result-object v22

    const-string v23, "text/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v22

    if-eqz v22, :cond_11

    const-string v22, "text/calendar"

    invoke-interface/range {p0 .. p0}, Lcom/android/emailcommon/mail/Part;->getMimeType()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    if-nez v22, :cond_11

    .line 648
    const-string v22, "Content-Type"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Lcom/android/emailcommon/mail/Part;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 649
    .local v20, "strMime":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 651
    .local v7, "bAtt":Z
    if-nez v20, :cond_f

    .line 652
    :try_start_0
    new-instance v22, Ljava/lang/Exception;

    invoke-direct/range {v22 .. v22}, Ljava/lang/Exception;-><init>()V

    throw v22
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 660
    :catch_0
    move-exception v22

    .line 662
    :cond_e
    :goto_6
    if-eqz v7, :cond_a

    .line 663
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 654
    :cond_f
    move-object/from16 v3, v20

    .local v3, "arr$":[Ljava/lang/String;
    :try_start_1
    array-length v0, v3

    move/from16 v17, v0

    .local v17, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_7
    move/from16 v0, v17

    if-ge v14, v0, :cond_e

    aget-object v21, v3, v14

    .line 655
    .local v21, "tempMime":Ljava/lang/String;
    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v22

    const-string v23, "name"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v22

    if-eqz v22, :cond_10

    .line 656
    const/4 v7, 0x1

    .line 657
    goto :goto_6

    .line 654
    :cond_10
    add-int/lit8 v14, v14, 0x1

    goto :goto_7

    .line 673
    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v7    # "bAtt":Z
    .end local v14    # "i$":I
    .end local v17    # "len$":I
    .end local v20    # "strMime":[Ljava/lang/String;
    .end local v21    # "tempMime":Ljava/lang/String;
    :cond_11
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5
.end method

.method public static decode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 65
    if-nez p0, :cond_0

    .line 66
    const/4 v0, 0x0

    .line 72
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lorg/apache/james/mime4j/codec/DecoderUtil;->decodeGeneric(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static decodeBody(Ljava/io/InputStream;Ljava/lang/String;)Lcom/android/emailcommon/mail/Body;
    .locals 5
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "contentTransferEncoding"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 556
    invoke-static {p0, p1}, Lcom/android/emailcommon/internet/MimeUtility;->getInputStreamForContentTransferEncoding(Ljava/io/InputStream;Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object p0

    .line 557
    new-instance v2, Lcom/android/emailcommon/internet/BinaryTempFileBody;

    invoke-direct {v2}, Lcom/android/emailcommon/internet/BinaryTempFileBody;-><init>()V

    .line 558
    .local v2, "tempBody":Lcom/android/emailcommon/internet/BinaryTempFileBody;
    invoke-virtual {v2}, Lcom/android/emailcommon/internet/BinaryTempFileBody;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    .line 560
    .local v1, "out":Ljava/io/OutputStream;
    :try_start_0
    invoke-static {p0, v1}, Lorg/apache/commons/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 570
    if-eqz v1, :cond_0

    .line 571
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 574
    :cond_0
    :goto_0
    return-object v2

    .line 563
    :catch_0
    move-exception v0

    .line 564
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    new-instance v3, Ljava/io/IOException;

    const-string v4, "PARSE_CANCEL"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 570
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 571
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_1
    throw v3

    .line 565
    :catch_1
    move-exception v3

    .line 570
    if-eqz v1, :cond_0

    .line 571
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    goto :goto_0
.end method

.method public static fold(Ljava/lang/String;I)Ljava/lang/String;
    .locals 9
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "usedCharacters"    # I

    .prologue
    const/16 v8, 0x4c

    const/4 v7, 0x0

    .line 138
    const/16 v2, 0x4c

    .line 140
    .local v2, "maxCharacters":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 141
    .local v1, "length":I
    add-int v6, p1, v1

    if-gt v6, v8, :cond_0

    .line 151
    .end local p0    # "s":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 144
    .restart local p0    # "s":Ljava/lang/String;
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 146
    .local v4, "sb":Ljava/lang/StringBuilder;
    neg-int v0, p1

    .line 147
    .local v0, "lastLineBreak":I
    invoke-static {p0, v7}, Lcom/android/emailcommon/internet/MimeUtility;->indexOfWsp(Ljava/lang/String;I)I

    move-result v5

    .line 149
    .local v5, "wspIdx":I
    :goto_1
    if-ne v5, v1, :cond_1

    .line 150
    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-virtual {p0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 154
    :cond_1
    add-int/lit8 v6, v5, 0x1

    invoke-static {p0, v6}, Lcom/android/emailcommon/internet/MimeUtility;->indexOfWsp(Ljava/lang/String;I)I

    move-result v3

    .line 156
    .local v3, "nextWspIdx":I
    sub-int v6, v3, v0

    if-le v6, v8, :cond_2

    .line 157
    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-virtual {p0, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    const-string v6, "\r\n"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    move v0, v5

    .line 162
    :cond_2
    move v5, v3

    .line 163
    goto :goto_1
.end method

.method public static foldAndEncode2(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "usedCharacters"    # I

    .prologue
    .line 115
    sget-object v1, Lorg/apache/james/mime4j/codec/EncoderUtil$Usage;->TEXT_TOKEN:Lorg/apache/james/mime4j/codec/EncoderUtil$Usage;

    invoke-static {p0, v1, p1}, Lorg/apache/james/mime4j/codec/EncoderUtil;->encodeIfNecessary(Ljava/lang/String;Lorg/apache/james/mime4j/codec/EncoderUtil$Usage;I)Ljava/lang/String;

    move-result-object v0

    .line 118
    .local v0, "encoded":Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/android/emailcommon/internet/MimeUtility;->fold(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getHeaderParameter(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p0, "header"    # Ljava/lang/String;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 198
    if-nez p0, :cond_1

    .line 224
    :cond_0
    :goto_0
    return-object v4

    .line 201
    :cond_1
    invoke-static {p0}, Lcom/android/emailcommon/internet/MimeUtility;->unfold(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "(;)(?=(?:[^\"]|\"[^\"]*\")*$)"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 202
    .local v7, "parts":[Ljava/lang/String;
    if-nez p1, :cond_2

    .line 203
    aget-object v8, v7, v10

    if-eqz v8, :cond_0

    aget-object v8, v7, v10

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_0

    .line 206
    aget-object v8, v7, v10

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 208
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 209
    .local v3, "lowerCaseName":Ljava/lang/String;
    move-object v0, v7

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v6, v0, v1

    .line 210
    .local v6, "part":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 211
    const-string v8, "="

    invoke-virtual {v6, v8, v12}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v5

    .line 212
    .local v5, "parameterParts":[Ljava/lang/String;
    array-length v8, v5

    if-lt v8, v12, :cond_0

    .line 215
    aget-object v8, v5, v11

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 216
    .local v4, "parameter":Ljava/lang/String;
    const-string v8, "\""

    invoke-virtual {v4, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v8, "\""

    invoke-virtual {v4, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    if-le v8, v11, :cond_0

    .line 218
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v4, v11, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 209
    .end local v4    # "parameter":Ljava/lang/String;
    .end local v5    # "parameterParts":[Ljava/lang/String;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static getInputStreamForContentTransferEncoding(Ljava/io/InputStream;Ljava/lang/String;)Ljava/io/InputStream;
    .locals 2
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "contentTransferEncoding"    # Ljava/lang/String;

    .prologue
    .line 535
    if-eqz p1, :cond_0

    .line 536
    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/android/emailcommon/internet/MimeUtility;->getHeaderParameter(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 537
    const-string v1, "quoted-printable"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 538
    new-instance v0, Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;

    invoke-direct {v0, p0}, Lorg/apache/james/mime4j/codec/QuotedPrintableInputStream;-><init>(Ljava/io/InputStream;)V

    .end local p0    # "in":Ljava/io/InputStream;
    .local v0, "in":Ljava/io/InputStream;
    move-object p0, v0

    .line 545
    .end local v0    # "in":Ljava/io/InputStream;
    .restart local p0    # "in":Ljava/io/InputStream;
    :cond_0
    :goto_0
    return-object p0

    .line 539
    :cond_1
    const-string v1, "base64"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 542
    new-instance v0, Lorg/apache/james/mime4j/codec/Base64InputStream;

    invoke-direct {v0, p0}, Lorg/apache/james/mime4j/codec/Base64InputStream;-><init>(Ljava/io/InputStream;)V

    .end local p0    # "in":Ljava/io/InputStream;
    .restart local v0    # "in":Ljava/io/InputStream;
    move-object p0, v0

    .end local v0    # "in":Ljava/io/InputStream;
    .restart local p0    # "in":Ljava/io/InputStream;
    goto :goto_0
.end method

.method public static getTextFromPart(Lcom/android/emailcommon/mail/Part;)Ljava/lang/String;
    .locals 15
    .param p0, "part"    # Lcom/android/emailcommon/mail/Part;

    .prologue
    const/4 v14, 0x1

    .line 271
    const/4 v3, 0x0

    .line 273
    .local v3, "in":Ljava/io/InputStream;
    if-eqz p0, :cond_e

    :try_start_0
    invoke-interface {p0}, Lcom/android/emailcommon/mail/Part;->getBody()Lcom/android/emailcommon/mail/Body;

    move-result-object v12

    if-eqz v12, :cond_e

    .line 274
    invoke-interface {p0}, Lcom/android/emailcommon/mail/Part;->getBody()Lcom/android/emailcommon/mail/Body;

    move-result-object v12

    invoke-interface {v12}, Lcom/android/emailcommon/mail/Body;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    .line 275
    invoke-interface {p0}, Lcom/android/emailcommon/mail/Part;->getMimeType()Ljava/lang/String;

    move-result-object v6

    .line 276
    .local v6, "mimeType":Ljava/lang/String;
    if-eqz v6, :cond_e

    const-string v12, "text/*"

    invoke-static {v6, v12}, Lcom/android/emailcommon/internet/MimeUtility;->mimeTypeMatches(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 282
    new-instance v8, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 283
    .local v8, "out":Ljava/io/ByteArrayOutputStream;
    if-eqz v3, :cond_0

    .line 285
    invoke-static {v3, v8}, Lorg/apache/commons/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 286
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 288
    :cond_0
    const/4 v3, 0x0

    .line 295
    invoke-interface {p0}, Lcom/android/emailcommon/mail/Part;->getContentType()Ljava/lang/String;

    move-result-object v12

    const-string v13, "charset"

    invoke-static {v12, v13}, Lcom/android/emailcommon/internet/MimeUtility;->getHeaderParameter(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 296
    .local v0, "charset":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v12

    if-eqz v12, :cond_1

    const-string v12, ""

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 301
    invoke-static {v0}, Lorg/apache/james/mime4j/util/CharsetUtil;->toJavaCharset(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 304
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v12

    if-eqz v12, :cond_2

    const-string v12, ""

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 305
    :cond_2
    const-string v9, ""
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    .local v9, "outChar":Ljava/lang/String;
    :try_start_1
    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v11

    .line 308
    .local v11, "tempStr":Ljava/lang/String;
    const-string v12, "charset"

    invoke-virtual {v11, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 309
    .local v4, "index":I
    if-le v4, v14, :cond_5

    .line 310
    add-int/lit8 v2, v4, 0x1e

    .line 311
    .local v2, "end":I
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v12

    if-ge v12, v2, :cond_3

    .line 312
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v2

    .line 313
    :cond_3
    invoke-virtual {v11, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const-string v13, "="

    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 314
    .local v5, "listChar":[Ljava/lang/String;
    array-length v12, v5

    if-le v12, v14, :cond_d

    .line 315
    const/4 v12, 0x1

    aget-object v12, v5, v12

    const-string v13, "\""

    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    aget-object v9, v12, v13

    .line 320
    :goto_0
    const-string v12, "(?i)3D"

    const-string v13, ""

    invoke-virtual {v9, v12, v13}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 321
    const-string v12, "22"

    const-string v13, ""

    invoke-virtual {v9, v12, v13}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 322
    const-string v12, " "

    invoke-virtual {v9, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 323
    const-string v12, " "

    invoke-virtual {v9, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    aget-object v9, v12, v13

    .line 325
    :cond_4
    invoke-static {v9}, Lorg/apache/james/mime4j/util/CharsetUtil;->toJavaCharset(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 334
    .end local v2    # "end":I
    .end local v4    # "index":I
    .end local v5    # "listChar":[Ljava/lang/String;
    .end local v9    # "outChar":Ljava/lang/String;
    .end local v11    # "tempStr":Ljava/lang/String;
    :cond_5
    :goto_1
    if-eqz v0, :cond_6

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v12

    if-eqz v12, :cond_6

    const-string v12, ""

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 335
    :cond_6
    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v12

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v13

    invoke-static {v12, v13}, Lorg/apache/james/mime4j/codec/DecoderUtil;->charsetDetect([BI)Ljava/lang/String;

    move-result-object v0

    .line 336
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v12

    if-eqz v12, :cond_7

    const-string v12, ""

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_7

    .line 341
    invoke-static {v0}, Lorg/apache/james/mime4j/util/CharsetUtil;->toJavaCharset(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 343
    :cond_7
    const-string v12, "MimeUtility"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Detected charset from content: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :cond_8
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v12

    if-eqz v12, :cond_9

    const-string v12, ""

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 352
    :cond_9
    invoke-static {}, Lorg/apache/james/mime4j/util/CharsetUtil;->getLocalCharset()Ljava/lang/String;

    move-result-object v0

    .line 357
    :cond_a
    if-nez v0, :cond_b

    .line 358
    const-string v0, "ASCII"

    .line 363
    :cond_b
    invoke-virtual {v8, v0}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 364
    .local v10, "result":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 386
    if-eqz v3, :cond_c

    .line 387
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 391
    .end local v0    # "charset":Ljava/lang/String;
    .end local v6    # "mimeType":Ljava/lang/String;
    .end local v8    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v10    # "result":Ljava/lang/String;
    :cond_c
    :goto_2
    return-object v10

    .line 317
    .restart local v0    # "charset":Ljava/lang/String;
    .restart local v2    # "end":I
    .restart local v4    # "index":I
    .restart local v5    # "listChar":[Ljava/lang/String;
    .restart local v6    # "mimeType":Ljava/lang/String;
    .restart local v8    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local v9    # "outChar":Ljava/lang/String;
    .restart local v11    # "tempStr":Ljava/lang/String;
    :cond_d
    :try_start_4
    invoke-virtual {v11, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    const-string v13, ":"

    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    aget-object v9, v12, v13

    .line 318
    const-string v12, "\""

    invoke-virtual {v9, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    aget-object v9, v12, v13
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 386
    .end local v0    # "charset":Ljava/lang/String;
    .end local v2    # "end":I
    .end local v4    # "index":I
    .end local v5    # "listChar":[Ljava/lang/String;
    .end local v6    # "mimeType":Ljava/lang/String;
    .end local v8    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v9    # "outChar":Ljava/lang/String;
    .end local v11    # "tempStr":Ljava/lang/String;
    :cond_e
    if-eqz v3, :cond_f

    .line 387
    :try_start_5
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 391
    :cond_f
    :goto_3
    const/4 v10, 0x0

    goto :goto_2

    .line 369
    :catch_0
    move-exception v7

    .line 375
    .local v7, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_6
    const-string v12, "Email"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Unable to getTextFromPart "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v7}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 386
    if-eqz v3, :cond_f

    .line 387
    :try_start_7
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_3

    .line 388
    :catch_1
    move-exception v12

    goto :goto_3

    .line 376
    .end local v7    # "oom":Ljava/lang/OutOfMemoryError;
    :catch_2
    move-exception v1

    .line 382
    .local v1, "e":Ljava/lang/Exception;
    :try_start_8
    const-string v12, "Email"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Unable to getTextFromPart "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 386
    if-eqz v3, :cond_f

    .line 387
    :try_start_9
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    goto :goto_3

    .line 388
    :catch_3
    move-exception v12

    goto :goto_3

    .line 385
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v12

    .line 386
    if-eqz v3, :cond_10

    .line 387
    :try_start_a
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    .line 389
    :cond_10
    :goto_4
    throw v12

    .line 388
    .restart local v0    # "charset":Ljava/lang/String;
    .restart local v6    # "mimeType":Ljava/lang/String;
    .restart local v8    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local v10    # "result":Ljava/lang/String;
    :catch_4
    move-exception v12

    goto :goto_2

    .end local v0    # "charset":Ljava/lang/String;
    .end local v6    # "mimeType":Ljava/lang/String;
    .end local v8    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v10    # "result":Ljava/lang/String;
    :catch_5
    move-exception v12

    goto :goto_3

    :catch_6
    move-exception v13

    goto :goto_4

    .line 327
    .restart local v0    # "charset":Ljava/lang/String;
    .restart local v6    # "mimeType":Ljava/lang/String;
    .restart local v8    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local v9    # "outChar":Ljava/lang/String;
    :catch_7
    move-exception v12

    goto/16 :goto_1
.end method

.method public static getTextFromPart2(Lcom/android/emailcommon/mail/Part;Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "part"    # Lcom/android/emailcommon/mail/Part;
    .param p1, "contentEncoding"    # Ljava/lang/String;

    .prologue
    .line 399
    const/4 v2, 0x0

    .line 401
    .local v2, "in":Ljava/io/InputStream;
    if-eqz p0, :cond_7

    :try_start_0
    invoke-interface {p0}, Lcom/android/emailcommon/mail/Part;->getBody()Lcom/android/emailcommon/mail/Body;

    move-result-object v7

    if-eqz v7, :cond_7

    .line 402
    invoke-interface {p0}, Lcom/android/emailcommon/mail/Part;->getBody()Lcom/android/emailcommon/mail/Body;

    move-result-object v7

    invoke-interface {v7}, Lcom/android/emailcommon/mail/Body;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    .line 403
    invoke-interface {p0}, Lcom/android/emailcommon/mail/Part;->getMimeType()Ljava/lang/String;

    move-result-object v3

    .line 404
    .local v3, "mimeType":Ljava/lang/String;
    if-eqz v3, :cond_7

    const-string v7, "text/*"

    invoke-static {v3, v7}, Lcom/android/emailcommon/internet/MimeUtility;->mimeTypeMatches(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 410
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 411
    .local v5, "out":Ljava/io/ByteArrayOutputStream;
    if-eqz v2, :cond_0

    .line 413
    invoke-static {v2, v5}, Lorg/apache/commons/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 414
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 416
    :cond_0
    const/4 v2, 0x0

    .line 423
    invoke-interface {p0}, Lcom/android/emailcommon/mail/Part;->getContentType()Ljava/lang/String;

    move-result-object v7

    const-string v8, "charset"

    invoke-static {v7, v8}, Lcom/android/emailcommon/internet/MimeUtility;->getHeaderParameter(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 424
    .local v0, "charset":Ljava/lang/String;
    if-eqz v0, :cond_3

    const-string v7, "EUC-KR"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "EUC_KR"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 427
    :cond_1
    if-eqz p1, :cond_2

    if-eqz p1, :cond_3

    const-string v7, "8BIT"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 430
    :cond_2
    const-string v0, "UTF-8"

    .line 431
    const-string v7, "Email"

    const-string v8, "2003 account, text/html, EUC-KR and 8BIT conditions are TRUE"

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    :cond_3
    if-eqz v0, :cond_4

    .line 440
    invoke-static {v0}, Lorg/apache/james/mime4j/util/CharsetUtil;->toJavaCharset(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 445
    :cond_4
    if-nez v0, :cond_5

    .line 448
    invoke-static {}, Lorg/apache/james/mime4j/util/CharsetUtil;->getLocalCharset()Ljava/lang/String;

    move-result-object v0

    .line 453
    :cond_5
    invoke-virtual {v5, v0}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 454
    .local v6, "result":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 476
    if-eqz v2, :cond_6

    .line 477
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 481
    .end local v0    # "charset":Ljava/lang/String;
    .end local v3    # "mimeType":Ljava/lang/String;
    .end local v5    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v6    # "result":Ljava/lang/String;
    :cond_6
    :goto_0
    return-object v6

    .line 476
    :cond_7
    if-eqz v2, :cond_8

    .line 477
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 481
    :cond_8
    :goto_1
    const/4 v6, 0x0

    goto :goto_0

    .line 459
    :catch_0
    move-exception v4

    .line 465
    .local v4, "oom":Ljava/lang/OutOfMemoryError;
    :try_start_3
    const-string v7, "Email"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unable to getTextFromPart "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 476
    if-eqz v2, :cond_8

    .line 477
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 478
    :catch_1
    move-exception v7

    goto :goto_1

    .line 466
    .end local v4    # "oom":Ljava/lang/OutOfMemoryError;
    :catch_2
    move-exception v1

    .line 472
    .local v1, "e":Ljava/lang/Exception;
    :try_start_5
    const-string v7, "Email"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Unable to getTextFromPart "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/emailcommon/utility/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 476
    if-eqz v2, :cond_8

    .line 477
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_1

    .line 478
    :catch_3
    move-exception v7

    goto :goto_1

    .line 475
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    .line 476
    if-eqz v2, :cond_9

    .line 477
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 479
    :cond_9
    :goto_2
    throw v7

    .line 478
    .restart local v0    # "charset":Ljava/lang/String;
    .restart local v3    # "mimeType":Ljava/lang/String;
    .restart local v5    # "out":Ljava/io/ByteArrayOutputStream;
    .restart local v6    # "result":Ljava/lang/String;
    :catch_4
    move-exception v7

    goto :goto_0

    .end local v0    # "charset":Ljava/lang/String;
    .end local v3    # "mimeType":Ljava/lang/String;
    .end local v5    # "out":Ljava/io/ByteArrayOutputStream;
    .end local v6    # "result":Ljava/lang/String;
    :catch_5
    move-exception v7

    goto :goto_1

    :catch_6
    move-exception v8

    goto :goto_2
.end method

.method private static indexOfWsp(Ljava/lang/String;I)I
    .locals 4
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "fromIndex"    # I

    .prologue
    .line 171
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 172
    .local v2, "len":I
    move v1, p1

    .local v1, "index":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 173
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 174
    .local v0, "c":C
    const/16 v3, 0x20

    if-eq v0, v3, :cond_0

    const/16 v3, 0x9

    if-ne v0, v3, :cond_1

    .line 177
    .end local v0    # "c":C
    .end local v1    # "index":I
    :cond_0
    :goto_1
    return v1

    .line 172
    .restart local v0    # "c":C
    .restart local v1    # "index":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v0    # "c":C
    :cond_2
    move v1, v2

    .line 177
    goto :goto_1
.end method

.method public static isHTMLViewable(Lcom/android/emailcommon/mail/Part;)Z
    .locals 2
    .param p0, "viewable"    # Lcom/android/emailcommon/mail/Part;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 689
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/android/emailcommon/mail/Part;->getContentType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "text/html"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 690
    const/4 v0, 0x1

    .line 691
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPlainTextViewable(Lcom/android/emailcommon/mail/Part;)Z
    .locals 2
    .param p0, "viewable"    # Lcom/android/emailcommon/mail/Part;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/emailcommon/mail/MessagingException;
        }
    .end annotation

    .prologue
    .line 695
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/android/emailcommon/mail/Part;->getContentType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 696
    const/4 v0, 0x1

    .line 697
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static mimeTypeMatches(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p0, "mimeType"    # Ljava/lang/String;
    .param p1, "matchAgainst"    # Ljava/lang/String;

    .prologue
    .line 499
    if-nez p0, :cond_0

    .line 500
    const/4 v1, 0x0

    .line 503
    :goto_0
    return v1

    .line 501
    :cond_0
    const-string v1, "\\*"

    const-string v2, "\\.\\*"

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v1, v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 503
    .local v0, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    goto :goto_0
.end method

.method public static unfold(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 53
    if-nez p0, :cond_1

    .line 54
    const/4 p0, 0x0

    .line 61
    .local v0, "patternMatcher":Ljava/util/regex/Matcher;
    :cond_0
    :goto_0
    return-object p0

    .line 56
    .end local v0    # "patternMatcher":Ljava/util/regex/Matcher;
    :cond_1
    sget-object v1, Lcom/android/emailcommon/internet/MimeUtility;->PATTERN_CR_OR_LF:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 57
    .restart local v0    # "patternMatcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->reset()Ljava/util/regex/Matcher;

    .line 59
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static unfoldAndDecode(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 77
    invoke-static {p0}, Lcom/android/emailcommon/internet/MimeUtility;->unfold(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/emailcommon/internet/MimeUtility;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
