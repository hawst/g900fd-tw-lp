.class Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;
.super Ljava/lang/Object;
.source "MimeMessage.java"

# interfaces
.implements Lorg/apache/james/mime4j/parser/ContentHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/emailcommon/internet/MimeMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MimeMessageBuilder"
.end annotation


# instance fields
.field private stack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/emailcommon/internet/MimeMessage;


# direct methods
.method public constructor <init>(Lcom/android/emailcommon/internet/MimeMessage;)V
    .locals 1

    .prologue
    .line 727
    iput-object p1, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->this$0:Lcom/android/emailcommon/internet/MimeMessage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 725
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    .line 728
    return-void
.end method

.method private convert2utf8(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "str"    # Ljava/lang/String;
    .param p2, "charset"    # Ljava/lang/String;

    .prologue
    .line 752
    const/4 v0, 0x0

    .line 755
    .local v0, "after":Ljava/lang/String;
    :try_start_0
    const-string v7, "8859_1"

    invoke-virtual {p1, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    .line 756
    .local v2, "b":[B
    invoke-static {p2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-virtual {v7}, Ljava/nio/charset/Charset;->newDecoder()Ljava/nio/charset/CharsetDecoder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 758
    .local v3, "decoder":Ljava/nio/charset/CharsetDecoder;
    :try_start_1
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/nio/charset/CharsetDecoder;->decode(Ljava/nio/ByteBuffer;)Ljava/nio/CharBuffer;

    move-result-object v6

    .line 759
    .local v6, "r":Ljava/nio/CharBuffer;
    invoke-virtual {v6}, Ljava/nio/CharBuffer;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/nio/charset/CharacterCodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    move-object v1, v0

    .line 772
    .end local v0    # "after":Ljava/lang/String;
    .end local v2    # "b":[B
    .end local v3    # "decoder":Ljava/nio/charset/CharsetDecoder;
    .end local v6    # "r":Ljava/nio/CharBuffer;
    :goto_0
    return-object v1

    .line 763
    .restart local v0    # "after":Ljava/lang/String;
    .restart local v2    # "b":[B
    .restart local v3    # "decoder":Ljava/nio/charset/CharsetDecoder;
    :catch_0
    move-exception v4

    .line 764
    .local v4, "e":Ljava/nio/charset/CharacterCodingException;
    :try_start_2
    new-instance v1, Ljava/lang/String;

    const-string v7, "UTF-8"

    invoke-direct {v1, v2, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_1

    .end local v0    # "after":Ljava/lang/String;
    .local v1, "after":Ljava/lang/String;
    move-object v0, v1

    .line 767
    .end local v1    # "after":Ljava/lang/String;
    .restart local v0    # "after":Ljava/lang/String;
    goto :goto_0

    .line 769
    .end local v2    # "b":[B
    .end local v3    # "decoder":Ljava/nio/charset/CharsetDecoder;
    .end local v4    # "e":Ljava/nio/charset/CharacterCodingException;
    :catch_1
    move-exception v5

    .line 770
    .local v5, "e1":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v5}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    move-object v1, v0

    .line 772
    .local v1, "after":Ljava/lang/Object;
    goto :goto_0
.end method

.method private expect(Ljava/lang/Class;)V
    .locals 3
    .param p1, "c"    # Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 776
    iget-object v0, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 777
    new-instance v0, Lorg/apache/james/mime4j/MimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Internal stack MimeException: Expected \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' found \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/james/mime4j/MimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 781
    :cond_0
    return-void
.end method


# virtual methods
.method public body(Ljava/io/InputStream;)V
    .locals 5
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 1032
    const-class v4, Lcom/android/emailcommon/internet/MimeMultipart;

    invoke-direct {p0, v4}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->expect(Ljava/lang/Class;)V

    .line 1033
    new-instance v3, Lcom/android/emailcommon/internet/BinaryTempFileBody;

    invoke-direct {v3}, Lcom/android/emailcommon/internet/BinaryTempFileBody;-><init>()V

    .line 1034
    .local v3, "tempBody":Lcom/android/emailcommon/internet/BinaryTempFileBody;
    invoke-virtual {v3}, Lcom/android/emailcommon/internet/BinaryTempFileBody;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    .line 1036
    .local v2, "out":Ljava/io/OutputStream;
    :try_start_0
    invoke-static {p1, v2}, Lorg/apache/commons/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    move-result v1

    .line 1037
    .local v1, "numCopied":I
    iget-object v4, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->this$0:Lcom/android/emailcommon/internet/MimeMessage;

    invoke-virtual {v4, v1}, Lcom/android/emailcommon/internet/MimeMessage;->setSize(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1042
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 1045
    .end local v1    # "numCopied":I
    :goto_0
    :try_start_1
    iget-object v4, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/emailcommon/internet/MimeMultipart;

    invoke-virtual {v4, v3}, Lcom/android/emailcommon/internet/MimeMultipart;->setBody(Lcom/android/emailcommon/mail/Body;)V
    :try_end_1
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1049
    return-void

    .line 1039
    :catch_0
    move-exception v4

    .line 1042
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    throw v4

    .line 1046
    :catch_1
    move-exception v0

    .line 1047
    .local v0, "me":Lcom/android/emailcommon/mail/MessagingException;
    new-instance v4, Lorg/apache/james/mime4j/MimeException;

    invoke-direct {v4, v0}, Lorg/apache/james/mime4j/MimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method

.method public body(Lorg/apache/james/mime4j/stream/BodyDescriptor;Ljava/io/InputStream;)V
    .locals 3
    .param p1, "bd"    # Lorg/apache/james/mime4j/stream/BodyDescriptor;
    .param p2, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 1022
    const-class v2, Lcom/android/emailcommon/mail/Part;

    invoke-direct {p0, v2}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->expect(Ljava/lang/Class;)V

    .line 1023
    invoke-interface {p1}, Lorg/apache/james/mime4j/stream/BodyDescriptor;->getTransferEncoding()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/android/emailcommon/internet/MimeUtility;->decodeBody(Ljava/io/InputStream;Ljava/lang/String;)Lcom/android/emailcommon/mail/Body;

    move-result-object v0

    .line 1025
    .local v0, "body":Lcom/android/emailcommon/mail/Body;
    :try_start_0
    iget-object v2, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/mail/Part;

    invoke-interface {v2, v0}, Lcom/android/emailcommon/mail/Part;->setBody(Lcom/android/emailcommon/mail/Body;)V
    :try_end_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1029
    return-void

    .line 1026
    :catch_0
    move-exception v1

    .line 1027
    .local v1, "me":Lcom/android/emailcommon/mail/MessagingException;
    new-instance v2, Lorg/apache/james/mime4j/MimeException;

    invoke-direct {v2, v1}, Lorg/apache/james/mime4j/MimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public endBodyPart()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 1068
    const-class v0, Lcom/android/emailcommon/mail/BodyPart;

    invoke-direct {p0, v0}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->expect(Ljava/lang/Class;)V

    .line 1069
    iget-object v0, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 1070
    return-void
.end method

.method public endHeader()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 977
    const-class v5, Lcom/android/emailcommon/mail/Part;

    invoke-direct {p0, v5}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->expect(Ljava/lang/Class;)V

    .line 978
    iget-object v5, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    invoke-virtual {v5}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/emailcommon/mail/Part;

    .line 979
    .local v3, "p":Lcom/android/emailcommon/mail/Part;
    instance-of v5, v3, Lcom/android/emailcommon/mail/Message;

    if-eqz v5, :cond_0

    .line 981
    if-nez v3, :cond_1

    .line 1006
    :cond_0
    :goto_0
    return-void

    .line 983
    :cond_1
    :try_start_0
    const-string v5, "Content-Type"

    invoke-interface {v3, v5}, Lcom/android/emailcommon/mail/Part;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 984
    .local v2, "headerData":[Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 986
    const/4 v5, 0x0

    aget-object v1, v2, v5

    .line 987
    .local v1, "data":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v5, "charset"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 988
    iget-object v5, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->this$0:Lcom/android/emailcommon/internet/MimeMessage;

    # invokes: Lcom/android/emailcommon/internet/MimeMessage;->getCharsetNamefromContentType(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v5, v1}, Lcom/android/emailcommon/internet/MimeMessage;->access$000(Lcom/android/emailcommon/internet/MimeMessage;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 989
    .local v4, "tempcharset":Ljava/lang/String;
    const/4 v0, 0x0

    .line 990
    .local v0, "charset":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 991
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/apache/james/mime4j/util/CharsetUtil;->toJavaCharset(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 993
    :cond_2
    const-string v5, "Email"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "endheder:org charset : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",javacharset="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 995
    if-nez v0, :cond_0

    if-eqz v4, :cond_0

    .line 996
    const-string v5, "charset"

    const-string v6, "MBP; charset"

    invoke-virtual {v1, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 997
    const-string v5, "UTF-8"

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 998
    const-string v5, "Content-Type"

    invoke-interface {v3, v5, v1}, Lcom/android/emailcommon/mail/Part;->setHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1001
    .end local v0    # "charset":Ljava/lang/String;
    .end local v1    # "data":Ljava/lang/String;
    .end local v2    # "headerData":[Ljava/lang/String;
    .end local v4    # "tempcharset":Ljava/lang/String;
    :catch_0
    move-exception v5

    goto :goto_0
.end method

.method public endMessage()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 801
    const-class v9, Lcom/android/emailcommon/internet/MimeMessage;

    invoke-direct {p0, v9}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->expect(Ljava/lang/Class;)V

    .line 802
    iget-object v9, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    invoke-virtual {v9}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/emailcommon/mail/Part;

    .line 803
    .local v6, "p":Lcom/android/emailcommon/mail/Part;
    instance-of v9, v6, Lcom/android/emailcommon/mail/Message;

    if-eqz v9, :cond_0

    .line 805
    :try_start_0
    const-string v9, "Content-Type"

    invoke-interface {v6, v9}, Lcom/android/emailcommon/mail/Part;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_1

    .line 963
    :cond_0
    :goto_0
    return-void

    .line 807
    :cond_1
    const-string v9, "Content-Type"

    invoke-interface {v6, v9}, Lcom/android/emailcommon/mail/Part;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    aget-object v2, v9, v10

    .line 808
    .local v2, "data":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 809
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 810
    .local v3, "datalow":Ljava/lang/String;
    const-string v9, "Email"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "content type : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    if-eqz v3, :cond_0

    .line 812
    const-string v9, "charset"

    invoke-virtual {v3, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    const-string v9, "MBP"

    invoke-virtual {v2, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 819
    iget-object v9, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->this$0:Lcom/android/emailcommon/internet/MimeMessage;

    # invokes: Lcom/android/emailcommon/internet/MimeMessage;->getCharsetNamefromContentType(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v9, v2}, Lcom/android/emailcommon/internet/MimeMessage;->access$000(Lcom/android/emailcommon/internet/MimeMessage;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 820
    .local v7, "tempcharset":Ljava/lang/String;
    const/4 v1, 0x0

    .line 821
    .local v1, "charset":Ljava/lang/String;
    if-eqz v7, :cond_2

    .line 822
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lorg/apache/james/mime4j/util/CharsetUtil;->toJavaCharset(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 824
    :cond_2
    const-string v9, "Email"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "endmessage:org charset : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ",javacharset="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/android/emailcommon/utility/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    if-nez v1, :cond_3

    .line 827
    move-object v1, v7

    .line 837
    :cond_3
    const-string v9, "From"

    invoke-interface {v6, v9}, Lcom/android/emailcommon/mail/Part;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 847
    .local v5, "from":[Ljava/lang/String;
    if-eqz v5, :cond_4

    if-eqz v1, :cond_4

    .line 849
    const/4 v9, 0x0

    aget-object v9, v5, v9

    invoke-virtual {v9, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 850
    const-string v9, "From"

    const/4 v10, 0x0

    aget-object v10, v5, v10

    invoke-direct {p0, v10, v1}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->convert2utf8(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v6, v9, v10}, Lcom/android/emailcommon/mail/Part;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 863
    :cond_4
    const-string v9, "To"

    invoke-interface {v6, v9}, Lcom/android/emailcommon/mail/Part;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 871
    .local v8, "to":[Ljava/lang/String;
    if-eqz v8, :cond_5

    if-eqz v1, :cond_5

    .line 873
    const/4 v9, 0x0

    aget-object v9, v8, v9

    invoke-virtual {v9, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 874
    const-string v9, "To"

    const/4 v10, 0x0

    aget-object v10, v8, v10

    invoke-direct {p0, v10, v1}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->convert2utf8(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v6, v9, v10}, Lcom/android/emailcommon/mail/Part;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 885
    :cond_5
    const-string v9, "CC"

    invoke-interface {v6, v9}, Lcom/android/emailcommon/mail/Part;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 893
    .local v0, "cc":[Ljava/lang/String;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 895
    const/4 v9, 0x0

    aget-object v9, v0, v9

    invoke-virtual {v9, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 896
    const-string v9, "CC"

    const/4 v10, 0x0

    aget-object v10, v0, v10

    invoke-direct {p0, v10, v1}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->convert2utf8(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v6, v9, v10}, Lcom/android/emailcommon/mail/Part;->setHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 955
    .end local v0    # "cc":[Ljava/lang/String;
    .end local v1    # "charset":Ljava/lang/String;
    .end local v2    # "data":Ljava/lang/String;
    .end local v3    # "datalow":Ljava/lang/String;
    .end local v5    # "from":[Ljava/lang/String;
    .end local v7    # "tempcharset":Ljava/lang/String;
    .end local v8    # "to":[Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 957
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 920
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v2    # "data":Ljava/lang/String;
    .restart local v3    # "datalow":Ljava/lang/String;
    :cond_6
    :try_start_1
    const-string v9, "From"

    invoke-interface {v6, v9}, Lcom/android/emailcommon/mail/Part;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 921
    .restart local v5    # "from":[Ljava/lang/String;
    if-eqz v5, :cond_7

    .line 922
    const/4 v9, 0x0

    aget-object v9, v5, v9

    const-string v10, "=?"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 924
    const/4 v9, 0x0

    aget-object v9, v5, v9

    invoke-static {v9}, Lorg/apache/james/mime4j/codec/DecoderUtil;->chardet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 925
    .restart local v1    # "charset":Ljava/lang/String;
    if-eqz v1, :cond_7

    .line 926
    const-string v9, "From"

    const/4 v10, 0x0

    aget-object v10, v5, v10

    invoke-direct {p0, v10, v1}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->convert2utf8(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v6, v9, v10}, Lcom/android/emailcommon/mail/Part;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 933
    .end local v1    # "charset":Ljava/lang/String;
    :cond_7
    const-string v9, "To"

    invoke-interface {v6, v9}, Lcom/android/emailcommon/mail/Part;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 934
    .restart local v8    # "to":[Ljava/lang/String;
    if-eqz v8, :cond_8

    .line 935
    const/4 v9, 0x0

    aget-object v9, v8, v9

    const-string v10, "=?"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_8

    .line 937
    const/4 v9, 0x0

    aget-object v9, v8, v9

    invoke-static {v9}, Lorg/apache/james/mime4j/codec/DecoderUtil;->chardet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 938
    .restart local v1    # "charset":Ljava/lang/String;
    if-eqz v1, :cond_8

    .line 939
    const-string v9, "To"

    const/4 v10, 0x0

    aget-object v10, v8, v10

    invoke-direct {p0, v10, v1}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->convert2utf8(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v6, v9, v10}, Lcom/android/emailcommon/mail/Part;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    .end local v1    # "charset":Ljava/lang/String;
    :cond_8
    const-string v9, "CC"

    invoke-interface {v6, v9}, Lcom/android/emailcommon/mail/Part;->getHeader(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 944
    .restart local v0    # "cc":[Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 945
    const/4 v9, 0x0

    aget-object v9, v0, v9

    const-string v10, "=?"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 947
    const/4 v9, 0x0

    aget-object v9, v0, v9

    invoke-static {v9}, Lorg/apache/james/mime4j/codec/DecoderUtil;->chardet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 948
    .restart local v1    # "charset":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 949
    const-string v9, "CC"

    const/4 v10, 0x0

    aget-object v10, v0, v10

    invoke-direct {p0, v10, v1}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->convert2utf8(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v6, v9, v10}, Lcom/android/emailcommon/mail/Part;->setHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public endMultipart()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 1052
    iget-object v0, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 1053
    return-void
.end method

.method public epilogue(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 1073
    const-class v0, Lcom/android/emailcommon/internet/MimeMultipart;

    invoke-direct {p0, v0}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->expect(Ljava/lang/Class;)V

    .line 1080
    return-void
.end method

.method public field(Lorg/apache/james/mime4j/stream/Field;)V
    .locals 5
    .param p1, "rawField"    # Lorg/apache/james/mime4j/stream/Field;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 1106
    const-class v2, Lcom/android/emailcommon/mail/Part;

    invoke-direct {p0, v2}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->expect(Ljava/lang/Class;)V

    .line 1108
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ":"

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    .line 1109
    .local v1, "tokens":[Ljava/lang/String;
    iget-object v2, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/mail/Part;

    const/4 v3, 0x0

    aget-object v3, v1, v3

    const/4 v4, 0x1

    aget-object v4, v1, v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/android/emailcommon/mail/Part;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1113
    return-void

    .line 1110
    .end local v1    # "tokens":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1111
    .local v0, "me":Lcom/android/emailcommon/mail/MessagingException;
    new-instance v2, Lorg/apache/james/mime4j/MimeException;

    invoke-direct {v2, v0}, Lorg/apache/james/mime4j/MimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public preamble(Ljava/io/InputStream;)V
    .locals 6
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 1083
    const-class v4, Lcom/android/emailcommon/internet/MimeMultipart;

    invoke-direct {p0, v4}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->expect(Ljava/lang/Class;)V

    .line 1084
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 1087
    .local v3, "sb":Ljava/lang/StringBuffer;
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    .local v0, "b":I
    const/4 v4, -0x1

    if-eq v0, v4, :cond_0

    .line 1088
    int-to-char v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1090
    .end local v0    # "b":I
    :catch_0
    move-exception v1

    .line 1091
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    new-instance v4, Lorg/apache/james/mime4j/MimeException;

    invoke-direct {v4, v1}, Lorg/apache/james/mime4j/MimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 1094
    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v0    # "b":I
    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/emailcommon/internet/MimeMultipart;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/emailcommon/internet/MimeMultipart;->setPreamble(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1098
    return-void

    .line 1095
    :catch_1
    move-exception v2

    .line 1096
    .local v2, "me":Lcom/android/emailcommon/mail/MessagingException;
    new-instance v4, Lorg/apache/james/mime4j/MimeException;

    invoke-direct {v4, v2}, Lorg/apache/james/mime4j/MimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method

.method public raw(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 1101
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public startBodyPart()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 1056
    const-class v2, Lcom/android/emailcommon/internet/MimeMultipart;

    invoke-direct {p0, v2}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->expect(Ljava/lang/Class;)V

    .line 1059
    :try_start_0
    new-instance v0, Lcom/android/emailcommon/internet/MimeBodyPart;

    invoke-direct {v0}, Lcom/android/emailcommon/internet/MimeBodyPart;-><init>()V

    .line 1060
    .local v0, "bodyPart":Lcom/android/emailcommon/internet/MimeBodyPart;
    iget-object v2, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/internet/MimeMultipart;

    invoke-virtual {v2, v0}, Lcom/android/emailcommon/internet/MimeMultipart;->addBodyPart(Lcom/android/emailcommon/mail/BodyPart;)V

    .line 1061
    iget-object v2, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    invoke-virtual {v2, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1065
    return-void

    .line 1062
    .end local v0    # "bodyPart":Lcom/android/emailcommon/internet/MimeBodyPart;
    :catch_0
    move-exception v1

    .line 1063
    .local v1, "me":Lcom/android/emailcommon/mail/MessagingException;
    new-instance v2, Lorg/apache/james/mime4j/MimeException;

    invoke-direct {v2, v1}, Lorg/apache/james/mime4j/MimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public startHeader()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 966
    const-class v0, Lcom/android/emailcommon/mail/Part;

    invoke-direct {p0, v0}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->expect(Ljava/lang/Class;)V

    .line 967
    return-void
.end method

.method public startMessage()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 784
    iget-object v2, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 785
    iget-object v2, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    iget-object v3, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->this$0:Lcom/android/emailcommon/internet/MimeMessage;

    invoke-virtual {v2, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 796
    :goto_0
    return-void

    .line 787
    :cond_0
    const-class v2, Lcom/android/emailcommon/mail/Part;

    invoke-direct {p0, v2}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->expect(Ljava/lang/Class;)V

    .line 789
    :try_start_0
    new-instance v0, Lcom/android/emailcommon/internet/MimeMessage;

    invoke-direct {v0}, Lcom/android/emailcommon/internet/MimeMessage;-><init>()V

    .line 790
    .local v0, "m":Lcom/android/emailcommon/internet/MimeMessage;
    iget-object v2, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/emailcommon/mail/Part;

    invoke-interface {v2, v0}, Lcom/android/emailcommon/mail/Part;->setBody(Lcom/android/emailcommon/mail/Body;)V

    .line 791
    iget-object v2, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    invoke-virtual {v2, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 792
    .end local v0    # "m":Lcom/android/emailcommon/internet/MimeMessage;
    :catch_0
    move-exception v1

    .line 793
    .local v1, "me":Lcom/android/emailcommon/mail/MessagingException;
    new-instance v2, Lorg/apache/james/mime4j/MimeException;

    invoke-direct {v2, v1}, Lorg/apache/james/mime4j/MimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public startMultipart(Lorg/apache/james/mime4j/stream/BodyDescriptor;)V
    .locals 3
    .param p1, "bd"    # Lorg/apache/james/mime4j/stream/BodyDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/james/mime4j/MimeException;
        }
    .end annotation

    .prologue
    .line 1009
    const-class v2, Lcom/android/emailcommon/mail/Part;

    invoke-direct {p0, v2}, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->expect(Ljava/lang/Class;)V

    .line 1011
    iget-object v2, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/emailcommon/mail/Part;

    .line 1013
    .local v0, "e":Lcom/android/emailcommon/mail/Part;
    :try_start_0
    new-instance v1, Lcom/android/emailcommon/internet/MimeMultipart;

    invoke-interface {v0}, Lcom/android/emailcommon/mail/Part;->getContentType()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/emailcommon/internet/MimeMultipart;-><init>(Ljava/lang/String;)V

    .line 1014
    .local v1, "multiPart":Lcom/android/emailcommon/internet/MimeMultipart;
    invoke-interface {v0, v1}, Lcom/android/emailcommon/mail/Part;->setBody(Lcom/android/emailcommon/mail/Body;)V

    .line 1015
    iget-object v2, p0, Lcom/android/emailcommon/internet/MimeMessage$MimeMessageBuilder;->stack:Ljava/util/Stack;

    invoke-virtual {v2, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/android/emailcommon/mail/MessagingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1019
    .end local v1    # "multiPart":Lcom/android/emailcommon/internet/MimeMultipart;
    :goto_0
    return-void

    .line 1016
    :catch_0
    move-exception v2

    goto :goto_0
.end method
