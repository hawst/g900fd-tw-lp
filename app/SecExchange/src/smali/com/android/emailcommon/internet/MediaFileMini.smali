.class public Lcom/android/emailcommon/internet/MediaFileMini;
.super Ljava/lang/Object;
.source "MediaFileMini.java"


# static fields
.field private static sFileTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/emailcommon/internet/MediaFileMini;->sFileTypeMap:Ljava/util/HashMap;

    .line 82
    const-string v0, "EML"

    const-string v1, "message/rfc822"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v0, "MP3"

    const-string v1, "audio/mpeg"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v0, "M4A"

    const-string v1, "audio/mp4"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v0, "WAV"

    const-string v1, "audio/x-wav"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v0, "AMR"

    const-string v1, "audio/amr"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string v0, "AWB"

    const-string v1, "audio/amr-wb"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v0, "WMA"

    const-string v1, "audio/x-ms-wma"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v0, "OGG"

    const-string v1, "audio/ogg"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v0, "OGA"

    const-string v1, "audio/ogg"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v0, "AAC"

    const-string v1, "audio/aac"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v0, "3GA"

    const-string v1, "audio/3gpp"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v0, "FLAC"

    const-string v1, "audio/flac"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-string v0, "MPGA"

    const-string v1, "audio/mpeg"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v0, "MP4_A"

    const-string v1, "audio/mp4"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v0, "3GP_A"

    const-string v1, "audio/3gpp"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v0, "3G2_A"

    const-string v1, "audio/3gpp2"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v0, "ASF_A"

    const-string v1, "audio/x-ms-asf"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    const-string v0, "3GPP_A"

    const-string v1, "audio/3gpp"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const-string v0, "MID"

    const-string v1, "audio/midi"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const-string v0, "XMF"

    const-string v1, "audio/midi"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v0, "MXMF"

    const-string v1, "audio/midi"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v0, "RTTTL"

    const-string v1, "audio/midi"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const-string v0, "SMF"

    const-string v1, "audio/sp-midi"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    const-string v0, "IMY"

    const-string v1, "audio/imelody"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v0, "MIDI"

    const-string v1, "audio/midi"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v0, "RTX"

    const-string v1, "audio/midi"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string v0, "OTA"

    const-string v1, "audio/midi"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string v0, "PYA"

    const-string v1, "audio/vnd.ms-playready.media.pya"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v0, "QCP"

    const-string v1, "audio/qcelp"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v0, "MPEG"

    const-string v1, "video/mpeg"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v0, "MPG"

    const-string v1, "video/mpeg"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v0, "MP4"

    const-string v1, "video/mp4"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v0, "M4V"

    const-string v1, "video/mp4"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v0, "3GP"

    const-string v1, "video/3gpp"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v0, "3GPP"

    const-string v1, "video/3gpp"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v0, "3G2"

    const-string v1, "video/3gpp2"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v0, "3GPP2"

    const-string v1, "video/3gpp2"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v0, "WMV"

    const-string v1, "video/x-ms-wmv"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const-string v0, "ASF"

    const-string v1, "video/x-ms-asf"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const-string v0, "AVI"

    const-string v1, "video/avi"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v0, "DIVX"

    const-string v1, "video/divx"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v0, "FLV"

    const-string v1, "video/flv"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v0, "MKV"

    const-string v1, "video/mkv"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string v0, "SDP"

    const-string v1, "application/sdp"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string v0, "MOV"

    const-string v1, "video/quicktime"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const-string v0, "PYV"

    const-string v1, "video/vnd.ms-playready.media.pyv"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v0, "WEBM"

    const-string v1, "video/webm"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v0, "JPG"

    const-string v1, "image/jpeg"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string v0, "JPEG"

    const-string v1, "image/jpeg"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const-string v0, "MY5"

    const-string v1, "image/vnd.tmo.my5"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v0, "GIF"

    const-string v1, "image/gif"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v0, "PNG"

    const-string v1, "image/png"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v0, "BMP"

    const-string v1, "image/x-ms-bmp"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const-string v0, "WBMP"

    const-string v1, "image/vnd.wap.wbmp"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string v0, "M3U"

    const-string v1, "audio/x-mpegurl"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    const-string v0, "PLS"

    const-string v1, "audio/x-scpls"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v0, "WPL"

    const-string v1, "application/vnd.ms-wpl"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v0, "PDF"

    const-string v1, "application/pdf"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v0, "RTF"

    const-string v1, "application/msword"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v0, "DOC"

    const-string v1, "application/msword"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v0, "GOLF"

    const-string v1, "image/*"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v0, "DOCX"

    const-string v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v0, "DOT"

    const-string v1, "application/msword"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const-string v0, "DOTX"

    const-string v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.template"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string v0, "CSV"

    const-string v1, "text/comma-separated-values"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    const-string v0, "XLS"

    const-string v1, "application/vnd.ms-excel"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const-string v0, "XLSX"

    const-string v1, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    const-string v0, "XLTX"

    const-string v1, "application/vnd.openxmlformats-officedocument.spreadsheetml.template"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string v0, "PPS"

    const-string v1, "application/vnd.ms-powerpoint"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const-string v0, "PPT"

    const-string v1, "application/vnd.ms-powerpoint"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const-string v0, "PPTX"

    const-string v1, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v0, "POT"

    const-string v1, "application/vnd.ms-powerpoint"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const-string v0, "POTX"

    const-string v1, "application/vnd.openxmlformats-officedocument.presentationml.template"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v0, "ASC"

    const-string v1, "text/plain"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v0, "TXT"

    const-string v1, "text/plain"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const-string v0, "GUL"

    const-string v1, "application/jungumword"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const-string v0, "EPUB"

    const-string v1, "application/epub+zip"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v0, "ACSM"

    const-string v1, "application/vnd.adobe.adept+xml"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v0, "SWF"

    const-string v1, "application/x-shockwave-flash"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const-string v0, "SVG"

    const-string v1, "image/svg+xml"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const-string v0, "DCF"

    const-string v1, "application/vnd.oma.drm.content"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const-string v0, "ODF"

    const-string v1, "application/vnd.oma.drm.content"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v0, "APK"

    const-string v1, "application/apk"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string v0, "JAD"

    const-string v1, "text/vnd.sun.j2me.app-descriptor"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const-string v0, "JAR"

    const-string v1, "application/java-archive "

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const-string v0, "VCS"

    const-string v1, "text/x-vCalendar"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v0, "ICS"

    const-string v1, "text/x-vCalendar"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const-string v0, "VTS"

    const-string v1, "text/x-vtodo"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string v0, "VCF"

    const-string v1, "text/x-vcard"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const-string v0, "VNT"

    const-string v1, "text/x-vnote"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v0, "HTML"

    const-string v1, "text/html"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v0, "HTM"

    const-string v1, "text/html"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v0, "XHTML"

    const-string v1, "text/html"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v0, "XML"

    const-string v1, "application/xhtml+xml"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string v0, "MHT"

    const-string v1, "multipart/related"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const-string v0, "MHTM"

    const-string v1, "multipart/related"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    const-string v0, "MHTML"

    const-string v1, "multipart/related"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v0, "WGT"

    const-string v1, "application/vnd.samsung.widget"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string v0, "HWP"

    const-string v1, "application/x-hwp"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v0, "ZIP"

    const-string v1, "application/zip"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const-string v0, "SNB"

    const-string v1, "application/snb"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const-string v0, "SSF"

    const-string v1, "application/ssf"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string v0, "WEBP"

    const-string v1, "image/webp"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    const-string v0, "SNBKP"

    const-string v1, "application/octet-stream"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    const-string v0, "SMBKP"

    const-string v1, "application/octet-stream"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const-string v0, "SPD"

    const-string v1, "application/spd"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    const-string v0, "SCC"

    const-string v1, "application/scc"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string v0, "PFX"

    const-string v1, "application/x-pkcs12"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const-string v0, "P12"

    const-string v1, "application/x-pkcs12"

    invoke-static {v0, v1}, Lcom/android/emailcommon/internet/MediaFileMini;->addFileType(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static addFileType(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "extension"    # Ljava/lang/String;
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 17
    sget-object v0, Lcom/android/emailcommon/internet/MediaFileMini;->sFileTypeMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    return-void
.end method

.method public static getExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 217
    if-nez p0, :cond_1

    .line 222
    :cond_0
    :goto_0
    return-object v1

    .line 219
    :cond_1
    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 220
    .local v0, "lastDot":I
    if-ltz v0, :cond_0

    .line 222
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getMimeTypeForView(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "extention"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 204
    if-nez p0, :cond_1

    .line 214
    :cond_0
    :goto_0
    return-object v0

    .line 206
    :cond_1
    invoke-static {p0}, Lcom/android/emailcommon/internet/MediaFileMini;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 207
    if-eqz p0, :cond_0

    .line 209
    sget-object v1, Lcom/android/emailcommon/internet/MediaFileMini;->sFileTypeMap:Ljava/util/HashMap;

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 211
    .local v0, "mediaType":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 212
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "application/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
